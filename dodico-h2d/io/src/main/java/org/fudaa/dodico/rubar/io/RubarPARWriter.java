/**
 * @creation 8 nov. 2004
 * @modification $Date: 2006-09-19 14:45:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import gnu.trove.TIntObjectHashMap;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.h2d.rubar.H2DRubarDicoParams;
import org.fudaa.dodico.h2d.rubar.H2dRubarDicoModel;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author Fred Deniger
 * @version $Id: RubarPARWriter.java,v 1.6 2006-09-19 14:45:52 deniger Exp $
 */
public class RubarPARWriter extends FileOpWriterCharSimpleAbstract {
  public RubarPARWriter() {
  }

  private void writeIntObject(final TIntObjectHashMap _m) {
    final String[] firstCol = H2dRubarDicoModel.getKeys(H2DRubarDicoParams.getNbTransportBlock(_m));
    final int[] keys = _m.keys();
    Arrays.sort(keys);
    final int size = keys.length;
    try {
      for (int i = 0; i < size; i++) {
        writelnToOut(firstCol[i] + (String) _m.get(keys[i]));
      }
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
  }

  @Override
  protected void internalWrite(final Object _o) {
    if (_o instanceof TIntObjectHashMap) {
      writeIntObject((TIntObjectHashMap) _o);
    } else {
      donneesInvalides(_o);
    }
  }
}
