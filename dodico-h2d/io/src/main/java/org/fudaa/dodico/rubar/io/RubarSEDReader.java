package org.fudaa.dodico.rubar.io;

import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.collection.CtuluListDouble;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentCouche;
import org.fudaa.dodico.h2d.rubar.H2dRubarSedimentCoucheContainer;

/**
 * Lecteur de fichier SED direct: stocke toutes les valeurs en m�moire.
 * @author CANEL Christophe (Genesis)
 */
public class RubarSEDReader extends FileOpReadCharSimpleAbstract {

  static final int[] FMT_COUCHES = new int[]{12, 10, 10, 15, 12, 10, 10, 15, 12, 10, 10, 15, 12, 10, 10, 15, 12, 10, 10, 15, 12, 10, 10, 15, 12,
    10, 10, 15, 12, 10, 10, 15, 12, 10, 10, 15, 12, 10, 10, 15};
  private int nbNoeuds = -1;
  
  @Override
  protected Object internalRead() {
    if (nbNoeuds <= 0) {
      analyze_.addFatalError("Le nombre de noeuds n'est pas pr�cis�");
    }
    in_.setBlankZero(true);
    
    final CtuluListDouble times = new CtuluListDouble();
    final List<H2dRubarSedimentCoucheContainer[]> values = new ArrayList<H2dRubarSedimentCoucheContainer[]>();
    
    try {
      final int[] fmtTime = getFormatTimeLine();
      //10 couches:
      final int[] fmtNoeud = FMT_COUCHES;
      
      while (true) {
        in_.readFields(fmtTime);
        times.add(in_.doubleField(0));
        H2dRubarSedimentCoucheContainer[] couches = new H2dRubarSedimentCoucheContainer[nbNoeuds];
        values.add(couches);
        //        final double[][] values = new double[4][nbNoeuds];
        for (int i = 0; i < this.nbNoeuds; i++) {
          couches[i] = new H2dRubarSedimentCoucheContainer();
          in_.readFields(fmtNoeud);
          int k = 0;
          for (int j = 0; j < couches[i].getNbCouches(); j++) {
            H2dRubarSedimentCouche coucheItem = couches[i].getCoucheItem(j);
            coucheItem.setEpaisseur(in_.doubleField(k++));
            coucheItem.setDiametre(in_.doubleField(k++));
            coucheItem.setEtendue(in_.doubleField(k++));
            coucheItem.setContrainte(in_.doubleField(k++));
          }
        }
        
      }
    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Fin du fichier");
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
      return null;
    } catch (final NumberFormatException e) {
      analyze_.manageException(e, in_.getLineNumber());
      return null;
    }
    
    final RubarSEDResult r = new RubarSEDResult();
    final int nbTimes = times.getSize();
    
    r.time = times.getValues();
    r.values = new H2dRubarSedimentCoucheContainer[nbTimes][nbNoeuds];
    for (int i = 0; i < nbTimes; i++) {
      r.values[i] = values.get(i);
    }
    
    return r;
  }

  /**
   * @return le nombre de noeuds � lire
   */
  public int getNbNoeuds() {
    return nbNoeuds;
  }

  /**
   * @param nbNoeuds le nombre de noeuds � lire
   */
  public void setNbNoeuds(final int nbNoeuds) {
    this.nbNoeuds = nbNoeuds;
  }

  public static int[] getFormatTimeLine() {
    return new int[]{15};
  }
}
