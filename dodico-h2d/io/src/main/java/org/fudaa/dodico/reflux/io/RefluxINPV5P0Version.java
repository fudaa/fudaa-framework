package org.fudaa.dodico.reflux.io;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoEntiteGenerate;
import org.fudaa.dodico.dico.DicoEntiteList;
import org.fudaa.dodico.dico.DicoLanguage;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.h2d.reflux.H2dRefluxDicoModel;
import org.fudaa.dodico.h2d.reflux.H2dRefluxDicoVersion;
import org.fudaa.dodico.h2d.reflux.H2dRefluxNodalPropertiesMng;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dProjetType;
import org.fudaa.dodico.h2d.type.H2dRefluxBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * Derniere version.
 * 
 * @author Fred Deniger
 * @version $Id: RefluxINPV5P0Version.java,v 1.3 2007-06-29 15:19:38 deniger Exp $
 */
public class RefluxINPV5P0Version extends H2dRefluxDicoVersion {

  DicoEntite[] entiteContrib_;

  /**
   * @param _version id version.
   */
  public RefluxINPV5P0Version(final String _version) {
    super(INPFileFormat.getInstance(), _version, false);
  }

  protected RefluxINPV5P0Version(final String _version, boolean _containContribSrc) {
    super(INPFileFormat.getInstance(), _version, _containContribSrc);
  }

  public H2dRefluxDicoModel getRefluxDico() {
    return (H2dRefluxDicoModel) super.getDico();
  }

  private void buildListContribution() {
    String rubrique = "Coefficients contribution";
    if (DicoLanguage.getCurrentID() == DicoLanguage.ENGLISH_ID) {
      rubrique = "Contribution coefficients";
    }
    final List l = new ArrayList(18);
    DicoEntite ent;
    final DicoEntiteList entites = getRefluxModel().getEntites();
    for (final Iterator it = entites.iterator(); it.hasNext();) {
      ent = (DicoEntite) it.next();
      if (rubrique.equals(ent.getRubrique())) {
        l.add(ent);
      }
    }
    Collections.sort(l, DicoEntiteGenerate.getComparatorIndex());
    entiteContrib_ = new DicoEntite[l.size()];
    l.toArray(entiteContrib_);
  }

  public CLVReader createCLVReader() {
    return CLTransitoireFileFormat.getInstance().createCLVReader();
  }

  /**
   * @return le writer
   */
  public CLVWriter createCLVWriter() {
    return CLTransitoireFileFormat.getInstance().createCLVWriter();
  }

  /**
   * @return le reader inp.
   */
  public INPReaderAbstract createINPReader() {
    return new INPReaderV5P0(this);
  }

  /**
   * @return le writer inp.
   */
  public INPWriterAbstract createINPWriter() {
    return new INPWriterV5P0(this);
  }

  @Override
  public final FileReadOperationAbstract createReader() {
    return createINPReader();
  }

  @Override
  public final FileWriteOperationAbstract createWriter() {
    return createINPWriter();
  }

  /**
   * @return format
   */
  public int[] getBlocSimpleFormat() {
    return new int[] { 5 };
  }

  /**
   * @param _id l'identifiant inp
   * @return le type de bord correspondant.
   */
  public H2dBoundaryType getBordType(final int _id) {
    if (_id == getCodeBordLiquide()) {
      return H2dRefluxBoundaryType.LIQUIDE;
    } else if (_id == getCodeBordLiquideDebit()) {
      return H2dRefluxBoundaryType.LIQUIDE_DEBIT_IMPOSE;
    } else if (_id == getCodeBordSolideFrottement()) {
      return H2dRefluxBoundaryType.SOLIDE;
    }
    return null;
  }

  /**
   * @param _id le type du bord
   * @return l'id inp
   */
  public int getBordTypeId(final H2dBoundaryType _id) {
    if (_id == H2dRefluxBoundaryType.LIQUIDE_DEBIT_IMPOSE) {
      return getCodeBordLiquideDebit();
    } else if (_id.isLiquide()) {
      return getCodeBordLiquide();
    } else if ((_id == H2dRefluxBoundaryType.SOLIDE) || (_id == H2dRefluxBoundaryType.SOLIDE_FROTTEMENT)) {
      return getCodeBordSolideFrottement();
    }
    return -1;
  }

  /**
   * @param _f le tableau a parcourir
   * @return le fichier contant l'extension clv
   */
  @Override
  public String getCLTransitoireFichier(final String[] _f) {
    return CtuluLibString.findStringEndWith(_f, getClTransitoireFileExtension());
  }

  /**
   * @return l'extension des fichiers cl transitoire utilises
   */
  @Override
  public String getClTransitoireFileExtension() {
    return "clv";
  }

  /**
   * @param _id l'identifiant inp
   * @return type equivalent ou null si inconnu
   */
  public H2dBcType getClType(final int _id) {
    if (_id == getCodeLibre()) {
      return H2dBcType.LIBRE;
    } else if (_id == getCodePermanent()) {
      return H2dBcType.PERMANENT;
    } else if (_id == getCodeTransitoire()) {
      return H2dBcType.TRANSITOIRE;
    }
    return null;
  }

  /**
   * @param _t le type
   * @return l'id inp correspondant.-1 si inconnu.
   */
  public int getCode(final H2dBcType _t) {
    if (_t == H2dBcType.LIBRE) {
      return getCodeLibre();
    } else if (_t == H2dBcType.PERMANENT) {
      return getCodePermanent();
    } else if (_t == H2dBcType.TRANSITOIRE) {
      return getCodeTransitoire();
    }
    return -1;
  }

  /**
   * @return format
   */
  public int getCodeBordLiquide() {
    return 6;
  }

  /**
   * @return format
   */
  public int getCodeBordLiquideDebit() {
    return 7;
  }

  /**
   * @return format
   */
  public int getCodeBordSolideFrottement() {
    return 8;
  }

  /**
   * @return format
   */
  public int getCodeFin() {
    return 0;
  }

  /**
   * @return format
   */
  public int getCodeFond() {
    return 5;
  }

  /**
   * @return format
   */
  public int getCodeLibre() {
    return 0;
  }

  /**
   * @return format
   */
  public int getCodePermanent() {
    return 2;
  }

  /**
   * @return format
   */
  public int getCodeTransitoire() {
    return 1;
  }

  /**
   * @param _index l'index demande.
   * @return le mot-cle correspondant au coef de contribution
   */
  public DicoEntite getCoefContribution(final int _index) {
    if (entiteContrib_ == null) {
      buildListContribution();
    }
    if (_index < 0 || _index >= entiteContrib_.length) {
      new Throwable("index must belong to the interval 0," + entiteContrib_.length).printStackTrace();
      return null;
    }
    return entiteContrib_[_index];
  }

  /**
   * @return nb de coefficient de contribution.
   */
  @Override
  public int getCoefContributionNb() {
    if (entiteContrib_ == null) {
      buildListContribution();
    }
    return entiteContrib_.length;
  }

  /**
   * @return format
   */
  public int[] getCoefficientContributionFormat() {
    return new int[] { 20, 10, 10, 10, 10, 10, 10 };
  }

  /**
   * @return format
   */
  public String getCOND() {
    return "COND";
  }

  /**
   * @return format
   */
  public int[] getCONDCodeFormat() {
    return new int[] { 5, 5, 5 };
  }

  /**
   * @return COOR
   */
  public String getCOOR() {
    return "COOR";
  }

  /**
   * @return le format concernant la ligne declarant le bloc Coor.
   */
  public int[] getCOORFormat() {
    return new int[] { 5, 15, 5, 5 };
  }

  /**
   * @return format
   */
  public String getCRAD() {
    return "BLOC: CRAD";
  }

  /**
   * @return format
   */
  public int[] getCRADFormat() {
    return new int[] { 15, 5 };
  }

  /**
   * @return BLOC: DATA
   */
  public String getDATA() {
    return "BLOC: DATA";
  }

  /**
   * @return format
   */
  public String getDLPN() {
    return "DLPN";
  }

  /**
   * @return 3
   */
  public int getDLPNDllPointExtremite() {
    return 3;
  }

  /**
   * @return 2
   */
  public int getDLPNDllPointMilieu() {
    return 2;
  }

  /**
   * @return format
   */
  public int[] getDLPNFormat() {
    return new int[] { 5, 15, 5 };
  }

  /**
   * @return format
   */
  public String getELEM() {
    return "ELEM";
  }

  /**
   * @return format
   */
  public int[] getELEMFormat() {
    return new int[] { 5, 15, 5, 5 };
  }

  /**
   * @return format
   */
  public int getELEMTailleBord() {
    return 3;
  }

  /**
   * @return format
   */
  public int getELEMTailleFond() {
    return 6;
  }

  /**
   * @return format
   */
  public int[] getELEMValeurFormat() {
    return new int[] { 5, 5, 5, 5, 5, 5 };
  }

  /**
   * @return sep entete
   */
  public String getEnteteSeparateur() {
    return " ===============================================================================";
  }

  /**
   * @return entete start
   */
  public String getENTETEVersionLineStart() {
    return "Cree par";
  }

  /**
   * @return REFLUX
   */
  public String getENTETEVersionPrefixe() {
    return "REFLUX";
  }

  /**
   * @return format
   */
  public String getFICHIER() {
    return "FICHIER:";
  }

  @Override
  public String getFileExtensionForRadiation(final H2dProjetType _proj) {
    return "crv";
  }

  @Override
  public String getFileExtensionForVent(final H2dProjetType _proj) {
    return "vtv";
  }

  /**
   * @return format
   */
  public String getFLUT() {
    return "BLOC: FLUT";
  }

  /**
   * @return COEFFICIENTS
   */
  public String getFLUTCoefficientContribution() {
    return "COEFFICIENTS";
  }

  /**
   * @return format
   */
  public int[] getFLUTFreqImpressionFormat() {
    return new int[] { 15, 10 };
  }

  /**
   * @return IMPRESSION
   */
  public String getFLUTImpression() {
    return "IMPRESSION";
  }

  /**
   * @return format flut methode
   */
  public int[] getFLUTMethodeResolutionFormat() {
    return new int[] { 20, 10, 10, 10, 5 };
  }

  /**
   * @return format flut
   */
  public int[] getFLUTNbPasTemps() {
    return new int[] { 10, 10 };
  }

  /**
   * @return format flut schema
   */
  public int[] getFLUTSchemaResolutionFormat() {
    return new int[] { 20, 10, 10 };
  }

  @Override
  public String getForceFileExtension(final H2dVariableType _v, final H2dProjetType _proj) {
    if (_v == H2dRefluxNodalPropertiesMng.getVentVar()) {
      return getFileExtensionForVent(_proj);
    } else if (_v == H2dRefluxNodalPropertiesMng.getRadiationVar()) {
      return getFileExtensionForRadiation(_proj);
    }
    return null;
  }

  /**
   * @return format
   */
  public int[] getHuitDoubleFormat() {
    return new int[] { 10, 10, 10, 10, 10, 10, 10, 10 };
  }

  /**
   * @return format
   */
  public String getIMPRESSION() {
    return "IMPRESSION:";
  }

  public int getNbDigitsForCRADFile() {
    return H2dRefluxDicoVersion.getDoubleColSize();
  }

  /**
   * @return 10
   */
  public int getNbMaxFichiers() {
    return 10;
  }

  /**
   * @param _f le tableau a parcourir
   * @return le fichier contant l'extension pnv
   */
  @Override
  public String getPnTransitoireFichier(final String[] _f) {
    return CtuluLibString.findStringEndWith(_f, getPnTransitoireFileExtension());
  }

  /**
   * @return "pnv"
   */
  @Override
  public String getPnTransitoireFileExtension() {
    return "pnv";
  }

  /**
   * @param _v la variable
   * @return la position dans le tableau des cl du fichier inp.
   */
  public int getPositionForCond(final H2dVariableType _v) {
    if (_v == H2dVariableType.VITESSE_U) {
      return 0;
    } else if (_v == H2dVariableType.VITESSE_V) {
      return 1;
    } else if (_v == H2dVariableType.COTE_EAU) {
      return 2;
    }
    return -1;
  }

  /**
   * @return format
   */
  public String getPREL() {
    return "PREL";
  }

  /**
   * @return format
   */
  public int[] getPRELFormat() {
    return new int[] { 5, 15, 5, 5 };
  }

  /**
   * @return les mot-cl�s impression
   */
  @Override
  public DicoEntite[] getPrintEntite() {
    return getEntitesForHeading(new String[] { "Impression", "Print" });
  }

  /**
   * @return format
   */
  public String getPRND() {
    return "PRND";
  }

  /**
   * @return format
   */
  public int[] getPRNDFormat() {
    return new int[] { 5, 15, 5 };
  }

  @Override
  public String getRadiation(final String[] _f, final H2dProjetType _proj) {
    return CtuluLibString.findStringEndWith(_f, getFileExtensionForRadiation(_proj));
  }

  /**
   * @return le modele des mot-cles.
   */
  @Override
  public H2dRefluxDicoModel getRefluxModel() {
    return (H2dRefluxDicoModel) super.getDico();
  }

  /**
   * @return format
   */
  public int[] getCONDNoeudIdxFormat() {
    return new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
  }

  public int[] getGroupElemCodeFormat() {
    return new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
  }

  @Override
  public FileFormatSoftware getSoftVersion() {
    final FileFormatSoftware r = new FileFormatSoftware(FileFormatSoftware.REFLUX_IS);
    r.soft_ = getCodeName();
    r.language_ = super.getDico().getLanguage();
    r.version_ = getDico().getVersion();
    return r;
  }

  /**
   * @return format
   */
  public String getSOLR() {
    return "BLOC: SOLR";
  }

  /**
   * @return format
   */
  public int[] getSOLRFormat() {
    return new int[] { 15, 5 };
  }

  /**
   * @param _f le tableau de recherche
   * @return l'element du tableau contenant la bonne extension
   */
  public String getSolution(final String[] _f) {
    return CtuluLibString.findStringEndWith(_f, getSolutionFileExtension());
  }

  /**
   * @return "sov"
   */
  @Override
  public String getSolutionFileExtension() {
    return "sov";
  }

  /**
   * @return "sfv"
   */
  @Override
  public String getSolutionFinaleExtension() {
    return "sfv";
  }

  /**
   * @return "siv"
   */
  @Override
  public String getSolutionInitFileExtension() {
    return "siv";
  }

  /**
   * @param _f le tableau de recherche
   * @return l'element du tableau contenant la bonne extension
   */
  @Override
  public String getSolutionInitiales(final String[] _f) {
    return CtuluLibString.findStringEndWith(_f, getSolutionInitFileExtension());
  }

  /**
   * @return format
   */
  public String getSTOP() {
    return "STOP";
  }

  /**
   * @return format
   */
  public String getTPEL() {
    return "TPEL";
  }

  /**
   * @return format
   */
  public String getTRANS() {
    return "BLOC: TRANS";
  }

  public int[] getTypeProjetFormat() {
    return new int[] { 15, 5, 5 };
  }

  /**
   * @return le format concernant les lignes declarant les coordonn�es.
   */
  public int[] getTroisDoubleFormat() {
    return new int[] { 10, 10, 10 };
  }

  @Override
  public String getVent(final String[] _f, final H2dProjetType _proj) {
    return CtuluLibString.findStringEndWith(_f, getFileExtensionForVent(_proj));
  }

  /**
   * @return format
   */
  public String getVENT() {
    return "BLOC: VENT";
  }

  /**
   * @return format
   */
  public int[] getVENTFormat() {
    return new int[] { 15, 5 };
  }

  /**
   * Surcharge pour indique au lecteur de format INP de ne lire que le maillage.
   */
  @Override
  public CtuluIOOperationSynthese readGrid(final File _f, final ProgressionInterface _prog) {
    final INPReaderAbstract i = createINPReader();
    i.setReadOnlyGrid(true);
    final CtuluIOOperationSynthese r = i.read(_f, _prog);
    i.setProgressReceiver(null);
    return r;
  }

  /**
   *
   */
  public CtuluIOOperationSynthese readListPoint(final File _f, final ProgressionInterface _prog) {
    final CtuluIOOperationSynthese s = read(_f, _prog);
    s.setSource(((INPInterface) s.getSource()).getGrid());
    return s;
  }

  /**
   * @param _f le fichier cible
   * @param _inter la source
   * @param _prog la barre de progression
   * @return la synthese de l'operation
   */
  public CtuluIOOperationSynthese write(final File _f, final INPInterface _inter, final ProgressionInterface _prog) {
    final INPWriterAbstract w = createINPWriter();
    w.setFile(_f);
    w.setProgressReceiver(_prog);
    return w.write(_inter);
  }

  @Override
  public CtuluIOOperationSynthese writeGrid(final File _f, final EfGridSource _m, final ProgressionInterface _prog) {
    return null;
  }

  @Override
    public boolean canReadGrid()
    {
        return true;
    }
    
  @Override
    public boolean canWriteGrid()
    {
        return true;
    }
    
  @Override
    public boolean hasBoundaryConditons()
    {
        // TODO Voir si correct
        return false;
    }

}