/**
 * @creation 22 d�c. 2004 @modification $Date: 2006-09-19 14:45:52 $ @license GNU General Public License 2 @copyright (c)1998-2001 CETMEF 2 bd
 *     Gambetta F-60231 Compiegne @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.dodico.h2d.H2dEvolutionVariableMap;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireBrecheI;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageElementaireInterface;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.util.Set;

/**
 * @author Fred Deniger
 * @version $Id: RubarOuvrageElementaireBreche.java,v 1.4 2006-09-19 14:45:52 deniger Exp $
 */
public class RubarOuvrageElementaireBreche implements H2dRubarOuvrageElementaireBrecheI {
  int brecheType;
  double coefficientErosionLineaire;
  double debutTemps;
  double coteCrete;
  double cotePied;
  double largeurCrete;
  double largeurPied;
  double diametreMedian;
  double coeffStrickler;
  double masseVolumiqueGrain;
  double porosite;
  double coteFond;
  double dimensionInitiale;
  double pasDeTemps;
  double coeffPerteDeCharge;
  int indicateur;
  int nbPasDeTempsMax;

  @Override
  public int getBrecheType() {
    return brecheType;
  }

  @Override
  public double getDebutTemps() {
    return debutTemps;
  }

  @Override
  public void evolutionChanged(EvolutionReguliereInterface _e) {
  }

  @Override
  public double getCoteCrete() {
    return coteCrete;
  }

  @Override
  public double getCoefficientErosionLineaire() {
    return coefficientErosionLineaire;
  }

  @Override
  public void setProjetType(final H2dRubarProjetType projet, final int nbTransportBlock, final CtuluCommandComposite cmd) {
  }

  @Override
  public double getCotePied() {
    return cotePied;
  }

  @Override
  public double getLargeurCrete() {
    return largeurCrete;
  }

  @Override
  public double getLargeurPied() {
    return largeurPied;
  }

  @Override
  public double getDiametreMedian() {
    return diametreMedian;
  }

  @Override
  public double getCoeffStrickler() {
    return coeffStrickler;
  }

  @Override
  public double getMasseVolumiqueGrain() {
    return masseVolumiqueGrain;
  }

  @Override
  public double getPorosite() {
    return porosite;
  }

  @Override
  public double getCoteFond() {
    return coteFond;
  }

  @Override
  public double getDimensionInitiale() {
    return dimensionInitiale;
  }

  @Override
  public void fillWithTransientCurves(String pref, H2dEvolutionVariableMap r) {
  }

  @Override
  public void fillWithTarageEvolution(String prefix, Set allTarage) {

  }

  @Override
  public boolean containsEvolution(EvolutionReguliereInterface tarage) {
    return false;
  }

  @Override
  public double getPasDeTemps() {
    return pasDeTemps;
  }

  @Override
  public double getCoeffPerteDeCharge() {
    return coeffPerteDeCharge;
  }

  @Override
  public int getRenardSurverseId() {
    return indicateur;
  }

  @Override
  public int getNbPasDeTempsMax() {
    return nbPasDeTempsMax;
  }

  @Override
  public H2dRubarOuvrageElementaireInterface getCopyWithoutEvt() {
    return null;
  }

  @Override
  public H2dRubarOuvrageType getType() {
    return H2dRubarOuvrageType.BRECHE;
  }

  public final double[] getValues() {
    double[] values = new double[NB_VALUES];

    values[0] = debutTemps;
    values[1] = coteCrete;
    values[2] = cotePied;
    values[3] = largeurCrete;
    values[4] = largeurPied;
    values[5] = diametreMedian;
    values[6] = coeffStrickler;
    values[7] = masseVolumiqueGrain;
    values[8] = porosite;
    values[9] = coteFond;
    values[10] = dimensionInitiale;
    values[11] = pasDeTemps;
    values[12] = coeffPerteDeCharge;
    values[13] = coefficientErosionLineaire;

    return values;
  }

  @Override
  public double getValue(final int _idx) {
    switch (_idx) {
      case 0: {
        return debutTemps;
      }
      case 1: {
        return coteCrete;
      }
      case 2: {
        return cotePied;
      }
      case 3: {
        return largeurCrete;
      }
      case 4: {
        return largeurPied;
      }
      case 5: {
        return diametreMedian;
      }
      case 6: {
        return coeffStrickler;
      }
      case 7: {
        return masseVolumiqueGrain;
      }
      case 8: {
        return porosite;
      }
      case 9: {
        return coteFond;
      }
      case 10: {
        return dimensionInitiale;
      }
      case 11: {
        return pasDeTemps;
      }
      case 12: {
        return coeffPerteDeCharge;
      }
      case 13: {
        return coefficientErosionLineaire;
      }
    }
    throw new IndexOutOfBoundsException();
  }
}
