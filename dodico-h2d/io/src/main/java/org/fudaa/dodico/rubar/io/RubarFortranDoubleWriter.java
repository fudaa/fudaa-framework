package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;
import org.fudaa.dodico.fortran.FortranDoubleReaderResultInterface;
import org.fudaa.dodico.fortran.FortranDoubleWriter;

import java.io.File;
import java.util.Arrays;

public class RubarFortranDoubleWriter extends FileWriteOperationAbstract {
  private final int[] baseFmt;
  private final int sizeColumnToAdd;
  private FortranDoubleWriter doubleWriter;

  public RubarFortranDoubleWriter(int[] baseFmt, int sizeColumnToAdd) {
    this.baseFmt = baseFmt;
    this.sizeColumnToAdd = sizeColumnToAdd;
  }

  @Override
  protected void internalWrite(Object _o) {
    FortranDoubleReaderResultInterface data = (FortranDoubleReaderResultInterface) _o;
    int nbColumn = data.getNbColonne();
    int[] fmtToUse = baseFmt;
    if (nbColumn > baseFmt.length) {
      fmtToUse = new int[nbColumn];
      Arrays.fill(fmtToUse, sizeColumnToAdd);
      for (int i = 0; i < baseFmt.length; i++) {
        fmtToUse[i] = baseFmt[i];
      }
    }
    doubleWriter.setFmt(fmtToUse);
    doubleWriter.internalWrite(data);
  }

  @Override
  protected FortranInterface getFortranInterface() {
    return doubleWriter.getFortranInterface();
  }

  @Override
  public void setFile(File _f) {
    doubleWriter = new FortranDoubleWriter(null);
    doubleWriter.setFile(_f);
    analyze_ = doubleWriter.getAnalyze();
  }
}
