package org.fudaa.dodico.rubar.io;

import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

import java.util.Arrays;
import java.util.List;

public class RubarRESResultLine {
    private int heure;
    private int minute;
    private int second;
    private double coteEauAmont;
    private double largeurDiametreBreche;
    private double coteFondBreche;
    private double debitLiquide;
    private double debitSolide;

    public RubarRESResultLine(int heure, int minute, int second, double coteEauAmont, double coteFondBreche, double largeurDiametreBreche,
                              double debitLiquide, double debitSolide) {
        this.heure = heure;
        this.minute = minute;
        this.second = second;
        this.coteEauAmont = coteEauAmont;
        this.largeurDiametreBreche = largeurDiametreBreche;
        this.coteFondBreche = coteFondBreche;
        this.debitLiquide = debitLiquide;
        this.debitSolide = debitSolide;
    }

    public double getTime() {
        return second + minute * 60 + heure * 3600;
    }

    public static List<H2dVariableType> getVariables() {
        return Arrays.asList(H2dVariableTransType.COTE_EAU_AMONT, H2dVariableTransType.LARGEUR_DIAMETRE_BRECHE,
            H2dVariableTransType.COTE_FOND_BRECHE, H2dVariableTransType.DEBIT_LIQUIDE, H2dVariableTransType.DEBIT_SOLIDE);
    }

    public double getValue(H2dVariableType var) {
        if (H2dVariableTransType.COTE_EAU_AMONT.equals(var)) {
            return coteEauAmont;
        }
        if (H2dVariableTransType.LARGEUR_DIAMETRE_BRECHE.equals(var)) {
            return largeurDiametreBreche;
        }
        if (H2dVariableTransType.COTE_FOND_BRECHE.equals(var)) {
            return coteFondBreche;
        }
        if (H2dVariableTransType.DEBIT_LIQUIDE.equals(var)) {
            return debitLiquide;
        }
        if (H2dVariableTransType.DEBIT_SOLIDE.equals(var)) {
            return debitSolide;
        }
        throw new IllegalAccessError("var not supported");
    }

    /**
     * @return the heure
     */
    public int getHeure() {
        return heure;
    }

    /**
     * @return the minute
     */
    public int getMinute() {
        return minute;
    }

    /**
     * @return the second
     */
    public int getSecond() {
        return second;
    }

    /**
     * @return the coteEauAmont
     */
    public double getCoteEauAmont() {
        return coteEauAmont;
    }

    /**
     * @return the largeurDiametreBreche
     */
    public double getLargeurDiametreBreche() {
        return largeurDiametreBreche;
    }

    /**
     * @return the coteFondBreche
     */
    public double getCoteFondBreche() {
        return coteFondBreche;
    }

    /**
     * @return the debitLiquide
     */
    public double getDebitLiquide() {
        return debitLiquide;
    }

    /**
     * @return the debitSolide
     */
    public double getDebitSolide() {
        return debitSolide;
    }
}
