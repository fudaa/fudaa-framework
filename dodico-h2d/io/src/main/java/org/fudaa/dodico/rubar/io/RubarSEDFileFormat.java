package org.fudaa.dodico.rubar.io;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 *
 * @author CANEL Christophe (Genesis)
 *
 */
public class RubarSEDFileFormat extends FileFormatUnique {

  /**
   * Initialise les donnees.
   */
  public RubarSEDFileFormat() {
    super(1);
    extensions_ = new String[]{"sed"};
    super.description_ = H2dResource.getS("Description des couches sÚdimentaires");
    super.id_ = "RUBAR_SED";
    super.nom_ = "Rubar sed";
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createReader()
   */
  @Override
  public FileReadOperationAbstract createReader() {
    return new RubarSEDReaderDirect();
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createWriter()
   */
  @Override
  public FileWriteOperationAbstract createWriter() {
    return new RubarSEDWriter();
  }

  /**
   * @param f
   * @param prog
   * @param nbNoeuds
   * @return la synthese de l'operation
   */
  public CtuluIOOperationSynthese read(final File f, final ProgressionInterface prog, final int nbNoeuds) {
    final RubarSEDReaderDirect i = new RubarSEDReaderDirect();
    i.setNbNoeuds(nbNoeuds);
    final CtuluIOOperationSynthese r = i.read(f, prog);
    i.setProgressReceiver(null);
    return r;
  }
}