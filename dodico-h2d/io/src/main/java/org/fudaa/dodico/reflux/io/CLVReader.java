/**
 * @creation 28 ao�t 2003
 * @modification $Date: 2006-09-19 14:45:47 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;

import org.fudaa.dodico.fortran.FortranBinaryInputStream;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.mesure.EvolutionReguliere;

/**
 * @author deniger
 * @version $Id: CLVReader.java,v 1.15 2006-09-19 14:45:47 deniger Exp $
 */
public class CLVReader extends FileReadOperationAbstract {

  BufferedInputStream in_;
  EvolutionReguliere[] evols_;
  FileFormatVersionInterface version_;
  long nbOctets_;

  /**
   * @param _version la version utilisee
   */
  public CLVReader(final FileFormatVersionInterface _version) {
    version_ = _version;
  }

  /**
   *
   */
  @Override
  protected Object internalRead() {
    if (evols_ == null) {
      analyze_.addWarn(H2dResource.getS("Le nombre de courbe sera evalu�"), -1);
    }
    try {
      FortranBinaryInputStream in = null;
      if (evols_ == null) {
        in = new FortranBinaryInputStream(in_, false);
        in_.mark(8);
        final int nbPasTemps = in.readInteger();
        setEvolutionsNb((int) (nbOctets_ / (16 * (nbPasTemps + 1)) - 1));
        in_.reset();
      }
      if (evols_ == null) {
        analyze_.addFatalError(H2dResource.getS("Le nombre de courbes est inconnue"));
        return null;
      }
      in = new FortranBinaryInputStream(in_, false);
      final int nbEvol = evols_.length;
      in.setRecordLength((nbEvol + 1) * 4);
      in.readRecord();
      final int nbPasTemps = in.readInteger();
      for (int i = nbEvol - 1; i >= 0; i--) {
        evols_[i].ensureCapacity(nbPasTemps);
      }
      double t;
      final ProgressionUpdater up = new ProgressionUpdater(progress_);
      up.majProgessionStateOnly();
      up.setValue(nbPasTemps, 4);
      for (int i = 0; i < nbPasTemps; i++) {
        in.readRecord();
        t = in.readDoublePrecision();
        for (int j = 0; j < nbEvol; j++) {
          final double d = in.readDoublePrecision();
          evols_[j].add(t, d);
        }
        up.majAvancement();
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
    return evols_;
  }

  @Override
  protected FortranInterface getFortranInterface() {
    return new FortranInterface() {

      @Override
      public void close() throws IOException {
        if (in_ != null) {
          in_.close();
        }
      }
    };
  }

  @Override
  public void setFile(final File _f) {
    nbOctets_ = _f.length();
    analyze_ = new CtuluAnalyze();
    try {
      in_ = new BufferedInputStream(new FileInputStream(_f));
    } catch (final FileNotFoundException e) {
      analyze_.manageException(e);
    }
  }

  protected void setEvolutions(final EvolutionReguliere[] _evols) {
    evols_ = _evols;
  }

  /**
   * @param _nb le nombre d'evolution attendue
   */
  public void setEvolutionsNb(final int _nb) {
    if (_nb > 0) {
      evols_ = new EvolutionReguliere[_nb];
      for (int i = _nb - 1; i >= 0; i--) {
        evols_[i] = new EvolutionReguliere("CL" + i);
      }
    }
  }

}