package org.fudaa.dodico.rubar.io;

import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.rubar.io.RubarDFZResult.RubarDZFResultBloc;

/**
 *
 * @author CANEL Christophe (Genesis)
 *
 */
public class RubarDZFReader extends FileOpReadCharSimpleAbstract {

  private int nbNoeuds = -1;

  @Override
  protected Object internalRead() {
    if (nbNoeuds <= 0) {
      analyze_.addFatalError("Le nombre de noeuds n'est pas pr�cis�");

      return null;
    }

    final RubarDFZResult result = new RubarDFZResult();
    RubarDZFResultBloc currentBloc = null;

    try {
      final int[] fmtTime = new int[]{15};
      final int[] fmtNoeud = new int[10];
      Arrays.fill(fmtNoeud, 8);

      while (true) {
        in_.readFields(fmtTime);
        currentBloc = new RubarDZFResultBloc(in_.doubleField(0));

        result.getBlocs().add(currentBloc);
        for (int i = 0; i < this.nbNoeuds; ) {
          in_.readFields(fmtNoeud);
          final int numberOfFields = in_.getNumberOfFields();
          for (int j = 0; (j < numberOfFields) && (i < this.nbNoeuds); j++) {
            currentBloc.getDzfs().add(in_.doubleField(j));
            i++;
          }
        }
      }
    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Fin du fichier");
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
      return null;
    } catch (final NumberFormatException e) {
      analyze_.manageException(e, in_.getLineNumber());
      return null;
    }

    return result;
  }

  /**
   * @return le nombre de noeuds � lire
   */
  public int getNbNoeuds() {
    return nbNoeuds;
  }

  /**
   * @param nbNoeuds le nombre de noeuds � lire
   */
  public void setNbNoeuds(final int nbNoeuds) {
    this.nbNoeuds = nbNoeuds;
  }
}
