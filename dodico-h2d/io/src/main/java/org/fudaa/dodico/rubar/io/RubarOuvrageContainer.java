/**
 * @creation 22 d�c. 2004
 * @modification $Date: 2006-09-19 14:45:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.util.List;

import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageContainer;
import org.fudaa.dodico.h2d.rubar.H2dRubarOuvrageI;

/**
 * @author Fred Deniger
 * @version $Id: RubarOuvrageContainer.java,v 1.5 2006-09-19 14:45:51 deniger Exp $
 */
public class RubarOuvrageContainer implements H2dRubarOuvrageContainer {

  List ouvrage_;

  /**
   * @return le nombre d'ouvrage contenu par ce conteneur
   */
  @Override
  public int getNbOuvrage() {
    return ouvrage_ == null ? 0 : ouvrage_.size();
  }

  /**
   * @param _i appartien a [0,getNbOuvrage()[
   * @return l'ouvrage demande sans verif sur les indices de bords.
   */
  @Override
  public H2dRubarOuvrageI getOuvrage(final int _i) {
    return (H2dRubarOuvrageI) ouvrage_.get(_i);
  }

}