/*
 * @creation 12 mars 2004
 * @modification $Date: 2007-06-20 12:21:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import gnu.trove.TDoubleArrayList;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.collection.CtuluArrayDouble;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: RefluxSolutionReader.java,v 1.21 2007-06-20 12:21:48 deniger Exp $
 */
public class RefluxSolutionReader extends FileOpReadCharSimpleAbstract {

  File f_;
  boolean readOnlyLastStep_;
  RefluxRefondeSolutionFileFormat version_;

  /**
   * @param _ft la version utilisee
   */
  public RefluxSolutionReader(final RefluxRefondeSolutionFileFormat _ft) {
    version_ = _ft;
  }

  @Override
  protected Object internalRead() {
    return readSI();
  }

  @Override
  protected void processFile(final File _f) {
    f_ = _f;
  }

  protected RefluxSolutionInterface readSI() {
    RefluxSolutionAdapter r = null;
    if ((f_ == null) || (in_ == null)) {
      analyze_.addFatalError(H2dResource.getS("Le flux est null"));
      return null;
    }
    final File parent = f_.getParentFile();
    final File dat = new File(parent, CtuluLibFile.getSansExtension(f_.getName()) + ".dat");
    int nbNds = -1;
    r = new RefluxSolutionAdapter();
    if (dat.exists()) {
      final Properties prs = new Properties();
      FileInputStream readDat=null;
      try {
        readDat = new FileInputStream(dat);
        prs.load(readDat);
        readDat.close();
      } catch (final FileNotFoundException e) {
        analyze_.manageException(e);
      } catch (final IOException e) {
        analyze_.manageException(e);
      }
      finally{
        if(readDat!=null) {
          try {
            readDat.close();
          } catch (final IOException _evt) {
            FuLog.error(_evt);
            
          }
        }
      }
      nbNds = Integer.parseInt(prs.getProperty("nbNoeuds", "-1"));
      // r.setTypeProjet(Integer.parseInt(prs.getProperty("tpProbleme", "0")));
    }
    final List etape = readOnlyLastStep_ ? null : new ArrayList(500);
    RefluxSolutionAdapter.SIByTime first = null;
    try {
      in_.setBlankZero(true);
      final String s = in_.readLine().trim();
      final int delim = version_.getFirstLine();
      if (Integer.parseInt(s) != delim) {
        analyze_.addFatalError(DodicoLib.getS("Format du champ incorrect"), 1);
        return null;
      }
      final int[] fmtEtape = version_.getEtapeFormat();
      in_.readFields(fmtEtape);
      first = new RefluxSolutionAdapter.SIByTime(in_.doubleField(5), null);
      in_.readFields(version_.getNbVariableFormat());
      final int nbVar = in_.intField(1);
      if (nbVar <= 0) {
        analyze_.addFatalError(DodicoLib.getS("Format du champ incorrect"), in_.getLineNumber());
        return null;
      }
      final int[] dataFmt = new int[nbVar + 1];
      dataFmt[0] = version_.getFirstDataColLength();
      final int temp = version_.getDataColLength();
      Arrays.fill(dataFmt, 1, dataFmt.length, temp);
      int nbNodeRead = 0;
      // on d�termine le nombre de noeuds
      final TDoubleArrayList[] ls = new TDoubleArrayList[nbVar];
      for (int i = nbVar - 1; i >= 0; i--) {
        ls[i] = new TDoubleArrayList(nbNds < 0 ? 500 : (nbNds + 1));
      }
      in_.readFields(dataFmt);
      while (in_.intField(0) != delim) {
        for (int i = nbVar - 1; i >= 0; i--) {
          ls[i].add(in_.doubleField(i + 1));
        }
        nbNodeRead++;
        in_.readFields(dataFmt);
      }
      if ((nbNds > 0) && (nbNodeRead != nbNds)) {
        analyze_.addWarn(H2dResource.getS("Le nombre de noeuds annonc� par le fichier dat est incorrect"), -1);
      }
      CtuluArrayDouble[] step = new CtuluArrayDouble[nbVar];
      for (int i = nbVar - 1; i >= 0; i--) {
        step[i] = new CtuluArrayDouble(ls[i].toNativeArray());
      }
      first.setValues(step);
      if (!readOnlyLastStep_) {
        etape.add(first);
      }
      String line = in_.getLine();
      // indice pour connaitre le point lu en cours.
      int idxEnCours = 0;
      while (line != null) {
        in_.readFields(fmtEtape);
        first = new RefluxSolutionAdapter.SIByTime(in_.doubleField(5), null);
        step = new CtuluArrayDouble[nbVar];
        idxEnCours = 0;
        while (idxEnCours < nbNodeRead) {
          in_.readFields(dataFmt);
          for (int i = nbVar - 1; i >= 0; i--) {
            if (step[i] == null) {
              step[i] = new CtuluArrayDouble(nbNodeRead);
            }
            step[i].set(idxEnCours, in_.doubleField(i + 1));
          }
          idxEnCours++;
        }
        if (idxEnCours != nbNodeRead) {
          analyze_.addFatalError(H2dResource.getS("Le nombre de noeuds lu est incorrect"), in_.getLineNumber());
          return null;
        }
        first.setValues(step);
        if (!readOnlyLastStep_) {
          etape.add(first);
        }
        line = in_.readLine();
        if ((line != null) && (Integer.parseInt(line.trim()) != delim)) {
          analyze_.addFatalError(DodicoLib.getS("Format du champ incorrect"), in_.getLineNumber());
        }
      }
    } catch (final NumberFormatException _fmt) {
      addAnalyzeLigneLue();
      analyze_.addFatalError(_fmt.getMessage(), in_.getLineNumber());
      return null;
    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug(f_.getAbsolutePath() + " end of file");
      }
    } catch (final IllegalArgumentException e) {
      analyze_.manageException(e);
      return null;
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
    if (readOnlyLastStep_) {
      r.setTimes(new RefluxSolutionAdapter.SIByTime[] { first });
    } else {
      final RefluxSolutionAdapter.SIByTime[] rf = new RefluxSolutionAdapter.SIByTime[etape.size()];
      etape.toArray(rf);
      r.setTimes(rf);
    }
    return r;
  }

  /**
   * @return Returns the readOnlyLastStep.
   */
  public boolean isReadOnlyLastStep() {
    return readOnlyLastStep_;
  }

  /**
   * @param _readOnlyLastStep The readOnlyLastStep to set.
   */
  public void setReadOnlyLastStep(final boolean _readOnlyLastStep) {
    readOnlyLastStep_ = _readOnlyLastStep;
  }

}