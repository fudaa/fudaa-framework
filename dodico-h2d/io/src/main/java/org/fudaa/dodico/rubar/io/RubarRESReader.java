package org.fudaa.dodico.rubar.io;

import java.io.EOFException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;

/**
 * @author CANEL Christophe (Genesis)
 */
public class RubarRESReader extends FileOpReadCharSimpleAbstract {
  private static final Map<String, String> setters = new HashMap<String, String>();

  static {
    setters.put("IT", "setIt");
    setters.put("COTE EN CRETE", "setCoteEnCrete");
    setters.put("LARGEUR EN CRETE", "setLargeurEnCrete");
    setters.put("COTE EN PIED", "setCoteEnPied");
    setters.put("LARGEUR EN PIED", "setLargeurEnPied");
    setters.put("DIAMETRE DES GRAINS (D50)", "setDiametreGrains");
    setters.put("POROSITE", "setPorosite");
    setters.put("MASSE VOLUMIQUE", "setMasseVolumique");
    setters.put("STRICKLER DONNE", "setStricklerDonne");
    setters.put("STRICKLER CALCULE", "setStricklerCalcule");
    setters.put("LARGEUR MAXIMALE DE BRECHE", "setLargeurMaxBreche");
    setters.put("TEMPS DE DEBUT DE RUPTURE", "setTempsDebutRupture");
    setters.put("COTE DU TERRAIN NATUREL", "setCoteTerrainNaturel");
    setters.put("COTE DE LA BRECHE", "setCoteBreche");
    setters.put("RAYON DU RENARD", "setRayonRenard");
    setters.put("VOLUME ERODE", "setVolumeErode");
    setters.put("VOLUME PARTI", "setVolumeParti");
  }

  @Override
  protected Object internalRead() {
    final RubarRESResult r = new RubarRESResult();
    in_.setJumpBlankLine(true);

    try {
      int[] fmt = new int[] { 5, 5, 5, 15, 10, 10, 10, 10 };
      boolean readTable = false;
      List<RubarRESResultLine> currentLines = null;

      while (true) {
        String currentLine = in_.readLine();

        if (currentLine.endsWith("s")) {
          currentLine = currentLine.substring(1, currentLine.length() - 1);
        }

        if (this.isValueLine(currentLine)) {
          readTable = false;

          this.setParameter(getKey(currentLine), getValue(currentLine), r.getParameters());
        } else if (this.isArrayLine(currentLine, fmt)) {
          if (!readTable) {
            //c'est un peu tordu pour lire la premi�re ligne...
            if (currentLines == null) {
              currentLines = r.getResultatsGeneral();
            } else {
              currentLines = r.getResultatsOuvrageB();
            }
          }

          readTable = true;

          in_.analyzeCurrentLine(fmt);
          currentLines.add(this.createResultLine());
        } else {
          readTable = false;
        }
      }
    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Fin du fichier");
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
      return null;
    } catch (final NumberFormatException e) {
      analyze_.manageException(e, in_.getLineNumber());
      return null;
    }
    return r;
  }

  private void setParameter(String key, double value, RubarRESResultParameters parameters) {
    String method = setters.get(key);

    try {
      parameters.getClass().getDeclaredMethod(method, double.class).invoke(parameters, value);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private RubarRESResultLine createResultLine() {
    return new RubarRESResultLine(in_.intField(0), in_.intField(1), in_.intField(2), in_.doubleField(3), in_.doubleField(4), in_.doubleField(5),
        in_.doubleField(6), in_.doubleField(7));
  }

  private double getValue(String line) {
    int index = line.lastIndexOf("=");

    if ((index > -1) && (index < (line.length() - 1))) {
      String value = line.substring(index + 1);

      try {
        return Double.parseDouble(value);
      } catch (NumberFormatException e) {
      }
    }

    return Double.NaN;
  }

  private String getKey(String line) {
    int index = line.lastIndexOf("=");

    if (index > 0) {
      String key = line.substring(0, index).trim();

      if (!key.equals("")) {
        return key.toUpperCase();
      }
    }

    return null;
  }

  private boolean isValueLine(String line) {
    if ((getKey(line) == null) || (getValue(line) == Double.NaN)) {
      return false;
    }

    return true;
  }

  private boolean isArrayLine(String line, int[] format) {
    int nbChar = 0;

    for (int i = 0; i < format.length; i++) {
      nbChar += format[i];
    }

    if (line.length() == nbChar) {
      int startIndex = 0;
      int endIndex = 0;

      for (int i = 0; i < format.length; i++) {
        startIndex = endIndex;
        endIndex += format[i];

        char charToTest = line.charAt(endIndex - 1);

        if (!Character.isDefined(charToTest)) {
          return false;
        }

        try {
          Double.parseDouble(line.substring(startIndex, endIndex));
        } catch (NumberFormatException e) {
          return false;
        }
      }

      return true;
    }

    return false;
  }
}
