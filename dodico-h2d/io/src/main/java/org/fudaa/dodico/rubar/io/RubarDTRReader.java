/**
 * @creation 21 d�c. 2004
 * @modification $Date: 2006-09-19 14:45:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.io.IOException;

import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;

/**
 * @author Fred Deniger
 * @version $Id: RubarDTRReader.java,v 1.5 2006-09-19 14:45:51 deniger Exp $
 */
public class RubarDTRReader extends FileOpReadCharSimpleAbstract {

  @Override
  protected Object internalRead() {
    RubarDTRResult r = null;
    try {
      in_.readFields();
      r = new RubarDTRResult();
      r.time_ = in_.doubleField(0);
      in_.readFields();
      final int nbPt = in_.intField(0);
      r.x_ = new double[nbPt];
      r.y_ = new double[nbPt];
      for (int i = 0; i < nbPt; i++) {
        in_.readFields();
        r.x_[i] = in_.doubleField(1);
        r.y_[i] = in_.doubleField(2);
      }

    } catch (final NumberFormatException e) {
      analyze_.manageException(e);
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
    return r;
  }
}