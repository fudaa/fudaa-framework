/**
 *  @creation     12 juin 2003
 *  @modification $Date: 2006-09-19 14:45:56 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.exec;

import java.io.File;

import org.fudaa.ctulu.CtuluUI;

import org.fudaa.dodico.calcul.CalculExecBatch;

/**
 * @author deniger
 * @version $Id: RefluxExec.java,v 1.14 2006-09-19 14:45:56 deniger Exp $
 */
public class RefluxExec extends CalculExecBatch {

  /**
   * Lancement dans un nouveau repertoire et ajout des commandes pour le mode terminal.
   */
  public RefluxExec() {
    super("reflux");
    setStartCmdUse(true);
  }

  @Override
  public String[] getLaunchCmd(final File _paramsFile, final CtuluUI _ui) {
    return super.getLaunchCmdWithTemplate(_paramsFile, _ui);
  }

}