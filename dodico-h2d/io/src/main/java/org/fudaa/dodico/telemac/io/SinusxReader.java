/**
 * @creation 2002-11-21
 * @modification $Date: 2008-04-01 17:12:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import gnu.trove.TDoubleArrayList;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.ctulu.gis.GISAttribute;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISCoordinateSequence;
import org.fudaa.ctulu.gis.GISDataModelPointToMultiPointAdapter;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISZone;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.gis.GISZoneCollectionPolygone;
import org.fudaa.ctulu.gis.GISZoneCollectionPolyligne;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;

/**
 * @version $Id$
 * @author Fred Deniger
 */
public class SinusxReader extends FileOpReadCharSimpleAbstract implements CtuluActivity {

  /** Le signe moins. */
  private final static String POINT_MOINS = "-";
  /** Le signe plus. */
  private final static String POINT_PLUS = "+";

  boolean stop_;

  private static boolean estPoint(final String _field) {
    if (_field == null) {
      return false;
    }
    if (_field.indexOf(CtuluLibString.DOT) >= 0) {
      return true;
    }
    if (_field.startsWith(POINT_MOINS)) {
      return true;
    }
    if (_field.startsWith(POINT_PLUS)) {
      return true;
    }
    boolean estNombre = true;
    try {
      Double.parseDouble(_field);
    } catch (final NumberFormatException _e) {
      estNombre = false;
    }
    return estNombre;
  }

  private String name_;
  /** */
  private int nbOctets_;

  FileFormatVersionInterface v_;

  /**
   * @param _v la version a utiliser
   */
  public SinusxReader(final FileFormatVersionInterface _v) {
    v_ = _v;
  }

  @Override
  protected Object internalRead() {
    return readZones();
  }

  @Override
  protected void processFile(final File _f) {
    name_ = CtuluLibFile.getSansExtension(_f.getName());
    nbOctets_ = (int) _f.length();
  }

  /**
   * En retour, les polylignes sont stock�es dans une seule GISZoneCollectionPolyligne, les polygones dans une
   * seule GISZoneCollectionPolygone, les points par blocs, chaque bloc correspondant � une GISZoneCollectionPoint.<p>
   * Les collections de points poss�dent un attribut TITRE, chaque point de la collection ayant m�me TITRE, qui correspond
   * au nom du semis dont ils font partie. Ils poss�dent �galement un attribut ATT_NUM_BLOC pour pouvoir les regrouper.<p>
   * 
   * Utilise FortranReader ( donc un buffer).
   * 
   * @return les infos non bloquantes.
   */
  public synchronized GISZone readZones() {
    final SinusxKeyWord key = new SinusxKeyWord();
    final GISZone defZone = new GISZone();
    GISZoneCollectionPoint ptDefZone = null;
    
    GISAttributeInterface[] attPts=new GISAttributeInterface[]{
        GISAttributeConstants.BATHY,
        GISAttributeConstants.TITRE,
        GISDataModelPointToMultiPointAdapter.ATT_NUM_BLOC,
        GISAttributeConstants.NATURE
    };

//    Geometry geom;
    String name = "";
    
    int idx = 0;
    defZone.setNom(name_);
    // GISZone profilZone = null;
    if (super.in_ == null) {
      analyze_.addError(H2dResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }
    TDoubleArrayList polyligneXyz = null;
    boolean ferme = false;
    boolean niveau = false;
    String type = null;
    try {
      int lu = 0;
      boolean afficheAvance = false;
      if ((progress_ != null) && (nbOctets_ > 0)) {
        afficheAvance = true;
        progress_.setProgression(0);
      }
      int pourcentageEnCours = 0;
      String ligne;
      String bloc;
      // Si le lecteur n'est pas pret....
      // if(!fr.ready())
      // throw new IOException(DodicoLib.formateString("Lecteur non pr�t", fr));
      boolean mapInfo = false;
      // Lecture de l'entete du fichier les commentaires sont sautes
      do {
        in_.readFields();
        bloc = in_.stringField(0);
        ligne = in_.getLine();
        if (ligne.indexOf("MAPINFO") > 0) {
          mapInfo = true;
        }
      } while (key.isBlocCommentaire(bloc));
      // MNTEntite ent = null;
      // approximation
      lu += 150;
      // L'implantation de FortranReader ne semble pas correcte: une exception
      // est envoyee des que la ligne est null
      while (true) {
        if (stop_) {
          return null;
        }
        bloc = in_.stringField(0);
        // Lecture de l'entete
        GISZoneCollectionPoint pt = null;
        polyligneXyz = null;
        if (key.isBlocSep(bloc)) {
          type = in_.stringField(1);
          in_.readFields();
          bloc = in_.stringField(0);
          // Lecture entete de bloc (infos)
          while (!estPoint(bloc)) {

            if (key.isBlocNomEntite(bloc)) {
              name=in_.getLine().substring(key.getBlocNomEntite().length()).trim();
//              if (FuLog.isDebug()) {
//                FuLog.debug("DTE: name " + in_.getLine().substring(key.getBlocNomEntite().length()).trim());
//              }
            } else if (key.isBlocInfos(bloc)) {
              final String t1 = in_.stringField(1);
              final int nbChamps = in_.getNumberOfFields();
              if (estPoint(t1) && (nbChamps == 2)) {
                if (key.isTypeCourbe(type) && mapInfo) {
                  analyze_.addInfo(H2dResource.getS("Erreur MapInfo trait�"), in_.getLineNumber());
                  type = key.getTypeCourbeNiveau();
                }
                // A voir pour les autres type de donnees
              } else if (nbChamps == 3 && (key.isTypeCourbe(type)|| key.isTypeCourbeNiveau(type))) {
                if (key.isCourbeFermee(t1)) {
                  ferme = true;
                } else {
                  ferme = false;
                }

              }
            }
            in_.readFields();
            bloc = in_.stringField(0);
          }
          // Lecture des infos terminee => Creation du container de points
          if (key.isTypeCourbe(type)) {
            // DEBUG:FERME??
            polyligneXyz = new TDoubleArrayList();
          } 
          else if (key.isTypeCourbeNiveau(type)) {
            polyligneXyz = new TDoubleArrayList();
            niveau = true;
            /*
             * temp.niveau(true); defZone.ajoutePolyligne(temp); ent = temp; temp = null; ent.setNom(nom);
             */
            // ent.setNom(nom);
          } 
          else if (key.isTypeProfil(type)) {
            analyze_.addWarn(H2dResource.getS("Profils non g�r�s"), in_.getLineNumber());
            pt = defZone.createPointContainer();
            pt.setAttributes(attPts, null);
            if(pt.getAttributeIsZ()==null)
              pt.setAttributeIsZ(GISAttributeConstants.BATHY);
            idx++;
//            pt.setTitle("profil", null);
          } 
          else if (key.isTypeSemis(type)) {
            if (ptDefZone == null) {
              ptDefZone = defZone.createPointContainer();
              pt = ptDefZone;
              pt.setAttributes(attPts, null);
              if(pt.getAttributeIsZ()==null)
                pt.setAttributeIsZ(GISAttributeConstants.BATHY);
              idx++;
            } else {
              pt = defZone.createPointContainer();
              pt.setAttributes(attPts, null);
              if(pt.getAttributeIsZ()==null)
                pt.setAttributeIsZ(GISAttributeConstants.BATHY);
              idx++;
//              pt.setTitle(name_ + " " + (idx++), null);
            }
          }
          else {
            analyze_.addWarn(H2dResource.getS("TYPE inconnu : assimil� a un semis"), in_.getLineNumber());
            pt = defZone.createPointContainer();
            pt.setAttributes(attPts, null);
            if(pt.getAttributeIsZ()==null)
              pt.setAttributeIsZ(GISAttributeConstants.BATHY);
            idx++;
//            pt.setTitle(H2dResource.getS("type inconnu"), null);
          } 
        }
        else {
          analyze_.addWarn(H2dResource.getS("Liste de points uniquement"), in_.getLineNumber());
          if (ptDefZone == null) {
            ptDefZone = defZone.createPointContainer();
            pt = ptDefZone;
            pt.setAttributes(attPts, null);
            if(pt.getAttributeIsZ()==null)
              pt.setAttributeIsZ(GISAttributeConstants.BATHY);
            idx++;
            name=name_+" "+(idx);
          } else {
            pt = defZone.createPointContainer();
            pt.setAttributes(attPts, null);
            if(pt.getAttributeIsZ()==null)
              pt.setAttributeIsZ(GISAttributeConstants.BATHY);
            idx++;
            name=name_+" "+(idx);
//            pt.setTitle(name_ + " " + (idx++), null);
          }
        }
        lu += 30;
        // Lecture des points du bloc
        while (!key.isBlocSep(bloc)) {
          if (stop_) {
            return null;
          }
          if (estPoint(bloc)) {
            if (pt != null) {
              if(key.isTypeSemis(type))
                type=GISAttributeConstants.ATT_NATURE_SM;
              else
                type="";
              pt.add(Double.parseDouble(bloc), in_.doubleField(1), in_.doubleField(2),Arrays.asList(new Object[]{null,name,idx-1, type}),null);
            } else if (polyligneXyz != null) {
              polyligneXyz.add(Double.parseDouble(bloc));
              polyligneXyz.add(in_.doubleField(1));
              polyligneXyz.add(in_.doubleField(2));
            }
          }
          lu += 48;
          in_.readFields();
          bloc = in_.stringField(0);
          if ((afficheAvance) && ((lu * 100 / nbOctets_) >= (pourcentageEnCours + 20))) {
            pourcentageEnCours += 20;
            progress_.setProgression(pourcentageEnCours);
          }
        }
        // Fin de lecture des points du bloc => Si polyligne ou polygone, cr�ation et ajout des points.
        if (polyligneXyz != null)
          addLigne(defZone, polyligneXyz, name, type, ferme, niveau);
        ferme = false;
        niveau = false;
      }
    } catch (final EOFException e) {} catch (final IOException e) {
      analyze_.manageException(e);
    } catch (final NumberFormatException e) {
      analyze_.manageException(e);
    }
    if (stop_)
      return null;
    if (polyligneXyz != null)
      addLigne(defZone, polyligneXyz, name, type, ferme, niveau);
    
    for(int i=0;i<defZone.getPointCollections().length;i++)
      defZone.getPointCollections()[i].setAttributeIsZ(GISAttributeConstants.BATHY);
    for(int i=0;i<defZone.getPolyligneCollections().length;i++)
      defZone.getPolyligneCollections()[i].setAttributeIsZ(GISAttributeConstants.BATHY);
    for(int i=0;i<defZone.getPolygoneCollections().length;i++)
      defZone.getPolygoneCollections()[i].setAttributeIsZ(GISAttributeConstants.BATHY);
    for (int i=0; i<defZone.getNumGeometries(); i++) {
      ((GISZoneCollection)defZone.getGeometry(i)).postImport(0);
    }
    return defZone;
  }
  
  private void addLigne(GISZone _defZone, TDoubleArrayList _polyligneXyz, String _name, String _type, 
      boolean _ferme, boolean _niveau) {
    
    if (_polyligneXyz.size()<=3) { // Une polyligne ne peut pas contenir moins de 2 points.
      analyze_.addWarn(H2dResource.getS("La polyligne {0} est vide ou ne contient qu'un point",_name), in_.getLineNumber());
      return;
    }
    
    // D�termination de la nature
    String nature="";
    final SinusxKeyWord key = new SinusxKeyWord();
    if (key.isTypeCourbe(_type))
      nature="";
    else if (key.isTypeCourbeNiveau(_type))
      nature=GISAttributeConstants.ATT_NATURE_CN;
    else if (key.isTypeProfil(_type))
      nature=GISAttributeConstants.ATT_NATURE_PF;
    
    Geometry geom;

    if (!_ferme) {
      _ferme = isClosed(_polyligneXyz);
    }
    if (_ferme) {
      GISZoneCollectionPolygone polygones;
      if (_defZone.getNumPolyligones()==0) {
        polygones = _defZone.createPolygoneContainer();
        GISAttribute[] atts=new GISAttribute[]{
            GISAttributeConstants.BATHY,
            GISAttributeConstants.TITRE,
            GISAttributeConstants.NATURE
            };
        polygones.setAttributes(atts, null);
        if(polygones.getAttributeIsZ()==null)
          polygones.setAttributeIsZ(GISAttributeConstants.BATHY);
      }
      else {
        polygones=_defZone.getPolygoneCollections()[0];
      }
      
      _polyligneXyz.add(_polyligneXyz.getQuick(0));
      _polyligneXyz.add(_polyligneXyz.getQuick(1));
      _polyligneXyz.add(_polyligneXyz.getQuick(2));
      if (_niveau)
        geom=GISGeometryFactory.INSTANCE.createLinearRingNiveau(new GISCoordinateSequence(_polyligneXyz));
      else
        geom=GISGeometryFactory.INSTANCE.createLinearRing(new GISCoordinateSequence(_polyligneXyz));
      polygones.addGeometry(geom, new Object[]{null,_name, nature}, null);
    } 
    else {
      GISZoneCollectionPolyligne polylignes;
      if (_defZone.getNumPolylignes()==0) {
        GISAttribute[] atts=new GISAttribute[]{
            GISAttributeConstants.BATHY,
            GISAttributeConstants.TITRE,
            GISAttributeConstants.NATURE
            };
        polylignes = _defZone.createPolyligneContainer();
        polylignes.setAttributes(atts, null);
        if(polylignes.getAttributeIsZ()==null)
          polylignes.setAttributeIsZ(GISAttributeConstants.BATHY);
      }
      else {
        polylignes=_defZone.getPolyligneCollections()[0];
      }
      if (_niveau)
        geom=GISGeometryFactory.INSTANCE.createLineStringNiveau(new GISCoordinateSequence(_polyligneXyz));
      else
        geom=GISGeometryFactory.INSTANCE.createLineString(new GISCoordinateSequence(_polyligneXyz));
      polylignes.addGeometry(geom, new Object[]{null,_name, nature}, null);
    }
    _ferme = false;
    _niveau = false;
  }

  public boolean isClosed(final TDoubleArrayList _list) {
    if (_list.size() > 9) {
      final Coordinate c1 = new Coordinate(_list.get(0), _list.get(1), 0);
      final int idx = _list.size() - 3;
      final Coordinate c2 = new Coordinate(_list.get(idx), _list.get(idx + 1), 0);
      return (c1.distance(c2) < 1E-3);
    }
    return false;
  }

  @Override
  public void stop() {

    stop_ = true;
  }
}
