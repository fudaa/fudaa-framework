package org.fudaa.dodico.zesri.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.List;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.fudaa.commun.FudaaPreferences;

/**
 * Classe writer de fichier au format Z ESRI
 *
 * @author Adrien Hadoux
 *
 */
public class ZESRIWriter extends FileWriteOperationAbstract {

    public CtuluIOOperationSynthese write(final File f, List<List<GISPoint>> profils) {
        
     int nbDigits=FudaaPreferences.FUDAA.getIntegerProperty(ZesriExportPreferencesComponent.ZESRI_EXPORT_NB_DIGITS, -1);
    DecimalFormat fmt=CtuluLib.getNoEffectDecimalFormat();
    if (nbDigits != -1) {
      fmt.setMaximumFractionDigits(nbDigits);
    }
        
CtuluIOOperationSynthese op = new CtuluIOOperationSynthese();
        // -- ecriture dans le fichier --//
        BufferedWriter writer = null;
        try {
             writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(f), "utf-8"));
            String endOfLine = "win".equals(System.getProperty("os.name").toLowerCase()) ? "\r\n" : "\n";
            StringBuffer buffer = null;
            int indiceProfil = 0;
            for (List<GISPoint> profil : profils) {
                buffer = new StringBuffer();
                buffer.append("" + (indiceProfil++)).append(endOfLine);
                for (GISPoint p : profil) {
                    buffer.append(("" + fmt.format(p.getX())).replace(",", ".")).append(",").append(
                            ("" + fmt.format(p.getY())).replace(",", ".")).append(",").append(("" + fmt.format(p.getZ())).replace(",", ".")).append(endOfLine);
                }
                buffer.append("END").append(endOfLine);
                writer.write(buffer.toString());
            }
            writer.write("END");
        } catch (final IOException e) {

        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (Exception ex) {
                
            }
            return op;
        }
    }

    @Override
    protected void internalWrite(Object _o) {
       
    }

    @Override
    protected FortranInterface getFortranInterface() {
       return null;
    }

    @Override
    public void setFile(File _f) {
       
    }


}
