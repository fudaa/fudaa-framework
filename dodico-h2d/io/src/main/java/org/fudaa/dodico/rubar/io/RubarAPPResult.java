/**
 * @creation 14 d�c. 2004
 * @modification $Date: 2006-09-19 14:45:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.dodico.h2d.rubar.H2dRubarAppInterface;
import org.fudaa.dodico.mesure.EvolutionReguliere;

/**
 * @author Fred Deniger
 * @version $Id: RubarAPPResult.java,v 1.3 2006-09-19 14:45:52 deniger Exp $
 */
public class RubarAPPResult implements H2dRubarAppInterface {

  public RubarAPPResult() {
    super();
  }

  EvolutionReguliere[] evols_;
  int[] eltEvolIdx_;

  /**
   * @return le nombre d'element d�fini par cette resultat
   */
  @Override
  public int getNbElt() {
    return eltEvolIdx_ == null ? 0 : eltEvolIdx_.length;
  }

  /**
   * @return le nombre d'element d�fini par cette resultat
   */
  @Override
  public int getNbEvol() {
    return evols_ == null ? 0 : evols_.length;
  }

  @Override
  public EvolutionReguliere getEvolForElt(final int _idxElt) {
    return getEvol(getEvolIdx(_idxElt));
  }

  /**
   * @param _i l'indice [0;getNbEvol()[
   * @return l'evolution correspondante.
   */
  @Override
  public EvolutionReguliere getEvol(final int _i) {
    return evols_[_i];
  }

  /**
   * @param _eltIdx l'element demande
   * @return l'indice de la courbe correspondante
   */
  @Override
  public int getEvolIdx(final int _eltIdx) {
    return eltEvolIdx_[_eltIdx];
  }

}