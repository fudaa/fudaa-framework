/*
 * @creation 27 avr. 07
 * @modification $Date: 2007-06-28 09:25:07 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import org.fudaa.ctulu.CtuluNumberFormatFortran;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.collection.CollectionPointDataDoubleInterface;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;

import org.fudaa.dodico.fortran.FortranDoubleReaderResultInterface;
import org.fudaa.dodico.h2d.reflux.H2dRefluxNodalPropertiesMng;

public class RefluxSollicitationFortranAdapter implements FortranDoubleReaderResultInterface {
  final CtuluNumberFormatI doubleFormater_;
  // final CtuluNumberFormatI intFormater_;
  final H2dRefluxNodalPropertiesMng.ForceItem values_;

  // final CtuluCollectionDouble firstCol_;

  public RefluxSollicitationFortranAdapter(final H2dRefluxNodalPropertiesMng.ForceItem _values) {
    super();
    values_ = _values;
    doubleFormater_ = new CtuluNumberFormatFortran(12);

  }

  @Override
  public CollectionPointDataDoubleInterface createXYValuesInterface() {
    return null;
  }

  @Override
  public CtuluCollectionDouble getCollectionFor(final int _col) {
    return values_.getValues(values_.getVar(_col));
  }

  @Override
  public CtuluNumberFormatI getFormat(final int _col) {
    return doubleFormater_;
  }

  @Override
  public int getNbColonne() {
    return values_.getNbVar();
  }

  @Override
  public int getNbLigne() {
    return values_.getNbPt();
  }

  @Override
  public double getValue(final int _ligne, final int _col) {
    return getCollectionFor(_col).getValue(_ligne);
  }

}