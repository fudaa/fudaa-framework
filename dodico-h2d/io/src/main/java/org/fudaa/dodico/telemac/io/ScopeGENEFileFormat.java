package org.fudaa.dodico.telemac.io;

import java.io.File;
import java.util.Map;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fichiers.FileFormatSoftware;
import org.fudaa.dodico.mesure.EvolutionFileFormatVersion;

/**
 * Manager de fichiers au format scope. Utilis� pour la cr�ation des graphes. au
 * format SCOPGENE
 * 
 * @author Adrien Hadoux
 * 
 */
public class ScopeGENEFileFormat extends FileFormatUnique implements EvolutionFileFormatVersion {

  static final ScopeGENEFileFormat INSTANCE = new ScopeGENEFileFormat();

  /**
   * @return singleton
   */
  public static ScopeGENEFileFormat getInstance() {
    return INSTANCE;
  }

  private ScopeGENEFileFormat() {
    super(1);
    extensions_ = new String[] { ScopeKeyWord.TYPE_COURBE_MIXTE,ScopeKeyWord.TYPE_COURBE_MIXTE2,ScopeKeyWord.TYPE_COURBE_MIXTE3,ScopeKeyWord.TYPE_COURBE_MIXTE4,ScopeKeyWord.TYPE_COURBE_MIXTE5 };
    id_ = "SCOPE";
    nom_ = "Scope";
    description_ = DodicoLib.getS("Comporte les d�finitions de courbes temporelles et spatiales.");
    software_ = FileFormatSoftware.TELEMAC_IS;
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new ScopeReaderGene(this);
  }

  @Override
  public CtuluIOOperationSynthese write(final File _f, final Object _source, final ProgressionInterface _prog) {
    return super.write(_f, _source, _prog);
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new ScopeWriterGENE(this);
  }

  @Override
  public CtuluIOOperationSynthese readEvolutions(File _f, ProgressionInterface _prog, Map _options) {
    return super.read(_f, _prog);
  }
  
  

  
}
