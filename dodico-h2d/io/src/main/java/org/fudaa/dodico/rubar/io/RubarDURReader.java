package org.fudaa.dodico.rubar.io;

import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;

/**
 * @author CANEL Christophe (Genesis)
 */
public class RubarDURReader extends FileOpReadCharSimpleAbstract {
  private int nbNoeuds = -1;

  @Override
  protected Object internalRead() {
    if (nbNoeuds <= 0) {
      analyze_.addFatalError("Le nombre de noeuds n'est pas pr�cis�");
    }
    CtuluArrayDouble zValues=new CtuluArrayDouble(nbNoeuds);
    final RubarDURResult r = new RubarDURResult(zValues);

    try {
      int[] fmt = new int[10];
      Arrays.fill(fmt, 8);
      final int nbFieldByLine = fmt.length;
      int tmpOnLine = 0;
      in_.readFields(fmt);
      for (int i = 0; i < nbNoeuds; i++) {
        if (tmpOnLine == nbFieldByLine) {
          in_.readFields(fmt);
          tmpOnLine = 0;
        }
        zValues.set(i,in_.doubleField(tmpOnLine));
        tmpOnLine++;
      }
    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Fin du fichier");
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
      return null;
    } catch (final NumberFormatException e) {
      analyze_.manageException(e, in_.getLineNumber());
      return null;
    }
    return r;
  }

  /**
   * @return le nombre de noeuds � lire
   */
  public int getNbNoeuds() {
    return nbNoeuds;
  }

  /**
   * @param nbNoeuds le nombre de noeuds � lire
   */
  public void setNbNoeuds(final int nbNoeuds) {
    this.nbNoeuds = nbNoeuds;
  }
}