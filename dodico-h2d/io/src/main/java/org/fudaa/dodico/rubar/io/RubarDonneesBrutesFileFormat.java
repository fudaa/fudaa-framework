/**
 * @creation 12 oct. 2004
 * @modification $Date: 2006-09-19 14:45:51 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.fileformat.FileFormatUnique;

/**
 * @author Fred Deniger
 * @version $Id: RubarDonneesBrutesFileFormat.java,v 1.5 2006-09-19 14:45:51 deniger Exp $
 */
public abstract class RubarDonneesBrutesFileFormat extends FileFormatUnique {
  private boolean newFormat;

  public boolean isNewFormat() {
    return newFormat;
  }

  public void setNewFormat(boolean newFormat) {
    this.newFormat = newFormat;
  }

  public RubarDonneesBrutesFileFormat() {
    super(1);
  }

  public abstract boolean isNuage();
}
