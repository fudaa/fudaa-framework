/*
 * @creation 15 f�vr. 07
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.util.Set;
import java.util.TreeSet;

import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInteger;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.fortran.FortranWriter;

import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;

/**
 * Une classe pour ecrire des casiers au format Rubar.
 * 
 * @author Bertrand Marchand
 * @version $Id$
 */
public class RubarCasierWriter extends FileOpWriterCharSimpleAbstract {
  
  /**
   * Une entr�e altitude/surface comparable pour le tri suivant les altitudes. Le tri se fait suivant une altitude croissante.
   */
  private class AltSurfEntry implements Comparable<AltSurfEntry> {
    public double surf;
    public double alt;
    
    /**
     * Cr�ation d'une entr�e altitude/surface.
     * @param _alt L'altitude
     * @param _surf La surface associ�e.
     */
    public AltSurfEntry(double _alt, double _surf) {
      surf=_surf;
      alt=_alt;
    }
    
    @Override
    public int compareTo(AltSurfEntry _o) {
      return alt>_o.alt ? 1:alt<_o.alt ? -1:0;
    }
  }
  
  /** A utiliser pour chaque ligne de niveau, comme index sur le casier auquel elle se rapporte */
  public static final GISAttributeInterface INDEX_CASIER=new GISAttributeInteger("Index casier");

  /**
   * Ecrit les casiers. Les casiers sont consid�r�s conformes, c'est a dire a plus de 3 points et 0 ou plus lignes de niveaux.
   * Les surfaces correspondant aux lignes de niveau sont �crites par altitudes croissantes.
   * 
   * @param _o Un tableau GISDataModel[2]. 
   * [0] : le modele des contours de casiers
   * [1] : le modele des lignes de niveau ferm�es internes aux casiers. Il poss�de un attribut INDEX_CASIER, qui indique le
   * casier auquel chaque ligne de niveau se rapporte.
   */
  @Override
  protected void internalWrite(final Object _o) {
    if (!(_o instanceof GISDataModel[]) || ((GISDataModel[])_o).length!=2) {
      donneesInvalides(_o);
      return;
    }
    
    GISDataModel mdcasiers=((GISDataModel[])_o)[0];
    GISDataModel mdniv=((GISDataModel[])_o)[1];
    
    final int iattName=mdcasiers.getIndiceOf(GISAttributeConstants.TITRE);
    final int iattRef=mdniv.getIndiceOf(INDEX_CASIER);
    if (iattRef==-1) {
      analyze_.addFatalError(DodicoLib.getS("Arguments d'appel � RubarCasierWriter invalides"));
      return;
    }
    
    final FortranWriter writer = new FortranWriter(out_);
    // Format en principe F10.0,F10.0, mais on triche pour que les lecteurs au format libre fonctionnent aussi.
    final int[] fmt={1,9,1,9};
    
    try {
      Set<AltSurfEntry> sAltSurfs=new TreeSet<AltSurfEntry>();
      
      for (int icas = 0; icas < mdcasiers.getNumGeometries(); icas++) {
        GISPolygone gcasier=((GISPolygone)mdcasiers.getGeometry(icas));

        // Tri des couples par altitude croissante.
        
        sAltSurfs.clear();
        // Le contour casier
        sAltSurfs.add(new AltSurfEntry(getAltitude(gcasier.getCoordinateSequence()),getSurface(gcasier)));
        // Les lignes de niveau pour ce casier.
        for (int iniv=0; iniv< mdniv.getNumGeometries(); iniv++) {
          if (mdniv.getValue(iattRef, iniv).equals(icas)) {
            GISPolygone gniv=((GISPolygone)mdniv.getGeometry(iniv));
            sAltSurfs.add(new AltSurfEntry(getAltitude(gniv.getCoordinateSequence()),getSurface(gniv)));
          }
        }
        
        // Ecriture du nom
        String name;
        if (iattName==-1||(name=(String)mdcasiers.getValue(iattName, icas))==null||name.length()>3) name="C"+(icas+1);
        writer.writeln("$"+name);
        
        // Ecriture des couples altitude/surface
        for (AltSurfEntry entry : sAltSurfs) {
          writer.doubleField(3,entry.surf);
          writer.doubleField(1,entry.alt);
          writer.writeFields(fmt);
        }
      }

    } catch (final Exception _evt) {
      analyze_.manageException(_evt);
    }
    
  }
  
  /**
   * Retourne l'altitude moyenne de la sequence pass�e en argument.
   * @param _seq La sequence de coordonn�es.
   * @return L'altitude moyenne.
   */
  private double getAltitude(CoordinateSequence _seq) {
    return GISLib.getMoyZ(_seq);
  }
  
  /**
   * Retourne la surface (en hectares) de la g�om�trie pass�e en argument.
   * @param _geom La g�om�trie pass�e en argument.
   * @return La surface en hectares. 0 pour un point.
   */
  private double getSurface(Geometry _geom) {
    return _geom.getArea()/10000;
  }
}
