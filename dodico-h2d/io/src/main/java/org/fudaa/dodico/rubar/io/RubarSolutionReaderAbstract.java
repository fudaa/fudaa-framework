/*
 * @creation 27 sept. 06
 * @modification $Date: 2006-09-28 13:41:03 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.io.File;

import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;

/**
 * @author fred deniger
 * @version $Id: RubarSolutionReaderAbstract.java,v 1.1 2006-09-28 13:41:03 deniger Exp $
 */
public abstract class RubarSolutionReaderAbstract extends FileOpReadCharSimpleAbstract{

  protected int nbElt_ = -1;
  protected int nbValues_;
  protected File f_;
  protected double nbValueByLigne_ = 8d;

  /**
   * @return le nombre d'elements
   */
  public final int getNbElt() {
    return nbElt_;
  }

  /**
   * @return le nombre de valeurs
   */
  public final int getNbValues() {
    return nbValues_;
  }

  /**
   * @param _nbElt le nombre d'elements a lire
   */
  public final void setNbElt(final int _nbElt) {
    nbElt_ = _nbElt;
  }

  /**
   * @param _nbValues le nombre de valeurs
   */
  public final void setNbValues(final int _nbValues) {
    nbValues_ = _nbValues;
  }

  @Override
  protected void processFile(final File _f) {
    f_ = _f;
  }

  public final void setNbValueByLigne(final double _nbValueByLigne) {
    nbValueByLigne_ = _nbValueByLigne;
  }

  public double getNbValueByLigne() {
    return nbValueByLigne_;
  }

}
