/*
 * @file TelemacDicoManager.java
 *
 * @creation 14 avr. 2003
 *
 * @modification $Date: 2007-06-13 12:56:51 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.org
 */
package org.fudaa.dodico.telemac;

import org.fudaa.ctulu.CtuluResult;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.dodico.dico.*;
import org.fudaa.dodico.h2d.telemac.H2dTelemacDicoParams;

import java.io.File;
import java.nio.file.Paths;

/**
 * @author deniger
 */
public class TelemacDicoManager extends DicoManager {
  /**
   * Singleton.
   */
  public static final TelemacDicoManager INSTANCE = new TelemacDicoManager();

  static {
    INSTANCE.init(new TelemacDicoManagerLoader().load());
  }

  /**
   * @return singleton
   */
  public static TelemacDicoManager getINSTANCE() {
    return INSTANCE;
  }

  private TelemacDicoManager() {
    super(TelemacDicoManagerLoader.getDicoZipUri());
  }

  /**
   * @param dicoCasFileFormat format a tester
   * @return true si telemac2D
   */
  public static boolean isTelemac2d(final DicoCasFileFormat dicoCasFileFormat) {
    return dicoCasFileFormat != null && Telemac2dFileFormat.getInstance().getID().equals(dicoCasFileFormat.getID());
  }

  static boolean isTelemac3d(final DicoCasFileFormat dicoCasFileFormat) {
    return dicoCasFileFormat != null && H2dTelemacDicoParams.getTelemac3dId().equals(dicoCasFileFormat.getName());
  }

  /**
   * @param fileFormat format a tester
   * @return true si telemac2D
   */
  public static boolean isStbtel(final FileFormat fileFormat) {
    return fileFormat != null && H2dTelemacDicoParams.getStbtelId().equals(fileFormat.getName());
  }

  @Override
  public DicoCasFileFormatVersion createVersionImpl(final DicoCasFileFormat dicoCasFileFormat, final String version, final int language) {
    return createTelemacVersionImpl(dicoCasFileFormat, getDicoModel(dicoCasFileFormat, version, null, language, null).getResultat(), language);
  }

  /**
   * @return une version de telemac 2d.
   */
  private Telemac2dFileFormat.TelemacVersion createTelemac2dVersionImpl(final Telemac2dFileFormat fileFormat,
                                                                        final DicoModelAbstract modelAbstract, final int language) {
    return fileFormat.createVersion(modelAbstract);
  }

  private CtuluResult<DicoModelAbstract> getDicoModel(final DicoCasFileFormat dicoCasFileFormat, final String v, final File file,
                                                      final int language, ProgressionInterface progression) {
    CtuluResult<DicoModelAbstract> res = new CtuluResult<>();
    if (file != null) {
      DicoDynamiqueGenerator generator = DicoDynamiqueGenerator.loadGenerator(file, progression);
      res.setAnalyze(generator.getResult());
      res.setResultat(generator.buildModel(null, language));
    } else {
      res.setResultat(createDico(dicoCasFileFormat.getName(), v, language));
    }
    return res;
  }

  public CtuluResult<TelemacDicoFileFormatVersion> createTelemacVersionImpl(final DicoCasFileFormat dicoCasFileFormat,
                                                                            final String versionKnown, final File dicoFile,
                                                                            final int language, ProgressionInterface progression) {
    CtuluResult<DicoModelAbstract> res = getDicoModel(dicoCasFileFormat, versionKnown, dicoFile, language, progression);
    CtuluResult<TelemacDicoFileFormatVersion> finalRes = new CtuluResult<>();
    finalRes.setAnalyze(res.getAnalyze());
    if (res.getResultat() != null) {
      finalRes.setResultat(createTelemacVersionImpl(dicoCasFileFormat, res.getResultat(), language));
    }
    return finalRes;
  }

  private TelemacDicoFileFormatVersion createTelemacVersionImpl(final DicoCasFileFormat dicoCasFileFormat,
                                                                final DicoModelAbstract model, final int language) {

    if (Telemac2dFileFormat.getInstance() == dicoCasFileFormat) {
      return createTelemac2dVersionImpl((Telemac2dFileFormat) dicoCasFileFormat, model,
          language);
    }
    if (model == null) {
      return null;
    }
    if (dicoCasFileFormat.getName().equals(H2dTelemacDicoParams.getSubief2dId())) {
      return new TelemacSubief2dFileFormatVersion(dicoCasFileFormat,
          model);
    }
    if (dicoCasFileFormat.getName().equals(H2dTelemacDicoParams.getStbtelId())) {
      return new TelemacStbtelFileFormatVersion(dicoCasFileFormat,
          model);
    }
    return new TelemacDicoFileFormatVersion(dicoCasFileFormat, model);
  }
}
