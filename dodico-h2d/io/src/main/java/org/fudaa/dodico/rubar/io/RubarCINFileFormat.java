/**
 * @creation 8 juin 2004
 * @modification $Date: 2006-11-15 09:22:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: RubarCINFileFormat.java,v 1.11 2006-11-15 09:22:53 deniger Exp $
 */
public class RubarCINFileFormat extends FileFormatUnique {


  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createReader()
   */
  @Override
  public FileReadOperationAbstract createReader(){
    return new RubarCINReader();
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createWriter()
   */
  @Override
  public FileWriteOperationAbstract createWriter(){
    return new RubarCINWriter();
  }

  /**
   * @param _f
   * @param _prog
   * @param _nbElt
   * @param nbConcentrationBlock the number of concentration block. Use -1 if no transport
   * @return le resultat de l'operation
   */
  public CtuluIOOperationSynthese read(final File _f,final ProgressionInterface _prog,final int _nbElt,final int nbConcentrationBlock){
    final RubarCINReader i = new RubarCINReader();
    i.setNbElt(_nbElt);
    i.setNbBlockTransport(nbConcentrationBlock);
    final CtuluIOOperationSynthese r = i.read(_f, _prog);
    i.setProgressReceiver(null);
    return r;
  }

  private static RubarCINFileFormat instance_;

  /**
   * @return l'instance a utiliser
   */
  public final static RubarCINFileFormat getInstance(){
    //pas besoin de synchronise. S'il y a 2 instances, c'est pas grave
    if (instance_ == null) {
      instance_ = new RubarCINFileFormat();
    }
    return instance_;
  }

  /**
   * Initialise les donnees.
   */
  public RubarCINFileFormat() {
    super(1);
    extensions_ = new String[] { "cin"};
    super.description_ = H2dResource.getS("Conditions initiales par maille");
    super.id_ = "RUBAR_CIN";
    super.nom_ = "Rubar cin";
  }

}
