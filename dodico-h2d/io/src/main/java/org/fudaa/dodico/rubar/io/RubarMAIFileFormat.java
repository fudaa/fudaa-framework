/**
 * @creation 8 juin 2004
 * @modification $Date: 2007-01-19 13:07:22 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.ef.impl.EfGridSourceDefaut;
import org.fudaa.dodico.h2d.resource.H2dResource;

import java.io.File;

/**
 * @author Fred Deniger
 * @version $Id: RubarMAIFileFormat.java,v 1.11 2007-01-19 13:07:22 deniger Exp $
 */
public class RubarMAIFileFormat extends FileFormatUnique implements FileFormatGridVersion {
  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createReader()
   */
  @Override
  public FileReadOperationAbstract createReader() {
    return new RubarMAIReader();
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createWriter()
   */
  @Override
  public FileWriteOperationAbstract createWriter() {
    final RubarMAIWriter rubarMAIWriter = new RubarMAIWriter();
    rubarMAIWriter.setNewFormat(newFormat);
    return rubarMAIWriter;
  }

  private static RubarMAIFileFormat instance_;

  /**
   * @return l'instance a utiliser
   */
  public final static RubarMAIFileFormat getInstance() {
    // pas besoin de synchronise. S'il y a 2 instances, c'est pas grave
    if (instance_ == null) {
      instance_ = new RubarMAIFileFormat();
    }
    return instance_;
  }

  public static RubarMAIFileFormat getFormat(boolean newFormat) {
    if (newFormat) {
      return getInstance();
    }
    return new RubarMAIFileFormat(newFormat);
  }

  public boolean canWriteOnlyGrid() {
    return false;
  }

  @Override
  public CtuluIOOperationSynthese readGrid(final File _f, final ProgressionInterface _prog) {
    final CtuluIOOperationSynthese s = read(_f, _prog);
    if (s.getSource() != null) {
      final EfGridSourceDefaut r = new EfGridSourceDefaut((EfGridInterface) s.getSource(), this);
      s.setSource(r);
    }
    return s;
  }

  public CtuluIOOperationSynthese readListPoint(final File _f, final ProgressionInterface _prog) {
    return null;
  }

  @Override
  public CtuluIOOperationSynthese writeGrid(final File _f, final EfGridSource _m, final ProgressionInterface _prog) {
    return write(_f, _m == null ? null : _m.getGrid(), _prog);
  }

  private final boolean newFormat;

  public RubarMAIFileFormat() {
    this(true);
  }

  /**
   * Initialise les donnees.
   */
  public RubarMAIFileFormat(boolean newFormat) {
    super(1);
    this.newFormat = newFormat;
    extensions_ = new String[]{"mai"};
    super.description_ = H2dResource.getS("Maillage");
    super.id_ = "RUBAR_MAI";
    super.nom_ = newFormat ? "Rubar maillage (format MAI - 9 digits)" : "Rubar maillage (format MAI - 6 digits)";
  }

  @Override
  public boolean canReadGrid() {
    return true;
  }

  @Override
  public boolean canWriteGrid() {
    return true;
  }

  @Override
  public boolean hasBoundaryConditons() {
    // TODO Voir si correct
    return false;
  }
}
