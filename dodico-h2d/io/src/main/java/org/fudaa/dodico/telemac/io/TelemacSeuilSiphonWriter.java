/**
 * @creation 7 d�c. 2004
 * @modification $Date: 2006-09-19 14:45:49 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import java.io.IOException;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuilContainer;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuilInterface;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSeuilSiphonContainer;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSiphonContainer;
import org.fudaa.dodico.h2d.telemac.H2dTelemacSiphonInterface;

/**
 * @author Fred Deniger
 * @version $Id: TelemacSeuilSiphonWriter.java,v 1.9 2006-09-19 14:45:49 deniger Exp $
 */
public class TelemacSeuilSiphonWriter extends FileOpWriterCharSimpleAbstract {

  boolean isFrench_;

  @Override
  protected void internalWrite(final Object _o) {
    isFrench_ = CtuluLib.isFrenchLanguageSelected();
    if (!(_o instanceof H2dTelemacSeuilSiphonContainer)) {
      donneesInvalides(_o);
      return;
    }
    try {
      final H2dTelemacSeuilSiphonContainer c = (H2dTelemacSeuilSiphonContainer) _o;
      if (c.getNbSeuil() > 0) {
        writeSeuil(c);
      }
      if (c.getNbSiphon() > 0) {
        writeSiphon(c);
      }
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
  }

  private void writeSeuil(final H2dTelemacSeuilContainer _c) throws IOException {
    final String entete = isFrench_ ? "Nb de seuils Option pour la vitesse tangentielle"
        : "Nb of weirs Option for tangential velocity";
    writelnToOut(entete);
    writelnToOut("     " + _c.getNbSeuil() + "             " + _c.getSeuilVitesseTangentielle());
    for (int i = 0; i < _c.getNbSeuil(); i++) {
      final H2dTelemacSeuilInterface s = _c.getSeuil(i);
      writelnToOut(isFrench_ ? "---------------------------- Singularite "
          : "---------------------------- singularity " + (i + 1));

      writelnToOut(isFrench_ ? "Nb de points" : "Nb of points");
      writelnToOut(Integer.toString(s.getNbPoint()));
      writelnToOut(isFrench_ ? "Point cote 1" : "Points side 1");
      StringBuffer buf = new StringBuffer();
      for (int j = 0; j < s.getNbPoint(); j++) {
        if (j != 0) {
          buf.append(CtuluLibString.ESPACE);
        }
        buf.append(s.getPtIdx1(j) + 1);
      }
      writelnToOut(buf.toString());
      writelnToOut(isFrench_ ? "Point cote 2" : "Points side 2");
      buf = new StringBuffer();
      for (int j = 0; j < s.getNbPoint(); j++) {
        if (j != 0) {
          buf.append(CtuluLibString.ESPACE);
        }
        buf.append(s.getPtIdx2(j) + 1);
      }
      writelnToOut(buf.toString());
      writelnToOut(isFrench_ ? "Cotes des digues" : "Level of the dick");
      buf = new StringBuffer();
      for (int j = 0; j < s.getNbPoint(); j++) {
        if (j != 0) {
          buf.append(CtuluLibString.ESPACE);
        }
        buf.append(s.getCote(j));
      }
      writelnToOut(buf.toString());
      writelnToOut(isFrench_ ? "Coefficients de debit" : "flowrate coefficients");
      buf = new StringBuffer();
      for (int j = 0; j < s.getNbPoint(); j++) {
        if (j != 0) {
          buf.append(CtuluLibString.ESPACE);
        }
        buf.append(s.getCoefDebit(j));
      }
      writelnToOut(buf.toString());
    }

  }

  private void writeSiphon(final H2dTelemacSiphonContainer _c) throws IOException {
    final String entete = isFrench_ ? "Relaxation Nombre de siphons" : "Relaxation Number of siphons";
    writelnToOut(entete);
    writelnToOut("     " + _c.getRelaxation() + "          " + _c.getNbSiphon());
    final int nbSip = _c.getNbSiphon();
    final String[] cols = new String[] { "I1", "I2", "d1", "d2", "CE1", "CE2", "CS1", "CS2", "S12", "L12", "z1", "z2",
        "a1", "a2" };
    final int[] maxSize = new int[cols.length];
    for (int i = nbSip - 1; i >= 0; i--) {
      final H2dTelemacSiphonInterface sip = _c.getSiphon(i);
      String test = Integer.toString(sip.getSource1() + 1);
      if (test.length() > maxSize[0]) {
        maxSize[0] = test.length();
      }
      test = Integer.toString(sip.getSource2() + 1);
      if (test.length() > maxSize[1]) {
        maxSize[1] = test.length();
      }
      for (int j = cols.length - 1; j >= 2; j--) {
        test = Double.toString(sip.getValue(j - 2));
        if (test.length() > maxSize[j]) {
          maxSize[j] = test.length();
        }
      }
    }
    StringBuffer buf = new StringBuffer();
    for (int i = 0; i < cols.length; i++) {
      if (i != 0) {
        buf.append(CtuluLibString.ESPACE);
      }
      buf.append(CtuluLibString.adjustSizeBefore(maxSize[i], cols[i]));
    }
    writelnToOut(buf.toString());
    for (int i = 0; i < nbSip; i++) {
      buf = new StringBuffer();
      final H2dTelemacSiphonInterface sip = _c.getSiphon(i);
      String test = Integer.toString(sip.getSource1() + 1);
      buf.append(CtuluLibString.adjustSizeBefore(maxSize[0], test));
      test = Integer.toString(sip.getSource2() + 1);
      buf.append(CtuluLibString.ESPACE);
      buf.append(CtuluLibString.adjustSizeBefore(maxSize[1], test));
      for (int j = 2; j < cols.length; j++) {
        buf.append(CtuluLibString.ESPACE);
        test = Double.toString(sip.getValue(j - 2));
        buf.append(CtuluLibString.adjustSizeBefore(maxSize[j], test));
      }
      writelnToOut(buf.toString());
    }

  }
}