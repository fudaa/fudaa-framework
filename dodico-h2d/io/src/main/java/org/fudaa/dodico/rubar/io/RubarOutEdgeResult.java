/**
 * @creation 7 f�vr. 2005
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.dodico.h2d.type.H2dRubarBcTypeList;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryType;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageRef;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: RubarOutEdgeResult.java,v 1.7 2007-05-04 13:47:30 deniger Exp $
 */
public class RubarOutEdgeResult {
  public static final H2dVariableType DEBIT_VAR = H2dVariableType.DEBIT_M3;
  Map<H2dVariableType, EvolutionReguliere> evolutions = new HashMap<>();
  double longueur_;
  H2dRubarOuvrageRef ouvType_;
  H2dRubarBoundaryType type_;
  double xCentre_;
  double yCentre_;
  double zCentre_;

  /**
   * @param _t la variable demande
   * @return l'evolution correspondante
   */
  public EvolutionReguliereInterface getEvol(final H2dVariableType _t) {
    return evolutions.get(_t);
  }

  protected EvolutionReguliere getEvolutionModifiable(H2dVariableType variableType) {
    EvolutionReguliere evolutionReguliere = evolutions.get(variableType);
    if (evolutionReguliere == null) {
      evolutionReguliere = new EvolutionReguliere();
      evolutions.put(variableType, evolutionReguliere);
    }

    return evolutionReguliere;
  }

  public List<H2dVariableType> getVariables(){
    return new ArrayList<>(evolutions.keySet());
  }


  /**
   * @return -1 si vide
   */
  public double getLastTimeStep() {
    for (EvolutionReguliere ev : evolutions.values()) {
      if (ev != null && ev.getNbValues() >= 0) {
        return ev.getMaxX();
      }
    }
    return -1;
  }

  public final double getLongueur() {
    return longueur_;
  }

  /**
   * @return le type de l'ouvrage
   */
  public final H2dRubarOuvrageRef getOuvType() {
    return ouvType_;
  }

  /**
   * @return le type de frontiere
   */
  public final H2dRubarBoundaryType getType() {
    return type_;
  }

  public final double getXCentre() {
    return xCentre_;
  }

  public final double getYCentre() {
    return yCentre_;
  }

  public final double getZCentre() {
    return zCentre_;
  }

  public boolean isBcEdge() {
    return type_ != null;
  }

  /**
   * @return true si concentrationLue
   */
  public boolean isConcentrationSet() {
    return !evolutions.isEmpty();
  }

  public boolean isEmpty() {
    for (EvolutionReguliere ev : evolutions.values()) {
      if (ev != null && ev.getNbValues() >= 0) {
        return false;
      }
    }
    return true;
  }

  public boolean isOuvrageEdge() {
    return ouvType_ != null;
  }

  protected void addValue(H2dVariableType variable, double time, double value) {
    getEvolutionModifiable(variable).add(time, value);
  }

  protected final void setLongueur(final double _longueur) {
    longueur_ = _longueur;
  }

  /**
   * @param _i la ref lue dans le ficheir out
   * @return true si supportee
   */
  protected boolean setRef(final int _i) {
    if (_i >= 0) {
      type_ = H2dRubarBcTypeList.getExternType(_i);
      if (type_ != null) {
        return true;
      }
    } else if (_i == -1) {
      ouvType_ = H2dRubarOuvrageRef.COMPUTE_WITH_SAINT_VENANT;
      return true;
    } else if (_i == -2) {
      ouvType_ = H2dRubarOuvrageRef.NO_COMPUTE_WITH_SAINT_VENANT;
      return true;
    }
    return false;
  }

  protected final void setXCentre(final double _centre) {
    xCentre_ = _centre;
  }

  protected final void setYCentre(final double _centre) {
    yCentre_ = _centre;
  }

  final void setZCentre(final double _centre) {
    zCentre_ = _centre;
  }
}
