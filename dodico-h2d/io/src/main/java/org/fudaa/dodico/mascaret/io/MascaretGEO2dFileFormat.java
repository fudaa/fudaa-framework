/*
 * @creation     13 nov. 2008
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.mascaret.io;

import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class MascaretGEO2dFileFormat extends FileFormatUnique {

  public MascaretGEO2dFileFormat() {
    super(1);
    nom_ = DodicoLib.getS("Mascaret profils 2d");
    description_ = H2dResource.getS("Fichier contenant des profils géoréférencés");
    extensions_=new String[]{"georef"};
  }
  static final MascaretGEO2dFileFormat INSTANCE = new MascaretGEO2dFileFormat();

  /**
   * @return singleton
   */
  public static MascaretGEO2dFileFormat getInstance() {
    return INSTANCE;
  }
  
  @Override
  public FileReadOperationAbstract createReader() {
    return new MascaretGEOReader();
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new MascaretGEOWriter(false);
  }

}
