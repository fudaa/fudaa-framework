/**
 * @creation 14 juin 2004
 * @modification $Date: 2007-01-10 09:04:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.io.EOFException;
import java.io.IOException;
import java.util.Arrays;

import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.collection.CtuluArrayDouble;

import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: RubarFRTDIFReader.java,v 1.2 2007-01-10 09:04:26 deniger Exp $
 */
public class RubarFRTDIFReader extends FileOpReadCharSimpleAbstract {

  int nbElt_ = -1;

  @Override
  protected Object internalRead() {
    if (nbElt_ <= 0) {
      analyze_.addFatalError(H2dResource.getS("Le nombre d'�lements n'est pas pr�cis�"));
      return null;
    }
    final double[] frt = new double[nbElt_];
    try {
      final int[] fmt = new int[10];
      final int nbFieldByLine = fmt.length;
      Arrays.fill(fmt, 8);
      int tmpOnLine = 0;
      in_.readFields(fmt);
      final ProgressionUpdater up = new ProgressionUpdater(progress_);
      up.setValue(3, nbElt_);
      for (int i = 0; i < nbElt_; i++) {
        if (tmpOnLine == (nbFieldByLine)) {
          in_.readFields(fmt);
          tmpOnLine = 0;
        }
        frt[i] = in_.doubleField(tmpOnLine);
        tmpOnLine++;
        up.majAvancement();
      }
    } catch (final EOFException e) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("Fin du fichier");
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
      return null;
    } catch (final NumberFormatException e) {
      analyze_.manageException(e, in_.getLineNumber());
      return null;
    }
    return new CtuluArrayDouble(frt);
  }

  /**
   * @return le nombre d'element a lire
   */
  public int getNbElt() {
    return nbElt_;
  }

  /**
   * @param _nbElt le nombre d'elements � lire
   */
  public void setNbElt(final int _nbElt) {
    nbElt_ = _nbElt;
  }
}