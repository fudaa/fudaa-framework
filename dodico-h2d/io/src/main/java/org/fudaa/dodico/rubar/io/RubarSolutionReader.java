/**
 * @creation 16 d�c. 2004
 * @modification $Date: 2007-06-05 08:59:15 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import com.memoire.fu.FuLog;
import gnu.trove.TDoubleArrayList;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.fichiers.DodicoLineNumberReader;
import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.h2d.resource.H2dResource;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Permet de lire les fichiers TPS et TPC. Il faut pr�ciser le nombre d'elements et le nombre de variable. TPS=3 variables Transport TPC=3 aussi
 *
 * @author Fred Deniger
 * @version $Id: RubarSolutionReader.java,v 1.16 2007-06-05 08:59:15 deniger Exp $
 */
public class RubarSolutionReader extends RubarSolutionReaderAbstract {
  private boolean isTruncatedResultats_;
  private boolean lineError_;
  private DodicoLineNumberReader specReader_;
  private double timeRemoved_;

  protected long getCurrentPosInReader() {
    return specReader_.getPositionInStream();
  }

  public ProgressionInterface getProg() {
    return super.progress_;
  }

  @Override
  protected Object internalRead() {
    if (nbElt_ <= 0) {
      return null;
    }
    final TDoubleArrayList time = new TDoubleArrayList();
    final RubarSolutionSequentielResult r = new RubarSolutionSequentielResult();
    // intialisation de base
    r.nbVar_ = nbValues_;
    r.nbElt_ = nbElt_;
    FileChannel ch = null;
    try {
      final String s = in_.readLine();
      // on recupere le premier pas de temps
      time.add(Double.parseDouble(s));
      // on enregistre la longueur de la ligne
      r.timeLength_ = getCurrentPosInReader();
      // Le nombre de ligne pour chaque valeur
      r.nbLigne_ = (int) Math.ceil(nbElt_ / nbValueByLigne_);
      // on lit la premiere ligne, pour conna�tre la taille d'une ligne
      in_.readLine();
      long pos = getCurrentPosInReader();
      r.resultLength_ = pos - r.timeLength_;
      // on lit les autres lignes sauf la derniere

      if (r.nbLigne_ > 1) {
        for (int i = r.nbLigne_ - 2; i > 0; i--) {
          in_.readLine();
        }
        pos = getCurrentPosInReader();
        // lecture de la derniere ligne
        in_.readLine();
        r.resultLastLineLength_ = getCurrentPosInReader() - pos;
      } // Cas particulier ou il y a moins de 10 elements dans le maillage
      else {
        r.resultLastLineLength_ = r.resultLength_;
        r.resultLength_ = 0;
      }
      pos = getCurrentPosInReader();
      // on a plus besoin du linereader
      in_.close();
      in_ = null;
      // represente la taille prise pour les valeurs d'une variable
      long lengthForVariable = r.resultLastLineLength_ + (r.nbLigne_ - 1) * r.resultLength_;
      // on va se balader dans le fichier pour rechercher les pas de temps
      ch = new FileInputStream(f_).getChannel();
      ch.position(pos + (nbValues_ - 1) * lengthForVariable);
      final ByteBuffer buf = ByteBuffer.allocate((int) r.timeLength_);
      // represente la taille prise pour les valeurs de toutes les variables.
      lengthForVariable = lengthForVariable * nbValues_;
      while (ch.position() < (ch.size() - r.timeLength_)) {
        buf.rewind();
        if (ch.read(buf) != r.timeLength_) {
          break;
        }
        // positions.add(ch.position());
        buf.rewind();
        final String str = new String(buf.array());
        final char c = str.charAt(str.length() - 1);
        if (c != '\r' && c != '\n') {
          lineError_ = true;
        }
        try {
          time.add(Double.parseDouble(str));
        } catch (final NumberFormatException e) {
          analyze_.addError(e.getMessage(), time.size());
          isTruncatedResultats_ = true;
          timeRemoved_ = time.get(time.size() - 1);
          break;
        }

        ch.position(ch.position() + lengthForVariable);
      }
      // pour le dernier pas de temps :: si incomplet on ne le lit pas
      if (ch.size() - ch.position() < 0) {
        analyze_.addError(H2dResource.getS("Fichier tronqu�"), -1);
        isTruncatedResultats_ = true;
        timeRemoved_ = time.remove(time.size() - 1);
      }
    } catch (final EOFException e) {
    } catch (final NumberFormatException e) {
      e.printStackTrace();
      analyze_.manageException(e);
    } catch (final IOException e) {
      e.printStackTrace();
    } finally {
      try {
        if (ch != null) {
          ch.close();
        }
      } catch (final IOException _evt) {
        FuLog.error(_evt);
      }
    }
    r.timeStep_ = time.toNativeArray();
    return r;
  }

  /**
   * On utilise un reader sp�cial qui permet de conna�tre la position.
   */
  @Override
  protected void setFile(final Reader _r) {
    specReader_ = new DodicoLineNumberReader(_r);
    in_ = new FortranReader(specReader_);
  }

  public final double getTimeRemoved() {
    return timeRemoved_;
  }

  public final boolean isLineError() {
    return lineError_;
  }

  public final boolean isTruncatedResultats() {
    return isTruncatedResultats_;
  }
}
