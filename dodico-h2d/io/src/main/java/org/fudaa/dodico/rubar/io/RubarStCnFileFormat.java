/*
 * @creation 4 d�c. 06
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.io.File;

import com.memoire.bu.BuFileFilter;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author fred deniger
 * @version $Id$
 */
public class RubarStCnFileFormat extends FileFormatUnique {
  
  private RubarStCnFileFormat() {
    super(1);
    extensions_ = new String[] { "st", "cn", "m" };
    super.description_ = H2dResource.getS("Fichier de sections");
    super.id_ = "RUBAR_ST";
    super.nom_ = "Cemagref st";

  }
  
  private static final RubarStCnFileFormat INSTANCE = new RubarStCnFileFormat();

  /**
   * @return singleton
   */
  public static RubarStCnFileFormat getInstance() {
    return INSTANCE;
  }

  public static BuFileFilter createStFilter() {
    return new BuFileFilter(new String[] { "st", "cn" }, "Cemagref st");

  }

  @Override
  public CtuluIOOperationSynthese read(final File _f, final ProgressionInterface _prog) {
    return super.read(_f, _prog);
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new RubarStReader();
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return new RubarStWriter();
  }

}
