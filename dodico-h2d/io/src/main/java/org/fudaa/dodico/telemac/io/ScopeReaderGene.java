package org.fudaa.dodico.telemac.io;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * Classe reader de fichier au format scope Gene, evolu� par rapport a s ou t. Utilise fortranReader
 *
 * @author Adrien Hadoux
 *
 */
public class ScopeReaderGene extends FileOpReadCharSimpleAbstract implements CtuluActivity {

  FileFormatVersionInterface v_;
  private String name_;
  ScopeKeyWord key = new ScopeKeyWord();
  private int nbOctets_;
  private String extension_;

  public ScopeReaderGene(final FileFormatVersionInterface _v) {
    v_ = _v;
  }

  @Override
  protected void processFile(final File _f) {
    name_ = CtuluLibFile.getSansExtension(_f.getName());
    nbOctets_ = (int) _f.length();
    extension_ = CtuluLibFile.getExtension(_f.getName());
  }

  @Override
  protected Object internalRead() {
    return readStructure();
  }

  @Override
  public void stop() {
  }

  public void setProgression(int val, String desc) {
    if (progress_ != null) {
      progress_.setProgression(val);
      progress_.setDesc(desc);
    }
  }

  /**
   * Lit selon l extension un format gen ou alors s ou t plsu simple a lire.
   *
   * @return
   */
  ScopeStructure.Gene readStructure() {

    if (super.in_ == null) {
      analyze_.addError(H2dResource.getS("Flux d'entr�e non trouv�"), 0);
      return null;
    }

    ScopeStructure.Gene structure = new ScopeStructure.Gene();



    try {
      if ((progress_ != null) && (nbOctets_ > 0)) {
        setProgression(0, H2dResource.getS("R�cup�ration des titres et nons des variables"));
      }

      // -- creation du type de courbe contenu dans les datas --//
      structure.type = extension_;

      // --on lit touts les titres tant qu il y a un com --//

      readHeader(structure);


      readVariable(structure);

      setProgression(30, H2dResource.getS("R�cup�ration des valeurs"));

      // --on lit le tableau IPARAM --//
      in_.readFields();
      final int MODE_ECLATE = in_.intField(0);
      final int SEPARATOR = in_.intField(1);

      int indicePasDeTemps = -1;

      in_.readFields();
      // -- on lit toutes les pas de temps --//
      try {
        while (true) {


          if (!key.isBlocCommentaireGENE(in_.getLine())) {

            if (MODE_ECLATE == 0) {

              readValuesModeNonEclate(structure, SEPARATOR);

            } else if (MODE_ECLATE == 1) {

              readValuesModeEclate(structure, SEPARATOR);

            } else {
              in_.readFields();
            }
          } else {
            in_.readFields();
          }
        }

      } catch (EOFException Exc) {
        return structure;
      }

    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }

  }

  public void readHeader(ScopeStructure.Gene structure) throws IOException {
    String ligne = null;
    do {
      in_.readFields();

      ligne = in_.getLine();
      // titre
      if (key.isBlocCommentaireGENE(ligne)) {
        structure.addTitle(ligne);
      }

    } while (key.isBlocCommentaireGENE(ligne));

  }

  public void readVariable(ScopeStructure.Gene structure) throws IOException {
    // --on lit le nombre de variables--//
    int nbVar = in_.intField(0);

    // -- on lit les params des pas de temps et on init les liste de var
    // associees
    for (int i = 0; i < nbVar; i++) {
      in_.readFields();

      structure.addVariable(in_.getLine());
    }
  }

  /**
   * MODE ECLATE 0 == toutes les valeurs des variabels sont sur la meme ligne
   *
   * @param structure
   * @param SEPARATOR
   * @param indicePasDeTemps
   * @throws IOException
   */
  public void readValuesModeNonEclate(ScopeStructure.Gene structure, int SEPARATOR)
          throws IOException {
    if (in_.getLine().startsWith(key.getScopeGENESeparator(SEPARATOR))) {

      // -- on est arriv� a une ligne de pas de temps, on ajoute le pas de temps
      // dans la structure --//
      String valuePasDetemps = in_.getLine();
      structure.addSeparator(valuePasDetemps);


      in_.readFields();

      // -- tant qu'on ne revient pas a un nouveau pas de temps on
      // ajoute les var --//
      while (!in_.getLine().startsWith(key.getScopeGENESeparator(SEPARATOR))) {
        if (!key.isBlocCommentaireGENE(in_.getLine())) {
          // ce sont des var on les ajoute toutes
          int nbFields = in_.getNumberOfFields();
          for (int i = 0; i < nbFields; i++) {
            // on inscrit la valeur pour la variable du pas de temps
            // correspondant
            double value = key.VALUE_UNDEFINED;
            if (!key.isUndefined(in_.stringField(i))) {
              value = in_.doubleField(i);
            }


            structure.addValueForVariableAtSeparator(value, i, valuePasDetemps);
          }
        }
        // on lit la suite
        in_.readFields();
      }

    } else {
      in_.readFields();
    }
  }

  /**
   * MODE ECLATE 1 == toutes les valeurs des variabels sont sur la meme ligne
   *
   * @param structure
   * @param SEPARATOR
   * @param indicePasDeTemps
   * @throws IOException
   */
  public void readValuesModeEclate(ScopeStructure.Gene structure, int SEPARATOR)
          throws IOException {
    if (in_.getLine().startsWith(key.getScopeGENESeparator(SEPARATOR))) {

      // -- on est arriv� a une ligne de pas de temps --//

      String valuePasDetemps = in_.getLine();
      structure.addSeparator(valuePasDetemps);

      in_.readFields();

      // -- tant qu'on ne revient pas a un nouveau pas de temps on
      // ajoute les var --//
      while (!in_.getLine().startsWith(key.getScopeGENESeparator(SEPARATOR))) {

        // -- il faut passer les lignes qui comencent par VARIABLE= ou
        // commentaire --//
        if (!key.isBlocCommentaireGENE(in_.getLine()) && !in_.getLine().startsWith("VARIABLE =")) {

          // ce sont des var on les lis ligne par ligne
          int indiceVar = 0;
          // tant qu on ne revient pas sur une ligne du type VAR=
          while (!in_.getLine().startsWith("VARIABLE =")) {


            // on inscrit la valeur pour la variable du pas de temps
            // correspondant
            double value = in_.doubleField(0);
            structure.addValueForVariableAtSeparator(value, indiceVar, valuePasDetemps);
            indiceVar++;

            // on lit la var suivante
            in_.readFields();
          }

        } else // on lit la suite
        {
          in_.readFields();
        }
      }

    }

  }
}
