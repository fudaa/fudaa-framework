/**
 *  @creation     12 mars 2004
 *  @modification $Date: 2007-01-10 09:04:24 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;

import org.fudaa.dodico.h2d.type.H2dProjetType;

/**
 * @author Fred Deniger
 * @version $Id: RefluxSolutionAdapter.java,v 1.10 2007-01-10 09:04:24 deniger Exp $
 */
public class RefluxSolutionAdapter implements RefluxSolutionInterface {

  SIByTime[] times_;
  H2dProjetType type_;

  public RefluxSolutionAdapter() {
    super();
  }

  /**
   * @return le type de projet
   */
  public H2dProjetType getProjetType() {
    return type_;
  }

  /**
   * @return le nombre de pas de temps
   */
  @Override
  public int getNbTimeStep() {
    return times_.length;
  }

  /**
   * @author Fred Deniger
   * @version $Id: RefluxSolutionAdapter.java,v 1.10 2007-01-10 09:04:24 deniger Exp $
   */
  public static class SIByTime {

    private final double t_;
    CtuluCollectionDouble[] values_;

    /**
     * @param _t
     * @param _values
     */
    public SIByTime(final double _t, final CtuluCollectionDouble[] _values) {
      values_ = _values;
      t_ = _t;
    }

    /**
     * @return le nombre de valeurs
     */
    public int getNbValues() {
      return values_.length;
    }

    /**
     * @return le nombre de points
     */
    public int getNbPt() {
      return values_[0].getSize();
    }

    /**
     * @param _idxValues l'index de la valeur voulu
     * @param _idxPoint l'index du point
     * @return la valeur sans verif de limites
     */
    public double getValues(final int _idxValues, final int _idxPoint) {
      return values_[_idxValues].getValue(_idxPoint);
    }

    /**
     * @param _values The values to set.
     */
    public void setValues(final CtuluCollectionDouble[] _values) {
      values_ = _values;
    }

    /**
     * @return Returns the t.
     */
    public double getT() {
      return t_;
    }
  }

  /**
   * @return Returns the times.
   */
  public SIByTime[] getTimes() {
    return times_;
  }

  @Override
  public CtuluCollectionDouble getData(final int _idxVar, final int _time) {
    return times_[_time].values_[_idxVar];
  }

  @Override
  public int getNbPt() {
    return CtuluLibArray.isEmpty(times_) ? 0 : times_[0].getNbPt();
  }

  @Override
  public int getNbValue() {
    return CtuluLibArray.isEmpty(times_) ? 0 : times_[0].getNbValues();
  }

  @Override
  public double getTimeStep(final int _i) {
    return times_[_i].getT();
  }

  /**
   * @param _times The times to set.
   */
  public void setTimes(final SIByTime[] _times) {
    times_ = _times;
  }
}
