/**
 * 
 */
package org.fudaa.dodico.rubar.io;

import java.util.ArrayList;
import java.util.List;

import org.fudaa.ctulu.gis.GISPoint;

/**
 * @author CANEL Christophe (Genesis)
 */
public class RubarNUAResult {
  public static class RubarNUAResultLine {
    private final int areteIdx;
    private final GISPoint point;
    private final double coeff;

    public RubarNUAResultLine(int areteIdx, GISPoint point, double coeff) {
      this.point = point;
      this.coeff = coeff;
      this.areteIdx = areteIdx;
    }

    public int getAreteIdx() {
      return areteIdx;
    }

    /**
     * @return the point
     */
    public GISPoint getPoint() {
      return point;
    }

    /**
     * @return the coeff
     */
    public double getCoeff() {
      return coeff;
    }

  }

  public static class RubarNUAResultBloc {
    private double time;
    private List<RubarNUAResultLine> lines = new ArrayList<RubarNUAResultLine>();

    public RubarNUAResultBloc(double time) {
      this.time = time;
    }

    /**
     * @return the time
     */
    public double getTime() {
      return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(double time) {
      this.time = time;
    }

    /**
     * @return the lines
     */
    public List<RubarNUAResultLine> getLines() {
      return lines;
    }
  }

  private List<RubarNUAResultBloc> blocs = new ArrayList<RubarNUAResultBloc>();

  public int getNbBlocSize() {
    return blocs.size();
  }

  public RubarNUAResultBloc getBloc(int idx) {
    return blocs.get(idx);
  }

  /**
   * @return the blocs
   */
  public List<RubarNUAResultBloc> getBlocs() {
    return blocs;
  }
}
