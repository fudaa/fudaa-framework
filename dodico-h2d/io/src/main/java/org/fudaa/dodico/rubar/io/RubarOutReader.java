/**
 * @creation 7 f�vr. 2005
 * @modification $Date: 2007-05-04 13:47:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.fortran.FileOpReadCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2dRubarBcMng;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: RubarOutReader.java,v 1.10 2007-05-04 13:47:30 deniger Exp $
 */
public class RubarOutReader extends FileOpReadCharSimpleAbstract {
  public final boolean isReadConcentration() {
    return readConcentration_;
  }

  public final void setReadConcentration(final boolean _readConcentration) {
    readConcentration_ = _readConcentration;
  }

  public final void setRes(final TIntObjectHashMap _r) {
    res_ = _r;
  }

  TIntObjectHashMap res_;
  boolean readConcentration_;

  private int[] getNewFmt(int lineLength) {
    final int lengthValueColumn = 12;
    boolean firstIndexIsNine = lineLength % lengthValueColumn == 0;
    int firstIndexSize = firstIndexIsNine ? 9 : 6;
    int nbBlockReal = (lineLength - (firstIndexSize + 3)) / lengthValueColumn;
    int[] newFmt = new int[2 + nbBlockReal];
    Arrays.fill(newFmt, lengthValueColumn);
    newFmt[0] = firstIndexSize;
    newFmt[newFmt.length - 1] = 3;
    return newFmt;
  }

  String filename_;

  @Override
  public void setFile(final File _f) {
    super.setFile(_f);
    if (_f != null) {
      filename_ = _f.getName();
    }
  }

  public boolean isTempLine(final String _s) {
    return _s != null && _s.startsWith(tempsId_) && _s.indexOf(":") > 0;
  }

  final String tempsId_ = "TEMPS";

  @Override
  protected Object internalRead() {
    if (res_ == null) {
      res_ = new TIntObjectHashMap();
    }
    try {
      in_.setJumpBlankLine(true);
      String line = in_.readLine().trim();

      while (line != null && !isTempLine(line)) {
        line = in_.readLine().trim();
      }
      int[] newFmt = null;
      while (line != null && isTempLine(line)) {
        final int idx = line.indexOf(':');
        final double t = Double.parseDouble(line.substring(idx + 1));
        // on lit l'entete
        in_.readLine();
        // on lit le separateur ---
        in_.readLine();
        if (newFmt == null) {
          in_.readFields();
        } else {
          in_.readFields(newFmt);
        }
        // on lit les aretes pour un pas de temps
        line = in_.getLine();
        while (line != null && !isTempLine(line)) {
          // la fin du fichier finit par des volumes
          if (in_.getLine().trim().startsWith("VOLUME")) {
            return res_;
          }
          // fichier mas, les donn�es se finissent par un s�parateur on oublie tout
          else if (in_.getLine().indexOf("---") > 0) {
            while (!isTempLine(line)) {
              line = in_.readLine().trim();
            }
            break;
          }
          // on passe dans le nouveau format si on n'a pas 8 champs
          if (newFmt == null && in_.getNumberOfFields() != 8) {
            newFmt = getNewFmt(in_.getLine().length());
            in_.analyzeCurrentLine(newFmt);
          }
          final int idxA = in_.intField(0) - 1;
          if (idxA < 0) {
            analyze_.addFatalError(CtuluLib.getS("format du fichier {0} n'est pas correct", getFilename()), in_.getLineNumber());
            return null;
          }
          RubarOutEdgeResult res = (RubarOutEdgeResult) res_.get(idxA);
          if (res == null) {
            res = readEdgeResult();
            final boolean conforme = res.setRef(in_.intField(in_.getNumberOfFields() - 1));
            if (!conforme) {
              analyze_.addFatalError(H2dResource.getS("Code inconnu") + CtuluLibString.getEspaceString(in_.intField(in_.getNumberOfFields() - 1)));
              return null;
            }
            res_.put(idxA, res);
          }
          updateEvols(t, res);
          in_.readFields();
          line = in_.getLine().trim();
          if (line != null && newFmt != null && !line.startsWith(tempsId_)) {
            in_.analyzeCurrentLine(newFmt);
          }
        }
      }
    } catch (final EOFException e) {
      return res_;
    } catch (final NumberFormatException e) {
      analyze_.addFatalError(CtuluLib.getS("Le format du fichier {0} n'est pas correct ou le fichier est vide", getFilename()), in_.getLineNumber());
      e.printStackTrace();
      return null;
    } catch (final IOException e) {
      analyze_.manageException(e);
      return null;
    }
    return res_;
  }

  private String getFilename() {
    return filename_ == null ? "OUT" : filename_;
  }

  private void updateEvols(final double _t, final RubarOutEdgeResult _res) {
    if (_res == null) {
      return;
    }
    if (!isReadConcentration()) {
      _res.addValue(H2dVariableType.HAUTEUR_EAU, _t, in_.doubleField(1));
      _res.addValue(RubarOutEdgeResult.DEBIT_VAR, _t, in_.doubleField(2));
    } else {
      int nbValueByBlock = 3;
      int nbVariableNonTransport = 5;
      int nbBlock = (in_.getNumberOfFields() - nbVariableNonTransport) / nbValueByBlock;
      final List<H2dVariableTransType> transportVariables = H2dRubarBcMng.getTransportVariables(nbBlock);
      for (int i = 0; i < transportVariables.size(); i++) {
        _res.addValue(transportVariables.get(i), _t, in_.doubleField(2 + i));
      }
    }
  }

  private RubarOutEdgeResult readEdgeResult() {
    RubarOutEdgeResult res;
    res = new RubarOutEdgeResult();
    if (readConcentration_) {
      res.setXCentre(in_.doubleField(in_.getNumberOfFields() - 3));
      res.setYCentre(in_.doubleField(in_.getNumberOfFields() - 2));
    } else {
      res.setLongueur(in_.doubleField(3));
      res.setZCentre(in_.doubleField(4));
      res.setXCentre(in_.doubleField(5));
      res.setYCentre(in_.doubleField(6));
    }
    return res;
  }
}
