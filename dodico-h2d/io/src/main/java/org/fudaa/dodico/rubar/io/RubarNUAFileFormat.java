package org.fudaa.dodico.rubar.io;

import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * 
 * @author CANEL Christophe (Genesis)
 *
 */
public class RubarNUAFileFormat extends FileFormatUnique {
  /**
   * Initialise les donnees.
   */
  public RubarNUAFileFormat() {
    super(1);
    extensions_ = new String[] { "nua"};
    super.description_ = H2dResource.getS("Valeurs du coefficient de diffusion lorsque celui ci est calcul�");
    super.id_ = "RUBAR_NUA";
    super.nom_ = "Rubar nua";
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createReader()
   */
  @Override
  public FileReadOperationAbstract createReader(){
    return new RubarNUAReader();
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createWriter()
   */
  @Override
  public FileWriteOperationAbstract createWriter(){
    return null;
  }
}