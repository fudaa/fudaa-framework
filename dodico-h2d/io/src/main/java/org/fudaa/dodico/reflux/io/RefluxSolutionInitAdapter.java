/*
 *  @creation     20 avr. 2005
 *  @modification $Date: 2007-01-10 09:04:25 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.collection.CtuluArrayDouble;

import org.fudaa.dodico.h2d.reflux.H2dRefluxSolutionInitAdapterInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: RefluxSolutionInitAdapter.java,v 1.6 2007-01-10 09:04:25 deniger Exp $
 */
public final class RefluxSolutionInitAdapter implements H2dRefluxSolutionInitAdapterInterface {

  double[] ze_;
  double[] u_;
  double[] v_;

  /**
   * @param _u
   * @param _v
   * @param _h
   */
  public RefluxSolutionInitAdapter(final double[] _u, final double[] _v, final double[] _h) {
    super();
    u_ = _u;
    v_ = _v;
    ze_ = _h;
  }

  @Override
  public boolean isValideVariable(final H2dVariableType _t) {
    return _t == H2dVariableType.VITESSE_U || _t == H2dVariableType.VITESSE_V || _t == H2dVariableType.COTE_EAU;
  }

  @Override
  public void setValuesIn(final H2dVariableType _t, final CtuluArrayDouble _dest,
      final CtuluCommandContainer _newParam) {
    if (_t == H2dVariableType.COTE_EAU) {
      _dest.setAll(ze_, _newParam);
    } else if (_t == H2dVariableType.VITESSE_U) {
      _dest.setAll(u_, _newParam);
    } else if (_t == H2dVariableType.VITESSE_V) {
      _dest.setAll(v_, _newParam);
    } else {
      FuLog.error("variable " + _t.getName() + " not supported");
    }
  }

  @Override
  public double getCoteEau(final int _idx) {
    return ze_[_idx];
  }

  @Override
  public int getNbPt() {
    return ze_.length;
  }

  @Override
  public double getU(final int _idx) {
    return u_[_idx];
  }

  @Override
  public double getV(final int _idx) {
    return v_[_idx];
  }

}
