package org.fudaa.dodico.rubar.io;

import java.io.File;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author CANEL Christophe (Genesis)
 */
public class RubarDURFileFormat extends FileFormatUnique {
  private static RubarDURFileFormat instance_;

  /**
   * Initialise les donnees.
   */
  public RubarDURFileFormat() {
    super(1);
    extensions_ = new String[] { "dur" };
    super.description_ = H2dResource.getS("Cotes du fond inérodable (version avec transport)");
    super.id_ = "RUBAR_DUR";
    super.nom_ = "Rubar dur";
  }

  /**
   * @return l'instance a utiliser
   */
  public static RubarDURFileFormat getInstance() {
    // pas besoin de synchronise. S'il y a 2 instances, c'est pas grave
    if (instance_ == null) {
      instance_ = new RubarDURFileFormat();
    }
    return instance_;
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createReader()
   */
  @Override
  public FileReadOperationAbstract createReader() {
    return new RubarDURReader();
  }

  /**
   * @see org.fudaa.ctulu.fileformat.FileFormatVersion#createWriter()
   */
  @Override
  public FileWriteOperationAbstract createWriter() {
    return new RubarDURWriter();
  }

  /**
   * @param f
   * @param prog
   * @param nbNoeuds
   * @return la synthese de l'operation
   */
  public CtuluIOOperationSynthese read(final File f, final ProgressionInterface prog, final int nbNoeuds) {
    final RubarDURReader i = new RubarDURReader();
    i.setNbNoeuds(nbNoeuds);
    final CtuluIOOperationSynthese r = i.read(f, prog);
    i.setProgressReceiver(null);
    return r;
  }
}