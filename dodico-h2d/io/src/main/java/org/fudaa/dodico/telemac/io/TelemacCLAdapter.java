/**
 * @modification $Date: 2006-09-19 14:45:49 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.telemac.io;

import org.fudaa.dodico.h2d.telemac.H2dTelemacCLElementSource;
import org.fudaa.dodico.h2d.telemac.H2dTelemacCLSourceInterface;

/**
 * @author deniger
 * @version $Id: TelemacCLAdapter.java,v 1.18 2006-09-19 14:45:49 deniger Exp $
 */
public class TelemacCLAdapter implements H2dTelemacCLSourceInterface {

  private final H2dTelemacCLElementSource[] l_;

  /**
   * Les conditions limites passees en parametres doivent etre dans l'ordre de la numerotation des pts frontiere.
   * 
   * @param _l les cl.
   */
  public TelemacCLAdapter(final H2dTelemacCLElementSource[] _l) {
    l_ = _l;
  }

  /**
   * Autant que de points frontieres.
   */
  @Override
  public int getNbLines() {
    return l_.length;
  }

  /**
   * @param _frIdx l'indice de frontiere
   * @return l'indice global correspondant a l'indice de frontier <code>_frIdx</code>
   */
  public int getGlobalIdx(final int _frIdx) {
    return l_[_frIdx].ptGlobalIdx_;
  }

  /**
   *
   */
  @Override
  public boolean getLine(final int _i, final H2dTelemacCLElementSource _ele) {
    _ele.initFrom(l_[_i]);
    return true;
  }
}