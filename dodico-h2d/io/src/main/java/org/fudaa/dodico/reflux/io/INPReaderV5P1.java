/*
 * @creation 21 mars 2003
 * @modification $Date: 2007-06-29 15:10:24 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import gnu.trove.TIntArrayList;

import java.io.IOException;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibArray;

import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.h2d.H2dNodalPropertyMixteMutable;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBordIndexGeneral;
import org.fudaa.dodico.h2d.reflux.H2dRefluxBoundaryConditionMutable;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.mesure.EvolutionReguliere;

/**
 * @author deniger
 * @version $Id: INPReaderV5P1.java,v 1.1 2007-06-29 15:10:24 deniger Exp $
 */
public class INPReaderV5P1 extends INPReaderAbstract {

  H2dRefluxBoundaryConditionMutable[] cl_;

  TIntArrayList elemWithNoGroup_;

  TIntArrayList[] groupeByElem_;

  /**
   * le reader : format par defaut (le dernier).
   */
  public INPReaderV5P1() {
    this(INPFileFormat.getInstance().getLastV5P1Impl());
  }

  /**
   * @param _f la version a utiliser.
   */
  public INPReaderV5P1(final RefluxINPV5P1Version _f) {
    super(_f);
  }

  @Override
  protected void analyzeElemMailEntete() {
    nbGroupElem_ = in_.intField(4);
    nbPropEle_ = in_.intField(5);
    groupeByElem_ = initArrayListForGroup();
    elemWithNoGroup_ = new TIntArrayList(50);
  }

  /**
   * D�ja cr�e dans le bloc POIN.
   */
  @Override
  protected H2dRefluxBoundaryConditionMutable[] createConditionLimites() {
    return cl_;
  }

  @Override
  protected int[][] defineGroupe(final TemplateGroupeElement[] _propElem) throws IOException {
    int nb;
    // cas bizarre rencontr� avec reflux V5P0: indice null= libre pour toutes les propri�t�s
    if (elemWithNoGroup_.size() > 0) {
      nb = elemWithNoGroup_.size();
      for (int i = 0; i < nb; i++) {
        int idx = elemWithNoGroup_.getQuick(i);
        for (int j = 0; j < nbPropEle_; j++) {
          _propElem[j].ajouteLibre(idx);
        }
      }
    }
    return CtuluLibArray.toArray(groupeByElem_);

  }

  @Override
  protected boolean finishCreationInElemBloc(int _idxElt, int _nbPtPerElement, H2dRefluxBordIndexGeneral _general) {
    if (_general != null) {
      int bordCode = in_.intField(_nbPtPerElement + 1);
      H2dBoundaryType bordType = version_.getBordType(bordCode);
      if (bordType != null) {
        if (Fu.DEBUG && FuLog.isDebug()) FuLog.debug("DRE: L3 n�= " + bordType.getName());
      } else {
        analyze_.addFatalError(H2dResource.getS("Code d'�l�ment inconnu") + "(" + bordCode + ")", in_.getLineNumber());
        addAnalyzeLigneLue();
        return false;
      }
      _general.setBord(bordType);
    }
    int gr = in_.intField(_nbPtPerElement + 2) - 1;
    if (gr < 0) elemWithNoGroup_.add(_idxElt);
    else
      groupeByElem_[gr].add(_idxElt);
    return true;

  }

  @Override
  protected boolean isElemBord(int _nbPtPerElement) {
    // le nouveau format de reflux indique le code de l'element en 8eme colonne
    return in_.intField(_nbPtPerElement + 1) != version_.getCodeFond();
  }

  @Override
  protected void readBlocElem() throws IOException {
    in_.analyzeCurrentLine(version_.getELEMFormat());
  }

  @Override
  protected EfNode[] readCoord() throws IOException {
    in_.readFields(version_.getCOORFormat());
    if (!"POIN".equals(in_.stringField(0))) {
      analyze_.addFatalError(H2dResource.getS("Le bloc des coordonn�es est introuvable"), in_.getLineNumber());
      addAnalyzeLigneLue();
      return null;
    }
    int nbCoord = in_.intField(2);
    if (nbCoord != 3) {
      analyze_.addFatalError(H2dResource.getS("Le nombre de coordonn�es doit �tre �gal � 3"), in_.getLineNumber());
      addAnalyzeLigneLue();
      return null;
    }
    int nbPt = in_.intField(3);
    if (nbPt <= 0) {
      analyze_.addFatalError(H2dResource.getS("Le nombre de points est invalide"), in_.getLineNumber());
      addAnalyzeLigneLue();
      return null;
    }

    int nbPropNodal = in_.intField(5);
    // construction du maillage
    // SMaillageIndex maillage = new SMaillageIndex();
    final EfNode[] pts = new EfNode[nbPt];
    cl_ = new H2dRefluxBoundaryConditionMutable[nbPt];
    up_.setValue(10, nbPt);
    up_.majProgessionStateOnly(H2dResource.getS("Lecture des noeuds"));
    // l'indice dans les colonnes de la premieres donn�es concernant les props nodales
    final int idxDebPropNodal = 5;
    int nbPropNodalSup = nbPropNodal - 2;
    // normalement, reflux ne prevoit que 3 colonnes max, donc on aurait pu se passer du tableau
    // mais on ne sait jamais
    // la premiere colonne = angle
    // 2eme = debi
    // 3eme= precipitation
    // pas d'autre mais il se peut bien d'ou:
    H2dNodalPropertyMixteMutable[] propNodales = null;
    if (nbPropNodal > 2) {
      propNodales = new H2dNodalPropertyMixteMutable[nbPropNodalSup];
      propNodales[0] = new H2dNodalPropertyMixteMutable("precipitation", nbPt);
      for (int i = 1; i < nbPropNodalSup; i++) {
        propNodales[i] = new H2dNodalPropertyMixteMutable("col " + (nbPropNodal + i), nbPt);
      }
    }
    up_.majProgessionStateOnly("POIN");
    int idxPn = evolsPN_.size() + 1;
    up_.setValue(10, nbPt);
    for (int i = 0; i < nbPt; i++) {
      if (stop_) {
        return null;
      }
      // format libres
      in_.readFields();
      // le nombre de degre de liberte ( la 5eme colonne) ne sert a rien dans notre cas.
      // mis ici en commentaire au cas ou un jour cela serve.
      // int nbLiberte=in_.intField(4);
      pts[i] = new EfNode(in_.doubleField(1), in_.doubleField(2), in_.doubleField(3));
      int angleCode = in_.intField(idxDebPropNodal);
      if (angleCode == version_.getCodePermanent()) {
        cl_[i] = createDefaultCL();
        cl_[i].setIndexPt(i);
        cl_[i].setNormale(in_.doubleField(idxDebPropNodal + 1));
        int idxPrem = idxDebPropNodal + 2;
        int qCode = in_.intField(idxPrem);
        H2dBcType type = version_.getClType(qCode);
        if (type != H2dBcType.LIBRE) {
          // Pour r�cuperer la valeur du fichier inp
          cl_[i].setQTypePermanent(in_.doubleField(idxPrem + 1));
          if (type == H2dBcType.TRANSITOIRE) {
            EvolutionReguliere evol = new EvolutionReguliere("PN" + idxPn++);
            evolsPN_.add(evol);
            cl_[i].setQTransitoire(evol);
          }
        }
      }
      if (propNodales != null) {
        int idxPrem = idxDebPropNodal + 4;
        for (int j = 0; j < nbPropNodalSup; j++) {
          H2dBcType type = version_.getClType(in_.intField(idxPrem + (2 * j)));
          if (type == H2dBcType.PERMANENT) {
            propNodales[j].setFixed(i, in_.doubleField(idxPrem + (2 * j + 1)));
          } else if (type == H2dBcType.TRANSITOIRE) {
            EvolutionReguliere evol = new EvolutionReguliere("PN" + idxPn++);
            evolsPN_.add(evol);
            propNodales[j].setTransient(i, evol);
          }

        }
      }
      up_.majAvancement();
    }
    projet_.setPropNodales(propNodales);
    return pts;
  }

  // inutile dans ce format tout fait dans le bloc COORD/POIN
  @Override
  protected void readDLPN() throws IOException {}


  @Override
  protected boolean readPRELentete() throws IOException {
    return true;
  }

  /**
   * Deja fait dans le bloc COOR/POIN.
   */
  @Override
  protected boolean readPRND(H2dRefluxBoundaryConditionMutable[] _clTemp) throws IOException {
    return true;
  }

  @Override
  protected boolean readTPEL() throws IOException {
    return true;
  }
}