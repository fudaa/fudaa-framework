/**
 * @creation 21 d�c. 2004
 * @modification $Date: 2007-02-15 17:09:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author Fred Deniger
 * @version $Id: RubarTRCResult.java,v 1.4 2007-02-15 17:09:20 deniger Exp $
 */
public class RubarTRCResult implements RubarLimnigrammeResult {
  EvolutionReguliereInterface[][] evols_;

  @Override
  public int getNbPoint() {
    return evols_ == null ? 0 : evols_.length;
  }

  @Override
  public int getNbVar() {
    return (evols_ == null || evols_.length == 0) ? 0 : evols_[0].length;
  }

  @Override
  public int getNbTimeStep() {
    return (evols_ == null || evols_.length == 0 || evols_[0] == null || evols_[0].length == 0 || evols_[0][0] == null) ? 0
        : evols_[0][0].getNbValues();
  }

  @Override
  public EvolutionReguliereInterface getEvolFor(final int _idxVar, final int _nbPoint) {
    if (_idxVar >= evols_[_nbPoint].length) {
      return null;
    }
    return evols_[_nbPoint][_idxVar];
  }

  public EvolutionReguliereInterface getEvolForU(final int _nbPoint) {
    return evols_[_nbPoint][1];
  }

  public EvolutionReguliereInterface getEvolForV(final int _nbPoint) {
    return evols_[_nbPoint][0];
  }

  public EvolutionReguliereInterface getEvolForH(final int _nbPoint) {
    return evols_[_nbPoint][2];
  }
}
