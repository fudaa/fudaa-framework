/**
 * @creation 16 d�c. 2004
 * @modification $Date: 2006-09-28 13:40:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.rubar.io;

import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;

import com.memoire.fu.FuLog;

import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * Permet de lire les fichiers TPS et TPC. Il faut pr�ciser le nombre d'elements et le nombre de variable. TPS=3
 * variables Transport TPC=3 aussi
 * 
 * @author Fred Deniger
 * @version $Id: RubarSolutionCorrection.java,v 1.5 2006-09-28 13:40:53 deniger Exp $
 */
public class RubarSolutionCorrection extends RubarSolutionReaderAbstract {

  FortranWriter out_;

  int valueNbDecimal_ = 10;

  int timeNbDecimal_ = 15;

  public final void setNbValueByLigne(final int _nbValueByLigne) {
    nbValueByLigne_ = _nbValueByLigne;
  }

  public void setOutFile(final File _f) throws IOException {
    out_ = new FortranWriter(new BufferedWriter(new FileWriter(_f)));
  }

  @Override
  protected Object internalRead() {
    if (nbElt_ <= 0) {
      return null;
    }
    if (out_ == null) {
      FuLog.error("Le fichier de sortie est ind�termin�");
      return null;
    }
    if (progress_ != null) {
      progress_.setDesc(H2dResource.getS("Correction du fichier"));
    }

    out_.setSpaceBefore(true);
    final int[] timeFmt = new int[] { timeNbDecimal_ };
    final int[] valuesFmt = new int[(int)nbValueByLigne_];
    Arrays.fill(valuesFmt, valueNbDecimal_);
    final int nbLines = (int) Math.ceil(nbElt_ / nbValueByLigne_);
    try {
      while (true) {
        String s = in_.readLine();
        if (s == null) {
          break;
        }
        s = s.trim();
        out_.stringField(0, s);
        out_.writeFields(timeFmt);
        for (int i = 0; i < nbValues_; i++) {
          for (int j = 0; j < nbLines; j++) {
            in_.readFields(valuesFmt);
            for (int val = 0; val < in_.getNumberOfNotEmptyField(); val++) {
              out_.stringField(val, in_.stringField(val));
            }
            out_.writeFields(valuesFmt);
          }
        }
      }
    } catch (final EOFException e) {

    } catch (final NumberFormatException e) {
      analyze_.manageException(e);
    } catch (final IOException e) {
      e.printStackTrace();
    } finally {
        try {
          out_.close();
        } catch (final IOException _evt) {
          analyze_.manageException(_evt);
        }
    }
    return null;
  }

  /**
   * On utilise un reader sp�cial qui permet de conna�tre la position.
   */
  @Override
  protected void setFile(final Reader _r) {
    in_ = new FortranReader(_r);
  }
}