package org.fudaa.dodico.telemac.io;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.dodico.fortran.FileOpWriterCharSimpleAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * Classe writer de fichier au format scope. Utilise fortranWriter
 * 
 * @author Adrien Hadoux
 * 
 */
public class ScopeWriterSorT extends FileOpWriterCharSimpleAbstract implements CtuluActivity {

  boolean stop_;
  final FileFormatVersionInterface v_;

  ScopeKeyWord key_ = new ScopeKeyWord();
  
  public ScopeWriterSorT(final FileFormatVersionInterface _v) {
    v_ = _v;
   
  }
  
  
  @Override
  public void stop() {
    stop_ = true;
  }

  @Override
  protected void internalWrite(Object _o) {
    if (_o instanceof ScopeStructure) {
      writeStruct((ScopeStructure.SorT) _o);
    } else {
      donneesInvalides(_o);
    }
  }
  
  /**
   * Ecriture de la structure dans un fichier
   * 
   * @param _struct
   */
  protected void writeStruct(ScopeStructure.SorT _struct) {

    if (super.out_ == null) {
      analyze_.addError(H2dResource.getS("Flux d'entr�e non trouv�"), 0);
      return;
    }
    
    // -- ecriture dans le fichier --//
    try {
   // -- ecriture des 3 premieres lignes titres --//
      writeHeader();
      
    // -- ecritures des variables --//
      writeVariable(_struct);
    
 // -- ecritures de toutes les valeurs des variables --//
      writeData(_struct);
      
      
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
  }
  
  public void setProgression(int val, String desc) {
    if (progress_ != null) {
      progress_.setProgression(val);
      progress_.setDesc(desc);
    }
  }
  
  
  protected void writeHeader() throws IOException {
    String bc = key_.getBlocCommentaireSorT();
    setProgression(0, H2dResource.getS("Cr�ation des en-t�tes") + bc);
    writelnToOut(bc + H2dResource.getS("Fichier g�n�r� par Fudaa") + bc);
    final String date = new SimpleDateFormat("yyyy-M-d  k:mm:ss").format(Calendar.getInstance().getTime());
    writelnToOut(bc + "Version " + key_.getVersion() + " - Date " + date + bc);
    writelnToOut(bc);
  }
  
  protected void writeVariable(ScopeStructure.SorT _struct) throws IOException {
    String bc = key_.getBlocCommentaireSorT();
    setProgression(10, H2dResource.getS("Cr�ation des variables"));

    for (int i = 0; i < _struct.getNbVariables(); i++) {
      writelnToOut(bc + _struct.getVariable(i) + bc);
    }
  }
  
  protected void writeData(ScopeStructure.SorT _struct) throws IOException {
    String bc = key_.getBlocCommentaireSorT();
    setProgression(30, H2dResource.getS("Cr�ation des donn�es"));
    for (int i = 0; i < _struct.getNbValuePerVariables(); i++) {
      writelnToOut(_struct.getListValuesofAllVariable(i));
    }
  }
  
  
}
