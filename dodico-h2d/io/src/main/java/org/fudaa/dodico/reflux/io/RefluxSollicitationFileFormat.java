/*
 * @creation 27 avr. 07
 * @modification $Date: 2007-06-28 09:25:07 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.reflux.io;

import java.util.Arrays;

import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;

import org.fudaa.dodico.fortran.FortranDoubleWriter;
import org.fudaa.dodico.h2d.reflux.H2dRefluxDicoVersion;

/**
 * @author fred deniger
 * @version $Id: RefluxSollicitationFileFormat.java,v 1.5 2007-06-28 09:25:07 deniger Exp $
 */
public class RefluxSollicitationFileFormat extends FileFormatUnique {

  int[] fmt_;

  public RefluxSollicitationFileFormat(final int _nbVar, final String _ext) {
    super(1);
    fmt_ = new int[_nbVar];
    Arrays.fill(fmt_, H2dRefluxDicoVersion.getDoubleColSize());
    extensions_ = new String[] { _ext };
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return new RefluxSollicitationReader(fmt_);
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    final FortranDoubleWriter writer = new FortranDoubleWriter(fmt_);
    writer.setEntete("-999");
    return writer;
  }

}
