/**
 *  Format Z Esri File
 * @author Adrien Hadoux
 */
package org.fudaa.dodico.zesri.io;

import java.io.File;
import java.util.List;

import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.gis.GISPoint;

import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fichiers.FileFormatSoftware;

/**
 * @author Adrien Hadoux
 * 
 */
public final class ZESRiFileFormat extends FileFormatUnique {

  static final ZESRiFileFormat INSTANCE = new ZESRiFileFormat();
  
  private String version_="1.0";

  /**
   * @return singleton
   */
  public static ZESRiFileFormat getInstance() {
    return INSTANCE;
  }

  private ZESRiFileFormat() {
    super(1);
    extensions_ = new String[] { "_polyligne.gen","dat","txt" };
    id_ = "ZESRI";
    nom_ = "Zesri";
    description_ = DodicoLib.getS("Comporte les définitions de points,polylignes et polygones");
    software_ = FileFormatSoftware.TELEMAC_IS;
  }
  

  public void setVersionName(String _version) {
    version_=_version;
  }
  
  @Override
  public String getVersionName() {
    return version_;
  }

  @Override
  public FileReadOperationAbstract createReader() {
    return null;
  }

  @Override
  public CtuluIOOperationSynthese write(final File _f, final Object _source, final ProgressionInterface _prog) {
    return createWriter().write(_f, (List<List<GISPoint>>) _source);
  }

  @Override
  public ZESRIWriter createWriter() {
    
      return new ZESRIWriter();
  }

}