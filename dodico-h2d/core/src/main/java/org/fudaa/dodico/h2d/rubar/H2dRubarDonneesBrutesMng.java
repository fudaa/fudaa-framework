/*
 * @creation 1 oct. 2004
 * @modification $Date: 2007-03-02 13:00:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;

import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarDonneesBrutesMng.java,v 1.11 2007-03-02 13:00:52 deniger Exp $
 */
public final class H2dRubarDonneesBrutesMng implements H2dRubarDonneesBrutesListener {
  public static final String BATY_ID = "BATHY";
  public static final String FRT_ID = "FRT";
  public static final String INI_ID = "INI";
  public static final String DIF_ID = "DIF";
  boolean isModified_;
  List listener_;
  Map m_;
  H2dRubarProjetType type_;

  @Override
  public void donneesBrutesNuageDataChanged(final H2dRubarDonneesBrutes _source) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dRubarDonneesBrutesListener) listener_.get(i)).donneesBrutesNuageDataChanged(_source);
      }
    }
    isModified_ = true;
  }

  @Override
  public void donneesBrutesNuageDataNombreChanged(final H2dRubarDonneesBrutes _source) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dRubarDonneesBrutesListener) listener_.get(i)).donneesBrutesNuageDataNombreChanged(_source);
      }
    }
    isModified_ = true;
  }

  @Override
  public void donneesBrutesNuageSupportAddedOrRemoved(final H2dRubarDonneesBrutes _source) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dRubarDonneesBrutesListener) listener_.get(i)).donneesBrutesNuageSupportAddedOrRemoved(_source);
      }
    }
    isModified_ = true;
  }

  @Override
  public void donneesBrutesNuageSupportChanged(final H2dRubarDonneesBrutes _source) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dRubarDonneesBrutesListener) listener_.get(i)).donneesBrutesNuageSupportChanged(_source);
      }
    }
    isModified_ = true;
  }

  protected H2dRubarDonneesBrutesMng(final H2dRubarParameters _params, final H2dRubarNumberFormatter _fmt) {
    type_ = _params.getProjetType();
    m_ = new HashMap();
    H2dRubarDonneesBrutes b = new H2dRubarDonneesBrutesBathy(_fmt);
    b.setListener(this);
    m_.put(BATY_ID, b);
    b = new H2dRubarDonneesBrutesFrottement(_fmt);
    b.setListener(this);
    m_.put(FRT_ID, b);
    b = new H2dRubarDonneesBrutesIni(_params, _fmt);
    b.setListener(this);
    m_.put(INI_ID, b);
    b = new H2dRubarDonneesBrutesDiffusion(_fmt);
    b.setListener(this);
    m_.put(DIF_ID, b);
  }

  public void setNbConcentrationBlocks(final int newNbConcentrationBlocks, final CtuluCommandComposite _cmd) {
    for (final Iterator it = m_.values().iterator(); it.hasNext(); ) {
      ((H2dRubarDonneesBrutes) it.next()).setNbConcentrationBlocks(newNbConcentrationBlocks, _cmd);
    }
  }

  protected void setProjetType(final H2dRubarProjetType _type, final CtuluCommandComposite _cmd) {
    if (type_ != _type) {
      type_ = _type;
      if (m_ != null) {
        for (final Iterator it = m_.values().iterator(); it.hasNext(); ) {
          ((H2dRubarDonneesBrutes) it.next()).setProjectTypeChanged(type_, _cmd);
        }
      }
    }
  }

  public void addListener(final H2dRubarDonneesBrutesListener _l) {
    if (_l == null) {
      return;
    }
    if (listener_ == null) {
      listener_ = new ArrayList(6);
    }
    listener_.add(_l);
  }

  public boolean containsListener(final H2dRubarDonneesBrutesListener _l) {
    if (_l == null) {
      return false;
    }
    return (listener_ != null) && listener_.contains(_l);
  }

  @Override
  public void donneesBrutesDataChanged(final H2dRubarDonneesBrutes _source) {

    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dRubarDonneesBrutesListener) listener_.get(i)).donneesBrutesDataChanged(_source);
      }
    }
    isModified_ = true;
  }

  @Override
  public void donneesBrutesDataNombreChanged(final H2dRubarDonneesBrutes _source) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dRubarDonneesBrutesListener) listener_.get(i)).donneesBrutesDataNombreChanged(_source);
      }
    }
    isModified_ = true;
  }

  public void donneesBrutesMngCreated(final H2dRubarDonneesBrutes _sourceCreated) {
  }

  @Override
  public void donneesBrutesSupportAddedOrRemoved(final H2dRubarDonneesBrutes _source) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dRubarDonneesBrutesListener) listener_.get(i)).donneesBrutesSupportAddedOrRemoved(_source);
      }
    }
    isModified_ = true;
  }

  @Override
  public void donneesBrutesSupportChanged(final H2dRubarDonneesBrutes _source) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dRubarDonneesBrutesListener) listener_.get(i)).donneesBrutesSupportChanged(_source);
      }
    }
    isModified_ = true;
  }

  @Override
  public void donneesBrutesTypeChanged(final H2dRubarDonneesBrutes _source) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dRubarDonneesBrutesListener) listener_.get(i)).donneesBrutesTypeChanged(_source);
      }
    }
    isModified_ = true;
  }

  /**
   * @param _id l'identifiant
   * @return null si pas encore definie.
   */
  public H2dRubarDonneesBrutes get(final String _id) {
    if (m_ != null) {
      return (H2dRubarDonneesBrutes) m_.get(_id);
    }
    return null;
  }

  public boolean isModified() {
    return isModified_;
  }

  public void removeListener(final H2dRubarDonneesBrutesListener _l) {
    if (_l == null) {
      return;
    }
    if (listener_ != null) {
      listener_.remove(_l);
    }
  }

  public void setModified(final boolean _isModified) {
    isModified_ = _isModified;
  }
}
