/*
 * @creation 27 avr. 07
 * @modification $Date: 2007-04-30 14:21:35 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dNodalPropertiesMngAbstract;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author fred deniger
 * @version $Id: H2DTelemacNodalPropertiesMngAbstract.java,v 1.1 2007-04-30 14:21:35 deniger Exp $
 */
public abstract class H2DTelemacNodalPropertiesMngAbstract extends H2dNodalPropertiesMngAbstract {

  public H2DTelemacNodalPropertiesMngAbstract(final EfGridInterface _nbNodes) {
    super(_nbNodes);
  }

  @Override
  public boolean canVariableBeenAdd(final H2dVariableType _t) {
    return _t != H2dVariableType.HAUTEUR_EAU && _t != H2dVariableType.COTE_EAU && _t != H2dVariableType.VITESSE_U
        && _t != H2dVariableType.VITESSE_V;
  }

  @Override
  public boolean canVariableBeenRemoved(final H2dVariableType _t) {
    return _t != H2dVariableType.HAUTEUR_EAU && _t != H2dVariableType.BATHYMETRIE && _t != H2dVariableType.COTE_EAU;
  }

  @Override
  public int getVariablesBlockLength() {
    return -1;
  }

}
