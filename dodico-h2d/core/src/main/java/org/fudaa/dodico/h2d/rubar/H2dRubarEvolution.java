/*
 * @creation 10 nov. 2004
 * @modification $Date: 2007-05-22 13:11:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import com.memoire.fu.FuLog;
import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluNumberFormater;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.CtuluValueValidator;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionListener;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarEvolution.java,v 1.11 2007-05-22 13:11:25 deniger Exp $
 */
public class H2dRubarEvolution extends EvolutionReguliere {

  /**
   * Utilise pour les elements vide.
   */
  public static final H2dRubarEvolution EMPTY_EVOL = new H2dRubarEvolution(CtuluResource.CTULU.getString("Vide")) {

    @Override
    public void setNom(String _string) {
    }
  };

  static {
    H2dRubarEvolution.EMPTY_EVOL.add(0, 0);
  }

  /**
   * @param _t la variable utilisee pour le temps.
   * @param _evol l'evolution de base
   * @param _l le listener ( en fait un renvoyeur d'evt)
   * @return l'evolution h2d correctement initialisee.
   */
  public static H2dRubarEvolution createH2dRubarEvolution(final H2dVariableType _t, final EvolutionReguliereInterface _evol,
                                                          final EvolutionListener _l) {
    if (_evol == null) {
      return null;
    }
    final H2dRubarEvolution r = new H2dRubarEvolution(_evol);
    r.setListener(_l);
    if (_l == null) {
      FuLog.error("Attention: evolution creee sans listener");
    }
    r.setFmt(H2dRubarTimeCondition.getTimeFmt(_t), H2dRubarTimeCondition.getFmt(_t), null);
    return r;
  }
  CtuluNumberFormater timeFmt_;
  CtuluNumberFormater yFmt_;

  public H2dRubarEvolution() {
    super();
  }

  /**
   * @param _evolReg
   */
  public H2dRubarEvolution(final EvolutionReguliereInterface _evolReg) {
    super(_evolReg);
  }

  /**
   * @param _evol
   */
  public H2dRubarEvolution(final H2dRubarEvolution _evol) {
    super(_evol);
    yFmt_ = _evol.yFmt_;
  }

  /**
   * @param _n
   */
  public H2dRubarEvolution(final int _n) {
    super(_n);
  }

  /**
   * @param _end
   * @param _v
   */
  public H2dRubarEvolution(final int _end, final double _v) {
    super(_end, _v);
  }

  /**
   * @param _nom
   */
  public H2dRubarEvolution(final String _nom) {
    super(_nom);
  }

  /**
   * @param _timeFmt le format a utiliser pour le temps
   */
  protected final void setTimeFmt(final CtuluNumberFormater _timeFmt) {
    timeFmt_ = _timeFmt;
  }

  @Override
  public EvolutionReguliere duplicate() {
    return new H2dRubarEvolution(this);
  }

  public CtuluNumberFormater getTimeFmt() {
    return timeFmt_ == null ? getDefaultTimeFmt() : timeFmt_;
  }

  public static CtuluNumberFormater getDefaultTimeFmt() {
    return H2dRubarNumberFormatter.TIME;
  }

  /**
   * @return le format a utiliser pour y
   */
  public final CtuluNumberFormater getYFmt() {
    return yFmt_;
  }

  /**
   * @param _yV le format a appliquer
   * @return null si ok
   */
  public int[] idxErrorForFmt(final CtuluValueValidator _yV, final CtuluValueValidator _xV) {
    final TIntArrayList l = new TIntArrayList();
    for (int i = getNbValues() - 1; i >= 0; i--) {
      if ((!_xV.isValueValid(getX(i))) || (_yV != null && !_yV.isValueValid(getY(i)))) {
        l.add(i);
      }
    }
    if (l.size() == 0) {
      return null;
    }
    l.sort();
    return l.toNativeArray();
  }

  public boolean isIdxErrorForFmt(final CtuluNumberFormater _xFmt, final CtuluNumberFormater _yFmt) {
    if (_yFmt == null) {
      return false;
    }
    if (_yFmt != yFmt_) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("rubar courbe valide courbe");
      }
      return isIdxErrorForFmt(_xFmt == null ? getTimeFmt().getValueValidator() : _xFmt.getValueValidator(),
                              _yFmt.getValueValidator());
    }
    return false;
  }

  /**
   * @param _xV le format a appliquer pour x
   * @param _yV le format a appliquer
   * @return true si probleme de format
   */
  public boolean isIdxErrorForFmt(final CtuluValueValidator _xV, final CtuluValueValidator _yV) {
    final CtuluValueValidator time = _xV == null ? getTimeFmt().getValueValidator() : _xV;
    if (_yV == null) {
      for (int i = getNbValues() - 1; i >= 0; i--) {
        if (!H2dRubarNumberFormatter.TIME.isValid(getX(i))) {
          return true;
        }
      }
    } else {
      for (int i = getNbValues() - 1; i >= 0; i--) {
        if (!time.isValueValid(getX(i)) || (!_yV.isValueValid(getY(i)))) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public boolean isValuesValid(final double _x, final double _y) {
    if (yFmt_ != null) {
      return getTimeFmt().isValid(_x) && yFmt_.isValid(_y);
    }
    return getTimeFmt().isValid(_x);
  }

  /**
   * @param _xFmt format pour x
   * @param _yFmt format a utiliser pour y
   * @param _cmd le receveur de commande.
   */
  public final void setFmt(final CtuluNumberFormater _xFmt, final CtuluNumberFormater _yFmt, final CtuluCommandContainer _cmd) {
    final CtuluNumberFormater time = _xFmt == null ? getDefaultTimeFmt() : _xFmt;
    if (yFmt_ != _yFmt || (time != getTimeFmt())) {
      yFmt_ = _yFmt;
      timeFmt_ = time;
      final int[] idxs = idxErrorForFmt(yFmt_ == null ? null : yFmt_.getValueValidator(), timeFmt_.getValueValidator());
      if (idxs != null) {
        CtuluLibMessage.info("indexs are invalid for " + getNom() + " nb " + idxs.length);
        remove(idxs, _cmd);
      }
    }
  }
}