/*
 * @creation 11 juin 2004
 *
 * @modification $Date: 2007-06-13 14:45:57 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.dico.DicoCasFileFormatVersionAbstract;
import org.fudaa.dodico.dico.DicoDataType;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoModelAbstract;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Fred Deniger
 */
public class H2dRubarDicoModel extends DicoModelAbstract {
  private static final String TEMPS_DEBUT_CALCUL_EN = "Computation start time";
  private static final String TEMPS_DEBUT_CALCUL_FR = "Temps d�but du calcul";
  private static final String VISCOSITY_EN = "Viscosity or turbulence coefficient";
  private static final String VISCOSITE_FR = "Coefficient de viscosit� ou de turbulence";
  public static final String OUVRAGES_HYDRAULIQUES_PLUIE_TURBULENCES = "Ouvrages hydrauliques / pluie / turbulences";
  public static final String HYDRAULIC_STRUCTURES_RAIN_TURBULENCE = "Hydraulic structures/rain/turbulence";

  public H2dRubarDicoModel() {
    super();
  }

  /**
   * @param _language
   */
  public H2dRubarDicoModel(final String _language) {
    super(_language);
  }

  public static final boolean workActivatedByValue(String value) {
    return
        "1".equals(value) || "3".equals(value)
            || "11".equals(value) || "13".equals(value)
            || "21".equals(value) || "23".equals(value)
            || "31".equals(value) || "33".equals(value)
        ;
  }

  public static final boolean rainActivatedByValue(String value) {
    return
        "2".equals(value) || "3".equals(value)
            || "12".equals(value) || "13".equals(value)
            || "22".equals(value) || "23".equals(value)
            || "32".equals(value) || "33".equals(value)
        ;
  }

  /**
   * @param _language
   */
  public H2dRubarDicoModel(final int _language) {
    super(_language);
  }

  @Override
  public String getVersion() {
    return "1.0";
  }

  public H2dRubarProjetType getProjectType() {
    return H2dRubarProjetType.COURANTOLOGIE_2D;
  }

  @Override
  protected String[] createNoms() {
    return null;
  }

  @Override
  protected DicoEntite[] createEntites() {
    final DicoEntite[] r = new DicoEntite[25];
    final DicoDataType.Binaire typeBinaire = DicoDataType.Binaire.EMPTY;
    int index = 0;
    final String[] t = new String[2];
    String[] rGeneral = new String[]{"G�n�ral", "General"};
    t[0] = "Reprise de calcul";
    t[1] = "Computation continued";
    DicoEntite.Simple ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(index);
    t[1] = getFalse();
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    t[0] = "Pr�cision sur les hauteurs";
    t[1] = "Precision for elevation";
    DicoDataType.Reel type = new DicoDataType.Reel();
    type.setControle(0);
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setIndex(index);
    ent.setNiveau(0);
    t[1] = "10-4";
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    t[0] = "Acc�l�ration de la pesanteur";
    t[1] = "Gravity acceleration";
    type = new DicoDataType.Reel();
    type.setControle(0);
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setIndex(index);
    ent.setNiveau(0);
    t[1] = "9.81";
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    // "Temps d�but du calcul"
    t[0] = TEMPS_DEBUT_CALCUL_FR;
    t[1] = TEMPS_DEBUT_CALCUL_EN;
    type = new DicoDataType.Reel();
    type.setControle(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
    // type.setControle(0); // type.setControle(0); demande A.Paquier juin 2007
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setIndex(index);
    ent.setNiveau(0);
    t[1] = "0.0";
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    // "Temps fin calcul"
    t[0] = "Temps fin calcul";
    t[1] = "Computation end time";
    type = new DicoDataType.Reel();
    type.setControle(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
    // type.setControle(0); demande A.Paquier juin 2007
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setIndex(index);
    ent.setNiveau(0);
    ent.isRequired();
    t[1] = "0.0";
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    // Valeur pas de temps initial
    t[0] = "Valeur pas de temps initial";
    t[1] = "Initial time step";
    type = new DicoDataType.Reel();
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setRequired(true);
    ent.setIndex(index);
    ent.setNiveau(0);
    t[1] = "0.01";
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    t[0] = "Calcul � nombre de Courant constant";
    t[1] = "Computation with constant \"Courant\" number";
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setIndex(index);
    ent.setNiveau(0);
    t[1] = getFalse();
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    t[0] = "Valeur maximale du nombre Courant";
    t[1] = "Maximum \"Courant\" number";
    type = new DicoDataType.Reel();
    type.setControle(0.0, 1.0);
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setIndex(index);
    ent.setNiveau(0);
    t[1] = "0.9";
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    t[0] = "Sch�ma calcul";
    t[1] = "Computation scheme";
    DicoDataType.Entier typeE = new DicoDataType.Entier();
    typeE.setChoice(new String[]{CtuluLibString.ZERO, CtuluLibString.UN, "2"}, new String[]{"G", "Van Leer ordre 1", "Van Leer ordre 2"});
    ent = new DicoEntite.Simple(t[languageIndex_], typeE);
    ent.setIndex(index);
    ent.setNiveau(0);
    t[1] = "2";
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    t[0] = "Conditions limites variables dans le temps";
    t[1] = "Boundary conditions depending on time";
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(index);
    t[1] = getFalse();
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    t[0] = "Correction de Van Leer";
    t[1] = "Van Leer correction";
    type = new DicoDataType.Reel();
    type.setControle(0.5, 1);
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setNiveau(0);
    ent.setIndex(index);
    t[1] = "0.6";
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    t[0] = OUVRAGES_HYDRAULIQUES_PLUIE_TURBULENCES;
    t[1] = HYDRAULIC_STRUCTURES_RAIN_TURBULENCE;
    // gestion des nouveaux ouvrages 6 novembre 2006
    typeE = new DicoDataType.Entier();
    //Attention si vous modifiez ces valeur, il faut modifier les methodes workActivatedByValue et rainActivatedByValue
    String[] chx = new String[]{CtuluLibString.ZERO, CtuluLibString.UN, "2", "3", "10", "11", "12", "13", "20", "21", "22", "23", "30", "31", "32", "33"};
    if (languageIndex_ == 0) {
      typeE.setChoice(chx, new String[]{
          "Pas d'ouvrage/Pas d'apport de pluie",//0
          "Ouvrage/Pas d'apport de pluie",//1
          "Pas d'ouvrage/Apport de pluie",//2
          "Ouvrage/Apport de pluie", //3
          "Turbulence fonction surface libre - Pas d'ouvrage/Pas d'apport de pluie",//10
          "Turbulence fonction surface libre - Ouvrage/Pas d'apport de pluie", //11
          "Turbulence fonction surface libre - Pas d'ouvrage/Apport de pluie",//12
          "Turbulence fonction surface libre - Ouvrage/Apport de pluie", //13
          "Turbulence fonction frottement - Pas d'ouvrage/Pas d'apport de pluie",//20
          "Turbulence fonction frottement - Ouvrage/Pas d'apport de pluie", //21
          "Turbulence fonction frottement - Pas d'ouvrage/Apport de pluie",//22
          "Turbulence fonction frottement - Ouvrage/Apport de pluie",//23
          "Porosit� - Pas d'ouvrage/Pas d'apport de pluie",//30
          "Porosit� - Ouvrage/Pas d'apport de pluie", //31
          "Porosit� - Pas d'ouvrage/Apport de pluie",//32
          "Porosit� - Ouvrage/Apport de pluie"//33

      });
    } else {
      typeE.setChoice(chx, new String[]{
          "No works/no rain inflow",//
          "Works/No rain inflow",
          "No works/Rain inflow",
          "Works/Rain inflow",
          "Turbulence function of free surface - No works/no rain inflow",
          "Turbulence function of free surface - Works/No rain inflow",
          "Turbulence function of free surface - No works/Rain inflow",
          "Turbulence function of free surface - Works/Rain inflow",
          "Turbulence function of friction - No works/no rain inflow",
          "Turbulence function of friction - Works/No rain inflow",
          "Turbulence function of friction - No works/Rain inflow",
          "Turbulence function of friction - Works/Rain inflow",
          "Porosity - No works/no rain inflow",//30
          "Porosity - Works/No rain inflow", //31
          "Porosity - No works/Rain inflow",//32
          "Porosity - Works/Rain inflow"//33
      });
    }
    ent = new DicoEntite.Simple(t[languageIndex_], typeE);
    ent.setNiveau(0);
    ent.setIndex(index);
    t[1] = CtuluLibString.ZERO;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    t[0] = "D�finition du frottement";
    t[1] = "Friction definition";
    typeE = new DicoDataType.Entier();
    chx = new String[]{CtuluLibString.ZERO, CtuluLibString.UN, CtuluLibString.DEUX};
    typeE.setChoice(chx, new String[]{"Chezy", "Strickler", "Darcy Weisbach"});
    ent = new DicoEntite.Simple(t[languageIndex_], typeE);
    ent.setNiveau(0);
    ent.setIndex(index);
    t[1] = CtuluLibString.UN;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    t[0] = "Pas de temps de la sauvegarde du champ vitesse de l'eau (TPS)";
    t[1] = "Time step for saving water velocity field (TPS)";
    type = new DicoDataType.Reel();
    type.setControle(0);
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setNiveau(0);
    ent.setIndex(index);
    ent.setRequired(true);
    t[1] = "3600";
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    rGeneral = new String[]{"Fichiers r�sultats", "Results file"};
    t[0] = "fichier limnigrammes (TRC)";
    t[1] = "File hydrograph (TRC)";
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(index);
    t[1] = getFalse();
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    t[0] = "Edition maillage (EDM)";
    t[1] = "Grid Edition (EDM)";
    final DicoDataType.Entier entier = new DicoDataType.Entier();
    entier.setControle(0, 7);
    ent = new DicoEntite.Simple(t[languageIndex_], entier);
    ent.setNiveau(0);
    ent.setIndex(index);
    t[1] = CtuluLibString.ZERO;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    t[0] = "Edition des conditions limites(ECL)";
    t[1] = "Boundary conditions edition (ECL)";
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(index);
    t[1] = getFalse();
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    t[0] = "Edition des conditions initiales (ECI)";
    t[1] = "Initial condition edition (ECI)";
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(index);
    t[1] = getFalse();
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    t[0] = "Edition des frottements de fond (EFR)";
    t[1] = "Bottom friction edition (EFR)";
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(index);
    t[1] = getFalse();
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    t[0] = "Valeurs aux ar�tes et sur les limites (OUT)";
    t[1] = "Flow at structures and boundaries (OUT)";
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(index);
    t[1] = getTrue();
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    t[0] = "Maxima des r�sultats depuis le d�but du calcul (ENV)";
    t[1] = "Maximal values from the computation beginning (ENV)";
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(index);
    t[1] = getTrue();
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    rGeneral = new String[]{"G�n�ral", "General"};
    type = new DicoDataType.Reel();
    type.setControle(-1);
    t[0] = VISCOSITE_FR;
    t[1] = VISCOSITY_EN;
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setNiveau(0);
    ent.setIndex(index);
    t[1] = "0.001";
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    type = new DicoDataType.Reel();
    type.setControle(0);
    t[0] = "Coefficient de frottement � la paroi";
    t[1] = "Friction coefficient for wall";
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setNiveau(0);
    ent.setIndex(index);
    t[1] = CtuluLibString.ZERO;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    type = new DicoDataType.Reel();
    type.setControle(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
    t[0] = "Vitesse du vent selon X";
    t[1] = "Wind velocity along X";
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setNiveau(0);
    ent.setIndex(index);
    t[1] = CtuluLibString.ZERO;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    type = new DicoDataType.Reel();
    type.setControle(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
    t[0] = "Vitesse du vent selon Y";
    t[1] = "Wind velocity along Y";
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setNiveau(0);
    ent.setIndex(index);
    t[1] = CtuluLibString.ZERO;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    ent.setRubrique(rGeneral[languageIndex_]);
    r[index++] = ent.getImmutable();

    return r;
  }

  private String getTrue() {
    return "true";
  }

  private String getFalse() {
    return "false";
  }

  @Override
  public String getCodeName() {
    return "rubar";
  }

  public static DicoEntite getDiffusionEntite(final DicoCasFileFormatVersionAbstract _vers) {
    return _vers.getEntiteFor(new String[]{VISCOSITE_FR, VISCOSITY_EN});
  }

  public static DicoEntite getInitalTime(final DicoCasFileFormatVersionAbstract _vers) {
    return _vers.getEntiteFor(new String[]{TEMPS_DEBUT_CALCUL_EN, TEMPS_DEBUT_CALCUL_FR});
  }

  public static DicoEntite getHauteurMini(final DicoCasFileFormatVersionAbstract _vers) {
    return _vers.getEntiteFor(new String[]{"Pr�cision sur les hauteurs", "Precision for elevation"});
  }

  public static DicoEntite getOuvrage(final DicoCasFileFormatVersionAbstract _vers) {
    return _vers.getEntiteFor(new String[]{OUVRAGES_HYDRAULIQUES_PLUIE_TURBULENCES, HYDRAULIC_STRUCTURES_RAIN_TURBULENCE});
  }

  public static String[] getKeys(int nbTransportBlock) {
    List<String> keys = new ArrayList<>();
    // correction mot-cl�s suite demande A.Paquier du 25 octobre 2006
    String[] keysNonTransport = new String[]{" REPRISE (0/1) 1=REPR             :", " PRECISION SUR LES HAUTEURS       :", " VALEUR DE LA GRAVITE             :",
        " TEMPS DEBUT DE CALCUL            :", " TEMPS FIN DE CALCUL              :", " VALEUR PAS DE TEMPS INITIAL      :",
        " CALCUL A CFL CONST OUI(1)/0      :", " VALEUR DE CFL                    :", " SCHEMA CALCUL G(0)/V1(1)/V2(2)   :",
        " CONDITIONS LIMITES F(T) OUI(1)/0 :", " CORRECTION VAN LEER              :", " OUVRAGES, APPORTS ...            :",
        " STRICKLER(1)/CHEZY(0)            :", " PAS DE TEMPS DE STOCKAGE         :", " EDITION LIMNIGRAMMES             :",
        " EDITION MAILLAGE  (0/1)          :", " EDITION COND LIM  (0/1)          :", " EDITION COND INIT  (0/1)         :",
        " EDITION FROTTEMENTS  (0/1)       :", " EDITION VALEURS LIMITES (0/1)    :", " EDITION ENVELOPPES  (0/1)        :",
        " CONSTANTE DE VISCOSITE           :", " FROTTEMENT A LA PAROI            :", " VITESSE DU VENT SUR OX           :",
        " VITESSE DU VENT SUR OY           :"};
    keys.addAll(Arrays.asList(keysNonTransport));
    int nbBlock = Math.max(nbTransportBlock, 1);
    int columnSize = 34;
    String[] transportKey = new String[]{" CONSTANTE DE VISCOSITE SUR OX", " CONSTANTE DE VISCOSITE SUR OY",
        " NOMBRE DE SCHMIDT", " MASSE VOLUMIQUE / DENSITE SED", " MASSE VOLUMIQUE / DENSITE EAU",
        " COEFFICIENT EROSION", " OPTION DE CALCUL", " D50 DES SEDIMENTS",
        " COEFFICIENT DE VITESSE DE DEPOT", " POROSITE DES SEDIMENTS EN PLACE", " D90 (EN M) OU CONTRAINTE CRITIQUE"};
    for (int idxBlock = 0; idxBlock < nbBlock; idxBlock++) {
      for (String s : transportKey) {
        String withIndex = idxBlock == 0 ? s : (s + CtuluLibString.getEspaceString(idxBlock + 1));
        withIndex = CtuluLibString.adjustSize(34, withIndex);
        keys.add(withIndex + ":");
      }
    }

    return keys.toArray(new String[keys.size()]);
  }
}
