/*
 * @creation 28 sept. 2004
 * @modification $Date: 2007-01-10 09:04:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import gnu.trove.TIntDoubleHashMap;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;

/**
 * @author Fred Deniger
 * @version $Id: H2dParallelogrammeData.java,v 1.17 2007-01-10 09:04:28 deniger Exp $
 */
public class H2dParallelogrammeData extends H2dParallelogrammeDataAbstract {
  class CommandSetXY implements CtuluCommand {
    int i_;
    double newX_;
    double newY_;
    double oldX_;
    double oldY_;

    /**
     * @param _i
     * @param _oldX
     * @param _oldY
     * @param _newX
     * @param _newY
     */
    public CommandSetXY(final int _i, final double _oldX, final double _oldY, final double _newX, final double _newY) {
      super();
      i_ = _i;
      oldX_ = _oldX;
      oldY_ = _oldY;
      newX_ = _newX;
      newY_ = _newY;
    }

    @Override
    public void redo() {
      setX(i_, newX_);
      setY(i_, newY_);
      fireXYChanged();
    }

    @Override
    public void undo() {
      setX(i_, oldX_);
      setY(i_, oldY_);
      fireXYChanged();
    }
  }

  protected CtuluCollectionDoubleEdit[] values_;

  /**
   * @param _interface l'interface permettant l'initialisation : NON NULL.
   */
  /*
   * public H2dParallelogrammeData(H2dParallelogrammeDataInterface _interface) { this(_interface,
   * _interface.getNbValues()); }
   */
  protected void setDataToDefault(final int _idx, final CtuluCommandContainer _cmd) {
    final CtuluCollectionDoubleEdit old = values_[_idx];
    // on demande de creer un modele a valeur unique
    values_[_idx] = createDataModel(_idx);
    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          setDataToDefault(_idx, null);
        }

        @Override
        public void undo() {
          values_[_idx] = old;
        }
      });
    }
  }

  TIntDoubleHashMap defaultValues_;

  public double getDefaultValues(final int _idx) {
    return defaultValues_ == null ? 0 : defaultValues_.get(_idx);
  }

  public void setDefaultValues(final int _idx, final double _val) {
    if (defaultValues_ == null) {
      defaultValues_ = new TIntDoubleHashMap(getNbValues());
    }
    defaultValues_.put(_idx, _val);
  }

  /**
   * initialise les valeurs mais garde le nombre de valeurs definies.
   *
   * @param _interface
   */
  public void initFromKeepingSameNumberOfValue(final H2dParallelogrammeDataInterface _interface) {
    if (_interface == null) {
      return;
    }
    x1_ = _interface.getX1();
    x2_ = _interface.getX2();
    x4_ = _interface.getX4();
    y1_ = _interface.getY1();
    y2_ = _interface.getY2();
    y4_ = _interface.getY4();
    if ((values_ != null) && (_interface.getNbValues() > 0)) {
      final int max = Math.min(_interface.getNbValues(), values_.length);
      for (int i = max - 1; i >= 0; i--) {
        if (_interface.getValues(i) != null) {
          values_[i] = createDataModel(i);
          values_[i].initWithDouble(_interface.getValues(i), false);
        }
      }
    }
  }

  /**
   * @param _nbValues le nombre de valeurs.
   */
  public H2dParallelogrammeData(final int _nbValues) {
    values_ = new CtuluCollectionDoubleEdit[_nbValues];
  }

  protected CtuluCollectionDoubleEdit createDataModel(final int _idx) {
    final double v = getDefaultValues(_idx);
    final CtuluArrayDouble r = new CtuluArrayDouble(4);
    if (v != 0) {
      r.setAll(v, null);
    }
    return r;
  }

  protected void fireDataStructureChanged() {

  }

  protected void fireXYChanged() {

  }

  protected void removeLastData(final int nbToRemove, final CtuluCommandContainer _cmd) {
    if (values_.length == 0) {
      return;
    }
    final CtuluCollectionDoubleEdit[] r = new CtuluCollectionDoubleEdit[nbToRemove];
    for (int i = 0; i < nbToRemove; i++) {
      r[i] = values_[values_.length - nbToRemove + i];
    }
    final CtuluArrayDouble[] newModel = new CtuluArrayDouble[values_.length - nbToRemove];
    System.arraycopy(values_, 0, newModel, 0, newModel.length);
    values_ = newModel;
    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          removeLastData(nbToRemove, null);
        }

        @Override
        public void undo() {
          addData(r, null);
        }
      });
    }
    fireDataStructureChanged();
  }

  protected void setNbValue(final int _i) {
    if (_i != values_.length) {
      final CtuluArrayDouble[] temp = new CtuluArrayDouble[_i];
      final int nb = Math.min(values_.length, _i);
      System.arraycopy(values_, 0, temp, 0, nb);
      values_ = temp;
    }
  }

  protected void setX(final int _i, final double _x) {
    if (_i == 0) {
      x1_ = _x;
    }
    if (_i == 1) {
      x2_ = _x;
    }
    if (_i == 3) {
      x4_ = _x;
    }
  }

  protected void setY(final int _i, final double _y) {
    if (_i == 0) {
      y1_ = _y;
    }
    if (_i == 1) {
      y2_ = _y;
    }
    if (_i == 3) {
      y4_ = _y;
    }
  }

  /**
   * Ajoute une ligne de valeur.
   *
   * @param _cmd le receveur de commandes
   */
  public final void addData(int nbData, final CtuluCommandContainer _cmd) {
    final CtuluCollectionDoubleEdit[] toAdd = new CtuluCollectionDoubleEdit[nbData];
    for (int i = 0; i < nbData; i++) {
      toAdd[i] = createDataModel(getNbValues() + i);
    }
    addData(toAdd, _cmd);
  }

  /**
   * Ajoute a la fin le modele passe en parametre. N'envoie pas d'evt et ne fait pas de copie de _m.
   *
   * @param toAdd le modele a ajouter
   * @param _cmd le receveur de commande
   */
  public final void addData(final CtuluCollectionDoubleEdit[] toAdd, final CtuluCommandContainer _cmd) {

    final CtuluCollectionDoubleEdit[] newModel = new CtuluCollectionDoubleEdit[values_.length + toAdd.length];
    System.arraycopy(values_, 0, newModel, 0, values_.length);
    for (int i = 0; i < toAdd.length; i++) {
      newModel[values_.length + i] = toAdd[i];
      if (toAdd[i].getSize() != 4) {
        throw new IllegalArgumentException();
      }
    }
    values_ = newModel;
    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          addData(toAdd, null);
        }

        @Override
        public void undo() {
          removeLastData(toAdd.length, null);
        }
      });
    }
    fireDataStructureChanged();
  }

  /**
   * A utiliser pour modifier une valeur. Si c'est pour consulter, il vaut mieux utiliser getValue (pas de creation
   * automatique).
   *
   * @param _idxVal l'indice de la valeur demandee
   * @return le model correspondant (lazy creation)
   */
  public CtuluCollectionDoubleEdit getDoubleData(final int _idxVal) {
    CtuluCollectionDoubleEdit m = values_[_idxVal];
    // lazy creation
    if (m == null) {
      m = createDataModel(_idxVal);
      values_[_idxVal] = m;
    }
    return m;
  }

  @Override
  public int getNbValues() {
    return values_ == null ? 0 : values_.length;
  }

  @Override
  public double getValue(final int _valueIdx, final int _x) {
    final CtuluCollectionDoubleEdit m = values_[_valueIdx];
    return m == null ? getDefaultValues(_valueIdx) : m.getValue(_x);
  }

  @Override
  public CtuluCollectionDoubleEdit getValues(final int _valueIdx) {
    return values_[_valueIdx];
  }

  @Override
  public boolean isValuesDefined() {
    return values_ != null;
  }

  /**
   * @param _i l'indice du point a modifier
   * @param _x la nouvelle valeur de x
   * @param _y la nouvelle valeur de y
   * @param _cmd le receveur de commande
   * @return true si modif.
   */
  public boolean set(final int _i, final double _x, final double _y, final CtuluCommandContainer _cmd) {
    if ((_i < 0) || (_i > 3) || (_i == 2)) {
      new Throwable().printStackTrace();
      return false;
    }
    final double oldx = getX(_i);
    final double oldy = getY(_i);
    if ((oldx != _x) || (oldy != _y)) {
      setX(_i, _x);
      setY(_i, _y);
      if (_cmd != null) {
        _cmd.addCmd(new CommandSetXY(_i, oldx, oldy, _x, _y));
      }
      fireXYChanged();
    }
    return false;
  }
}
