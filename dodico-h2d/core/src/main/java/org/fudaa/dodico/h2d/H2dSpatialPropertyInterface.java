/*
 * @creation 22 sept. 2004
 * @modification $Date: 2007-06-11 13:06:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import java.util.List;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;

/**
 * Classes pour des propri�t�s d�finies sur des points ind�pendants. Non d�finis par des indexs de points.
 * 
 * @author Fred Deniger
 * @version $Id: H2dSpatialPropertyInterface.java,v 1.7 2007-06-11 13:06:08 deniger Exp $
 */
public interface H2dSpatialPropertyInterface {

  /**
   * @return le nombre de points total
   */
  int getNbPoint();

  /**
   * @return le nombre de valeurs
   */
  int getNbValues();

  /**
   * @return la liste des identifiant. Liste non modifiable
   */
  List getValuesId();

  /**
   * @param _i l'indice [0;nbPoint[
   * @return le x du point i
   */
  double getX(int _i);

  /**
   * @param _i l'indice [0;nbPoint[
   * @return le y du point i
   */
  double getY(int _i);

  /**
   * @param _id l'identifiant
   * @param _i l'indice [0;nbPoint[
   * @return la valeur au point i
   */
  double getValue(CtuluVariable _id, int _i);

  /**
   * @param _id l'identifiant de l'objet
   * @return la liste des valeurs (non modifiable!)
   */
  CtuluCollectionDouble getValuesFor(CtuluVariable _id);

  /**
   * @return true si les donn�es sont definis � partir d'une grille reguliere
   */
  boolean isDefinedOnGrilleReguliere();

  /**
   * @param _r l'intervalle a modifier avec le min,max en X
   */
  void getXRange(CtuluRange _r);

  /**
   * @param _r l'intervalle a modifier avec le min,max en Y
   */
  void getYRange(CtuluRange _r);

  /**
   * @param _r l'intervalle a modifier avec le min,max de la valeur
   */
  void getValueRange(CtuluRange _r);

}