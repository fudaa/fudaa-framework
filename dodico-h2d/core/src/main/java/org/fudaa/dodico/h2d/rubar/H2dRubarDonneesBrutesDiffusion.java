/*
 * @creation 1 oct. 2004
 * @modification $Date: 2006-11-20 14:23:55 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarDonneesBrutesDiffusion.java,v 1.1 2006-11-20 14:23:55 deniger Exp $
 */
public class H2dRubarDonneesBrutesDiffusion extends H2dRubarDonneesBrutes {

  public H2dRubarDonneesBrutesDiffusion(final H2dRubarNumberFormatter _fmt) {
    super(1, _fmt);
  }

  @Override
  public String getID() {
    return H2dRubarDonneesBrutesMng.DIF_ID;
  }

  @Override
  public String getTitle() {
    return H2dResource.getS("diffusion");
  }

  @Override
  public Object getVariableId(final int _idx) {
    return H2dVariableType.COEF_DIFFUSION;
  }

  @Override
  public final boolean isVariableActive(final int _idx) {
    return true;
  }
}