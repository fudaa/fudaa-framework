/*
 * @creation 14 juin 2004
 * @modification $Date: 2007-04-30 14:21:34 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import com.memoire.bu.BuValueValidator;
import java.util.Map;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarElementProperty.java,v 1.8 2007-04-30 14:21:34 deniger Exp $
 */
public class H2dRubarElementProperty extends H2dRubarElementPropertyAbstract {

  final H2dRubarNumberFormatter fmt_;

  /**
   * @param _m une table H2DVariableType->double[]
   */
  public H2dRubarElementProperty(final EfGridInterface _grid, final H2dRubarNumberFormatter _fmt, final Map _m) {
    super(_grid, _m);
    fmt_ = _fmt;
  }

  @Override
  public BuValueValidator getValidator(final H2dVariableType _t) {
    return fmt_.getFormatterFor(_t).getValueValidator();
  }

  @Override
  public CtuluNumberFormatI getFormater(final H2dVariableType _t) {
    return fmt_.getFormatterFor(_t).getNumberFormat();
  }

}