/*
 * @creation 3 nov. 2003
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import java.util.Iterator;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author deniger
 * @version $Id: H2dTelemacTransient.java,v 1.14 2006-09-19 14:43:25 deniger Exp $
 */
public final class H2dTelemacTransient {

  public static final String OLD_TRACER_ID = "T";
  /**
   * Liste des variables pouvant avoir un comportement transitoire.
   */
  public final static CtuluPermanentList TRANSIENT_VARIABLES = new CtuluPermanentList(CtuluLibArray
      .sort(new H2dVariableType[] { H2dVariableType.DEBIT, H2dVariableType.COTE_EAU, H2dVariableTransType.TRACEUR,
          H2dVariableType.VITESSE_U, H2dVariableType.VITESSE_V }));

  private H2dTelemacTransient() {
    super();
  }

  /**
   * @param _telemacId l'identifiant utilise dans le fichier des frontieres liquides
   * @return la variables correspondante ou null si aucune.
   */
  public static H2dVariableType getVariableForTelemacId(final String _telemacId) {
    if(OLD_TRACER_ID.equals(_telemacId)){
      return H2dVariableTransType.TRACEUR;
    }
    for (final Iterator it = TRANSIENT_VARIABLES.iterator(); it.hasNext();) {
      final H2dVariableType v = (H2dVariableType) it.next();
      if (_telemacId.equals(v.getTelemacID())) {
        return v;
      }
    }
    return null;
  }
}