/*
 * @creation 21 ao�t 2003
 * @modification $Date: 2007-04-30 14:21:37 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dRefluxBoundaryType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

/**
 * Une classe pour definir un bord pour les fichiers e-s de reflux (inp). Classe mal foutue ( contient des donn�es de
 * bord avec frottement pour tous les types de bords). De plus, les tableaux d'indices ne sont pas proteg�s: a utiliser
 * avec attention.
 * 
 * @author deniger
 * @version $Id: H2dRefluxBordIndexGeneral.java,v 1.21 2007-04-30 14:21:37 deniger Exp $
 */
public class H2dRefluxBordIndexGeneral {

  /**
   * Renvoie un bord contenant l'indice _i1 en partant de la fin du tableau.
   * 
   * @param _i1 l'indice a checher
   * @param _bords les bords a parcourir
   * @return null si aucun bord.
   */
  public static H2dRefluxBordIndexGeneral findBordWithIndex(final int _i1, final H2dRefluxBordIndexGeneral[] _bords) {
    final int n = _bords.length - 1;
    for (int i = n; i >= 0; i--) {
      final H2dRefluxBordIndexGeneral general = _bords[i];
      if (general.containsIndex(_i1)) {
        return general;
      }
    }
    return null;
  }

  /**
   * Renvoie un bord contenant les indices _i1 et _i2 en partant de la fin du tableau.
   * 
   * @param _i1 un indice a chercher
   * @param _i2 l'autre
   * @param _bords les bords a parcourir
   * @return le bord trouve ou null si aucun.
   */
  public static H2dRefluxBordIndexGeneral findBordWithIndex(final int _i1, final int _i2,
      final H2dRefluxBordIndexGeneral[] _bords) {
    final int n = _bords.length - 1;
    for (int i = n; i >= 0; i--) {
      final H2dRefluxBordIndexGeneral general = _bords[i];
      if (general.containsIndex(_i1) && general.containsIndex(_i2)) {
        return general;
      }
    }
    return null;
  }

  /**
   * @param _bds les bords Reflux
   * @return true si contient des sollicitations.
   */
  public static boolean isSollicitation(final H2dRefluxBordIndexGeneral[] _bds) {
    if (!CtuluLibArray.isEmpty(_bds)) {
      for (int i = 0; i < _bds.length; i++) {
        final H2dRefluxBordIndexGeneral general = _bds[i];
        if (general != null && general.getBordType() == H2dRefluxBoundaryType.LIQUIDE_DEBIT_IMPOSE) {
          return true;
        }

      }
    }
    return false;
  }

  private int[] index_;

  H2dBoundaryType bord_;
  double rugosite_;
  EvolutionReguliereAbstract rugositeTransitoireCourbe_;
  H2dBcType rugositeType_;

  /**
   *
   */
  public H2dRefluxBordIndexGeneral() {
    bord_ = H2dRefluxBoundaryType.SOLIDE;
  }

  /**
   * @param _b le type. Si null, solide sera utilise
   */
  public H2dRefluxBordIndexGeneral(final H2dBoundaryType _b) {
    bord_ = (_b == null ? H2dRefluxBoundaryType.SOLIDE : _b);
  }

  /**
   * @param _i l'indice a tester
   * @return true si contient l'indice _i
   */
  public boolean containsIndex(final int _i) {
    final int n = index_.length - 1;
    for (int i = n; i >= 0; i--) {
      if (index_[i] == _i) {
        return true;
      }
    }
    return false;
  }

  /**
   * @return le type
   */
  public H2dBoundaryType getBordType() {
    return bord_;
  }

  /**
   * @return le tableau des indices
   */
  public int[] getIndex() {
    return index_;
  }

  /**
   * @return la rugosite
   */
  public double getRugosite() {
    return rugosite_;
  }

  /**
   * @return l'evolution utilise pour la rugosite ou null si aucune.
   */
  public EvolutionReguliereAbstract getRugositeTransitoireCourbe() {
    return rugositeTransitoireCourbe_;
  }

  /**
   * @return le type pour la rugosite
   */
  public H2dBcType getRugositeType() {
    return rugositeType_;
  }

  /**
   * Met a jour les donnees correspondantes au bord solide avec frottement si necessaire.
   * 
   * @param _type le nouveau type
   */
  public void setBord(final H2dBoundaryType _type) {
    bord_ = _type;
    if (bord_ != H2dRefluxBoundaryType.SOLIDE_FROTTEMENT) {
      rugositeTransitoireCourbe_ = null;
      rugositeType_ = null;
    }
  }

  /**
   * @param _is les nouveaux indices
   */
  public void setIndex(final int[] _is) {
    index_ = _is;
  }

  /**
   * Ne met a jour que si le bord est de type SOLIDE_FROTTEMENT et que la condition est H2dClType.PERMANENT.
   * 
   * @param _d la nouvelle valeur pour la rugosite
   */
  public void setRugosite(final double _d) {
    if ((bord_ == H2dRefluxBoundaryType.SOLIDE_FROTTEMENT) && (rugositeType_ == H2dBcType.PERMANENT)) {
      rugosite_ = _d;
    }
  }

  /**
   * Ne met a jour que si le bord est de type SOLIDE_FROTTEMENT.
   * 
   * @param _evolution la nouvelle evolution
   */
  public void setRugositeTransitoireCourbe(final EvolutionReguliereAbstract _evolution) {
    if (bord_ == H2dRefluxBoundaryType.SOLIDE_FROTTEMENT) {
      rugositeTransitoireCourbe_ = _evolution;
    }
  }

  /**
   * Affecte la valeur type_ et met a jour les autres donnees (valeur rugosite, evolution transitoire...).
   * 
   * @param _type le nouveau comportement pour la rugosite
   */
  public void setRugositeType(final H2dBcType _type) {
    if (bord_ == H2dRefluxBoundaryType.SOLIDE_FROTTEMENT) {
      rugositeType_ = _type;
      if (rugositeType_ == H2dBcType.LIBRE) {
        rugosite_ = 0d;
        rugositeTransitoireCourbe_ = null;
      } else if (rugositeType_ == H2dBcType.PERMANENT) {
        rugositeTransitoireCourbe_ = null;
      } else if (rugositeType_ == H2dBcType.TRANSITOIRE) {
        rugosite_ = 0;
      }
    }
  }

  @Override
  public String toString() {
    String r = "boundary " + bord_ + CtuluLibString.ESPACE + CtuluLibString.getObjectInString(index_, true);
    if (bord_ == H2dRefluxBoundaryType.SOLIDE_FROTTEMENT) {
      r += CtuluLibString.ESPACE + "friction ( " + rugositeType_ + ", " + rugosite_ + ", " + rugositeTransitoireCourbe_
          + ")";
    }
    return r;
  }
}