/*
 *  @creation     22 d�c. 2004
 *  @modification $Date: 2006-04-07 09:23:04 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.dodico.h2d.H2dEvolutionVariableMap;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.util.Set;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarOuvrageElementaireInterface.java,v 1.2 2006-04-07 09:23:04 deniger Exp $
 */
public interface H2dRubarOuvrageElementaireInterface {
  /**
   * @return le type de l'ouvrage
   */
  H2dRubarOuvrageType getType();

  /**
   * @return une copie de l'ouvrage elementaire sans evt
   */
  H2dRubarOuvrageElementaireInterface getCopyWithoutEvt();

  void setProjetType(final H2dRubarProjetType projet, final int nbTransportBlock, final CtuluCommandComposite cmd);

  /**
   * @param prefix
   * @param allTarage will containt all tarage curves
   */
  void fillWithTarageEvolution(String prefix, Set allTarage);

  void fillWithTransientCurves(String pref, H2dEvolutionVariableMap r);

  void evolutionChanged(EvolutionReguliereInterface _e);

  boolean containsEvolution(EvolutionReguliereInterface tarage);
}
