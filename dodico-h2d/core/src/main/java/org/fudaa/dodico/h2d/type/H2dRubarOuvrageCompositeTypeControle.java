package org.fudaa.dodico.h2d.type;

import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author genesis
 *
 */
public enum H2dRubarOuvrageCompositeTypeControle {

  ELEVATION("Z",H2dResource.getS("Niveau")),
  TIME("T",H2dResource.getS("Temps"));

  private final String desc;
  private final String rubarId;


  private H2dRubarOuvrageCompositeTypeControle(String rubarId, String desc) {
    this.rubarId = rubarId;
    this.desc = desc;
  }


  public String getDesc() {
    return desc;
  }


  public String getRubarId() {
    return rubarId;
  }

}
