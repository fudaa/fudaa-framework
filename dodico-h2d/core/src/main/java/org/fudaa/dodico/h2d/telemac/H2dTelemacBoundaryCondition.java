/*
 * @creation 25 juin 2003
 * @modification $Date: 2007-05-22 13:11:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.dodico.h2d.H2dBoundaryCondition;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryTypeCommon;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author deniger
 * @version $Id: H2dTelemacBoundaryCondition.java,v 1.15 2007-05-22 13:11:25 deniger Exp $
 */
public class H2dTelemacBoundaryCondition extends H2dBoundaryCondition {

  public final static int SOLID_UV = 0;
  public final static int SOLID_U_ZERO = 1;
  public final static int SOLID_V_ZERO = 2;

  public static final int getSolidIdxMixte() {
    return -1;
  }

  public static final boolean isSolidIdxMixte(final int _idx) {
    return _idx < 0 || _idx > 2;
  }

  public static final String getSolidUVDesc(final int _idx) {
    if (_idx == SOLID_U_ZERO) {
      return H2dResource.getS("Vitesse u nulle");
    }
    if (_idx == SOLID_V_ZERO) {
      return H2dResource.getS("Vitesse v nulle");
    }
    return H2dResource.getS("Vitesses libres");

  }

  public static String[] getSolidUVDesc() {
    return new String[] { getSolidUVDesc(SOLID_UV), getSolidUVDesc(SOLID_U_ZERO), getSolidUVDesc(SOLID_V_ZERO) };
  }

  /**
   * Memento pour cl.
   * 
   * @author Fred Deniger
   * @version $Id: H2dTelemacBoundaryCondition.java,v 1.15 2007-05-22 13:11:25 deniger Exp $
   */
  public static class TelemacCLMemento {

    double frottementParoi_;
    int soliduv_;
    double h_;
    double traceur_;
    double traceurCoefA_;
    double traceurCoefB_;
    double u_;
    double v_;

    @Override
    public String toString() {
      return "u=" + u_ + " ,v=" + v_ + ",h=" + h_ + ", t=" + traceur_ + ", ta=" + traceurCoefA_ + ", tb="
          + frottementParoi_;
    }
  }

  double frottementParoi_;
  double traceur_;
  double traceurCoefA_;
  double traceurCoefB_;
  int solidUVType_;

  public H2dTelemacBoundaryCondition() {
    super();
  }

  public int getSolidUVState() {
    return solidUVType_;
  }

  protected TelemacCLMemento createMemento() {
    final TelemacCLMemento r = new TelemacCLMemento();
    r.u_ = u_;
    r.v_ = v_;
    r.h_ = hOrSl_;
    r.soliduv_ = solidUVType_;
    r.traceur_ = traceur_;
    r.traceurCoefA_ = traceurCoefA_;
    r.traceurCoefB_ = traceurCoefB_;
    r.frottementParoi_ = frottementParoi_;
    return r;
  }

  @Override
  public double getConcentration() {
    return getTracer();
  }

  @Override
  public double getEvolSedimValue() {
    return getTracer();
  }

  @Override
  public double getFriction() {
    return frottementParoi_;
  }

  @Override
  public double getTracer() {
    return traceur_;
  }

  @Override
  public double getTracerCoeffA() {
    return traceurCoefA_;
  }

  @Override
  public double getTracerCoeffB() {
    return traceurCoefB_;
  }

  protected void initFrom(final H2dTelemacCLElementSource _s) {
    u_ = _s.u_;
    v_ = _s.v_;
    hOrSl_ = _s.h_;
    frottementParoi_ = _s.friction_;
    traceur_ = _s.tracer_;
    traceurCoefA_ = _s.tracerCoefA_;
    traceurCoefB_ = _s.tracerCoefB_;
    indexPt_ = _s.ptGlobalIdx_;
    solidUVType_ = SOLID_UV;
    if (_s.bordType_ == H2dBoundaryTypeCommon.SOLIDE) {
      if (_s.uType_ == H2dBcType.PERMANENT && Math.abs(_s.u_) < 1E-15) {
        solidUVType_ = SOLID_U_ZERO;
      } else if (_s.vType_ == H2dBcType.PERMANENT && Math.abs(_s.v_) < 1E-15) {
        solidUVType_ = SOLID_V_ZERO;
      }

    }
  }

  protected void initFromMemento(final TelemacCLMemento _m) {
    if (_m != null) {
      u_ = _m.u_;
      v_ = _m.v_;
      hOrSl_ = _m.h_;
      solidUVType_ = _m.soliduv_;
      traceur_ = _m.traceur_;
      traceurCoefA_ = _m.traceurCoefA_;
      traceurCoefB_ = _m.traceurCoefB_;
      frottementParoi_ = _m.frottementParoi_;
    }
  }

  protected boolean setFriction(final double _d) {
    if (_d == frottementParoi_) {
      return false;
    }
    frottementParoi_ = _d;
    return true;
  }

  protected boolean setH(final double _d) {
    if (_d == hOrSl_) {
      return false;
    }
    hOrSl_ = _d;
    return true;
  }

  protected void setIndexPt(final int _i) {
    indexPt_ = _i;
  }

  protected boolean setT(final double _d) {
    if (_d == traceur_) {
      return false;
    }
    traceur_ = _d;
    return true;
  }

  protected boolean setUvType(final int _type) {
    if (solidUVType_ != _type && !isSolidIdxMixte(_type)) {
      solidUVType_ = _type;
      if (solidUVType_ == SOLID_U_ZERO) {
        u_ = 0;
      } else if (solidUVType_ == SOLID_V_ZERO) {
        v_ = 0;
      }
      return true;
    }
    return false;
  }

  protected boolean setTraceurCoefA(final double _d) {
    if (_d == traceurCoefA_) {
      return false;
    }
    traceurCoefA_ = _d;
    return true;
  }

  protected boolean setTraceurCoefB(final double _d) {
    if (_d == traceurCoefB_) {
      return false;
    }
    traceurCoefB_ = _d;
    return true;
  }

  protected boolean setU(final double _d) {
    if (_d == u_) {
      return false;
    }
    u_ = _d;
    return true;
  }

  protected boolean setV(final double _d) {
    if (_d == v_) {
      return false;
    }
    v_ = _d;
    return true;
  }

  /**
   * Set the value for the variable _variable and return true if change (or if the variable type is not supported).
   * 
   * @return true if the change is done.
   */
  protected boolean setValue(final H2dVariableType _variable, final double _d) {
    if (_variable == H2dVariableType.HAUTEUR_EAU) {
      return setH(_d);
    } else if (_variable == H2dVariableType.VITESSE_U) {
      return setU(_d);
    } else if (_variable == H2dVariableType.VITESSE_V) {
      return setV(_d);
    } else if ((_variable == H2dVariableTransType.TRACEUR) || (_variable == H2dVariableTransType.CONCENTRATION)
        || (_variable == H2dVariableTransType.EVOLUTION_SEDIMENTAIRE)) {
      return setT(_d);
    } else if (_variable == H2dVariableTransType.TRACEUR_COEF_A) {
      return setTraceurCoefA(_d);
    } else if (_variable == H2dVariableTransType.TRACEUR_COEF_B) {
      return setTraceurCoefB(_d);
    } else if (_variable == H2dVariableType.COEF_FROTTEMENT) {
      return setFriction(_d);
    }
    new Throwable().printStackTrace();
    return false;
  }
}