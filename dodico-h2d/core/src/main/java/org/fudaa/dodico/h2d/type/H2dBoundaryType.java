/*
 * @creation 26 juin 2003
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.commun.DodicoEnumType;

/**
 * @author deniger
 * @version $Id: H2dBoundaryType.java,v 1.19 2006-09-19 14:43:25 deniger Exp $
 */
public abstract class H2dBoundaryType extends DodicoEnumType {

  private boolean isLiquide_;

  /**
   * Cree un bord liquide sans nom.
   */
  public H2dBoundaryType() {
    this(CtuluLibString.EMPTY_STRING, false);
  }

  /**
   * Cree un bord liquide.
   *
   * @param _nom le nom du bord
   */
  public H2dBoundaryType(final String _nom) {
    this(_nom, false);
  }

  /**
   * @param _nom le nom du bord
   * @param _isSolide true si solide
   */
  public H2dBoundaryType(final String _nom, boolean _isSolide) {
    super(_nom);
    isLiquide_ = !_isSolide;
  }


  /**
   * @return true si ce type est liquide
   */
  public boolean isLiquide() {
    return isLiquide_;
  }

  /**
   * @return true si ce type est solide
   */
  public boolean isSolide() {
    return !isLiquide_;
  }

}