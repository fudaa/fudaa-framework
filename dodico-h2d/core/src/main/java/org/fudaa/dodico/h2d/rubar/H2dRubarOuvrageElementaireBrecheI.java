/*
 * @creation     3 janv. 2005
 * @modification $Date: 2006-04-11 08:06:16 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarOuvrageElementaireBrecheI.java,v 1.3 2006-04-11 08:06:16 deniger Exp $
 */
public interface H2dRubarOuvrageElementaireBrecheI extends H2dRubarOuvrageElementaireInterface {
  /**
   * Le nombre de valeurs gerees par l'ouvrage elementaire breche.
   */
  int NB_VALUES = 14;

  /**
   * @return 0 si renard -1 si surverse
   */
  int getBrecheType();
  /**
   * @return le temps de d�but de br�che ou un valeur n�gative si le temps de d�but de br�che vaut le temps de d�but de simulation
   */
  double getDebutTemps();
  /**
   * @return la cote de cr�te.
   */
  double getCoteCrete();

  /**
   * @return oefficient d'�rosion lin�aire
   */
  double getCoefficientErosionLineaire();
  /**
   * @return la cote de pied.
   */
  double getCotePied();
  /**
   * @return la largeur de cr�te.
   */
  double getLargeurCrete();
  /**
   * @return la largeur de pied.
   */
  double getLargeurPied();
  /**
   * @return le diam�tre m�dian D50 (en millim�tres) de la courbe granulom�trique.
   */
  double getDiametreMedian();
  /**
   * @return le coefficient de Strickler de l'�coulement hydraulique sur et dans la digue
   */
  double getCoeffStrickler();
  /**
   * @return la masse volumique des grains (en kg/m3).
   */
  double getMasseVolumiqueGrain();
  /**
   * @return la porosit� (rapport du vide sur le volume total).
   */
  double getPorosite();
  /**
   * @return la cote du fond de la br�che ou du renard � l'instant initial.
   */
  double getCoteFond();
  /**
   * @return la dimension initiale de la br�che en millim�tres.
   */
  double getDimensionInitiale();
  /**
   * @return le pas de temps de stockage de l��volution de la digue (fichier RES).
   */
  double getPasDeTemps();
  /**
   * @return le coefficient de perte de charge singuli�re dans la retenue � l'amont du renard ou de la br�che (valeur comprise entre 0 et 0,5).
   */
  double getCoeffPerteDeCharge();
  /**
   * @return un indicateur de valeur "0" pour un renard, "-1" pour une surverse.
   */
  int getRenardSurverseId();
  /**
   * @return le nombre de pas de temps maximal (limit� selon annexe 3) pour le stockage de l��rosion de la digue.
   */
  int getNbPasDeTempsMax();
  /**
   * @return les valeurs dans l'ordre du fichier
   */
  // double[] getValues();
  /**
   * @param _idx l'indice de la valuer
   * @return la valeur demande [0;12[
   */
  double getValue(int _idx);
}
