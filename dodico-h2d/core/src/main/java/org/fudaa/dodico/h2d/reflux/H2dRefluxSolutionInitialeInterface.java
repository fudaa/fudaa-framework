/*
 * @creation 12 mars 2004
 * @modification $Date: 2007-04-30 14:21:37 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.h2d.H2dSiSourceInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $id$
 */
public interface H2dRefluxSolutionInitialeInterface extends H2dSiSourceInterface {

  /**
   * @param _t la variable a tester
   * @return true si des solutions initiales existent pour cette variable
   */
  boolean isValideVariable(H2dVariableType _t);

  /**
   * @param _d la cote globale
   * @param _prog la barre de pogression
   * @param _cmd le receveur de commande
   */
  void initFromCoteEau(double _d, ProgressionInterface _prog, CtuluCommandContainer _cmd);

  /**
   * @return l'adapter pour sauver les solutions initiales
   */
  Object createWriterInterface();

  /**
   * @param _interface les nouvelles solutions initiales
   * @param _a l'analyse de l'operation
   * @return la commande si modif. Null sinon
   */
  CtuluCommand initFromSI(H2dRefluxSolutionInitAdapterInterface _interface, CtuluAnalyze _a);

}