/*
 * @creation 14 d�c. 2004
 * @modification $Date: 2006-04-19 13:19:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.mesure.EvolutionReguliere;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarAppInterface.java,v 1.4 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dRubarAppInterface {

  /**
   * @return le nombre d'element d�fini par cette resultat
   */
  int getNbElt();

  /**
   * @return le nombre d'element d�fini par cette resultat
   */
  int getNbEvol();

  /**
   * @param _i l'indice [0;getNbEvol()[
   * @return l'evolution correspondante.
   */
  EvolutionReguliere getEvol(int _i);

  /**
   * @param _idxElt l'indice [0;getNbElt()[
   * @return l'evol correspondante
   */
  EvolutionReguliere getEvolForElt(int _idxElt);

  /**
   * @param _eltIdx l'element demande
   * @return l'indice de la courbe correspondante
   */
  int getEvolIdx(int _eltIdx);
}