package org.fudaa.dodico.h2d.rubar;

/**
 * @author CANEL Christophe (Genesis)
 */
public interface H2dRubarOuvrageElementaireDeversoirHydrauliqueI extends H2dRubarOuvrageElementaireInterface {

  /**
   * Le nombre de valeurs gerees par l'ouvrage elementaire d�versoir.
   */
  int NB_VALUES = 4;

  /**
   * @return le coeffecient de d�bit COEF
   */
  double getCoefficientDebit();

  /**
   * @return la cote de mise en charge ZORI
   */
  double getCoteMisEnchargeZm();

  /**
   * @return cote de seuil ZDEV
   */
  double getCoteSeuilZd();

  /**
   * @return longueur de d�versement LONG
   */
  double getLongDeversement();
}