/*
 *  @creation     6 avr. 2005
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

/**
 * @author Fred Deniger
 * @version $Id: H2DSiProfilListener.java,v 1.4 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2DSiProfilListener {

  void siProfilGeometryChanged();

  void siProfilDataChanged();

}
