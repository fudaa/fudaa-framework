package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.h2d.type.H2dRubarBoundaryType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

public class H2dRubarTarageContent {

  private EvolutionReguliereAbstract evolution;

  private H2dRubarBoundaryType tarageType;

  public H2dRubarTarageContent(EvolutionReguliereAbstract evolution, H2dRubarBoundaryType tarageType) {
    super();
    this.evolution = evolution;
    this.tarageType = tarageType;
  }

  public H2dRubarTarageContent() {
    super();
  }

  public boolean isSame(H2dRubarTarageContent other) {
    if (other == null) {
      return false;
    }
    return evolution == other.evolution && tarageType == other.tarageType;
  }

  public boolean isSame(EvolutionReguliereAbstract evol, H2dRubarBoundaryType type) {
    return evolution == evol && tarageType == type;
  }

  public EvolutionReguliereAbstract getEvolution() {
    return evolution;
  }

  public H2dRubarBoundaryType getTarageType() {
    return tarageType;
  }

  public void setEvolution(EvolutionReguliereAbstract evolution) {
    this.evolution = evolution;
  }

  public void setTarageType(H2dRubarBoundaryType tarageType) {
    this.tarageType = tarageType;
  }

}
