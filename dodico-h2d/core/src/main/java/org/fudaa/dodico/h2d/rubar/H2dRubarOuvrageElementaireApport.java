/*
 * @creation 3 janv. 2005
 * @modification $Date: 2007-03-15 16:59:40 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.dodico.h2d.H2dEvolutionVariableMap;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarOuvrageElementaireApport.java,v 1.10 2007-03-15 16:59:40 deniger Exp $
 */
public class H2dRubarOuvrageElementaireApport extends H2dRubarOuvrageElementaireAbstract implements
    H2dRubarOuvrageElementaireApportI {
  Set<EvolutionReguliere> allEvols;
  Map<H2dVariableType, EvolutionReguliere> evols_;

  protected H2dRubarOuvrageElementaireApport(final LinkedHashMap<H2dVariableType, ? extends EvolutionReguliereAbstract> evols) {
    setEvols(evols);
  }

  protected H2dRubarOuvrageElementaireApport(final H2dRubarOuvrageElementaireApport _a, final boolean _ouvrageParent) {
    initFrom(_a, _ouvrageParent);
  }

  /**
   * Constructeur dangereux rien d'initaliser.
   */
  public H2dRubarOuvrageElementaireApport(H2dRubarProjetType projet, int nbTransportsBlocks) {
    LinkedHashMap<H2dVariableType, EvolutionReguliere> defaultMap = new LinkedHashMap<>();

    if (H2dRubarProjetType.TRANSPORT.equals(projet)) {
      List<H2dVariableType> qVariables = getTransportVariables(nbTransportsBlocks);
      for (H2dVariableType var : qVariables) {
        defaultMap.put(var, H2dRubarEvolution.EMPTY_EVOL);
      }
    } else {
      defaultMap.put(getVariable(), H2dRubarEvolution.EMPTY_EVOL);
    }

    setEvols(defaultMap);
  }

  public List<H2dVariableType> getVariables() {
    return new ArrayList<>(evols_.keySet());
  }

  protected List<H2dVariableType> getTransportVariables(int nbBlock) {
    List<H2dVariableType> variables = new ArrayList<>();
    variables.add(H2dVariableType.DEBIT_M3);
    variables.addAll(H2dRubarBcMng.getTransportVariables(nbBlock));
    return variables;
  }

  protected H2dVariableType getVariable() {
    return H2dVariableType.DEBIT_M3;
  }

  /**
   * @param _e l'evolution a dupliquer
   * @return une nouvelle evolution respectant les validation
   */
  private static H2dRubarEvolution duplicateEvol(final EvolutionReguliereAbstract _e) {
    if (_e == null || _e.getClass().equals(OuvrageEvolution.class)) {
      return (OuvrageEvolution) _e;
    }
    return new OuvrageEvolution(_e);
  }

  private static Map<H2dVariableType, EvolutionReguliere> duplicateEvols(
      LinkedHashMap<H2dVariableType, ? extends EvolutionReguliereAbstract> in) {
    if (in == null) {
      return Collections.emptyMap();
    }
    Map<H2dVariableType, EvolutionReguliere> res = new LinkedHashMap<H2dVariableType, EvolutionReguliere>();
    for (Entry<H2dVariableType, ? extends EvolutionReguliereAbstract> entry : in.entrySet()) {
      res.put(entry.getKey(), duplicateEvol(entry.getValue()));
    }
    return res;
  }

  @Override
  public void evolutionChanged(final EvolutionReguliereInterface _e) {
    if (allEvols != null && allEvols.contains(_e)) {
      fireEvt();
    }
  }

  @Override
  public void fillWithTransientCurves(String prefixe, H2dEvolutionVariableMap r) {
    if (evols_ != null) {
      for (Entry<H2dVariableType, EvolutionReguliere> entry : evols_.entrySet()) {
        entry.getValue().setNom(prefixe + "/" + getType() + ": " + entry.getKey().getLongName());
        r.add(entry.getValue(), entry.getKey(), 0);
      }
    }
  }

  @Override
  public void fillWithTarageEvolution(String prefix, Set allTarage) {

  }

  @Override
  public EvolutionReguliereAbstract getEvol(H2dVariableType variable) {
    return evols_ == null ? null : evols_.get(variable);
  }

  @Override
  public LinkedHashMap<H2dVariableType, EvolutionReguliere> getEvols() {
    return new LinkedHashMap<>(evols_ == null ? Collections.<H2dVariableType, EvolutionReguliere>emptyMap() : evols_);
  }

  final void setEvols(final LinkedHashMap<H2dVariableType, ? extends EvolutionReguliereAbstract> evols) {
    evols_ = duplicateEvols(evols);
    rebuildAllEvols();
  }

  protected void fireEvt() {
    if (ouvrageParent_ != null) {
      ouvrageParent_.getMng().fireOuvrageElementaireChanged(ouvrageParent_, this);
    }
  }

  /**
   * @param _e la nouvelle evolution
   * @param _cmd le receveur de commande
   * @return true si modif
   */
  public boolean updateEvol(final H2dVariableType var, final EvolutionReguliere _e, final CtuluCommandContainer _cmd) {
    final EvolutionReguliere old = evols_.get(var);
    if (old == null) {
      return false;
    }
    if (_e == old || old.isEquivalentTo(_e)) {
      return false;
    }
    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void undo() {
          evols_.put(var, old);
          rebuildAllEvols();
          fireEvt();
        }

        @Override
        public void redo() {
          evols_.put(var, _e);
          rebuildAllEvols();
          fireEvt();
        }
      });
    }
    evols_.put(var, _e);
    rebuildAllEvols();
    fireEvt();
    return true;
  }

  private void rebuildAllEvols() {
    allEvols = evols_ == null ? null : new HashSet<>(evols_.values());
  }

  @Override
  public void setProjetType(final H2dRubarProjetType projet, final int nbTransportBlock, final CtuluCommandComposite cmd) {
    final List<H2dVariableType> allVariables = getTransportVariables(nbTransportBlock);
    if (H2dRubarProjetType.TRANSPORT.equals(projet)) {
      if (evols_.size() != allVariables.size()) {
        final Map<H2dVariableType, EvolutionReguliere> old = new HashMap<H2dVariableType, EvolutionReguliere>(evols_);
        for (H2dVariableType var : allVariables) {
          if (!evols_.containsKey(var)) {
            evols_.put(var, H2dRubarEvolution.EMPTY_EVOL);
          }
        }
        final List<H2dVariableType> all = new ArrayList<>(evols_.keySet());
        for (H2dVariableType variableType : all) {
          if (!allVariables.contains(variableType)) {
            evols_.remove(variableType);
          }
        }
        rebuildAllEvols();
        fireEvt();
        if (cmd != null) {
          cmd.addCmd(new CtuluCommand() {
            @Override
            public void undo() {
              evols_.clear();
              evols_.putAll(old);
              rebuildAllEvols();
              fireEvt();
            }

            @Override
            public void redo() {
              setProjetType(projet, nbTransportBlock, null);
            }
          });
        }
      }
    } else {
      if (evols_.size() != 1 || evols_.get(getVariable()) == null) {
        final Map<H2dVariableType, EvolutionReguliere> old = new HashMap<H2dVariableType, EvolutionReguliere>(evols_);
        evols_.clear();
        evols_.put(getVariable(), old.get(getVariable()));
        if (evols_.get(getVariable()) == null) {
          evols_.put(getVariable(), H2dRubarEvolution.EMPTY_EVOL);
        }
        rebuildAllEvols();
        fireEvt();
        if (cmd != null) {
          cmd.addCmd(new CtuluCommand() {
            @Override
            public void undo() {
              evols_.clear();
              evols_.putAll(old);
              fireEvt();
              rebuildAllEvols();
            }

            @Override
            public void redo() {
              setProjetType(projet, nbTransportBlock, null);
            }
          });
        }
      }
    }
  }

  protected final void initFrom(final H2dRubarOuvrageElementaireApport _a, final boolean _ouvrageParent) {
    if (_ouvrageParent) {
      ouvrageParent_ = _a.ouvrageParent_;
    }
    evols_ = new LinkedHashMap<>(_a.evols_);
    rebuildAllEvols();
  }

  @Override
  public H2dRubarOuvrageElementaireInterface getCopyWithoutEvt() {
    return new H2dRubarOuvrageElementaireApport(this, false);
  }

  @Override
  public H2dRubarOuvrageType getType() {
    return H2dRubarOuvrageType.APPORT_DEBIT;
  }

  private static class OuvrageEvolution extends H2dRubarEvolution {
    public OuvrageEvolution(EvolutionReguliereInterface _evolReg) {
      super(_evolReg);
    }

    @Override
    public boolean isValuesValid(final double _x, final double _y) {
      return true;
    }
  }
}
