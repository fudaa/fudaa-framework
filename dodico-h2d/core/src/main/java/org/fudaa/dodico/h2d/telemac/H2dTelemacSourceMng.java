/*
 * @creation 29 nov. 2004
 *
 * @modification $Date: 2007-04-30 14:21:35 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import com.memoire.fu.FuLog;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntHashSet;
import gnu.trove.TIntIntHashMap;
import gnu.trove.TObjectIntHashMap;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.CtuluListObject;
import org.fudaa.dodico.dico.*;
import org.fudaa.dodico.h2d.resource.H2dResource;

import java.util.*;

/**
 * Telemac 2d uniquement: gestion des siphons.
 *
 * @author Fred Deniger
 * @version $Id: H2dTelemacSourceMng.java,v 1.18 2007-04-30 14:21:35 deniger Exp $
 */
public class H2dTelemacSourceMng implements DicoParamsListener, H2dTelemacTracerLinkedManager {
  @Override
  public void dicoParamsVersionChanged(final DicoParams _cas) {
  }

  private class SourceModel extends CtuluListObject {
    public SourceModel() {
      super();
    }

    public SourceModel(final Collection _init) {
      super(_init);
    }

    public SourceModel(final CtuluListObject _init) {
      super(_init);
    }

    public SourceModel(final int _nb) {
      super(_nb);
    }

    private void majTaille() {

      final int nb = this.getSize();
      if (nb == 0) {
        xEnt_.setRequired(false);
        yEnt_.setRequired(false);
        qEnt_.setRequired(false);
        tracerValuesOnSources.setRequired(false);
        final DicoEntite[] v = new DicoEntite[vectEnt_.size()];
        vectEnt_.toArray(v);
        dico_.removeValues(v, null);
      } else {
        xEnt_.setRequired(true);
        yEnt_.setRequired(true);
        qEnt_.setRequired(true);
        for (int i = vectEnt_.size() - 1; i >= 0; i--) {
          final DicoEntite.Vecteur v = (DicoEntite.Vecteur) vectEnt_.get(i);
          v.setNbElem(nb);
          if (isValueRequired(v) || isValueSet(v)) {
            dico_.setValue(v, computeValueForDico(v));
          }
        }
      }
      updateValidationStates();
    }

    @Override
    protected void fireObjectAdded(int _newIdx, Object _newGeom) {
      majTaille();
    }

    @Override
    protected void fireObjectChanged(int _oldIdx, Object _oldGeom) {
      majTaille();
    }

    @Override
    protected void fireObjectModified(int _idx, Object _geom) {
      majTaille();
    }
  }

  private String[] siphonVar_;
  CtuluListObject sources_;
  // debit,vx,vy,traceur
  Map defaultValues_;
  boolean isVitessesSet_;
  private DicoParams dico_;
  List listener_;
  DicoEntite nbSiphonEnt_;
  DicoEntite.Vecteur qEnt_;
  // valeur par d�faut importante demande n� 1708536 sur sourceforge.
  double siphonRelaxation_ = 0.2;
  CtuluListObject siphons_;
  DicoEntite.Vecteur tracerValuesOnSources;
  CtuluPermanentList vectEnt_;
  DicoEntite.Vecteur vxEnt_;
  DicoEntite.Vecteur vyEnt_;
  DicoEntite.Vecteur xEnt_;
  DicoEntite.Vecteur yEnt_;

  public DicoParams getDico() {
    return dico_;
  }

  public final double getSiphonRelaxation() {
    return siphonRelaxation_;
  }

  /**
   * @param _newVal la nouvelle valeur pour la relaxation
   * @param _cmd le receveur de commande
   */
  public void modifySiphonRelaxation(final double _newVal, final CtuluCommandContainer _cmd) {
    if (_newVal != siphonRelaxation_) {
      final double oldValue = siphonRelaxation_;
      siphonRelaxation_ = _newVal;
      if (_cmd != null) {
        _cmd.addCmd(new CtuluCommand() {
          @Override
          public void undo() {
            siphonRelaxation_ = oldValue;
            fireCulvertChanged();
          }

          @Override
          public void redo() {
            siphonRelaxation_ = _newVal;
            fireCulvertChanged();
          }
        });
      }
      fireCulvertChanged();
    }
  }

  H2dTelemacTracerMng tracerManager;

  /**
   * @param tracerManager les parameters dico permettant d'initialiser les valeurs
   */
  public H2dTelemacSourceMng(H2dTelemacTracerMng tracerManager) {
    CtuluLibMessage.info("init sources");
    dico_ = tracerManager.getDico();
    this.tracerManager = tracerManager;
    initMotCle();
    vectEnt_ = new CtuluPermanentList(new DicoEntite[]{xEnt_, yEnt_, qEnt_, vxEnt_, vyEnt_, tracerValuesOnSources});
    if (nbSiphonEnt_ != null) {
      nbSiphonEnt_.setModifiable(false);
    }
    final CtuluAnalyze a = new CtuluAnalyze();
    firstUpdateFromDico(a);
    if (a.containsErrors()) {
      a.printResume();
    }
    dico_.addModelListener(this);
    tracerManager.addLinkedManager(this);
  }

  public DicoEntite.Vecteur getTracerValuesOnSourcesDicoEntite() {
    return tracerValuesOnSources;
  }

  private void fireSourceChanged(final DicoEntite _v) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dtelemacSiponListener) listener_.get(i)).sourcesChanged(_v);
      }
    }
  }

  private void firstUpdateFromDico(final CtuluAnalyze _analyze) {

    final int nbSiphonUsed = nbSiphonEnt_ == null ? 0 : Integer.parseInt(dico_.getValue(nbSiphonEnt_));
    sources_ = new SourceModel();
    if (dico_.isValueSetFor(xEnt_)) {
      final int nbEnt = vectEnt_.size();
      int nbSourceDefined = -1;
      final String[][] vals = new String[nbEnt][];
      for (int i = 0; i < nbEnt; i++) {
        final DicoEntite.Vecteur v = (DicoEntite.Vecteur) vectEnt_.get(i);
        if (v == tracerValuesOnSources) {
          continue;
        }
        vals[i] = getValueFromDico(v, nbSourceDefined, _analyze);
        if (vals[i] != null) {
          if (nbSourceDefined < 0) {
            nbSourceDefined = vals[i].length;
          } else {
            nbSourceDefined = Math.max(nbSourceDefined, vals[i].length);
          }
        }
      }
      CtuluLibMessage.info("number of sources " + nbSourceDefined);
      final List l = new ArrayList();
      int nbTraceur = getNbTracers();
      if (nbSourceDefined > 0) {
        for (int siph = 0; siph < nbSourceDefined; siph++) {
          final H2dTelemacSource s = new H2dTelemacSource();
          l.add(s);
          for (int i = 0; i < nbEnt; i++) {
            final DicoEntite.Vecteur v = (DicoEntite.Vecteur) vectEnt_.get(i);
            if (v == tracerValuesOnSources) {
              s.setTraceurs(new double[nbTraceur]);
              continue;
            }
            setValue(s, v, getValueFromArray(vals[i], siph));
            if (siph == 0) {
              if (isValueRequired(v)) {
                v.setRequired(true);
              }
              v.setNbElem(nbSourceDefined);
            }
          }
        }
        if (nbSiphonUsed > nbSourceDefined) {
          FuLog.error("probleme");
        }
      }

      sources_ = new SourceModel(l);
      // on met a jour les mot-cles errones
      for (int i = 0; i < nbEnt; i++) {
        final DicoEntite.Vecteur v = (DicoEntite.Vecteur) vectEnt_.get(i);
        if (nbSourceDefined == 0) {
          dico_.removeValue(v);
        } else if (v != tracerValuesOnSources && isValueSet(v) && (vals[i].length != nbSourceDefined)) {
          dico_.setValue(v, computeValueForDico(v), null);
        }
      }
      // si vx initialise, vy doit l'etre aussi
      if (isValueSet(vxEnt_) && !isValueSet(vyEnt_)) {
        dico_.setValue(vyEnt_, computeValueForDico(vyEnt_), null);
        vxEnt_.setRequired(true);
        vyEnt_.setRequired(true);
      }
      if (isValueSet(vyEnt_) && !isValueSet(vxEnt_)) {
        dico_.setValue(vxEnt_, computeValueForDico(vxEnt_), null);
        vxEnt_.setRequired(true);
        vyEnt_.setRequired(true);
      }
    }
    majFromDico(tracerValuesOnSources);
  }

  public boolean isTracerUsed() {
    return getNbTracers() > 0;
  }

  private void updateValidationStates() {
    final int nbTraceursUsed = getNbTracers();
    final int nbSource = getNbSource();
    tracerValuesOnSources.setNbElem(nbSource * nbTraceursUsed);
    tracerValuesOnSources.setRequired(tracerValuesOnSources.getNbElemFixed() > 0);
    dico_.testIfValid(tracerValuesOnSources);
  }

  /**
   * @param _s la source a tester
   * @return l'indice de cette source ou -1 si non trouvee
   */
  public int indexOf(final H2dTelemacSource _s) {
    return sources_ == null ? -1 : sources_.indexOf(_s);
  }

  private double getValue(final int _idx, final DicoEntite.Vecteur _ent) {
    if (_ent == xEnt_) {
      return getSource(_idx).getX();
    } else if (_ent == yEnt_) {
      return getSource(_idx).getY();
    } else if (_ent == qEnt_) {
      return getSource(_idx).getQ();
    } else if (_ent == vxEnt_) {
      return getSource(_idx).getVx();
    } else if (_ent == vyEnt_) {
      return getSource(_idx).getVy();
    }
    new Throwable().printStackTrace();
    return 0;
  }

  private double getValueFromArray(final String[] _tab, final int _i) {
    if (_tab == null || _i >= _tab.length) {
      return 0;
    }
    return Double.parseDouble(_tab[_i]);
  }

  private String[] getValueFromDico(final DicoEntite.Vecteur _v, final int _nbDef, final CtuluAnalyze _analyze) {
    if (dico_.isValueSetFor(_v)) {
      final String[] r = _v.getValues(dico_.getValue(_v));
      if (_nbDef > 0 && r.length != _nbDef && _analyze != null) {
        _analyze.addError(H2dResource.getS("Incoh�rence dans les mot-cl�s d�finissant les siphons"), -1);
      }
      return r;
    }
    return null;
  }

  private void initMotCle() {
    final DicoCasFileFormatVersionAbstract model = dico_.getDicoFileFormatVersion();
    nbSiphonEnt_ = model.getEntiteFor(new String[]{"NOMBRE DE SIPHONS", "NUMBER OF CULVERTS"});
    if (nbSiphonEnt_ == null) {
      new Throwable().printStackTrace();
    }
    xEnt_ = (DicoEntite.Vecteur) model.getEntiteFor(new String[]{"ABSCISSES DES SOURCES", "ABSCISSAE OF SOURCES"});
    if (xEnt_ == null) {
      new Throwable().printStackTrace();
    }

    yEnt_ = (DicoEntite.Vecteur) model.getEntiteFor(new String[]{"ORDONNEES DES SOURCES", "ORDINATES OF SOURCES"});
    if (yEnt_ == null) {
      new Throwable().printStackTrace();
    }
    qEnt_ = (DicoEntite.Vecteur) model.getEntiteFor(new String[]{"DEBITS DES SOURCES", "WATER DISCHARGE OF SOURCES"});
    if (qEnt_ == null) {
      new Throwable().printStackTrace();
    }
    tracerValuesOnSources = (DicoEntite.Vecteur) model.getEntiteFor(new String[]{"VALEURS DES TRACEURS DES SOURCES",
        "VALUES OF THE TRACERS AT THE SOURCES"});

    vxEnt_ = (DicoEntite.Vecteur) model.getEntiteFor(new String[]{"VITESSES DES SOURCES SELON X", "VELOCITIES OF THE SOURCES ALONG X"});
    if (vxEnt_ == null) {
      new Throwable().printStackTrace();
    }
    vyEnt_ = (DicoEntite.Vecteur) model.getEntiteFor(new String[]{"VITESSES DES SOURCES SELON Y", "VELOCITIES OF THE SOURCES ALONG Y"});
    if (vyEnt_ == null) {
      new Throwable().printStackTrace();
    }
  }

  private boolean isStringAppliable(final String _s) {
    return _s != null && _s.trim().length() > 0;
  }

  /**
   * @return les noms des variables a definir en chaque extremite d'un siphon
   */
  public String[] getExtremiteVarGeneralName() {
    return new String[]{H2dResource.getS("Angle par rapport � x"), H2dResource.getS("Coefficient de perte de charge en entr�e"),
        H2dResource.getS("Coefficient de perte de charge en sortie"), H2dResource.getS("Cote des entonnement"),
        H2dResource.getS("angle (degr�) de la conduite par rapport au fond")};
  }

  private String getStringForSource(final String _init, final int _srcIdx) {
    return new StringBuffer(_init.length() + 10).append(_init).append(CtuluLibString.ESPACE).append(H2dResource.getS("source"))
        .append(CtuluLibString.ESPACE).append((_srcIdx == 1) ? (CtuluLibString.UN) : (CtuluLibString.DEUX)).toString();
  }

  private void majSiphonVar() {
    if (siphonVar_ == null) {
      siphonVar_ = new String[]{getStringForSource(H2dResource.getS("Angle par rapport � x"), 1),// d1 0
          getStringForSource(H2dResource.getS("Angle par rapport � x"), 2),// d2 1
          getStringForSource(H2dResource.getS("Coefficient de perte de charge en entr�e"), 1),// ce1
          // 2
          getStringForSource(H2dResource.getS("Coefficient de perte de charge en entr�e"), 2),// ce2
          // 3
          getStringForSource(H2dResource.getS("Coefficient de perte de charge en sortie"), 1),// cs1
          // 4
          getStringForSource(H2dResource.getS("Coefficient de perte de charge en sortie"), 2),// cs2
          // 5
          H2dResource.getS("Section de la conduite"),// s12 6
          H2dResource.getS("Coefficient de perte de charge lin�aire"),// l12 7
          getStringForSource(H2dResource.getS("Cote des entonnement"), 1),// z1 8
          getStringForSource(H2dResource.getS("Cote des entonnement"), 2),// z2 9
          getStringForSource(H2dResource.getS("angle (degr�) de la conduite par rapport au fond"), 1),// a1 10
          getStringForSource(H2dResource.getS("angle (degr�) de la conduite par rapport au fond"), 2),// a2 11
      };
    }
  }

  private boolean setTraceur(final H2dTelemacSource _s, final String _d) {
    String[] values = tracerValuesOnSources.getValues(_d);
    String[] currentValues = _s.getTracersInString();
    double[] doubleValues = null;
    if (values != null) {
      doubleValues = new double[values.length];
      for (int i = 0; i < doubleValues.length; i++) {
        try {
          if (CtuluLibString.isEmpty(values[i])) {
            doubleValues[i] = Double.parseDouble(currentValues[i]);
          } else {
            doubleValues[i] = Double.parseDouble(values[i]);
          }
        } catch (NumberFormatException e) {
          doubleValues[i] = _s.getTracers()[i];
        }
      }
    }
    return setTraceur(_s, doubleValues);
  }

  private boolean setTraceur(final H2dTelemacSource _s, final double[] _d) {
    int nbTraceur = getNbTracers();
    if (nbTraceur == 0) {
      _s.setTraceurs(null);
    } else {
      double[] vals = new double[nbTraceur];
      if (_d != null) {
        int nb = Math.min(_d.length, vals.length);
        for (int i = 0; i < nb; i++) {
          vals[i] = _d[i];
        }
      }
      _s.setTraceurs(vals);
    }
    return true;
  }

  private int getNbTracers() {
    return tracerManager.getNbTraceurs();
  }

  private boolean setValue(final H2dTelemacSource _s, final DicoEntite _ent, final double _d) {
    if (_ent == xEnt_) {
      return _s.setX(_d);
    } else if (_ent == yEnt_) {
      return _s.setY(_d);
    } else if (_ent == qEnt_) {
      return _s.setQ(_d);
    } else if (_ent == vxEnt_) {
      return _s.setVx(_d);
    } else if (_ent == vyEnt_) {
      return _s.setVy(_d);
    }
    new Throwable().printStackTrace();
    return false;
  }

  private boolean setValue(final int _idx, final DicoEntite _ent, final double _d) {
    return setValue(getSource(_idx), _ent, _d);
  }

  private void setValueAndUpdateDico(final int _idx, final String _val, final DicoEntite.Vecteur _ent, final CtuluCommandContainer _cmd) {
    if (_ent == tracerValuesOnSources) {
      setTraceur(getSource(_idx), _val);
      final String newVal = computeValueForDico(_ent);
      dico_.setValue(_ent, newVal, _cmd);
    } else if ((isValueSet(_ent) && (getValueFromDico(_ent, -1, null).length != sources_.getSize()))
        || (isStringAppliable(_val) && setValue(_idx, _ent, Double.parseDouble(_val)))) {
      final String newVal = computeValueForDico(_ent);
      dico_.setValue(_ent, newVal, _cmd);
    }
  }

  protected void majFromDico(final DicoEntite.Vecteur _v) {
    majFromDico(_v, null);
  }

  protected void majFromDico(final DicoEntite.Vecteur _v, CtuluCommandContainer cmp) {
    if (sources_ == null) {
      return;
    }
    if (_v == tracerValuesOnSources) {
      int nbTraceur = getNbTracers();
      int nbSources = sources_.getSize();
      int nbTotal = nbTraceur * nbSources;
      final String[] vals = getValueFromDico(_v, nbTotal, null);
      tracerValuesOnSources.setRequired(nbTotal > 0);
      tracerValuesOnSources.setNbElem(nbTotal);
      for (int i = 0; i < nbSources; i++) {
        getSource(i).ensureTracerSizeIs(nbTraceur);
      }
      if (vals == null || vals.length != nbTotal) {
        String old = dico_.getValue(_v);
        if (old == null) {
          old = CtuluLibString.EMPTY_STRING;
        }
        String computeValueForDico = computeValueForDico(_v);
        if (computeValueForDico == null) {
          computeValueForDico = CtuluLibString.EMPTY_STRING;
        }
        if (!old.equals(computeValueForDico)) {
          if (CtuluLibString.isEmpty(computeValueForDico)) {
            dico_.removeValue(_v);
          } else {
            dico_.setValue(_v, computeValueForDico, cmp);
          }
        }
      } else {
        for (int i = 0; i < nbSources; i++) {
          double[] traceurs = new double[nbTraceur];
          for (int traceurIdx = 0; traceurIdx < traceurs.length; traceurIdx++) {
            traceurs[traceurIdx] = Double.parseDouble(vals[i * nbTraceur + traceurIdx]);
          }
          setTraceur(getSource(i), traceurs);
        }
      }
    } else {
      final String[] vals = getValueFromDico(_v, sources_.getSize(), null);
      if (vals == null || vals.length != sources_.getSize()) {
        dico_.setValue(_v, computeValueForDico(_v));
      } else {
        for (int i = vals.length - 1; i >= 0; i--) {
          setValue(i, _v, Double.parseDouble(vals[i]));
        }
      }
    }
  }

  String computeValueForDico(final DicoEntite.Vecteur _ent) {
    final String[] temp = new String[getNbSource()];
    for (int i = getNbSource() - 1; i >= 0; i--) {
      temp[i] = getDisplayValue(i, _ent);
    }
    if (_ent == tracerValuesOnSources) {
      return CtuluLibString.arrayToString(temp, _ent.getSepChar());
    }
    return _ent.getStringValuesArray(temp);
  }

  /**
   * Ne fait pas de controle pour savoir si le listener est deja contenu.
   *
   * @param _l le listener a ajouter
   */
  public void addListener(final H2dtelemacSiponListener _l) {
    if (listener_ == null) {
      listener_ = new ArrayList(10);
    }
    listener_.add(_l);
  }

  @Override
  public void shiftTracersValues(int[] idx, final CtuluCommandContainer _cmd) {
    TIntHashSet idxToAvoid = new TIntHashSet();
    int nbTraceurs = getNbTracers();
    for (int i = 0; i < idx.length; i++) {
      int index = idx[i];
      if (index >= 0 && index < nbTraceurs) {
        idxToAvoid.add(index);
      }
    }
    CtuluCommandComposite cmp = new CtuluCommandCompositeInverse();

    List<String> newSourceValues = new ArrayList<String>();
    int nbSource = getNbSource();
    for (int idxSource = 0; idxSource < nbSource; idxSource++) {
      double[] t = getSource(idxSource).getTracers();
      List<String> values = new ArrayList<String>(nbTraceurs);
      for (int traceurIdx = 0; traceurIdx < nbTraceurs; traceurIdx++) {
        if (!idxToAvoid.contains(traceurIdx)) {
          values.add(Double.toString(t[traceurIdx]));
        }
      }
      for (int traceurIdx = values.size(); traceurIdx < nbTraceurs; traceurIdx++) {
        values.add(CtuluLibString.ZERO);
      }
      newSourceValues.addAll(values);
    }
    String newValuesOnSource = tracerValuesOnSources.getStringValue(newSourceValues);
    Map<DicoEntite, String> newValues = new HashMap<DicoEntite, String>();
    newValues.put(tracerValuesOnSources, newValuesOnSource);
    DicoMultipleUndoCommands undo = new DicoMultipleUndoCommands(newValues, dico_);
    dico_.setValue(tracerValuesOnSources, newValuesOnSource, null);
    cmp.addCmd(undo);
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
  }

  /**
   * Ajoute une nouvelles source. Les deux premieres valeurs X et Y sont requises. Si _vals[0] ou _vals[1] est null, la
   * commande sera arretee.
   *
   * @param _vals les valeurs : taille de la liste des mot-cles. Si une valeur n�cessaire manque, une valeur par defaut
   *     sera utilisee
   * @param _cmd le receveur de commande
   */
  public void addSource(final String[] _vals, final CtuluCommandContainer _cmd) {
    if ((_vals == null) || (_vals.length != vectEnt_.size())) {
      return;
    }
    if ((_vals[0] == null) || (_vals[0].trim().length() == 0)) {
      return;
    }
    if ((_vals[1] == null) || (_vals[1].trim().length() == 0)) {
      return;
    }
    final H2dTelemacSource sip = new H2dTelemacSource();
    for (int i = 0; i < _vals.length; i++) {
      final DicoEntite.Vecteur v = (DicoEntite.Vecteur) vectEnt_.get(i);
      if (isValueRequired(v) || isValueSet(v)) {
        if (v == tracerValuesOnSources) {
          setTraceur(sip, _vals[i]);
        } else {
          final String valeur = _vals[i] == null ? v.getType().getDefaultValue() : _vals[i];
          final double d = Double.parseDouble(valeur);
          setValue(sip, (DicoEntite.Vecteur) vectEnt_.get(i), d);
        }
      }
    }
    boolean first = false;
    if (sources_ == null || sources_.getSize() == 0) {
      first = true;
      sources_ = new SourceModel();
    }
    sources_.addObject(sip, _cmd);
    if (first && (isVitessesSet_)) {
      setSourceDefined(isVitessesSet_, getDefaultValue(vxEnt_), getDefaultValue(vyEnt_), null);
    }
  }

  /**
   * @param _l le listener a tester
   * @return true si contenu
   */
  public boolean containsListener(final H2dtelemacSiponListener _l) {
    return listener_ != null && listener_.contains(_l);
  }

  @Override
  public void dicoParamsEntiteAdded(final DicoParams _cas, final DicoEntite _ent) {
    // c'est un mot-cle gere
    if (vectEnt_.indexOf(_ent) >= 0) {
      majFromDico((DicoEntite.Vecteur) _ent);
      fireSourceChanged(_ent);
    }
  }

  @Override
  public void dicoParamsEntiteRemoved(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
    //we manage the cases if tracer are required (number >0).
    if (_ent == tracerValuesOnSources) {
      if (isTracerUsed() && getNbSource() > 0) {
        dico_.setValue(_ent, _oldValue);
      }
    }
    // c'est un mot-cle gere
    else if (vectEnt_.indexOf(_ent) >= 0) {
      // mot-cle requis aie: on update les mot-cles
      if (getNbSource() > 0 && isValueRequired(_ent)) {
        new Throwable().printStackTrace();
        dico_.setValue(_ent, computeValueForDico((DicoEntite.Vecteur) _ent));
      }
      // sinon on initialise les valeurs
      else {
        for (int i = sources_.getSize() - 1; i >= 0; i--) {
          setValue(i, _ent, 0);
        }
        /*
         * //on met a jour l'etat sauf si c'est le dernier siphon if (definedValues_ != null && getNbSource() > 0)
         * definedValues_[vectEnt_.indexOf(_ent)] = false;
         */
        fireSourceChanged(_ent);
      }
    }
  }

  @Override
  public void dicoParamsEntiteUpdated(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
    if (vectEnt_.indexOf(_ent) >= 0) {
      majFromDico((DicoEntite.Vecteur) _ent);
    }
    fireSourceChanged(_ent);
  }

  @Override
  public void dicoParamsEntiteCommentUpdated(final DicoParams _cas, final DicoEntite _ent) {
  }

  public void dicoParamsProjectModifyStateChanged(final DicoParams _cas) {
  }

  public void dicoParamsStateLoadedEntiteChanged(final DicoParams _cas, final DicoEntite _ent) {
  }

  @Override
  public void dicoParamsValidStateEntiteUpdated(final DicoParams _cas, final DicoEntite _ent) {
  }

  /**
   * @param _selectedSiphon les siphons selectionnes
   * @param _ent le mot-cles concerne
   * @return la valeur commune ou null si non commune. Pour les traceurs, retourne une chaine de caractere avec chaine
   *     vide si non commun.
   */
  public String getCommonValues(final int[] _selectedSiphon, final DicoEntite.Vecteur _ent) {
    if (_selectedSiphon == null || _selectedSiphon.length == 0) {
      return null;
    }
    if (_ent == tracerValuesOnSources) {
      String[] values = getSource(_selectedSiphon[0]).getTracersInString();
      for (int idxSource = _selectedSiphon.length - 1; idxSource > 0; idxSource--) {
        String[] valuesToCompare = getSource(_selectedSiphon[idxSource]).getTracersInString();
        for (int idxTracer = 0; idxTracer < valuesToCompare.length; idxTracer++) {
          String string = valuesToCompare[idxTracer];
          if (!string.equals(values[idxTracer])) {
            values[idxTracer] = CtuluLibString.ESPACE;
          }
        }
      }
      return tracerValuesOnSources.getStringValuesArray(values);
    }
    final String init = getDisplayValue(_selectedSiphon[0], _ent);
    for (int i = _selectedSiphon.length - 1; i > 0; i--) {
      if (!init.equals(getDisplayValue(_selectedSiphon[i], _ent))) {
        return null;
      }
    }
    return init;
  }

  private void getCommonSiphonEndsValues(final int _idxSiphon, final int _endSelect, final int[] _var1, final int[] _var2, final double[] _dest,
                                         final boolean _init) {
    final H2dtelemacSiphon siphon = getSiphon(_idxSiphon);
    // extrem 1 uniqument
    if (_endSelect == 1) {
      if (_init) {
        siphon.getValues(_var1, _dest);
      } else {
        siphon.keepCommonValues(_var1, _dest);
      }
    } else if (_endSelect == 2) {
      if (_init) {
        siphon.getValues(_var2, _dest);
      } else {
        siphon.keepCommonValues(_var2, _dest);
      }
    } else if (_endSelect == 3) {
      if (_init) {
        siphon.getValues(_var1, _dest);
        siphon.keepCommonValues(_var2, _dest);
      } else {
        siphon.keepCommonValues(_var1, _dest);
        siphon.keepCommonValues(_var2, _dest);
      }
    } else {
      new Throwable().printStackTrace();
    }
  }

  /**
   * @param _idxSiphon les siphons dont les extremites sont selectionnees
   * @param _idxExtremiteSelec pour chaque siphon, les extremites selectionnees 1,2,3 (les deux)
   * @return les valeurs communes (-1 si non communes)
   */
  public double[] getCommonSiphonEndsValues(final int[] _idxSiphon, final int[] _idxExtremiteSelec) {
    if ((_idxSiphon == null) || (_idxSiphon.length == 0) || (_idxExtremiteSelec == null) || (_idxExtremiteSelec.length != _idxSiphon.length)) {
      return null;
    }
    final int[] var1 = getSiphonSource1IndexVar();
    final int[] var2 = getSiphonSource2IndexVar();
    final double[] r = new double[var1.length];
    getCommonSiphonEndsValues(_idxSiphon[0], _idxExtremiteSelec[0], var1, var2, r, true);
    for (int i = _idxSiphon.length - 1; i >= 0; i--) {
      getCommonSiphonEndsValues(_idxSiphon[i], _idxExtremiteSelec[i], var1, var2, r, false);
    }
    return r;
  }

  /**
   * @param _idx l'indice de la source a dupliquer
   * @param _cmd la commande
   * @return l'indice de la source ajoutee
   */
  public int duplicateSource(final int _idx, final CtuluCommandContainer _cmd) {
    final int r = sources_.getSize();
    sources_.addObject(new H2dTelemacSource(getSource(_idx)), _cmd);
    return r;
  }

  /**
   * @param _idx1 l'indice de la source 1 pour le siphon a ajouter
   * @param _idx2 l'indice de la source 2 pour le siphon a ajouter
   * @param _cmd le receveur de commande
   * @return l'indice du nouveau siphon. -1 si non ajoute (source deja utilisee)
   */
  public int addSiphon(final int _idx1, final int _idx2, final CtuluCommandContainer _cmd) {
    if (isSiphonUsingSource(_idx1) || isSiphonUsingSource(_idx2)) {
      CtuluLibMessage.debug("sources already used");
      return -1;
    }
    if (siphons_ == null) {
      siphons_ = new SiphonModel();
    }
    final int r = siphons_.getSize();
    final H2dtelemacSiphon newSip = new H2dtelemacSiphon(_idx1, _idx2, getSource(_idx1), getSource(_idx2), siphonsDefaultValues_);
    siphons_.addObject(newSip, _cmd);
    return r;
  }

  private final double[] siphonsDefaultValues_ = new double[]{0, 0, 0.5, 0.5, 1, 1, -1, 0, -1, -1, 0, 0};

  /**
   * @param _i l'indice de la variable
   * @return la valeur par defaut a proposer pour cette variable
   */
  public double getSiphonDefaultValues(final int _i) {
    return siphonsDefaultValues_[_i];
  }

  public String getDefaultValue(final DicoEntite _ent) {
    if (defaultValues_ != null && defaultValues_.containsKey(_ent)) {
      return (String) defaultValues_.get(_ent);
    }
    return _ent.getType().getDefaultValue();
  }

  void setTraceurSetFirst(final boolean _r, final CtuluCommandContainer _cmd) {
    //TODO
    //    final boolean old = isTraceurSet_;
    //    isTraceurSet_ = _r;
    //    if (_cmd != null) {
    //      _cmd.addCmd(new CtuluCommand() {
    //
    //        public void undo() {
    //          setTraceurSetFirst(old, null);
    //        }
    //
    //        public void redo() {
    //          setTraceurSetFirst(_r, null);
    //        }
    //      });
    //    }
  }

  void setVitesseSetFirst(final boolean _r, final CtuluCommandContainer _cmd) {
    final boolean old = isVitessesSet_;
    isVitessesSet_ = _r;
    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void undo() {
          setVitesseSetFirst(old, null);
        }

        @Override
        public void redo() {
          setVitesseSetFirst(_r, null);
        }
      });
    }
  }

  public void setDefautValue(final DicoEntite _ent, final String _s, final CtuluCommandContainer _cmd) {
    if (_s != null && !_s.equals(getDefaultValue(_ent))) {
      if (defaultValues_ == null) {
        defaultValues_ = new HashMap(5);
      }
      final Object old = defaultValues_.put(_ent, _s);
      if (_cmd != null) {
        _cmd.addCmd(new CtuluCommand() {
          @Override
          public void undo() {
            if (old == null) {
              defaultValues_.remove(_ent);
            } else {
              defaultValues_.put(_ent, old);
            }
          }

          @Override
          public void redo() {
            setDefautValue(_ent, _s, null);
          }
        });
      }
    }
  }

  /**
   * @param _idx l'indice du siphon
   * @param _ent le mot-cles correspondant
   * @return la valeur correspondante (Double.toString)
   */
  public String getDisplayValue(final int _idx, final DicoEntite.Vecteur _ent) {
    if (_ent == tracerValuesOnSources) {
      double[] t = getSource(_idx).getTracers();
      if (t == null || t.length == 0) {
        return CtuluLibString.EMPTY_STRING;
      }
      return CtuluLibString.arrayToString(t, tracerValuesOnSources.getSepChar());
    }
    return Double.toString(getValue(_idx, _ent));
  }

  /**
   * @return les mots-cles utilises pour les definir les sources
   */
  public CtuluPermanentList getKeywordForSource() {
    return vectEnt_;
  }

  /**
   * @return le nombre de siphons definis
   */
  public int getNbSiphon() {
    return siphons_ == null ? 0 : siphons_.getSize();
  }

  /**
   * @return le nombre de sources definies
   */
  public int getNbSource() {
    return sources_ == null ? 0 : sources_.getSize();
  }

  /**
   * Pas de controle.
   *
   * @param _idx l'indice du siphon demande
   * @return le siphon demande
   */
  public H2dtelemacSiphon getSiphon(final int _idx) {
    return (H2dtelemacSiphon) siphons_.getValueAt(_idx);
  }

  /**
   * @return les indices a utiliser pour editer les variable globlaes (section et coefficient)
   */
  public int[] getSiphonCommonIndexVar() {
    return new int[]{6, 7};
  }

  /**
   * @param _idxSiphon les siphons concernees
   * @param _idxValues les indices des variables
   * @return tableau de la meme taille que idxValues contenant les valeurs communes.
   */
  public Double[] getSiphonCommonValuesFor(final int[] _idxSiphon, final int[] _idxValues) {
    if (_idxValues == null || getNbSiphon() == 0) {
      return null;
    }
    final Double[] r = new Double[_idxValues.length];
    if ((_idxSiphon != null) && (_idxSiphon.length > 0)) {
      H2dtelemacSiphon sip = getSiphon(_idxSiphon[0]);
      for (int valueIdx = _idxValues.length - 1; valueIdx >= 0; valueIdx--) {
        r[valueIdx] = CtuluLib.getDouble(sip.getValue(_idxValues[valueIdx]));
      }
      for (int i = _idxSiphon.length - 1; i > 0; i--) {
        sip = getSiphon(_idxSiphon[i]);
        for (int valueIdx = _idxValues.length - 1; valueIdx >= 0; valueIdx--) {
          if (r[valueIdx] != null && r[valueIdx].doubleValue() != sip.getValue(_idxValues[valueIdx])) {
            r[valueIdx] = null;
          }
        }
      }
    }
    return r;
  }

  /**
   * @return les indices a utiliser pour modifier les variables de la source 1 d'un siphon
   */
  public int[] getSiphonSource1IndexVar() {
    return new int[]{0, 2, 4, 8, 10};
  }

  /**
   * @return les indices a utiliser pour modifier les variables de la source 2 d'un siphon
   */
  public int[] getSiphonSource2IndexVar() {
    return new int[]{1, 3, 5, 9, 11};
  }

  /**
   * Les variables utilisees pour les siphons sont rang�es dans un ordre pr�cis a respecter.
   *
   * @param _i l'indice demande
   * @return la variable concernee
   */
  public String getSiphonVariableName(final int _i) {
    majSiphonVar();
    return siphonVar_[_i];
  }

  /**
   * Aucun test effectue sur _i.
   *
   * @param _i l'indice de la source
   * @return la source d'indice _i
   */
  public H2dTelemacSource getSource(final int _i) {
    return (H2dTelemacSource) sources_.getValueAt(_i);
  }

  /**
   * @param _e le mot-cle a tester
   * @return true si correspond a vx ou vy
   */
  public boolean isCoordonnesEnt(final DicoEntite _e) {
    return _e == xEnt_ || _e == yEnt_;
  }

  /**
   * @param _dicoIdx l'indice du mot-cle
   * @return true si le mot-cle doit etre cree par defaut
   */
  public boolean isSetByDefault(final int _dicoIdx) {
    final DicoEntite ent = (DicoEntite) vectEnt_.get(_dicoIdx);
    if (isMutableValues(ent)) {
      if (ent == tracerValuesOnSources) {
        return isTracerUsed();
      } else if (isVitesse(ent)) {
        return isVitessesSet_;
      } else {
        new Throwable().printStackTrace();
        return false;
      }
    }
    return true;
  }

  public boolean isMutableValues(final DicoEntite _ent) {
    return _ent == vxEnt_ || _ent == vyEnt_ || _ent == tracerValuesOnSources;
  }

  public String getTraceurName(int idxTraceur) {
    return tracerManager.getTraceurName(idxTraceur);
  }

  /**
   * @param _ent le mot-cle a tester
   * @return true si la valeur est requis (x,y).
   */
  public final boolean isValueRequired(final DicoEntite _ent) {
    return (_ent == xEnt_) || (_ent == yEnt_) || (_ent == qEnt_) || (_ent == tracerValuesOnSources && getNbTracers() > 0);
  }

  public boolean isVx(final DicoEntite _ent) {
    return _ent == vxEnt_;
  }

  public boolean isVitesse(final DicoEntite _ent) {
    return isVx(_ent) || isVy(_ent);
  }

  public boolean isVy(final DicoEntite _ent) {
    return _ent == vyEnt_;
  }

  public boolean isDebit(final DicoEntite _ent) {
    return _ent == qEnt_;
  }

  public boolean isTraceur(final DicoEntite _ent) {
    return _ent == tracerValuesOnSources;
  }

  /**
   * @param _ent le mot-cle a tester
   * @return true si la valeur correspondante est cree
   */
  public boolean isValueSet(final DicoEntite _ent) {
    return dico_.isValueSetFor(_ent);
  }

  /**
   * @return true si le mot-cle definissant la vitesse selon x est cree
   */
  public boolean isVitesseDefined() {
    return isVxDefined();
  }

  boolean isVxDefined() {
    return dico_.isValueSetFor(vxEnt_);
  }

  boolean isVyDefined() {
    return dico_.isValueSetFor(vyEnt_);
  }

  /**
   * Permet d'importer des siphons.
   *
   * @param _c le conteneur de siphons
   * @param _analyse le receveur des erreurs.
   */
  public void loadSiphons(final H2dTelemacSiphonContainer _c, final CtuluAnalyze _analyse) {
    if (_c == null || _c.getNbSiphon() == 0) {
      return;
    }
    // que faire si des siphons ont d�ja ete importes
    if (siphons_ != null) {
      new Throwable().printStackTrace();
    }
    // mise a jour de la relaxation
    siphonRelaxation_ = _c.getRelaxation();
    // servira a savoir le nombre de siphons corrects et reellement charges
    final int nbSiphonDefini = _c.getNbSiphon();
    final int nbSrc = getNbSource();
    // liste temporaire contenant les siphons corrects
    final List siphons = new ArrayList(nbSiphonDefini);
    // pour l'instant telemac ne support que deux siphons utilise la meme source.
    final TIntHashSet idxUsed = new TIntHashSet();
    majSiphonVar();
    final int nbVar = siphonVar_.length;
    for (int i = 0; i < nbSiphonDefini; i++) {
      final H2dTelemacSiphonInterface si = _c.getSiphon(i);
      final int i1 = si.getSource1();
      final int i2 = si.getSource2();
      // ne doit jamais arriver
      if (si.getSize() == nbVar) {
        if ((i1 >= nbSrc) || (i2 >= nbSrc)) {
          if (_analyse != null) {
            _analyse.addError(H2dResource.getS("Le siphon {0} est ignor� : indices invalides", CtuluLibString.getString(i + 1)), i + 1);
          }
        }
        // les indices sont deja utilises :erreur
        // A ENLEVER LORSQUE TELEMAC SUPPORTERA LE CAS UNE SOURCE-> PLUSIEURS SIPHONS
        else if (idxUsed.contains(i1) || idxUsed.contains(i2)) {
          if (_analyse != null) {
            _analyse.addError(
                H2dResource.getS("Le siphon {0} est ignor� : sources d�j� utilis�es par un autre siphon", CtuluLibString.getString(i + 1)), i + 1);
          }
        }
        // tout est ok: on y va
        else {
          idxUsed.add(i1);
          idxUsed.add(i2);
          // indices correct
          siphons.add(new H2dtelemacSiphon(i1, i2, getSource(i1), getSource(i2), si));
        }
      } else {
        new Throwable().printStackTrace();
      }
    }
    siphons_ = new SiphonModel(siphons);
    // on met a jour le mot-cle correspondant
    dico_.setValue(nbSiphonEnt_, CtuluLibString.getString(siphons_.getSize()));
    nbSiphonEnt_.setModifiable(false);
  }

  /**
   * @param _idxSiphons les indices des siphons a modifier
   * @param _idxValues les indices des valeurs a modifier
   * @param _values les nouveelles valeurs
   * @param _cmd le receveur de commandes
   */
  public void modifySiphon(final int[] _idxSiphons, final int[] _idxValues, final double[] _values, final CtuluCommandContainer _cmd) {
    if (siphons_ == null || _idxSiphons == null || _idxValues == null || _values == null || _idxValues.length != _values.length) {
      CtuluLibMessage.debug("error in parameter for modifyong culvert");
      return;
    }
    final CtuluCommandComposite cmd = new CtuluCommandComposite() {
      @Override
      protected boolean isActionToDoAfterUndoOrRedo() {
        return true;
      }

      @Override
      protected void actionToDoAfterUndoOrRedo() {
        fireCulvertChanged();
      }
    };
    for (int i = _idxSiphons.length - 1; i >= 0; i--) {
      final H2dtelemacSiphon sip = getSiphon(_idxSiphons[i]);
      // int cmdSize = cmd.getNbCmd();
      sip.setValues(_idxValues, _values, cmd);
      // int cmdSizeAfter = cmd.getNbCmd();
    }
    if ((_cmd != null) && !cmd.isEmpty()) {
      _cmd.addCmd(cmd);
    }
  }

  private class SiphonModel extends CtuluListObject {
    /**
     *
     */
    public SiphonModel() {
      super();
    }

    /**
     * @param _init
     */
    public SiphonModel(final Collection _init) {
      super(_init);
    }

    /**
     * @param _init
     */
    public SiphonModel(final CtuluListObject _init) {
      super(_init);
    }

    /**
     * @param _nb
     */
    public SiphonModel(final int _nb) {
      super(_nb);
    }

    @Override
    protected void fireObjectAdded(int _newIdx, Object _newGeom) {
      dico_.setValue(nbSiphonEnt_, CtuluLibString.getString(getNbSiphon()));
      fireCulvertAdded();
    }

    @Override
    protected void fireObjectRemoved(int _oldIdx, Object _oldGeom) {
      dico_.setValue(nbSiphonEnt_, CtuluLibString.getString(getNbSiphon()));
      fireCulvertRemoved();
    }

    @Override
    protected void fireObjectModified(int _idx, Object _geom) {
      new Throwable().printStackTrace();
      fireCulvertRemoved();
    }
  }

  /**
   * @param _idxSiphons les indices des siphons a enlever
   * @param _cmd le receveur de commande
   */
  public void removeSiphons(final int[] _idxSiphons, final CtuluCommandContainer _cmd) {
    if (siphons_ != null) {
      // l'action a bien eu lieu
      siphons_.remove(_idxSiphons, _cmd);
    }
  }

  protected void fireCulvertChanged() {
    defaultCulvertEvent();
  }

  protected void fireCulvertRemoved() {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dtelemacSiponListener) listener_.get(i)).culvertRemoved();
      }
    }
  }

  protected void fireCulvertAdded() {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dtelemacSiponListener) listener_.get(i)).culvertAdded();
      }
    }
  }

  private void defaultCulvertEvent() {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dtelemacSiponListener) listener_.get(i)).culvertValueChanged(false);
      }
    }
  }

  /**
   * @param _idx l'indice de la source
   * @param _vals les valeurs a modifier
   * @param _cmd le receveur de commande
   */
  public void modifyVecteur(final int _idx, final String[] _vals, final CtuluCommandContainer _cmd) {
    if (_vals.length != vectEnt_.size()) {
      FuLog.error("taille incorrect");
    }
    final CtuluCommandComposite cmd = _cmd == null ? null : new CtuluCommandComposite();
    for (int i = vectEnt_.size() - 1; i >= 0; i--) {
      setValueAndUpdateDico(_idx, _vals[i], (DicoEntite.Vecteur) vectEnt_.get(i), cmd);
    }
    if (_cmd != null && cmd != null && cmd.getNbCmd() > 0) {
      _cmd.addCmd(cmd);
    }
  }

  /**
   * @param _l le listener a enlever
   * @return true si enleve
   */
  public boolean removeListener(final H2dtelemacSiponListener _l) {
    if (listener_ != null) {
      return listener_.remove(_l);
    }
    return false;
  }

  /**
   * @param _idxSource les indices des sources a tester
   * @return le tableau des siphons utilisant ces sources
   */
  public int[] getSiphonUsingSource(final int[] _idxSource) {
    // pas de siphons ou parametres incorrect on renvoie tableau vide
    if (siphons_ == null || (_idxSource == null) || (_idxSource.length == 0)) {
      return new int[0];
    }
    final TIntArrayList r = new TIntArrayList();
    for (int i = siphons_.getSize() - 1; i >= 0; i--) {
      if (getSiphon(i).useOneSource(_idxSource)) {
        r.add(i);
      }
    }
    return r.toNativeArray();
  }

  /**
   * @param _idxSource les indices des sources a tester
   * @return le tableau indices siphons utilisateur->indices de l'extremite concerne. Vaut trois si les deux.
   */
  public TIntIntHashMap getSiphonUsingSourceAndEnds(final int[] _idxSource) {
    // pas de siphons ou parametres incorrect on renvoie tableau vide
    if (siphons_ == null || (_idxSource == null) || (_idxSource.length == 0)) {
      return new TIntIntHashMap();
    }
    final TIntIntHashMap r = new TIntIntHashMap();
    for (int i = siphons_.getSize() - 1; i >= 0; i--) {
      final int result = getSiphon(i).useOneSourceAndWhichEnd(_idxSource);
      if (result > 0) {
        r.put(i, result);
      }
    }
    return r;
  }

  /**
   * @param _idxSource les indices des sources a tester
   * @return le tableau des siphons utilisant ces sources
   */
  public boolean isSiphonUsingSource(final int[] _idxSource) {
    // pas de siphons ou parametres incorrect on renvoie tableau vide
    if (siphons_ == null || (_idxSource == null) || (_idxSource.length == 0)) {
      return false;
    }
    for (int i = siphons_.getSize() - 1; i >= 0; i--) {
      if (getSiphon(i).useOneSource(_idxSource)) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param _idxSource les indices des sources a tester
   * @return le tableau des siphons utilisant ces sources
   */
  public boolean isSiphonUsingSource(final int _idxSource) {
    // pas de siphons ou parametres incorrect on renvoie tableau vide
    if (siphons_ == null || (_idxSource < 0)) {
      return false;
    }
    for (int i = siphons_.getSize() - 1; i >= 0; i--) {
      if (getSiphon(i).useSource(_idxSource)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void nbTracerChanged(CtuluCommandContainer cmp) {
    CtuluCommandComposite composite = new CtuluCommandCompositeInverse();
    cmp.addCmd(createUpdateValideStatesCommand());
    majFromDico(tracerValuesOnSources, composite);
    if (cmp != null) {
      cmp.addCmd(composite.getSimplify());
    }
  }

  private CtuluCommand createUpdateValideStatesCommand() {
    return new CtuluCommand() {
      @Override
      public void undo() {
        updateValidationStates();
      }

      @Override
      public void redo() {
        updateValidationStates();
      }
    };
  }

  /**
   * @param _idxSource les indices des sources a tester
   * @return le tableau des siphons utilisant ces sources
   */
  public int getSiphonUsingSource(final int _idxSource) {
    // pas de siphons ou parametres incorrect on renvoie tableau vide
    if (siphons_ == null || (_idxSource < 0)) {
      return -1;
    }
    for (int i = siphons_.getSize() - 1; i >= 0; i--) {
      if (getSiphon(i).useSource(_idxSource)) {
        return i;
      }
    }
    return -1;
  }

  /**
   * @param _idxCulvert les indices des siphons
   * @return les indices des sources utilisees par les siphons
   */
  public int[] getSrcUsedByCulvert(final int[] _idxCulvert) {
    if (siphons_ == null || (_idxCulvert == null) || (_idxCulvert.length == 0)) {
      return new int[0];
    }
    // normalement une arraylist suffit pour savoir quels sources sont utilisees
    // au cas telemac autorise plusieurs siphons sur une source
    final TIntHashSet r = new TIntHashSet();
    for (int i = _idxCulvert.length - 1; i >= 0; i--) {
      getSiphon(_idxCulvert[i]).fillWithUsedSrc(r);
    }

    return r.toArray();
  }

  /**
   * @param _idx les indices des sources a enlever
   * @param _cmd la commande
   */
  public void removeSource(final int[] _idx, final CtuluCommandContainer _cmd) {
    final int[] siphons = getSiphonUsingSource(_idx);
    // des siphons utilisent les sources : il faut les enlever et refaire la numerotation
    if (siphons != null && siphons.length > 0) {
      // a la fin de la commande il y aura une renumerotation
      final CtuluCommandCompositeInverse cmd = new CtuluCommandCompositeInverse() {
        @Override
        protected boolean isActionToDoAfterUndoOrRedo() {
          return true;
        }

        @Override
        protected void actionToDoAfterUndoOrRedo() {
          renumeroteSiphons();
        }
      };
      // Etape 1 on enleve les sources
      sources_.remove(_idx, cmd);
      // Etape 2 on enleve les siphons concernes
      siphons_.remove(siphons, cmd);
      // Etape 3 on renumerote
      renumeroteSiphons();
      if (_cmd != null) {
        _cmd.addCmd(cmd);
      }
    } else {
      sources_.remove(_idx, _cmd);
    }
  }

  protected void renumeroteSiphons() {
    if (siphons_ != null) {
      final TObjectIntHashMap srcIdx = new TObjectIntHashMap(sources_.getSize());
      for (int i = sources_.getSize() - 1; i >= 0; i--) {
        srcIdx.put(getSource(i), i);
      }
      for (int i = siphons_.getSize() - 1; i >= 0; i--) {
        getSiphon(i).updateNumerotation(srcIdx);
      }
    }
  }

  public void setDebitDefaultValue(final String _v, final CtuluCommandContainer _cmd) {
    setDefautValue(qEnt_, _v, _cmd);
  }

  /**
   * @param _vitesse si le traceur doivent etre definies
   * @param _traceurValue la valeur du traceur
   * @param _vx la valeur de vx
   * @param _vy la valeur de vy
   * @param _cmd le receveur de commandes
   */
  public void setSourceDefined(boolean _vitesse, final String _vx, final String _vy, final CtuluCommandContainer _cmd) {
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    boolean isTraceur = isTracerUsed();
    if (_vitesse != isVitessesSet_) {
      setVitesseSetFirst(_vitesse, cmp);
    }
    setDefautValue(vxEnt_, _vx, cmp);
    setDefautValue(vyEnt_, _vy, cmp);
    if (getNbSource() == 0) {
      if (_cmd != null) {
        _cmd.addCmd(cmp.getSimplify());
      }
      return;
    }

    if (_vitesse && ((!isValueSet(vxEnt_)) || (!isValueSet(vyEnt_)))) {
      double vx = 0;
      double vy = 0;
      if (_vx != null) {
        vx = Double.parseDouble(_vx);
      }
      if (_vy != null) {
        vy = Double.parseDouble(_vy);
      }
      for (int sip = getNbSource() - 1; sip >= 0; sip--) {
        setValue(sip, vxEnt_, vx);
      }
      for (int sip = getNbSource() - 1; sip >= 0; sip--) {
        setValue(sip, vyEnt_, vy);
      }
      vxEnt_.setRequired(true);
      vyEnt_.setRequired(true);
      dico_.setValue(vxEnt_, computeValueForDico(vxEnt_), cmp);
      dico_.setValue(vyEnt_, computeValueForDico(vyEnt_), cmp);
    } else if (!_vitesse && ((isValueSet(vxEnt_)) || (isValueSet(vyEnt_)))) {
      vxEnt_.setRequired(false);
      vyEnt_.setRequired(false);
      dico_.removeValue(vxEnt_, cmp);
      dico_.removeValue(vyEnt_, cmp);
    }

    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
  }

  public int getNbTraceurs() {
    return tracerManager.getNbTraceurs();
  }

  public H2dTelemacTracerMng getTracerManager() {
    return tracerManager;
  }
}
