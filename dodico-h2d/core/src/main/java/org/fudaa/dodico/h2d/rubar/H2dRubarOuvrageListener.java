/*
 * @creation 3 janv. 2005
 * @modification $Date: 2006-04-19 13:19:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarOuvrageListener.java,v 1.3 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dRubarOuvrageListener {

  /**
   * Envoye si un ouvrage a ete ajoute.
   *
   * @param _mng le manager concerne
   */
  void ouvrageAdded(H2dRubarOuvrageMng _mng);

  /**
   * Envoye si un ouvrage a ete enleve.
   *
   * @param _mng le manager concerne
   */
  void ouvrageRemoved(H2dRubarOuvrageMng _mng);

  /**
   * Envoye si un ouvrage elementaire a ete modifie.
   *
   * @param _mng le manager concerne
   * @param _ouv l'ouvrage concerne
   * @param _i l'ouvrage elementaire concerne
   */
  void ouvrageElementaireChanged(H2dRubarOuvrageMng _mng, H2dRubarOuvrage _ouv, H2dRubarOuvrageElementaireInterface _i);

  /**
   * Envoye si un ouvrage a ete modifie.
   *
   * @param _mng le manager concerne
   * @param _ouv l'ouvrage concerne
   * @param _ouvrageElementaireAddedOrRemoved true si le changement concerne l'ajout ou la suppression d'un ouvrage
   *          elementaire
   */
  void ouvrageChanged(H2dRubarOuvrageMng _mng, H2dRubarOuvrage _ouv, boolean _ouvrageElementaireAddedOrRemoved);

}