/*
 *  @creation     25 mai 2005
 *  @modification $Date: 2007-06-05 08:59:12 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import com.memoire.bu.BuValueValidator;
import java.util.Collection;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.ctulu.interpolation.InterpolationSupportValuesMultiI;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: H2dVariableProviderInterface.java,v 1.8 2007-06-05 08:59:12 deniger Exp $
 */
public interface H2dVariableProviderInterface extends InterpolationSupportValuesMultiI{
  /**
   *
   * @return -1 if transport block should be displayed. Otherwise the sie of transport blocks
   */
   int getVariablesBlockLength();

  /**
   * @return les variables a modifier
   */
  H2dVariableType[] getVarToModify();
  

  boolean containsVarToModify();

  /**
   * @return la liste des variables qui peuvent etre utilisee : isosurface, expression
   */
  Collection getUsableVariables();

  /**
   * @param _t la variable
   * @return le modele contenant les valeur associee
   */
  CtuluCollectionDoubleEdit getModifiableModel(H2dVariableType _t);

  CtuluCollectionDouble getViewedModel(H2dVariableType _t);

  /**
   * @param _t la variable
   * @return le validateur
   */
  BuValueValidator getValidator(H2dVariableType _t);

  CtuluNumberFormatI getFormater(H2dVariableType _t);

  /**
   * @return le maillage support
   */
  EfGridInterface getGrid();

  /**
   * @return true si les variables sont d�finies en chaque element. sinon varialbes ponctuelles.
   */
  boolean isElementVar();

}
