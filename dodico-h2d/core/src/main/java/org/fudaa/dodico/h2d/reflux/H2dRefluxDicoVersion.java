package org.fudaa.dodico.h2d.reflux;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.dodico.dico.DicoCasFileFormatVersionAbstract;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.ef.FileFormatGridVersion;
import org.fudaa.dodico.h2d.type.H2dProjetType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * Derniere version.
 * 
 * @author Fred Deniger
 * @version $Id: H2dRefluxDicoVersion.java,v 1.2 2007-06-29 15:10:28 deniger Exp $
 */
public abstract class H2dRefluxDicoVersion extends DicoCasFileFormatVersionAbstract implements FileFormatGridVersion {

  /**
   * @param _version id version.
   */
  public H2dRefluxDicoVersion(final FileFormat _fmt, final String _version, boolean _contientSrc) {
    super(_fmt, new H2dRefluxDicoModel(_version, _contientSrc));
  }

  /**
   * @return le modele des mot-cles.
   */
  public H2dRefluxDicoModel getRefluxModel() {
    return (H2dRefluxDicoModel) super.getDico();
  }

  /**
   * @return l'extension des fichiers cl transitoire utilises
   */
  public abstract String getClTransitoireFileExtension();

  public final DicoEntite getSollicitationRadiation() {
    return getEntiteFor(new String[] { "Contraintes de radiation" });
  }

  public final DicoEntite getSollicitationVent() {
    return getEntiteFor(new String[] { "Forces du vent" });
  }

  /**
   * @return les mot-cl�s contribution
   */
  public final DicoEntite[] getContributionEntite() {
    return getEntitesForHeading(new String[] { "Coefficients contribution", "Contribution coefficients" });
  }

  /**
   * @return les mot-cl�s impression
   */
  public DicoEntite[] getPrintEntite() {
    return getEntitesForHeading(new String[] { "Impression", "Print" });
  }

  /**
   * @return "pnv"
   */
  public String getPnTransitoireFileExtension() {
    return "pnv";
  }

  /**
   * @return "siv"
   */
  public String getSolutionInitFileExtension() {
    return "siv";
  }

  /**
   * @return "sov"
   */
  public String getSolutionFileExtension() {
    return "sov";
  }

  /**
   * @return "sfv"
   */
  public String getSolutionFinaleExtension() {
    return "sfv";
  }

  /**
   * @param _f le tableau a parcourir
   * @return le fichier contant l'extension clv
   */
  public String getCLTransitoireFichier(final String[] _f) {
    return CtuluLibString.findStringEndWith(_f, getClTransitoireFileExtension());
  }

  /**
   * @param _f le tableau a parcourir
   * @return le fichier contant l'extension pnv
   */
  public String getPnTransitoireFichier(final String[] _f) {
    return CtuluLibString.findStringEndWith(_f, getPnTransitoireFileExtension());
  }

  /**
   * @param _f le tableau de recherche
   * @return l'element du tableau contenant la bonne extension
   */
  public String getSolutionInitiales(final String[] _f) {
    return CtuluLibString.findStringEndWith(_f, getSolutionInitFileExtension());
  }

  public String getVent(final String[] _f, final H2dProjetType _proj) {
    return CtuluLibString.findStringEndWith(_f, getFileExtensionForVent(_proj));
  }

  public String getRadiation(final String[] _f, final H2dProjetType _proj) {
    return CtuluLibString.findStringEndWith(_f, getFileExtensionForRadiation(_proj));
  }

  public String getForceFileExtension(final H2dVariableType _v, final H2dProjetType _proj) {
    if (_v == H2dRefluxNodalPropertiesMng.getVentVar()) {
      return getFileExtensionForVent(_proj);
    } else if (_v == H2dRefluxNodalPropertiesMng.getRadiationVar()) {
      return getFileExtensionForRadiation(_proj);
    }
    return null;
  }

  public String getFileExtensionForVent(final H2dProjetType _proj) {
    return "vtv";
  }

  public String getFileExtensionForRadiation(final H2dProjetType _proj) {
    return "crv";
  }

  /**
   * @return nb de coefficient de contribution.
   */
  public abstract int getCoefContributionNb();

  @Override
  public final DicoEntite getFichierPrincipalEntite() {
    return null;
  }

  /**
   * @return null
   */
  @Override
  public final DicoEntite getTitreEntite() {
    return null;
  }

  public static int getDoubleColSize() {
    return 12;
  }

}