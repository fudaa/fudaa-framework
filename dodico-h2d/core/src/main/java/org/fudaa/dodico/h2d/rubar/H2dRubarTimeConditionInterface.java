/*
 * @creation 24 juin 2004
 * @modification $Date: 2007-01-19 13:07:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.mesure.EvolutionReguliere;

/**
 * @author Fred Deniger
 */
public interface H2dRubarTimeConditionInterface {

  EvolutionReguliere getHEvol();

  EvolutionReguliere getQnEvol();

  EvolutionReguliere getQtEvol();

  /**
   *
   * @return the number of concentration blocks.
   */
  int getNbConcentrationBlock();


  EvolutionReguliere getConcentrationEvolution(int blockIdx);

  EvolutionReguliere getDiametreEvolution(int blockIdx);

  EvolutionReguliere getEtendueEvolution(int blockIdx);

  /**
   * @return true si transport
   */
  boolean isTrans();


}
