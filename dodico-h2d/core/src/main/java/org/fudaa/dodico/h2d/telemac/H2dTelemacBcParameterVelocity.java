/*
 * @creation 24 mai 2004
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParamsInterface;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

final class H2dTelemacBcParameterVelocity extends H2dTelemacBcParameter {

  H2dTelemacBcParameterVelocity(final H2dTelemacBcManager _mng, final DicoEntite.Vecteur _ent, final H2dBoundaryType[] _bords) {
    super(_mng, _ent, _bords);
  }

  @Override
  public String getValueFromBord(final H2dTelemacBoundary _b, int valueIdx) {
    return _b.getVelocity();
  }

  @Override
  public boolean isKeywordImposed() {
    return false;
  }

  @Override
  protected boolean setValueInBord(final H2dTelemacBoundary _b, final String _s, int valueIdx) {
    return _b.setVelocity(_s);
  }

  @Override
  public H2dVariableType getVariable() {
    return H2dVariableType.VITESSE;
  }

  @Override
  public H2dVariableType[] getVariablesForPoint(final H2dTelemacDicoFileFormatVersion _v, final H2dTelemacBoundary _t,
      final DicoParamsInterface _dico) {
    if ((isParameterForType(_t.getType())) && (!isValueSetInDico(_dico))) {
      return new H2dVariableType[] {
          H2dVariableType.VITESSE_U, H2dVariableType.VITESSE_V };
    }
    return null;
  }
}