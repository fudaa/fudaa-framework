package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

public class H2dRubarSedimentCouche {
  private double epaisseur;
  private double diametre;
  private double etendue;
  private double contrainte;

  public double getEpaisseur() {
    return epaisseur;
  }

  public void setEpaisseur(double epaisseur) {
    this.epaisseur = epaisseur;
  }

  public double getDiametre() {
    return diametre;
  }

  public void setDiametre(double diametre) {
    this.diametre = diametre;
  }

  public double getValue(H2dVariableType type) {
    if (type == H2dVariableTransType.ETENDUE) {
      return etendue;
    }
    if (type == H2dVariableTransType.DIAMETRE) {
      return diametre;
    }
    if (type == H2dVariableTransType.EPAISSEUR) {
      return epaisseur;
    }
    if (type == H2dVariableTransType.CONTRAINTE) {
      return contrainte;
    }
    throw new IllegalAccessError();
  }

  public void setValue(H2dVariableType type, double value) {
    if (type == H2dVariableTransType.ETENDUE) {
      etendue = value;
    }
    if (type == H2dVariableTransType.DIAMETRE) {
      diametre = value;
    }
    if (type == H2dVariableTransType.EPAISSEUR) {
      epaisseur = value;
    }
    if (type == H2dVariableTransType.CONTRAINTE) {
      contrainte = value;
    }
  }

  public double getEtendue() {
    return etendue;
  }

  public void setEtendue(double etendue) {
    this.etendue = etendue;
  }

  public double getContrainte() {
    return contrainte;
  }

  public void setContrainte(double contrainte) {
    this.contrainte = contrainte;
  }
}
