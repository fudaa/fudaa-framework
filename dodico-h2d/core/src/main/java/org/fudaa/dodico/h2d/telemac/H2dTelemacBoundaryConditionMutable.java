/*
 * @creation 25 juin 2003
 * @modification $Date: 2006-09-19 14:43:24 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

/**
 * @author deniger
 * @version $Id: H2dTelemacBoundaryConditionMutable.java,v 1.8 2006-09-19 14:43:24 deniger Exp $
 */
public class H2dTelemacBoundaryConditionMutable extends H2dTelemacBoundaryCondition {

  public H2dTelemacBoundaryConditionMutable() {
    super();
  }

  @Override
  public boolean setFriction(final double _s){
    return super.setFriction(_s);
  }

  /**
   * @param _s nouvelle valeur du traceur
   */
  public void setTraceur(final double _s){
    traceur_ = _s;
  }

  @Override
  public boolean setTraceurCoefA(final double _s){
    return super.setTraceurCoefA(_s);
  }

  @Override
  public boolean setTraceurCoefB(final double _s){
    return super.setTraceurCoefB(_s);
  }

  @Override
  public boolean setH(final double _s){
    return super.setH(_s);
  }

  @Override
  public void setIndexPt(final int _s){
    indexPt_ = _s;
  }

  @Override
  public boolean setU(final double _s){
    return super.setU(_s);
  }

  @Override
  public boolean setV(final double _s){
    return super.setV(_s);
  }
}