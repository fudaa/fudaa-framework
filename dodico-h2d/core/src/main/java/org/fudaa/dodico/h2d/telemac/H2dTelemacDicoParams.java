/*
 * @creation 8 oct. 2003
 * @modification $Date: 2006-09-19 14:43:24 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import java.util.Map;
import org.fudaa.dodico.dico.DicoCasInterface;
import org.fudaa.dodico.dico.DicoParams;

/**
 * @author deniger
 * @version $Id: H2dTelemacDicoParams.java,v 1.10 2006-09-19 14:43:24 deniger Exp $
 */
public class H2dTelemacDicoParams extends DicoParams {

  /**
   * @param _i l'interface contenant les sources
   * @param _dico le format utilise
   */
  public H2dTelemacDicoParams(final DicoCasInterface _i, final H2dTelemacDicoFileFormatVersion _dico) {
    super(_i, _dico);
  }

  /**
   * @param _keysValue la table des cles-valeurs
   * @param _keysCommentaire la table des cles-commentaires
   * @param _dico le format
   */
  public H2dTelemacDicoParams(final Map _keysValue, final Map _keysCommentaire, final H2dTelemacDicoFileFormatVersion _dico) {
    super(_keysValue, _keysCommentaire, _dico);
  }

  /**
   * @return le format utilise
   */
  public H2dTelemacDicoFileFormatVersion getTelemacDicoFileFormatVersion() {
    return (H2dTelemacDicoFileFormatVersion) getDicoFileFormatVersion();
  }

  /**
   * @return identifiant
   */
  public static final String getArtemisId() {
    return "artemis";
  }

  /**
   * @return identifiant
   */
  public static final String getEstel2dId() {
    return "estel2d";
  }

  /**
   * @return identifiant
   */
  public static final String getEstel3dId() {
    return "estel3d";
  }

  /**
   * @return identifiant
   */
  public static final String getPostel3dId() {
    return "postel3d";
  }

  /**
   * @return identifiant
   */
  public static final String getSisypheId() {
    return "sisyphe";
  }

  /**
   * @return identifiant
   */
  public static final String getStbtelId() {
    return "stbtel";
  }

  /**
   * @return identifiant
   */
  public static final String getSubief2dId() {
    return "subief2d";
  }

  /**
   * @return identifiant
   */
  public static final String getSubief3dId() {
    return "subief3d";
  }

  /**
   * @return identifiant
   */
  public static final String getTelemac3dId() {
    return "telemac3d";
  }

  /**
   * @return identifiant
   */
  public static final String getTomawacId() {
    return "tomawac";
  }
  public static final String getWaqtelId() {
    return "waqtel";
  }

}