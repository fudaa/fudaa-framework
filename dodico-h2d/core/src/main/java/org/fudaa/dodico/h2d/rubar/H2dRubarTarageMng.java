/*
 * @creation 1 sept. 2004
 *
 * @modification $Date: 2007-03-27 16:09:55 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import com.memoire.fu.FuLog;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntObjectHashMap;
import gnu.trove.TIntObjectIterator;
import gnu.trove.TIntObjectProcedure;
import org.fudaa.ctulu.*;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dRubarBcTypeList;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryTarageGroupType;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryType;
import org.fudaa.dodico.mesure.EvolutionListener;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarTarageMng.java,v 1.16 2007-03-27 16:09:55 deniger Exp $
 */
public final class H2dRubarTarageMng implements EvolutionListener/* implements EvolutionListenerDispatcher */ {
  static int idx;

  protected static H2dRubarTarageMng createTarageMng(final TIntObjectHashMap _mInit, final CtuluAnalyze _analyse) {
    final H2dRubarTarageMng r = new H2dRubarTarageMng();
    r.initWith(_mInit, _analyse);
    return r;
  }

  public static String getDefaultNom() {
    return H2dResource.getS("Loi de tarage") + CtuluLibString.ESPACE + (++idx);
  }

  private final Map<H2dRubarBoundaryTarageGroupType, EvolutionReguliereAbstract> tarageGroups_ = new HashMap<H2dRubarBoundaryTarageGroupType, EvolutionReguliereAbstract>();
  TIntObjectHashMap<H2dRubarTarageContent> globalIdxTarageType_;
  Set listener_;
  Map<H2dRubarBoundaryTarageGroupType, TIntArrayList> idxByTarageGroup = new HashMap<H2dRubarBoundaryTarageGroupType, TIntArrayList>();

  private H2dRubarTarageMng() {
  }

  /**
   * @param _a
   * @param _tarageEvol
   * @param type forcement de type H2dRubarBcTypeList.TARAGE
   */
  private CtuluCommand addTarage(final int[] _a, final EvolutionReguliereAbstract _tarageEvol, final H2dRubarBoundaryType type) {
    if ((_tarageEvol == EvolutionReguliere.MIXTE) || (_tarageEvol == null) || type != H2dRubarBcTypeList.TARAGE) {
      return null;
    }
    final TIntObjectHashMap<H2dRubarTarageContent> old = new TIntObjectHashMap<H2dRubarTarageContent>(_a.length);
    for (int i = _a.length - 1; i >= 0; i--) {
      // l'ancien objet
      final H2dRubarTarageContent oldContent = globalIdxTarageType_.get(_a[i]);
      if (oldContent == null || !oldContent.isSame(_tarageEvol, type)) {
        _tarageEvol.setListener(this);
        _tarageEvol.setUsed(true);
        //si l'ancien contenu est de type tarage mais pas un groupe on descend l'utilisation
        if (oldContent != null) {
          if (!oldContent.getTarageType().isTypeTarageGlobal() && oldContent.getEvolution() != null) {
            oldContent.getEvolution().setUnUsed(true);
          }
        }
        globalIdxTarageType_.put(_a[i], new H2dRubarTarageContent(_tarageEvol, type));
        old.put(_a[i], oldContent);
      }
    }
    // des modifs
    if (old.size() > 0) {
      if (!_tarageEvol.isNomSet()) {
        _tarageEvol.setNom(getDefaultNom());
      }
      // normalement, il faut que les evolutions soient initialises avec le bon listener.
      if (_tarageEvol.getListener() != this) {
        _tarageEvol.setListener(this);
        FuLog.warning(getClass().getName() + " l: listener incorrect");
      }
      return new CtuluCommand() {
        @Override
        public void redo() {
          final TIntObjectIterator<H2dRubarTarageContent> iterator = old.iterator();
          for (int i = old.size(); i-- > 0; ) {
            iterator.advance();
            _tarageEvol.setUsed(true);
            final H2dRubarTarageContent o = globalIdxTarageType_.put(iterator.key(), new H2dRubarTarageContent(_tarageEvol, type));
            if (o != null && !o.getTarageType().isTypeTarageGlobal() && o.getEvolution() != null) {
              o.getEvolution().setUnUsed(true);
            }
          }
          fireTarageUsedChanged();
        }

        @Override
        public void undo() {
          final TIntObjectIterator<H2dRubarTarageContent> iterator = old.iterator();
          for (int i = old.size(); i-- > 0; ) {
            iterator.advance();
            // on met a jour le compteur de la courbe de tarage
            _tarageEvol.setUnUsed(true);
            // l'ancien valeur est nulle, on enleve l'indice
            if (iterator.value() == null) {
              globalIdxTarageType_.remove(iterator.key());
            }
            // sinon, on met a jour l'indice.
            else {
              final H2dRubarTarageContent e = (H2dRubarTarageContent) iterator.value();
              if (!e.getTarageType().isTypeTarageGlobal()) {
                e.getEvolution().setUsed(true);
              }
              globalIdxTarageType_.put(iterator.key(), e);
            }
          }
          fireTarageUsedChanged();
        }
      };
    }
    return null;
  }

  /**
   * @param _a
   * @param type forcement de type H2dRubarBcTypeList.TARAGE
   */
  private CtuluCommand addTarageGroupe(final int[] _a, final H2dRubarBoundaryType type) {
    if (!type.isTypeTarageGlobal()) {
      return null;
    }
    final TIntObjectHashMap<H2dRubarTarageContent> old = new TIntObjectHashMap<H2dRubarTarageContent>(_a.length);
    for (int i = _a.length - 1; i >= 0; i--) {
      // l'ancien objet
      final H2dRubarTarageContent oldContent = globalIdxTarageType_.get(_a[i]);
      if (oldContent == null || oldContent.getTarageType() != type) {
        //si l'ancien contenu est de type tarage on descend l'utilisation
        if (oldContent != null) {
          if (!oldContent.getTarageType().isTypeTarageGlobal() && oldContent.getEvolution() != null) {
            oldContent.getEvolution().setUnUsed(true);
          }
        }
        globalIdxTarageType_.put(_a[i], new H2dRubarTarageContent(tarageGroups_.get(type), type));
        old.put(_a[i], oldContent);
      }
    }
    // des modifs
    if (old.size() > 0) {
      fireTarageUsedChanged();
      return new CtuluCommand() {
        @Override
        public void redo() {
          final TIntObjectIterator<H2dRubarTarageContent> iterator = old.iterator();
          for (int i = old.size(); i-- > 0; ) {
            iterator.advance();
            final H2dRubarTarageContent o = globalIdxTarageType_.put(iterator.key(), new H2dRubarTarageContent(tarageGroups_.get(type), type));
            if (o != null && !o.getTarageType().isTypeTarageGlobal() && o.getEvolution() != null) {
              o.getEvolution().setUnUsed(true);
            }
          }
          fireTarageUsedChanged();
        }

        @Override
        public void undo() {
          final TIntObjectIterator<H2dRubarTarageContent> iterator = old.iterator();
          for (int i = old.size(); i-- > 0; ) {
            iterator.advance();
            // l'ancien valeur est nulle, on enleve l'indice
            if (iterator.value() == null) {
              globalIdxTarageType_.remove(iterator.key());
            }
            // sinon, on met a jour l'indice.
            else {
              final H2dRubarTarageContent e = (H2dRubarTarageContent) iterator.value();
              if (!e.getTarageType().isTypeTarageGlobal()) {
                e.getEvolution().setUsed(true);
              }
              globalIdxTarageType_.put(iterator.key(), e);
            }
          }
          fireTarageUsedChanged();
        }
      };
    }
    return null;
  }

  /**
   * @param _l le listener a ajouter
   */
  public void addTarageListener(final H2dRubarTarageListener _l) {
    if (listener_ == null) {
      listener_ = new HashSet(3);
    }
    listener_.add(_l);
  }

  protected void clear() {
    if ((globalIdxTarageType_ != null) && (globalIdxTarageType_.size() > 0)) {
      final H2dRubarTarageContent[] o = globalIdxTarageType_.getValues(new H2dRubarTarageContent[globalIdxTarageType_.size()]);
      for (int i = o.length - 1; i >= 0; i--) {
        o[i].getEvolution().setListener(null);
      }
      globalIdxTarageType_.clear();
    }
    if (tarageGroups_ != null && !tarageGroups_.isEmpty()) {
      Collection<EvolutionReguliereAbstract> values = tarageGroups_.values();
      for (EvolutionReguliereAbstract evolutionReguliereAbstract : values) {
        evolutionReguliereAbstract.setUnUsed(true);
        evolutionReguliereAbstract.setListener(null);
      }
      tarageGroups_.clear();
    }
    idxByTarageGroup.clear();
  }

  /**
   * @param _l le listener a tester
   * @return true si est contenu
   */
  public boolean containsTarageListener(final H2dRubarTarageListener _l) {
    return (listener_ != null) && listener_.contains(_l);
  }

  @Override
  public void evolutionChanged(final EvolutionReguliereInterface _e) {
    fireEvolutionChanged(_e);
  }

  /**
   * ignoree.
   */
  @Override
  public void evolutionUsedChanged(final EvolutionReguliereInterface _e, final int _old, final int _new, final boolean _isAdjusting) {
    fireTarageUsedChanged();
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("tarage curve " + _e.getNom() + " used new=" + _new + " old " + _old);
    }
  }

  protected void fireEvolutionChanged(final EvolutionReguliereInterface _e) {

    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2dRubarTarageListener) it.next()).tarageCourbeChanged(this, _e);
      }
    }
  }

  protected void fireTarageGroupeUsedChanged(H2dRubarBoundaryTarageGroupType groupeType) {
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2dRubarTarageListener) it.next()).TarageGroupeUsedChanged(this, groupeType);
      }
    }
  }

  public void fireTarageUsedChanged() {
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2dRubarTarageListener) it.next()).tarageUsedChanged(this);
      }
    }
  }

  public boolean isAllCurveSet(final int[] _globalIdx) {
    if ((_globalIdx != null) && (_globalIdx.length > 0)) {
      final EvolutionReguliereAbstract r = getEvol(_globalIdx[0]);
      if (r == null) {
        return false;
      }
    }
    return true;
  }

  public EvolutionReguliereAbstract getCommonEvolution(final int[] _globalIdx) {
    if ((_globalIdx != null) && (_globalIdx.length > 0)) {
      final EvolutionReguliereAbstract r = getEvol(_globalIdx[0]);
      if (r != null) {
        for (int i = _globalIdx.length - 1; i > 0; i--) {
          if (!r.equals(getEvol(_globalIdx[i]))) {
            return null;
          }
        }
        return r;
      }
    }
    return null;
  }

  /**
   * @param _idxGlobal l'indice global de l'arete
   * @return la courbe de tarage correspondante
   */
  public EvolutionReguliereAbstract getEvol(final int _idxGlobal) {
    H2dRubarTarageContent h2dRubarTarageContent = globalIdxTarageType_.get(_idxGlobal);
    if (h2dRubarTarageContent != null && h2dRubarTarageContent.getTarageType().isTypeTarageGlobal()) {
      return tarageGroups_.get(h2dRubarTarageContent.getTarageType());
    }
    return h2dRubarTarageContent == null ? null : h2dRubarTarageContent.getEvolution();
  }

  public Set getEvolutions() {
    if (globalIdxTarageType_ == null) {
      return Collections.EMPTY_SET;
    }
    HashSet<EvolutionReguliereAbstract> evols = new HashSet<EvolutionReguliereAbstract>();
    H2dRubarTarageContent[] values = globalIdxTarageType_.getValues(new H2dRubarTarageContent[globalIdxTarageType_.size()]);
    for (H2dRubarTarageContent h2dRubarTarageContent : values) {
      if (h2dRubarTarageContent != null && h2dRubarTarageContent.getEvolution() != null) {
        evols.add(h2dRubarTarageContent.getEvolution());
      }
    }
    return evols;
  }

  /**
   * @param _t le type recherche
   * @return les conditions globales pour ce type de bord.
   */
  public EvolutionReguliereAbstract getGroupTimeCondition(final H2dRubarBoundaryTarageGroupType _t) {
    return (tarageGroups_ == null) ? null : (EvolutionReguliereAbstract) tarageGroups_.get(_t);
  }

  public TIntObjectHashMap getMapForWrite() {
    final TIntObjectHashMap r = globalIdxTarageType_ == null ? new TIntObjectHashMap() : new TIntObjectHashMap(globalIdxTarageType_);
    if (globalIdxTarageType_ != null) {
      final TIntObjectIterator<H2dRubarTarageContent> iterator = globalIdxTarageType_.iterator();
      for (int i = globalIdxTarageType_.size(); i-- > 0; ) {
        iterator.advance();
        r.put(iterator.key(), iterator.value().getEvolution());
      }
    }
    return r;
  }

  public int getNbEvolution() {
    return globalIdxTarageType_ == null ? 0 : globalIdxTarageType_.size();
  }

  public boolean isUsed(H2dRubarBoundaryType type) {
    if (type == null || globalIdxTarageType_ == null) {
      return false;
    }
    if (!type.isTypeTarageGlobal() && type != H2dRubarBcTypeList.TARAGE) {
      return false;
    }
    TIntObjectIterator<H2dRubarTarageContent> iterator = globalIdxTarageType_.iterator();
    for (int i = globalIdxTarageType_.size(); i-- > 0; ) {
      iterator.advance();
      if (iterator.value().getTarageType() == type) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param _r l'evolution a adapter pour ce mng: modifie le listener.
   */
  public void initEvolutionForThis(final EvolutionReguliereAbstract _r) {
    _r.setListener(this);
    if (!_r.isNomSet()) {
      _r.setNom(getDefaultNom());
    }
  }

  protected void initWith(final TIntObjectHashMap<H2dRubarTarageContent> _mInit, final CtuluAnalyze _analyse) {
    clear();
    if (_mInit == null) {
      if (globalIdxTarageType_ == null) {
        globalIdxTarageType_ = new TIntObjectHashMap();
      }
      return;
    }
    //    final TIntObjectHashMap m = new TIntObjectHashMap(_mInit);
    globalIdxTarageType_ = new TIntObjectHashMap<H2dRubarTarageContent>(_mInit);
    if (_mInit.size() > 0) {
      final EvolutionReguliere defaultEvolution = new EvolutionReguliere();
      defaultEvolution.setNom(H2dResource.getS("defaut"));
      defaultEvolution.add(1, 1);
      defaultEvolution.setListener(this);
      final Set<String> groupWithNonConstantValue = new HashSet<String>();
      final Set<EvolutionReguliereAbstract> usedEvolutionWithoutGroupEvolutions = new HashSet<EvolutionReguliereAbstract>();
      TIntObjectIterator<H2dRubarTarageContent> iterator = _mInit.iterator();
      for (int i = _mInit.size(); i-- > 0; ) {
        iterator.advance();
        if (!iterator.value().getTarageType().isTypeTarageGlobal() && iterator.value().getEvolution() != null) {
          usedEvolutionWithoutGroupEvolutions.add(iterator.value().getEvolution());
        }
      }
      final Map doublons = EvolutionReguliere.getDoublons(usedEvolutionWithoutGroupEvolutions
          .toArray(new EvolutionReguliereInterface[usedEvolutionWithoutGroupEvolutions.size()]));
      final TIntArrayList idxNull = new TIntArrayList();
      final TIntObjectProcedure<H2dRubarTarageContent> proc = new TIntObjectProcedure<H2dRubarTarageContent>() {
        @Override
        public boolean execute(int _a, H2dRubarTarageContent _b) {
          if (_b.getTarageType().isTypeTarageGlobal()) {
            H2dRubarBoundaryTarageGroupType groupType = (H2dRubarBoundaryTarageGroupType) _b.getTarageType();
            String groupName = groupType.getShortName();
            EvolutionReguliereAbstract evol = _b.getEvolution();
            if (evol == null) {
              idxNull.add(_a);
            } else {
              evol.setNom(groupName);
            }
            EvolutionReguliereAbstract alreadySet = tarageGroups_.get(_b.getTarageType());
            if (alreadySet != null) {
              if (evol != null && !alreadySet.isEquivalentTo(evol)) {
                groupWithNonConstantValue.add(groupName);
              }
              if (alreadySet == defaultEvolution && evol != null) {
                defaultEvolution.setUnUsed(true);
                evol.setUsed(true);
                tarageGroups_.put((H2dRubarBoundaryTarageGroupType) _b.getTarageType(), evol);
              }
            } else {
              if (evol == null) {
                defaultEvolution.setUsed(true);
              } else {
                evol.setUsed(true);
              }
              tarageGroups_.put((H2dRubarBoundaryTarageGroupType) _b.getTarageType(), evol == null ? defaultEvolution : evol);
            }
            globalIdxTarageType_.put(_a, new H2dRubarTarageContent(tarageGroups_.get(groupType), groupType));
          } else {
            if (_b.getEvolution() == null) {
              idxNull.add(_a);
              globalIdxTarageType_.put(_a, new H2dRubarTarageContent(defaultEvolution, _b.getTarageType()));
              defaultEvolution.setUsed(true);
            } else {
              EvolutionReguliereAbstract e = (EvolutionReguliereAbstract) _b.getEvolution();
              if (doublons.containsKey(_b)) {
                e = (EvolutionReguliereAbstract) doublons.get(_b);
                globalIdxTarageType_.put(_a, new H2dRubarTarageContent(e, _b.getTarageType()));
              } else {
                globalIdxTarageType_.put(_a, new H2dRubarTarageContent(e, _b.getTarageType()));
                e.setListener(H2dRubarTarageMng.this);
                if (!e.isNomSet()) {
                  e.setNom(getDefaultNom());
                }
              }
              e.setUsed(true);
            }
          }
          return true;
        }
      };
      updateIndiceFromGroups();
      _mInit.forEachEntry(proc);
      if (idxNull.size() > 0) {
        _analyse.addError(H2dResource.getS("Les lois de tarages n'�taient pas d�finies pour les indices {0}",
            CtuluLibString.arrayToString(idxNull.toNativeArray())), -1);
      }
      if (groupWithNonConstantValue.size() > 0) {
        _analyse.addError(
            H2dResource.getS("Les lois de tarages n'�taient pas constantes pour les groupes {0}",
                CtuluLibString.arrayToString(groupWithNonConstantValue.toArray(new String[groupWithNonConstantValue.size()]))), -1);
      }
    }
  }

  private void updateIndiceFromGroups() {
    //on r�initialise les groupes par indice
    H2dRubarTarageContent[] values = globalIdxTarageType_.getValues(new H2dRubarTarageContent[globalIdxTarageType_.size()]);
    for (H2dRubarTarageContent h2dRubarTarageContent : values) {
      if (h2dRubarTarageContent.getTarageType().isTypeTarageGlobal()) {
        h2dRubarTarageContent.setEvolution(tarageGroups_.get(h2dRubarTarageContent.getTarageType()));
      }
    }
  }

  /**
   * @param _t le type de bord a tester
   * @return true si le groupe est defini
   */
  public boolean isTarageGroupDefined(final H2dRubarBoundaryTarageGroupType _t) {
    return (tarageGroups_ != null) && (tarageGroups_.containsKey(_t));
  }

  protected CtuluCommand modifyTarage(final int[] _a, final H2dRubarBoundaryType _t, final EvolutionReguliereAbstract _tarageEvolution) {
    if ((_a == null) || (_a.length == 0)) {
      return null;
    }
    if (_t == H2dRubarBcTypeList.TARAGE) {
      return addTarage(_a, _tarageEvolution, _t);
    } else if (_t.isTypeTarageGlobal()) {
      return addTarageGroupe(_a, _t);
    } else {
      return removeTarage(_a);
    }
  }

  private CtuluCommand removeTarage(final int[] _a) {
    final TIntObjectHashMap<H2dRubarTarageContent> old = new TIntObjectHashMap<H2dRubarTarageContent>(_a.length);
    int t;
    for (int i = _a.length - 1; i >= 0; i--) {
      t = _a[i];
      if (globalIdxTarageType_.contains(t)) {
        final H2dRubarTarageContent e = (H2dRubarTarageContent) globalIdxTarageType_.remove(t);
        if (!e.getTarageType().isTypeTarageGlobal()) {
          e.getEvolution().setUnUsed(true);
        }
        old.put(t, e);
      }
    }
    if (old.size() == 0) {
      return null;
    }
    fireTarageUsedChanged();
    return new CtuluCommand() {
      @Override
      public void redo() {
        final TIntObjectIterator<H2dRubarTarageContent> iterator = old.iterator();
        for (int i = old.size(); i-- > 0; ) {
          iterator.advance();
          final H2dRubarTarageContent e = (H2dRubarTarageContent) globalIdxTarageType_.remove(iterator.key());
          if (!e.getTarageType().isTypeTarageGlobal()) {
            e.getEvolution().setUnUsed(true);
          }
        }

        fireTarageUsedChanged();
      }

      @Override
      public void undo() {
        final TIntObjectIterator<H2dRubarTarageContent> iterator = old.iterator();
        for (int i = old.size(); i-- > 0; ) {
          iterator.advance();
          final H2dRubarTarageContent e = iterator.value();
          if (e.getTarageType().isTypeTarageGlobal()) {
            e.setEvolution(tarageGroups_.get(e.getTarageType()));
          } else {
            e.getEvolution().setUsed(true);
          }
          globalIdxTarageType_.put(iterator.key(), e);
        }
        fireTarageUsedChanged();
      }
    };
  }

  /**
   * @param _l le listener a enlever
   */
  public void removeTarageListener(final H2dRubarTarageListener _l) {
    if (listener_ != null) {
      listener_.remove(_l);
    }
  }

  /**
   * Permet d'initialiser (si pas deja fait) le groupe.
   */
  public CtuluCommand setDefined(final H2dRubarBoundaryTarageGroupType _t, final EvolutionReguliereAbstract evolution) {
    if (isTarageGroupDefined(_t)) {
      return null;
    }
    final CtuluCommandCompositeInverse inv = new CtuluCommandCompositeInverse();
    evolution.setUsed(true);
    evolution.setListener(this);
    tarageGroups_.put(_t, evolution);
    fireTarageGroupeUsedChanged(_t);
    inv.addCmd(new CtuluCommand() {
      @Override
      public void redo() {
        evolution.setUsed(true);
        tarageGroups_.put(_t, evolution);
        fireTarageGroupeUsedChanged(_t);
      }

      @Override
      public void undo() {
        tarageGroups_.remove(_t).setUnUsed(true);
        fireTarageGroupeUsedChanged(_t);
      }
    });
    return inv.getSimplify();
  }

  /**
   * @param _t le groupe a modifier
   * @param _evolInit l'evolution a mettre
   * @param _type le type concerne qn,qt ou h
   * @return la commande ou null si aucune modif
   */
  public CtuluCommand setEvolution(final H2dRubarBoundaryTarageGroupType _t, final EvolutionReguliereAbstract _evolInit) {
    if (!isTarageGroupDefined(_t)) {
      return null;
    }
    final EvolutionReguliereAbstract old = getGroupTimeCondition(_t);
    if (old == _evolInit) {
      return null;
    }
    final CtuluCommandCompositeInverse cmd = new CtuluCommandCompositeInverse();
    if (_evolInit != null) {
      _evolInit.setUsed(true);
      _evolInit.setListener(this);
    }
    if (old != null) {
      old.setUnUsed(true);
    }
    tarageGroups_.put(_t, _evolInit);
    updateIndiceFromGroups();
    fireTarageGroupeUsedChanged(_t);
    cmd.addCmd(new CtuluCommand() {
      @Override
      public void redo() {
        final EvolutionReguliereAbstract current = getGroupTimeCondition(_t);
        if (current != null) {
          current.setUnUsed(true);
        }
        if (_evolInit != null) {
          _evolInit.setUsed(true);
        }
        tarageGroups_.put(_t, _evolInit);
        updateIndiceFromGroups();
        fireTarageGroupeUsedChanged(_t);
      }

      @Override
      public void undo() {
        final EvolutionReguliereAbstract current = getGroupTimeCondition(_t);
        if (current != null) {
          current.setUnUsed(true);
        }
        if (old != null) {
          old.setUsed(true);
        }
        tarageGroups_.put(_t, old);
        updateIndiceFromGroups();
        fireTarageGroupeUsedChanged(_t);
      }
    });
    return cmd.getSimplify();
  }

  /**
   * Permet de supprimer (si non utilise) la definition d'un groupe de bord.
   *
   * @param _t le type de bord a annuler
   * @return la commande si modif
   */
  public CtuluCommand setUndefined(final H2dRubarBoundaryTarageGroupType _t) {
    if (!isTarageGroupDefined(_t)) {
      return null;
    }
    final EvolutionReguliereAbstract timeMem = tarageGroups_.remove(_t);
    fireTarageGroupeUsedChanged(_t);
    return new CtuluCommand() {
      @Override
      public void redo() {
        tarageGroups_.remove(_t);
        fireTarageGroupeUsedChanged(_t);
      }

      @Override
      public void undo() {
        tarageGroups_.put(_t, timeMem);
        fireTarageGroupeUsedChanged(_t);
      }
    };
  }
}
