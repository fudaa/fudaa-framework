/*
 *  @creation     15 d�c. 2004
 *  @modification $Date: 2007-05-04 13:46:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: H2dVariableTimeType.java,v 1.7 2007-05-04 13:46:37 deniger Exp $
 */
public class H2dVariableTimeType extends H2dVariableType {

  /**
   * COEF_SCHEMA .
   */
  public static final H2dVariableTimeType COEF_SCHEMA = new H2dVariableTimeType(H2dResource.getS("Coefficient sch�ma"),
      null, "coefScheme") {

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return 0;
    }
  };
  /**
   * FREQUENCE_PRINT_SCHEMA.
   */
  public static final H2dVariableType FREQUENCE_PRINT_SCHEMA = new H2dVariableType(H2dResource
      .getS("Fr�quence impression"), null, "freq_print") {

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return 0;
    }

    @Override
    public boolean isInteger() {
      return true;
    }
  };
  /**
   * NB_MAX_IT_METHOD .
   */
  public static final H2dVariableType NB_MAX_IT_METHOD = new H2dVariableType(
      H2dResource.getS("Nombre d'it�ration max"), null, "nb_max_it") {

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return 0;
    }

    @Override
    public boolean isInteger() {
      return true;
    }
  };
  /**
   * NB_TIME_STEP_SCHEMA.
   */
  public static final H2dVariableTimeType NB_TIME_STEP_SCHEMA = new H2dVariableTimeType(H2dResource
      .getS("Nombre de pas de temps"), null, "nb_timestep_scheme") {

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return 0;
    }

    @Override
    public boolean isInteger() {
      return true;
    }
  };
  /**
   * PRECISION_METHOD .
   */
  public static final H2dVariableTimeType PRECISION_METHOD = new H2dVariableTimeType(H2dResource.getS("Pr�cision"),
      null, "acc_meth") {

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return 0;
    }
  };
  /**
   * VALUE_TIME_STEP_SCHEMA .
   */
  public static final H2dVariableTimeType VALUE_TIME_STEP_SCHEMA = new H2dVariableTimeType(H2dResource
      .getS("Valeur pas de temps"), null, "value_time_step") {

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return 0;
    }
  };

  /**
   * @param _nom
   * @param _telemacID
   * @param _varShortName
   */
  public H2dVariableTimeType(final String _nom, final String _telemacID, final String _varShortName) {
    super(_nom, _telemacID, _varShortName);
  }

  public H2dVariableTimeType() {
    super(null);
  }

  @Override
  public double getValue(final H2dVariableTypeContainer _container) {
    return 0;
  }

}
