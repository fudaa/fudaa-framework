/*
 * @file H2dTelemacBordManager.java
 * 
 * @creation 9 sept. 2003
 * 
 * @modification $Date: 2007-05-04 13:46:37 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandCompositeInverse;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.dico.DicoParamsLinkedSource;
import org.fudaa.dodico.ef.AllFrontierIteratorInterface;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dBcFrontierBlockAbstract;
import org.fudaa.dodico.h2d.H2dBcFrontierBlockInterface;
import org.fudaa.dodico.h2d.H2dBcListener;
import org.fudaa.dodico.h2d.H2dBcManagerBlockAbstract;
import org.fudaa.dodico.h2d.H2dBoundary;
import org.fudaa.dodico.h2d.H2dBoundaryTypeControllerInterface;
import org.fudaa.dodico.h2d.H2dEvolutionUseCounter;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dBoundaryTypeCommon;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionListenerDispatcher;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author deniger
 * @version $Id: H2dTelemacBcManager.java,v 1.45 2007-05-04 13:46:37 deniger Exp $
 */
public final class H2dTelemacBcManager extends H2dBcManagerBlockAbstract implements H2dBoundaryTypeControllerInterface, DicoParamsLinkedSource,
    H2dTelemacTracerLinkedManager {

  DicoParams dico_;

  H2dTelemacBoundary[] liquidBorder_;

  H2dTelemacBcParameter[] prescribedParameters_;

  H2dTelemacBcParameter[] prescribedOptions_;

  H2dTelemacEvolutionUsedListener evolListener_;

  private CtuluPermanentList bordList_;

  H2dTelemacTracerMng tracerManager;

  private H2dTelemacBcManager(final EfGridInterface _m) {
    super(_m);
  }

  protected void fillWithUsedEvol(final Set _s) {
    if (liquidBorder_ == null) {
      return;
    }
    for (int i = liquidBorder_.length - 1; i >= 0; i--) {
      liquidBorder_[i].fillWithUsedEvol(_s);
    }
  }

  protected boolean isOneCurveUsed() {
    if (liquidBorder_ == null) {
      return false;
    }
    for (int i = liquidBorder_.length - 1; i >= 0; i--) {
      if (liquidBorder_[i].isOneCurveUsed()) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param _s la tableau a remplir
   */
  public void fillWithVariableEvol(final H2dEvolutionUseCounter _s) {
    if (liquidBorder_ == null) {
      return;
    }
    for (int i = liquidBorder_.length - 1; i >= 0; i--) {
      liquidBorder_[i].fillWithVariableEvol(_s, i);
    }
  }

  /**
   * @return le premier type de bord liquide.
   */
  public H2dBoundaryType getDefaultLiquidType() {
    final CtuluPermanentList l = getBordList();
    final int n = l.size();
    H2dBoundaryType r;
    for (int i = 0; i < n; i++) {
      r = (H2dBoundaryType) l.get(i);
      if (r.isLiquide()) {
        return r;
      }
    }
    return null;
  }

  /**
   * @return true si la rugosite peut etre modifiee
   */
  public boolean isRugosityModifiable() {
    return getH2dFileFormat().isRugosityModifiable();
  }

  @Override
  public int getNbBoundaryType() {
    return getBordList().size();
  }

  /**
   * @return la liste des bords supportees
   */
  @Override
  public CtuluPermanentList getBordList() {
    if (bordList_ == null) {
      bordList_ = getH2dFileFormat().getAllowedBordTypeList();
    }
    return bordList_;
  }

  /**
   * Envoie un evt.
   */
  public void fireEvolutionUsedChanged() {
    if (!isTransientAvailable()) {
      return;
    }
    if (evolListener_ != null) {
      evolListener_.globalEvolutionUsedChanged();
    }
  }

  /**
   * @return true si des evolutions sont utilisees dans les frontieres liquides
   */
  public boolean isEvolutionUsed() {
    if (liquidBorder_ != null) {
      for (int i = liquidBorder_.length - 1; i >= 0; i--) {
        if (liquidBorder_[i].isOneCurveUsed()) {
          return true;
        }
      }
    }
    return false;
  }

  private void setFrontier(final H2dTelemacBordParFrontiere[] _f) {
    super.bordByFrontier_ = _f;
    computeLiquidBoundaries();
  }

  protected CtuluCommand removeTransientVar(final int _i, final H2dVariableType _t) {
    final H2dTelemacBoundary b = liquidBorder_[_i];
    if (b.isVariableTransientOneTime(_t)) {
      final int oldDeb = b.getIdxDeb();
      final int oldFin = b.getIdxFin();
      final List<EvolutionReguliereAbstract> evols = b.getEvolutions(_t);
      b.removeTransientVar(_t);
      return new CtuluCommand() {

        @Override
        public void undo() {
          final H2dTelemacBoundary bound = liquidBorder_[_i];
          if ((bound.getIdxDeb() == oldDeb) && (bound.getIdxFin() == oldFin)) {
            bound.setVariableTransient(_t, evols);
          } else {
            new Throwable().printStackTrace();
          }
        }

        @Override
        public void redo() {
          final H2dTelemacBoundary bound = liquidBorder_[_i];
          if ((bound.getIdxDeb() == oldDeb) && (bound.getIdxFin() == oldFin)) {
            bound.removeTransientVar(_t);

          } else {
            new Throwable().printStackTrace();
          }
        }
      };
    }
    return null;
  }

  protected boolean isLiquidBoundaryDefined() {
    return (liquidBorder_ != null) && (liquidBorder_.length > 0);
  }

  /**
   * @return le nombre de frontiere liquide
   */
  @Override
  public int getNbLiquideBoundaries() {
    return liquidBorder_ == null ? 0 : liquidBorder_.length;
  }

  @Override
  public void nbTracerChanged(CtuluCommandContainer cmp) {
    if (liquidBorder_ == null) {
      return;
    }
    final CtuluCommandCompositeInverse r = new CtuluCommandCompositeInverse();
    String value = getTracerDefaultValue();
    for (H2dTelemacBoundary boundary : liquidBorder_) {
      boundary.setNbTracer(getNbTraceurs(), value, null, r);
    }
    getBcParameter(H2dVariableTransType.TRACEUR).updateDicoWhenStructureChange(this, r);
    if (cmp != null) {
      cmp.addCmd(r);
    }

  }

  @Override
  public CtuluCommand keywordRemoved(final DicoEntite _ent) {
    if ((_ent == null) || (!isLiquidBoundaryDefined())) {
      return null;
    }
    final CtuluCommandCompositeInverse r = new CtuluCommandCompositeInverse();
    if (!isTransientAvailable()) {
      return r.getSimplify();
    }
    final H2dTelemacBcParameter p = getPrescribedFor(_ent);
    if (p == null) {
      return r.getSimplify();
    }
    final H2dVariableType v = p.getVariable();

    for (int i = liquidBorder_.length - 1; i >= 0; i--) {
      if (p.isParameterForType(liquidBorder_[i].getType())) {
        r.addCmd(removeTransientVar(i, v));
      }
    }
    final CtuluCommand rf = r.getSimplify();
    if (rf != null) {
      fireEvolutionUsedChanged();
    }
    return rf;
  }

  private String getTracerDefaultValue() {
    return getBcParameter(H2dVariableTransType.TRACEUR).getDefaultDataTypeValue();
  }

  /**
   * @return true si des variables peuvent avoir un comportement transitoire
   */
  public boolean isTransientAvailable() {
    return ((H2dTelemacDicoFileFormatVersion) dico_.getDicoFileFormatVersion()).isTransientAvailable();
  }

  /**
   * @param _b le bord concerne
   * @param _t la variable a tester
   * @return true si la variable est considere comme fixee sur le bord.
   */
  public boolean isVariableFixedOnBorder(final H2dTelemacBoundary _b, final H2dVariableType _t) {
    final H2dTelemacBcParameter param = getBcParameter(_t);
    return param == null ? false : param.isValueFixedFor(this, _b);
  }

  /**
   * Surcharger uniquement pour des raisons de performances et permettre aux inner classes d'utiliser cette methode sans
   * faire appel a des methodes emulees.
   */
  @Override
  protected boolean setBordType(final H2dBoundary _b, final H2dBoundaryType _type) {
    return super.setBordType(_b, _type);
  }

  /**
   * @param _t le point limite concerne
   * @return les variables autorisees pour ce point.
   */
  public H2dVariableType[] getVariablesForPoint(final H2dTelemacBoundary _t) {
    final Set r = new HashSet(10);
    H2dVariableType[] vs;
    final H2dBoundaryType type = _t.getType();
    if (type.isSolide()) {
      return isRugosityModifiable() ? new H2dVariableType[] { H2dVariableType.COEF_FROTTEMENT } : null;
    }

    for (int i = prescribedParameters_.length - 1; i >= 0; i--) {
      if (prescribedParameters_[i].isParameterForType(type)) {
        vs = prescribedParameters_[i].getVariablesForPoint(getH2dFileFormat(), _t, dico_);
        if (vs != null) {
          r.addAll(Arrays.asList(vs));
        }
      }
    }
    final H2dVariableType[] rf = new H2dVariableType[r.size()];
    r.toArray(rf);
    return rf;
  }

  /**
   * @param _t le point limite concerne
   * @return les infos pour ce bord
   */
  public Map getVariablesForPointInfo(final H2dTelemacBoundary _t) {
    if (_t.getType().isSolide()) {
      return null;
    }
    Map r = null;
    Map temp;
    final H2dBoundaryType type = _t.getType();
    for (int i = prescribedParameters_.length - 1; i >= 0; i--) {
      if (prescribedParameters_[i].isParameterForType(type)) {
        temp = prescribedParameters_[i].getVariablesForPointInfo(getH2dFileFormat(), _t, dico_);
        if (temp != null) {
          if (r == null) {
            r = temp;
          } else {
            r.putAll(temp);
          }
        }
      }
    }
    return r;
  }

  /**
   * @return les evolutions sur les frontieres liquides
   */
  public List<H2dTelemacEvolutionFrontiere> getLiquideEvolution() {
    if (liquidBorder_ == null) {
      return null;
    }
    final List<H2dTelemacEvolutionFrontiere> r = new ArrayList<H2dTelemacEvolutionFrontiere>();
    int n = liquidBorder_.length;
    for (int i = 0; i < n; i++) {
      liquidBorder_[i].fillListWithLiquidEvolution(r, i);
    }
    n = r.size();
    if (n == 0) {
      return null;
    }
    return r;
  }

  public int getNbTraceurs() {
    return tracerManager.getNbTraceurs();
  }

  /**
   * @param _liquid les nouvelles evolutions sur les frontieres liquides
   * @param _listener le listener
   */
  public void setEvolution(final H2dTelemacEvolutionFrontiere[] _liquid, final EvolutionListenerDispatcher _listener) {
    if ((liquidBorder_ != null) && (_liquid != null)) {
      final int nbLiq = liquidBorder_.length;
      int idx;
      for (int i = _liquid.length - 1; i >= 0; i--) {
        H2dTelemacEvolutionFrontiere liq = _liquid[i];
        idx = liq.getTelemacEvolution().getIndexFrontiere();
        if ((idx >= 0) && (idx < nbLiq)) {
          final H2dTelemacBoundary b = liquidBorder_[idx];
          final EvolutionReguliereAbstract evol = EvolutionReguliere.createTimeEvolution(liq.getTelemacEvolution().getEvolution(), null);
          if (H2dVariableTransType.TRACEUR.equals(liq.getTelemacEvolution().getVariableType())) {
            b.setTracerTransient(evol, liq.getIdxVariable());
          } else {
            b.setVariableTransient(liq.getTelemacEvolution().getVariableType(), evol, 0);
          }
          evol.setListener(_listener);
        }
      }
    }
  }

  /**
   * @param _t la variable a considerer
   * @return les parametres pour cette variable
   */
  public H2dTelemacBcParameter getBcParameter(final H2dVariableType _t) {
    for (int i = prescribedParameters_.length - 1; i >= 0; i--) {
      if (prescribedParameters_[i].isParameterFor(_t)) {
        return prescribedParameters_[i];
      }
    }
    return null;
  }

  /**
   * @param _l le listener pour les modic concernant les cl.
   */
  public void addClChangedListener(final H2dTelemacBcListener _l) {
    if (listeners_ == null) {
      listeners_ = new ArrayList(10);
      listeners_.add(_l);
    } else if (!listeners_.contains(_l)) {
      listeners_.add(_l);
    }
  }

  /**
   * @param _frIdx l'indice de la frontiere voulu
   * @param _deb l'indice de debut du bord liquide demande
   * @param _fin l'indice de fin du bord liquide demande
   * @return le bord liquide correspondant ou null si aucun.
   */
  public H2dTelemacBoundary getLiquidBord(final int _frIdx, final int _deb, final int _fin) {
    if ((_frIdx >= 0) && (_frIdx < bordByFrontier_.length)) {
      return ((H2dTelemacBordParFrontiere) bordByFrontier_[_frIdx]).getTelemacLiquidBord(_deb, _fin);
    }
    return null;
  }

  /**
   * @param _frontierIdx l'indice de la frontiere en question
   * @param _idxSelect les indices des points selectionnes
   * @return l'unique bord contenant tous les points selectionnes. null si aucun ou plus d'un
   */
  public H2dTelemacBoundary getUniqueSelectedBoundary(final int _frontierIdx, final int[] _idxSelect) {
    if ((_frontierIdx >= 0) && (_frontierIdx < bordByFrontier_.length)) {
      return ((H2dTelemacBordParFrontiere) bordByFrontier_[_frontierIdx]).getUniqueSelectedTelemacBord(_idxSelect);
    }
    return null;
  }

  /**
   * @param _frIdx l'indice de la frontiere en question
   * @param _ptsIdx les indices des points selectionnes
   * @return collections contenant tous les bords contenant au moins un indice du tableau
   */
  public Collection getSelectedBorder(final int _frIdx, final int[] _ptsIdx) {
    if ((_frIdx >= 0) && (_frIdx < getNbBcFrontier())) {
      return ((H2dTelemacBordParFrontiere) bordByFrontier_[_frIdx]).getBordsContainingIdx(_ptsIdx);
    }
    return null;
  }

  /**
   * Agrandit un bord suivant plusieurs conditions: un seul bord liquide doit etre modifie.
   * 
   * @param _frIdx l'indice de la frontiere
   * @param _min le nouveau min du bord
   * @param _max le nouveau max du bord
   * @return la commande si modif ou null sinon
   */
  public CtuluCommand increaseLiquidBorder(final int _frIdx, final int _min, final int _max) {
    if ((_frIdx >= 0) && (_frIdx < bordByFrontier_.length)) {
      final H2dTelemacBordParFrontiere f = (H2dTelemacBordParFrontiere) bordByFrontier_[_frIdx];
      return f.increaseLiquidBorder(_min, _max);
    }
    return null;
  }

  /**
   * @param _b le bord liquide a enlever
   * @return la commande si modif; null sinon
   */
  public CtuluCommand removeLiquidBorder(final H2dTelemacBoundary _b) {
    if (_b.getType().isLiquide()) {
      final int idx = _b.getIdxFrontiere();
      if ((idx >= 0) && (idx < bordByFrontier_.length)) {
        return ((H2dTelemacBordParFrontiere) bordByFrontier_[idx]).removeLiquidBorder(_b);
      }
    }
    return null;
  }

  /**
   * Enleve les points liquides (transforme en solide) compris dans l'intervalle [_idxDeb,_idxFin]. Les bords liquides
   * vides seront enleves.
   * 
   * @param _idxDeb l'indice de debut
   * @param _idxFin l'indice de fin
   * @param _frontierIdx l'indice de la frontiere
   * @return la commande si modif; null sinon
   */
  public CtuluCommand removeLiquidPts(final int _idxDeb, final int _idxFin, final int _frontierIdx) {
    if ((_frontierIdx >= 0) && (_frontierIdx < bordByFrontier_.length)) {
      return ((H2dTelemacBordParFrontiere) bordByFrontier_[_frontierIdx]).removeLiquidBorderAndCmd(_idxDeb, _idxFin);
    }
    return null;
  }

  /**
   * Le nouveau bord liquide ne doit pas toucher un autre bord liquide.
   * 
   * @param _idxDeb l'indice de deb du nouveau bord liquide
   * @param _idxFin l'indice de fin du nouveau bord liquide
   * @param _frontierIdx la frontiere concernee
   * @param _type le type du nouveau bord liquide
   * @return la commande si modif; null sinon
   */
  public CtuluCommand addLiquidPts(final int _idxDeb, final int _idxFin, final int _frontierIdx, final H2dBoundaryType _type) {
    if ((_frontierIdx >= 0) && (_frontierIdx < bordByFrontier_.length)) {
      return ((H2dTelemacBordParFrontiere) bordByFrontier_[_frontierIdx]).addLiquidPts(_idxDeb, _idxFin, _type);
    }
    return null;
  }

  boolean isBoundaryTypeUsed(final H2dBoundaryType[] _b) {
    if (liquidBorder_ != null) {
      for (int i = liquidBorder_.length - 1; i >= 0; i--) {
        if (Arrays.binarySearch(_b, liquidBorder_[i].getType()) >= 0) {
          return true;
        }
      }
    }
    return false;
  }

  boolean isBoundaryTypeUsed(final List _b) {
    if (liquidBorder_ != null) {
      for (int i = liquidBorder_.length - 1; i >= 0; i--) {
        if (Collections.binarySearch(_b, liquidBorder_[i].getType()) >= 0) {
          return true;
        }
      }
    }
    return false;
  }

  protected void updateEntiteState() {
    int n = prescribedParameters_.length;
    H2dTelemacBcParameter p;
    for (int i = n - 1; i >= 0; i--) {
      p = prescribedParameters_[i];
      final DicoEntite.Vecteur v = p.getEntite();
      if (v != null) {
        final int old = v.getNbElemFixed();
        if (!isBoundaryTypeUsed(p.getBoundariesType())) {
          v.setNbElem(0);
        } else {
          v.setNbElem(liquidBorder_.length * p.getNbInternValues());
        }
        if (old != v.getNbElemFixed()) {
          dico_.testIfValid(v);
        }
      }
    }
    n = prescribedOptions_ == null ? 0 : prescribedOptions_.length;
    for (int i = n - 1; i >= 0; i--) {
      p = prescribedOptions_[i];
      final DicoEntite.Vecteur v = p.getEntite();
      if (isBoundaryTypeUsed(p.getBoundariesType())) {
        v.setNbElem(liquidBorder_.length);
      } else {
        v.setNbElem(0);
      }
      dico_.testIfValid(v);
    }
  }

  protected CtuluCommand updateAllParameterWhenStructureChanged() {
    int n = prescribedParameters_.length;
    final CtuluCommandCompositeInverse r = new CtuluCommandCompositeInverse();
    for (int i = n - 1; i >= 0; i--) {
      prescribedParameters_[i].updateDicoWhenStructureChange(this, r);
    }
    n = prescribedOptions_.length;
    for (int i = n - 1; i >= 0; i--) {
      prescribedOptions_[i].updateDicoWhenStructureChange(this, r);
    }
    return r.getSimplify();
  }

  /**
   * @return les parametres concernant le traceur
   */
  public H2dTelemacBcParameter getPrescribedTracer() {
    if (prescribedParameters_ == null) {
      return null;
    }
    final DicoEntite ent = getTracerEntite();
    if (ent != null) {
      for (int i = prescribedParameters_.length - 1; i >= 0; i--) {
        if (prescribedParameters_[i].getEntite() == ent) {
          return prescribedParameters_[i];
        }
      }
    }
    return null;
  }

  /**
   * @return true si le mot-cle associe au tracer est initialise
   */
  public boolean isTracerEntiteSet() {
    final DicoEntite ent = getTracerEntite();
    return (ent != null) ? dico_.isValueSetFor(ent) : false;
  }

  /**
   * @return le mot-cle associe au traceur.
   */
  public DicoEntite getTracerEntite() {
    return prescribedParameters_ == null ? null : getH2dFileFormat().getPrescribedTracer();
  }

  /**
   * @return le nombre de parametres limites.
   */
  public int getNbLimitParametres() {
    return prescribedParameters_ == null ? 0 : prescribedParameters_.length;
  }

  /**
   * @return le nombre d'option limite
   */
  public int getNbLimitOptions() {
    return prescribedOptions_ == null ? 0 : prescribedOptions_.length;
  }

  /**
   * @return nouveau tableau contenant tous les parametres limites
   */
  public H2dTelemacBcParameter[] getLimitParameters() {
    if (prescribedParameters_ != null) {
      final H2dTelemacBcParameter[] r = new H2dTelemacBcParameter[prescribedParameters_.length];
      System.arraycopy(prescribedParameters_, 0, r, 0, r.length);
      return r;
    }
    return null;
  }

  /**
   * @return les variables pouvant etre definies avec un mot-cl�
   */
  public H2dTelemacBcParameter[] getVariablesWithKeyword() {
    if (prescribedParameters_ != null) {
      final List r = new ArrayList(prescribedParameters_.length);
      for (int i = 0; i < prescribedParameters_.length; i++) {
        if (prescribedParameters_[i].getEntite() != null) {
          r.add(prescribedParameters_[i]);
        }
      }
      if (r.size() > 0) {
        final H2dTelemacBcParameter[] rf = new H2dTelemacBcParameter[r.size()];
        r.toArray(rf);
        return rf;
      }
    }
    return null;
  }

  /**
   * @return les parametres utilises,disposant d'un mot-cle et variable dans l'espace
   */
  public H2dTelemacBcParameter[] getUsedParametersWithKeyword() {
    if (prescribedParameters_ != null) {
      final Set r = new HashSet(prescribedParameters_.length);
      for (int i = getNbLiquidBoundary() - 1; i >= 0; i--) {
        final H2dTelemacBcParameter[] vs = getLimiteParameterFor(liquidBorder_[i].getType());
        for (int j = vs.length - 1; j >= 0; j--) {
          if (vs[j].getEntite() != null) {
            r.add(vs[j]);
          }

        }
      }
      if (r.size() > 0) {
        final H2dTelemacBcParameter[] rf = new H2dTelemacBcParameter[r.size()];
        r.toArray(rf);
        return rf;
      }
    }
    return null;
  }

  /**
   * @return le modele du fichier cas
   */
  public DicoParams getDicoParams() {
    return dico_;
  }

  /**
   * @return toutes les options limites.
   */
  public H2dTelemacBcParameter[] getLimitOptions() {
    if (prescribedOptions_ != null) {
      final H2dTelemacBcParameter[] r = new H2dTelemacBcParameter[prescribedOptions_.length];
      System.arraycopy(prescribedOptions_, 0, r, 0, r.length);
      return r;
    }
    return null;
  }

  protected void computeLiquidBoundaries() {
    final List l = new ArrayList(getNbTotalBoudary());
    final int n = bordByFrontier_.length;
    for (int i = 0; i < n; i++) {
      final H2dBcFrontierBlockInterface bordBound = bordByFrontier_[i];
      final int nbBord = bordBound.getNbBord();
      for (int j = 0; j < nbBord; j++) {
        H2dTelemacBoundary bord = (H2dTelemacBoundary) bordBound.getBord(j);
        if (bord.getType().isLiquide()) {
          l.add(bord);
        }
      }
    }
    liquidBorder_ = new H2dTelemacBoundary[l.size()];
    l.toArray(liquidBorder_);
  }

  /**
   * @param _t le type de bord
   * @return tous les parametres limites concernes par ce type de bord.
   */
  public H2dTelemacBcParameter[] getLimiteParameterFor(final H2dBoundaryType _t) {
    final List r = new ArrayList(prescribedParameters_.length);
    for (int i = prescribedParameters_.length - 1; i >= 0; i--) {
      if (Collections.binarySearch(prescribedParameters_[i].getBoundariesType(), _t) >= 0) {
        r.add(prescribedParameters_[i]);
      }
    }
    final H2dTelemacBcParameter[] rf = new H2dTelemacBcParameter[r.size()];
    r.toArray(rf);
    return rf;
  }

  @Override
  public List getVariableFor(final H2dBoundaryType _t) {
    final List r = new ArrayList(prescribedParameters_.length);
    for (int i = prescribedParameters_.length - 1; i >= 0; i--) {
      if (Collections.binarySearch(prescribedParameters_[i].getBoundariesType(), _t) >= 0) {
        r.add(prescribedParameters_[i].getVariable());
      }
    }
    return r;
  }

  /**
   * @param _idxInLiquidBoundary l'indice dans les bords liquides
   * @return le bord liquide d'indice _idxInLiquidBoundary dans la numerotation des bords liquide
   */
  public H2dBoundary getLiquidBoundary(final int _idxInLiquidBoundary) {
    return liquidBorder_[_idxInLiquidBoundary];
  }

  /**
   * @param _b le bord a tester
   * @return l'indice dans la numerotation des bords liquides. -1 si non trouve.
   */
  public int getIdxLiquidBord(final H2dBoundary _b) {
    final int n = liquidBorder_.length - 1;
    for (int i = n; i >= 0; i--) {
      if (liquidBorder_[i] == _b) {
        return i;
      }
    }
    return -1;
  }

  /**
   * @param _l le listener a virer
   */
  public void removeClChangedListener(final H2dBcListener _l) {
    if (listeners_ != null) {
      listeners_.remove(_l);
    }
  }

  /**
   * @return le format utilise pour lire les donnees.
   */
  public H2dTelemacDicoFileFormatVersion getH2dFileFormat() {
    return (H2dTelemacDicoFileFormatVersion) dico_.getDicoFileFormatVersion();
  }

  /**
   * @param _idxOnFrontier l'indice dans la numerotation des bords frontieres
   * @return le bord demande ou null si non trouve
   */
  public H2dBoundary getBordFromIdxOnFrontier(final int _idxOnFrontier) {
    final int[] r = new int[2];
    maillage_.getFrontiers().getIdxFrontierIdxPtInFrontierFromFrontier(_idxOnFrontier, r);
    final H2dTelemacBordParFrontiere b = (H2dTelemacBordParFrontiere) super.bordByFrontier_[r[0]];
    return b.getBordContainingIdx(r[1]);
  }

  /**
   * Permet d'initialiser les variables "de bord" en fonction du fichier dico.
   * 
   * @param _params le fichier dico source
   * @param _analyze le resultat de la commande
   */
  public void setDicoParams(final H2dTelemacTracerMng tracer, final DicoParams _params, final CtuluAnalyze _analyze) {
    if (_params == dico_) {
      return; // remove old listener
    }
    this.tracerManager = tracer;
    tracer.addLinkedManager(this);
    if (dico_ != null) {
      dico_.unsetLinkedSource();
    }
    dico_ = _params;
    dico_.addLinkedSource(this);
    if (dico_ == null) {
      CtuluLibMessage.error("no dico for boundary management");
    } else if ((dico_.getDicoFileFormatVersion() instanceof H2dTelemacDicoFileFormatVersion)) {
      H2dTelemacBcParameterFactory.buildVariables(this, _analyze);
      updateEntiteState();
    } else {
      if (_analyze != null) {
        _analyze.addFatalError(H2dResource.getS("Les param�tres du fichiers dico ne correspondent pas � un projet hydraulique 2D"));
      }
    }
  }

  /**
   * @return l'analyse contenant toutes les erreurs des parametres et options limites.
   */
  public CtuluAnalyze getErrorMessageForBcParameter() {
    final CtuluAnalyze r = new CtuluAnalyze();
    for (int i = prescribedParameters_.length - 1; i >= 0; i--) {
      prescribedParameters_[i].fillAnalyseWithError(dico_, r);
    }
    for (int i = prescribedOptions_.length - 1; i >= 0; i--) {
      prescribedOptions_[i].fillAnalyseWithError(dico_, r);
    }
    return r;
  }

  /**
   * Methode a appeler pour corriger les erreur sur les mot-cles definissant des cl.
   */
  public void correctErrorForBcParameter() {
    updateAllParameterWhenStructureChanged();
  }

  protected H2dTelemacBoundary createLiquidBord(final int _idxFr, final H2dBoundaryType _b) {
    final H2dTelemacBoundary r = new H2dTelemacBoundary(_idxFr, this);
    r.setBoundaryType(_b, null);
    return r;
  }

  protected H2dBoundary createBord(final int _idxFr, final H2dBoundaryType _b) {
    final H2dTelemacBoundary r = new H2dTelemacBoundary(_idxFr, this);
    r.setBoundaryType(_b, null);
    return r;
  }

  private H2dTelemacBordParFrontiere createTelemacBordParFr(final List _l, final H2dTelemacBoundaryCondition[] _cl, final int _i) {
    return new H2dTelemacBordParFrontiere(this, _l, _cl, _i);
  }

  protected static H2dTelemacBoundary initFrom(final int _idxFr, final H2dTelemacCLElementSource _s, final H2dTelemacBcManager _this) {
    final H2dTelemacBoundary r = new H2dTelemacBoundary(_idxFr, _this);
    r.setBoundaryType(_s.bordType_, null);
    r.setUVType(_s.uType_, _s.vType_, null);
    r.setTracerType(_s.tracerType_, null);
    return r;
  }

  /**
   * Les conditions limites doivent etre donnees dans l'ordre de la numerotation frontiere. C'est l'ordre par defaut
   * donn� par les lecteurs. The boundary limits must be ordered according to the boundary numerotation.
   * 
   * @param _maillage le maillage support
   * @param _clSource la source pour les cl.
   * @param _analyze le receveur des erreurs.
   * @param _progression la barre de progression
   * @return les parametres limites correctement initialises
   */
  public static H2dTelemacBcManager init(final EfGridInterface _maillage, H2dTelemacTracerMng tracerMng, final H2dTelemacCLSourceInterface _clSource,
      final CtuluAnalyze _analyze, final ProgressionInterface _progression) {
    final EfFrontierInterface frontiere = _maillage.getFrontiers();
    final int nbPtFrontiere = frontiere.getNbTotalPt();
    if (_clSource.getNbLines() != nbPtFrontiere) {
      if (_analyze != null) {
        _analyze.addFatalError(H2dResource.getS("Le nombre de conditions limites ne correspond pas au nombre de points fronti�res"));
      }
      return null;
    }
    H2dTelemacBoundary bordEnCours = null;
    H2dTelemacBoundary firstBord = null;
    final H2dTelemacBcManager r = new H2dTelemacBcManager(_maillage);
    final H2dTelemacBordParFrontiere[] bordByFrontier = new H2dTelemacBordParFrontiere[frontiere.getNbFrontier()];
    // le nb de frontiere
    final int n = frontiere.getNbFrontier();

    final ProgressionUpdater up = new ProgressionUpdater(_progression);
    up.setValue(4, n);
    up.majAvancement();

    H2dBoundaryType bordType;
    boolean tracerTypeIsConstant = true;
    boolean uTypeIsConstant = true;
    boolean vTypeIsConstant = true;
    // boolean hTypeIsConstant= true;
    final H2dTelemacCLElementSource clEltSource = new H2dTelemacCLElementSource();
    final AllFrontierIteratorInterface it = frontiere.getAllFrontierIterator();
    int globalIdx, boundaryIdx;
    for (int i = 0; i < n; i++) {
      final List bords = new ArrayList();
      bordEnCours = null;
      firstBord = null;
      final int nbPt = frontiere.getNbPt(i);
      final H2dTelemacBoundaryCondition[] clFrontier = new H2dTelemacBoundaryCondition[nbPt];
      for (int j = 0; j < nbPt; j++) {
        if (!it.hasNext()) {
          if (_analyze != null) {
            _analyze.addFatalError(DodicoLib.getS("Probl�me interne"));
          }
          return null;
        }
        globalIdx = it.next();
        boundaryIdx = it.getFrontierIdx();
        final H2dTelemacBoundaryCondition cl = new H2dTelemacBoundaryCondition();
        _clSource.getLine(boundaryIdx, clEltSource);
        cl.initFrom(clEltSource);
        clFrontier[j] = cl;
        bordType = clEltSource.bordType_;
        if (cl.getIndexPt() != globalIdx) {
          orderIsFalse(_analyze, globalIdx, cl);
          return null;
        }
        if (bordEnCours == null) {
          bordEnCours = initFrom(i, clEltSource, r);
          bordEnCours.setIdxmaillageFrontiere(i);
          bordEnCours.setIdxDeb(j);
          bordEnCours.setIdxFin(j);
          tracerTypeIsConstant = true;
          uTypeIsConstant = true;
          vTypeIsConstant = true;
          // hTypeIsConstant= true;
          firstBord = bordEnCours;
          bords.add(bordEnCours);
        } else {
          if (bordType == bordEnCours.getType()) {
            if (tracerTypeIsConstant && (clEltSource.tracerType_ != bordEnCours.getTracerType())) {
              tracerTypeIsConstant = false;
              bordEnCours.setTracerType(H2dBcType.LIBRE, null);
              _analyze.addInfo(H2dResource.getS("Le traceur n'est pas homog�ne sur le m�me bord "
                  + "(point {0} dans la num�rotation des points de bords)", CtuluLibString.getString(boundaryIdx)));
            }
            if (uTypeIsConstant && (clEltSource.uType_ != bordEnCours.getUType())) {
              uTypeIsConstant = false;
              vTypeIsConstant = false;
              final StringBuffer err = new StringBuffer(H2dResource.getS("La vitesse u n'est pas homog�ne sur le m�me bord "
                  + "(point {0} dans la num�rotation des points de bords", CtuluLibString.getString(boundaryIdx)));

              checkVelocities(_analyze, bordEnCours, bordType, err);
            }
            if (vTypeIsConstant && (clEltSource.vType_ != bordEnCours.getVType())) {
              uTypeIsConstant = false;
              vTypeIsConstant = false;
              final StringBuffer err = new StringBuffer(H2dResource.getS("La vitesse v n'est pas homog�ne sur le m�me bord "
                  + "(point {0} dans la num�rotation des points de bords", CtuluLibString.getString(boundaryIdx)));
              checkVelocities(_analyze, bordEnCours, bordType, err);
            }
            bordEnCours.setIdxFin(j);
          } else { // attention: les bords solides gagnent sur les bord liquides (joli!)
            // pour le reste on va dans le sens ipobo
            if (bordType != H2dBoundaryTypeCommon.SOLIDE) {
              bordEnCours.setIdxFin(j);
            }
            bordEnCours = initFrom(i, clEltSource, r);
            bordEnCours.setIdxDeb(j);
            // le bord solide se propage.
            if (bordType == H2dBoundaryTypeCommon.SOLIDE) {
              bordEnCours.setIdxDeb(j - 1);
            }
            bordEnCours.setIdxFin(j);
            tracerTypeIsConstant = true;
            uTypeIsConstant = true;
            vTypeIsConstant = true;
            // hTypeIsConstant= true;
            bords.add(bordEnCours);
          }
        }
      }
      if (bordEnCours != null && bordEnCours.getIdxFin() != (nbPt - 1)) {
        if (_analyze != null) {
          _analyze.addFatalError(H2dResource.getS("D�termination des bords impossible :erreur dans les indexs"));
        }
        new Throwable().printStackTrace();
        return null;
      }
      if (bordEnCours != null) {
        bordEnCours.setIdxFin(0);
      }
      if ((bordEnCours != null && bordEnCours != firstBord) && (bordEnCours.getType() == firstBord.getType())) {
        // the boundaries are computed from segment.
        // if the first found boundary has only one point, it's not a valid segment
        // so the first boundary is merged with the last one and removed.
        if (firstBord.getIdxFin() == 0) {
          bordEnCours.setIdxFin(firstBord.getIdxFin());
          bords.remove(firstBord);
        } else {
          firstBord.setIdxDeb(bordEnCours.getIdxDeb());
          bords.remove(bordEnCours);
        }
      } // bord unique
      if (bords.size() == 1 && firstBord != null) {
        firstBord.setIdxDeb(0);
        firstBord.setIdxFin(0);
      } else if (firstBord != null && firstBord.getIdxDeb() < 0) {
        if (firstBord.getIdxDeb() != -1) {
          if (_analyze != null) {
            _analyze.addFatalError(H2dResource.getS("D�termination des bords impossible :erreur dans l'index du premier bord"));
          }
          return null;
        } // propagation du bord solide.
        firstBord.setIdxDeb(nbPt - 1);
        bordEnCours.setIdxFin(nbPt - 1);
      }
      checkIsolatedBc(_analyze, bords);
      bordByFrontier[i] = r.createTelemacBordParFr(bords, clFrontier, i);
      up.majAvancement();
    }
    r.setFrontier(bordByFrontier);
    r.setDicoParams(tracerMng, tracerMng.getDico(), _analyze);
    return r;
  }

  private static boolean orderIsFalse(final CtuluAnalyze _analyze, final int _globalIdx, final H2dTelemacBoundaryCondition _cl) {
    boolean ok;
    if (_analyze != null) {
      _analyze.addFatalError(H2dResource.getS("les conditions limites ne sont pas dans l'ordre de la fronti�re")
          + CtuluLibString.LINE_SEP
          + H2dResource.getS("L'index lu est invalide ( lu: {0} attendu: {1})", CtuluLibString.getString(_cl.getIndexPt() + 1),
              CtuluLibString.getString(_globalIdx + 1)));
    }
    ok = false;
    return ok;
  }

  private static void checkIsolatedBc(final CtuluAnalyze _analyze, final List _bords) {
    final int nBords = _bords.size();
    for (int j = 0; j < nBords; j++) {
      final H2dTelemacBoundary b = (H2dTelemacBoundary) _bords.get(j);
      if ((nBords > 1) && (b.getIdxDeb() == b.getIdxFin())) {
        if (_analyze != null) {
          _analyze.addWarn(H2dResource.getS("Des conditions limites sont isol�es"), -1);
        }
        break;
      }
    }
  }

  /**
   * il est possible qu'un bord avec vitesse imposee ait les deux vitesses en libre. on corrige cela en mettant les 2
   * vitesses en permanent.
   */
  private static void checkVelocities(final CtuluAnalyze _analyze, final H2dTelemacBoundary _bordEnCours, final H2dBoundaryType _bordType,
      final StringBuffer _err) {
    if ((_bordType == H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES) || (_bordType == H2dBoundaryTypeCommon.LIQUIDE_VITESSES_IMPOSEES)) {
      _bordEnCours.setUVType(true, true, null);
      _err.append(H2dResource.getS("Par la suite, u et v seront suppos�es fixes pour le bord de type {0}.", _bordType.toString()));
      _analyze.addError(_err.toString());
    } else if (_bordType == H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE) {
      _bordEnCours.setUVType(false, false, null);
      _err.append(H2dResource.getS("Par la suite, u et v seront suppos�es libres pour le bord de type {0}.", _bordType.toString()));
      _analyze.addError(_err.toString());
    }
  }

  /**
   * @param _maillage le maillage support
   * @return les parametres de bord correctement initialises.
   */
  public static H2dTelemacBcManager init(final EfGridInterface _maillage) {
    final EfFrontierInterface frontiere = _maillage.getFrontiers();
    final H2dTelemacBordParFrontiere[] bordByFrontier = new H2dTelemacBordParFrontiere[frontiere.getNbFrontier()];
    final H2dTelemacBcManager r = new H2dTelemacBcManager(_maillage);
    for (int i = frontiere.getNbFrontier() - 1; i >= 0; i--) {
      final H2dTelemacBoundary b = new H2dTelemacBoundary(i, r);
      b.setIdxDeb(0);
      b.setIdxFin(0);
      b.setBoundaryType(H2dBoundaryTypeCommon.SOLIDE, null);
      final H2dTelemacBoundaryCondition[] cl = new H2dTelemacBoundaryCondition[frontiere.getNbPt(i)];
      for (int j = cl.length - 1; j >= 0; j--) {
        cl[j] = new H2dTelemacBoundaryCondition();
        cl[j].setIndexPt(frontiere.getIdxGlobal(i, j));
      }
      final List l = new ArrayList();
      l.add(b);
      bordByFrontier[i] = r.createTelemacBordParFr(l, cl, i);
    }
    final H2dTelemacBoundaryConditionMutable[] clInit = new H2dTelemacBoundaryConditionMutable[frontiere.getNbTotalPt()];
    for (final AllFrontierIteratorInterface it = frontiere.getAllFrontierIterator(); it.hasNext();) {
      final H2dTelemacBoundaryConditionMutable cl = new H2dTelemacBoundaryConditionMutable();
      cl.setIndexPt(it.next());
      clInit[it.getFrontierIdx()] = cl;
    }
    r.setFrontier(bordByFrontier);
    return r;
  }

  /**
   * @param _bord le bord concerne
   * @param _t la variable concernee
   * @param _isImposed true si doit etre impose par mot-cle
   * @return la commande si modif; null sinon
   */
  public CtuluCommand setLiquidBordVariableActivatedAndCmd(final H2dTelemacBoundary _bord, final H2dVariableType _t, final boolean _isImposed) {
    if ((_bord == null) || (!_bord.getType().isLiquide())) {
      return null;
    }
    final int idx = getIdxLiquidBord(_bord);
    if (idx < 0) {
      return null;
    }
    final boolean old = _bord.isTypeImposed(_t);
    if (_bord.setTypeImposed(_t, _isImposed, this)) {
      final int deb = _bord.getIdxDeb();
      final int fin = _bord.getIdxFin();
      return new CtuluCommand() {

        @Override
        public void undo() {
          final H2dTelemacBoundary b = liquidBorder_[idx];
          if ((b.getIdxDeb() == deb) && (b.getIdxFin() == fin)) {
            b.setTypeImposed(_t, old, H2dTelemacBcManager.this);
          }
        }

        @Override
        public void redo() {
          final H2dTelemacBoundary b = liquidBorder_[idx];
          if ((b.getIdxDeb() == deb) && (b.getIdxFin() == fin)) {
            b.setTypeImposed(_t, _isImposed, H2dTelemacBcManager.this);
          }
        }
      };
    }
    return null;
  }

  /**
   * @param _bord le bord concerne
   * @param _uTypeImposed true si u est impose sur le bord
   * @param _vTypeImposed true si v est impose sur le bord
   * @return la commande si modif.
   */
  public CtuluCommand setLiquidBordUVAndCmd(final H2dTelemacBoundary _bord, final boolean _uTypeImposed, final boolean _vTypeImposed) {
    if ((_bord == null) || (!_bord.getType().isLiquide())) {
      return null;
    }
    final int idx = getIdxLiquidBord(_bord);
    if (idx < 0) {
      return null;
    }
    final boolean oldU = _bord.isUTypeImposed();
    final boolean oldV = _bord.isVTypeImposed();
    if (_bord.setUVType(_uTypeImposed, _vTypeImposed, H2dTelemacBcManager.this)) {
      final int deb = _bord.getIdxDeb();
      final int fin = _bord.getIdxFin();
      return new CtuluCommand() {

        @Override
        public void undo() {
          final H2dTelemacBoundary b = liquidBorder_[idx];
          if ((b.getIdxDeb() == deb) && (b.getIdxFin() == fin)) {
            b.setUVType(oldU, oldV, H2dTelemacBcManager.this);
          }
        }

        @Override
        public void redo() {
          final H2dTelemacBoundary b = liquidBorder_[idx];
          if ((b.getIdxDeb() == deb) && (b.getIdxFin() == fin)) {
            b.setUVType(_uTypeImposed, _vTypeImposed, H2dTelemacBcManager.this);
          } else {
            new Throwable("bord not found").printStackTrace();
          }
        }
      };
    }
    return null;
  }

  /**
   * Modifie (si possible) les evolutions utilisees pour le bord donne.
   * 
   * @param _b le bord concerne
   * @param _varEvol Variable->Evolution
   * @param tracerVar must be the same size than the tracers.
   * @return la commande si modif. null sinon
   */
  public CtuluCommand setEvolution(final H2dTelemacBoundary _b, final Map _varEvol, final List<EvolutionReguliereInterface> tracerVar) {
    final int i = getIdxLiquidBord(_b);
    if (i < 0) {
      return null;
    }
    final Map old = _b.createVarEvolMemento();
    if (_b.setVariableTransient(_varEvol)) {
      final int deb = _b.getIdxDeb();
      final int fin = _b.getIdxFin();
      fireEvolutionUsedChanged();
      return new CtuluCommand() {

        private H2dTelemacBoundary find() {
          if (liquidBorder_ == null) {
            return null;
          }
          for (H2dTelemacBoundary boundary : liquidBorder_) {
            if ((boundary.getIdxDeb() == deb) && (boundary.getIdxFin() == fin)) {
              return boundary;
            }
          }
          return null;
        }

        @Override
        public void undo() {
          final H2dTelemacBoundary b = find();
          if (b != null) {
            b.setVariableTransient(old);
            fireEvolutionUsedChanged();
          }
        }

        @Override
        public void redo() {
          final H2dTelemacBoundary b = find();
          if (b != null) {
            b.setVariableTransient(_varEvol);
            fireEvolutionUsedChanged();
          }
        }
      };
    }
    return null;
  }

  /**
   * @param _b le bord concerne
   * @param _newType le nouveau type du bord
   * @return la commande ou null si aucune modif.
   */
  public CtuluCommand setLiquidBordTypeAndCmd(final H2dBoundary _b, final H2dBoundaryType _newType) {
    return setLiquidBordTypeEvolAndCmd(_b, _newType, Collections.<H2dVariableType, List<EvolutionReguliereAbstract>> emptyMap());
  }

  /**
   * @param _b le bord a modifier
   * @param _newType le nouveau type du bord
   * @param _evol les evolutions a utiliser (variable->Evol)
   * @return la commande ou null si aucune modif
   */
  public CtuluCommand setLiquidBordTypeEvolAndCmd(final H2dBoundary _b, final H2dBoundaryType _newType,
      final Map<H2dVariableType, List<EvolutionReguliereAbstract>> _evol) {

    final int i = getIdxLiquidBord(_b);
    if (i < 0) {
      return null;
    }
    final H2dTelemacBoundary old = ((H2dTelemacBoundary) _b).createMemento();
    final boolean changedType = changeLiquidBordType(_b, _newType);
    final boolean isEvolChanged = ((H2dTelemacBoundary) _b).setVariableTransient(_evol);
    if (changedType || isEvolChanged) {
      fireEvolutionUsedChanged();
      final CtuluCommandCompositeInverse r = new CtuluCommandCompositeInverse();
      r.addCmd(new CtuluCommand() {

        private H2dTelemacBoundary find() {
          if (liquidBorder_ == null) {
            return null;
          }
          for (H2dTelemacBoundary boundary : liquidBorder_) {
            if ((boundary.getIdxDeb() == old.getIdxDeb()) && (boundary.getIdxFin() == old.getIdxFin())) {
              return boundary;
            }
          }
          return null;
        }

        @Override
        public void undo() {
          final H2dTelemacBoundary b = find();
          if (b != null) {
            b.updateFrom(old, H2dTelemacBcManager.this);
            fireEvolutionUsedChanged();
          }
        }

        @Override
        public void redo() {
          final H2dTelemacBoundary b = find();
          if (b != null) {
            changeLiquidBordType(b, _newType);
            if (isEvolChanged) {
              ((H2dTelemacBoundary) b).setVariableTransient(_evol);
            }
            fireEvolutionUsedChanged();
          }
        }
      });
      r.addCmd(updateAllParameterWhenStructureChanged());
      return r.getSimplify();
    }
    return null;
  }

  protected void fireBoundaryParametersChanged(final H2dTelemacBoundary _b) {
    super.fireBcParametersChanged(_b);
  }

  /**
   * @param _frontier la frontiere modifiee
   */
  protected void fireFrontierStructureChange(final H2dBcFrontierBlockAbstract _frontier) {
    super.fireBcFrontierStructureChanged(_frontier);
  }

  @Override
  protected void fireParametersForBoundaryPtsChange() {
    super.fireParametersForBoundaryPtsChange();
  }

  @Override
  protected void fireParametersForBoundaryPtsChange(final H2dVariableType _t) {
    super.fireParametersForBoundaryPtsChange(_t);
  }

  /**
   * @param _b le bord a modifier
   * @param _newType le nouveau type pour le bord
   * @return true si l'action a eu lieu
   */
  public boolean changeLiquidBordType(final H2dBoundary _b, final H2dBoundaryType _newType) {
    if ((_newType.isLiquide()) && (_b.getType().isLiquide()) && (_b.getType() != _newType)) {
      super.setBordType(_b, _newType);
      return true;
    }
    return false;
  }

  /**
   * @return le nombre de parametre impose
   */
  public int getNbPrescribedParameters() {
    return prescribedParameters_.length;
  }

  /**
   * @param _i l'indice du parametre impose
   * @return le parametre impose correspondant
   */
  public H2dTelemacBcParameter getPrescribedParameters(final int _i) {
    return prescribedParameters_[_i];
  }

  private H2dTelemacBcParameter getPrescribedFor(final DicoEntite _ent) {
    for (int i = prescribedParameters_.length - 1; i >= 0; i--) {
      if (prescribedParameters_[i].getEntite() == _ent) {
        return prescribedParameters_[i];
      }
    }
    for (int i = prescribedOptions_.length - 1; i >= 0; i--) {
      if (prescribedOptions_[i].getEntite() == _ent) {
        return prescribedOptions_[i];
      }
    }
    return null;
  }

  /**
   * @param _e
   * @return true si le mot-cle est utilis� pour d�finir des comportements de bords.
   */
  public boolean isKeywordUsedByBc(final DicoEntite _e) {
    return getPrescribedFor(_e) != null;
  }

  /**
   * @return la source correspondante utilisee pour les sauvegardes
   */
  public H2dTelemacCLSourceInterface createCLSourceInterface() {
    return new H2dTelemacCLSourceInterface() {

      final int[] tmp_ = new int[2];

      H2dTelemacBoundary bord_;

      @Override
      public int getNbLines() {
        return getNbTotalPoint();
      }

      @Override
      public boolean getLine(final int _i, final H2dTelemacCLElementSource _ele) {
        final boolean b = getGrid().getFrontiers().getIdxFrontierIdxPtInFrontierFromFrontier(_i, tmp_);
        if (!b) {
          return false;
        }
        bord_ = (H2dTelemacBoundary) getBlockFrontier(tmp_[0]).getBordContainingIdx(tmp_[1]);
        final H2dTelemacBoundaryCondition cl = (H2dTelemacBoundaryCondition) getBlockFrontier(tmp_[0]).getCl(tmp_[1]);
        if (bord_ == null) {
          return false;
        }
        _ele.initFrom(cl, bord_.getUType(), bord_.getVType(),
        /* bord.getHType(), */
        bord_.getTracerType(), bord_.getType());
        return true;
      }
    };
  }

  @Override
  public CtuluCommand keywordAddedOrUpdated(final DicoEntite _ent, String oldValue, String newValue) {
    final H2dTelemacBcParameter p = getPrescribedFor(_ent);
    if (p != null) {
      p.computeValueWhenKeywordChanged(this);
    }
    return null;
  }

  @Override
  public void shiftTracersValues(int[] idx, CtuluCommandContainer _cmd) {
    if (liquidBorder_ == null) {
      return;
    }
    CtuluCommandComposite cmp = new CtuluCommandComposite();
    String value = getTracerDefaultValue();
    for (H2dTelemacBoundary boundary : liquidBorder_) {
      boundary.setNbTracer(getNbTraceurs(), value, idx, cmp);
    }
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }

  }

  /**
   * @return Returns the evolListener.
   */
  public H2dTelemacEvolutionUsedListener getEvolListener() {
    return evolListener_;
  }

  /**
   * @param _evolListener The evolListener to set.
   */
  public void setEvolListener(final H2dTelemacEvolutionUsedListener _evolListener) {
    evolListener_ = _evolListener;
  }

  public String getTracerName(int idx) {
    return tracerManager.getTraceurName(idx);
  }
}