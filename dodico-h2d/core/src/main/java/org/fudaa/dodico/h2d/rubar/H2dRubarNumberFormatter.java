/*
 * @creation 27 sept. 2004
 *
 * @modification $Date: 2006-11-20 14:23:55 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluNumberFormater;
import org.fudaa.ctulu.fileformat.FortranLib;
import org.fudaa.dodico.h2d.type.H2dRubarFileType;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarNumberFormatter.java,v 1.17 2006-11-20 14:23:55 deniger Exp $
 */
public class H2dRubarNumberFormatter {
  /**
   * Format F8.3 pour nombre positif.
   */
  private static final CtuluNumberFormater F_8_3 = FortranLib.getFormater(8, 3, false);
  public static final DecimalFormat DEFAULT_XY_FMT = CtuluLib.getDecimalFormat();

  {
    DEFAULT_XY_FMT.setMaximumFractionDigits(6);
  }

  private CtuluNumberFormater formatterXY;
  private CtuluNumberFormater formatterXYDonneesBrutes;
  public final static CtuluNumberFormater TIME = FortranLib.getFormater(15, 3, false);
  private final Map variableNumberFormat = new HashMap(10);
  int nbDecForXY = 1;
  H2dRubarFileType xyFormat;

  /**
   * @param _is4 true si x,y comporte 4 d�cimales
   */
  public H2dRubarNumberFormatter(final boolean _is4, H2dRubarFileType xyFormat) {
    if (_is4) {
      nbDecForXY = 4;
    }
    this.xyFormat = xyFormat;
  }

  public void setXyFormat(H2dRubarFileType xyFormat) {
    if (xyFormat != this.xyFormat) {
      this.xyFormat = xyFormat;
      formatterXYDonneesBrutes = null;
      formatterXY = null;
    }
  }

  private int getNbXYChars() {
    if (xyFormat == H2dRubarFileType.NEW_FORMAT_9_DIGITS) {
      return 12;
    }
    return 8;
  }

  private void buildFmt() {
    createFrottement();
    createDebit();
    createHauteur();
    createCote();
    createConcentration();
    createBathy();
    createDiffusion();
    variableNumberFormat.put(H2dVariableTransType.EPAISSEUR, FortranLib.getFormater(12, 6, true));
    variableNumberFormat.put(H2dVariableTransType.DIAMETRE, FortranLib.getFormater(10, 7, true));
    variableNumberFormat.put(H2dVariableTransType.ETENDUE, FortranLib.getFormater(10, 7, true));
    variableNumberFormat.put(H2dVariableTransType.CONTRAINTE, FortranLib.getFormater(15, 5, false));
  }

  private void createFrottement() {
    final CtuluNumberFormater frottement = FortranLib.getFormater(8, 3, true);
    variableNumberFormat.put(H2dVariableType.COEF_FROTTEMENT, frottement);
    variableNumberFormat.put(H2dVariableType.COEF_FROTTEMENT_FOND, frottement);
  }

  public CtuluNumberFormater getCINFormat(final H2dVariableType _t) {
    if (_t == H2dVariableType.HAUTEUR_EAU) {
      return FortranLib.getFormater(10, 4, true);
    }
    if (_t == H2dVariableType.COTE_EAU) {
      return FortranLib.getFormater(10, 3, false);
    }
    if ((_t == H2dVariableTransType.CONCENTRATION)) {
      return FortranLib.getFormater(10, 7, true);
    }
    return FortranLib.getFormater(10, 5, false);
  }

  /**
   * @param _id l'identifiant (en general une variable
   * @return le format correspondant
   */
  public CtuluNumberFormater getFormatterFor(final H2dVariableType _id) {
    if (variableNumberFormat.size() == 0) {
      buildFmt();
    }
    if (_id.getParentVariable() != null) {
      return (CtuluNumberFormater) variableNumberFormat.get(_id.getParentVariable());
    }
    return (CtuluNumberFormater) variableNumberFormat.get(_id);
  }

  private void createHauteur() {
    variableNumberFormat.put(H2dVariableType.HAUTEUR_EAU, FortranLib.getFormater(8, 4, true));
  }

  private void createDiffusion() {
    variableNumberFormat.put(H2dVariableType.COEF_DIFFUSION, FortranLib.getFormater(8, 5, true));
  }

  private void createCote() {
    variableNumberFormat.put(H2dVariableType.COTE_EAU, F_8_3);
  }

  private void createConcentration() {
    final CtuluNumberFormater concentration = FortranLib.getFormater(10, 7, true);
    variableNumberFormat.put(H2dVariableTransType.CONCENTRATION, concentration);
  }

  /**
   * @return les formats a utiliser pour le frottement
   */
  public final CtuluNumberFormater getFrottement() {
    if (variableNumberFormat.size() == 0) {
      buildFmt();
    }
    return (CtuluNumberFormater) variableNumberFormat.get(H2dVariableType.COEF_FROTTEMENT);
  }

  private void createDebit() {
    variableNumberFormat.put(H2dVariableType.DEBIT, FortranLib.getFormater(8, 5, false));
  }

  private void createBathy() {
    if (nbDecForXY == 4) {
      variableNumberFormat.put(H2dVariableType.BATHYMETRIE, FortranLib.getFormater(8, 4, false));
    } else {
      variableNumberFormat.put(H2dVariableType.BATHYMETRIE, F_8_3);
    }
    variableNumberFormat.put(H2dVariableTransType.FOND_INERODABLE, F_8_3);
  }

  public final CtuluNumberFormater getTime() {
    return TIME;
  }

  public static CtuluNumberFormater createNumberFormateur(final int _nbField, final int _nbDec) {
    if (_nbField == 8 && _nbDec == 3) {
      return F_8_3;
    }
    return FortranLib.getFormater(_nbField, _nbDec);
  }

  /**
   * @return les formats a utiliser pour x,y
   */
  public final CtuluNumberFormater getXY() {
    if (formatterXY == null) {
      formatterXY = createNumberFormateur(getNbXYChars(), nbDecForXY);
    }
    return formatterXY;
  }

  /**
   * @return le formatteur pour xy, dans le cas des fichiers donn�es brutes
   */
  public final CtuluNumberFormater getXYDb() {
    if (formatterXYDonneesBrutes == null) {
      formatterXYDonneesBrutes = createNumberFormateur(getNbXYChars(), 1);
    }
    return formatterXYDonneesBrutes;
  }
}
