/*
 * @creation 27 sept. 06
 * @modification $Date: 2006-10-19 14:12:32 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import org.fudaa.ctulu.CtuluRange;

/**
 * @author fred deniger
 * @version $Id: H2dParallelogrammeDataAbstract.java,v 1.2 2006-10-19 14:12:32 deniger Exp $
 */
public abstract class H2dParallelogrammeDataAbstract implements H2dParallelogrammeDataInterface {

  double x1_;
  double x2_;
  double x4_;
  double y1_;
  double y2_;
  double y4_;

  /**
   * @return le max x.
   */
  public final double getXMax() {
    double r = x1_;
    if (x2_ > r) {
      r = x2_;
    }
    if (x4_ > r) {
      r = x4_;
    }
    return r;
  }

  /**
   * @param _r l'objet a initialise avec le min/Max.
   */
  public final void getXrange(final CtuluRange _r) {
    _r.max_ = x1_;
    _r.min_ = x1_;
    if (x2_ > _r.max_) {
      _r.max_ = x2_;
    }
    if (x2_ < _r.min_) {
      _r.min_ = x2_;
    }
    if (x4_ < _r.min_) {
      _r.min_ = x4_;
    }
    if (x4_ > _r.max_) {
      _r.max_ = x4_;
    }
    final double x3 = x2_ - x1_ + x4_;
    if (x3 < _r.min_) {
      _r.min_ = x3;
    }
    if (x3 > _r.max_) {
      _r.max_ = x3;
    }

  }

  /**
   * @param _r l'objet a initialise avec le min/Max.
   */
  public final void getYrange(final CtuluRange _r) {
    _r.max_ = y1_;
    _r.min_ = y1_;
    if (y2_ > _r.max_) {
      _r.max_ = y2_;
    }
    if (y4_ > _r.max_) {
      _r.max_ = y4_;
    }
    if (y2_ < _r.min_) {
      _r.min_ = y2_;
    }
    if (y4_ < _r.min_) {
      _r.min_ = y4_;
    }
    final double y3 = y2_ - y1_ + y4_;
    if (y3 < _r.min_) {
      _r.min_ = y3;
    }
    if (y3 > _r.max_) {
      _r.max_ = y3;
    }
  }

  /**
   * @return le max y.
   */
  public final double getYMax() {
    double r = y1_;
    if (y2_ > r) {
      r = y2_;
    }
    if (y4_ > r) {
      r = y4_;
    }
    return r;
  }

  /**
   * @param _i appartient a [0;3]
   * @return la valeur de x
   */
  public final double getX(final int _i) {
    if (_i == 0) {
      return x1_;
    }
    if (_i == 1) {
      return x2_;
    }
    if (_i == 3) {
      return x4_;
    }
    if (_i == 2) {
      return x2_ - x1_ + x4_;
    }
    new Throwable().printStackTrace();
    return -1;
  }

  /**
   * @param _i appartient a [0;3]
   * @return la valeur de y
   */
  public final double getY(final int _i) {
    if (_i == 0) {
      return y1_;
    }
    if (_i == 1) {
      return y2_;
    }
    if (_i == 3) {
      return y4_;
    }
    if (_i == 2) {
      return y2_ - y1_ + y4_;
    }
    new Throwable().printStackTrace();
    return -1;
  }

  @Override
  public final double getX1() {
    return x1_;
  }

  @Override
  public final double getX2() {
    return x2_;
  }

  @Override
  public final double getX4() {
    return x4_;
  }

  @Override
  public final double getY1() {
    return y1_;
  }

  @Override
  public final double getY2() {
    return y2_;
  }

  @Override
  public final double getY4() {
    return y4_;
  }

  protected void setX1(final double _x1) {
    x1_ = _x1;
  }

  protected void setX2(final double _x2) {
    x2_ = _x2;
  }

  protected void setX4(final double _x4) {
    x4_ = _x4;
  }

  protected void setY1(final double _y1) {
    y1_ = _y1;
  }

  protected void setY2(final double _y2) {
    y2_ = _y2;
  }

  protected void setY4(final double _y4) {
    y4_ = _y4;
  }

}
