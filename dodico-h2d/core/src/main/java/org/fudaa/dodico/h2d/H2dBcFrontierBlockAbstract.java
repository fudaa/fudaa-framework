/**
 * @creation 13 nov. 2003
 * @modification $Date: 2007-06-05 08:59:12 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import com.memoire.fu.FuLog;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dBoundaryTypeCommon;

/**
 * Gere les bords (ensemble de points adjacents) d'une frontiere.
 *
 * @author deniger
 * @version $Id: H2dBcFrontierBlockAbstract.java,v 1.22 2007-06-05 08:59:12 deniger Exp $
 */
public abstract class H2dBcFrontierBlockAbstract implements H2dBcFrontierBlockInterface {

  protected List bords_;

  protected int maillageBordIdx_;

  /**
   * @param _l la liste des bords sur cette frontiere
   * @param _i l'index de la frontiere
   */
  public H2dBcFrontierBlockAbstract(final List _l, final int _i) {
    bords_ = _l;
    maillageBordIdx_ = _i;
  }

  public boolean contains(final H2dBoundary _b) {
    return (bords_ != null) && (bords_.contains(_b));
  }

  /**
   * @return l'indice de la frontiere
   */
  public int getFrIdx() {
    return maillageBordIdx_;
  }

  public double[] getAbsCurviligne(final EfGridInterface _grid, final H2dBoundary _boundary) {
    return _grid.getFrontiers().getAbsCurviligne(_grid, maillageBordIdx_, _boundary.getIdxDeb(),
        _boundary.getNPointInBord(getNbPt()));
  }

  /**
   * @param _idx l'indice non compris entre [0,getNbPt[
   * @return indice entre [0,getNbPt[ resultat division euclidienne.
   */
  public int getReelIdxOnFr(final int _idx) {
    if (_idx < 0) {
      return getNbPt() + _idx % getNbPt();
    }
    if (_idx >= getNbPt()) {
      return _idx % getNbPt();
    }
    return _idx;
  }

  protected void setFirstBord() {
    if (bords_.size() == 0) {
      final H2dBoundary b = createDefaultBoundary();
      b.setIdxDeb(0);
      b.setIdxFin(0);
      bords_.add(b);
    } else if (bords_.size() == 1) {
      final H2dBoundary b = (H2dBoundary) bords_.get(0);
      b.setIdxDeb(0);
      b.setIdxFin(0);
      return;
    } else {
      H2dBoundary prec = (H2dBoundary) bords_.get(0);
      if (!prec.containsZeroIndex()) {
        prec = (H2dBoundary) bords_.get(bords_.size() - 1);
        if (prec.containsZeroIndex()) {
          bords_.add(0, bords_.remove(bords_.size() - 1));
        } else if (((H2dBoundary) bords_.get(1)).containsZeroIndex()) {
          bords_.add(bords_.remove(0));
        }
        prec = (H2dBoundary) bords_.get(0);
        if (!prec.containsZeroIndex()) {
          FuLog.error("DH2: The first bord must contains zero idx");
        }
      }
    }
  }

  /**
   * @param _t le type du bord voulu
   * @return le bord avec le type _t et initialise par defaut.
   */
  public abstract H2dBoundary createBord(H2dBoundaryType _t);

  /**
   * @param _t le bord a dupliquer
   * @return le bord "clone"
   */
  public abstract H2dBoundary createMementoFor(H2dBoundary _t);

  /**
   * @param _t le bord a dupliquer
   * @return le bord clone
   */
  public abstract H2dBoundary createNewBordFrom(H2dBoundary _t);

  protected boolean insertSolid(final int _min, final int _max, final int _bord) {
    if ((_min == _max) || ((H2dBoundary.getNbPointInBord(_min, _max, getNbPt()) < 3))) {
      return false;
    }
    final H2dBoundary b = (H2dBoundary) bords_.get(_bord);
    if (b.getType().isLiquide()) {
      final int fin = b.getIdxFin();
      b.setIdxFin(_min);
      final H2dBoundary bsolid = createBord(H2dBoundaryTypeCommon.SOLIDE);
      bsolid.setBoundaryType(H2dBoundaryTypeCommon.SOLIDE, null);
      bsolid.setIdxDeb(_min);
      bsolid.setIdxFin(_max);
      final H2dBoundary newLiquid = createNewBordFrom(b);
      newLiquid.setIdxDeb(_max);
      newLiquid.setIdxFin(fin);
      bords_.add(_bord + 1, newLiquid);
      bords_.add(_bord + 1, bsolid);
      setFirstBord();
      return true;
    }
    return false;
  }

  protected H2dBoundary getLiquidBordIntersectBy(final int _min, final int _max) {
    final int nb = getNbPt();
    for (int i = bords_.size() - 1; i >= 0; i--) {
      final H2dBoundary b = getBord(i);
      if ((b.isIntersectBy(_min, _max, nb) >= 0) && (b.getType().isLiquide())) {
        return b;
      }
    }
    return null;
  }

  protected boolean increaseLiquidBorderIntern(final int _min, final int _max) {
    final int n = bords_.size();
    if (n == 1) {
      return false;
    }
    final int nbPt = getNbPt();
    final List newBord = new ArrayList(n);
    boolean liquidBordFound = false;
    for (int i = 0; i < n; i++) {
      final H2dBoundary b = createMementoFor((H2dBoundary) bords_.get(i));
      final int intersect = b.isIntersectBy(_min, _max, nbPt);
      newBord.add(b);
      if (intersect >= 0) {
        if (intersect == H2dBoundary.INTERSECT_INCLUDE_IN_BOUNDARY) {
          return false;
        }
        if (b.getType().isLiquide()) {
          if (liquidBordFound) {
            return false;
          }
          liquidBordFound = true;
          if (intersect == H2dBoundary.INTERSECT_CONTAINS_BOUNDARY) {
            b.setIdxDeb(_min);
            b.setIdxFin(_max);
          } else {
            if (b.isBeginIdxSelected(_min, _max)) {
              b.setIdxDeb(_min);
            } else {
              b.setIdxFin(_max);
            }
          }
        } else {
          if (intersect == H2dBoundary.INTERSECT_CONTAINS_BOUNDARY) {
            return false;
          }
          if (b.isBeginIdxSelected(_min, _max)) {
            if ((_max == b.getIdxFin()) || (H2dBoundary.getNbPointInBord(_max, b.getIdxFin(), nbPt) < 3)) {
              return false;
            }
            b.setIdxDeb(_max);
          } else {
            if ((_min == b.getIdxDeb()) || (H2dBoundary.getNbPointInBord(b.getIdxDeb(), _min, nbPt) < 3)) {
              return false;
            }
            b.setIdxFin(_min);
          }

        }
      }
    }
    bords_ = newBord;
    setFirstBord();
    return true;
  }

  /**
   * @param _min l'indice min du nouveau bord liquide
   * @param _max l'indice max du nouveau bord liquide
   * @param _b le type du bord a inserer
   * @return null si pas de bord insere ou le bord insere
   */
  protected boolean insertLiquid(final int _min, final int _max, final H2dBoundaryType _b) {
    final int n = bords_.size();
    final int nbPt = getNbPt();
    if ((_min == _max) || (H2dBoundary.getNbPointInBord(_min, _max, nbPt) < 2) || (_b.isSolide())) {
      return false;
    }
    if (n == 1) {
      final H2dBoundary b = (H2dBoundary) bords_.get(0);
      if (b.getType().isSolide()) {
        b.setIdxDeb(_max);
        b.setIdxFin(_min);
        final H2dBoundary newBord = createBord(_b);
        newBord.setIdxDeb(_min);
        newBord.setIdxFin(_max);
        if (newBord.isFirst()) {
          bords_.add(0, newBord);
        } else {
          bords_.add(newBord);
        }
        return true;
      }
      return false;
    }
    for (int i = 0; i < n; i++) {
      final H2dBoundary b = (H2dBoundary) bords_.get(i);
      if ((b.getType().isSolide()) && (b.isIntersectBy(_min, _max, nbPt) == H2dBoundary.INTERSECT_INCLUDE_IN_BOUNDARY)) {
        final int fin = b.getIdxFin();
        b.setIdxFin(_min);
        final H2dBoundary newBord = createBord(_b);
        newBord.setIdxDeb(_min);
        newBord.setIdxFin(_max);
        final H2dBoundary newBordSolid = createBord(H2dBoundaryTypeCommon.SOLIDE);
        newBordSolid.setIdxDeb(_max);
        newBordSolid.setIdxFin(fin);
        if ((i == 0) && (b.getIdxDeb() != 0)) {
          if (b.isFirst()) {
            bords_.add(1, newBordSolid);
            bords_.add(1, newBord);
          } else {
            bords_.remove(i);
            bords_.add(b);
            if (newBord.isFirst()) {
              bords_.add(0, newBordSolid);
              bords_.add(0, newBord);
            } else {
              bords_.add(newBord);
              bords_.add(0, newBordSolid);
              if (!newBordSolid.isFirst()) {
                new Throwable().printStackTrace();
              }
            }
          }
        } else if (i == n - 1) {
          bords_.add(newBord);
          bords_.add(newBordSolid);
        } else {
          bords_.add(i + 1, newBordSolid);
          bords_.add(i + 1, newBord);
        }
        if (!((H2dBoundary) bords_.get(0)).containsZeroIndex()) {
          new Throwable().printStackTrace();
        }
        return true;
      }
    }
    return false;
  }

  /**
   * @return un nouveau bord par defaut
   */
  public abstract H2dBoundary createDefaultBoundary();

  @Override
  public final int getNbBord() {
    return bords_.size();
  }

  @Override
  public final H2dBoundary getBord(final int _idxBord) {
    if (_idxBord >= bords_.size()) {
      return null;
    }
    return (H2dBoundary) bords_.get(_idxBord);
  }

  /**
   * @param _s la liste a remplir
   */
  @Override
  public final void fillWithUsedBoundaryType(final Set _s) {
    for (int i = bords_.size() - 1; i >= 0; i--) {
      _s.add(getBord(i).getType());
    }
  }

  @Override
  public H2dBoundary getBordContainingIdx(final int _indexOnThisFrontier) {
    if (_indexOnThisFrontier < 0) {
      return null;
    }
    final int n = bords_.size();
    if (n == 1) {
      return (H2dBoundary) bords_.get(0);
    }
    for (int i = 0; i < n; i++) {
      final H2dBoundary b = (H2dBoundary) bords_.get(i);
      if (b.containsIdx(_indexOnThisFrontier)) {
        return b;
      } else if ((_indexOnThisFrontier == b.getIdxFin()) && (i < n)) {
        return getBord(i + 1);
      }
    }
    return null;
  }

  /**
   * @param _idx les indices des points devant appartenir au bord
   * @return le bord unique contenant tous les points du tableau
   */
  public H2dBoundary getUniqueSelectedBord(final int[] _idx) {
    if ((_idx == null) || (_idx.length == 0)) {
      return null;
    }
    final int n = _idx.length;
    final H2dBoundary b = getBordContainingIdx(_idx[0]);
    if (n == 1) {
      return b;
    }
    for (int i = n - 1; i > 0; i--) {
      if (!b.containsIdx(_idx[i])) {
        return null;
      }
    }
    return b;
  }

  public int getBoundaryPosition(final H2dBoundary _b) {
    if ((_b != null) && (bords_ != null)) {
      return bords_.indexOf(_b);
    }
    return -1;
  }

  /**
   * @param _deb l'indice de debut
   * @param _fin l'indice de fin
   * @return le bord ayant comme indice de debut _deb et de fin _fin. null si aucun
   */
  public H2dBoundary getBord(final int _deb, final int _fin) {
    for (int i = bords_.size() - 1; i >= 0; i--) {
      final H2dBoundary b = getBord(i);
      if ((b.getIdxFin() == _fin) && (b.getIdxDeb() == _deb)) {
        return b;
      }
    }
    return null;
  }

  protected boolean removeLiquidBoundaryIntern(final int _min, final int _max) {
    int n = bords_.size();
    if (n == 1) {
      final H2dBoundary b = (H2dBoundary) bords_.get(0);
      if (b.getType().isLiquide()) {
        return insertSolid(_min, _max, 0);
      }
      return false;
    }
    final int nbPt = getNbPt();
    if ((_min == 0) && (_max == nbPt - 1)) {
      bords_.clear();
      final H2dBoundary b = createBord(H2dBoundaryTypeCommon.SOLIDE);
      b.setIdxDeb(0);
      b.setIdxFin(0);
      bords_.add(b);
    }
    boolean r = false;
    boolean first;
    boolean twoSolidBord = false;
    final List newList = new ArrayList(n + 2);
    boolean change = false;
    for (int i = 0; i < n; i++) {
      final H2dBoundary b = (H2dBoundary) bords_.get(i);
      if (b.getType().isLiquide()) {
        final int inters = b.isIntersectBy(_min, _max, nbPt);
        if (inters >= 0) {
          if (inters == 0) {
            H2dBoundary bsolid = null;
            first = b.isBeginIdxSelected(_min, _max);
            change = false;
            if (first) { // b.setIdxDeb((_max == (nbPt - 1) ? 0 : _max + 1));
              if (_min == _max) {
                change = true;
                b.setIdxDeb((_max == nbPt - 1 ? 0 : (_max + 1)));
                r = true;
              } else {
                if (b.getIdxDeb() == _max) { // no change
                  newList.add(b);
                } else {
                  r = true;
                  change = true;
                  b.setIdxDeb(_max);
                }
              }
              if (change) {
                if (i == 0) {
                  bsolid = (H2dBoundary) bords_.get(nbPt - 1);
                } else {
                  bsolid = (H2dBoundary) bords_.get(i - 1);
                }
                if (b.getNPointInBord(nbPt) <= 1) {
                  bsolid.setIdxFin(b.getIdxFin());
                  twoSolidBord = true;
                } else {
                  bsolid.setIdxFin(b.getIdxDeb());
                  newList.add(b);
                }
              }
            } else { // b.setIdxFin(_min == 0 ? nbPt - 1 : _min - 1));
              if (_min == _max) {
                r = true;
                change = true;
                b.setIdxFin((_max == 0 ? nbPt - 1 : (_max - 1)));
              } else {
                if (b.getIdxFin() == _min) { // no change
                  newList.add(b);
                } else {
                  r = true;
                  change = true;
                  b.setIdxFin(_min);
                }
              }
              if (change) {
                if (i < (n - 1)) {
                  bsolid = (H2dBoundary) bords_.get(i + 1);
                } else {
                  bsolid = (H2dBoundary) bords_.get(0);
                }
                if ((b.getIdxDeb() == b.getIdxFin()) || (b.getNPointInBord(nbPt) <= 1)) {
                  bsolid.setIdxDeb(b.getIdxDeb());
                  twoSolidBord = true;
                } else {
                  bsolid.setIdxDeb(b.getIdxFin());
                  newList.add(b);
                }
              }
            }
            if ((bsolid != null) && (bsolid.getType().isLiquide())) {
              new Throwable().printStackTrace();
            }
          } else if (inters == 1) {
            return insertSolid(_min, _max, i);
          } else if (inters == 2) {
            r = true;
            twoSolidBord = true;
          }
        } else {
          newList.add(b);
        }
      } else {
        newList.add(b);
      }
    }
    if (r) {
      if (twoSolidBord) {
        n = newList.size();
        for (int i = n - 1; i >= 0; i--) {
          final H2dBoundary bEnCours = (H2dBoundary) newList.get(i);
          if (bEnCours.getType().isSolide()) {
            final H2dBoundary bPrec = (H2dBoundary) newList.get((i == 0 ? (newList.size() - 1) : (i - 1)));
            if (bPrec.getType() == bEnCours.getType()) {
              bPrec.setIdxFin(bEnCours.getIdxFin());
              newList.remove(i);
            }
          }
        }
      }
      bords_ = newList;
    }
    setFirstBord();
    return r;
  }

  /**
   * @param _indexToSearch les indices qui doivent etre recherches
   * @return les bords contenant au moins un indice du tableau
   */
  public Collection getBordsContainingIdx(final int[] _indexToSearch) {
    if ((_indexToSearch == null) || (_indexToSearch.length == 0)) {
      return null;
    }
    final int n = bords_.size();
    final Set l = new HashSet(n);
    if (n == 1) {
      l.add(bords_.get(0));
      return l;
    }
    for (int j = _indexToSearch.length - 1; j >= 0; j--) {
      for (int i = 0; i < n; i++) {
        final H2dBoundary b = (H2dBoundary) bords_.get(i);
        if (b.containsIdx(_indexToSearch[j])) {
          l.add(b);
        }
      }
    }
    return (l.size() == 0) ? null : l;
  }

  protected boolean removeBordLiquid(final int _idx) {
    final int n = bords_.size();
    if (!getBord(_idx).getType().isLiquide()) {
      return false;
    }
    if (n == 1) {
      ((H2dBoundary) bords_.get(0)).setBoundaryType(H2dBoundaryTypeCommon.SOLIDE, null);
    } else if (n == 2) {
      bords_.remove(_idx);
      final H2dBoundary b = ((H2dBoundary) bords_.get(0));
      b.setIdxDeb(0);
      b.setIdxFin(0);
    } else {
      final H2dBoundary prec = (H2dBoundary) bords_.get(((_idx == 0) ? n - 1 : _idx - 1));
      final int suivIdx = ((_idx == (n - 1)) ? 0 : _idx + 1);
      final H2dBoundary suiv = (H2dBoundary) bords_.get(suivIdx);
      bords_.remove(_idx);
      prec.setIdxFin(suiv.getIdxFin());
      bords_.remove(suiv);
    }
    setFirstBord();
    return true;
  }
}