/*
 *  @creation     30 nov. 2004
 *  @modification $Date: 2007-05-04 13:46:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.dodico.dico.DicoEntite;

/**
 * @author Fred Deniger
 * @version $Id: H2dtelemacSiponListener.java,v 1.4 2007-05-04 13:46:37 deniger Exp $
 */
public interface H2dtelemacSiponListener {

  /**
   * Des sources ont ete modifiees.
   * 
   * @param _keyword le mot-cl� concern�
   */
  void sourcesChanged(DicoEntite _keyword);

  /**
   * Des siphons ont ete modifies.
   * 
   * @param _connexionChanged true si des connexions ont ete modifie
   */
  void culvertValueChanged(boolean _connexionChanged);

  /**
   * siphons ajoutes.
   */
  void culvertAdded();

  /**
   * siphons enleves.
   */
  void culvertRemoved();

}
