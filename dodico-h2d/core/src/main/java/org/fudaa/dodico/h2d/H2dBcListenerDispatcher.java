/*
 *  @creation     18 mai 2004
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: H2dBcListenerDispatcher.java,v 1.6 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dBcListenerDispatcher {

  /**
   * @param _b le bord modifie
   * @param _t la variable modifiee
   */
  void fireBcParametersChanged(H2dBoundary _b, H2dVariableType _t);

  /**
   * Call when the parameters (friction,velocity,..) is changed for some points.
   *
   * @param _t if not null, represent the variable changed
   */
  void fireBcPointsParametersChanged(H2dVariableType _t);

  /**
   * @param b le bord modifie
   * @param _old l'ancien type de cette frontiere
   */
  void fireBcTypeChanged(H2dBoundary _b, H2dBoundaryType _old);

  /**
   * Call when the boundary disposition is changed for a frontier.
   *
   * @param _b la frontiere modifiee
   */
  void fireBcFrontierStructureChanged(H2dBcFrontierInterface _b);

}
