/*
 * @creation 28 ao�t 2003
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;

/**
 * @author deniger
 * @version $Id: H2dEvolutionFrontiereLiquideMutable.java,v 1.12 2006-09-19 14:43:25 deniger Exp $
 */
public class H2dEvolutionFrontiereLiquideMutable extends H2dEvolutionFrontiereLiquide {
  
  

  /**
   * @param _t la variable
   * @param _idx l'indice du bord concerne
   */
  public H2dEvolutionFrontiereLiquideMutable(final H2dVariableType _t, final int _idx) {
    this(_t, _idx, new EvolutionReguliere());
  }

  /**
   * @param _t la variable
   * @param _idx l'indice du bord concerne
   * @param _e l'evolution support
   */
  public H2dEvolutionFrontiereLiquideMutable(final H2dVariableType _t, final int _idx, final EvolutionReguliere _e) {
    super(_t, _idx, _e);
  }

  /**
   * @param _t le pas de temps a ajoute
   * @param _v la valeur pour ce pas de temps
   */
  public void add(final double _t, final double _v) {
    evol_.add(_t, _v);
  }

  @Override
  public int getPasTempNb() {
    return evol_.getNbValues();
  }

  /**
   * @param _s le nouveau nom de l'evolution
   */
  public void setNom(final String _s) {
    evol_.setNom(_s);
  }

  /**
   * @param _s la nouvelle unite
   */
  public void setUnite(final String _s) {
    evol_.setUnite(_s);
  }

}