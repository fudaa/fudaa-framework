/*
 * @creation 28 sept. 2004
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

/**
 * @author Fred Deniger
 * @version $Id: H2dRegularGrid.java,v 1.6 2006-09-19 14:43:25 deniger Exp $
 */
public class H2dRegularGrid implements H2dRegularGridInterface {

  double dx_;
  double dy_;

  int nbX_;
  int nbY_;
  double x0_;
  double y0_;

  /**
   * Initialisation par defaut a 0.
   */
  public H2dRegularGrid() {

  }

  /**
   * @param _g la grille utilisee pour l'initialisation
   */
  public H2dRegularGrid(final H2dRegularGridInterface _g) {
    initFromGrid(_g);
  }

  public double getXmax() {
    return x0_ + (nbX_ - 1) * dx_;
  }

  public double getXmin() {
    return x0_;
  }

  public double getYmin() {
    return y0_;
  }

  public double getYmax() {
    return y0_ + (nbY_ - 1) * dy_;
  }

  /**
   * @param _nbX
   * @param _nbY
   * @param _dx
   * @param _dy
   * @param _x0
   * @param _y0
   */
  public H2dRegularGrid(final int _nbX, final int _nbY, final double _dx, final double _dy, final double _x0, final double _y0) {
    nbX_ = _nbX;
    nbY_ = _nbY;
    dx_ = _dx;
    dy_ = _dy;
    x0_ = _x0;
    y0_ = _y0;
  }

  protected final void initFromGrid(final H2dRegularGridInterface _g) {
    nbX_ = _g.getNbPtOnX();
    nbY_ = _g.getNbPtOnY();
    dx_ = _g.getDX();
    dy_ = _g.getDY();
    x0_ = _g.getX0();
    y0_ = _g.getY0();
  }

  @Override
  public double getDX() {
    return dx_;
  }

  @Override
  public double getDY() {
    return dy_;
  }

  /**
   * @return le nombre de point de la grille
   */
  @Override
  public int getNbPoint() {
    return nbX_ * nbY_;
  }

  @Override
  public int getNbPtOnX() {
    return nbX_;
  }

  @Override
  public int getNbPtOnY() {
    return nbY_;
  }

  @Override
  public double getX0() {
    return x0_;
  }

  @Override
  public double getY0() {
    return y0_;
  }

  /**
   * @param _g la grille a tester
   * @return true si meme valeur
   */
  public boolean isEquivalent(final H2dRegularGridInterface _g) {
    return (nbX_ == _g.getNbPtOnX()) && (nbY_ == _g.getNbPtOnY()) && (dx_ == _g.getDX()) && (dy_ == _g.getDY())
        && (x0_ == _g.getX0()) && (y0_ == _g.getY0());
  }

}