/*
 * @creation 8 nov. 2004
 *
 * @modification $Date: 2007-04-12 16:16:49 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.dico.DicoCasFileFormatVersionAbstract;
import org.fudaa.dodico.dico.DicoDataType;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;

import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: H2DRubarDicoModelTransport.java,v 1.9 2007-04-12 16:16:49 deniger Exp $
 */
public class H2DRubarDicoModelTransport extends H2dRubarDicoModel {
  private static final String FIRST_COEFFICIENT_FOR_SURFACE_VELOCITY_FR = "Premier coefficient pour la vitesse de surface";
  private static final String FIRST_COEFFICIENT_FOR_SURFACE_VELOCITY = "First coefficient for surface velocity";
  private static final String SECOND_COEFFICIENT_FOR_SURFACE_VELOCITY_FR = "Second coefficient pour la vitesse de surface";
  private static final String SECOND_COEFFICIENT_FOR_SURFACE_VELOCITY = "Second coefficient for surface velocity";
  private static final String MULTIPLYING_FACTOR_FOR_BEDLOAD_DISCHARGE_FR = "Coefficient multiplicateur du d�bit solide par charriage";
  private static final String MULTIPLYING_FACTOR_FOR_BEDLOAD_DISCHARGE = "Multiplying factor for bedload discharge";
  private static final String SEDIMENT_DENSITY_EN = "Sediments density / Per volume ratio";
  private static final String SEDIMENT_DENSITY_FR = "Densit� des s�diments / Masse volumique";
  private static final String WATER_DENSITY_EN = "Water density / Per volume ratio";
  private static final String WATER_DENSITY_FR = "Densit� de l'eau / Masse volumique";
  private static final String TRANSPORT_COMPUTATION_OPTION = "Transport computation option";
  private static final String TRANSPORT_COMPUTATION_OPTION_FR = "Option de calcul de transport";
  private static final String DIAMETER_SEDIMENT_EN = "D50 for sediments";
  private static final String DIAMETER_SEDIMENT_FR = "D50 des s�diments";
  private static final String EROSION_COEFFICIENT = "Erosion Coefficient";
  private static final String EROSION_COEFFICIENT_FR = "Coefficient d'�rosion";
  private static final String FALL_VELOCITY_EN = "Fall velocity of the particles";
  private static final String FALL_VELOCITY_FRENCH = "Vitesse de chute des particules";
  public static final int ENTITY_NOT_FOUND = -1;
  private static final String COEFFICIENT_DE_VITESSE_DE_DEPOT_FR = "Coefficient de vitesse de d�p�t";
  private static final String COEFFICIENT_DE_VITESSE_DE_DEPOT_EN = "Deposition velocity coefficient";
  private static final String LOADING_DISTANCE_FR = "Distance de chargement pour le diam�tre";
  private static final String LOAD_DISTANCE_EN = "Adaptation length for diameter ";
  private static final String LOADING_VELOCITY_FR = "distance de chargement pour l'�tendue";
  private static final String LOADING_VELOCITY_EN = "Adaptation length for geometrical standard deviation";
  private final int nbTransportBlocks;

  public H2DRubarDicoModelTransport(int nbTransportBlocks) {
    this.nbTransportBlocks = Math.max(1, nbTransportBlocks);
  }

  public static boolean isFallVelocity(DicoEntite dicoEntite) {
    return isEntite(dicoEntite, FALL_VELOCITY_EN, FALL_VELOCITY_FRENCH);
  }

  public static boolean isMultiplyingFactorBedloadDischargeActivated(Map<DicoEntite, Object> values, final DicoCasFileFormatVersionAbstract _vers, int idxBlock) {
    int intValue = getTransportOption(values, _vers, idxBlock);
    return (intValue >= 11 && intValue <= 19) || (intValue >= 31 && intValue <= 39) || (intValue >= 51 && intValue <= 59)
        || (intValue >= 71 && intValue <= 79);
  }

  public static boolean isModifiable(DicoEntite entity, Map<DicoEntite, Object> values, final DicoCasFileFormatVersionAbstract _vers) {
    int block = isErosionCoefficient(entity);
    if (block >= 0) {
      return !isMultiplyingFactorBedloadDischargeActivated(values, _vers, block) && !isCoefficientForSurfaceActivated(values, _vers, block);
    }
    block = isMultiplyingFactorForBedloadDischarge(entity);
    if (block >= 0) {
      return isMultiplyingFactorBedloadDischargeActivated(values, _vers, block);
    }
    block = isFirstCoefficientForSurfaceVelocity(entity);
    if (block >= 0) {
      return isCoefficientForSurfaceActivated(values, _vers, block);
    }
    block = isSecondCoefficientForSurfaceVelocity(entity);
    if (block >= 0) {
      return isCoefficientForSurfaceActivated(values, _vers, block);
    }
    block = isDiameterSediment(entity);
    if (block >= 0) {
      return !isCoefficientForSurfaceActivated(values, _vers, block);
    }
    block = isLoadingDistance(entity);
    if (block >= 0) {
      return isLoadingActivated(values, _vers, block);
    }
    block = isLoadingVelocity(entity);
    if (block >= 0) {
      return isLoadingActivated(values, _vers, block);
    }
    return true;
  }

  public static boolean isCoefficientForSurfaceActivated(Map<DicoEntite, Object> values, final DicoCasFileFormatVersionAbstract _vers, int idxBlock) {
    int intValue = getTransportOption(values, _vers, idxBlock);
    if (intValue == 1 || intValue == 2) {
      double waterDensity = getDoubleValue(values, _vers, new String[]{WATER_DENSITY_EN, WATER_DENSITY_FR});
      double sedimentDensity = getDoubleValue(values, _vers, new String[]{SEDIMENT_DENSITY_FR, SEDIMENT_DENSITY_EN});
      return waterDensity > sedimentDensity;
    }
    return false;
  }

  public static boolean isLoadingActivated(Map<DicoEntite, Object> values, final DicoCasFileFormatVersionAbstract _vers, int idxBlock) {
    int intValue = getTransportOption(values, _vers, idxBlock);
    return intValue > 100 || intValue < -100;
  }

  public static double getDoubleValue(Map<DicoEntite, Object> values, final DicoCasFileFormatVersionAbstract _vers, String[] names) {
    DicoEntite entiteFor = _vers.getEntiteFor(names);
    String value = (String) values.get(entiteFor);
    if (value == null) {
      value = entiteFor.getDefautValue();
    }
    try {
      return Double.parseDouble(value);
    } catch (NumberFormatException e1) {
    }
    return 0;
  }

  public static int getTransportOption(Map<DicoEntite, Object> values, final DicoCasFileFormatVersionAbstract _vers, int idxBlock) {
    DicoEntite e = getTransportOption(_vers, idxBlock);
    String value = (String) values.get(e);
    if (value == null) {
      value = e.getDefautValue();
    }
    try {
      return Integer.parseInt(value);
    } catch (NumberFormatException e1) {
    }
    return -1;
  }

  public static int isErosionCoefficient(DicoEntite dicoEntite) {
    return getBlockIdx(dicoEntite, EROSION_COEFFICIENT, EROSION_COEFFICIENT_FR);
  }

  private static int getBlockIdx(DicoEntite dicoEntite, String fr, String en) {
    if (isEntite(dicoEntite, fr, en)) {
      return 0;
    }
    if (!startsWithEntite(dicoEntite, fr, en)) {
      return -1;
    }
    return getBlockIdx(dicoEntite);
  }

  /**
   *
   * @param dicoEntite
   * @return indice du block en commencant a 0. Une entite nomm� BLABLA 2 a pour indice 1.
   */
  public static int getBlockIdx(DicoEntite dicoEntite) {
    String idx = StringUtils.substringAfterLast(dicoEntite.getNom(), " ");
    if (StringUtils.isNotBlank(idx)) {
      try {
        return Integer.parseInt(idx.trim()) - 1;
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
    return 0;
  }

  public static int isMultiplyingFactorForBedloadDischarge(DicoEntite dicoEntite) {
    return getBlockIdx(dicoEntite, MULTIPLYING_FACTOR_FOR_BEDLOAD_DISCHARGE, MULTIPLYING_FACTOR_FOR_BEDLOAD_DISCHARGE_FR);
  }

  public static int isLoadingDistance(DicoEntite dicoEntite) {
    return getBlockIdx(dicoEntite, LOADING_DISTANCE_FR, LOAD_DISTANCE_EN);
  }

  public static int isLoadingVelocity(DicoEntite dicoEntite) {
    return getBlockIdx(dicoEntite, LOADING_VELOCITY_FR, LOADING_VELOCITY_EN);
  }

  public static int isFirstCoefficientForSurfaceVelocity(DicoEntite dicoEntite) {
    return getBlockIdx(dicoEntite, FIRST_COEFFICIENT_FOR_SURFACE_VELOCITY, FIRST_COEFFICIENT_FOR_SURFACE_VELOCITY_FR);
  }

  public static int isSecondCoefficientForSurfaceVelocity(DicoEntite dicoEntite) {
    return getBlockIdx(dicoEntite, SECOND_COEFFICIENT_FOR_SURFACE_VELOCITY, SECOND_COEFFICIENT_FOR_SURFACE_VELOCITY_FR);
  }

  public static int isDiameterSediment(DicoEntite dicoEntite) {
    return getBlockIdx(dicoEntite, DIAMETER_SEDIMENT_EN, DIAMETER_SEDIMENT_FR);
  }

  public static DicoEntite getTransportOption(final DicoCasFileFormatVersionAbstract _vers, int idxBlock) {
    return _vers.getEntiteFor(new String[]{getString(TRANSPORT_COMPUTATION_OPTION, idxBlock), getString(TRANSPORT_COMPUTATION_OPTION_FR, idxBlock)});
  }

  public static boolean isEntite(DicoEntite dicoEntite, String nameFr, String nameEn) {
    return dicoEntite != null && (nameFr.equals(dicoEntite.getNom()) || nameEn.equals(dicoEntite.getNom()));
  }

  public static boolean startsWithEntite(DicoEntite dicoEntite, String nameFr, String nameEn) {
    return dicoEntite != null && (dicoEntite.getNom().startsWith(nameFr) || dicoEntite.getNom().startsWith(nameEn));
  }

  @Override
  public H2dRubarProjetType getProjectType() {
    return H2dRubarProjetType.TRANSPORT;
  }

  private static String getString(String initString, int idx) {
    if (idx == 0) {
      return initString;
    }
    return initString + CtuluLibString.getEspaceString(idx + 1);
  }

  @Override
  protected DicoEntite[] createEntites() {
    final DicoEntite[] init = super.createEntites();
    final DicoEntite[] r = new DicoEntite[init.length + (21 * nbTransportBlocks)];
    System.arraycopy(init, 0, r, 0, init.length);
    final String[] t = new String[2];
    int index = init.length;
    int indexInPar = init.length;
    final String[] rGeneral = new String[]{"Transport", "Transport"};
    for (int idxBlock = 0; idxBlock < nbTransportBlocks; idxBlock++) {
      //WARN: the line "Viscosity along X" has 4 values:
      DicoDataType.Reel type = new DicoDataType.Reel();
      t[0] = getString("Constante de viscosit� selon X", idxBlock);
      t[1] = getString("Viscosity along X", idxBlock);
      DicoEntite.Simple ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setIndex(indexInPar);
      ent.setPosition(0);
      t[1] = "0";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();

      type = new DicoDataType.Reel();
      type.setControle(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
      t[0] = getString("Coefficient multiplicateur de la vitesse solide par charriage", idxBlock);
      t[1] = getString("Multiplying factor for bedload velocity", idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setIndex(indexInPar);
      ent.setPosition(1);
      t[1] = "1";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();

      type = new DicoDataType.Reel();
      type.setControle(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
      t[0] = getString("Exposant de d�pendance de la contrainte critique � la pente", idxBlock);
      t[1] = getString("Exponent for critical stress function of slope", idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setIndex(indexInPar);
      ent.setPosition(2);
      t[1] = "0";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();

      type = new DicoDataType.Reel();
      type.setControle(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
      t[0] = getString(FALL_VELOCITY_FRENCH, idxBlock);
      t[1] = getString(FALL_VELOCITY_EN, idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setIndex(indexInPar++);
      ent.setPosition(3);
      t[1] = "0";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();

      //WARN: the line "Viscosity along Y" has 3 values:
      type = new DicoDataType.Reel();
      t[0] = getString("Constante de viscosit� selon Y", idxBlock);
      t[1] = getString("Viscosity along Y", idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setIndex(indexInPar);
      ent.setPosition(0);
      t[1] = "0";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();

      type = new DicoDataType.Reel();
      t[0] = getString("Coefficient A de d�viation du charriage", idxBlock);
      t[1] = getString("Coefficient A for bedload deviation", idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setIndex(indexInPar);
      ent.setPosition(1);
      t[1] = "7";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();

      type = new DicoDataType.Reel();
      t[0] = getString("Coefficient B de d�viation du charriage", idxBlock);
      t[1] = getString("Coefficient B for bedload deviation", idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setIndex(indexInPar++);
      ent.setPosition(2);
      t[1] = "0.85";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();
      // SIGM
      type = new DicoDataType.Reel();
      t[0] = getString("Coefficient de Schmidt", idxBlock);
      t[1] = getString("Schmidt coefficient", idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setIndex(indexInPar++);
      t[1] = "0";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();
      // DENS
      type = new DicoDataType.Reel();
      t[0] = getString(SEDIMENT_DENSITY_FR, idxBlock);
      t[1] = getString(SEDIMENT_DENSITY_EN, idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setIndex(indexInPar++);
      t[1] = "2700";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();
      // DEN
      type = new DicoDataType.Reel();
      t[0] = getString(WATER_DENSITY_FR, idxBlock);
      t[1] = getString(WATER_DENSITY_EN, idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setIndex(indexInPar++);
      t[1] = "1000";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();
      //WARN on the erosion several values can be used
      // M
      type = new DicoDataType.Reel();
      t[0] = getString(EROSION_COEFFICIENT_FR, idxBlock);
      t[1] = getString(EROSION_COEFFICIENT, idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setIndex(indexInPar);
      t[1] = "0.000003";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();

      type = new DicoDataType.Reel();
      t[0] = getString(MULTIPLYING_FACTOR_FOR_BEDLOAD_DISCHARGE_FR, idxBlock);
      t[1] = getString(MULTIPLYING_FACTOR_FOR_BEDLOAD_DISCHARGE, idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setPosition(1);
      ent.setModifiable(false);
      ent.setIndex(indexInPar);
      t[1] = "1";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();

      type = new DicoDataType.Reel();
      t[0] = getString(SECOND_COEFFICIENT_FOR_SURFACE_VELOCITY_FR, idxBlock);
      t[1] = getString(SECOND_COEFFICIENT_FOR_SURFACE_VELOCITY, idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setModifiable(false);
      ent.setPosition(2);
      ent.setIndex(indexInPar++);
      t[1] = "0.03";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();

      // IKDS
      final DicoDataType.Entier typeE = new DicoDataType.Entier();
      t[0] = getString(TRANSPORT_COMPUTATION_OPTION_FR, idxBlock);
      t[1] = getString(TRANSPORT_COMPUTATION_OPTION, idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], typeE);
      ent.setNiveau(0);
      ent.setIndex(indexInPar++);
      t[1] = "1";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();
      // DIAM
      // mise a jour des valeurs par d�faut.
      type = new DicoDataType.Reel();
      t[0] = getString(DIAMETER_SEDIMENT_FR, idxBlock);
      t[1] = getString(DIAMETER_SEDIMENT_EN, idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setIndex(indexInPar);
      ent.setDefautValue("0.0");
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();

      type = new DicoDataType.Reel();
      t[0] = getString(FIRST_COEFFICIENT_FOR_SURFACE_VELOCITY_FR, idxBlock);
      t[1] = getString(FIRST_COEFFICIENT_FOR_SURFACE_VELOCITY, idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setPosition(1);
      ent.setModifiable(false);
      ent.setIndex(indexInPar++);
      t[1] = "1.16";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();

      // ALPHA
      type = new DicoDataType.Reel();
      t[0] = getString(COEFFICIENT_DE_VITESSE_DE_DEPOT_FR, idxBlock);
      t[1] = getString(COEFFICIENT_DE_VITESSE_DE_DEPOT_EN, idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setIndex(indexInPar);
      ent.setDefautValue("0.01");
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();

      type = new DicoDataType.Reel();
      t[0] = getString(LOADING_DISTANCE_FR, idxBlock);
      t[1] = getString(LOAD_DISTANCE_EN, idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setPosition(1);
      ent.setIndex(indexInPar);
      ent.setDefautValue("1");
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();

      type = new DicoDataType.Reel();
      t[0] = getString(LOADING_VELOCITY_FR, idxBlock);
      t[1] = getString(LOADING_VELOCITY_EN, idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setPosition(2);
      ent.setIndex(indexInPar++);
      ent.setDefautValue("1");
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();

      // POROSITE DES SEDIMENTS
      type = new DicoDataType.Reel();
      t[0] = getString("Porosit� des s�diments", idxBlock);
      t[1] = getString("Sediments porosity", idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setIndex(indexInPar++);
      t[1] = "0.3";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();
      // KDS
      type = new DicoDataType.Reel();
      t[0] = getString("D90 / contrainte critique de cisaillement", idxBlock);
      t[1] = getString("D90 / Critical shear stress", idxBlock);
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setNiveau(0);
      ent.setIndex(indexInPar++);
      ent.setDefautValue("0.05");
      ent.setRubrique(rGeneral[languageIndex_]);
      r[index++] = ent.getImmutable();
    }
    return r;
  }
}
