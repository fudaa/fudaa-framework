/*
 * @creation 25 juin 2003
 * @modification $Date: 2007-06-28 09:25:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.h2d;

import com.memoire.fu.FuLog;
import gnu.trove.TObjectDoubleHashMap;
import gnu.trove.TObjectDoubleProcedure;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.dodico.h2d.type.H2dResolutionMethodType;
import org.fudaa.dodico.h2d.type.H2dResolutionSchemaType;
import org.fudaa.dodico.h2d.type.H2dVariableTimeType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author deniger
 * @version $Id: H2dTimeStepGroup.java,v 1.17 2007-06-28 09:25:08 deniger Exp $
 */
public class H2dTimeStepGroup {

  TObjectDoubleHashMap varValueMethode_;

  TObjectDoubleHashMap varValueSchema_;

  H2dResolutionSchemaType schema_;

  H2dResolutionMethodType methode_;

  /**
   * @param _v la variable
   * @return la valeur par defaut. 0 si variable 'inconnue'
   */
  public static double getDefaultValue(final H2dVariableType _v) {
    double r = 0;
    if (_v == H2dVariableType.RELAXATION) {
      r = 1d;
    } else if (_v == H2dVariableTimeType.PRECISION_METHOD) {
      r = 0.001d;
    } else if (_v == H2dVariableType.PRECISION_BCD_METHOD) {
      r = 5e-4;
    } else if (_v == H2dVariableTimeType.NB_MAX_IT_METHOD) {
      r = 2d;
    } else if ((_v == H2dVariableTimeType.FREQUENCE_PRINT_SCHEMA) || (_v == H2dVariableTimeType.NB_TIME_STEP_SCHEMA)) {
      r = 1d;
    }
    return r;
  }

  public H2dTimeStepGroup() {
    schema_ = H2dResolutionSchemaType.STATIONNAIRE;

  }

  /**
   * @param _init le groupe utilise pour l'initialisation
   */
  public H2dTimeStepGroup(final H2dTimeStepGroup _init) {
    initFrom(_init);
  }

  /**
   * @param _init les groupes a cloner
   * @return les groupes clones.
   */
  public static H2dTimeStepGroup[] cloneTimeStep(final H2dTimeStepGroup[] _init) {
    final H2dTimeStepGroup[] r = new H2dTimeStepGroup[_init.length];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = new H2dTimeStepGroup(_init[i]);
    }
    return r;
  }

  /**
   * @param _g1 le premier tableau a comparer
   * @param _g2 le deuxieme tableau a comparer
   * @return true si egaux
   * @see H2dTimeStepGroup#equals(Object)
   */
  public static boolean isSame(final H2dTimeStepGroup[] _g1, final H2dTimeStepGroup[] _g2) {
    if (_g1.length == _g2.length) {
      for (int i = _g1.length - 1; i >= 0; i--) {
        if (!_g1[i].equalsValue(_g2[i])) {
          return false;
        }
      }
    }
    return false;
  }

  /**
   * @return un groupe stationnaire
   */
  public static H2dTimeStepGroup createDefaultStationnaire() {
    final H2dTimeStepGroup r = new H2dTimeStepGroup();
    r.setSchema(H2dResolutionSchemaType.STATIONNAIRE);
    r.setMethode(H2dResolutionMethodType.LINEAIRE);
    return r;
  }

  /**
   * @return valeur par defaut pour le coef de schema kawarah
   */
  public static double getKawarahDefaultCoefSchema() {
    return 0.92;
  }

  /**
   * @return valeur par defaut pour le coef de schema
   */
  public static double getEulerDefaultCoefSchema() {
    return 1;
  }

  /**
   * @return un groupe transitoire
   */
  public static H2dTimeStepGroup createDefaultTransient() {
    final H2dTimeStepGroup r = new H2dTimeStepGroup();
    r.setSchema(H2dResolutionSchemaType.EULER);
    r.setMethode(H2dResolutionMethodType.LINEAIRE);
    r.setNbPasTemps(10);
    r.setValeurPasTemps(0.1);
    r.setCoefSchema(1);
    return r;
  }

  /**
   * @param _step les groupes a tester
   * @return true si une viscosite transitoire est utilisee
   */
  public static Double isViscosityTransitUsed(final H2dTimeStepGroup[] _step) {
    for (int i = 0; i < _step.length; i++) {
      if (_step[i].getMethode().needViscosity()) {
        return CtuluLib.getDouble(_step[i].getMethodeValue(H2dVariableType.VISCOSITE));
      }
    }
    return null;
  }

  /**
   * @return la longueur de ce pas de temps ( nb pas de temps * valeur pas de temps)
   */
  public double getTimeLength() {
    if (schema_.isUsed(H2dVariableTimeType.NB_TIME_STEP_SCHEMA)) {
      return getValeurPasTemps() * getNbPasTemps();
    }
    return 0d;
  }

  /**
   * Initialise les donnees.
   * 
   * @param _init la source pour initialies les donnees
   */
  public final void initFrom(final H2dTimeStepGroup _init) {
    varValueMethode_ = _init.varValueMethode_ == null ? null : CtuluLib.copy(_init.varValueMethode_);
    varValueSchema_ = _init.varValueSchema_ == null ? null : CtuluLib.copy(_init.varValueSchema_);
    schema_ = _init.schema_;
    if (schema_ == null) {
      FuLog.error(new Throwable("le schema est null"));
    }
    methode_ = _init.methode_;
  }

  @Override
  public boolean equals(final Object _o) {
    if (_o == this) {
      return true;
    }
    if (_o instanceof H2dTimeStepGroup) {
      return equalsValue((H2dTimeStepGroup) _o);
    }
    return false;
  }

  /**
   * @param _o le groupe a comparer
   * @return true si tout egale
   */
  public boolean equalsValue(final H2dTimeStepGroup _o) {
    if (_o == null) {
      return false;
    }
    if (_o == this) {
      return true;
    }
    return (schema_ == _o.schema_)
        && (methode_ == _o.methode_)
        && (((varValueMethode_ == null) && (_o.varValueMethode_ == null)) || ((varValueMethode_ != null) && (varValueMethode_
            .equals(_o.varValueMethode_))))
        && (((varValueSchema_ == null) && (_o.varValueSchema_ == null)) || ((varValueSchema_ != null) && (varValueSchema_
            .equals(_o.varValueSchema_))));
  }

  /**
   * @return le coef de schema
   */
  public double getCoefSchema() {
    return varValueSchema_ == null ? 0 : varValueSchema_.get(H2dVariableTimeType.COEF_SCHEMA);
  }

  /**
   * @return frequence impression
   */
  public int getFrequenceImpression() {
    return (int) getSchemaValue(H2dVariableTimeType.FREQUENCE_PRINT_SCHEMA);
  }

  /**
   * @return le nb de pas de temps
   */
  public int getNbPasTemps() {
    return (int) getSchemaValue(H2dVariableTimeType.NB_TIME_STEP_SCHEMA);
  }

  /**
   * @return le nombre max d'iteration
   */
  public int getNombreIterationMax() {
    return (int) getMethodeValue(H2dVariableTimeType.NB_MAX_IT_METHOD);
  }

  /**
   * @return la precision de la methode
   */
  public double getPrecision() {
    return getMethodeValue(H2dVariableTimeType.PRECISION_METHOD);
  }

  /**
   * @return precision pour les bancs couvrant/decouvrant
   */
  public double getPrecisionBancCouvrantDecouvrant() {
    return getMethodeValue(H2dVariableType.PRECISION_BCD_METHOD);
  }

  /**
   * @return coef de relaxation
   */
  public double getRelaxation() {
    return getMethodeValue(H2dVariableType.RELAXATION);
  }

  /**
   * @return la valeur du pas de temps
   */
  public double getValeurPasTemps() {
    return getSchemaValue(H2dVariableTimeType.VALUE_TIME_STEP_SCHEMA);
  }

  /**
   * @param _d nouvelle valeur du pas de temps
   */
  public void setCoefSchema(final double _d) {
    setSchemaValue(H2dVariableTimeType.COEF_SCHEMA, _d);
  }

  /**
   * @param _i la nouvelle frequence d'impression
   */
  public void setFrequenceImpression(final int _i) {
    setSchemaValue(H2dVariableTimeType.FREQUENCE_PRINT_SCHEMA, _i);
  }

  /**
   * @param _i le nouveau pas de temps
   */
  public void setNbPasTemps(final int _i) {
    setSchemaValue(H2dVariableTimeType.NB_TIME_STEP_SCHEMA, _i);
  }

  /**
   * @param _i le nb max d'iteration
   */
  public void setNombreIterationMax(final int _i) {
    setMethodeValue(H2dVariableTimeType.NB_MAX_IT_METHOD, _i);
  }

  /**
   * @param _d la nouvelle precision
   */
  public void setPrecision(final double _d) {
    setMethodeValue(H2dVariableTimeType.PRECISION_METHOD, _d);
  }

  /**
   * @param _d la nouvelle precision
   */
  public void setPrecisionBancCouvrantDecouvrant(final double _d) {
    setMethodeValue(H2dVariableType.PRECISION_BCD_METHOD, _d);
  }

  /**
   * @param _d la nouvelle relaxation
   */
  public void setRelaxation(final double _d) {
    setMethodeValue(H2dVariableType.RELAXATION, _d);
  }

  /**
   * @param _d la nouvelle valeur du pas de temps
   */
  public void setValeurPasTemps(final double _d) {
    setSchemaValue(H2dVariableTimeType.VALUE_TIME_STEP_SCHEMA, _d);
  }

  /**
   * @param _t la variable demande
   * @return la valeur specifiee pour la methode. Si la valeur par defaut est renvoye (et 0 si var inconnue)
   */
  public double getMethodeValue(final H2dVariableType _t) {
    if ((varValueMethode_ != null) && (varValueMethode_.contains(_t))) {
      return varValueMethode_.get(_t);
    }
    return getDefaultValue(_t);
  }

  /**
   * @param _t la variable demande
   * @return la valeur specifiee pour le schema. Si la valeur par defaut est renvoye (et 0 si var inconnue)
   */
  public double getSchemaValue(final H2dVariableType _t) {
    if ((varValueSchema_ != null) && (varValueSchema_.contains(_t))) {
      return varValueSchema_.get(_t);
    }
    return getDefaultValue(_t);
  }

  /**
   * @param _t la variable schema a initialise
   * @param _v la nouvelle valeur pour cette variable
   */
  public void setSchemaValue(final H2dVariableType _t, final double _v) {
    if (schema_.isUsed(_t)) {
      if (varValueSchema_ == null) {
        varValueSchema_ = new TObjectDoubleHashMap();
      }
      varValueSchema_.put(_t, _v);
    }
  }

  /**
   * @param _t la variable methode a initialise
   * @param _v la nouvelle valeur pour cette variable
   */
  public void setMethodeValue(final H2dVariableType _t, final double _v) {
    if (methode_.isUsed(_t)) {
      if (varValueMethode_ == null) {
        varValueMethode_ = new TObjectDoubleHashMap();
      }
      varValueMethode_.put(_t, _v);
    }
  }

  /**
   * @return le type de la methode de resolution
   */
  public H2dResolutionMethodType getMethode() {
    return methode_;
  }

  /**
   * @return le type du schema de resolution
   */
  public H2dResolutionSchemaType getSchema() {
    return schema_;
  }

  /**
   * @param _type le nouveau type de methode
   */
  public void setMethode(final H2dResolutionMethodType _type) {
    if (_type != null && _type != methode_) {
      methode_ = _type;
      if (varValueMethode_ != null) {
        varValueMethode_.retainEntries(new TObjectDoubleProcedure() {

          @Override
          public boolean execute(final Object _o, final double _d) {
            return methode_.isUsed((H2dVariableType) _o);
          }
        });
      }
    }
  }

  /**
   * @param _type le nouveau type de schema
   */
  public void setSchema(final H2dResolutionSchemaType _type) {
    if (_type != null && schema_ != _type) {
      schema_ = _type;
      if (varValueSchema_ != null) {
        varValueSchema_.retainEntries(new TObjectDoubleProcedure() {

          @Override
          public boolean execute(final Object _o, final double _d) {
            return schema_.isUsed((H2dVariableType) _o);
          }
        });
      }
    }
  }

  @Override
  public int hashCode() {
    return (schema_ == null ? 0 : schema_.hashCode()) + (methode_ == null ? 0 : methode_.hashCode())
        + (varValueMethode_ == null ? 0 : varValueMethode_.hashCode())
        + (varValueSchema_ == null ? 0 : varValueSchema_.hashCode());
  }

  /**
   * @param _beginTime le temps initiale
   * @param _gp le groupe a considerer
   * @return le temps min calcule a partir du premier element du tableau.
   */
  public static double getMinTime(final double _beginTime, final H2dTimeStepGroup[] _gp) {
    if (_gp == null) {
      return 0;
    }
    // si stationnaire: on s'en fout.
    // TODO: il faudrait enregistrer les courbes non utilis�es dans le fichier ".fzip"

    if (_gp[0].getSchema() == H2dResolutionSchemaType.STATIONNAIRE) {
      return 1.;
    }
    final double res = _beginTime - _gp[0].getValeurPasTemps();
    // sinon on propose le temps de d�but: s'il est n�gatif on renvoie zero
    return res < 0 ? 0 : res;
  }

  /**
   * @param _beginTime le temps initiale
   * @param _gp le groupe a considerer
   * @return le temps max calcule a partir du tableau des groupes et en ajoute le temps du debut
   */
  public static double getMaxTime(final double _beginTime, final H2dTimeStepGroup[] _gp) {
    if (_gp == null) {
      return 0;
    }
    if (_gp[0].getSchema() == H2dResolutionSchemaType.STATIONNAIRE) {
      return 1;
    }
    double r = _beginTime;
    for (int i = _gp.length - 1; i >= 0; i--) {
      r += _gp[i].getTimeLength();
    }
    // on ajoute un pas de temps � la fin pour �tre que les courbes sont bien comprises dans la dur�e de simulation
    return r + _gp[_gp.length - 1].getValeurPasTemps();
  }

  /* *//**
         * @param _beginTime le pas de temps initiale
         * @param _gp les groupes a considerer
         * @return tous les pas de temps a partir des des donnees des groupes
         */
  /*
   * public static double[] getTimeStep(final double _beginTime, final H2dTimeStepGroup[] _gp) { if (_gp == null) {
   * return null; } H2dTimeStepGroup t = _gp[0]; if (t.getSchema() == H2dResolutionSchemaType.STATIONNAIRE) { return new
   * double[] { 1 }; } final TDoubleArrayList r = new TDoubleArrayList(); final int n = _gp.length; int nbPas; double
   * vTemps; double vToAdd = _beginTime; for (int i = 0; i < n; i++) { t = _gp[i]; nbPas = t.getNbPasTemps(); vTemps =
   * t.getValeurPasTemps(); for (int j = nbPas; j > 0; j--) { vToAdd += vTemps; r.add(vToAdd); } } return
   * r.toNativeArray(); }
   */
}