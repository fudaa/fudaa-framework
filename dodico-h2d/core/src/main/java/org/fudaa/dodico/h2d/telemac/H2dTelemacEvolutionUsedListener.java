/*
 * @creation 19 mai 2004
 * @modification $Date: 2006-04-19 13:19:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

/**
 * @author Fred Deniger
 * @version $Id: H2dTelemacEvolutionUsedListener.java,v 1.4 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dTelemacEvolutionUsedListener {

  /**
   * Evt envoye lorsque l'etat des evolutions a pu etre modifiee.
   */
  void globalEvolutionUsedChanged();

}