/*
 * @creation 28 sept. 2004
 * @modification $Date: 2007-01-10 09:04:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandCompositeInverse;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.collection.CtuluListObject;

import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: H2dParallelogrammeManager.java,v 1.13 2007-01-10 09:04:27 deniger Exp $
 */
public class H2dParallelogrammeManager {
  private class ParallelogrammeModel extends CtuluListObject {
    /**
     *
     */
    public ParallelogrammeModel() {
      super();
    }

    @Override
    protected void fireObjectAdded(int _newIdx, Object _newGeom) {
      fireSupportAddOrRemoved();
    }

    @Override
    protected void fireObjectChanged(int _oldIdx, Object _oldGeom) {
      fireSupportChanged();
    }

    @Override
    protected void fireObjectRemoved(int _oldIdx, Object _oldGeom) {
      fireSupportAddOrRemoved();
    }

    @Override
    protected void fireObjectModified(int _idx, Object _geom) {
      fireSupportAddOrRemoved();
    }

    @Override
    protected void internActionPointRemoved(final int _i, final CtuluCommandContainer _c) {
    }

    @Override
    protected void internActionPointInserted(final int _i, final List _values, final CtuluCommandContainer _c) {
    }

    @Override
    protected void internActionPointAdded(final List _values, final CtuluCommandContainer _c, final int _nbValues) {
    }

    @Override
    protected void internActionCleared(final CtuluCommandContainer _c) {
    }
  }

  CtuluListObject model_;
  int nbValues_;

  public H2dParallelogrammeManager(final int _nbValues) {
    nbValues_ = _nbValues;
  }

  protected H2dParallelogrammeDataAbstract createParallelogramme(final H2dParallelogrammeDataInterface _model) {
    final H2dParallelogrammeData r = new H2dParallelogrammeData(nbValues_);
    if (_model == null) {
      r.initFromKeepingSameNumberOfValue(_model);
    }
    return r;
  }

  protected void fireSupportChanged() {

  }

  protected void fireSupportAddOrRemoved() {

  }

  /**
   * Appele lorsque des lignes de donnees ont ete ajoutees/supprimees.
   */
  protected void fireDataNombreChanged() {

  }

  protected class CommandAddData implements CtuluCommand {
    @Override
    public void redo() {
      nbValues_++;
      fireDataNombreChanged();
    }

    @Override
    public void undo() {
      nbValues_--;
      fireDataNombreChanged();
    }
  }

  protected class CommandRemoveData implements CtuluCommand {
    @Override
    public void redo() {
      nbValues_--;
      fireDataNombreChanged();
    }

    @Override
    public void undo() {
      nbValues_++;
      fireDataNombreChanged();
    }
  }

  public void initFrom(final H2dParallelogrammeManagerInterface _inter) {
    model_ = null;
    final int nbPar = _inter.getNbParall();
    for (int i = 0; i < nbPar; i++) {
      final H2dParallelogrammeDataInterface inter = _inter.getParall(i);
      addParallelogramme(inter, null);
    }
  }

  public H2dParallelogrammeDataAbstract addParallelogramme(final H2dParallelogrammeDataInterface _model,
                                                           final CtuluCommandContainer _cmd) {
    if (model_ == null) {
      model_ = new ParallelogrammeModel();
    }
    final H2dParallelogrammeDataAbstract r = createParallelogramme(_model);
    model_.addObject(r, _cmd);
    return r;
  }

  protected void removeLastData(int nbToRemove, final CtuluCommandContainer _cmd) {
    CtuluCommandComposite c = null;
    if (_cmd != null) {
      c = new CtuluCommandCompositeInverse();
    }
    for (int i = getNbParall() - 1; i >= 0; i--) {
      getParall(i).removeLastData(nbToRemove, c);
    }
    fireDataNombreChanged();
    if (_cmd != null && c != null && c.getNbCmd() > 0) {
      c.addCmd(new CommandRemoveData());
      _cmd.addCmd(c.getSimplify());
    }
  }

  protected void addLastData(int nbData, final CtuluCommandContainer _cmd) {
    CtuluCommandComposite c = null;
    if (_cmd != null) {
      c = new CtuluCommandCompositeInverse();
    }
    for (int i = getNbParall() - 1; i >= 0; i--) {
      getParall(i).addData(nbData, c);
    }
    fireDataNombreChanged();
    if (_cmd != null && c != null && c.getNbCmd() > 0) {
      c.addCmd(new CommandAddData());
      _cmd.addCmd(c.getSimplify());
    }
  }

  protected void setDataToDefaut(final int _idx, final CtuluCommandContainer _cmd) {
    CtuluCommandComposite c = null;
    if (_cmd != null) {
      c = new CtuluCommandCompositeInverse();
    }
    for (int i = getNbParall() - 1; i >= 0; i--) {
      getParall(i).setDataToDefault(_idx, _cmd);
    }
    if (_cmd != null && c != null) {
      _cmd.addCmd(c.getSimplify());
    }
  }

  public int getNbParall() {
    return model_ == null ? 0 : model_.getSize();
  }

  public H2dParallelogrammeData getParall(final int _i) {
    if (model_ == null) {
      return null;
    }
    return (H2dParallelogrammeData) model_.getValueAt(_i);
  }

  public boolean removeParallel(final int _i, final CtuluCommandContainer _cmd) {
    if (model_ != null) {
      return model_.remove(_i, _cmd);
    }
    return false;
  }

  public boolean removeParallel(final int[] _i, final CtuluCommandContainer _cmd) {
    if (model_ != null) {
      return model_.remove(_i, _cmd);
    }
    return false;
  }
}
