/*
 * @creation 14 juin 2004
 *
 * @modification $Date: 2007-05-22 13:11:25 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormater;
import org.fudaa.dodico.h2d.H2dEvolutionUseCounter;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionListenerDispatcher;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarTimeCondition.java,v 1.27 2007-05-22 13:11:25 deniger Exp $
 */
public class H2dRubarTimeConditionConcentrationBlock {
  private H2dRubarEvolution concentration;
  private H2dRubarEvolution diametre;
  private H2dRubarEvolution etendue;

  protected H2dRubarTimeConditionConcentrationBlock(
      final H2dRubarEvolution concentration, final H2dRubarEvolution diametre, final H2dRubarEvolution etendue, final CtuluCommandContainer _cmd) {
    this.concentration = concentration;
    if (this.concentration != null) {
      CtuluNumberFormater fmt = H2dRubarTimeCondition.getFmt(H2dVariableTransType.CONCENTRATION);
      this.concentration.setFmt(H2dRubarEvolution.getDefaultTimeFmt(), fmt, _cmd);
    }
    this.diametre = diametre;
    if (this.diametre != null) {
      CtuluNumberFormater fmt = H2dRubarTimeCondition.getFmt(H2dVariableTransType.DIAMETRE);
      this.diametre.setFmt(H2dRubarEvolution.getDefaultTimeFmt(), fmt, _cmd);
    }
    this.etendue = etendue;
    if (this.etendue != null) {
      CtuluNumberFormater fmt = H2dRubarTimeCondition.getFmt(H2dVariableTransType.ETENDUE);
      this.etendue.setFmt(H2dRubarEvolution.getDefaultTimeFmt(), fmt, _cmd);
    }
  }

  public H2dRubarEvolution getConcentration() {
    return concentration;
  }

  public H2dRubarEvolution getDiametre() {
    return diametre;
  }

  public H2dRubarEvolution getEtendue() {
    return etendue;
  }

  protected H2dRubarTimeConditionConcentrationBlock(final H2dRubarTimeConditionInterface _i, final EvolutionListenerDispatcher _l, int blockIdx) {
    if (_i != null) {
      concentration = H2dRubarEvolution.createH2dRubarEvolution(H2dVariableTransType.CONCENTRATION, _i.getConcentrationEvolution(blockIdx), _l);
      diametre = H2dRubarEvolution.createH2dRubarEvolution(H2dVariableTransType.DIAMETRE, _i.getDiametreEvolution(blockIdx), _l);
      etendue = H2dRubarEvolution.createH2dRubarEvolution(H2dVariableTransType.ETENDUE, _i.getEtendueEvolution(blockIdx), _l);
    }
  }

  public static boolean isTransientVariable(H2dVariableType variableType) {
    return (variableType == H2dVariableTransType.CONCENTRATION
        || variableType == H2dVariableTransType.DIAMETRE
        || variableType == H2dVariableTransType.ETENDUE);
  }

  /**
   * @param _m tableau variableType->Evolution
   */
  public H2dRubarTimeConditionConcentrationBlock(final Map _m) {
    if (_m != null) {
      concentration = (H2dRubarEvolution) _m.get(H2dVariableTransType.CONCENTRATION);
      diametre = (H2dRubarEvolution) _m.get(H2dVariableTransType.DIAMETRE);
      etendue = (H2dRubarEvolution) _m.get(H2dVariableTransType.ETENDUE);
    }
  }

  public H2dRubarTimeConditionConcentrationBlock() {
  }

  private void addError(final CtuluAnalyze _an, final String _data, final int _areteIdx) {
    if (_an != null) {
      _an.addError(H2dResource.getS(_data + " d�finie par d�faut"), _areteIdx);
    }
  }

  private void initHCat(final EvolutionListenerDispatcher _l) {
    concentration = H2dRubarTimeCondition.init(H2dVariableTransType.CONCENTRATION, concentration, _l);
  }

  private void initDiam(final EvolutionListenerDispatcher _l) {
    diametre = H2dRubarTimeCondition.init(H2dVariableTransType.DIAMETRE, diametre, _l);
  }

  private void initEt(final EvolutionListenerDispatcher _l) {
    etendue = H2dRubarTimeCondition.init(H2dVariableTransType.ETENDUE, etendue, _l);
  }

  protected void clearCurves() {
    if (concentration != null) {
      concentration.setListener(null);
    }
    if (diametre != null) {
      diametre.setListener(null);
    }
    if (etendue != null) {
      etendue.setListener(null);
    }
  }

  protected H2dRubarTimeConditionConcentrationBlock createCopy() {
    return new H2dRubarTimeConditionConcentrationBlock(concentration, diametre, etendue, null);
  }

  protected void removeNonTorrentiel() {
    if (concentration == H2dRubarParameters.NON_TORENTIEL) {
      concentration = null;
    }
    if (diametre == H2dRubarParameters.NON_TORENTIEL) {
      diametre = null;
    }
    if (etendue == H2dRubarParameters.NON_TORENTIEL) {
      etendue = null;
    }
  }

  protected boolean setEvolution(final H2dVariableType _t, final H2dRubarEvolution _e, final CtuluCommandContainer _cmd, boolean changeUseParameter) {
    final EvolutionReguliereAbstract old = getEvol(_t);
    boolean modified = false;
    if (_t == H2dVariableTransType.CONCENTRATION) {
      modified = setConcentration(_e, _cmd);
    } else if (_t == H2dVariableTransType.DIAMETRE) {
      modified = setDiametre(_e, _cmd);
    } else if (_t == H2dVariableTransType.ETENDUE) {
      modified = setEtendue(_e, _cmd);
    }
    if (changeUseParameter) {
      if (modified) {
        if (old != null) {
          old.setUnUsed(true);
        }
        if (_e != null) {
          _e.setUsed(true);
        }
      }
    }
    return modified;
  }

  protected boolean setConcentration(final H2dRubarEvolution _e, final CtuluCommandContainer _cmd) {
    if (_e != concentration) {
      concentration = _e;
      if (concentration != null) {
        concentration.setFmt(H2dRubarEvolution.getDefaultTimeFmt(), H2dRubarTimeCondition.getFmt(H2dVariableTransType.CONCENTRATION), _cmd);
      }
      return true;
    }
    return false;
  }

  protected boolean setDiametre(final H2dRubarEvolution _e, final CtuluCommandContainer _cmd) {
    if (_e != diametre) {
      diametre = _e;
      if (diametre != null) {
        diametre.setFmt(H2dRubarEvolution.getDefaultTimeFmt(), H2dRubarTimeCondition.getFmt(H2dVariableTransType.DIAMETRE), _cmd);
      }
      return true;
    }
    return false;
  }

  protected boolean setEtendue(final H2dRubarEvolution _e, final CtuluCommandContainer _cmd) {
    if (_e != etendue) {
      etendue = _e;
      if (etendue != null) {
        etendue.setFmt(H2dRubarEvolution.getDefaultTimeFmt(), H2dRubarTimeCondition.getFmt(H2dVariableTransType.ETENDUE), _cmd);
      }
      return true;
    }
    return false;
  }

  protected void setUnUsed() {
    if (concentration != null) {
      concentration.setUnUsed(true);
    }
    if (diametre != null) {
      diametre.setUnUsed(true);
    }
    if (etendue != null) {
      etendue.setUnUsed(true);
    }
  }

  protected void setUsed() {
    if (concentration != null) {
      concentration.setUsed(true);
    }
    if (diametre != null) {
      diametre.setUsed(true);
    }
    if (etendue != null) {
      etendue.setUsed(true);
    }
  }

  /**
   * @param _pref le prefixe pour les noms
   */
  protected void updateNom(final String _pref, final String suffixe) {
    if (concentration != null) {
      concentration.setNom(_pref + CtuluLibString.ESPACE + H2dVariableTransType.CONCENTRATION + suffixe);
    }
    if (diametre != null) {
      diametre.setNom(_pref + CtuluLibString.ESPACE + H2dVariableTransType.DIAMETRE + suffixe);
    }
    if (etendue != null) {
      etendue.setNom(_pref + CtuluLibString.ESPACE + H2dVariableTransType.ETENDUE + suffixe);
    }
  }

  protected void verify(final List _varAllowed, final CtuluAnalyze _an, final int _areteIdx, final EvolutionListenerDispatcher _l) {
    if (_varAllowed.contains(H2dVariableTransType.CONCENTRATION) && (concentration == null)) {
      //      addError(_an, "concentration", _areteIdx);
      initHCat(_l);
    } else if (!_varAllowed.contains(H2dVariableTransType.CONCENTRATION) && (concentration != null)) {
      concentration = null;
    }
    if (_varAllowed.contains(H2dVariableTransType.DIAMETRE) && (diametre == null)) {
      //      addError(_an, "diam�tre", _areteIdx);
      initDiam(_l);
    } else if (!_varAllowed.contains(H2dVariableTransType.DIAMETRE) && (diametre != null)) {
      diametre = null;
    }
    if (_varAllowed.contains(H2dVariableTransType.ETENDUE) && (etendue == null)) {
      //      addError(_an, "�tendue", _areteIdx);
      initEt(_l);
    } else if (!_varAllowed.contains(H2dVariableTransType.ETENDUE) && (etendue != null)) {
      etendue = null;
    }
  }

  /**
   * @param _m la table a remplir avec les evolutions-Variables.
   */
  public void fillWithTransientCurves(final H2dEvolutionUseCounter _m, final int _idx) {
    if (concentration != null) {
      _m.add(concentration, H2dVariableTransType.CONCENTRATION, _idx);
    }
    if (diametre != null) {
      _m.add(diametre, H2dVariableTransType.DIAMETRE, _idx);
    }
    if (etendue != null) {
      _m.add(etendue, H2dVariableTransType.ETENDUE, _idx);
    }
  }

  /**
   * @param _var COTE_DEBIT, DEBIT_TANGENTIEL ou DEBIT_NORMAL
   * @return l'evolution correspondante ou null si non trouvee.
   */
  public H2dRubarEvolution getEvol(final H2dVariableType _var) {
    if (_var == H2dVariableTransType.CONCENTRATION) {
      return concentration;
    }
    if (_var == H2dVariableTransType.DIAMETRE) {
      return diametre;
    }
    if (_var == H2dVariableTransType.ETENDUE) {
      return etendue;
    }
    return null;
  }

  /**
   * @return les evolutions differentes utilisees
   */
  public EvolutionReguliereAbstract[] getEvols() {
    final Set<EvolutionReguliereAbstract> r = new HashSet(3);
    if (concentration != null) {
      r.add(concentration);
    }
    if (etendue != null) {
      r.add(etendue);
    }
    if (diametre != null) {
      r.add(diametre);
    }
    if (r.size() == 0) {
      return null;
    }
    final EvolutionReguliereAbstract[] rf = new EvolutionReguliereAbstract[r.size()];
    r.toArray(rf);
    return rf;
  }

  public EvolutionReguliere getConcentrationEvolution() {
    return concentration;
  }

  public H2dRubarEvolution getDiametreEvolution() {
    return diametre;
  }

  public H2dRubarEvolution getEtendueEvolution() {
    return etendue;
  }

  /**
   * @return true si toutes les evolutions sont nulles
   */
  public boolean isAllNull() {
    return (concentration == null) && (diametre == null) && (etendue == null);
  }

  /**
   * A utiliser pour connaitre les variables qui ne sont pas definies.
   *
   * @param _requiredVariable la liste des variables qui doivent etre definies.
   * @return la liste des variables de cet objet qui ne sont pas definies comme demande par le parametres
   *     _requiredVariable. Si toutes les variables sont definies renvoie null.
   */
  public void isVariableRequiredSet(final List _requiredVariable, Set<H2dVariableType> required) {
    if (_requiredVariable == null) {
      return;
    }
    if (_requiredVariable.contains(H2dVariableTransType.CONCENTRATION) && (concentration == null)) {
      required.add(H2dVariableTransType.CONCENTRATION);
    }
    if (_requiredVariable.contains(H2dVariableTransType.DIAMETRE) && (diametre == null)) {
      required.add(H2dVariableTransType.DIAMETRE);
    }
    if (_requiredVariable.contains(H2dVariableTransType.ETENDUE) && (etendue == null)) {
      required.add(H2dVariableTransType.ETENDUE);
    }
  }

  /**
   * @param _l la liste des variables qui doivent etre definies
   * @return true si toutes les variables sont definies
   */
  public boolean isVariableSet(final List _l) {
    if (_l.contains(H2dVariableTransType.CONCENTRATION) && (concentration == null)) {
      return false;
    }
    if (_l.contains(H2dVariableTransType.DIAMETRE) && (diametre == null)) {
      return false;
    }
    if (_l.contains(H2dVariableTransType.ETENDUE) && (etendue == null)) {
      return false;
    }
    return true;
  }

  /**
   * Permet de modifier toutes les variables de cette cl. Les variables qui ne sont pas contenues dans la liste
   * <code>_varToModified</code> seront mis a zero (pas de courbes). Les variables contenues seront comparees avec
   * <code>_clToInitFrom</code> et mis a jour si differentes.
   *
   * @param _varToModified la liste des variables qui seront cree.
   * @param _clToInitFrom la source pour la modif
   * @return true si modif
   */
  public boolean majWithNewList(final List _varToModified, final H2dRubarTimeConditionConcentrationBlock _clToInitFrom) {
    boolean r = false;
    if ((_varToModified == null) || (_varToModified.size() == 0)) {
      r = (concentration != null) || (diametre != null) || (etendue != null);
      if (concentration != null) {
        concentration.setUnUsed(true);
      }
      if (diametre != null) {
        diametre.setUnUsed(true);
      }
      if (etendue != null) {
        etendue.setUnUsed(true);
      }
      concentration = null;
      diametre = null;
      etendue = null;
    } else {
      if (_varToModified != null && _varToModified.contains(H2dVariableTransType.CONCENTRATION)) {
        if ((_clToInitFrom.concentration != null) && (concentration != _clToInitFrom.concentration)) {
          if (concentration != null) {
            concentration.setUnUsed(true);
          }
          concentration = _clToInitFrom.concentration;
          r = true;
          if (concentration == H2dRubarParameters.NON_TORENTIEL) {
            concentration = null;
          } else {
            concentration.setUsed(true);
          }
        }
        if (concentration == null) {
          new Throwable().printStackTrace();
        }
      } else if (concentration != null) {
        concentration.setUnUsed(true);
        concentration = null;
        r = true;
      }
      if (_varToModified != null && _varToModified.contains(H2dVariableTransType.DIAMETRE)) {
        if ((_clToInitFrom.diametre != null) && (diametre != _clToInitFrom.diametre)) {
          if (diametre != null) {
            diametre.setUnUsed(true);
          }
          diametre = _clToInitFrom.diametre;
          r = true;
          if (diametre == H2dRubarParameters.NON_TORENTIEL) {
            diametre = null;
          } else {
            diametre.setUsed(true);
          }
        }
        if (diametre == null) {
          new Throwable().printStackTrace();
        }
      } else if (diametre != null) {
        diametre.setUnUsed(true);
        diametre = null;
        r = true;
      }
      if (_varToModified != null && _varToModified.contains(H2dVariableTransType.ETENDUE)) {
        if ((_clToInitFrom.etendue != null) && (etendue != _clToInitFrom.etendue)) {
          if (etendue != null) {
            etendue.setUnUsed(true);
          }
          etendue = _clToInitFrom.etendue;
          r = true;
          if (etendue == H2dRubarParameters.NON_TORENTIEL) {
            etendue = null;
          } else {
            etendue.setUsed(true);
          }
        }
        if (etendue == null) {
          new Throwable().printStackTrace();
        }
      } else if (etendue != null) {
        etendue.setUnUsed(true);
        etendue = null;
        r = true;
      }
    }
    return r;
  }

  public void getInvalidEvol(List<H2dRubarEvolution> listOfInvalidCurves) {
    CtuluNumberFormater fmt = H2dRubarTimeCondition.getFmt(H2dVariableTransType.CONCENTRATION);
    if (concentration != null && (concentration.getYFmt() != fmt)) {
      if (concentration.isIdxErrorForFmt(H2dRubarEvolution.getDefaultTimeFmt().getValueValidator(), fmt.getValueValidator())) {
        listOfInvalidCurves.add(concentration);
      }
    }
    fmt = H2dRubarTimeCondition.getFmt(H2dVariableTransType.DIAMETRE);
    if (diametre != null && (diametre.getYFmt() != fmt)) {
      if (diametre.isIdxErrorForFmt(H2dRubarEvolution.getDefaultTimeFmt().getValueValidator(), fmt.getValueValidator())) {
        listOfInvalidCurves.add(diametre);
      }
    }
    fmt = H2dRubarTimeCondition.getFmt(H2dVariableTransType.ETENDUE);
    if (etendue != null && (etendue.getYFmt() != fmt)) {
      if (etendue.isIdxErrorForFmt(H2dRubarEvolution.getDefaultTimeFmt().getValueValidator(), fmt.getValueValidator())) {
        listOfInvalidCurves.add(etendue);
      }
    }
  }
}
