/*
 * @creation 16 juin 2004
 * @modification $Date: 2007-03-02 13:00:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.h2d.type.H2dRubarBoundaryFlowrateGroupType;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarBcListener.java,v 1.9 2007-03-02 13:00:52 deniger Exp $
 */
public interface H2dRubarBcListener {
  void nodeInGridChanged();

  void bathyChanged();

  void fondDurChanged();

  void projectTypeChanged();

  /**
   * When the number of concentration blocks changed.
   */
  void numberOfConcentrationChanged();

  /**
   * Envoye si modif sur les cl.
   */
  void timeClChanged();

  /**
   * Envoye si le type d'une arete est modifie.
   */
  void areteTypeChanged();

  /**
   * Envoye si un groupe est modifie.
   *
   * @param _t le type du groupe modifie
   */
  void flowrateGroupChanged(H2dRubarBoundaryFlowrateGroupType _t);
}
