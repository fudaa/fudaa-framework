/*
 * @creation 23 oct. 2003
 * @modification $Date: 2006-04-19 13:19:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import org.fudaa.dodico.h2d.H2dBcListener;

/**
 * @author deniger
 * @version $Id: H2dRefluxBcListener.java,v 1.8 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dRefluxBcListener extends H2dBcListener {

  /**
   * Methode appelee lorsque une normale a ete changee.
   */
  void bcPointsNormalChanged();
}