/*
 * @creation 14 d�c. 2004
 *
 * @modification $Date: 2007-05-22 13:11:25 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.collection.CtuluArrayObject;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEditAbstract;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.dico.DicoParamsListener;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;

import java.util.*;

/**
 * @author Fred Deniger
 */
public class H2dRubarSedimentMng {
  private class SaveAdapter implements H2dRubarSedimentInterface {
    @Override
    public H2dRubarSedimentCoucheContainer getCoucheContainer(int idxTime, int idxNoeud) {
      return (H2dRubarSedimentCoucheContainer) couchesByNoeud.getObjectValueAt(idxNoeud);
    }

    @Override
    public int getNbNoeuds() {
      return nbNodes;
    }

    @Override
    public int getNbTimes() {
      return 1;
    }

    @Override
    public double getTimes(int idx) {
      return getSedimentTimeFromDico();
    }
  }

  private class SedimentModelObject extends CtuluArrayObject {
    /**
     * @param _nb
     */
    public SedimentModelObject(final int _nb) {
      super(_nb);
    }

    @Override
    protected void fireObjectChanged(int _indexGeom, Object _newValue) {
      fireSedimentChanged();
    }
  }

  public static class ValuesByNodeModel extends CtuluCollectionDoubleEditAbstract {
    private final int coucheIdx;
    private final H2dVariableTransType parentVariable;
    private final H2dRubarSedimentMng rubarSedimentMng;

    public ValuesByNodeModel(H2dRubarSedimentVariableType variable, H2dRubarSedimentMng rubarSedimentMng) {
      super();
      this.rubarSedimentMng = rubarSedimentMng;
      coucheIdx = variable.getCoucheIdx();
      parentVariable = variable.getParentVariable();
    }

    @Override
    protected void fireObjectChanged(int _indexGeom, Object _newValue) {
      rubarSedimentMng.fireSedimentChanged();
    }

    @Override
    public int getSize() {
      return rubarSedimentMng.getNbNoeud();
    }

    @Override
    public double getValue(int idxNode) {
      H2dRubarSedimentCoucheContainer couches = rubarSedimentMng.getCouches(idxNode);
      return couches.getCoucheItem(coucheIdx).getValue(parentVariable);
    }


    @Override
    protected void internalSetValue(int _i, double _newV) {
      H2dRubarSedimentCoucheContainer couches = rubarSedimentMng.getCouches(_i);
      couches.getCoucheItem(coucheIdx).setValue(parentVariable, _newV);
    }
  }

  private final CtuluArrayObject couchesByNoeud;
  int nbNodes;
  Set<H2dRubarSedimentListener> listener_;
  private static final List<H2dRubarSedimentVariableType> VARIABLES;

  static {
    List<H2dRubarSedimentVariableType> variable = new ArrayList<H2dRubarSedimentVariableType>();
    int nb = H2dRubarSedimentCoucheContainer.NB_COUCHES_MAX;
    for (int i = 0; i < nb; i++) {
      variable.add(new H2dRubarSedimentVariableType(i, H2dRubarSedimentVariableType.EPAISSEUR));
      variable.add(new H2dRubarSedimentVariableType(i, H2dRubarSedimentVariableType.DIAMETRE));
      variable.add(new H2dRubarSedimentVariableType(i, H2dRubarSedimentVariableType.ETENDUE));
      variable.add(new H2dRubarSedimentVariableType(i, H2dRubarSedimentVariableType.CONTRAINTE));
    }

    VARIABLES = Collections.unmodifiableList(variable);
  }

  public static List<H2dRubarSedimentVariableType> getVariables() {
    return VARIABLES;
  }

  Map<H2dRubarSedimentVariableType, CtuluCollectionDoubleEdit> editablesList = new HashMap<>();
  private DicoParams dicoParams;
  H2dRubarSedimentInterface app;
  private int timeIdxUsed;

  /**
   * @param _app l
   */
  public H2dRubarSedimentMng(final H2dRubarSedimentInterface _app, DicoParams dicoParams) {
    this.dicoParams = dicoParams;
    couchesByNoeud = new SedimentModelObject(nbNodes);
    addListener(dicoParams);
    setCouches(_app);
    buildModels();
  }

  public H2dRubarSedimentMng(final int nbNodes, DicoParams dicoParams) {
    this.nbNodes = nbNodes;
    this.dicoParams = dicoParams;
    addListener(dicoParams);
    couchesByNoeud = new SedimentModelObject(nbNodes);
    for (int i = nbNodes - 1; i >= 0; i--) {
      couchesByNoeud.set(i, new H2dRubarSedimentCoucheContainer());
    }
    buildModels();
  }

  private void addListener(DicoParams dicoParams) {
    dicoParams.addModelListener(new DicoParamsListener() {
      @Override
      public void dicoParamsEntiteAdded(DicoParams _cas, DicoEntite _ent) {
        updateInitTimeIfNeeded(_ent);
      }

      @Override
      public void dicoParamsEntiteCommentUpdated(DicoParams _cas, DicoEntite _ent) {
      }

      @Override
      public void dicoParamsEntiteRemoved(DicoParams _cas, DicoEntite _ent, String _oldValue) {
        updateInitTimeIfNeeded(_ent);
      }

      @Override
      public void dicoParamsEntiteUpdated(DicoParams _cas, DicoEntite _ent, String _oldValue) {
        updateInitTimeIfNeeded(_ent);
      }

      @Override
      public void dicoParamsValidStateEntiteUpdated(DicoParams _cas, DicoEntite _ent) {
      }

      @Override
      public void dicoParamsVersionChanged(DicoParams _cas) {

      }
    });
  }

  /**
   * @param _l le listener a ajouter
   */
  public void addSedimentListener(final H2dRubarSedimentListener _l) {
    if (listener_ == null) {
      listener_ = new HashSet<H2dRubarSedimentListener>(3);
    }
    listener_.add(_l);
  }

  private void buildModels() {
    List<H2dRubarSedimentVariableType> all = getVariables();
    for (H2dRubarSedimentVariableType h2dRubarSedimentVariableType : all) {
      editablesList.put(h2dRubarSedimentVariableType, new ValuesByNodeModel(h2dRubarSedimentVariableType, this));
    }
  }

  public void clear(final CtuluCommandContainer _cmd) {
    couchesByNoeud.setAll(null, _cmd);
  }

  /**
   * @param _l le listener a tester
   * @return true si est contenu
   */
  public boolean containsAppListener(final H2dRubarSedimentListener _l) {
    return (listener_ != null) && listener_.contains(_l);
  }

  protected void fireSedimentChanged() {
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2dRubarSedimentListener) it.next()).sedimentChanged(this);
      }
    }
  }

  public CtuluCollectionDoubleEdit getCollectionDoubleEditable(H2dRubarSedimentVariableType var) {
    return editablesList.get(var);
  }

  public H2dRubarSedimentCoucheContainer getCouches(int idxNode) {
    return (H2dRubarSedimentCoucheContainer) couchesByNoeud.getValueAt(idxNode);
  }

  /**
   * @return le nombre d'element
   */
  public final int getNbNoeud() {
    return nbNodes;
  }

  /**
   * @return l'interface a utiliser pour sauvegarder les chroniques d'apport
   */
  public H2dRubarSedimentInterface getSavedInterface() {
    return new SaveAdapter();
  }

  private double getSedimentTimeFromDico() {
    DicoEntite initalTime = H2dRubarDicoModel.getInitalTime(dicoParams.getDicoFileFormatVersion());
    if (initalTime != null) {
      String value = dicoParams.getValue(initalTime);
      if (value != null) {
        try {
          return Double.parseDouble(value);
        } catch (NumberFormatException e) {
        }
      }
    }
    return 0;
  }

  int getTimeIdx() {
    if (app != null && app.getNbTimes() > 0) {
      double time = getSedimentTimeFromDico();
      int max = app.getNbTimes();
      for (int i = 0; i < max; i++) {
        if (CtuluLib.isEquals(time, app.getTimes(i), 1e-2)) {
          return i;
        }
      }
    }
    return 0;
  }

  /**
   * @param _l le listener a enlever
   */
  public void removeListener(final H2dRubarSedimentListener _l) {
    if (listener_ != null) {
      listener_.remove(_l);
    }
  }

  public void set(int[] idx, H2dRubarSedimentCoucheContainer container, CtuluCommandContainer _cmd) {
    couchesByNoeud.setAll(null, _cmd);
  }

  protected void setCouches(final H2dRubarSedimentInterface _app) {
    if (_app == null) {
      return;
    }
    this.app = _app;
    nbNodes = _app.getNbNoeuds();
    timeIdxUsed = getTimeIdx();
    couchesByNoeud.removeAll(null);
    for (int i = nbNodes - 1; i >= 0; i--) {
      H2dRubarSedimentCoucheContainer coucheContainer = _app.getCoucheContainer(timeIdxUsed, i);
      if (coucheContainer == null) {
        coucheContainer = new H2dRubarSedimentCoucheContainer();
      }
      couchesByNoeud.set(i, coucheContainer);
    }
  }

  protected void updateInitTimeIfNeeded(DicoEntite _ent) {
    if (H2dRubarDicoModel.getInitalTime(dicoParams.getDicoFileFormatVersion()) == _ent) {
      int newTimeIdx = getTimeIdx();
      if (newTimeIdx != timeIdxUsed) {
        setCouches(app);
      }
    }
  }
}
