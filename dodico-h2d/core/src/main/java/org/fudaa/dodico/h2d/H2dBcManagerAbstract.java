/*
 * @creation 20 nov. 2003
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import java.util.Iterator;
import java.util.List;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * Gestion des frontieres d'un projet. Contient le maillage.
 *
 * @author deniger
 * @version $Id: H2dBcManagerAbstract.java,v 1.16 2006-09-19 14:43:25 deniger Exp $
 */
public class H2dBcManagerAbstract implements H2dBcListenerDispatcher {

  protected List listeners_;

  protected EfGridInterface maillage_;

  /**
   * @param _m le maillage contenant les frontieres
   */
  public H2dBcManagerAbstract(final EfGridInterface _m) {
    maillage_ = _m;
    if (maillage_ == null) {
      new Throwable("grid is null").printStackTrace();
    }
  }

  protected void fireParametersForBoundaryPtsChange() {
    fireParametersForBoundaryPtsChange(null);
  }

  protected void fireParametersForBoundaryPtsChange(final H2dVariableType _t) {
    if (listeners_ != null) {
      for (final Iterator it = listeners_.iterator(); it.hasNext();) {
        ((H2dBcListener) it.next()).bcPointsParametersChanged(_t);
      }
    }
  }

  @Override
  public void fireBcFrontierStructureChanged(final H2dBcFrontierInterface _frontier) {
    if (listeners_ != null) {
      for (final Iterator it = listeners_.iterator(); it.hasNext();) {
        final H2dBcListener l = (H2dBcListener) it.next();
        l.bcFrontierStructureChanged(_frontier);
      }
    }
  }

  /**
   * @param _b la frontiere modifie. La variable est passee a null.
   */
  public void fireBcParametersChanged(final H2dBoundary _b) {
    if (listeners_ != null) {
      for (final Iterator it = listeners_.iterator(); it.hasNext();) {
        ((H2dBcListener) it.next()).bcParametersChanged(_b, null);
      }
    }
  }

  @Override
  public void fireBcParametersChanged(final H2dBoundary _b, final H2dVariableType _t) {
    if (listeners_ != null) {
      for (final Iterator it = listeners_.iterator(); it.hasNext();) {
        ((H2dBcListener) it.next()).bcParametersChanged(_b, _t);
      }
    }
  }

  @Override
  public void fireBcPointsParametersChanged(final H2dVariableType _t) {
    if (listeners_ != null) {
      for (final Iterator it = listeners_.iterator(); it.hasNext();) {
        ((H2dBcListener) it.next()).bcPointsParametersChanged(_t);
      }
    }
  }

  @Override
  public void fireBcTypeChanged(final H2dBoundary _b, final H2dBoundaryType _old) {
    if (listeners_ != null) {
      for (final Iterator it = listeners_.iterator(); it.hasNext();) {
        ((H2dBcListener) it.next()).bcBoundaryTypeChanged(_b, _old);
      }
    }
  }

  /**
   * @return le maillage
   */
  public final EfGridInterface getGrid() {
    return maillage_;
  }

  /**
   * @return le nombre total des points frontieres
   */
  public int getNbTotalPoint() {
    return maillage_.getFrontiers().getNbTotalPt();
  }

  /**
   * @param _idxFrontiere l'indice de la frontiere
   * @param _idxPtSurFrontiere l'indice du point sur la frontiere
   * @return le point de la frontiere n�<code>_idxFrontiere</code> et d'indice <code>_idxPtSurFrontiere</code> sur
   *         cette frontiere
   */
  /*
   * public final GISPoint getPoint(int _idxFrontiere,int _idxPtSurFrontiere){ return maillage_
   * .getPt(maillage_.getFrontiers().getIdxGlobal(_idxFrontiere, _idxPtSurFrontiere)); }
   */
}