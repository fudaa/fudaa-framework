/*
 * @creation 14 juin 2004
 * @modification $Date: 2007-06-11 13:06:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TObjectDoubleHashMap;
import gnu.trove.TObjectDoubleIterator;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarElementPropertyAbstract.java,v 1.23 2007-06-11 13:06:08 deniger Exp $
 */
public abstract class H2dRubarElementPropertyAbstract implements H2dVariableProviderInterface {
  protected class ElementDoubleArray extends CtuluArrayDouble {
    final H2dVariableType varElt_;

    /**
     * @param _init
     */
    public ElementDoubleArray(final H2dVariableType _var, final CtuluArrayDouble _init) {
      super(_init);
      varElt_ = _var;
    }

    /**
     * @param _init
     */
    public ElementDoubleArray(final H2dVariableType _var, final CtuluCollectionDouble _init) {
      super(_init);
      varElt_ = _var;
    }

    /**
     * @param _init
     */
    public ElementDoubleArray(final H2dVariableType _var, final double[] _init) {
      super(_init);
      varElt_ = _var;
    }

    /**
     * @param _nb
     */
    public ElementDoubleArray(final H2dVariableType _var, final int _nb) {
      super(_nb);
      varElt_ = _var;
    }

    /**
     * @param _init
     */
    public ElementDoubleArray(final H2dVariableType _var, final Object[] _init) {
      super(_init);
      varElt_ = _var;
    }

    /**
     * @param _init
     */
    public ElementDoubleArray(final H2dVariableType _var, final TDoubleArrayList _init) {
      super(_init);
      varElt_ = _var;
    }

    @Override
    protected void fireObjectChanged(int _indexGeom, Object _newValue) {
      H2dRubarElementPropertyAbstract.this.valuesChanged(varElt_);
    }

    @Override
    protected boolean internalSet(final int _i, final double _newV) {
      return super.internalSet(_i, _newV);
    }
  }

  /**
   * @param _doubleV map Object->double[]
   * @return -1 si taille differente
   */
  public static int getCommonValueSizeMapDouble(final Map _doubleV) {
    int r = -1;
    if (_doubleV != null) {
      for (final Iterator it = _doubleV.values().iterator(); it.hasNext(); ) {
        final int nSize = ((double[]) it.next()).length;
        if (r < 0) {
          r = nSize;
        } else if (r != nSize) {
          return -1;
        }
      }
    }
    return r;
  }

  Map<H2dVariableType, CtuluCollectionDoubleEdit> vValues_;
  protected EfGridInterface grid_;
  protected List var_;

  /**
   * @param _m une table H2DVariableType->double[]
   */
  public H2dRubarElementPropertyAbstract(final EfGridInterface _grid, final Map _m) {
    setMap(_m);
    grid_ = _grid;
  }

  protected CtuluArrayDouble createDoubleModel(final H2dVariableType _v, final CtuluCollectionDoubleEdit _init,
                                               final int _nbElt) {
    final CtuluArrayDouble r = new ElementDoubleArray(_v, _init);
    if (r.getSize() != _nbElt) {
      throw new IllegalArgumentException();
    }
    return r;
  }

  protected CtuluArrayDouble createDoubleModel(final H2dVariableType _v, final int _nbElt) {
    return new ElementDoubleArray(_v, _nbElt);
  }

  protected void setMap(final Map<H2dVariableType, double[]> _v) {
    vValues_ = new TreeMap();
    if (_v != null) {
      for (final Iterator it = _v.entrySet().iterator(); it.hasNext(); ) {
        final Map.Entry<H2dVariableType, double[]> e = (Map.Entry<H2dVariableType, double[]>) it.next();
        vValues_.put(e.getKey(), new CtuluArrayDouble(e.getValue()));
      }
      var_ = Collections.unmodifiableList(new ArrayList(vValues_.keySet()));
    }
  }

  protected void valuesChanged(final H2dVariableType _v) {

  }

  @Override
  public boolean containsVarToModify() {
    return var_ != null && var_.size() > 0;
  }

  /**
   * @param _idx les indices a considerer
   * @param _v la variable voulue
   * @return la valeur commune aux indices. Chaine vide si pas de valeur commune ou null si la variable n'est pas geree.
   */
  public String getCommonValue(final int[] _idx, final H2dVariableType _v) {
    final CtuluCollectionDoubleEdit d = (CtuluCollectionDoubleEdit) vValues_.get(_v);
    if (d == null) {
      return null;
    }
    if (d.isSameValues(_idx)) {
      return Double.toString(d.getValue(_idx[0]));
    }
    return CtuluLibString.EMPTY_STRING;
  }

  /**
   * @return la taille en commun des donnees. -1 si taille differente (ou vide).
   */
  public int getCommonValueSize() {
    int r = -1;
    for (final Iterator it = vValues_.values().iterator(); it.hasNext(); ) {
      final int nSize = ((CtuluCollectionDoubleEdit) it.next()).getSize();
      if (r < 0) {
        r = nSize;
      } else if (r != nSize) {
        return -1;
      }
    }
    return r;
  }

  @Override
  public final EfGridInterface getGrid() {
    return grid_;
  }

  /**
   * @param _t la variable a considerer
   * @return les doubles correspondants
   */
  @Override
  public CtuluCollectionDoubleEdit getModifiableModel(final H2dVariableType _t) {
    return (CtuluCollectionDoubleEdit) vValues_.get(_t);
  }

  @Override
  public Collection getUsableVariables() {
    return var_;
  }

  @Override
  public int getVariablesBlockLength() {
    return -1;
  }
  /**
   * @param _t la variable demandee
   * @param _i l'index
   * @return la valeur
   */
  public double getValue(final H2dVariableType _t, final int _i) {
    final CtuluCollectionDouble v = getViewedModel(_t);
    if (v != null) {
      return v.getValue(_i);
    }
    return Double.MIN_VALUE;
  }

  /**
   * @return une liste non modifiables des variables utilisees en si
   */
  public List getVars() {
    return var_;
  }

  @Override
  public H2dVariableType[] getVarToModify() {
    final H2dVariableType[] var = new H2dVariableType[var_.size()];
    var_.toArray(var);
    return var;
  }

  @Override
  public CtuluCollectionDouble getViewedModel(final H2dVariableType _t) {
    return getModifiableModel(_t);
  }

  @Override
  public CtuluCollectionDouble getValues(CtuluVariable _var) {
    return getViewedModel((H2dVariableType) _var);
  }

  @Override
  public final boolean isElementVar() {
    return true;
  }

  public void set(final int[] _eltIdx, final TObjectDoubleHashMap _typeValue, final CtuluCommandContainer _cmd) {
    if (_typeValue.size() == 0) {
      return;
    }
    final CtuluCommandComposite cmd = new CtuluCommandComposite();
    final TObjectDoubleIterator it = _typeValue.iterator();
    for (int i = _typeValue.size(); i-- > 0; ) {
      it.advance();
      setValues((H2dVariableType) it.key(), _eltIdx, it.value(), cmd);
    }
    if (_cmd != null) {
      _cmd.addCmd(cmd);
    }
  }


  public void setValues(final H2dVariableType _var, final int[] _idxPtElt, final double _values,
                        final CtuluCommandContainer _cmd) {
    final CtuluCollectionDoubleEdit d = (CtuluCollectionDoubleEdit) vValues_.get(_var);
    if (d == null) {
      return;
    }
    d.set(_idxPtElt, _values, _cmd);
  }

  public void setValues(final H2dVariableType _var, final int[] _idxPtElt, final double[] _values,
                        final CtuluCommandContainer _cmd) {
    final CtuluCollectionDoubleEdit d = (CtuluCollectionDoubleEdit) vValues_.get(_var);
    if (d == null) {
      return;
    }
    d.set(_idxPtElt, _values, _cmd);
  }
}
