/*
 * @creation 27 oct. 2003
 * 
 * @modification $Date: 2006-09-19 14:43:24 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.dodico.h2d.H2dBcListenerDispatcher;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * A UTILISER UNIQUEMENT POUR LES TESTS.
 * 
 * @author deniger
 * @version $Id: H2dTelemacBoundaryMutable.java,v 1.10 2006-09-19 14:43:24 deniger Exp $
 */
public class H2dTelemacBoundaryMutable extends H2dTelemacBoundary {

  public H2dTelemacBoundaryMutable(final int _idxFr) {
    super(_idxFr, null);
  }

  /**
   * @param _type le nouveau type
   * @return true si changement
   */
  public boolean setBoundaryType(final H2dBoundaryType _type) {
    if ((_type != null) && (_type != type_)) {
      type_ = _type;
      return true;
    }
    return false;
  }

  @Override
  public boolean setFlowrate(final String _string) {
    return super.setFlowrate(_string);
  }

  @Override
  public void setIdxDeb(final int _i) {
    super.setIdxDeb(_i);
  }

  @Override
  public void setIdxFin(final int _i) {
    super.setIdxFin(_i);
  }

  @Override
  public void setIdxmaillageFrontiere(final int _i) {
    super.setIdxmaillageFrontiere(_i);
  }

  @Override
  public boolean setLiquidOption(final String _string) {
    return super.setLiquidOption(_string);
  }

  @Override
  public boolean setTracer(String _string, int idx) {
    return super.setTracer(_string, idx);
  }

  @Override
  public boolean setTracerTypeImposed(final boolean _b, final H2dBcListenerDispatcher _l) {
    return super.setTracerTypeImposed(_b, _l);
  }

  @Override
  public boolean setValue(final H2dVariableType _type, final String _string) {
    return super.setValue(_type, _string);
  }

  @Override
  public boolean setVelocity(final String _string) {
    return super.setVelocity(_string);
  }

  @Override
  public boolean setVelocityProfil(final String _string) {
    return super.setVelocityProfil(_string);
  }
}