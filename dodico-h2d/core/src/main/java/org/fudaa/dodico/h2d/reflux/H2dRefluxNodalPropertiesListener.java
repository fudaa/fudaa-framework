/*
 * @creation 30 avr. 07
 * @modification $Date: 2007-06-14 11:59:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author fred deniger
 * @version $Id: H2dRefluxNodalPropertiesListener.java,v 1.2 2007-06-14 11:59:08 deniger Exp $
 */
public interface H2dRefluxNodalPropertiesListener {

  /**
   * @param _t la variable modifiee
   * @param _addedOrRemove TODO
   */
  void nodalPropertyChanged(H2dVariableType _t, boolean _addedOrRemove);

}
