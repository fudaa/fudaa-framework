/*
 *  @creation     22 d�c. 2004
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import java.text.NumberFormat;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarDTRResult.java,v 1.4 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dRubarDTRResult {

  /**
   * @return le pas de temps de stockage
   */
  double getTimeStep();

  /**
   * @return le nombre de point defini
   */
  int getNbPoint();

  NumberFormat getXYFormatter();

  /**
   * @param _i l'indice du point
   * @return le x en i
   */
  double getX(int _i);

  /**
   * @param _i l'indice du point
   * @return le y en i
   */
  double getY(int _i);
}