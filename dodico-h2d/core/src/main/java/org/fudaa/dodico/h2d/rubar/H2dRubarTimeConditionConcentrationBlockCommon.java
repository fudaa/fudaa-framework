/*
 * @creation 7 juil. 2004
 *
 * @modification $Date: 2007-03-02 13:00:51 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

/**
 * Conteneur tres libre afin de connaitre les valeurs communes a plusieurs points.
 *
 * @author Fred Deniger
 */
public class H2dRubarTimeConditionConcentrationBlockCommon {
  /**
   * @return un conteneur par defaut
   */
  public static H2dRubarTimeConditionConcentrationBlockCommon getEmptyCommon() {
    final H2dRubarTimeConditionConcentrationBlockCommon r = new H2dRubarTimeConditionConcentrationBlockCommon(null);
    r.concentrationCommon = false;
    r.diametreCommon = false;
    r.etendueCommon = false;
    r.diametre = null;
    r.etendue = null;
    r.concentration = null;
    return r;
  }

  public EvolutionReguliereAbstract concentration;
  public boolean concentrationCommon;
  /**
   * true si h commun.
   */
  public boolean diametreCommon;
  public EvolutionReguliereAbstract diametre;
  /**
   * true si h commun.
   */
  public EvolutionReguliereAbstract etendue;
  /**
   * true si h commun.
   */
  public boolean etendueCommon;

  /**
   * @param _c source pour initialise ce conteneur. Tous les booleens sont mis a true.
   */
  public H2dRubarTimeConditionConcentrationBlockCommon(final H2dRubarTimeConditionConcentrationBlock _c) {
    if (_c != null) {

      concentration = _c.getConcentration();
      etendue = _c.getEtendue();
      diametre = _c.getDiametre();
    }
    diametreCommon = true;
    etendueCommon = true;
    concentrationCommon = true;
  }

  /**
   * @param _t la variable h,qn ou qt
   * @return l'evolution correspondante.
   */
  public EvolutionReguliereAbstract getEvol(final H2dVariableType _t) {
    if (_t == H2dVariableTransType.CONCENTRATION) {
      return concentration;
    }
    if (_t == H2dVariableTransType.ETENDUE) {
      return etendue;
    }
    if (_t == H2dVariableTransType.DIAMETRE) {
      return diametre;
    }
    return null;
  }

  /**
   * Le boolean est a true si toutes les variables testees ont la meme evolution pour la meme variable.
   *
   * @param _t la variable h,qn ou qt
   * @return l'etat du boolean correspondant.
   */
  public boolean getVarState(final H2dVariableType _t) {
    if (_t == H2dVariableTransType.CONCENTRATION) {
      return concentrationCommon;
    }
    if (_t == H2dVariableTransType.DIAMETRE) {
      return diametreCommon;
    }
    if (_t == H2dVariableTransType.ETENDUE) {
      return etendueCommon;
    }
    new Throwable().printStackTrace();
    return false;
  }

  /**
   * Si true, cela signifie que les valeurs testees n'ont rien en commun.
   *
   * @return true si tous les booleens sont a false
   */
  public final boolean isNothingInCommons() {
    return ((!concentrationCommon) && (!diametreCommon) && (!etendueCommon));
  }

  /**
   * Permet de mettre a jour les booleens de ce conteneurs. Par exemple, si l'evolution h de _s est differente de
   * l'evolution h de ce conteneur le boolean h est mis a false et this.h a null. Attention avec cette methode: seules
   * les evolutions egales sont gardees.
   *
   * @param _s la condition a comparer
   * @return true si changement
   */
  public boolean keepSameEvolution(final H2dRubarTimeConditionConcentrationBlock _s) {
    boolean r = false;
    if (_s == null) {
      if (concentration != null) {
        concentration = null;
        concentrationCommon = false;
        r = true;
      }
      if (diametre != null) {
        diametre = null;
        diametreCommon = false;
        r = true;
      }
      if (etendue != null) {
        etendue = null;
        etendueCommon = false;
        r = true;
      }
    } else {
      if (concentration != _s.getConcentration()) {
        r = true;
        concentrationCommon = false;
        concentration = null;
      }
      if (etendue != _s.getEtendue()) {
        r = true;
        etendueCommon = false;
        etendue = null;
      }
      if (diametre != _s.getDiametre()) {
        r = true;
        diametreCommon = false;
        diametre = null;
      }
    }
    return r;
  }
}
