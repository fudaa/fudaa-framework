/*
 * @creation 3 nov. 2003
 * @modification $Date: 2006-07-10 15:19:34 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

/**
 * @author deniger
 * @version $Id: H2dVariableTypeContainer.java,v 1.7 2006-07-10 15:19:34 deniger Exp $
 */
public interface H2dVariableTypeContainer {

  /**
   * @return Alpha longueur melange
   */
  double getAlphaMixtureLength();

  /**
   * @return concentration
   */
  double getConcentration();

  /**
   * @return diam�tre
   */
  double getDiametre();

  /**
   * @return �tendue
   */
  double getEtendue();

  /**
   * @return evolution sedimento
   */
  double getEvolSedimValue();

  /**
   * @return friction
   */
  double getFriction();

  /**
   * @return elevation
   */
  double getH();
  
  double getSurfaceLibre();

  /**
   * @return perte de charge
   */
  double getPressureLoss();

  /**
   * @return flowrate
   */
  double getQ();

  /**
   * @return tracer
   */
  double getTracer();

  /**
   * @return tracer coef a
   */
  double getTracerCoeffA();

  /**
   * @return tracer coef b
   */
  double getTracerCoeffB();

  /**
   * @return velocity u
   */
  double getU();

  /**
   * @return velocity v
   */
  double getV();

  /**
   * @return ?
   */
  double getVelocity();

  /**
   * @return viscosite
   */
  double getViscosity();

  double getContrainte();

  double getEpaisseur();
}