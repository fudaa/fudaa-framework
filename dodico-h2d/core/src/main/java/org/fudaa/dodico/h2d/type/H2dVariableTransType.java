/*
 * @creation 15 d�c. 2004
 *
 * @modification $Date: 2007-05-04 13:46:37 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: H2dVariableTransType.java,v 1.12 2007-05-04 13:46:37 deniger Exp $
 */
public class H2dVariableTransType extends H2dVariableType {
  /**
   * ALPHA_LONGUEUR_MELANGE.
   */
  public static final H2dVariableTransType ALPHA_LONGUEUR_MELANGE = new H2dVariableTransType(H2dResource.getS("Alpha longueur m�lange"), null,
      "mixLenght") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getAlphaMixtureLength();
    }
  };
  public static final H2dVariableTransType VITESSE_FROTTEMENT = new H2dVariableTransType(H2dResource.getS("Vitesse de frottement"), null, "vitfro") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getAlphaMixtureLength();
    }
  };
  /**
   * Apport pluvial.
   */
  public static final H2dVariableTransType APPORT_PLUIE = new H2dVariableTransType(H2dResource.getS("Chronique d'apport"), null, "nbchro") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return 0;
    }

    @Override
    public Object getCommonUnit() {
      if (CtuluLib.isFrenchLanguageSelected()) {
        return "mm/heure";
      }
      return "mm/hour";
    }
  };
  /**
   * La concentration.
   */
  public static final H2dVariableTransType CONCENTRATION = new H2dVariableTransType(H2dResource.getS("Concentration"), null, "conc") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getConcentration();
    }
  };
  /**
   * La concentration.
   */
  public static final H2dVariableTransType DIAMETRE = new H2dVariableTransType(H2dResource.getS("Diam�tre"), null, "diam") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getDiametre();
    }
  };
  /**
   * La concentration.
   */
  public static final H2dVariableTransType ETENDUE = new H2dVariableTransType(H2dResource.getS("Etendue"), null, "et") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getEtendue();
    }
  };
  public static final H2dVariableTransType EPAISSEUR = new H2dVariableTransType(H2dResource.getS("Epaisseur"), null, "epaisseur") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getEpaisseur();
    }
  };
  public static final H2dVariableTransType CONTRAINTE = new H2dVariableTransType(H2dResource.getS("Contrainte"), null, "conttrainte") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getContrainte();
    }
  };
  public static final H2dVariableTransType FOND_INERODABLE = new H2dVariableTransType(H2dResource.getS("Cote du fond in�rodable"), "m",
      "bottom_inerodable") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      new Throwable().printStackTrace();
      return -1;
    }
  };
  public static final H2dVariableTransType DEPOT = new H2dVariableTransType(H2dResource.getS("Taux de d�p�t"), "mm/h", "depot") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      new Throwable().printStackTrace();
      return -1;
    }
  };
  public static final H2dVariableTransType VARIATION_COTE_FOND = new H2dVariableTransType(H2dResource.getS("Variation des cotes de fond"), "", "var_fond") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      new Throwable().printStackTrace();
      return -1;
    }
  };
  /**
   * TRACEUR .
   */
  public static final H2dVariableTransType TRACEUR = new H2dVariableTransType(H2dResource.getS("Traceur"), "TR", "tracer") {
    @Override
    public boolean canBeTransient() {
      return true;
    }

    @Override
    public Object getCommonUnit() {
      return "g/l";
    }

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getTracer();
    }
  };
  /**
   * TRACEUR_COEF_A .
   */
  public static final H2dVariableTransType TRACEUR_COEF_A = new H2dVariableTransType(H2dResource.getS("Traceur coeff A"), null, "tracer_a") {
    @Override
    public Object getCommonUnit() {
      return CtuluLibString.EMPTY_STRING;
    }

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getTracerCoeffA();
    }
  };
  /**
   * TRACEUR_COEF_B.
   */
  public static final H2dVariableTransType TRACEUR_COEF_B = new H2dVariableTransType(H2dResource.getS("Traceur coeff B"), null, "tracer_b") {
    @Override
    public Object getCommonUnit() {
      return CtuluLibString.EMPTY_STRING;
    }

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getTracerCoeffB();
    }
  };
  /**
   * Evolution sedimentaire.
   */
  public static final H2dVariableTransType EVOLUTION_SEDIMENTAIRE = new H2dVariableTransType(H2dResource.getS("Valeur �volutions s�dimentaires"),
      null, "evol_sed") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getEvolSedimValue();
    }
  };
  public static final H2dVariableTransType COTE_EAU_AMONT = new H2dVariableTransType(H2dResource.getS("Cote eau amont"), null, "cote_eau_amont") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      throw new IllegalArgumentException("not implemented");
    }
  };
  public static final H2dVariableTransType LARGEUR_DIAMETRE_BRECHE = new H2dVariableTransType(H2dResource.getS("Largeur diam�tre br�che"), null,
      "largeur_diametre_breche") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      throw new IllegalArgumentException("not implemented");
    }
  };
  public static final H2dVariableTransType COTE_FOND_BRECHE = new H2dVariableTransType(H2dResource.getS("Cote fond br�che"), null,
      "cote_fond_breche") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      throw new IllegalArgumentException("not implemented");
    }
  };
  public static final H2dVariableTransType DEBIT_LIQUIDE = new H2dVariableTransType(H2dResource.getS("D�bit liquide"), null,
      "debit_liquide") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      throw new IllegalArgumentException("not implemented");
    }
  };
  public static final H2dVariableTransType DEBIT_SOLIDE = new H2dVariableTransType(H2dResource.getS("D�bit solide"), null,
      "debit_solide") {
    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      throw new IllegalArgumentException("not implemented");
    }
  };
  private H2dVariableTransType parentVariable;

  /**
   * @param _nom
   * @param _telemacID
   * @param _varShortName
   */
  public H2dVariableTransType(final String _nom, final String _telemacID, final String _varShortName) {
    super(_nom, _telemacID, _varShortName);
  }

  private H2dVariableTransType() {
    super(null);
  }

  public static H2dVariableTransType createIndexedVar(H2dVariableTransType parentVar, int i) {

    final String varShortName = parentVar.getShortName() + "_" + CtuluLibString.getString(i);
    if (H2dVariableType.shortNameVar.containsKey(varShortName)) {
      return (H2dVariableTransType) H2dVariableType.shortNameVar.get(varShortName);
    }
    H2dVariableTransType result = new H2dVariableTransType(parentVar.getName() + CtuluLibString.getEspaceString(i), null,
        varShortName);
    result.blockIdx = i;
    result.setParentVariable(parentVar);
    return result;
  }

  private int blockIdx = -1;

  public int getBlockIdx() {
    return blockIdx;
  }

  private void setParentVariable(H2dVariableTransType parentVariable) {
    this.parentVariable = parentVariable;
  }

  @Override
  public H2dVariableTransType getParentVariable() {
    return parentVariable;
  }

  @Override
  public double getValue(final H2dVariableTypeContainer _container) {
    return 0;
  }
}
