/*
 * @creation 17 nov. 06
 *
 * @modification $Date: 2006-11-20 08:39:59 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.h2d.type.H2dRubarOuvrageTypeClient;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;

public class H2dRubarOuvrageTypeCreator implements H2dRubarOuvrageTypeClient {
  public H2dRubarOuvrageElementaireInterface result_;
  private H2dRubarGridAreteSource gridAreteSource;
  H2dRubarProjetType projetType;
  int nbTransportBlock;

  public H2dRubarOuvrageTypeCreator(H2dRubarProjetType projetType, int nbTransportBlock) {
    this.projetType = projetType;
    this.nbTransportBlock = nbTransportBlock;
  }

  public H2dRubarGridAreteSource getGridAreteSource() {
    return gridAreteSource;
  }

  public void setGridAreteSource(H2dRubarGridAreteSource gridAreteSource) {
    this.gridAreteSource = gridAreteSource;
  }

  @Override
  public void visitApport(final Object _o) {
    result_ = new H2dRubarOuvrageElementaireApport(projetType, nbTransportBlock);
  }

  @Override
  public void visitBreche(final Object _o) {
    result_ = new H2dRubarOuvrageElementaireBreche();
  }

  @Override
  public void visitDeversoir(final Object _o) {
    result_ = new H2dRubarOuvrageElementaireDeversoir();
  }

  @Override
  public void visitOrificeCirculaire(Object _o) {
    result_ = new H2dRubarOuvrageElementaireOrificeCirculaire();
  }

  @Override
  public void visitComposite(Object _o) {
    result_ = new H2dRubarOuvrageElementaireComposite(gridAreteSource);
  }

  @Override
  public void visitDeversoirHydraulique(Object _o) {
    result_ = new H2dRubarOuvrageElementaireDeversoirHydraulique();
  }

  @Override
  public void visitTransfert(final Object _o) {
    result_ = new H2dRubarOuvrageElementaireTransfert(projetType,nbTransportBlock);
  }
}
