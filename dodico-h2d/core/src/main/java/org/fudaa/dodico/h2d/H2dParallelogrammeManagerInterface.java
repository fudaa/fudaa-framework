/*
 *  @creation     29 sept. 2004
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

/**
 * @author Fred Deniger
 * @version $Id: H2dParallelogrammeManagerInterface.java,v 1.5 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dParallelogrammeManagerInterface {
  /**
   * @param _i le paral d'indice _i.
   * @return les donnees concernant le parallelogramme i. Null si non parall.
   */
  H2dParallelogrammeDataInterface getParall(int _i);

  /**
   * A utiliser si isParall=true.
   *
   * @return le nombre de parall utilise. 0 si "isParall=false".
   */
  int getNbParall();

}
