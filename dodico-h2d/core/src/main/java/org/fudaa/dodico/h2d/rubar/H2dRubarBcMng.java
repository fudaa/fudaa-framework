/*
 * @creation 9 juin 2004
 *
 * @modification $Date: 2007-04-30 14:21:34 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import com.memoire.fu.FuLog;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntObjectHashMap;
import gnu.trove.TIntObjectIterator;
import gnu.trove.TIntObjectProcedure;
import org.fudaa.ctulu.*;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.ef.impl.EfGridSourceDefaut;
import org.fudaa.dodico.h2d.H2dEvolutionUseCounter;
import org.fudaa.dodico.h2d.H2dEvolutionVariableMap;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.*;
import org.fudaa.dodico.mesure.EvolutionListenerDispatcherDefault;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.text.NumberFormat;
import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarBcMng.java,v 1.41 2007-04-30 14:21:34 deniger Exp $
 */
public final class H2dRubarBcMng extends EvolutionListenerDispatcherDefault {
  public static final int NEW_TRANSPORT_PROJECT_DEFAULT_BLOCK = 1;

  protected static class AreteMemento {
    private final H2dRubarArete ar_;
    private final H2dRubarBoundaryType type_;
    H2dRubarTimeCondition t_;

    /**
     * @param _edge l'arete non nulle a copier
     */
    public AreteMemento(final H2dRubarArete _edge) {
      ar_ = _edge;
      type_ = ar_.getType();
    }

    /**
     * @return l'arete memorisee
     */
    public H2dRubarArete getAr() {
      return ar_;
    }

    /**
     * @return les conditions tempo memorisees
     */
    public H2dRubarTimeCondition getT() {
      return t_;
    }

    /**
     * @return le type enregistre dans le memento
     */
    public H2dRubarBoundaryType getType() {
      return type_;
    }

    /**
     * @param _t les conditions a memoriser
     */
    public void setT(final H2dRubarTimeCondition _t) {
      t_ = _t;
    }
  }

  public GridSrcAdapter getSrcAdapter() {
    srcAdapter_.computeAreteEntrante();
    return srcAdapter_;
  }

  /**
   * Attention les aretes entrantes ne sont pas recalulees.
   *
   * @return le maillage plus les connexions
   */
  public H2dRubarGridAreteSource getGridWithConnectionAndWithoutLimit() {
    return srcAdapter_;
  }

  protected static H2dRubarBcMng createNew(final H2dRubarProjetType _type, final H2dRubarGrid _g) {
    final H2dRubarBcMng r = new H2dRubarBcMng();
    r.g_ = _g;
    r.type_ = _type == null ? H2dRubarProjetType.COURANTOLOGIE_2D : _type;
    r.tarages_ = H2dRubarTarageMng.createTarageMng(null, null);
    return r;
  }

  private GridSrcAdapter srcAdapter_;

  /**
   * @param _in la progression
   * @param _an l'analyse a remplir
   * @param _s la source
   * @param _liquidIdxTimeCl idxArete->Condition variable dans le temps
   * @param _areteIdxGlobalTarage idxArete->Loi de tarage
   */
  protected boolean create(final H2dRubarProjetType _type, final H2dRubarGridAreteSource _s,
                           H2dRubarTimeConditionInterface[] _liquidIdxTimeCl,
                           final TIntObjectHashMap _areteIdxGlobalTarage, final ProgressionInterface _in, final CtuluAnalyze _an,
                           final boolean[] _cliChanged, int nbConcentrationBlocks) {
    type_ = (_type == null ? H2dRubarProjetType.COURANTOLOGIE_2D : _type);
    this.nbConcentrationBlocks = nbConcentrationBlocks;
    // initialisation des champs deja crees
    // maillage
    if (g_ == null) {
      g_ = _s.getRubarGrid();
    } else {
      g_.initWith(_s.getRubarGrid());
    }
    g_.createIndexRegular(_in);
    // l'adapteur pour r��crire le tout
    srcAdapter_ = new GridSrcAdapter(_s.getNbDecimal(), _s.elementsVoisinsParElement(), _s.elementsVoisinsParArete());
    // courbes temporelles
    if (timeCl_.size() > 0) {
      final Object[] o = timeCl_.getValues();
      // on enleve les listeners par securite utile ??
      for (int i = o.length - 1; i >= 0; i--) {
        ((H2dRubarTimeCondition) o[i]).clearCurves();
      }
      timeCl_.clear();
    }

    // groupe
    if ((flowrateGroups_ != null) && (flowrateGroups_.size() > 0)) {
      // on enleve les listeners par securite utile ??
      for (final Iterator it = flowrateGroups_.values().iterator(); it.hasNext(); ) {
        ((H2dRubarTimeCondition) it.next()).clearCurves();
      }
      flowrateGroups_.clear();
    }
    // pour le tarage cela est fait a la fin
    // fin initialisation
    TIntObjectHashMap<H2dRubarTarageContent> tarageContentByIdxGlobal = new TIntObjectHashMap<H2dRubarTarageContent>();
    if (_areteIdxGlobalTarage != null) {
      TIntObjectIterator iterator = _areteIdxGlobalTarage.iterator();
      for (int i = _areteIdxGlobalTarage.size(); i-- > 0; ) {
        iterator.advance();
        H2dRubarTarageContent content = new H2dRubarTarageContent();
        content.setEvolution((EvolutionReguliereAbstract) iterator.value());
        tarageContentByIdxGlobal.put(iterator.key(), content);
      }
    }
    final H2dRubarGrid g = _s.getRubarGrid();
    int nb = g.getNbAretes();
    final List l = new ArrayList(nb / 2);
    final TIntArrayList lidx = new TIntArrayList(nb / 2);
    H2dRubarArete as;

    final NumberFormat fm = CtuluLibString.getFormatForIndexingInteger(nb);

    final TIntArrayList externeTrans = new TIntArrayList();
    boolean isAllLibre = true;
    final TIntArrayList tarageArete = new TIntArrayList();
    for (int i = 0; i < nb; i++) {
      as = g.getRubarArete(i);
      if (as.isExtern()) {
        l.add(as);
        lidx.add(i);
        if (as.isExternAndLiquidAndTransient()) {
          if (as.type_ != H2dRubarBcTypeList.LIBRE) {
            isAllLibre = false;
          }
          externeTrans.add(i);
        }
        // les courbes de tarages: si le tableau ne contient pas les elements demandes, ils sont
        // ajout�s.
        if (as.isTarageType()) {
          tarageArete.add(i);
          // si pas de courbes, on met a null et c'est le manager de loi de tarage qui
          // modifiera le tout
          if (!tarageContentByIdxGlobal.contains(i)) {
            tarageContentByIdxGlobal.put(i, new H2dRubarTarageContent(null, as.getType()));
          } else {
            tarageContentByIdxGlobal.get(i).setTarageType(as.getType());
          }
        }
      }
    }
    tarageArete.sort();
    final TIntArrayList idxWithError = new TIntArrayList();
    final TIntObjectProcedure proc = new TIntObjectProcedure() {
      @Override
      public boolean execute(int _a, Object _b) {
        final int i = tarageArete.binarySearch(_a);
        if (i < 0) {
          idxWithError.add(_a);
          return false;
        }
        return true;
      }
    };
    tarageContentByIdxGlobal.retainEntries(proc);
    if (idxWithError.size() > 0) {
      _an.addError(H2dResource.getS("Le fichier 'tar' d�finit des lois de tarages pour des ar�tes incompatibles"), -1);
      CtuluLibMessage.info("bad edges: " + CtuluLibString.arrayToString(idxWithError.toNativeArray()));
    }
    if (_in != null) {
      _in.setProgression(70);
    }
    final int[] idxGlobalFromIdxAreteLimiteEntrante = _s.getIdxAreteLimiteEntrante();
    nb = idxGlobalFromIdxAreteLimiteEntrante == null ? 0 : idxGlobalFromIdxAreteLimiteEntrante.length;

    boolean messageConLimDone = false;
    if (nb != externeTrans.size()) {
      if (isAllLibre && nb == 0) {
        FuLog.warning(" free edges only");
      } else {
        _an.addWarn(H2dResource.getS(
            "Des conditions limites liquides sont mal d�finies (fichier dat), des valeur par d�faut ont �t� charg�es"));
        FuLog.all(
            "aretes calcule\n" + CtuluLibString.arrayToString(externeTrans.toNativeArray()) + "\naretes lu"
                + CtuluLibString.arrayToString(idxGlobalFromIdxAreteLimiteEntrante) + "\nnb lus= " + nb + " nb attendu " + externeTrans.size());
        messageConLimDone = true;
        _liquidIdxTimeCl = null;
      }
    } //TODO Verifier si correct d'avoir mis else if au lieu de if.
    else if (nb > 0) {
      externeTrans.sort();
      if (!messageConLimDone && !Arrays.equals(externeTrans.toNativeArray(), CtuluLibArray.copy(
          idxGlobalFromIdxAreteLimiteEntrante))) {
        _an.addWarn(H2dResource.getS(
            "Des conditions limites liquides sont mal d�finies (fichier dat), des valeur par d�faut ont �t� charg�es"));
      }
      int idx;
      boolean isGroup;
      final TIntArrayList timeNotDefined = new TIntArrayList();
      if (idxGlobalFromIdxAreteLimiteEntrante != null) {
        for (int i = 0; i < nb; i++) {
          idx = idxGlobalFromIdxAreteLimiteEntrante[i];
          as = g.getRubarArete(idx);
          final List list = getVariableAllowed(as.getType());
          if ((list != null) && (list.size() > 0)) {
            H2dRubarTimeCondition timeS = _liquidIdxTimeCl == null ? null : (H2dRubarTimeCondition.create(_liquidIdxTimeCl[i],
                this));
            // on cree des cl par defaut
            if (timeS == null) {
              timeNotDefined.add(idx);
              timeS = new H2dRubarTimeCondition(null, null, null, null);
              timeS.verify(list, null, idx, H2dRubarBcMng.this);
            } // on verifie que toutes les courbes sont correctes
            else {
              timeS.verify(list, _an, idx, this);
            }
            isGroup = as.getType().isTypeDebitGlobal();
            // groupe debit
            if (isGroup) {
              if ((flowrateGroups_ == null) || (!flowrateGroups_.containsKey(as.getType()))) {
                if (flowrateGroups_ == null) {
                  flowrateGroups_ = new HashMap(10);
                }
                timeS.updateNom(((H2dRubarBoundaryFlowrateGroupType) as.getType()).getShortName());
                flowrateGroups_.put(as.getType(), timeS);
                timeS.setUsed();
              } // la courbe est deja definie en tant que 'groupe debit'
              else {
                timeS = (H2dRubarTimeCondition) flowrateGroups_.get(as.getType());
              }
            } // ce n'est pas un groupe: on met a jour le nom uniquement
            else {
              timeS.updateNom(H2dResource.getS("Ar�te") + CtuluLibString.ESPACE + fm.format(idx + 1));
            }

            timeS.verify(list, _an, idx, this);
            // pour les groupes, on utilise un avatar ..
            timeCl_.put(idx, isGroup ? H2dRubarTimeCondition.NULL_TIME_COND : timeS);

            if (!isGroup) {
              timeS.setUsed();
            }
          }
        }
      }
      // ecriture des indices errones dans le fichier de log
      if (timeNotDefined.size() > 0) {
        final String s = DodicoLib.getS("Des conditions limites ont �t� cr��es par d�faut");
        _an.addWarn(s + ". " + CtuluLibString.LINE_SEP + DodicoLib.getS("Les indices sont dans le fichier de log"), -1);
        CtuluLibMessage.info(s);
        CtuluLibMessage.info(CtuluLibString.arrayToString(timeNotDefined.toNativeArray()));
        _cliChanged[0] = true;
      }
    }
    externeAretes_ = new H2dRubarArete[l.size()];
    if (tarages_ == null) {
      tarages_ = H2dRubarTarageMng.createTarageMng(tarageContentByIdxGlobal, _an);
    } else {
      tarages_.initWith(tarageContentByIdxGlobal, _an);
    }
    // on groupes les courbes identiques
    H2dRubarBcMng.groupCommonsCurves(timeCl_);
    // r.tarages_.addTarageListener(r);
    l.toArray(externeAretes_);
    globalIdx_ = lidx.toNativeArray();
    return true;
  }

  private Set h2dListener_;
  private CtuluPermanentList timeVars_;

  protected void setProjectDispatcher(final H2dRubarProjectDispatcherListener _old, final H2dRubarProjectDispatcherListener _new) {
    if (_old != null) {
      removeListener(_old);
      removeEvolutionListener(_old);
      if (tarages_ != null) {
        tarages_.removeTarageListener(_old);
      }
    }
    if (_new != null) {
      addListener(_new);
      addEvolutionListener(_new);
      if (tarages_ != null) {
        tarages_.addTarageListener(_new);
      }
    }
  }

  H2dRubarArete[] externeAretes_;
  Map flowrateGroups_;
  H2dRubarGrid g_;
  int[] globalIdx_;
  boolean isModified_;
  final TIntObjectHashMap timeCl_ = new TIntObjectHashMap();
  private H2dRubarTarageMng tarages_;
  /**
   *
   */
  H2dRubarProjetType type_;
  int nbConcentrationBlocks = NEW_TRANSPORT_PROJECT_DEFAULT_BLOCK;
  int nbDecimal_;

  /**
   * @param nbConcentrationBlocks the number of blocks
   * @return [CONCENTRATION, DIAMETRE, ETENDUE] * number of blocks.
   */
  public static List<H2dVariableTransType> getTransportVariables(int nbConcentrationBlocks) {
    List<H2dVariableTransType> variables = new ArrayList<>();
    variables.add(H2dVariableTransType.CONCENTRATION);
    variables.add(H2dVariableTransType.DIAMETRE);
    variables.add(H2dVariableTransType.ETENDUE);
    for (int i = 1; i < nbConcentrationBlocks; i++) {
      variables.add(H2dVariableTransType.createIndexedVar(H2dVariableTransType.CONCENTRATION, i + 1));
      variables.add(H2dVariableTransType.createIndexedVar(H2dVariableTransType.DIAMETRE, i + 1));
      variables.add(H2dVariableTransType.createIndexedVar(H2dVariableTransType.ETENDUE, i + 1));
    }
    return variables;
  }

  /**
   * @param variableType
   * @return l'index du block si trouv�.
   */
  public static int getIndexBlock(H2dVariableType variableType) {
    if (variableType instanceof H2dVariableTransType) {
      H2dVariableTransType transType = (H2dVariableTransType) variableType;
      final H2dVariableTransType parentVariable = transType.getParentVariable();
      if (parentVariable == H2dVariableTransType.CONCENTRATION
          || parentVariable == H2dVariableTransType.DIAMETRE
          || parentVariable == H2dVariableTransType.ETENDUE
      ) {
        return transType.getBlockIdx() ;
      }
    }
    return -1;
  }

  public int getNbConcentrationBlocks() {
    return nbConcentrationBlocks;
  }

  public void setNbConcentrationBlocks(final int newNbConcentrationBlocks, CtuluCommandComposite _cmd) {

    final CtuluCommandComposite cmp = _cmd == null ? null : new CtuluCommandCompositeInverse();

    final int old = this.nbConcentrationBlocks;
    this.nbConcentrationBlocks = newNbConcentrationBlocks;

    initLimitConditions(cmp);

    if (cmp != null) {
      cmp.addCmd(new CtuluCommand() {
        @Override
        public void undo() {
          setNbConcentrationBlocks(old, null);
        }

        @Override
        public void redo() {
          setNbConcentrationBlocks(newNbConcentrationBlocks, null);
        }
      });
    }
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    if (old != newNbConcentrationBlocks) {
      fireNumberOfConcentrationChanged();
    }
  }

  private void initLimitConditions(CtuluCommandComposite cmp) {
    final Object[] cl = timeCl_.getValues();
    if (!CtuluLibArray.isEmpty(cl)) {
      for (int i = 0; i < cl.length; i++) {
        final H2dRubarTimeCondition cli = (H2dRubarTimeCondition) cl[i];
        initTimeCondition(cmp, cli);
      }
      Collection values = flowrateGroups_ == null ? Collections.emptyList() : flowrateGroups_.values();
      for (Object object : values) {
        initTimeCondition(cmp, (H2dRubarTimeCondition) object);
      }
    }
  }

  /**
   * @return les aretes entrantes dans l'ordre utilise par le fichier dat.
   */
  protected int[] getAreteEntrante() {
    if ((externeAretes_ == null) || (externeAretes_.length == 0)) {
      return new int[0];
    }
    final TIntArrayList l = new TIntArrayList(externeAretes_.length);
    for (int i = 0; i < externeAretes_.length; i++) {
      if (externeAretes_[i].isExternAndLiquidAndTransient()) {
        l.add(globalIdx_[i]);
      }
    }
    final int[] res = l.toNativeArray();
    Arrays.sort(res);
    return res;
  }

  private class GridSrcAdapter extends EfGridSourceDefaut implements H2dRubarGridAreteSource {
    int[][] elementsVoisinsParArete_;
    int[][] elementsVoisinsParElement_;
    int[] areteEntrante_;

    /**
     * @param _nbDecimal
     * @param _elementsVoisinsParArete
     * @param _elementsVoisinsParElement
     */
    public GridSrcAdapter(final int _nbDecimal, final int[][] _elementsVoisinsParElement, final int[][] _elementsVoisinsParArete) {
      super(g_, null);
      nbDecimal_ = _nbDecimal;
      elementsVoisinsParArete_ = _elementsVoisinsParArete;
      elementsVoisinsParElement_ = _elementsVoisinsParElement;
    }

    protected void computeAreteEntrante() {
      areteEntrante_ = getAreteEntrante();
    }

    @Override
    public int[][] elementsVoisinsParArete() {
      return elementsVoisinsParArete_;
    }

    @Override
    public int[][] elementsVoisinsParElement() {
      return elementsVoisinsParElement_;
    }

    @Override
    public int[] getIdxAreteLimiteEntrante() {
      return areteEntrante_;
    }

    @Override
    public int getNbDecimal() {
      return nbDecimal_;
    }

    @Override
    public H2dRubarGrid getRubarGrid() {
      return g_;
    }
  }

  protected H2dRubarBcMng() {
    super();
  }

  private void createDefaultCurves() {
    if (defaultEvolForConcentration_ != null) {
      return;
    }
    defaultEvolForConcentration_ = H2dRubarEvolution.createH2dRubarEvolution(H2dVariableTransType.CONCENTRATION,
        new EvolutionReguliere(), this);
    defaultEvolForConcentration_.setNom(H2dResource.getS("D�faut"));
    defaultEvolForDiametre_ = H2dRubarEvolution.createH2dRubarEvolution(H2dVariableTransType.DIAMETRE, new EvolutionReguliere(),
        this);
    defaultEvolForDiametre_.setNom(H2dResource.getS("D�faut"));
    defaultEvolForEtendue_ = H2dRubarEvolution.createH2dRubarEvolution(H2dVariableTransType.ETENDUE, new EvolutionReguliere(),
        this);
    defaultEvolForEtendue_.setNom(H2dResource.getS("D�faut"));
  }

  private H2dRubarTimeCondition getTimeCondition(final int _glIdx) {
    return (H2dRubarTimeCondition) timeCl_.get(_glIdx);
  }

  private void removeTimeCl(final int _globalIdx) {
    final H2dRubarTimeCondition t = (H2dRubarTimeCondition) timeCl_.remove(_globalIdx);
    if (t != null) {
      t.setUnUsed();
    }
  }

  public boolean isTimeConditionInit() {
    return timeCl_.size() > 0;
  }

  private void restoreTimeCl(final int _globIdx, final H2dRubarTimeCondition _t, final H2dRubarBoundaryType _bcType) {
    if (timeCl_.contains(_globIdx)) {
      getTimeCondition(_globIdx).majWithNewList(getVariableAllowed(_bcType), _t);
    } else {
      final H2dRubarTimeCondition n = _t.createCopy();
      n.setUsed();
      timeCl_.put(_globIdx, n);
    }
  }

  private boolean verifyGroupDebit(final H2dRubarBoundaryType _t, final H2dRubarTimeCondition _s) {
    if (_t.isTypeDebitGlobal()) {
      if ((this.flowrateGroups_ != null) && (this.flowrateGroups_.containsKey(_t))) {
        final H2dRubarTimeCondition t = (H2dRubarTimeCondition) flowrateGroups_.get(_t);
        _s.h_ = null;
        _s.qn_ = (H2dRubarEvolution) t.getQnEvol();
        _s.qt_ = (H2dRubarEvolution) t.getQtEvol();
        return true;
      }
      new Throwable().printStackTrace();
    }
    return false;
  }

  /**
   * Initialise l'etat "modifie".
   */
  protected void clearModified() {
    isModified_ = false;
  }

  public void iterateOnBcCurves(final H2dEvolutionUseCounter _m) {
    for (int i = 0; i < getNbBcArete(); i++) {
      final int global = getGlobalIdx(i);
      final H2dRubarTimeCondition cl = getTimeCondition(global);
      if (cl != null) {
        cl.fillWithTransientCurves(_m, i);
      }
    }
  }

  public void iterateOnTarageCurves(final H2dEvolutionUseCounter _m) {
    if (tarages_ == null) {
      return;
    }
    for (int i = 0; i < getNbBcArete(); i++) {
      final int global = getGlobalIdx(i);
      final EvolutionReguliereInterface evol = tarages_.getEvol(global);
      if (evol != null) {
        _m.add(evol, H2dVariableType.LOI_TARAGE, i);
      }
    }
  }

  public void fillWithTransientCurves(final H2dEvolutionVariableMap _m) {
    iterateOnBcCurves(_m);
    if (flowrateGroups_ != null) {
      for (final Iterator it = flowrateGroups_.values().iterator(); it.hasNext(); ) {
        final H2dRubarTimeCondition cond = (H2dRubarTimeCondition) it.next();
        // -1 pour les groupes
        cond.fillWithTransientCurves(_m, -1);
        EvolutionReguliereAbstract e = cond.getQtEvol();
        if (e != null && _m.getVarFor(e) != null) {
          _m.reaffectEvol(e, H2dVariableType.DEBIT_M3);
        }
        e = cond.getQnEvol();
        if (e != null && _m.getVarFor(e) != null) {
          _m.reaffectEvol(e, H2dVariableType.DEBIT_M3);
        }
      }
    }
  }

  protected void fireEdgeTypeChanged() {
    isModified_ = true;
    if (h2dListener_ != null) {
      for (final Iterator it = h2dListener_.iterator(); it.hasNext(); ) {
        ((H2dRubarBcListener) it.next()).areteTypeChanged();
      }
    }
  }

  protected void fireBathymetrieChanged() {
    isModified_ = true;
    if (h2dListener_ != null) {
      for (final Iterator it = h2dListener_.iterator(); it.hasNext(); ) {
        ((H2dRubarBcListener) it.next()).bathyChanged();
      }
    }
  }

  protected void fireFondDurChanged() {
    isModified_ = true;
    if (h2dListener_ != null) {
      for (final Iterator it = h2dListener_.iterator(); it.hasNext(); ) {
        ((H2dRubarBcListener) it.next()).fondDurChanged();
      }
    }
  }

  protected void fireFlowrateGroupChanged(final H2dRubarBoundaryFlowrateGroupType _t) {
    isModified_ = true;
    if (h2dListener_ != null) {
      synchronized (h2dListener_) {
        for (final Iterator it = h2dListener_.iterator(); it.hasNext(); ) {
          ((H2dRubarBcListener) it.next()).flowrateGroupChanged(_t);
        }
      }
    }
  }

  protected void fireTimeClChanged() {
    isModified_ = true;
    if (h2dListener_ != null) {
      for (final Iterator it = h2dListener_.iterator(); it.hasNext(); ) {
        ((H2dRubarBcListener) it.next()).timeClChanged();
      }
    }
  }

  protected void fireProjectTypeChanged() {
    isModified_ = true;
    if (h2dListener_ != null) {
      for (final Iterator it = h2dListener_.iterator(); it.hasNext(); ) {
        ((H2dRubarBcListener) it.next()).projectTypeChanged();
      }
    }
  }

  protected void fireNumberOfConcentrationChanged() {
    if (h2dListener_ != null) {
      for (final Iterator it = h2dListener_.iterator(); it.hasNext(); ) {
        ((H2dRubarBcListener) it.next()).numberOfConcentrationChanged();
      }
    }
  }

  protected void initTimeClFromMemento(final int _globalIdx, final AreteMemento _mem) {
    _mem.getAr().setType(_mem.getType());
    if (_mem.t_ == null) {
      removeTimeCl(_globalIdx);
    } else {
      restoreTimeCl(_globalIdx, _mem.t_, _mem.getType());
    }
    updateGroupeCourbe(_globalIdx, _mem.getAr().getType());
  }

  /**
   * @param _a non null et de taille >0
   * @param _newType le nouveau bord
   * @param _mementos
   * @return true si changement dans les cli
   */
  protected boolean modifyAreteIntern(final H2dRubarGrid _g, final int[] _a, final H2dRubarBoundaryType _newType,
                                      final H2dRubarTimeCondition _varTypeEvolutions, final TIntObjectHashMap _mementos) {
    // true si on veut enregistrer les anciennes valeurs des aretes
    final boolean addMemento = _mementos != null;
    H2dRubarBcMng.AreteMemento mem = null;
    // verif des groupes debit pour etre sur.
    verifyGroupDebit(_newType, _varTypeEvolutions);
    List v = getVariableAllowed(_newType);
    if (_newType.isTypeDebitGlobal()) {
      v = null;
    }
    boolean timeModified = false;
    for (int i = _a.length - 1; i >= 0; i--) {
      final H2dRubarArete a = _g.getRubarArete(_a[i]);
      // on enregsitre les arete liquides
      if (addMemento) {
        mem = createAreteMemento(a);
      }
      if (a.setType(_newType)) {
        updateTimeValues(_a[i], v, _varTypeEvolutions, mem, _newType);
        // pour simplifier ... on ne regarde pas si les courbes changent r�ellement.
        timeModified = true;
        if (addMemento) {
          _mementos.put(_a[i], mem);
        }
      } else {
        final boolean t = updateTimeValues(_a[i], v, _varTypeEvolutions, mem, _newType);
        timeModified |= t;
        if (t && addMemento) {
          _mementos.put(_a[i], mem);
        }
      }
    }
    return timeModified;
  }

  /**
   * @return la table a utiliser pour sauvegarder le fichier CLI
   */
  public H2dRubarTimeConditionInterface[] getCliInOrder() {
    H2dRubarTimeConditionInterface[] r = null;
    final int[] idx = getAreteEntrante();
    if (idx == null) {
      return null;
    }
    r = new H2dRubarTimeConditionInterface[idx.length];
    for (int i = 0; i < idx.length; i++) {
      final int globalIdx = idx[i];
      final H2dRubarBoundaryType type = g_.getRubarArete(globalIdx).getType();
      if (type != H2dRubarBcTypeList.TARAGE && !H2dRubarBcTypeList.TARAGE.equals(type) && !(type instanceof H2dRubarBoundaryTarageGroupType)) {
        H2dRubarTimeCondition cn = null;
        if (type.isTypeDebitGlobal()) {
          cn = getGroupTimeCondition((H2dRubarBoundaryFlowrateGroupType) type);
        } else {
          cn = (H2dRubarTimeCondition) timeCl_.get(globalIdx);
        }
        if (cn == null && type != H2dRubarBcTypeList.CRITIQUE) {
          FuLog.error("DRU: time condition is null for idx= " + globalIdx + " type " + type, new Throwable());
        }
        r[i] = cn;
      }
    }
    return r;
  }

  public boolean hasDataForCli() {
    final int[] idx = getAreteEntrante();
    if (idx == null) {
      return false;
    }
    for (int i = 0; i < idx.length; i++) {
      final int globalIdx = idx[i];
      final H2dRubarBoundaryType type = g_.getRubarArete(globalIdx).getType();
      if (!H2dRubarBcTypeList.TARAGE.equals(type) && !H2dRubarBcTypeList.CRITIQUE.equals(type)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Met a jour les donnees des aretes.
   *
   * @param _idxGlobal
   * @param _v
   * @param _timeSource
   * @param _mem
   * @return true si modif
   */
  protected boolean updateTimeValues(final int _idxGlobal, final List _v, final H2dRubarTimeCondition _timeSource,
                                     final AreteMemento _mem,
                                     final H2dRubarBoundaryType _newType) {
    H2dRubarTimeCondition mem = null;
    H2dRubarTimeCondition toModify = (H2dRubarTimeCondition) timeCl_.get(_idxGlobal);
    if ((toModify != null) && (_mem != null)) {
      mem = toModify.createCopy();
    }
    boolean modified = false;
    if (toModify == null) {
      if ((_v != null) && (_v.size() > 0)) {
        modified = true;
        toModify = _timeSource.createCopy();
        toModify.removeNonTorrentiel();
        toModify.setUsed();
        timeCl_.put(_idxGlobal, toModify);
      }
    } else {
      modified = toModify.majWithNewList(_v, _timeSource);
      if (toModify.isAllNull()) {
        timeCl_.remove(_idxGlobal);
      }
    }
    if (modified && _mem != null) {
      _mem.t_ = mem;
    }
    updateGroupeCourbe(_idxGlobal, _newType);

    return modified;
  }

  private void updateGroupeCourbe(final int _idxGlobal, final H2dRubarBoundaryType _bcType) {
    // ce cas correspond au groupe debit. On utilise un classe de courbe nulle.
    if (_bcType.isLiquide() && (!timeCl_.contains(_idxGlobal))) {
      timeCl_.put(_idxGlobal, H2dRubarTimeCondition.NULL_TIME_COND);
    }
  }

  protected boolean verifyTimeData(final int[] _areteIdx, final H2dRubarBoundaryType _t, final H2dRubarTimeCondition _s) {
    if (_t.isTypeDebitGlobal()) {
      return verifyGroupDebit(_t, _s);
    }
    final List l = getVariableRequired(_t);
    if ((l == null) || (l.size() == 0)) {
      return true;
    }
    final List varNotSet = _s.isVariableRequiredSet(l);
    if (varNotSet == null) {
      return true;
    }
    for (int i = _areteIdx.length - 1; i >= 0; i--) {
      final H2dRubarTimeCondition c = (H2dRubarTimeCondition) timeCl_.get(_areteIdx[i]);
      if (c == null) {
        return false;
      }
      if (!c.isVariableSet(varNotSet)) {
        return false;
      }
    }
    return true;
  }

  /**
   * @param _l le listener a ajouter
   */
  public void addListener(final H2dRubarBcListener _l) {
    if (h2dListener_ == null) {
      h2dListener_ = new HashSet(10);
    }
    if (_l == null) {
      FuLog.error("DHY: listener is null");
    } else {
      h2dListener_.add(_l);
    }
  }

  /**
   * @param _a l'arete a copier
   * @return le memento correspondant
   */
  public AreteMemento createAreteMemento(final H2dRubarArete _a) {
    return new AreteMemento(_a);
  }

  /**
   * @return la liste de toutes les variables valides pour les conditions de bord
   */
  public CtuluPermanentList getAllTimeVar() {
    if (timeVars_ == null) {
      if (type_ == H2dRubarProjetType.COURANTOLOGIE_2D) {
        timeVars_ = new CtuluPermanentList(CtuluLibArray.sort(new Object[]{H2dVariableType.DEBIT_NORMAL, H2dVariableType.DEBIT_TANGENTIEL,
            H2dVariableType.COTE_EAU}));
      } else {
        timeVars_ = new CtuluPermanentList(
            CtuluLibArray.sort(
                new Object[]{H2dVariableType.DEBIT_NORMAL, H2dVariableType.DEBIT_TANGENTIEL,
                    H2dVariableType.COTE_EAU, H2dVariableTransType.CONCENTRATION, H2dVariableTransType.DIAMETRE, H2dVariableTransType.ETENDUE}));
      }
    }
    return timeVars_;
  }

  /**
   * @param _idx l'indice sur la frontiere
   * @return l'arete d'indice de bord _idx
   */
  public H2dRubarArete getBcArete(final int _idx) {
    return externeAretes_[_idx];
  }

  /**
   * @return la liste des bords utilisables
   */
  public CtuluPermanentList getBordList() {
    return H2dRubarBcTypeList.getList();
  }

  /**
   * @param _ar les index globaux d'arete
   * @return les valeurs communes a ces index
   */
  public H2dRubarTimeConditionCommon getCommonTimeCondition(final int[] _ar) {
    if ((_ar == null) || (_ar.length == 0)) {
      return H2dRubarTimeConditionCommon.getEmptyCommon(nbConcentrationBlocks);
    }
    H2dRubarArete a = g_.getRubarArete(_ar[0]);
    H2dRubarTimeConditionCommon r0 = null;
    if (a.getType().isTypeDebitGlobal()) {
      r0 = new H2dRubarTimeConditionCommon(getGroupTimeCondition((H2dRubarBoundaryFlowrateGroupType) a.getType()));
    } else {
      r0 = new H2dRubarTimeConditionCommon((H2dRubarTimeCondition) timeCl_.get(_ar[0]));
    }
    for (int i = _ar.length - 1; i > 0; i--) {
      a = g_.getRubarArete(_ar[i]);
      if (a == null) {
        new Throwable("prob null").printStackTrace();
        return null;
      }
      if (a.getType().isTypeDebitGlobal()) {
        r0.keepSameEvolution(getGroupTimeCondition((H2dRubarBoundaryFlowrateGroupType) a.getType()));
      } else {
        r0.keepSameEvolution((H2dRubarTimeCondition) timeCl_.get(_ar[i]));
      }
      if (r0.isNothingInCommon()) {
        return r0;
      }
    }
    return r0;
  }

  /**
   * @return le tableau des types de type "groupe d�bit"
   */
  public H2dRubarBoundaryFlowrateGroupType[] getDebitGroupType() {
    return H2dRubarBcTypeList.getDebitGroupType(getBordList());
  }

  /**
   * @param _frIdx l'indice de l'arete de frontiere
   * @return l'indice global
   */
  public int getGlobalIdx(final int _frIdx) {
    return globalIdx_[_frIdx];
  }

  /**
   * @return le maillage utilise
   */
  public H2dRubarGrid getGridVolume() {
    return g_;
  }

  /**
   * @param _t le type recherche
   * @return les conditions globales pour ce type de bord.
   */
  public H2dRubarTimeCondition getGroupTimeCondition(final H2dRubarBoundaryFlowrateGroupType _t) {
    return (flowrateGroups_ == null) ? null : (H2dRubarTimeCondition) flowrateGroups_.get(_t);
  }

  /**
   * @return le nombre d'arete de bord.
   */
  public int getNbBcArete() {
    return externeAretes_ == null ? 0 : externeAretes_.length;
  }

  /**
   * @return le manager des courbes de tarage
   */
  public H2dRubarTarageMng getTarageMng() {
    return tarages_;
  }

  /**
   * @return la liste des type de bord reellement utilises
   */
  public List getUsedBoundaryType() {
    final Set r = new HashSet(15);
    if (externeAretes_ == null) {
      return Collections.EMPTY_LIST;
    }
    for (int i = externeAretes_.length - 1; i >= 0; i--) {
      r.add(externeAretes_[i].getType());
    }
    final List rf = new ArrayList(r);
    Collections.sort(rf);
    return rf;
  }

  /**
   * @param _t le type de bord a tester
   * @return la list des variables correspondantes
   */
  public List getVariableAllowed(final H2dRubarBoundaryType _t) {
    if (_t == null) {
      return null;
    }
    return _t.getAllowedVariable(type_);
  }

  /**
   * @param _t le type de bord a tester
   * @return la list des variables correspondantes
   */
  public List getVariableRequired(final H2dRubarBoundaryType _t) {
    return _t.getRequiredVariable(type_);
  }

  /**
   * @param _t le type de bord a tester
   * @return true si le groupe est defini
   */
  public boolean isFlowrateGroupDefined(final H2dRubarBoundaryFlowrateGroupType _t) {
    return (flowrateGroups_ != null) && (flowrateGroups_.containsKey(_t));
  }

  /**
   * @return true si modifie
   */
  public boolean isModified() {
    return isModified_;
  }

  /**
   * @param _t le groupe a tester
   * @return true si le type de bord est utilise
   */
  public boolean isUsedBoundaryType(final H2dRubarBoundaryType _t) {
    if (externeAretes_ == null || _t == null) {
      return false;
    }
    if (_t.isSolide()) {
      return externeAretes_.length != timeCl_.size();
    }
    final TIntObjectIterator it = timeCl_.iterator();
    for (int i = timeCl_.size(); i-- > 0; ) {
      it.advance();
      if (_t == g_.getRubarArete(it.key()).getType()) {
        return true;
      }
    }
    return tarages_.isUsed(_t);
  }

  /**
   * @param _a les aretes a modifier.
   * @param _t le nouveau type de bord.
   * @param _s les courbes pour chaque type de variables.
   * @return la commande si modif
   */
  public CtuluCommand modifyArete(final int[] _a, final H2dRubarBoundaryType _t, final H2dRubarTimeCondition _s,
                                  final EvolutionReguliereAbstract _tarageEvolution) {
    if (!verifyTimeData(_a, _t, _s)) {
      CtuluLibMessage.error("des courbes ne sont pas d�finies");
      return null;
    }
    final CtuluCommandCompositeInverse r = new CtuluCommandCompositeInverse();
    if ((_t == H2dRubarBcTypeList.TARAGE) && (_tarageEvolution == null)) {
      CtuluLibMessage.error("courbe de tarage non definie");
      return null;
    }
    r.addCmd(getTarageMng().modifyTarage(_a, _t, _tarageEvolution));
    final TIntObjectHashMap mem = new TIntObjectHashMap(_a.length);
    final boolean timeCliModified = modifyAreteIntern(g_, _a, _t, _s, mem);
    if (mem.size() > 0) {
      if (h2dListener_ != null) {
        fireEdgeTypeChanged();
      }
      if (timeCliModified) {
        fireTimeClChanged();
      }
      r.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          modifyAreteIntern(g_, mem.keys(), _t, _s, null);
          fireEdgeTypeChanged();
          if (timeCliModified) {
            fireTimeClChanged();
          }
        }

        @Override
        public void undo() {
          final TIntObjectIterator iterator = mem.iterator();
          for (int i = mem.size(); i-- > 0; ) {
            iterator.advance();
            final H2dRubarBcMng.AreteMemento a = (H2dRubarBcMng.AreteMemento) iterator.value();
            initTimeClFromMemento(iterator.key(), a);
          }
          isModified_ = true;
          fireEdgeTypeChanged();
          if (timeCliModified) {
            fireTimeClChanged();
          }
        }
      });
    }
    return r.getSimplify();
  }

  /**
   * @param _l le listener a enlever
   */
  public void removeListener(final H2dRubarBcListener _l) {
    if (h2dListener_ != null) {
      synchronized (h2dListener_) {
        h2dListener_.remove(_l);
      }
    }
  }

  /**
   * Permet d'initialiser (si pas deja fait) le groupe.
   *
   * @param _t le groupe a definir
   * @return la commande si modif TODO transport
   */
  public CtuluCommand setDefined(final H2dRubarBoundaryFlowrateGroupType _t, final H2dRubarTimeCondition timeCondition) {
    if (isFlowrateGroupDefined(_t)) {
      return null;
    }
    if (flowrateGroups_ == null) {
      flowrateGroups_ = new TreeMap();
    }
    final CtuluCommandCompositeInverse inv = new CtuluCommandCompositeInverse();
    final H2dRubarTimeCondition n = timeCondition.createCopy();
    n.removeNonTorrentielForHauteurOnly();
    n.setUsed();
    flowrateGroups_.put(_t, n);
    fireFlowrateGroupChanged(_t);
    inv.addCmd(new CtuluCommand() {
      @Override
      public void redo() {
        n.setUsed();
        flowrateGroups_.put(_t, n);
        fireFlowrateGroupChanged(_t);
      }

      @Override
      public void undo() {
        ((H2dRubarTimeCondition) flowrateGroups_.remove(_t)).setUnUsed();
        fireFlowrateGroupChanged(_t);
      }
    });
    return inv.getSimplify();
  }

  /**
   * @param _t le groupe a modifier
   * @param _evolInit l'evolution a mettre
   * @param _type le type concerne qn,qt ou h
   * @return la commande ou null si aucune modif
   */
  public CtuluCommand setEvolution(final H2dRubarBoundaryFlowrateGroupType _t, final H2dRubarEvolution _evolInit,
                                   final H2dVariableType _type, final int blockIdx) {
    if (!isFlowrateGroupDefined(_t)) {
      return null;
    }
    final H2dRubarEvolution evol = _evolInit == H2dRubarParameters.NON_TORENTIEL ? null : _evolInit;
    final H2dRubarTimeCondition r = getGroupTimeCondition(_t);
    final H2dRubarEvolution old = r.getEvol(_type, blockIdx);
    if (old == evol) {
      return null;
    }
    final CtuluCommandCompositeInverse cmd = new CtuluCommandCompositeInverse();
    r.setEvolution(_type, evol, blockIdx, cmd, true);
    fireFlowrateGroupChanged(_t);
    cmd.addCmd(new CtuluCommand() {
      @Override
      public void redo() {
        final H2dRubarTimeCondition redo = getGroupTimeCondition(_t);
        redo.setEvolution(_type, evol, blockIdx, null, true);
        fireFlowrateGroupChanged(_t);
      }

      @Override
      public void undo() {
        final H2dRubarTimeCondition rundo = getGroupTimeCondition(_t);
        rundo.setEvolution(_type, old, blockIdx, null, true);
        fireFlowrateGroupChanged(_t);
      }
    });
    return cmd.getSimplify();
  }

  /**
   * Permet de supprimer (si non utilise) la definition d'un groupe de bord.
   *
   * @param _t le type de bord a annuler
   * @return la commande si modif
   */
  public CtuluCommand setUndefined(final H2dRubarBoundaryFlowrateGroupType _t) {
    if (!isFlowrateGroupDefined(_t) || isUsedBoundaryType(_t)) {
      return null;
    }
    final H2dRubarTimeCondition timeMem = (H2dRubarTimeCondition) flowrateGroups_.remove(_t);
    fireFlowrateGroupChanged(_t);
    return new CtuluCommand() {
      @Override
      public void redo() {
        flowrateGroups_.remove(_t);
        fireFlowrateGroupChanged(_t);
      }

      @Override
      public void undo() {
        flowrateGroups_.put(_t, timeMem);
        fireFlowrateGroupChanged(_t);
      }
    };
  }

  public static void groupCommonsCurves(final TIntObjectHashMap _idxCl) {
    if (_idxCl == null) {
      return;
    }
    final Object[] o = _idxCl.getValues();

    if (!CtuluLibArray.isEmpty(o)) {
      final H2dRubarTimeCondition[] cl = new H2dRubarTimeCondition[o.length];
      System.arraycopy(o, 0, cl, 0, o.length);
      for (final Iterator it = H2dRubarTimeCondition.VAR_FORMAT.keySet().iterator(); it.hasNext(); ) {
        groupCommonsCurves(cl, (H2dVariableType) it.next());
      }
    }
  }

  public static void groupCommonsCurves(final H2dRubarTimeCondition[] _cl, final H2dVariableType variable) {
    final BitSet done = new BitSet(_cl.length);
    for (int i = 0; i < _cl.length; i++) {
      // si c'est un groupe on ne s'en occupe pas
      final H2dRubarTimeCondition timeCondition = _cl[i];
      if (timeCondition != H2dRubarTimeCondition.NULL_TIME_COND) {
        int nbBlock = 1;
        if (H2dRubarTimeConditionConcentrationBlock.isTransientVariable(variable)) {
          nbBlock = timeCondition.getNbConcentrationBlock();
        }
        // courbe deja prise
        for (int blockIdx = 0; blockIdx < nbBlock; blockIdx++) {
          final H2dRubarEvolution e = timeCondition.getEvol(variable, blockIdx);
          if (e != null) {
            for (int j = i + 1; j < _cl.length; j++) {
              if (!done.get(j)) {
                final EvolutionReguliere ej = _cl[j].getEvol(variable, blockIdx);
                if (e.isEquivalentTo(ej)) {
                  done.set(j);
                  _cl[j].setEvolution(variable, e, blockIdx, null, true);
                }
              }
            }
          }
        }
      }
    }
  }

  protected H2dRubarProjetType getType() {
    return type_;
  }

  H2dRubarEvolution defaultEvolForConcentration_;
  H2dRubarEvolution defaultEvolForDiametre_;
  H2dRubarEvolution defaultEvolForEtendue_;

  protected void setType(final H2dRubarProjetType _type, final CtuluCommandContainer _cmd) {
    timeVars_ = null;
    final H2dRubarProjetType old = type_;
    type_ = _type;
    final CtuluCommandComposite cmp = _cmd == null ? null : new CtuluCommandCompositeInverse();
    initLimitConditions(cmp);

    if (_cmd != null) {
      cmp.addCmd(new CtuluCommand() {
        @Override
        public void undo() {
          type_ = old;
          fireProjectTypeChanged();
        }

        @Override
        public void redo() {
          type_ = _type;
          fireProjectTypeChanged();
        }
      });
    }
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }

    fireProjectTypeChanged();
  }

  private void initTimeCondition(final CtuluCommandComposite cmp, final H2dRubarTimeCondition cli) {
    if (type_ == H2dRubarProjetType.TRANSPORT) {
      createDefaultCurves();
      cli.initTransport(defaultEvolForConcentration_, defaultEvolForDiametre_, defaultEvolForDiametre_, nbConcentrationBlocks, cmp);
    } else {
      cli.clearTransport(cmp);
    }
  }

  public void fireGridPointChanged() {
    isModified_ = true;
    if (h2dListener_ != null) {
      for (final Iterator it = h2dListener_.iterator(); it.hasNext(); ) {
        ((H2dRubarBcListener) it.next()).nodeInGridChanged();
      }
    }
  }
}
