/*
 *  @creation     28 nov. 2003
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

/**
 * @author deniger
 * @version $Id: H2DNodalPropertyMixteChangeListener.java,v 1.6 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2DNodalPropertyMixteChangeListener {
  /**
   * @param _source la propriete changee
   */
  void nodalPropertyMixteChanged(H2dNodalPropertyMixte _source);
}
