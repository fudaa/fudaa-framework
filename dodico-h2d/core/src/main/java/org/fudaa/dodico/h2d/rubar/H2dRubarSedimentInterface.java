package org.fudaa.dodico.h2d.rubar;


public interface H2dRubarSedimentInterface {

  int getNbTimes();

  int getNbNoeuds();

  double getTimes(int idx);

  H2dRubarSedimentCoucheContainer getCoucheContainer(int idxTime, int idxNoeud);

}