/*
 * @creation 17 mai 2004
 * 
 * @modification $Date: 2006-09-19 14:43:24 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import com.memoire.fu.FuLog;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandCompositeInverse;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.dico.DicoParamsInterface;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * Les variable de telemac pouvant etre gere par un mot-cl� ou non.
 * 
 * @author Fred Deniger
 * @version $Id: H2dTelemacBcParameter.java,v 1.19 2006-09-19 14:43:24 deniger Exp $
 */
public abstract class H2dTelemacBcParameter {

  private final DicoEntite.Vecteur ent_;

  private final List bordType_;

  boolean isFixedByUser_ = true;
  protected final H2dTelemacBcManager bcMng;

  H2dTelemacBcParameter(final H2dTelemacBcManager _mng, final DicoEntite.Vecteur _ent, final H2dBoundaryType[] _bords) {
    ent_ = _ent;
    this.bcMng = _mng;
    // l'ordre est important
    Arrays.sort(_bords);
    bordType_ = new CtuluPermanentList(_bords);

    if (ent_ != null) {
      ent_.setNbElem(_mng.getNbLiquidBoundary() * getNbInternValues());
    }
    firstUse(_mng);
    computeValueWhenKeywordChanged(_mng);
  }

  public Map getVariablesForPointInfo(final H2dTelemacDicoFileFormatVersion _v, final H2dTelemacBoundary _t, final DicoParamsInterface _dico) {
    return null;
  }

  /**
   * @return true si le parametre doit etre active pour chaque frontiere : traceur.
   */
  public boolean hasToBeActivatedForBoundary() {
    return false;
  }

  /**
   * @param _p le conteneur des mot-cles
   * @param _a l'analyse qui recevra les messages d'erreur
   */
  public void fillAnalyseWithError(final DicoParams _p, final CtuluAnalyze _a) {
    if ((ent_ != null) && (!_p.isValueValideFor(ent_))) {
      _a.addError(ent_.getNom() + " : " + getErrorString(_p), -1);
    }
  }

  /**
   * Methode appelee la premiere fois pour initialiser correctement le parametre.
   * 
   * @param _mng le manager
   */
  private void firstUse(final H2dTelemacBcManager _mng) {
    if (ent_ == null) {
      return;
    }
    int newVal = 0;
    if (_mng.isBoundaryTypeUsed(bordType_)) {
      newVal = _mng.getNbLiquidBoundary() * getNbInternValues();
    } else {
      newVal = 0;
    }
    _mng.dico_.setTaille(ent_, newVal, null);
    ent_.setRequired(isKeywordImposed());
    // le mot-cle correspondant est utilise: la variable est fixe sur les bords
    if (_mng.dico_.isValueSetFor(ent_)) {
      setFixedByUser(true);
    } else {
      // si aucun bord du type correspondant n'est utilise, il est fixe sur les bord
      boolean used = false;
      for (int i = _mng.getNbLiquidBoundary() - 1; i >= 0 && !used; i--) {
        if (isParameterForType(_mng.getLiquidBoundary(i).getType())) {
          used = true;
        }
      }
      // pas utilise
      if (used) {
        if (this.hasToBeActivatedForBoundary()) {
          final H2dVariableType t = getVariable();
          boolean imposed = false;
          for (int i = _mng.getNbLiquidBoundary() - 1; i >= 0 && !imposed; i--) {
            if (((H2dTelemacBoundary) _mng.getLiquidBoundary(i)).isTypeImposed(t)) {
              imposed = true;
            }
          }
          // jamais impos�e
          setFixedByUser(!imposed);
        }
      } else {
        setFixedByUser(true);
      }
    }

    _mng.dico_.testIfValid(ent_);
  }

  /**
   * A appeler lorsque la structure des bords a ete modifiee.
   * 
   * @param _mng le manager des bords
   * @param _c le vecteur dans lequel il faut ajouter les commandes (undo/redo)
   */
  public void updateDicoWhenStructureChange(final H2dTelemacBcManager _mng, final CtuluCommandContainer _c) {
    if (ent_ == null) {
      return;
    }
    int newVal = 0;
    if (_mng.isBoundaryTypeUsed(bordType_)) {
      newVal = _mng.getNbLiquidBoundary() * getNbInternValues();
      final boolean isImposed = isKeywordImposed() || isFixedByUser_;
      _mng.dico_.setRequired(ent_, isImposed, _c);
      if (isImposed || (_mng.dico_.isValueSetFor(ent_))) {
        _mng.dico_.setValue(ent_, getDicoValueFromBoundaries(_mng), _c);
      }
    } else {
      newVal = 0;
      _mng.dico_.setRequired(ent_, false, _c);
      _mng.dico_.removeValue(ent_, _c);
    }
    _mng.dico_.setTaille(ent_, newVal, _c);
  }

  /**
   * A appeler lorsque l'utilisateur a modifier l'etat du parametre.
   * 
   * @param _mng le manager des bords
   * @return la commande
   */
  public CtuluCommand updateWhenUserStateChanged(final H2dTelemacBcManager _mng) {
    final CtuluCommandCompositeInverse r = new CtuluCommandCompositeInverse();
    updateDicoWhenStructureChange(_mng, r);
    return r.getSimplify();
  }

  /**
   * @return les type de bords geres par cette variable
   */
  public List getBoundariesType() {
    return bordType_;
  }

  /**
   * @param _p le fichier dico source
   * @return true si la variable est definie dans le fichier dico
   */
  public boolean isValueSetInDico(final DicoParamsInterface _p) {
    return _p.isValueSetFor(ent_);
  }

  /**
   * @param _b la frontiere a considerer
   * @param _t la variable a tester
   * @return true si cette variable est transient
   */
  public boolean isValueTransientFor(final H2dTelemacBoundary _b, final H2dVariableType _t, int idxIntern) {
    return _b.isVariableTransient(_t, idxIntern);
  }

  /**
   * @param _p le manager
   * @param _b la frontiere a tester
   * @return true si la variable est definie comme fixe sur tout le bord
   */
  public boolean isValueFixedFor(final H2dTelemacBcManager _p, final H2dTelemacBoundary _b) {
    return isValueSetInDico(_p.dico_);
  }

  /**
   * @param _b la frontiere a tester
   * @return true si cette variable est definie comme transient
   */
  public boolean isValueTransientFor(final H2dTelemacBoundary _b, int idxIntern) {
    return (_b.getType().isLiquide()) && (_b.isVariableTransient(getVariable(), idxIntern));
  }

  /**
   * @param _p le fichier dico
   * @param _b la frontiere a tester
   * @return true si fixe ou transient
   */
  public boolean isValueFixedOrTransient(final H2dTelemacBcManager _p, final H2dTelemacBoundary _b, int idxIntern) {
    return isValueFixedFor(_p, _b) || isValueTransientFor(_b, idxIntern);
  }

  /**
   * @param _v la variable a tester
   * @return true si cette variable est cette variable
   */
  public boolean isParameterFor(final H2dVariableType _v) {
    return false;
  }

  /**
   * @param _v le tableau a tester
   * @return true si est une variable pour une des variables
   */
  public boolean isParameterFor(final H2dVariableType[] _v) {
    if (_v == null) {
      return false;
    }
    for (int i = _v.length - 1; i >= 0; i--) {
      if (isParameterFor(_v[i])) {
        return true;
      }
    }
    return false;
  }

  /**
   * @return la variable principale
   */
  public abstract H2dVariableType getVariable();

  /**
   * @param _t le type de bord
   * @return true si doit etre fixe pour ce type de variable
   */
  public final boolean isParameterForType(final H2dBoundaryType _t) {
    return Collections.binarySearch(bordType_, _t) >= 0;
  }

  /**
   * Par defaut, renvoie l'oppose de la methode isVariableOnSpace.
   * 
   * @return true si le mot-cl� doit etre initialise avec le type de bord support�
   */
  public abstract boolean isKeywordImposed();

  /**
   * @return le mot-cl� correspondant
   */
  public DicoEntite.Vecteur getEntite() {
    return ent_;
  }

  public int getNbInternValues() {
    return 1;
  }

  public String getInternValueName(int idx) {
    return null;
  }

  /**
   * @param _b le bord a considerer
   * @param valueIdx TODO
   * @return la valeur pour cette variable a partir du bord _b
   */
  public abstract String getValueFromBord(H2dTelemacBoundary _b, int valueIdx);

  /**
   * Permet d'initialiser le bord _b avec la valeur s.
   * 
   * @param _b le bord a modifier
   * @param _s la nouvelle valeur
   * @param valueIdx TODO
   * @return true si la valeur a ete mise
   */
  protected abstract boolean setValueInBord(H2dTelemacBoundary _b, String _s, int valueIdx);

  protected boolean setValueInBord(final H2dTelemacBoundary _b, final String[] _s) {
    if (_s != null && _s.length == 1) {
      return setValueInBord(_b, _s[0], 0);
    }
    return false;
  }

  /**
   * @param _bcMng le manager de frontiere
   * @param _b la frontiere a modifier
   * @param valueIdx TODO
   * @param _value la nouvelle valeur
   * @return la commande de modif ou null si pas de modif.
   */
  public boolean setValue(final H2dTelemacBcManager _bcMng, final H2dTelemacBoundary _b, int valueIdx, final String _value,
      final CtuluCommandContainer _cmd) {
    if ((_value != null) && (_value.trim().length() > 0) && (!_value.equals(getValueFromBord(_b, valueIdx)))) {
      if (_bcMng.isBoundaryTypeUsed(this.bordType_)) {
        final String s = buildNewValues(_bcMng, _b, valueIdx, _value);
        if (s != null) {
          return _bcMng.dico_.setValue(ent_, s, _cmd);
        }
      }
    }
    return false;
  }

  public boolean setValues(final H2dTelemacBcManager _bcMng, final H2dTelemacBoundary _b, final String[] _value, final CtuluCommandContainer _cmd) {
    if (_bcMng.isBoundaryTypeUsed(this.bordType_)) {
      final String oldValue = _bcMng.dico_.getValue(ent_);
      final String s = buildNewValues(_bcMng, _b, _value);
      if (s != null) {
        _bcMng.dico_.setValue(ent_, s, null);
        if (_cmd != null) {
          final int deb = _b.getIdxDeb();
          final int end = _b.getIdxFin();
          _cmd.addCmd(new CtuluCommand() {

            private H2dTelemacBoundary find() {
              if (_bcMng.liquidBorder_ == null) {
                return null;
              }
              for (H2dTelemacBoundary boundary : _bcMng.liquidBorder_) {
                if ((boundary.getIdxDeb() == deb) && (boundary.getIdxFin() == end)) {
                  return boundary;
                }
              }
              return null;
            }

            @Override
            public void undo() {
              H2dTelemacBoundary find = find();
              if (find != null) {
                _bcMng.dico_.setValue(ent_, oldValue, null);
              }
            }

            @Override
            public void redo() {
              H2dTelemacBoundary find = find();
              if (find != null) {
                setValues(_bcMng, find, _value, null);
              }

            }
          });
        }
      }
    }
    return false;
  }

  /**
   * @param _bcMng le manager de frontiere
   * @param _b la frontiere a modifier
   * @param _value la nouvelle valeur
   * @return le chaine
   */
  private String buildNewValues(final H2dTelemacBcManager _bcMng, final H2dTelemacBoundary _b, int tracerIdx, final String _value) {
    final int n = _bcMng.getNbLiquidBoundary();
    final String defautValue = ent_.getType().getDefaultValue();
    String value = _value;
    if ((_value == null) || (_value.trim().length() == 0)) {
      value = defautValue;
    }
    if (ent_.getType().isValide(value)) {
      setValueInBord(_b, value, tracerIdx);
      int nbIntern = getNbInternValues();
      final String[] r = new String[n * nbIntern];
      int idx = 0;
      for (int i = 0; i < n; i++) {
        boolean useDefault = !isParameterForType(_bcMng.getLiquidBoundary(i).getType());
        for (int intern = 0; intern < nbIntern; intern++) {
          if (useDefault) {
            r[idx] = defautValue;
          } else {
            r[idx] = getValueFromBord((H2dTelemacBoundary) _bcMng.getLiquidBoundary(i), intern);
            if (r[idx] == null) {
              r[idx] = defautValue;
            }
          }
          idx++;
        }
      }
      return ent_.getStringValuesArray(r);
    }
    return null;
  }

  /**
   * @param _bcMng le manager de frontiere
   * @param _b la frontiere a modifier
   * @param _value la nouvelle valeur
   * @return le chaine
   */
  private String buildNewValues(final H2dTelemacBcManager _bcMng, final H2dTelemacBoundary _b, final String[] _value) {
    final int n = _bcMng.getNbLiquidBoundary();
    final String defautValue = ent_.getType().getDefaultValue();
    setValueInBord(_b, _value);
    int nbIntern = getNbInternValues();
    final String[] r = new String[n * nbIntern];
    int idx = 0;
    for (int i = 0; i < n; i++) {
      boolean useDefault = !isParameterForType(_bcMng.getLiquidBoundary(i).getType());
      for (int intern = 0; intern < nbIntern; intern++) {
        if (useDefault) {
          r[idx] = defautValue;
        } else {
          r[idx] = getValueFromBord((H2dTelemacBoundary) _bcMng.getLiquidBoundary(i), intern);
          if (CtuluLibString.isEmpty(r[idx])) {
            r[idx] = defautValue;
          }
        }
        idx++;
      }
    }
    return ent_.getStringValuesArray(r);
  }

  /**
   * Recupere les valeurs dans les frontieres pour construire la chaine.
   * 
   * @param _bcMng le manager
   * @return la chaine a ajouter au fichier dico
   */
  public String getDicoValueFromBoundaries(final H2dTelemacBcManager _bcMng) {
    final int n = _bcMng.getNbLiquidBoundary();
    final int nbIntern = getNbInternValues();
    final String defautValue = ent_.getType().getDefaultValue();
    final String[] r = new String[n * nbIntern];
    int idx = 0;
    for (int i = 0; i < n; i++) {
      boolean isType = isParameterForType(_bcMng.getLiquidBoundary(i).getType());
      for (int intern = 0; intern < nbIntern; intern++) {
        if (isType) {
          r[idx] = getValueFromBord((H2dTelemacBoundary) _bcMng.getLiquidBoundary(i), intern);
        }
        if (r[idx] == null) {
          r[idx] = defautValue;
        }
        idx++;
      }
    }
    return ent_.getStringValuesArray(r);
  }

  /**
   * @return la valeur par de l'entite
   */
  public String getDefaultDataTypeValue() {
    String defaultValue = ent_.getType().getDefaultValue();
    return CtuluLibString.isEmpty(defaultValue) ? CtuluLibString.ZERO : defaultValue;
  }

  /**
   * @param _b la frontiere a initialises
   */
  public void setDefaultValue(final H2dTelemacBoundary _b) {
    if (_b.getType().isLiquide()) {
      int nb = getNbInternValues();
      for (int intern = 0; intern < nb; intern++) {
        setValueInBord(_b, ent_.getType().getDefaultValue(), intern);
      }
    }
  }

  /**
   * @param _p le fichier dico
   * @return true si valide
   */
  public boolean isValid(final DicoParamsInterface _p) {
    return _p.isValueValideFor(ent_);
  }

  /**
   * @param _p la fichier dico de ref
   * @return la chaine d'erreur ou nul si aucune
   */
  public String getErrorString(final DicoParamsInterface _p) {
    return _p.getInvalidMessage(ent_);
  }

  final void computeValueWhenKeywordChanged(final H2dTelemacBcManager _bcMng) {
    if (_bcMng == null) {
      return;
    }
    final int nb = _bcMng.getNbLiquidBoundary();
    if (_bcMng.dico_.isValueSetFor(ent_)) {
      final String s = _bcMng.dico_.getValue(ent_);
      final String[] values = ent_.getValues(s);
      int nbIntern = getNbInternValues();
      for (int frontier = 0; frontier < nb; frontier++) {
        _bcMng.getLiquidBoundary(frontier).checkNbValues(nbIntern, getDefaultDataTypeValue(), getVariable());
      }
      if (values.length != nb * nbIntern) {
        FuLog.all(H2dResource.getS("Le mot-cl� {0} doit contenir {1} champs (soit le nombre de fronti�res liquides)", ent_.getNom(),
            CtuluLibString.getString(nb)));
      }
      int nbLiquidBoundaryInKeyWord = values.length / getNbInternValues();
      int idx = 0;

      final int min = Math.min(nbLiquidBoundaryInKeyWord, nb);

      for (int i = 0; i < min; i++) {
        for (int intern = 0; intern < getNbInternValues(); intern++) {
          String value = idx >= values.length ? ent_.getType().getDefaultValue() : values[idx];
          setValueInBord((H2dTelemacBoundary) _bcMng.getLiquidBoundary(i), value, intern);
          idx++;
        }
      }
      for (int i = min; i < nb; i++) {
        for (int intern = 0; intern < getNbInternValues(); intern++) {
          setValueInBord((H2dTelemacBoundary) _bcMng.getLiquidBoundary(i), ent_.getType().getDefaultValue(), intern);
        }
      }
    } else {
      for (int i = nb - 1; i >= 0; i--) {
        for (int intern = 0; intern < getNbInternValues(); intern++) {
          setValueInBord((H2dTelemacBoundary) _bcMng.getLiquidBoundary(i), ent_.getType().getDefaultValue(), intern);
        }
      }
    }
  }

  /**
   * Renvoie les variables qui peuvent etre definies localement sur les points du bord passe en parametre.
   * 
   * @param _v la version du projet
   * @param _t le bord en question
   * @param _dico le fichier dico associee
   * @return les variables modifiables pour les points du bord _t.
   */
  public H2dVariableType[] getVariablesForPoint(final H2dTelemacDicoFileFormatVersion _v, final H2dTelemacBoundary _t, final DicoParamsInterface _dico) {
    if (getVariable() == null) {
      return null;
    }
    if (isValueSetInDico(_dico)) {
      return null;
    }
    return new H2dVariableType[] { getVariable() };
  }

  /**
   * @return un comparateur pour les parametres de bords :compare alpha les noms des variables.
   */
  public static Comparator createVariableComparator() {
    return new EntiteComparator();
  }

  static class EntiteComparator implements Comparator {

    @Override
    public int compare(final Object _o1, final Object _o2) {
      final H2dTelemacBcParameter v1 = (H2dTelemacBcParameter) _o1;
      final H2dTelemacBcParameter v2 = (H2dTelemacBcParameter) _o2;
      return v1.getVariable().compareTo(v2.getVariable());
    }
  }

  /**
   * @return Returns the isFixedByUser.
   */
  public boolean isFixedByUser() {
    return isFixedByUser_;
  }

  /**
   * @param _isFixedByUser The isFixedByUser to set.
   */
  private void setFixedByUser(final boolean _isFixedByUser) {
    isFixedByUser_ = _isFixedByUser;
  }

  /**
   * Permet de modifier la politique d'utilisation de ce parametre. Si modif, la commande est ajouter au vecteur de
   * commandes passe en parametre.
   * 
   * @param _isFixedByUser la nouvelle valeur pour la politique de l'utilisateur
   * @param _mng le manager de bord concerne
   * @param _c le vecteur des undo/redo
   */
  public void setFixedByUserAndCmd(final boolean _isFixedByUser, final H2dTelemacBcManager _mng, final CtuluCommandCompositeInverse _c) {
    if (_isFixedByUser == isFixedByUser_) {
      return;
    }
    final boolean old = isFixedByUser_;
    isFixedByUser_ = _isFixedByUser;
    _c.addCmd(new CtuluCommand() {

      @Override
      public void undo() {
        isFixedByUser_ = old;

      }

      @Override
      public void redo() {
        isFixedByUser_ = !old;
      }
    });
    updateDicoWhenStructureChange(_mng, _c);
    if (!isFixedByUser_ && (!isKeywordImposed())) {
      _mng.getDicoParams().removeValue(ent_, _c);
    }
  }
}