/*
 * @creation 25 juin 2003
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.h2d;

import gnu.trove.TDoubleProcedure;
import gnu.trove.TIntDoubleHashMap;

/**
 * @author deniger
 * @version $Id: H2dNodalProperty.java,v 1.10 2006-09-19 14:43:25 deniger Exp $
 */
public class H2dNodalProperty {

  /**
   * Procedure qui compare si le double est toujours le meme.
   *
   * @author Fred Deniger
   * @version $Id: H2dNodalProperty.java,v 1.10 2006-09-19 14:43:25 deniger Exp $
   */
  public static class TSameDouble implements TDoubleProcedure {

    private boolean first_ = true;
    private boolean r_ = true;
    private double value_;

    @Override
    public boolean execute(final double _d) {
      if (first_) {
        value_ = _d;
        first_ = false;
        return true;
      }
      if (_d != value_) {
        r_ = false;
        return false;
      }
      return true;
    }

    /**
     * A utiliser que si isPermanent renvoie true.
     *
     * @return la valeur permanente rencontree
     */
    public double getPermanentValue() {
      return value_;
    }

    /**
     * @return true si toutes les valeurs rencontrees sont les memes.
     */
    public boolean isPermanent() {
      return r_;
    }
  }

  protected double defaultValue_;
  protected TIntDoubleHashMap indicePermanentValeur_;
  protected String nom_;
  protected String unite_;

  /**
   * @param _nom le nom de la propriete nodale
   */
  public H2dNodalProperty(final String _nom) {
    nom_ = _nom;
  }

  /**
   * Cherche si les valeurs specifiees ne sont pas toutes les meme. Si oui, le tableau est videe est la valeur par
   * defaut est utilisee.
   */
  public void computeFixe() {
    if (indicePermanentValeur_.size() != 0) {
      final TSameDouble test = new TSameDouble();
      indicePermanentValeur_.forEachValue(test);
      if (test.isPermanent()) {
        defaultValue_ = test.getPermanentValue();
        indicePermanentValeur_.clear();
      }
    }
  }

  /**
   * @return la valeur par defaut utilisee
   */
  public double getDefaultValue() {
    return defaultValue_;
  }

  /**
   * @return le nom de cette propriete
   */
  public String getNom() {
    return nom_;
  }

  /**
   * @return le nombre de point explicitement specifie
   */
  public int getPermanentSpecifieNb() {
    return indicePermanentValeur_.size();
  }

  /**
   * @param _i l'indice du point
   * @return si ce point est specifie, renvoie la valeur. Sinon renvoie la valeur par defaut
   */
  public double getPermanentValueForIndex(final int _i) {
    if ((indicePermanentValeur_ != null) && (indicePermanentValeur_.size() > 0)) {
      return indicePermanentValeur_.get(_i);
    }
    return defaultValue_;
  }

  /**
   * @return l'unite de cette prop.
   */
  public String getUnite() {
    return unite_;
  }
}