/*
 * @creation 14 d�c. 2004
 *
 * @modification $Date: 2007-05-22 13:11:25 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import gnu.trove.TDoubleHashSet;
import gnu.trove.TIntObjectHashMap;
import gnu.trove.TObjectIntHashMap;
import gnu.trove.TObjectIntIterator;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormater;
import org.fudaa.ctulu.collection.CtuluArrayObject;
import org.fudaa.ctulu.fileformat.FortranLib;
import org.fudaa.dodico.h2d.H2dEvolutionUseCounter;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.mesure.EvolutionListener;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author Fred Deniger
 */
public class H2dRubarVentMng implements EvolutionListener {
  private class VentModelObject extends CtuluArrayObject {
    /**
     * @param _nb
     */
    public VentModelObject(final int _nb) {
      super(_nb);
    }

    @Override
    protected boolean internalSet(final int _i, final Object _newV) {
      if (super.list_[_i] != _newV) {
        if (super.list_[_i] != null) {
          final H2dRubarEvolution e = (H2dRubarEvolution) super.list_[_i];
          e.setUnUsed(false);
          if (e.getUsed() == 0) {
            updateUsedCourbe(e);
          }
        }
        if (_newV != null) {
          final H2dRubarEvolution e = (H2dRubarEvolution) _newV;
          e.setListener(H2dRubarVentMng.this);
          e.setUsed(false);
          if (e.getUsed() == 1) {
            updateUsedCourbe(e);
          }
        }
        return super.internalSet(_i, _newV);
      }
      return false;
    }

    @Override
    protected void fireObjectChanged(int _indexGeom, Object _newValue) {
      ventEvolChanged();
    }
  }

  final static CtuluNumberFormater VENT_TIME_FORMAT = FortranLib.getFormater(15, 4, false);
  CtuluArrayObject eltXIdx_;
  CtuluArrayObject eltYIdx_;
  Set listener_;
  int nbElt_;

  /**
   * @return le nombre d'element
   */
  public final int getNbElt() {
    return nbElt_;
  }

  /**
   * @param _app l
   */
  public H2dRubarVentMng(final H2dRubarVentInterface _app) {
    nbElt_ = _app.getNbElt();
    loadData(_app);
  }

  public void loadData(H2dRubarVentInterface _app) {
    if (_app == null) {
      return;
    }
    createModel();
    final TIntObjectHashMap intEvolX = new TIntObjectHashMap();
    final TIntObjectHashMap intEvolY = new TIntObjectHashMap();
    int idx = 1;
    for (int i = nbElt_ - 1; i >= 0; i--) {
      // l'evolution est deja enregistree: on la reprend
      int evolIdx = _app.getEvolIdx(i);
      if (intEvolX.contains(evolIdx)) {
        eltXIdx_.set(i, intEvolX.get(evolIdx));
        eltYIdx_.set(i, intEvolY.get(evolIdx));
      } else {
        EvolutionReguliere evolX = _app.getEvolAlongXForElt(i);
        EvolutionReguliere evolY = _app.getEvolAlongYForElt(i);
        // l'evolution est vide, on met une valeur nulle
        if (!isEvolEmpty(evolX) || !isEvolEmpty(evolY)) {
          // a continer
          final H2dRubarEvolution finalEvolX = H2dRubarEvolution.createH2dRubarEvolution(H2dVariableTransType.VENT_X, evolX, this);
          final H2dRubarEvolution finalEvolY = H2dRubarEvolution.createH2dRubarEvolution(H2dVariableTransType.VENT_Y, evolY, this);
          evolutionsUsed_.add(finalEvolX);
          evolutionsUsed_.add(finalEvolY);
          finalEvolX.setNom(H2dResource.getS("Vent X") + CtuluLibString.ESPACE + (idx));
          finalEvolY.setNom(H2dResource.getS("Vent Y") + CtuluLibString.ESPACE + (idx++));
          eltXIdx_.set(i, finalEvolX);
          eltYIdx_.set(i, finalEvolY);
          intEvolX.put(evolIdx, finalEvolX);
          intEvolY.put(evolIdx, finalEvolY);
        }
      }
    }
  }

  /**
   * @return l'interface a utiliser pour sauvegarder les chroniques d'apport
   */
  public H2dRubarVentInterface getSavedInterface() {
    return new SaveAdapter();
  }

  private class SaveAdapter implements H2dRubarVentInterface {
    int[] idx_;
    EvolutionReguliere[] evolsX_;
    EvolutionReguliere[] evolsY_;

    protected SaveAdapter() {
      final int nbElt = eltXIdx_.getSize();
      idx_ = new int[nbElt];
      int tmp = 0;
      final Map<H2dRubarEvolution, TObjectIntHashMap<H2dRubarEvolution>> evolXevolYIndice = new HashMap<H2dRubarEvolution, TObjectIntHashMap<H2dRubarEvolution>>();
      for (int i = 0; i < nbElt; i++) {
        H2dRubarEvolution ex = H2dRubarVentMng.this.getEvolutionAlongX(i);
        H2dRubarEvolution ey = H2dRubarVentMng.this.getEvolutionAlongY(i);
        // si null on met l'evolution vide
        if (ex == null) {
          ex = H2dRubarEvolution.EMPTY_EVOL;
        }
        if (ey == null) {
          ey = H2dRubarEvolution.EMPTY_EVOL;
        }
        TObjectIntHashMap<H2dRubarEvolution> evoly = evolXevolYIndice.get(ex);
        if (evoly == null) {
          evoly = new TObjectIntHashMap<H2dRubarEvolution>();
          evolXevolYIndice.put(ex, evoly);
        }
        if (evoly.contains(ey)) {
          idx_[i] = evoly.get(ey);
        } else {
          evoly.put(ey, tmp);
          idx_[i] = tmp++;
        }
      }
      evolsX_ = new EvolutionReguliere[tmp];
      evolsY_ = new EvolutionReguliere[tmp];
      for (Entry<H2dRubarEvolution, TObjectIntHashMap<H2dRubarEvolution>> entry : evolXevolYIndice.entrySet()) {
        final TObjectIntIterator<H2dRubarEvolution> it = entry.getValue().iterator();
        for (int i = entry.getValue().size(); i-- > 0; ) {
          it.advance();
          EvolutionReguliere ex = entry.getKey();
          EvolutionReguliere ey = it.key();
          TDoubleHashSet commonX = new TDoubleHashSet();
          if (!ex.isEvolutionWithSameX(ey, commonX)) {
            final double[] common = commonX.toArray();
            Arrays.sort(common);
            ex = ex.createEvolutionFromInterpolation(common);
            ey = ey.createEvolutionFromInterpolation(common);
          }
          evolsX_[it.value()] = ex;
          evolsY_[it.value()] = ey;
        }
      }
    }

    @Override
    public EvolutionReguliere getEvolAlongXForElt(int idxElt) {
      return evolsX_[idx_[idxElt]];
    }

    @Override
    public EvolutionReguliere getEvolAlongYForElt(int idxElt) {
      return evolsY_[idx_[idxElt]];
    }

    @Override
    public EvolutionReguliere getEvolutionAlongX(int idx) {
      return evolsX_[idx];
    }

    @Override
    public EvolutionReguliere getEvolutionAlongY(int idx) {
      return evolsY_[idx];
    }

    @Override
    public int getEvolIdx(final int _eltIdx) {
      return idx_[_eltIdx];
    }

    @Override
    public int getNbElt() {
      return idx_.length;
    }

    @Override
    public int getNbEvol() {
      return evolsX_.length;
    }
  }

  public void fillWithTransientCurves(final H2dEvolutionUseCounter _r) {
    if (eltYIdx_ != null) {
      for (int i = eltYIdx_.getSize() - 1; i >= 0; i--) {
        final H2dRubarEvolution ex = (H2dRubarEvolution) eltXIdx_.getObjectValueAt(i);
        if (ex != null) {
          _r.add(ex, H2dVariableTransType.VENT_X, i);
        }
        final H2dRubarEvolution ey = (H2dRubarEvolution) eltYIdx_.getObjectValueAt(i);
        if (ey != null) {
          _r.add(ey, H2dVariableTransType.VENT_Y, i);
        }
      }
    }
  }

  Set<H2dRubarEvolution> evolutionsUsed_;

  protected void updateUsedCourbe(final H2dRubarEvolution _e) {
    if (_e.isUsed() && !evolutionsUsed_.contains(_e)) {
      evolutionsUsed_.add(_e);
      appEvolIsUsedChanged(_e);
    } else if ((!_e.isUsed()) && evolutionsUsed_.contains(_e)) {
      evolutionsUsed_.remove(_e);
      appEvolIsUsedChanged(_e);
    }
  }

  public boolean isUsed(final EvolutionReguliereInterface _e) {
    return evolutionsUsed_ != null && evolutionsUsed_.contains(_e);
  }

  /**
   * @return tableau des courbes utilisees.
   */
  public EvolutionReguliereAbstract[] getUsedCourbes() {
    if (eltXIdx_ != null) {
      final EvolutionReguliereAbstract[] rf = new EvolutionReguliereAbstract[evolutionsUsed_.size()];
      evolutionsUsed_.toArray(rf);
      return rf;
    }
    return null;
  }

  /**
   * @param _evol l'evolution a tester
   * @return true si evolution vide
   */
  protected static boolean isEvolEmpty(final EvolutionReguliereInterface _evol) {
    if (_evol == null || _evol.getNbValues() == 0) {
      return true;
    }
    if (_evol.getNbValues() == 1) {
      return (_evol.getX(0) == 0) && (_evol.getY(0) == 0);
    }
    return false;
  }

  public H2dRubarVentMng(final int _nbElt) {
    nbElt_ = _nbElt;
  }

  protected final void createModel() {
    if (eltXIdx_ == null) {
      eltXIdx_ = new VentModelObject(nbElt_);
      eltYIdx_ = new VentModelObject(nbElt_);
      evolutionsUsed_ = new HashSet();
    }
  }

  /**
   * @param _l le listener a ajouter
   */
  public void addVentListener(final H2dRubarVentListener _l) {
    if (listener_ == null) {
      listener_ = new HashSet(3);
    }
    listener_.add(_l);
  }

  public void clear(final CtuluCommandContainer _cmd) {
    if (eltXIdx_ != null) {
      final int[] idx = new int[nbElt_];
      for (int i = nbElt_ - 1; i >= 0; i--) {
        idx[i] = i;
      }
      CtuluCommandComposite cmp = _cmd == null ? null : new CtuluCommandComposite();
      eltXIdx_.set(idx, null, _cmd);
      eltYIdx_.set(idx, null, _cmd);
      if (_cmd != null) {
        _cmd.addCmd(cmp);
      }
    }
  }

  /**
   * @param _l le listener a tester
   * @return true si est contenu
   */
  public boolean containsAppListener(final H2dRubarVentListener _l) {
    return (listener_ != null) && listener_.contains(_l);
  }

  @Override
  public void evolutionChanged(final EvolutionReguliereInterface _e) {
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2dRubarVentListener) it.next()).ventEvolutionContentChanged(this, _e);
      }
    }
  }

  @Override
  public void evolutionUsedChanged(final EvolutionReguliereInterface _e, final int _old, final int _new, final boolean _isAdjusting) {
  }

  protected void ventEvolChanged() {
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2dRubarVentListener) it.next()).ventChanged(this);
      }
    }
  }

  protected void appEvolIsUsedChanged(final EvolutionReguliereInterface _e) {
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2dRubarVentListener) it.next()).ventEvolutionUsedChanged(this, _e);
      }
    }
  }

  public H2dRubarEvolution getCommonEvolAlongX(final int[] _idx) {
    if (_idx == null || _idx.length == 0) {
      return null;
    }
    H2dRubarEvolution r = getEvolutionAlongX(_idx[0]);
    if (r == null) {
      r = H2dRubarEvolution.EMPTY_EVOL;
    }
    for (int i = _idx.length - 1; i > 0; i--) {
      H2dRubarEvolution test = getEvolutionAlongX(_idx[i]);
      if (test == null) {
        test = H2dRubarEvolution.EMPTY_EVOL;
      }
      if (test != r) {
        return null;
      }
    }
    return r;
  }

  public H2dRubarEvolution getCommonEvolAlongY(final int[] _idx) {
    if (_idx == null || _idx.length == 0) {
      return null;
    }
    H2dRubarEvolution r = getEvolutionAlongY(_idx[0]);
    if (r == null) {
      r = H2dRubarEvolution.EMPTY_EVOL;
    }
    for (int i = _idx.length - 1; i > 0; i--) {
      H2dRubarEvolution test = getEvolutionAlongY(_idx[i]);
      if (test == null) {
        test = H2dRubarEvolution.EMPTY_EVOL;
      }
      if (test != r) {
        return null;
      }
    }
    return r;
  }

  /**
   * @param _i l'indice de l'element
   * @return l'evolution correspondante
   */
  public H2dRubarEvolution getEvolutionAlongX(final int _i) {
    return eltXIdx_ == null ? null : (H2dRubarEvolution) eltXIdx_.getValueAt(_i);
  }

  public H2dRubarEvolution getEvolutionAlongY(final int _i) {
    return eltYIdx_ == null ? null : (H2dRubarEvolution) eltYIdx_.getValueAt(_i);
  }

  /**
   * @return true si des evolution sont d�finies
   */
  public boolean isSet() {
    return evolutionsUsed_ != null && evolutionsUsed_.size() > 0;
  }

  /**
   * @param _l le listener a enlever
   */
  public void removeVentListener(final H2dRubarVentListener _l) {
    if (listener_ != null) {
      listener_.remove(_l);
    }
  }

  /**
   * @param _idx les indices a modifier
   * @param _e l'�volution
   * @param _cmd le receveur de commande
   */
  public void setVent(final int[] _idx, final H2dRubarEvolution _ex, final H2dRubarEvolution _ey, final CtuluCommandContainer _cmd) {
    H2dRubarEvolution ex = _ex;
    H2dRubarEvolution ey = _ey;
    if (ex == H2dRubarEvolution.EMPTY_EVOL || isEvolEmpty(ex)) {
      ex = null;
    }
    if (ey == H2dRubarEvolution.EMPTY_EVOL || isEvolEmpty(ey)) {
      ey = null;
    }
    if (eltXIdx_ == null) {
      createModel();
    }
    CtuluCommandComposite cmp = _cmd == null ? null : new CtuluCommandComposite();
    eltXIdx_.set(_idx, ex, cmp);
    eltYIdx_.set(_idx, ey, cmp);
    if (_cmd != null) {
      _cmd.addCmd(cmp);
    }
  }

  /**
   * @param _idx les indices a modifier
   * @param _e l'�volution
   * @param _cmd le receveur de commande
   */
  public void setVentX(final int[] _idx, final H2dRubarEvolution _ex, final CtuluCommandContainer _cmd) {
    H2dRubarEvolution ex = _ex;
    if (ex == H2dRubarEvolution.EMPTY_EVOL || isEvolEmpty(ex)) {
      ex = null;
    }
    if (eltXIdx_ == null) {
      createModel();
    }
    eltXIdx_.set(_idx, ex, _cmd);
  }

  /**
   * @param _idx les indices a modifier
   * @param _e l'�volution
   * @param _cmd le receveur de commande
   */
  public void setVentY(final int[] _idx, final H2dRubarEvolution _ey, final CtuluCommandContainer _cmd) {
    H2dRubarEvolution ey = _ey;
    if (ey == H2dRubarEvolution.EMPTY_EVOL || isEvolEmpty(ey)) {
      ey = null;
    }
    if (eltXIdx_ == null) {
      createModel();
    }
    eltYIdx_.set(_idx, ey, _cmd);
  }
}
