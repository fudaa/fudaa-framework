/*
 *  @creation     28 sept. 2004
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.h2d.H2dParallelogrammeManagerInterface;
import org.fudaa.dodico.h2d.H2dRegularGridDataInterface;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarVF2MResultInterface.java,v 1.3 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dRubarVF2MResultInterface extends H2dParallelogrammeManagerInterface {

  /**
   * @return true si utilisation de parallelogrammes.
   */
  boolean isParall();

  /**
   * @return si null n'est pas prise en compte.
   */
  String getFirstLine();

  /**
   * A utiliser si isParall=false.
   *
   * @return les donnees de la grille reguliere. Null si parall.
   */
  H2dRegularGridDataInterface getGridData();

}
