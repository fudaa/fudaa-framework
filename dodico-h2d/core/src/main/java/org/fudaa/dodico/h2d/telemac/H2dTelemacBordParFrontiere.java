/*
 * @creation 24 mai 2004
 * @modification $Date: 2006-09-19 14:43:24 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntIntHashMap;
import gnu.trove.TObjectDoubleHashMap;
import gnu.trove.TObjectDoubleIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandCompositeInverse;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.dodico.h2d.H2dBcFrontierBlockAbstract;
import org.fudaa.dodico.h2d.H2dBoundary;
import org.fudaa.dodico.h2d.H2dBoundaryCondition;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dBoundaryTypeCommon;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * Les frontieres telemac.
 *
 * @author Fred Deniger
 * @version $Id: H2dTelemacBordParFrontiere.java,v 1.3 2006-09-19 14:43:24 deniger Exp $
 */
public class H2dTelemacBordParFrontiere extends H2dBcFrontierBlockAbstract {

  final H2dTelemacBcManager manager_;

  protected H2dTelemacBoundaryCondition[] cl_;

  public double[] getAbsCurviligne(final H2dBoundary _b) {
    return super.getAbsCurviligne(manager_.getGrid(), _b);
  }

  protected H2dTelemacBordParFrontiere(final H2dTelemacBcManager _manager, final List _bords,
      final H2dTelemacBoundaryCondition[] _clIntBord, final int _maillageindex) {
    super(_bords, _maillageindex);
    manager_ = _manager;
    cl_ = _clIntBord;
  }

  public int getTotalLiquidBorderNb() {
    return manager_.liquidBorder_ == null ? 0 : manager_.liquidBorder_.length;
  }

  /**
   * @param _deb l'indice de deb cherche
   * @param _fin l'indice de fin cherche
   * @return le bord ayant comme extremite [_deb,_fin]. null si aucun.
   */
  public H2dTelemacBoundary getTelemacLiquidBord(final int _deb, final int _fin) {
    final int n = getTotalLiquidBorderNb();
    for (int i = n - 1; i >= 0; i--) {
      final H2dTelemacBoundary b = manager_.liquidBorder_[i];
      if ((b.getIdxFrontiere() == maillageBordIdx_) && (b.getIdxFin() == _fin) && (b.getIdxDeb() == _deb)) {
        return b;
      }
    }
    return null;
  }

  @Override
  public H2dBoundary createMementoFor(final H2dBoundary _b) {
    return ((H2dTelemacBoundary) _b).createMemento();
  }

  @Override
  public H2dBoundary createNewBordFrom(final H2dBoundary _b) {
    final H2dTelemacBoundary r = new H2dTelemacBoundary(getFrIdx(), manager_);
    r.updateFrom((H2dTelemacBoundary) _b, manager_);
    return r;
  }

  /**
   * @param _b le bord a tester
   * @param _t la variable a tester
   * @return true si cette variable est fixee (definie par mot-cle)
   */
  public boolean isVariableFixedOnBorder(final H2dTelemacBoundary _b, final H2dVariableType _t) {
    return manager_.isVariableFixedOnBorder(_b, _t);
  }

  protected void setBord(final List _l) {
    for (int i = bords_.size() - 1; i >= 0; i--) {
      ((H2dTelemacBoundary) bords_.get(i)).setEvolutionUsedState(false, true);
    }
    bords_ = new ArrayList(_l);
    for (int i = bords_.size() - 1; i >= 0; i--) {
      ((H2dTelemacBoundary) bords_.get(i)).setEvolutionUsedState(true, false);
    }
  }

  /**
   * Transforme les points appartenant a l'intervalle en point solide. le tableau des bords liquides est mis a jour et
   * les evt sont envoye.
   *
   * @param _idxDeb l'indice de deb a enlever
   * @param _idxFin l'indice de fin a enlever
   * @return commande si modif; null sinon
   */
  public CtuluCommand removeLiquidBorderAndCmd(final int _idxDeb, final int _idxFin) {
    final List l = saveMemento();
    if (removeLiquidBoundaryIntern(_idxDeb, _idxFin)) {
      final CtuluCommandCompositeInverse r = new CtuluCommandCompositeInverse();
      // on ne peut pas savoir si un bord a ete suprimme
      manager_.fireEvolutionUsedChanged();
      r.addCmd(new CtuluCommand() {

        @Override
        public void redo() {
          removeLiquidBoundaryIntern(_idxDeb, _idxFin);
          H2dTelemacBordParFrontiere.this.manager_.fireEvolutionUsedChanged();
        }

        @Override
        public void undo() {
          setBord(l);
          managerComputeLiquidBoundaries();
          H2dTelemacBordParFrontiere.this.manager_.fireEvolutionUsedChanged();
          fireFrontierStructureChange();
        }
      });
      r.addCmd(manager_.updateAllParameterWhenStructureChanged());
      return r.getSimplify();
    }
    return null;
  }

  /**
   * Le nouveau bord liquide ne doit pas toucher un autre bord liquide. Ajoute le nuoveau bord liquide. Met a jour les
   * tableaux internes et envoie les evts.
   *
   * @param _idxDeb l'indice de deb du bord liquide a ajouter
   * @param _idxFin l'indice de fin du bord liquide a ajouter
   * @param _type le type du nouveau bord liquide
   * @return commande si modif; null sinon.
   */
  public CtuluCommand addLiquidPts(final int _idxDeb, final int _idxFin, final H2dBoundaryType _type) {
    final List l = saveMemento();
    if (insertLiquid(_idxDeb, _idxFin, _type)) {
      final CtuluCommandCompositeInverse r = new CtuluCommandCompositeInverse();
      r.addCmd(new CtuluCommand() {

        @Override
        public void redo() {
          insertLiquid(_idxDeb, _idxFin, _type);
        }

        @Override
        public void undo() {
          setBord(l);
          managerComputeLiquidBoundaries();
          fireFrontierStructureChange();
        }
      });
      r.addCmd(manager_.updateAllParameterWhenStructureChanged());
      return r.getSimplify();
    }
    return null;
  }

  public void setVitesseForSelectedPoints(final int[] _idx, final int[] _newValue, final CtuluCommandContainer _cmd) {
    if (_idx == null || _idx.length == 0 || _newValue == null || _newValue.length != _idx.length) {
      return;
    }

    final TIntIntHashMap idxOldValue = _cmd == null ? null : new TIntIntHashMap(_idx.length);
    final TIntIntHashMap idxNewValue = _cmd == null ? null : new TIntIntHashMap(_idx.length);
    boolean done = false;
    for (int i = _idx.length - 1; i >= 0; i--) {
      final int k = _idx[i];
      final int old = cl_[k].getSolidUVState();
      if (cl_[k].setUvType(_newValue[i])) {
        done = true;
        if (idxOldValue != null) {
          idxOldValue.put(k, old);
          idxOldValue.put(k, _newValue[i]);
        }
      }
    }
    if (done) {
      if (_cmd != null&&idxOldValue!=null) {
        final int[] modifiedIdx = idxOldValue.keys();
        final int[] newValue = new int[modifiedIdx.length];
        final int[] oldValue = new int[modifiedIdx.length];
        for (int i = modifiedIdx.length - 1; i >= 0; i--) {
          newValue[i] = idxNewValue.get(modifiedIdx[i]);
          oldValue[i] = idxOldValue.get(modifiedIdx[i]);
        }
        _cmd.addCmd(new CtuluCommand() {

          @Override
          public void redo() {
            setVitesseForSelectedPoints(modifiedIdx, newValue, null);
          }

          @Override
          public void undo() {
            setVitesseForSelectedPoints(modifiedIdx, oldValue, null);

          }
        });

      }
      manager_.fireBcPointsParametersChanged(null);

    }
  }

  public void setVitesseForSelectedPoints(final int[] _idx, final int _newValue, final CtuluCommandContainer _cmd) {
    if (_idx == null || _idx.length == 0 || H2dTelemacBoundaryCondition.isSolidIdxMixte(_newValue)) {
      return;
    }
    final TIntIntHashMap idxOldValue = _cmd == null ? null : new TIntIntHashMap(_idx.length);
    boolean modified = false;
    for (int i = _idx.length - 1; i >= 0; i--) {
      final int k = _idx[i];
      final int old = cl_[k].getSolidUVState();
      if (cl_[k].setUvType(_newValue)) {
        modified = true;
        if (idxOldValue != null) {
          idxOldValue.put(k, old);
        }
      }
    }
    if (modified) {
      manager_.fireBcPointsParametersChanged(null);
      if (idxOldValue != null&&_cmd!=null) {
        final int[] modifiedIdx = idxOldValue.keys();
        final int[] oldValues = idxOldValue.getValues();
        _cmd.addCmd(new CtuluCommand() {

          @Override
          public void redo() {
            setVitesseForSelectedPoints(modifiedIdx, _newValue, null);
          }

          @Override
          public void undo() {
            setVitesseForSelectedPoints(modifiedIdx, oldValues, null);

          }

        });
      }
    }
  }

  /**
   * Change the values for some boundary points: index in the array _idxOnFrontier.
   *
   * @param _variableTypeValue [H2dVariableType,value] the new values
   * @param _idxOnFrontier the index concerned by the changes.
   * @return a dodicoCommand if there a lesat on change.
   */
  public CtuluCommand setValuesForBoundaryPoints(final int[] _idxOnFrontier, final TObjectDoubleHashMap _variableTypeValue) { // verify
    // the
    // parameters
    if ((_idxOnFrontier == null) || (_idxOnFrontier.length == 0) || (_variableTypeValue == null)
        || (_variableTypeValue.size() == 0)) {
      return null;
    }
    final CtuluCommandCompositeInverse c = new CtuluCommandCompositeInverse();
    final int nbVar = _variableTypeValue.size();
    final TObjectDoubleIterator it = _variableTypeValue.iterator();
    for (int i = nbVar; i-- > 0;) {
      it.advance();
      final H2dVariableType var = (H2dVariableType) it.key();
      c.addCmd(setValuesForBoundaryPoints(_idxOnFrontier, var, it.value()));
    }
    return c.getSimplify();
  }

  public CtuluCommand setValuesForBoundaryPoints(final int[] _idxOnFrontier, final H2dVariableType _t, final double[] _vs) { // verify
    // the
    // parameters
    if ((_idxOnFrontier == null) || (_idxOnFrontier.length == 0) || (_t == null) || (_vs == null)
        || (_vs.length != _idxOnFrontier.length)) {
      return null;
    }
    final int n = _idxOnFrontier.length;
    final int nbCl = cl_.length;
    final TIntArrayList modifyIdx = new TIntArrayList(_idxOnFrontier.length);
    final TDoubleArrayList oldV = new TDoubleArrayList(_idxOnFrontier.length);
    final TDoubleArrayList newV = new TDoubleArrayList(_idxOnFrontier.length);
    double nv;
    for (int j = n - 1; j >= 0; j--) {
      nv = _vs[j];
      final int k = _idxOnFrontier[j];
      if ((k >= 0) && (k < nbCl)) {
        final double old = cl_[k].getValue(_t);
        if (cl_[k].setValue(_t, nv)) {
          modifyIdx.add(k);
          oldV.add(old);
          newV.add(nv);
        }
      }
    }
    // If there is a modification a DodicoCommand is built.
    if (modifyIdx.size() > 0) {
      manager_.fireParametersForBoundaryPtsChange(_t);
      final int[] idxs = modifyIdx.toNativeArray();
      final double[] oldValues = oldV.toNativeArray();
      final double[] newValues = newV.toNativeArray();
      return new CtuluCommand() {

        @Override
        public void redo() {
          for (int i = idxs.length - 1; i >= 0; i--) {
            cl_[idxs[i]].setValue(_t, newValues[i]);
          }
          mngFireBoundaryPtChanged(_t);
        }

        @Override
        public void undo() {
          for (int i = idxs.length - 1; i >= 0; i--) {
            cl_[idxs[i]].setValue(_t, oldValues[i]);
          }
          mngFireBoundaryPtChanged(_t);
        }
      };
    }
    return null;
  }

  /**
   * Change the values for some boundary points: index in the array _idxOnFrontier.
   *
   * @param _idxOnFrontier the index concerned by the changes.
   * @return a dodicoCommand if there a lesat on change.
   */
  public CtuluCommand setValuesForBoundaryPoints(final int[] _idxOnFrontier, final H2dVariableType _t, final double _v) {

    if ((_idxOnFrontier == null) || (_idxOnFrontier.length == 0) || (_t == null)) {
      return null;
    }
    final double[] vs = new double[_idxOnFrontier.length];
    Arrays.fill(vs, _v);
    return setValuesForBoundaryPoints(_idxOnFrontier, _t, vs);
  }

  /**
   * Increase a liquid boundary from the index _min to the index _max. Only one liquid boundary must be selected by the
   * segment [_min,_max]
   *
   * @param _min la nouvelle borne min du bord liquide
   * @param _max la nouvelle borne max du bord liquide
   * @return null if no action or if the segment _min,_max is invalid.
   */
  public CtuluCommand increaseLiquidBorder(final int _min, final int _max) {
    final List old = saveMemento();
    if (increaseLiquidBorderIntern(_min, _max)) {
      setFirstBord();
      manager_.computeLiquidBoundaries();
      manager_.updateAllParameterWhenStructureChanged();
      manager_.fireFrontierStructureChange(this);
      return new CtuluCommand() {

        @Override
        public void redo() {
          increaseLiquidBorderIntern(_min, _max);
          managerComputeLiquidBoundaries();
          H2dTelemacBordParFrontiere.this.manager_.updateAllParameterWhenStructureChanged();
          fireFrontierStructureChange();
        }

        @Override
        public void undo() {
          setBord(old);
          H2dTelemacBordParFrontiere.this.manager_.computeLiquidBoundaries();
          H2dTelemacBordParFrontiere.this.manager_.updateAllParameterWhenStructureChanged();
          fireFrontierStructureChange();
        }
      };
    }
    return null;
  }

  @Override
  public String toString() {
    final int n = bords_.size();
    final StringBuffer r = new StringBuffer();
    for (int i = 0; i < n; i++) {
      r.append(bords_.get(i).toString());
      if (i < n - 1) {
        r.append(" , ");
      }
    }
    return r.toString();
  }

  protected H2dTelemacBordParFrontiere(final H2dTelemacBcManager _manager, final H2dTelemacBoundary _m,
      final H2dTelemacBoundaryCondition[] _clIntBord, final int _maillageindex) {
    super(new ArrayList(10), _maillageindex);
    manager_ = _manager;
    bords_.add(_m);
    cl_ = _clIntBord;
    maillageBordIdx_ = _maillageindex;
  }

  @Override
  public int getNbPt() {
    return cl_.length;
  }

  public int[] getIdxIn(final H2dTelemacBoundary _b) {
    return _b.getIdx(getNbPt());
  }

  /**
   * @param _v le type de variable a tester
   * @return true if all the values are equals on the boundary points for the variable _v.
   */
  public boolean isValueConstantOnPoint(final H2dVariableType _v) {
    final int n = cl_.length;
    // get the first value to test
    final double val = cl_[n - 1].getValue(_v);
    // return false if one value is different
    for (int i = n - 2; i >= 0; i--) {
      if (cl_[i].getValue(_v) != val) {
        return false;
      }
    }
    return true;
  }

  /**
   * @param _b le bord a parcourir
   * @param _v la variable a tester
   * @return si tous les points du bord _b ont la meme valeur pour _v
   */
  public boolean isValueConstantOnPoint(final H2dBoundary _b, final H2dVariableType _v) {
    final H2dBoundary.BordIndexIterator it = new H2dBoundary.BordIndexIterator(getNbPt(), _b);
    final double val = cl_[it.next()].getValue(_v);
    while (it.hasNext()) {
      if (val != cl_[it.next()].getValue(_v)) {
        return false;
      }
    }
    return true;
  }

  /**
   * @param _v la variable a tester
   * @param _selectedIdxOnThisFrontier les indices des points (dans la numerotation de cette frontiere)
   * @return true if all the values are equals on the selected boundary points for the variable _v.
   */
  public boolean isValueConstantOnPoint(final H2dVariableType _v, final int[] _selectedIdxOnThisFrontier) {
    if ((_selectedIdxOnThisFrontier == null) || (_selectedIdxOnThisFrontier.length == 0)) {
      return true;
    }
    final int n = _selectedIdxOnThisFrontier.length;
    // get the first value to test
    final double val = cl_[_selectedIdxOnThisFrontier[n - 1]].getValue(_v);
    // return false if one value is different
    for (int i = n - 2; i >= 0; i--) {
      if (cl_[_selectedIdxOnThisFrontier[i]].getValue(_v) != val) {
        return false;
      }
    }
    return true;
  }

  public int isSolidTypeConstantOnPoint(final int[] _selectedIdxOnThisFrontier) {
    if ((_selectedIdxOnThisFrontier == null) || (_selectedIdxOnThisFrontier.length == 0)) {
      return H2dTelemacBoundaryCondition
          .getSolidIdxMixte();
    }
    final int n = _selectedIdxOnThisFrontier.length;
    // get the first value to test
    final int val = cl_[_selectedIdxOnThisFrontier[n - 1]].getSolidUVState();
    // return false if one value is different
    for (int i = n - 2; i >= 0; i--) {
      if (cl_[_selectedIdxOnThisFrontier[i]].getSolidUVState() != val) {
        return H2dTelemacBoundaryCondition
            .getSolidIdxMixte();
      }
    }
    return val;
  }

  /**
   * @return memento de tous les bords de cette frontiere
   */
  public List saveMemento() {
    final List r = new ArrayList(bords_.size());
    final int n = bords_.size();
    for (int i = 0; i < n; i++) {
      r.add(((H2dTelemacBoundary) bords_.get(i)).createMemento());
    }
    return r;
  }

  /**
   * @param _b le bord liquid a enlever
   * @return la commande ou null si pas de changement.
   */
  public CtuluCommand removeLiquidBorder(final H2dTelemacBoundary _b) {
    final int idxInFrontier = bords_.indexOf(_b);
    if (idxInFrontier >= 0) {
      final List old = saveMemento();
      final boolean isEvolPrese = _b.isOneCurveUsed();
      removeBordLiquid(idxInFrontier);
      if (isEvolPrese) {
        manager_.fireEvolutionUsedChanged();
      }
      final CtuluCommandCompositeInverse r = new CtuluCommandCompositeInverse();
      r.addCmd(new CtuluCommand() {

        @Override
        public void redo() {
          removeBordLiquid(idxInFrontier);
          if (isEvolPrese) {
            H2dTelemacBordParFrontiere.this.manager_.fireEvolutionUsedChanged();
          }
        }

        @Override
        public void undo() {
          setBord(old);
          managerComputeLiquidBoundaries();
          if (isEvolPrese) {
            H2dTelemacBordParFrontiere.this.manager_.fireEvolutionUsedChanged();
          }
          fireFrontierStructureChange();
        }
      });
      r.addCmd(manager_.updateAllParameterWhenStructureChanged());
      return r.getSimplify();
    }
    return null;
  }

  void fireFrontierStructureChange() {
    manager_.fireFrontierStructureChange(H2dTelemacBordParFrontiere.this);
  }

  @Override
  protected boolean removeBordLiquid(final int _idx) {
    final H2dTelemacBoundary b = (H2dTelemacBoundary) bords_.get(_idx);
    final boolean r = super.removeBordLiquid(_idx);
    if (r) {
      b.setEvolutionUsedState(false, false);
      managerComputeLiquidBoundaries();
      manager_.fireFrontierStructureChange(this);
    }
    return r;
  }

  @Override
  public H2dBoundary createDefaultBoundary() {
    final H2dTelemacBoundary b = new H2dTelemacBoundary(getFrIdx(), manager_);
    b.setBoundaryType(H2dBoundaryTypeCommon.SOLIDE, null);
    return b;
  }

  /**
   * @param _ptIdxOnThisFrontier les indices des points selectionnees.
   * @return l'unique bord contenant tous les indices du tableau. null si aucun ou plus d'1.
   */
  public H2dTelemacBoundary getUniqueSelectedTelemacBord(final int[] _ptIdxOnThisFrontier) {
    return (H2dTelemacBoundary) getUniqueSelectedBord(_ptIdxOnThisFrontier);
  }

  /**
   * @param _ptIdxOnThisFrontier l'indice du point sur cette frontiere
   * @return la condition pour l'indice demande.
   */
  public H2dTelemacBoundaryCondition getTelemacCl(final int _ptIdxOnThisFrontier) {
    return cl_[_ptIdxOnThisFrontier];
  }

  @Override
  public H2dBoundaryCondition getCl(final int _idxOnThisFrontier) {
    return cl_[_idxOnThisFrontier];
  }

  /**
   * @param _boundaryIdx l'indice du bord demande.
   * @return le type du traceur pour le bord
   */
  public H2dBcType getTracerType(final int _boundaryIdx) {
    return ((H2dTelemacBoundary) bords_.get(_boundaryIdx)).getTracerType();
  }

  @Override
  public H2dBoundary createBord(final H2dBoundaryType _t) {
    return manager_.createBord(getFrIdx(), _t);
  }

  @Override
  protected boolean insertLiquid(final int _min, final int _max, final H2dBoundaryType _b) {
    final boolean r = super.insertLiquid(_min, _max, _b);
    if (r) {
      managerComputeLiquidBoundaries();
      manager_.fireFrontierStructureChange(this);
    }
    return r;
  }

  @Override
  protected boolean increaseLiquidBorderIntern(final int _min, final int _max) {
    return super.increaseLiquidBorderIntern(_min, _max);
  }

  @Override
  public boolean removeLiquidBoundaryIntern(final int _min, final int _max) {
    final boolean r = super.removeLiquidBoundaryIntern(_min, _max);
    if (r) {
      managerComputeLiquidBoundaries();
      manager_.fireFrontierStructureChange(this);
    }
    return r;
  }

  void managerComputeLiquidBoundaries() {
    manager_.computeLiquidBoundaries();
  }

  void mngFireBoundaryPtChanged(final H2dVariableType _t) {
    H2dTelemacBordParFrontiere.this.manager_.fireParametersForBoundaryPtsChange(_t);
  }
}