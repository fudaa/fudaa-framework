/*
 * @creation 7 oct. 2003
 * @modification $Date: 2006-04-19 13:19:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.dodico.h2d.telemac;

/**
 * @author deniger
 * @version $Id: H2dTelemacCLSourceInterface.java,v 1.10 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dTelemacCLSourceInterface {

  /**
   * @return le nombre de ligne
   */
  int getNbLines();

  /**
   * Doit renvoyer les conditions limites dans l'ordre de numérotation de la frontiere.
   *
   * @return true si operation reussie
   * @param _i l'indice
   * @param _ele l'ele a modifier
   */
  boolean getLine(int _i, H2dTelemacCLElementSource _ele);
}