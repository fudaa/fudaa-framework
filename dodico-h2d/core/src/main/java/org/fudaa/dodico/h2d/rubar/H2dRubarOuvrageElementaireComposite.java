package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.collection.CtuluListDouble;
import org.fudaa.ctulu.collection.CtuluListObject;
import org.fudaa.dodico.h2d.H2dEvolutionVariableMap;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageCompositeTypeControle;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.util.Arrays;
import java.util.Set;

public class H2dRubarOuvrageElementaireComposite extends H2dRubarOuvrageElementaireAbstract implements H2dRubarOuvrageElementaireCompositeI {
  CtuluListObject ouvragesElementaires;
  H2dRubarOuvrageCompositeTypeControle typeControle = H2dRubarOuvrageCompositeTypeControle.TIME;
  CtuluListDouble values_;
  private double xAmont;
  private double xAval;
  private double yAmont;
  private double yAval;
  private int avalMesh = -1;
  private int amontMesh = -1;
  private H2dRubarGridAreteSource grid;

  public H2dRubarGridAreteSource getGrid() {
    return grid;
  }

  public H2dRubarOuvrageElementaireComposite() {
    super();
    values_ = new CtuluListDouble(100);
    ouvragesElementaires = new CtuluListObject();
  }

  @Override
  public void setProjetType(H2dRubarProjetType projet, int nbTransportBlock, CtuluCommandComposite cmd) {
    int intern = getNbOuvrages();
    for (int i = 0; i < intern; i++) {
      ((H2dRubarOuvrageElementaireInterface) ouvragesElementaires.getObjectValueAt(i)).setProjetType(projet, nbTransportBlock, cmd);
    }
  }

  @Override
  public void setOuvrageParent(H2dRubarOuvrage ouvrageParent) {
    super.setOuvrageParent(ouvrageParent);
    int intern = getNbOuvrages();
    for (int i = 0; i < intern; i++) {
      ((H2dRubarOuvrageElementaireAbstract) ouvragesElementaires.getObjectValueAt(i)).setOuvrageParent(ouvrageParent);
    }
  }

  @Override
  public void evolutionChanged(EvolutionReguliereInterface _e) {
    if (ouvragesElementaires != null) {
      int nb = ouvragesElementaires.getSize();
      for (int i = 0; i < nb; i++) {
        ((H2dRubarOuvrageElementaireInterface) ouvragesElementaires.getValueAt(i)).evolutionChanged(_e);
      }
    }
  }

  @Override
  public void fillWithTransientCurves(String pref, H2dEvolutionVariableMap r) {
    if (ouvragesElementaires != null) {
      int nb = ouvragesElementaires.getSize();
      for (int i = 0; i < nb; i++) {
        ((H2dRubarOuvrageElementaireInterface) ouvragesElementaires.getValueAt(i)).fillWithTransientCurves(pref + " / " + H2dResource.getS(
            "Composite {0}", Integer.toString(i + 1)), r);
      }
    }
  }

  @Override
  public void fillWithTarageEvolution(String prefix, Set allTarage) {
    if (ouvragesElementaires != null) {
      int nb = ouvragesElementaires.getSize();
      for (int i = 0; i < nb; i++) {
        ((H2dRubarOuvrageElementaireInterface) ouvragesElementaires.getValueAt(i)).fillWithTarageEvolution(prefix + " / " + H2dResource.getS(
            "Composite {0}", Integer.toString(i + 1)), allTarage);
      }
    }
  }

  public H2dRubarOuvrageElementaireComposite(H2dRubarGridAreteSource grid) {
    this();
    this.grid = grid;
  }

  public H2dRubarOuvrageElementaireComposite(H2dRubarGridAreteSource grid, H2dRubarOuvrage ouvrageParent_, double[] values,
                                             H2dRubarOuvrageElementaireInterface[] ouvrages) {
    super();
    this.grid = grid;
    this.ouvrageParent_ = ouvrageParent_;
    this.values_ = new CtuluListDouble(values);
    ouvragesElementaires = new CtuluListObject(Arrays.asList(ouvrages));
  }

  public void setOuvrages(H2dRubarOuvrageElementaireInterface[] ouvrages) {
    ouvragesElementaires.removeAll();
    ouvragesElementaires.addAll(ouvrages, null, null);
    int size = ouvragesElementaires.getSize() - 1;
    if (size != values_.getSize()) {
      if (size <= 0) {
        values_.removeAll();
      }
      if (size > values_.getSize()) {
        for (int i = values_.getSize(); i < size; i++) {
          values_.add(0);
        }
      } else {
        for (int i = size; i < values_.getSize(); i++) {
          values_.remove(values_.getSize() - 1);
        }
      }
    }
  }

  @Override
  public H2dRubarOuvrageElementaireInterface getCopyWithoutEvt() {
    H2dRubarOuvrageElementaireInterface[] ouvrages = getOuvrages();
    H2dRubarOuvrageElementaireInterface[] copiedOuvrages = new H2dRubarOuvrageElementaireInterface[ouvrages.length];
    for (int i = 0; i < ouvrages.length; i++) {
      copiedOuvrages[i] = ouvrages[i].getCopyWithoutEvt();
    }
    H2dRubarOuvrageElementaireComposite res = new H2dRubarOuvrageElementaireComposite(grid, ouvrageParent_, getValeurs(),
        copiedOuvrages);
    res.setXAmont(xAmont);
    res.setYAmont(yAmont);
    res.setXAval(xAval);
    res.setYAval(yAval);
    res.setAmontMesh(amontMesh);
    res.setAvalMesh(avalMesh);
    res.setTypeControle(typeControle);
    res.grid = grid;
    return res;
  }

  @Override
  public H2dRubarOuvrageElementaireInterface[] getOuvrages() {
    return (H2dRubarOuvrageElementaireInterface[]) ouvragesElementaires.toArray(
        new H2dRubarOuvrageElementaireInterface[ouvragesElementaires.getSize()]);
  }

  public int getNbOuvrages() {
    return ouvragesElementaires.getSize();
  }

  @Override
  public H2dRubarOuvrageType getType() {
    return H2dRubarOuvrageType.COMPOSITE;
  }

  @Override
  public H2dRubarOuvrageCompositeTypeControle getTypeControle() {
    return typeControle;
  }

  @Override
  public double[] getValeurs() {
    return values_.getValues();
  }

  @Override
  public double getXAmont() {
    if (amontMesh < 0) {
      return xAmont;
    }
    return grid.getGrid().getMoyCentreXElement(amontMesh);
  }

  public void setAmontMesh(int idx) {
    this.amontMesh = idx;
  }

  public int getAmontMesh() {
    return amontMesh;
  }

  public void setAvalMesh(int avalMesh) {
    this.avalMesh = avalMesh;
  }

  public int getAvalMesh() {
    return avalMesh;
  }

  @Override
  public double getXAval() {
    if (avalMesh < 0) {
      return xAval;
    }
    return grid.getGrid().getMoyCentreXElement(avalMesh);
  }

  @Override
  public double getYAmont() {
    if (amontMesh < 0) {
      return yAmont;
    }
    return grid.getGrid().getMoyCentreYElement(amontMesh);
  }

  @Override
  public double getYAval() {
    if (avalMesh < 0) {
      return yAval;
    }
    return grid.getGrid().getMoyCentreYElement(avalMesh);
  }

  public void setTypeControle(H2dRubarOuvrageCompositeTypeControle typeControle) {
    this.typeControle = typeControle;
  }

  public void setXAmont(double xAmont) {
    this.xAmont = xAmont;
  }

  public void setXAval(double xAval) {
    this.xAval = xAval;
  }

  public void setYAmont(double yAmont) {
    this.yAmont = yAmont;
  }

  public void setYAval(double yAval) {
    this.yAval = yAval;
  }

  public void setValues(Double[] values) {
    if (values == null || values.length != this.values_.getSize()) {
      return;
    }
    values_.removeAll();
    values_.addAllObject(values, null);
  }
}
