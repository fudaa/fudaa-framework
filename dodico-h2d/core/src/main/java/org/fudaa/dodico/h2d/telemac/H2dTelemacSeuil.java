/*
 * @creation 3 d�c. 2004
 * @modification $Date: 2007-04-16 16:34:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.locationtech.jts.geom.Envelope;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.collection.CtuluListDouble;
import org.fudaa.ctulu.collection.CtuluListInteger;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author Fred Deniger
 * @version $Id: H2dTelemacSeuil.java,v 1.12 2007-04-16 16:34:19 deniger Exp $
 */
public class H2dTelemacSeuil implements H2dTelemacSeuilInterface {

  CtuluListDouble cotes_;
  CtuluListDouble debit_;
  CtuluListInteger index1_;
  CtuluListInteger index2_;
  boolean isIdxValide_;

  /**
   * @return true si les indices sont valides :ils se suivent dans la num�roation de bord
   */
  public final boolean isIdxValide() {
    return isIdxValide_;
  }

  /**
   * @param _cote1 true si cote 1 du seuil
   * @param _idx l'indice [0,getNbPoint -2[
   * @param _g le maillage
   * @return la distance entre le point _idx et _idx+1
   */
  public double getDistanceXY(final boolean _cote1, final int _idx, final EfGridInterface _g) {
    if (_idx >= 0 && _idx < getNbPoint() - 1) {
      final EfFrontierInterface fr = _g.getFrontiers();
      final int idxPt1 = fr.getIdxGlobalFrom(_cote1 ? index1_.getValue(_idx) : index2_.getValue(_idx));
      final int idxPt2 = fr.getIdxGlobalFrom(_cote1 ? index1_.getValue(_idx + 1) : index2_.getValue(_idx + 1));
      return CtuluLibGeometrie.getDistance(_g.getPtX(idxPt1), _g.getPtY(idxPt1), _g.getPtX(idxPt2), _g.getPtY(idxPt2));
    }
    return -1;
  }

  protected final void setIdxValid(final boolean _isIdxFalse) {
    isIdxValide_ = _isIdxFalse;
  }

  protected H2dTelemacSeuil(final H2dTelemacSeuilMng _mng, final H2dTelemacSeuilInterface _i) {
    final int nbPt = _i.getNbPoint();
    index1_ = new CtuluListInteger(nbPt, nbPt + 5);
    index2_ = new CtuluListInteger(nbPt, nbPt + 5);
    final double[] coteValues = new double[nbPt];
    final double[] debitValues = new double[nbPt];
    for (int i = 0; i < nbPt; i++) {
      index1_.set(i, _i.getPtIdx1(i));
      index2_.set(i, _i.getPtIdx2(i));
      coteValues[i] = _i.getCote(i);
      final double d = _i.getCoefDebit(i);
      if (d > 0) {
        debitValues[i] = d;
      }
    }
    cotes_ = _mng.createDoubleModel(coteValues);
    debit_ = _mng.createDoubleModel(debitValues);
  }

  protected H2dTelemacSeuil(final H2dTelemacSeuilMng _mng, final int[] _c1, final int[] _c2, final double _cotes,
      final double _debit) {
    index1_ = new CtuluListInteger(_c1);
    index2_ = new CtuluListInteger(_c2);
    cotes_ = _mng.createDoubleModel(_c1.length);
    cotes_.setAll(_cotes);
    debit_ = _mng.createDoubleModel(_c1.length);
    if (_debit > 0) {
      debit_.setAll(_debit);
    }
  }

  /**
   * @param _v la valeur commune de la cote
   * @return true si changement
   */
  protected boolean setValueForCote(final double _v, final CtuluCommandContainer _cmd) {
    if (cotes_.isAllSameValue() && cotes_.getValue(0) == _v) {
      return false;
    }
    return cotes_.setAll(_v, _cmd);
  }

  /**
   * @param _v la valeur commune du coef de debit
   * @return true si changement
   */
  protected boolean setValueForDebit(final double _v, final CtuluCommandContainer _cmd) {
    if (debit_.isAllSameValue() && debit_.getValue(0) == _v) {
      return false;
    }
    return debit_.setAll(_v, _cmd);
  }

  @Override
  public double getCoefDebit(final int _idx) {
    return debit_.getValue(_idx);
  }

  /**
   * @return la valeur commune si existe. null sinon
   */
  public Double getCommonCoefficientCote() {
    if (cotes_.isAllSameValue()) {
      return CtuluLib.getDouble(cotes_.getValue(0));
    }
    return null;
  }

  /**
   * @return la valeur commune si existe. null sinon
   */
  public Double getCommonCoefficientDebit() {
    if (debit_.isAllSameValue()) {
      return CtuluLib.getDouble(debit_.getValue(0));
    }
    return null;
  }

  @Override
  public double getCote(final int _idx) {
    return cotes_.getValue(_idx);
  }

  /**
   * @return la cote max
   */
  public double getCoteMax() {
    return cotes_.getMax();
  }

  /**
   * @return la cote min
   */
  public double getCoteMin() {
    return cotes_.getMin();
  }

  /**
   * @return le debit max
   */
  public double getDebitMax() {
    return debit_.getMax();
  }

  /**
   * @return le debit min
   */
  public double getDebitMin() {
    return debit_.getMin();
  }

  /**
   * @param _g le maillage concerne
   * @return l'envelope englobant
   */
  public Envelope getMinMaxPt(final EfGridInterface _g) {
    final EfFrontierInterface fr = _g.getFrontiers();
    final Envelope r = new Envelope();
    for (int i = getNbPoint() - 1; i >= 0; i--) {
      int global = fr.getIdxGlobalFrom(getPtIdx1(i));
      r.expandToInclude(_g.getPtX(global), _g.getPtY(global));
      // point 2
      global = fr.getIdxGlobalFrom(getPtIdx2(i));
      r.expandToInclude(_g.getPtX(global), _g.getPtY(global));
    }
    return r;
  }

  public double[] getCurvIndexes(final EfGridInterface _grid) {
    final double[] res = new double[getNbPoint()];
    final int lastIdx = _grid.getFrontiers().getIdxGlobalFrom(getPtIdx1(0));
    double lastX = _grid.getPtX(lastIdx);
    double lastY = _grid.getPtY(lastIdx);
    for (int i = 1; i < res.length; i++) {
      final int currentIdx = _grid.getFrontiers().getIdxGlobalFrom(getPtIdx1(i));
      final double currentX = _grid.getPtX(currentIdx);
      final double currentY = _grid.getPtY(currentIdx);
      res[i] = res[i - 1] + CtuluLibGeometrie.getDistance(lastX, lastY, currentX, currentY);
      lastX = currentX;
      lastY = currentY;
    }
    return res;
  }

  @Override
  public int getNbPoint() {
    return index1_.getSize();
  }

  @Override
  public int getPtIdx1(final int _idx) {
    return index1_.getValue(_idx);
  }

  @Override
  public int getPtIdx2(final int _idx) {
    return index2_.getValue(_idx);
  }

  /**
   * @param _i l'indice a tester
   * @return true si cette indice est utilise par ce seuil
   */
  public boolean isUsedIdx(final int _i) {
    return index1_.contains(_i) || index2_.contains(_i);
  }

}
