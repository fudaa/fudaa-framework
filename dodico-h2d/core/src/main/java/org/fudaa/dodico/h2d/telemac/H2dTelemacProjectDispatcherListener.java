/*
 * @creation 8 oct. 2003
 * @modification $Date: 2006-04-19 13:19:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.dodico.h2d.H2dProjectDispatcherListener;
import org.fudaa.dodico.h2d.H2dSIListener;

/**
 * @author deniger
 * @version $Id: H2dTelemacProjectDispatcherListener.java,v 1.11 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dTelemacProjectDispatcherListener extends H2dProjectDispatcherListener, H2dTelemacBcListener,
    H2dTelemacEvolutionUsedListener, H2dtelemacSiponListener, H2dTelemacSeuilListener, H2dTelemacNodalPropListener,
    H2dSIListener {
  
  void gridNodeXYChanged();

}