/*
 * @creation 18 oct. 2004
 * @modification $Date: 2007-01-10 09:04:22 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;

/**
 * @author Fred Deniger
 * @version $Id: H2DRubarFrictionModel.java,v 1.10 2007-01-10 09:04:22 deniger Exp $
 */
public final class H2DRubarFrictionModel extends CtuluArrayDouble {

  Set frictionListener_;

  /**
   * @param _init
   */
  public H2DRubarFrictionModel(final double[] _init) {
    super(_init);
  }

  public Set getListeners() {
    return frictionListener_ == null ? null : new HashSet(frictionListener_);
  }

  /**
   * @param _init intialisation
   */
  public H2DRubarFrictionModel(final CtuluCollectionDoubleEdit _init) {
    super(_init);
  }

  /**
   * @param _nb
   */
  public H2DRubarFrictionModel(final int _nb) {
    super(_nb);
  }

  public void addListener(final H2DRubarFrictionListener _si) {
    if (_si == null) {
      return;
    }
    if (frictionListener_ == null) {
      frictionListener_ = new HashSet();
    }
    frictionListener_.add(_si);
  }

  public void addListeners(final Collection _si) {
    if (_si == null) {
      return;
    }
    if (frictionListener_ == null) {
      frictionListener_ = new HashSet();
    }
    frictionListener_.addAll(_si);
  }

  public void removeListener(final H2DRubarFrictionListener _si) {
    if ((frictionListener_ == null) || (_si == null)) {
      return;
    }
    frictionListener_.remove(_si);
  }

  @Override
  protected void fireObjectChanged(int _indexGeom, Object _newValue) {
    if (frictionListener_ != null) {
      for (final Iterator it = frictionListener_.iterator(); it.hasNext();) {
        ((H2DRubarFrictionListener) it.next()).frictionChanged();
      }
    }
  }
}