/*
 * @creation 14 juin 2004
 * 
 * @modification $Date: 2006-09-19 14:43:25 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarBoundaryFlowrateGroupType.java,v 1.5 2006-09-19 14:43:25 deniger Exp $
 */
public class H2dRubarBoundaryFlowrateGroupType extends H2dRubarBoundaryFlowrateType {

  /**
   * @param _n
   * @param _rubarIdx
   */
  public H2dRubarBoundaryFlowrateGroupType(final String _n, final int _rubarIdx) {
    super(_n, _rubarIdx);
  }

  @Override
  public final boolean isTypeDebitGlobal() {
    return true;
  }

  /**
   * @return un nom court du bord (GR 1, ...)
   */
  public final String getShortName() {
    return "GR " + H2dRubarBcTypeList.to2DigitsString(rubarIdx_ - 30);
  }

}