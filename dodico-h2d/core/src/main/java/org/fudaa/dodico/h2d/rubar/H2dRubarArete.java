/*
 * @creation 9 juin 2004
 * @modification $Date: 2006-09-19 14:43:22 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.ef.EfSegment;
import org.fudaa.dodico.h2d.type.H2dRubarBcTypeList;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryType;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarArete.java,v 1.14 2006-09-19 14:43:22 deniger Exp $
 */
public class H2dRubarArete extends EfSegment {

  H2dRubarBoundaryType type_;

  /**
   * @param _pt1 l'indice 1 de l'arete
   * @param _pt2 l'indice 2 de l'arete
   * @param _t le type de l'arete
   */
  public H2dRubarArete(final int _pt1, final int _pt2, final H2dRubarBoundaryType _t) {
    super(_pt1, _pt2);
    type_ = _t;
  }

  /**
   * @param _s l'arete utilise pour l'initialisation
   */
  public H2dRubarArete(final H2dRubarArete _s) {
    super(_s);
    initRubarDataOnly(_s);
  }

  /**
   * @return true si arete externe et liquide
   */
  public boolean isExternAndLiquid() {
    return isExtern() && (type_.isLiquide());
  }

  /**
   * @return true si arete externe et liquide demandant une courbe temporelle
   */
  public boolean isExternAndLiquidAndTransient() {
    return isExtern() && (type_.needsTimeCurve());
  }

  public boolean isTarageType() {
    return type_ == H2dRubarBcTypeList.TARAGE || type_.isTypeTarageGlobal();
  }

  /**
   * @return une arete identique a celle-ci.
   */
  public H2dRubarArete createMemento() {
    return new H2dRubarArete(this);
  }

  /**
   * Recupere uniquement les donnees propres a rubar (attention !).
   *
   * @param _a l'arete source
   */
  private void initRubarDataOnly(final H2dRubarArete _a) {
    type_ = _a.type_;
  }

  /**
   * Recupere toutes les donnes depuis l'aretes passees en parametres.
   *
   * @param _a l'arete memento
   */
  protected void initFromMemento(final H2dRubarArete _a) {
    super.initFromSegment(_a);
    initRubarDataOnly(_a);
  }

  protected boolean setType(final H2dRubarBoundaryType _t) {
    if (_t == null) {
      return false;
    }
    if (type_ == null) {
      new Throwable("Interdit de modifier le type d'une arete interne").printStackTrace();
      return false;
    }
    if (_t.equals(H2dRubarBcTypeList.MIXTE)) {
      new Throwable("Mixte n'est pas un type valide").printStackTrace();
      return false;
    }
    if (!_t.equals(type_)) {
      type_ = _t;
      return true;
    }
    return false;

  }

  /**
   * @return true si externe
   */
  public boolean isExtern() {
    return type_ != null;
  }

  /**
   * @return le type de la frontiere
   */
  public H2dRubarBoundaryType getType() {
    return type_;
  }

  /**
   * @param _ar le tableau des aretes a tester
   * @param _g le maillage contenant les aretes
   * @return si une arete est intérieure renvoie null. Si toutes les aretes de meme type, renvoie le type commun. Sinon
   *         renvoie mixte.
   */
  public static H2dRubarBoundaryType getCommonType(final int[] _ar, final H2dRubarGrid _g) {
    if ((_ar == null) || (_ar.length == 0)) {
      return null;
    }
    H2dRubarBoundaryType r = _g.getRubarArete(_ar[0]).getType();
    if (r == null) {
      return r;
    }
    H2dRubarBoundaryType rt;
    for (int i = _ar.length - 1; i >= 1; i--) {
      rt = _g.getRubarArete(_ar[i]).getType();
      if (rt == null) {
        return null;
      } else if (!rt.equals(r)) {
        r = H2dRubarBcTypeList.MIXTE;
      }

    }
    return r;

  }
}
