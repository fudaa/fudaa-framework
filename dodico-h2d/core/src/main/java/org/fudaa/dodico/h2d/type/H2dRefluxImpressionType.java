/*
 * @creation 26 juin 2003
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import java.util.Iterator;
import java.util.List;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.dodico.commun.DodicoEnumType;

/**
 * @author deniger
 * @version $Id: H2dRefluxImpressionType.java,v 1.16 2006-09-19 14:43:25 deniger Exp $
 */
public final class H2dRefluxImpressionType extends DodicoEnumType {

  /**
   * IMPRESSION DONNEES.
   */
  public static final H2dRefluxImpressionType DONNEES = new H2dRefluxImpressionType("DONNEES");
  /**
   * IMPRESSION POINTEURS.
   */
  public static final H2dRefluxImpressionType POINTEURS = new H2dRefluxImpressionType("POINTEURS");
  /**
   * IMPRESSION TABLES_ELEM.
   */
  public static final H2dRefluxImpressionType TABLES_ELEM = new H2dRefluxImpressionType("TABLES_ELEM");
  /**
   * IMPRESSION TABLES_GLOB.
   */
  public static final H2dRefluxImpressionType TABLES_GLOB = new H2dRefluxImpressionType("TABLES_GLOB");
  /**
   * IMPRESSION CIEL.
   */
  public static final H2dRefluxImpressionType CIEL = new H2dRefluxImpressionType("CIEL");
  /**
   * IMPRESSION INITIALISATION.
   */
  public static final H2dRefluxImpressionType INITIALISATION = new H2dRefluxImpressionType("INITIALISATION");
  /**
   * IMPRESSION MISES_A_JOUR.
   */
  public static final H2dRefluxImpressionType MISES_A_JOUR = new H2dRefluxImpressionType("MISES_A_JOUR");
  /**
   * IMPRESSION RESOLUTION.
   */
  public static final H2dRefluxImpressionType RESOLUTION = new H2dRefluxImpressionType("RESOLUTION");
  /**
   * IMPRESSION ITERATION.
   */
  public static final H2dRefluxImpressionType ITERATION = new H2dRefluxImpressionType("ITERATION");
  /**
   * La liste des impression.
   */
  public final static List LIST = new CtuluPermanentList(CtuluLibArray.sort(new H2dRefluxImpressionType[] { DONNEES,
      POINTEURS, TABLES_ELEM, TABLES_GLOB, CIEL, INITIALISATION, MISES_A_JOUR, RESOLUTION, ITERATION }));

  public static H2dRefluxImpressionType[] getConstantArray() {
    return new H2dRefluxImpressionType[] { DONNEES, POINTEURS, TABLES_ELEM, TABLES_GLOB, CIEL, INITIALISATION,
        MISES_A_JOUR, RESOLUTION, ITERATION };
  }

  /**
   * @param _id l'identifiant reflux concernant les coef d'impression
   * @return la classes correspondante ou null si non trouvee.
   */
  public static H2dRefluxImpressionType getTypeForRefluxId(final String _id) {
    H2dRefluxImpressionType t;
    for (final Iterator it = LIST.iterator(); it.hasNext();) {
      t = (H2dRefluxImpressionType) it.next();
      if (t.getName().equals(_id)) {
        return t;
      }
    }
    return null;
  }

  H2dRefluxImpressionType() {
    this(CtuluLibString.EMPTY_STRING);
  }

  /**
   * @param _id
   * @param _nom
   */
  H2dRefluxImpressionType(final String _nom) {
    super(_nom);
  }

  public DodicoEnumType[] getArray() {
    return getConstantArray();
  }
}