/*
 * @creation 24 juin 2004
 * @modification $Date: 2007-04-30 14:21:34 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import java.util.HashMap;
import java.util.Map;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * Une classe utilisee pour connaitre l'utilisation de courbe par un projet. Si une evolution est utilise par plusieurs
 * type de variables, elle sera affecte a la variable null.
 *
 * @author Fred Deniger
 * @version $Id: H2dEvolutionVariableMap.java,v 1.10 2007-04-30 14:21:34 deniger Exp $
 */
public class H2dEvolutionVariableMap implements H2dEvolutionUseCounter {

  Map varEvol_;

  /**
   * Si une evolution est utilise par plusieurs type de variables, elle sera affecte a la variable null.
   *
   * @param _evol l'evolution
   * @param _t la variable representee par cette evolution
   */
  @Override
  public void add(final EvolutionReguliereInterface _evol, final H2dVariableType _t, final int _idxGlobal) {
    if (varEvol_ == null) {
      varEvol_ = new HashMap();
    }
    if (varEvol_.containsKey(_evol) && (varEvol_.get(_evol) != _t)) {
      varEvol_.put(_evol, null);
    } else {
      varEvol_.put(_evol, _t);
    }
  }

  /**
   * Reaffecte � l'evolution _e la variable _t.
   *
   * @param _e l'evol
   * @param _t la variable
   */
  public void reaffectEvol(final EvolutionReguliereAbstract _e, final H2dVariableType _t) {
    if (varEvol_ == null) {
      varEvol_ = new HashMap();
    }
    varEvol_.put(_e, _t);
  }

  public boolean isSet(final EvolutionReguliereAbstract _e) {
    return varEvol_ != null && varEvol_.containsKey(_e);
  }

  public H2dVariableType getVarFor(final EvolutionReguliereAbstract _evol) {
    if (varEvol_ == null) {
      return null;
    }
    return (H2dVariableType) varEvol_.get(_evol);
  }

  /**
   * @return la table (evol->H2DVariable)
   */
  public Map getVarEvol() {
    return varEvol_;
  }
}