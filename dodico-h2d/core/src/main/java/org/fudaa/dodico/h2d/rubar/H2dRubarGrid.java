/*
 * @creation 10 juin 2004
 * @modification $Date: 2007-05-04 13:46:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleAbstract;
import org.fudaa.dodico.ef.EfElementType;
import org.fudaa.dodico.ef.EfElementVolume;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.EfNode;
import org.fudaa.dodico.ef.EfSegment;
import org.fudaa.dodico.ef.EfVoisinageFinderActivity;
import org.fudaa.dodico.ef.impl.EfGridDefaultAbstract;
import org.fudaa.dodico.ef.impl.EfGridVolumeArray;
import org.fudaa.dodico.h2d.type.H2dRubarBcTypeList;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryType;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarGrid.java,v 1.18 2007-05-04 13:46:38 deniger Exp $
 */
public class H2dRubarGrid extends EfGridVolumeArray {


  public static H2dRubarGridAreteSource createCompleteGridFrom(final EfGridInterface _grid,
      final ProgressionInterface _prog, final CtuluAnalyze _analyse, final int _nbDec) {
    final EfVoisinageFinderActivity act = new EfVoisinageFinderActivity();
    final boolean ok = act.process(_grid, _prog, _analyse);
    if (!ok) {
      return null;
    }
    final EfSegment[] edges = act.getEdges();
    if (edges == null) {
      return null;
    }
    final H2dRubarArete[] rubarEdges = new H2dRubarArete[edges.length];
    final int[][] eltVoisinsParArete = act.getElementVoisinsParArete();
    for (int i = 0; i < edges.length; i++) {
      final int[] idx = eltVoisinsParArete[i];
      // si l'arete n'a qu'un seul element voisin, c'est une arete frontiere et solide
      final H2dRubarBoundaryType type = (idx[0] < 0 || idx[1] < 0) ? H2dRubarBcTypeList.SOLIDE : null;
      rubarEdges[i] = new H2dRubarArete(edges[i].getPt1Idx(), edges[i].getPt2Idx(), type);
    }
    final H2dRubarGrid newGrid = new H2dRubarGrid(_grid.getNodes(), act.getElts(), rubarEdges);
    final H2dRubarGridAreteDefault res = new H2dRubarGridAreteDefault(newGrid, act.getElementVoisinsParElement(),
        eltVoisinsParArete);
    res.nbDecimal_ = _nbDec;
    return res;
  }

  public H2dRubarGrid(final EfNode[] _pts, final EfElementVolume[] _elts, final H2dRubarArete[] _aretes) {
    super(_pts, _elts, _aretes);
  }

  public H2dRubarGrid(final EfNode[] _pts, final EfElementVolume[] _elts, final H2dRubarArete[] _aretes,
      final EfElementType _type) {
    super(_pts, _elts, _aretes);
    super.typeElt_ = _type;
  }

  protected void initWith(final H2dRubarGrid _g) {
    super.set(_g);
  }

  protected void initWithPoints(final H2dRubarGrid _g) {
    super.initPtFrom(_g);
  }

  public Double getCommonZ(final int[] _idx) {
    return CtuluCollectionDoubleAbstract.getCommonValue(EfGridDefaultAbstract.getBathyModel(this), _idx);
  }

  /**
   * @param _idx l'index de l'arete
   * @return l'arete correspondante
   */
  public H2dRubarArete getRubarArete(final int _idx) {
    return (H2dRubarArete) getArete(_idx);
  }

  @Override
  protected boolean setZIntern(final int _i, final double _newV) {
    return super.setZIntern(_i, _newV);
  }

  /**
   * @param _i l'indice de l'arete
   * @return le x du centre de l'arete _i
   */
  public double getXCentreArete(final int _i) {
    return getArete(_i).getCentreX(this);
  }
  
  public double getZCentreArete(final int _i) {
    return getArete(_i).getCentreZ(this);
  }

  /**
   * @param _i l'indice de l'arete
   * @return le y du centre de l'arete _i
   */
  public double getYCentreArete(final int _i) {
    return getArete(_i).getCentreY(this);
  }


}
