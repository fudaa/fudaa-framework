/*
 * @creation 3 janv. 2005
 * @modification $Date: 2007-05-04 13:46:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.dodico.h2d.H2dEvolutionVariableMap;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarOuvrageElementaireTransfert.java,v 1.8 2007-05-04 13:46:38 deniger Exp $
 */
public class H2dRubarOuvrageElementaireTransfert extends H2dRubarOuvrageElementaireApport {
  /**
   * @param _e l'evolution
   */
  public H2dRubarOuvrageElementaireTransfert(final LinkedHashMap<H2dVariableType, ? extends EvolutionReguliereAbstract> _e) {
    super(_e);
  }

  protected List<H2dVariableType> getTransportVariables() {
    return H2dRubarOuvrage.getTarageVariables();
  }

  @Override
  protected H2dVariableType getVariable() {
    return H2dVariableType.LOI_TARAGE;
  }

  /**
   * Constructeur dangereux rien d'initaliser.
   */
  public H2dRubarOuvrageElementaireTransfert(H2dRubarProjetType projet, int nbTransportsBlocks) {
    super(projet, nbTransportsBlocks);
  }

  protected H2dRubarOuvrageElementaireTransfert(final H2dRubarOuvrageElementaireTransfert _a, final boolean _ouvrageParent) {
    super(_a, _ouvrageParent);
  }

  protected final void initFrom(final H2dRubarOuvrageElementaireTransfert _a, final boolean _ouvrageParent) {
    if (_ouvrageParent) {
      ouvrageParent_ = _a.ouvrageParent_;
    }
    super.setEvols(_a.getEvols());
  }

  @Override
  public H2dRubarOuvrageElementaireInterface getCopyWithoutEvt() {
    return new H2dRubarOuvrageElementaireTransfert(this, false);
  }

  @Override
  public final H2dRubarOuvrageType getType() {
    return H2dRubarOuvrageType.TRANSFERT_DEBIT_TARAGE;
  }

  @Override
  public void fillWithTransientCurves(String prefixe, H2dEvolutionVariableMap r) {

  }

  @Override
  public void setProjetType(final H2dRubarProjetType projet, final int nbTransportBlock, final CtuluCommandComposite cmd) {
    //ne doit rien faire dans ce cas car pas lie au type de projet
  }

  @Override
  public boolean containsEvolution(EvolutionReguliereInterface tarage) {
    if (evols_ != null) {
      for (EvolutionReguliere evol : evols_.values()) {
        if (evol == tarage) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public void fillWithTarageEvolution(String prefix, Set allTarage) {
    if (evols_ != null) {
      for (Map.Entry<H2dVariableType, EvolutionReguliere> entry : evols_.entrySet()) {
        entry.getValue().setNom(prefix + "/ " + getType());
        allTarage.add(entry.getValue());
      }
    }
  }
}
