/*
 * @creation 7 juil. 2004
 *
 * @modification $Date: 2007-03-02 13:00:51 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

import java.util.ArrayList;
import java.util.List;

/**
 * Conteneur tres libre afin de connaitre les valeurs communes a plusieurs points.
 *
 * @author Fred Deniger
 */
public class H2dRubarTimeConditionCommon {
  /**
   * @return un conteneur par defaut
   */
  public static H2dRubarTimeConditionCommon getEmptyCommon(int nbConcentration) {
    final H2dRubarTimeConditionCommon r = new H2dRubarTimeConditionCommon(null);
    r.hCommon_ = false;
    r.qnCommon_ = false;
    r.qtCommon_ = false;
    r.qt_ = null;
    r.qn_ = null;
    r.h_ = null;
    for (int i = 0; i < nbConcentration; i++) {
      r.concentrationsBlock.add(H2dRubarTimeConditionConcentrationBlockCommon.getEmptyCommon());
    }
    return r;
  }

  List<H2dRubarTimeConditionConcentrationBlockCommon> concentrationsBlock = new ArrayList<>();
  /**
   * l'evolution commune.
   */
  public EvolutionReguliereAbstract h_;
  /**
   * true si h commun.
   */
  public boolean hCommon_;
  /**
   * l'evolution commune.
   */
  public EvolutionReguliereAbstract qn_;
  /**
   * true si qn commun.
   */
  public boolean qnCommon_;
  /**
   * l'evolution commune.
   */
  public EvolutionReguliereAbstract qt_;
  /**
   * true si qt commun.
   */
  public boolean qtCommon_;

  /**
   * @param _c source pour initialise ce conteneur. Tous les booleens sont mis a true.
   */
  public H2dRubarTimeConditionCommon(final H2dRubarTimeCondition _c) {
    if (_c != null) {
      h_ = _c.h_;
      qn_ = _c.qn_;
      qt_ = _c.qt_;
      for (int idxBlock = 0; idxBlock < _c.getNbConcentrationBlock(); idxBlock++) {
        concentrationsBlock.add(new H2dRubarTimeConditionConcentrationBlockCommon(_c.concentrationBlocks.get(idxBlock)));
      }
    }
    hCommon_ = true;
    qnCommon_ = true;
    qtCommon_ = true;
  }

  public List<H2dRubarTimeConditionConcentrationBlockCommon> getConcentrationsBlock() {
    return concentrationsBlock;
  }

  public int getNbBlocks() {
    return concentrationsBlock.size();
  }

  /**
   * @param _t la variable h,qn ou qt
   * @return l'evolution correspondante.
   */
  public EvolutionReguliereAbstract getEvol(final H2dVariableType _t, int idxBlock) {
    if (H2dRubarTimeConditionConcentrationBlock.isTransientVariable(_t)) {
      if (concentrationsBlock.size() > idxBlock) {
        return concentrationsBlock.get(idxBlock).getEvol(_t);
      }
      return null;
    }
    if (_t == H2dVariableType.DEBIT_NORMAL) {
      return qn_;
    }
    if (_t == H2dVariableType.DEBIT_TANGENTIEL) {
      return qt_;
    }
    if (_t == H2dVariableType.COTE_EAU) {
      return h_;
    }
    return null;
  }

  /**
   * Le boolean est a true si toutes les variables testees ont la meme evolution pour la meme variable.
   *
   * @param _t la variable h,qn ou qt
   * @return l'etat du boolean correspondant.
   */
  public boolean getVarState(final H2dVariableType _t, int idxBlock) {
    if (H2dRubarTimeConditionConcentrationBlock.isTransientVariable(_t)) {
      return concentrationsBlock.get(idxBlock).getVarState(_t);
    }
    if (_t == H2dVariableType.DEBIT_NORMAL) {
      return qnCommon_;
    }
    if (_t == H2dVariableType.DEBIT_TANGENTIEL) {
      return qtCommon_;
    }
    if (_t == H2dVariableType.COTE_EAU) {
      return hCommon_;
    }
    new Throwable().printStackTrace();
    return false;
  }

  public final boolean isNothingInCommonConcentration() {
    for (H2dRubarTimeConditionConcentrationBlockCommon blockCommon : concentrationsBlock) {
      if (!blockCommon.isNothingInCommons()) {
        return false;
      }
    }
    return true;
  }

  /**
   * Si true, cela signifie que les valeurs testees n'ont rien en commun.
   *
   * @return true si tous les booleens sont a false
   */
  public final boolean isNothingInCommon() {
    return ((!hCommon_) && (!qtCommon_) && (!qnCommon_) && (isNothingInCommonConcentration()));
  }

  /**
   * Permet de mettre a jour les booleens de ce conteneurs. Par exemple, si l'evolution h de _s est differente de
   * l'evolution h de ce conteneur le boolean h est mis a false et this.h a null. Attention avec cette methode: seules
   * les evolutions egales sont gardees.
   *
   * @param _s la condition a comparer
   * @return true si changement
   */
  public boolean keepSameEvolution(final H2dRubarTimeCondition _s) {
    boolean r = false;
    if (_s == null) {
      if (h_ != null) {
        h_ = null;
        hCommon_ = false;
        r = true;
      }
      for (H2dRubarTimeConditionConcentrationBlockCommon blockCommon : concentrationsBlock) {
        blockCommon.keepSameEvolution(null);
      }
      if (qn_ != null) {
        qn_ = null;
        qnCommon_ = false;
        r = true;
      }
      if (qt_ != null) {
        qt_ = null;
        qtCommon_ = false;
        r = true;
      }
    } else {
      if (h_ != _s.h_) {
        r = true;
        hCommon_ = false;
        h_ = null;
      }
      if (qn_ != _s.qn_) {
        r = true;
        qnCommon_ = false;
        qn_ = null;
      }
      if (qt_ != _s.qt_) {
        r = true;
        qtCommon_ = false;
        qt_ = null;
      }
      int idx = 0;
      for (H2dRubarTimeConditionConcentrationBlockCommon blockCommon : concentrationsBlock) {
        blockCommon.keepSameEvolution(_s.concentrationBlocks.get(idx++));
      }
    }
    return r;
  }
}
