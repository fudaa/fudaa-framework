/*
 * @creation 14 juin 2004
 *
 * @modification $Date: 2007-05-22 13:11:25 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.*;
import org.fudaa.ctulu.fileformat.FortranLib;
import org.fudaa.dodico.h2d.H2dEvolutionUseCounter;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionListenerDispatcher;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

import java.util.*;

/**
 * @author Fred Deniger
 */
public class H2dRubarTimeCondition implements H2dRubarTimeConditionInterface {
  final static Map VAR_FORMAT;

  static {
    VAR_FORMAT = new HashMap(4);
    // ces formats different de ceux utilise dans H2dRubarNumberFormatter !
    CtuluNumberFormater fmt = FortranLib.getFormater(10, 3, false);
    VAR_FORMAT.put(H2dVariableType.COTE_EAU, fmt);
    VAR_FORMAT.put(H2dVariableType.DEBIT_TANGENTIEL, fmt);
    VAR_FORMAT.put(H2dVariableType.DEBIT, fmt);
    VAR_FORMAT.put(H2dVariableType.DEBIT_NORMAL, fmt);
    fmt = FortranLib.getFormater(10, 7, false);
    VAR_FORMAT.put(H2dVariableTransType.CONCENTRATION, fmt);
    VAR_FORMAT.put(H2dVariableTransType.DIAMETRE, fmt);
    VAR_FORMAT.put(H2dVariableTransType.ETENDUE, fmt);
    fmt = FortranLib.getFormater(15, 4, false);
    VAR_FORMAT.put(H2dVariableTransType.APPORT_PLUIE, fmt);
  }

  protected final static H2dRubarTimeCondition NULL_TIME_COND = new H2dRubarTimeCondition(null, null, null, null);

  protected final static H2dRubarEvolution init(final H2dVariableType _t, final H2dRubarEvolution _e, final EvolutionListenerDispatcher _l) {
    if (_e != null) {
      _e.setFmt(getTimeFmt(_t), getFmt(_t), null);
      return _e;
    }
    final EvolutionReguliere e = new EvolutionReguliere();
    e.add(0, 0);
    return H2dRubarEvolution.createH2dRubarEvolution(_t, e, _l);
  }

  /**
   * @param _init la source pour initialiser la nouvelle condition temporelle
   * @param _l le listener
   * @return condition temporelle correctement initialisee.
   */
  public static H2dRubarTimeCondition create(final H2dRubarTimeConditionInterface _init, final EvolutionListenerDispatcher _l) {
    if (_init == null) {
      return null;
    }
    return new H2dRubarTimeCondition(_init, _l);
  }

  public static CtuluNumberFormater getFmt(final H2dVariableType _t) {
    if (_t == null) {
      return null;
    }
    return (CtuluNumberFormater) VAR_FORMAT.get(_t);
  }

  /**
   * @return la liste des evol contenants des erreurs.
   */
  public List<H2dRubarEvolution> getInvalidEvol() {
    List<H2dRubarEvolution> r = new ArrayList<>();
    CtuluNumberFormater fmt = getFmt(H2dVariableType.DEBIT_NORMAL);
    if (h_ != null && (h_.getYFmt() != fmt)) {
      if (h_.isIdxErrorForFmt(H2dRubarEvolution.getDefaultTimeFmt().getValueValidator(), fmt.getValueValidator())) {
        r.add(h_);
      }
    }
    if (qn_ != null && (qn_.getYFmt() != fmt)) {
      if (qn_.isIdxErrorForFmt(H2dRubarEvolution.getDefaultTimeFmt().getValueValidator(), fmt.getValueValidator())) {
        r.add(qn_);
      }
    }
    if (qt_ != null && (qt_.getYFmt() != fmt)) {
      if (qt_.isIdxErrorForFmt(H2dRubarEvolution.getDefaultTimeFmt().getValueValidator(), fmt.getValueValidator())) {
        r.add(qt_);
      }
    }
    for (H2dRubarTimeConditionConcentrationBlock block : this.concentrationBlocks) {
      block.getInvalidEvol(r);
    }

    return r;
  }

  /**
   * @param _h la courbe h a v�rifier
   * @param _qn la courbe qn a verifier
   * @param _qt la courbe qt a verifier
   * @param _hcat la courbe hcat a verifier
   * @return la liste des evol contenants des erreurs.
   */
  public static List getInvalidEvol(final H2dRubarEvolution _h, final H2dRubarEvolution _qn, final H2dRubarEvolution _qt,
                                    final H2dRubarEvolution _hcat, final H2dRubarEvolution _diam, final H2dRubarEvolution _et) {
    List r = null;
    CtuluNumberFormater fmt = getFmt(H2dVariableType.DEBIT_NORMAL);
    if (_h != null && (_h.getYFmt() != fmt)) {
      if (_h.isIdxErrorForFmt(H2dRubarEvolution.getDefaultTimeFmt().getValueValidator(), fmt.getValueValidator())) {
        r = new ArrayList(6);
        r.add(_h);
      }
    }
    if (_qn != null && (_qn.getYFmt() != fmt)) {
      if (_qn.isIdxErrorForFmt(H2dRubarEvolution.getDefaultTimeFmt().getValueValidator(), fmt.getValueValidator())) {
        if (r == null) {
          r = new ArrayList(6);
        }
        r.add(_qn);
      }
    }
    if (_qt != null && (_qt.getYFmt() != fmt)) {
      if (_qt.isIdxErrorForFmt(H2dRubarEvolution.getDefaultTimeFmt().getValueValidator(), fmt.getValueValidator())) {
        if (r == null) {
          r = new ArrayList(6);
        }
        r.add(_qt);
      }
    }
    fmt = getFmt(H2dVariableTransType.CONCENTRATION);
    if (_hcat != null && (_hcat.getYFmt() != fmt)) {
      if (_hcat.isIdxErrorForFmt(H2dRubarEvolution.getDefaultTimeFmt().getValueValidator(), fmt.getValueValidator())) {
        if (r == null) {
          r = new ArrayList(6);
        }
        r.add(_hcat);
      }
    }
    fmt = getFmt(H2dVariableTransType.DIAMETRE);
    if (_diam != null && (_diam.getYFmt() != fmt)) {
      if (_diam.isIdxErrorForFmt(H2dRubarEvolution.getDefaultTimeFmt().getValueValidator(), fmt.getValueValidator())) {
        if (r == null) {
          r = new ArrayList(6);
        }
        r.add(_diam);
      }
    }
    fmt = getFmt(H2dVariableTransType.ETENDUE);
    if (_hcat != null && (_hcat.getYFmt() != fmt)) {
      if (_et.isIdxErrorForFmt(H2dRubarEvolution.getDefaultTimeFmt().getValueValidator(), fmt.getValueValidator())) {
        if (r == null) {
          r = new ArrayList(6);
        }
        r.add(_et);
      }
    }
    return r;
  }

  public static CtuluNumberFormater getTimeFmt(final H2dVariableType _t) {
    if (_t == H2dVariableTransType.APPORT_PLUIE) {
      return H2dRubarApportSpatialMng.APP_TIME_FORMAT;
    }
    if (_t == H2dVariableType.VENT_X || _t == H2dVariableType.VENT_Y) {
      return H2dRubarVentMng.VENT_TIME_FORMAT;
    }
    return H2dRubarEvolution.getDefaultTimeFmt();
  }

  H2dRubarEvolution h_;
  H2dRubarEvolution qn_;
  H2dRubarEvolution qt_;
  List<H2dRubarTimeConditionConcentrationBlock> concentrationBlocks = new ArrayList<>();

  @Override
  public int getNbConcentrationBlock() {
    return concentrationBlocks.size();
  }

  public H2dRubarTimeCondition(int nbBlocks) {
    for (int i = 0; i < nbBlocks; i++) {
      concentrationBlocks.add(new H2dRubarTimeConditionConcentrationBlock());
    }
  }

  protected H2dRubarTimeCondition(final H2dRubarEvolution _h, final H2dRubarEvolution _qn, final H2dRubarEvolution _qt,
                                  final CtuluCommandContainer _cmd) {
    CtuluNumberFormater fmt = getFmt(H2dVariableType.COTE_EAU);
    h_ = _h;
    if (h_ != null) {
      h_.setFmt(H2dRubarEvolution.getDefaultTimeFmt(), fmt, _cmd);
    }
    qn_ = _qn;
    if (qn_ != null) {
      qn_.setFmt(H2dRubarEvolution.getDefaultTimeFmt(), fmt, _cmd);
    }
    qt_ = _qt;
    if (qt_ != null) {
      qt_.setFmt(H2dRubarEvolution.getDefaultTimeFmt(), fmt, _cmd);
    }
  }

  protected H2dRubarTimeCondition(final H2dRubarTimeConditionInterface h2dRubarTimeConditionInterface, final EvolutionListenerDispatcher dispatcher) {
    if (h2dRubarTimeConditionInterface != null) {
      h_ = H2dRubarEvolution.createH2dRubarEvolution(H2dVariableType.COTE_EAU, h2dRubarTimeConditionInterface.getHEvol(), dispatcher);
      qn_ = H2dRubarEvolution.createH2dRubarEvolution(H2dVariableType.DEBIT_NORMAL, h2dRubarTimeConditionInterface.getQnEvol(), dispatcher);
      qt_ = H2dRubarEvolution.createH2dRubarEvolution(H2dVariableType.DEBIT_TANGENTIEL, h2dRubarTimeConditionInterface.getQtEvol(), dispatcher);
      for (int i = 0; i < h2dRubarTimeConditionInterface.getNbConcentrationBlock(); i++) {
        concentrationBlocks.add(new H2dRubarTimeConditionConcentrationBlock(h2dRubarTimeConditionInterface, dispatcher, i));
      }
    }
  }

  public void clearTransport(final CtuluCommandComposite cmp) {
    if (concentrationBlocks.isEmpty()) {
      return;
    }
    final List<H2dRubarTimeConditionConcentrationBlock> oldBlocks = new ArrayList<>(concentrationBlocks);
    concentrationBlocks.clear();
    if (cmp != null) {
      cmp.addCmd(new CtuluCommand() {
        @Override
        public void undo() {
          concentrationBlocks = new ArrayList<>(oldBlocks);
        }

        @Override
        public void redo() {
          clearTransport(null);
        }
      });
    }
  }

  public void initTransport(final H2dRubarEvolution concentration, final H2dRubarEvolution diametre, final H2dRubarEvolution etendue,
                            final int nbBlock,
                            final CtuluCommandComposite cmp) {
    if (concentrationBlocks.size() == nbBlock) {
      return;
    }
    final List<H2dRubarTimeConditionConcentrationBlock> oldBlocks = new ArrayList<>(concentrationBlocks);
    int currentBlock = concentrationBlocks.size();
    for (int i = currentBlock; i < nbBlock; i++) {
      H2dRubarTimeConditionConcentrationBlock block = new H2dRubarTimeConditionConcentrationBlock(concentration, diametre, etendue, null);
      concentrationBlocks.add(block);
    }
    for (int i = nbBlock; i < currentBlock; i++) {
      concentrationBlocks.remove(concentrationBlocks.size() - 1);
    }
    if (cmp != null) {
      cmp.addCmd(new CtuluCommand() {
        @Override
        public void undo() {
          concentrationBlocks = new ArrayList<>(oldBlocks);
        }

        @Override
        public void redo() {
          initTransport(concentration, diametre, etendue, nbBlock, null);
        }
      });
    }
  }

  @Override
  public EvolutionReguliere getConcentrationEvolution(int blockIdx) {
    if (CtuluLibArray.isEmpty(concentrationBlocks)) {
      return null;
    }
    return concentrationBlocks.get(blockIdx).getConcentrationEvolution();
  }

  @Override
  public EvolutionReguliere getDiametreEvolution(int blockIdx) {
    if (CtuluLibArray.isEmpty(concentrationBlocks)) {
      return null;
    }
    return concentrationBlocks.get(blockIdx).getDiametreEvolution();
  }

  @Override
  public EvolutionReguliere getEtendueEvolution(int blockIdx) {
    if (CtuluLibArray.isEmpty(concentrationBlocks)) {
      return null;
    }
    return concentrationBlocks.get(blockIdx).getEtendueEvolution();
  }


  private void initH(final EvolutionListenerDispatcher _l) {
    h_ = init(H2dVariableType.COTE_EAU, h_, _l);
  }

  private void initQn(final EvolutionListenerDispatcher _l) {
    qn_ = init(H2dVariableType.DEBIT_NORMAL, qn_, _l);
  }

  private void initQt(final EvolutionListenerDispatcher _l) {
    qt_ = init(H2dVariableType.DEBIT_TANGENTIEL, qt_, _l);
  }

  protected void clearCurves() {
    if (h_ != null) {
      h_.setListener(null);
    }
    if (qn_ != null) {
      qn_.setListener(null);
    }
    if (qt_ != null) {
      qt_.setListener(null);
    }
    for (H2dRubarTimeConditionConcentrationBlock concentrationBlock : concentrationBlocks) {
      concentrationBlock.clearCurves();
    }
  }

  protected H2dRubarTimeCondition createCopy() {
    final H2dRubarTimeCondition h2dRubarTimeCondition = new H2dRubarTimeCondition(h_, qn_, qt_, null);
    for (H2dRubarTimeConditionConcentrationBlock concentrationBlock : concentrationBlocks) {
      h2dRubarTimeCondition.concentrationBlocks.add(concentrationBlock.createCopy());
    }
    return h2dRubarTimeCondition;
  }

  public void removeNonTorrentielForHauteurOnly() {
    if (h_ == H2dRubarParameters.NON_TORENTIEL) {
      h_ = null;
    }
  }

  protected void removeNonTorrentiel() {
    removeNonTorrentielForHauteurOnly();
    if (qn_ == H2dRubarParameters.NON_TORENTIEL) {
      qn_ = null;
    }
    if (qt_ == H2dRubarParameters.NON_TORENTIEL) {
      qt_ = null;
    }
    for (H2dRubarTimeConditionConcentrationBlock concentrationBlock : concentrationBlocks) {
      concentrationBlock.removeNonTorrentiel();
    }
  }

  public boolean setEvolution(final H2dVariableType _t, final H2dRubarEvolution _e, int blockIdx, final CtuluCommandContainer _cmd, boolean changeUseParameter) {
    final EvolutionReguliereAbstract old = getEvol(_t, blockIdx);
    if (H2dRubarTimeConditionConcentrationBlock.isTransientVariable(_t)
    ) {
      return concentrationBlocks.get(blockIdx).setEvolution(_t, _e, _cmd, changeUseParameter);
    }
    boolean r;
    if (_t == H2dVariableType.DEBIT_NORMAL) {
      r = setQn(_e, _cmd);
    } else if (_t == H2dVariableType.DEBIT_TANGENTIEL) {
      r = setQt(_e, _cmd);
    } else if (_t == H2dVariableType.COTE_EAU) {
      r = setH(_e, _cmd);
    } else {
      throw new RuntimeException("Variable " + _t.getName() + " not supported here.");
    }
    if (changeUseParameter) {
      if (r) {
        if (old != null) {
          old.setUnUsed(true);
        }
        if (_e != null) {
          _e.setUsed(true);
        }
      }
    }
    return r;
  }

  protected boolean setH(final H2dRubarEvolution _e, final CtuluCommandContainer _cmd) {
    if (_e != h_) {
      h_ = _e;
      if (h_ != null) {
        h_.setFmt(H2dRubarEvolution.getDefaultTimeFmt(), getFmt(H2dVariableType.COTE_EAU), _cmd);
      }
      return true;
    }
    return false;
  }

  protected boolean setQn(final H2dRubarEvolution _e, final CtuluCommandContainer _cmd) {
    if (_e != qn_) {
      qn_ = _e;
      if (qn_ != null) {
        qn_.setFmt(H2dRubarEvolution.getDefaultTimeFmt(), getFmt(H2dVariableType.DEBIT_NORMAL), _cmd);
      }
      return true;
    }
    return false;
  }

  protected boolean setQt(final H2dRubarEvolution _e, final CtuluCommandContainer _cmd) {
    if (_e != qt_) {
      qt_ = _e;
      if (qt_ != null) {
        qt_.setFmt(H2dRubarEvolution.getDefaultTimeFmt(), getFmt(H2dVariableType.DEBIT_TANGENTIEL), _cmd);
      }
      return true;
    }
    return false;
  }

  protected void setUnUsed() {
    if (h_ != null) {
      h_.setUnUsed(true);
    }
    if (qn_ != null) {
      qn_.setUnUsed(true);
    }
    if (qt_ != null) {
      qt_.setUnUsed(true);
    }
    for (H2dRubarTimeConditionConcentrationBlock concentrationBlock : concentrationBlocks) {
      concentrationBlock.setUnUsed();
    }
  }

  protected void setUsed() {
    if (h_ != null) {
      h_.setUsed(true);
    }
    if (qn_ != null) {
      qn_.setUsed(true);
    }
    if (qt_ != null) {
      qt_.setUsed(true);
    }
    for (H2dRubarTimeConditionConcentrationBlock concentrationBlock : concentrationBlocks) {
      concentrationBlock.setUsed();
    }
  }

  /**
   * @param _pref le prefixe pour les noms
   */
  protected void updateNom(final String _pref) {
    if (h_ != null) {
      h_.setNom(_pref + CtuluLibString.ESPACE + H2dVariableType.COTE_EAU);
    }
    if (qn_ != null) {
      qn_.setNom(_pref + CtuluLibString.ESPACE + H2dVariableType.DEBIT_NORMAL);
    }
    if (qt_ != null) {
      qt_.setNom(_pref + CtuluLibString.ESPACE + H2dVariableType.DEBIT_TANGENTIEL);
    }
    int idx = 1;
    for (H2dRubarTimeConditionConcentrationBlock concentrationBlock : concentrationBlocks) {
      concentrationBlock.updateNom(_pref, " " + (idx++));
    }
  }

  protected void verify(final List _varAllowed, final CtuluAnalyze _an, final int _areteIdx, final EvolutionListenerDispatcher _l) {
    if (_varAllowed.contains(H2dVariableType.COTE_EAU) && (h_ == null)) {
      initH(_l);
    } else if (!_varAllowed.contains(H2dVariableType.COTE_EAU) && (h_ != null)) {
      h_ = null;
    }
    if (_varAllowed.contains(H2dVariableType.DEBIT_NORMAL) && (qn_ == null)) {
      initQn(_l);
    } else if (!_varAllowed.contains(H2dVariableType.DEBIT_NORMAL) && (qn_ != null)) {
      qn_ = null;
    }
    if (_varAllowed.contains(H2dVariableType.DEBIT_TANGENTIEL) && (qt_ == null)) {
      initQt(_l);
    } else if (!_varAllowed.contains(H2dVariableType.DEBIT_TANGENTIEL) && (qt_ != null)) {
      qt_ = null;
    }
    for (H2dRubarTimeConditionConcentrationBlock concentrationBlock : concentrationBlocks) {
      concentrationBlock.verify(_varAllowed, _an, _areteIdx, _l);
    }
  }

  /**
   * @param _m la table a remplir avec les evolutions-Variables.
   */
  public void fillWithTransientCurves(final H2dEvolutionUseCounter _m, final int _idx) {
    if (h_ != null) {
      _m.add(h_, H2dVariableType.COTE_EAU, _idx);
    }
    if (qn_ != null) {
      _m.add(qn_, H2dVariableType.DEBIT_NORMAL, _idx);
    }
    if (qt_ != null) {
      _m.add(qt_, H2dVariableType.DEBIT_TANGENTIEL, _idx);
    }
    for (H2dRubarTimeConditionConcentrationBlock concentrationBlock : concentrationBlocks) {
      concentrationBlock.fillWithTransientCurves(_m, _idx);
    }
  }

  /**
   * @param _var COTE_DEBIT, DEBIT_TANGENTIEL ou DEBIT_NORMAL
   * @return l'evolution correspondante ou null si non trouvee.
   */
  public H2dRubarEvolution getEvol(final H2dVariableType _var, int blockIdx) {
    if (_var == H2dVariableType.COTE_EAU) {
      return h_;
    }
    if (_var == H2dVariableType.DEBIT_TANGENTIEL) {
      return qt_;
    }
    if (_var == H2dVariableType.DEBIT_NORMAL) {
      return qn_;
    }
    if (H2dRubarTimeConditionConcentrationBlock.isTransientVariable(_var)) {
      return concentrationBlocks.get(blockIdx).getEvol(_var);
    }
    return null;
  }

  @Override
  public H2dRubarEvolution getHEvol() {
    return h_;
  }

  @Override
  public H2dRubarEvolution getQnEvol() {
    return qn_;
  }

  @Override
  public H2dRubarEvolution getQtEvol() {
    return qt_;
  }

  /**
   * @return true si toutes les evolutions sont nulles
   */
  public boolean isAllNull() {
    for (H2dRubarTimeConditionConcentrationBlock concentrationBlock : concentrationBlocks) {
      if (!concentrationBlock.isAllNull()) {
        return false;
      }
    }
    return (h_ == null) && (qn_ == null) && (qt_ == null);
  }

  @Override
  public boolean isTrans() {
    return !concentrationBlocks.isEmpty();
  }

  /**
   * A utiliser pour connaitre les variables qui ne sont pas definies.
   *
   * @param _requiredVariable la liste des variables qui doivent etre definies.
   * @return la liste des variables de cet objet qui ne sont pas definies comme demande par le parametres
   *   _requiredVariable. Si toutes les variables sont definies renvoie null.
   */
  public List isVariableRequiredSet(final List _requiredVariable) {
    if (_requiredVariable == null) {
      return null;
    }
    final List r = new ArrayList(_requiredVariable.size());
    if (_requiredVariable.contains(H2dVariableType.COTE_EAU) && (h_ == null)) {
      r.add(H2dVariableType.COTE_EAU);
    }
    if (_requiredVariable.contains(H2dVariableType.DEBIT_NORMAL) && (qn_ == null)) {
      r.add(H2dVariableType.DEBIT_NORMAL);
    }
    if (_requiredVariable.contains(H2dVariableType.DEBIT_TANGENTIEL) && (qt_ == null)) {
      r.add(H2dVariableType.DEBIT_TANGENTIEL);
    }
    Set<H2dVariableType> blocks = new HashSet();
    for (H2dRubarTimeConditionConcentrationBlock concentrationBlock : concentrationBlocks) {
      concentrationBlock.isVariableRequiredSet(_requiredVariable, blocks);
    }
    r.addAll(blocks);
    return r.size() == 0 ? null : r;
  }

  /**
   * @param _l la liste des variables qui doivent etre definies
   * @return true si toutes les variables sont definies
   */
  public boolean isVariableSet(final List _l) {
    if (_l.contains(H2dVariableType.COTE_EAU) && (h_ == null)) {
      return false;
    }
    if (_l.contains(H2dVariableType.DEBIT_TANGENTIEL) && (qt_ == null)) {
      return false;
    }
    if (_l.contains(H2dVariableType.DEBIT_NORMAL) && (qn_ == null)) {
      return false;
    }
    for (H2dRubarTimeConditionConcentrationBlock concentrationBlock : concentrationBlocks) {
      if (!concentrationBlock.isVariableSet(_l)) {
        return false;
      }
    }
    return true;
  }

  /**
   * Permet de modifier toutes les variables de cette cl. Les variables qui ne sont pas contenues dans la liste
   * <code>_varToModified</code> seront mis a zero (pas de courbes). Les variables contenues seront comparees avec
   * <code>_clToInitFrom</code> et mis a jour si differentes.
   *
   * @param _varToModified la liste des variables qui seront cree.
   * @param _clToInitFrom la source pour la modif
   * @return true si modif
   */
  public boolean majWithNewList(final List _varToModified, final H2dRubarTimeCondition _clToInitFrom) {
    boolean r = false;
    if ((_varToModified == null) || (_varToModified.size() == 0)) {
      r = (h_ != null) || (qn_ != null) || (qt_ != null);
      int idx = 0;
      for (H2dRubarTimeConditionConcentrationBlock concentrationBlock : concentrationBlocks) {
        r |= concentrationBlock.majWithNewList(_varToModified, _clToInitFrom.concentrationBlocks.get(idx++));
      }
      if (h_ != null) {
        h_.setUnUsed(true);
      }
      if (qn_ != null) {
        qn_.setUnUsed(true);
      }
      if (qt_ != null) {
        qt_.setUnUsed(true);
      }
      h_ = null;
      qn_ = null;
      qt_ = null;
    } else {
      if (_varToModified.contains(H2dVariableType.COTE_EAU)) {
        if ((_clToInitFrom.h_ != null) && (h_ != _clToInitFrom.h_)) {
          if (h_ != null) {
            h_.setUnUsed(true);
          }
          h_ = _clToInitFrom.h_;
          r = true;
          if (h_ == H2dRubarParameters.NON_TORENTIEL) {
            h_ = null;
          } else {
            h_.setUsed(true);
          }
        }
        if (h_ == null) {
          new Throwable().printStackTrace();
        }
      } else if (h_ != null) {
        h_.setUnUsed(true);
        h_ = null;
        r = true;
      }
      if (_varToModified.contains(H2dVariableType.DEBIT_NORMAL)) {
        if ((_clToInitFrom.qn_ != null) && (qn_ != _clToInitFrom.qn_)) {
          if (qn_ != null) {
            qn_.setUnUsed(true);
          }
          qn_ = _clToInitFrom.qn_;
          if (qn_ == H2dRubarParameters.NON_TORENTIEL) {
            qn_ = null;
          } else {
            qn_.setUsed(true);
          }
          r = true;
        }
        if (qn_ == null) {
          new Throwable().printStackTrace();
        }
      } else if (qn_ != null) {
        qn_.setUnUsed(true);
        qn_ = null;
        r = true;
      }
      if (_varToModified.contains(H2dVariableType.DEBIT_TANGENTIEL)) {
        if ((_clToInitFrom.qt_ != null) && (qt_ != _clToInitFrom.qt_)) {
          if (qt_ != null) {
            qt_.setUnUsed(true);
          }
          qt_ = _clToInitFrom.qt_;
          if (qt_ == H2dRubarParameters.NON_TORENTIEL) {
            qt_ = null;
          } else {
            qt_.setUsed(true);
          }
          r = true;
        }
        if (qt_ == null) {
          new Throwable().printStackTrace();
        }
      } else if (qt_ != null) {
        qt_.setUnUsed(true);
        qt_ = null;
        r = true;
      }
    }
    return r;
  }
}
