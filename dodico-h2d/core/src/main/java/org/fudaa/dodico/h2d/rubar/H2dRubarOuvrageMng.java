/*
 * @creation 3 janv. 2005
 * @modification $Date: 2007-04-30 14:21:34 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import com.memoire.fu.FuEmptyArrays;
import com.memoire.fu.FuLog;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntHashSet;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.collection.CtuluListObject;
import org.fudaa.dodico.h2d.H2dEvolutionVariableMap;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageRef;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.mesure.EvolutionListener;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarOuvrageMng.java,v 1.13 2007-04-30 14:21:34 deniger Exp $
 */
public class H2dRubarOuvrageMng implements H2dRubarOuvrageContainer, EvolutionListener {
  void fillWithTransientCurves(H2dEvolutionVariableMap r) {
    int nb = getNbOuvrage();
    for (int i = 0; i < nb; i++) {
      H2dRubarOuvrageI ouvrage = getOuvrage(i);
      int nbOuvrageElementaires = ouvrage.getNbOuvrageElementaires();
      String pref = H2dResource.getS("Ouvrage {0}", Integer.toString(i + 1));
      for (int j = 0; j < nbOuvrageElementaires; j++) {
        String prefj = pref;
        if (nbOuvrageElementaires > 1) {
          prefj = prefj + " (" + (j + 1) + ") ";
        }
        ouvrage.getOuvrageElementaire(j).fillWithTransientCurves(prefj + " ", r);
      }
    }
  }

  @Override
  public void evolutionChanged(EvolutionReguliereInterface _e) {
    int nb = getNbOuvrage();
    for (int i = 0; i < nb; i++) {
      H2dRubarOuvrageI ouvrage = getOuvrage(i);
      int nbOuvrageElementaires = ouvrage.getNbOuvrageElementaires();
      for (int j = 0; j < nbOuvrageElementaires; j++) {
        ouvrage.getOuvrageElementaire(j).evolutionChanged(_e);
      }
    }
  }

  @Override
  public void evolutionUsedChanged(EvolutionReguliereInterface _e, int _old, int _new, boolean _isAdjusting) {
  }

  public boolean containsTarage(EvolutionReguliereInterface tarage) {
    int nb = getNbOuvrage();
    for (int i = 0; i < nb; i++) {
      H2dRubarOuvrageI ouvrage = getOuvrage(i);
      int nbOuvrageElementaires = ouvrage.getNbOuvrageElementaires();
      for (int j = 0; j < nbOuvrageElementaires; j++) {
        if (ouvrage.getOuvrageElementaire(j).containsEvolution(tarage)) {
          return true;
        }
      }
    }
    return false;
  }

  public Collection getAllTarageEvolutions() {
    Set allTarage = new HashSet();
    int nb = getNbOuvrage();
    for (int i = 0; i < nb; i++) {
      String pref = H2dResource.getS("Ouvrage {0}", Integer.toString(i + 1));
      H2dRubarOuvrageI ouvrage = getOuvrage(i);
      int nbOuvrageElementaires = ouvrage.getNbOuvrageElementaires();
      for (int j = 0; j < nbOuvrageElementaires; j++) {
        String prefj = pref;
        if (nbOuvrageElementaires > 1) {
          prefj = prefj + " (" + (j + 1) + ") ";
        }
        ouvrage.getOuvrageElementaire(j).fillWithTarageEvolution(prefj + " ", allTarage);
      }
    }
    return allTarage;
  }

  protected class OuvrageModel extends CtuluListObject {
    public OuvrageModel() {
      super();
    }

    public OuvrageModel(final Collection _init) {
      super(_init);
    }

    public OuvrageModel(final CtuluListObject _init) {
      super(_init);
    }

    public OuvrageModel(final int _nb) {
      super(_nb);
    }

    @Override
    protected void fireObjectAdded(int _newIdx, Object _newGeom) {
      H2dRubarOuvrageMng.this.fireOuvrageAdded();
    }

    @Override
    protected void fireObjectChanged(int _oldIdx, Object _oldGeom) {
      new Throwable("ne doit pas arriver").printStackTrace();
      H2dRubarOuvrageMng.this.fireOuvrageChanged(null, true);
    }

    @Override
    protected void fireObjectRemoved(int _oldIdx, Object _oldGeom) {
      H2dRubarOuvrageMng.this.fireOuvrageRemoved();
    }

    @Override
    protected void fireObjectModified(int _idx, Object _geom) {
      H2dRubarOuvrageMng.this.fireOuvrageRemoved();
    }
  }

  private H2dRubarGridAreteSource grid_;
  private List listener_;
  private CtuluListObject ouv_;
  H2dRubarOuvrageRef defaut_;

  @Override
  public H2dRubarOuvrageI getOuvrage(final int _i) {
    return getRubarOuvrage(_i);
  }

  public H2dRubarOuvrageMng(final H2dRubarGridAreteSource _grid) {
    grid_ = _grid;
  }

  protected void createModel() {
    if (ouv_ == null) {
      ouv_ = new OuvrageModel();
    }
  }

  protected void createModel(final Collection _c) {
    if (ouv_ == null) {
      ouv_ = new OuvrageModel(_c);
    }
  }

  protected void fireOuvrageAdded() {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        getListener(i).ouvrageAdded(this);
      }
    }
  }

  protected void fireOuvrageChanged(final H2dRubarOuvrage _target, final boolean _ouvElemAddedOrRemoved) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        getListener(i).ouvrageChanged(this, _target, _ouvElemAddedOrRemoved);
      }
    }
  }

  protected void fireOuvrageElementaireChanged(final H2dRubarOuvrage _target,
                                               final H2dRubarOuvrageElementaireInterface _i) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        getListener(i).ouvrageElementaireChanged(this, _target, _i);
      }
    }
  }

  protected void fireOuvrageRemoved() {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        getListener(i).ouvrageRemoved(this);
      }
    }
  }

  protected H2dRubarOuvrageListener getListener(final int _i) {
    return (H2dRubarOuvrageListener) listener_.get(_i);
  }

  /**
   * @return le maillage support
   */
  public final H2dRubarGridAreteSource getGrid() {
    return grid_;
  }

  final void setDefaut(final H2dRubarOuvrageRef _defaut) {
    defaut_ = _defaut;
  }

  /**
   * Ne verifie pas si le listener est deja present.
   *
   * @param _l le listener a ajouter
   */
  public void addListener(final H2dRubarOuvrageListener _l) {
    if (listener_ == null) {
      listener_ = new ArrayList(5);
    }
    listener_.add(_l);
  }

  /**
   * @param _i l'ouvrage a ajouter
   * @param _cmd le receveur de commandes
   */
  public void addOuvrage(final H2dRubarOuvrageI _i, final CtuluCommandContainer _cmd) {
    if (!isInitialized()) {
      return;
    }
    createModel();
    ouv_.addObject(_i, _cmd);
  }

  public boolean isSelectionValide(final int[] _idxSelected) {
    if (!isInitialized()) {
      FuLog.warning("can't add works");
      return false;
    }
    return H2dRubarOuvrage.isSelectionCorrect(_idxSelected, grid_);
  }

  public String isValid(final int[] _eltIntern, final int _elt1, final int _arete1, final int _elt2, final int _arete2) {
    if (!isInitialized()) {
      return H2dResource.getS("Fichier dat non charg�");
    }
    if (_elt1 < 0) {
      return H2dResource.getS("Element 1 ind�fini");
    }
    final TIntHashSet set = new TIntHashSet();
    set.addAll(_eltIntern);
    set.add(_elt1);
    if (_elt2 >= 0) {
      set.add(_elt2);
    }
    if (!H2dRubarOuvrage.isSelectionCorrect(_elt1, _elt2, _eltIntern, grid_)) {
      return H2dResource.getS("Les �l�ments ne sont pas adjacents");
    }
    if (!grid_.getRubarGrid().getEltVolume(_elt1).containsAreteIdx(_arete1)) {
      return H2dResource.getS("L'ar�te {0} est mal d�finie", CtuluLibString.UN);
    }
    if (_elt2 >= 0 && !grid_.getRubarGrid().getEltVolume(_elt2).containsAreteIdx(_arete2)) {
      return H2dResource.getS("L'ar�te {0} est mal d�finie", CtuluLibString.DEUX);
    }
    return null;
  }

  /**
   * @param _eltIntern les elements internes
   * @param _elt1 l'element 1 amond
   * @param _arete1 l'arete 1
   * @param _elt2 l'element 2 aval
   * @param _arete2 l'arete 2
   * @param _cmd le receveur de commande
   * @return l'ouvrage cree.
   */
  public H2dRubarOuvrage addOuvrage(final int[] _eltIntern, final int _elt1, final int _arete1, final int _elt2,
                                    final int _arete2, final CtuluCommandContainer _cmd) {
    final String error = isValid(_eltIntern, _elt1, _arete1, _elt2, _arete2);
    if (error != null) {
      return null;
    }
    H2dRubarOuvrageRef ref = getDefaultOuvrageRef();
    if (ref.getRubarCode() == -2 && _elt2 < 0) {
      ref = H2dRubarOuvrageRef.COMPUTE_WITH_SAINT_VENANT;
    }
    final H2dRubarOuvrage o = new H2dRubarOuvrage(this, _elt1, _arete1, _elt2, _arete2, _eltIntern, ref);
    addOuvrage(o, _cmd);
    return o;
  }

  /**
   * @param _l le listener a tester
   * @return true si deja enregistre
   */
  public boolean containsListener(final H2dRubarOuvrageListener _l) {
    return (listener_ != null) && listener_.contains(_l);
  }

  public H2dRubarOuvrageRef getDefaultOuvrageRef() {
    if (defaut_ == null) {
      defaut_ = H2dRubarOuvrageRef.COMPUTE_WITH_SAINT_VENANT;
    }
    return defaut_;
  }

  /**
   * @return le nombre d'ouvrage contenu
   */
  @Override
  public int getNbOuvrage() {
    return ouv_ == null ? 0 : ouv_.getSize();
  }

  public void setProjetType(final H2dRubarProjetType projet, final int nbTransportBlock, final CtuluCommandContainer cmd) {
    CtuluCommandComposite cmp = null;
    if (cmd != null) {
      cmp = new CtuluCommandComposite();
    }
    for (int i = 0; i < getNbOuvrage(); i++) {
      getRubarOuvrage(i).setProjetType(projet, nbTransportBlock, cmp);
    }
    if (cmp != null) {
      cmd.addCmd(cmp.getSimplify());
    }
  }

  /**
   * @return les aretes deja utilis�es par les ouvrages
   */
  public int[] getUsedEdge() {
    final TIntHashSet res = new TIntHashSet();
    for (int i = getNbOuvrage() - 1; i >= 0; i--) {
      final H2dRubarOuvrage ouv = getRubarOuvrage(i);
      res.add(ouv.getArete1());
      if (ouv.isElt2Set()) {
        res.add(ouv.getArete2());
      }
    }
    return res.size() == 0 ? FuEmptyArrays.INT0 : res.toArray();
  }

  /**
   * @param _i l'indice de l'ouvrage [0,getNbOuvrage()[
   * @return l'ouvrage demande
   */
  public H2dRubarOuvrage getRubarOuvrage(final int _i) {
    return (H2dRubarOuvrage) ouv_.getValueAt(_i);
  }

  public boolean isInitialized() {
    return grid_ != null;
  }

  protected void setGrid(final H2dRubarGridAreteSource _s) {
    if (_s != grid_) {
      grid_ = _s;
      ouv_ = null;
    }
  }

  /**
   * @param _container le conteneur d'ouvrage utilise pour l'initialisation
   * @param _analyze le receveur d'info
   */
  public void initWith(final H2dRubarOuvrageContainer _container, final CtuluAnalyze _analyze) {
    if (ouv_ != null) {
      ouv_ = null;
    }
    if (_container != null) {
      final int nb = _container.getNbOuvrage();
      final List l = new ArrayList(nb);
      boolean erreur = false;
      final TIntArrayList listOuvrageWithBadCoordinates = new TIntArrayList();
      final TIntArrayList listOuvrageInErreur = new TIntArrayList();
      for (int i = 0; i < nb; i++) {
        final H2dRubarOuvrage o = H2dRubarOuvrage.buildOuvrage(_container.getOuvrage(i), this, _analyze, i,
            listOuvrageWithBadCoordinates);
        if (o == null) {
          listOuvrageInErreur.add(i);
          erreur = true;
        } else {
          l.add(o);
        }
      }
      if (erreur && !_analyze.containsFatalError()) {
        _analyze.addFatalError(H2dResource.getS("Des ouvrages ont �t� ignor�es"));
        int[] ouvrageIgnored = listOuvrageInErreur.toNativeArray();
        for (int i = 0; i < ouvrageIgnored.length; i++) {
          _analyze.addWarn(H2dResource.getS("Index de l'ouvrage ignor�: {0}", CtuluLibString.getString(ouvrageIgnored[i] + 1)));
        }
      } else if (listOuvrageWithBadCoordinates.size() > 0) {
        _analyze.addError(
            H2dResource.getS(
                "Des centres de mailles et/ou d'ar�tes ont de mauvaises coordonn�es (dist>0.1).\nLes mailles (ar�tes) les plus proches ont �t� utilis�es.\nNombre d'ouvrage(s) concern�(s): {0}.",
                CtuluLibString.getString(listOuvrageWithBadCoordinates.size())), -1);
        int[] ouvrageWithBadCoordinate = listOuvrageWithBadCoordinates.toNativeArray();
        for (int i = 0; i < ouvrageWithBadCoordinate.length; i++) {
          _analyze.addWarn(H2dResource.getS("Index de l'ouvrage corrig�: {0}", CtuluLibString.getString(ouvrageWithBadCoordinate[i] + 1)));
        }
        FuLog.warning("H2D: ouvrages contenant coordonn�es fausses:" + listOuvrageWithBadCoordinates.toString());
      }
      createModel(l);
    }
  }

  /**
   * @return true si vide
   */
  public boolean isEmpty() {
    return getNbOuvrage() == 0;
  }

  /**
   * @param _l le listener a enlever
   */
  public void removeListener(final H2dRubarOuvrageListener _l) {
    if (listener_ != null) {
      listener_.remove(_l);
    }
  }

  /**
   * @param _i l'indice de l'ouvrage a enlever
   * @param _cmd le receveur de commandes
   */
  public void removeOuvrage(final int[] _i, final CtuluCommandContainer _cmd) {
    if (ouv_ == null) {
      return;
    }
    ouv_.remove(_i, _cmd);
  }
}
