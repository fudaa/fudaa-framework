/*
 * @creation 1 juin 2004
 * @modification $Date: 2006-09-19 14:43:24 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: H2dTelemacBcParameterSedimentEvol.java,v 1.8 2006-09-19 14:43:24 deniger Exp $
 */
public class H2dTelemacBcParameterSedimentEvol extends H2dTelemacBcParameterConcentration {

  public H2dTelemacBcParameterSedimentEvol() {
    super();
  }

  @Override
  public H2dVariableType getVariable() {
    return H2dVariableTransType.EVOLUTION_SEDIMENTAIRE;
  }

  @Override
  public boolean isFixedByUser() {
    return false;
  }

  public boolean isValueTransientFor(final H2dTelemacBoundary _b) {
    return false;
  }

  @Override
  public boolean isKeywordImposed() {
    return false;
  }

  @Override
  public boolean hasToBeActivatedForBoundary() {
    return false;
  }

  @Override
  public boolean isValueFixedFor(final H2dTelemacBcManager _p, final H2dTelemacBoundary _b) {
    return false;
  }
}