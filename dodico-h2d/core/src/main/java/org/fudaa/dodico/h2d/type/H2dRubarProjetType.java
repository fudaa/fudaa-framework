/*
 * @creation 26 juin 2003
 * @modification $Date: 2007-04-30 14:21:37 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import org.fudaa.dodico.commun.DodicoEnumType;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.rubar.H2DRubarDicoModelTransport;
import org.fudaa.dodico.h2d.rubar.H2dRubarDicoModel;

/**
 * @author deniger
 * @version $Id: H2dRubarProjetType.java,v 1.14 2007-04-30 14:21:37 deniger Exp $
 */
public abstract class H2dRubarProjetType extends DodicoEnumType {
  /**
   * Type couranto 2D.
   */
  public static final H2dRubarProjetType COURANTOLOGIE_2D = new H2dRubarProjetType(H2dResource.getS("Courantologie 2D"), false) {
    @Override
    public H2dRubarDicoModel createDicoModel(int nbTransportBlock) {
      return new H2dRubarDicoModel();
    }
  };
  /**
   * Type transport.
   */
  public static final H2dRubarProjetType TRANSPORT = new H2dRubarProjetType(H2dResource.getS("Transport"), true) {
    @Override
    public H2dRubarDicoModel createDicoModel(int nbTransportBlock) {
      return new H2DRubarDicoModelTransport(nbTransportBlock);
    }
  };

  public final static H2dRubarProjetType[] getConstantArray() {
    return new H2dRubarProjetType[]{COURANTOLOGIE_2D, TRANSPORT};
  }

  private final boolean transportType;

  /**
   * @param _nom le nom
   */
  H2dRubarProjetType(final String _nom, boolean transport) {
    super(_nom);
    this.transportType = transport;
  }

  H2dRubarProjetType() {
    this(null, false);
  }

  public abstract H2dRubarDicoModel createDicoModel(int nbTransportBlock);

  public boolean isTransportType() {
    return transportType;
  }

  public DodicoEnumType[] getArray() {
    return getConstantArray();
  }
}
