/*
 * @creation 3 d�c. 2004
 * @modification $Date: 2007-01-19 13:07:21 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.locationtech.jts.geom.Envelope;
import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntArrayList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.collection.CtuluListDouble;
import org.fudaa.ctulu.collection.CtuluListObject;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfFrontier;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: H2dTelemacSeuilMng.java,v 1.16 2007-01-19 13:07:21 deniger Exp $
 */
public class H2dTelemacSeuilMng implements H2dTelemacSeuilContainer {

  protected class SeuilModel extends CtuluListObject {

    public SeuilModel() {
      super();
    }

    public SeuilModel(final Collection _init) {
      super(_init);
    }

    public SeuilModel(final CtuluListObject _init) {
      super(_init);
    }

    public SeuilModel(final int _nb) {
      super(_nb);
    }

    @Override
    protected void fireObjectModified(int _idx, Object _geom) {
      p_.setValue(nbSeuilKw_, CtuluLibString.getString(getNbSeuil()));
      H2dTelemacSeuilMng.this.fireSeuilAdded();
    }

    @Override
    protected void fireObjectAdded(int _newIdx, Object _newGeom) {
      p_.setValue(nbSeuilKw_, CtuluLibString.getString(getNbSeuil()));
      H2dTelemacSeuilMng.this.fireSeuilAdded();
    }

    @Override
    protected void fireObjectRemoved(int _oldIdx, Object _oldGeom) {
      p_.setValue(nbSeuilKw_, CtuluLibString.getString(getNbSeuil()));
      H2dTelemacSeuilMng.this.fireSeuilRemoved();
    }
  }

  private Map seuilMinMax_;

  EfGridInterface grid_;

  List listener_;

  DicoEntite nbSeuilKw_;

  DicoParams p_;

  CtuluListObject seuils_;

  int vitTan_;

  /**
   * @param _g le maillage : non null.
   */
  public H2dTelemacSeuilMng(final EfGridInterface _g, final H2dTelemacDicoParams _params) {
    grid_ = _g;
    if (grid_ == null) {
      throw new IllegalArgumentException("grid must not be null");
    }
    p_ = _params;
    nbSeuilKw_ = _params.getTelemacDicoFileFormatVersion().getSeuilNombreMotCle();
    nbSeuilKw_.setModifiable(false);
  }

  protected void addWarn(final int _seuilIdx, final boolean _test2, final CtuluAnalyze _analyze) {
    if (_analyze == null) {
      return;
    }
    final StringBuffer b = new StringBuffer(H2dResource.getS("Seuil"));
    b.append(CtuluLibString.ESPACE).append(_seuilIdx + 1).append(':');
    b.append(H2dResource.getS("Les indices de la c�te {0} ne respectent pas l'ordre de la fronti�re",
        _test2 ? CtuluLibString.DEUX : CtuluLibString.UN));
    _analyze.addWarn(b.toString(), -1);
  }

  protected void createSeuil() {
    if (seuils_ == null) {
      seuils_ = new SeuilModel(10);
    }
  }

  protected void createSeuil(final List _initList) {
    if (seuils_ == null) {
      seuils_ = new SeuilModel(_initList);
    }
  }

  public double[] getCurvAbs(final H2dTelemacSeuil _seuil) {
    return _seuil.getCurvIndexes(grid_);
  }

  protected void fireSeuilAdded() {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dTelemacSeuilListener) listener_.get(i)).seuilAdded();
      }
    }

  }

  protected void fireSeuilChanged(final boolean _b) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dTelemacSeuilListener) listener_.get(i)).seuilChanged(_b);
      }
    }

  }

  protected void fireSeuilRemoved() {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dTelemacSeuilListener) listener_.get(i)).seuilRemoved();
      }
    }

  }

  /**
   * @param _idxSeuil l'indice du seuil
   * @param _idxPt l'indice du point a modifier
   * @param _newValue la nouvelle valeur pour la cote du seuil
   * @param _cmd le receveur de commandes
   * @return true si modif
   */
  public boolean modifySeuilCote(final int _idxSeuil, final int _idxPt, final double _newValue, final CtuluCommandContainer _cmd) {
    final H2dTelemacSeuil s = getTelemacSeuil(_idxSeuil);
    return s.cotes_.set(_idxPt, _newValue, _cmd);
  }

  /**
   * @param _s le seuil a tester
   * @return l'indice du seuil dans ce manager. -1 si non contenu
   */
  public int containsSeuil(final H2dTelemacSeuil _s) {
    return seuils_ == null ? -1 : seuils_.indexOf(_s);
  }

  /**
   * @param _idxSeuil l'indice du seuil
   * @param _idxPt l'indice du point a modifier
   * @param _newValue la nouvelle valeur pour la cote du seuil
   * @param _cmd le receveur de commandes
   * @return true si modif
   */
  public boolean modifySeuilCote(final int _idxSeuil, final int[] _idxPt, final double[] _newValue, final CtuluCommandContainer _cmd) {
    final H2dTelemacSeuil s = getTelemacSeuil(_idxSeuil);
    return s.cotes_.set(_idxPt, _newValue, _cmd);
  }

  protected CtuluListDouble createDoubleModel(final int _nbPt) {
    return new SeuilItemModel(_nbPt, _nbPt + 5);
  }

  protected CtuluListDouble createDoubleModel(final double[] _v) {
    return new SeuilItemModel(_v);
  }

  private class SeuilItemModel extends CtuluListDouble {

    @Override
    protected void fireObjectChanged(int _indexGeom, Object _newValue) {
      H2dTelemacSeuilMng.this.fireSeuilChanged(false);
    }

    /**
     *
     */
    public SeuilItemModel() {
      super();
    }

    /**
     * @param _init
     */
    public SeuilItemModel(final CtuluListDouble _init) {
      super(_init);
    }

    /**
     * @param _init
     */
    public SeuilItemModel(final double[] _init) {
      super(_init);
    }

    /**
     * @param _nb
     * @param _initCapacity
     */
    public SeuilItemModel(final int _nb, final int _initCapacity) {
      super(_nb, _initCapacity);
    }

    /**
     * @param _init
     */
    public SeuilItemModel(final TDoubleArrayList _init) {
      super(_init);
    }
  }

  /**
   * @param _idxSeuil l'indice du seuil
   * @param _idxPt l'indice du point a modifier
   * @param _newValue la nouvelle valeur pour le debit
   * @param _cmd le receveur de commandes
   * @return true si modif
   */
  public boolean modifySeuilDebit(final int _idxSeuil, final int _idxPt, final double _newValue, final CtuluCommandContainer _cmd) {
    if (_newValue < 0) {
      return false;
    }
    final H2dTelemacSeuil s = getTelemacSeuil(_idxSeuil);

    return s.debit_.set(_idxPt, _newValue, _cmd);

  }

  /**
   * @param _idxSeuil l'indice du seuil
   * @param _idxPt les indices des points a modifier
   * @param _newValue les nouvelles valeurs pour le debit
   * @param _cmd le receveur de commandes
   * @return true si modif
   */
  public boolean modifySeuilDebit(final int _idxSeuil, final int[] _idxPt, final double[] _newValue, final CtuluCommandContainer _cmd) {
    for (int i = _newValue.length; i >= 0; i--) {
      if (_newValue[i] < 0) {
        _newValue[i] = 0;
      }
    }
    final H2dTelemacSeuil s = getTelemacSeuil(_idxSeuil);
    return s.debit_.set(_idxPt, _newValue, _cmd);

  }

  /**
   * @param _i l'indice du seuil
   * @return le seuil version telemacou
   */
  public H2dTelemacSeuil getTelemacSeuil(final int _i) {
    return (H2dTelemacSeuil) seuils_.getValueAt(_i);
  }

  /**
   * @param _s le seuil a tester
   * @param _test2 true si test 2
   * @param _analyze l'analyze :peut-etre nulle
   * @param _seuil l'indice du seuil: utilise uniquement pour remplir les warnings
   * @return false si invalide
   */
  protected boolean isIdxValide(final H2dTelemacSeuilInterface _s, final boolean _test2, final CtuluAnalyze _analyze, final int _seuil) {
    // le premier indice
    int first = _test2 ? _s.getPtIdx2(0) : _s.getPtIdx1(0);
    // le deuxieme
    final int deuze = _test2 ? _s.getPtIdx2(1) : _s.getPtIdx1(1);
    final EfFrontierInterface fr = grid_.getFrontiers();
    final int[] idxFrIdxOnFr = new int[2];
    // iterateur sur la frontiere
    fr.getIdxFrontierIdxPtInFrontierFromFrontier(first, idxFrIdxOnFr);
    final EfFrontier.FrontierIterator it = fr.getFrontierIterator(idxFrIdxOnFr[0], idxFrIdxOnFr[1]);
    it.next();
    final int secIdx = it.getGlobalFrIdx();
    // sens inverse du bord
    if (secIdx != deuze) {
      it.setReverse(true);
      // on revient au point de depart
      it.next();
      // le point suivant
      it.next();
      if (it.getGlobalFrIdx() != deuze) {
        if (CtuluLibMessage.DEBUG) {
          CtuluLibMessage.debug("Second index wrong");
        }
        addWarn(_seuil, _test2, _analyze);
        return false;
      }
    }
    final int nb = _s.getNbPoint();
    for (int i = 2; i < nb; i++) {
      it.next();
      first = _test2 ? _s.getPtIdx2(i) : _s.getPtIdx1(i);
      if (it.getGlobalFrIdx() != first) {
        if (CtuluLibMessage.DEBUG) {
          CtuluLibMessage.debug("index " + i + " wrong expected " + it.getGlobalFrIdx()
              + " read " + first);
        }
        addWarn(_seuil, _test2, _analyze);
        return false;
      }
    }
    return true;

  }

  /**
   * Ne fait pas de controle pour savoir si le listener est deja contenu.
   *
   * @param _l le listener a ajouter
   */
  public void addListener(final H2dTelemacSeuilListener _l) {
    if (listener_ == null) {
      listener_ = new ArrayList(10);
    }
    listener_.add(_l);
  }

  /**
   * @param _l le listener a tester
   * @return true si contenu
   */
  public boolean containsListener(final H2dTelemacSeuilListener _l) {
    return listener_ != null && listener_.contains(_l);
  }

  /**
   * @return le maillage utilise.
   */
  public final EfGridInterface getGrid() {
    return grid_;
  }

  public Envelope getMinMax() {
    final Envelope e = getMinMaxForSeuil(0);
    for (int i = getNbSeuil() - 1; i >= 0; i--) {
      e.expandToInclude(getMinMaxForSeuil(i));
    }
    return e;
  }

  public Envelope getMinMaxForSeuil(final int _idxSeuil) {
    final H2dTelemacSeuil seuil = getTelemacSeuil(_idxSeuil);
    if (seuilMinMax_ != null && seuilMinMax_.containsKey(seuil)) {
      return (Envelope) seuilMinMax_.get(seuil);
    }
    final Envelope e = getTelemacSeuil(_idxSeuil).getMinMaxPt(grid_);
    if (seuilMinMax_ == null) {
      seuilMinMax_ = new HashMap();
    }
    seuilMinMax_.put(seuil, e);
    return e;
  }

  /**
   * @param _idx les indices des seuils a enlever
   * @param _cmd le receveur de commande
   */
  public void removeSeuil(final int[] _idx, final CtuluCommandContainer _cmd) {
    if (seuils_ != null) {
      seuils_.remove(_idx, _cmd);
    }
  }

  @Override
  public int getNbSeuil() {
    return seuils_ == null ? 0 : seuils_.getSize();
  }

  @Override
  public H2dTelemacSeuilInterface getSeuil(final int _i) {
    return (H2dTelemacSeuilInterface) seuils_.getValueAt(_i);
  }

  @Override
  public int getSeuilVitesseTangentielle() {
    return vitTan_;
  }

  /**
   * @param _frIdx l'indice a tester
   * @return true si un seuil utilise deja cet indice de frontiere
   */
  public boolean isUsed(final int _frIdx) {
    for (int i = getNbSeuil() - 1; i >= 0; i--) {
      if (getTelemacSeuil(i).isUsedIdx(_frIdx)) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param _idxSeuils les indices des seuils a tester
   * @return la valeurs commune pour la cote pour l'ensembre. null si pas de valeurs communes
   */
  public Double getCoteCommonValue(final int[] _idxSeuils) {
    if (_idxSeuils == null || _idxSeuils.length == 0) {
      return null;
    }
    final Double r = getTelemacSeuil(_idxSeuils[0]).getCommonCoefficientCote();
    if (r != null) {
      for (int i = _idxSeuils.length - 1; i > 0; i--) {
        final Double r2 = getTelemacSeuil(_idxSeuils[i]).getCommonCoefficientCote();
        if ((r2 == null) || (r2.doubleValue() != r.doubleValue())) {
          return null;
        }
      }
    }
    return r;
  }

  public void setValueForDebit(final int[] _seuilIdx, final double _v, final CtuluCommandContainer _cmd) {
    if ((_seuilIdx == null) || (_seuilIdx.length == 0)) {
      return;
    }
    final CtuluCommandComposite cmd = new CtuluCommandComposite() {

      @Override
      protected void actionToDoAfterUndoOrRedo() {
        fireSeuilChanged(false);
      }

      @Override
      protected boolean isActionToDoAfterUndoOrRedo() {
        return true;
      }
    };
    boolean change = false;
    for (int i = _seuilIdx.length - 1; i >= 0; i--) {
      if (getTelemacSeuil(_seuilIdx[i]).setValueForDebit(_v, cmd)) {
        change = true;
      }
    }

    if (change) {
      if (_cmd != null && cmd.getNbCmd() > 0) {
        _cmd.addCmd(cmd);
      }
      fireSeuilChanged(false);
    }
  }

  public void setValueForCote(final int[] _seuilIdx, final double _v, final CtuluCommandContainer _cmd) {
    if ((_seuilIdx == null) || (_seuilIdx.length == 0)) {
      return;
    }
    final CtuluCommandComposite cmd = new CtuluCommandComposite() {

      @Override
      protected void actionToDoAfterUndoOrRedo() {
        fireSeuilChanged(false);
      }

      @Override
      protected boolean isActionToDoAfterUndoOrRedo() {
        return true;
      }
    };
    boolean change = false;
    for (int i = _seuilIdx.length - 1; i >= 0; i--) {
      if (getTelemacSeuil(_seuilIdx[i]).setValueForCote(_v, cmd)) {
        change = true;
      }
    }
    if (change) {
      if (_cmd != null && cmd.getNbCmd() > 0) {
        _cmd.addCmd(cmd);
      }
      fireSeuilChanged(false);
    }
  }

  /**
   * @param _idxSeuils les indices des seuils a tester
   * @return la valeurs commune pour le debit pour l'ensembre. null si pas de valeurs communes
   */
  public Double getCoefDebitCommonValue(final int[] _idxSeuils) {
    if (_idxSeuils == null || _idxSeuils.length == 0) {
      return null;
    }
    final Double r = getTelemacSeuil(_idxSeuils[0]).getCommonCoefficientDebit();
    if (r != null) {
      for (int i = _idxSeuils.length - 1; i > 0; i--) {
        final Double r2 = getTelemacSeuil(_idxSeuils[i]).getCommonCoefficientDebit();
        if ((r2 == null) || (r2.doubleValue() != r.doubleValue())) {
          return null;
        }
      }
    }
    return r;
  }

  public void addSeuil(final int[] _c1, final int[] _c2, final CtuluCommandContainer _cmd) {
    final H2dTelemacSeuil s = new H2dTelemacSeuil(this, _c1, _c2, 0, 0.4);
    if (isIdxValide(s, true, null, 1)) {
      s.setIdxValid(isIdxValide(s, false, null, 1));
    } else {
      s.setIdxValid(false);
    }
    if (seuils_ == null) {
      seuils_ = new SeuilModel();
    }
    seuils_.addObject(s, _cmd);
  }

  /**
   * @param _seuils les nouveaux seuils
   * @param _an le receveur des messages
   */
  public void loadSeuil(final H2dTelemacSeuilContainer _seuils, final CtuluAnalyze _an) {
    if (_seuils == null) {
      return;
    }
    if (seuils_ != null) {
      new Throwable("weirs already initialized").printStackTrace();
    }

    final int nbSeuil = _seuils.getNbSeuil();
    TIntArrayList l = new TIntArrayList();
    final List seuilAjoute = new ArrayList();
    for (int i = 0; i < nbSeuil; i++) {
      final H2dTelemacSeuilInterface s = _seuils.getSeuil(i);
      // on test les indices de la cote 1.
      boolean valide = isIdxValide(s, false, _an, i);
      if (valide) {
        // on test les indices de la cote 2 seulement si necessaire.
        valide = isIdxValide(s, true, _an, i);
      }
      // si non valide
      if (!valide) {
        l.add(i + 1);
      }
      final H2dTelemacSeuil seuil = new H2dTelemacSeuil(this, s);
      seuil.setIdxValid(valide);
      seuilAjoute.add(seuil);
    }
    // des seuil sont erron�es
    if (l.size() > 0) {
      CtuluLibMessage.info("bad weirs " + CtuluLibString.arrayToString(l.toNativeArray()));
    }
    l = null;
    // on vide
    seuils_ = null;
    createSeuil(seuilAjoute);
    CtuluLibMessage.info("number of weirs " + getNbSeuil());
    p_.setValue(nbSeuilKw_, CtuluLibString.getString(getNbSeuil()));
  }

  /**
   * @return les 2 options pour la vitesse tangentielles
   */
  public String[] getOptionsForVelocity() {
    return new String[] { H2dResource.getS("Vitesses nulles"),
        H2dResource.getS("Vitesses calcul�es avec la formule de Ch�zy") };
  }

  /**
   * @param _newVal la nouvelle valeur pour la relaxation
   * @param _cmd le receveur de commande
   */
  public void modifyVitesseTangentielles(final int _newVal, final CtuluCommandContainer _cmd) {

    if (_newVal != vitTan_) {
      if ((_newVal != 0) && (_newVal != 1)) {
        return;
      }
      final int oldValue = vitTan_;
      vitTan_ = _newVal;
      if (_cmd != null) {
        _cmd.addCmd(new CtuluCommand() {

          @Override
          public void redo() {
            vitTan_ = _newVal;
            fireSeuilChanged(false);
          }

          @Override
          public void undo() {
            vitTan_ = oldValue;
            fireSeuilChanged(false);
          }
        });
      }
      fireSeuilChanged(false);
    }
  }

  /**
   * @param _l le listener a enlever
   * @return true si enleve
   */
  public boolean removeListener(final H2dTelemacSeuilListener _l) {
    if (listener_ != null) {
      return listener_.remove(_l);
    }
    return false;
  }

}
