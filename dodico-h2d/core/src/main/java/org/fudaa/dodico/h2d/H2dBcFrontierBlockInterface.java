/**
 * @creation 9 sept. 2003
 * @modification $Date: 2006-04-19 13:19:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import java.util.Set;

/**
 * Classe decrivant des frontieres contenant des bords par "block" (H2dBoundary):
 * des points adjacents.(RefluxSolutionSequentielResult).
 * @author deniger
 * @version $Id: H2dBcFrontierBlockInterface.java,v 1.8 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dBcFrontierBlockInterface extends H2dBcFrontierInterface {

  /**
   * @return the number of boundaries belonging to this frontier
   */
  int getNbBord();

  /**
   * @param _idxBord l'indice du bord dans la frontiere
   * @return the boundary wich index is _idxBord
   */
  H2dBoundary getBord(int _idxBord);

  /**
   * @param _s la liste a remplir avec les bords utilises
   */
  void fillWithUsedBoundaryType(Set _s);

  /**
   * @param _indexOnThisFrontier l'indice de frontiere du point a rechercher
   * @return the first bord (in boundary numbering) containing the index _indexOnThisFrontier
   */
  H2dBoundary getBordContainingIdx(int _indexOnThisFrontier);
}