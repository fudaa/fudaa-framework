/*
 *  @creation     10 janv. 2005
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: H2dTelemacNodalPropListener.java,v 1.5 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dTelemacNodalPropListener {

  void nodalPropChanged(H2dVariableType _t);

  void nodalPropAdded(H2dVariableType _t);

  void nodalPropRemoved(H2dVariableType _t);

}
