/*
 * @creation 28 sept. 2004
 * @modification $Date: 2006-04-19 13:19:38 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarDonneesBrutesListener.java,v 1.8 2006-04-19 13:19:38 deniger Exp $
 */
public interface H2dRubarDonneesBrutesListener {

  void donneesBrutesTypeChanged(H2dRubarDonneesBrutes _source);

  void donneesBrutesDataChanged(H2dRubarDonneesBrutes _source);

  void donneesBrutesDataNombreChanged(H2dRubarDonneesBrutes _source);

  void donneesBrutesSupportAddedOrRemoved(H2dRubarDonneesBrutes _source);

  void donneesBrutesSupportChanged(H2dRubarDonneesBrutes _source);

  void donneesBrutesNuageDataChanged(H2dRubarDonneesBrutes _source);

  void donneesBrutesNuageDataNombreChanged(H2dRubarDonneesBrutes _source);

  void donneesBrutesNuageSupportAddedOrRemoved(H2dRubarDonneesBrutes _source);

  void donneesBrutesNuageSupportChanged(H2dRubarDonneesBrutes _source);

}