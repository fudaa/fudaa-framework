/*
 * @creation 27 nov. 2003
 * @modification $Date: 2007-06-14 11:59:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import gnu.trove.TIntDoubleHashMap;
import gnu.trove.TIntDoubleIterator;
import gnu.trove.TIntHashSet;
import gnu.trove.TIntIterator;
import gnu.trove.TIntObjectHashMap;
import gnu.trove.TIntObjectIterator;
import gnu.trove.TIntObjectProcedure;
import java.util.Map;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluParser;
import org.fudaa.ctulu.CtuluTroveProcedure;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEditAbstract;
import org.fudaa.dodico.h2d.H2dEvolutionUseCounter;
import org.fudaa.dodico.h2d.H2dNodalPropertyMixte;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.nfunk.jep.Variable;

/**
 * @author deniger
 * @version $Id: H2dRefluxElementProperty.java,v 1.24 2007-06-14 11:59:08 deniger Exp $
 */
public class H2dRefluxElementProperty extends H2dNodalPropertyMixte {

  class RefluxMemento {

    TIntDoubleHashMap permanentToAdd_;
    TIntHashSet permanentToRemove_;

    TIntObjectHashMap transitoireToAdd_;
    TIntHashSet transitoireToRemove_;

    protected void undo() {
      boolean modifie = false;
      EvolutionReguliereAbstract evol;
      if (transitoireToRemove_ != null) {
        modifie = true;
        final TIntObjectHashMap target = getIndiceTransitoireEvolution();
        final TIntIterator it = transitoireToRemove_.iterator();
        for (int i = transitoireToRemove_.size(); i-- > 0;) {
          evol = (EvolutionReguliereAbstract) target.remove(it.next());
          evol.setUnUsed(false);
        }
      }
      if (permanentToRemove_ != null) {
        modifie = true;
        final TIntDoubleHashMap target = getIndicePermanentValeur();
        final TIntIterator it = permanentToRemove_.iterator();
        for (int i = permanentToRemove_.size(); i-- > 0;) {
          target.remove(it.next());
        }
      }
      if (permanentToAdd_ != null) {
        modifie = true;
        final TIntDoubleHashMap target = getIndicePermanentValeur();
        final TIntDoubleIterator it = permanentToAdd_.iterator();
        for (int i = permanentToAdd_.size(); i-- > 0;) {
          it.advance();
          target.put(it.key(), it.value());
        }
      }
      if (transitoireToAdd_ != null) {
        modifie = true;
        final TIntObjectHashMap target = getIndiceTransitoireEvolution();
        final TIntObjectIterator it = transitoireToAdd_.iterator();
        for (int i = transitoireToAdd_.size(); i-- > 0;) {
          it.advance();
          evol = (EvolutionReguliereAbstract) it.value();
          evol.setUsed(false);
          target.put(it.key(), evol);
        }
      }
      if (modifie) {
        fireEvent();
      }
    }

    /**
     * @return true si ce memento est vide
     */
    public boolean isEmpty() {
      return (transitoireToAdd_ == null) && (transitoireToRemove_ == null) && (permanentToAdd_ == null)
          && (permanentToRemove_ == null);
    }

    /**
     * @param _idxElt l'indice de l'element
     * @param _old l'ancienne valeur qui sera a ajouter
     * @param _nbTotalElt le nombre total d'elements qui seront modifies(optimisation afin de construire un tableau avec
     *          la bonne)
     */
    public void saveToAddPermanent(final int _idxElt, final double _old, final int _nbTotalElt) {
      if (permanentToAdd_ == null) {
        permanentToAdd_ = new TIntDoubleHashMap(_nbTotalElt);
      }
      permanentToAdd_.put(_idxElt, _old);
    }

    /**
     * @param _idxElt l'indice de l'element
     * @param _oldEvolToAdd l'ancienne valeur qui sera a ajouter
     * @param _nbTotalElt le nombre total d'elements qui seront modifies(optimisation afin de construire un tableau avec
     *          la bonne)
     */
    public void saveToAddTransitoire(final int _idxElt, final Object _oldEvolToAdd, final int _nbTotalElt) {
      if (_oldEvolToAdd == null) {
        new Throwable().printStackTrace();
      }
      if (transitoireToAdd_ == null) {
        transitoireToAdd_ = new TIntObjectHashMap(_nbTotalElt);
      }
      transitoireToAdd_.put(_idxElt, _oldEvolToAdd);
    }

    /**
     * @param _idxElt l'indice de l'element qui sera a enlever des permanents
     * @param _nbTotalElt le nombre total d'elements qui seront modifies(optimisation afin de construire un tableau avec
     *          la bonne)
     */
    public void saveToRemovePermanent(final int _idxElt, final int _nbTotalElt) {
      if (permanentToRemove_ == null) {
        permanentToRemove_ = new TIntHashSet(_nbTotalElt);
      }
      permanentToRemove_.add(_idxElt);
    }

    /**
     * @param _idxElt l'indice de l'element qui sera a enlever des transitoires
     * @param _nbTotalElt le nombre total d'elements qui seront modifies(optimisation afin de construire un tableau avec
     *          la bonne)
     */
    public void saveToRemoveTransitoire(final int _idxElt, final int _nbTotalElt) {
      if (transitoireToRemove_ == null) {
        transitoireToRemove_ = new TIntHashSet(_nbTotalElt);
      }
      transitoireToRemove_.add(_idxElt);
    }
  }

  /**
   * @param _t la variable
   * @return la prop elementaires correspondantes a la variable
   */
  public static H2dRefluxElementProperty getElementProperty(final H2dVariableType _t, final int _nbElt) {
    return getElementProperty(_t, null, _nbElt);
  }

  /**
   * @param _t la variable
   * @param _m la propriete qui servira pour initialiser la propriete (peut etre null)
   * @return la prop elementaires correspondantes a la variable
   */
  public static H2dRefluxElementProperty getElementProperty(final H2dVariableType _t, final H2dNodalPropertyMixte _m,
      final int _nbElt) {
    H2dRefluxElementProperty r = null;
    if ((_t == H2dVariableType.VISCOSITE) || (_t == H2dVariableType.RUGOSITE)) {
      r = _m == null ? new H2dRefluxElementImposedProperty(_t.getName(), _nbElt) : new H2dRefluxElementImposedProperty(
          _t.getName(), _nbElt);
    } else {
      r = _m == null ? new H2dRefluxElementProperty(_t.getName(), _nbElt, 0) : new H2dRefluxElementProperty(_t
          .getName(), _nbElt);
    }
    if (_m != null) {
      r.initDataFrom(_m);
    }
    return r;
  }

  int nbElt_;

  H2dRefluxElementProperty(final H2dNodalPropertyMixte _m, int _nbElt) {
    super(_m.getNom());
    super.initDataFrom(_m);
    nbElt_ = _nbElt;

  }

  /**
   * NE fait rien.
   */
  public H2dRefluxElementProperty(final String _nom, int _nbElt) {
    super(_nom, null, null);
    nbElt_ = _nbElt;
  }

  public H2dRefluxElementProperty(final String _nom, final int _nbElt, final double _defaultVal) {
    super(_nom);
    final int[] idx = new int[_nbElt];
    for (int i = 0; i < idx.length; i++) {
      idx[i] = i;
    }
    final H2dRefluxValue val = new H2dRefluxValue();
    val.setType(H2dBcType.PERMANENT);
    val.setValue(_defaultVal);
    setValue(idx, val);
  }

  /**
   * Initialise l'utilisation des evolutions.
   * 
   * @param _nom le nom de la prop
   * @param _indiceFixeValeur les indices ayant des valeurs fixes
   * @param _indiceTransitoireEvolution les indices ayant des comportement transitoires
   */
  public H2dRefluxElementProperty(final String _nom, final TIntDoubleHashMap _indiceFixeValeur,
      final TIntObjectHashMap _indiceTransitoireEvolution) {
    super(_nom, _indiceFixeValeur, _indiceTransitoireEvolution);
    initEvolutionUsed(_indiceTransitoireEvolution);
  }

  /**
   * Initialise l'utilisation des evolutions.
   * 
   * @param _indiceFixeValeur les indices ayant des valeurs fixes
   * @param _indiceTransitoireEvolution les indices ayant des comportement transitoires
   */
  public H2dRefluxElementProperty(final TIntDoubleHashMap _indiceFixeValeur,
      final TIntObjectHashMap _indiceTransitoireEvolution) {
    super(_indiceFixeValeur, _indiceTransitoireEvolution);
    initEvolutionUsed(_indiceTransitoireEvolution);
  }

  private void initEvolutionUsed(final TIntObjectHashMap _indiceEvol) {
    if (_indiceEvol != null) {
      final Object[] o = _indiceEvol.getValues();
      for (int i = o.length - 1; i >= 0; i--) {
        ((EvolutionReguliereAbstract) o[i]).setUsed(false);
      }
    }

  }

  /**
   * redefinie pour augmenter la visibilité.
   */
  @Override
  protected void fireEvent() {
    super.fireEvent();
  }

  /**
   * redefinie pour augmenter la visibilité.
   */
  protected TIntObjectHashMap getIndiceTransitoireEvolution() {
    return indiceTransitoireEvolution_;
  }

  protected void replaceEvol(final Map _evolEquivEvol) {
    if (indiceTransitoireEvolution_ != null) {
      final TIntObjectIterator intObject = indiceTransitoireEvolution_.iterator();
      for (int i = indiceTransitoireEvolution_.size(); i-- > 0;) {
        intObject.advance();
        final EvolutionReguliereAbstract newE = (EvolutionReguliereAbstract) _evolEquivEvol.get(intObject.value());
        if (newE != null) {
          newE.setUsed(false);
          indiceTransitoireEvolution_.put(intObject.key(), newE);
        }
      }

    }
  }

  protected void setNbElt(final int _i) {
    if (_i > 0) {
      nbElt_ = _i;
    }
  }

  protected final CtuluCommand setValue(final int[] _idx, final H2dRefluxValue _v) {
    final RefluxMemento mem = setValueIntern(_idx, _v, true);
    if ((mem != null) && (!mem.isEmpty())) {
      return new CtuluCommand() {

        @Override
        public void redo() {
          setValueIntern(_idx, _v, false);
        }

        @Override
        public void undo() {
          mem.undo();
        }
      };
    }
    return null;
  }

  protected CtuluCommand setDoubleValues(final int[] _idx, final double[] _val, final boolean _canChangeTransient) {
    final RefluxMemento mem = setDoubleValueIntern(_idx, _val, _canChangeTransient, true);
    if ((mem != null) && (!mem.isEmpty())) {
      return new CtuluCommand() {

        @Override
        public void redo() {
          setDoubleValueIntern(_idx, _val, _canChangeTransient, false);
        }

        @Override
        public void undo() {
          mem.undo();
        }
      };
    }
    return null;
  }

  protected RefluxMemento setValueIntern(final int[] _idx, final H2dRefluxValue _v, final boolean _save) {
    if ((_idx == null) || (_idx.length == 0)) {
      return null;
    }
    if (_v.getType() == H2dBcType.MIXTE) {
      return null;
    }

    RefluxMemento r = null;
    if (_save) {
      r = new RefluxMemento();
    }
    boolean modified = false;
    final CtuluParser expr = _v.getOldExpr();
    Variable oldVar = null;
    if (expr != null) {
      oldVar = expr.getVar(CtuluParser.getOldVariable());
    }
    if (_v.getType() == H2dBcType.PERMANENT) {
      modified |= setValuePermanent(_idx, _v, _save, r, expr, oldVar);
    } else if (_v.getType() == H2dBcType.TRANSITOIRE) {
      modified |= setValueTransitoire(_idx, _v, _save, r);
    } else if (_v.getType() == H2dBcType.LIBRE) {
      modified |= setValueLibre(_idx, _save, r);
    }
    if (modified) {
      fireEvent();
    }
    return r;
  }

  private boolean setValuePermanent(final int[] _idx, final H2dRefluxValue _v, final boolean _save,
      final RefluxMemento _r, final CtuluParser _expr, final Variable _oldVar) {

    boolean modified = false;
    final int n = _idx.length;
    EvolutionReguliereAbstract evol;
    for (int i = n - 1; i >= 0; i--) {
      final int t = _idx[i];
      if ((indiceTransitoireEvolution_ != null) && (indiceTransitoireEvolution_.contains(t))) {
        if (indicePermanentValeur_ == null) {
          indicePermanentValeur_ = createIndicePermanent();
        }
        evol = (EvolutionReguliereAbstract) indiceTransitoireEvolution_.remove(t);
        evol.setUnUsed(false);
        double val = _v.getValue();
        // pour une evolution, on prend la premiere valeur pour évaluer la formule.
        if (_expr != null && _oldVar != null) {
          _oldVar.setValue(CtuluLib.getDouble(evol.getY(0)));
          val = _expr.getValue();
        }
        indicePermanentValeur_.put(t, val);
        modified = true;
        if (_save) {
          _r.saveToAddTransitoire(t, evol, n);
          _r.saveToRemovePermanent(t, n);
        }
      } else if ((indicePermanentValeur_ != null) && (indicePermanentValeur_.contains(t))) {
        final double old = indicePermanentValeur_.get(t);
        if (_oldVar != null || ((_v.isDoubleValueConstant()) && (_v.getValue() != old))) {
          double val = _v.getValue();
          if (_expr != null && _oldVar != null) {
            _oldVar.setValue(CtuluLib.getDouble(old));
            val = _expr.getValue();
          }
          indicePermanentValeur_.put(t, val);
          modified = true;
          if (_save) {
            _r.saveToAddPermanent(t, old, n);
          }
        }
      } else {
        if (indicePermanentValeur_ == null) {
          indicePermanentValeur_ = createIndicePermanent();
        }
        double val = _v.getValue();
        if (_expr != null && _oldVar != null) {
          _oldVar.setValue(CtuluLib.ZERO);
          val = _expr.getValue();
        }
        indicePermanentValeur_.put(t, val);
        modified = true;
        if (_save) {
          _r.saveToRemovePermanent(t, n);
        }
      }
    }
    return modified;
  }

  private TIntDoubleHashMap createIndicePermanent() {
    return new TIntDoubleHashMap(nbElt_ > 0 ? nbElt_ : 100);
  }

  private boolean setValueLibre(final int[] _idx, final boolean _save, final RefluxMemento _r) {

    boolean modified = false;
    final int n = _idx.length;
    EvolutionReguliereAbstract evol;
    for (int i = n - 1; i >= 0; i--) {
      final int t = _idx[i];
      if ((indicePermanentValeur_ != null) && (indicePermanentValeur_.contains(t))) {
        final double old = indicePermanentValeur_.remove(t);
        modified = true;
        if (_save) {
          _r.saveToAddPermanent(t, old, n);
        }
      } else if ((indiceTransitoireEvolution_ != null) && (indiceTransitoireEvolution_.contains(t))) {
        evol = (EvolutionReguliereAbstract) indiceTransitoireEvolution_.remove(t);
        evol.setUnUsed(false);
        modified = true;
        if (_save) {
          _r.saveToAddTransitoire(t, evol, n);
        }
      }
    }
    return modified;
  }

  boolean setValueTransitoire(final int[] _idx, final H2dRefluxValue _v, final boolean _save, final RefluxMemento _r) {
    EvolutionReguliereAbstract evol;
    boolean modified = false;
    final int n = _idx.length;
    for (int i = n - 1; i >= 0; i--) {
      final int t = _idx[i];
      if ((indicePermanentValeur_ != null) && (indicePermanentValeur_.contains(t))) {
        if (_v.isEvolutionFixed()) {
          final double old = indicePermanentValeur_.remove(t);
          if (indiceTransitoireEvolution_ == null) {
            indiceTransitoireEvolution_ = new TIntObjectHashMap(nbElt_ > 0 ? nbElt_ : 100);
          }
          indiceTransitoireEvolution_.put(t, _v.getEvolution());
          _v.getEvolution().setUsed(false);
          modified = true;
          if (_save) {
            _r.saveToAddPermanent(t, old, n);
            _r.saveToRemoveTransitoire(t, n);
          }
        } else {
          new Throwable().printStackTrace();
        }
      } else if ((indiceTransitoireEvolution_ != null) && (indiceTransitoireEvolution_.contains(t))) {
        if ((_v.isEvolutionFixed()) && (indiceTransitoireEvolution_.get(t) != _v.getEvolution())) {
          evol = (EvolutionReguliereAbstract) indiceTransitoireEvolution_.put(t, _v.getEvolution());
          _v.getEvolution().setUsed(false);
          evol.setUnUsed(false);
          modified = true;
          if (_save) {
            _r.saveToAddTransitoire(t, evol, n);
          }
        }
      } else if (_v.isEvolutionFixed()) {
        indiceTransitoireEvolution_.put(t, _v.getEvolution());
        _v.getEvolution().setUsed(false);
        modified = true;
        if (_save) {
          _r.saveToRemoveTransitoire(t, n);
        }
      }
    }
    return modified;
  }

  protected RefluxMemento setDoubleValueIntern(final int[] _idx, final double[] _vals,
      final boolean _canChangeTransient, final boolean _save) {
    if (CtuluLibArray.isEmpty(_idx) || _vals == null || _vals.length != _idx.length) {
      return null;
    }
    final int n = _idx.length;
    int t;
    RefluxMemento r = null;
    EvolutionReguliereAbstract evol;
    if (_save) {
      r = new RefluxMemento();
    }
    boolean modified = false;
    for (int i = n - 1; i >= 0; i--) {
      t = _idx[i];
      final double newVal = _vals[i];
      if ((indiceTransitoireEvolution_ != null) && (indiceTransitoireEvolution_.contains(t))) {
        // ne pas changer les elements transitoires.
        if (!_canChangeTransient) {
          continue;
        }
        if (indicePermanentValeur_ == null) {
          indicePermanentValeur_ = createIndicePermanent();
        }
        indicePermanentValeur_.put(t, newVal);
        evol = (EvolutionReguliereAbstract) indiceTransitoireEvolution_.remove(t);
        evol.setUnUsed(false);
        modified = true;
        if (_save && r != null) {
          r.saveToAddTransitoire(t, evol, n);
          r.saveToRemovePermanent(t, n);
        }
      } else if ((indicePermanentValeur_ != null) && (indicePermanentValeur_.contains(t))) {
        final double old = indicePermanentValeur_.get(t);
        if (newVal != old) {
          indicePermanentValeur_.put(t, newVal);
          modified = true;
          if (_save && r != null) {
            r.saveToAddPermanent(t, old, n);
          }
        }
      } else {
        if (indicePermanentValeur_ == null) {
          indicePermanentValeur_ = createIndicePermanent();
        }
        indicePermanentValeur_.put(t, newVal);
        modified = true;
        if (_save && r != null) {
          r.saveToRemovePermanent(t, n);
        }
      }
    }
    if (modified) {
      fireEvent();
    }
    return r;
  }

  /**
   * @return true si contiens des comportements transitoire
   */
  public boolean containsPnTransient() {
    return (indiceTransitoireEvolution_ != null) && (indiceTransitoireEvolution_.size() > 0);
  }

  /**
   * @param _m la table a remplir
   * @param _t la variable demandee
   */
  public void fillWithEvolVar(final H2dEvolutionUseCounter _m, final H2dVariableType _t) {
    if (indiceTransitoireEvolution_ == null) {
      return;
    }
    final TIntObjectProcedure p = new TIntObjectProcedure() {
      @Override
      public boolean execute(int _a, Object _b) {
        _m.add((EvolutionReguliereAbstract) _b, _t, _a);
        return true;
      }
    };
    indiceTransitoireEvolution_.forEachEntry(p);

  }

  /**
   * @param _idx l'indice de l'element demande
   * @param _v la valeur a modifer avec les valeurs pour l'element d'indice _s
   */
  public void fillWithValue(final int _idx, final H2dRefluxValue _v) {
    if ((indicePermanentValeur_ != null) && indicePermanentValeur_.contains(_idx)) {
      _v.setAllValues(H2dBcType.PERMANENT, indicePermanentValeur_.get(_idx), null);
    } else if ((indiceTransitoireEvolution_ != null) && indiceTransitoireEvolution_.contains(_idx)) {
      _v.setAllValues(H2dBcType.TRANSITOIRE, 0, (EvolutionReguliereAbstract) indiceTransitoireEvolution_.get(_idx));
    } else {
      _v.setAllValues(H2dBcType.LIBRE, 0, null);
    }
  }

  /**
   * @param _idxs les indices a parcourir
   * @return la valeur commune. Non null
   */
  public H2dRefluxValue getCommonValue(final int[] _idxs) {
    if ((_idxs == null) || (_idxs.length == 0)) {
      return null;
    }
    final H2dRefluxValue r = new H2dRefluxValue();
    final H2dRefluxValue temp = new H2dRefluxValue();
    final int n = _idxs.length;
    fillWithValue(_idxs[n - 1], r);
    for (int i = n - 2; i >= 0; i--) {
      fillWithValue(_idxs[i], temp);
      if (H2dRefluxValue.compareTwoValueAndIsDiff(r, temp)) {
        if (r.getType() == H2dBcType.MIXTE) {
          return r;
        }
      }
    }
    return r;
  }

  /**
   * @return le nombre de point fixe en permanent ou en transitoire.
   */
  public int getNbValueFixedOrTransient() {
    return getNbPermanentPt() + getValeurTransitoireNb();
  }

  /**
   * Redefinie pour utilisation dans des inner classes locales.
   */
  @Override
  protected TIntDoubleHashMap getIndicePermanentValeur() {
    return super.getIndicePermanentValeur();
  }

  /**
   * @return
   * @return la valeur max des valeurs permanentes
   */
  public double getPermanentMax() {
    if (indicePermanentValeur_ == null) {
      return 0;
    }
    final CtuluTroveProcedure.DoubleMaxValue maxFinder = new CtuluTroveProcedure.DoubleMaxValue();
    indicePermanentValeur_.forEachValue(maxFinder);
    return maxFinder.getMax();
  }

  /**
   * @return la valeur min des valeurs permanentes
   */
  public double getPermanentMin() {
    if (indicePermanentValeur_ == null) {
      return 0;
    }
    final CtuluTroveProcedure.DoubleMinValue maxFinder = new CtuluTroveProcedure.DoubleMinValue();
    indicePermanentValeur_.forEachValue(maxFinder);
    return maxFinder.getMin();
  }

  /**
   * @param _s l'indice du point
   * @return la valeur non nulle contenant les donnees en ce point
   */
  public H2dRefluxValue getValue(final int _s) {
    final H2dRefluxValue v = new H2dRefluxValue();
    fillWithValue(_s, v);
    return v;
  }

  public CtuluCollectionDoubleEdit createEditInterface() {
    return new CtuluCollectionDoubleEditAbstract() {

      @Override
      public double getValue(final int _i) {
        return getIndicePermanentValeur() == null ? 0 : getIndicePermanentValeur().get(_i);
      }

      @Override
      public int getSize() {
        return nbElt_;
      }

      @Override
      protected void internalSetValue(final int _i, final double _newV) {
        if (getIndicePermanentValeur() != null && getIndicePermanentValeur().contains(_i)) {
          getIndicePermanentValeur().put(_i, _newV);
        }

      }

      @Override
      protected void fireObjectChanged(int _indexGeom, Object _newValue) {
        H2dRefluxElementProperty.this.fireEvent();
      }

    };
  }

}