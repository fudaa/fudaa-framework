/*
 * @creation 9 juin 2004
 *
 * @modification $Date: 2007-06-11 13:06:08 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import com.memoire.bu.BuValueValidator;
import com.memoire.fu.FuLog;
import gnu.trove.TIntObjectHashMap;
import gnu.trove.TObjectDoubleHashMap;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEditAbstract;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dEvolutionVariableMap;
import org.fudaa.dodico.h2d.H2dParameters;
import org.fudaa.dodico.h2d.H2dProjectDispatcherListener;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.*;
import org.fudaa.dodico.mesure.EvolutionListenerDispatcher;

import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarParameters.java,v 1.43 2007-06-11 13:06:08 deniger Exp $
 */
public final class H2dRubarParameters extends H2dParameters {
  public static final class NodalParameter implements H2dVariableProviderInterface {
    private final H2dRubarBcMng bcMng_;
    private final H2dRubarSedimentMng sedimentMng;
    private final BathyDoubleModel bathyModel_;
    private H2dRubarNumberFormatter fmt_;
    Map<H2dVariableType, CtuluCollectionDoubleEdit> modelByVariable = new LinkedHashMap<H2dVariableType, CtuluCollectionDoubleEdit>();

    public NodalParameter(H2dRubarBcMng bcMng_, H2dRubarNumberFormatter fmt, final H2dRubarSedimentMng sedimentMng) {
      super();
      this.bcMng_ = bcMng_;
      this.sedimentMng = sedimentMng;
      bathyModel_ = new BathyDoubleModel(bcMng_);
      modelByVariable.put(H2dVariableType.BATHYMETRIE, bathyModel_);
      if (bcMng_.getType().equals(H2dRubarProjetType.TRANSPORT)) {
        addTransportVariables();
      }
      this.fmt_ = fmt;
    }

    @Override
    public int getVariablesBlockLength() {
      return 4;
    }

    private void addTransportVariables() {
      FondDurDoubleModel fondDur = new FondDurDoubleModel(bcMng_);
      fondDur.setAll(-9999d);
      modelByVariable.put(H2dVariableTransType.FOND_INERODABLE, fondDur);
      List<H2dRubarSedimentVariableType> variables = sedimentMng.getVariables();
      for (H2dRubarSedimentVariableType h2dRubarSedimentVariableType : variables) {
        CtuluCollectionDoubleEdit doubleEditable = sedimentMng.getCollectionDoubleEditable(h2dRubarSedimentVariableType);
        modelByVariable.put(h2dRubarSedimentVariableType, doubleEditable);
      }
    }

    @Override
    public boolean containsVarToModify() {
      return true;
    }

    protected void setProjetType(final H2dRubarProjetType _type, final CtuluCommandComposite _cmd) {
      if (H2dRubarProjetType.TRANSPORT.equals(_type)) {
        addTransportVariables();
        if (_cmd != null) {
          _cmd.addCmd(new CtuluCommand() {
            @Override
            public void undo() {
              setProjetType(H2dRubarProjetType.COURANTOLOGIE_2D, null);
            }

            @Override
            public void redo() {
              setProjetType(H2dRubarProjetType.TRANSPORT, null);
            }
          });
        }
      } else {
        final CtuluCollectionDoubleEdit old = modelByVariable.remove(H2dVariableTransType.FOND_INERODABLE);
        List<H2dRubarSedimentVariableType> variables = sedimentMng.getVariables();
        for (H2dRubarSedimentVariableType h2dRubarSedimentVariableType : variables) {
          modelByVariable.remove(h2dRubarSedimentVariableType);
        }
        if (_cmd != null) {
          _cmd.addCmd(new CtuluCommand() {
            @Override
            public void undo() {
              modelByVariable.put(H2dVariableTransType.FOND_INERODABLE, old);
              List<H2dRubarSedimentVariableType> variables = sedimentMng.getVariables();
              for (H2dRubarSedimentVariableType h2dRubarSedimentVariableType : variables) {
                modelByVariable.put(h2dRubarSedimentVariableType, sedimentMng.getCollectionDoubleEditable(
                    h2dRubarSedimentVariableType));
              }
            }

            @Override
            public void redo() {
              modelByVariable.remove(H2dVariableTransType.FOND_INERODABLE);
              List<H2dRubarSedimentVariableType> variables = sedimentMng.getVariables();
              for (H2dRubarSedimentVariableType h2dRubarSedimentVariableType : variables) {
                modelByVariable.remove(h2dRubarSedimentVariableType);
              }
            }
          });
        }
      }
      sedimentMng.fireSedimentChanged();
    }

    @Override
    public EfGridInterface getGrid() {
      return bcMng_.g_;
    }

    @Override
    public CtuluCollectionDouble getViewedModel(final H2dVariableType _t) {
      return modelByVariable.get(_t);
    }

    @Override
    public CtuluCollectionDouble getValues(CtuluVariable _var) {
      return getViewedModel((H2dVariableType) _var);
    }

    @Override
    public CtuluCollectionDoubleEdit getModifiableModel(final H2dVariableType _t) {
      return modelByVariable.get(_t);
    }

    @Override
    public Collection getUsableVariables() {
      return Arrays.asList(getVarToModify());
    }

    @Override
    public BuValueValidator getValidator(final H2dVariableType _t) {
      return fmt_.getFormatterFor(_t).getValueValidator();
    }

    @Override
    public CtuluNumberFormatI getFormater(final H2dVariableType _t) {
      return fmt_.getFormatterFor(_t).getNumberFormat();
    }

    @Override
    public H2dVariableType[] getVarToModify() {
      return modelByVariable.keySet().toArray(new H2dVariableType[modelByVariable.size()]);
    }

    @Override
    public boolean isElementVar() {
      return false;
    }

    public void setValues(final H2dVariableType _var, final int[] _idxPtElt, final double _values,
                          final CtuluCommandContainer _cmd) {
      bathyModel_.set(_idxPtElt, _values, _cmd);
    }

    public void setValues(final H2dVariableType _var, final int[] _idxPtElt, final double[] _values,
                          final CtuluCommandContainer _cmd) {
      bathyModel_.set(_idxPtElt, _values, _cmd);
    }
  }

  private static class BathyDoubleModel extends CtuluCollectionDoubleEditAbstract {
    private final H2dRubarBcMng bcMng_;

    public BathyDoubleModel(H2dRubarBcMng bcMng_) {
      super();
      this.bcMng_ = bcMng_;
    }

    @Override
    protected void fireObjectChanged(int _indexGeom, Object _newValue) {
      bcMng_.fireBathymetrieChanged();
    }

    @Override
    protected void internalSetValue(final int _i, final double _newV) {
      bcMng_.g_.setZIntern(_i, _newV);
    }

    @Override
    public int getSize() {
      return bcMng_.g_.getPtsNb();
    }

    @Override
    public double getValue(final int _i) {
      return bcMng_.g_.getPtZ(_i);
    }
  }

  private static class FondDurDoubleModel extends CtuluArrayDouble {
    private final H2dRubarBcMng bcMng_;

    public FondDurDoubleModel(H2dRubarBcMng bcMng_) {
      super(bcMng_.g_.getPtsNb());
      this.bcMng_ = bcMng_;
    }

    @Override
    protected void fireObjectChanged(int _indexGeom, Object _newValue) {
      bcMng_.fireFondDurChanged();
    }
  }

  /**
   * Type utilise pour le cas d'une condition non_torrentiel.
   */
  public final static H2dRubarEvolution NON_TORENTIEL = new H2dRubarEvolution(H2dResource.getS("Non torrentiel"));

  /**
   * @param _s les sources
   * @param _areteIdxTimeCondition index global de l'arete->Condtions variables dans le temps
   * @param _inter la progression
   * @param _para les parametres
   * @param _areteIdxGlobalTarage indice global->Evolution
   * @param _analyze le receveur des analyses
   * @return les parametres cr�es ou nul si erreur fatale
   */
  public static H2dRubarParameters createParameters(final H2dRubarProjetType _type, final H2dRubarGridAreteSource _s,
                                                    final H2DRubarDicoParams _para,
                                                    final H2dRubarTimeConditionInterface[] _areteIdxTimeCondition,
                                                    final TIntObjectHashMap _areteIdxGlobalTarage,
                                                    final ProgressionInterface _inter, final CtuluAnalyze _analyze,
                                                    final boolean[] _cliChanged, H2dRubarFileType xyFormat, int nbTransportBlock) {
    if (_s == null) {
      return null;
    }
    final H2dRubarParameters r = new H2dRubarParameters(_para);
    _s.getGrid().computeBord(_inter, _analyze);
    _s.getGrid().createIndexRegular(_inter);
    r.bcMng_ = new H2dRubarBcMng();
    r.bcMng_.create(_type, _s, _areteIdxTimeCondition, _areteIdxGlobalTarage, _inter, _analyze, _cliChanged, nbTransportBlock);
    r.setDefaultFriction();
    r.setDefaultSI();
    r.fmt_ = new H2dRubarNumberFormatter(_s.getNbDecimal() == 4, xyFormat);
    _inter.setDesc(H2dResource.getS("Construction du mod�le"));
    r.getNodalData();//to create the node data
    return r;
  }

  /**
   * @param _s les sources
   * @param _inter la progression
   * @param _dico les parametres
   * @param _type le type du projet. Si null courantol 2D sera utilisee
   * @param _analyze le receveur des analyses
   * @return les parametres cr�es ou nul si erreur fatale
   */
  public static H2dRubarParameters createParametersFoNewProject(final H2dRubarGrid _s, final int _nbDecimal,
                                                                final H2DRubarDicoParams _dico,
                                                                final H2dRubarProjetType _type, final ProgressionInterface _inter,
                                                                final CtuluAnalyze _analyze, H2dRubarFileType xyFormat) {
    if (_s == null) {
      return null;
    }
    final H2dRubarParameters r = new H2dRubarParameters(_dico);
    try {
      _s.getGrid().computeBord(_inter, _analyze);
    } catch (final RuntimeException _evt) {
      _analyze.addFatalError(H2dResource.getS("Le maillage semble mal construit."));
      FuLog.warning(_evt);
      return null;
    }
    _s.getGrid().createIndexRegular(_inter);
    r.bcMng_ = H2dRubarBcMng.createNew(_type, _s);
    r.bcMng_.nbDecimal_ = _nbDecimal;
    r.fmt_ = new H2dRubarNumberFormatter(_nbDecimal == 4, xyFormat);
    r.setDefaultFriction();
    r.setDefaultSI();
    r.getNodalData();
    return r;
  }

  H2dRubarApportSpatialMng appMng_;
  H2dRubarVentMng ventMng_;
  H2dRubarSedimentMng sedimentMng_;
  H2dRubarBcMng bcMng_;
  NodalParameter nodalParameter;
  H2dRubarDonneesBrutesMng brutes_;
  H2DRubarDiffusionModel difProp_;
  H2DRubarFrictionModel frtProp_;
  H2dRubarNumberFormatter fmt_;
  H2dRubarLimniMng limniMng_;
  H2dRubarProjectDispatcherListener listener_;
  H2dRubarOuvrageMng ouvrageMng_;
  H2dRubarSI si_;

  /**
   * @param _p les parametres de simulation
   */
  private H2dRubarParameters(final DicoParams _p) {
    super(_p);
  }

  public boolean changeProjetType(final H2dRubarProjetType _newType, final CtuluCommandContainer _cmd) {
    if (_newType == bcMng_.getType()) {
      return false;
    }
    final CtuluCommandComposite cmp = _cmd == null ? null : new CtuluCommandCompositeInverse();

    getNodalData().setProjetType(_newType, cmp);

    if (brutes_ != null) {
      brutes_.setProjetType(_newType, cmp);
    }
    if (si_ != null) {
      si_.setProjectType(_newType, bcMng_.nbConcentrationBlocks, cmp);
    }
    bcMng_.setType(_newType, cmp);
    ((H2DRubarDicoParams) dicoParams_).changedTo(_newType, bcMng_.nbConcentrationBlocks, cmp);
    if (ouvrageMng_ != null) {
      ouvrageMng_.setProjetType(_newType, bcMng_.nbConcentrationBlocks, cmp);
    }
    if (_cmd != null) {
      _cmd.addCmd(cmp);
    }
    return true;
  }

  public void setNbConcentrationBlocks(final int newNbConcentrationBlocks, final CtuluCommandContainer _cmd) {
    if (newNbConcentrationBlocks == bcMng_.getNbConcentrationBlocks()) {
      return;
    }
    updateConcentrationBlocks(newNbConcentrationBlocks, _cmd);
  }

  /**
   * Update all data for concentration block. DO not test if the old block number is the same
   *
   * @param newNbConcentrationBlocks
   * @param _cmd
   */
  public void updateConcentrationBlocks(int newNbConcentrationBlocks, CtuluCommandContainer _cmd) {
    final CtuluCommandComposite cmp = _cmd == null ? null : new CtuluCommandCompositeInverse();

    bcMng_.setNbConcentrationBlocks(newNbConcentrationBlocks, cmp);
    if (brutes_ != null) {
      brutes_.setNbConcentrationBlocks(newNbConcentrationBlocks, cmp);
    }
    if (si_ != null) {
      si_.setProjectType(getProjetType(), newNbConcentrationBlocks, cmp);
    }

    final H2DRubarDicoParams dicoParams = (H2DRubarDicoParams) this.dicoParams_;
    final H2DRubarDicoCasFileFormatVersion dicoFileFormatVersion = (H2DRubarDicoCasFileFormatVersion) dicoParams.getDicoFileFormatVersion();
    if (!dicoFileFormatVersion.getProjectType().equals(getProjetType())
        || (getProjetType().isTransportType() && newNbConcentrationBlocks != dicoFileFormatVersion
        .getNbTransportBlocks())) {
      dicoParams.changedTo(getProjetType(), bcMng_.nbConcentrationBlocks, cmp);
    }
    if (ouvrageMng_ != null) {
      ouvrageMng_.setProjetType(getProjetType(), newNbConcentrationBlocks, cmp);
    }
    if (_cmd != null) {
      _cmd.addCmd(cmp);
    }
  }

  public void setXyFormat(H2dRubarFileType xyFormat) {
    fmt_.setXyFormat(xyFormat);
  }

  protected boolean[] checkSiFr() {
    return new boolean[]{checkSI(), checkFriction()};
  }

  public boolean checkFriction() {
    if (frtProp_.getSize() != getMaillage().getEltNb()) {
      setDefaultFriction();
      return true;
    }
    return false;
  }

  public boolean checkSI() {
    if ((si_ == null) || (si_.getCommonValueSize() != getMaillage().getEltNb())) {
      setDefaultSI();
      return true;
    }
    return false;
  }

  /**
   * Attention si le gestionnaire est deja cree, rien n'est fait.
   *
   * @param _app les apports lus
   * @return le nouveau gestionnaire d'apport
   */
  public H2dRubarApportSpatialMng createAppMng(final H2dRubarAppInterface _app) {
    if (appMng_ == null) {
      if (_app != null && _app.getNbElt() == getMaillage().getEltNb()) {
        appMng_ = new H2dRubarApportSpatialMng(_app);
      } else {
        appMng_ = new H2dRubarApportSpatialMng(getMaillage().getEltNb());
      }
      if (listener_ != null) {
        appMng_.addAppListener(listener_);
      }
    } else if (_app != null && _app.getNbElt() == appMng_.getNbElt()) {
      appMng_.loadData(_app);
    }
    return appMng_;
  }

  /**
   * Attention si le gestionnaire est deja cree, rien n'est fait.
   *
   * @param _app les apports lus
   * @return le nouveau gestionnaire d'apport
   */
  public H2dRubarVentMng createVentMng(final H2dRubarVentInterface _app) {
    if (ventMng_ == null) {
      if (_app != null && _app.getNbElt() == getMaillage().getEltNb()) {
        ventMng_ = new H2dRubarVentMng(_app);
      } else {
        ventMng_ = new H2dRubarVentMng(getMaillage().getEltNb());
      }
      if (listener_ != null) {
        ventMng_.addVentListener(listener_);
      }
    } else if (_app != null && _app.getNbElt() == ventMng_.getNbElt()) {
      ventMng_.loadData(_app);
    }
    return ventMng_;
  }

  /**
   * Attention si le gestionnaire est deja cree, rien n'est fait.
   *
   * @param _app les apports lus
   * @return le nouveau gestionnaire d'apport
   */
  public H2dRubarSedimentMng initSedimentMng(final H2dRubarSedimentInterface _app) {
    if (sedimentMng_ == null) {
      if (_app != null && _app.getNbNoeuds() == getMaillage().getPtsNb()) {
        sedimentMng_ = new H2dRubarSedimentMng(_app, dicoParams_);
      } else {
        sedimentMng_ = new H2dRubarSedimentMng(getMaillage().getPtsNb(), dicoParams_);
      }
      if (listener_ != null) {
        sedimentMng_.addSedimentListener(listener_);
      }
    } else {
      if (_app != null) {
        sedimentMng_.setCouches(_app);
      }
    }
    return sedimentMng_;
  }

  public boolean isDiffusionCreated() {
    return difProp_ != null;
  }

  public void createDiffusion(final double _init, final CtuluCommandManager _mng) {
    final double[] vs = new double[getGridVolume().getEltNb()];
    Arrays.fill(vs, _init);
    createDiffusion(vs, _mng);
  }

  public void setUsedDiffusion(final CtuluCommandManager _mng) {
    if (difProp_ != null) {
      getDicoParams().setValue(getDiffusionEntite(), "-1", _mng);
    }
  }

  public void createDiffusion(final double[] _init, final CtuluCommandManager _mng) {
    if (difProp_ != null) {
      return;
    }
    double[] values = _init;
    if (values == null || values.length != getGridVolume().getEltNb()) {
      values = new double[getGridVolume().getEltNb()];
    }
    difProp_ = new H2DRubarDiffusionModel(values);
    difProp_.addListeners(diffusionListener_);
    H2DRubarDiffusionModel.fireObjectChanged(diffusionListener_, true);
    final CtuluCommandComposite cmp = _mng == null ? null : new CtuluCommandComposite();
    getDicoParams().setValue(getDiffusionEntite(), "-1", cmp);
    if (cmp != null) {
      cmp.addCmd(new CtuluCommand() {
        @Override
        public void undo() {
          removeDiffusion(null, null);
        }

        @Override
        public void redo() {
          createDiffusion(_init, null);
        }
      });
      // pas null
      if (_mng != null) {
        _mng.addCmd(cmp);
      }
    }
  }

  public void removeDiffusion(final CtuluCommandManager _mng, final String _newVal) {
    if (difProp_ == null) {
      return;
    }
    final H2DRubarDiffusionModel old = _mng == null ? null : difProp_;

    diffusionListener_ = difProp_.getListeners();
    difProp_ = null;
    H2DRubarDiffusionModel.fireObjectChanged(diffusionListener_, true);
    final CtuluCommandComposite cmp = _mng == null ? null : new CtuluCommandComposite();
    if (_newVal != null) {
      getDicoParams().setValue(getDiffusionEntite(), _newVal, cmp);
    }
    if (cmp != null) {
      cmp.addCmd(new CtuluCommand() {
        @Override
        public void undo() {
          difProp_ = old;
          difProp_.fireObjectChanged(true);
        }

        @Override
        public void redo() {
          removeDiffusion(null, _newVal);
        }
      });
      // pas null
      if (_mng != null) {
        _mng.addCmd(cmp.getSimplify());
      }
    }
  }

  public H2dRubarDonneesBrutesMng createDonneesBrutes() {
    if (brutes_ != null) {
      return brutes_;
    }
    brutes_ = new H2dRubarDonneesBrutesMng(this, fmt_);
    brutes_.addListener(listener_);
    return brutes_;
  }

  /**
   * @param _r la source contenant les points des limni
   * @param _analyze le receveur d'info
   * @return le nouveau gestionnaire d'apport
   */
  public H2dRubarLimniMng createLimniMng(final H2dRubarDTRResult _r, final CtuluAnalyze _analyze) {
    if (limniMng_ == null) {
      if (_r == null) {
        limniMng_ = new H2dRubarLimniMng(getGridVolume());
      } else {
        limniMng_ = H2dRubarLimniMng.init(getGridVolume(), _r, _analyze);
      }
      if (listener_ != null) {
        limniMng_.addListener(listener_);
      }
    }
    return limniMng_;
  }

  @Override
  public CtuluPermanentList getAllowedBordType() {
    return bcMng_.getBordList();
  }

  /**
   * @return le gestionnaire des apports. non null.
   */
  public H2dRubarApportSpatialMng getAppMng() {
    createAppMng(null);
    return appMng_;
  }

  /**
   * @return le gestionnaire des apports. non null.
   */
  public H2dRubarVentMng getVentMng() {
    createVentMng(null);
    return ventMng_;
  }

  public H2dRubarSedimentMng getSedimentMng() {
    initSedimentMng(null);
    return sedimentMng_;
  }

  public BathyDoubleModel getBathyModel() {
    return getNodalData().bathyModel_;
  }

  /**
   * @return le manager de bord
   */
  public H2dRubarBcMng getBcMng() {
    return bcMng_;
  }

  public H2DRubarDiffusionModel getDiffusion() {
    return difProp_;
  }

  Set diffusionListener_ = new HashSet();

  public void addDiffusionListener(final H2DRubarDiffusionListener _l) {
    if (isDiffusionCreated()) {
      difProp_.addListener(_l);
    } else {
      diffusionListener_.add(_l);
    }
  }

  public void removeDiffusionListener(final H2DRubarDiffusionListener _l) {
    if (isDiffusionCreated()) {
      difProp_.removeListener(_l);
    } else {
      diffusionListener_.remove(_l);
    }
  }

  public H2dRubarDonneesBrutesMng getDonneesBrutes() {
    return brutes_;
  }

  /**
   * A Am�liorer ....
   *
   * @param _v
   */
  public CtuluArrayDouble getEltModel(final H2dVariableType _v) {
    if (_v == H2dVariableType.COEF_FROTTEMENT_FOND) {
      return frtProp_;
    }
    return difProp_;
  }

  @Override
  public H2dVariableProviderInterface getElementData() {
    return new H2dVariableProviderInterface() {
      @Override
      public boolean containsVarToModify() {
        return true;
      }

      @Override
      public int getVariablesBlockLength() {
        return 3;
      }

      @Override
      public EfGridInterface getGrid() {
        return H2dRubarParameters.this.getMaillage();
      }

      @Override
      public CtuluCollectionDoubleEdit getModifiableModel(final H2dVariableType _t) {
        return getEltModel(_t);
      }

      @Override
      public CtuluCollectionDouble getValues(CtuluVariable _var) {
        return getViewedModel((H2dVariableType) _var);
      }

      @Override
      public CtuluCollectionDouble getViewedModel(final H2dVariableType _t) {
        return getEltModel(_t);
      }

      @Override
      public Collection getUsableVariables() {
        return Arrays.asList(getVarToModify());
      }

      @Override
      public BuValueValidator getValidator(final H2dVariableType _t) {
        return getFmt().getFormatterFor(_t).getValueValidator();
      }

      @Override
      public CtuluNumberFormatI getFormater(final H2dVariableType _t) {
        return getFmt().getFormatterFor(_t).getNumberFormat();
      }

      @Override
      public H2dVariableType[] getVarToModify() {
        if (difProp_ != null) {
          return new H2dVariableType[]{H2dVariableType.COEF_FROTTEMENT_FOND, H2dVariableType.COEF_DIFFUSION};
        }
        return new H2dVariableType[]{H2dVariableType.COEF_FROTTEMENT_FOND};
      }

      @Override
      public boolean isElementVar() {
        return true;
      }

      public void setValues(final H2dVariableType _var, final int[] _idxPtElt, final double _values,
                            final CtuluCommandContainer _cmd) {
        final CtuluArrayDouble model = getEltModel(_var);
        if (model != null) {
          model.set(_idxPtElt, _values, _cmd);
        }
      }

      public void setValues(final H2dVariableType _var, final int[] _idxPtElt, final double[] _values,
                            final CtuluCommandContainer _cmd) {
        final CtuluArrayDouble model = getEltModel(_var);
        if (model != null) {
          model.set(_idxPtElt, _values, _cmd);
        }
      }
    };
  }

  public EvolutionListenerDispatcher getEvolutionDispatcher() {
    return bcMng_;
  }

  /**
   * @return le formatteur de nombre
   */
  public H2dRubarNumberFormatter getFmt() {
    return fmt_;
  }

  /**
   * @return les proprietes par elements
   */
  public H2DRubarFrictionModel getFriction() {
    return frtProp_;
  }

  /**
   * @return le maillage
   */
  public H2dRubarGrid getGridVolume() {
    return bcMng_.g_;
  }

  /**
   * @return le gestionnaire des points limni.
   */
  public H2dRubarLimniMng getLimniMng() {
    createLimniMng(null, null);
    return limniMng_;
  }

  /**
   * @return le listener global pour les parametres
   */
  public H2dRubarProjectDispatcherListener getListener() {
    return listener_;
  }

  @Override
  public EfGridInterface getMaillage() {
    return getGridVolume();
  }

  @Override
  public H2dProjectDispatcherListener getMainListener() {
    return listener_;
  }

  @Override
  public NodalParameter getNodalData() {
    if (nodalParameter == null) {
      initSedimentMng(null);
      nodalParameter = new NodalParameter(bcMng_, fmt_, sedimentMng_);
    }
    return nodalParameter;
  }

  public H2dRubarOuvrageMng getOuvrageMng() {
    if (ouvrageMng_ == null) {
      ouvrageMng_ = new H2dRubarOuvrageMng(bcMng_.getGridWithConnectionAndWithoutLimit());
      ouvrageMng_.setProjetType(getProjetType(), bcMng_.nbConcentrationBlocks, null);
      if (listener_ != null) {
        ouvrageMng_.addListener(listener_);
      }
      bcMng_.addEvolutionListener(ouvrageMng_);
    }
    return ouvrageMng_;
  }

  @Override
  public EvolutionListenerDispatcher getProjectDispatcher() {
    return getEvolutionDispatcher();
  }

  /**
   * @return le type du projet
   */
  public H2dRubarProjetType getProjetType() {
    return getBcMng().getType();
  }

  /**
   * @return les solutions initiales
   */
  public H2dRubarSI getSi() {
    return si_;
  }

  @Override
  public H2dVariableProviderInterface getSiData() {
    return si_;
  }

  /**
   * @return le manager des courbes de tarage.
   */
  public H2dRubarTarageMng getTarageMng() {
    return bcMng_.getTarageMng();
  }

  @Override
  public List getTransientVar() {
    if (isTransport()) {
      return H2dRubarBcTypeList.TRANSIENT_VAR_TRANSPORT;
    }
    return H2dRubarBcTypeList.TRANSIENT_VAR;
  }

  @Override
  public Map getUsedEvolutionVariables() {
    final H2dEvolutionVariableMap r = new H2dEvolutionVariableMap();
    bcMng_.fillWithTransientCurves(r);
    if (appMng_ != null) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("fill with storm curves");
      }
      appMng_.fillWithTransientCurves(r);
    }
    if (ventMng_ != null) {
      ventMng_.fillWithTransientCurves(r);
    }
    if (ouvrageMng_ != null) {
      ouvrageMng_.fillWithTransientCurves(r);
    }
    return r.getVarEvol();
  }

  public void initOuvrageMng(final H2dRubarOuvrageContainer _c, final CtuluAnalyze _analyze) {
    if (ouvrageMng_ == null) {
      getOuvrageMng();
    }
    if (ouvrageMng_ != null) {
      ouvrageMng_.initWith(_c, _analyze);
      //to be sure that all structure are initialized with the correct evols.
      ouvrageMng_.setProjetType(getProjetType(), bcMng_.nbConcentrationBlocks, null);
    }
  }

  /**
   * A utiliser avec attention.
   *
   * @param _g le donneur de point
   */
  public void initPt(final H2dRubarGrid _g) {
    getGridVolume().initWithPoints(_g);
  }

  public boolean isDonneesBrutesUsed() {
    return brutes_ != null;
  }

  public boolean isTransport() {
    return getProjetType() == H2dRubarProjetType.TRANSPORT;
  }

  /**
   * @param _s
   * @param _fr
   * @param _siVarDouble
   * @param _progress
   * @param _an
   * @return tableau de 2 boolean. [si,frt]. si true, les valeurs ont du etre initialisees a zero
   */
  public boolean[] reinitAll(final H2dRubarGridAreteSource _s, final CtuluArrayDouble _fr,
                             final H2DRubarSolutionsInitialesInterface _siVarDouble,
                             final ProgressionInterface _progress, final CtuluAnalyze _an) {
    bcMng_.create(bcMng_.getType(), _s, null, null, _progress, _an, new boolean[1], bcMng_.getNbConcentrationBlocks());
    if (_s.getNbDecimal() != bcMng_.nbDecimal_) {
      _an.addError("Probleme dans les d�cimales pour x,y : non constantes", -1);
    }
    if (_fr != null) {
      setFriction(_fr);
    }
    if (_siVarDouble != null) {
      setSI(_siVarDouble);
    }
    if (ouvrageMng_ != null) {
      ouvrageMng_.setGrid(_s);
    }
    return checkSiFr();
  }

  /**
   * @return initialise les apports
   */
  public boolean reinitApp() {
    if (appMng_ != null) {
      appMng_.clear(null);
      return true;
    }
    return false;
  }

  /**
   * @param _dico les nouvelles valeurs du dico
   */
  public void reinitDicoParams(final DicoParams _dico) {
    dicoParams_.removeAll();
    dicoParams_.addAllValues(_dico);
  }

  /**
   * @param _siValues s. si null ne fait rien
   * @param _fr si null ne fait rien
   * @return tableau de 2 boolean. [si,frt]. si true, les valeurs ont du etre initialisees a zero
   */
  public boolean[] reinitSiFrt(final H2DRubarSolutionsInitialesInterface _siValues, final CtuluArrayDouble _fr) {
    if (_fr != null) {
      setFriction(_fr);
    }
    if (_siValues != null) {
      setSI(_siValues);
    }
    return checkSiFr();
  }

  public Double getDIfValueInDico() {
    final DicoEntite ent = getDiffusionEntite();
    Double val = CtuluLib.getDouble(-1);
    try {
      val = CtuluLib.getDouble(Double.parseDouble(getDicoParams().getValue(ent)));
    } catch (final NumberFormatException _evt) {
    }
    return val;
  }

  private DicoEntite getDiffusionEntite() {
    return H2dRubarDicoModel.getDiffusionEntite(getDicoParams().getDicoFileFormatVersion());
  }

  public boolean reinitDIF(final CtuluArrayDouble _dif) {
    if (difProp_ == null) {
      createDiffusion(_dif.getValues(), null);
    } else {
      difProp_.setAll(_dif, null);
    }
    if (difProp_.getSize() != getGridVolume().getEltNb()) {
      setDefaultDiffusion();
      return true;
    }
    return false;
  }

  /**
   * Initialise la friction a 0.
   */
  public void setDefaultFriction() {
    if ((frtProp_ == null) || (frtProp_.getSize() != getMaillage().getEltNb())) {
      Set listeners = null;
      if (frtProp_ != null) {
        listeners = frtProp_.getListeners();
      }
      frtProp_ = new H2DRubarFrictionModel(new double[getGridVolume().getEltNb()]);
      if (listeners != null) {
        frtProp_.addListeners(listeners);
      }
      if (listener_ != null) {
        frtProp_.addListener(listener_);
      }
    } else {
      frtProp_.setAll(0, null);
    }
  }

  public void setDefaultDiffusion() {
    if ((difProp_ != null) || (difProp_.getSize() != getMaillage().getEltNb())) {
      if (difProp_ != null) {
        diffusionListener_ = difProp_.getListeners();
      }
      difProp_ = new H2DRubarDiffusionModel(new double[getGridVolume().getEltNb()]);
      difProp_.addListeners(diffusionListener_);
      if (listener_ != null) {
        frtProp_.addListener(listener_);
      }
    } else {
      difProp_.setAll(0, null);
    }
  }

  /**
   * Construit des si par defaut (0 partout).
   */
  public void setDefaultSI() {
    if (si_ == null) {
      si_ = new H2dRubarSI(dicoParams_, getGridVolume(), isTransport(), bcMng_.nbConcentrationBlocks);
      if (listener_ != null) {
        si_.addListener(listener_);
      }
    } else {
      si_.setDefaultSI(getGridVolume(), isTransport(), bcMng_.nbConcentrationBlocks);
    }
  }

  /**
   * @param _v les valeurs de frictions
   */
  public void setFriction(final CtuluCollectionDoubleEdit _v) {
    if ((_v != null) && (_v.getSize() == getGridVolume().getEltNb())) {
      if (frtProp_ == null) {
        frtProp_ = new H2DRubarFrictionModel(_v);
      } else {
        frtProp_.setAll(_v, null);
      }
    } else {
      setDefaultFriction();
    }
  }

  public void setFondDur(final CtuluCollectionDouble fondDur) {
    NodalParameter nodalData = getNodalData();
    CtuluCollectionDoubleEdit modifiableModel = nodalData.getModifiableModel(H2dVariableTransType.FOND_INERODABLE);
    if (modifiableModel != null) {
      modifiableModel.initWithDouble(fondDur, true);
    }
  }

  public void setDiffusion(final CtuluCollectionDoubleEdit _v) {
    if ((_v != null) && (_v.getSize() == getGridVolume().getEltNb())) {
      if (difProp_ == null) {
        difProp_ = new H2DRubarDiffusionModel(_v);
      } else {
        difProp_.setAll(_v, null);
      }
    } else {
      setDefaultDiffusion();
    }
  }

  /**
   * Enleve si necessaire les anciens listeners.
   *
   * @param _listener le nouveau listener
   */
  public void setListener(final H2dRubarProjectDispatcherListener _listener) {
    bcMng_.setProjectDispatcher(listener_, _listener);
    if (_listener != listener_) {
      if (listener_ != null) {
        dicoParams_.removeModelListener(listener_);
        if (brutes_ != null) {
          brutes_.removeListener(listener_);
        }
        si_.removeListener(_listener);
        frtProp_.removeListener(_listener);
        if (appMng_ != null) {
          appMng_.removeAppListener(listener_);
        }
        if (ventMng_ != null) {
          ventMng_.removeVentListener(listener_);
        }
        if (limniMng_ != null) {
          limniMng_.removeListener(listener_);
        }
        if (ouvrageMng_ != null) {
          ouvrageMng_.removeListener(listener_);
        }
        if (fmt_ != null) {
          frtProp_.removeListener(listener_);
        }
        removeDiffusionListener(listener_);
      }
      listener_ = _listener;
      if (listener_ != null) {
        dicoParams_.addModelListener(listener_);
        if (brutes_ != null) {
          brutes_.addListener(listener_);
        }
        si_.addListener(listener_);
        frtProp_.addListener(listener_);
        if (appMng_ != null) {
          appMng_.addAppListener(listener_);
        }
        if (ventMng_ != null) {
          ventMng_.addVentListener(listener_);
        }
        if (limniMng_ != null) {
          limniMng_.addListener(listener_);
        }
        if (ouvrageMng_ != null) {
          ouvrageMng_.addListener(listener_);
        }
        if (fmt_ != null) {
          frtProp_.addListener(listener_);
        }
        addDiffusionListener(listener_);
      }
    }
  }

  /**
   * @param _si les nouvelles solutions initiales
   */
  public void setSI(final H2DRubarSolutionsInitialesInterface _si) {
    if (si_ == null) {
      si_ = new H2dRubarSI(dicoParams_, getMaillage(), _si, isTransport(), bcMng_.nbConcentrationBlocks);
    } else {
      si_.setData(_si, getMaillage(), isTransport(), bcMng_.nbConcentrationBlocks);
    }
  }

  /**
   * @param _eltIdx les elements a modifier
   * @param _typeValue la correspondance variableType->Valeur
   * @param _cmd le receveur de commande
   */
  public void setSIValue(final int[] _eltIdx, final TObjectDoubleHashMap _typeValue, final CtuluCommandContainer _cmd) {
    if (si_ == null) {
      return;
    }
    si_.set(_eltIdx, _typeValue, _cmd);
  }
}
