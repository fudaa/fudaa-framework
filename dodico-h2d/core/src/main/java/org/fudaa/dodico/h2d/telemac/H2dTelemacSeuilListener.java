/*
 *  @creation     3 d�c. 2004
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

/**
 * @author Fred Deniger
 * @version $Id: H2dTelemacSeuilListener.java,v 1.3 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dTelemacSeuilListener {

  /**
   * Envoye lorsque des valeurs sont modifiees.
   *
   * @param _xyChanged true si des indices ont ete modifie. false si seuls des valeurs (debit,..) ont ete modifiees
   */
  void seuilChanged(boolean _xyChanged);

  /**
   * Un seuil a ete ajoute.
   */
  void seuilAdded();

  /**
   * Un seuil a ete enleve.
   */
  void seuilRemoved();

}
