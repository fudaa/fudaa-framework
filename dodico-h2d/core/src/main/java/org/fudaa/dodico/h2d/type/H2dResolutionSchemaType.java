/*
 * @creation 27 juin 2003
 * @modification $Date: 2007-06-29 15:10:31 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.h2d.type;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.dodico.commun.DodicoEnumType;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author deniger
 * @version $Id: H2dResolutionSchemaType.java,v 1.19 2007-06-29 15:10:31 deniger Exp $
 */
public final class H2dResolutionSchemaType extends DodicoEnumType {

  /**
   * Liste des variable qui peuvent etre utilise par un schema.
   */
  public final static CtuluPermanentList TRANSITOIRE_VAR = new CtuluPermanentList(CtuluLibArray
      .sort(new H2dVariableType[] { H2dVariableTimeType.FREQUENCE_PRINT_SCHEMA, H2dVariableTimeType.COEF_SCHEMA,
          H2dVariableTimeType.NB_TIME_STEP_SCHEMA, H2dVariableTimeType.VALUE_TIME_STEP_SCHEMA }));
  /**
   * Schema stationnaire. Accepte toutes les methodes de resolutions. Aucune variable n'est a definir.
   */
  public final static H2dResolutionSchemaType STATIONNAIRE = new H2dResolutionSchemaType(H2dResource
      .getS("Stationnaire"), "STATIONNAIRE", new H2dResolutionMethodType[] { H2dResolutionMethodType.SELECT_LUMPING,
      H2dResolutionMethodType.SELECT_LUMPING_BCD, H2dResolutionMethodType.NEWTON_RAPHSON,
      H2dResolutionMethodType.NEWTON_RAPHSON_BCD, H2dResolutionMethodType.LINEAIRE });
  /**
   * Schema kawahara. Accepte toutes les methodes de select lumping. Toutes les variables sont a definir.
   */
  public final static H2dResolutionSchemaType KAWAHARA = new H2dResolutionSchemaType(H2dResource.getS("Kawahara"),
      "SELUMP", new H2dResolutionMethodType[] { H2dResolutionMethodType.SELECT_LUMPING,
          H2dResolutionMethodType.SELECT_LUMPING_BCD }, TRANSITOIRE_VAR);
  public final static H2dResolutionSchemaType LAXWEN = new H2dResolutionSchemaType(H2dResource
      .getS("LaxWendroff NE PAS UTILISER BETA VERSION"), "LAXWEN", new H2dResolutionMethodType[] {
      H2dResolutionMethodType.LAXWEN, H2dResolutionMethodType.LAXWEN_BCD }, TRANSITOIRE_VAR);
  /**
   * Schema stationnaire. Accepte toutes les methodes de newton et la methode lineaire.Toutes les variables sont a
   * definir.
   */
  public final static H2dResolutionSchemaType EULER = new H2dResolutionSchemaType(H2dResource.getS("Euler"), "EULER",
      new H2dResolutionMethodType[] { H2dResolutionMethodType.NEWTON_RAPHSON,
          H2dResolutionMethodType.NEWTON_RAPHSON_BCD, H2dResolutionMethodType.LINEAIRE }, TRANSITOIRE_VAR);
  /**
   * La liste de tous les schema.
   */
  public static final List LIST = new CtuluPermanentList(CtuluLibArray.sort(new H2dResolutionSchemaType[] {
      STATIONNAIRE, KAWAHARA, EULER, LAXWEN }));

  public static H2dResolutionSchemaType[] getConstantArray() {
    return new H2dResolutionSchemaType[] { STATIONNAIRE, KAWAHARA, EULER, LAXWEN };
  }

  /**
   * @param _id l'identifiant reflux
   * @return le schema correspondant (LIST) ou null si aucun.
   */
  public static H2dResolutionSchemaType getTypeForRefluxId(final String _id) {
    H2dResolutionSchemaType t;
    for (final Iterator it = LIST.iterator(); it.hasNext();) {
      t = (H2dResolutionSchemaType) it.next();
      if (t.refluxId_.equals(_id)) {
        return t;
      }
    }
    return null;
  }

  /**
   * @return Les schema euler et kawahara
   */
  public static H2dResolutionSchemaType[] getRefluxTranSchema() {
    return new H2dResolutionSchemaType[] { H2dResolutionSchemaType.EULER, H2dResolutionSchemaType.KAWAHARA,
        H2dResolutionSchemaType.LAXWEN };
  }

  String refluxId_;
  CtuluPermanentList methodeList_;
  CtuluPermanentList varList_;

  H2dResolutionSchemaType() {
    this(CtuluLibString.EMPTY_STRING, CtuluLibString.EMPTY_STRING, null, null);
  }

  /**
   * @param _nom le nom
   * @param _refluxId l'identifiant reflux (fichier inp)
   * @param _methode les methode associees
   */
  public H2dResolutionSchemaType(final String _nom, final String _refluxId, final H2dResolutionMethodType[] _methode) {
    this(_nom, _refluxId, _methode, null);
  }

  /**
   * @param _nom le nom
   * @param _refluxId l'identifiant reflux (fichier inp)
   * @param _methode les methode associees
   * @param _var les variables associees
   */
  public H2dResolutionSchemaType(final String _nom, final String _refluxId, final H2dResolutionMethodType[] _methode,
      final CtuluPermanentList _var) {
    super(_nom);
    refluxId_ = _refluxId;
    methodeList_ = new CtuluPermanentList(CtuluLibArray.sort(_methode));
    if (_var != null) {
      varList_ = _var;
    }
  }

  /**
   * @param _methode la methode a tester
   * @return true si cette methode est supporte par ce schema.
   */
  public boolean isSupported(final H2dResolutionMethodType _methode) {
    return Collections.binarySearch(methodeList_, _methode) >= 0;
  }

  /**
   * @return la liste des methodes supportees.
   */
  public CtuluPermanentList getMethodList() {
    return methodeList_;
  }

  /**
   * @return la liste des variables a specifier.
   */
  public CtuluPermanentList getVarList() {
    return varList_;
  }

  public DodicoEnumType[] getArray() {
    return getConstantArray();
  }

  /**
   * @param _t la variable a tester
   * @return true si appartient aux variables a specifier.
   */
  public boolean isUsed(final H2dVariableType _t) {
    return (varList_ != null) && (Collections.binarySearch(varList_, _t) >= 0);
  }

  /**
   * @return l'id reflux (fichier inp)
   */
  public String getRefluxId() {
    return refluxId_;
  }
}