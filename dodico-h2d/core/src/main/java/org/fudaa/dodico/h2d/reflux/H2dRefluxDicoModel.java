/*
 *  @file         H2dRefluxEntiteList.java
 *  @creation     27 juin 2003
 *  @modification $Date: 2007-06-29 15:10:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.dodico.h2d.reflux;

import com.memoire.fu.FuLog;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.dico.DicoDataType;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoModelAbstract;

/**
 * @author deniger
 * @version $Id: H2dRefluxDicoModel.java,v 1.10 2007-06-29 15:10:28 deniger Exp $
 */
public class H2dRefluxDicoModel extends DicoModelAbstract {
  String version_;
  final boolean containSrc_;

  /**
   * @param _v le num de version
   */
  public H2dRefluxDicoModel(final String _v, boolean _containSrc) {
    super();
    version_ = _v;
    containSrc_ = _containSrc;
  }

  /**
   * @param _languageId l'indice du langage
   * @see org.fudaa.dodico.dico.DicoLanguage#getCurrentID()
   */
  public H2dRefluxDicoModel(final int _languageId, boolean _containSrc) {
    super(_languageId);
    containSrc_ = _containSrc;
  }

  @Override
  public DicoEntite[] createEntites() {
    final List r = new ArrayList(20);
    int index = 0;
    DicoDataType.Reel type = new DicoDataType.Reel();
    type.setControle(0, 1);
    final String[] t = new String[2];
    t[0] = "Convection";
    t[1] = "Convection";
    DicoEntite.Simple ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setNiveau(0);
    ent.setIndex(index);
    final String un = "1.0";
    t[1] = un;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    final String coefContrib = "Coefficients contribution";
    t[0] = coefContrib;
    final String coefContribEN = "Contribution coefficients";
    t[1] = coefContribEN;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;

    type = new DicoDataType.Reel();
    type.setControle(0, 1);
    t[1] = "Diffusion";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setIndex(index);
    ent.setNiveau(0);
    t[1] = un;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    t[0] = coefContrib;
    t[1] = coefContribEN;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;
    type = new DicoDataType.Reel();
    type.setControle(0, 1);
    t[1] = "Continuit� lin�aire";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setIndex(index);
    ent.setNiveau(0);
    t[1] = un;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    t[0] = coefContrib;
    t[1] = coefContribEN;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;
    type = new DicoDataType.Reel();
    type.setControle(0, 1);
    t[1] = "Continuit� non lin�aire";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setIndex(index);
    ent.setNiveau(0);
    t[1] = un;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    t[0] = coefContrib;
    t[1] = coefContribEN;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;
    type = new DicoDataType.Reel();
    type.setControle(0, 1);
    t[1] = "Gravitation";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setIndex(index);
    ent.setNiveau(0);
    t[1] = un;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    t[0] = coefContrib;
    t[1] = coefContribEN;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;
    type = new DicoDataType.Reel();
    type.setControle(0, 1);
    t[1] = "Frottement";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setIndex(index);
    ent.setNiveau(0);
    t[1] = un;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    t[0] = coefContrib;
    t[1] = coefContribEN;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;
    type = new DicoDataType.Reel();
    type.setControle(0, 1);
    t[1] = "Coriolis";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setIndex(index);
    ent.setNiveau(0);
    t[1] = "0.0";
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    t[0] = coefContrib;
    t[1] = coefContribEN;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;
    type = new DicoDataType.Reel();
    type.setControle(0, 1);
    t[1] = "Contraintes de radiation";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setIndex(index);
    ent.setNiveau(0);
    t[1] = "0.0";
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    t[0] = coefContrib;
    t[1] = coefContribEN;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;

    type = new DicoDataType.Reel();
    type.setControle(0, 1);
    t[1] = "Forces du vent";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], type);
    ent.setIndex(index);
    ent.setNiveau(0);
    t[1] = "0.0";
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    t[0] = coefContrib;
    t[1] = coefContribEN;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;
    // Terme de sources et de puits
    if (containSrc_) {
      type = new DicoDataType.Reel();
      type.setControle(0, 1);
      t[1] = "Terme de sources et de puits";
      t[0] = t[1];
      ent = new DicoEntite.Simple(t[languageIndex_], type);
      ent.setIndex(index);
      ent.setNiveau(0);
      t[1] = "0.0";
      t[0] = t[1];
      ent.setDefautValue(t[languageIndex_]);
      t[0] = coefContrib;
      t[1] = coefContribEN;
      ent.setRubrique(t[languageIndex_]);
      r.add(ent.getImmutable());
      index++;
    }

    // impression
    int imprIndex = 0;
    final DicoDataType.Binaire typeBinaire = DicoDataType.Binaire.EMPTY;
    t[1] = "DONNEES";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(imprIndex++);
    final String falseVal = "false";
    t[1] = falseVal;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    final String impression = "Impression";
    t[0] = impression;
    final String print = "Print";
    t[1] = print;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;
    t[1] = "POINTEURS";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(imprIndex++);
    t[1] = falseVal;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    t[0] = impression;
    t[1] = print;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;
    t[1] = "TABLES_ELEM";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(imprIndex++);
    t[1] = falseVal;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    t[0] = impression;
    t[1] = print;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;
    t[1] = "TABLES_GLOB";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(imprIndex++);
    t[1] = falseVal;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    t[0] = impression;
    t[1] = print;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;
    t[1] = "CIEL";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(imprIndex++);
    t[1] = falseVal;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    t[0] = impression;
    t[1] = print;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;
    t[1] = "INITIALISATION";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(imprIndex++);
    t[1] = falseVal;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    t[0] = impression;
    t[1] = print;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;
    // correction typo
    t[1] = "MISES_A_JOUR";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(imprIndex++);
    t[1] = falseVal;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    t[0] = impression;
    t[1] = print;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;
    t[1] = "RESOLUTION";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(imprIndex++);
    t[1] = falseVal;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    t[0] = impression;
    t[1] = print;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;
    t[1] = "ITERATION";
    t[0] = t[1];
    ent = new DicoEntite.Simple(t[languageIndex_], typeBinaire);
    ent.setNiveau(0);
    ent.setIndex(imprIndex++);
    t[1] = falseVal;
    t[0] = t[1];
    ent.setDefautValue(t[languageIndex_]);
    t[0] = impression;
    t[1] = print;
    ent.setRubrique(t[languageIndex_]);
    r.add(ent.getImmutable());
    index++;
    return (DicoEntite[]) r.toArray(new DicoEntite[r.size()]);
  }

  @Override
  protected String[] createNoms() {
    FuLog.warning("non implante");
    return null;
  }

  @Override
  public String getCodeName() {
    return "reflux";
  }

  @Override
  public String getVersion() {
    return version_;
  }
}
