/*
 * @creation 25 juin 2003
 * @modification $Date: 2007-05-22 13:11:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import gnu.trove.TDoubleHashSet;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

/**
 * @author deniger
 * @version $Id: H2dEvolutionFrontiereLiquide.java,v 1.19 2007-05-22 13:11:26 deniger Exp $
 */
public class H2dEvolutionFrontiereLiquide {

  protected EvolutionReguliere evol_;
  int indexFrontiere_;

  H2dVariableType variable_;

  /**
   * @param _var la variable concerne
   * @param _index l'indice de la frontiere liquide
   * @param _r l'evolution a considerer
   */
  public H2dEvolutionFrontiereLiquide(final H2dVariableType _var, final int _index, final EvolutionReguliere _r) {
    variable_ = _var;
    indexFrontiere_ = _index;
    evol_ = _r;
  }
  

  /**
   *
   */
  public void setIndexFrontiere(final int _i) {
    indexFrontiere_ = _i;
  }

  /**
   * @param _newTimeStep les nouveaux pas de temps support pour l'interpolation
   * @return nouvelle evolution interpolee sur les pas de temps
   */
  public EvolutionReguliere createEvolutionFromInterpolation(final double[] _newTimeStep) {
    return evol_.createEvolutionFromInterpolation(_newTimeStep);
  }

  /**
   * @return le tableau des pas de temps
   */
  public double[] getTimeSteps() {
    return evol_.getArrayX();
  }

  /**
   * Peut etre tres long .
   * 
   * @return chaine contenant tous les pas de temps/valeurs.
   */
  public String getDescriptionTotale() {
    return evol_.getDescriptionTotale();
  }

  /**
   * @return l'evolution
   */
  public EvolutionReguliereAbstract getEvolution() {
    return evol_;
  }

  /**
   * @return l'indice de la frontiere concernee
   */
  public int getIndexFrontiere() {
    return indexFrontiere_;
  }

  /**
   * @return le nom de l'evolution
   */
  public String getNom() {
    return evol_.getNom();
  }

  /**
   * @return le nombre de pas de temps
   */
  public int getPasTempNb() {
    return evol_.getNbValues();
  }

  /**
   * @param _index l'indice du pas de temps
   * @return la valeur du pas de temps <code>_index</code>
   */
  public double getPasDeTempsAt(final int _index) {
    return evol_.getX(_index);
  }

  /**
   * @param _i l'indice de la valeur voulue
   * @return la valeur a l'indice _i
   */
  public double getValueAt(final int _i) {
    return evol_.getY(_i);
  }

  

  /**
   * @return l'unite
   */
  public String getUnite() {
    return evol_.getUnite();
  }

  /**
   * @return la variable
   */
  public H2dVariableType getVariableType() {
    return variable_;
  }

  /**
   * @param _l l'evolution a comparer
   * @param _s la table stockant l'union des pas de temps
   * @return true si meme base.
   */
  public boolean isEvolutionWithSameX(final H2dEvolutionFrontiereLiquide _l, final TDoubleHashSet _s) {
    return evol_.isEvolutionWithSameX(_l.evol_, _s);
  }

}