/*
 * @creation 25 juin 2003
 * 
 * @modification $Date: 2007-02-15 17:09:21 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import com.memoire.fu.FuLog;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.h2d.type.H2dVariableTypeContainer;

/**
 * @author deniger
 * @version $Id: H2dBoundaryCondition.java,v 1.18 2007-02-15 17:09:21 deniger Exp $
 */
public class H2dBoundaryCondition implements Comparable, H2dVariableTypeContainer {

  protected int indexPt_;

  protected double u_, v_, hOrSl_;

  public H2dBoundaryCondition() {
    super();
  }

  @Override
  public double getSurfaceLibre() {
    return 0;
  }

  /**
   * @param _c la condition a utiliser pour l'initialisation
   */
  public H2dBoundaryCondition(final H2dBoundaryCondition _c) {
    initValuesIntern(_c);
    if (_c != null) {
      indexPt_ = _c.indexPt_;
    }
  }

  private void initValuesIntern(final H2dBoundaryCondition _c) {
    if (_c != null) {
      u_ = _c.u_;
      v_ = _c.v_;
      hOrSl_ = _c.hOrSl_;
    }
  }

  protected void initFrom(final H2dBoundaryCondition _c) {
    if (_c != null) {
      initValuesIntern(_c);
      indexPt_ = _c.indexPt_;
    }
  }

  protected void initValueFrom(final H2dBoundaryCondition _c) {
    initValuesIntern(_c);
  }

  /**
   * Compare les conditions limites selon l'indice du point seulement.
   */
  @Override
  public int compareTo(final Object _o) {
    if (_o instanceof H2dBoundaryCondition) {
      return indexPt_ - ((H2dBoundaryCondition) _o).indexPt_;
    }
    throw new IllegalArgumentException("o must be a H2dConditionLimite");
  }

  /**
   * Envoie une exception: a surcharger.
   */
  @Override
  public double getAlphaMixtureLength() {
    errorNotSupported();
    return 0;
  }

  private void errorNotSupported() {
    FuLog.error("DH2: not supported");
  }

  /**
   * Envoie une exception: a surcharger.
   */
  @Override
  public double getConcentration() {
    new Throwable().printStackTrace();
    return 0;
  }

  @Override
  public double getEpaisseur() {
    new Throwable().printStackTrace();
    return 0;
  }

  @Override
  public double getContrainte() {
    new Throwable().printStackTrace();
    return 0;
  }

  /**
   * Envoie une exception: a surcharger.
   */
  @Override
  public double getDiametre() {
    new Throwable().printStackTrace();
    return 0;
  }

  /**
   * Envoie une exception: a surcharger.
   */
  @Override
  public double getEtendue() {
    new Throwable().printStackTrace();
    return 0;
  }

  /**
   * Envoie une exception: a surcharger.
   */
  @Override
  public double getEvolSedimValue() {
    new Throwable().printStackTrace();
    return 0;
  }

  /**
   * Envoie une exception. A surcharger
   */
  @Override
  public double getFriction() {
    errorNotSupported();
    return 0;
  }

  /**
   * @return la hauteur d'eau
   */
  @Override
  public double getH() {
    return hOrSl_;
  }

  /**
   * @return L'indice globale du point pour cette condition limite
   */
  public final int getIndexPt() {
    return indexPt_;
  }

  /**
   * Envoie une exception. A surcharger
   */
  @Override
  public double getPressureLoss() {
    errorNotSupported();
    return 0;
  }

  /**
   * Envoie une exception. A surcharger
   */
  @Override
  public double getQ() {
    errorNotSupported();
    return 0;
  }

  /**
   * Envoie une exception. A surcharger
   */
  @Override
  public double getTracer() {
    errorNotSupported();
    return 0;
  }

  /**
   * Envoie une exception. A surcharger
   */
  @Override
  public double getTracerCoeffA() {
    errorNotSupported();
    return 0;
  }

  /**
   * Envoie une exception. A surcharger
   */
  @Override
  public double getTracerCoeffB() {
    errorNotSupported();
    return 0;
  }

  /**
   * @return La vitesse u
   */
  @Override
  public double getU() {
    return u_;
  }

  /**
   * @return la vitesse v
   */
  @Override
  public double getV() {
    return v_;
  }

  /**
   * Utilise le fait que cette classe derive de H2dVariableTypeContainer.
   * 
   * @param _t la variable demandee
   * @return la variable de cette condition limite.
   */
  public double getValue(final H2dVariableType _t) {
    return _t.getValue(this);
  }

  /**
   * Envoie une exception. A surcharger.
   */
  @Override
  public double getVelocity() {
    errorNotSupported();
    return 0;
  }

  /**
   * Envoie une exception. A surcharger.
   */
  @Override
  public double getViscosity() {
    errorNotSupported();
    return 0;
  }
}