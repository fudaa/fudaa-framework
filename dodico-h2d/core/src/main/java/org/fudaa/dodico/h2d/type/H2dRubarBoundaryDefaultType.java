/*
 * @creation 7 juil. 2004
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import java.util.List;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluPermanentList;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarBoundaryDefaultType.java,v 1.5 2006-09-19 14:43:25 deniger Exp $
 */
public class H2dRubarBoundaryDefaultType extends H2dRubarBoundaryType {

  private CtuluPermanentList list_;

  private CtuluPermanentList listTrans_;

  /**
   * @param _n le nom du bord
   * @param _isSolid true si solide
   * @param _rubarIdx l'id rubar
   * @param _o liste des variables autorisees pour ce type de bord
   */
  public H2dRubarBoundaryDefaultType(final String _n, final boolean _isSolid, final int _rubarIdx, final Object[] _o, final Object[] _oTransport) {
    super(_n, _isSolid, _rubarIdx);
    if (_o != null) {
      list_ = new CtuluPermanentList(CtuluLibArray.sort(_o));
    }
    if (_oTransport != null) {
      listTrans_ = new CtuluPermanentList(CtuluLibArray.sort(_oTransport));
    }
  }

  /**
   * Construit un bord liquide.
   *
   * @param _n le nom du bord
   * @param _rubarIdx l'id rubar
   * @param _o liste des variables autorisees pour ce type de bord
   */
  public H2dRubarBoundaryDefaultType(final String _n, final int _rubarIdx, final Object[] _o, final Object[] _oTransport) {
    this(_n, false, _rubarIdx, _o, _oTransport);
  }

  /**
   * Construit un bord liquide sans variables autorisees.
   *
   * @param _n le nom du bord
   * @param _rubarIdx l'id rubar
   */
  public H2dRubarBoundaryDefaultType(final String _n, final int _rubarIdx) {
    super(_n, false, _rubarIdx);
  }

  /**
   * Construit un bord liquide.
   *
   * @param _n le nom du bord
   * @param _rubarIdx l'id rubar
   * @param _l la liste des variables autorisees.
   */
  public H2dRubarBoundaryDefaultType(final String _n, final int _rubarIdx, final CtuluPermanentList _l) {
    super(_n, false, _rubarIdx);
    list_ = _l;
  }

  public H2dRubarBoundaryDefaultType(final String _n, final int _rubarIdx, final CtuluPermanentList _l, final CtuluPermanentList _transport) {
    super(_n, false, _rubarIdx);
    list_ = _l;
    listTrans_ = _transport;
  }

  @Override
  public List getAllowedVariable(final H2dRubarProjetType _t) {
    if (_t == H2dRubarProjetType.TRANSPORT) {
      return listTrans_;
    }
    return list_;
  }

}