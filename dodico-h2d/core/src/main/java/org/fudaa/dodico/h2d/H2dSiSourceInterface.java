/*
 *  @creation     19 avr. 2005
 *  @modification $Date: 2007-04-16 16:34:21 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import java.util.List;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: H2dSiSourceInterface.java,v 1.7 2007-04-16 16:34:21 deniger Exp $
 */
public interface H2dSiSourceInterface extends H2dVariableProviderContainerInterface {

  /**
   * @return le maillage
   */
  EfGridInterface getMaillage();

  List getVariables();

  int testCoteEau(CtuluListSelectionInterface _selection, CtuluCommandContainer _cmd);

  /**
   * @param _var la variable init demandee
   * @return le tableau des valeurs
   */
  CtuluCollectionDoubleEdit getModifiableModel(H2dVariableType _var);

  /**
   * @param _listener le listener a ajouter
   */
  void addListener(final H2dSIListener _listener);

  void removeListener(final H2dSIListener _listener);

  /**
   * @return les valeurs de la cote d'eau sur tous les points
   */
  double[] getZe();

  /**
   * @return les valeurs de la vitesse u sur tous les points
   */
  double[] getU();

  /**
   * @return les valeurs de la vitesses v sur tous les points
   */
  double[] getV();

  /**
   * @return les valeurs de la bathy
   */
  CtuluCollectionDouble getBathyModel();

  /**
   * @param _ze les nouvelles cotes
   * @param _u les nouvelles vitesses u
   * @param _v les nouvelles vitesses v
   * @param _cmd le receveur de commande.
   */
  void set(double[] _ze, double[] _u, double[] _v, CtuluCommandContainer _cmd);

  void setHToZero(int[] _idx, CtuluCommandContainer _cmd);

  /**
   * @return true si les solutions initiales ont ete activees
   */
  boolean isSolutionInitialesActivated();

}
