/*
 * @creation 28 sept. 2004
 * @modification $Date: 2007-01-10 09:04:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import gnu.trove.TIntDoubleHashMap;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;

/**
 * @author Fred Deniger
 * @version $Id: H2dRegularGridData.java,v 1.16 2007-01-10 09:04:27 deniger Exp $
 */
public class H2dRegularGridData implements H2dRegularGridDataInterface {
  private class CommandSetGrid implements CtuluCommand {
    H2dRegularGrid newGrid_;
    H2dRegularGrid oldGrid_;
    CtuluCollectionDoubleEdit[] oldModel_;

    protected CommandSetGrid(final H2dRegularGrid _old, final CtuluCollectionDoubleEdit[] _olds) {
      oldModel_ = _olds;
      oldGrid_ = _old;
    }

    @Override
    public void redo() {
      grid_.initFromGrid(newGrid_);
      if (oldModel_ != null) {
        for (int i = values_.length - 1; i >= 0; i--) {
          values_[i] = null;
        }
      }
      fireGridChanged(oldModel_ != null);
    }

    @Override
    public void undo() {
      newGrid_ = new H2dRegularGrid(grid_);
      grid_.initFromGrid(oldGrid_);
      if (oldModel_ != null) {
        for (int i = values_.length - 1; i >= 0; i--) {
          values_[i] = oldModel_[i];
        }
      }
      fireGridChanged(oldModel_ != null);
    }
  }

  protected CtuluCollectionDoubleEdit[] values_;
  TIntDoubleHashMap defaultValues_;
  H2dRegularGrid grid_;

  protected CtuluCollectionDoubleEdit[] getValues() {
    return values_;
  }

  protected void initValFrom(final H2dRegularGridDataInterface _interface) {
    grid_ = new H2dRegularGrid(_interface);
    final int max = Math.min(values_.length, _interface.getNbValues()) - 1;
    for (int i = max; i >= 0; i--) {
      values_[i] = createModel(i, _interface.getDoubleModel(i));
    }
  }

  public H2dRegularGridData(final int _nbValues) {
    grid_ = new H2dRegularGrid();
    values_ = new CtuluCollectionDoubleEdit[_nbValues];
  }

  protected void addData(int nbToAdd, final CtuluCommandContainer _cmd) {
    final CtuluCollectionDoubleEdit[] toAdd = new CtuluCollectionDoubleEdit[nbToAdd];
    for (int i = 0; i < nbToAdd; i++) {
      toAdd[i] = createModel(values_.length + i);
    }
    addData(toAdd, _cmd);
  }

  /**
   * Ajoute a la fin le modele passe en parametre. N'envoie pas d'evt et ne fait pas de copie de _m.
   *
   * @param _m le modele a ajouter
   */
  protected void addData(final CtuluCollectionDoubleEdit[] _m, final CtuluCommandContainer _cmd) {

    final CtuluCollectionDoubleEdit[] newModel = new CtuluCollectionDoubleEdit[values_.length + _m.length];
    System.arraycopy(values_, 0, newModel, 0, values_.length);
    for (int i = 0; i < _m.length; i++) {
      newModel[values_.length + i] = _m[i];
      if (_m[i].getSize() != getNbPoint()) {
        throw new IllegalArgumentException();
      }
    }

    values_ = newModel;

    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          addData(_m, null);
        }

        @Override
        public void undo() {
          removeLastData(1, null);
        }
      });
    }

    fireDataNombreChanged();
  }

  protected CtuluCollectionDoubleEdit createModel(final int _idx) {
    return createModel(_idx, null);
  }

  /**
   * A surcharger pour creer ces propres modeles :utile pour les evt.
   *
   * @return un modele neuf.
   */
  protected CtuluCollectionDoubleEdit createModel(final int _idx, final CtuluCollectionDoubleEdit _m) {
    if (_m == null) {
      final double initVal = getDefaultValues(_idx);
      if (initVal != 0) {
        final CtuluArrayDouble r = new CtuluArrayDouble(getNbPoint());
        r.setAll(initVal, null);
      }

      return new CtuluArrayDouble(getNbPoint());
    }

    if (_m.getSize() != getNbPoint()) {
      throw new IllegalArgumentException();
    }
    return new CtuluArrayDouble(_m);
  }

  protected void fireDataChanged() {

  }

  protected void fireDataNombreChanged() {

  }

  protected void fireGridChanged(final boolean _nbPointChanged) {

  }

  /**
   * @return true si au moins un jeu de donnee est different de zero.
   */
  protected boolean isAValueSet() {
    for (int i = values_.length - 1; i >= 0; i--) {
      if (values_[i] != null) {
        return true;
      }
    }
    return false;
  }

  protected void removeLastData(final int nb, final CtuluCommandContainer _cmd) {
    if (values_.length == 0) {
      return;
    }
    final CtuluCollectionDoubleEdit[] r = new CtuluCollectionDoubleEdit[nb];
    for (int i = 0; i < nb; i++) {
      r[i] = values_[values_.length - nb + i];
    }
    final CtuluCollectionDoubleEdit[] newModel = new CtuluCollectionDoubleEdit[values_.length - nb];
    System.arraycopy(values_, 0, newModel, 0, newModel.length);
    values_ = newModel;
    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          removeLastData(nb, null);
        }

        @Override
        public void undo() {
          addData(r, null);
        }
      });
    }
    fireDataNombreChanged();
  }

  protected void setNbValue(final int _i) {
    if (_i != values_.length) {
      final CtuluCollectionDoubleEdit[] temp = new CtuluCollectionDoubleEdit[_i];
      final int nb = Math.min(values_.length, _i);
      System.arraycopy(values_, 0, temp, 0, nb);
      values_ = temp;
    }
  }

  public double getDefaultValues(final int _idx) {
    return defaultValues_ == null ? 0 : defaultValues_.get(_idx);
  }

  /**
   * @param _idxVal
   * @return le model pour la valeur d'indice _idxVal.
   */
  @Override
  public CtuluCollectionDoubleEdit getDoubleModel(final int _idxVal) {
    CtuluCollectionDoubleEdit m = values_[_idxVal];
    if (m == null) {
      m = createModel(_idxVal, null);
      values_[_idxVal] = m;
    }
    return m;
  }

  @Override
  public CtuluCollectionDoubleEdit getDoubleValues(final int _i) {
    return getDoubleModel(_i);
  }

  @Override
  public double getDX() {
    return grid_.dx_;
  }

  @Override
  public double getDY() {
    return grid_.dy_;
  }

  public H2dRegularGridInterface getGridInterface() {
    return grid_;
  }

  /**
   * @return le nombre de point de la grille
   */
  @Override
  public int getNbPoint() {
    return grid_.getNbPoint();
  }

  @Override
  public int getNbPtOnX() {
    return grid_.nbX_;
  }

  @Override
  public int getNbPtOnY() {
    return grid_.nbY_;
  }

  public double getX(final int _i) {
    final int col = _i / getNbPtOnY();
    return getXmin() + col * getDX();
  }

  public double getY(final int _i) {
    final int col = _i / getNbPtOnY();
    final int ligne = _i - col * getNbPtOnY();
    return getYmin() + ligne * getDY();
  }

  @Override
  public int getNbValues() {
    return values_ == null ? 0 : values_.length;
  }

  @Override
  public Object getValueIdentifier(final int _i) {
    return null;
  }

  @Override
  public double getValues(final int _valIdx, final int _idxPt) {
    final CtuluCollectionDoubleEdit m = values_[_valIdx];
    if (m == null) {
      return getDefaultValues(_valIdx);
    }
    return m.getValue(_idxPt);
  }

  @Override
  public double getX0() {
    return grid_.x0_;
  }

  public double getXmax() {
    return grid_.getXmax();
  }

  public double getXmin() {
    return grid_.getXmin();
  }

  @Override
  public double getY0() {
    return grid_.y0_;
  }

  public double getYmax() {
    return grid_.getYmax();
  }

  public double getYmin() {
    return grid_.getYmin();
  }

  public void setDefaultValues(final int _idx, final double _val) {
    if (defaultValues_ == null) {
      defaultValues_ = new TIntDoubleHashMap(getNbValues());
    }
    defaultValues_.put(_idx, _val);
  }

  /**
   * @param _g la nouvelle grille
   * @param _cmd le receveur de commande
   * @return true si changement
   */
  public boolean setGrid(final H2dRegularGridInterface _g, final CtuluCommandContainer _cmd) {
    // les grilles sont equivalentes : on oublie
    if (grid_.isEquivalent(_g)) {
      return false;
    }
    // on fait une sauvegarde de l'ancienne grille
    final H2dRegularGrid oldGrid = new H2dRegularGrid(grid_);
    // on met a jour la grille
    grid_.initFromGrid(_g);
    // true si le nb de point a ete modifie
    final boolean nbPointChanged = oldGrid.getNbPoint() != grid_.getNbPoint();
    CtuluCollectionDoubleEdit[] oldDatas = null;
    // on enregistre les anciennes valeurs si necessaire
    if (nbPointChanged) {
      if (_cmd != null) {
        oldDatas = new CtuluCollectionDoubleEdit[values_.length];
      }
      for (int i = values_.length - 1; i >= 0; i--) {
        if (_cmd != null && oldDatas != null) {
          oldDatas[i] = values_[i];
        }
        values_[i] = null;
      }
    }
    // on enregistre la commande
    if (_cmd != null) {
      _cmd.addCmd(new CommandSetGrid(oldGrid, oldDatas));
    }
    fireGridChanged(nbPointChanged);
    return true;
  }
}
