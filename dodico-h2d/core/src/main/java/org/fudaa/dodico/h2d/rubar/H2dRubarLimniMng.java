/*
 * @creation 21 d�c. 2004
 * @modification $Date: 2007-04-30 14:21:34 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntHashSet;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.collection.CtuluListInteger;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarLimniMng.java,v 1.15 2007-04-30 14:21:34 deniger Exp $
 */
public class H2dRubarLimniMng implements H2dRubarDTRResult {

  private class IntegerModel extends CtuluListInteger {

    public IntegerModel() {
      super();
    }

    public IntegerModel(final int _init) {
      super(_init);
    }

    @Override
    protected void fireDataAdded() {
      fireEltIdxChanged();
    }

    @Override
    protected void fireDataChanged() {
      fireEltIdxChanged();
    }

    @Override
    protected void fireDataRemoved() {
      fireEltIdxChanged();
    }

    @Override
    protected void fireIntStructureChanged() {
      fireEltIdxChanged();
    }

  }

  public final Envelope getEnv() {
    if (env_ == null) {
      env_ = new Envelope();
      for (int i = getNbPoint() - 1; i >= 0; i--) {
        env_.expandToInclude(getX(i), getY(i));
      }
    }
    return env_;
  }

  /**
   * @param _grid
   */
  public H2dRubarLimniMng(final EfGridInterface _grid) {
    super();
    grid_ = _grid;
  }

  /**
   * Reinitialise toutes les valeurs.
   * 
   * @param _g le maillage non stock�
   * @param _res les points de limni
   * @param _analyze le receveur d'info
   * @return le manager des elts limni
   */
  public static H2dRubarLimniMng init(final EfGridInterface _g, final H2dRubarDTRResult _res,
      final CtuluAnalyze _analyze) {
    final H2dRubarLimniMng r = new H2dRubarLimniMng(_g);
    r.initLimni(_res, _analyze);
    return r;
  }

  private final EfGridInterface grid_;

  private CtuluListInteger idxElt_;

  List listener_;

  double timeStepStock_;

  Coordinate tmp_;

  private void initTmp() {
    if (tmp_ == null) {
      tmp_ = new Coordinate();
    }
  }

  protected void createModel() {
    if (idxElt_ == null) {
      idxElt_ = new IntegerModel();
    }
  }

  protected void fireEltIdxChanged() {
    env_ = null;
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext();) {
        ((H2dRubarLimniListener) it.next()).limniPointChanged(this);
      }
    }
  }

  protected void fireTimeStepChanged() {
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext();) {
        ((H2dRubarLimniListener) it.next()).limniTimeStepChanged(this);
      }
    }
  }

  /**
   * @param _cmd le receveur de commanfe
   */
  public boolean add(final double _x, final double _y, final CtuluCommandContainer _cmd) {
    initTmp();
    final int idx = grid_.getEltContainingXY(_x, _y, tmp_);
    if (idx >= 0 && (idxElt_ == null || !idxElt_.contains(idx))) {
      createModel();
      idxElt_.add(idx, _cmd);
      return true;
    }
    return false;
  }

  public boolean add(final int _idx, final CtuluCommandContainer _cmd) {
    if (_idx >= 0 && (idxElt_ == null || !idxElt_.contains(_idx))) {
      createModel();
      idxElt_.add(_idx, _cmd);
      return true;
    }
    return false;
  }

  /**
   * @param _limni le listener a ajouter
   */
  public void addListener(final H2dRubarLimniListener _limni) {
    if (_limni == null) {
      return;
    }
    if (listener_ == null) {
      listener_ = new ArrayList();
    }
    listener_.add(_limni);
  }

  /**
   * @return l'interface pour le fichier DTR.
   */
  public H2dRubarDTRResult createResultAdapter() {
    return this;
  }

  public int getEltContaining(final double _x, final double _y) {
    initTmp();
    return grid_.getEltContainingXY(_x, _y, tmp_);
  }

  public final EfGridInterface getGrid() {
    return grid_;
  }

  public final int getIdxElt(final int _i) {
    return idxElt_.getValue(_i);
  }

  /**
   * @return le nombre de points limni
   */
  public int getNbLimni() {
    return idxElt_ == null ? 0 : idxElt_.getSize();
  }

  @Override
  public int getNbPoint() {
    return getNbLimni();
  }

  public double getStockage() {
    return timeStepStock_;
  }

  @Override
  public double getTimeStep() {
    return timeStepStock_;
  }

  Envelope env_;

  @Override
  public double getX(final int _i) {
    if (idxElt_.getValue(_i) >= 0) {
      return grid_.getMoyCentreXElement(idxElt_.getValue(_i));
    }
    return 999.99;
  }

  @Override
  public NumberFormat getXYFormatter() {
    return H2dRubarNumberFormatter.DEFAULT_XY_FMT;
  }

  @Override
  public double getY(final int _i) {
    if (idxElt_.getValue(_i) >= 0) {
      return grid_.getMoyCentreYElement(idxElt_.getValue(_i));
    }
    return 999.99;
  }

  public double getZ(final int _i) {
    if (idxElt_.getValue(_i) >= 0) {
      return grid_.getMoyCentreZElement(idxElt_.getValue(_i));
    }
    return 999.99;
  }

  public boolean initLimni(final H2dRubarDTRResult _res, final CtuluAnalyze _analyze) {
    return initLimni(_res, _analyze, false);
  }

  /**
   * Si keepBadLimni est a true, on doit garder les indices meme les -1.
   * 
   * @param _analyze le receveur d'info
   * @param _res
   */
  /**
   * @param _res
   * @param _analyze
   * @param _keepBadLimni
   * @return true si modification
   */
  public boolean initLimni(final H2dRubarDTRResult _res, final CtuluAnalyze _analyze, final boolean _keepBadLimni) {
    timeStepStock_ = _res.getTimeStep();
    // pour ne pas dupliquer les entiers
    final int n = _res.getNbPoint();
    final TIntHashSet s = new TIntHashSet(n);
    // pour garder les entiers dans l'ordre
    final TIntArrayList list = new TIntArrayList(n);
    final TIntArrayList wrongPoints = new TIntArrayList(n / 2);
    final double maxDistForWrongPoints = 1E-2;
    initTmp();
    // on garde que les limnigrammes appartenant a un element du maillage et on supprime
    // les dupliqu�s.
    for (int i = 0; i < n; i++) {
      final int idxElt = grid_.getEltContainingXY(_res.getX(i), _res.getY(i), tmp_);
      if (_keepBadLimni || (idxElt >= 0 && !s.contains(idxElt))) {
        s.add(idxElt);
        list.add(idxElt);
        // on teste le point donn� pour savoir s'il n'est pas trop loin du centre de la maille.
        if (idxElt >= 0) {
          if (CtuluLibGeometrie.getDistance(_res.getX(i), _res.getY(i), grid_.getMoyCentreXElement(idxElt), grid_
              .getMoyCentreYElement(idxElt)) > maxDistForWrongPoints) {
            wrongPoints.add(i);
          }

        }
      }
    }
    boolean res = false;
    if (s.size() != _res.getNbPoint()) {
      res = true;
      final String txt = H2dResource.getS("Certains points de stockage de limnigrammes sont ignor�s");
      if (_analyze != null) {
        _analyze.addWarn(txt, -1);
      }
      CtuluLibMessage.info(txt);
    }
    if (wrongPoints.size() > 0) {
      res = true;
      final String txt = H2dResource
          .getS("Des points de stockage sont mal positionn�s. Ils ont �t� replac�s au centre de l'�l�ment englobant.");
      if (_analyze != null) {
        _analyze.addWarn(txt, -1);
      }
      CtuluLibMessage.info(txt + CtuluLibString.LINE_SEP + CtuluLibString.arrayToString(wrongPoints.toNativeArray()));
    }
    if (idxElt_ == null) {
      idxElt_ = new IntegerModel(s.size());
    } else {
      idxElt_.removeAll();
    }
    idxElt_.addAll(list.toNativeArray(), null);
    return res;
  }

  public boolean isAlreadySet(final Coordinate _c) {
    if (idxElt_ == null) {
      return false;
    }
    initTmp();
    final int idx = grid_.getEltContainingXY(_c.x, _c.y, tmp_);
    return idxElt_.contains(idx);
  }

  /**
   * @return true si vide
   */
  public boolean isEmpty() {
    return getNbLimni() == 0;
  }

  /**
   * @param _idx les indices des elements a enlever
   * @param _cmd le receveur de commandes
   */
  public void removeLimni(final int[] _idx, final CtuluCommandContainer _cmd) {
    if (idxElt_ != null) {
      idxElt_.remove(_idx, _cmd);
    }
  }

  /**
   * @param _limni le listener a enlever
   */
  public void removeListener(final H2dRubarLimniListener _limni) {
    if ((listener_ == null) || (_limni == null)) {
      return;
    }
    listener_.remove(_limni);
  }

  /**
   * @param _t le nouveau pas de temps de stockage
   * @param _cmd le receveur de commande
   */
  public void setTimeStepStockage(final double _t, final CtuluCommandContainer _cmd) {
    if (_t != timeStepStock_ && _t >= 0) {
      final double old = timeStepStock_;
      timeStepStock_ = _t;
      if (_cmd != null) {
        _cmd.addCmd(new CtuluCommand() {

          @Override
          public void redo() {
            setTimeStepStockage(_t, null);
          }

          @Override
          public void undo() {
            setTimeStepStockage(old, null);
          }
        });
      }
      fireTimeStepChanged();
    }
  }
}
