/*
 * @creation 18 oct. 2004
 * @modification $Date: 2007-04-30 14:21:34 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;

/**
 * @author Fred Deniger
 * @version $Id: H2DRubarDiffusionModel.java,v 1.4 2007-04-30 14:21:34 deniger Exp $
 */
public final class H2DRubarDiffusionModel extends CtuluArrayDouble {

  Set diffusionListener_;

  /**
   * @param _init
   */
  public H2DRubarDiffusionModel(final double[] _init) {
    super(_init);
  }

  /**
   * @param _init intialisation
   */
  public H2DRubarDiffusionModel(final CtuluCollectionDoubleEdit _init) {
    super(_init);
  }

  /**
   * @param _nb
   */
  public H2DRubarDiffusionModel(final int _nb) {
    super(_nb);
  }

  public void addListener(final H2DRubarDiffusionListener _si) {
    if (_si == null) {
      return;
    }
    if (diffusionListener_ == null) {
      diffusionListener_ = new HashSet();
    }
    diffusionListener_.add(_si);
  }

  public void addListeners(final Collection _si) {
    if (_si == null) {
      return;
    }
    if (diffusionListener_ == null) {
      diffusionListener_ = new HashSet();
    }
    diffusionListener_.addAll(_si);
  }

  public Set getListeners() {
    return new HashSet(diffusionListener_);
  }

  public void removeListener(final H2DRubarDiffusionListener _si) {
    if ((diffusionListener_ == null) || (_si == null)) {
      return;
    }
    diffusionListener_.remove(_si);
  }

  @Override
  protected void fireObjectChanged(int _indexGeom, Object _newValue) {
    fireObjectChanged(diffusionListener_,false);
  }
  protected void fireObjectChanged(final boolean _b) {
    fireObjectChanged(diffusionListener_,_b);
  }

  protected static void fireObjectChanged(final Set _s,final boolean _add) {
    if (_s != null) {
      for (final Iterator it = _s.iterator(); it.hasNext();) {
        ((H2DRubarDiffusionListener) it.next()).diffusionChanged(_add);
      }
    }
  }
}