/*
 * @creation 28 sept. 2004
 * @modification $Date: 2007-03-27 16:09:59 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.ctulu.collection.CtuluContainerModelDouble;

/**
 * @author Fred Deniger
 * @version $Id: H2dRegularGridDataInterface.java,v 1.10 2007-03-27 16:09:59 deniger Exp $
 */
public interface H2dRegularGridDataInterface extends H2dRegularGridInterface, CtuluContainerModelDouble {
  double getValues(int _valIdx, int _idxPt);

  CtuluCollectionDoubleEdit getDoubleModel(int _idx);
}