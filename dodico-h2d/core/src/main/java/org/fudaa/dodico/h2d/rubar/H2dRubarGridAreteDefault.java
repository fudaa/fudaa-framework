/**
 * @creation 9 juin 2004
 * @modification $Date: 2007-04-30 14:21:34 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGridSourcesAbstract;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarGridAreteDefault.java,v 1.3 2007-04-30 14:21:34 deniger Exp $
 */
public class H2dRubarGridAreteDefault extends EfGridSourcesAbstract implements H2dRubarGridAreteSource {

  H2dRubarGrid grid_;
  /**
   * les elements voisins par aretes.
   */
  int[][] iaElementVoisin_;
  int[] idxAreteLimiteEntrante_;
  /**
   * les elements voisins par elements.
   */
  int[][] ieElementVoisin_;

  int nbDecimal_;

  /**
   * @param _g le maillage volumique
   * @param _ieVoisinParElt le tableau des elt voisins pour chaque elt
   * @param _eltVoisinParArete le tableau des elt voisins pour chaque arete
   */
  public H2dRubarGridAreteDefault(final H2dRubarGrid _g, final int[][] _ieVoisinParElt, final int[][] _eltVoisinParArete) {
    grid_ = _g;
    ieElementVoisin_ = _ieVoisinParElt;
    iaElementVoisin_ = _eltVoisinParArete;
  }

  /**
   * @see org.fudaa.dodico.h2d.rubar.H2dRubarGridAreteSource#elementsVoisinsParArete()
   */
  @Override
  public int[][] elementsVoisinsParArete() {
    return iaElementVoisin_;
  }

  /**
   * @see org.fudaa.dodico.h2d.rubar.H2dRubarGridAreteSource#elementsVoisinsParElement()
   */
  @Override
  public int[][] elementsVoisinsParElement() {
    return ieElementVoisin_;
  }

  /**
   * @see org.fudaa.dodico.h2d.rubar.H2dRubarGridAreteSource#getGrid()
   */
  @Override
  public EfGridInterface getGrid() {
    return grid_;
  }

  @Override
  public int[] getIdxAreteLimiteEntrante() {
    return idxAreteLimiteEntrante_;
  }

  @Override
  public int getNbDecimal() {
    return nbDecimal_;
  }

  @Override
  public H2dRubarGrid getRubarGrid() {
    return grid_;
  }

  /**
   * L'ordre est important car utilise dans d'autre fichier.
   * 
   * @param _idxAreteLimiteEntrante les indices des aretes ouvertes.
   */
  public void setIdxAreteLimiteEntrante(final int[] _idxAreteLimiteEntrante) {
    idxAreteLimiteEntrante_ = _idxAreteLimiteEntrante;
  }

  public void setNbDecimal(final int _nbDecimal) {
    nbDecimal_ = _nbDecimal;
  }

}