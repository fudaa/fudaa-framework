package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;

public class H2dRubarSedimentVariableType extends H2dVariableTransType {
  final int coucheIdx;
  private final H2dVariableTransType parentVariable;

  public H2dRubarSedimentVariableType(int couche, H2dVariableTransType parentVariable) {
    super(parentVariable.getName() + " " + H2dResource.getS("Couche {0}", Integer.toString(couche + 1)), null, "sed_" + parentVariable.getShortName() + "_" + (couche + 1));
    this.coucheIdx = couche;
    this.parentVariable = parentVariable;
  }

  public int getCoucheIdx() {
    return coucheIdx;
  }

  @Override
  public H2dVariableTransType getParentVariable() {
    return parentVariable;
  }
}
