/*
 *  @creation     27 nov. 2003
 *  @modification $Date: 2007-06-14 11:59:08 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import com.memoire.fu.FuLog;
import org.fudaa.dodico.h2d.H2dNodalPropertyMixte;
import org.fudaa.dodico.h2d.type.H2dProjetType;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author deniger
 * @version $Id: H2dRefluxElementPropertyCourant2dMng.java,v 1.18 2007-06-14 11:59:08 deniger Exp $
 */
public class H2dRefluxElementPropertyCourant2dMng extends H2dRefluxElementPropertyMngAbstract {

  boolean isLmg_;

  /**
   * @param _mixte les proprietes lues
   * @param _isLmg true si longueur de melange
   * @param _nbElt le nombre total d'element pour le projet
   */
  public H2dRefluxElementPropertyCourant2dMng(final H2dNodalPropertyMixte[] _mixte, final boolean _isLmg,
      final int _nbElt) {
    super(_nbElt);
    this.isLmg_ = _isLmg;
    if ((_mixte == null) || (_mixte.length != 4)) {

      if ((_mixte != null) && (_mixte.length != 4)) {
        FuLog.warning("DH2: NOMBRE DE PROPRIETE INVALIDE " + _mixte.length);
      }
      addProp(H2dVariableType.VISCOSITE, H2dRefluxElementProperty.getElementProperty(H2dVariableType.VISCOSITE, _nbElt));
      addProp(H2dVariableType.RUGOSITE, H2dRefluxElementProperty.getElementProperty(H2dVariableType.RUGOSITE, _nbElt));
      if (_isLmg) {
        addProp(H2dVariableTransType.ALPHA_LONGUEUR_MELANGE, new H2dRefluxElementProperty(
            H2dVariableTransType.ALPHA_LONGUEUR_MELANGE.getName(), _nbElt));
      }
      addProp(H2dVariableType.PERTE_CHARGE, H2dRefluxElementProperty.getElementProperty(H2dVariableType.PERTE_CHARGE,
          _nbElt));
    } else {
      addProp(H2dVariableType.VISCOSITE, H2dRefluxElementProperty.getElementProperty(H2dVariableType.VISCOSITE,
          _mixte[0], _nbElt));
      addProp(H2dVariableType.RUGOSITE, H2dRefluxElementProperty.getElementProperty(H2dVariableType.RUGOSITE,
          _mixte[1], _nbElt));
      if (_isLmg) {
        addProp(H2dVariableTransType.ALPHA_LONGUEUR_MELANGE, H2dRefluxElementProperty.getElementProperty(
            H2dVariableTransType.ALPHA_LONGUEUR_MELANGE, _mixte[2], _nbElt));
      }
      addProp(H2dVariableType.PERTE_CHARGE, H2dRefluxElementProperty.getElementProperty(H2dVariableType.PERTE_CHARGE,
          _mixte[3], _nbElt));
    }
    initVar();
  }

  protected boolean setProjectType(final H2dProjetType _t, int _nbElt) {
    if (_t == H2dProjetType.COURANTOLOGIE_2D) {
      return setLmg(false, _nbElt);
    } else if (_t == H2dProjetType.COURANTOLOGIE_2D_LMG) {
      return setLmg(true, _nbElt);
    }
    new Throwable().printStackTrace();
    return false;
  }

  protected boolean setLmg(final boolean _b, int _nbElt) {
    if (isLmg_ != _b) {
      isLmg_ = _b;
      initVar();
      if (isLmg_) {
        addProp(H2dVariableTransType.ALPHA_LONGUEUR_MELANGE, new H2dRefluxElementProperty(
            H2dVariableTransType.ALPHA_LONGUEUR_MELANGE.getName(), _nbElt));
      } else {
        removeProp(H2dVariableTransType.ALPHA_LONGUEUR_MELANGE);
      }
      return true;
    }
    return false;
  }

  /**
   *
   */
  @Override
  public H2dNodalPropertyMixte[] getPropertiesInInpOrder() {
    final H2dNodalPropertyMixte[] r = new H2dNodalPropertyMixte[4];
    r[0] = getProp(H2dVariableType.VISCOSITE);
    r[1] = getProp(H2dVariableType.RUGOSITE);
    if (isLmg_) {
      r[2] = getProp(H2dVariableTransType.ALPHA_LONGUEUR_MELANGE);
    } else {
      r[2] = H2dNodalPropertyMixte.createPermanentNull(nbElt_);
    }
    r[3] = getProp(H2dVariableType.PERTE_CHARGE);
    return r;
  }

  @Override
  public boolean projectTypeCanBeChanged(final H2dProjetType _new) {
    return (_new == H2dProjetType.COURANTOLOGIE_2D) || (_new == H2dProjetType.COURANTOLOGIE_2D_LMG);
  }
}