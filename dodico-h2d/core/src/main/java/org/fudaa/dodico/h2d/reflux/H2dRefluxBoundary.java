/*
 * @creation 13 nov. 2003
 * @modification $Date: 2006-09-19 14:43:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import org.fudaa.dodico.h2d.H2dBcListenerDispatcher;
import org.fudaa.dodico.h2d.H2dBoundary;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;

/**
 * @author deniger
 * @version $Id: H2dRefluxBoundary.java,v 1.12 2006-09-19 14:43:23 deniger Exp $
 */
public class H2dRefluxBoundary extends H2dBoundary {

  /**
   * Un iterateur sur les indices milieux d'un bord.
   *
   * @author Fred Deniger
   * @version $Id: H2dRefluxBoundary.java,v 1.12 2006-09-19 14:43:23 deniger Exp $
   */
  public static final class RefluxBordMiddleIndexIterator {

    private int indexEnCours_;
    private int indexFinal_;

    private int nbPt_;

    public RefluxBordMiddleIndexIterator() {}

    /**
     * @param _nbPt le nombre de point de la frontiere englobante
     * @param _b le bord concerne
     */
    public RefluxBordMiddleIndexIterator(final int _nbPt, final H2dRefluxBoundary _b) {
      set(_nbPt, _b);
    }

    /**
     * @param _nbPt le nombre de point de la frontiere englobante
     * @param _min le min du bord concerne
     * @param _max le max du bord concerne
     */
    public RefluxBordMiddleIndexIterator(final int _nbPt, final int _min, final int _max) {
      set(_nbPt, _min, _max);
    }

    /**
     * @return true si possede un point suivant
     */
    public boolean hasNext() {
      return indexEnCours_ <= indexFinal_;
    }

    /**
     * Avance par pas de deux (indices milieux).
     *
     * @return l'index sur la frontiere
     */
    public int next() {
      if (indexEnCours_ > indexFinal_) {
        throw new IllegalAccessError("end of iterator");
      }
      final int r = (indexEnCours_ < nbPt_) ? indexEnCours_ : indexEnCours_ - nbPt_;
      indexEnCours_ += 2;
      return r;
    }

    /**
     * @param _nbPt le nombre de points de la frontiere
     * @param _b le bord a parcourir.
     */
    public void set(final int _nbPt, final H2dBoundary _b) {
      nbPt_ = _nbPt;
      // bd_= _b;
      final int d = _b.getIdxDeb();
      final int f = _b.getIdxFin();
      if (_b.isUnique()) {
        indexEnCours_ = 1;
        indexFinal_ = nbPt_ - 1;
      } else if (d > f) {
        indexEnCours_ = d + 1;
        indexFinal_ = nbPt_ + f - 1;
      } else {
        indexEnCours_ = d + 1;
        indexFinal_ = f - 1;
      }
    }

    /**
     * @param _nbPt le nombre de points de la frontiere
     * @param _min l'indice min du bord
     * @param _max l'indice max du bord
     */
    public void set(final int _nbPt, final int _min, final int _max) {
      nbPt_ = _nbPt;
      // bd_= _b;
      final int d = _min;
      final int f = _max;
      if (_min == _max) {
        indexEnCours_ = 1;
        indexFinal_ = nbPt_ - 1;
      } else if (d > f) {
        indexEnCours_ = d + 1;
        indexFinal_ = nbPt_ + f - 1;
      } else {
        indexEnCours_ = d + 1;
        indexFinal_ = f - 1;
      }
    }
  }

  public H2dRefluxBoundary(final int _idxFr) {
    super(_idxFr);
  }

  /**
   * @param _b bord servant a l'initialisation
   */
  public H2dRefluxBoundary(final H2dBoundary _b) {
    super(_b);
  }

  @Override
  protected boolean setBoundaryType(final H2dBoundaryType _type, final H2dBcListenerDispatcher _l) {
    return super.setBoundaryType(_type, _l);
  }

  
  /**
   * Redefinie pour assurer la visibilite dans les inner class.
   */
  @Override
  protected void setIdxDeb(final int _i) {
    super.setIdxDeb(_i);
  }

  /**
   * Redefinie pour assurer la visibilite dans les inner class.
   */
  @Override
  protected void setIdxFin(final int _i) {
    super.setIdxFin(_i);
  }

  /**
   * Redefinie pour assurer la visibilite dans les inner class.
   */
  @Override
  protected void setIdxmaillageFrontiere(final int _i) {
    super.setIdxmaillageFrontiere(_i);
  }

  /**
   * @param _nb le nombre de point de la frontiere englobante
   * @return un iterateur sur les indices de ce bord.
   */
  public RefluxBordMiddleIndexIterator createMidIt(final int _nb) {
    return new RefluxBordMiddleIndexIterator(_nb, this);
  }
}