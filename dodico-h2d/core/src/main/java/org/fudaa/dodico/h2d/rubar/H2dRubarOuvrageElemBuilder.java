/*
 * @creation 3 janv. 2005
 * 
 * @modification $Date: 2007-05-04 13:46:38 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import java.util.ArrayList;
import java.util.List;
import org.fudaa.dodico.ef.operation.EfIndexHelper;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageTypeClient;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarOuvrageElemBuilder.java,v 1.5 2007-05-04 13:46:38 deniger Exp $
 */
public class H2dRubarOuvrageElemBuilder implements H2dRubarOuvrageTypeClient {

  H2dRubarOuvrageElementaireInterface elem_;
  H2dRubarOuvrage ouvrageParent_;

  H2dRubarOuvrageElemBuilder() {
  }

  final H2dRubarOuvrage getOuvrageParent() {
    return ouvrageParent_;
  }

  final void setOuvrageParent(final H2dRubarOuvrage _mng) {
    ouvrageParent_ = _mng;
  }

  /**
   * @return le dernier ouvrage elementaire construit
   */
  public final H2dRubarOuvrageElementaireInterface getLastBuildOuvrageElementaire() {
    return elem_;
  }

  @Override
  public void visitApport(final Object _o) {
    final H2dRubarOuvrageElementaireApportI appi = (H2dRubarOuvrageElementaireApportI) _o;
    elem_ = new H2dRubarOuvrageElementaireApport(appi.getEvols());
    ((H2dRubarOuvrageElementaireAbstract) elem_).ouvrageParent_ = ouvrageParent_;
  }

  @Override
  public void visitBreche(final Object _o) {
    final H2dRubarOuvrageElementaireBrecheI appi = (H2dRubarOuvrageElementaireBrecheI) _o;
    final double[] val = new double[H2dRubarOuvrageElementaireBrecheI.NB_VALUES];
    for (int i = val.length - 1; i >= 0; i--) {
      val[i] = appi.getValue(i);
    }
    elem_ = new H2dRubarOuvrageElementaireBreche(ouvrageParent_, val, appi.getNbPasDeTempsMax(), appi.getRenardSurverseId());
    ((H2dRubarOuvrageElementaireBreche) elem_).setTypeOfBreaching(appi.getBrecheType(), null);
    ((H2dRubarOuvrageElementaireAbstract) elem_).ouvrageParent_ = ouvrageParent_;
  }

  @Override
  public void visitDeversoir(final Object _o) {
    final H2dRubarOuvrageElementaireDeversoirI appi = (H2dRubarOuvrageElementaireDeversoirI) _o;
    final double[] val = new double[H2dRubarOuvrageElementaireDeversoirI.NB_VALUES];
    val[0] = appi.getLongDeversement();
    val[1] = appi.getCoteSeuilZd();
    val[2] = appi.getCoteMisEnchargeZm();
    val[3] = appi.getCoefficientDebit();
    elem_ = new H2dRubarOuvrageElementaireDeversoir(ouvrageParent_, val);
    ((H2dRubarOuvrageElementaireAbstract) elem_).ouvrageParent_ = ouvrageParent_;
  }

  @Override
  public void visitComposite(Object _o) {
    final H2dRubarOuvrageElementaireCompositeI appi = (H2dRubarOuvrageElementaireCompositeI) _o;
    final double[] val = appi.getValeurs();
    H2dRubarOuvrageElementaireInterface[] ouvrages = appi.getOuvrages();
    List<H2dRubarOuvrageElementaireInterface> transformed = new ArrayList<H2dRubarOuvrageElementaireInterface>();
    if (ouvrages != null) {
      H2dRubarOuvrageElemBuilder builder = new H2dRubarOuvrageElemBuilder();
      builder.ouvrageParent_ = ouvrageParent_;
      for (H2dRubarOuvrageElementaireInterface ouvrage : ouvrages) {
        ouvrage.getType().visit(builder, ouvrage);
        transformed.add(builder.getLastBuildOuvrageElementaire());
      }
    }
    H2dRubarOuvrageElementaireComposite composite = new H2dRubarOuvrageElementaireComposite(grid, ouvrageParent_, val,
            (H2dRubarOuvrageElementaireInterface[]) transformed.toArray(
            new H2dRubarOuvrageElementaireInterface[transformed.size()]));
    elem_ = composite;
    composite.setXAmont(appi.getXAmont());
    composite.setYAmont(appi.getYAmont());
    composite.setXAval(appi.getXAval());
    composite.setYAval(appi.getYAval());
    composite.setAmontMesh(EfIndexHelper.getElementEnglobant(grid.getGrid(), appi.getXAmont(), appi.getYAmont(), null));
    composite.setAvalMesh(EfIndexHelper.getElementEnglobant(grid.getGrid(), appi.getXAval(), appi.getYAval(), null));
    composite.setTypeControle(appi.getTypeControle());
    ((H2dRubarOuvrageElementaireAbstract) elem_).ouvrageParent_ = ouvrageParent_;

  }

  @Override
  public void visitDeversoirHydraulique(Object _o) {
    final H2dRubarOuvrageElementaireDeversoirHydrauliqueI appi = (H2dRubarOuvrageElementaireDeversoirHydrauliqueI) _o;
    final double[] val = new double[H2dRubarOuvrageElementaireDeversoirHydrauliqueI.NB_VALUES];
    val[0] = appi.getLongDeversement();
    val[1] = appi.getCoteSeuilZd();
    val[2] = appi.getCoteMisEnchargeZm();
    val[3] = appi.getCoefficientDebit();
    elem_ = new H2dRubarOuvrageElementaireDeversoirHydraulique(ouvrageParent_, val);
    ((H2dRubarOuvrageElementaireAbstract) elem_).ouvrageParent_ = ouvrageParent_;
  }

  @Override
  public void visitOrificeCirculaire(Object _o) {
    final H2dRubarOuvrageElementaireOrificeCirculaireI appi = (H2dRubarOuvrageElementaireOrificeCirculaireI) _o;
    final double[] val = new double[H2dRubarOuvrageElementaireDeversoirHydrauliqueI.NB_VALUES];
    val[0] = appi.getLongDeversement();
    val[1] = appi.getCoteSeuilZd();
    val[2] = appi.getDiametre();
    val[3] = appi.getCoefficientDebit();
    elem_ = new H2dRubarOuvrageElementaireOrificeCirculaire(ouvrageParent_, val);
    ((H2dRubarOuvrageElementaireAbstract) elem_).ouvrageParent_ = ouvrageParent_;

  }

  @Override
  public void visitTransfert(final Object _o) {
    final H2dRubarOuvrageElementaireTransfertDebitI appi = (H2dRubarOuvrageElementaireTransfertDebitI) _o;
    elem_ = new H2dRubarOuvrageElementaireTransfert(appi.getEvols());
    ((H2dRubarOuvrageElementaireAbstract) elem_).ouvrageParent_ = ouvrageParent_;
  }
  H2dRubarGridAreteSource grid;

  public void setGrid(H2dRubarGridAreteSource grid) {
    this.grid = grid;

  }
}