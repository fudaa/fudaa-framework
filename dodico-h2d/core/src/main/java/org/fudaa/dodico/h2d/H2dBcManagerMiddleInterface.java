/*
 *  @creation     21 nov. 2003
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import java.util.List;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author deniger
 * @version $Id: H2dBcManagerMiddleInterface.java,v 1.11 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dBcManagerMiddleInterface {
  /**
   * @param _idxFrontier l'indice de la frontiere demandee
   * @return les bords de la frontiere _i
   */
  H2dBcFrontierMiddleInterface getMiddleFrontier(int _idxFrontier);

  /**
   * @return le maillage
   */
  EfGridInterface getGrid();

  /**
   * @return la liste des bords utilises
   */
  List getUsedBoundaryType();

  /**
   * @return le nombre de type de bord gere
   */
  int getNbBoundaryType();

  /**
   * @return la liste des type des bords supportes
   */
  CtuluPermanentList getAllowedBordTypeList();
}
