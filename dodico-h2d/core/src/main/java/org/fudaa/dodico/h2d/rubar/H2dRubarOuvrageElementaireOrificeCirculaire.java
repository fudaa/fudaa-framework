/*
 * @creation 3 janv. 2005
 *
 * @modification $Date: 2007-01-10 09:04:24 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.dodico.h2d.H2dEvolutionVariableMap;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;

import java.util.Set;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarOuvrageElementaireDeversoir.java,v 1.10 2007-01-10 09:04:24 deniger Exp $
 */
public class H2dRubarOuvrageElementaireOrificeCirculaire extends H2dRubarOuvrageElementaireAbstract implements
    H2dRubarOuvrageElementaireOrificeCirculaireI, H2dRubarOuvrageElementaireValuablesInterface {
  protected class ValuesDoubleModel extends CtuluArrayDouble {
    public ValuesDoubleModel(final CtuluArrayDouble _init) {
      super(_init);
    }

    public ValuesDoubleModel(final double[] _values) {
      super(_values);
    }

    public ValuesDoubleModel(final int _capacity) {
      super(_capacity);
    }

    @Override
    protected void fireObjectChanged(int _indexGeom, Object _newValue) {
      if (ouvrageParent_ != null) {
        ouvrageParent_.getMng().fireOuvrageElementaireChanged(ouvrageParent_, H2dRubarOuvrageElementaireOrificeCirculaire.this);
      }
    }
  }

  CtuluArrayDouble values_;

  H2dRubarOuvrageElementaireOrificeCirculaire(final H2dRubarOuvrage _mng, final double[] _values) {
    ouvrageParent_ = _mng;
    values_ = new ValuesDoubleModel(_values);
  }

  /**
   * Toutes les valeurs a zero.
   */
  public H2dRubarOuvrageElementaireOrificeCirculaire() {
    values_ = new ValuesDoubleModel(NB_VALUES);
  }

  H2dRubarOuvrageElementaireOrificeCirculaire(final H2dRubarOuvrageElementaireOrificeCirculaire _d, final boolean _ouvParent) {
    initFrom(_d, _ouvParent);
  }

  final void initFrom(final H2dRubarOuvrageElementaireOrificeCirculaire _d, final boolean _ouvParent) {
    if (_ouvParent) {
      ouvrageParent_ = _d.ouvrageParent_;
    }
    values_ = new ValuesDoubleModel(_d.values_);
  }

  @Override
  public void fillWithTransientCurves(String pref, H2dEvolutionVariableMap r) {
  }

  @Override
  public void fillWithTarageEvolution(String prefix, Set allTarage) {

  }

  public static String[] getValuesTitle() {
    return new String[]{H2dResource.getS("Longueur de la conduite"), H2dResource.getS("Cote de seuil"),
        H2dResource.getS("Diametre"), H2dResource.getS("Coefficient de d�bit")};
  }

  @Override
  public double getCoefficientDebit() {
    return values_.getValue(3);
  }

  /**
   * @param _v les nouvelles valeurs
   * @param _cmd le receveur de commandes
   * @return true si modif
   */
  @Override
  public boolean setAllValues(final double[] _v, final CtuluCommandContainer _cmd) {
    return values_.setAll(_v, _cmd);
  }

  @Override
  public H2dRubarOuvrageElementaireInterface getCopyWithoutEvt() {
    return new H2dRubarOuvrageElementaireOrificeCirculaire(this, false);
  }

  @Override
  public double getDiametre() {
    return values_.getValue(2);
  }

  @Override
  public double getCoteSeuilZd() {
    return values_.getValue(1);
  }

  @Override
  public double getLongDeversement() {
    return values_.getValue(0);
  }

  @Override
  public final H2dRubarOuvrageType getType() {
    return H2dRubarOuvrageType.ORIFICE_CIRCULAIRE;
  }

  @Override
  public String[] getValuesName() {
    return getValuesTitle();
  }

  /**
   * @param _idx l'indice de la valeur
   * @return la valeur
   */
  @Override
  public double getValue(final int _idx) {
    return values_.getValue(_idx);
  }
}
