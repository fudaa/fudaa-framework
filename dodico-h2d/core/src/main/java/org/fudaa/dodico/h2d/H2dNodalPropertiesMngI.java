/*
 * @creation 9 mars 2006
 * @modification $Date: 2007-04-30 14:21:35 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author fred deniger
 * @version $Id: H2dNodalPropertiesMngI.java,v 1.1 2007-04-30 14:21:35 deniger Exp $
 */
public interface H2dNodalPropertiesMngI extends H2dVariableProviderInterface, H2dVariableProviderContainerInterface {
  boolean canVariableBeenAdd(H2dVariableType _t);

  boolean canVariableBeenRemoved(H2dVariableType _t);


  boolean containsValuesFor(H2dVariableType _var);

  void initValues(final H2dVariableType _key, final double _values, final CtuluCommandContainer _cmd);

  void initValues(final H2dVariableType _key, final double[] _values, final CtuluCommandContainer _cmd);

  void removeValues(final H2dVariableType _key, final CtuluCommandContainer _cmd);

  boolean isEmpty();

  List getAllVariables();

  Map getVarValues();

}
