/*
 * @creation 9 juin 2004
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.fudaa.dodico.commun.DodicoEnumType;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarBoundaryType.java,v 1.4 2006-09-19 14:43:25 deniger Exp $
 */
public abstract class H2dRubarBoundaryType extends H2dBoundaryType {

  int rubarIdx_;

  /**
   * @param _n le nom du type de bord
   * @param _isSolid true si solide
   * @param _rubarIdx l'index rubar
   */
  public H2dRubarBoundaryType(final String _n, final boolean _isSolid, final int _rubarIdx) {
    super(_n, _isSolid);
    rubarIdx_ = _rubarIdx;
  }

  /**
   * Test si bord liquide non libre et non tarage.
   *
   * @return true si type demandant une courbe transitoire fonction du temps.
   */
  public boolean needsTimeCurve() {
    return isLiquide() && (this != H2dRubarBcTypeList.LIBRE);
  }

  /**
   * @return la liste des variables autorisee pour ce type de bord.
   */
  public abstract List getAllowedVariable(H2dRubarProjetType _t);

  /**
   * @return la liste des variables qui doivent etre definies pour ce type de bord.
   */
  public List getRequiredVariable(final H2dRubarProjetType _t) {
    final List t = getAllowedVariable(_t);
    if (t == null) {
      return null;
    }
    final List r = new ArrayList(t);
    for (final Iterator it = r.iterator(); it.hasNext();) {
      if (isTorrentielVar((H2dVariableType) it.next())) {
        it.remove();
      }
    }
    return r;
  }

  public DodicoEnumType[] getArray() {
    return H2dRubarBcTypeList.getConstantArray();
  }

  /**
   * @param _t la variable tester
   * @return true si variable utilisee que dans le cas torrentiel
   */
  public boolean isTorrentielVar(final H2dVariableType _t) {
    return false;
  }

  /**
   * @param _n le nom du type de bord
   * @param _rubarIdx l'index rubar
   */
  public H2dRubarBoundaryType(final String _n, final int _rubarIdx) {
    this(_n, false, _rubarIdx);
  }

  /**
   * @return l'index rubar
   */
  public int getRubarIdx() {
    if (rubarIdx_ == H2dRubarBcTypeList.MIXTE.rubarIdx_) {
      new Throwable().printStackTrace();
    }
    return rubarIdx_;
  }

  /**
   * @return true si appartiend a un groupe debit
   */
  public boolean isTypeDebitGlobal() {
    return false;
  }
  
  /**
   * @return true si appartiend a un groupe debit
   */
  public boolean isTypeTarageGlobal() {
    return false;
  }

}
