/*
 * @creation 28 juin 07
 * @modification $Date: 2007-06-29 15:10:31 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import gnu.trove.TIntDoubleHashMap;
import gnu.trove.TIntObjectHashMap;

/**
 * @author fred deniger
 * @version $Id: H2dNodalPropertyMixteMutable.java,v 1.1 2007-06-29 15:10:31 deniger Exp $
 */
public class H2dNodalPropertyMixteMutable extends H2dNodalPropertyMixte {

  final int nbPt_;

  public H2dNodalPropertyMixteMutable(String _nom, int _nbPt) {
    super(_nom);
    nbPt_ = _nbPt;
  }

  public void setFixed(int _idx, double _v) {
    if (indiceTransitoireEvolution_ != null) indiceTransitoireEvolution_.remove(_idx);
    if (indicePermanentValeur_ == null) indicePermanentValeur_ = new TIntDoubleHashMap(nbPt_);
    indicePermanentValeur_.put(_idx, _v);
  }

  public void setTransient(int _idx, Object _evol) {
    if (indicePermanentValeur_ != null) indicePermanentValeur_.remove(_idx);
    // 50 c'est pas mal pour des points transitoires
    if (indiceTransitoireEvolution_ == null) indiceTransitoireEvolution_ = new TIntObjectHashMap(50);
    indiceTransitoireEvolution_.put(_idx, _evol);
  }

}
