/*
 * @creation 22 sept. 2003
 * @modification $Date: 2006-09-19 14:43:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.resource;

import com.memoire.bu.BuResource;
import org.fudaa.dodico.commun.DodicoResource;
import org.fudaa.dodico.ef.resource.EfResource;

/**
 * @author deniger
 * @version $Id: H2dResource.java,v 1.6 2006-09-19 14:43:26 deniger Exp $
 */
public final class H2dResource extends DodicoResource {

  public final static H2dResource H2D = new H2dResource(EfResource.EF);

  public static String getS(final String _s) {
    return H2D.getString(_s);
  }

  public static String getS(final String _s, final String _v0) {
    return H2D.getString(_s, _v0);
  }

  public static String getS(final String _s, final String _v0, final String _v1) {
    return H2D.getString(_s, _v0, _v1);
  }

  private H2dResource(final BuResource _parent) {
    super(_parent);
  }
}