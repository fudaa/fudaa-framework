/*
 * @creation 25 juin 2003
 * @modification $Date: 2007-06-20 12:21:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.h2d.type;

import java.util.HashMap;
import java.util.Map;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.commun.DodicoEnumType;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author deniger
 * @version $Id: H2dVariableType.java,v 1.46 2007-06-20 12:21:52 deniger Exp $
 */
public class H2dVariableType extends DodicoEnumType implements CtuluVariable {

  static Map shortNameVar = new HashMap();

  private static abstract class H2dVariableTypeDebit extends H2dVariableType {

    /**
     * @param _nom le nom
     * @param _telemacID l'id telemac
     */
    public H2dVariableTypeDebit(final String _nom, final String _telemacID, final String _shortName) {
      super(_nom, _telemacID, _shortName);
    }

    @Override
    public Object getCommonUnit() {
      return "m2/s";
    }

    @Override
    public H2dVariableType getParentVariable() {
      return H2dVariableType.DEBIT;
    }

    @Override
    public final boolean isVitesse() {
      return false;
    }
  }

  private static abstract class H2dVariableTypeDebitM3 extends H2dVariableType {

    /**
     * @param _nom le nom
     * @param _telemacID l'id telemac
     */
    public H2dVariableTypeDebitM3(final String _nom, final String _telemacID, final String _shortName) {
      super(_nom, _telemacID, _shortName);
    }

    @Override
    public final Object getCommonUnit() {
      return "m3/s";
    }

    @Override
    public H2dVariableType getParentVariable() {
      return H2dVariableType.DEBIT_M3;
    }

    @Override
    public final boolean isVitesse() {
      return false;
    }
  }

  private static abstract class H2dVariableTypeVitesse extends H2dVariableType {

    /**
     * @param _nom le nom
     * @param _telemacID l'id telemac
     */
    public H2dVariableTypeVitesse(final String _nom, final String _telemacID, final String _shortName) {
      super(_nom, _telemacID, _shortName);
    }

    @Override
    public H2dVariableType getParentVariable() {
      return H2dVariableType.VITESSE;
    }

    @Override
    public Object getCommonUnit() {
      return getVitesseUnit();

    }

    @Override
    public final boolean isVitesse() {
      return true;
    }
  }

  public static String getVitesseUnit() {
    return "m/s";
  }

  public static final H2dVariableType POINT_X = new H2dVariableType(H2dResource.getS("X"),
          null, "x") {

            @Override
            public double getValue(final H2dVariableTypeContainer _c) {
              new Throwable().printStackTrace();
              return 0;
            }
          };
  public static final H2dVariableType POINT_Y = new H2dVariableType(H2dResource.getS("Y"),
          null, "y") {

            @Override
            public double getValue(final H2dVariableTypeContainer _c) {
              new Throwable().printStackTrace();
              return 0;
            }
          };

  /**
   * COEF_FROTTEMENT.
   */
  public static final H2dVariableType COEF_FROTTEMENT = new H2dVariableType(H2dResource.getS("Coefficient frottement"),
          null, "frc") {

            @Override
            public double getValue(final H2dVariableTypeContainer _c) {
              return _c.getFriction();
            }
          };

  public static final H2dVariableType SANS = new H2dVariableType(H2dResource.getS("Sans"), null, "sans") {

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return 0;
    }
  };

  public static final H2dVariableType COEF_DIFFUSION = new H2dVariableType(H2dResource.getS("Coefficient diffusion"),
          null, "dif") {

            @Override
            public double getValue(final H2dVariableTypeContainer _c) {
              new Throwable().printStackTrace();
              return 0;
            }
          };
  public static final H2dVariableType LOI_TARAGE = new H2dVariableType(H2dResource.getS("Loi de tarage"), null,
          "tarage") {

            @Override
            public double getValue(final H2dVariableTypeContainer _c) {
              new Throwable().printStackTrace();
              return 0;
            }
          };

  public static final H2dVariableType TEMPS = new H2dVariableType(H2dResource.getS("Temps"), null, "t") {

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return 0;
    }

    @Override
    public Object getCommonUnit() {
      return "s";
    }
  };
  public static final H2dVariableType COEF_FROTTEMENT_FOND = new H2dVariableType(H2dResource
          .getS("Coefficient frottement de fond"), null, "bottom_frc") {

            @Override
            public double getValue(final H2dVariableTypeContainer _c) {
              return _c.getFriction();
            }
          };

  /**
   * Cote d'eau.
   */
  // public static final H2dVariableType COTE_EAU = new H2dVariableType(H2dResource.getS("Cote eau"), "SL", "zw") {
  public static final H2dVariableType COTE_EAU = new H2dVariableType(H2dResource.getS("Surface libre"), "SL", "zw") {
    @Override
    public boolean canBeTransient() {
      return true;
    }

    @Override
    public Object getCommonUnit() {
      return "m";

    }

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getSurfaceLibre();
    }
  };

  public static final H2dVariableType DEBIT_M3 = new H2dVariableTypeDebitM3(H2dResource.getS("D�bit") + " m3/s", "Qm3",
          "Qm3") {

    @Override
    public boolean canBeTransient() {
      return true;
    }

            @Override
            public double getValue(final H2dVariableTypeContainer _c) {
              return 0;
            }
          };

  /**
   * debit.
   */
  public static final H2dVariableType DEBIT = new H2dVariableTypeDebit(H2dResource.getS("D�bit"), "Q", "Q") {

    @Override
    public boolean canBeTransient() {
      return true;
    }

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getQ();
    }
  };
  public static final H2dVariableType DEBIT_NORMAL = new H2dVariableTypeDebit(H2dResource.getS("D�bit normal"), null,
          "qu") {

            @Override
            public double getValue(final H2dVariableTypeContainer _c) {
              new Throwable().printStackTrace();
              return -1;
            }
          };
  public static final H2dVariableType DEBIT_TANGENTIEL = new H2dVariableTypeDebit(H2dResource.getS("D�bit tangentiel"),
          null, "qv") {

            @Override
            public double getValue(final H2dVariableTypeContainer _c) {
              new Throwable().printStackTrace();
              return -1;
            }
          };

  public static final H2dVariableType DEBIT_X = new H2dVariableTypeDebit(H2dResource.getS("D�bit selon X"), null, "qx") {

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      new Throwable().printStackTrace();
      return -1;
    }
  };
  public static final H2dVariableType DEBIT_Y = new H2dVariableTypeDebit(H2dResource.getS("D�bit selon Y"), null, "qy") {

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      new Throwable().printStackTrace();
      return -1;
    }
  };
  public static final H2dVariableType HAUTEUR_EAU = new H2dVariableType(H2dResource.getS("Hauteur d'eau"), null, "h") {

    @Override
    public Object getCommonUnit() {
      return "m";

    }

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getH();
    }
  };
  public static final H2dVariableType PERTE_CHARGE = new H2dVariableType(H2dResource.getS("Perte de charge"), null,
          "frc_loss") {

            @Override
            public double getValue(final H2dVariableTypeContainer _c) {
              return _c.getPressureLoss();
            }
          };
  public static final H2dVariableType PRECISION_BCD_METHOD = new H2dVariableType(
          H2dResource.getS("Pr�cision banc c/d"), null, "acc_bcd_meth") {

            @Override
            public double getValue(final H2dVariableTypeContainer _c) {
              return 0;
            }
          };
  public static final H2dVariableType RELAXATION = new H2dVariableType(H2dResource.getS("Relaxation"), null, "relax") {

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return 0;
    }
  };
  public static final H2dVariableType RUGOSITE = new H2dVariableType(H2dResource.getS("Rugosit�"), null, "rough") {

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getFriction();
    }
  };

  public final static H2dVariableTypeCreated createTempVar(final String _name, final Map _strVar) {
    return new H2dVariableTypeCreated(_name, _strVar);
  }

  public final static H2dVariableTypeCreated createTempVar(final String _name, final String _shortName,
          final Map _strName) {
    return new H2dVariableTypeCreated(_name, _shortName, _strName);
  }

  public final static H2dVariableTypeCreated createTempVar(final String _name, final String _shortName,
          final String _unit, final Map _strName) {
    return new H2dVariableTypeCreated(_name, _shortName, _unit, _strName);
  }

  public static final H2dVariableType BATHYMETRIE = new H2dVariableType(H2dResource.getS("Bathym�trie"), null, "z") {

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return 0;
    }

    @Override
    public Object getCommonUnit() {
      return "m";
    }
  };
  public static final H2dVariableType TEMPERATURE = new H2dVariableType(H2dResource.getS("Temp�rature"), null, "t�") {

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return 0;
    }

    @Override
    public Object getCommonUnit() {
      return "C";
    }
  };

  public static final H2dVariableType VISCOSITE = new H2dVariableType(H2dResource.getS("Viscosit�"), null, "visc") {

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getViscosity();
    }
  };
  public static final H2dVariableType VITESSE = new H2dVariableTypeVitesse(H2dResource.getS("Vitesse"), null, "V") {

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getVelocity();
    }
  };

  public static final H2dVariableType VITESSE_U = new H2dVariableTypeVitesse(H2dResource.getS("Vitesse u"), "U", "u") {

    @Override
    public boolean canBeTransient() {
      return true;
    }

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getU();
    }

  };
  public static final H2dVariableType VITESSE_W = new H2dVariableTypeVitesse(H2dResource.getS("Vitesse w"), "W", "w") {

    @Override
    public boolean canBeTransient() {
      return true;
    }

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return 0;
    }

  };
  public static final H2dVariableType VITESSE_V = new H2dVariableTypeVitesse(H2dResource.getS("Vitesse v"), "V", "v") {

    @Override
    public boolean canBeTransient() {
      return true;
    }

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getV();
    }
  };
  public static final H2dVariableType VITESSE_NORMALE = new H2dVariableTypeVitesse(H2dResource.getS("Vitesse normale"),
          "U", "un") {

    @Override
    public boolean canBeTransient() {
      return true;
    }

    @Override
    public double getValue(final H2dVariableTypeContainer _c) {
      return _c.getU();
    }

          };
  public static final H2dVariableType VITESSE_TANGENTIELLE = new H2dVariableTypeVitesse(H2dResource
          .getS("Vitesse tangentielle"), "V", "ut") {

    @Override
    public boolean canBeTransient() {
      return true;
    }

            @Override
            public double getValue(final H2dVariableTypeContainer _c) {
              return _c.getV();
            }
          };

  public static final H2dVariableType VENT = new H2dVariableType(H2dResource.getS("Vent"), null, "wind") {
    @Override
    public String getCommonUnitString() {
      return getVitesseUnit();
    }

  };

  public static final H2dVariableType VENT_X = new H2dVariableType(H2dResource.getS("Vent selon X"), null, "windX") {
    @Override
    public String getCommonUnitString() {
      return getVitesseUnit();
    }

    @Override
    public H2dVariableType getParentVariable() {
      return VENT;
    }

  };

  public static final H2dVariableType VENT_Y = new H2dVariableType(H2dResource.getS("Vent selon Y"), null, "windY") {
    @Override
    public String getCommonUnitString() {
      return getVitesseUnit();
    }

    @Override
    public H2dVariableType getParentVariable() {
      return VENT;
    }

  };
  // les sollicitations .... unit�s ?
  public static final H2dVariableType SXX = new H2dVariableType("SXX", null, "sxx");
  public static final H2dVariableType PHASE = new H2dVariableType("Phase", null, "phase");
  public static final H2dVariableType DIRECTION = new H2dVariableType("Direction moyenne", null, "direction");
  public static final H2dVariableType PERIODE = new H2dVariableType("P�riode", null, "periode");
  public static final H2dVariableType HOULE = new H2dVariableType("Hauteur de houle", null, "hHoule");
  public static final H2dVariableType SXY = new H2dVariableType("SXY", null, "sxy");
  public static final H2dVariableType SYY = new H2dVariableType("SYY", null, "syy");
  public static final H2dVariableType RADIATION = new H2dVariableType("Contraintes de radiation", null, "rad");

  /**
   * @param _t la variable a tester
   * @return true si caracterise une des 3 variables lies au traceur traceur, coef a et coef b.
   */
  public final static boolean isTracerVariable(final H2dVariableType _t) {
    return (_t == H2dVariableTransType.TRACEUR) || (_t == H2dVariableTransType.TRACEUR_COEF_A)
            || (_t == H2dVariableTransType.TRACEUR_COEF_B);
  }

  private String telemacID_;
  String varShortName_;

  H2dVariableType(final String _nom, final String _telemacID, final String _varShortName) {
    super(_nom);
    telemacID_ = _telemacID;
    varShortName_ = _varShortName;
    if (varShortName_ != null) {
      if (shortNameVar.containsKey(varShortName_)) {
        new Throwable(varShortName_ + " already used").printStackTrace();
      }
      shortNameVar.put(varShortName_, this);
    }
  }

  protected void setShortName(final String _newShort) {
    if (_newShort != varShortName_) {
      if (shortNameVar.containsKey(varShortName_) && shortNameVar.get(varShortName_) == this) {
        shortNameVar.remove(varShortName_);
        shortNameVar.put(_newShort, this);
      }
      varShortName_ = _newShort;
    }

  }

  private H2dVariableType() {
    super(null);
  }

  H2dVariableType(final String _nom) {
    super(_nom);
  }

  public DodicoEnumType[] getArray() {
    return (DodicoEnumType[]) shortNameVar.values().toArray(new DodicoEnumType[shortNameVar.values().size()]);
  }

  /**
   * @param _shortName le nom court a rechercher
   * @param _shortNameVarCreated le tableau des variables temporaires cr��es: peut etre null
   * @return la variable trouv�e
   */
  public static H2dVariableType getVarFromShortName(final String _shortName, final Map _shortNameVarCreated) {
    final H2dVariableType t = (H2dVariableType) shortNameVar.get(_shortName);
    if (t == null && _shortNameVarCreated != null) {
      return (H2dVariableType) _shortNameVarCreated.get(_shortName);
    }
    return t;

  }

  /**
   * @return nom qui sera utilise pour les formules
   */
  public String getShortName() {
    return varShortName_;
  }

  /**
   * @return true si peut etre transient
   */
  public boolean canBeTransient() {
    return false;
  }

  /**
   * @return l'unite
   */
  @Override
  public Object getCommonUnit() {
    return null;
  }

  public String getCommonUnitString() {
    final Object unit = getCommonUnit();
    return unit == null ? null : unit.toString();
  }

  /**
   * Par exemple renvoie vitesse pour vitesse_u et vitesse_v.
   *
   * @return la variable parent. null par defaut
   */
  public H2dVariableType getParentVariable() {
    return null;
  }

  /**
   * @return l'id telemac si applicable ou null si aucun
   */
  public String getTelemacID() {
    return telemacID_;
  }

  @Override
  public String getID() {
    return getShortName();
  }

  @Override
  public String getLongName() {
    return getName();
  }

  /**
   * @param _container
   * @return la valeur (0 par defaut)
   */
  public double getValue(final H2dVariableTypeContainer _container) {
    return 0;
  }

  /**
   * Pour l'affichage.
   *
   * @return si valeur entiere
   */
  public boolean isInteger() {
    return false;
  }

  /**
   * Optimisation.
   *
   * @return true si vitesse
   */
  public boolean isVitesse() {
    return false;
  }
}
