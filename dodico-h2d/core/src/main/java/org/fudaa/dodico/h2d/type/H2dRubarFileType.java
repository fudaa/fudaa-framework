/*
 * @creation 26 juin 2003
 * @modification $Date: 2007-04-30 14:21:37 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import org.fudaa.dodico.commun.DodicoEnumType;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author deniger
 * @version $Id: H2dRubarProjetType.java,v 1.14 2007-04-30 14:21:37 deniger Exp $
 */
public class H2dRubarFileType extends DodicoEnumType {
  /**
   * Type couranto 2D.
   */
  public static final H2dRubarFileType OLD_FORMAT_6_DIGITS = new H2dRubarFileType(H2dResource.getS("Ancien format (6 digits)"));
  /**
   * Type transport.
   */
  public static final H2dRubarFileType NEW_FORMAT_9_DIGITS = new H2dRubarFileType(H2dResource.getS("Nouveau format (9 digits)"));

  public final static H2dRubarFileType[] getConstantArray() {
    return new H2dRubarFileType[]{NEW_FORMAT_9_DIGITS, OLD_FORMAT_6_DIGITS};
  }

  /**
   * @param _nom le nom
   */
  H2dRubarFileType(final String _nom) {
    super(_nom);
  }

  H2dRubarFileType() {
    super(null);
  }
}
