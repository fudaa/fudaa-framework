/*
 * @creation 24 mai 2004
 * 
 * @modification $Date: 2006-09-19 14:43:24 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParamsInterface;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

final class H2dTelemacBcParameterTracer extends H2dTelemacBcParameter {

  @Override
  public H2dVariableType[] getVariablesForPoint(final H2dTelemacDicoFileFormatVersion _v, final H2dTelemacBoundary _t, final DicoParamsInterface _dico) {
    if ((!isValueSetInDico(_dico)) && (_t.isTracerImposed())) {
      return new H2dVariableType[] { H2dVariableTransType.TRACEUR, H2dVariableTransType.TRACEUR_COEF_A, H2dVariableTransType.TRACEUR_COEF_B };
    }
    return null;
  }

  

  H2dTelemacBcParameterTracer(final H2dTelemacBcManager _mng, final DicoEntite.Vecteur _ent, final H2dBoundaryType[] _bords) {
    super(_mng, _ent, _bords);
    isFixedByUser_ = false;
  }

  @Override
  public String getValueFromBord(final H2dTelemacBoundary _b, int valueIdx) {
    return (String) _b.getTracer().get(valueIdx);
  }
  

  @Override
  public int getNbInternValues() {
    return bcMng.getNbTraceurs();
  }

  @Override
  public String getInternValueName(int idx) {
    return bcMng.getTracerName(idx);
  }

  @Override
  public boolean hasToBeActivatedForBoundary() {
    return true;
  }
  

  /**
   * @return true
   */
  public boolean isVariableOnSpace() {
    return true;
  }

  @Override
  public boolean isKeywordImposed() {
    return false;
  }

  @Override
  protected boolean setValueInBord(final H2dTelemacBoundary _b, final String _s, int valueIdx) {
    return _b.setTracer(_s, valueIdx);
  }
  
  @Override
  protected boolean setValueInBord(final H2dTelemacBoundary _b, final String[] _s) {
    return _b.setTracer(_s);
  }

  @Override
  public boolean isParameterFor(final H2dVariableType _v) {
    return (_v == H2dVariableTransType.TRACEUR) || (_v == H2dVariableTransType.TRACEUR_COEF_A) || (_v == H2dVariableTransType.TRACEUR_COEF_B);
  }

  @Override
  public H2dVariableType getVariable() {
    return H2dVariableTransType.TRACEUR;
  }
}