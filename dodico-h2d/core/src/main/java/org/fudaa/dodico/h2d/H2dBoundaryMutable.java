/*
 *  @creation     20 ao�t 2003
 *  @modification $Date: 2006-09-19 14:43:25 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.dodico.h2d;

import org.fudaa.dodico.h2d.type.H2dBoundaryType;

/**
 * @author Fred Deniger
 * @version $Id: H2dBoundaryMutable.java,v 1.11 2006-09-19 14:43:25 deniger Exp $
 */
public class H2dBoundaryMutable extends H2dBoundary {
  public H2dBoundaryMutable(final int _idxFr) {
    super(_idxFr);

  }

  @Override
  public void setIdxDeb(final int _i) {
    idxDeb_ = _i;
  }

  @Override
  public void setIdxFin(final int _i) {
    idxFin_ = _i;
  }

  @Override
  public void setIdxmaillageFrontiere(final int _i) {
    idxmaillageFrontiere_ = _i;
  }

  @Override
  public boolean setBoundaryType(final H2dBoundaryType _type, final H2dBcListenerDispatcher _l) {
    return super.setBoundaryType(_type, _l);
  }
}
