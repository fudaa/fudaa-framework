/*
 * @creation 10 janv. 2005
 * @modification $Date: 2007-06-11 13:06:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import com.memoire.bu.BuValueValidator;
import com.memoire.fu.FuLog;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dNodalPropertiesMngI;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: H2dTelemacNodalProperties.java,v 1.18 2007-06-11 13:06:09 deniger Exp $
 */
public class H2dTelemacNodalProperties implements H2dNodalPropertiesMngI {

  H2dTelemacSIProperties si_;

  H2dNodalPropertiesMngI current_;

  H2DTelemacNodalPropertiesDelegate delegate_;

  transient protected List listener_;

  protected void createDelegate() {
    delegate_ = new H2DTelemacNodalPropertiesDelegate(getGrid(), this);
  }

  void initNodalProperties(final Map _props) {
    if (si_ == null || (si_.isEmpty())) {
      delegate_.initalize(_props);
    } else {
      FuLog.error("CI mode");
    }
  }

  @Override
  public int getVariablesBlockLength() {
    return -1;
  }

  @Override
  public boolean containsVarToModify() {
    return current_.containsVarToModify();
  }

  public H2dTelemacNodalProperties(final H2dTelemacSIProperties _si, final boolean _isSiActivated) {
    si_ = _si;
    if (_isSiActivated) {
      current_ = _si;
    } else {
      delegate_ = new H2DTelemacNodalPropertiesDelegate(_si.getGrid(), this);
      current_ = delegate_;
    }
  }

  public boolean isSiActivated() {
    return current_ == si_;
  }

  @Override
  public boolean canVariableBeenAdd(final H2dVariableType _t) {
    return current_.canVariableBeenAdd(_t);
  }

  @Override
  public boolean containsValuesFor(final H2dVariableType _var) {
    return current_.containsValuesFor(_var);
  }

  @Override
  public List getAllVariables() {
    return current_.getAllVariables();
  }

  @Override
  public EfGridInterface getGrid() {
    return current_.getGrid();
  }

  @Override
  public CtuluCollectionDouble getViewedModel(final H2dVariableType _t) {
    return current_.getViewedModel(_t);
  }

  @Override
  public CtuluCollectionDouble getValues(CtuluVariable _var) {
    return getViewedModel((H2dVariableType) _var);
  }

  @Override
  public CtuluCollectionDoubleEdit getModifiableModel(final H2dVariableType _t) {
    return current_.getModifiableModel(_t);
  }

  @Override
  public Collection getUsableVariables() {
    return current_.getUsableVariables();
  }

  @Override
  public BuValueValidator getValidator(final H2dVariableType _t) {
    return current_.getValidator(_t);
  }

  @Override
  public CtuluNumberFormatI getFormater(final H2dVariableType _t) {
    return current_.getFormater(_t);
  }

  @Override
  public H2dVariableProviderInterface getVariableDataProvider() {
    return current_.getVariableDataProvider();
  }

  @Override
  public H2dVariableType[] getVarToModify() {
    return current_.getVarToModify();
  }

  @Override
  public void initValues(final H2dVariableType _key, final double _values, final CtuluCommandContainer _cmd) {
    current_.initValues(_key, _values, _cmd);
  }

  @Override
  public void initValues(final H2dVariableType _key, final double[] _values, final CtuluCommandContainer _cmd) {
    current_.initValues(_key, _values, _cmd);
  }

  @Override
  public boolean isElementVar() {
    return current_.isElementVar();
  }

  @Override
  public boolean isEmpty() {
    return current_.isEmpty();
  }

  @Override
  public void removeValues(final H2dVariableType _key, final CtuluCommandContainer _cmd) {
    current_.removeValues(_key, _cmd);
  }

//  public void setValues(final H2dVariableType _var, final int[] _idxPtElt, final double _values,
//      final CtuluCommandContainer _cmd) {
//    current_.setValues(_var, _idxPtElt, _values, _cmd);
//  }
//
//  public void setValues(final H2dVariableType _var, final int[] _idxPtElt, final double[] _values,
//      final CtuluCommandContainer _cmd) {
//    current_.setValues(_var, _idxPtElt, _values, _cmd);
//  }

  protected void fireChanged(final H2dVariableType _var) {
    // if (si_ != null) FuLog.error("si is defined");

    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dTelemacNodalPropListener) listener_.get(i)).nodalPropChanged(_var);

      }
    }
  }

  @Override
  public boolean canVariableBeenRemoved(final H2dVariableType _t) {
    return true;
  }

  protected void fireVarAdded(final H2dVariableType _var) {
    // if (si_ != null) FuLog.error("si is defined");
    delegate_.clearVarList();
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dTelemacNodalPropListener) listener_.get(i)).nodalPropAdded(_var);

      }
    }
  }

  protected void fireVarRemoved(final H2dVariableType _var) {
    if (si_ != null) {
      FuLog.error("si is defined");
    }
    delegate_.clearVarList();
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dTelemacNodalPropListener) listener_.get(i)).nodalPropRemoved(_var);

      }
    }
  }

  /**
   * @param _listener listener
   */
  public void addListener(final H2dTelemacNodalPropListener _listener) {
    if (listener_ == null) {
      listener_ = new ArrayList();
    }
    listener_.add(_listener);
  }

  /**
   * @param _listener le listener a tester
   * @return true si contenu
   */
  public boolean containsListener(final H2dTelemacNodalPropListener _listener) {
    return listener_ != null && listener_.contains(_listener);
  }

  /**
   * @param _listener le listener a virer
   */
  public void removeListener(final H2dTelemacNodalPropListener _listener) {
    if (listener_ != null) {
      listener_.remove(_listener);
    }
  }

  void setCIActivated() {
    delegate_ = null;
    current_ = si_;
  }

  public void activeSi(final Map _varValues, final CtuluCommandContainer _cmd) {
    if (_cmd != null) {
      final Map savedValue = delegate_.getVarValues();
      _cmd.addCmd(new CtuluCommand() {

        @Override
        public void redo() {
          activeSi(_varValues, null);
        }

        @Override
        public void undo() {
          unactiveSi(savedValue, null);

        }

      });
    }
    si_.initalize(_varValues);
    setCIActivated();

  }

  public void unactiveSi(final Map _varValues, final CtuluCommandContainer _cmd) {
    if (_cmd != null) {
      final Map saved = si_.getVarValues();
      _cmd.addCmd(new CtuluCommand() {

        @Override
        public void redo() {
          unactiveSi(_varValues, null);
        }

        @Override
        public void undo() {
          activeSi(saved, null);
        }

      });
    }
    setCIunactivated(_varValues);

  }

  public void activeSi(final double _coteEau, final Map _otherVar, final CtuluCommandContainer _cmd) {
    if (_cmd != null) {
      final Map saved = delegate_.getVarValues();
      _cmd.addCmd(new CtuluCommand() {

        @Override
        public void redo() {
          activeSi(_coteEau, _otherVar, null);
        }

        @Override
        public void undo() {
          unactiveSi(saved, null);
        }

      });
    }
    si_.initSi(_coteEau, _otherVar, delegate_.getModifiableModel(H2dVariableType.BATHYMETRIE));
    setCIActivated();

  }

  void setCIunactivated(final Map _newVal) {
    delegate_ = new H2DTelemacNodalPropertiesDelegate(getGrid(), this);
    if (_newVal != null) {
      delegate_.initalize(_newVal);
    }
    current_ = delegate_;
    si_.clear();
  }

  @Override
  public Map getVarValues() {
    return current_.getVarValues();
  }
}
