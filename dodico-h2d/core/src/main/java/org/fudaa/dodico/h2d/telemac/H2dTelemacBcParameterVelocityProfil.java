/*
 * @creation 24 mai 2004
 * @modification $Date: 2006-09-19 14:43:24 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

final class H2dTelemacBcParameterVelocityProfil extends H2dTelemacBcParameter {

  H2dTelemacBcParameterVelocityProfil(final H2dTelemacBcManager _mng, final DicoEntite.Vecteur _ent, final H2dBoundaryType[] _bords) {
    super(_mng, _ent, _bords);
  }

  @Override
  public String getValueFromBord(final H2dTelemacBoundary _b, int valueIdx) {
    return _b.getVelocityProfil();
  }

  @Override
  public boolean isKeywordImposed() {
    return false;
  }

  @Override
  protected boolean setValueInBord(final H2dTelemacBoundary _b, final String _s, int valueIdx) {
    return _b.setVelocityProfil(_s);
  }

  @Override
  public H2dVariableType getVariable() {
    return null;
  }
}