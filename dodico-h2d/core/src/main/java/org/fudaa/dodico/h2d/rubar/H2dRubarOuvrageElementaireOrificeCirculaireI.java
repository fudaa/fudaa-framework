package org.fudaa.dodico.h2d.rubar;

/**
 * @author CANEL Christophe (Genesis)
 */
public interface H2dRubarOuvrageElementaireOrificeCirculaireI extends H2dRubarOuvrageElementaireInterface {

  /**
   * Le nombre de valeurs gerees par l'ouvrage elementaire O.
   */
  int NB_VALUES = 4;

  /**
   * @return le coeffecient de d�bit COEF
   */
  double getCoefficientDebit();

  /**
   * @return le diam�tre (ZORI)
   */
  double getDiametre();

  /**
   * @return cote de seuil ZDEV
   */
  double getCoteSeuilZd();

  /**
   * @return longueur de d�versement LONG
   */
  double getLongDeversement();
}