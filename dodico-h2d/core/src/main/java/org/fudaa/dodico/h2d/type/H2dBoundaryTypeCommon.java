/*
 * @creation 14 juin 2004
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.commun.DodicoEnumType;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: H2dBoundaryTypeCommon.java,v 1.7 2006-09-19 14:43:25 deniger Exp $
 */
public final class H2dBoundaryTypeCommon extends H2dBoundaryType {

  /**
   * Type utilise pour caracteriser une reunion de type different.
   */
  public final static H2dBoundaryTypeCommon MIXTE = new H2dBoundaryTypeCommon(H2dResource.getS("Mixte"), true);
  /**
   * Type solide.
   */
  public final static H2dBoundaryTypeCommon SOLIDE = new H2dBoundaryTypeCommon(H2dResource.getS("Solide"), true);
  /**
   * Type solide avec frottement.
   */
  public final static H2dBoundaryTypeCommon SOLIDE_FROTTEMENT = new H2dBoundaryTypeCommon(H2dResource
      .getS("Solide frottement"), true);
  /**
   * Type liquide.
   */
  public final static H2dBoundaryTypeCommon LIQUIDE = new H2dBoundaryTypeCommon(H2dResource.getS("Liquide"));
  /**
   * Type liquide vitesses imposees.
   */
  public final static H2dBoundaryTypeCommon LIQUIDE_VITESSES_IMPOSEES = new H2dBoundaryTypeCommon(H2dResource
      .getS("Liquide vitesses impos�es"));
  /**
   * Type liquide vitesses et hauteur imposees.
   */
  public final static H2dBoundaryTypeCommon LIQUIDE_HAUTEUR_VITESSES_IMPOSEES = new H2dBoundaryTypeCommon(H2dResource
      .getS("Liquide hauteur et vitesses impos�es"));
  /**
   * Type liquide hauteur imposee.
   */
  public final static H2dBoundaryTypeCommon LIQUIDE_HAUTEUR_IMPOSEE = new H2dBoundaryTypeCommon(H2dResource
      .getS("Liquide hauteur impos�e"));
  /**
   * Type liquide debit impose.
   */
  public final static H2dBoundaryTypeCommon LIQUIDE_DEBIT_IMPOSE = new H2dBoundaryTypeCommon(H2dResource
      .getS("Liquide d�bit impos�"));
  /**
   * Type liquide onde incidence.
   */
  public final static H2dBoundaryTypeCommon LIQUIDE_ONDE_INCIDENCE = new H2dBoundaryTypeCommon(H2dResource
      .getS("Liquide onde incidence"));

  public static H2dBoundaryTypeCommon[] getConstantArray() {
    return new H2dBoundaryTypeCommon[] { MIXTE, SOLIDE, SOLIDE_FROTTEMENT, LIQUIDE, LIQUIDE_VITESSES_IMPOSEES,
        LIQUIDE_HAUTEUR_VITESSES_IMPOSEES, LIQUIDE_HAUTEUR_IMPOSEE, LIQUIDE_DEBIT_IMPOSE, LIQUIDE_ONDE_INCIDENCE };
  }

  /**
   * @param _nom
   * @param _isSolide
   */
  public H2dBoundaryTypeCommon(final String _nom, final boolean _isSolide) {
    super(_nom, _isSolide);
  }

  /**
   * @param _nom
   */
  H2dBoundaryTypeCommon() {
    super(CtuluLibString.EMPTY_STRING);
  }

  /**
   * @param _nom
   */
  public H2dBoundaryTypeCommon(final String _nom) {
    super(_nom);
  }

  public DodicoEnumType[] getArray() {
    return getConstantArray();
  }

}