/*
 * @creation 12 avr. 2006
 * @modification $Date: 2007-04-30 14:22:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.collection.CollectionPointDataDoubleInterface;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.fortran.FortranDoubleReaderResult;
import org.fudaa.dodico.fortran.FortranDoubleReaderResultInterface;
import org.fudaa.dodico.h2d.type.H2dRubarFileType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author fred deniger
 * @version $Id: TrExportRubarDonneesBrutesAdapter.java,v 1.8 2007-04-30 14:22:44 deniger Exp $
 */
public class H2dRubarDonneesBrutesAdapter implements FortranDoubleReaderResultInterface {
  final H2dRubarNumberFormatter formatter;
  final EfGridData data_;
  final H2dVariableType[] vars_;
  int idxBadValue_ = -1;
  final double badValueHauteur_ = 999.99;
  final double badValue_ = 9999.99;
  final boolean isElt_;

  /**
   * @param _idxVar l'indice de la variable ( x et y ne sont pas pris en compte)
   */
  public void setBadValueFor(final int _idxVar) {
    idxBadValue_ = _idxVar;
  }

  public H2dRubarDonneesBrutesAdapter(final EfGridData _data, final H2dVariableType[] _vars, final boolean _isElt, H2dRubarFileType fileType) {
    super();
    data_ = _data;
    vars_ = _vars;
    isElt_ = _isElt;
    formatter = new H2dRubarNumberFormatter(true, fileType);
  }

  @Override
  public CtuluCollectionDouble getCollectionFor(final int _col) {
    return FortranDoubleReaderResult.getCollectionFor(_col, this);
  }

  public H2dRubarDonneesBrutesAdapter(final EfGridData _data, final H2dVariableType _vars, H2dRubarFileType fileType) {
    super();
    data_ = _data;
    vars_ = new H2dVariableType[]{_vars};
    isElt_ = data_.isElementVar(_vars);
    formatter = new H2dRubarNumberFormatter(true, fileType);
  }

  @Override
  public CollectionPointDataDoubleInterface createXYValuesInterface() {
    return null;
  }

  @Override
  public CtuluNumberFormatI getFormat(final int _col) {
    if (_col < 2) {
      return formatter.getXYDb().getNumberFormat();
    }
    return formatter.getFormatterFor(vars_[_col - 2]).getNumberFormat();
  }

  @Override
  public int getNbColonne() {
    return 2 + vars_.length;
  }

  @Override
  public int getNbLigne() {
    return isElt_ ? data_.getGrid().getEltNb() : data_.getGrid().getPtsNb();
  }

  public double getBadValues(final H2dVariableType _var) {
    if (_var == H2dVariableType.HAUTEUR_EAU) {
      return badValueHauteur_;
    }
    return badValue_;
  }

  Map<H2dVariableType, EfData> savedDatas = new HashMap<H2dVariableType, EfData>();

  @Override
  public double getValue(final int _ligne, final int _col) {
    if (_col == 0) {
      return isElt_ ? data_.getGrid().getMoyCentreXElement(_ligne) : data_.getGrid().getPtX(_ligne);
    } else if (_col == 1) {
      return isElt_ ? data_.getGrid().getMoyCentreYElement(_ligne) : data_.getGrid().getPtY(
          _ligne);
    }
    final H2dVariableType variableType = vars_[_col - 2];
    if (idxBadValue_ == _col - 2 || !data_.isDefined(variableType)) {
      return getBadValues(variableType);
    }
    try {
      EfData savedData = savedDatas.get(variableType);
      if (savedData == null) {
        savedData = data_.getData(variableType, 0);
        savedDatas.put(variableType, savedData);
      }
      if (savedData == null) {
        return data_.getData(variableType, 0, _ligne);
      }
      return savedData.getValue(_ligne);
    } catch (final IOException _evt) {
      FuLog.error(_evt);

      // la valeur d'erreur de Rubar
      return badValue_;
    }
  }
}
