/*
 * @creation 20 mars 2003
 *
 * @modification $Date: 2007-06-11 13:06:08 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.interpolation.InterpolationVectorContainer;
import org.fudaa.dodico.dico.DicoCasFileFormatVersionAbstract;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.mesure.EvolutionListenerDispatcher;

import java.util.*;

/**
 * @author deniger
 * @version $Id: H2dParameters.java,v 1.27 2007-06-11 13:06:08 deniger Exp $
 */
public abstract class H2dParameters {
  protected DicoParams dicoParams_;

  protected H2dParameters(final DicoParams _dico) {
    dicoParams_ = _dico;
  }

  public abstract H2dProjectDispatcherListener getMainListener();

  public H2dVariableProviderContainerInterface getNodalProvider() {
    return new H2dVariableProviderContainerInterface() {
      @Override
      public H2dVariableProviderInterface getVariableDataProvider() {
        return H2dParameters.this.getNodalData();
      }
    };
  }

  public H2dGridDataAdapter createGridDataAdapter() {
    final List l = new ArrayList(4);
    H2dVariableProviderInterface prov = getNodalData();
    if (prov != null) {
      l.add(prov);
    }
    prov = getElementData();
    if (prov != null) {
      l.add(prov);
    }
    prov = getSiData();
    if (prov != null) {
      l.add(prov);
    }
    return new H2dGridDataAdapter(l);
  }

  /**
   * The user can edit the grid (moving point). If these modifications make the grid invalid, the project
   * should not be savable but we should propose export features.
   */
  private boolean gridInvalid;

  public void setGridInvalid(boolean gridInvalid) {
    this.gridInvalid = gridInvalid;
  }

  public boolean isGridInvalid() {
    return gridInvalid;
  }

  public H2dVariableProviderContainerInterface getElementProvider() {
    return new H2dVariableProviderContainerInterface() {
      @Override
      public H2dVariableProviderInterface getVariableDataProvider() {
        return H2dParameters.this.getElementData();
      }
    };
  }

  public H2dVariableProviderContainerInterface getSiProvider() {
    return new H2dVariableProviderContainerInterface() {
      @Override
      public H2dVariableProviderInterface getVariableDataProvider() {
        return H2dParameters.this.getSiData();
      }
    };
  }

  /**
   * @return la liste des types de bords gere par ce projet
   */
  public abstract CtuluPermanentList getAllowedBordType();

  /**
   * @return les parametres dico (cle=valeur)
   */
  public DicoParams getDicoParams() {
    return dicoParams_;
  }

  /**
   * @return le maillage
   */
  public abstract EfGridInterface getMaillage();

  /**
   * @param _b le bord a tester
   * @return le nombre de point de ce bord
   */
  public int getNPointInBord(final H2dBoundary _b) {
    final int deb = _b.getIdxDeb();
    final int fin = _b.getIdxFin();
    if (fin > deb) {
      return fin - deb + 1;
    }
    return fin + 1 + getMaillage().getFrontiers().getNbPt(_b.getIdxFrontiere()) - deb;
  }

  /**
   * @return le renvoyeur d'evt pour les evolutions
   */
  public abstract EvolutionListenerDispatcher getProjectDispatcher();

  /**
   * @return la liste des variables pouvant etre specifiees par une courbes transitoire
   */
  public abstract List getTransientVar();

  /**
   * @return un tableau Evolution->Variable. Si une evolution est utilisee pour different type de variable, null sera
   *     utilisee
   */
  public abstract Map getUsedEvolutionVariables();

  /**
   * @return la version utilisee pour initialiser le projet
   */
  public DicoCasFileFormatVersionAbstract getVersion() {
    return dicoParams_.getDicoFileFormatVersion();
  }

  /**
   * @return les donn�es pour les solutions initiales
   */
  public abstract H2dVariableProviderInterface getSiData();

  public H2dGridDataAdapter createSiGridDataAdapter() {
    return new H2dGridDataAdapter(Arrays.asList(getSiData()));
  }

  /**
   * @return les donn�es nodales
   */
  public abstract H2dVariableProviderInterface getNodalData();

  /**
   * @return les donn�es elementaires
   */
  public abstract H2dVariableProviderInterface getElementData();

  InterpolationVectorContainer default_;

  public InterpolationVectorContainer getVectorContainer() {
    if (default_ == null) {
      default_ = createDefaultVectorContainer();
    }
    return default_;
  }

  public static InterpolationVectorContainer createDefaultVectorContainer() {
    InterpolationVectorContainer defaultC = new InterpolationVectorContainer();
    return defaultC;
  }
}
