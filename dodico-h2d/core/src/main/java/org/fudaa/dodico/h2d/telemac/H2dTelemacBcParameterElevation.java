/*
 * @creation 24 mai 2004
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

final class H2dTelemacBcParameterElevation extends H2dTelemacBcParameter {

  H2dTelemacBcParameterElevation(final H2dTelemacBcManager _mng, final DicoEntite.Vecteur _ent, final H2dBoundaryType[] _bords) {
    super(_mng, _ent, _bords);
  }

  @Override
  public String getValueFromBord(final H2dTelemacBoundary _b, int valueIdx) {
    return _b.getElevation();
  }

  @Override
  public boolean isKeywordImposed() {
    return true;
  }

  @Override
  protected boolean setValueInBord(final H2dTelemacBoundary _b, final String _s, int valueIdx) {
    return _b.setElevation(_s);
  }

  @Override
  public boolean isParameterFor(final H2dVariableType _v) {
    return _v == H2dVariableType.COTE_EAU;
  }

  public boolean isValueTransientFor(final H2dTelemacBoundary _b) {
    return (_b.getType().isLiquide()) && (_b.isVariableTransient(H2dVariableType.COTE_EAU, 0));
  }

  @Override
  public H2dVariableType getVariable() {
    return H2dVariableType.COTE_EAU;
  }
}