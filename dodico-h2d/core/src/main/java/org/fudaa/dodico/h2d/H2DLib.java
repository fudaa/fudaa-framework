/*
 *  @creation     4 ao�t 2005
 *  @modification $Date: 2007-04-30 14:21:35 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import com.memoire.bu.BuValueValidator;
import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: H2DLib.java,v 1.14 2007-04-30 14:21:35 deniger Exp $
 */
public final class H2DLib {

  /**
   * le validateur pour valeur positive.
   */
  public final static BuValueValidator POS = BuValueValidator.MIN(0);

  public static boolean containsVarWithName(final Collection _c, final String _name) {
    if (_c == null) {
      return false;
    }
    for (final Iterator iter = _c.iterator(); iter.hasNext();) {
      if (((H2dVariableType) iter.next()).getName().equals(_name)) {
        return true;
      }

    }
    return false;
  }

  public static H2dVariableType getVarWithName(final Collection _c, final String _name) {
    if (_c == null) {
      return null;
    }
    for (final Iterator iter = _c.iterator(); iter.hasNext();) {
      final H2dVariableType test = (H2dVariableType) iter.next();
      if (test.getName().equals(_name)) {
        return test;
      }

    }
    return null;
  }

  public static int getIdxVarWithName(final List _c, final String _name) {
    if (_c == null) {
      return -1;
    }
    for (int i = 0; i < _c.size(); i++) {
      final H2dVariableType test = (H2dVariableType) _c.get(i);
      if (test.getName().equals(_name)) {
        return i;
      }
    }
    return -1;
  }

  public static H2dVariableType getVarWithName(final Collection _c, final String _name, final String _name1) {
    if (_c == null) {
      return null;
    }
    for (final Iterator iter = _c.iterator(); iter.hasNext();) {
      final H2dVariableType test = (H2dVariableType) iter.next();
      if (test.getName().equals(_name)) {
        return test;
      }
      if (test.getName().equals(_name1)) {
        return test;
      }

    }
    return null;
  }

  private H2DLib() {};

  public static void setHToZero(final H2dSiSourceInterface _src, final int[] _idx, final CtuluCommandContainer _cmd) {
    if (_src.getBathyModel() == null) {
      return;
    }
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    final CtuluCollectionDoubleEdit cote = _src.getModifiableModel(H2dVariableType.COTE_EAU);
    if (cote == null) {
      throw new IllegalArgumentException("pas de cote d'eau");
    }
    final double[] newCote = new double[_idx.length];
    for (int i = newCote.length - 1; i >= 0; i--) {
      newCote[i] = _src.getBathyModel().getValue(_idx[i]);
    }
    cote.set(_idx, newCote, cmp);
    final CtuluCollectionDoubleEdit u = _src.getModifiableModel(H2dVariableType.VITESSE_U);
    if (u != null) {
      u.set(_idx, 0, cmp);
    }
    final CtuluCollectionDoubleEdit v = _src.getModifiableModel(H2dVariableType.VITESSE_V);
    if (v != null) {
      v.set(_idx, 0, cmp);
    }
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
  }

  /**
   * @param _src la source a modifi�
   * @param _errAutoriseSurH si la hauteur d'eau est inf�rieure ou �gale � cette valeur elle sera annul�e. En g�n�ral
   *          vaut 0 mais dans certains cas des le syst�me de mod�lisation
   * @param _selection
   * @param _cmd
   * @return
   */
  public static int testCoteEau(final H2dSiSourceInterface _src, final CtuluListSelectionInterface _selection,

  final CtuluCommandContainer _cmd) {
    return testCoteEau(_src, 0, _selection, _cmd);
  }

  public static int testCoteEau(final H2dSiSourceInterface _src, final double _errAutoriseSurH,
      final CtuluListSelectionInterface _selection, final CtuluCommandContainer _cmd) {
    final CtuluCollectionDoubleEdit cote = _src.getModifiableModel(H2dVariableType.COTE_EAU);
    if (cote == null) {
      return 0;
    }
    final CtuluCollectionDouble bathy = _src.getBathyModel();
    final CtuluCollectionDoubleEdit u = _src.getModifiableModel(H2dVariableType.VITESSE_U);
    final CtuluCollectionDoubleEdit v = _src.getModifiableModel(H2dVariableType.VITESSE_V);
    final TIntArrayList listIdx = new TIntArrayList();
    final TDoubleArrayList coteVal = new TDoubleArrayList();
    if (bathy != null) {
      final int nb = cote.getSize();
      for (int i = 0; i < nb; i++) {
        if (_selection == null || _selection.isSelected(i)) {
          final double bathyi = bathy.getValue(i);
          final double hi = cote.getValue(i) - bathyi;
          final boolean isHauteurNull = hi <= _errAutoriseSurH;
          final boolean isHauteurStrictNull = isHauteurNull && hi < _errAutoriseSurH;
          // true si une des vitesses doit �tre changer: si la hauteur d'eau est null et si une des vitesses et
          // strictement sup�rieure � 0.
          final boolean isVitesseMustBeChanged = isHauteurNull
              && ((isStrictementPositif(u, i)) || (isStrictementPositif(v, i)));
          if (isHauteurStrictNull || isVitesseMustBeChanged) {
            listIdx.add(i);
            coteVal.add(bathyi);
          }
        }
      }
    }
    if (listIdx.size() > 0) {
      final int[] idx = listIdx.toNativeArray();
      final CtuluCommandComposite cmp = _cmd == null ? null : new CtuluCommandComposite();
      cote.set(idx, coteVal.toNativeArray(), cmp);
      if (u != null) {
        u.set(idx, 0, cmp);
      }
      if (v != null) {
        v.set(idx, 0, cmp);
      }
      if (cmp != null && _cmd != null) {
        _cmd.addCmd(cmp.getSimplify());
      }
    }
    return listIdx.size();
  }

  private static boolean isStrictementPositif(final CtuluCollectionDoubleEdit _list, final int _idxToTest) {
    return _list != null && _list.getValue(_idxToTest) > 0;
  }
}
