/*
 * @creation 12 mars 2004
 * @modification $Date: 2007-06-11 13:06:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import com.memoire.bu.BuValueValidator;
import com.memoire.fu.FuLog;
import gnu.trove.TDoubleArrayList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEditAbstract;
import org.fudaa.dodico.ef.EfElement;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGridDefaultAbstract;
import org.fudaa.dodico.h2d.H2DLib;
import org.fudaa.dodico.h2d.H2dSIListener;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $id$
 */
public class H2dRefluxSICourant2D implements H2dRefluxSolutionInitialeInterface, H2dVariableProviderInterface {

  /**
   * L'interface permettant de sauvegarder les si.
   * 
   * @author Fred Deniger
   * @version $Id: H2dRefluxSICourant2D.java,v 1.31 2007-06-11 13:06:09 deniger Exp $
   */
  private class Courant2DWriterInterface implements H2dRefluxSolutionInitAdapterInterface {

    boolean[] transit_;

    public Courant2DWriterInterface() {
      transit_ = H2dRefluxSICourant2D.this.getNoeudsTransit();
    }

    @Override
    public boolean isValideVariable(final H2dVariableType _t) {
      return H2dRefluxSICourant2D.this.isValideVariable(_t);
    }

    @Override
    public void setValuesIn(final H2dVariableType _t, final CtuluArrayDouble _dest, final CtuluCommandContainer _cmd) {
      if (_t == H2dVariableType.COTE_EAU) {
        _dest.setAll(coteEau_.getValues(), _cmd);
      } else if (_t == H2dVariableType.VITESSE_U) {
        _dest.setAll(u_.getValues(), _cmd);
      } else if (_t == H2dVariableType.VITESSE_V) {
        _dest.setAll(v_.getValues(), _cmd);
      } else {
        FuLog.error("variable " + _t.getName() + " not supported");
      }
    }

    @Override
    public double getCoteEau(final int _idx) {
      final double r = coteEau_.getValue(_idx);
      if (transit_[_idx]) {
        return 0d;
      }
      return r;
    }

    @Override
    public int getNbPt() {
      return g_.getPtsNb();
    }

    @Override
    public double getU(final int _idx) {
      if (transit_[_idx]) {
        return 0;
      }
      return u_.getValue(_idx);
    }

    @Override
    public double getV(final int _idx) {
      if (transit_[_idx]) {
        return 0;
      }
      return v_.getValue(_idx);
    }
  }

  private class RefluxModelArray extends CtuluArrayDouble {

    /**
     * @param _init
     */
    public RefluxModelArray(final CtuluArrayDouble _init) {
      super(_init);
    }

    /**
     * @param _init
     */
    public RefluxModelArray(final CtuluCollectionDouble _init) {
      super(_init);
    }

    /**
     * @param _init
     */
    public RefluxModelArray(final double[] _init) {
      super(_init);
    }

    /**
     * @param _nb
     */
    public RefluxModelArray(final int _nb) {
      super(_nb);
    }

    /**
     * @param _init
     */
    public RefluxModelArray(final Object[] _init) {
      super(_init);
    }

    /**
     * @param _init
     */
    public RefluxModelArray(final TDoubleArrayList _init) {
      super(_init);
    }

    @Override
    protected void fireObjectChanged(int _indexGeom, Object _newValue) {
      H2dRefluxSICourant2D.this.fireChanged(null);
    }
  }

  CtuluCollectionDouble bottomModel_;

  final EfGridInterface g_;

  final CtuluArrayDouble coteEau_;
  List listener_;
  final CtuluArrayDouble u_;
  final CtuluArrayDouble v_;

  CtuluPermanentList list_ = new CtuluPermanentList(CtuluLibArray.sort(new H2dVariableType[] {
      H2dVariableType.COTE_EAU, H2dVariableType.VITESSE_U, H2dVariableType.VITESSE_V }));

  /**
   * les 3 tableaux doivent avoir la meme taille ( nombre de points).
   * 
   * @param _u valeurs pour u
   * @param _v valeurs pour v
   * @param _coteEau valeurs pour h
   * @param _g le maillage
   */
  public H2dRefluxSICourant2D(final double[] _u, final double[] _v, final double[] _coteEau, final EfGridInterface _g) {
    g_ = _g;
    if (g_ == null) {
      throw new IllegalArgumentException("grid is null");
    }
    u_ = new RefluxModelArray(_u);
    v_ = new RefluxModelArray(_v);
    coteEau_ = new RefluxModelArray(_coteEau);
    if (_u.length != _v.length) {
      throw new IllegalArgumentException("u and v with different size");
    }
    if (_u.length != _coteEau.length) {
      throw new IllegalArgumentException("u and h with different size");
    }
    if (_u.length != _g.getPtsNb()) {
      throw new IllegalArgumentException("u and nbNode are not equals");
    }
  }

  @Override
  public void setHToZero(final int[] _idx, final CtuluCommandContainer _cmd) {
    H2DLib.setHToZero(this, _idx, _cmd);
  }

  @Override
  public int testCoteEau(final CtuluListSelectionInterface _selection, final CtuluCommandContainer _cmd) {
    return H2DLib.testCoteEau(this, _selection, _cmd);
  }

  protected void fireChanged(final H2dVariableType _var) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dSIListener) listener_.get(i)).siChanged(_var);

      }
    }
  }

  protected void fireVarAdded(final H2dVariableType _var) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dSIListener) listener_.get(i)).siAdded(_var);

      }
    }
  }

  /**
   * @param _listener listener
   */
  @Override
  public void addListener(final H2dSIListener _listener) {
    if (listener_ == null) {
      listener_ = new ArrayList();
    }
    listener_.add(_listener);
  }

  /**
   * @param _listener le listener a tester
   * @return true si contenu
   */
  public boolean containsListener(final H2dSIListener _listener) {
    return listener_ != null && listener_.contains(_listener);
  }

  /**
   * @return une interface pour l'ecriture des solutions initiales
   */
  @Override
  public Object createWriterInterface() {
    return new Courant2DWriterInterface();
  }

  /**
   * Construit un model special.
   */
  @Override
  public CtuluCollectionDouble getBathyModel() {
    if (bottomModel_ == null) {
      bottomModel_ = EfGridDefaultAbstract.getBathyModel(g_);
    }
    return bottomModel_;
  }

  @Override
  public EfGridInterface getMaillage() {
    return g_;
  }

  @Override
  public CtuluCollectionDoubleEdit getModifiableModel(final H2dVariableType _t) {
    if (_t == H2dVariableType.COTE_EAU) {
      return coteEau_;
    } else if (_t == H2dVariableType.VITESSE_U) {
      return u_;
    } else if (_t == H2dVariableType.VITESSE_V) {
      return v_;
    } else if (_t == H2dVariableType.HAUTEUR_EAU) {
      return getHauteurEau();
    } else {
      return null;
    }
  }

  @Override
  public Collection getUsableVariables() {
    final List r = new ArrayList(Arrays.asList(getVarToModify()));
    r.add(H2dVariableType.HAUTEUR_EAU);
    return r;
  }

  /**
   * Determination des noeuds de transit. Si au moins 1 des noeuds de l'element est mouille et au moins 1 est sec =>
   * Element de transit. Les noeuds non mouilles sont transit
   * 
   * @return <i>true </i> noeud de transit, <i>false </i> sinon.
   */
  public boolean[] getNoeudsTransit() {
    boolean[] nTrs;
    nTrs = new boolean[g_.getPtsNb()];
    final CtuluCollectionDoubleEdit hauteur = getHauteurEau();
    for (int i = g_.getEltNb() - 1; i >= 0; i--) {
      final EfElement el = g_.getElement(i);
      final boolean pt0 = (hauteur.getValue(el.getPtIndex(0)) <= 0.);
      final boolean pt2 = (hauteur.getValue(el.getPtIndex(2)) <= 0.);
      final boolean pt4 = (hauteur.getValue(el.getPtIndex(4)) <= 0.);
      if (pt0 != pt2) {
        nTrs[el.getPtIndex(1)] = true;
      }
      if (pt2 != pt4) {
        nTrs[el.getPtIndex(3)] = true;
      }
      if (pt0 != pt4) {
        nTrs[el.getPtIndex(5)] = true;
      }
    }
    return nTrs;
  }

  HauteurEauModel hauteurEauModel_;

  public CtuluCollectionDoubleEdit getHauteurEau() {
    if (hauteurEauModel_ == null) {
      hauteurEauModel_ = new HauteurEauModel();
      // pas de cache .....
      hauteurEauModel_.setUseCache(false);
    }
    return hauteurEauModel_;
  }

  @Override
  public double[] getU() {
    return u_.getValues();
  }

  @Override
  public double[] getV() {
    return v_.getValues();
  }

  @Override
  public List getVariables() {
    return list_;
  }

  @Override
  public double[] getZe() {
    final double[] he = coteEau_.getValues();
    for (int i = he.length - 1; i >= 0; i--) {
      he[i] = he[i] + g_.getPtZ(i);
    }
    return he;
  }

  @Override
  public void initFromCoteEau(final double _d, final ProgressionInterface _prog, final CtuluCommandContainer _cmd) {
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    /*
     * double[] newH = new double[g_.getPtsNb()]; for (int i = newH.length - 1; i >= 0; i--) { newH[i] = _d -
     * g_.getPtZ(i); }
     */
    coteEau_.setAll(_d, cmp);
    if (FuLog.isTrace()) {
      FuLog.trace("DHR: set cote eau " + _d);
    }
    u_.setAll(0, cmp);
    v_.setAll(0, cmp);
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
  }

  @Override
  public int getVariablesBlockLength() {
    return -1;
  }

  @Override
  public CtuluCommand initFromSI(final H2dRefluxSolutionInitAdapterInterface _interface, final CtuluAnalyze _a) {
    if (g_.getPtsNb() != _interface.getNbPt()) {
      FuLog.error("bad number of nodes");
      return null;
    }
    if (_interface.isValideVariable(H2dVariableType.COTE_EAU) && _interface.isValideVariable(H2dVariableType.VITESSE_U)
        && _interface.isValideVariable(H2dVariableType.VITESSE_V)) {
      // maj de u
      final CtuluCommandComposite cmp = new CtuluCommandComposite();
      _interface.setValuesIn(H2dVariableType.VITESSE_U, u_, cmp);
      // maj de v
      _interface.setValuesIn(H2dVariableType.VITESSE_V, v_, cmp);
      _interface.setValuesIn(H2dVariableType.COTE_EAU, coteEau_, cmp);
      return cmp.getSimplify();
    }
    FuLog.error("bad variables");
    return null;

  }

  @Override
  public boolean isSolutionInitialesActivated() {
    return true;
  }

  @Override
  public BuValueValidator getValidator(final H2dVariableType _t) {
    return BuValueValidator.DOUBLE;
  }

  @Override
  public CtuluNumberFormatI getFormater(final H2dVariableType _t) {
    return null;
  }

  @Override
  public boolean isValideVariable(final H2dVariableType _t) {
    return (_t == H2dVariableType.VITESSE_U) || (_t == H2dVariableType.VITESSE_V) || (_t == H2dVariableType.COTE_EAU);
  }

  /**
   * @param _listener le listener a virer
   */
  @Override
  public void removeListener(final H2dSIListener _listener) {
    if (listener_ != null) {
      listener_.remove(_listener);
    }
  }

  @Override
  public void set(final double[] _ze, final double[] _u, final double[] _v, final CtuluCommandContainer _cmd) {
    for (int i = _ze.length - 1; i >= 0; i--) {
      if ((_ze[i] - g_.getPtZ(i)) <= 0) {
        _ze[i] = g_.getPtZ(i);
        _u[i] = 0;
        _v[i] = 0;
      }
    }
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    coteEau_.setAll(_ze, cmp);
    u_.setAll(_u, cmp);
    v_.setAll(_v, cmp);
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }

  }

  public boolean isEquiv(final H2dRefluxSICourant2D _si) {
    if (_si.g_.getPtsNb() != g_.getPtsNb()) {
      return false;
    }
    return _si.coteEau_.isEquivalentTo(coteEau_) && _si.u_.isEquivalentTo(u_) && _si.v_.isEquivalentTo(v_);
  }

  class HauteurEauModel extends CtuluCollectionDoubleEditAbstract {

    @Override
    public int getSize() {
      return g_.getPtsNb();
    }

    @Override
    public double getValue(final int _i) {
      return coteEau_.getValue(_i) - g_.getPtZ(_i);
    }

    @Override
    protected void internalSetValue(final int _i, final double _newV) {
      coteEau_.set(_i, g_.getPtZ(_i) + _newV);

    }
  }

  @Override
  public H2dVariableProviderInterface getVariableDataProvider() {
    return this;
  }

  @Override
  public EfGridInterface getGrid() {
    return g_;
  }

  @Override
  public H2dVariableType[] getVarToModify() {
    final H2dVariableType[] rf = new H2dVariableType[list_.size()];
    list_.toArray(rf);
    return rf;
  }

  @Override
  public CtuluCollectionDouble getViewedModel(final H2dVariableType _t) {
    return getModifiableModel(_t);
  }

  @Override
  public CtuluCollectionDouble getValues(CtuluVariable _var) {
    return getViewedModel((H2dVariableType) _var);
  }

  @Override
  public boolean containsVarToModify() {
    return list_ != null && list_.size() > 0;
  }

  @Override
  public boolean isElementVar() {
    return false;
  }

  public void setValues(final H2dVariableType _var, final int[] _idxPtElt, final double _values,
      final CtuluCommandContainer _cmd) {
    final CtuluCollectionDoubleEdit model = getModifiableModel(_var);
    if (model != null) {
      model.set(_idxPtElt, _values, _cmd);
    }

  }

  public void setValues(final H2dVariableType _var, final int[] _idxPtElt, final double[] _values,
      final CtuluCommandContainer _cmd) {
    final CtuluCollectionDoubleEdit model = getModifiableModel(_var);
    if (model != null) {
      model.set(_idxPtElt, _values, _cmd);
    }

  }

}
