/*
 * @creation 27 avr. 07
 * @modification $Date: 2007-06-28 09:25:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandCompositeInverse;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleAbstract;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEditAbstract;
import org.fudaa.dodico.dico.DicoDataType;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.ef.impl.EFGridArrayZ;
import org.fudaa.dodico.fortran.FortranDoubleReaderResultInterface;
import org.fudaa.dodico.h2d.H2dNodalPropertiesMngAbstract;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author fred deniger
 * @version $Id: H2dRefluxNodalPropertiesMng.java,v 1.5 2007-06-28 09:25:08 deniger Exp $
 */
public class H2dRefluxNodalPropertiesMng extends H2dNodalPropertiesMngAbstract {

  class BathyDoubleModel extends CtuluCollectionDoubleEditAbstract {

    final EFGridArrayZ maillage_;

    public BathyDoubleModel(final EFGridArrayZ _maillage) {
      super();
      maillage_ = _maillage;
    }

    @Override
    protected void fireObjectChanged(int _indexGeom, Object _newValue) {
      H2dRefluxNodalPropertiesMng.this.fireNodalPropertyChanged(H2dVariableType.BATHYMETRIE);
    }

    @Override
    protected void internalSetValue(final int _i, final double _newV) {
      maillage_.setZIntern(_i, _newV);

    }

    @Override
    public int getSize() {
      return maillage_.getPtsNb();
    }

    @Override
    public double getValue(final int _i) {
      return maillage_.getPtZ(_i);
    }
  }

  public interface ForceItem {

    int getNbVar();

    H2dVariableType[] getVar();

    H2dVariableType getMainVar();

    H2dVariableType getVar(int _idx);

    CtuluCollectionDouble getValues(H2dVariableType _var);

    int getNbPt();

    boolean isActivated();

    boolean setEnable(double[] _values, CtuluCommandContainer _cmd);

    boolean setEnable(FortranDoubleReaderResultInterface _values, CtuluAnalyze _analyze, CtuluCommandContainer _cmd);

    void setUnable(CtuluCommandContainer _cmd);
  }

  public class ForceItemDefault implements ForceItem {

    final H2dVariableType[] vars_;
    final H2dVariableType main_;
    final DicoEntite entite_;

    public ForceItemDefault(final H2dVariableType _main, final H2dVariableType[] _vars, final DicoEntite _entite) {
      super();
      main_ = _main;
      vars_ = _vars;
      entite_ = _entite;
      entite_.setModifiable(false);
    }

    @Override
    public H2dVariableType getMainVar() {
      return main_;
    }

    @Override
    public int getNbPt() {
      return getGrid().getPtsNb();
    }

    @Override
    public CtuluCollectionDouble getValues(final H2dVariableType _var) {
      return H2dRefluxNodalPropertiesMng.this.getViewedModel(_var);
    }

    @Override
    public int getNbVar() {
      return vars_.length;
    }

    @Override
    public H2dVariableType[] getVar() {
      final H2dVariableType[] res = new H2dVariableType[getNbVar()];
      for (int i = 0; i < res.length; i++) {
        res[i] = getVar(i);
      }
      return res;
    }

    @Override
    public H2dVariableType getVar(final int _idx) {
      return vars_[_idx];
    }

    @Override
    public boolean isActivated() {
      return H2dRefluxNodalPropertiesMng.this.containsValuesFor(getVar(0));
    }

    @Override
    public boolean setEnable(final double[] _values, final CtuluCommandContainer _cmd) {
      if (_values.length != getNbVar()) { return false; }
      CtuluCommandComposite cmp = null;
      if (_cmd != null) {
        cmp = new CtuluCommandCompositeInverse();
      }
      for (int i = 0; i < vars_.length; i++) {
        H2dRefluxNodalPropertiesMng.this.initValues(vars_[i], H2dRefluxNodalPropertiesMng.this
            .getCollectionForUnique(_values[i]), cmp);
      }
      finishActivation(_cmd, cmp);
      return true;
    }

    private void finishActivation(final CtuluCommandContainer _cmd, final CtuluCommandComposite _cmp) {

      final double old = ((DicoDataType.Reel) entite_.getType()).getControleMinValue();
      ((DicoDataType.Reel) entite_.getType()).setControle(getMinWhenActivated());
      if (_cmd != null) {
        _cmp.addCmd(new EntiteModifiableValue(entite_, true, getMinWhenActivated(), old));
      }
      entite_.setModifiable(true);
      params_.setValue(entite_, CtuluLibString.UN, _cmp);
      if (_cmd != null) {
        _cmd.addCmd(_cmp);
      }
    }

    @Override
    public boolean setEnable(final FortranDoubleReaderResultInterface _res, final CtuluAnalyze _analyze,
        final CtuluCommandContainer _cmd) {
      if (_res == null) { return false; }
      if (_res.getNbColonne() != vars_.length) {
        _analyze.addError(H2dResource.getS("Les sollicitations ne sont pas correctes.") + CtuluLibString.ESPACE
            + H2dResource.getS("Nombre de variables invalides:") + CtuluLibString.getEspaceString(_res.getNbColonne()),
            -1);
        return false;
      }
      if (_res.getNbLigne() != getGrid().getPtsNb()) {
        _analyze.addError(H2dResource.getS("Les sollicitations ne sont pas correctes.") + CtuluLibString.ESPACE
            + H2dResource.getS("nombre de points invalide") + CtuluLibString.getEspaceString(_res.getNbLigne()), -1);
        return false;
      }
      CtuluCommandComposite cmp = null;
      if (_cmd != null) {
        cmp = new CtuluCommandCompositeInverse();
      }
      for (int i = 0; i < vars_.length; i++) {
        H2dRefluxNodalPropertiesMng.this.initValues(vars_[i], _res.getCollectionFor(i), cmp);
      }
      finishActivation(_cmd, cmp);
      return true;
    }

    private double getMinWhenActivated() {
      return 0.001;
    }

    @Override
    public void setUnable(final CtuluCommandContainer _cmd) {
      if (isActivated()) {
        final CtuluCommandComposite cmp = _cmd == null ? null : new CtuluCommandCompositeInverse();
        for (int i = 0; i < vars_.length; i++) {
          removeValues(vars_[i], cmp);
        }

        final double old = ((DicoDataType.Reel) entite_.getType()).getControleMinValue();
        ((DicoDataType.Reel) entite_.getType()).setControle(0);

        entite_.setModifiable(false);
        if (_cmd != null) {
          cmp.addCmd(new EntiteModifiableValue(entite_, false, 0, old));
        }
        params_.setValue(entite_, CtuluLibString.ZERO, cmp);
        if (_cmd != null) {
          _cmd.addCmd(cmp);
        }
      }
    }

  }

  private static class EntiteModifiableValue implements CtuluCommand {
    final DicoEntite ent_;
    final boolean newVal_;
    final double newMin_;
    final double oldMin_;

    public EntiteModifiableValue(final DicoEntite _ent, final boolean _newVal, final double _newMin,
        final double _oldMin) {
      super();
      ent_ = _ent;
      newVal_ = _newVal;
      newMin_ = _newMin;
      oldMin_ = _oldMin;
    }

    @Override
    public void redo() {
      ent_.setModifiable(newVal_);
      ((DicoDataType.Reel) ent_.getType()).setControle(newMin_);

    }

    @Override
    public void undo() {
      ent_.setModifiable(!newVal_);
      ((DicoDataType.Reel) ent_.getType()).setControle(oldMin_);

    }
  }

  private final BathyDoubleModel bathy_;

  Map varForceData_;

  DicoParams params_;

  public H2dRefluxNodalPropertiesMng(final H2dRefluxParameters _params) {
    super(_params.maillage_);
    params_ = _params.getDicoParams();
    bathy_ = new BathyDoubleModel((EFGridArrayZ) _params.maillage_);

    getCurrentVarValues().put(H2dVariableType.BATHYMETRIE, bathy_);
    clearVarList();
    varForceData_ = new HashMap();
    varForceData_.put(getRadiationVar(), new ForceItemDefault(getRadiationVar(), new H2dVariableType[] {
        H2dVariableType.SXX, H2dVariableType.SXY, H2dVariableType.SYY }, _params.getDicoVersion()
        .getSollicitationRadiation()));
    varForceData_.put(getVentVar(), new ForceItemDefault(getVentVar(), new H2dVariableType[] { H2dVariableType.VENT_X,
        H2dVariableType.VENT_Y }, _params.getDicoVersion().getSollicitationVent()));
  }

  Set listener_ = new HashSet();

  protected void fireChanged(final H2dVariableType _var, final boolean _addedRemoved) {
    for (final Iterator it = listener_.iterator(); it.hasNext();) {
      ((H2dRefluxNodalPropertiesListener) it.next()).nodalPropertyChanged(_var, _addedRemoved);
    }
  }

  @Override
  protected void fireChanged(final H2dVariableType _var) {
    fireChanged(_var, false);
  }

  protected void fireNodalPropertyChanged(final H2dVariableType _t) {
    fireChanged(_t, false);
  }

  @Override
  protected void fireVarAddedProcess(final H2dVariableType _var) {
    fireChanged(_var, true);
  }

  @Override
  protected void fireVarRemovedProcess(final H2dVariableType _var) {
    fireChanged(_var, true);
  }

  @Override
  public boolean canVariableBeenAdd(final H2dVariableType _t) {
    return true;
  }

  @Override
  public boolean canVariableBeenRemoved(final H2dVariableType _t) {
    return _t != H2dVariableType.BATHYMETRIE;
  }

  public H2dVariableType[] getForceVar() {
    return new H2dVariableType[] { getVentVar(), getRadiationVar() };
  }

  public ForceItem getForceData(final H2dVariableType _t) {
    return (ForceItem) varForceData_.get(_t);
  }

  public boolean init(final H2dVariableType _force, final FortranDoubleReaderResultInterface _res,
      final CtuluAnalyze _analyse, final CtuluCommandContainer _cmd) {
    final ForceItem it = (ForceItem) varForceData_.get(_force);
    if (it != null) { return it.setEnable(_res, _analyse, _cmd); }
    return false;
  }

  public static H2dVariableType getRadiationVar() {
    return H2dVariableType.RADIATION;
  }

  public static H2dVariableType getVentVar() {
    return H2dVariableType.VENT;
  }

  public boolean isVentEnable() {
    return getForceData(getVentVar()).isActivated();
  }

  public boolean isRadiationEnable() {
    return getForceData(getRadiationVar()).isActivated();
  }

  public void addListener(final H2dRefluxNodalPropertiesListener _listener) {
    listener_.add(_listener);
  }

  CtuluCollectionDouble wind_;

  @Override
  public CtuluCollectionDouble getViewedModel(final H2dVariableType _t) {
    if (_t == H2dVariableType.VENT) {
      if (wind_ == null) {
        wind_ = new Wind();
      }
      return wind_;
    }
    return super.getViewedModel(_t);
  }

  protected class Wind extends CtuluCollectionDoubleAbstract {

    @Override
    public int getSize() {
      final CtuluCollectionDouble d = getViewedModel(H2dVariableType.VENT_X);
      return d == null ? 0 : d.getSize();
    }

    @Override
    public double getValue(final int _i) {
      final CtuluCollectionDouble vx = getViewedModel(H2dVariableType.VENT_X);
      if (vx != null) {
        final CtuluCollectionDouble vy = getViewedModel(H2dVariableType.VENT_Y);
        if (vy != null) {
          final double vxi = vx.getValue(_i);
          final double vyi = vy.getValue(_i);
          return Math.hypot(vxi, vyi);
        }
      }
      return 0;
    }

  }

  @Override
  public Collection getUsableVariables() {
    Collection res = super.getUsableVariables();
    if (res.contains(H2dVariableType.VENT_X) && res.contains(H2dVariableType.VENT_Y)) {
      final List newL = new ArrayList(res);
      newL.add(H2dVariableType.VENT);
      res = new CtuluPermanentList(newL);
    }
    return res;
  }

  public void removeListener(final H2dRefluxNodalPropertiesListener _listener) {
    listener_.remove(_listener);
  }

}
