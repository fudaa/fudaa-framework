/*
 * @creation 1 oct. 2004
 * 
 * @modification $Date: 2006-09-19 14:43:22 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarDonneesBrutesBathy.java,v 1.7 2006-09-19 14:43:22 deniger Exp $
 */
public class H2dRubarDonneesBrutesBathy extends H2dRubarDonneesBrutes {

  /**
   * @param _fmt le formatteur de nombre
   */
  public H2dRubarDonneesBrutesBathy(final H2dRubarNumberFormatter _fmt) {
    super(1, _fmt);
  }

  @Override
  public String getID() {
    return H2dRubarDonneesBrutesMng.BATY_ID;
  }


  @Override
  public String getTitle() {
    return H2dResource.getS("bathymétrie");
  }

  @Override
  public Object getVariableId(final int _idx) {
    return H2dVariableType.BATHYMETRIE;
  }

  @Override
  public final boolean isVariableActive(final int _idx) {
    return true;
  }
}