/*
 *  @creation     29 sept. 2004
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

/**
 * @author Fred Deniger
 * @version $Id: H2dRegularGridInterface.java,v 1.6 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dRegularGridInterface {

  double getDX();

  double getDY();

  /**
   * @return le nombre de point de la grille
   */
  int getNbPoint();

  int getNbPtOnX();

  int getNbPtOnY();

  double getX0();

  double getY0();
}