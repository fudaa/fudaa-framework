/*
 * @creation 9 mars 2006
 * @modification $Date: 2007-04-30 14:21:36 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author fred deniger
 * @version $Id: H2DTelemacNodalPropertiesDelegate.java,v 1.4 2007-04-30 14:21:36 deniger Exp $
 */
public class H2DTelemacNodalPropertiesDelegate extends H2DTelemacNodalPropertiesMngAbstract {

  final H2dTelemacNodalProperties target_;

  public H2DTelemacNodalPropertiesDelegate(final EfGridInterface _grid, final H2dTelemacNodalProperties _target) {
    super(_grid);
    target_ = _target;
  }

  @Override
  protected void fireChanged(final H2dVariableType _var) {
    target_.fireChanged(_var);

  }

  /**
   * pour le rendre visible dans ce package.
   */
  @Override
  protected void clearVarList() {
    super.clearVarList();
  }

  @Override
  protected void fireVarAddedProcess(final H2dVariableType _var) {
    target_.fireVarAdded(_var);
  }

  @Override
  protected void fireVarRemovedProcess(final H2dVariableType _var) {
    target_.fireVarRemoved(_var);

  }

}
