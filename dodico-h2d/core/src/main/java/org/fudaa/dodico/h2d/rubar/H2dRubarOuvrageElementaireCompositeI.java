package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.h2d.type.H2dRubarOuvrageCompositeTypeControle;

/**
 * @author CANEL Christophe (Genesis)
 */
public interface H2dRubarOuvrageElementaireCompositeI extends H2dRubarOuvrageElementaireInterface {
  /**
   * @return le type de contrôle (T temps ou Z niveau).
   */
  H2dRubarOuvrageCompositeTypeControle getTypeControle();
  /**
   * @return x maille amont pour z.
   */
  double getXAmont();
  /**
   * @return y maille amont pour z.
   */
  double getYAmont();
  /**
   * @return x maille aval pour z.
   */
  double getXAval();
  /**
   * @return y maille aval pour z.
   */
  double getYAval();
  /**
   * @return valeurs de temps ou niveau.
   */
  double[] getValeurs();
  /**
   * @return ouvrages élémentaires.
   */
  H2dRubarOuvrageElementaireInterface[] getOuvrages();
}