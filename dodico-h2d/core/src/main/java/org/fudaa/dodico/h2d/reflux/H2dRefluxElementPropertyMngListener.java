/*
 * @creation 28 nov. 2003
 * @modification $Date: 2006-04-19 13:19:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.dodico.h2d.reflux;

import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author deniger
 * @version $Id: H2dRefluxElementPropertyMngListener.java,v 1.5 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dRefluxElementPropertyMngListener {

  /**
   * @param source la source de l'evt
   * @param _v la variable modifiee
   */
  void elementPropertyChanged(H2dRefluxElementPropertyMngAbstract _source,H2dVariableType _v);
}