/*
 * @creation 28 mars 07
 * @modification $Date: 2007-04-30 14:21:34 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.dico.DicoCasFileFormatAbstract;
import org.fudaa.dodico.dico.DicoCasFileFormatVersion;
import org.fudaa.dodico.dico.DicoModelAbstract;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;

/**
 * @author fred deniger
 * @version $Id: H2DRubarDicoCasFileFormatVersion.java,v 1.2 2007-04-30 14:21:34 deniger Exp $
 */
public abstract class H2DRubarDicoCasFileFormatVersion extends DicoCasFileFormatVersion {

  public H2DRubarDicoCasFileFormatVersion(final DicoCasFileFormatAbstract _ft, final DicoModelAbstract _dico) {
    super(_ft, _dico);
  }

  public abstract  H2dRubarProjetType getProjectType();

  public abstract  int getNbTransportBlocks() ;
  
  public abstract H2DRubarDicoCasFileFormatVersion createNewForProject(H2dRubarProjetType _type,int nbTransportBlock);

}
