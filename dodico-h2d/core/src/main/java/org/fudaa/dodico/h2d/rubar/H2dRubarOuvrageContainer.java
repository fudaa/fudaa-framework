/*
 *  @creation     3 janv. 2005
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarOuvrageContainer.java,v 1.3 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dRubarOuvrageContainer {

  /**
   * @return le nombre d'ouvrage contenu par ce conteneur
   */
  int getNbOuvrage();

  /**
   * @param _i appartien a [0,getNbOuvrage()[
   * @return l'ouvrage demande sans verif sur les indices de bords.
   */
  H2dRubarOuvrageI getOuvrage(int _i);
}