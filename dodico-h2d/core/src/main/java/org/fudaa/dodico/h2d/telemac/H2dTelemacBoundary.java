/*
 * @creation 23 oct. 2003
 * 
 * @modification $Date: 2007-06-05 08:59:12 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import com.memoire.fu.FuLog;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.dodico.h2d.H2dBcListenerDispatcher;
import org.fudaa.dodico.h2d.H2dBoundary;
import org.fudaa.dodico.h2d.H2dBoundaryTypeControllerInterface;
import org.fudaa.dodico.h2d.H2dEvolutionFrontiereLiquide;
import org.fudaa.dodico.h2d.H2dEvolutionUseCounter;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dBoundaryTypeCommon;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author deniger
 * @version $Id: H2dTelemacBoundary.java,v 1.27 2007-06-05 08:59:12 deniger Exp $
 */
public class H2dTelemacBoundary extends H2dBoundary {

  H2dBcType tracerType_;

  H2dBcType uType_;

  H2dBcType vType_;

  String elevation_;

  String velocity_;

  String flowrate_;

  List<String> tracer_ = new ArrayList<String>();

  String velocityProfil_;

  String liquidOption_;

  Map<H2dVariableType, EvolutionReguliereAbstract> varEvolutionButTracer_ = new HashMap<H2dVariableType, EvolutionReguliereAbstract>();

  /**
   * Contains the evolutions for tracer in the right order.
   */
  List<EvolutionReguliereAbstract> tracersEvolution = new ArrayList<EvolutionReguliereAbstract>();

  H2dBoundaryTypeControllerInterface controller_;

  /**
   * @param _controller le controleur de validite
   */
  public H2dTelemacBoundary(final int _idxFr, final H2dBoundaryTypeControllerInterface _controller) {
    super(_idxFr);
    tracerType_ = H2dBcType.LIBRE;
    uType_ = H2dBcType.LIBRE;
    vType_ = H2dBcType.LIBRE;
    controller_ = _controller;
  }

  /**
   * @param _s la collection a remplir avec les evolutions utilisees.
   */
  public void fillWithUsedEvol(final Set _s) {
    if (varEvolutionButTracer_ != null) {
      for (final Iterator it = varEvolutionButTracer_.values().iterator(); it.hasNext();) {
        _s.add(it.next());
      }
    }
  }

  /**
   * @return true si au moins une evolution est utilisee.
   */
  public boolean isOneCurveUsed() {
    for (EvolutionReguliereAbstract evol : tracersEvolution) {
      if (evol != null) {
        return true;
      }
    }
    return !varEvolutionButTracer_.isEmpty();

  }

  /**
   * @param _s la table a remplir avec evolution/variable
   */
  public void fillWithVariableEvol(final H2dEvolutionUseCounter _s, final int _idx) {
    if (varEvolutionButTracer_ != null) {
      for (final Iterator it = varEvolutionButTracer_.entrySet().iterator(); it.hasNext();) {
        final Map.Entry e = (Map.Entry) it.next();
        _s.add((EvolutionReguliereAbstract) e.getValue(), (H2dVariableType) e.getKey(), _idx);
      }
    }
    for (EvolutionReguliereAbstract evol : tracersEvolution) {
      if (evol != null)
        _s.add(evol, H2dVariableTransType.TRACEUR, _idx);
    }
  }

  /**
   * @return un memento pour ce bord.
   */
  public H2dTelemacBoundary createMemento() {
    final H2dTelemacBoundary r = new H2dTelemacBoundary(getIdxFrontiere(), controller_);
    r.initFrom(this);
    return r;
  }

  public List<EvolutionReguliereAbstract> getTracersEvolution() {
    return Collections.unmodifiableList(tracersEvolution);
  }

  private void initSimpleValues(final H2dTelemacBoundary _init, final H2dBcListenerDispatcher _l) {
    idxDeb_ = _init.idxDeb_;
    idxFin_ = _init.idxFin_;
    setBoundaryType(_init.type_, _l);
    elevation_ = _init.elevation_;
    velocity_ = _init.velocity_;
    flowrate_ = _init.flowrate_;
    setTracerType(_init.tracerType_, _l);
    tracer_ = _init.tracer_;
    velocityProfil_ = _init.velocityProfil_;
    liquidOption_ = _init.liquidOption_;
    setUVType(_init.uType_, _init.vType_, _l);
  }

  protected final void fillListWithLiquidEvolution(final List<H2dTelemacEvolutionFrontiere> _l, final int _idx) {
    if (!varEvolutionButTracer_.isEmpty()) {
      for (final Iterator it = varEvolutionButTracer_.entrySet().iterator(); it.hasNext();) {
        final Map.Entry e = (Map.Entry) it.next();
        final EvolutionReguliere evol = (EvolutionReguliere) e.getValue();
        final H2dEvolutionFrontiereLiquide liq = new H2dEvolutionFrontiereLiquide((H2dVariableType) e.getKey(), _idx, evol);
        if (evol.getUnite() == null) {
          evol.setUnite(((H2dVariableType) e.getKey()).getCommonUnit());
        }
        _l.add(new H2dTelemacEvolutionFrontiere(liq, -1));
      }

    }
    if (tracersEvolution != null) {
      int nb = tracersEvolution.size();
      for (int i = 0; i < nb; i++) {
        final EvolutionReguliere evol = (EvolutionReguliere) tracersEvolution.get(i);
        if (evol != null) {
          final H2dEvolutionFrontiereLiquide liq = new H2dEvolutionFrontiereLiquide(H2dVariableTransType.TRACEUR, _idx, evol);
          if (evol.getUnite() == null) {
            evol.setUnite(H2dVariableTransType.TRACEUR.getCommonUnit());
          }
          _l.add(new H2dTelemacEvolutionFrontiere(liq, i));
        }
      }
    }
  }

  /**
   * Met a jour ce bord � partir d'un autre :pas d'evt. L'utilisation des evolutions n'est pas mise a jour.
   * 
   * @param _init le bord a partir d'un autre
   */
  private void initFrom(final H2dTelemacBoundary _init) {
    controller_ = _init.controller_;
    initSimpleValues(_init, null);
    if (_init.varEvolutionButTracer_ == null) {
      if (varEvolutionButTracer_ != null) {
        varEvolutionButTracer_.clear();
      }
    } else {
      varEvolutionButTracer_ = new HashMap(_init.varEvolutionButTracer_);
    }
    tracersEvolution.clear();
    tracersEvolution.addAll(_init.tracersEvolution);
  }

  /**
   * @return un memento pour le tableau des evolutions (var->evol)
   */
  public final Map<H2dVariableType, List<EvolutionReguliereAbstract>> createVarEvolMemento() {
    Map<H2dVariableType, List<EvolutionReguliereAbstract>> res = new HashMap<H2dVariableType, List<EvolutionReguliereAbstract>>();
    Set<Entry<H2dVariableType, EvolutionReguliereAbstract>> entrySet = varEvolutionButTracer_.entrySet();
    for (Entry<H2dVariableType, EvolutionReguliereAbstract> entry : entrySet) {
      EvolutionReguliereAbstract value = entry.getValue();
      if (value != null) {
        res.put(entry.getKey(), new ArrayList<EvolutionReguliereAbstract>(Arrays.asList(value)));
      }
    }
    if (!tracersEvolution.isEmpty()) {
      res.put(H2dVariableTransType.TRACEUR, new ArrayList<EvolutionReguliereAbstract>(tracersEvolution));
    }
    return res;
  }

  /**
   * Permet de restaurer les valeurs enregistrees par le memento.
   * 
   * @param _m le tableau cree par la methode createVarEvolMemento
   */
  public final void updateVarEvolFromMemento(final Map<H2dVariableType, List<EvolutionReguliereAbstract>> _m) {
    final Map newMap = new HashMap(_m.size());
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("synchronize current and memento");
    }
    for (final Iterator<Entry<H2dVariableType, EvolutionReguliereAbstract>> it = varEvolutionButTracer_.entrySet().iterator(); it.hasNext();) {
      final Map.Entry<H2dVariableType, EvolutionReguliereAbstract> e = it.next();
      final H2dVariableType v = e.getKey();
      final EvolutionReguliereAbstract evol = e.getValue();
      if (_m.containsKey(v)) {
        final EvolutionReguliereAbstract savedEvol = _m.get(v).get(0);
        newMap.put(v, savedEvol);
        if (!savedEvol.equals(evol)) {
          evol.setUnUsed(false);
          savedEvol.setUsed(false);
        }
      } else {
        evol.setUnUsed(false);
      }
    }
    for (final Iterator<Entry<H2dVariableType, List<EvolutionReguliereAbstract>>> it = _m.entrySet().iterator(); it.hasNext();) {
      final Map.Entry<H2dVariableType, List<EvolutionReguliereAbstract>> e = it.next();
      final H2dVariableType v = e.getKey();
      if (isTracer(v)) {
        continue;//tracer will be managed later.
      }
      final EvolutionReguliereAbstract evol = e.getValue().get(0);
      if (!varEvolutionButTracer_.containsKey(v)) {
        evol.setUsed(false);
        newMap.put(v, evol);
      }
    }
    varEvolutionButTracer_ = newMap;
    List<EvolutionReguliereAbstract> tracers = _m.get(H2dVariableTransType.TRACEUR);
    int nbTracers = tracersEvolution.size();
    assert (tracers.size() == nbTracers);
    for (int i = 0; i < nbTracers; i++) {
      EvolutionReguliereAbstract current = tracersEvolution.get(i);
      EvolutionReguliereAbstract newEvol = tracers.get(i);
      if (current != newEvol) {
        if (current != null) {
          current.setUnUsed(false);
        }
        if (newEvol != null) {
          newEvol.setUsed(false);
        }
        tracersEvolution.set(i, newEvol);
      }
    }

  }

  @Override
  public void checkNbValues(int nbIntern, String defaultValue, H2dVariableType variable) {
    if (isTracer(variable) && tracer_.size() != nbIntern) {
      List<String> olValues = new ArrayList<String>(tracer_);
      List<EvolutionReguliereAbstract> olEvolution = new ArrayList<EvolutionReguliereAbstract>(tracersEvolution);
      for (EvolutionReguliereAbstract evolutionReguliereAbstract : olEvolution) {
        evolutionReguliereAbstract.setUnUsed(false);
      }
      tracer_.clear();
      olEvolution.clear();
      for (int i = 0; i < nbIntern; i++) {
        String val = defaultValue;
        EvolutionReguliereAbstract evol = null;
        if (i < olValues.size()) {
          val = olValues.get(i);
        }
        if (i < olEvolution.size()) {
          evol = olEvolution.get(i);
        }
        if (evol != null) {
          evol.setUsed(false);
        }
        tracer_.add(val);
        tracersEvolution.add(evol);
      }

    }
  }

  /**
   * Met a jour a partir de init et envoie les evt necessaires.
   * 
   * @param _init le source
   */
  protected final void updateFrom(final H2dTelemacBoundary _init, final H2dBcListenerDispatcher _l) {
    initSimpleValues(_init, _l);
    updateVarEvolFromMemento(_init.createVarEvolMemento());
  }

  /**
   * @param _t la varialbe demande
   * @return l'evolution correspondante ou null si aucune.
   */
  private EvolutionReguliereAbstract getEvolution(final H2dVariableType _t) {
    return ((varEvolutionButTracer_ == null) ? null : (EvolutionReguliere) varEvolutionButTracer_.get(_t));
  }

  public EvolutionReguliereAbstract getEvolution(H2dVariableType type, int idx) {
    if (isTracer(type)) {
      return tracersEvolution.get(idx);
    }
    return getEvolution(type);

  }

  public List<EvolutionReguliereAbstract> getEvolutions(H2dVariableType type) {
    if (isTracer(type)) {
      return new ArrayList<EvolutionReguliereAbstract>(tracersEvolution);
    }
    EvolutionReguliereAbstract evolution = getEvolution(type);
    if (evolution == null) {
      return Collections.emptyList();
    }
    List<EvolutionReguliereAbstract> res = new ArrayList<EvolutionReguliereAbstract>();
    res.add(evolution);
    return res;

  }

  /**
   * Pour les bords permettant de modifier les comportements des variables u et v, renvoie true si les 2 composantes
   * peuvent etre initialis�e en fixe (bord vitesses imposees et hauteur et vitesses imposees). For the boundaries for
   * wich the velocity comportment ( u and v) can be modified (free or fixed) return true, if u an d v can be set to
   * fixed.
   * 
   * @param _b le type de bord a tester
   * @return true si u et v peuvent etre permanent
   */
  public final static boolean canUandVBeSetAsPrescibedFor(final H2dBoundaryType _b) {
    return ((_b == H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES) || (_b == H2dBoundaryTypeCommon.LIQUIDE_VITESSES_IMPOSEES));
  }

  /**
   * Pour les bords permettant de modifier les comportements des variables u et v, . For the boundaries for wich the
   * velocity comportment ( u and v) can be modified (free or fixed) return true, if u an d v can be set to free.
   * 
   * @param _b le type a tester
   * @return true si les 2 composantes peuvent etre initialis�e en libre ( bord debit)
   */
  public final static boolean canUandVBeSetAsFreeFor(final H2dBoundaryType _b) {
    return _b == H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE;
  }

  /**
   * @param _b le type a tester
   * @return true si les comportement de u et v peuvent etre modifies
   */
  public final static boolean canUandVBeChanged(final H2dBoundaryType _b) {
    return ((_b == H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES) || (_b == H2dBoundaryTypeCommon.LIQUIDE_VITESSES_IMPOSEES) || (_b == H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE));
  }

  /**
   * @return true si les comportement de u et v peuvent etre modifies
   */
  public final boolean canUandVBeChanged() {
    return canUandVBeChanged(getType());
  }

  /**
   * @return comportement du traceur
   */
  public H2dBcType getTracerType() {
    return tracerType_;
  }

  /**
   * @return true si permanent
   */
  public boolean isTracerImposed() {
    return tracerType_ == H2dBcType.PERMANENT;
  }

  /**
   * @return true si permanent
   */
  public boolean isUTypeImposed() {
    return uType_ == H2dBcType.PERMANENT;
  }

  /**
   * @return true si permanent
   */
  public boolean isVTypeImposed() {
    return vType_ == H2dBcType.PERMANENT;
  }

  /**
   * @param _isImposed true si doit etre impose
   * @param _l le listener cible de l'evt (peut etre null)
   * @return true si modif
   */
  private boolean setUTypeImposed(final boolean _isImposed, final H2dBcListenerDispatcher _l) {
    if (_isImposed != isUTypeImposed()) {
      uType_ = _isImposed ? H2dBcType.PERMANENT : H2dBcType.LIBRE;
      if (_l != null) {
        _l.fireBcParametersChanged(this, H2dVariableType.VITESSE_U);
      }
      return true;
    }
    return false;
  }

  protected void setEvolutionUsedState(final boolean _b, final boolean _isAdjusting) {
    if ((varEvolutionButTracer_ != null) && (varEvolutionButTracer_.size() > 0)) {
      if (_b) {
        for (final Iterator it = varEvolutionButTracer_.values().iterator(); it.hasNext();) {
          ((EvolutionReguliereAbstract) it.next()).setUsed(_isAdjusting);
        }
      } else {
        for (final Iterator it = varEvolutionButTracer_.values().iterator(); it.hasNext();) {
          ((EvolutionReguliereAbstract) it.next()).setUnUsed(_isAdjusting);
        }
      }
    }
  }

  private boolean setVTypeImposed(final boolean _b, final H2dBcListenerDispatcher _l) {
    if (_b != isVTypeImposed()) {
      vType_ = _b ? H2dBcType.PERMANENT : H2dBcType.LIBRE;
      if (_l != null) {
        _l.fireBcParametersChanged(this, H2dVariableType.VITESSE_U);
      }
      return true;
    }
    return false;
  }

  protected boolean setTracerTypeImposed(final boolean _b, final H2dBcListenerDispatcher _l) {
    if ((getType().isLiquide()) && (_b != isTracerImposed())) {
      tracerType_ = _b ? H2dBcType.PERMANENT : H2dBcType.LIBRE;
      if (_l != null) {
        _l.fireBcParametersChanged(this, H2dVariableTransType.TRACEUR);
      }
      return true;
    }
    return false;
  }

  protected boolean setTracerType(final H2dBcType _t, final H2dBcListenerDispatcher _l) {
    if ((_t == H2dBcType.PERMANENT) || (_t == H2dBcType.LIBRE)) {
      return setTracerTypeImposed(_t == H2dBcType.PERMANENT, _l);
    }
    return false;
  }

  @Override
  protected boolean setBoundaryType(final H2dBoundaryType _type, final H2dBcListenerDispatcher _l) {
    final boolean r = super.setBoundaryType(_type, _l);
    if (r) {
      // boolean typeModified = false;
      boolean tracer = isTracerImposed();
      if (!canTracerBeSet()) {
        tracer = false;
      }
      boolean uImposed = _type == H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES || _type == H2dBoundaryTypeCommon.LIQUIDE_VITESSES_IMPOSEES;
      /* typeModified |= */setTypeImposed(uImposed, uImposed, tracer, _l);
    }
    if ((varEvolutionButTracer_ != null) && (varEvolutionButTracer_.size() > 0)) {
      final List t = controller_.getVariableFor(getType());
      if ((t != null) && (t.size() > 0)) {
        for (final Iterator it = varEvolutionButTracer_.entrySet().iterator(); it.hasNext();) {
          final Map.Entry e = (Map.Entry) it.next();
          if (!t.contains(e.getKey())) {
            it.remove();
            ((EvolutionReguliereAbstract) e.getValue()).setUnUsed(false);
          }
        }
      } else {
        setEvolutionUsedState(false, false);
        varEvolutionButTracer_.clear();
      }
    }
    return r;
  }

  @Override
  protected void setIdxDeb(final int _i) {
    super.setIdxDeb(_i);
  }

  @Override
  protected void setIdxFin(final int _i) {
    super.setIdxFin(_i);
  }

  @Override
  protected void setIdxmaillageFrontiere(final int _i) {
    super.setIdxmaillageFrontiere(_i);
  }

  /**
   * @return valeur de l'elevation (def par mot-cle)
   */
  public String getElevation() {
    return elevation_;
  }

  /**
   * @return valeur du debit (def par mot-cle)
   */
  public String getFlowrate() {
    return flowrate_;
  }

  /**
   * @return valeur de l'option liquide (def par mot-cle)
   */
  public String getLiquidOption() {
    return liquidOption_;
  }

  /**
   * @return valeur pour le traceur (def par mot-cle)
   */
  public List<String> getTracer() {
    return tracer_;
  }

  /**
   * @return valeur pour la vitesse (def par mot-cle)
   */
  public String getVelocity() {
    return velocity_;
  }

  /**
   * @return le profil pour les vitesses
   */
  public String getVelocityProfil() {
    return velocityProfil_;
  }

  protected boolean setElevation(final String _string) {
    if (_string != elevation_) {
      elevation_ = _string;
      return true;
    }
    return false;
  }

  protected boolean setFlowrate(final String _string) {
    if (_string != flowrate_) {
      flowrate_ = _string;
      return true;
    }
    return false;
  }

  protected boolean setLiquidOption(final String _string) {
    if (_string != liquidOption_) {
      liquidOption_ = _string;
      return true;
    }
    return false;
  }

  protected boolean setTracer(final String _string, int idx) {
    String set = tracer_.set(idx, _string);
    return !CtuluLib.isEquals(set, _string);
  }

  protected boolean setTracer(final String[] _string) {
    int nb = Math.min(_string.length, tracer_.size());
    boolean mod = false;
    for (int i = 0; i < nb; i++) {
      mod |= setTracer(_string[i], i);
    }

    return mod;
  }

  /**
   *
   */
  protected boolean setVelocity(final String _string) {
    if (_string != velocity_) {
      velocity_ = _string;
      return true;
    }
    return false;
  }

  /**
   *
   */
  protected boolean setVelocityProfil(final String _string) {
    if (_string != velocityProfil_) {
      velocityProfil_ = _string;
      return true;
    }
    return false;
  }

  protected boolean setValue(final H2dVariableType _type, final String _string) {
    if (_type == H2dVariableType.VITESSE) {
      return setVelocity(_string);
    } else if (_type == H2dVariableType.COTE_EAU) {
      return setElevation(_string);
    } else if (_type == H2dVariableType.DEBIT) {
      return setFlowrate(_string);
    } else if (_type == H2dVariableTransType.TRACEUR) {
      throw new IllegalAccessError("tracer must be set as an array");
    }
    FuLog.error("variable unknown for " + getClass().getName());
    new Throwable().printStackTrace();
    return false;
  }

  /**
   * @param _t le type a tester
   * @return true si impose permanent (impose par mot-cle)
   */
  public boolean isTypeImposed(final H2dVariableType _t) {
    if (_t == H2dVariableType.VITESSE_U) {
      return isUTypeImposed();
    } else if (_t == H2dVariableType.VITESSE_V) {
      return isVTypeImposed();
    } else if (_t == H2dVariableTransType.TRACEUR) {
      return isTracerImposed();
    } else {
      FuLog.error("variable unknown " + _t);
      new Throwable().printStackTrace();
      return false;
    }
  }

  /**
   * @param _v la variable a tester
   * @param idxVariable TODO
   * @return si evolution definie pour cette variable
   */
  public boolean isVariableTransient(final H2dVariableType _v, int idxVariable) {
    if (isTracer(_v)) {
      return !tracersEvolution.isEmpty() && tracersEvolution.size() > idxVariable && tracersEvolution.get(idxVariable) != null;
    }
    return varEvolutionButTracer_.containsKey(_v);
  }

  public boolean isVariableTransientOneTime(final H2dVariableType _v) {
    if (isTracer(_v)) {
      for (EvolutionReguliereAbstract evol : tracersEvolution) {
        if (evol != null) {
          return true;
        }
      }
      return false;
    }
    return varEvolutionButTracer_.containsKey(_v);
  }

  /**
   * @param _t le type a tester
   * @return true si le traceur peut etre modifie pour ce type
   */
  public static boolean canTracerBeSet(final H2dBoundaryType _t) {
    return _t.isLiquide();
  }

  /**
   * @return si le traceur peut etre modifie
   */
  public boolean canTracerBeSet() {
    return canTracerBeSet(getType());
  }

  protected boolean removeTransientVar(final H2dVariableType _t) {
    if (isTracer(_t) && !tracersEvolution.isEmpty()) {
      for (EvolutionReguliereInterface evol : tracersEvolution) {
        if (evol != null) {
          evol.setUnUsed(false);
        }
      }
      tracersEvolution.clear();

    }
    if ((varEvolutionButTracer_ != null) && (varEvolutionButTracer_.containsKey(_t))) {
      varEvolutionButTracer_.remove(_t).setUnUsed(false);
      return true;
    }
    return false;
  }

  protected boolean setVariableTransient(final H2dVariableType _v, final EvolutionReguliereAbstract _evol, int idxVar) {
    if (controller_.getVariableFor(getType()).contains(_v)) {
      return setVariableTransientWithoutControl(_v, _evol, idxVar);
    }
    return false;
  }

  protected boolean setVariableTransient(final H2dVariableType _v, final List<EvolutionReguliereAbstract> _evol) {
    if (controller_.getVariableFor(getType()).contains(_v)) {
      int nb = _evol.size();
      for (int i = 0; i < nb; i++) {
        return setVariableTransientWithoutControl(_v, _evol.get(i), i);
      }

    }
    return false;
  }

  private boolean setVariableTransientWithoutControl(final H2dVariableType _v, final EvolutionReguliereAbstract _evol, int idxVar) {

    if (isTracer(_v)) {
      EvolutionReguliereInterface old = tracersEvolution.set(idxVar, _evol);
      if (_evol != null) {
        _evol.setUsed(false);
      }
      if (old != null) {
        old.setUnUsed(false);
      }
      return true;

    }
    if (_evol == null) {
      return false;
    }
    if ((!varEvolutionButTracer_.containsKey(_v))) {
      _evol.setUsed(false);
      varEvolutionButTracer_.put(_v, _evol);
      return true;
    }
    final EvolutionReguliereAbstract old = varEvolutionButTracer_.get(_v);
    if (old != _evol) {
      old.setUnUsed(false);
      varEvolutionButTracer_.put(_v, _evol);
      _evol.setUsed(false);
      return true;
    }
    return false;
  }

  private boolean isTracer(final H2dVariableType _v) {
    return H2dVariableTransType.TRACEUR.equals(_v);
  }

  protected boolean setVariableTransient(final Map<H2dVariableType, List<EvolutionReguliereAbstract>> _m) {
    if (_m == null) {
      return false;
    }
    boolean r = false;
    H2dVariableType t;
    // on controle que les evolutions sont autorisees par ce
    // le type de bord: on enleve les variables non contenues
    // dans les variables autorisees
    final List avail = controller_.getVariableFor(getType());
    for (final Iterator it = _m.keySet().iterator(); it.hasNext();) {
      final Object o = it.next();
      if (!avail.contains(o)) {
        it.remove();
      }
    }
    // On enleve du tableau courant les variables qui ne sont
    // plus a utiliser
    if (varEvolutionButTracer_ != null) {
      for (final Iterator it = varEvolutionButTracer_.keySet().iterator(); it.hasNext();) {
        t = (H2dVariableType) it.next();
        if (!_m.containsKey(t)) {
          r |= removeTransientVar(t);
        }
      }
    }
    if (_m.isEmpty()) {
      int nb = tracersEvolution.size();
      for (int intern = 0; intern < nb; intern++) {
        r |= setVariableTransientWithoutControl(H2dVariableTransType.TRACEUR,null, intern);
      }

    } else {
      // on met a jour les nouvelles evol
      for (Entry<H2dVariableType, List<EvolutionReguliereAbstract>> e : _m.entrySet()) {
        if (H2dVariableTransType.TRACEUR.equals(e.getKey())) {
          List<EvolutionReguliereAbstract> newTracerEvolutions = e.getValue();
          int nb = tracersEvolution.size();
          int nbIn = newTracerEvolutions.size();
          assert nb == nbIn;
          for (int intern = 0; intern < nb; intern++) {
            r |= setVariableTransientWithoutControl(H2dVariableTransType.TRACEUR, newTracerEvolutions.get(intern), intern);
          }
        } else {
          r |= setVariableTransientWithoutControl(e.getKey(), e.getValue().get(0), 0);
        }
      }
    }
    return r;
  }

  protected boolean setTypeImposed(final boolean _u, final boolean _v, final boolean _t, final H2dBcListenerDispatcher _l) {
    return setUVType(_u, _v, _l) | setTracerTypeImposed(_t, _l);
  }

  protected boolean setTypeImposed(final H2dVariableType _t, final boolean _b, final H2dBcListenerDispatcher _l) {
    if (_t == H2dVariableTransType.TRACEUR) {
      return setTracerTypeImposed(_b, _l);
    }
    boolean uI = isUTypeImposed();
    boolean vI = isVTypeImposed();
    if (_t == H2dVariableType.VITESSE_U) {
      uI = _b;
    } else if (_t == H2dVariableType.VITESSE_V) {
      vI = _b;
    } else {
      new Throwable("inconnu").printStackTrace();
    }
    return setUVType(uI, vI, _l);
  }

  protected boolean setUVType(final boolean _uImposed, final boolean _vImposed, final H2dBcListenerDispatcher _l) {
    final H2dBoundaryType t = getType();
    boolean r = false;
    if ((t == H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES) || (t == H2dBoundaryTypeCommon.LIQUIDE_VITESSES_IMPOSEES)) {
      if ((_uImposed) || (_vImposed)) {
        r = ((setUTypeImposed(_uImposed, _l) | setVTypeImposed(_vImposed, _l)));
      }
    } else if (t == H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE) {
      if ((!_uImposed) || (!_vImposed)) {
        r = (setUTypeImposed(_uImposed, _l) | setVTypeImposed(_vImposed, _l));
      }
    }
    if (r && (_l != null)) {
      _l.fireBcParametersChanged(this, null);
    }
    return r;
  }

  protected boolean setUVType(final H2dBcType _u, final H2dBcType _v, final H2dBcListenerDispatcher _l) {
    return setUVType(_u == H2dBcType.PERMANENT, _v == H2dBcType.PERMANENT, _l);
  }

  /**
   * For a solid boundary, the extrem idx are not belonging to this boundary Si c'est un bord solid, les extremites ne
   * sont pas prise en compte.
   * 
   * @param _idx l'index sur la frontiere
   * @return true si cette frontiere contient l'index
   */
  @Override
  public boolean containsIdx(final int _idx) {
    if (getType().isLiquide()) {
      return super.containsIdx(_idx);
    }
    return idxDeb_ == idxFin_ ? true : ((idxDeb_ < idxFin_) ? ((_idx > idxDeb_) && (_idx < idxFin_))
        : ((_idx >= 0) && ((_idx < idxFin_) || (_idx > idxDeb_))));
  }

  @Override
  public int[] getIdx(final int _nbPoint) {
    if (getType().isLiquide()) {
      return super.getIdx(_nbPoint);
    }
    final int[] res = super.getIdx(_nbPoint);
    if (res == null || res.length < 2) {
      return null;
    }
    final int[] resSolid = new int[res.length - 2];
    System.arraycopy(res, 1, resSolid, 0, resSolid.length);
    return resSolid;

  }

  /**
   * @return comportement pour u
   */
  public H2dBcType getUType() {
    return uType_;
  }

  /**
   * @return comportement pour v
   */
  public H2dBcType getVType() {
    return vType_;
  }

  protected void setNbTracer(final int nb, final String defaultValue, final int[] idxToRemove, CtuluCommandContainer cmp) {
    if (tracersEvolution.size() == nb && tracer_.size() == nb && idxToRemove == null) {
      return;
    }
    final List<EvolutionReguliereAbstract> old = new ArrayList<EvolutionReguliereAbstract>();
    old.addAll(tracersEvolution);
    final List<EvolutionReguliereInterface> removed = new ArrayList<EvolutionReguliereInterface>();
    //first we remove asked indexes:
    if (idxToRemove != null) {
      Arrays.sort(idxToRemove);
      for (int i = idxToRemove.length - 1; i >= 0; i--) {
        int idx = idxToRemove[i];
        EvolutionReguliereInterface remove = tracersEvolution.remove(idx);
        if (remove != null) {
          removed.add(remove);
          remove.setUnUsed(false);
        }

      }
    }
    for (int i = tracersEvolution.size() - 1; i >= nb; i--) {
      EvolutionReguliereInterface remove = tracersEvolution.remove(i);
      if (remove != null) {
        removed.add(remove);
        remove.setUnUsed(false);
      }
    }
    int start = tracersEvolution.size();
    for (int i = start; i < nb; i++) {
      tracersEvolution.add(null);
    }

    List<String> currentValues = new ArrayList<String>();
    currentValues.addAll(tracer_);

    if (idxToRemove != null) {
      Arrays.sort(idxToRemove);
      for (int i = idxToRemove.length - 1; i >= 0; i--) {
        int idx = idxToRemove[i];
        currentValues.remove(idx);
      }
    }
    int min = Math.min(nb, currentValues.size());
    final List<String> oldValues = tracer_;
    tracer_ = new ArrayList<String>();
    for (int i = 0; i < min; i++) {
      tracer_.add(currentValues.get(i));
    }
    for (int i = min; i < nb; i++) {
      tracer_.add(defaultValue);
    }
    if (cmp != null) {
      cmp.addCmd(new CtuluCommand() {

        @Override
        public void undo() {
          tracer_ = oldValues;
          for (EvolutionReguliereInterface current : tracersEvolution) {
            if (current != null) {
              current.setUnUsed(false);
            }
          }
          for (EvolutionReguliereInterface oldEvolution : old) {
            if (oldEvolution != null) {
              oldEvolution.setUsed(false);
            }
          }
          tracersEvolution.clear();
          tracersEvolution.addAll(old);

        }

        @Override
        public void redo() {
          setNbTracer(nb, defaultValue, idxToRemove, null);

        }
      });
    }

  }

  public void setTracerTransient(EvolutionReguliereAbstract evol, int idxVariable) {
    tracersEvolution.set(idxVariable, evol);
    if (evol != null) {
      evol.setUsed(false);
    }
  }
}