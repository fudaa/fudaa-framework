/*
 * @creation 28 sept. 2004
 *
 * @modification $Date: 2007-05-04 13:46:38 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import gnu.trove.TIntArrayList;
import gnu.trove.TIntIntHashMap;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.*;
import org.fudaa.ctulu.gis.*;
import org.fudaa.ctulu.gis.GISReprojectInterpolateurI.DoubleTarget;
import org.fudaa.dodico.commun.DodicoEnumType;
import org.fudaa.dodico.fortran.FortranDoubleReaderResult;
import org.fudaa.dodico.fortran.FortranDoubleReaderResultInterface;
import org.fudaa.dodico.h2d.*;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarDonneesBrutes.java,v 1.32 2007-05-04 13:46:38 deniger Exp $
 */
public abstract class H2dRubarDonneesBrutes {
  public int getVariableBlockLength() {
    return -1;
  }

  class NuageFileModel implements FortranDoubleReaderResultInterface {
    @Override
    public CollectionPointDataDoubleInterface createXYValuesInterface() {
      return nuage_;
    }

    @Override
    public CtuluNumberFormatI getFormat(final int _col) {
      if (_col <= 1) {
        return fmt_.getXYDb().getNumberFormat();
      }
      return fmt_.getFormatterFor((H2dVariableType) getVariableId(_col - 2)).getNumberFormat();
    }

    @Override
    public int getNbColonne() {
      return 2 + nuage_.getNbAttributes();
    }

    @Override
    public int getNbLigne() {
      return nuage_.getNumPoints();
    }

    @Override
    public CtuluCollectionDouble getCollectionFor(final int _col) {
      return FortranDoubleReaderResult.getCollectionFor(_col, this);
    }

    @Override
    public double getValue(final int _ligne, final int _col) {
      if (_col == 0) {
        return nuage_.getX(_ligne);
      }
      if (_col == 1) {
        return nuage_.getY(_ligne);
      }
      return nuage_.getDatas(_col - 2).getValue(_ligne);
    }
  }

  private static class UnactiveModelDoubleArray extends CtuluArrayDoubleUnique {
    /**
     * @param _v
     */
    public UnactiveModelDoubleArray(final double _v) {
      super(_v);
    }

    @Override
    public int getSize() {
      return 4;
    }
  }

  private class UnactiveModelDoubleListGrid extends CtuluArrayDoubleUnique {
    /**
     * @param _v
     */
    public UnactiveModelDoubleListGrid(final double _v) {
      super(_v);
    }

    @Override
    public int getSize() {
      return grid_ == null ? 0 : grid_.getNbPoint();
    }
  }

  private class UnactiveModelDoubleListNuage extends CtuluCollectionDoubleUnique implements GISAttributeModelDoubleInterface,
      GISAttributeModelObservable {
    final GISAttributeDouble att_;

    /**
     * @param _v
     */
    public UnactiveModelDoubleListNuage(final double _v, final GISAttributeDouble _att) {
      super(_v);
      att_ = _att;
      att_.setEditable(false);
    }

    @Override
    public GISAttributeModel createSubModel(final int[] _idxToRemove) {
      return null;
    }

    @Override
    public GISAttributeModel createUpperModel(final int _numObject, final TIntIntHashMap _newIdxOldIdx) {
      return null;
    }

    @Override
    public GISAttributeModelDoubleInterface deriveNewModel(final int _numObject, final DoubleTarget _interpol) {
      return null;
    }

    @Override
    public GISAttributeModel deriveNewModel(final int _numObject, final GISReprojectInterpolateurI _interpol) {
      return null;
    }

    @Override
    public GISAttributeInterface getAttribute() {
      return att_;
    }

    @Override
    public Object getAverage() {
      return CtuluLib.getDouble(getValue(0));
    }

    @Override
    public int getSize() {
      return nuage_ == null ? 0 : nuage_.getNumPoints();
    }

    @Override
    public void setListener(final GISAttributeListener _listener) {
    }
  }

  class VF2MFileModel implements H2dRubarVF2MResultInterface {
    @Override
    public String getFirstLine() {
      return H2dRubarDonneesBrutes.this.getFirstLine();
    }

    @Override
    public H2dRegularGridDataInterface getGridData() {
      return grid_;
    }

    @Override
    public int getNbParall() {
      return parall_.getNbParall();
    }

    @Override
    public H2dParallelogrammeDataInterface getParall(final int _i) {
      return parall_.getParall(_i);
    }

    @Override
    public boolean isParall() {
      return parall_ != null;
    }
  }

  class DoubleArrayModel extends CtuluArrayDouble {
    /**
     * @param _nb
     */
    public DoubleArrayModel(final int _nb) {
      super(_nb);
    }

    @Override
    protected void fireObjectChanged(int _indexGeom, Object _newValue) {
      fireDonneesBrutesDataChanged();
    }
  }

  class Parallel extends H2dParallelogrammeData {
    Parallel() {
      super(nbVariable_);
    }

    @Override
    protected CtuluCollectionDoubleEdit createDataModel(final int _idx) {
      if (isVariableActive(_idx)) {
        return new DoubleArrayModel(4);
      }
      return new UnactiveModelDoubleArray(getIndeterminedValue(_idx));
    }

    @Override
    protected void fireXYChanged() {
      fireDonneesBrutesSupportChanged();
    }

    @Override
    public void initFromKeepingSameNumberOfValue(final H2dParallelogrammeDataInterface _interface) {
      super.initFromKeepingSameNumberOfValue(_interface);
    }
  }

  class TypeOrganisationCommand implements CtuluCommand {
    OrganizationType new_;
    Object newData_;
    OrganizationType old_;
    Object oldData_;

    /**
     * @param _old
     * @param _oldData
     */
    public TypeOrganisationCommand(final OrganizationType _old, final Object _oldData) {
      super();
      old_ = _old;
      oldData_ = _oldData;
    }

    @Override
    public void redo() {
      if (typeOrganisation_ != null) {
        typeOrganisation_.removeTypeData(H2dRubarDonneesBrutes.this);
      }
      typeOrganisation_ = new_;
      if (typeOrganisation_ != null) {
        typeOrganisation_.setTypeData(H2dRubarDonneesBrutes.this, newData_);
      }
      fireDonneesBrutesTypeChanged();
    }

    @Override
    public void undo() {
      if (typeOrganisation_ != null) {
        new_ = typeOrganisation_;
        newData_ = typeOrganisation_.removeTypeData(H2dRubarDonneesBrutes.this);
      }
      typeOrganisation_ = old_;
      if (typeOrganisation_ != null) {
        typeOrganisation_.setTypeData(H2dRubarDonneesBrutes.this, oldData_);
      }
      fireDonneesBrutesTypeChanged();
    }
  }

  /**
   * grille speciale.
   *
   * @author Fred Deniger
   * @version $Id: H2dRubarDonneesBrutes.java,v 1.32 2007-05-04 13:46:38 deniger Exp $
   */
  public final class GridData extends H2dRegularGridData {
    protected GridData() {
      super(nbVariable_);
    }

    @Override
    protected void addData(int nbToAdd, final CtuluCommandContainer _cmd) {
      super.addData(nbToAdd, _cmd);
    }

    @Override
    protected CtuluCollectionDoubleEdit createModel(final int _idx) {
      if (isVariableActive(_idx)) {
        return new DoubleArrayModel(getNbPoint());
      }
      return new UnactiveModelDoubleListGrid(getIndeterminedValue(_idx));
    }

    @Override
    protected CtuluCollectionDoubleEdit createModel(final int _idx, final CtuluCollectionDoubleEdit _m) {
      if (!H2dRubarDonneesBrutes.this.isVariableActive(_idx)) {
        return new UnactiveModelDoubleListGrid(getIndeterminedValue(_idx));
      }
      final DoubleArrayModel r = new DoubleArrayModel(getNbPoint());
      r.initWith(_m, false);
      return r;
    }

    @Override
    protected void fireDataChanged() {
      fireDonneesBrutesDataChanged();
    }

    @Override
    protected void fireDataNombreChanged() {
      fireDonneesBrutesDataNombreChanged();
    }

    @Override
    protected void fireGridChanged(final boolean _nbPointChanged) {
      if (_nbPointChanged) {
        fireDonneesBrutesSupportAddOrRemoved();
      } else {
        fireDonneesBrutesSupportChanged();
      }
    }

    @Override
    protected CtuluCollectionDoubleEdit[] getValues() {
      return super.getValues();
    }

    protected void initFrom(final H2dRegularGridDataInterface _interface) {
      super.initValFrom(_interface);
    }

    @Override
    protected void removeLastData(int nbToRemove, final CtuluCommandContainer _cmd) {
      super.removeLastData(nbToRemove, _cmd);
    }

    protected void setDataToDefault(final int _idx, final CtuluCommandContainer _cmd) {
      final CtuluCollectionDoubleEdit old = super.values_[_idx];
      // on demande de creer un modele a valeur unique
      super.values_[_idx] = createModel(_idx);
      if (_cmd != null) {
        _cmd.addCmd(new CtuluCommand() {
          @Override
          public void redo() {
            setDataToDefault(_idx, null);
          }

          @Override
          public void undo() {
            getValues()[_idx] = old;
          }
        });
      }
    }

    @Override
    public Object getValueIdentifier(final int _i) {
      return H2dRubarDonneesBrutes.this.getVariableId(_i);
    }
  }

  class NuageListener extends GISZoneListenerDispatcher {
    @Override
    public void attributeAction(Object _source, int att, GISAttributeInterface _att, int _action) {
      fireDonneesBrutesNuageDataNombreChanged();
    }

    @Override
    public void attributeValueChangeAction(Object _source, int att, GISAttributeInterface _att, int geom, Object value) {
      fireDonneesBrutesNuageDataChanged();
    }

    @Override
    public void objectAction(Object _source, int geom, Object _obj, int _action) {
      if (_action == OBJECT_ACTION_MODIFY) {
        fireDonneesBrutesNuageSupportChanged();
      } else {
        fireDonneesBrutesNuageSupportAddOrRemoved();
      }
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: H2dRubarDonneesBrutes.java,v 1.32 2007-05-04 13:46:38 deniger Exp $
   */
  public class NuagePoint extends GISZoneCollectionPoint implements CtuluContainerModelDouble, CollectionPointDataDoubleInterface {
    @Override
    public int getNbValues() {
      return attr_ == null ? 0 : attr_.length;
    }

    /**
     * @param _nbExpectedPoint
     */
    public NuagePoint(final int _nbExpectedPoint) {
      super(_nbExpectedPoint, new NuageListener());
      majAttributes(nbVariable_);
    }

    boolean idxDone = false;
    int idx = -1;

    @Override
    protected int getIdxAttZ() {
      if (idxDone) {
        return idx;
      }
      idxDone = true;
      int nbVariable = getNbVariable();
      for (int i = 0; i < nbVariable; i++) {
        if (H2dVariableType.BATHYMETRIE.equals(getVariableId(i))) {
          idx = i;
          attributeIsZ_ = (GISAttributeDouble) getAttribute(idx);
          return i;
        }
      }
      return idx;
    }

    protected void addData(int nbData, final CtuluCommandContainer _cmd) {
      GISAttributeModelObservable[] toAdd = new GISAttributeModelObservable[nbData];
      for (int i = 0; i < nbData; i++) {
        toAdd[i] = createDoubleModel(super.attr_.length + i);
      }
      addData(toAdd, _cmd);
    }

    protected void addData(GISAttributeModelObservable[] _m, final CtuluCommandContainer _cmd) {
      final GISAttributeModelObservable[] old = super.attr_;
      final GISAttributeModelObservable[] newAtt = new GISAttributeModelObservable[old.length + _m.length];
      System.arraycopy(old, 0, newAtt, 0, old.length);
      for (int i = 0; i < _m.length; i++) {
        _m[i].setListener(this);
        newAtt[old.length + i] = _m[i];
      }
      super.initAttributes(newAtt, _cmd);
    }

    protected GISAttributeDouble createAtt(final int _idx) {
      return new GISAttributeDouble(getVariableId(_idx).toString(), true);
    }

    protected GISAttributeModelObservable createDoubleModel(final int _idx) {
      GISAttributeModelObservable r = null;
      if (isVariableActive(_idx)) {
        r = new GISAttributeModelDoubleList(getNumPoints(), (int) (getNumPoints() * 1.5), createAtt(_idx));
      } else {
        r = new UnactiveModelDoubleListNuage(getIndeterminedValue(_idx), createAtt(_idx));
      }
      r.setListener(this);
      return r;
    }

    protected GISAttributeModelObservable[] getAtts() {
      return attr_;
    }

    /**
     * Surcharge pour etre acceder depuis de CtuluCommand.
     */
    protected CtuluListDouble getDatas(final int _idx) {
      return (CtuluListDouble) attr_[_idx];
    }

    @Override
    protected void initAttributes(final GISAttributeInterface[] _att) {
      super.initAttributes(_att);
    }

    protected void initFromAndKeepSameNbValues(final CollectionPointDataDoubleInterface _i, boolean shouldMajAttributes) {
      if (_i == null) {
        return;
      }
      super.geometry_.removeAll();
      final int nb = _i.getNbPoint();
      final int nbRead = _i.getNbValues();
      final int nbData = Math.min(nbRead, getNbValues());
      if (shouldMajAttributes) {
        majAttributes(nbData);
      }
      GISPoint[] pts = new GISPoint[nb];
      for (int i = 0; i < nb; i++) {
        pts[i] = new GISPoint(_i.getX(i), _i.getY(i), _i.getZ(i));
      }
      super.addAll(pts, null, null);
      for (int i = 0; i < nbData; i++) {
        if (getData(i) instanceof CtuluListDouble) {
          _i.fill(i, (CtuluListDouble) getData(i));
        }
      }
    }

    public final void majAttributes(final int _nbValues) {
      final GISAttributeModelObservable[] att = new GISAttributeModelObservable[_nbValues];
      for (int i = 0; i < _nbValues; i++) {
        final GISAttributeModelObservable l = createDoubleModel(i);
        l.setListener(this);
        att[i] = l;
      }
      super.initAttributes(att);
    }

    protected void removeLastData(int nb, final CtuluCommandContainer _cmd) {
      final GISAttributeModelObservable[] old = super.attr_;
      final int newSize = old.length - nb;
      final GISAttributeModelObservable[] newAtt = new GISAttributeModelObservable[newSize];
      System.arraycopy(old, 0, newAtt, 0, newAtt.length);
      super.initAttributes(newAtt, _cmd);
    }

    protected void setDataToDefault(final int _idx, final CtuluCommandContainer _cmd) {
      final GISAttributeModelObservable old = super.attr_[_idx];
      // on demande de creer un modele a valeur unique
      super.attr_[_idx] = createDoubleModel(_idx);
      super.attr_[_idx].setListener(this);
      fireAttributeValueChanged(_idx, attr_[_idx].getAttribute(), -1, null);
      if (_cmd != null) {
        _cmd.addCmd(new CtuluCommand() {
          @Override
          public void redo() {
            setDataToDefault(_idx, null);
          }

          @Override
          public void undo() {
            getAtts()[_idx] = old;
            fireAttributeValueChanged(_idx, attr_[_idx].getAttribute(), -1, null);
          }
        });
      }
    }

    @Override
    public void fill(final int _idxValue, final CtuluListDouble _m) {
      final CtuluListDouble m = (CtuluListDouble) getData(_idxValue);
      if (m == null) {
        _m.initWith(new double[getNbPoint()]);
      } else {
        _m.initWith(m);
      }
    }

    @Override
    public CtuluCollectionDoubleEdit getDoubleValues(final int _i) {
      return getDatas(_i);
    }

    @Override
    public int getNbPoint() {
      return getNumPoints();
    }

    @Override
    public double getDoubleValue(final int _att, final int _pt) {
      return getDatas(_att).getValue(_pt);
    }

    @Override
    public Object getValueIdentifier(final int _i) {
      return H2dRubarDonneesBrutes.this.getVariableId(_i);
    }

    /**
     * @param _valIdx l'indice de la valeur
     * @param _idx les indices a modifier
     * @param _val les nouvelles valeurs
     * @param _cmd le receveur de commande
     */
    public void setValue(final int _valIdx, final int[] _idx, final double[] _val, final CtuluCommandContainer _cmd) {
      getDatas(_valIdx).set(_idx, _val, _cmd);
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: H2dRubarDonneesBrutes.java,v 1.32 2007-05-04 13:46:38 deniger Exp $
   */
  public static abstract class OrganizationType extends DodicoEnumType {
    OrganizationType(final String _nom) {
      super(_nom);
    }

    protected abstract void createType(H2dRubarDonneesBrutes _b);

    protected abstract Object getTypeObject(H2dRubarDonneesBrutes _b);

    protected abstract boolean isEmpty(H2dRubarDonneesBrutes _b);

    protected abstract Object removeTypeData(H2dRubarDonneesBrutes _b);

    protected abstract void setTypeData(H2dRubarDonneesBrutes _b, Object _o);

    public DodicoEnumType[] getArray() {
      return new OrganizationType[]{GRILLE, PARALL};
    }

    // public abstract boolean isNuageType();

    public abstract void visit(H2dRubarDonneesBrutesVisitorClient _c, H2dRubarDonneesBrutes _b);
  }

  /**
   * @author Fred Deniger
   * @version $Id: H2dRubarDonneesBrutes.java,v 1.32 2007-05-04 13:46:38 deniger Exp $
   */
  public class ParallelMng extends H2dParallelogrammeManager {
    ParallelMng() {
      super(10);
    }

    ParallelMng(final int _nb) {
      super(_nb);
    }

    @Override
    protected void addLastData(int nbToAdd, final CtuluCommandContainer _cmd) {
      super.addLastData(nbToAdd, _cmd);
    }

    @Override
    protected H2dParallelogrammeDataAbstract createParallelogramme(final H2dParallelogrammeDataInterface _model) {
      final H2dParallelogrammeData r = new Parallel();
      if (_model != null) {
        r.initFromKeepingSameNumberOfValue(_model);
      }
      return r;
    }

    @Override
    protected void fireDataNombreChanged() {
      fireDonneesBrutesDataNombreChanged();
    }

    @Override
    protected void fireSupportAddOrRemoved() {
      fireDonneesBrutesSupportAddOrRemoved();
    }

    @Override
    protected void fireSupportChanged() {
      fireDonneesBrutesDataNombreChanged();
    }

    @Override
    protected void removeLastData(int nbToRemove, final CtuluCommandContainer _cmd) {
      super.removeLastData(nbToRemove, _cmd);
    }

    protected void setDataToDefault(final int _idx, final CtuluCommandContainer _cmd) {
      super.setDataToDefaut(_idx, _cmd);
    }
  }

  /**
   * Identifiant pour la grille.
   */
  public final static OrganizationType GRILLE = new OrganizationType(H2dResource.getS("Grille r�guli�re")) {
    @Override
    protected void createType(final H2dRubarDonneesBrutes _b) {
      if (_b.grid_ == null) {
        _b.grid_ = _b.createGridData(null);
      }
    }

    @Override
    protected Object getTypeObject(final H2dRubarDonneesBrutes _b) {
      return _b.grid_;
    }

    @Override
    protected boolean isEmpty(final H2dRubarDonneesBrutes _b) {
      return (_b == null) || (_b.grid_ == null) || (_b.grid_.getNbPoint() == 0);
    }

    @Override
    protected Object removeTypeData(final H2dRubarDonneesBrutes _b) {
      final Object r = _b.grid_;
      _b.grid_ = null;
      return r;
    }

    @Override
    protected void setTypeData(final H2dRubarDonneesBrutes _b, final Object _o) {
      if (_o instanceof GridData) {
        _b.grid_ = (GridData) _o;
      }
    }

    @Override
    public void visit(final H2dRubarDonneesBrutesVisitorClient _c, final H2dRubarDonneesBrutes _b) {
      _c.visitGrid(_b);
    }
  };
  /**
   * Identifiant pour les parall�logrammes.
   */
  public final static OrganizationType PARALL = new OrganizationType(H2dResource.getS("Parall�logramme")) {
    @Override
    protected void createType(H2dRubarDonneesBrutes _b) {
      if (_b.parall_ == null) {
        _b.parall_ = _b.createParallMng();
      }
    }

    @Override
    protected Object getTypeObject(H2dRubarDonneesBrutes _b) {
      return _b.parall_;
    }

    @Override
    protected boolean isEmpty(H2dRubarDonneesBrutes _b) {
      return (_b == null) || (_b.parall_ == null) || (_b.parall_.getNbParall() == 0);
    }

    @Override
    protected Object removeTypeData(H2dRubarDonneesBrutes _b) {
      final Object r = _b.parall_;
      _b.parall_ = null;
      return r;
    }

    @Override
    protected void setTypeData(H2dRubarDonneesBrutes _b, Object _o) {
      if (_o instanceof H2dParallelogrammeManager) {
        _b.parall_ = (ParallelMng) _o;
      }
    }

    @Override
    public void visit(H2dRubarDonneesBrutesVisitorClient _c, H2dRubarDonneesBrutes _b) {
      _c.visitParall(_b);
    }
  };
  /**
   * La liste immutable des organisations possibles.
   */
  public final static CtuluPermanentList TYPE_DONNEES_BRUTES = new CtuluPermanentList(CtuluLibArray.sort(new Object[]{GRILLE, PARALL}));
  protected GridData grid_;
  protected Object identifier_;
  protected H2dRubarDonneesBrutesListener listener_;
  protected int nbVariable_;
  protected NuagePoint nuage_;
  protected ParallelMng parall_;
  protected OrganizationType typeOrganisation_;
  String firstLine_;
  H2dRubarNumberFormatter fmt_;

  private H2dRubarDonneesBrutes() {

  }

  protected H2dRubarDonneesBrutes(final int _nbVara, final H2dRubarNumberFormatter _fmt) {
    nbVariable_ = _nbVara;
    fmt_ = _fmt;
  }

  private void setDefined() {
    typeOrganisation_.createType(this);
  }

  protected GridData createGridData(final H2dRegularGridDataInterface _grid) {
    final GridData r = new GridData();
    if (_grid != null) {
      r.initFrom(_grid);
    }
    return r;
  }

  protected NuagePoint createNuagePoint(final CollectionPointDataDoubleInterface _init) {
    final NuagePoint r = new NuagePoint(_init == null ? 30 : _init.getNbPoint());
    if (_init != null) {
      r.initFromAndKeepSameNbValues(_init, true);
    }
    return r;
  }

  protected ParallelMng createParallMng() {
    return createParallMng(10);
  }

  protected ParallelMng createParallMng(final int _nb) {
    return new ParallelMng(_nb);
  }

  protected void fireDonneesBrutesDataChanged() {
    if (listener_ != null) {
      listener_.donneesBrutesDataChanged(this);
    }
  }

  protected void fireDonneesBrutesDataNombreChanged() {
    if (listener_ != null) {
      listener_.donneesBrutesDataNombreChanged(this);
    }
  }

  protected void fireDonneesBrutesNuageDataChanged() {
    if (listener_ != null) {
      listener_.donneesBrutesNuageDataChanged(this);
    }
  }

  protected void fireDonneesBrutesNuageDataNombreChanged() {
    if (listener_ != null) {
      listener_.donneesBrutesNuageDataNombreChanged(this);
    }
  }

  protected void fireDonneesBrutesNuageSupportAddOrRemoved() {
    if (listener_ != null) {
      listener_.donneesBrutesNuageSupportAddedOrRemoved(this);
    }
  }

  protected void fireDonneesBrutesNuageSupportChanged() {
    if (listener_ != null) {
      listener_.donneesBrutesNuageSupportChanged(this);
    }
  }

  protected void fireDonneesBrutesSupportAddOrRemoved() {
    if (listener_ != null) {
      listener_.donneesBrutesSupportAddedOrRemoved(this);
    }
  }

  protected void fireDonneesBrutesSupportChanged() {
    if (listener_ != null) {
      listener_.donneesBrutesSupportChanged(this);
    }
  }

  protected void fireDonneesBrutesTypeChanged() {
    if (listener_ != null) {
      listener_.donneesBrutesTypeChanged(this);
    }
  }

  protected void setVariableToDefault(final int _idx, final CtuluCommandContainer _cmd) {
    if (nuage_ != null) {
      nuage_.setDataToDefault(_idx, _cmd);
    } else if (grid_ != null) {
      grid_.setDataToDefault(_idx, _cmd);
    } else if (parall_ != null) {
      parall_.setDataToDefault(_idx, _cmd);
    } else {
      new Throwable().printStackTrace();
    }
  }

  /**
   * @return les indices des variables valides.
   */
  public int[] getAvailableVariableId() {
    final int n = getNbVariable();
    final TIntArrayList r = new TIntArrayList(n);
    for (int i = 0; i < n; i++) {
      if (isVariableActive(i)) {
        r.add(i);
      }
    }
    return r.toNativeArray();
  }

  public Object getFileModel() {
    if (getTypeOrganisation() == null) {
      return null;
    }
    if ((parall_ != null) || (grid_ != null)) {
      return new VF2MFileModel();
    }
    return null;
  }

  public Object getFileNuageModel() {
    if (nuage_ != null) {
      return new NuageFileModel();
    }
    return null;
  }

  /**
   * A enlever.
   *
   * @return une ligne est parfois mise au debut du fichier ini
   */
  public String getFirstLine() {
    return firstLine_;
  }

  /**
   * @return le formatteur de nombre
   */
  public H2dRubarNumberFormatter getFmt() {
    return fmt_;
  }

  public final GridData getGrid() {
    return grid_;
  }

  public abstract String getID();

  public double getIndeterminedValue(final int _i) {
    return 0;
  }

  public final H2dRubarDonneesBrutesListener getListener() {
    return listener_;
  }

  public final int getNbVariable() {
    return nbVariable_;
  }

  public final NuagePoint getNuage() {
    if (nuage_ == null) {
      nuage_ = createNuagePoint(null);
    }
    return nuage_;
  }

  public final H2dParallelogrammeManager getParall() {
    return parall_;
  }

  public abstract String getTitle();

  public final OrganizationType getTypeOrganisation() {
    return typeOrganisation_;
  }

  public abstract Object getVariableId(int _idx);

  /**
   * @param _inter l'interface servant a initialiser les donn�es
   * @return true si donn�es initialisees
   */
  public boolean initDonneesBrutes(final CollectionPointDataDoubleInterface _inter) {
    if (_inter == null) {
      return false;
    }
    nuage_ = createNuagePoint(_inter);
    return true;
  }

  /**
   * @param _inter l'interface servant a initialiser les donn�es
   * @return true si donn�es initialisees
   */
  public boolean initDonneesBrutes(final H2dParallelogrammeManagerInterface _inter) {
    if (_inter == null) {
      return false;
    }
    grid_ = null;
    // le nombre de variables attendues.
    typeOrganisation_ = PARALL;
    final int nbPar = _inter.getNbParall();
    if (nbPar == 0) {
      return true;
    }
    parall_ = createParallMng(nbPar);
    parall_.initFrom(_inter);
    fireDonneesBrutesTypeChanged();
    return true;
  }

  /**
   * @param _inter l'interface servant a initialiser les donn�es
   * @return true si donn�es initialisees
   */
  public boolean initDonneesBrutes(final H2dRegularGridDataInterface _inter) {
    if (_inter == null) {
      return false;
    }
    parall_ = null;
    typeOrganisation_ = GRILLE;
    grid_ = createGridData(_inter);
    fireDonneesBrutesTypeChanged();
    return true;
  }

  public boolean isDefined() {
    return (typeOrganisation_ != null) && (typeOrganisation_.getTypeObject(this) != null);
  }

  /**
   * @return true si vide
   */
  public boolean isOrganisationEmpty() {
    return (typeOrganisation_ == null) || (typeOrganisation_.isEmpty(this));
  }

  public boolean isNuageEmpty() {
    return (nuage_ == null) || (nuage_.getNumPoints() == 0);
  }

  /**
   * @param _idx l'indice � prendre a compte
   * @return true si la donnee est activee
   */
  public boolean isVariableActive(final int _idx) {
    return true;
  }

  /**
   * A enlever.
   *
   * @param _s la nouvelle valeur de la premiere ligne
   */
  public void setFirstLine(final String _s) {
    firstLine_ = _s;
  }

  public final void setListener(final H2dRubarDonneesBrutesListener _listener) {
    listener_ = _listener;
  }

  /**
   * @param _newP le nouveau type de projet
   * @param _cmd le receveur de commande
   * @return true si modification induite.
   */
  protected boolean setProjectTypeChanged(final H2dRubarProjetType _newP, final CtuluCommandComposite _cmd) {
    return false;
  }

  public void setNbConcentrationBlocks(final int newNbConcentrationBlocks, final CtuluCommandComposite _cmd) {

  }

  /**
   * @param _typeOrganisation le nouveau type
   */
  public final void setTypeOrganisation(final OrganizationType _typeOrganisation) {
    setTypeOrganisation(_typeOrganisation, null);
  }

  /**
   * Modifie le type des donnees.
   *
   * @param _typeOrganisation le nouveau type
   * @param _cmd le receveur de commande : peut etre null
   */
  public final void setTypeOrganisation(final OrganizationType _typeOrganisation, final CtuluCommandContainer _cmd) {
    if (typeOrganisation_ != _typeOrganisation) {
      final OrganizationType old = typeOrganisation_;
      Object oldData = null;
      if (old != null) {
        oldData = old.removeTypeData(this);
      }
      typeOrganisation_ = _typeOrganisation;
      if (_cmd != null) {
        _cmd.addCmd(new TypeOrganisationCommand(old, oldData));
      }
      if (typeOrganisation_ != null) {
        setDefined();
      }
      fireDonneesBrutesTypeChanged();
    }
  }
}
