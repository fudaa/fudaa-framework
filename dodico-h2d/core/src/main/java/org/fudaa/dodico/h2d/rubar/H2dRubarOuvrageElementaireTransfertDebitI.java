/*
 * @creation 3 janv. 2005
 * @modification $Date: 2006-10-24 12:48:37 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

import java.util.LinkedHashMap;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarOuvrageElementaireTransfertDebitI.java,v 1.4 2006-10-24 12:48:37 deniger Exp $
 */
public interface H2dRubarOuvrageElementaireTransfertDebitI extends H2dRubarOuvrageElementaireInterface {
  /**
   * @return l'evolution gere pour le transfert
   */
  EvolutionReguliereAbstract getEvol(H2dVariableType variable);

  LinkedHashMap<H2dVariableType, ? extends EvolutionReguliereAbstract> getEvols();
}
