/*
 *  @creation     5 janv. 2005
 *  @modification $Date: 2006-04-19 13:19:38 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarOuvrageElementaireAbstract.java,v 1.4 2006-04-19 13:19:38 deniger Exp $
 */
public abstract class H2dRubarOuvrageElementaireAbstract implements H2dRubarOuvrageElementaireInterface {

  @Override
  public String toString() {
    return getType().getName();
  }
  protected H2dRubarOuvrage ouvrageParent_;

  public void setOuvrageParent(H2dRubarOuvrage ouvrageParent_) {
    this.ouvrageParent_ = ouvrageParent_;
  }
  
  

  public H2dRubarOuvrage getOuvrageParent() {
    return ouvrageParent_;
  }

  @Override
  public boolean containsEvolution(EvolutionReguliereInterface tarage) {
    return false;
  }

  @Override
  public void evolutionChanged(EvolutionReguliereInterface _e) {
  }
  
  @Override
  public void setProjetType(final H2dRubarProjetType projet, final int nbTransportBlock, final CtuluCommandComposite cmd){
    
  }
}
