/*
 * @creation 25 juin 2003
 * @modification $Date: 2006-10-24 12:48:36 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import gnu.trove.TIntDoubleHashMap;
import gnu.trove.TIntObjectHashMap;
import gnu.trove.TObjectProcedure;
import java.util.Set;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

/**
 * Pour connaitre une valeur: dans un premier temps demande le type.
 * 
 * @author deniger
 * @version $Id: H2dNodalPropertyMixte.java,v 1.14 2006-10-24 12:48:36 deniger Exp $
 */
public class H2dNodalPropertyMixte {

  protected String nom_;
  protected TIntObjectHashMap indiceTransitoireEvolution_;
  protected TIntDoubleHashMap indicePermanentValeur_;
  protected H2DNodalPropertyMixteChangeListener listener_;

  protected TIntDoubleHashMap getIndicePermanentValeur() {
    return indicePermanentValeur_;
  }

  /**
   * @return une propriete ou toutes les valeurs sont libres
   */
  public static H2dNodalPropertyMixte createFreeProp() {
    return new H2dNodalPropertyMixte(null) {

      /*
       * public int getPermanentSpecifieNb(){ return 0; } public double getPermanentValueForIndex(int _i){ return 0; }
       */

      @Override
      public H2dBcType getTypeForEltIdx(final int _i) {
        return H2dBcType.LIBRE;
      }

      @Override
      public EvolutionReguliereAbstract getTransitoireEvol(final int _eltIdx) {
        return null;
      };

      @Override
      public int getValeurTransitoireNb() {
        return 0;
      }
    };
  }

  /**
   * @param _nbElt le nombre d'element a prendre en compte
   * @return une propriete ou toutes les valeurs sont fixes et nulles.
   */
  public static H2dNodalPropertyMixte createPermanentNull(final int _nbElt) {
    return new H2dNodalPropertyMixte(null) {

      @Override
      public H2dBcType getTypeForEltIdx(final int _i) {
        return H2dBcType.PERMANENT;
      }

      @Override
      public EvolutionReguliereAbstract getTransitoireEvol(final int _eltIdx) {
        return null;
      };

      @Override
      public int getValeurTransitoireNb() {
        return 0;
      }
    };
  }

  public H2dNodalPropertyMixte(final String _nom) {
    nom_ = _nom;
  }

  /**
   * @param _indiceFixeValeur les indices des valeurs fixes
   * @param _indiceTransitoireEvolution les indices des points transitoires
   */
  public H2dNodalPropertyMixte(final TIntDoubleHashMap _indiceFixeValeur,
      final TIntObjectHashMap _indiceTransitoireEvolution) {
    this(null, _indiceFixeValeur, _indiceTransitoireEvolution);
  }

  /**
   * @param _nom le nom de la prop
   * @param _indiceFixeValeur les indices des valeurs fixes
   * @param _indiceTransitoireEvolution les indices des points transitoires
   */
  public H2dNodalPropertyMixte(final String _nom, final TIntDoubleHashMap _indiceFixeValeur,
      final TIntObjectHashMap _indiceTransitoireEvolution) {
    nom_ = _nom;
    indicePermanentValeur_ = _indiceFixeValeur;
    indiceTransitoireEvolution_ = _indiceTransitoireEvolution;
  }

  public String getNom() {
    return nom_;
  }

  /**
   * @return le nombre de point specifie par une courbes
   */
  public int getValeurTransitoireNb() {
    return indiceTransitoireEvolution_ == null ? 0 : indiceTransitoireEvolution_.size();
  }

  protected final void initDataFrom(final H2dNodalPropertyMixte _m) {
    if (_m == null) {
      return;
    }
    this.indicePermanentValeur_ = _m.indicePermanentValeur_;
    // this.nom_ = _m.nom_;
    this.indiceTransitoireEvolution_ = _m.indiceTransitoireEvolution_;
  }

  /**
   * @param _r la collection a remplir avec les evolutions utilisees
   */
  public void fillWithUsedEvolution(final Set _r) {
    if (indiceTransitoireEvolution_ == null) {
      return;
    }
    final TObjectProcedure p = new TObjectProcedure() {

      @Override
      public boolean execute(Object _object) {
        _r.add(_object);
        return true;
      }
    };
    indiceTransitoireEvolution_.forEachValue(p);
  }

  /**
   * Renvoie le type de variable pour l'element d'indice i.
   * 
   * @param _i l'indice demande
   * @return permanent si non transitoire et non libre.
   */
  public H2dBcType getTypeForEltIdx(final int _i) {
    if ((indiceTransitoireEvolution_ != null) && (indiceTransitoireEvolution_.contains(_i))) {
      return H2dBcType.TRANSITOIRE;
    } else if ((indicePermanentValeur_ != null) && (indicePermanentValeur_.contains(_i))) {
      return H2dBcType.PERMANENT;
    }
    return H2dBcType.LIBRE;
  }

  /**
   * @param _eltIdx l'indice demande
   * @return null si non adapte
   */
  public EvolutionReguliereAbstract getTransitoireEvol(final int _eltIdx) {
    if (indiceTransitoireEvolution_ != null) {
      return (EvolutionReguliereAbstract) indiceTransitoireEvolution_.get(_eltIdx);
    }
    return null;
  }

  /**
   * @return true si des valeurs libres peuvent etre specifiee
   */
  public boolean isFreeAllowed() {
    return true;
  }

  /**
   * @return le nombre de valeur fixe
   */
  public int getNbPermanentPt() {
    return (indicePermanentValeur_ == null ? 0 : indicePermanentValeur_.size());
  }

  /**
   * @param _i l'indice du point demande
   * @return 0 si non adapte
   */
  public double getPermanentValueFor(final int _i) {
    return (indicePermanentValeur_ == null ? 0d : indicePermanentValeur_.get(_i));
  }

  protected void fireEvent() {
    if (listener_ != null) {
      listener_.nodalPropertyMixteChanged(this);
    }
  }

  /**
   * @return le listener de cette propriete
   */
  public H2DNodalPropertyMixteChangeListener getListener() {
    return listener_;
  }

  /**
   * @param _listener le nouveau listener de cette prop
   */
  public void setListener(final H2DNodalPropertyMixteChangeListener _listener) {
    listener_ = _listener;
  }

}