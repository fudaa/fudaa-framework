/*
 * @creation 18 oct. 2004
 *
 * @modification $Date: 2007-04-30 14:21:34 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import com.memoire.bu.BuValueValidator;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.*;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGridDefaultAbstract;
import org.fudaa.dodico.h2d.H2DLib;
import org.fudaa.dodico.h2d.H2dSIListener;
import org.fudaa.dodico.h2d.H2dSiSourceInterface;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarSI.java,v 1.24 2007-04-30 14:21:34 deniger Exp $
 */
public class H2dRubarSI extends H2dRubarElementPropertyAbstract implements H2DRubarSolutionsInitialesInterface, H2dSiSourceInterface {
  private class RealVitesseModel extends CtuluCollectionDoubleEditAbstract {
    private final H2dRubarElementPropertyAbstract.ElementDoubleArray debit_;
    private final CtuluCollectionDouble hauteur_ = getRealHauteurModel();

    RealVitesseModel(final H2dVariableType _debitVar) {
      debit_ = (H2dRubarElementPropertyAbstract.ElementDoubleArray) getDoubleInterface(_debitVar);
    }

    @Override
    protected void internalSetValue(final int _i, final double _newV) {
      debit_.internalSet(_i, _newV * hauteur_.getValue(_i));
    }

    @Override
    public int getSize() {
      return hauteur_.getSize();
    }

    @Override
    public double getValue(final int _i) {
      final double h = hauteur_.getValue(_i);
      // pour eviter les division par zero
      if (h < hIsZero_) {
        return 0;
      }
      return debit_.getValue(_i) / hauteur_.getValue(_i);
    }
  }

  static class RealCoteModel extends CtuluCollectionDoubleEditAbstract {
    private final CtuluCollectionDouble bathy_;
    private final H2dRubarElementPropertyAbstract.ElementDoubleArray hauteur_;

    protected RealCoteModel(final H2dRubarSI _si) {
      bathy_ = _si.getBathyModel();
      hauteur_ = (H2dRubarElementPropertyAbstract.ElementDoubleArray) _si.getDoubleInterface(H2dVariableType.HAUTEUR_EAU);
    }

    @Override
    protected void internalSetValue(final int _i, final double _newV) {
      hauteur_.internalSet(_i, _newV - bathy_.getValue(_i));
    }

    @Override
    public int getSize() {
      return bathy_.getSize();
    }

    @Override
    public double getValue(final int _i) {
      return bathy_.getValue(_i) + hauteur_.getValue(_i);
    }
  }

  static class RealHauteurModel extends CtuluCollectionDoubleEditAbstract {
    private final CtuluCollectionDouble bathy_;
    private final H2dRubarElementPropertyAbstract.ElementDoubleArray cote_;

    protected RealHauteurModel(final H2dRubarSI _si) {
      bathy_ = _si.getBathyModel();
      cote_ = (H2dRubarElementPropertyAbstract.ElementDoubleArray) _si.getDoubleInterface(H2dVariableType.COTE_EAU);
    }

    @Override
    protected void internalSetValue(final int _i, final double _newV) {
      cote_.internalSet(_i, _newV + bathy_.getValue(_i));
      clearCache();
    }

    @Override
    public int getSize() {
      return bathy_.getSize();
    }

    @Override
    public double getValue(final int _i) {
      return cote_.getValue(_i) - bathy_.getValue(_i);
    }
  }

  /**
   * Valeur indeterminee pour h.
   */
  public final static double H_INDETERMINED_VALUE = 999.999;
  /**
   * Valeur indeterminee pour les autres.
   */
  public final static double INDETERMINED_VALUE = 9999.999;
  double hIsZero_ = 1e-4;
  boolean isCote_;
  boolean isVitesse_;
  boolean isConcentrationParHauteur_;
  Set listener_;
  // pour tester lors de la lecture
  double maxHeValTest_ = 999.99;
  RealCoteModel realCote_;
  RealHauteurModel realHauteur_;
  double t0_;
  final DicoParams rubarParams_;

  /**
   * @param _grid le maillage support
   * @param _transport true si transport
   */
  public H2dRubarSI(final DicoParams _params, final EfGridInterface _grid, final boolean _transport, final int nbTransportBlocks) {
    super(_grid, null);
    rubarParams_ = _params;
    vValues_ = new TreeMap();
    setDefaultSI(_grid, _transport, nbTransportBlocks);
  }

  /**
   * @param _grid le maillage associ�
   * @param _i les valeurs initiales
   * @param _trans true si transport
   */
  public H2dRubarSI(final DicoParams _params, final EfGridInterface _grid, final H2DRubarSolutionsInitialesInterface _i, final boolean _trans, final int nbTransportBlocks) {
    super(_grid, null);
    rubarParams_ = _params;
    vValues_ = new TreeMap();
    setData(_i, _grid, _trans, nbTransportBlocks);
  }

  private CtuluArrayDoubleUnique createUniqueForCote(final int _nbElt) {
    return new CtuluArrayDoubleUnique(9999.999, _nbElt);
  }

  private CtuluArrayDoubleUnique createUniqueForHauteur(final int _nbElt) {
    return new CtuluArrayDoubleUnique(999.9999, _nbElt);
  }

  private CtuluCollectionDoubleEdit getRealCoteModel() {
    if (isCote_) {
      return getDoubleInterface(H2dVariableType.COTE_EAU);
    }
    if (realCote_ == null) {
      realCote_ = new RealCoteModel(this);
    }
    return realCote_;
  }

  private CtuluCollectionDoubleEdit getRealVitesseU() {
    if (isVitesse_) {
      return getDoubleInterface(H2dVariableType.VITESSE_U);
    }
    return new RealVitesseModel(H2dVariableType.DEBIT_X);
  }

  private CtuluCollectionDoubleEdit getRealVitesseV() {
    // si vitesse on utilise le modele stocke, sinon on cree un nouveu
    if (isVitesse_) {
      return getDoubleInterface(H2dVariableType.VITESSE_V);
    }
    return new RealVitesseModel(H2dVariableType.DEBIT_Y);
  }

  protected void fireBeginTimeChanged() {
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2dSIListener) it.next()).siChanged(H2dVariableType.TEMPS);
      }
    }
  }

  protected void fireCoteHauteurChanged() {

    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2dSIListener) it.next()).siChanged(H2dVariableType.HAUTEUR_EAU);
      }
    }
  }

  protected void fireTransportChanged() {
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2dSIListener) it.next()).siRemoved(null);
      }
    }
  }

  protected void fireVitesseParameterChanged() {
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2dSIListener) it.next()).siChanged(H2dVariableType.VITESSE);
      }
    }
  }

  /**
   * @return list of transport variables.
   */
  protected final List<H2dVariableTransType> getTransportVariables(int nbTransportBlock) {
    return H2dRubarBcMng.getTransportVariables(nbTransportBlock);
  }

  private H2dVariableTransType getConcentrationVar() {
    return H2dVariableTransType.CONCENTRATION;
  }

  protected CtuluCollectionDoubleEdit getRealHauteurModel() {
    if (isCote_) {
      if (realHauteur_ == null) {
        realHauteur_ = new RealHauteurModel(this);
      }
      return realHauteur_;
    }
    return getDoubleInterface(H2dVariableType.HAUTEUR_EAU);
  }

  /**
   * @return vitesse u ou debit x
   */
  protected final H2dVariableType getVitesseU() {
    return isVitesse_ ? H2dVariableType.VITESSE_U : H2dVariableType.DEBIT_X;
  }

  /**
   * @return vitesse v ou debit y
   */
  protected final H2dVariableType getVitesseV() {
    return isVitesse_ ? H2dVariableType.VITESSE_V : H2dVariableType.DEBIT_Y;
  }

  @Override
  protected void setMap(final Map _v) {
  }

  protected void valuesChanged() {
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2dSIListener) it.next()).siChanged(null);
      }
    }
  }

  @Override
  protected void valuesChanged(final H2dVariableType _v) {
    if (_v == H2dVariableType.HAUTEUR_EAU && realCote_ != null) {
      realCote_.clearCache();
    }
    if (_v == H2dVariableType.COTE_EAU && realHauteur_ != null) {
      realHauteur_.clearCache();
    }
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        H2dSIListener siListener = (H2dSIListener) it.next();
        siListener.siChanged(_v);
        if (_v == H2dVariableType.COTE_EAU) {
          siListener.siChanged(H2dVariableType.HAUTEUR_EAU);
        } else if (_v == H2dVariableType.HAUTEUR_EAU) {
          siListener.siChanged(H2dVariableType.COTE_EAU);
        }
      }
    }
  }

  @Override
  public void addListener(final H2dSIListener _listener) {
    if (_listener == null) {
      return;
    }
    if (listener_ == null) {
      listener_ = new HashSet();
    }
    listener_.add(_listener);
  }

  @Override
  public CtuluCollectionDouble getBathyModel() {
    return EfGridDefaultAbstract.getElementBathyModel(grid_);
  }

  boolean isCoteViewed_;

  public boolean isCoteViewed() {
    return isCoteViewed_;
  }

  @Override
  public CtuluCollectionDouble getViewedModel(final H2dVariableType _t) {
    if (_t == H2dVariableType.BATHYMETRIE) {
      return getBathyModel();
    }
    return getDoubleInterface(_t);
  }

  /**
   * Surcharger pour cr�er d'eventuels adaptateur pour simuler des variables interessantes.
   */
  public CtuluCollectionDoubleEdit getDoubleInterface(final H2dVariableType _t) {
    if (_t == H2dVariableType.HAUTEUR_EAU && isCote_) {
      return getRealHauteurModel();
    } else if (_t == H2dVariableType.COTE_EAU && !isCote_) {
      return getRealCoteModel();
    }
    final CtuluCollectionDoubleEdit r = (CtuluCollectionDoubleEdit) vValues_.get(_t);
    if (r == null) {
      if (_t == H2dVariableType.COTE_EAU) {
        return getRealCoteModel();
      }
      if (_t == H2dVariableType.HAUTEUR_EAU) {
        return getRealHauteurModel();
      }
      if (_t == H2dVariableType.VITESSE_U) {
        return getRealVitesseU();
      }
      if (_t == H2dVariableType.VITESSE_V) {
        return getRealVitesseV();
      }
    }
    return r;
  }

  @Override
  public EfGridInterface getMaillage() {
    return grid_;
  }

  @Override
  public CtuluCollectionDoubleEdit getModifiableModel(final H2dVariableType _var) {
    return getDoubleInterface(_var);
  }

  @Override
  public int getNbValues() {
    return super.vValues_.size();
  }

  @Override
  public final double getT0() {
    return t0_;
  }

  @Override
  public double[] getU() {
    return getRealVitesseU().getValues();
  }

  /**
   * On s'assure que la cote d'eau et la hauteur soient ajout�es.
   */
  @Override
  public Collection getUsableVariables() {
    final Set res = new HashSet(super.getUsableVariables());
    res.add(H2dVariableType.HAUTEUR_EAU);
    res.add(H2dVariableType.COTE_EAU);
    res.add(H2dVariableType.BATHYMETRIE);
    return res;
  }

  @Override
  public double[] getV() {
    return getRealVitesseV().getValues();
  }

  @Override
  public BuValueValidator getValidator(final H2dVariableType _t) {
    return BuValueValidator.DOUBLE;
  }

  @Override
  public CtuluNumberFormatI getFormater(final H2dVariableType _t) {
    return null;
  }

  @Override
  public CtuluCollectionDoubleEdit getValues(final int _i) {
    if (_i == 0) {
      return getDoubleInterface(H2dVariableType.HAUTEUR_EAU);
    }
    if (_i == 1) {
      return getDoubleInterface(getVitesseU());
    }
    if (_i == 2) {
      return getDoubleInterface(getVitesseV());
    }
    if (_i == 3) {
      return getDoubleInterface(H2dVariableType.COTE_EAU);
    }
    H2dVariableTransType type = transportVariables.get(_i - 4);
    return getDoubleInterface(type);
  }

  @Override
  public H2dVariableProviderInterface getVariableDataProvider() {
    return this;
  }

  @Override
  public List getVariables() {
    return var_;
  }

  @Override
  public double[] getZe() {
    return getRealCoteModel().getValues();
  }

  @Override
  public boolean isSolutionInitialesActivated() {
    return true;
  }

  @Override
  public boolean isVitesse() {
    return isVitesse_;
  }

  @Override
  public void removeListener(final H2dSIListener _listener) {
    if (listener_ != null) {
      listener_.remove(_listener);
    }
  }

  @Override
  public void set(final double[] _ze, final double[] _u, final double[] _v, final CtuluCommandContainer _cmd) {
    final CtuluCommandComposite cmp = _cmd == null ? null : new CtuluCommandComposite();
    getRealCoteModel().setAll(_ze, cmp);
    getRealVitesseU().setAll(_u, cmp);
    getRealVitesseV().setAll(_v, cmp);
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
  }

  /**
   * @param _d le temps de d�but
   * @param _cmd le receveur de commande
   */
  public void setBeginTime(final double _d, final CtuluCommandContainer _cmd) {
    // si modification, on change,on envoie un evt et on cr�e une commande
    if (_d != t0_) {
      final double old = t0_;
      t0_ = _d;
      fireBeginTimeChanged();
      if (_cmd != null) {
        _cmd.addCmd(new CtuluCommand() {
          @Override
          public void redo() {
            setBeginTime(_d, null);
          }

          @Override
          public void undo() {
            setBeginTime(old, null);
          }
        });
      }
    }
  }

  private List<H2dVariableTransType> getUsedTransportVariables() {
    List<H2dVariableTransType> transVariables = new ArrayList<>();
    for (H2dVariableType variableType : vValues_.keySet()) {
      if (variableType instanceof H2dVariableTransType) {
        transVariables.add((H2dVariableTransType) variableType);
      }
    }
    return transVariables;
  }

  private List<H2dVariableType> getUsedNonTransportVariables() {
    List<H2dVariableType> transVariables = new ArrayList<>();
    for (Object variableType : var_) {
      if (!(variableType instanceof H2dVariableTransType)) {
        transVariables.add((H2dVariableType) variableType);
      }
    }
    return transVariables;
  }

  /**
   * Used to have the list of variables in correct order
   */
  List<H2dVariableTransType> transportVariables;

  public final void setData(final H2DRubarSolutionsInitialesInterface _i, final EfGridInterface _grid, final boolean _trans, final int nbTransportBlock) {
    // 11-12-2006: les variables hauteur d'eau, cote d'eau peuvent �tre d�finies en partie sur des noeuds....
    // l'analyse doit �tre am�lior�e ....
    if (isVitesse_ != _i.isVitesse()) {
      vValues_.remove(getVitesseU());
      vValues_.remove(getVitesseV());
    }
    if (isConcentrationParHauteur_ != _i.isConcentrationParHauteur()) {
      for (H2dVariableTransType usedTransportVariable : getUsedTransportVariables()) {
        vValues_.remove(usedTransportVariable);
      }
    }
    isVitesse_ = _i.isVitesse();
    isConcentrationParHauteur_ = _i.isConcentrationParHauteur();
    grid_ = _grid;
    final int nbElt = grid_.getEltNb();
    t0_ = _i.getT0();
    final Map temp = new HashMap(vValues_);
    // la hauteur d'eau
    final CtuluCollectionDoubleEdit he = _i.getValues(0);

    // par d�faut, on suppose que la hauteur est d�finie sur tous les points
    isCote_ = he.getValue(0) > maxHeValTest_;
    // on parcourt toutes les donn�es de la hauteur d'eau pour d�tecter si le mode change
    // la variable isAllSame indique que les conditions initiales sont d�finies de la meme mani�re pour tous les noeuds.
    boolean isAllSame = true;
    for (int i = 0; i < nbElt && isAllSame; i++) {
      boolean isHNonDefinie = he.getValue(i) >= maxHeValTest_;
      // si les donn�es sont d�finies sur la cote d'eau: toutes les donn�es sont identiques si h est ind�finin
      // sinon c'est l'inverse
      isAllSame = isCote_ ? isHNonDefinie : !isHNonDefinie;
    }
    // la cote d'eau qui sera modifiee si les valeurs sont h�t�rog�nes.
    CtuluCollectionDoubleEdit cote = _i.getValues(3);
    // les donn�es sont h�t�rog�nes: on donne la priorit� � la cote d'eau
    // et on cr�e un tableau de cote d'eau correspond
    if (!isAllSame) {
      isCote_ = true;
      final double[] coteValues = new double[nbElt];
      final CtuluCollectionDouble bathy = EfGridDefaultAbstract.getElementBathyModel(grid_);
      for (int i = 0; i < nbElt; i++) {
        final boolean isHNonDefinie = he.getValue(i) >= maxHeValTest_;
        // point d�finissant une cote d'eau
        if (isHNonDefinie) {
          coteValues[i] = cote.getValue(i);
          // point d�finissant une hauteur d'eau
        } else {
          coteValues[i] = bathy.getValue(i) + he.getValue(i);
        }
      }
      cote = new CtuluArrayDouble(coteValues);
    }

    final List var = new ArrayList();
    // definition avec la cote

    if (isCote_) {
      vValues_.put(H2dVariableType.HAUTEUR_EAU, createUniqueForHauteur(nbElt));
      vValues_.put(H2dVariableType.COTE_EAU, createDoubleModel(H2dVariableType.COTE_EAU, cote, nbElt));
      var.add(H2dVariableType.COTE_EAU);
    }
    // definition avec la hauteur
    else {
      vValues_.put(H2dVariableType.COTE_EAU, createUniqueForCote(nbElt));
      vValues_.put(H2dVariableType.HAUTEUR_EAU, createDoubleModel(H2dVariableType.HAUTEUR_EAU, he, nbElt));
      var.add(H2dVariableType.HAUTEUR_EAU);
    }
    // DEBIT X
    H2dVariableType t = getVitesseU();
    CtuluArrayDouble old = (CtuluArrayDouble) temp.get(t);
    if (old == null) {
      vValues_.put(t, createDoubleModel(t, _i.getValues(1), nbElt));
    } else {
      old.initWithDouble(_i.getValues(1), false);
      vValues_.put(t, old);
    }
    var.add(t);
    // DEBIT Y
    t = getVitesseV();
    var.add(t);
    old = (CtuluArrayDouble) temp.get(t);
    if (old == null) {
      vValues_.put(t, createDoubleModel(t, _i.getValues(2), nbElt));
    } else {
      old.initWithDouble(_i.getValues(2), false);
      vValues_.put(t, old);
    }
    if (_trans) {
      transportVariables = getTransportVariables(nbTransportBlock);
      int idxTransport = 0;
      for (H2dVariableType tranVar : transportVariables) {
        var.add(tranVar);
        old = (CtuluArrayDouble) temp.get(tranVar);
        CtuluCollectionDoubleEdit array = _i.getValues(4 + idxTransport);
        idxTransport++;
        if (array == null) {
          array = new CtuluArrayDouble(nbElt);
        }
        if (old == null) {
          vValues_.put(tranVar, createDoubleModel(tranVar, array, nbElt));
        } else {
          old.initWithDouble(array, false);
          vValues_.put(tranVar, old);
        }
      }
    }
    var_ = Collections.unmodifiableList(var);
    isCoteViewed_ = isCote_;
  }

  @Override
  public int getVariablesBlockLength() {
    return 3;
  }

  public final void setDefaultSI(final EfGridInterface _grid, final boolean _transport, final int nbTransportBlocks) {
    grid_ = _grid;
    final int nbElt = grid_.getEltNb();
    vValues_.clear();
    final List var = new ArrayList();
    if (isCote_) {
      vValues_.put(H2dVariableType.COTE_EAU, createDoubleModel(H2dVariableType.COTE_EAU, nbElt));
      vValues_.put(H2dVariableType.HAUTEUR_EAU, createUniqueForHauteur(nbElt));
      var.add(H2dVariableType.COTE_EAU);
    } else {
      vValues_.put(H2dVariableType.HAUTEUR_EAU, createDoubleModel(H2dVariableType.HAUTEUR_EAU, nbElt));
      vValues_.put(H2dVariableType.COTE_EAU, createUniqueForCote(nbElt));
      var.add(H2dVariableType.HAUTEUR_EAU);
    }
    H2dVariableType t = getVitesseU();
    vValues_.put(t, createDoubleModel(t, nbElt));
    var.add(t);
    t = getVitesseV();
    var.add(t);
    vValues_.put(t, createDoubleModel(t, nbElt));
    if (_transport) {
      transportVariables = getTransportVariables(nbTransportBlocks);
      for (H2dVariableType h2dVariableType : transportVariables) {
        var.add(h2dVariableType);
        vValues_.put(h2dVariableType, createDoubleModel(h2dVariableType, nbElt));
      }
    }
    var_ = Collections.unmodifiableList(var);
  }

  public void setCoteViewed(final boolean _b) {
    if (_b != isCoteViewed_) {
      isCoteViewed_ = _b;
      final List varToModify = new ArrayList(var_);
      varToModify.remove(isCoteViewed_ ? H2dVariableType.HAUTEUR_EAU : H2dVariableType.COTE_EAU);
      varToModify.add(0, isCoteViewed_ ? H2dVariableType.COTE_EAU : H2dVariableType.HAUTEUR_EAU);
      var_ = Collections.unmodifiableList(varToModify);
      fireCoteHauteurChanged();
    }
  }

  @Override
  public void setHToZero(final int[] _idx, final CtuluCommandContainer _cmd) {
    H2DLib.setHToZero(this, _idx, _cmd);
  }

  private Map<H2dVariableType, CtuluCollectionDoubleEdit> updateVariableContent(List<H2dVariableType> newVariableTypes, Map<H2dVariableType, CtuluCollectionDoubleEdit> toAdd) {
    final List<H2dVariableType> oldVariableTypes = new ArrayList<>(var_);
    Map<H2dVariableType, CtuluCollectionDoubleEdit> removed = new HashMap<>();

    //we remove
    for (H2dVariableType oldVariableType : oldVariableTypes) {
      if (!newVariableTypes.contains(oldVariableType)) {
        removed.put(oldVariableType, vValues_.remove(oldVariableType));
      }
    }
    //we add
    for (H2dVariableType newVariableType : newVariableTypes) {
      if (!vValues_.containsKey(newVariableType)) {
        CtuluCollectionDoubleEdit newValues = null;
        if (toAdd != null) {
          newValues = toAdd.get(newVariableType);
        }
        if (newValues == null) {
          newValues = createDoubleModel(newVariableType, grid_.getEltNb());
        }
        vValues_.put(newVariableType, newValues);
      }
    }

    var_ = Collections.unmodifiableList(new ArrayList(newVariableTypes));
    fireTransportChanged();
    return removed;
  }

  public void setProjectType(final H2dRubarProjetType _newType, final int nbTransportBlock, final CtuluCommandContainer _cmd) {
    final List<H2dVariableType> oldVariableTypes = new ArrayList<>(var_);
    final List<H2dVariableTransType> oldTransportVariables = new ArrayList<>();
    if (transportVariables != null) {
      oldTransportVariables.addAll(transportVariables);
    }
    final List<H2dVariableType> newVariableTypes = new ArrayList<>();
    newVariableTypes.addAll(getUsedNonTransportVariables());
    if (_newType.isTransportType()) {
      transportVariables = getTransportVariables(nbTransportBlock);
      newVariableTypes.addAll(transportVariables);
    }
    if (oldVariableTypes.size() == newVariableTypes.size()) {
      return;
    }
    final Map<H2dVariableType, CtuluCollectionDoubleEdit> removed = updateVariableContent(newVariableTypes, null);

    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          transportVariables = getTransportVariables(nbTransportBlock);
          updateVariableContent(newVariableTypes, null);
        }

        @Override
        public void undo() {
          transportVariables = oldTransportVariables;
          updateVariableContent(oldVariableTypes, removed);
        }
      });
    }
  }

  @Override
  public int testCoteEau(final CtuluListSelectionInterface _selection, final CtuluCommandContainer _cmd) {
    final String val = rubarParams_.getValue(H2dRubarDicoModel.getHauteurMini(rubarParams_.getDicoFileFormatVersion()));
    double hMin = this.hIsZero_;
    if (val != null) {
      try {
        hMin = Double.parseDouble(val);
      } catch (final NumberFormatException _evt) {
        // FuLog.error(_evt);
        // on s'en fout...

      }
    }
    // on prend le max de 0 et hMin car c'est le but de cette m�thode
    // normalement hMin est >=0 mais avec Rubar on ne sait jamais.
    return H2DLib.testCoteEau(this, Math.max(0, hMin), _selection, _cmd);
  }

  @Override
  public boolean isConcentrationParHauteur() {
    return isConcentrationParHauteur_;
  }
}
