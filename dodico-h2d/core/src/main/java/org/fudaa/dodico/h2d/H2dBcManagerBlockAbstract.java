/*
 * @creation 9 sept. 2003
 * @modification $Date: 2007-01-19 13:07:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;

/**
 * @author deniger
 * @version $Id: H2dBcManagerBlockAbstract.java,v 1.14 2007-01-19 13:07:19 deniger Exp $
 */
public abstract class H2dBcManagerBlockAbstract extends H2dBcManagerAbstract implements H2dBcManagerBlockInterface {

  protected H2dBcFrontierBlockInterface[] bordByFrontier_;

  protected H2dBcManagerBlockAbstract(final EfGridInterface _m) {
    super(_m);
  }

  protected H2dBcManagerBlockAbstract(final EfGrid _m, final H2dBcFrontierBlockInterface[] _borders) {
    super(_m);
    bordByFrontier_ = _borders;
  }

  protected boolean setBordType(final H2dBoundary _b, final H2dBoundaryType _type) {
    return _b.setBoundaryType(_type, this);
  }

  @Override
  public final H2dBcFrontierBlockInterface getBlockFrontier(final int _i) {
    return bordByFrontier_[_i];
  }

  /**
   * Permet de recuperer un bord a partir de l'indice "cumule". Tous les bords de toutes les frontieres sont parcourus
   * jusqu'a ce que l'indice soit le bon.
   *
   * @param _idxGeneral l'indice de bord general
   * @return le bord d'indice _idxGeneral
   */
  public final H2dBoundary getBoundary(final int _idxGeneral) {
    final int n = bordByFrontier_.length;
    int shiftL = 0;
    for (int i = 0; i < n; i++) {
      if (_idxGeneral < (shiftL + bordByFrontier_[i].getNbBord())) {
        return bordByFrontier_[i].getBord(_idxGeneral - shiftL);
      }
      shiftL += bordByFrontier_[i].getNbBord();
    }
    return null;
  }

  /**
   * @param _frIdx l'indice de la frontiere voulu
   * @param _idxOnFrontier l'indice du bord sur cette frontiere
   * @return le bord n�<code>_idxOnFrontier</code> appartenant a la frontiere <code>_frIdx</code>.
   */
  public final H2dBoundary getBoundary(final int _frIdx, final int _idxOnFrontier) {
    return getBlockFrontier(_frIdx).getBord(_idxOnFrontier);
  }

  /**
   * @return le nombre de frontiere
   */
  public final int getNbBcFrontier() {
    return bordByFrontier_.length;
  }

  /**
   * @return le nombre de frontiere liquide
   */
  public int getNbLiquidBoundary() {
    final int n = bordByFrontier_.length;
    int r = 0;
    for (int i = 0; i < n; i++) {
      final H2dBcFrontierBlockInterface bordBound = bordByFrontier_[i];
      final int nbBord = bordBound.getNbBord() - 1;
      for (int j = nbBord; j >= 0; j--) {
        if (bordBound.getBord(j).getType().isLiquide()) {
          r++;
        }
      }
    }
    return r;
  }

  /**
   * @return le nombre total de bord (sur toutes les frontieres)
   */
  public final int getNbTotalBoudary() {
    int r = 0;
    for (int i = bordByFrontier_.length - 1; i >= 0; i--) {
      r += bordByFrontier_[i].getNbBord();
    }
    return r;
  }

  @Override
  public final List getUsedBoundaryType() {
    final Set s = new HashSet(getNbBoundaryType());
    for (int i = bordByFrontier_.length - 1; i >= 0; i--) {
      bordByFrontier_[i].fillWithUsedBoundaryType(s);
    }
    final List r = new ArrayList(s);
    Collections.sort(r);
    return r;
  }
}