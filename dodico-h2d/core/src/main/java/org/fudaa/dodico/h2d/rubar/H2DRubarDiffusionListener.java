/*
 *  @creation     18 oct. 2004
 *  @modification $Date: 2006-11-20 14:23:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

/**
 * @author Fred Deniger
 * @version $Id: H2DRubarDiffusionListener.java,v 1.1 2006-11-20 14:23:55 deniger Exp $
 */
public interface H2DRubarDiffusionListener {

  /**
   * Envoye si la friction a ete modifiee.
   */
  void diffusionChanged(boolean _addOrRemove);

}
