/*
 * @creation 21 juin 2004
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import org.fudaa.dodico.commun.DodicoEnumType;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: H2dRefluxBoundaryType.java,v 1.4 2006-09-19 14:43:25 deniger Exp $
 */
public final class H2dRefluxBoundaryType extends H2dBoundaryType {

  /**
   * Type liquide.
   */
  public final static H2dRefluxBoundaryType LIQUIDE = new H2dRefluxBoundaryType(H2dResource.getS("Ouvert"));
  /**
   * Type liquide avec debit impose.
   */
  public final static H2dRefluxBoundaryType LIQUIDE_DEBIT_IMPOSE = new H2dRefluxBoundaryType(H2dResource
      .getS("Ouvert d�bit impos�"));
  /**
   * Type mixte pour caracteriser les selections comportement des bords heterogenes.
   */
  public final static H2dRefluxBoundaryType MIXTE = new H2dRefluxBoundaryType(H2dResource.getS("Mixte"), true);
  /**
   * Type ferme.
   */
  public final static H2dRefluxBoundaryType SOLIDE = new H2dRefluxBoundaryType(H2dResource.getS("Ferm�"), true);
  /**
   * Type ferme avec frottement.
   */
  public final static H2dRefluxBoundaryType SOLIDE_FROTTEMENT = new H2dRefluxBoundaryType(H2dResource
      .getS("Ferm� avec frottement"), true);

  /**
   * @param _nom
   * @param _isSolide
   */
  public H2dRefluxBoundaryType(final String _nom, final boolean _isSolide) {
    super(_nom, _isSolide);
  }

  /**
   * @param _nom
   */
  public H2dRefluxBoundaryType(final String _nom) {
    super(_nom);
  }

  public DodicoEnumType[] getArray() {
    return new H2dRefluxBoundaryType[] { LIQUIDE, LIQUIDE_DEBIT_IMPOSE, MIXTE, SOLIDE, SOLIDE_FROTTEMENT };
  }

}