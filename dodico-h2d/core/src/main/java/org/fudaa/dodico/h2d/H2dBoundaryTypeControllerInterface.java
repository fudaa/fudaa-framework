/*
 *  @creation     18 mai 2004
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import java.util.List;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;

/**
 * Cette interface a pour but de controler les variables autorisees pour un type de bord.
 *
 * @author Fred Deniger
 * @version $Id: H2dBoundaryTypeControllerInterface.java,v 1.6 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dBoundaryTypeControllerInterface {

  /**
   * @param _t le type de bord en question
   * @return une liste non modifiable des variables autorisees pour ce type de bord
   */
  List getVariableFor(H2dBoundaryType _t);

}
