/*
 * @creation 18 oct. 2004
 * @modification $Date: 2007-03-02 13:00:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;

/**
 * @author Fred Deniger
 * @version $Id: H2DRubarSolutionsInitialesInterface.java,v 1.8 2007-03-02 13:00:52 deniger Exp $
 */
public interface H2DRubarSolutionsInitialesInterface {
  int getCommonValueSize();

  /**
   * @return le nombre de valeurs
   */
  int getNbValues();

  /**
   * @return le t 0
   */
  double getT0();

  /**
   * @return true si vitesse ou concentration fois hauteur
   */
  boolean isVitesse();

  /**
   * @return true si on utilise une concentration et non une concentration par hauteur
   */
  boolean isConcentrationParHauteur();

  /**
   * @param _i l'indice [0;nbValue[
   * @return le model
   */
  CtuluCollectionDoubleEdit getValues(int _i);

}