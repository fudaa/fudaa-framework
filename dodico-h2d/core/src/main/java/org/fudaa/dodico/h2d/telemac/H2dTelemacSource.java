/*
 * @creation 29 nov. 2004
 * 
 * @modification $Date: 2006-09-19 14:43:24 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.ctulu.collection.CtuluListDouble;

/**
 * Valeurs du siphon. Ces valeurs sont g�r�es par le fichier cas.
 * 
 * @author Fred Deniger
 * @version $Id: H2dTelemacSource.java,v 1.5 2006-09-19 14:43:24 deniger Exp $
 */
public class H2dTelemacSource {

  double q_;// optionel debit
  double vx_;// optionel vitesse selon x de la source
  double vy_;// optionel vitesse selon y de la source
  double x_;// requis abscisse de la source
  double y_;// requis ordonnes de la source
  CtuluListDouble traceurs = new CtuluListDouble();

  protected H2dTelemacSource() {

  }

  protected H2dTelemacSource(final H2dTelemacSource _s) {
    traceurs = new CtuluListDouble(_s.traceurs);
    vx_ = _s.vx_;
    vy_ = _s.vy_;
    y_ = _s.y_;
    x_ = _s.x_;
    q_ = _s.q_;
  }

  protected final boolean setQ(final double _q) {
    if (_q != q_) {
      q_ = _q;
      return true;
    }
    return false;
  }

  protected final boolean setTraceurs(final double[] _t) {
    traceurs.removeAll();
    if (_t != null) {
      traceurs.addAll(_t, null);
    }
    return true;
  }

  protected final boolean setVx(final double _vx) {
    if (_vx != vx_) {
      vx_ = _vx;
      return true;
    }
    return false;
  }

  protected final boolean setVy(final double _vy) {
    if (_vy != vy_) {
      vy_ = _vy;
      return true;
    }
    return false;
  }

  protected final boolean setX(final double _x) {
    if (_x != x_) {
      x_ = _x;
      return true;
    }
    return false;
  }

  protected final boolean setY(final double _y) {
    if (_y != y_) {
      y_ = _y;
      return true;
    }
    return false;
  }

  /**
   * @return le debit
   */
  public final double getQ() {
    return q_;
  }

  /**
   * @return le traceur
   */
  public final double[] getTracers() {
    return traceurs.getValues();
  }

  public final String[] getTracersInString() {
    double[] tracers = getTracers();
    String[] strings = new String[tracers.length];
    for (int i = 0; i < strings.length; i++) {
      strings[i] = Double.toString(tracers[i]);

    }
    return strings;
  }

  /**
   * @return la vitesse selon x
   */
  public final double getVx() {
    return vx_;
  }

  /**
   * @return la vitesse selon y
   */
  public final double getVy() {
    return vy_;
  }

  /**
   * @return l'abscisse
   */
  public final double getX() {
    return x_;
  }

  /**
   * @return l'ordonnee
   */
  public final double getY() {
    return y_;
  }

  protected void ensureTracerSizeIs(int nbTraceur) {
    if (nbTraceur == traceurs.getSize()) {
      return;
    }
    double[] values = new double[nbTraceur];
    int max = Math.min(nbTraceur, traceurs.getSize());
    for (int i = 0; i < max; i++) {
      values[i] = traceurs.getValue(i);
    }
    traceurs.initWith(values);
  }
}