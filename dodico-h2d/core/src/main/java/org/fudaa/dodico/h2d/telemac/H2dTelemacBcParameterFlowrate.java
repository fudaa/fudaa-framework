/*
 * @creation 24 mai 2004
 * @modification $Date: 2006-09-19 14:43:24 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import java.util.HashMap;
import java.util.Map;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoParamsInterface;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dBoundaryTypeCommon;
import org.fudaa.dodico.h2d.type.H2dVariableType;

final class H2dTelemacBcParameterFlowrate extends H2dTelemacBcParameter {

  DicoEntite profilVitesse_;

  H2dTelemacBcParameterFlowrate(final H2dTelemacBcManager _mng, final DicoEntite.Vecteur _ent, final H2dBoundaryType[] _bords,
      final DicoEntite.Vecteur _entProfilVitesse) {
    super(_mng, _ent, _bords);
    profilVitesse_ = _entProfilVitesse;
  }

  @Override
  public String getValueFromBord(final H2dTelemacBoundary _b, int valueIdx) {
    return _b.getFlowrate();
  }

  @Override
  public boolean isKeywordImposed() {
    return true;
  }

  @Override
  protected boolean setValueInBord(final H2dTelemacBoundary _b, final String _s, int valueIdx) {
    return _b.setFlowrate(_s);
  }

  @Override
  public boolean isValueFixedFor(final H2dTelemacBcManager _p, final H2dTelemacBoundary _b) {
    if (_b.getType().isLiquide()) {
      if (isValid(_p.dico_)) {
        return true;
      }
      if (_p.getH2dFileFormat().isFlowrateComputeFromVelocities(_b.getVelocityProfil())) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean isParameterFor(final H2dVariableType _v) {
    return _v == H2dVariableType.DEBIT;
  }

  @Override
  public H2dVariableType[] getVariablesForPoint(final H2dTelemacDicoFileFormatVersion _v, final H2dTelemacBoundary _t,
      final DicoParamsInterface _dico) {
    if ((_t.getType() == H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE) && (_dico.isValueSetFor(profilVitesse_))) {
      final String s = _t.getVelocityProfil();
      /*
       * if (_v.isFlowrateComputeFromElevation(s)) { return new H2dVariableType[] { H2dVariableType.HAUTEUR_EAU}; } else
       */if (_v.isFlowrateComputeFromVelocityU(s)) {
        return new H2dVariableType[] { H2dVariableType.VITESSE_U };
      } else if (_v.isFlowrateComputeFromVelocities(s)) {
        return new H2dVariableType[] { H2dVariableType.VITESSE_U, H2dVariableType.VITESSE_V };
      }
    }
    return null;
  }

  @Override
  public Map getVariablesForPointInfo(final H2dTelemacDicoFileFormatVersion _v, final H2dTelemacBoundary _t,
      final DicoParamsInterface _dico) {
    Map r = null;
    if ((_t.getType() == H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE) && (_dico.isValueSetFor(profilVitesse_))) {
      final String s = _t.getVelocityProfil();
      if (_v.isFlowrateComputeFromElevation(s)) {
        r = new HashMap(1);
        r.put(H2dVariableType.HAUTEUR_EAU, H2dResource.getS("Qn=racine carree(h)"));
      } else if (_v.isFlowrateComputeFromVelocityU(s)) {
        r = new HashMap(1);
        r.put(H2dVariableType.VITESSE_U, H2dResource.getS("ratio pour Qn"));
      } else if (_v.isFlowrateComputeFromVelocities(s)) {
        r = new HashMap(1);
        r.put(H2dVariableType.VITESSE_U, H2dResource.getS("ratio pour Qn"));
        r.put(H2dVariableType.VITESSE_V, H2dResource.getS("ratio pour Qt"));
      }
    }
    return r;
  }

  
  @Override
  public H2dVariableType getVariable() {
    return H2dVariableType.DEBIT;
  }
}