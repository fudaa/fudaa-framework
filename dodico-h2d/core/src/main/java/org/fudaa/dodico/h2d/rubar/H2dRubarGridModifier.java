package org.fudaa.dodico.h2d.rubar;

import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.ef.operation.EfEdgeMapper;
import org.fudaa.dodico.h2d.type.H2dRubarBoundaryType;

public class H2dRubarGridModifier implements H2dRubarGridModifierContrat {
  private final H2dRubarGrid source;

  public H2dRubarGridModifier(H2dRubarGrid source) {
    this.source = source;
  }

  @Override
  public void modify(H2dRubarGridAreteSource toModify, ProgressionInterface progressionInterface) {
    EfEdgeMapper<H2dRubarBoundaryType> mapper = createMapper();
    final int nbAreteTarget = toModify.getRubarGrid().getNbAretes();
    final TIntArrayList areteEntrantIndexes = new TIntArrayList();
    for (int i = 0; i < nbAreteTarget; i++) {
      final H2dRubarArete arete = toModify.getRubarGrid().getRubarArete(i);
      if (arete.getType() != null) {
        final H2dRubarBoundaryType rubarBoundaryType = mapper.getObject(arete.getCentreX(toModify.getRubarGrid()), arete.getCentreY(toModify.getRubarGrid()));
        if (rubarBoundaryType != null) {
          arete.setType(rubarBoundaryType);
          if (arete.isExternAndLiquidAndTransient()) {
            areteEntrantIndexes.add(i);
          }
        }
      }
    }
    if (toModify instanceof H2dRubarGridAreteDefault) {
      ((H2dRubarGridAreteDefault) toModify).setIdxAreteLimiteEntrante(areteEntrantIndexes.toNativeArray());
    }
  }

  private EfEdgeMapper<H2dRubarBoundaryType> createMapper() {
    EfEdgeMapper<H2dRubarBoundaryType> mapper = new EfEdgeMapper<>(source);
    int nbArete = source.getNbAretes();
    for (int i = 0; i < nbArete; i++) {
      final H2dRubarArete rubarArete = source.getRubarArete(i);
      if (rubarArete.getType() != null && rubarArete.getType().isLiquide()) {
        mapper.addEdge(rubarArete.getPt1Idx(), rubarArete.getPt2Idx(), rubarArete.getType());
      }
    }
    return mapper;
  }
}
