/*
 * @creation 23 mars 2004
 * @modification $Date: 2007-01-10 09:04:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: H2dRefluxSolutionInitAdapterInterface.java,v 1.5 2007-01-10 09:04:26 deniger Exp $
 */
public interface H2dRefluxSolutionInitAdapterInterface {

  /**
   * @param _t la variable a tester
   * @return true si support�e.
   */
  boolean isValideVariable(H2dVariableType _t);

  /**
   * @param _t la variable a consid�rer
   * @param _dest le tableau a modifier
   * @param _cmd TODO
   */
  void setValuesIn(H2dVariableType _t, CtuluArrayDouble _dest, CtuluCommandContainer _cmd);

  /**
   * @return le nombre de point considere
   */
  int getNbPt();

  /**
   * @param _idx
   * @return le vitesse u
   */
  double getU(int _idx);

  /**
   * @param _idx l'indice du point
   * @return la vitesse v au point donne
   */
  double getV(int _idx);

  /**
   * Doit donner la hauteur d'eau correcte au point (point transit,...).
   *
   * @param _idx
   * @return la hauteur d'eau au point
   */
  double getCoteEau(int _idx);
}