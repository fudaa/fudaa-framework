/*
 * @creation 30 sept. 2003
 * 
 * @modification $Date: 2006-09-19 14:43:24 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.fileformat.FileFormatVersionInterface;
import org.fudaa.dodico.dico.DicoCasFileFormat;
import org.fudaa.dodico.dico.DicoCasFileFormatVersion;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoLanguage;
import org.fudaa.dodico.dico.DicoModelAbstract;
import org.fudaa.dodico.h2d.type.H2dBoundaryHouleType;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dBoundaryTypeCommon;

/**
 * @author deniger
 * @version $Id: H2dTelemacDicoFileFormatVersion.java,v 1.23 2006-09-19 14:43:24 deniger Exp $
 */
public abstract class H2dTelemacDicoFileFormatVersion extends DicoCasFileFormatVersion {

  /**
   * @param _ft le format.
   * @param _dico le dico utilise.
   */
  public H2dTelemacDicoFileFormatVersion(final DicoCasFileFormat _ft, final DicoModelAbstract _dico) {
    super(_ft, _dico);
  }

  public abstract boolean isTelemac2d();

  public abstract boolean isStbtel();

  /**
   * @return le mot-cle definissant le nombre de seuils.
   */
  public DicoEntite getSeuilNombreMotCle() {
    return getEntiteFor(new String[] { "NOMBRE DE SEUILS", "NUMBER OF WEIRS" });
  }

  public DicoEntite getTracersNombreMotCle() {
    return getEntiteFor(new String[] { "NOMBRE DE TRACEURS", "NUMBER OF TRACERS" });
  }

  /**
   * @return le mot-cl� d�finissant le fichier de reprise
   */
  public abstract DicoEntite getRepriseFileKeyword();

  /**
   * @return le mot-cl� d�finissant le boolean d'activation
   */
  public abstract DicoEntite getRepriseBooleanKeyword();

  /**
   * @return true si les siphons sont support�s par ce modele
   */
  public abstract boolean isSiphonAvailable();

  /**
   * @return true si les courbes transitoires sont support�es par ce modele
   */
  public abstract boolean isTransientAvailable();

  /**
   * @return true si la friction de fond est modifiable
   */
  public boolean isBottomFrictionModifiable() {
    final String codeName = getCodeName();
    return "telemac2d".equals(codeName) || "subief2d".equals(codeName);
  }

  /**
   * @return true si la viscosit� est modifiable
   */
  public boolean isViscosityModifiable() {
    final String codeName = getCodeName();
    return "telemac2d".equals(codeName) || "subief2d".equals(codeName);
  }

  /**
   * @return les noms du mot-cl� d�finissant le coefficient de frottement
   */
  public String[] getFrictionCoefKeyWord() {
    return new String[] { "COEFFICIENT DE FROTTEMENT", "FRICTION COEFFICIENT" };
  }

  /**
   * @return les mot-cl�s (fr,anglais) utilis�s pour d�crire le fichier de reprise de calcul.
   */
  public String[] getRepriseFileName() {
    return new String[] { "FICHIER DU CALCUL PRECEDENT", "PREVIOUS COMPUTATION FILE" };
  }

  /**
   * @return les mot-cl�s (fr,anglais) utilis�s pour activer la reprise de calcul.
   */
  public String[] getRepriseBooleanName() {
    return new String[] { "SUITE DE CALCUL", "COMPUTATION CONTINUED" };
  }

  /**
   * @return les noms du mot-cl� d�finissant le coefficient de frottement
   */
  public String[] getViscosityKeyWord() {
    return new String[] { "COEFFICIENT DE DIFFUSION DES VITESSES", "VELOCITY DIFFUSIVITY" };
  }

  /**
   * @return les variables de fond
   */
  public abstract String[] getVariableFond();

  /**
   * @return le format a utiliser pour serafin
   */
  public abstract FileFormatVersionInterface getDefaultSerafinFormat();

  /**
   * Seul artemis renvoie false.
   * 
   * @return true si les points de bord sont editable
   */
  public boolean isBcPointEditable() {
    return !getCodeName().equals(H2dTelemacDicoParams.getArtemisId());
  }

  /**
   * @return l'identifiant friction en anglais
   */
  public String getFrFrictionKey() {
    return "FROTTEMENT";
  }

  /**
   * @return l'identifiant friction en francais
   */
  public String getEnFrictionKey() {
    return "BOTTOM FRICTION";
  }

  /**
   * @return l'identifiant friction en anglais
   */
  public String getFrViscosityKey() {
    return "VISCOSITE TURB.";
  }

  /**
   * @return l'identifiant friction en francais
   */
  public String getEnViscosityKey() {
    return "VISCOSITY";
  }

  /**
   * @return l'identifiant friction correspondant au langage utilise
   */
  public String getFrictionKey() {
    if (getDico().getLanguageIndex() == DicoLanguage.ENGLISH_ID) {
      return getEnFrictionKey();
    }
    return getFrFrictionKey();
  }

  /**
   * @return l'identifiant friction correspondant au langage utilise
   */
  public String getViscosityKey() {
    if (getDico().getLanguageIndex() == DicoLanguage.ENGLISH_ID) {
      return getEnViscosityKey();
    }
    return getFrViscosityKey();
  }

  /**
   * @return le nom de la variable pour le fond (dans le bon langage definit par le dico)
   */
  public abstract String getBottomKey();

  /**
   * @return l'identifiant friction ne correspondant pas au langage utilise
   */
  public String getOtherFrictionKey() {
    if (getDico().getLanguageIndex() == DicoLanguage.ENGLISH_ID) {
      return getFrFrictionKey();
    }
    return getEnFrictionKey();
  }

  /**
   * @return true si la rugosite est modifiable.
   */
  public boolean isRugosityModifiable() {
    return (!getCodeName().equals(H2dTelemacDicoParams.getTomawacId())) && (!getCodeName().equals(H2dTelemacDicoParams.getSubief2dId()))
        && (!getCodeName().equals(H2dTelemacDicoParams.getSisypheId()));
  }

  @Override
  public String getCodeName() {
    return getFileFormat().getName();
  }

  /**
   * @return mot-cle definissant le fichier de maillage
   */
  public abstract DicoEntite getMaillageEntiteFile();

  /**
   * @return mot-cle definissant le fichier des cl
   */
  public abstract DicoEntite getCLEntiteFile();

  /**
   * @return mot-cle definissant le fichier des cl liquide (telemac 2d)
   */
  public abstract DicoEntite getCLLiquideEntiteFile();

  /**
   * @return mot-cle definissant le fichier des seuils siphons (telemac 2d)
   */
  public abstract DicoEntite getSeuilSiphonFile();

  /**
   * @return mot-cle definissant le debit
   */
  public abstract DicoEntite getPrescribedFlowrate();

  /**
   * @return mot-cle definissant la vitesse
   */
  public abstract DicoEntite getPrescribedVelocity();

  /**
   * @return mot-cle definissant le cote d'eau
   */
  public abstract DicoEntite getPrescribedElevation();

  /**
   * @return mot-cle definissant le traceur
   */
  public abstract DicoEntite getPrescribedTracer();

  public DicoEntite getInitialElevationKw() {
    return getEntiteFor(new String[] { "COTE INITIALE", "INITIAL ELEVATION" });
  }

  /**
   * @return mot-cle definissant le profil des vitesses
   */
  public abstract DicoEntite getVelocityProfils();

  /**
   * @return mot-cle definissant l'option liquide
   */
  public abstract DicoEntite getLiquidBoundaryOption();

  /**
   * @return mot-cle definissant le nombre de pas de temps
   */
  public abstract DicoEntite getNbPasTemps();

  /**
   * @return mot-cle definissant la valeur du pas de temps
   */
  public abstract DicoEntite getPasTemps();

  /**
   * @return mot-cle definissant la duree du calcul
   */
  public abstract DicoEntite getDureeDuCalcul();

  /**
   * @param _value la valeur du mot-cle profil de vitesse
   * @see #getVelocityProfils()
   * @return true si le debit est definit a partir des vitesses
   */
  public abstract boolean isFlowrateComputeFromVelocities(final String _value);

  /**
   * @param _value la valeur du mot-cle profil de vitesse
   * @see #getVelocityProfils()
   * @return true si le debit est definit a partir de la vitesse u uniquement
   */
  public abstract boolean isFlowrateComputeFromVelocityU(final String _value);

  /**
   * @param _value la valeur du mot-cle profil de vitesse
   * @see #getVelocityProfils()
   * @return true si le debit est definit a partir de la hauteur
   */
  public abstract boolean isFlowrateComputeFromElevation(final String _value);

  /**
   * @return la liste des type de bord gere par code en cours.
   */
  public CtuluPermanentList getAllowedBordTypeList() {
    CtuluPermanentList list = null;
    final String codeName = getCodeName();
    if (codeName.equals(H2dTelemacDicoParams.getArtemisId())) {
      list = new CtuluPermanentList(CtuluLibArray.sort(new H2dBoundaryType[] { H2dBoundaryTypeCommon.SOLIDE,
          H2dBoundaryHouleType.LIQUIDE_HOULE_IMPOSEE, H2dBoundaryHouleType.LIQUIDE_HOULE_INCIDENCE, H2dBoundaryHouleType.LIQUIDE_SORTIE_LIBRE }));
    } else if ((codeName.equals(H2dTelemacDicoParams.getSubief2dId())) || codeName.equals(H2dTelemacDicoParams.getTomawacId())
        || codeName.equals(H2dTelemacDicoParams.getSisypheId())) {
      list = new CtuluPermanentList(CtuluLibArray.sort(new H2dBoundaryType[] { H2dBoundaryTypeCommon.SOLIDE, H2dBoundaryHouleType.FRONTIERE_LIBRE,
          H2dBoundaryHouleType.FRONTIERE_VALEURS_IMPOSEES }));
    } else {
      list = new CtuluPermanentList(CtuluLibArray.sort(new H2dBoundaryType[] { H2dBoundaryTypeCommon.SOLIDE, H2dBoundaryTypeCommon.LIQUIDE,
          H2dBoundaryTypeCommon.LIQUIDE_VITESSES_IMPOSEES, H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_IMPOSEE,
          H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES, H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE,
          H2dBoundaryTypeCommon.LIQUIDE_ONDE_INCIDENCE }));
    }
    return list;
  }

  public abstract boolean isTelemac3d();
}