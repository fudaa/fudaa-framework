/*
 * @creation 1 juin 2004
 * @modification $Date: 2006-09-19 14:43:24 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.dodico.dico.DicoParamsInterface;
import org.fudaa.dodico.h2d.type.H2dBoundaryHouleType;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * Parametre limite propre a la concentration (tomawac).
 *
 * @author Fred Deniger
 * @version $Id: H2dTelemacBcParameterConcentration.java,v 1.7 2006-09-19 14:43:24 deniger Exp $
 */
public class H2dTelemacBcParameterConcentration extends H2dTelemacBcParameter {

  /**
   * Initialise par defaut.
   */
  public H2dTelemacBcParameterConcentration() {
    super(null, null, new H2dBoundaryType[] { H2dBoundaryHouleType.FRONTIERE_VALEURS_IMPOSEES });
  }

  @Override
  public H2dVariableType getVariable() {
    return H2dVariableTransType.CONCENTRATION;
  }

  @Override
  public boolean isKeywordImposed() {
    return false;
  }

  @Override
  public String getValueFromBord(final H2dTelemacBoundary _b, int valueIdx) {
    return null;
  }

  @Override
  protected boolean setValueInBord(final H2dTelemacBoundary _b, final String _s, int valueIdx) {
    return false;
  }

  @Override
  public H2dVariableType[] getVariablesForPoint(final H2dTelemacDicoFileFormatVersion _v, final H2dTelemacBoundary _t,
      final DicoParamsInterface _dico) {
    if (_t.getType().equals(H2dBoundaryHouleType.FRONTIERE_VALEURS_IMPOSEES)) {
      return new H2dVariableType[] { getVariable() };
    }
    return null;
  }
}