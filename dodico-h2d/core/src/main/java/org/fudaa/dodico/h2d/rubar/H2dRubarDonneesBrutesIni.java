/*
 * @creation 1 oct. 2004
 * @modification $Date: 2007-03-02 13:00:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.collection.CollectionPointDataDoubleInterface;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarDonneesBrutesIni.java,v 1.14 2007-03-02 13:00:52 deniger Exp $
 */
public class H2dRubarDonneesBrutesIni extends H2dRubarDonneesBrutes {
  List<H2dVariableType> variables = new ArrayList<>();
  final H2dRubarParameters params_;
  int nbTransportBlocks;

  @Override
  public int getVariableBlockLength() {
    return 3;
  }

  /**
   * @param _fmt le formatteur de nombre
   */
  public H2dRubarDonneesBrutesIni(final H2dRubarParameters _params, final H2dRubarNumberFormatter _fmt) {
    super(0, _fmt);
    params_ = _params;
    updateVariablesContents(_params.getProjetType(), _params.getBcMng().nbConcentrationBlocks, null);
  }

  private void updateVariables(H2dRubarProjetType projectType, int nbTransportBlocks) {
    this.nbTransportBlocks = nbTransportBlocks;
    variables.clear();
    variables.add(H2dVariableType.HAUTEUR_EAU);
    variables.add(H2dVariableType.DEBIT_X);
    variables.add(H2dVariableType.DEBIT_Y);
    variables.add(H2dVariableType.COTE_EAU);
    if (projectType.isTransportType()) {
      variables.addAll(params_.getBcMng().getTransportVariables(nbTransportBlocks));
    }
    nbVariable_ = variables.size();
  }

  public CtuluCollectionDouble getBathy() {
    return params_.getBathyModel();
  }

  @Override
  public double getIndeterminedValue(final int _i) {
    return getInderteminedValue();
  }

  public int getCoteIdx() {
    return 3;
  }

  public int getHauteurIdx() {
    return 0;
  }

  public double getInderteminedValue() {
    return 999.999;
  }

  @Override
  public String getFirstLine() {
    return "0.0";
  }

  @Override
  public String getID() {
    return H2dRubarDonneesBrutesMng.INI_ID;
  }

  @Override
  public String getTitle() {
    return H2dResource.getS("solutions initiales");
  }

  @Override
  public Object getVariableId(final int _idx) {
    if (_idx >= 0 && _idx < getNbVariable()) {
      return variables.get(_idx);
    }
    return null;
  }

  @Override
  protected NuagePoint createNuagePoint(final CollectionPointDataDoubleInterface _init) {
    final NuagePoint r = new NuagePoint(_init == null ? 30 : _init.getNbPoint());
    if (_init != null) {
      r.initFromAndKeepSameNbValues(_init, false);
    }
    return r;
  }

  @Override
  public boolean isVariableActive(final int _idx) {
    return true;
  }

  @Override
  public void setNbConcentrationBlocks(final int newNbConcentrationBlocks, final CtuluCommandComposite _cmd) {
    updateVariablesContents(params_.getProjetType(), newNbConcentrationBlocks, _cmd);
  }

  @Override
  protected boolean setProjectTypeChanged(final H2dRubarProjetType h2dRubarProjetType, final CtuluCommandComposite _cmd) {
    return updateVariablesContents(h2dRubarProjetType, params_.getBcMng().nbConcentrationBlocks, _cmd);
  }

  private boolean updateVariablesContents(final H2dRubarProjetType h2dRubarProjetType, final int nbTransportBlocks, CtuluCommandComposite _cmd) {
    final List<H2dVariableType> oldList = new ArrayList<>(variables);
    updateVariables(h2dRubarProjetType, nbTransportBlocks);

    final int oldSize = oldList.size();
    final int newSize = variables.size();
    if (oldSize == newSize) {
      return false;
    }
    if (newSize > oldSize) {
      addData(newSize - oldSize, _cmd);
    } else if (newSize < oldSize) {
      removeData(oldSize - newSize, _cmd);
    }
    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void undo() {
          variables = oldList;
          nbVariable_ = variables.size();
        }

        @Override
        public void redo() {
          updateVariables(h2dRubarProjetType, nbTransportBlocks);
        }
      });
    }
    fireDonneesBrutesDataChanged();
    return true;
  }

  private void removeData(int nbToRemove, final CtuluCommandContainer _cmd) {
    if (nuage_ != null) {
      nuage_.removeLastData(nbToRemove, _cmd);
    }
    if (grid_ != null) {
      grid_.removeLastData(nbToRemove, _cmd);
    }
    if (parall_ != null) {
      parall_.removeLastData(nbToRemove, _cmd);
    }
  }

  private void addData(final int nbToAdd, final CtuluCommandContainer _cmd) {
    if (nuage_ != null) {
      nuage_.addData(nbToAdd, _cmd);
    }
    if (grid_ != null) {
      grid_.addData(nbToAdd, _cmd);
    }
    if (parall_ != null) {
      parall_.addLastData(nbToAdd, _cmd);
    }
  }
}
