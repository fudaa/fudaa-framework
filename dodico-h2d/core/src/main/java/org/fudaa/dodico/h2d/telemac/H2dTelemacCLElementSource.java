/*
 * @creation 7 nov. 2003
 * 
 * @modification $Date: 2007-06-05 08:59:12 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dBoundaryTypeCommon;

/**
 * Conteneur pour transferer les valeurs entre les operation io et les modele de donnees.
 * 
 * @author deniger
 * @version $Id: H2dTelemacCLElementSource.java,v 1.10 2007-06-05 08:59:12 deniger Exp $
 */
public class H2dTelemacCLElementSource {

  /**
   * comportement de u.
   */
  public H2dBcType uType_;
  /**
   * comportement de v.
   */
  public H2dBcType vType_;
  /**
   * comportement du traceur.
   */
  public H2dBcType tracerType_;
  /**
   * comportement de u.
   */
  public H2dBoundaryType bordType_;

  /**
   * valeur de u.
   */
  public double u_;
  /**
   * valeur de v.
   */
  public double v_;
  /**
   * valeur de h.
   */
  public double h_;
  /**
   * valeur de la friction.
   */
  public double friction_;
  /**
   * valeur du traceur.
   */
  public double tracer_;
  /**
   * valeur du coef A du traceur.
   */
  public double tracerCoefA_;
  /**
   * valeur du coef B du traceur.
   */
  public double tracerCoefB_;
  /**
   * L'indice global du point (dans la numerotation des points du maillage).
   */
  public int ptGlobalIdx_;

  /**
   * @return une condition pour un bord solide
   */
  public static H2dTelemacCLElementSource createDefault() {
    H2dTelemacCLElementSource res = new H2dTelemacCLElementSource();
    res.bordType_ = H2dBoundaryTypeCommon.SOLIDE;
    res.uType_ = H2dBcType.LIBRE;
    res.vType_ = H2dBcType.LIBRE;
    res.tracerType_ = H2dBcType.LIBRE;
    return res;

  }

  /**
   *
   */
  public H2dTelemacCLElementSource() {
    super();
  }


  /**
   * @param _cl la condition source
   * @param _uType comportement de u
   * @param _vType comportement de v
   * @param _tracerType comportement du traceur
   * @param _bordType le type du bord.
   */
  public final void initFrom(final H2dTelemacBoundaryCondition _cl, final H2dBcType _uType, final H2dBcType _vType,
      final H2dBcType _tracerType, final H2dBoundaryType _bordType) {
    friction_ = _cl.frottementParoi_;
    u_ = _cl.getU();
    v_ = _cl.getV();
    h_ = _cl.getH();
    tracer_ = _cl.getTracer();
    tracerCoefA_ = _cl.getTracerCoeffA();
    tracerCoefB_ = _cl.getTracerCoeffB();
    uType_ = _uType;
    vType_ = _vType;
    // hType_= _hType;
    tracerType_ = _tracerType;
    bordType_ = _bordType;
    if (bordType_.isSolide()) {
      if (_cl.getSolidUVState() == H2dTelemacBoundaryCondition.SOLID_U_ZERO) {
        uType_ = H2dBcType.PERMANENT;
        u_ = 0;
      } else if (_cl.getSolidUVState() == H2dTelemacBoundaryCondition.SOLID_V_ZERO) {
        vType_ = H2dBcType.PERMANENT;
        v_ = 0;
      }

    }
    ptGlobalIdx_ = _cl.getIndexPt();
  }

  /**
   * @param _cl la source pour l'initialisation
   */
  public void initFrom(final H2dTelemacCLElementSource _cl) {
    friction_ = _cl.friction_;
    u_ = _cl.u_;
    v_ = _cl.v_;
    h_ = _cl.h_;
    tracer_ = _cl.tracer_;
    tracerCoefA_ = _cl.tracerCoefA_;
    tracerCoefB_ = _cl.tracerCoefB_;
    uType_ = _cl.uType_;
    vType_ = _cl.vType_;
    // hType_= cl.hType_;
    tracerType_ = _cl.tracerType_;
    bordType_ = _cl.bordType_;
    ptGlobalIdx_ = _cl.ptGlobalIdx_;
  }
}