/*
 *  @creation     1 juin 2004
 *  @modification $Date: 2006-09-19 14:43:25 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.commun.DodicoEnumType;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: H2dBoundaryHouleType.java,v 1.9 2006-09-19 14:43:25 deniger Exp $
 */
public final class H2dBoundaryHouleType extends H2dBoundaryType {

  /**
   * Bord houle incidente.
   */
  public final static H2dBoundaryHouleType LIQUIDE_HOULE_INCIDENCE = new H2dBoundaryHouleType(H2dResource
      .getS("Liquide houle incidente"));
  /**
   * bord sortie libre.
   */
  public final static H2dBoundaryHouleType LIQUIDE_SORTIE_LIBRE = new H2dBoundaryHouleType(H2dResource
      .getS("Liquide sortie libre"));
  /**
   * bord houle imposee.
   */
  public final static H2dBoundaryHouleType LIQUIDE_HOULE_IMPOSEE = new H2dBoundaryHouleType(H2dResource
      .getS("Liquide houle impos�e"));
  /**
   * Tomawac Fronti�re libre.
   */
  public final static H2dBoundaryHouleType FRONTIERE_LIBRE = new H2dBoundaryHouleType(H2dResource
      .getS("Fronti�re libre"));
  /**
   * Tomawac Frontiere a valeurs impos�es.
   */
  public final static H2dBoundaryHouleType FRONTIERE_VALEURS_IMPOSEES = new H2dBoundaryHouleType(H2dResource
      .getS("Fronti�re � valeurs impos�es"));

  public static H2dBoundaryHouleType[] getConstantArray() {
    return new H2dBoundaryHouleType[] { LIQUIDE_HOULE_INCIDENCE, LIQUIDE_SORTIE_LIBRE, LIQUIDE_HOULE_IMPOSEE,
        FRONTIERE_LIBRE, FRONTIERE_VALEURS_IMPOSEES };
  }

  H2dBoundaryHouleType() {
    this(CtuluLibString.EMPTY_STRING);
  }

  /**
   * @param _nom
   */
  H2dBoundaryHouleType(final String _nom) {
    super(_nom);
  }

  /**
   * @param _nom
   * @param _isSolide
   */
  public H2dBoundaryHouleType(final String _nom, final boolean _isSolide) {
    super(_nom, _isSolide);
  }

  public DodicoEnumType[] getArray() {
    return getConstantArray();
  }

}
