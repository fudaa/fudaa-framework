/*
 *  @creation     26 juin 2003
 *  @modification $Date: 2007-06-29 15:10:27 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

/**
 * @author deniger
 * @version $Id: H2dRefluxBoundaryConditionMutable.java,v 1.13 2007-06-29 15:10:27 deniger Exp $
 */
public class H2dRefluxBoundaryConditionMutable extends H2dRefluxBoundaryCondition {
  public H2dRefluxBoundaryConditionMutable() {}

  @Override
  public boolean setNormale(final double _d) {
    return super.setNormale(_d);
  }

  @Override
  public boolean setU(final double _d) {
    return super.setU(_d);
  }

  @Override
  public boolean setH(final double _new) {
    return super.setH(_new);
  }

  @Override
  public boolean setQ(final double _d) {
    return super.setQ(_d);
  }

  @Override
  public boolean setV(final double _d) {
    return super.setV(_d);
  }

  /**
   * @param _cl le nouveau type pour u
   */
  public void setUType(final H2dBcType _cl) {
    uType_ = _cl;
  }

  /**
   * @param _cl le nouveau type pour h
   */
  public void setHType(final H2dBcType _cl) {
    hType_ = _cl;
  }

  /**
   * @param _cl le nouveau type pour q
   */
  public void setQType(final H2dBcType _cl) {
    qType_ = _cl;
  }

  @Override
  public boolean setHTypeFree() {
    return super.setHTypeFree();
  }

  /**
   * @param _cl le nouveau type pour v
   */
  public void setVType(final H2dBcType _cl) {
    if (_cl == null) {
      new Throwable().printStackTrace();
    } else {
      vType_ = _cl;
    }
  }

  /**
   * @param _idxGeneral le nouvel index general
   */
  @Override
  public void setIndexPt(final int _idxGeneral) {
    super.setIndexPt(_idxGeneral);
  }

  @Override
  public boolean setHTransitoire(final EvolutionReguliereAbstract _evol) {
    return super.setHTransitoire(_evol);
  }
  
  @Override
  public boolean setQTypePermanent(double _d) {
    return super.setQTypePermanent(_d);
  }

  @Override
  public boolean setQTransitoire(final EvolutionReguliereAbstract _evol) {
    return super.setQTransitoire(_evol);
  }

  @Override
  public boolean setUTransitoire(final EvolutionReguliereAbstract _evol) {
    return super.setUTransitoire(_evol);
  }

  @Override
  public boolean setVTransitoire(final EvolutionReguliereAbstract _evol) {
    return super.setVTransitoire(_evol);
  }
}
