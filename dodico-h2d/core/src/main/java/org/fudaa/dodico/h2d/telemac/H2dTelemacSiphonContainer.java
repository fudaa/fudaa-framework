/*
 *  @creation     1 d�c. 2004
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

/**
 * @author Fred Deniger
 * @version $Id: H2dTelemacSiphonContainer.java,v 1.3 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dTelemacSiphonContainer {

  /**
   * @return le nombre de siphons d�finis par ce container
   */
  int getNbSiphon();

  /**
   * @param _i l'indice du siphon demande
   * @return le siphon _i
   */
  H2dTelemacSiphonInterface getSiphon(int _i);

  /**
   * @return le coefficient de relaxation
   */
  double getRelaxation();

}
