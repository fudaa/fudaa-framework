package org.fudaa.dodico.h2d.telemac;

import org.fudaa.dodico.h2d.H2dEvolutionFrontiereLiquide;

/**
 * Use by the tracer: the idxVariable is the position of the tracer in the dico file.
 * 
 * @author genesis
 */
public class H2dTelemacEvolutionFrontiere implements Comparable<H2dTelemacEvolutionFrontiere> {

  H2dEvolutionFrontiereLiquide evolution;

  int idxVariable = -1;

  public H2dTelemacEvolutionFrontiere() {
    super();
  }

  @Override
  public int compareTo(H2dTelemacEvolutionFrontiere o) {
    if (o == this) {
      return 0;
    }
    if (o == null) {
      return -1;
    }
    int delta = evolution.getVariableType().compareTo(o.getTelemacEvolution().getVariableType());
    if (delta == 0) {
      delta = evolution.getIndexFrontiere() - o.getTelemacEvolution().getIndexFrontiere();
    }
    if (delta == 0) {
      delta = idxVariable - o.idxVariable;
    }
    return delta;
  }

  public H2dTelemacEvolutionFrontiere(H2dEvolutionFrontiereLiquide evolution, int idxVariable) {
    super();
    this.idxVariable = idxVariable;
    this.evolution = evolution;
  }

  public int getIdxVariable() {
    return idxVariable;
  }

  public H2dEvolutionFrontiereLiquide getTelemacEvolution() {
    return evolution;
  }

  public boolean isIdxVariableDefined() {
    return idxVariable >= 0;
  }

  public void setEvolution(H2dEvolutionFrontiereLiquide evolution) {
    this.evolution = evolution;
  }

  public void setIdxVariable(int idxVariable) {
    this.idxVariable = idxVariable;
  }

}
