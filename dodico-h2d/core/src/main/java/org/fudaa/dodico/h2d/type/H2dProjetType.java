/*
 * @creation 26 juin 2003
 * @modification $Date: 2007-04-30 14:21:37 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import java.util.Iterator;
import java.util.List;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.dodico.commun.DodicoEnumType;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author deniger
 * @version $Id: H2dProjetType.java,v 1.19 2007-04-30 14:21:37 deniger Exp $
 */
public final class H2dProjetType extends DodicoEnumType {

  /**
   * Type couranto 2D.
   */
  public static final H2dProjetType COURANTOLOGIE_2D = new H2dProjetType(H2dResource.getS("Courantologie 2D"),
      "VIT_NU_CON");
  /**
   * Type couranto 2D avec longueur de melange.
   */
  public static final H2dProjetType COURANTOLOGIE_2D_LMG = new H2dProjetType(H2dResource
      .getS("Courantologie 2D longueur m�lange"), "VIT_NU_LMG");
  /**
   * Type transport.
   */
  public static final H2dProjetType TRANSPORT = new H2dProjetType(H2dResource.getS("Transport"), "TEMPERATURE");
  /**
   * La liste des types de projet.
   */
  public static final List LIST = new CtuluPermanentList(CtuluLibArray.sort(new H2dProjetType[] { COURANTOLOGIE_2D,
      COURANTOLOGIE_2D_LMG, TRANSPORT }));

  public static H2dProjetType[] getConstantArray() {
    return new H2dProjetType[] { COURANTOLOGIE_2D, COURANTOLOGIE_2D_LMG, TRANSPORT };
  }

  /**
   * @param _inpId l'identifiant utilise dans les fichiers inp
   * @return le type du projet ou null si id inconnu
   */
  public static H2dProjetType getProjetFromRefluxId(final String _inpId) {
    H2dProjetType t;
    for (final Iterator it = LIST.iterator(); it.hasNext();) {
      t = (H2dProjetType) it.next();
      if (t.refluxId_.equals(_inpId)) {
        return t;
      }
    }
    return null;
  }

  private String refluxId_;

  H2dProjetType() {
    this(CtuluLibString.EMPTY_STRING, CtuluLibString.EMPTY_STRING);
  }

  /**
   * @param _nom le nom
   * @param _refluxId l'identifiant des fichiers inp.
   */
  public H2dProjetType(final String _nom, final String _refluxId) {
    super(_nom);
    refluxId_ = _refluxId;
  }

  /**
   * @return l'identifiant des fichiers inp.
   */
  public String getRefluxId() {
    return refluxId_;
  }


  public DodicoEnumType[] getArray() {
    return getConstantArray();
  }
}