/*
 * @creation 10 janv. 2005
 * @modification $Date: 2007-06-11 13:06:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import com.memoire.bu.BuValueValidator;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleUnique;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: H2dNodalPropertiesMngAbstract.java,v 1.3 2007-06-11 13:06:08 deniger Exp $
 */
public abstract class H2dNodalPropertiesMngAbstract implements H2dNodalPropertiesMngI {
  private static class PermanentList extends CtuluPermanentList {
    /**
     * Initialisation avec les cles de varModel_.
     */
    public PermanentList(final Map _varModel) {
      super(CtuluLibArray.sort(_varModel.keySet().toArray()));
    }

    protected void superAddAll(final Map _varModel) {
      superClear();
      final Object[] o = _varModel.keySet().toArray();
      super.superAddAll(Arrays.asList(CtuluLibArray.sort(o)));
    }

    @Override
    protected void superClear() {
      super.superClear();
    }
  }

  /**
   * Les propri�t�s.
   *
   * @author Fred Deniger
   * @version $Id: H2dNodalPropertiesMngAbstract.java,v 1.3 2007-06-11 13:06:08 deniger Exp $
   */
  public class H2dNodalPropertyItem extends CtuluArrayDouble {
    transient H2dVariableType var_;

    /**
     * @param _var la variables
     * @param _values les valeurs correspondantes
     */
    public H2dNodalPropertyItem(final H2dVariableType _var, final CtuluCollectionDouble _values) {
      super(_values);
      if (_values.getSize() != getGrid().getPtsNb()) {
        throw new IllegalArgumentException("must contain " + getGrid().getPtsNb() + " items");
      }
      var_ = _var;
    }

    /**
     * @param _var la variable
     * @param _capacity la capacit� du tableau
     */
    public H2dNodalPropertyItem(final H2dVariableType _var) {
      super(getGrid().getPtsNb());
      var_ = _var;
    }

    @Override
    protected void fireObjectChanged(int _indexGeom, Object _newValue) {
      fireChanged(var_);
    }
  }

  /**
   * @param _var la variable
   * @return le validateur non null.
   */
  public static BuValueValidator getValueValidator(final H2dVariableType _var) {
    if (_var == H2dVariableType.COEF_FROTTEMENT_FOND || _var == H2dVariableType.HAUTEUR_EAU
        || _var == H2dVariableType.COEF_FROTTEMENT) {
      return H2DLib.POS;
    }
    return BuValueValidator.DOUBLE;
  }

  final EfGridInterface grid_;
  Map varModel_;
  transient PermanentList vars_;

  protected void clearVarList() {
    if (vars_ != null) {
      vars_.superClear();
    }
  }

  @Override
  public int getVariablesBlockLength() {
    return -1;
  }

  /**
   * @param _nbNodes le nombre de noeuds
   */
  public H2dNodalPropertiesMngAbstract(final EfGridInterface _nbNodes) {
    grid_ = _nbNodes;
    if (grid_ == null) {
      throw new IllegalArgumentException();
    }
  }

  protected abstract void fireChanged(final H2dVariableType _var);

  protected final void fireVarAdded(final H2dVariableType _var) {
    clearVarList();
    fireVarAddedProcess(_var);
  }

  protected abstract void fireVarAddedProcess(final H2dVariableType _var);

  protected abstract void fireVarRemovedProcess(final H2dVariableType _var);

  protected final void fireVarRemoved(final H2dVariableType _var) {
    clearVarList();
    fireVarRemovedProcess(_var);
  }

  /**
   * @param _var la variable a tester
   * @return true si la variable _t est utilisee par ce modele
   */
  @Override
  public boolean containsValuesFor(final H2dVariableType _var) {
    return varModel_ != null && varModel_.containsKey(_var);
  }

  /**
   * @return les variables definies
   */
  @Override
  public List getAllVariables() {
    if (vars_ == null) {
      if (varModel_ != null) {
        vars_ = new PermanentList(varModel_);
      }
    } else if (vars_.size() != varModel_.size()) {
      vars_.superAddAll(varModel_);
    }
    return vars_ == null ? Collections.EMPTY_LIST : vars_;
  }

  /**
   * @param _var la variable
   * @return le modele correspondant ou null si aucun;
   */
  @Override
  public CtuluCollectionDoubleEdit getModifiableModel(final H2dVariableType _var) {
    if (varModel_ == null) {
      return null;
    }
    return (CtuluCollectionDoubleEdit) varModel_.get(_var);
  }

  /**
   * @return les propri�t�s qui ne sont pas des propri�t�s nodales
   */
  /*
   * public Set getNodalProperties(){ Set r = new HashSet(); List init = getAllVariableList(); int nb = init.size(); for
   * (int i = 0; i < nb; i++) { H2dVariableType t = (H2dVariableType) init.get(i); if (!isSIProperties(t)) r.add(t); }
   * return r; }
   */
  public CtuluCollectionDouble getCollectionForUnique(final double _v) {
    return new CtuluCollectionDoubleUnique(_v, getGrid().getPtsNb());
  }

  /**
   * @param _key la variable a ajouter
   * @param _values la valeurs initiales
   * @param _cmd le receveur de commandes
   */
  @Override
  public void initValues(final H2dVariableType _key, final double _values, final CtuluCommandContainer _cmd) {
    initValues(_key, getCollectionForUnique(_values), _cmd);
  }

  /**
   * Permet d'initialisater une variable : sans evt et sans undo.
   *
   * @param _key la variable a initialiser
   * @param _values les valeurs
   */
  public void initValues(final H2dVariableType _key, final CtuluCollectionDouble _values) {
    if (!containsValuesFor(_key)) {
      if (varModel_ == null) {
        varModel_ = new HashMap(5);
      }
      varModel_.put(_key, new H2dNodalPropertyItem(_key, _values));
    }
  }

  @Override
  public void initValues(final H2dVariableType _key, final double[] _values, final CtuluCommandContainer _cmd) {
    initValues(_key, new CtuluArrayDouble(_values), _cmd);
  }

  /**
   * @param _key la variable a ajouter
   * @param _values la valeurs initiales
   * @param _cmd le receveur de commandes
   */
  public void initValues(final H2dVariableType _key, final CtuluCollectionDouble _values,
                         final CtuluCommandContainer _cmd) {
    if (!containsValuesFor(_key)) {
      if (varModel_ == null) {
        varModel_ = new HashMap(5);
      }
      final H2dNodalPropertyItem item = new H2dNodalPropertyItem(_key, _values);
      varModel_.put(_key, item);
      if (_cmd != null) {
        _cmd.addCmd(createUndoCmd(_key, _values));
      }
      fireVarAdded(_key);
    }
  }

  /**
   * @param _key
   * @param _values
   */
  private CtuluCommand createUndoCmd(final H2dVariableType _key, final CtuluCollectionDouble _values) {
    final CtuluArrayDouble copy = new CtuluArrayDouble(_values.getValues());
    return new CtuluCommand() {
      @Override
      public void redo() {
        final H2dNodalPropertyItem it2 = new H2dNodalPropertyItem(_key, copy);
        varModel_.put(_key, it2);
        fireVarAdded(_key);
      }

      @Override
      public void undo() {
        varModel_.remove(_key);
        fireVarRemoved(_key);
      }
    };
  }

  /**
   * @param _var la variable
   * @return true si variables correspondant a une solution initiale connue
   */
  public boolean isSIProperties(final H2dVariableType _var) {
    return _var == H2dVariableType.VITESSE_U || _var == H2dVariableType.VITESSE_V
        || _var == H2dVariableType.HAUTEUR_EAU;
  }

  /**
   * @param _key la variable a enlever
   * @param _cmd le receveur de commandes
   */
  @Override
  public void removeValues(final H2dVariableType _key, final CtuluCommandContainer _cmd) {
    if (varModel_ != null && containsValuesFor(_key)) {
      final H2dNodalPropertyItem old = (H2dNodalPropertyItem) varModel_.remove(_key);
      if (_cmd != null) {
        _cmd.addCmd(new CtuluCommand() {
          @Override
          public void redo() {
            removeValues(_key, null);
            fireVarRemoved(_key);
          }

          @Override
          public void undo() {
            varModel_.put(_key, old);
            fireVarAdded(_key);
          }
        });
      }
      fireVarRemoved(_key);
    }
  }

  @Override
  public H2dVariableProviderInterface getVariableDataProvider() {
    return isEmpty() ? null : this;
  }

  @Override
  public Collection getUsableVariables() {
    return getAllVariables();
  }

  @Override
  public EfGridInterface getGrid() {
    return grid_;
  }

  @Override
  public BuValueValidator getValidator(final H2dVariableType _t) {
    return getValueValidator(_t);
  }

  @Override
  public CtuluNumberFormatI getFormater(final H2dVariableType _t) {
    return null;
  }

  @Override
  public H2dVariableType[] getVarToModify() {
    final List l = getAllVariables();
    final H2dVariableType[] var = new H2dVariableType[l.size()];
    l.toArray(var);
    return var;
  }

  @Override
  public boolean containsVarToModify() {
    final List l = getAllVariables();
    return l != null && l.size() > 0;
  }

  @Override
  public final boolean isElementVar() {
    return false;
  }

  public void initalize(final Map _varValues) {
    for (final Iterator it = _varValues.entrySet().iterator(); it.hasNext(); ) {
      final Map.Entry e = (Map.Entry) it.next();
      final H2dVariableType t = (H2dVariableType) e.getKey();
      final Object o = e.getValue();
      CtuluCollectionDouble d = null;
      if (o instanceof CtuluCollectionDouble) {
        d = ((CtuluCollectionDouble) o);
      } else if (o instanceof double[]) {
        d = new CtuluArrayDouble((double[]) e.getValue());
      }
      if (d != null && d.getSize() == grid_.getPtsNb() && !containsValuesFor(t)) {
        initValues(t, d);
      }
    }
  }

  /**
   * @param _var la variables
   * @param _idx les indices a modifier
   * @param _values la valeur pour tous les indices
   * @param _cmd le receveur de commande
   */
  public void setValues(final H2dVariableType _var, final int[] _idx, final double _values,
                        final CtuluCommandContainer _cmd) {
    final CtuluCollectionDoubleEdit model = getModifiableModel(_var);
    if (model != null) {
      model.set(_idx, _values, _cmd);
    }
  }

  /**
   * @param _var la variables
   * @param _idx les indices a modifier
   * @param _values les valeurs correspondants aux indices : tableau de meme taille
   * @param _cmd le receveur de commande
   */
  public void setValues(final H2dVariableType _var, final int[] _idx, final double[] _values,
                        final CtuluCommandContainer _cmd) {
    final CtuluCollectionDoubleEdit model = getModifiableModel(_var);
    if (model != null) {
      model.set(_idx, _values, _cmd);
    }
  }

  protected final Map getCurrentVarValues() {
    if (varModel_ == null) {
      varModel_ = new HashMap();
    }
    return varModel_;
  }

  /**
   * Une copie de la table.
   */
  @Override
  public Map getVarValues() {
    return varModel_ == null ? null : new HashMap(varModel_);
  }

  @Override
  public CtuluCollectionDouble getViewedModel(final H2dVariableType _t) {
    return getModifiableModel(_t);
  }

  @Override
  public CtuluCollectionDouble getValues(CtuluVariable _var) {
    return getViewedModel((H2dVariableType) _var);
  }

  /**
   * @return true si vide
   */
  @Override
  public boolean isEmpty() {
    return varModel_ == null || varModel_.size() == 0;
  }
}
