/*
 * @creation 7 juin 2004
 * @modification $Date: 2007-06-14 11:59:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import gnu.trove.TIntDoubleHashMap;
import gnu.trove.TIntObjectHashMap;
import org.fudaa.dodico.h2d.H2dNodalPropertyMixte;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

/**
 * @author Fred Deniger
 * @version $Id: H2dRefluxElementImposedProperty.java,v 1.13 2007-06-14 11:59:08 deniger Exp $
 */
public class H2dRefluxElementImposedProperty extends H2dRefluxElementProperty {

  private final double defaultValue_ = 0;

  /**
   * @param _m
   */
  protected H2dRefluxElementImposedProperty(final H2dNodalPropertyMixte _m, final int _nbElt) {
    super(_m, _nbElt);
  }


  public H2dRefluxElementImposedProperty(final String _nom, final int _nbElt) {
    super(_nom, _nbElt);
    final int[] idx = new int[_nbElt];
    for (int i = 0; i < idx.length; i++) {
      idx[i] = i;
    }
    final H2dRefluxValue val = new H2dRefluxValue();
    val.setType(H2dBcType.PERMANENT);
    val.setValue(defaultValue_);
    super.setValue(idx, val);
  }

  /**
   * @param _nom
   * @param _indiceFixeValeur
   * @param _indiceTransitoireEvolution
   */
  public H2dRefluxElementImposedProperty(final String _nom, final TIntDoubleHashMap _indiceFixeValeur,
      final TIntObjectHashMap _indiceTransitoireEvolution) {
    super(_nom, _indiceFixeValeur, _indiceTransitoireEvolution);
  }

  /**
   * @param _indiceFixeValeur
   * @param _indiceTransitoireEvolution
   */
  public H2dRefluxElementImposedProperty(final TIntDoubleHashMap _indiceFixeValeur,
      final TIntObjectHashMap _indiceTransitoireEvolution) {
    super(_indiceFixeValeur, _indiceTransitoireEvolution);
  }

  @Override
  public void fillWithValue(final int _s, final H2dRefluxValue _v) {
    if ((indicePermanentValeur_ != null) && indicePermanentValeur_.contains(_s)) {
      _v.setAllValues(H2dBcType.PERMANENT, indicePermanentValeur_.get(_s), null);
    } else if ((indiceTransitoireEvolution_ != null) && indiceTransitoireEvolution_.contains(_s)) {
      _v.setAllValues(H2dBcType.TRANSITOIRE, 0, (EvolutionReguliereAbstract) indiceTransitoireEvolution_.get(_s));
    } else {
      _v.setAllValues(H2dBcType.PERMANENT, defaultValue_, null);
    }
  }

  @Override
  public double getPermanentValueFor(final int _i) {
    return (indicePermanentValeur_ == null ? defaultValue_ : indicePermanentValeur_.get(_i));
  }

  @Override
  public H2dBcType getTypeForEltIdx(final int _i) {
    final H2dBcType r = super.getTypeForEltIdx(_i);
    if (r == H2dBcType.LIBRE) {
      return H2dBcType.PERMANENT;
    }
    return r;
  }

  @Override
  public final boolean isFreeAllowed() {
    return false;
  }

}