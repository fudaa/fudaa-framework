/*
 * @creation 9 juin 2004
 * 
 * @modification $Date: 2007-02-02 11:21:18 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarBcTypeList.java,v 1.8 2007-02-02 11:21:18 deniger Exp $
 */
public final class H2dRubarBcTypeList {

  private static CtuluPermanentList list;

  /**
   * Les variables permanentes. Attention cette liste est egalement utilisee par les type de bord
   */
  public final static CtuluPermanentList TRANSIENT_VAR = new CtuluPermanentList(CtuluLibArray.sort(new Object[] { H2dVariableType.DEBIT_NORMAL,
      H2dVariableType.DEBIT_TANGENTIEL, H2dVariableType.COTE_EAU, H2dVariableTransType.APPORT_PLUIE, H2dVariableType.DEBIT_M3, H2dVariableType.VENT }));

  /**
   * Les variables cas transport permanentes. Attention cette liste est egalement utilisee par les type de bord
   */
  public final static CtuluPermanentList TRANSIENT_VAR_TRANSPORT = new CtuluPermanentList(CtuluLibArray.sort(new Object[] {
      H2dVariableType.DEBIT_NORMAL, H2dVariableType.DEBIT_TANGENTIEL, H2dVariableType.COTE_EAU, H2dVariableTransType.CONCENTRATION,
      H2dVariableTransType.DIAMETRE, H2dVariableTransType.ETENDUE, H2dVariableTransType.APPORT_PLUIE, H2dVariableType.DEBIT_M3, H2dVariableType.VENT }));

  /**
   * rubar =4.
   */
  public final static H2dRubarBoundaryType COTE_EAU = new H2dRubarBoundaryDefaultType(H2dResource.getS("Cote d'eau impos�e"), 4,
      new Object[] { H2dVariableType.COTE_EAU }, new Object[] { H2dVariableType.COTE_EAU });

  /**
   * rubar =5.
   */
  public final static H2dRubarBoundaryType COTE_EAU_DEBIT_TANGENTIELLE = new H2dRubarBoundaryDefaultType(H2dResource.getS("Cote impos�e en entr�e"),
      5, TRANSIENT_VAR, TRANSIENT_VAR_TRANSPORT) {

    @Override
    public boolean isTorrentielVar(H2dVariableType _t) {
      return _t == H2dVariableType.DEBIT_NORMAL;
    }
  };

  /**
   * rubar =7.
   */
  public final static H2dRubarBoundaryType CRITIQUE = new H2dRubarBoundaryDefaultType(H2dResource.getS("R�gime critique"), 7);

  /**
   * rubar =3.
   */
  public final static H2dRubarBoundaryType DEBIT = new H2dRubarBoundaryFlowrateType(createDbName(), 3);

  private static String createDbName() {
    return H2dResource.getS("D�bit impos�");
  }

  private static String createGroupeName(final int _i) {
    String idx = to2DigitsString(_i);
    return H2dResource.getS("D�bit impos�") + CtuluLibString.ESPACE + H2dResource.getS("groupe") + " " + idx;
  }

  public static String to2DigitsString(final int _i) {
    return _i < 10 ? "0" + _i : Integer.toString(_i);
  }

  private static String createTarageGroupeName(final int _i) {
    String idx = to2DigitsString(_i);
    return H2dResource.getS("Loi de tarage") + CtuluLibString.ESPACE + H2dResource.getS("groupe") + " " + idx;
  }

  private final static H2dRubarBoundaryFlowrateGroupType[] FLOWRATE_GROUPS;
  private final static H2dRubarBoundaryTarageGroupType[] TARAGE_GROUPS;

  static {
    int nbFlowate = 29;
    int nbTarage = 29;
    int firstIndice = 31;
    FLOWRATE_GROUPS = new H2dRubarBoundaryFlowrateGroupType[nbFlowate];
    for (int i = 0; i < nbFlowate; i++) {
      FLOWRATE_GROUPS[i] = new H2dRubarBoundaryFlowrateGroupType(createGroupeName(i + 1), firstIndice + i);
    }
    TARAGE_GROUPS = new H2dRubarBoundaryTarageGroupType[nbTarage];
    firstIndice = 61;
    for (int i = 0; i < nbFlowate; i++) {
      TARAGE_GROUPS[i] = new H2dRubarBoundaryTarageGroupType(createTarageGroupeName(i + 1), firstIndice + i);
    }

  }

  /**
   * rubar =1.
   */
  public final static H2dRubarBoundaryType LIBRE = new H2dRubarBoundaryDefaultType(H2dResource.getS("Libre"), 1);

  /**
   * Type utilise pour caracteriser un groupement d'arete ayant des types diff�rents.
   */
  public final static H2dRubarBoundaryType MIXTE = new H2dRubarBoundaryDefaultType(H2dResource.getS("Mixte"), true, -100, null, null);

  /**
   * Type de bord solid rubar=2.
   */
  public final static H2dRubarBoundaryType SOLIDE = new H2dRubarBoundaryDefaultType(H2dResource.getS("Solide"), true, 2, null, null);

  /**
   * rubar =6.
   */
  public final static H2dRubarBoundaryType TARAGE = new H2dRubarBoundaryDefaultType(H2dResource.getS("Loi de tarage"), false, 6, null, null);

  /**
   * @param _l la liste a parcourir
   * @return recupere les type "groupe d�bit" dans la liste
   */
  public static H2dRubarBoundaryFlowrateGroupType[] getDebitGroupType(final List _l) {
    final List l = new ArrayList(_l.size());
    for (final Iterator it = _l.iterator(); it.hasNext();) {
      final H2dRubarBoundaryType t = (H2dRubarBoundaryType) it.next();
      if (t.isTypeDebitGlobal()) {
        l.add(t);
      }
    }
    final H2dRubarBoundaryFlowrateGroupType[] r = new H2dRubarBoundaryFlowrateGroupType[l.size()];
    l.toArray(r);
    return r;
  }

  public static H2dRubarBoundaryTarageGroupType[] getTarageGroupType() {
    final H2dRubarBoundaryTarageGroupType[] r = new H2dRubarBoundaryTarageGroupType[TARAGE_GROUPS.length];
    System.arraycopy(TARAGE_GROUPS, 0, r, 0, TARAGE_GROUPS.length);
    return r;
  }

  /**
   * @param _idx l'indice a tester
   * @return le type ayant le meme indice 'rubar'. null si aucun
   */
  public static H2dRubarBoundaryType getExternType(final int _idx) {
    final CtuluPermanentList listTmp = getList();
    H2dRubarBoundaryType t;
    for (final Iterator it = listTmp.iterator(); it.hasNext();) {
      t = (H2dRubarBoundaryType) it.next();
      if (_idx == t.getRubarIdx()) {
        return t;
      }
    }
    return null;
  }

  /**
   * @return la liste des types support�s par rubar
   */
  public static CtuluPermanentList getList() {
    if (list == null) {
      list = new CtuluPermanentList(CtuluLibArray.sort(getConstantArray()));
    }
    return list;
  }

  public static H2dRubarBoundaryType[] getConstantArray() {
    List<H2dRubarBoundaryType> types = new ArrayList<H2dRubarBoundaryType>(70);
    types.add(SOLIDE);
    types.add(LIBRE);
    types.add(COTE_EAU);
    types.add(COTE_EAU_DEBIT_TANGENTIELLE);
    types.add(CRITIQUE);
    types.add(DEBIT);
    types.addAll(Arrays.asList(FLOWRATE_GROUPS));
    types.add(TARAGE);
    types.addAll(Arrays.asList(TARAGE_GROUPS));
    return (H2dRubarBoundaryType[]) types.toArray(new H2dRubarBoundaryType[types.size()]);
  }

  /**
   *
   */
  private H2dRubarBcTypeList() {
    super();
  }

}