/*
 * @creation 29 nov. 2004
 * 
 * @modification $Date: 2007-04-30 14:21:35 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import gnu.trove.TIntHashSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandCompositeInverse;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.dico.DicoCasFileFormatVersionAbstract;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.dico.DicoEntite.Vecteur;
import org.fudaa.dodico.dico.DicoMultipleUndoCommands;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.dico.DicoParamsLinkedSource;
import org.fudaa.dodico.dico.DicoParamsListener;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * Manager tracers.
 *
 * @author Fred Deniger
 * @version $Id: H2dTelemacSourceMng.java,v 1.18 2007-04-30 14:21:35 deniger Exp $
 */
public class H2dTelemacTracerMng implements DicoParamsListener, DicoParamsLinkedSource {

  private DicoParams dico_;
  List<H2dTelemacTracerLinkedManager> linkedManager = new ArrayList<H2dTelemacTracerLinkedManager>();
  private Vecteur tracerInitialesValues;
  DicoEntite.Vecteur traceurNames;
  private DicoEntite.Simple traceurNombre_;

  public void addLinkedManager(H2dTelemacTracerLinkedManager manager) {
    linkedManager.add(manager);
  }

  /**
   * @param _params les parameters dico permettant d'initialiser les valeurs
   */
  public H2dTelemacTracerMng(final DicoParams _params) {
    dico_ = _params;
    initMotCle();
    final CtuluAnalyze a = new CtuluAnalyze();
    firstUpdateFromDico(a);
    if (a.containsErrors()) {
      a.printResume();
    }
    dico_.addModelListener(this);
    dico_.addLinkedSource(this);
  }

  public void addTracer(String name, String initialValueNoTested, final CtuluCommandContainer _cmd) {
    if (traceurNombre_ == null) {
      return;
    }
    int newSize = getNbTraceurs() + 1;
    if (name == null) {
      return;
    }
    String initialValue = CtuluLibString.isEmpty(initialValueNoTested) ? CtuluLibString.ZERO : initialValueNoTested;
    CtuluCommandComposite cmp = new CtuluCommandCompositeInverse();
    dico_.setValue(traceurNombre_, Integer.toString(newSize), cmp);
    String[] names = traceurNames.getValues(dico_.getValue(traceurNames));
    names[names.length - 1] = name;
    dico_.setValue(traceurNames, traceurNames.getStringValuesArray(names), cmp);

    String[] initialValues = tracerInitialesValues.getValues(dico_.getValue(tracerInitialesValues));
    initialValues[initialValues.length - 1] = initialValue;
    dico_.setValue(tracerInitialesValues, tracerInitialesValues.getStringValuesArray(initialValues), cmp);

    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }

  }

  private CtuluCommand createUpdateValideStatesCommand() {
    return new CtuluCommand() {

      @Override
      public void redo() {
        updateValidationStates();

      }

      @Override
      public void undo() {
        updateValidationStates();

      }
    };
  }

  @Override
  public void dicoParamsEntiteAdded(DicoParams _cas, DicoEntite _ent) {
    // TODO Auto-generated method stub
  }

  @Override
  public void dicoParamsEntiteCommentUpdated(final DicoParams _cas, final DicoEntite _ent) {
  }

  @Override
  public void dicoParamsEntiteRemoved(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
    if (traceurNombre_ == null) {
      return ;
    }
    //we manage the cases if tracer are required (number >0).
    if (_ent == tracerInitialesValues) {
      if (isTracerUsed()) {
        dico_.setValue(_ent, _oldValue);
      }
    }
  }

  @Override
  public void dicoParamsEntiteUpdated(final DicoParams _cas, final DicoEntite _ent, final String _oldValue) {
  }

  public void dicoParamsProjectModifyStateChanged(final DicoParams _cas) {
  }

  public void dicoParamsStateLoadedEntiteChanged(final DicoParams _cas, final DicoEntite _ent) {
  }

  @Override
  public void dicoParamsValidStateEntiteUpdated(final DicoParams _cas, final DicoEntite _ent) {
  }

  @Override
  public void dicoParamsVersionChanged(final DicoParams _cas) {
  }

  private void firstUpdateFromDico(final CtuluAnalyze _analyze) {
    if(traceurNombre_==null){//pas de traceurs pour ce logiciel.
      return;
    }
    updateTracersNameAndInitialValuesInDico(null);
  }

  public DicoParams getDico() {
    return dico_;
  }

  public int getNbTraceurs() {
    if (traceurNombre_ == null) {
      return 0;
    }
    return Integer.parseInt(dico_.getValue(traceurNombre_));
  }

//  private String getStringForSource(final String _init, final int _srcIdx) {
//    return new StringBuffer(_init.length() + 10).append(_init).append(CtuluLibString.ESPACE).append(H2dResource.getS("source")).append(
//            CtuluLibString.ESPACE).append((_srcIdx == 1) ? (CtuluLibString.UN) : (CtuluLibString.DEUX)).toString();
//  }

  public Vecteur getTracerInitialesValuesDicoEntite() {
    return tracerInitialesValues;
  }

  public String getTracerInitialValuesForDico() {
    if (traceurNombre_ == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    final int nbTraceursUsed = getNbTraceurs();
    String[] initialValues = null;
    if (dico_.isValueSetFor(tracerInitialesValues)) {
      initialValues = traceurNames.getValues(dico_.getValue(tracerInitialesValues));
    }
    if (initialValues == null) {
      initialValues = new String[0];
    }
    if (initialValues.length != nbTraceursUsed) {
      String[] newInitialValues = new String[nbTraceursUsed];
      int nb = Math.min(newInitialValues.length, initialValues.length);
      for (int i = 0; i < nb; i++) {
        newInitialValues[i] = initialValues[i];
      }
      for (int i = nb; i < newInitialValues.length; i++) {
        newInitialValues[i] = CtuluLibString.ZERO;
      }
      return tracerInitialesValues.getStringValuesArray(newInitialValues);
    }
    return dico_.getValue(tracerInitialesValues);
  }

  public DicoEntite.Vecteur getTracerNameDicoEntite() {
    return traceurNames;
  }

  public String[] getTracerNames() {
    String[] names = null;
//    final int nbTraceursUsed = getNbTraceurs();
    if (dico_.isValueSetFor(traceurNames)) {
      names = traceurNames.getValues(dico_.getValue(traceurNames));
    }
    if (names == null) {
      names = new String[0];
    }
    return names;
  }

  private String getTracerNamesForDico() {
    if (traceurNombre_ == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    String[] names = getTracerNames();
    final int nbTraceursUsed = getNbTraceurs();
    if (names.length != nbTraceursUsed) {
      String[] newNames = new String[nbTraceursUsed];
      int nb = Math.min(newNames.length, names.length);
      for (int i = 0; i < nb; i++) {
        newNames[i] = names[i];
      }
      for (int i = nb; i < newNames.length; i++) {
        newNames[i] = H2dResource.getS("Traceur {0}", CtuluLibString.getString(i + 1));
      }
      return traceurNames.getStringValuesArray(newNames);
    }
    return dico_.getValue(traceurNames);
  }

  public String getTraceurName(int idxTraceur) {
    if (traceurNombre_ == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    String[] values = traceurNames.getValues(dico_.getValue(traceurNames));
    if (values != null && idxTraceur >= 0 && idxTraceur < values.length) {
      return values[idxTraceur];
    }
    return CtuluLibString.EMPTY_STRING;
  }

//  private double getValueFromArray(final String[] _tab, final int _i) {
//    if (_tab == null || _i >= _tab.length) {
//      return 0;
//    }
//    return Double.parseDouble(_tab[_i]);
//
//  }
//
//  private String[] getValueFromDico(final DicoEntite.Vecteur _v, final int _nbDef, final CtuluAnalyze _analyze) {
//    if (dico_.isValueSetFor(_v)) {
//      final String[] r = _v.getValues(dico_.getValue(_v));
//      if (_nbDef > 0 && r.length != _nbDef && _analyze != null) {
//        _analyze.addError(H2dResource.getS("Incoh�rence dans les mot-cl�s d�finissant les siphons"), -1);
//      }
//      return r;
//    }
//    return null;
//  }
  private void initMotCle() {
    final DicoCasFileFormatVersionAbstract model = dico_.getDicoFileFormatVersion();
    tracerInitialesValues = (DicoEntite.Vecteur) model.getEntiteFor(
            new String[]{"VALEURS INITIALES DES TRACEURS", "INITIAL VALUES OF TRACERS"});
    traceurNames = (DicoEntite.Vecteur) model.getEntiteFor(new String[]{"NOMS DES TRACEURS", "NAMES OF TRACERS"});
    traceurNombre_ = (DicoEntite.Simple) model.getEntiteFor(new String[]{"NOMBRE DE TRACEURS", "NUMBER OF TRACERS"});
  }

  private boolean isStringAppliable(final String _s) {
    return _s != null && _s.trim().length() > 0;
  }

  public boolean isTracerUsed() {
    return getNbTraceurs() > 0;

  }

  /**
   * @param _ent le mot-cle a tester
   * @return true si la valeur correspondante est cree
   */
  public boolean isValueSet(final DicoEntite _ent) {
    return dico_.isValueSetFor(_ent);
  }

  @Override
  public CtuluCommand keywordAddedOrUpdated(DicoEntite _ent, String oldValue, String newValue) {
    if (traceurNombre_ == null) {
      return null;
    }
    if (_ent == traceurNombre_) {
      CtuluCommandComposite cmp = new CtuluCommandCompositeInverse();
      cmp.addCmd(createUpdateValideStatesCommand());
      updateTracersNameAndInitialValuesInDico(cmp);
      for (H2dTelemacTracerLinkedManager it : linkedManager) {
        it.nbTracerChanged(cmp);
      }
      return cmp.getSimplify();
    }
    return null;
  }

  @Override
  public CtuluCommand keywordRemoved(DicoEntite _ent) {
    if (traceurNombre_ == null) {
      return null;
    }
    if (_ent == traceurNombre_) {
      CtuluCommandComposite cmp = new CtuluCommandCompositeInverse();
      cmp.addCmd(createUpdateValideStatesCommand());
      //only to update required property.
      dico_.removeValue(tracerInitialesValues, cmp);
      dico_.removeValue(traceurNames, cmp);

      for (H2dTelemacTracerLinkedManager it : linkedManager) {
        it.nbTracerChanged(cmp);
      }
      return cmp.getSimplify();
    }
    return null;
  }

  public void removeTracers(int[] idx, final CtuluCommandContainer _cmd) {
    if (traceurNombre_ == null) {
      return;
    }
    TIntHashSet idxToAvoid = new TIntHashSet();
    int nbTraceurs = getNbTraceurs();
    for (int i = 0; i < idx.length; i++) {
      int index = idx[i];
      if (index >= 0 && index < nbTraceurs) {
        idxToAvoid.add(index);
      }
    }
    int newSize = nbTraceurs - idxToAvoid.size();
    CtuluCommandComposite cmp = new CtuluCommandComposite();

    List<String> newNames = new ArrayList<String>();
    List<String> newInitialValues = new ArrayList<String>();
    String[] initNames = traceurNames.getValues(dico_.getValue(traceurNames));
    String[] initInitialValues = tracerInitialesValues.getValues(dico_.getValue(tracerInitialesValues));
    for (int traceurIdx = 0; traceurIdx < nbTraceurs; traceurIdx++) {
      if (!idxToAvoid.contains(traceurIdx)) {
        newNames.add(initNames[traceurIdx]);
        newInitialValues.add(initInitialValues[traceurIdx]);
      }
    }
    String newInitialesValues = tracerInitialesValues.getStringValue(newInitialValues);
    String newNamesOnSource = traceurNames.getStringValue(newNames);
    Map<DicoEntite, String> newValues = new HashMap<DicoEntite, String>();
    newValues.put(tracerInitialesValues, newInitialesValues);
    newValues.put(traceurNames, newNamesOnSource);
    DicoMultipleUndoCommands undo = new DicoMultipleUndoCommands(newValues, dico_);
    for (H2dTelemacTracerLinkedManager it : linkedManager) {
      it.shiftTracersValues(idx, cmp);
    }
    dico_.setValue(traceurNombre_, Integer.toString(newSize), cmp);
    dico_.setValue(traceurNames, newNamesOnSource, null);
    dico_.setValue(tracerInitialesValues, newInitialesValues, null);
    cmp.addCmd(undo);
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }

  }

//  private boolean setTraceur(final H2dTelemacSource _s, final double[] _d) {
//    int nbTraceur = getNbTraceurs();
//    if (nbTraceur == 0) {
//      _s.setTraceurs(null);
//    } else {
//      double[] vals = new double[nbTraceur];
//      if (_d != null) {
//        int nb = Math.min(_d.length, vals.length);
//        for (int i = 0; i < nb; i++) {
//          vals[i] = _d[i];
//        }
//      }
//      _s.setTraceurs(vals);
//    }
//    return true;
//  }
  private void updateTracersNameAndInitialValuesInDico() {
    updateTracersNameAndInitialValuesInDico(null);

  }

  private void updateTracersNameAndInitialValuesInDico(CtuluCommandContainer cmd) {
    updateValidationStates();
    //we check the content.
    dico_.setValue(traceurNames, getTracerNamesForDico(), cmd);
    dico_.setValue(tracerInitialesValues, getTracerInitialValuesForDico(), cmd);

  }

  private void updateValidationStates() {
    final int nbTraceursUsed = getNbTraceurs();
    traceurNames.setRequired(nbTraceursUsed > 0);
    tracerInitialesValues.setRequired(nbTraceursUsed > 0);
    traceurNames.setNbElem(nbTraceursUsed);
    tracerInitialesValues.setNbElem(nbTraceursUsed);
    dico_.testIfValid(traceurNames);
    dico_.testIfValid(tracerInitialesValues);
  }
}