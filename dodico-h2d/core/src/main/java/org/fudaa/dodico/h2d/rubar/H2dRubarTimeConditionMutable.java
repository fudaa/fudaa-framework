/*
 * @creation 24 juin 2004
 * @modification $Date: 2007-05-22 13:11:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliere;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarTimeConditionMutable.java,v 1.15 2007-05-22 13:11:25 deniger Exp $
 */
public class H2dRubarTimeConditionMutable implements H2dRubarTimeConditionInterface {
  /**
   * l'evolution h.
   */
  public EvolutionReguliere h_;
  /**
   * l'evolution qn.
   */
  public EvolutionReguliere qn_;
  /**
   * l'evolution qt.
   */
  public EvolutionReguliere qt_;
  public List<H2DRubarTimeConcentrationMutableBlock> concentrationBlocks;
  /**
   * true si transitoire.
   */
  public boolean trans_;

  /**
   * @param _nb le nombre de pas de temps
   * @param nbConcentration the number of concentration blocks
   */
  public H2dRubarTimeConditionMutable(final int _nb, final int nbConcentration) {
    trans_ = nbConcentration > 0;
    h_ = new EvolutionReguliere(_nb);
    qn_ = new EvolutionReguliere(_nb);
    qt_ = new EvolutionReguliere(_nb);
    if (nbConcentration > 0) {
      concentrationBlocks = new ArrayList<>(nbConcentration);
      for (int i = 0; i < nbConcentration; i++) {
        concentrationBlocks.add(new H2DRubarTimeConcentrationMutableBlock(_nb));
      }
    }
  }

  @Override
  public int getNbConcentrationBlock() {
    return concentrationBlocks == null ? 0 : concentrationBlocks.size();
  }

  /**
   * @param _m tableau variableType->Evolution
   */
  public H2dRubarTimeConditionMutable(final Map<H2dVariableType, EvolutionReguliere> _m) {
    if (_m != null) {
      h_ = _m.get(H2dVariableType.COTE_EAU);
      qn_ = _m.get(H2dVariableType.DEBIT_NORMAL);
      qt_ = _m.get(H2dVariableType.DEBIT_TANGENTIEL);
    }
  }

  public void addValueDiametre(int idxBlock, double t, double value) {
    concentrationBlocks.get(idxBlock).diametre.add(t, value);
  }

  public void addValueEtendue(int idxBlock, double t, double value) {
    concentrationBlocks.get(idxBlock).etendue.add(t, value);
  }

  public void addValueConcentration(int idxBlock, double t, double value) {
    concentrationBlocks.get(idxBlock).concentration.add(t, value);
  }

  @Override
  public EvolutionReguliere getDiametreEvolution(int blockIdx) {
    return concentrationBlocks != null && concentrationBlocks.size() > blockIdx ? concentrationBlocks.get(blockIdx).diametre : null;
  }

  @Override
  public EvolutionReguliere getEtendueEvolution(int blockIdx) {
    return concentrationBlocks != null && concentrationBlocks.size() > blockIdx ? concentrationBlocks.get(blockIdx).etendue : null;
  }

  @Override
  public EvolutionReguliere getConcentrationEvolution(int blockIdx) {
    return concentrationBlocks != null && concentrationBlocks.size() > blockIdx ? concentrationBlocks.get(blockIdx).concentration : null;
  }

  @Override
  public boolean isTrans() {
    return trans_;
  }

  @Override
  public EvolutionReguliere getHEvol() {
    return h_;
  }

  @Override
  public EvolutionReguliere getQnEvol() {
    return qn_;
  }

  @Override
  public EvolutionReguliere getQtEvol() {
    return qt_;
  }
}
