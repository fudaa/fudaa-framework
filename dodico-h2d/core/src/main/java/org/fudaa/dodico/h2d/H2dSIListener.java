/*
 * @creation     10 janv. 2005
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: H2dSIListener.java,v 1.4 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dSIListener {

  /**
   * @param _var la variables dont les valeurs ont ete modifiees.
   */
  void siChanged(H2dVariableType _var);

  /**
   * @param _var la variable ajoutee
   */
  void siAdded(H2dVariableType _var);

  /**
   * @param _var la variable enlevee
   */
  void siRemoved(H2dVariableType _var);

}
