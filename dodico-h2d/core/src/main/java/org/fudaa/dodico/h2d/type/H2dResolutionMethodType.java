/*
 * @creation 27 juin 2003
 * @modification $Date: 2007-06-29 15:10:31 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.dodico.commun.DodicoEnumType;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author deniger
 * @version $Id: H2dResolutionMethodType.java,v 1.18 2007-06-29 15:10:31 deniger Exp $
 */
public final class H2dResolutionMethodType extends DodicoEnumType {

  /**
   * Methode de resolution lineaire.
   */
  public final static H2dResolutionMethodType LINEAIRE = new H2dResolutionMethodType(H2dResource.getS("Lin�aire"),
      "LINEAIRE", null);
  /**
   * Methode de resolution de newton raphson.
   */
  public final static H2dResolutionMethodType NEWTON_RAPHSON = new H2dResolutionMethodType(H2dResource
      .getS("Newton Raphson"), "NEWTON", new H2dVariableType[] { H2dVariableType.RELAXATION,
      H2dVariableTimeType.PRECISION_METHOD, H2dVariableTimeType.NB_MAX_IT_METHOD });
  public final static H2dResolutionMethodType LAXWEN = new H2dResolutionMethodType(H2dResource
      .getS("LaxWendroff NE PAS UTILISER BETA VERSION"), "LAXWEN", new H2dVariableType[] { H2dVariableType.RELAXATION,
      H2dVariableTimeType.PRECISION_METHOD, H2dVariableTimeType.NB_MAX_IT_METHOD });
  public final static H2dResolutionMethodType LAXWEN_BCD = new H2dResolutionMethodType(H2dResource
      .getS("LaxWendroff banc c/d NE PAS UTILISER BETA VERSION"), "LAXWEN_BCD", new H2dVariableType[] {
      H2dVariableType.RELAXATION, H2dVariableTimeType.PRECISION_METHOD, H2dVariableTimeType.NB_MAX_IT_METHOD,
      H2dVariableType.VISCOSITE, H2dVariableType.PRECISION_BCD_METHOD });
  /**
   * Methode de resolution de newton raphson banc couvrant/decouvrant.
   */
  public final static H2dResolutionMethodType NEWTON_RAPHSON_BCD = new H2dResolutionMethodType(H2dResource
      .getS("Newton Raphson banc c/d"), "NEWTON_BCD", new H2dVariableType[] { H2dVariableType.RELAXATION,
      H2dVariableTimeType.PRECISION_METHOD, H2dVariableTimeType.NB_MAX_IT_METHOD, H2dVariableType.VISCOSITE,
      H2dVariableType.PRECISION_BCD_METHOD });
  /**
   * Methode de resolution de select lumping.
   */
  public final static H2dResolutionMethodType SELECT_LUMPING = new H2dResolutionMethodType(H2dResource
      .getS("Select Lumping"), "SELUMP", null);
  /**
   * Methode de resolution de select lumping banc couvrant/decouvrant.
   */
  public final static H2dResolutionMethodType SELECT_LUMPING_BCD = new H2dResolutionMethodType(H2dResource
      .getS("Select Lumping banc c/d"), "SELUMP_BCD", new H2dVariableType[] { H2dVariableType.VISCOSITE,
      H2dVariableType.PRECISION_BCD_METHOD });
  /**
   * La liste des methode de resolution.
   */
  public static final List LIST = new CtuluPermanentList(CtuluLibArray.sort(new H2dResolutionMethodType[] { LINEAIRE,
      NEWTON_RAPHSON, NEWTON_RAPHSON_BCD, SELECT_LUMPING, SELECT_LUMPING_BCD, LAXWEN, LAXWEN_BCD }));

  public static H2dResolutionMethodType[] getConstantArray() {
    return new H2dResolutionMethodType[] { LINEAIRE, NEWTON_RAPHSON, NEWTON_RAPHSON_BCD, SELECT_LUMPING,
        SELECT_LUMPING_BCD, LAXWEN,LAXWEN_BCD };
  }

  private String refluxId_;
  private CtuluPermanentList var_;

  /**
   * @param _nom le nom de la methode
   * @param _refluxId l'identifiant reflux : fichier inp.
   * @param _var les variables associees
   */
  public H2dResolutionMethodType(final String _nom, final String _refluxId, final H2dVariableType[] _var) {
    super(_nom);
    refluxId_ = _refluxId;
    if (_var != null) {
      var_ = new CtuluPermanentList(CtuluLibArray.sort(_var));
    }
  }

  H2dResolutionMethodType() {
    this(CtuluLibString.EMPTY_STRING, CtuluLibString.EMPTY_STRING, null);
  }

  /**
   * @param _id l'identifiant reflux : fichier inp.
   * @return la methode correspondante presente dans la liste (LIST) si null si aucune.
   */
  public static H2dResolutionMethodType getTypeForRefluxId(final String _id) {
    H2dResolutionMethodType t;
    for (final Iterator it = LIST.iterator(); it.hasNext();) {
      t = (H2dResolutionMethodType) it.next();
      if (t.refluxId_.equals(_id)) {
        return t;
      }
    }
    return null;
  }

  /**
   * @return l'identifiant reflux : fichier inp.
   */
  public String getRefluxId() {
    return refluxId_;
  }

  /**
   * @return les variables associees
   */
  public CtuluPermanentList getVarList() {
    return var_;
  }

  /**
   * @param _t la variable a tester
   * @return true si supportee par cette methode
   */
  public boolean isUsed(final H2dVariableType _t) {
    return (var_ != null) && (Collections.binarySearch(var_, _t) >= 0);
  }

  public DodicoEnumType[] getArray() {
    return getConstantArray();
  }

  /**
   * @return true si la variable viscosite doit etre definie.
   */
  public boolean needViscosity() {
    return isUsed(H2dVariableType.VISCOSITE);
  }
}