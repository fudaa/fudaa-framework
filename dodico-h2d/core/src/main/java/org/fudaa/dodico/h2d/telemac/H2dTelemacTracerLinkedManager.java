package org.fudaa.dodico.h2d.telemac;

import org.fudaa.ctulu.CtuluCommandContainer;

public interface H2dTelemacTracerLinkedManager {

  void nbTracerChanged(CtuluCommandContainer cmp);

  /**
   * don't change the value number but shift left the values:
   * before:
   * [value1,value2,value3,value4]
   * 
   * call with idx= 1 ->
   * [value1,value3,value4,<default-value>]
   * 
   * @param idx
   * @param _cmd
   */
  void shiftTracersValues(int[] idx, final CtuluCommandContainer _cmd);

}
