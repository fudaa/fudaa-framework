/*
 * @creation 26 juin 2003
 * @modification $Date: 2007-06-29 15:10:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.h2d.reflux;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.dico.DicoParams;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EFGridArrayZ;
import org.fudaa.dodico.h2d.*;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dProjetType;
import org.fudaa.dodico.h2d.type.H2dResolutionSchemaType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionListenerDispatcher;
import org.fudaa.dodico.mesure.EvolutionListenerDispatcherDefault;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

/**
 * Classe contenant les parametres de base. Notation: cl: condition limite pn: propriete nodale.
 * 
 * @author deniger
 * @version $Id: H2dRefluxParameters.java,v 1.48 2007-06-29 15:10:28 deniger Exp $
 */
public final class H2dRefluxParameters extends H2dParameters {

  /**
   * La liste des comportements NON LIBRE autorises pour les conditions limites.
   */
  public static final CtuluPermanentList CL_SET_TYPE = new CtuluPermanentList(CtuluLibArray.sort(new H2dBcType[] {
      H2dBcType.PERMANENT, H2dBcType.TRANSITOIRE }));

  /**
   * La liste des comportements autorises pour les conditions limites.
   */
  public static final CtuluPermanentList CL_TYPE = new CtuluPermanentList(CtuluLibArray.sort(new H2dBcType[] {
      H2dBcType.LIBRE, H2dBcType.PERMANENT, H2dBcType.TRANSITOIRE }));

  /**
   * Initialise les parametres.
   * 
   * @param _maillage le maillage
   * @param _type le type du projet
   * @param _prog la barre de progression
   * @param _ft le format
   * @param _analyze le receveur d'erreur
   * @param _l le listener
   * @return le projet correctement initialise
   */
  public static H2dRefluxParameters init(final EfGridInterface _maillage, final H2dProjetType _type,
      final ProgressionInterface _prog, final H2dRefluxDicoVersion _ft, final CtuluAnalyze _analyze,
      final H2dRefluxProjectDispatcherListener _l) {
    final DicoParams params = new DicoParams(null, _ft);
    final H2dRefluxParameters r = new H2dRefluxParameters(params, _type);
    r.setMaillage(_maillage, _prog, _analyze);

    r.setClManager(H2dRefluxBcManager.init(_maillage));
    if (r.clMng_ == null) {
      return null;
    }
    r.propElem_ = H2dRefluxElementPropertyMngAbstract.createElementPropMng(r, null, r.maillage_.getEltNb(), _analyze);
    r.propNodal_ = new H2dRefluxNodalPropertiesMng(r);
    r.gptTemps_ = new H2dTimeStepGroup[] { H2dTimeStepGroup.createDefaultStationnaire() };
    r.setListener(_l);
    _l.projectTypeChanged();
    r.getMaillage().createIndexRegular(_prog);
    return r;
  }

  /**
   * @param _source les sources issues de la lecture d'un fichier inp.
   * @param _prog
   * @param _ft
   * @param _analyze
   * @param _si
   * @param _l
   * @return les parametres initialises
   */
  public static H2dRefluxParameters init(final H2dRefluxSourceInterface _source,
      final H2dRefluxSolutionInitAdapterInterface _si, final ProgressionInterface _prog,
      final H2dRefluxDicoVersion _ft, final CtuluAnalyze _analyze, final H2dRefluxProjectDispatcherListener _l) {
    if (_prog != null) {
      _prog.setDesc(H2dResource.getS("Analyse donn�es"));
      _prog.setProgression(0);
    }
    if (_ft == null) {
      _analyze.addFatalError(H2dResource.getS("Version non sp�cifi�e"));
      return null;
    }
    final DicoParams params = new DicoParams(_source.getEntiteValue(), null, _ft);
    final H2dRefluxParameters r = new H2dRefluxParameters(params, _source.getTypeProjet());
    final EvolutionListenerDispatcher listener = r.getProjectDispatcher();
    r.setMaillage(_source.getGrid(), _prog, _analyze);
    if (r.getMaillage() == null) {
      _analyze.addFatalError(H2dResource.getS("Maillage non sp�cifi�"));
      return null;
    }
    // Classement de cl
    // Classement de bord
    final H2dRefluxBcManager bords = H2dRefluxBcManager.init(_source, _analyze);
    if (_prog != null) {
      _prog.setProgression(60);
    }
    r.setClManager(bords);
    // sauv des prop elementaires
    r.propElem_ = H2dRefluxElementPropertyMngAbstract.createElementPropMng(r, _source.getPropElementaires(),
        r.maillage_.getEltNb(), _analyze);
    r.propNodal_ = new H2dRefluxNodalPropertiesMng(r);
    r.propNodal_.init(H2dRefluxNodalPropertiesMng.getRadiationVar(), _source.getRadiationResult(), _analyze, null);
    r.propNodal_.init(H2dRefluxNodalPropertiesMng.getVentVar(), _source.getVentResult(), _analyze, null);
    // sauv groupe pas de temps
    r.gptTemps_ = _source.getGroupePasTemps();
    if (_prog != null) {
      _prog.setProgression(80);
    }
    final Map s = r.getUsedEvolutionVariables();
    if (s != null) {
      final Map evolEquivEvol = new HashMap(s.size());
      final Map varBaseEvol = new HashMap();
      // Set baseEvolu= new HashSet(s.size());
      boolean foundEquiv;
      for (final Iterator it = s.entrySet().iterator(); it.hasNext();) {
        final Map.Entry e = (Map.Entry) it.next();
        final EvolutionReguliereAbstract eToCompare = (EvolutionReguliereAbstract) e.getKey();
        eToCompare.setListener(listener);
        final H2dVariableType v = (H2dVariableType) e.getValue();
        Set baseEvolu = (Set) varBaseEvol.get(v);
        if (baseEvolu == null) {
          baseEvolu = new HashSet(s.size());
          varBaseEvol.put(v, baseEvolu);
        }
        foundEquiv = false;
        for (final Iterator itBase = baseEvolu.iterator(); (itBase.hasNext()) && (!foundEquiv);) {
          final EvolutionReguliereAbstract eBase = (EvolutionReguliereAbstract) itBase.next();
          if (eToCompare.isEquivalentTo(eBase)) {
            foundEquiv = true;
            evolEquivEvol.put(eToCompare, eBase);
          }
        }
        if (!foundEquiv) {
          baseEvolu.add(eToCompare);
        }
      }
      if (evolEquivEvol.size() > 0) {
        r.clMng_.replaceEvol(evolEquivEvol);
        r.propElem_.replaceEvol(evolEquivEvol);
      }
    }
    // pour eviter d'avoir un pas de temps negatif au debut
    r.beginTime_ = Math.max(_source.getTimeBeginInForInpFile(), 0);
    r.getMaillage().createIndexRegular(_prog);

    // solution initiales
    r.setSolutionInitiales(_si);
    r.setListener(_l);
    return r;
  }

  private H2dRefluxBcManager clMng_;

  private H2dRefluxProjectDispatcherListener listener_;

  private H2dProjetType projetType_;

  private H2dRefluxSolutionInitialeInterface solutionInitiales_;

  double beginTime_;

  EvolutionListenerDispatcher evolutionDispatcher_;

  H2dTimeStepGroup[] gptTemps_;

  EfGridInterface maillage_;

  H2dRefluxElementPropertyMngAbstract propElem_;

  H2dRefluxNodalPropertiesMng propNodal_;

  private H2dRefluxParameters(final DicoParams _p, final H2dProjetType _t) {
    super(_p);
    projetType_ = _t;
  }

  H2dRefluxDicoVersion getDicoVersion() {
    return (H2dRefluxDicoVersion) super.dicoParams_.getDicoFileFormatVersion();
  }

  protected void fireProjectTypeChanged() {
    if (listener_ != null) {
      listener_.projectTypeChanged();
    }
  }
  
  public void fireNodeXYChanged(){
    if (listener_ != null) {
      listener_.nodeXYChanged();
    }
  }

  protected void fireTimeStepChanged() {
    if (listener_ != null) {
      listener_.timeStepChanged();
    }
  }

  protected void guessBeginTimeForTrans() {
    beginTime_ = 0;
    if (gptTemps_[0].getSchema() == H2dResolutionSchemaType.STATIONNAIRE) {
      return;
    }
    final Set evol = getUsedEvol();
    if (evol.size() > 0) {
      final EvolutionReguliereAbstract e = (EvolutionReguliereAbstract) evol.toArray()[0];
      beginTime_ = e.getMinX() - gptTemps_[0].getValeurPasTemps();
      if (beginTime_ < 0) {
        beginTime_ = 0;
      }
    }

  }

  protected void setClManager(final H2dRefluxBcManager _clMng) {
    if ((clMng_ != _clMng) && (_clMng != null)) {
      if (listener_ != null) {
        _clMng.removeClListener(listener_);
      }
      clMng_ = _clMng;
      if (listener_ != null) {
        clMng_.addClListener(listener_);
      }
    }
  }

  /**
   * @param _solutionInitiales The solutionInitiales to set.
   */
  protected void setSolutionInitiales(final H2dRefluxSolutionInitAdapterInterface _solutionInitiales) {
    if (_solutionInitiales == null) {
      return;
    }
    if (solutionInitiales_ == null) {
      final double[] h = new double[maillage_.getPtsNb()];
      final double[] u = new double[maillage_.getPtsNb()];
      final double[] v = new double[maillage_.getPtsNb()];
      for (int i = h.length - 1; i >= 0; i--) {
        h[i] = _solutionInitiales.getCoteEau(i);
        u[i] = _solutionInitiales.getU(i);
        v[i] = _solutionInitiales.getV(i);
      }
      solutionInitiales_ = new H2dRefluxSICourant2D(u, v, h, maillage_);
      if (listener_ != null) {
        solutionInitiales_.addListener(listener_);
      }
    }
  }

  /**
   * @return true si contient des cl transitoires.
   */
  public boolean contientClTransitoire() {
    return clMng_.containsClTransient();
  }

  /**
   * @return true si contient des prop nodales transitoires.
   */
  public boolean contientPnTransitoire() {
    return clMng_.containsPnTransient() || propElem_.containsPnTransient();
  }

  @Override
  public H2dGridDataAdapter createGridDataAdapter() {
    final List l = new ArrayList(4);
    H2dVariableProviderInterface prov = getNodalData();
    if (prov != null) {
      l.add(prov);
    }
    prov = new H2dRefluxVarEltProvider(this, true);
    l.add(prov);
    prov = getSiData();
    if (prov != null) {
      l.add(prov);
    }
    return new H2dGridDataAdapter(l);
  }

  @Override
  public CtuluPermanentList getAllowedBordType() {
    return clMng_.getAllowedBordTypeList();
  }

  /**
   * @return le manager des frontieres/cl.
   */
  public H2dBcManagerMiddleInterface getBcManager() {
    return clMng_;
  }

  /**
   * @return le temps initiale (prob reflux)
   */
  public double getBeginTimeForTrans() {
    return beginTime_;
  }

  @Override
  public H2dVariableProviderInterface getElementData() {
    return new H2dRefluxVarEltProvider(this, false);
  }

  /**
   * @return le manager des proprietes elementaires.
   */
  public H2dRefluxElementPropertyMngAbstract getElementPropMng() {
    return propElem_;
  }

  /**
   * @return the the available project types
   */
  public H2dProjetType[] getEnableChangeProjectType() {
    if ((projetType_ == H2dProjetType.COURANTOLOGIE_2D) || (projetType_ == H2dProjetType.COURANTOLOGIE_2D_LMG)) {
      return new H2dProjetType[] { H2dProjetType.COURANTOLOGIE_2D, H2dProjetType.COURANTOLOGIE_2D_LMG };
    }
    return null;
  }

  /**
   * @param _i l'indice du groupe demande
   * @return une copie du groupe demande
   */
  public H2dTimeStepGroup getGroupePasTemps(final int _i) {
    return new H2dTimeStepGroup(gptTemps_[_i]);
  }

  /**
   * @return le nombre de groupe de pas de temps
   */
  public int getGroupePasTempsNb() {
    return gptTemps_.length;
  }

  /**
   * @return un tableau contenant les copies des groupes tempo.
   */
  public H2dTimeStepGroup[] getGroupePasTempsTab() {
    return H2dTimeStepGroup.cloneTimeStep(gptTemps_);
  }

  /**
   * @return Returns the listener.
   */
  public H2dRefluxProjectDispatcherListener getListener() {
    return listener_;
  }

  @Override
  public EfGridInterface getMaillage() {
    return maillage_;
  }

  @Override
  public H2dProjectDispatcherListener getMainListener() {
    return listener_;
  }

  /**
   * @return le dernier pas de temps qui sera utilise par le code reflux
   */
  public double getMaxTime() {
    return H2dTimeStepGroup.getMaxTime(beginTime_, gptTemps_);
  }

  /**
   * @return le premier pas de temps qui sera utilise par le code reflux
   */
  public double getMinTime() {
    return H2dTimeStepGroup.getMinTime(beginTime_, gptTemps_);
  }

  @Override
  public H2dVariableProviderInterface getNodalData() {
    return propNodal_;
  }

  @Override
  public EvolutionListenerDispatcher getProjectDispatcher() {
    if (evolutionDispatcher_ == null) {
      evolutionDispatcher_ = new EvolutionListenerDispatcherDefault();
    }
    return evolutionDispatcher_;
  }

  /**
   * @return le tuype de projet en cours
   */
  public H2dProjetType getProjetType() {
    return projetType_;
  }

  public H2dRefluxNodalPropertiesMng getPropNodal() {
    return propNodal_;
  }

  /**
   * @return les prop elementaires dans l'ordre demande par les fichiers inp.
   */
  public H2dNodalPropertyMixte[] getPropElemArray() {
    return propElem_.getPropertiesInInpOrder();
  }

  /**
   * @return le manager pour les conditions limites
   */
  public H2dRefluxBcManager getRefluxClManager() {
    return clMng_;
  }

  @Override
  public H2dVariableProviderInterface getSiData() {
    return getSolutionInitiales().getVariableDataProvider();
  }

  /**
   * @return Returns the solutionInitiales.
   */
  public H2dRefluxSolutionInitialeInterface getSolutionInitiales() {
    return solutionInitiales_;
  }

  @Override
  public List getTransientVar() {
    final List r = new ArrayList(10);
    r.addAll(propElem_.getVarList());
    clMng_.fillWithTransientVar(r);
    return r;
  }

  /**
   * @return une map des evolutions utilisees
   */
  public Set getUsedEvol() {
    final Set evol = new HashSet();
    clMng_.fillWithUsedEvolution(evol);
    propElem_.fillWithUsedEvolution(evol);
    return evol;
  }

  @Override
  public Map getUsedEvolutionVariables() {
    final H2dEvolutionVariableMap evol = new H2dEvolutionVariableMap();
    clMng_.fillWithEvolVar(evol);
    propElem_.fillWithEvolVar(evol);
    return evol.getVarEvol();
  }

  public void initilialiseSolutionInitiale() {
    if (solutionInitiales_ == null) {
      final double[] nullArray = new double[maillage_.getPtsNb()];
      solutionInitiales_ = new H2dRefluxSICourant2D(nullArray, nullArray, nullArray, maillage_);
      if (listener_ != null) {
        solutionInitiales_.addListener(listener_);
      }
    }
  }

  /**
   * @return true if the project type can be modified
   */
  public boolean isProjectTypeModifiable() {
    return (projetType_ == H2dProjetType.COURANTOLOGIE_2D) || (projetType_ == H2dProjetType.COURANTOLOGIE_2D_LMG);
  }

  /**
   * @return true si les solutions initiales ont ete initialisee
   */
  public boolean isSolutionInitialSet() {
    return solutionInitiales_ != null;
  }

  /**
   * Envoie un evt si modif.
   * 
   * @param _t le nouveau temps initial.
   */
  public void setBeginTimeForTrans(final double _t) {
    if (_t != beginTime_) {
      fireTimeStepChanged();
      beginTime_ = _t;
      // test surement inutile.
      if (beginTime_ < 0) {
        beginTime_ = 0;
      }
    }
  }

  /**
   * @param _l le nouveau listener
   */
  public void setListener(final H2dRefluxProjectDispatcherListener _l) {
    if (_l == null) {
      return;
    }
    if (listener_ != null) {
      if (dicoParams_ != null) {
        dicoParams_.removeModelListener(listener_);
      }
      if (clMng_ != null) {
        clMng_.removeClListener(listener_);
      }
      propNodal_.removeListener(listener_);
      propElem_.removeListener(listener_);
      if (solutionInitiales_ != null) {
        solutionInitiales_.removeListener(_l);
      }
      if (evolutionDispatcher_ != null) {
        evolutionDispatcher_.removeEvolutionListener(listener_);
      }
      if (solutionInitiales_ != null) {
        solutionInitiales_.removeListener(listener_);
      }
    }
    clMng_.addClListener(_l);
    propElem_.addListener(_l);
    propNodal_.addListener(_l);
    dicoParams_.addModelListener(_l);
    if (solutionInitiales_ != null) {
      solutionInitiales_.addListener(_l);
    }
    if (evolutionDispatcher_ != null) {
      evolutionDispatcher_.addEvolutionListener(_l);
    }
    listener_ = _l;
  }

  /**
   * Determine si necessaire les frontieres du maillage. Le maillage ne peut etre initialise qu'une seule fois.
   * 
   * @param _m le nouveau maillage . Ne peut etre modifie qu'une seule fois.
   * @param _inter la barre de progression
   * @param _analyze les messages
   */
  public void setMaillage(final EfGridInterface _m, final ProgressionInterface _inter, final CtuluAnalyze _analyze) {
    if (maillage_ != null) {
      if (_analyze != null) {
        _analyze.addFatalError(H2dResource.getS("Maillage d�j� sp�cifi�"));
      }
      new Throwable().printStackTrace();
      return;
    }
    if (_m == null || _m.getEltNb() == 0) {
      _analyze.addFatalError(H2dResource.getS("Le maillage semble mal construit"));
      return;
    }
    maillage_ = new EFGridArrayZ(_m);
    if (maillage_.getFrontiers() == null) {
      maillage_.computeBord(_inter, _analyze);
    }
  }

  /**
   * @param _type le nouveau type du projet
   */
  public void setProjetType(final H2dProjetType _type) {
    if ((_type != projetType_) && (isProjectTypeModifiable())
        && ((_type == H2dProjetType.COURANTOLOGIE_2D) || (_type == H2dProjetType.COURANTOLOGIE_2D_LMG))) {
      projetType_ = _type;
      propElem_.setProjectType(projetType_);
      fireTimeStepChanged();
    }
  }

  /**
   * @param _new les nouveaux groupes
   * @return commande si modif, null sinon.
   */
  public CtuluCommand setTimeGroups(final H2dTimeStepGroup[] _new) {
    if (_new == null) {
      return null;
    }
    if (!H2dTimeStepGroup.isSame(_new, gptTemps_)) {
      final H2dTimeStepGroup[] old = gptTemps_;
      final H2dTimeStepGroup[] newT = H2dTimeStepGroup.cloneTimeStep(_new);
      gptTemps_ = newT;
      fireTimeStepChanged();
      return new CtuluCommand() {

        @Override
        public void redo() {
          gptTemps_ = newT;
          fireTimeStepChanged();
        }

        @Override
        public void undo() {
          gptTemps_ = old;
          fireTimeStepChanged();
        }
      };
    }
    return null;
  }

}
