/*
 * @creation 17 nov. 2003
 * @modification $Date: 2006-10-24 12:48:36 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import org.fudaa.ctulu.CtuluParser;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

/**
 * Cette classe permet de connaitre la valeur d'un parametre et surtout de determiner les "valeurs" communes dans une
 * selection. Ainsi, si la selection comporte des valeurs transitoire le type sera transitoire. Si les courbes de la
 * selection sont identiques, le champ evolution pointera vers cette evolution commun ( sinon l'evolution null sera
 * utilisee). De meme pour la cas permanent: pour ce cas un boolean est utilise pour determiner si la valeur est bien
 * constante ou non sur la selection.
 *
 * @author deniger
 * @version $Id: H2dRefluxValue.java,v 1.15 2006-10-24 12:48:36 deniger Exp $
 */
public class H2dRefluxValue {

  boolean isDoubleValueConstant_;
  private double value_;
  private H2dBcType type_;
  private EvolutionReguliereAbstract evolution_;
  CtuluParser oldExpr_;

  /**
   * @return l'expression utilisee pour modifier les valeurs
   */
  public final CtuluParser getOldExpr() {
    return oldExpr_;
  }

  /**
   * @param _oldExpr l'expression utilisee pour modifier les valeurs
   */
  public final void setOldExpr(final CtuluParser _oldExpr) {
    oldExpr_ = _oldExpr;
  }

  public H2dRefluxValue() {
    super();
    setAllValues(H2dBcType.LIBRE, 0, null, true);
  }

  /**
   * @param _v la valeur servant a l'initialisation
   */
  public H2dRefluxValue(final H2dRefluxValue _v) {
    if (_v == null) {
      setAllValues(H2dBcType.LIBRE, 0, null, true);
    } else {
      initFrom(_v);
    }
  }

  /**
   * @param _t le type
   * @param _v la valeur fixe
   * @param _evol la courbe transitoire
   * @param _isPermanentValueConstant true si la valeur permanent est constant
   */
  public H2dRefluxValue(final H2dBcType _t, final double _v, final EvolutionReguliereAbstract _evol, final boolean _isPermanentValueConstant) {
    setAllValues(_t, _v, _evol, _isPermanentValueConstant);
  }

  /**
   * @param _t le type
   * @param _v la valeur fixe
   * @param _evol la courbe transitoire
   */
  public H2dRefluxValue(final H2dBcType _t, final double _v, final EvolutionReguliereAbstract _evol) {
    setAllValues(_t, _v, _evol, true);
  }

  /**
   * Compare two values container and put the changes in the first argument.
   *
   * @param _set la valeur a modifier
   * @param _temp la valeur reference
   * @return true si modif
   */
  public static boolean compareTwoValueAndIsDiff(final H2dRefluxValue _set, final H2dRefluxValue _temp) {
    if (!_set.equalsValue(_temp)) {
      if (_set.getType() != _temp.getType()) {
        _set.setType(H2dBcType.MIXTE);
        return true;
      } else if ((_temp.getType() == H2dBcType.PERMANENT) && (_set.isDoubleValueConstant())
          && (_set.getValue() != _temp.getValue())) {
        _set.setDoubleValueConstant(false);
        return true;
      } else if ((_temp.getType() == H2dBcType.TRANSITOIRE) && (_set.isEvolutionFixed())
          && (_set.getEvolution() != _temp.getEvolution())) {
        _set.setEvolution(null);
        return true;
      }
    }
    return false;
  }

  protected final void initFrom(final H2dRefluxValue _v) {
    if (_v != null) {
      setAllValues(_v.type_, _v.value_, _v.evolution_, _v.isDoubleValueConstant_);
    }
  }

  protected void setAllValues(final H2dBcType _t, final double _value, final EvolutionReguliereAbstract _evol) {
    setType(_t);
    setValue(_value);
    setEvolution(_evol);
  }

  protected final void setAllValues(final H2dBcType _t, final double _value, final EvolutionReguliereAbstract _evol, final boolean _constant) {
    setType(_t);
    setValue(_value);
    setEvolution(_evol);
    isDoubleValueConstant_ = _constant;
  }

  /**
   * @return L'evolution commmune. Mixte si pas commune.
   */
  public EvolutionReguliereAbstract getEvolution() {
    return evolution_;
  }

  /**
   * @return Le type commun. Mixte si pas commun.
   */
  public H2dBcType getType() {
    return type_;
  }

  /**
   * @return la valeur permanente
   */
  public double getValue() {
    return value_;
  }

  @Override
  public int hashCode() {
    // calcul du hashcode
    int r = 13;
    final long l = Double.doubleToLongBits(value_);
    r += l ^ (l >> 32);
    if (evolution_ != null) {
      r += evolution_.hashCode();
    }
    if (isDoubleValueConstant_) {
      r++;
    }
    return r;
  }

  @Override
  public boolean equals(final Object _o) {
    return (_o instanceof H2dRefluxValue) ? equalsValue((H2dRefluxValue) _o) : false;
  }

  /**
   * @param _v la valeur a comparer
   * @return si tous les parametres sont egaux. Les pointeurs des evolutions sont compares.
   */
  public boolean equalsValue(final H2dRefluxValue _v) {
    if (_v != null) {
      return (_v.type_ == type_) && (_v.value_ == value_) && (_v.evolution_ == evolution_)
          && (_v.isDoubleValueConstant_ == isDoubleValueConstant_);
    }
    return false;
  }

  protected void setEvolution(final EvolutionReguliereAbstract _evolution) {
    if (type_ == H2dBcType.TRANSITOIRE) {
      evolution_ = _evolution;
    }
  }

  protected final void setType(final H2dBcType _type) {
    if (_type == H2dBcType.PERMANENT) {
      evolution_ = null;
      type_ = _type;
    } else if (_type == H2dBcType.TRANSITOIRE) {
      value_ = 0;
      type_ = _type;
    } else if (_type == H2dBcType.LIBRE) {
      value_ = 0;
      type_ = _type;
      evolution_ = null;
    } else if ((_type == null) || (_type == H2dBcType.MIXTE)) {
      value_ = 0;
      type_ = H2dBcType.MIXTE;
      evolution_ = null;
    } else {
      new Throwable(_type + " not supported").printStackTrace();
    }
  }

  protected void setValue(final double _d) {
    if (type_ == H2dBcType.PERMANENT) {
      value_ = _d;
    }
  }

  /**
   * @return true si le type est connu et non Mixte.
   */
  public boolean isTypeDetermined() {
    return (type_ != null) && (type_ != H2dBcType.MIXTE);
  }

  /**
   * @return true si type permanent et si la valeur est bien constante
   */
  public boolean isValueFixed() {
    return isDoubleValueConstant_ && (getType() == H2dBcType.PERMANENT);
  }

  /**
   * @return true si l'evolution est non null et le type est transitoire
   */
  public boolean isEvolutionFixed() {
    return (getEvolution() != null) && (getType() == H2dBcType.TRANSITOIRE);
  }

  /**
   * @return true si la valeur permanent est constant. Utiliser pour plusieurs valeurs.
   */
  public boolean isDoubleValueConstant() {
    return isDoubleValueConstant_;
  }

  protected void setDoubleValueConstant(final boolean _b) {
    isDoubleValueConstant_ = _b;
    if (!isDoubleValueConstant_) {
      value_ = 0;
    }
  }

  /**
   * @param _vToModify la valeur qui sera modifiee
   * @param _vToTest la valeur reference
   */
  public static void computeCommonValue(final H2dRefluxValue _vToModify, final H2dRefluxValue _vToTest) {
    if (!_vToModify.equalsValue(_vToTest)) {
      if (_vToModify.getType() == _vToTest.getType()) {
        if ((_vToModify.getType() == H2dBcType.PERMANENT) && (_vToModify.isDoubleValueConstant())
            && (_vToModify.getValue() != _vToTest.getValue())) {
          _vToModify.setDoubleValueConstant(false);
        } else if ((_vToModify.getType() == H2dBcType.TRANSITOIRE) && (_vToModify.isEvolutionFixed())
            && (_vToModify.getEvolution() != _vToTest.getEvolution())) {
          _vToModify.setEvolution(null);
        }
      } else {
        _vToModify.setType(H2dBcType.MIXTE);
      }
    }
  }
}