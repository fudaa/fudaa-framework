/*
 * @creation 27 nov. 2003
 * @modification $Date: 2007-04-30 14:21:37 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandCompositeInverse;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.dodico.h2d.H2DNodalPropertyMixteChangeListener;
import org.fudaa.dodico.h2d.H2dEvolutionUseCounter;
import org.fudaa.dodico.h2d.H2dNodalPropertyMixte;
import org.fudaa.dodico.h2d.type.H2dProjetType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author deniger
 * @version $Id: H2dRefluxElementPropertyMngAbstract.java,v 1.17 2007-04-30 14:21:37 deniger Exp $
 */
public abstract class H2dRefluxElementPropertyMngAbstract implements H2DNodalPropertyMixteChangeListener {

  /**
   * @param _p les parametres
   * @param _mixte les proprietes lues
   * @param _eltNb le nombre d'elts
   * @param _analyze le receveur d'erreur
   * @return le manager de proprietes elementaires.
   */
  public static H2dRefluxElementPropertyMngAbstract createElementPropMng(final H2dRefluxParameters _p,
      final H2dNodalPropertyMixte[] _mixte, final int _eltNb, final CtuluAnalyze _analyze) {
    return createElementPropMng(_p.getProjetType(), _mixte, _eltNb, _analyze);
  }

  /**
   * @param _p les type du projet
   * @param _mixte les proprietes lues
   * @param _eltNb le nombre d'elts
   * @param _analyze le receveur d'erreur
   * @return le manager de proprietes elementaires.
   */
  public static H2dRefluxElementPropertyMngAbstract createElementPropMng(final H2dProjetType _p,
      final H2dNodalPropertyMixte[] _mixte, final int _eltNb, final CtuluAnalyze _analyze) {
    if ((_p == H2dProjetType.COURANTOLOGIE_2D) || (_p == H2dProjetType.COURANTOLOGIE_2D_LMG)) {
      return new H2dRefluxElementPropertyCourant2dMng(_mixte, _p == H2dProjetType.COURANTOLOGIE_2D_LMG, _eltNb);
    }
    return null;
  }

  private final Map varNodalProp_;
  protected List listener_;
  protected int nbElt_;
  protected CtuluPermanentList var_;

  /**
   * @param _nbElt le nombre total d'element
   */
  public H2dRefluxElementPropertyMngAbstract(final int _nbElt) {
    varNodalProp_ = new TreeMap();
    nbElt_ = _nbElt;

  }

  protected void addProp(final H2dVariableType _t, final H2dRefluxElementProperty _p) {
    final Object o = varNodalProp_.put(_t, _p);
    if (o == _p) {
      return;
    }
    _p.setListener(this);
    if (o != null) {
      ((H2dRefluxElementProperty) o).setListener(null);
      fireEvent(_t);
    }
  }

  protected void fireEvent(final H2dVariableType _t) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dRefluxElementPropertyMngListener) listener_.get(i)).elementPropertyChanged(this, _t);
      }
    }
  }

  protected H2dRefluxElementProperty getProp(final H2dVariableType _t) {
    return (H2dRefluxElementProperty) varNodalProp_.get(_t);
  }

  protected H2dVariableType getVariable(final H2dNodalPropertyMixte _p) {
    if (varNodalProp_.containsValue(_p)) {
      for (final Iterator it = varNodalProp_.entrySet().iterator(); it.hasNext();) {
        final Map.Entry e = (Map.Entry) it.next();
        if (e.getValue() == _p) {
          return (H2dVariableType) e.getKey();
        }
      }
    }
    return null;
  }

  protected final void initVar() {
    var_ = new CtuluPermanentList(varNodalProp_.keySet().toArray());
  }

  protected void removeProp(final H2dVariableType _t) {
    final Object o = varNodalProp_.remove(_t);
    if (o != null) {
      ((H2dRefluxElementProperty) o).setListener(null);
      fireEvent(_t);
    }
  }

  protected void replaceEvol(final Map _evolEquivEvol) {
    for (final Iterator it = varNodalProp_.values().iterator(); it.hasNext();) {
      ((H2dRefluxElementProperty) it.next()).replaceEvol(_evolEquivEvol);
    }
  }

  protected boolean setProjectType(final H2dProjetType _t) {
    return false;
  }

  /**
   * @param _el le nouveau listener a ajouter
   */
  public void addListener(final H2dRefluxElementPropertyMngListener _el) {
    if (listener_ == null) {
      listener_ = new ArrayList(10);
    }
    listener_.add(_el);
  }

  /**
   * @return true si contient des prop nodales transitoire
   */
  public boolean containsPnTransient() {
    for (final Iterator it = varNodalProp_.values().iterator(); it.hasNext();) {
      if (((H2dRefluxElementProperty) it.next()).containsPnTransient()) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param _m la table a remplir avec les evolution
   */
  public void fillWithEvolVar(final H2dEvolutionUseCounter _m) {
    for (final Iterator it = varNodalProp_.entrySet().iterator(); it.hasNext();) {
      final Map.Entry e = (Map.Entry) it.next();
      final H2dRefluxElementProperty p = (H2dRefluxElementProperty) e.getValue();
      p.fillWithEvolVar(_m, (H2dVariableType) e.getKey());
    }
  }

  /**
   * @param _m la table a remplir avec les evolution
   */
  public void fillWithUsedEvolution(final Set _m) {
    for (final Iterator it = varNodalProp_.values().iterator(); it.hasNext();) {
      final H2dRefluxElementProperty p = (H2dRefluxElementProperty) it.next();
      p.fillWithUsedEvolution(_m);
    }
  }

  /**
   * @param _t la variable demandee
   * @return la prop elementaire correspondante ou null si aucune
   */
  public H2dRefluxElementProperty getEltProp(final H2dVariableType _t) {
    return (H2dRefluxElementProperty) varNodalProp_.get(_t);
  }

  /**
   * @return les variables qui doivent etre totalement imposees alors que tous les elt ne le sont pas
   */
  public H2dVariableType[] getEltPropNotAllSet() {
    final List r = new ArrayList();
    for (final Iterator it = varNodalProp_.entrySet().iterator(); it.hasNext();) {
      final Map.Entry e = (Map.Entry) it.next();
      final H2dRefluxElementProperty p = (H2dRefluxElementProperty) e.getValue();
      if ((!p.isFreeAllowed()) && (p.getNbValueFixedOrTransient() != nbElt_)) {
        r.add(e.getKey());
      }
    }
    if (r.size() == 0) {
      return null;
    }
    final H2dVariableType[] rf = new H2dVariableType[r.size()];
    r.toArray(rf);
    return rf;

  }

  /**
   * @return les prop dans l'ordre demande par les fichiers inp.
   */
  public abstract H2dNodalPropertyMixte[] getPropertiesInInpOrder();

  /**
   * Return for the selected idx the common values for variable type.
   * 
   * @param _selected les indices a parcourir
   * @return H2DVariableType->H2dRefluxValue
   */
  public SortedMap getVariableCommonValues(final int[] _selected) {
    final SortedMap r = new TreeMap();
    for (final Iterator it = varNodalProp_.entrySet().iterator(); it.hasNext();) {
      final Map.Entry e = (Map.Entry) it.next();
      final H2dVariableType v = (H2dVariableType) e.getKey();
      r.put(v, getEltProp(v).getCommonValue(_selected));
    }
    return r;
  }

  /**
   * @return les variables gerees
   */
  public CtuluPermanentList getVarList() {
    return var_;
  }

  @Override
  public void nodalPropertyMixteChanged(final H2dNodalPropertyMixte _source) {
    final H2dVariableType t = getVariable(_source);
    if (t != null) {
      fireEvent(t);
    }
  }

  /**
   * @param _new le nouveau type de projet
   * @return false
   */
  public boolean projectTypeCanBeChanged(final H2dProjetType _new) {
    return false;
  }

  /**
   * @param _el le listener a enlever
   */
  public void removeListener(final H2dRefluxElementPropertyMngListener _el) {
    if (listener_ != null) {
      listener_.remove(_el);
    }
  }

  /**
   * @param _selectedIdx les indices a modifier
   * @param _varValue les valeurs pour chaque variable (H2DVariable->H2dRefluxValue)
   * @return la commande si modif ou null si rien
   */
  public CtuluCommand setValues(final int[] _selectedIdx, final Map _varValue) {
    final CtuluCommandCompositeInverse r = new CtuluCommandCompositeInverse();
    for (final Iterator it = _varValue.entrySet().iterator(); it.hasNext();) {
      final Map.Entry e = (Map.Entry) it.next();
      final H2dRefluxElementProperty eltProp = getEltProp((H2dVariableType) e.getKey());
      if (eltProp != null) {
        r.addCmd(eltProp.setValue(_selectedIdx, (H2dRefluxValue) e.getValue()));
      }
    }
    return r.isEmpty() ? null : r;
  }

}