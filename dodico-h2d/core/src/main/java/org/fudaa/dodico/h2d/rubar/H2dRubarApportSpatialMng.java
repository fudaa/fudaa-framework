/*
 * @creation 14 d�c. 2004
 *
 * @modification $Date: 2007-05-22 13:11:25 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import gnu.trove.TIntObjectHashMap;
import gnu.trove.TObjectIntHashMap;
import gnu.trove.TObjectIntIterator;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormater;
import org.fudaa.ctulu.collection.CtuluArrayObject;
import org.fudaa.ctulu.fileformat.FortranLib;
import org.fudaa.dodico.h2d.H2dEvolutionUseCounter;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dVariableTransType;
import org.fudaa.dodico.mesure.EvolutionListener;
import org.fudaa.dodico.mesure.EvolutionReguliere;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarApportSpatialMng.java,v 1.14 2007-05-22 13:11:25 deniger Exp $
 */
public class H2dRubarApportSpatialMng implements EvolutionListener {
  private class ApportModelObject extends CtuluArrayObject {
    /**
     * @param _nb
     */
    public ApportModelObject(final int _nb) {
      super(_nb);
    }

    @Override
    protected boolean internalSet(final int _i, final Object _newV) {
      if (super.list_[_i] != _newV) {
        if (super.list_[_i] != null) {
          final H2dRubarEvolution e = (H2dRubarEvolution) super.list_[_i];
          e.setUnUsed(false);
          if (e.getUsed() == 0) {
            updateUsedCourbe(e);
          }
        }
        if (_newV != null) {
          final H2dRubarEvolution e = (H2dRubarEvolution) _newV;
          e.setListener(H2dRubarApportSpatialMng.this);
          e.setUsed(false);
          if (e.getUsed() == 1) {
            updateUsedCourbe(e);
          }
        }
        return super.internalSet(_i, _newV);
      }
      return false;
    }

    @Override
    protected void fireObjectChanged(int _indexGeom, Object _newValue) {
      appEvolChanged();
    }
  }

  final static CtuluNumberFormater APP_TIME_FORMAT = FortranLib.getFormater(15, 4, false);
  CtuluArrayObject eltIdx_;
  Set listener_;
  int nbElt_;

  /**
   * @return le nombre d'element
   */
  public final int getNbElt() {
    return nbElt_;
  }

  /**
   * @param _app l
   */
  public H2dRubarApportSpatialMng(final H2dRubarAppInterface _app) {
    nbElt_ = _app.getNbElt();
    createModel();
    loadData(_app);
  }

  public void loadData(H2dRubarAppInterface _app) {
    if (_app == null) {
      return;
    }
    final TIntObjectHashMap intEvol = new TIntObjectHashMap();
    int idx = 1;
    for (int i = nbElt_ - 1; i >= 0; i--) {
      // l'evolution est deja enregistree: on la reprend
      if (intEvol.contains(_app.getEvolIdx(i))) {
        eltIdx_.set(i, intEvol.get(_app.getEvolIdx(i)));
      } else {
        EvolutionReguliere evol = _app.getEvolForElt(i);
        // l'evolution est vide, on met une valeur nulle
        if (isEvolEmpty(evol)) {
          evol = null;
        } else {
          final H2dRubarEvolution finalEvol = H2dRubarEvolution.createH2dRubarEvolution(H2dVariableTransType.APPORT_PLUIE, evol, this);
          evolutionsUsed_.add(finalEvol);
          finalEvol.setUsed(true);
          finalEvol.setNom(H2dResource.getS("Apport") + CtuluLibString.ESPACE + (idx++));
          eltIdx_.set(i, finalEvol);
          intEvol.put(_app.getEvolIdx(i), finalEvol);
        }
      }
    }
  }

  /**
   * @return l'interface a utiliser pour sauvegarder les chroniques d'apport
   */
  public H2dRubarAppInterface getSavedInterface() {
    return new SaveAdapter();
  }

  private class SaveAdapter implements H2dRubarAppInterface {
    int[] idx_;
    EvolutionReguliere[] evols_;

    protected SaveAdapter() {
      final int nbElt = eltIdx_.getSize();
      idx_ = new int[nbElt];
      int tmp = 0;
      final TObjectIntHashMap evolInt = new TObjectIntHashMap();
      for (int i = 0; i < nbElt; i++) {
        H2dRubarEvolution e = H2dRubarApportSpatialMng.this.getEvol(i);
        // si null on met l'evolution vide
        if (e == null) {
          e = H2dRubarEvolution.EMPTY_EVOL;
        }
        if (evolInt.contains(e)) {
          idx_[i] = evolInt.get(e);
        } else {
          evolInt.put(e, tmp);
          idx_[i] = tmp++;
        }
      }
      if (tmp != evolInt.size()) {
        new Throwable().printStackTrace();
        return;
      }
      evols_ = new EvolutionReguliere[tmp];
      final TObjectIntIterator it = evolInt.iterator();
      for (int i = tmp; i-- > 0; ) {
        it.advance();
        evols_[it.value()] = (EvolutionReguliere) it.key();
      }
    }

    @Override
    public EvolutionReguliere getEvol(final int _i) {
      return evols_[_i];
    }

    @Override
    public EvolutionReguliere getEvolForElt(final int _idxElt) {
      return evols_[idx_[_idxElt]];
    }

    @Override
    public int getEvolIdx(final int _eltIdx) {
      return idx_[_eltIdx];
    }

    @Override
    public int getNbElt() {
      return idx_.length;
    }

    @Override
    public int getNbEvol() {
      return evols_.length;
    }
  }

  public void fillWithTransientCurves(final H2dEvolutionUseCounter _r) {
    if (eltIdx_ != null) {
      for (int i = eltIdx_.getSize() - 1; i >= 0; i--) {
        final H2dRubarEvolution e = (H2dRubarEvolution) eltIdx_.getObjectValueAt(i);
        if (e != null) {
          _r.add(e, H2dVariableTransType.APPORT_PLUIE, i);
        }
      }
    }
  }

  Set evolutionsUsed_;

  protected void updateUsedCourbe(final H2dRubarEvolution _e) {
    if (_e.isUsed() && !evolutionsUsed_.contains(_e)) {
      evolutionsUsed_.add(_e);
      appEvolIsUsedChanged(_e);
    } else if ((!_e.isUsed()) && evolutionsUsed_.contains(_e)) {
      evolutionsUsed_.remove(_e);
      appEvolIsUsedChanged(_e);
    }
  }

  public boolean isUsed(final EvolutionReguliereInterface _e) {
    return evolutionsUsed_!=null && evolutionsUsed_.contains(_e);
  }

  /**
   * @return tableau des courbes utilisees.
   */
  public EvolutionReguliereAbstract[] getUsedCourbes() {
    if (eltIdx_ != null) {
      final EvolutionReguliereAbstract[] rf = new EvolutionReguliereAbstract[evolutionsUsed_.size()];
      evolutionsUsed_.toArray(rf);
      return rf;
    }
    return null;
  }

  /**
   * @return true if at least one non empty curve is used.
   */
  public boolean containUsedCurveNotEmpty() {
    if (evolutionsUsed_ != null && !evolutionsUsed_.isEmpty()) {
      for (Object evol : evolutionsUsed_) {
        if (!isEvolEmpty((EvolutionReguliereInterface) evol)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * @param _evol l'evolution a tester
   * @return true si evolution vide
   */
  protected static boolean isEvolEmpty(final EvolutionReguliereInterface _evol) {
    if (_evol == null || _evol.getNbValues() == 0) {
      return true;
    }
    if (_evol.getNbValues() == 1) {
      return (_evol.getX(0) == 0) && (_evol.getY(0) == 0);
    }
    return false;
  }

  public H2dRubarApportSpatialMng(final int _nbElt) {
    nbElt_ = _nbElt;
    createModel();
  }

  protected final void createModel() {
    if (eltIdx_ == null) {
      eltIdx_ = new ApportModelObject(nbElt_);
      evolutionsUsed_ = new HashSet();
    }
  }

  /**
   * @param _l le listener a ajouter
   */
  public void addAppListener(final H2dRubarApportListener _l) {
    if (listener_ == null) {
      listener_ = new HashSet(3);
    }
    listener_.add(_l);
  }

  public void clear(final CtuluCommandContainer _cmd) {
    if (eltIdx_ != null) {
      final int[] idx = new int[nbElt_];
      for (int i = nbElt_ - 1; i >= 0; i--) {
        idx[i] = i;
      }
      eltIdx_.set(idx, null, _cmd);
    }
  }

  /**
   * @param _l le listener a tester
   * @return true si est contenu
   */
  public boolean containsAppListener(final H2dRubarApportListener _l) {
    return (listener_ != null) && listener_.contains(_l);
  }

  @Override
  public void evolutionChanged(final EvolutionReguliereInterface _e) {
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2dRubarApportListener) it.next()).apportEvolutionContentChanged(this, _e);
      }
    }
  }

  @Override
  public void evolutionUsedChanged(final EvolutionReguliereInterface _e, final int _old, final int _new, final boolean _isAdjusting) {
    /*
     * if (listener_ != null) { for (Iterator it = listener_.iterator(); it.hasNext();) { ((H2dRubarApportListener)
     * it.next()).apportEvolutionUseChanged(this,_e, _old, _new); } }
     */
  }

  protected void appEvolChanged() {
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2dRubarApportListener) it.next()).apportChanged(this);
      }
    }
  }

  protected void appEvolIsUsedChanged(final EvolutionReguliereInterface _e) {
    if (listener_ != null) {
      for (final Iterator it = listener_.iterator(); it.hasNext(); ) {
        ((H2dRubarApportListener) it.next()).apportEvolutionUsedChanged(this, _e);
      }
    }
  }

  public H2dRubarEvolution getCommonEvol(final int[] _idx) {
    if (_idx == null || _idx.length == 0) {
      return null;
    }
    H2dRubarEvolution r = getEvol(_idx[0]);
    if (r == null) {
      r = H2dRubarEvolution.EMPTY_EVOL;
    }
    for (int i = _idx.length - 1; i > 0; i--) {
      H2dRubarEvolution test = getEvol(_idx[i]);
      if (test == null) {
        test = H2dRubarEvolution.EMPTY_EVOL;
      }
      if (test != r) {
        return null;
      }
    }
    return r;
  }

  /**
   * @param _i l'indice de l'element
   * @return l'evolution correspondante
   */
  public H2dRubarEvolution getEvol(final int _i) {
    return eltIdx_ == null ? null : (H2dRubarEvolution) eltIdx_.getValueAt(_i);
  }

  /**
   * @return true si des evolution sont d�finies
   */
  public boolean isSet() {
    return evolutionsUsed_ != null && evolutionsUsed_.size() > 0;
  }

  /**
   * @param _l le listener a enlever
   */
  public void removeAppListener(final H2dRubarApportListener _l) {
    if (listener_ != null) {
      listener_.remove(_l);
    }
  }

  /**
   * @param _idx les indices a modifier
   * @param _e l'�volution
   * @param _cmd le receveur de commande
   */
  public void set(final int[] _idx, final H2dRubarEvolution _e, final CtuluCommandContainer _cmd) {
    H2dRubarEvolution e = _e;
    if (e == H2dRubarEvolution.EMPTY_EVOL || isEvolEmpty(e)) {
      e = null;
    }
    // pas de courbes initialisee et la nouvelle evolution est null on oublie
    if (eltIdx_ == null && (e == null)) {
      return;
    }
    if (eltIdx_ == null) {
      createModel();
    }
    eltIdx_.set(_idx, e, _cmd);
  }
}
