/*
 * @creation 1 sept. 2004
 * @modification $Date: 2006-04-19 13:19:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.h2d.type.H2dRubarBoundaryTarageGroupType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarTarageListener.java,v 1.7 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dRubarTarageListener {

  /**
   * Appele lorsque les coubres de tarages sont modifiees. Attention, il se peut que des courbes non utilisees envoi cet
   * evt.
   *
   * @param _e l'evolution modifiee
   */
  void tarageCourbeChanged(H2dRubarTarageMng _mng, EvolutionReguliereInterface _e);

  /**
   * Envoyee lorsque l'affectation de courbes de tarage est modifiee.
   */
  void tarageUsedChanged(H2dRubarTarageMng _mng);
  
  void TarageGroupeUsedChanged(H2dRubarTarageMng _mng,H2dRubarBoundaryTarageGroupType tarageGroupeType);

}