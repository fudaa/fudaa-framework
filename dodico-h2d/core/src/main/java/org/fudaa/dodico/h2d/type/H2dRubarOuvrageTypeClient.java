/*
 * @creation 3 janv. 2005
 * 
 * @modification $Date: 2006-04-19 13:19:39 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarOuvrageTypeClient.java,v 1.3 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dRubarOuvrageTypeClient {

  void visitComposite(Object _o);

  /**
   * Vister par type apport.
   * 
   * @param _o l'objet a passer
   */
  void visitApport(Object _o);

  /**
   * Vister par type deversoir.
   * 
   * @param _o l'objet a passer
   */
  void visitDeversoir(Object _o);

  /**
   * Vister par type breche.
   * 
   * @param _o l'objet a passer
   */
  void visitBreche(Object _o);

  /**
   * Vister par type transfert.
   * 
   * @param _o l'objet a passer
   */
  void visitTransfert(Object _o);

  void visitOrificeCirculaire(Object _o);

  void visitDeversoirHydraulique(Object _o);

}