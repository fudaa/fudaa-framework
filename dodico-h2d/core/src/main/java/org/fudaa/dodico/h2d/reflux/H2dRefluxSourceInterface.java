/*
 * @creation 20 ao�t 2003
 * @modification $Date: 2007-06-29 15:10:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import java.util.Map;
import org.fudaa.dodico.ef.EfGridSource;
import org.fudaa.dodico.fortran.FortranDoubleReaderResultInterface;
import org.fudaa.dodico.h2d.H2dNodalPropertyMixte;
import org.fudaa.dodico.h2d.H2dTimeStepGroup;
import org.fudaa.dodico.h2d.type.H2dProjetType;

/**
 * Utiliser pour l'ecriture des fichiers.
 * 
 * @author deniger
 * @version $Id: H2dRefluxSourceInterface.java,v 1.19 2007-06-29 15:10:29 deniger Exp $
 */
public interface H2dRefluxSourceInterface extends EfGridSource {

  /**
   * @return tous les fichiers necessaires
   */
  String[] getFichiers();

  /**
   * @return true si contient des sollicitations reparties
   */
  boolean contientSollicitationsReparties();

  /**
   * @return si contiens radiation
   */
  boolean contientRadiations();

  H2dNodalPropertyMixte[] getPropNodales();

  /**
   * @return si lecture pas de temps radiation
   */
  boolean isRadiationsLecturePasDeTemps();

  FortranDoubleReaderResultInterface getRadiationResult();

  FortranDoubleReaderResultInterface getVentResult();

  /**
   * @return si contient vent
   */
  boolean contientVent();

  /**
   * Cette valeur correspond au temps de d�marrage lu dans le fichier INP moins un pas de temps ( ou 0 si n�gatif). Cela
   * evite � Relfux de planter si les courbes ne commencent pas avant le pas de temps
   * 
   * @return le temps a partir duquel les evolutions doivent commencer
   */
  double getTimeBeginningForTrans();

  /**
   * @return le pas de temps de d�marrage pour le premier groupe de pas de temps. >0 si reprise
   */
  double getTimeBeginInForInpFile();

  /**
   * @return si vent lecture pas de temp
   */
  boolean isVentLecturePasDeTemps();

  /**
   * @return la version
   */
  String getVersion();

  /**
   * @return les mot-cles sous forme String->String
   */
  Map getEntiteValue();

  /**
   * @return le type du projet
   */
  H2dProjetType getTypeProjet();

  // EfGrid getGrid();

  /**
   * @return les prop elements
   */
  H2dNodalPropertyMixte[] getPropElementaires();

  /**
   * @param _idxGlobal l'indice global
   * @return la condition limite correspondante
   */
  H2dRefluxBoundaryCondition getConditionLimite(int _idxGlobal);

  /**
   * @return les groupes de pas de temps
   */
  H2dTimeStepGroup[] getGroupePasTemps();

  /**
   * @return les bords reflux.
   */
  H2dRefluxBordIndexGeneral[] getBords();
}