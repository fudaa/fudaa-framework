/*
 * @creation 9 juin 2004
 * @modification $Date: 2006-04-19 13:19:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.ef.EfGridSource;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarGridAreteSource.java,v 1.9 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dRubarGridAreteSource extends EfGridSource {

  /**
   * @return le nombre de decimales utilis�es pour x,y soit 1 ou 3
   */
  int getNbDecimal();

  /**
   * @return le maillage utilise
   */
  H2dRubarGrid getRubarGrid();

  /**
   * @return pour chaque element (dans l'ordre du maillage) les elements voisins
   */
  int[][] elementsVoisinsParElement();

  /**
   * @return pour chaque arete (dans l'ordre de la methode getAretes) les elements voisins
   */
  int[][] elementsVoisinsParArete();

  /**
   * @return les indices ordonnees des aretes "ouverte". Cela reste a definir.
   */
  int[] getIdxAreteLimiteEntrante();

}