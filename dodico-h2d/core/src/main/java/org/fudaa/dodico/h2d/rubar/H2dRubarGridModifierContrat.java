package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.ProgressionInterface;

public interface H2dRubarGridModifierContrat {

  void modify(H2dRubarGridAreteSource toModify, ProgressionInterface progressionInterface);
}
