/*
 * @creation 30 nov. 2004
 * @modification $Date: 2007-01-10 09:04:17 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import org.fudaa.ctulu.collection.CtuluCollectionDouble;

/**
 * Definition d'un siphon.
 *
 * @author Fred Deniger
 * @version $Id: H2dTelemacSiphonInterface.java,v 1.6 2007-01-10 09:04:17 deniger Exp $
 */
public interface H2dTelemacSiphonInterface extends CtuluCollectionDouble {

  /**
   * @return l'indice du siphon 1 pour la connexion
   */
  int getSource1();

  /**
   * @return l'indice du siphon 2 pour la connexion
   */
  int getSource2();

}