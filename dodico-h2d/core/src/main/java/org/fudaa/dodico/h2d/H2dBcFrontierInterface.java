/*
 *  @creation     20 nov. 2003
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

/**
 * Definition d'une frontiere : le nombre de point et les conditions limites en ces points.
 *
 * @author deniger
 * @version $Id: H2dBcFrontierInterface.java,v 1.7 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dBcFrontierInterface {
  /**
   * @param _idxOnThisFrontier l'indice du point sur cette frontiere
   * @return the boundary condition for the point which index is <code>_idxOnThisFrontier</code>
   */
  H2dBoundaryCondition getCl(int _idxOnThisFrontier);

  /**
   * @return the number of points belonging to this frontier
   */
  int getNbPt();
}
