/*
 *  @creation     20 janv. 2005
 *  @modification $Date: 2007-04-30 14:21:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import com.memoire.fu.FuLib;
import java.util.Map;
import org.fudaa.dodico.commun.DodicoEnumType;

/**
 * @author Fred Deniger
 * @version $Id: H2dVariableTypeCreated.java,v 1.11 2007-04-30 14:21:37 deniger Exp $
 */
public class H2dVariableTypeCreated extends H2dVariableType {

  public static String createShortName(final String _nom, final String _shortName, final Map _strVar) {
    String res = _shortName == null ? FuLib.clean(_nom) : _shortName;
    if (shortNameVar.containsKey(res) || isContained(_strVar, _shortName)) {
      String temp = res + "_1";
      int tempIdx = 2;
      while (shortNameVar.containsKey(temp) || isContained(_strVar, temp)) {
        temp = res + "_" + tempIdx++;
      }
      res = temp;
    }
    return res;

  }

  public static final boolean isContained(final Map _strVar, final String _s) {
    return _strVar != null && _strVar.containsKey(_s);
  }

  H2dVariableType parent_;

  String unit_;

  public H2dVariableTypeCreated(final String _nom, final Map _strVar) {
    this(_nom, null, null, _strVar);
  }

  public H2dVariableTypeCreated(final String _nom, final String _shortName, final Map _strVar) {
    this(_nom, _shortName, null, _strVar);
  }

  /**
   * @param _nom le nom
   * @param _shortName le nom court
   * @param _unit l'unit commune
   * @param _strVar le tableau nomCourt -> Variable creee permettant d'eviter que des noms courts soit utilises
   *          plusieurs fois.
   */
  public H2dVariableTypeCreated(final String _nom, final String _shortName, final String _unit, final Map _strVar) {
    super(_nom);
    unit_ = _unit;
    varShortName_ = createShortName(_nom, _shortName, _strVar);
    if (_strVar != null) {
      _strVar.put(varShortName_, this);
    }
  }

  @Override
  public Object getCommonUnit() {
    return unit_;
  }

  @Override
  public H2dVariableType getParentVariable() {
    return parent_;
  }

  @Override
  public double getValue(final H2dVariableTypeContainer _container) {
    return 0;
  }

  @Override
  public DodicoEnumType[] getArray() {
    return null;
  }

  public void removeShortName(final Map _shortName) {
    if (_shortName != null && _shortName.get(varShortName_) == this) {
      _shortName.remove(varShortName_);
    }
  }

  public final void setParent(final H2dVariableType _parent) {
    parent_ = _parent;
  }

  public void setUnit(final String _unit) {
    unit_ = _unit;
  }
}
