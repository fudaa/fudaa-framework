/*
 * @creation 20 ao�t 2003
 * 
 * @modification $Date: 2007-05-04 13:46:37 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@ists.sourceforge.net
 */
package org.fudaa.dodico.h2d.telemac;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfGridArray;
import org.fudaa.dodico.h2d.*;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionListenerDispatcher;
import org.fudaa.dodico.mesure.EvolutionListenerDispatcherDefault;

/**
 * @author deniger
 * @version $Id: H2dTelemacParameters.java,v 1.44 2007-05-04 13:46:37 deniger Exp $
 */
public class H2dTelemacParameters extends H2dParameters {

  private H2dTelemacBcManager clMng_;

  protected EfGridInterface maillage_;

  EvolutionListenerDispatcher dispatcher_;

  H2dTelemacProjectDispatcherListener listener_;

  H2dTelemacNodalProperties nodalProps_;

  H2dTelemacSeuilMng seuil_;
  final H2dTelemacTracerMng tracerManager;

  H2dTelemacSourceMng siphons_;

  @Override
  public H2dGridDataAdapter createGridDataAdapter() {
    final List l = new ArrayList(4);
    H2dVariableProviderInterface prov = getNodalData();
    if (prov != null) {
      l.add(prov);
    }
    prov = getElementData();
    if (prov != null) {
      l.add(prov);
    }
    return new H2dGridDataAdapter(l);
  }

  /**
   * @param _p les parametres du dico
   * @param _l le listener
   */
  public H2dTelemacParameters(final H2dTelemacDicoParams _p, final H2dTelemacProjectDispatcherListener _l) {
    super(_p);
    listener_ = _l;
    _p.addModelListener(_l);
    tracerManager = new H2dTelemacTracerMng(_p);
  }

  protected void setClManager(final H2dTelemacBcManager _clMng) {
    if ((clMng_ != _clMng) && (_clMng != null)) {
      _clMng.removeClChangedListener(listener_);
      clMng_ = _clMng;
      clMng_.addClChangedListener(listener_);
      clMng_.setEvolListener(listener_);
    }
  }

  @Override
  public CtuluPermanentList getAllowedBordType() {
    return clMng_.getBordList();
  }

  /**
   * @return copie des valeurs de friction de fond.
   */
  public CtuluCollectionDouble getBottomFriction() {
    return nodalProps_ == null ? null : nodalProps_.getViewedModel(H2dVariableType.COEF_FROTTEMENT_FOND);
  }

  /**
   * @return le manger des cl.
   */
  public H2dBcManagerBlockInterface getCLManager() {
    return clMng_;
  }

  /**
   * Cherche la duree de la simulation et renvoie 0 si un mot-cle concerne est invalide.
   * 
   * @return la duree du calcul return the duration of the computation
   */
  public long getComputationDuration() {
    double r = 0d;
    final DicoEntite eNbPasTemps = getTelemacDicoParams().getTelemacDicoFileFormatVersion().getNbPasTemps();
    final DicoEntite eDuree = getTelemacDicoParams().getTelemacDicoFileFormatVersion().getDureeDuCalcul();
    final DicoEntite ePasDeTemp = getTelemacDicoParams().getTelemacDicoFileFormatVersion().getPasTemps();
    // si les mot-cles pas de temps n'existent pas, on essaie avec eDuree
    if ((eNbPasTemps == null) || (ePasDeTemp == null)) {
      // si duree existe et est valide
      if ((eDuree != null) && (dicoParams_.isValueValideFor(eDuree))) {
        r = Double.parseDouble(dicoParams_.getValue(eDuree));
      }
    }
    // les mot-cl� existent et sont valides
    else if (dicoParams_.isValueValideFor(eNbPasTemps) && dicoParams_.isValueValideFor(ePasDeTemp)) {
      // mot-cle duree n'existe pas
      if (eDuree == null) {
        r = Double.parseDouble(dicoParams_.getValue(eNbPasTemps)) * Double.parseDouble(dicoParams_.getValue(ePasDeTemp));
      }
      // duree existe
      else if (dicoParams_.isValueValideFor(eDuree)) {
        if (dicoParams_.isValueSetFor(eDuree)) {
          final double duree = Double.parseDouble(dicoParams_.getValue(eDuree));
          // si duree et nb pas de temps existe, on prend celui qui conduit a la simulation la plus
          // longue
          if (dicoParams_.isValueSetFor(eNbPasTemps)) {
            final double duree2 = Double.parseDouble(dicoParams_.getValue(eNbPasTemps)) * Double.parseDouble(dicoParams_.getValue(ePasDeTemp));
            r = duree > duree2 ? duree : duree2;
          } else {
            r = duree;
          }
        } else {
          // cas simple
          r = Double.parseDouble(dicoParams_.getValue(eNbPasTemps)) * Double.parseDouble(dicoParams_.getValue(ePasDeTemp));
        }
      }
    }
    return (long) Math.ceil(r);
  }

  @Override
  public EfGridInterface getMaillage() {
    return maillage_;
  }

  @Override
  public EvolutionListenerDispatcher getProjectDispatcher() {
    if (dispatcher_ == null) {
      dispatcher_ = new EvolutionListenerDispatcherDefault();
      if (listener_ != null) {
        dispatcher_.addEvolutionListener(listener_);
      }
    }
    return dispatcher_;
  }

  /**
   * @return renvoie le manager de seuil. Cree si necessaire.
   */
  public H2dTelemacSeuilMng getSeuil() {
    if (seuil_ == null && getTelemacDicoParams().getTelemacDicoFileFormatVersion().getSeuilNombreMotCle() != null) {
      seuil_ = new H2dTelemacSeuilMng(getMaillage(), getTelemacDicoParams());
      if (listener_ != null) {
        seuil_.addListener(listener_);
      }
    }
    return seuil_;
  }

  public H2dTelemacSourceMng getSourceSiphonMng() {
    if (!getTelemacVersion().isTelemac2d() && !getTelemacVersion().isTelemac3d()) {
      return null;
    }
    if (siphons_ == null) {
      siphons_ = new H2dTelemacSourceMng(tracerManager);
      if (listener_ != null) {
        siphons_.addListener(listener_);
      }
    }
    return siphons_;
  }

  /**
   * @return le manger des cl.
   */
  public H2dTelemacBcManager getTelemacCLManager() {
    return clMng_;
  }

  /**
   * @return les parametres issu du dico
   */
  public H2dTelemacDicoParams getTelemacDicoParams() {
    return (H2dTelemacDicoParams) getDicoParams();
  }

  /**
   * @return la version utilises
   */
  public H2dTelemacDicoFileFormatVersion getTelemacVersion() {
    return (H2dTelemacDicoFileFormatVersion) super.getVersion();
  }

  @Override
  public List getTransientVar() {
    if (!isTransientAvailable()) {
      return Collections.EMPTY_LIST;
    }
    return H2dTelemacTransient.TRANSIENT_VARIABLES;
  }

  /**
   * @return collection des evolutions utilisees
   */
  public Set getUsedEvol() {
    final Set r = new HashSet();
    if (clMng_ != null) {
      clMng_.fillWithUsedEvol(r);
    }
    return r;
  }

  @Override
  public Map getUsedEvolutionVariables() {
    final H2dEvolutionVariableMap m = new H2dEvolutionVariableMap();
    if (clMng_ != null) {
      clMng_.fillWithVariableEvol(m);
    }
    if (m.getVarEvol() == null) {
      return Collections.EMPTY_MAP;
    }
    return m.getVarEvol();
  }

  /**
   * @param _cls le source initiales pour les cl
   * @param _progress la barre de progression
   * @param _analyze le receveur d'info
   */
  public void initCL(final H2dTelemacCLSourceInterface _cls, final ProgressionInterface _progress, final CtuluAnalyze _analyze) {
    if (getMaillage() == null) {
      if (_analyze != null) {
        _analyze.addFatalError(H2dResource.getS("Le maillage n'est pas sp�cifi�"));
      }
      new Throwable().printStackTrace();
      return;
    }
    final H2dTelemacBcManager clMng = H2dTelemacBcManager.init(getMaillage(), tracerManager, _cls, _analyze, _progress);
    if (clMng != null) {
      setClManager(clMng);
    }
  }

  /**
   * @param _t la variable a tester
   * @return true si cette variable est "nodale" et initialisee
   */
  public boolean isNodalPropInitialized(final H2dVariableType _t) {
    return nodalProps_ != null && nodalProps_.containsValuesFor(_t);
  }

  /**
   * @param _t la variable nodale
   * @return la valeur par d�faut ou null si aucune
   */
  public String getDefaultValueForNodaleProperties(final H2dVariableType _t) {
    if (_t == H2dVariableType.COEF_FROTTEMENT_FOND) {
      return getDefaultValueForFriction();
    } else if (_t == H2dVariableType.VISCOSITE) {
      return getDefaultValueForViscosity();
    }
    return CtuluLibString.ZERO;
  }

  /**
   * @return la valeur definie dans le fichier cas.
   */
  public String getDefaultValueForFriction() {
    return getValueFor(getTelemacVersion().getFrictionCoefKeyWord(), "50");
  }

  /**
   * @param _keyw les noms des mot-cl�s
   * @param _def la valeur par defaut
   * @return la valeur
   */
  private String getValueFor(final String[] _keyw, final String _def) {
    if (_keyw != null && dicoParams_ != null) {
      final DicoEntite e = getTelemacVersion().getEntiteFor(_keyw);
      if (e != null) {
        return dicoParams_.getValue(e);
      }
    }
    return _def;
  }

  /**
   * @return la valeur definie dans le fichier cas.
   */
  public String getDefaultValueForViscosity() {
    return getValueFor(getTelemacVersion().getViscosityKeyWord(), "1E-4");
  }

  /**
   * @return true si des comportements transitoires peuvent etre utilises.
   */
  public boolean isTransientAvailable() {
    return getTelemacVersion().isTransientAvailable();
  }

  /**
   * Mise a jour une seule fois.
   * 
   * @param evolutions les nouvelles frontieres liquides.
   */
  public void initEvolutions(final H2dTelemacEvolutionFrontiere[] evolutions) {
    if (evolutions != null) {
      clMng_.setEvolution(evolutions, getProjectDispatcher());
    }
  }

  public H2dNodalPropertiesMngI getNodalProperties() {
    if (nodalProps_ == null) {
      nodalProps_ = new H2dTelemacNodalProperties(getSolutionsInit(), false);
      if (listener_ != null) {
        nodalProps_.addListener(listener_);
      }
    }
    return nodalProps_;
  }

  public H2dTelemacNodalProperties getTelemacNodalProperties() {
    return (H2dTelemacNodalProperties) getNodalProperties();
  }

  public boolean initNodalProperties(final H2dVariableType _t, final double _v, final CtuluCommandContainer _cmd) {
    getNodalProperties();
    if (!nodalProps_.containsValuesFor(_t)) {
      nodalProps_.initValues(_t, _v, _cmd);
      return true;
    }
    return false;
  }

  public boolean removeNodalProperties(final H2dVariableType _t, final CtuluCommandContainer _cmd) {
    if (nodalProps_ != null) {
      nodalProps_.removeValues(_t, _cmd);
      return true;
    }
    return false;
  }

  public boolean isNodalPropertiesModifiable() {
    return getNodalProperties() != null && !getNodalProperties().isEmpty();
  }
  
  private boolean xyDoublePrecision;

  public void setXyDoublePrecision(boolean xyDoublePrecision) {
    this.xyDoublePrecision = xyDoublePrecision;
  }
  

  public boolean isXyDoublePrecision() {
    return xyDoublePrecision;
  }
  
  
  

  /**
   * Mise a jour une seule fois.
   * 
   * @param _m le nouveau maillage
   * @param _inter la barre de progression
   * @param _analyze receveur des erreurs.
   */
  public final void setMaillage(final EfGridInterface _m, final ProgressionInterface _inter, final CtuluAnalyze _analyze) {
    // pour eviter d'envoyer balader pour rien
    if (_m == maillage_) {
      return;
    }
    if (maillage_ != null || clMng_ != null) {
      if (_analyze != null) {
        _analyze.addFatalError(H2dResource.getS("Maillage d�j� sp�cifi�"));
      }
      new Throwable().printStackTrace();
      return;
    }
    maillage_ = new EfGridArray(_m);
    if (maillage_.getFrontiers() == null) {
      maillage_.computeBord(_inter, _analyze);
    }
    maillage_.createIndexRegular(_inter);
  }

  public void initializeNodalProperties(final Map _varValues) {
    if (_varValues == null || _varValues.size() == 0) {
      return;
    }
    ((H2dTelemacNodalProperties) getNodalProperties()).initNodalProperties(_varValues);
  }

  public void activeSi(final Map _varValues, final CtuluCommandContainer _cmd) {
    getNodalProperties();
    nodalProps_.activeSi(_varValues, _cmd);
  }

  public void unactiveSi(final Map _varValues, final CtuluCommandContainer _cmd) {
    getNodalProperties();
    nodalProps_.unactiveSi(_varValues, _cmd);
  }

  public void activeSi(final double _coteEau, final Map _varToAdd, final CtuluCommandContainer _cmd) {
    getNodalProperties();
    nodalProps_.activeSi(_coteEau, _varToAdd, _cmd);
  }

  /**
   * Initialise les cl en mettant tout a solide.
   * 
   * @param _analyze receveur des infos
   */
  public void setNoCL(final CtuluAnalyze _analyze) {
    setClManager(H2dTelemacBcManager.init(getMaillage()));
    clMng_.setDicoParams(tracerManager, getDicoParams(), _analyze);
  }

  /**
   * @return true si les solutions initiales ont �t� cr�es.
   */
  public boolean isSolutionInitialesCreated() {
    return solInit_ != null;
  }

  /**
   * @return le gestionnaire des solutions initiales
   */
  public H2dTelemacSIProperties getSolutionsInit() {
    if (solInit_ == null && getMaillage() != null) {
      solInit_ = new H2dTelemacSIProperties(this);
      if (listener_ != null) {
        solInit_.addListener(listener_);
      }
    }
    return solInit_;
  }

  private H2dTelemacSIProperties solInit_;

  private long initialDateInMillis;
  private long repriseInitialDateInMillis;

  /**
   * @param _l le listener (renvoyeur d'evt) de ce projet
   */
  public void setProjectListener(final H2dTelemacProjectDispatcherListener _l) {
    if (_l == listener_) {
      return;
    }
    if (listener_ != null) {
      if (clMng_ != null) {
        clMng_.removeClChangedListener(listener_);
      }
      if (dicoParams_ != null) {
        dicoParams_.removeModelListener(listener_);
      }
      if (dispatcher_ != null) {
        dispatcher_.removeEvolutionListener(listener_);
      }
      if (siphons_ != null) {
        siphons_.removeListener(listener_);
      }
      if (seuil_ != null) {
        seuil_.removeListener(listener_);
      }
      if (nodalProps_ != null) {
        nodalProps_.removeListener(listener_);
      }
      if (solInit_ != null) {
        solInit_.removeListener(listener_);
      }
    }
    listener_ = _l;
    if (_l != null) {
      if (clMng_ != null) {
        clMng_.addClChangedListener(_l);
      }
      if (dicoParams_ != null) {
        dicoParams_.addModelListener(_l);
      }
      if (dispatcher_ != null) {
        dispatcher_.addEvolutionListener(listener_);
      }
      if (siphons_ != null) {
        siphons_.addListener(listener_);
      }
      if (seuil_ != null) {
        seuil_.addListener(listener_);
      }
      if (nodalProps_ != null) {
        nodalProps_.addListener(listener_);
      }
      if (solInit_ != null) {
        solInit_.addListener(listener_);
      }
    }
  }

  @Override
  public H2dVariableProviderInterface getElementData() {
    return null;
  }

  @Override
  public H2dVariableProviderInterface getNodalData() {
    return getNodalProperties().getVariableDataProvider();
  }

  @Override
  public H2dVariableProviderInterface getSiData() {
    return getSolutionsInit().getVariableDataProvider();
  }

  @Override
  public H2dProjectDispatcherListener getMainListener() {
    return listener_;
  }

  public void fireGridNodeXYChanged() {
    if (listener_ != null) {
      listener_.gridNodeXYChanged();
    }

  }

  public void setInitialDateInMillis(long initialDateInMillis) {
    this.initialDateInMillis = initialDateInMillis;
  }

  public long getInitialDateInMillis() {
    return initialDateInMillis;
  }

  public void setRepriseInitialDate(long initialDate) {
    this.repriseInitialDateInMillis = initialDate;
  }

  public long getRepriseInitialDateInMillis() {
    return repriseInitialDateInMillis == 0 ? initialDateInMillis : repriseInitialDateInMillis;
  }

  public H2dTelemacTracerMng getTracerManager() {
    return tracerManager;
  }

}
