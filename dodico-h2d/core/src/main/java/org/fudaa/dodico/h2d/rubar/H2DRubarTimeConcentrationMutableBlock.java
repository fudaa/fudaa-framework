package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.mesure.EvolutionReguliere;

public class H2DRubarTimeConcentrationMutableBlock {
  /**
   * l'evolution hcat.
   */
  public EvolutionReguliere concentration;
  /**
   * l'evolution diam.
   */
  public EvolutionReguliere diametre;
  /**
   * l'evolution et.
   */
  public EvolutionReguliere etendue;

  public H2DRubarTimeConcentrationMutableBlock(int nbValues) {
    concentration = new EvolutionReguliere(nbValues);
    diametre = new EvolutionReguliere(nbValues);
    etendue = new EvolutionReguliere(nbValues);
  }
}
