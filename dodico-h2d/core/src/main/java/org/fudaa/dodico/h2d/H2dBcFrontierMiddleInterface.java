/*
 *  @creation     21 nov. 2003
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import org.fudaa.dodico.h2d.type.H2dBoundaryType;

/**
 * Decrit une frontieres contenant des L3 ( maillage T6). La frontiere est decomposee en segment de 3points :le point
 * milieu contient les donnees hydraulique principale.
 *
 * @author deniger
 * @version $Id: H2dBcFrontierMiddleInterface.java,v 1.8 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dBcFrontierMiddleInterface extends H2dBcFrontierInterface {
  /**
   * @param _i l'indice du point milieu
   * @return le type du segment dont le point milieu est _i
   */
  H2dBoundaryType getBcType(int _i);
}
