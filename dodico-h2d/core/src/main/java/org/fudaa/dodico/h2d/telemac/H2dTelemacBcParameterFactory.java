/*
 * @creation 17 mai 2004
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.dodico.dico.DicoEntite;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dBoundaryTypeCommon;

/**
 * @author Fred Deniger
 * @version $Id: H2dTelemacBcParameterFactory.java,v 1.12 2006-09-19 14:43:25 deniger Exp $
 */
public final class H2dTelemacBcParameterFactory {

  private H2dTelemacBcParameterFactory() {
    super();
  }

  /**
   * @param _manager le manager des cl
   * @param _analyze receveur des infos
   */
  protected static void buildVariables(final H2dTelemacBcManager _manager, final CtuluAnalyze _analyze) {
    final String codeName = _manager.getH2dFileFormat().getCodeName();
    if (codeName.equals(H2dTelemacDicoParams.getSubief2dId())) {
      doSubief(_manager);
      return;
    } else if (codeName.equals(H2dTelemacDicoParams.getSisypheId())) {
      doSisyphe(_manager);
      return;
    }

    final List values = new ArrayList(10);
    // VALUES//
    DicoEntite entTemp = _manager.getH2dFileFormat().getPrescribedFlowrate();
    final DicoEntite.Vecteur profilTemp = (DicoEntite.Vecteur) _manager.getH2dFileFormat().getVelocityProfils();
    // flowrate
    if ((entTemp != null) && (entTemp instanceof DicoEntite.Vecteur)) {
      values.add(new H2dTelemacBcParameterFlowrate(_manager, (DicoEntite.Vecteur) entTemp,
          new H2dBoundaryType[] { H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE }, profilTemp));
    }
    entTemp = _manager.getH2dFileFormat().getPrescribedVelocity();
    // velocity
    if ((entTemp != null) && (entTemp instanceof DicoEntite.Vecteur)) {
      values.add(new H2dTelemacBcParameterVelocity(_manager, (DicoEntite.Vecteur) entTemp, new H2dBoundaryType[] {
          H2dBoundaryTypeCommon.LIQUIDE_VITESSES_IMPOSEES, H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES }));
    }
    entTemp = _manager.getH2dFileFormat().getPrescribedElevation();
    // elevation
    if ((entTemp != null) && (entTemp instanceof DicoEntite.Vecteur)) {
      values.add(new H2dTelemacBcParameterElevation(_manager, (DicoEntite.Vecteur) entTemp, new H2dBoundaryType[] {
          H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_IMPOSEE, H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES }));
    }
    // tracer
    entTemp = _manager.getH2dFileFormat().getPrescribedTracer();
    if ((entTemp != null) && (entTemp instanceof DicoEntite.Vecteur)) {
      values.add(new H2dTelemacBcParameterTracer(_manager, (DicoEntite.Vecteur) entTemp, new H2dBoundaryType[] {
          H2dBoundaryTypeCommon.LIQUIDE_ONDE_INCIDENCE, H2dBoundaryTypeCommon.LIQUIDE_VITESSES_IMPOSEES,
          H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE, H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_IMPOSEE,
          H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES }));
    }
    _manager.prescribedParameters_ = new H2dTelemacBcParameter[values.size()];
    values.toArray(_manager.prescribedParameters_);
    values.clear();
    // OPTIONS//
    // velocity profil
    if (profilTemp == null) {
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("DHD: keyword profil does not exist");
      }
    } else {
      values.add(new H2dTelemacBcParameterVelocityProfil(_manager, profilTemp,
          new H2dBoundaryType[] { H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE }));
    }
    // liquid options
    entTemp = _manager.getH2dFileFormat().getLiquidBoundaryOption();
    if ((entTemp != null) && (entTemp instanceof DicoEntite.Vecteur)) {
      values.add(new H2dTelemacBcParameterLiquidOption(_manager, (DicoEntite.Vecteur) entTemp, new H2dBoundaryType[] {
          H2dBoundaryTypeCommon.LIQUIDE, H2dBoundaryTypeCommon.LIQUIDE_VITESSES_IMPOSEES,
          H2dBoundaryTypeCommon.LIQUIDE_DEBIT_IMPOSE, H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_IMPOSEE,
          H2dBoundaryTypeCommon.LIQUIDE_HAUTEUR_VITESSES_IMPOSEES, H2dBoundaryTypeCommon.LIQUIDE_ONDE_INCIDENCE }));
    } else if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("keyword liquid options does not exist");
    }
    _manager.prescribedOptions_ = new H2dTelemacBcParameter[values.size()];
    values.toArray(_manager.prescribedOptions_);
  }

  private static void doSisyphe(final H2dTelemacBcManager _manager) {
    _manager.prescribedParameters_ = new H2dTelemacBcParameter[1];
    _manager.prescribedParameters_[0] = new H2dTelemacBcParameterSedimentEvol();
    _manager.prescribedOptions_ = new H2dTelemacBcParameter[0];
  }

  private static void doSubief(final H2dTelemacBcManager _manager) {
    _manager.prescribedParameters_ = new H2dTelemacBcParameter[1];
    _manager.prescribedParameters_[0] = new H2dTelemacBcParameterConcentration();
    _manager.prescribedOptions_ = new H2dTelemacBcParameter[0];
  }

}