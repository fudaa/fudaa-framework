/*
 *  @creation     30 mars 2005
 *  @modification $Date: 2007-04-30 14:21:35 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluListSelectionInterface;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEditAbstract;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2DLib;
import org.fudaa.dodico.h2d.H2dSIListener;
import org.fudaa.dodico.h2d.H2dSiSourceInterface;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author Fred Deniger
 * @version $Id: H2dTelemacSIProperties.java,v 1.22 2007-04-30 14:21:35 deniger Exp $
 */
public class H2dTelemacSIProperties extends H2DTelemacNodalPropertiesMngAbstract implements H2dSiSourceInterface {

  class HauteurEauModel extends CtuluCollectionDoubleEditAbstract {

    final CtuluCollectionDouble bathy_ = getBathyModel();

    CtuluArrayDouble coteEau_ = (CtuluArrayDouble) getModifiableModel(H2dVariableType.COTE_EAU);

    @Override
    protected void internalSetValue(final int _i, final double _newV) {
      coteEau_.set(_i, bathy_.getValue(_i) + _newV);

    }

    @Override
    public int getSize() {
      return getMaillage().getPtsNb();
    }

    @Override
    public double getValue(final int _i) {
      return coteEau_.getValue(_i) - bathy_.getValue(_i);
    }
  }

  double timeStep_;

  transient protected List listener_;

  /**
   * @param _parameters les param�tres parents
   */
  public H2dTelemacSIProperties(final H2dTelemacParameters _parameters) {
    super(_parameters.getMaillage());
  }

  @Override
  protected void fireChanged(final H2dVariableType _var) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dSIListener) listener_.get(i)).siChanged(_var);

      }
    }
    if (model_ != null) {
      model_.clearCache();
    }
  }

  @Override
  public boolean isSolutionInitialesActivated() {
    return !isEmpty();
  }

  @Override
  protected void fireVarAddedProcess(final H2dVariableType _var) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dSIListener) listener_.get(i)).siAdded(_var);

      }
    }
  }

  @Override
  protected void fireVarRemovedProcess(final H2dVariableType _var) {
    if (listener_ != null) {
      for (int i = listener_.size() - 1; i >= 0; i--) {
        ((H2dSIListener) listener_.get(i)).siRemoved(_var);

      }
    }
  }

  final void clear() {
    if (getCurrentVarValues() == null) {
      return;
    }
    getCurrentVarValues().clear();
    clearVarList();

  }

  final void initSi(final double _initElevation, final Map _initVar, final CtuluCollectionDouble _bathy) {
    // on verifie que la cote d'eau est bien superieure a la bathy.
    final CtuluCollectionDouble bathy = _bathy;
    clear();
    final Map newMap = new HashMap((_initVar == null ? 0 : _initVar.size()) + 2);
    if (_initVar != null) {
      newMap.putAll(_initVar);

    }
    newMap.put(H2dVariableType.BATHYMETRIE, _bathy);
    final double[] ze = new double[getGrid().getPtsNb()];
    for (int i = ze.length - 1; i >= 0; i--) {
      ze[i] = _initElevation;
      if (bathy != null && ze[i] < bathy.getValue(i)) {
        ze[i] = bathy.getValue(i);
      }
    }
    newMap.put(H2dVariableType.COTE_EAU, ze);
    initalize(newMap);

  }

  public void removeRepriseValues(final Map _m) {
    if (_m != null) {
      _m.remove(H2dVariableType.HAUTEUR_EAU);
      _m.remove(H2dVariableType.COTE_EAU);
      _m.remove(H2dVariableType.VITESSE_U);
      _m.remove(H2dVariableType.VITESSE_V);
      _m.remove(H2dVariableType.DEBIT_X);
      _m.remove(H2dVariableType.DEBIT_Y);
    }
  }

  public boolean containRepriseValues(final Map _m) {
    if (_m != null) {
      return _m.containsKey(H2dVariableType.HAUTEUR_EAU) || _m.containsKey(H2dVariableType.COTE_EAU)
          || _m.containsKey(H2dVariableType.VITESSE_U) || _m.containsKey(H2dVariableType.VITESSE_V);
    }
    return false;
  }

  public boolean containsRequiredRepriseValues(final Map _m) {
    if (_m != null) {
      return _m.containsKey(H2dVariableType.HAUTEUR_EAU) || _m.containsKey(H2dVariableType.COTE_EAU);
    }
    return false;
  }

  /**
   * @param _listener listener
   */
  @Override
  public void addListener(final H2dSIListener _listener) {
    if (listener_ == null) {
      listener_ = new ArrayList();
    }
    listener_.add(_listener);
  }

  @Override
  public boolean canVariableBeenAdd(final H2dVariableType _t) {
    return _t != H2dVariableType.HAUTEUR_EAU && _t != H2dVariableType.COTE_EAU && _t != H2dVariableType.BATHYMETRIE;
  }

  public boolean containsEditableVariable() {
    return getCurrentVarValues() != null && super.getCurrentVarValues().size() > 0;
  }

  /**
   * @param _listener le listener a tester
   * @return true si contenu
   */
  public boolean containsListener(final H2dSIListener _listener) {
    return listener_ != null && listener_.contains(_listener);
  }

  /**
   * @return la bathymetrie
   */
  @Override
  public CtuluCollectionDouble getBathyModel() {
    return getViewedModel(H2dVariableType.BATHYMETRIE);
  }

  @Override
  public EfGridInterface getMaillage() {
    return super.getGrid();
  }

  /**
   * @param _t la variable
   * @return le tableau des doubles concern�s. null si la variable n'est pas supportee.
   */
  public double[] getModelArray(final H2dVariableType _t) {
    if (containsValuesFor(_t)) {
      return getViewedModel(_t).getValues();
    }
    return null;
  }

  HauteurEauModel model_;

  @Override
  public CtuluCollectionDoubleEdit getModifiableModel(final H2dVariableType _var) {
    if (_var == H2dVariableType.HAUTEUR_EAU) {
      if (model_ == null && super.containsValuesFor(H2dVariableType.BATHYMETRIE)
          && super.containsValuesFor(H2dVariableType.COTE_EAU)) {
        model_ = new HauteurEauModel();
      }
      return model_;
    }
    return super.getModifiableModel(_var);
  }

  /**
   * @return le pas de temps a utiliser (si).
   */
  public final double getTimeStep() {
    return timeStep_;
  }

  @Override
  public double[] getU() {
    return getModelArray(H2dVariableType.VITESSE_U);
  }

  @Override
  public Collection getUsableVariables() {
    if (isEmpty()) {
      return null;
    }
    return getVariablesListWhithDepth();
  }

  @Override
  public double[] getV() {
    return getModelArray(H2dVariableType.VITESSE_V);
  }

  @Override
  public H2dVariableProviderInterface getVariableDataProvider() {
    return isEmpty() ? null : super.getVariableDataProvider();
  }

  @Override
  public List getVariables() {
    return super.getAllVariables();
  }

  public Collection getVariablesListWhithDepth() {
    if (getBathyModel() != null && super.containsValuesFor(H2dVariableType.COTE_EAU)) {
      final Collection r = new HashSet(getAllVariables());
      r.add(H2dVariableType.HAUTEUR_EAU);
      return r;
    }
    return super.getAllVariables();
  }

  /**
   * toujours non null.
   */
  @Override
  public double[] getZe() {
    double[] r = getModelArray(H2dVariableType.COTE_EAU);
    if (r == null) {
      r = new double[getGrid().getPtsNb()];
    }
    return r;
  }

  /**
   * @param _timeStep initialisation du pas de temps
   */
  public final void initFirstTimeStep(final double _timeStep) {
    timeStep_ = _timeStep;
  }

  /**
   * @param _varMap le tableau variable->values
   * @param _repriseFile le fichier utilis�
   * @param _timeStep le pas de temps
   */
  public void load(final Map _varMap, final File _repriseFile, final double _timeStep) {
    super.initalize(_varMap);
    initFirstTimeStep(_timeStep);
  }

  /**
   * @param _listener le listener a virer
   */
  @Override
  public void removeListener(final H2dSIListener _listener) {
    if (listener_ != null) {
      listener_.remove(_listener);
    }
  }

  @Override
  public void set(final double[] _ze, final double[] _u, final double[] _v, final CtuluCommandContainer _cmd) {
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    // maj cote eau
    if (_ze != null) {
      if (containsValuesFor(H2dVariableType.COTE_EAU)) {
        // sinon, on met a jour les valeurs en cours
        final CtuluCollectionDoubleEdit arr = getModifiableModel(H2dVariableType.COTE_EAU);
        arr.setAll(_ze, cmp);
        // la cote n'est pas encore presente : on la creee
      } else {
        initValues(H2dVariableType.COTE_EAU, _ze, cmp);
      }
    }
    // maj u
    if (_u != null) {
      if (containsValuesFor(H2dVariableType.VITESSE_U)) {
        final CtuluCollectionDoubleEdit arr = getModifiableModel(H2dVariableType.VITESSE_U);
        arr.setAll(_u, cmp);
      } else {
        initValues(H2dVariableType.VITESSE_U, _u, cmp);
      }
    }
    // maj v
    if (_v != null) {
      if (containsValuesFor(H2dVariableType.VITESSE_V)) {
        final CtuluCollectionDoubleEdit arr = getModifiableModel(H2dVariableType.VITESSE_V);
        arr.setAll(_v, cmp);
      } else {
        initValues(H2dVariableType.VITESSE_V, _v, cmp);
      }
    }
    if (_cmd != null) {
      _cmd.addCmd(cmp.getSimplify());
    }
  }

  /**
   * @param _timeStep le pas de temps a utiliser
   * @param _cmd le receveur de commande
   */
  public final void setFirstTimeStep(final double _timeStep, final CtuluCommandContainer _cmd) {
    if (_timeStep != timeStep_) {
      final double old = timeStep_;
      timeStep_ = _timeStep;
      if (_cmd != null) {
        _cmd.addCmd(new CtuluCommand() {

          @Override
          public void redo() {
            setFirstTimeStep(_timeStep, null);
          }

          @Override
          public void undo() {
            setFirstTimeStep(old, null);
          }

        });
        fireChanged(null);
      }
    }

  }

  @Override
  public void setHToZero(final int[] _idx, final CtuluCommandContainer _cmd) {
    H2DLib.setHToZero(this, _idx, _cmd);
  }

  @Override
  public int testCoteEau(final CtuluListSelectionInterface _selection, final CtuluCommandContainer _cmd) {
    return H2DLib.testCoteEau(this, _selection, _cmd);
  }

}
