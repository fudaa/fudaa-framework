/*
 * @creation 3 janv. 2005
 *
 * @modification $Date: 2007-04-30 14:21:34 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.locationtech.jts.geom.Coordinate;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntHashSet;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.CtuluListObject;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.dodico.ef.EfSegment;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageRef;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

import java.util.*;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarOuvrage.java,v 1.15 2007-04-30 14:21:34 deniger Exp $
 */
public class H2dRubarOuvrage implements H2dRubarOuvrageI {
  public static int getNbVariableNonTransport() {
    return 1;
  }

  public static int getNbVariableTransport() {
    return 3;
  }

  public static List<H2dVariableType> getQVariables(int nbBlock) {
    List<H2dVariableType> res = new ArrayList<>(getNbVariableNonTransport() + nbBlock * getNbVariableTransport());
    res.add(H2dVariableType.DEBIT_M3);
    res.addAll(H2dRubarBcMng.getTransportVariables(nbBlock));
    return res;
  }

  public static List<H2dVariableType> getTarageVariables() {
    return Arrays.asList(H2dVariableType.LOI_TARAGE);
  }

  @Override
  public int getNbOuvrageElementaires() {
    return ouvIntern_ == null ? 0 : ouvIntern_.getSize();
  }

  public void setProjetType(final H2dRubarProjetType projet, final int nbTransportBlock, final CtuluCommandComposite cmd) {
    int nb = getNbOuvrageElementaires();
    for (int i = 0; i < nb; i++) {
      getOuvrageElementaire(i).setProjetType(projet, nbTransportBlock, cmd);
    }
  }

  @Override
  public boolean containsOuvrageElementaire() {
    return getNbOuvrageElementaires() > 0;
  }

  @Override
  public H2dRubarOuvrageElementaireInterface getOuvrageElementaire(final int _i) {
    return (H2dRubarOuvrageElementaireInterface) ouvIntern_.getValueAt(_i);
  }

  @Override
  public void fillWithTarageEvolution(String prefix, Set allTarage) {
    final int nb = getNbOuvrageElementaires();
    for (int i = 0; i < nb; i++) {
      getOuvrageElementaire(i).fillWithTarageEvolution("", allTarage);
    }
  }

  @Override
  public int getRubarRef() {
    return rubarRef_.getRubarCode();
  }

  public H2dRubarOuvrageElementaireInterface[] getElementaireOuvrages() {
    if (ouvIntern_ == null || ouvIntern_.getSize() == 0) {
      return null;
    }
    final H2dRubarOuvrageElementaireInterface[] r = new H2dRubarOuvrageElementaireInterface[ouvIntern_.getSize()];
    ouvIntern_.toArray(r);
    return r;
  }

  public boolean isMeshesAdjacent(H2dRubarGridAreteSource gridAreteSource) {
    final TIntHashSet set = new TIntHashSet();
    set.addAll(eltIntern_);
    set.add(elt1_);
    if (elt2_ >= 0) {
      set.add(elt2_);
    }
    return isAdjacents(set.toArray(), gridAreteSource);
  }

  public H2dRubarOuvrageElementaireInterface[] getCopyOfElementaireOuvrages() {
    if (ouvIntern_ == null || ouvIntern_.getSize() == 0) {
      return null;
    }
    final H2dRubarOuvrageElementaireInterface[] r = new H2dRubarOuvrageElementaireInterface[ouvIntern_.getSize()];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = getOuvrageElementaire(i).getCopyWithoutEvt();
    }
    return r;
  }

  @Override
  public H2dRubarOuvrageRef getOuvrageRef() {
    return rubarRef_;
  }

  @Override
  public double getXaAmont() {
    return mng_.getGrid().getRubarGrid().getXCentreArete(arete1_);
  }

  public double getZaAmont() {
    return mng_.getGrid().getRubarGrid().getZCentreArete(arete1_);
  }

  @Override
  public double getXaAval() {
    if (isElt2Set()) {
      return mng_.getGrid().getRubarGrid().getXCentreArete(arete2_);
    }
    return mng_.getGrid().getGrid().getMaxX() + 100;
  }

  @Override
  public double getXElementAmont() {
    return mng_.getGrid().getGrid().getMoyCentreXElement(elt1_);
  }

  @Override
  public double getXElementAval() {
    if (isElt2Set()) {
      return mng_.getGrid().getGrid().getMoyCentreXElement(elt2_);
    }
    return mng_.getGrid().getGrid().getMaxX() + 100;
  }

  @Override
  public double getXMailleIntern(final int _i) {
    return mng_.getGrid().getGrid().getMoyCentreXElement(getMailleIntern(_i));
  }

  @Override
  public double getYAmont() {
    final H2dRubarGrid g = mng_.getGrid().getRubarGrid();
    final EfSegment s = g.getArete(arete1_);
    return s.getCentreY(g);
  }

  @Override
  public double getYAval() {
    if (isElt2Set()) {
      return mng_.getGrid().getRubarGrid().getYCentreArete(arete2_);
    }
    return mng_.getGrid().getGrid().getMaxY() + 100;
  }

  @Override
  public double getYElementAmont() {
    return mng_.getGrid().getGrid().getMoyCentreYElement(elt1_);
  }

  @Override
  public double getYELementAval() {
    if (isElt2Set()) {
      return mng_.getGrid().getGrid().getMoyCentreYElement(elt2_);
    }
    return mng_.getGrid().getGrid().getMaxY() + 100;
  }

  @Override
  public double getYMailleIntern(final int _i) {
    return mng_.getGrid().getGrid().getMoyCentreYElement(getMailleIntern(_i));
  }

  protected class OuvrageInternModel extends CtuluListObject {
    /**
     *
     */
    public OuvrageInternModel() {
      super();
    }

    /**
     * @param _init
     */
    public OuvrageInternModel(final Collection _init) {
      super(_init);
    }

    /**
     * @param _init
     */
    public OuvrageInternModel(final CtuluListObject _init) {
      super(_init);
    }

    /**
     * @param _nb
     */
    public OuvrageInternModel(final int _nb) {
      super(_nb);
    }

    @Override
    protected void fireObjectAdded(int _newIdx, Object _newGeom) {
      H2dRubarOuvrage.this.mng_.fireOuvrageChanged(H2dRubarOuvrage.this, true);
    }

    @Override
    protected void fireObjectChanged(int _oldIdx, Object _oldGeom) {
      H2dRubarOuvrage.this.mng_.fireOuvrageChanged(H2dRubarOuvrage.this, true);
    }

    @Override
    protected void fireObjectRemoved(int _oldIdx, Object _oldGeom) {
      H2dRubarOuvrage.this.mng_.fireOuvrageChanged(H2dRubarOuvrage.this, true);
    }

    @Override
    protected void fireObjectModified(int _idx, Object _geom) {
      H2dRubarOuvrage.this.mng_.fireOuvrageChanged(H2dRubarOuvrage.this, true);
    }
  }

  String title_;

  final void setTitle(final String _title) {
    title_ = _title;
  }

  /**
   * @return le titre
   */
  public String getTitle() {
    return title_;
  }

  @Override
  public String toString() {
    return getTitle();
  }

  /**
   * @param _ouv l'ouvrage de ref
   * @param _mng le manager d'ouvrage parent
   * @param _analyze le receveur des messages
   * @param _i l'indice de l'ouvrage
   * @return null si erreur. Ouvrage correctement initialise sinon.
   */
  public static H2dRubarOuvrage buildOuvrage(final H2dRubarOuvrageI _ouv, final H2dRubarOuvrageMng _mng,
                                             final CtuluAnalyze _analyze, final int _i,
                                             final TIntArrayList listOuvrageWithBadCoordinates) {
    final H2dRubarOuvrage r = new H2dRubarOuvrage(_mng);
    r.rubarRef_ = _ouv.getRubarRef() == -1 ? H2dRubarOuvrageRef.COMPUTE_WITH_SAINT_VENANT : H2dRubarOuvrageRef.NO_COMPUTE_WITH_SAINT_VENANT;
    final H2dRubarGrid g = (H2dRubarGrid) _mng.getGrid().getGrid();
    final Coordinate m = new Coordinate();
    // on s'occupe de l'element 1
    r.elt1_ = g.getEltContainingXY(_ouv.getXElementAmont(), _ouv.getYElementAmont(), m);
    final double maxErr = 0.1;
    double dist = 0;

    if (r.elt1_ < 0) {
      if (_analyze != null) {
        _analyze.addError(H2dResource.getS("L'�l�ment de l'ouvrage {0} ne peut pas �tre retrouv�", Integer.toString(_i + 1)), _i);
      }
      return null;
    }
    dist = CtuluLibGeometrie.getDistance(_ouv.getXElementAmont(), _ouv.getYElementAmont(), g.getMoyCentreXElement(r.elt1_),
        g.getMoyCentreYElement(r.elt1_));
    boolean added = false;
    if (dist > maxErr) {
      added = true;
      listOuvrageWithBadCoordinates.add(_i);
    }
    // l'arete 1
    // pas de test serieux. on recupere seulement l'arete de l'element 1
    r.arete1_ = g.getNearestEdgesInElement(_ouv.getXaAmont(), _ouv.getYAmont(), r.elt1_, m);
    EfSegment arete = g.getArete(r.arete1_);
    if (r.arete1_ >= 0) {
      dist = CtuluLibGeometrie.getDistance(_ouv.getXaAmont(), _ouv.getYAmont(), arete.getCentreX(g), arete.getCentreY(g));
      if (dist > maxErr && !added) {
        added = true;
        listOuvrageWithBadCoordinates.add(_i);
      }
    }
    // l'element de sortie. Il peut etre en dehors du maillage donc inf a 0
    r.elt2_ = g.getEltContainingXY(_ouv.getXElementAval(), _ouv.getYELementAval(), m);
    if (r.elt2_ >= 0) {
      // l'arete 2
      r.arete2_ = g.getNearestEdgesInElement(_ouv.getXaAval(), _ouv.getYAval(), r.elt2_, m);
      arete = g.getArete(r.arete2_);
      dist = CtuluLibGeometrie.getDistance(_ouv.getXaAval(), _ouv.getYAval(), arete.getCentreX(g), arete.getCentreY(g));
      if (dist > maxErr && !added) {
        added = true;
        listOuvrageWithBadCoordinates.add(_i);
      }
    }

    final TIntHashSet eltIntern = new TIntHashSet(_ouv.getNbOuvrageElementaires());
    final int nbOuvInter = _ouv.getNbMailleIntern();
    for (int i = 0; i < nbOuvInter; i++) {
      final int idxTmp = g.getEltContainingXY(_ouv.getXMailleIntern(i), _ouv.getYMailleIntern(i), m);
      if (idxTmp < 0) {
        _analyze.addWarn(
            H2dResource.getS("Le point ({0},{1}) n'appartient pas au domaine", Double.toString(_ouv.getXMailleIntern(i)),
                Double.toString(_ouv.getYMailleIntern(i))), -1);
      } else if (idxTmp != r.elt1_ && idxTmp != r.elt2_) {
        dist = CtuluLibGeometrie.getDistance(_ouv.getXMailleIntern(i), _ouv.getYMailleIntern(i), g.getMoyCentreXElement(idxTmp),
            g.getMoyCentreYElement(idxTmp));
        if (dist > maxErr && !added) {
          listOuvrageWithBadCoordinates.add(_i);
          added = true;
        }
        eltIntern.add(idxTmp);
      }
    }
    // on recupere les indices des elements internes
    r.eltIntern_ = eltIntern.toArray();
    // les �l�ments sont mal d�finis
    if (!isSelectionCorrect(r.elt1_, r.elt2_, r.eltIntern_, _mng.getGrid())) {
      if (_analyze != null) {
        _analyze.addError(H2dResource.getS("Les �l�ments de l'ouvrage {0} ne sont pas adjacents", CtuluLibString.getString(_i + 1)), _i);
      }
      // si erreur: on ignore l'�l�ment aval
      if (r.elt2_ >= 0) {
        r.elt2_ = -1;
        r.arete2_ = -1;
      }
      // si erreur persisitente: on ignore les elements internes
      if (!isSelectionCorrect(r.elt1_, r.elt2_, r.eltIntern_, _mng.getGrid())) {
        r.eltIntern_ = null;
      }
    }

    //si un ouvrage a une reference  <> -1, les mailles doivent �tre adjacentes
    if (r.getOuvrageRef() != H2dRubarOuvrageRef.COMPUTE_WITH_SAINT_VENANT) {
      if (!isAdjacents(getAllMeshes(r.elt1_, r.elt2_, r.eltIntern_), _mng.getGrid())) {
        _analyze.addError(H2dResource.getS("L'ouvrage {0} doit avoir une r�f�rence �gale � -1 car contient 2 mailles non adjacents", CtuluLibString.getString(_i + 1)), _i);
      }
    }

    // mise a jour des ouvrages internes
    final List l = new ArrayList();
    final H2dRubarOuvrageElemBuilder builder = new H2dRubarOuvrageElemBuilder();
    builder.setGrid(_mng.getGrid());
    builder.setOuvrageParent(r);
    final int nb = _ouv.getNbOuvrageElementaires();
    for (int i = 0; i < nb; i++) {
      final H2dRubarOuvrageElementaireInterface elt = _ouv.getOuvrageElementaire(i);
      elt.getType().visit(builder, elt);
      if (builder.getLastBuildOuvrageElementaire() != null) {
        l.add(builder.getLastBuildOuvrageElementaire());
      }
    }
    r.buildModel(l);
    return r;
  }

  /**
   * @param _elt1 l'element 1 toujours test�
   * @param _elt2 l'element 2 teste si >=0
   * @param _eltIntern les elements internes
   * @param _grid le maillage support
   * @return si tous adjacents
   */
  public static boolean isSelectionCorrect(final int _elt1, final int _elt2, final int[] _eltIntern,
                                           final H2dRubarGridAreteSource _grid) {
    final int[] sel = getAllMeshes(_elt1, _elt2, _eltIntern);
    if (sel.length == 1) {
      return true;
    }
    return isSelectionCorrect(sel, _grid);
  }

  public static int[] getAllMeshes(int _elt1, int _elt2, int[] _eltIntern) {
    final TIntHashSet set = new TIntHashSet();
    if (_eltIntern != null) {
      set.addAll(_eltIntern);
    }
    set.add(_elt1);
    if (_elt2 >= 0) {
      set.add(_elt2);
    }
    final int[] sel = set.toArray();
    Arrays.sort(sel);
    return sel;
  }

  protected static void parcourt(final int _i, final int[] _selectedIdx, final boolean[] _done, final boolean[] _traverse,
                                 final H2dRubarGridAreteSource _grid) {
    final int[] adjacent = _grid.elementsVoisinsParElement()[_selectedIdx[_i]];
    if (_done[_i]) {
      return;
    }
    _done[_i] = true;
    for (int i = adjacent.length - 1; i >= 0; i--) {
      final int idxInSelected = Arrays.binarySearch(_selectedIdx, adjacent[i]);
      if (idxInSelected >= 0) {
        _traverse[idxInSelected] = true;
        if (!_done[idxInSelected]) {
          parcourt(idxInSelected, _selectedIdx, _done, _traverse, _grid);
        }
      }
    }
  }

  /**
   * @param _selectedIdx la selection
   * @return true si la selection est correcte
   */
  public static boolean isSelectionCorrect(final int[] _selectedIdx, final H2dRubarGridAreteSource _grid) {
    //for 2 meshes, we can select what we want.
    if (_selectedIdx != null && _selectedIdx.length == 2) {
      return true;
    }
    return isAdjacents(_selectedIdx, _grid);
  }

  /**
   * @param _selectedIdx la selection qui sera tri�e
   * @return true si la selection est correcte
   */
  public static boolean isAdjacents(final int[] _selectedIdx, final H2dRubarGridAreteSource _grid) {
    if (_selectedIdx == null || _selectedIdx.length == 0) {
      return false;
    }
    if (_selectedIdx.length == 1) {
      return true;
    }
    Arrays.sort(_selectedIdx);
    final boolean[] traverse = new boolean[_selectedIdx.length];
    parcourt(0, _selectedIdx, new boolean[_selectedIdx.length], traverse, _grid);
    for (int i = traverse.length - 1; i >= 0; i--) {
      if (!traverse[i]) {
        return false;
      }
    }
    return true;
  }

  int arete1_;
  int arete2_;
  H2dRubarOuvrageRef rubarRef_;
  int elt1_;
  int elt2_;
  int[] eltIntern_;
  H2dRubarOuvrageMng mng_;
  CtuluListObject ouvIntern_;
  private static int idxGlobal_;

  private H2dRubarOuvrage(final H2dRubarOuvrageMng _mng) {
    mng_ = _mng;
    title_ = H2dResource.getS("Ouvrage") + CtuluLibString.getEspaceString(++idxGlobal_);
  }

  H2dRubarOuvrage(final H2dRubarOuvrageMng _mng, final int _elt1, final int _arete1, final int _elt2, final int _arete2,
                  final int[] _eltIntern,
                  final H2dRubarOuvrageRef _ref) {
    this(_mng);
    elt1_ = _elt1;
    elt2_ = _elt2;
    arete1_ = _arete1;
    arete2_ = _arete2;
    eltIntern_ = CtuluLibArray.copy(_eltIntern);
    rubarRef_ = _ref;
  }

  /**
   * @param _ref la nouvelle reference de l'ouvrage
   * @param _elem les nouveaux ouvrages elementaires
   * @param _cmd le receveur de commandes
   * @return true si modif
   */
  public boolean setRefAndOuvrageElem(final H2dRubarOuvrageRef _ref, final H2dRubarOuvrageElementaireAbstract[] _elem,
                                      final CtuluCommandContainer _cmd) {
    final CtuluCommandComposite cmd = _cmd == null ? null : new CtuluCommandComposite();
    boolean r = setReference(_ref, cmd);
    r |= setOuvrageElementaire(_elem, cmd);
    if (_cmd != null) {
      _cmd.addCmd(cmd.getSimplify());
    }
    return r;
  }

  /**
   * @param _elem les nouveaux ouvrages elementaires
   * @param _cmd le receveur de commandes
   * @return true si modif
   */
  public boolean setOuvrageElementaire(final H2dRubarOuvrageElementaireAbstract[] _elem, final CtuluCommandContainer _cmd) {
    if ((_elem == null || _elem.length == 0)) {
      if (ouvIntern_ != null && ouvIntern_.getSize() > 0) {
        ouvIntern_.removeAll(_cmd);
        return true;
      }
      return false;
    }
    // max 5.
    if (_elem.length > 5) {
      return false;
    }
    for (int i = _elem.length - 1; i >= 0; i--) {
      _elem[i].setOuvrageParent(this);
    }
    final List l = Arrays.asList(_elem);
    ouvIntern_ = new OuvrageInternModel(l);
    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void undo() {
          ouvIntern_.removeAll();
        }

        @Override
        public void redo() {
          ouvIntern_ = new OuvrageInternModel(l);
          mng_.fireOuvrageChanged(H2dRubarOuvrage.this, true);
        }
      });
    }
    mng_.fireOuvrageChanged(this, true);
    return true;
  }

  /**
   * @param _ref la nouvelle ref de l'ouvrage
   * @param _cmd le receveur de commande
   * @return true si modif
   */
  public boolean setReference(final H2dRubarOuvrageRef _ref, final CtuluCommandContainer _cmd) {
    // nouvelle ref null ou identique a l'ancienne : on ne fait rien
    if (_ref == null || _ref == rubarRef_) {
      return false;
    }
    final H2dRubarOuvrageRef old = rubarRef_;
    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void undo() {
          setReference(old, null);
        }

        @Override
        public void redo() {
          setReference(_ref, null);
        }
      });
    }
    rubarRef_ = _ref;
    if (mng_ != null) {
      mng_.fireOuvrageChanged(this, false);
    }
    return true;
  }

  protected void buildModel(final List _l) {
    ouvIntern_ = new OuvrageInternModel(_l);
  }

  final H2dRubarOuvrageMng getMng() {
    return mng_;
  }

  public final int getArete1() {
    return arete1_;
  }

  public final int getArete2() {
    return arete2_;
  }

  public final int getElt1() {
    return elt1_;
  }

  @Override
  public boolean isElt2Set() {
    return elt2_ >= 0;
  }

  public final int getElt2() {
    return elt2_;
  }

  public final int getMailleIntern(final int _idx) {
    return eltIntern_[_idx];
  }

  @Override
  public final int getNbMailleIntern() {
    return eltIntern_ == null ? 0 : eltIntern_.length;
  }
}
