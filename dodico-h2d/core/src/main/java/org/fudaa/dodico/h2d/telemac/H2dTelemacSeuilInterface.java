/*
 * @creation 3 d�c. 2004
 * @modification $Date: 2006-04-19 13:19:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.telemac;

/**
 * @author Fred Deniger
 * @version $Id: H2dTelemacSeuilInterface.java,v 1.3 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dTelemacSeuilInterface {

  /**
   * @return le nombre de point utilise par ce seuil
   */
  int getNbPoint();

  /**
   * @param _idx l'indice du point dans ce seuil [0;getNbPoint()[
   * @return l'indice de frontiere
   */
  int getPtIdx1(int _idx);

  /**
   * @param _idx l'indice du point dans ce seuil [0;getNbPoint()[
   * @return l'indice de frontiere
   */
  int getPtIdx2(int _idx);

  /**
   * @param _idx l'indice du point dans ce seuil [0;getNbPoint()[
   * @return le coefficient du debit au point demande
   */
  double getCoefDebit(int _idx);

  /**
   * @param _idx l'indice du point dans ce seuil [0;getNbPoint()[
   * @return la cote au point demande.
   */
  double getCote(int _idx);

}