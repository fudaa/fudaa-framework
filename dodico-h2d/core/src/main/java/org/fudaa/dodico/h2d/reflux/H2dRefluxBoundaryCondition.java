/*
 * @creation 25 juin 2003
 * @modification $Date: 2007-05-22 13:11:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluParser;
import org.fudaa.dodico.ef.EfFrontierInterface;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.ef.impl.EfFrontier.FrontierIterator;
import org.fudaa.dodico.h2d.H2dBoundaryCondition;
import org.fudaa.dodico.h2d.H2dEvolutionUseCounter;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionListener;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;
import org.nfunk.jep.Variable;

/**
 * @author deniger
 * @version $Id: H2dRefluxBoundaryCondition.java,v 1.28 2007-05-22 13:11:23 deniger Exp $
 */
public class H2dRefluxBoundaryCondition extends H2dBoundaryCondition {

  /*  *//**
         * @param _ptIdx l'indice du point a cherche
         * @param _l la liste des bord reflux
         * @return le bord reflux contant le point d'indice general _ptIdx
         */
  /*
   * public static H2dRefluxBoundaryCondition getCLFastFromIdxGlobal(final int _ptIdx, final
   * H2dRefluxBoundaryCondition[] _l) { int lowIndex = 0; int highIndex = _l.length - 1; int temp, tempMid; while
   * (lowIndex <= highIndex) { tempMid = (lowIndex + highIndex) >> 1; temp = _l[tempMid].getIndexPt() - _ptIdx; if (temp <
   * 0) { lowIndex = tempMid + 1; } else if (temp > 0) { highIndex = tempMid - 1; } else { return _l[tempMid]; } }
   * return null; }
   */

  public static double computeNormal(final Coordinate _prec, final Coordinate _noeud, final Coordinate _suiv) {
    // Angle defini par le vecteur (noeud,prec)
    final double xalpha1 = _prec.x - _noeud.x;
    final double yalpha1 = _prec.y - _noeud.y;
    double alpha1;
    double alpha2;
    double xalpha2;
    if (xalpha1 != 0 || yalpha1 != 0) {
      alpha1 = Math.atan2(yalpha1, xalpha1);
    } else {
      alpha1 = 0;
    }
    // Angle defini par le vecteur (noeud,suiv)
    xalpha2 = _suiv.x - _noeud.x;
    final double yalpha2 = _suiv.y - _noeud.y;
    if (xalpha2 == 0 && yalpha2 == 0) {
      alpha2 = 0;
    } else {
      alpha2 = Math.atan2(yalpha2, xalpha2);
    }
    // Angle de la normale
    double valeur = (alpha1 + alpha2) / 2 * 180 / Math.PI;
    if (alpha2 - alpha1 <= 0) {
      valeur += 180;
    }
    return valeur;
  }

  public static void initializeNormales(final EfGridInterface _grid, final H2dRefluxBoundaryCondition[] _cls,
      final int _idxFr) {
    if (_grid == null || CtuluLibArray.isEmpty(_cls)) {
      return;
    }
    final EfFrontierInterface fr = _grid.getFrontiers();
    final Coordinate noeud = new Coordinate();
    final Coordinate prec = new Coordinate();
    final Coordinate suiv = new Coordinate();
    int current = 0;
    final int nb = fr.getNbPt(_idxFr);
    final FrontierIterator it = fr.getFrontierIterator(_idxFr);
    int idxGlSuivant;
    it.setReverse(true);
    it.next();
    int idxGlPrec = it.getGlobalIdx();
    it.setReverse(false);
    it.next();
    int idxGl = it.getGlobalIdx();
    for (int j = 0; j < nb; j++) {
      it.next();
      idxGlSuivant = it.getGlobalIdx();
      _grid.getCoord(idxGl, noeud);
      _grid.getCoord(idxGlPrec, prec);
      _grid.getCoord(idxGlSuivant, suiv);
      if (current >= _cls.length) {
        new Throwable().printStackTrace();
        break;
      }
      _cls[current++].setNormale(computeNormal(prec, noeud, suiv));
      idxGlPrec = idxGl;
      idxGl = idxGlSuivant;

    }
  }

  protected H2dBcType hType_;

  protected H2dBcType uType_;

  protected H2dBcType vType_;

  EvolutionReguliereAbstract hTransitoireCourbe_;

  double normale_;

  double q_;

  EvolutionReguliereAbstract qTransitoireCourbe_;

  H2dBcType qType_;

  EvolutionReguliereAbstract uTransitoireCourbe_;

  EvolutionReguliereAbstract vTransitoireCourbe_;

  public H2dRefluxBoundaryCondition() {
    super();
    uType_ = H2dBcType.LIBRE;
    vType_ = H2dBcType.LIBRE;
    hType_ = H2dBcType.LIBRE;
    qType_ = H2dBcType.LIBRE;
  }

  /**
   * @param _l la condition utilisee pour l'initialisation
   */
  public H2dRefluxBoundaryCondition(final H2dRefluxBoundaryCondition _l) {
    super(_l);
    if (_l == null) {
      uType_ = H2dBcType.LIBRE;
      vType_ = H2dBcType.LIBRE;
      hType_ = H2dBcType.LIBRE;
      qType_ = H2dBcType.LIBRE;
    } else {
      internInit(_l);
    }
  }

  protected void copyCurve(final EvolutionListener _l) {
    if (uTransitoireCourbe_ != null) {
      uTransitoireCourbe_ = (EvolutionReguliereAbstract) uTransitoireCourbe_.getCopy(_l);
    }
    if (vTransitoireCourbe_ != null) {
      vTransitoireCourbe_ = (EvolutionReguliereAbstract) vTransitoireCourbe_.getCopy(_l);
    }
    if (hTransitoireCourbe_ != null) {
      hTransitoireCourbe_ = (EvolutionReguliereAbstract) hTransitoireCourbe_.getCopy(_l);
    }
    if (qTransitoireCourbe_ != null) {
      qTransitoireCourbe_ = (EvolutionReguliereAbstract) qTransitoireCourbe_.getCopy(_l);
    }

  }

  /**
   * A method than can be used by the constructor.
   */
  private void internInit(final H2dRefluxBoundaryCondition _cond) {
    if (_cond != null) {
      super.initValueFrom(_cond);
      uType_ = _cond.uType_;
      vType_ = _cond.vType_;
      hType_ = _cond.hType_;
      qType_ = _cond.qType_;
      q_ = _cond.q_;
      uTransitoireCourbe_ = _cond.uTransitoireCourbe_;
      vTransitoireCourbe_ = _cond.vTransitoireCourbe_;
      hTransitoireCourbe_ = _cond.hTransitoireCourbe_;
      qTransitoireCourbe_ = _cond.qTransitoireCourbe_;
      normale_ = _cond.normale_;
    }
  }

  public H2dBcType getType(final H2dVariableType _target) {
    if (_target == H2dVariableType.COTE_EAU) {
      return hType_;
    } else if (_target == H2dVariableType.VITESSE_NORMALE) {
      return uType_;
    } else if (_target == H2dVariableType.VITESSE_TANGENTIELLE) {
      return vType_;
    } else if (_target == H2dVariableType.DEBIT) {
      return qType_;
    } else {
      error(_target.getName());
    }
    return null;
  }

  private static void error(final String _name) {
    FuLog.error(_name + " not supported");
  }

  @Override
  protected void initFrom(final H2dBoundaryCondition _cond) {
    if (_cond instanceof H2dRefluxBoundaryCondition) {
      initFrom((H2dRefluxBoundaryCondition) _cond);
    }
  }

  protected void initFrom(final H2dRefluxBoundaryCondition _cond) {
    super.initFrom(_cond);
    initValueFrom(_cond);
  }

  @Override
  protected void initValueFrom(final H2dBoundaryCondition _cond) {
    if (_cond instanceof H2dRefluxBoundaryCondition) {
      initValueFrom((H2dRefluxBoundaryCondition) _cond);
    }
  }

  protected void initValueFrom(final H2dRefluxBoundaryCondition _cond) {
    super.initValueFrom(_cond);
    internInit(_cond);
  }

  protected void replaceEvol(final Map _evolEquivEvol) {
    if ((uTransitoireCourbe_ != null) && (_evolEquivEvol.containsKey(uTransitoireCourbe_))) {
      uTransitoireCourbe_ = (EvolutionReguliereAbstract) _evolEquivEvol.get(uTransitoireCourbe_);
      uTransitoireCourbe_.setUsed(false);
    }
    if ((vTransitoireCourbe_ != null) && (_evolEquivEvol.containsKey(vTransitoireCourbe_))) {
      vTransitoireCourbe_ = (EvolutionReguliereAbstract) _evolEquivEvol.get(vTransitoireCourbe_);
      vTransitoireCourbe_.setUsed(false);
    }
    if ((hTransitoireCourbe_ != null) && (_evolEquivEvol.containsKey(hTransitoireCourbe_))) {
      hTransitoireCourbe_ = (EvolutionReguliereAbstract) _evolEquivEvol.get(hTransitoireCourbe_);
      hTransitoireCourbe_.setUsed(false);
    }
    if ((qTransitoireCourbe_ != null) && (_evolEquivEvol.containsKey(qTransitoireCourbe_))) {
      qTransitoireCourbe_ = (EvolutionReguliereAbstract) _evolEquivEvol.get(qTransitoireCourbe_);
      qTransitoireCourbe_.setUsed(false);
    }

  }

  /**
   * Permet de reinitialiser correctement les utilisations de evolutions de _newBoundary par rapport a celle-ci. A
   * appeler lorque _newBoundary doit remplacer cette condition.
   */
  protected void restoreEvolUsed(final H2dRefluxBoundaryCondition _newBoundary) {
    if (uTransitoireCourbe_ != _newBoundary.uTransitoireCourbe_) {
      if (uTransitoireCourbe_ != null) {
        uTransitoireCourbe_.setUnUsed(false);
      }
      if (_newBoundary.uTransitoireCourbe_ != null) {
        _newBoundary.uTransitoireCourbe_.setUsed(false);
      }
    }
    if (vTransitoireCourbe_ != _newBoundary.vTransitoireCourbe_) {
      if (vTransitoireCourbe_ != null) {
        vTransitoireCourbe_.setUnUsed(false);
      }
      if (_newBoundary.vTransitoireCourbe_ != null) {
        _newBoundary.vTransitoireCourbe_.setUsed(false);
      }
    }
    if (hTransitoireCourbe_ != _newBoundary.hTransitoireCourbe_) {
      if (hTransitoireCourbe_ != null) {
        hTransitoireCourbe_.setUnUsed(false);
      }
      if (_newBoundary.hTransitoireCourbe_ != null) {
        _newBoundary.hTransitoireCourbe_.setUsed(false);
      }
    }
    if (qTransitoireCourbe_ != _newBoundary.qTransitoireCourbe_) {
      if (qTransitoireCourbe_ != null) {
        qTransitoireCourbe_.setUnUsed(false);
      }
      if (_newBoundary.qTransitoireCourbe_ != null) {
        _newBoundary.qTransitoireCourbe_.setUsed(false);
      }
    }
    if (_newBoundary.isMiddleWithFriction() && (_newBoundary.getMiddleFriction().getFrictionEvolution() != null)) {
      _newBoundary.getMiddleFriction().getFrictionEvolution().setUsed(false);
    }
  }

  protected boolean setFree(final H2dVariableType _t) {
    if (_t == H2dVariableType.VITESSE_NORMALE) {
      return setUTypeFree();
    } else if (_t == H2dVariableType.VITESSE_TANGENTIELLE) {
      return setVTypeFree();
    } else if (_t == H2dVariableType.COTE_EAU) {
      return setHTypeFree();
    } else if (_t == H2dVariableType.DEBIT) {
      return setQTypeFree();
    } else {
      error(_t.getName());
    }
    return false;
  }

  protected boolean setH(final double _new) {
    if ((hType_ == H2dBcType.PERMANENT) && (hOrSl_ != _new)) {
      hOrSl_ = _new;
      return true;
    }
    return false;
  }

  protected boolean setHTransitoire(final EvolutionReguliereAbstract _evol) {
    if ((_evol != null) && ((hType_ == H2dBcType.TRANSITOIRE) || (hTransitoireCourbe_ != _evol))) {
      hType_ = H2dBcType.TRANSITOIRE;
      if (hTransitoireCourbe_ != null) {
        hTransitoireCourbe_.setUnUsed(false);
      }
      hTransitoireCourbe_ = _evol;
      hTransitoireCourbe_.setUsed(false);
      return true;
    }
    return false;
  }

  /**
   *
   */
  protected boolean setHTypeFree() {
    if (hType_ == H2dBcType.LIBRE) {
      return false;
    }
    hType_ = H2dBcType.LIBRE;
    if (hTransitoireCourbe_ != null) {
      hTransitoireCourbe_.setUnUsed(false);
    }
    hTransitoireCourbe_ = null;
    hOrSl_ = 0;
    return true;
  }

  protected boolean setHTypePermanent(final double _d) {
    if ((hType_ != H2dBcType.PERMANENT) || (hOrSl_ != _d)) {
      hType_ = H2dBcType.PERMANENT;
      hOrSl_ = _d;
      if (hTransitoireCourbe_ != null) {
        hTransitoireCourbe_.setUnUsed(false);
      }
      hTransitoireCourbe_ = null;
      return true;
    }
    return false;
  }

  /**
   *
   */
  protected boolean setHTypePermanentAndNull() {
    return setHTypePermanent(0);
  }

  protected void setIndexPt(final int _idx) {
    indexPt_ = _idx;
  }

  protected boolean setNormale(final double _d) {
    if (_d != normale_) {
      normale_ = _d;
      return true;
    }
    return false;
  }

  protected boolean setNormale(final H2dRefluxValue _val) {
    if (_val == null) {
      return false;
    }
    if (_val.getOldExpr() == null) {
      return setNormale(_val.getValue());
    }
    final CtuluParser parser = _val.getOldExpr();
    final Variable var = parser.getVar(CtuluParser.getOldVariable());
    if (var == null) {
      return false;
    }
    var.setValue(CtuluLib.getDouble(normale_));
    return setNormale(parser.getValue());
  }

  protected boolean setPermanent(final H2dVariableType _t, final double _v) {
    if (_t == H2dVariableType.VITESSE_NORMALE) {
      return setUTypePermanent(_v);
    } else if (_t == H2dVariableType.VITESSE_TANGENTIELLE) {
      return setVTypePermanent(_v);
    } else if (_t == H2dVariableType.COTE_EAU) {
      return setHTypePermanent(_v);
    } else if (_t == H2dVariableType.DEBIT) {
      return setQTypePermanent(_v);
    } else {
      error(_t.getName());
    }
    return false;
  }

  protected boolean setQ(final double _d) {
    if ((qType_ == H2dBcType.PERMANENT) && (q_ != _d)) {
      q_ = _d;
      return true;
    }
    return false;
  }

  protected boolean setQTransitoire(final EvolutionReguliereAbstract _evol) {
    if ((_evol != null) && ((qType_ != H2dBcType.TRANSITOIRE) || (qTransitoireCourbe_ != _evol))) {
      qType_ = H2dBcType.TRANSITOIRE;
      if (qTransitoireCourbe_ != null) {
        qTransitoireCourbe_.setUnUsed(false);
      }
      qTransitoireCourbe_ = _evol;
      qTransitoireCourbe_.setUsed(false);
      return true;
    }
    return false;
  }

  protected boolean setQTypeFree() {
    if (qType_ == H2dBcType.LIBRE) {
      return false;
    }
    qType_ = H2dBcType.LIBRE;
    if (qTransitoireCourbe_ != null) {
      qTransitoireCourbe_.setUnUsed(false);
    }
    qTransitoireCourbe_ = null;
    q_ = 0;
    return true;
  }

  protected boolean setQTypePermanent(final double _d) {
    if ((qType_ != H2dBcType.PERMANENT) || (q_ != _d)) {
      qType_ = H2dBcType.PERMANENT;
      q_ = _d;
      if (qTransitoireCourbe_ != null) {
        qTransitoireCourbe_.setUnUsed(false);
      }
      qTransitoireCourbe_ = null;
      return true;
    }
    return false;
  }

  protected boolean setQTypePermanentAndNull() {
    return setQTypePermanent(0);
  }

  protected boolean setTransitoire(final H2dVariableType _t, final EvolutionReguliereAbstract _v) {
    if (_v == null) {
      return false;
    }
    if (_t == H2dVariableType.VITESSE_NORMALE) {
      return setUTransitoire(_v);
    } else if (_t == H2dVariableType.VITESSE_TANGENTIELLE) {
      return setVTransitoire(_v);
    } else if (_t == H2dVariableType.COTE_EAU) {
      return setHTransitoire(_v);
    } else if (_t == H2dVariableType.DEBIT) {
      return setQTransitoire(_v);
    } else {
      error(_t.getName());
    }
    return false;
  }

  protected boolean setU(final double _d) {
    if ((uType_ == H2dBcType.PERMANENT) && (u_ != _d)) {
      u_ = _d;
      return true;
    }
    return false;
  }

  protected boolean setUTransitoire(final EvolutionReguliereAbstract _evol) {
    if ((_evol != null) && ((uType_ == H2dBcType.TRANSITOIRE) || (uTransitoireCourbe_ != _evol))) {
      uType_ = H2dBcType.TRANSITOIRE;
      if (uTransitoireCourbe_ != null) {
        uTransitoireCourbe_.setUnUsed(false);
      }
      uTransitoireCourbe_ = _evol;
      uTransitoireCourbe_.setUsed(false);
      return true;
    }
    return false;
  }

  protected boolean setUTypeFree() {
    if (uType_ == H2dBcType.LIBRE) {
      return false;
    }
    uType_ = H2dBcType.LIBRE;
    if (uTransitoireCourbe_ != null) {
      uTransitoireCourbe_.setUnUsed(false);
    }
    uTransitoireCourbe_ = null;
    u_ = 0;
    return true;
  }

  protected boolean setUTypePermanent(final double _d) {
    if ((uType_ != H2dBcType.PERMANENT) || (u_ != _d)) {
      uType_ = H2dBcType.PERMANENT;
      u_ = _d;
      if (uTransitoireCourbe_ != null) {
        uTransitoireCourbe_.setUnUsed(false);
      }
      uTransitoireCourbe_ = null;
      return true;
    }
    return false;
  }

  protected boolean setUTypePermanentAndNull() {
    return setUTypePermanent(0);
  }

  protected boolean setV(final double _d) {
    if ((vType_ == H2dBcType.PERMANENT) && (v_ != _d)) {
      v_ = _d;
      return true;
    }
    return false;
  }

  protected boolean setValue(final H2dVariableType _t, final H2dBcType _type, final double _v,
      final EvolutionReguliereAbstract _e) {
    if (_type == H2dBcType.LIBRE) {
      return setFree(_t);
    } else if (_type == H2dBcType.PERMANENT) {
      return setPermanent(_t, _v);
    } else if (_type == H2dBcType.TRANSITOIRE) {
      if (_e == null) {
        new Throwable("e is null").printStackTrace();
      }
      return setTransitoire(_t, _e);
    }
    new Throwable(_t + " is not supported");
    return false;
  }

  protected boolean setValues(final Map _m) {
    boolean r = false;
    for (final Iterator it = _m.entrySet().iterator(); it.hasNext();) {
      final Map.Entry e = (Map.Entry) it.next();
      r |= updateCompleteValue((H2dVariableType) e.getKey(), (H2dRefluxValue) e.getValue());
    }
    return r;
  }

  protected boolean setVTransitoire(final EvolutionReguliereAbstract _evol) {
    if ((_evol != null) && ((hType_ == H2dBcType.TRANSITOIRE) || (vTransitoireCourbe_ != _evol))) {
      vType_ = H2dBcType.TRANSITOIRE;
      if (vTransitoireCourbe_ != null) {
        vTransitoireCourbe_.setUnUsed(false);
      }
      vTransitoireCourbe_ = _evol;
      vTransitoireCourbe_.setUsed(false);
      return true;
    }
    return false;
  }

  protected boolean setVTypeFree() {
    if (vType_ == H2dBcType.LIBRE) {
      return false;
    }
    vType_ = H2dBcType.LIBRE;
    if (vTransitoireCourbe_ != null) {
      vTransitoireCourbe_.setUnUsed(false);
    }
    vTransitoireCourbe_ = null;
    v_ = 0;
    return true;
  }

  protected boolean setVTypePermanent(final double _d) {
    if ((vType_ != H2dBcType.PERMANENT) || (v_ != _d)) {
      vType_ = H2dBcType.PERMANENT;
      v_ = _d;
      if (vTransitoireCourbe_ != null) {
        vTransitoireCourbe_.setUnUsed(false);
      }
      vTransitoireCourbe_ = null;
      return true;
    }
    return false;
  }

  protected boolean setVTypePermanentAndNull() {
    return setVTypePermanent(0);
  }

  protected boolean updateCompleteValue(final H2dVariableType _t, final H2dRefluxValue _toSet) {
    if (_toSet == null) {
      new Throwable(_t.getName() + " can't be set").printStackTrace();
      return false;
    }
    final H2dBcType init = getType(_t);
    if (init == null) {
      new Throwable("unused variable " + _t.getName()).printStackTrace();
      return false;
    }
    if (_toSet.isTypeDetermined()) {
      final H2dBcType type = _toSet.getType();
      double val = _toSet.getValue();
      // dans le cas d'utilisation d'une formule
      if (type == H2dBcType.PERMANENT && _toSet.getOldExpr() != null) {
        final Variable oldVar = _toSet.getOldExpr().getVar(CtuluParser.getOldVariable());
        if (oldVar != null) {
          double oldVal = getValue(_t);
          // pour s'assurer qu'on utilise bien 0 si l'ancienne valeur est nulle.
          if (init == H2dBcType.LIBRE) {
            oldVal = 0;
          } else if (init == H2dBcType.TRANSITOIRE && getEvolution(_t).getNbValues() > 0) {
            oldVal = getEvolution(_t).getY(0);
          }
          oldVar.setValue(CtuluLib.getDouble(oldVal));
        }
        val = _toSet.getOldExpr().getValue();
      }
      // formule traitee: la nouvelle valeur est dans "val".
      if (type != init) {
        setValue(_t, type, val, _toSet.getEvolution());
        if ((type == H2dBcType.TRANSITOIRE) && (_toSet.getEvolution() == null)) {
          setPermanent(_t, 0);
        }
        return true;
      }
      if ((type == H2dBcType.PERMANENT) && (_toSet.isDoubleValueConstant())) {
        return setPermanent(_t, val);
      } else if ((type == H2dBcType.TRANSITOIRE) && (_toSet.isEvolutionFixed())) {
        return setTransitoire(_t, _toSet.getEvolution());
      }
    }
    return false;
  }

  /**
   * @return true si contient des donnees transitoires
   */
  public boolean containsClTransient() {
    return (uType_ == H2dBcType.TRANSITOIRE) || (vType_ == H2dBcType.TRANSITOIRE) || (hType_ == H2dBcType.TRANSITOIRE);
  }

  /**
   * @return true si le debit est transitoire
   */
  public boolean containsPnTransient() {
    return qType_ == H2dBcType.TRANSITOIRE;
  }

  /**
   * @return une nouvelle instance initialisee avec les donnees de celle-ci
   */
  public H2dRefluxBoundaryCondition createCopy() {
    return new H2dRefluxBoundaryCondition(this);
  }

  /**
   * @param _r la table a remplir avec les evolutions utilisees ( par type de variable)
   */
  public void fillWithEvolVar(final H2dEvolutionUseCounter _r) {
    final int idx = getIndexPt();
    if (uTransitoireCourbe_ != null) {
      _r.add(uTransitoireCourbe_, H2dVariableType.VITESSE_NORMALE, idx);
    }
    if (vTransitoireCourbe_ != null) {
      _r.add(vTransitoireCourbe_, H2dVariableType.VITESSE_TANGENTIELLE, idx);
    }
    if (hTransitoireCourbe_ != null) {
      _r.add(hTransitoireCourbe_, H2dVariableType.COTE_EAU, idx);
    }
    if (qTransitoireCourbe_ != null) {
      _r.add(qTransitoireCourbe_, H2dVariableType.DEBIT, idx);
    }
  }

  /**
   * @param _r la table a remplir avec les evolutions utilisees
   */
  public void fillWithUsedEvolution(final Set _r) {
    if (uTransitoireCourbe_ != null) {
      _r.add(uTransitoireCourbe_);
    }
    if (vTransitoireCourbe_ != null) {
      _r.add(vTransitoireCourbe_);
    }
    if (hTransitoireCourbe_ != null) {
      _r.add(hTransitoireCourbe_);
    }
    if (qTransitoireCourbe_ != null) {
      _r.add(qTransitoireCourbe_);
    }
  }

  /**
   * @param _t la variable demande
   * @param _valueToFill la valeur a modifier avec les valeurs contenues dans cette condition
   * @return true si la variable _t est bien supportee par cette condition.
   */
  public boolean fillWithValue(final H2dVariableType _t, final H2dRefluxValue _valueToFill) {
    if (_t == H2dVariableType.COTE_EAU) {
      fillWithValueH(_valueToFill);
    } else if (_t == H2dVariableType.VITESSE_NORMALE) {
      fillWithValueU(_valueToFill);
    } else if (_t == H2dVariableType.VITESSE_TANGENTIELLE) {
      fillWithValueV(_valueToFill);
    } else if (_t == H2dVariableType.DEBIT) {
      fillWithValueQ(_valueToFill);
    } else {
      return false;
    }
    return true;
  }

  /**
   * @param _valueToFill la variable a modifie avec les valeurs concernant la hauteur d'eau
   */
  public void fillWithValueH(final H2dRefluxValue _valueToFill) {
    _valueToFill.setAllValues(hType_, hOrSl_, hTransitoireCourbe_);
  }

  /**
   * @param _valueToFill la variable a modifie avec les valeurs concernant le debit
   */
  public void fillWithValueQ(final H2dRefluxValue _valueToFill) {
    _valueToFill.setAllValues(qType_, q_, qTransitoireCourbe_);
  }

  /**
   * @param _valueToFill la variable a modifie avec les valeurs concernant la vitesse u
   */
  public void fillWithValueU(final H2dRefluxValue _valueToFill) {
    _valueToFill.setAllValues(uType_, u_, uTransitoireCourbe_);
  }

  /**
   * @param _valueToFill la variable a modifie avec les valeurs concernant la vitesse v
   */
  public void fillWithValueV(final H2dRefluxValue _valueToFill) {
    _valueToFill.setAllValues(vType_, v_, vTransitoireCourbe_);
  }

  /**
   * @param _v la variable demande
   * @return l'evolution correspondante. peut etre null.
   */
  public EvolutionReguliereAbstract getEvolution(final H2dVariableType _v) {
    if (_v == H2dVariableType.VITESSE_NORMALE) {
      return uTransitoireCourbe_;
    } else if (_v == H2dVariableType.VITESSE_TANGENTIELLE) {
      return vTransitoireCourbe_;
    } else if (_v == H2dVariableType.COTE_EAU) {
      return hTransitoireCourbe_;
    } else if (_v == H2dVariableType.DEBIT) {
      return qTransitoireCourbe_;
    }
    return null;
  }

  @Override
  public double getSurfaceLibre() {
    return hOrSl_;
  }

  /**
   * @return la courbe utilisee pour la hauteur d'eau
   */
  public EvolutionReguliereInterface getHCourbe() {
    return getHTransitoireCourbe();
  }

  /**
   * @return la courbe utilisee pour la hauteur d'eau
   */

  public EvolutionReguliereAbstract getHTransitoireCourbe() {
    return hTransitoireCourbe_;
  }

  /**
   * @return le type pour la hauteur (fixe,transitoire, libre)
   */

  public H2dBcType getHType() {
    return hType_;
  }

  /**
   * @return null;
   */
  public H2dRefluxBoundaryConditionMiddle getMiddle() {
    return null;
  }

  /**
   * @return null;
   */
  public H2dRefluxBoundaryConditionMiddleFriction getMiddleFriction() {
    return null;
  }

  /**
   * @return la normale
   */
  public double getNormale() {
    return normale_;
  }

  /**
   * Le debit lineaire.
   */
  @Override
  public double getQ() {
    return q_;
  }

  /**
   * @return la courbe transitoire utilisee pour le debit lineaire
   */
  public EvolutionReguliereInterface getQCourbe() {
    return getQTransitoireCourbe();
  }

  /**
   * @return la courbe transitoire utilisee pour le debit lineaire
   */
  public EvolutionReguliereAbstract getQTransitoireCourbe() {
    return qTransitoireCourbe_;
  }

  /**
   * @return le type du debit lineaire
   */
  public H2dBcType getQType() {
    return qType_;
  }

  /**
   * @return la courbe transitoire pour la vitesse u.
   */
  public EvolutionReguliereInterface getUCourbe() {
    return getUTransitoireCourbe();
  }

  /**
   * @return la courbe transitoire pour la vitesse u.
   */
  public EvolutionReguliereAbstract getUTransitoireCourbe() {
    return uTransitoireCourbe_;
  }

  /**
   * @return le comportement de la vitesse u.
   */
  public H2dBcType getUType() {
    return uType_;
  }

  /**
   * @return la courbe transitoire pour la vitesse v.
   */
  public EvolutionReguliereInterface getVCourbe() {
    return getVTransitoireCourbe();
  }

  /**
   * @return la courbe transitoire pour la vitesse v.
   */
  public EvolutionReguliereAbstract getVTransitoireCourbe() {
    return vTransitoireCourbe_;
  }

  /**
   * @return la comportement de la vitesse v.
   */
  public H2dBcType getVType() {
    return vType_;
  }

  /**
   * Initialise l'utilisation des courbes transitoires.
   */
  public void initUsedEvol() {
    if (uTransitoireCourbe_ != null) {
      uTransitoireCourbe_.setUsed(false);
    }
    if (vTransitoireCourbe_ != null) {
      vTransitoireCourbe_.setUsed(false);
    }
    if (hTransitoireCourbe_ != null) {
      hTransitoireCourbe_.setUsed(false);
    }
    if (qTransitoireCourbe_ != null) {
      qTransitoireCourbe_.setUsed(false);
    }

  }

  /**
   * @return false
   */
  public boolean isMiddle() {
    return false;
  }

  /**
   * @return false
   */
  public boolean isMiddleWithFriction() {
    return false;
  }

  /**
   * @return true si cette condition est considere comme etant une condition de bord ouvert
   */
  public boolean isOpenInUVH() {
    // on teste u: le plus sur
    if ((uType_ == H2dBcType.LIBRE) || (uType_ == H2dBcType.TRANSITOIRE) || (hType_ != H2dBcType.LIBRE)
        || (vType_ == H2dBcType.TRANSITOIRE)) {
      return true;
    }
    if ((uType_ == H2dBcType.PERMANENT) && (u_ != 0)) {
      return true;
    }
    if ((vType_ == H2dBcType.PERMANENT) && (v_ != 0)) {
      return true;
    }
    // si h impos� ?
    return false;

  }

  @Override
  public String toString() {
    return super.toString() + " u " + uType_ + " v" + vType_ + " h" + hType_;
  }
}
