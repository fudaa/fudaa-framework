/*
 *  @creation     11 juil. 2005
 *  @modification $Date: 2007-06-11 13:06:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import com.memoire.bu.BuValueValidator;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.collection.CtuluAdapterDoubleInverse;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.H2dVariableProviderInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * @author Fred Deniger
 * @version $Id: H2dRefluxVarEltProvider.java,v 1.13 2007-06-11 13:06:09 deniger Exp $
 */
public final class H2dRefluxVarEltProvider implements H2dVariableProviderInterface {
  final EfGridInterface grid_;
  final boolean addFr_;
  final H2dRefluxElementPropertyMngAbstract props_;

  /**
   * @param _params
   */
  public H2dRefluxVarEltProvider(final H2dRefluxParameters _params, final boolean _addFr) {
    grid_ = _params.getMaillage();
    props_ = _params.getElementPropMng();
    addFr_ = _addFr;
  }

  @Override
  public EfGridInterface getGrid() {
    return grid_;
  }

  @Override
  public CtuluCollectionDouble getViewedModel(final H2dVariableType _t) {
    return getModifiableModel(_t);
  }

  @Override
  public CtuluCollectionDouble getValues(CtuluVariable _var) {
    return getViewedModel((H2dVariableType) _var);
  }

  @Override
  public Collection getUsableVariables() {
    if (addFr_) {
      final ArrayList l = new ArrayList(props_.getVarList());
      l.add(H2dVariableType.COEF_FROTTEMENT_FOND);
      return l;
    }
    return props_.getVarList();
  }

  @Override
  public H2dVariableType[] getVarToModify() {
    final H2dVariableType[] vars = new H2dVariableType[props_.getVarList().size()];
    props_.getVarList().toArray(vars);
    return vars;
  }

  @Override
  public boolean containsVarToModify() {
    return props_.getVarList() != null && props_.getVarList().size() > 0;
  }

  @Override
  public CtuluCollectionDoubleEdit getModifiableModel(final H2dVariableType _t) {
    if (_t == H2dVariableType.COEF_FROTTEMENT_FOND) {
      final CtuluCollectionDoubleEdit model = getModifiableModel(H2dVariableType.RUGOSITE);
      if (model != null) {
        return new CtuluAdapterDoubleInverse(model);
      }
    }
    return props_.getEltProp(_t).createEditInterface();
  }

  @Override
  public BuValueValidator getValidator(final H2dVariableType _t) {
    return BuValueValidator.DOUBLE;
  }

  @Override
  public CtuluNumberFormatI getFormater(final H2dVariableType _t) {
    return null;
  }

  @Override
  public int getVariablesBlockLength() {
    return -1;
  }

  @Override
  public boolean isElementVar() {
    return true;
  }

  public void setValues(final H2dVariableType _var, final int[] _idxPtElt, final double _values,
                        final CtuluCommandContainer _cmd) {
    final H2dRefluxElementProperty prop = props_.getEltProp(_var);
    if (prop != null) {
      final double[] vals = new double[_idxPtElt.length];
      Arrays.fill(vals, _values);
      final CtuluCommand cmd = prop.setDoubleValues(_idxPtElt, vals, false);
      if (_cmd != null && cmd != null) {
        _cmd.addCmd(cmd);
      }
    }
  }

  public void setValues(final H2dVariableType _var, final int[] _idxPtElt, final double[] _values,
                        final CtuluCommandContainer _cmd) {
    final H2dRefluxElementProperty prop = props_.getEltProp(_var);
    if (prop != null) {
      final CtuluCommand cmd = prop.setDoubleValues(_idxPtElt, _values, true);
      if (_cmd != null && cmd != null) {
        _cmd.addCmd(cmd);
      }
    }
  }
}
