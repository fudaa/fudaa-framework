/*
 * @creation 26 juin 2003
 * 
 * @modification $Date: 2006-09-19 14:43:25 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author deniger
 * @version $Id: H2dBoundary.java,v 1.20 2006-09-19 14:43:25 deniger Exp $
 */
public class H2dBoundary {

  /**
   * Aucune intersection.
   */
  public final static int INTERSECT_NULL = -1;

  /**
   * Intersection partielle.
   */
  public final static int INTERSECT_PARTIEL = 0;

  /**
   * Intersection est strictement inclue dans ce bord.
   */
  public final static int INTERSECT_INCLUDE_IN_BOUNDARY = 1;

  /**
   * Intersection contient le bord.
   */
  public final static int INTERSECT_CONTAINS_BOUNDARY = 2;

  protected H2dBoundaryType type_;

  protected int idxDeb_;

  protected int idxFin_;

  String name_;

  /**
   * La frontiere concerne par ce bord (0 =frontiere externe).
   */
  int idxmaillageFrontiere_;

  /**
   * type=null.
   */
  public H2dBoundary(final int _idxFr) {
    idxmaillageFrontiere_ = _idxFr;
  }

  /**
   * @param _b le bord servant pour l'initialisation
   */
  public H2dBoundary(final H2dBoundary _b) {
    type_ = _b.type_;
    idxDeb_ = _b.idxDeb_;
    idxFin_ = _b.idxFin_;
    name_ = _b.name_;
    idxmaillageFrontiere_ = _b.idxmaillageFrontiere_;
  }

  /**
   * @param _minSelect l'indice min selectionne
   * @param _maxSelect l'indice max selectionne
   * @return true si l'indice de debut est selectionne par [_minSelect,_maxSelect]
   */
  public boolean isBeginIdxSelected(final int _minSelect, final int _maxSelect) {
    return (((_minSelect <= _maxSelect) && ((_minSelect <= idxDeb_) && (_maxSelect >= idxDeb_))) || ((_minSelect > _maxSelect) && ((idxDeb_ >= _minSelect) || (idxDeb_ <= _maxSelect))));
  }

  /**
   * @param _min le min a tester
   * @param _minSelect l'indice min selectionne
   * @param _maxSelect l'indice max selectionne
   * @return true si min est selectionne par [_minSelect,_maxSelect]
   */
  public final static boolean isBeginIdxSelected(final int _min, final int _minSelect, final int _maxSelect) {
    return (((_minSelect <= _maxSelect) && ((_minSelect <= _min) && (_maxSelect >= _min))) || ((_minSelect > _maxSelect) && ((_min >= _minSelect) || (_min <= _maxSelect))));
  }

  /**
   * On considere que les bord liquides gagnent par rapport au bord solid.
   * 
   * @param _min l'indice _min de selection
   * @param _max l'indice _max de selection
   * @param _nbPt le nombre de point de la frontiere (utile pour les bords qui passent par l'origine).
   * @return INTERSECT_NULL si pas d'intersection <br>
   *         INTERSECT_PARTIEL si intersection partielle <br>
   *         INTERSECT_INCLUDE_IN_BORD si [_minSelect,_maxSelect] est strictement inclus de ce bord <br>
   *         INTERSECT_TOTAL si ce bord est inclus dans la selection
   */
  public int isIntersectBy(final int _min, final int _max, final int _nbPt) {
    if (isUnique()) {
      return INTERSECT_INCLUDE_IN_BOUNDARY;
    }
    return isBordIntersectBy(idxDeb_, idxFin_, _min, _max, _nbPt);
  }

  /**
   * @param _debBord l'indice de debut de bord
   * @param _finBord l'indice de fin de bord
   * @param _minSelect l'indice min de selection
   * @param _maxSelect l'indice max de selection
   * @param _nbPt le nombre de point de la frontiere contenante
   * @return INTERSECT_NULL si pas d'intersection <br>
   *         INTERSECT_PARTIEL si intersection partielle <br>
   *         INTERSECT_INCLUDE_IN_BORD si [_minSelect,_maxSelect] est strictement inclus de le bord [_deb,_fin] <br>
   *         INTERSECT_TOTAL si [_deb,_fin] est inclus dans la selection
   */
  public static int isBordIntersectBy(final int _debBord, final int _finBord, final int _minSelect, final int _maxSelect, final int _nbPt) {
    if (_debBord == _finBord) {
      return INTERSECT_INCLUDE_IN_BOUNDARY;
    }
    int deb = _debBord;
    int fin = _finBord;
    if (deb == _nbPt) {
      deb = 0;
    }
    if (fin < 0) {
      fin = _nbPt - 1;
    }
    if (deb < fin) {
      if (_minSelect <= _maxSelect) {
        return intersectMinMax(_minSelect, _maxSelect, deb, fin);
      }
      if ((deb > _maxSelect) && (fin < _minSelect)) {
        return INTERSECT_NULL;
      } else if ((fin <= _maxSelect) || (deb >= _minSelect)) {
        return INTERSECT_CONTAINS_BOUNDARY;
      } else {
        return INTERSECT_PARTIEL;
      }
    }
    if (_minSelect <= _maxSelect) {
      return intersectMaxMin(_minSelect, _maxSelect, deb, fin);
    }
    if ((_minSelect > deb) && (_maxSelect < fin)) {
      return INTERSECT_INCLUDE_IN_BOUNDARY;
    } else if ((_minSelect <= deb) && (_maxSelect >= fin)) {
      return INTERSECT_CONTAINS_BOUNDARY;
    }
    return INTERSECT_PARTIEL;
  }

  private static int intersectMaxMin(final int _minSelect, final int _maxSelect, final int _deb, final int _fin) {
    if ((_minSelect > _fin) && (_maxSelect < _deb)) {
      return INTERSECT_NULL;
    } else if ((_maxSelect < _fin) || (_minSelect > _deb)) {
      return INTERSECT_INCLUDE_IN_BOUNDARY;
    }
    return INTERSECT_PARTIEL;
  }

  private static int intersectMinMax(final int _minSelect, final int _maxSelect, final int _deb, final int _fin) {
    if ((_maxSelect < _deb) || (_minSelect > _fin)) {
      return INTERSECT_NULL;
    } else if ((_minSelect <= _deb) && (_maxSelect >= _fin)) {
      return INTERSECT_CONTAINS_BOUNDARY;
    } else if ((_minSelect > _deb) && (_maxSelect < _fin)) {
      return INTERSECT_INCLUDE_IN_BOUNDARY;
    } else {
      return INTERSECT_PARTIEL;
    }
  }

  /**
   * @return le type de ce bord
   */
  public H2dBoundaryType getType() {
    return type_;
  }

  protected boolean setBoundaryType(final H2dBoundaryType _type, final H2dBcListenerDispatcher _l) {
    if ((_type != null) && (_type != type_)) {
      final H2dBoundaryType old = type_;
      type_ = _type;
      if (_l != null) {
        _l.fireBcTypeChanged(this, old);
      }
      return true;
    }
    return false;
  }

  protected void setIdxDeb(final int _i) {
    idxDeb_ = _i;
  }

  /**
   *
   */
  protected void setIdxFin(final int _i) {
    idxFin_ = _i;
  }

  protected void setIdxmaillageFrontiere(final int _i) {
    idxmaillageFrontiere_ = _i;
  }

  @Override
  public String toString() {
    return "H2DBord " + (name_ == null ? CtuluLibString.EMPTY_STRING : name_) + "(" + idxmaillageFrontiere_ + " [" + idxDeb_ + CtuluLibString.VIR
        + idxFin_ + "] type " + type_ + ")";
  }

  /**
   * @return le resume ( bord de type ...)
   */
  public String getResume() {
    return H2dResource.getS("Bord de type {0}", type_.toString());
  }

  /**
   * Un iterateur sur les points d'un bord. Les bord contenant l'origine sont ainsi correctement gere.
   * 
   * @author Fred Deniger
   * @version $Id: H2dBoundary.java,v 1.20 2006-09-19 14:43:25 deniger Exp $
   */
  public static final class BordIndexIterator {

    private int nbPt_;

    private int indexEnCours_;

    private int indexFinal_;

    public BordIndexIterator() {
    }

    /**
     * @param _nbPt le nombre de point total sur la frontiere. The number of points of the frontier
     * @param _b le bord a parcourir
     */
    public BordIndexIterator(final int _nbPt, final H2dBoundary _b) {
      set(_nbPt, _b);
    }

    /**
     * @param _nbPt le nombre de point total sur la frontiere. The number of points of the frontier
     * @param _min l'indice de debut pour l'iteration
     * @param _max l'indice de fin pour l'iteration
     */
    public BordIndexIterator(final int _nbPt, final int _min, final int _max) {
      set(_nbPt, _min, _max);
    }

    /**
     * @param _nbPt le nombre de point total sur la frontiere.The number of points of the frontier
     * @param _b le bord a parcourir
     */
    public void set(final int _nbPt, final H2dBoundary _b) {
      nbPt_ = _nbPt;
      // bd_= _b;
      if (_b.isUnique()) {
        indexEnCours_ = 0;
        indexFinal_ = nbPt_ - 1;
      } else if (_b.idxDeb_ > _b.idxFin_) {
        indexEnCours_ = _b.idxDeb_;
        indexFinal_ = nbPt_ + _b.idxFin_;
      } else {
        indexEnCours_ = _b.idxDeb_;
        indexFinal_ = _b.idxFin_;
      }
    }

    /**
     * @param _nbPt le nb de point de la frontiere
     * @param _min l'indice de debut pour l'iteration
     * @param _max l'indice de fin pour l'iteration
     */
    public void set(final int _nbPt, final int _min, final int _max) {
      nbPt_ = _nbPt;
      // bd_= _b;
      if (_min == _max) {
        indexEnCours_ = 0;
        indexFinal_ = nbPt_ - 1;
      } else if (_min > _max) {
        indexEnCours_ = _min;
        indexFinal_ = nbPt_ + _max;
      } else {
        indexEnCours_ = _min;
        indexFinal_ = _max;
      }
    }

    /**
     * @return true si un nouveau point peut etre itere
     */
    public boolean hasNext() {
      return indexEnCours_ <= indexFinal_;
    }

    /**
     * @return l'indice sur le bord
     */
    public int next() {
      if (indexEnCours_ > indexFinal_) {
        throw new IllegalAccessError("end of iterator");
      }
      final int r = (indexEnCours_ < nbPt_) ? indexEnCours_ : indexEnCours_ - nbPt_;
      indexEnCours_++;
      return r;
    }
  }

  /**
   * @param _nb le nombre de point sur la frontiere contenant ce bord.
   * @return un iterateur pour les points de ce bord.
   */
  public BordIndexIterator createIterator(final int _nb) {
    return new BordIndexIterator(_nb, this);
  }

  /**
   * @return l'indice de debut de ce bord.
   */
  public int getIdxDeb() {
    return idxDeb_;
  }

  /**
   * @return true si bord unique. Soit si 'index de debut'='index de fin'.
   */
  public boolean isUnique() {
    return idxDeb_ == idxFin_;
  }

  /**
   * @param _idx l'indice a tester
   * @return true si cette indice est contenu par ce bord
   */
  public boolean containsIdx(final int _idx) {
    return idxDeb_ == idxFin_ ? true : ((idxDeb_ < idxFin_) ? ((_idx >= idxDeb_) && (_idx <= idxFin_))
        : ((_idx >= 0) && ((_idx <= idxFin_) || (_idx >= idxDeb_))));
  }

  /**
   * @return l'indice de fin
   */
  public int getIdxFin() {
    return idxFin_;
  }

  /**
   * @return l'index de la frontiere concernee par ce bord.
   */
  public int getIdxFrontiere() {
    return idxmaillageFrontiere_;
  }

  /**
   * @param _nbPoint le nombre de point de la frontiere englobante
   * @return le nombre de point contenu par ce bord
   */
  public int getNPointInBord(final int _nbPoint) {
    if (idxFin_ > idxDeb_) {
      return idxFin_ - idxDeb_ + 1;
    }
    return idxFin_ + 1 + _nbPoint - idxDeb_;
  }

  public int[] getIdx(final int _nbPoint) {
    final int[] res = new int[getNPointInBord(_nbPoint)];
    for (int i = res.length - 1; i >= 0; i--) {
      int idx = i + idxDeb_;
      if (idx >= _nbPoint) {
        idx -= _nbPoint;
      }
      res[i] = idx;

    }
    return res;
  }

  /**
   * @param _min l'indice de deb du bord
   * @param _max l'indice de fin du bord
   * @param _nbPoint le nombre de point sur la frontiere englobante
   * @return le nombre de point contenu par le bord [_min,_max]
   */
  public static int getNbPointInBord(final int _min, final int _max, final int _nbPoint) {
    if (_max > _min) {
      return _max - _min + 1;
    }
    return _max + 1 + _nbPoint - _min;
  }

  /**
   * @return le nom de ce bord. Une sorte d'identifiant
   */
  public String getName() {
    return name_;
  }

  /**
   * @return true si l'origine est contenu par ce bord.
   */
  public boolean containsZeroIndex() {
    if (isUnique()) {
      return true;
    } else if (idxDeb_ < idxFin_) {
      return idxDeb_ == 0;
    } else {
      return idxFin_ >= 0;
    }
  }

  /**
   * @return si est considere comme le premiere bord d'une frontiere (comme par 0 ou l'indice de fin est + petit que
   *         l'indice de debut et est strictement positif).
   */
  public boolean isFirst() {
    if (isUnique()) {
      return true;
    } else if (idxDeb_ < idxFin_) {
      return idxDeb_ == 0;
    } else {
      return idxFin_ > 0;
    }
  }

  /**
   * @param _string le nouveau nom
   */
  public void setName(final String _string) {
    name_ = _string;
  }

  public void checkNbValues(int nbIntern, String defaultValue, H2dVariableType variable) {
  }
}