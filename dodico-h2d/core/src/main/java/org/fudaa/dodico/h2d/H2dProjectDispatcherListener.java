/*
 *  @creation     8 oct. 2003
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.h2d;

import org.fudaa.dodico.dico.DicoParamsListener;
import org.fudaa.dodico.mesure.EvolutionListener;

/**
 * @author deniger
 * @version $Id: H2dProjectDispatcherListener.java,v 1.9 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dProjectDispatcherListener extends H2dBcListener, DicoParamsListener, EvolutionListener {

}
