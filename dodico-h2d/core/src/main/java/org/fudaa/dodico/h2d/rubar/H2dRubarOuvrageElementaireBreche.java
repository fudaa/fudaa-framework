/*
 * @creation 3 janv. 2005
 *
 * @modification $Date: 2007-03-02 13:00:52 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.dodico.h2d.H2dEvolutionVariableMap;
import org.fudaa.dodico.h2d.resource.H2dResource;
import org.fudaa.dodico.h2d.type.H2dRubarOuvrageType;

import java.util.Set;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarOuvrageElementaireBreche.java,v 1.11 2007-03-02 13:00:52 deniger Exp $
 */
public class H2dRubarOuvrageElementaireBreche extends H2dRubarOuvrageElementaireAbstract implements H2dRubarOuvrageElementaireBrecheI {
  public static final int MAX_TIME_STEP_NB = 9000;

  protected class ValuesDoubleModel extends CtuluArrayDouble {
    public ValuesDoubleModel(final CtuluArrayDouble _init) {
      super(_init);
    }

    public ValuesDoubleModel(final double[] _values) {
      super(_values);
    }

    public ValuesDoubleModel(final int _capacity) {
      super(_capacity);
    }

    @Override
    protected void fireObjectChanged(int _indexGeom, Object _newValue) {
      if (ouvrageParent_ != null) {
        ouvrageParent_.getMng().fireOuvrageElementaireChanged(ouvrageParent_, H2dRubarOuvrageElementaireBreche.this);
      }
    }
  }

  public static int getChoiceId(final int _selectedChoice) {
    if (_selectedChoice == 0) {
      return 0;
    }
    return -1;
  }

  public void setElevationOfTheTopOfTheDike(double val) {
    values_.set(1, val);
  }

  public void setElevationAtTheBasementOfTheDike(double val) {
    values_.set(2, val);
  }

  public void setBreachBottomElevation(double val) {
    values_.set(9, val);
  }

  public static String[] getValuesTitle() {
    return new String[]{
        H2dResource.getS("temps du d�but du calcul"),//0
        H2dResource.getS("Cote de la cr�te de la digue"),//1
        H2dResource.getS("altitude du pied de digue"),//2
        H2dResource.getS("Largeur en cr�te"),//3
        H2dResource.getS("Largeur en pied"),//4
        H2dResource.getS("diam�tre m�dian D50 (mm)"),//5
        H2dResource.getS("Coefficient de Strickler de l'�coulement sur et dans la digue"),//6
        H2dResource.getS("Masse volumique des grains (kg/m3)"),//7
        H2dResource.getS("Porosit�"),//8
        H2dResource.getS("Cote de fond br�che/renard � l'instant initial"),//9
        H2dResource.getS("Dimension initiale de la br�che (mm)"),//10
        H2dResource.getS("Pas de temps de stockage pour fichier RES"),//11
        H2dResource.getS("Coefficient de perte de charge singuli�re dans la retenue"),//12
        H2dResource.getS("Coefficient Erosion Lineaire")//13
    };
  }

  private static double[] getDefaultValues() {
    return new double[]{0, 0, 0, 5, 30, 0.1, 20, 2700, 0.3, 0, 0.1, 1, 0, 0
    };
  }

  private int brecheType_;
  private int nbPasTempsMax_ = 9000;
  private int surverseId_;
  CtuluArrayDouble values_;

  protected H2dRubarOuvrageElementaireBreche(final H2dRubarOuvrageElementaireBreche _b, final boolean _ouvrageParentSet) {
    initFrom(_b, _ouvrageParentSet);
  }

  H2dRubarOuvrageElementaireBreche(final H2dRubarOuvrage _mng, final double[] _values, final int _nbPasTemps, final int _typeRenard) {
    brecheType_ = 0;
    nbPasTempsMax_ = _nbPasTemps;
    surverseId_ = _typeRenard;
    ouvrageParent_ = _mng;
    values_ = new ValuesDoubleModel(_values);
  }

  @Override
  public void fillWithTransientCurves(String pref, H2dEvolutionVariableMap r) {
  }

  @Override
  public void fillWithTarageEvolution(String prefix, Set allTarage) {

  }

  /**
   * Iinitialisation avec toutes les valeurs a 0.
   */
  public H2dRubarOuvrageElementaireBreche() {
    values_ = new ValuesDoubleModel(NB_VALUES);
    values_.initWith(new CtuluArrayDouble(getDefaultValues()), false);
    surverseId_ = 0;
    brecheType_ = 0;
  }

  protected final void initFrom(final H2dRubarOuvrageElementaireBreche _b, final boolean _ouvrageParentSet) {
    if (_ouvrageParentSet) {
      ouvrageParent_ = _b.ouvrageParent_;
    }
    brecheType_ = _b.brecheType_;
    surverseId_ = _b.surverseId_;
    nbPasTempsMax_ = _b.nbPasTempsMax_;
    values_ = new ValuesDoubleModel(_b.values_);
  }

  @Override
  public H2dRubarOuvrageElementaireInterface getCopyWithoutEvt() {
    return new H2dRubarOuvrageElementaireBreche(this, false);
  }

  @Override
  public int getNbPasDeTempsMax() {
    return nbPasTempsMax_;
  }

  @Override
  public int getRenardSurverseId() {
    return surverseId_;
  }

  /**
   * @return les choix possibles pour la surverse/renard.
   */
  public String[] getSurverseChoices() {
    return new String[]{H2dResource.getS("Renard"), H2dResource.getS("Surverse")};
  }

  public String[] getTypeOfBreaching() {
    return new String[]{H2dResource.getS("Contrainte normale"), H2dResource.getS("Contrainte limit�e"),
        H2dResource.getS("contrainte limit�e et ouvrages successifs s'enchainant"),
        H2dResource.getS("contrainte normale et ouvrages successifs s'encha�nant")};
  }

  public int getSelectedSurverseChoice() {
    return (surverseId_ == -1) ? 1 : 0;
  }

  @Override
  public final H2dRubarOuvrageType getType() {
    return H2dRubarOuvrageType.BRECHE;
  }

  @Override
  public double getValue(final int _idx) {
    return values_.getValue(_idx);
  }

  public boolean setAllValues(final double[] _v, final CtuluCommandContainer _cmd) {
    return values_.setAll(_v, _cmd);
  }

  public boolean setSurverseId(final int _id, final CtuluCommandContainer _cmd) {
    if (_id == surverseId_ || (_id != 0 && _id != -1)) {
      return false;
    }

    if (_cmd != null) {
      final int old = surverseId_;
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          setSurverseId(_id, null);
        }

        @Override
        public void undo() {
          setSurverseId(old, null);
        }
      });
    }
    surverseId_ = _id;
    if (ouvrageParent_ != null) {
      ouvrageParent_.mng_.fireOuvrageElementaireChanged(ouvrageParent_, this);
    }
    return true;
  }

  public boolean setTypeOfBreaching(final int _id, final CtuluCommandContainer _cmd) {
    if (_id == brecheType_ || _id < 0 || _id > 3) {
      return false;
    }

    if (_cmd != null) {
      final int old = brecheType_;
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          setTypeOfBreaching(_id, null);
        }

        @Override
        public void undo() {
          setTypeOfBreaching(old, null);
        }
      });
    }
    brecheType_ = _id;
    if (ouvrageParent_ != null) {
      ouvrageParent_.mng_.fireOuvrageElementaireChanged(ouvrageParent_, this);
    }
    return true;
  }

  public boolean setTimeStep(final int _id, final CtuluCommandContainer _cmd) {
    if (_id == nbPasTempsMax_ || _id < 0 || _id > H2dRubarOuvrageElementaireBreche.MAX_TIME_STEP_NB) {
      return false;
    }

    if (_cmd != null) {
      final int old = nbPasTempsMax_;
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          setTimeStep(_id, null);
        }

        @Override
        public void undo() {
          setTimeStep(old, null);
        }
      });
    }
    nbPasTempsMax_ = _id;
    if (ouvrageParent_ != null) {
      ouvrageParent_.mng_.fireOuvrageElementaireChanged(ouvrageParent_, this);
    }
    return true;
  }

  @Override
  public int getBrecheType() {
    return brecheType_;
  }

  public void setBrecheType(int brecheType_) {
    this.brecheType_ = brecheType_;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getDebutTemps() {
    return values_.getValue(0);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getCoteCrete() {
    return values_.getValue(1);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getCotePied() {
    return values_.getValue(2);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getLargeurCrete() {
    return values_.getValue(3);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getLargeurPied() {
    return values_.getValue(4);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getDiametreMedian() {
    return values_.getValue(5);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getCoeffStrickler() {
    return values_.getValue(6);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getMasseVolumiqueGrain() {
    return values_.getValue(7);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getPorosite() {
    return values_.getValue(8);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getCoteFond() {
    return values_.getValue(9);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getDimensionInitiale() {
    return values_.getValue(10);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getPasDeTemps() {
    return values_.getValue(11);
  }

  @Override
  public double getCoefficientErosionLineaire() {
    return values_.getValue(13);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public double getCoeffPerteDeCharge() {
    return values_.getValue(12);
  }
}
