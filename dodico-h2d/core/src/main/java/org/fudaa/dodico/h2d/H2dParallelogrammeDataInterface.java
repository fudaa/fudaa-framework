/*
 *  @creation     28 sept. 2004
 *  @modification $Date: 2007-01-10 09:04:27 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;

/**
 * @author Fred Deniger
 * @version $Id: H2dParallelogrammeDataInterface.java,v 1.8 2007-01-10 09:04:27 deniger Exp $
 */
public interface H2dParallelogrammeDataInterface {

  double getX1();

  double getX2();

  double getX4();

  double getY1();

  double getY2();

  double getY4();

  boolean isValuesDefined();

  /**
   * @return le nombre de valeurs d�finies
   */
  int getNbValues();

  double getValue(int _valueIdx, int _ptIDx);

  CtuluCollectionDoubleEdit getValues(int _valueIdx);

}
