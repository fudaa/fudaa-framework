/*
 * @creation 22 d�c. 2004
 * @modification $Date: 2006-04-19 13:19:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarLimniListener.java,v 1.3 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dRubarLimniListener {

  /**
   * Envoye si le pas de temps de stockage a ete modifie.
   *
   * @param _mng le manager modifie
   */
  void limniTimeStepChanged(H2dRubarLimniMng _mng);

  /**
   * Envoye si des element ont �t� modifie.
   *
   * @param _mng le manager modifie
   */
  void limniPointChanged(H2dRubarLimniMng _mng);

}