/*
 * @creation 28 mars 07
 * @modification $Date: 2007-04-30 14:21:34 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import gnu.trove.TIntObjectHashMap;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.dico.*;
import org.fudaa.dodico.h2d.type.H2dRubarProjetType;

import java.util.Arrays;
import java.util.Map;

/**
 * @author fred deniger
 * @version $Id: H2DRubarDicoParams.java,v 1.2 2007-04-30 14:21:34 deniger Exp $
 */
public class H2DRubarDicoParams extends DicoParams {
  public static final int NB_TRANSPORT_PARAMETERS = 11;

  public H2DRubarDicoParams(final DicoCasInterface _i, final H2DRubarDicoCasFileFormatVersion _dico) {
    super(_i, _dico);
  }

  public H2DRubarDicoParams(final Map _inits, final Map _keysCommentaire, final H2DRubarDicoCasFileFormatVersion _dico) {
    super(_inits, _keysCommentaire, _dico);
  }

  protected H2dRubarDicoModel getRubarModel() {
    return (H2dRubarDicoModel) super.getDico();
  }

  protected void changeFormat(final H2DRubarDicoCasFileFormatVersion _newFmt) {
    ft_ = _newFmt;
  }

  @Override
  protected void fireVersionChanged() {
    super.fireVersionChanged();
  }

  public void changedTo(final H2dRubarProjetType newProjectType, final int nbTransportBlocks, final CtuluCommandContainer commandContainer) {
    final H2DRubarDicoCasFileFormatVersion old = (H2DRubarDicoCasFileFormatVersion) ft_;
    final String[] oldValues = getValues();
    ft_ = getRubarFormat().createNewForProject(newProjectType, nbTransportBlocks);
    removeAll();
    setValues(oldValues);
    fireVersionChanged();
    if (commandContainer != null) {
      commandContainer.addCmd(new CtuluCommand() {
        @Override
        public void undo() {

          changeFormat(old);
          removeAll();
          setValues(oldValues);
          fireVersionChanged();
        }

        @Override
        public void redo() {
          changedTo(newProjectType, nbTransportBlocks, null);
        }
      });
    }
  }

  private H2DRubarDicoCasFileFormatVersion getRubarFormat() {
    return ((H2DRubarDicoCasFileFormatVersion) ft_);
  }

  public static boolean isTransport(final TIntObjectHashMap _m) {
    return isTransport(_m.size());
  }

  public static boolean isTransport(final int size) {
    return size > 25;
  }

  public static int getNbTransportBlock(final int size) {
    if (!isTransport(size)) {
      return 0;
    }
    return (size - 25) / NB_TRANSPORT_PARAMETERS;
  }

  public static int getNbTransportBlock(final TIntObjectHashMap _m) {
    return getNbTransportBlock(_m.size());
  }

  public String[] getValues() {
    final DicoModelAbstract m = getDico();
    final DicoEntite[] kws = new DicoEntite[m.getEntiteNombre()];
    m.getEntites().toArray(kws);
    Arrays.sort(kws, new DicoEntiteComparator.Index());
    final String[] res = new String[kws.length];
    for (int i = 0; i < kws.length; i++) {
      final DicoEntite e = kws[i];
      String v = getValue(e);
      final DicoDataType type = e.getType();
      if (type instanceof DicoDataType.Binaire) {
        if (DicoDataType.Binaire.getValue(v)) {
          v = CtuluLibString.UN;
        } else {
          v = CtuluLibString.ZERO;
        }
      }
      res[i] = v;
    }
    return res;
  }

  public void setValues(final String[] _values) {
    if (_values == null) {
      return;
    }
    final DicoModelAbstract m = getDico();
    final DicoEntite[] kws = new DicoEntite[m.getEntiteNombre()];
    m.getEntites().toArray(kws);
    Arrays.sort(kws, new DicoEntiteComparator.Index());
    for (int i = 0; i < kws.length && i < _values.length; i++) {
      final DicoEntite e = kws[i];
      setValue(e, _values[i]);
    }
  }
}
