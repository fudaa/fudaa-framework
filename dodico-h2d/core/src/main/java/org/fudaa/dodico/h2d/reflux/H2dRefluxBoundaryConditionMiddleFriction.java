/*
 * @creation 20 nov. 2003
 * @modification $Date: 2007-05-22 13:11:23 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import java.util.Map;
import java.util.Set;
import org.fudaa.dodico.h2d.H2dEvolutionUseCounter;
import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dRefluxBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionListener;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

/**
 * @author deniger
 * @version $Id: H2dRefluxBoundaryConditionMiddleFriction.java,v 1.19 2007-05-22 13:11:23 deniger Exp $
 */
public class H2dRefluxBoundaryConditionMiddleFriction extends H2dRefluxBoundaryConditionMiddle {

  protected double friction_;
  protected EvolutionReguliereAbstract frictionEvol_;

  protected H2dBcType frictionType_;

  public H2dRefluxBoundaryConditionMiddleFriction() {
    super(null, H2dRefluxBoundaryType.SOLIDE_FROTTEMENT);
    frictionType_ = H2dBcType.LIBRE;
  }

  @Override
  protected void copyCurve(final EvolutionListener _l) {
    super.copyCurve(_l);
    if (frictionEvol_ != null) {
      frictionEvol_ = (EvolutionReguliereAbstract) frictionEvol_.getCopy(_l);
    }
  }

  /*
   * public boolean containsClTransient() { return super.containsClTransient() || (frictionType_ ==
   * H2dBcType.TRANSITOIRE); }
   */

  @Override
  public boolean containsPnTransient() {
    return super.containsPnTransient() || frictionType_ == H2dBcType.TRANSITOIRE;
  }

  @Override
  public void fillWithEvolVar(final H2dEvolutionUseCounter _r) {
    super.fillWithEvolVar(_r);
    if (frictionEvol_ != null) {
      _r.add(frictionEvol_, H2dVariableType.RUGOSITE, getIndexPt());
    }
  }

  /**
   * @param _cl la condition utilisee pour l'initialisation
   */
  public H2dRefluxBoundaryConditionMiddleFriction(final H2dRefluxBoundaryCondition _cl) {
    super(_cl, H2dRefluxBoundaryType.SOLIDE_FROTTEMENT);
    frictionType_ = H2dBcType.LIBRE;
    if ((_cl != null) && (_cl.isMiddleWithFriction())) {
      initFriction(_cl.getMiddleFriction());
    }
  }

  /**
   * @param _l la condition utilisee pour l'initialisation
   */
  public H2dRefluxBoundaryConditionMiddleFriction(final H2dRefluxBoundaryConditionMiddle _l) {
    super(_l);
    frictionType_ = H2dBcType.LIBRE;
    if ((_l != null) && (_l.isMiddleWithFriction())) {
      initFriction(_l.getMiddleFriction());
    }
  }

  private void initFriction(final H2dRefluxBoundaryConditionMiddleFriction _c) {
    if (_c != null) {
      frictionType_ = _c.frictionType_;
      friction_ = _c.friction_;
      frictionEvol_ = _c.frictionEvol_;
    }
  }

  @Override
  public final H2dBcType getType(final H2dVariableType _target) {
    if (_target == H2dVariableType.RUGOSITE) {
      return frictionType_;
    }
    return super.getType(_target);
  }

  @Override
  protected void replaceEvol(final Map _evolEquivEvol) {
    if ((frictionEvol_ != null) && (_evolEquivEvol.containsKey(frictionEvol_))) {
      if (frictionEvol_ != null) frictionEvol_.setUnUsed(true);
      frictionEvol_ = (EvolutionReguliereAbstract) _evolEquivEvol.get(frictionEvol_);
      frictionEvol_.setUsed(false);
    }
    super.replaceEvol(_evolEquivEvol);
  }

  @Override
  protected void restoreEvolUsed(final H2dRefluxBoundaryCondition _newBoundary) {
    if (uTransitoireCourbe_ != _newBoundary.uTransitoireCourbe_) {
      if (uTransitoireCourbe_ != null) {
        uTransitoireCourbe_.setUnUsed(false);
      }
      if (_newBoundary.uTransitoireCourbe_ != null) {
        _newBoundary.uTransitoireCourbe_.setUsed(false);
      }
    }
    if (vTransitoireCourbe_ != _newBoundary.vTransitoireCourbe_) {
      if (vTransitoireCourbe_ != null) {
        vTransitoireCourbe_.setUnUsed(false);
      }
      if (_newBoundary.vTransitoireCourbe_ != null) {
        _newBoundary.vTransitoireCourbe_.setUsed(false);
      }
    }
    if (hTransitoireCourbe_ != _newBoundary.hTransitoireCourbe_) {
      if (hTransitoireCourbe_ != null) {
        hTransitoireCourbe_.setUnUsed(false);
      }
      if (_newBoundary.hTransitoireCourbe_ != null) {
        _newBoundary.hTransitoireCourbe_.setUsed(false);
      }
    }
    if (qTransitoireCourbe_ != _newBoundary.qTransitoireCourbe_) {
      if (qTransitoireCourbe_ != null) {
        qTransitoireCourbe_.setUnUsed(false);
      }
      if (_newBoundary.qTransitoireCourbe_ != null) {
        _newBoundary.qTransitoireCourbe_.setUsed(false);
      }
    }
    if (_newBoundary.isMiddleWithFriction()) {
      final H2dRefluxBoundaryConditionMiddleFriction fr = _newBoundary.getMiddleFriction();
      if (frictionEvol_ != fr.frictionEvol_) {
        if (frictionEvol_ != null) {
          frictionEvol_.setUnUsed(false);
        }
        if (fr.frictionEvol_ != null) {
          frictionEvol_.setUsed(false);
        }
      }

    }
  }

  @Override
  protected H2dRefluxBoundaryConditionMiddle setBoundaryType(final H2dBoundaryType _type) {
    if ((_type == null) || (_type == H2dRefluxBoundaryType.SOLIDE_FROTTEMENT)) {
      return null;
    }
    return new H2dRefluxBoundaryConditionMiddle(this, _type);
  }

  @Override
  protected boolean setFree(final H2dVariableType _t) {
    if (_t == H2dVariableType.RUGOSITE) {
      return setFrictionFree();
    }
    return super.setFree(_t);
  }

  protected boolean setFrictionFree() {
    if (frictionType_ != H2dBcType.LIBRE) {
      friction_ = 0;
      if (frictionEvol_ != null) frictionEvol_.setUnUsed(false);
      frictionEvol_ = null;
      frictionType_ = H2dBcType.LIBRE;
      return true;
    }
    return false;
  }

  protected boolean setFrictionPermanent(final double _d) {
    if ((frictionType_ != H2dBcType.PERMANENT) || (_d != friction_)) {
      frictionType_ = H2dBcType.PERMANENT;
      if (frictionEvol_ != null) frictionEvol_.setUnUsed(false);
      frictionEvol_ = null;
      friction_ = _d;
      return true;
    }
    return false;
  }

  protected boolean setFrictionTransitoire(final EvolutionReguliereAbstract _evol) {
    if ((_evol != null) && ((frictionType_ != H2dBcType.TRANSITOIRE) || (frictionEvol_ != _evol))) {
      if (frictionEvol_ != null) frictionEvol_.setUnUsed(true);
      frictionEvol_ = _evol;
      frictionEvol_.setUsed(false);

      frictionType_ = H2dBcType.TRANSITOIRE;
      return true;
    }
    return false;
  }

  @Override
  protected boolean setPermanent(final H2dVariableType _t, final double _v) {
    if (_t == H2dVariableType.RUGOSITE) {
      return setFrictionPermanent(_v);
    }
    return super.setPermanent(_t, _v);
  }

  @Override
  protected boolean setTransitoire(final H2dVariableType _t, final EvolutionReguliereAbstract _v) {
    if (_t == H2dVariableType.RUGOSITE) {
      return setFrictionTransitoire(_v);
    }
    return super.setTransitoire(_t, _v);
  }

  @Override
  public H2dRefluxBoundaryCondition createCopy() {
    return new H2dRefluxBoundaryConditionMiddleFriction(this);
  }

  @Override
  public void fillWithUsedEvolution(final Set _r) {
    super.fillWithUsedEvolution(_r);
    if (frictionEvol_ != null) {
      _r.add(frictionEvol_);
    }
  }

  @Override
  public boolean fillWithValue(final H2dVariableType _t, final H2dRefluxValue _valueToFill) {
    if (_t == H2dVariableType.RUGOSITE) {
      fillWithValueFriction(_valueToFill);
      return true;
    }
    return super.fillWithValue(_t, _valueToFill);
  }

  /**
   * @param _valueToFill la valeur a modifier avec les donnees de friction
   */
  public void fillWithValueFriction(final H2dRefluxValue _valueToFill) {
    _valueToFill.setAllValues(frictionType_, friction_, frictionEvol_);
  }

  @Override
  public H2dBoundaryType getBoundaryType() {
    return H2dRefluxBoundaryType.SOLIDE_FROTTEMENT;
  }

  @Override
  public double getFriction() {
    return friction_;
  }

  /**
   * @return l'evolution utilisee pour la friction
   */
  public EvolutionReguliereAbstract getFrictionEvolution() {
    return frictionEvol_;
  }

  /**
   * @return le comportement de la friction
   */
  public H2dBcType getFrictionType() {
    return frictionType_;
  }

  /**
   * @return this;
   */
  @Override
  public final H2dRefluxBoundaryConditionMiddleFriction getMiddleFriction() {
    return this;
  }

  @Override
  public void initFrom(final H2dRefluxBoundaryCondition _c) {
    if (_c.isMiddleWithFriction()) {
      initFrom(_c.getMiddleFriction());
    } else if (_c.isMiddle()) {
      super.initFrom(_c.getMiddle());
    } else {
      new Throwable("can't init with a simple condition").printStackTrace();
    }
  }

  @Override
  public void initFrom(final H2dRefluxBoundaryConditionMiddle _c) {
    if (_c.isMiddleWithFriction()) {
      initFrom(_c.getMiddleFriction());
    } else {
      super.initFrom(_c);
    }
  }

  /**
   * @param _c la source pour l'initialisation
   */
  public void initFrom(final H2dRefluxBoundaryConditionMiddleFriction _c) {
    super.initFrom(_c);
    initFriction(_c);
  }

  @Override
  public void initUsedEvol() {
    super.initUsedEvol();
    if (frictionEvol_ != null) {
      frictionEvol_.setUsed(false);
    }
  }

  @Override
  public void initValueFrom(final H2dRefluxBoundaryCondition _c) {
    if (_c.isMiddleWithFriction()) {
      initValueFrom(_c.getMiddleFriction());
    } else if (_c.isMiddle()) {
      super.initValueFrom(_c.getMiddle());
    } else {
      new Throwable("can't init with a simple condition").printStackTrace();
    }
  }

  @Override
  public void initValueFrom(final H2dRefluxBoundaryConditionMiddle _c) {
    if (_c.isMiddleWithFriction()) {
      initValueFrom(_c.getMiddleFriction());
    } else {
      super.initValueFrom(_c);
    }
  }

  /**
   * @param _c la source pour l'initialisation
   */
  public void initValueFrom(final H2dRefluxBoundaryConditionMiddleFriction _c) {
    super.initValueFrom(_c);
    initFriction(_c);
  }

  @Override
  public final boolean isMiddleWithFriction() {
    return true;
  }

}