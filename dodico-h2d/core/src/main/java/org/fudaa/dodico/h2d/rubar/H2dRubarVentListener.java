/*
 *  @creation     14 d�c. 2004
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarApportListener.java,v 1.4 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dRubarVentListener {

  /**
   * Envoye lorsque le contenu a ete modifie.
   *
   * @param _mng le manager d'apport
   * @param _dest l'evolution modifiee
   */
  void ventEvolutionContentChanged(H2dRubarVentMng _mng, EvolutionReguliereInterface _dest);

  /**
   * Envoye lorsque les affectations d'evolution sur les element ont ete modifiees.
   *
   * @param _mng le manager d'apport
   */
  void ventChanged(H2dRubarVentMng _mng);

  /**
   * Utilise lorsqu'une evolution devient ou n'est plus utilisee.
   *
   * @param _mng le manager d'apport
   * @param _dest l'evolution qui n'est plus ou devient utilise
   */
  void ventEvolutionUsedChanged(H2dRubarVentMng _mng, EvolutionReguliereInterface _dest);

}
