/*
 * @creation 9 sept. 2003
 * @modification $Date: 2006-04-19 13:19:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import java.util.List;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.dodico.ef.EfGridInterface;

/**
 * @author deniger
 * @version $Id: H2dBcManagerBlockInterface.java,v 1.12 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dBcManagerBlockInterface {

  /**
   * @param _idxFrontiere l'index de la frontiere globales a considerer
   * @return les bords de la frontiere _idxFrontiere
   */
  H2dBcFrontierBlockInterface getBlockFrontier(int _idxFrontiere);

  /**
   * @return tous les types de bord utilisable
   */
  CtuluPermanentList getBordList();

  /**
   * @return le nombre de frontiere liquide
   */
  int getNbLiquideBoundaries();

  /**
   * @return le maillage
   */
  EfGridInterface getGrid();

  /**
   * @return le nombre de frontiere geree
   */
  int getNbBoundaryType();

  /**
   * @return la liste des bords utilises
   */
  List getUsedBoundaryType();
}