/*
 *  @creation     29 sept. 2003
 *  @modification $Date: 2006-04-21 07:59:20 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * @author deniger
 * @version $Id: H2dBcListener.java,v 1.11 2006-04-21 07:59:20 deniger Exp $
 */
public interface H2dBcListener {
  /**
   * Call when parameters is changed for a boundary.
   *
   * @param _b can be null
   * @param _t can be null
   */
  void bcParametersChanged(H2dBoundary _b, H2dVariableType _t);

  /**
   * Call when the parameters (friction,velocity,..) is changed for some points.
   *
   * @param _t if not null, represent the variable changed
   */
  void bcPointsParametersChanged(H2dVariableType _t);

  /**
   * @param _b le bord modifie
   * @param _old l'ancien type du bord
   */
  void bcBoundaryTypeChanged(H2dBoundary _b, H2dBoundaryType _old);

  /**
   * Call when the boundary disposition is changed for a frontier.
   *
   * @param _b la frontiere modifiee
   */
  void bcFrontierStructureChanged(H2dBcFrontierInterface _b);

}
