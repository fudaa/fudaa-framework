/*
 * @creation 14 mars 07
 * @modification $Date: 2007-03-15 16:59:36 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d;

import org.fudaa.dodico.h2d.type.H2dVariableType;
import org.fudaa.dodico.mesure.EvolutionReguliereInterface;

/**
 * @author fred deniger
 * @version $Id: H2dEvolutionUseCounter.java,v 1.1 2007-03-15 16:59:36 deniger Exp $
 */
public interface H2dEvolutionUseCounter {

  /**
   * Si une evolution est utilise par plusieurs type de variables, elle sera affecte a la variable null.
   *
   * @param _evol l'evolution
   * @param _t la variable representee par cette evolution
   * @param _idxGlobal l'indice du point ou de l'element global
   */
  void add(final EvolutionReguliereInterface _evol, final H2dVariableType _t, int _idxGlobal);

}