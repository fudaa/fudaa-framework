/*
 *  @creation     3 janv. 2005
 *  @modification $Date: 2006-04-19 13:19:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.h2d.type.H2dRubarOuvrageRef;

import java.util.Set;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarOuvrageI.java,v 1.5 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dRubarOuvrageI {

  int getNbOuvrageElementaires();

  int getRubarRef();

  H2dRubarOuvrageRef getOuvrageRef();

  boolean containsOuvrageElementaire();

  boolean isElt2Set();

  double getXaAmont();

  double getXaAval();

  double getXElementAmont();

  double getXElementAval();

  int getNbMailleIntern();

  double getXMailleIntern(int _i);

  double getYAmont();

  double getYAval();

  double getYElementAmont();

  double getYELementAval();

  double getYMailleIntern(int _i);

  H2dRubarOuvrageElementaireInterface getOuvrageElementaire(int _i);

  void fillWithTarageEvolution(String prefix, Set allTarage);
}
