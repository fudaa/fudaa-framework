/*
 * @creation 7 juil. 2004
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarBoundaryFlowrateType.java,v 1.6 2006-09-19 14:43:25 deniger Exp $
 */
public class H2dRubarBoundaryFlowrateType extends H2dRubarBoundaryType {

  /**
   * @param _n le nom du type
   * @param _rubarIdx l'index rubar
   */
  public H2dRubarBoundaryFlowrateType(final String _n, final int _rubarIdx) {
    super(_n, false, _rubarIdx);
  }

  @Override
  public boolean isTorrentielVar(final H2dVariableType _t) {
    return _t == H2dVariableType.COTE_EAU;
  }

  @Override
  public final List getAllowedVariable(final H2dRubarProjetType _t) {
    if (_t == H2dRubarProjetType.TRANSPORT) {
      return H2dRubarBcTypeList.TRANSIENT_VAR_TRANSPORT;
    }
    return H2dRubarBcTypeList.TRANSIENT_VAR;
  }

}