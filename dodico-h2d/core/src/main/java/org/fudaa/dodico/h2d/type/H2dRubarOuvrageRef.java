/*
 * @creation 26 juin 2003
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.commun.DodicoEnumType;
import org.fudaa.dodico.h2d.resource.H2dResource;

/**
 * @author deniger
 * @version $Id: H2dRubarOuvrageRef.java,v 1.8 2006-09-19 14:43:25 deniger Exp $
 */
public abstract class H2dRubarOuvrageRef extends DodicoEnumType {

  /**
   * Type couranto 2D.
   */
  public static final H2dRubarOuvrageRef COMPUTE_WITH_SAINT_VENANT = new H2dRubarOuvrageRef(H2dResource
      .getS("-1: Calcul avec les �quations de Saint-Venant")) {

    @Override
    public int getRubarCode() {
      return -1;
    }
  };
  /**
   * Type transport.
   */
  public static final H2dRubarOuvrageRef NO_COMPUTE_WITH_SAINT_VENANT = new H2dRubarOuvrageRef(H2dResource
      .getS("-2: Pas de calcul avec les �quations de Saint-Venant")) {

    @Override
    public int getRubarCode() {
      return -2;
    }
  };

  public final static H2dRubarOuvrageRef[] getConstantArray() {
    return new H2dRubarOuvrageRef[] { COMPUTE_WITH_SAINT_VENANT, NO_COMPUTE_WITH_SAINT_VENANT };
  }

  /**
   * @param _nom le nom
   */
  H2dRubarOuvrageRef(final String _nom) {
    super(_nom);
  }

  /**
   * @param _nom le nom
   */
  H2dRubarOuvrageRef() {
    super(CtuluLibString.EMPTY_STRING);
  }

  public DodicoEnumType[] getArray() {
    return getConstantArray();
  }

  public abstract int getRubarCode();

}
