package org.fudaa.dodico.h2d;

import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.dodico.ef.EfData;
import org.fudaa.dodico.ef.EfDataAdapter;
import org.fudaa.dodico.ef.EfGridData;
import org.fudaa.dodico.ef.EfGridInterface;
import org.fudaa.dodico.h2d.type.H2dVariableType;

import java.io.IOException;
import java.util.*;

/**
 * @author Fred Deniger
 */
public class H2dGridDataAdapter implements EfGridData {
  final Map varProvider_;
  final H2dVariableProviderInterface first_;

  public H2dVariableType[] getVar() {
    final H2dVariableType[] res = (H2dVariableType[]) varProvider_.keySet().toArray(new H2dVariableType[varProvider_.size()]);
    Arrays.sort(res);
    return res;
  }

  public H2dGridDataAdapter(final List _providers) {
    varProvider_ = new HashMap(20);
    first_ = (H2dVariableProviderInterface) _providers.get(0);
    for (int i = 0; i < _providers.size(); i++) {
      final H2dVariableProviderInterface pi = (H2dVariableProviderInterface) _providers.get(i);
      final Collection c = pi.getUsableVariables();
      if (c != null) {
        for (final Iterator it = c.iterator(); it.hasNext(); ) {
          final H2dVariableType var = (H2dVariableType) it.next();
          if (!varProvider_.containsKey(var)) {
            varProvider_.put(var, pi);
          }
        }
      }
    }
  }

  @Override
  public double getData(final CtuluVariable _o, final int _timeIdx, final int _idxObjet) throws IOException {
    final EfData data = getData(_o, _timeIdx);
    if (data != null) {
      return data.getValue(_idxObjet);
    }
    return 0;
  }

  @Override
  public EfData getData(final CtuluVariable _o, final int _timeIdx) throws IOException {
    final H2dVariableProviderInterface prov = getProv(_o);
    if (prov == null) {
      return null;
    }
    H2dVariableType var = (H2dVariableType) _o;
    return new EfDataAdapter(prov.getViewedModel(var), prov.isElementVar());
  }

  @Override
  public EfGridInterface getGrid() {
    return first_.getGrid();
  }

  private H2dVariableProviderInterface getProv(final Object _idxVar) {
    return (H2dVariableProviderInterface) varProvider_.get(_idxVar);
  }

  @Override
  public boolean isDefined(final CtuluVariable _var) {
    return varProvider_.containsKey(_var);
  }

  @Override
  public boolean isElementVar(final CtuluVariable _idxVar) {
    final H2dVariableProviderInterface prov = getProv(_idxVar);
    return prov != null && prov.isElementVar();
  }
}
