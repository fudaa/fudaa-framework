/*
 *  @creation     17 nov. 2003
 *  @modification $Date: 2006-10-24 12:48:36 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import org.fudaa.dodico.h2d.type.H2dBcType;
import org.fudaa.dodico.mesure.EvolutionReguliereAbstract;

/**
 * @author deniger
 * @version $Id: H2dRefluxValueCommonMutable.java,v 1.8 2006-10-24 12:48:36 deniger Exp $
 */
public class H2dRefluxValueCommonMutable extends H2dRefluxValue {
  public H2dRefluxValueCommonMutable() {
    super();
  }

  /**
   * @param _t
   * @param _value
   * @param _evol
   */
  public H2dRefluxValueCommonMutable(final H2dBcType _t, final double _value, final EvolutionReguliereAbstract _evol) {
    super(_t, _value, _evol);
  }

  public H2dRefluxValueCommonMutable(final H2dRefluxValue _v) {
    super(_v);
  }

  @Override
  public void setDoubleValueConstant(final boolean _b) {
    super.setDoubleValueConstant(_b);
  }

  @Override
  public void setAllValues(final H2dBcType _t, final double _value, final EvolutionReguliereAbstract _evol) {
    super.setAllValues(_t, _value, _evol);
  }

  @Override
  public void setEvolution(final EvolutionReguliereAbstract _evolution) {
    super.setEvolution(_evolution);
  }

  public void setValuePublic(final double _d) {
    super.setValue(_d);
  }

}
