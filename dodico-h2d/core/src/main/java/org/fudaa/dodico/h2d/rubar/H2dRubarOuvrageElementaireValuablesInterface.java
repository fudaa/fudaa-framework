package org.fudaa.dodico.h2d.rubar;

import org.fudaa.ctulu.CtuluCommandContainer;

public interface H2dRubarOuvrageElementaireValuablesInterface extends H2dRubarOuvrageElementaireInterface {

  String[] getValuesName();

  double getValue(final int _idx);

  boolean setAllValues(final double[] _v, final CtuluCommandContainer _cmd);

}
