/*
 * @creation 1 decembre 2004
 * @modification $Date: 2006-09-19 14:43:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.type;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.dodico.commun.DodicoEnumType;
import org.fudaa.dodico.h2d.resource.H2dResource;

import java.util.List;

/**
 * @author deniger
 * @version $Id: H2dRubarOuvrageType.java,v 1.7 2006-09-19 14:43:25 deniger Exp $
 */
public abstract class H2dRubarOuvrageType extends DodicoEnumType {
  /**
   * Br�che :B.
   */
  public static final H2dRubarOuvrageType BRECHE = new H2dRubarOuvrageType(H2dResource.getS("Br�che")) {
    @Override
    public String getRubarId() {
      return "B";
    }

    @Override
    public void visit(final H2dRubarOuvrageTypeClient _c, final Object _o) {
      if (_c != null) {
        _c.visitBreche(_o);
      }
    }
  };
  /**
   * Composite :C.
   */
  public static final H2dRubarOuvrageType COMPOSITE = new H2dRubarOuvrageType(H2dResource.getS("Composite")) {
    @Override
    public String getRubarId() {
      return "C";
    }

    @Override
    public void visit(final H2dRubarOuvrageTypeClient _c, final Object _o) {
      if (_c != null) {
        _c.visitComposite(_o);
      }
    }
  };
  /**
   * D�versoir : D.
   */
  public static final H2dRubarOuvrageType DEVERSOIR = new H2dRubarOuvrageType(H2dResource.getS("D�versoir")) {
    @Override
    public String getRubarId() {
      return "D";
    }

    @Override
    public void visit(H2dRubarOuvrageTypeClient _c, Object _o) {
      if (_c != null) {
        _c.visitDeversoir(_o);
      }
    }
  };
  /**
   * D�versoire hydraulique :H.
   */
  public static final H2dRubarOuvrageType DEVERSOIR_HYDRAULIQUE = new H2dRubarOuvrageType(H2dResource.getS("D�versoir hydraulique")) {
    @Override
    public String getRubarId() {
      return "H";
    }

    @Override
    public void visit(final H2dRubarOuvrageTypeClient _c, final Object _o) {
      if (_c != null) {
        //TODO Voir si correct.
        _c.visitDeversoirHydraulique(_o);
      }
    }
  };
  /**
   * Orifice circulaire :O.
   */
  public static final H2dRubarOuvrageType ORIFICE_CIRCULAIRE = new H2dRubarOuvrageType(H2dResource.getS("Orifice circulaire")) {
    @Override
    public String getRubarId() {
      return "O";
    }

    @Override
    public void visit(final H2dRubarOuvrageTypeClient _c, final Object _o) {
      if (_c != null) {
        //TODO Voir si correct.
        _c.visitOrificeCirculaire(_o);
      }
    }
  };
  /**
   * Apport d�bit : Q.
   */
  public static final H2dRubarOuvrageType APPORT_DEBIT = new H2dRubarOuvrageType(H2dResource.getS("Apport d�bit")) {
    @Override
    public String getRubarId() {
      return "Q";
    }

    @Override
    public void visit(final H2dRubarOuvrageTypeClient _c, final Object _o) {
      if (_c != null) {
        _c.visitApport(_o);
      }
    }
  };
  /**
   * Transfert de d�bit : Z.
   */
  public static final H2dRubarOuvrageType TRANSFERT_DEBIT_TARAGE = new H2dRubarOuvrageType(H2dResource.getS("Transfert d�bit")) {
    @Override
    public String getRubarId() {
      return "Z";
    }

    @Override
    public void visit(final H2dRubarOuvrageTypeClient _c, final Object _o) {
      if (_c != null) {
        _c.visitTransfert(_o);
      }
    }
  };
  /**
   * La liste des types de projet.
   */
  public static final List LIST = new CtuluPermanentList(CtuluLibArray.sort(new H2dRubarOuvrageType[]{DEVERSOIR,
      TRANSFERT_DEBIT_TARAGE, APPORT_DEBIT, BRECHE, DEVERSOIR_HYDRAULIQUE, COMPOSITE, ORIFICE_CIRCULAIRE}));

  public final static H2dRubarOuvrageType[] getConstantArray() {
    return new H2dRubarOuvrageType[]{DEVERSOIR, TRANSFERT_DEBIT_TARAGE, APPORT_DEBIT, BRECHE, DEVERSOIR_HYDRAULIQUE, COMPOSITE, ORIFICE_CIRCULAIRE};
  }

  /**
   * @param _nom le nom
   */
  H2dRubarOuvrageType(final String _nom) {
    super(_nom);
  }

  H2dRubarOuvrageType() {
    super(CtuluLibString.EMPTY_STRING);
  }

  /**
   * @return l'identifiant utiliser dans le fichier ouv.
   */
  public abstract String getRubarId();

  /**
   * @param _c le client a visiter
   * @param _o l'objet a passer
   */
  public abstract void visit(H2dRubarOuvrageTypeClient _c, Object _o);

  public DodicoEnumType[] getArray() {
    return getConstantArray();
  }
}
