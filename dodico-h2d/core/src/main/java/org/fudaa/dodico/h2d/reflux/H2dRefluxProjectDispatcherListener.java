/*
 * @creation 23 oct. 2003
 * @modification $Date: 2007-04-30 14:21:37 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import org.fudaa.dodico.h2d.H2dProjectDispatcherListener;
import org.fudaa.dodico.h2d.H2dSIListener;

/**
 * @author deniger
 * @version $Id: H2dRefluxProjectDispatcherListener.java,v 1.12 2007-04-30 14:21:37 deniger Exp $
 */
public interface H2dRefluxProjectDispatcherListener extends H2dProjectDispatcherListener, H2dRefluxBcListener,
    H2dRefluxElementPropertyMngListener, H2dSIListener,H2dRefluxNodalPropertiesListener {

  /**
   * Appele lorsque le type du projet est modifie.
   */
  void projectTypeChanged();
  
  
  void nodeXYChanged();

  /**
   * Appele lorsque les pas de temps sont modifies.
   */
  void timeStepChanged();



}