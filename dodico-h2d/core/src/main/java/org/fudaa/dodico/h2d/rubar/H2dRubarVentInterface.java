/*
 * @creation 14 d�c. 2004
 * @modification $Date: 2006-04-19 13:19:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;

import org.fudaa.dodico.mesure.EvolutionReguliere;

/**
 * @author Fred Deniger
 * @version $Id: H2dRubarAppInterface.java,v 1.4 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dRubarVentInterface {

  /**
   * @return le nombre d'element d�fini par cette resultat
   */
  int getNbElt();

  /**
   * @return le nombre d'element d�fini par cette resultat
   */
  int getNbEvol();

  /**
   * @param idx l'indice [0;getNbEvol()[
   * @return l'evolution correspondante.
   */
  EvolutionReguliere getEvolutionAlongX(int idx);
  EvolutionReguliere getEvolutionAlongY(int idx);

  /**
   * @param idxElt l'indice [0;getNbElt()[
   * @return l'evol correspondante
   */
  EvolutionReguliere getEvolAlongXForElt(int idxElt);
  EvolutionReguliere getEvolAlongYForElt(int idxElt);

  /**
   * @param eltIdx l'element demande
   * @return l'indice de la courbe correspondante
   */
  int getEvolIdx(int eltIdx);
}