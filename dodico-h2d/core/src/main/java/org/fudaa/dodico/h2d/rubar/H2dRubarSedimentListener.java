/*
 * @creation 14 d�c. 2004
 * 
 * @modification $Date: 2006-04-19 13:19:39 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.rubar;


/**
 * @author Fred Deniger
 * @version $Id: H2dRubarApportListener.java,v 1.4 2006-04-19 13:19:39 deniger Exp $
 */
public interface H2dRubarSedimentListener {

  /**
   * Envoye lorsque le contenu a ete modifie.
   * 
   * @param _mng le manager d'apport
   * @param _dest l'evolution modifiee
   */
  void sedimentChanged(H2dRubarSedimentMng _mng);

}
