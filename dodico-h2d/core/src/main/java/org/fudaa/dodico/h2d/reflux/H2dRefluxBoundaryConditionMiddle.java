/*
 * @creation 20 nov. 2003
 * @modification $Date: 2007-04-26 14:29:14 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.h2d.reflux;

import org.fudaa.dodico.h2d.type.H2dBoundaryType;
import org.fudaa.dodico.h2d.type.H2dRefluxBoundaryType;

/**
 * @author deniger
 * @version $Id: H2dRefluxBoundaryConditionMiddle.java,v 1.13 2007-04-26 14:29:14 deniger Exp $
 */
public class H2dRefluxBoundaryConditionMiddle extends H2dRefluxBoundaryCondition {

  private H2dBoundaryType type_;

  /**
   * Initialise le type a SOLIDE.
   */
  public H2dRefluxBoundaryConditionMiddle() {
    type_ = H2dRefluxBoundaryType.SOLIDE;
  }

  /**
   * @param _t le type a utilise. si null le type solide est utilise
   */
  public H2dRefluxBoundaryConditionMiddle(final H2dBoundaryType _t) {
    type_ = _t;
    if (type_ == null) {
      type_ = H2dRefluxBoundaryType.SOLIDE;
    }
  }

  /**
   * @param _l la condition utilisee pour l'initialisation
   * @param _t le type a utilise. si null le type solide est utilise
   */
  public H2dRefluxBoundaryConditionMiddle(final H2dRefluxBoundaryCondition _l, final H2dBoundaryType _t) {
    super(_l);
    type_ = _t == null ? H2dRefluxBoundaryType.SOLIDE : _t;
  }

  /**
   * @param _l la condition utilisee pour l'initialisation. Le type est repris et s'il est null, le type SOLIDE est
   *          utilise.
   */
  public H2dRefluxBoundaryConditionMiddle(final H2dRefluxBoundaryConditionMiddle _l) {
    super(_l);
    type_ = _l.getBoundaryType();
    if (type_ == null) {
      type_ = H2dRefluxBoundaryType.SOLIDE;
    }
  }

  @Override
  protected void initFrom(final H2dRefluxBoundaryCondition _c) {
    if (_c.isMiddle()) {
      initFrom(_c.getMiddle());
    } else {
      new Throwable("can't init with a no middle bc");
    }
  }

  protected void initFrom(final H2dRefluxBoundaryConditionMiddle _c) {
    super.initFrom(_c);
    type_ = _c.getBoundaryType();
  }

  @Override
  protected void initValueFrom(final H2dRefluxBoundaryCondition _c) {
    if (_c.isMiddle()) {
      initValueFrom(_c.getMiddle());
    } else {
      new Throwable("can't init with a no middle bc");
    }
  }

  protected void initValueFrom(final H2dRefluxBoundaryConditionMiddle _c) {
    super.initValueFrom(_c);
    type_ = _c.getBoundaryType();
  }

  protected H2dRefluxBoundaryConditionMiddle setBoundaryType(final H2dBoundaryType _type) {
    if ((_type == null) || (_type == type_)) {
      return null;
    }
    if (_type == H2dRefluxBoundaryType.SOLIDE_FROTTEMENT) {
      return new H2dRefluxBoundaryConditionMiddleFriction(this);
    }
    type_ = _type;
    return this;
  }

  @Override
  public H2dRefluxBoundaryCondition createCopy() {
    return new H2dRefluxBoundaryConditionMiddle(this);
  }

  /**
   * @return le type de bord
   */
  public H2dBoundaryType getBoundaryType() {
    return type_;
  }

  /**
   * @return this;
   */
  @Override
  public final H2dRefluxBoundaryConditionMiddle getMiddle() {
    return this;
  }

  @Override
  public final boolean isMiddle() {
    return true;
  }

  @Override
  public boolean isMiddleWithFriction() {
    return false;
  }
}