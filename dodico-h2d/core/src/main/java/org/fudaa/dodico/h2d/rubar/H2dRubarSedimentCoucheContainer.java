package org.fudaa.dodico.h2d.rubar;

public class H2dRubarSedimentCoucheContainer {
  static final int NB_COUCHES_MAX = 10;
  private final H2dRubarSedimentCouche[] couches = new H2dRubarSedimentCouche[NB_COUCHES_MAX];

  public H2dRubarSedimentCoucheContainer() {
    for (int i = 0; i < NB_COUCHES_MAX; i++) {
      couches[i] = new H2dRubarSedimentCouche();
    }
  }

  public int getNbCouches() {
    return couches.length;
  }

  public H2dRubarSedimentCouche getCoucheItem(int i) {
    return couches[i];
  }
}
