package org.fudaa.dodico.h2d.rubar;

import org.junit.Test;

import static org.junit.Assert.*;

public class H2DRubarDicoParamsTest {
  @Test
  public void isTransport() {
    assertFalse(H2DRubarDicoParams.isTransport(22));
    assertTrue(H2DRubarDicoParams.isTransport(36));
  }

  @Test
  public void getnbTransportBlock() {
    assertEquals(1,H2DRubarDicoParams.getNbTransportBlock(36));
    assertEquals(3,H2DRubarDicoParams.getNbTransportBlock(58));
  }
}
