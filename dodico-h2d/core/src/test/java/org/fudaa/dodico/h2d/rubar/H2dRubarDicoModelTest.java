package org.fudaa.dodico.h2d.rubar;

import org.junit.Test;

import static org.junit.Assert.*;

public class H2dRubarDicoModelTest {
  @Test
  public void getKeys() {
    String[] keys = H2dRubarDicoModel.getKeys(0);
    assertEquals(36, keys.length);
    assertEquals(" D90 (EN M) OU CONTRAINTE CRITIQUE:", keys[35]);

    keys = H2dRubarDicoModel.getKeys(3);
    assertEquals(58, keys.length);
    assertEquals(" D90 (EN M) OU CONTRAINTE CRITIQUE:", keys[35]);
    assertEquals(" CONSTANTE DE VISCOSITE SUR OX 2  :", keys[36]);
    assertEquals(" D90 (EN M) OU CONTRAINTE CRITIQUE:", keys[46]);
    assertEquals(" D50 DES SEDIMENTS 3              :", keys[54]);
  }
}
