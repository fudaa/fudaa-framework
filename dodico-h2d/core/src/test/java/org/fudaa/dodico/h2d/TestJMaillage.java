/*
 *  @file         TestMaillage.java
 *  @creation     3 juil. 2003
 *  @modification $Date: 2007-06-11 13:07:29 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.dodico.h2d;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.operation.linemerge.LineMerger;
import org.locationtech.jts.operation.polygonize.Polygonizer;
import gnu.trove.TIntArrayList;
import gnu.trove.TIntIntHashMap;
import gnu.trove.TObjectIntHashMap;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.interpolation.InterpolationParameters;
import org.fudaa.ctulu.interpolation.InterpolationResultsHolderI;
import org.fudaa.ctulu.interpolation.InterpolationSupportPoint;
import org.fudaa.ctulu.interpolation.InterpolationSupportValuesI;
import org.fudaa.ctulu.interpolation.SupportLocationI;
import org.fudaa.ctulu.interpolation.TestJInterpolation;
import org.fudaa.ctulu.interpolation.bilinear.InterpolationBilinearSupportSorted;
import org.fudaa.ctulu.interpolation.bilinear.InterpolatorBilinear;
import org.fudaa.dodico.commun.ProgressionTestAdapter;
import org.fudaa.dodico.ef.*;
import org.fudaa.dodico.ef.impl.EfGrid;
import org.fudaa.dodico.ef.impl.EfGridArray;
import org.fudaa.dodico.ef.impl.EfLibImpl;
import org.fudaa.dodico.ef.interpolation.EfInterpolationTargetGridAdapter;
import org.fudaa.dodico.h2d.type.H2dVariableType;

/**
 * <pre>
 *            8
 *          / | \
 *         / 7|8 \
 *        /   |   \
 *       7-- 5----  6
 *       |5/ |\ 4 /  /
 *       |/3 |T\ / 6/
 *       4-- 2-- 3 /
 *       |2/  \1| /
 *       |/  0 \|/
 *       0----- 1
 * </pre>
 * 
 * maillage ci-dessus (T pour trou).
 * 
 * @author deniger
 * @version $Id: TestJMaillage.java,v 1.17 2007-06-11 13:07:29 deniger Exp $
 */
public class TestJMaillage extends TestCase {

  EfGrid mail_;

  double eps_;

  ProgressionTestAdapter progress_;

  /**
   *
   */
  public TestJMaillage() {
    progress_ = null;
    eps_ = 1E-6;
  }

  public void testPolygonizer() {
    Coordinate[] top = new Coordinate[] { new Coordinate(1, 0), new Coordinate(2, 1), new Coordinate(-2, 1),
        new Coordinate(-1, 0) };
    Coordinate[] bot = new Coordinate[] { new Coordinate(1, 0), new Coordinate(2, -1), new Coordinate(-2, -1),
        new Coordinate(-1, 0), };
    Coordinate[] middle = new Coordinate[] { new Coordinate(10, 10), new Coordinate(15, 10), };
    Coordinate[] middle2 = new Coordinate[] { new Coordinate(10, 10), new Coordinate(20, 10), };
    Coordinate[] middle3 = new Coordinate[] { new Coordinate(1, 0), new Coordinate(-1, 0), };
    Polygonizer test = new Polygonizer();
    test.add(GISGeometryFactory.INSTANCE.createLineString(top));
    test.add(GISGeometryFactory.INSTANCE.createLineString(bot));
    test.add(GISGeometryFactory.INSTANCE.createLineString(middle));
    test.add(GISGeometryFactory.INSTANCE.createLineString(middle2));
    test.add(GISGeometryFactory.INSTANCE.createLineString(middle3));
    Collection polys = test.getPolygons();
    for (Iterator it = polys.iterator(); it.hasNext();) {
      System.out.println("poly " + it.next());
    }
    polys = test.getInvalidRingLines();
    for (Iterator it = polys.iterator(); it.hasNext();) {
      System.out.println("invalide " + it.next());
    }
    polys = test.getCutEdges();
    for (Iterator it = polys.iterator(); it.hasNext();) {
      System.out.println("cut " + it.next());
    }
    polys = test.getDangles();
    LineMerger merger = new LineMerger();
    for (Iterator it = polys.iterator(); it.hasNext();) {
      merger.add((Geometry) it.next());
    }
    for (Iterator it = merger.getMergedLineStrings().iterator(); it.hasNext();) {
      System.out.println("merged " + it.next());
    }

  }

  public void testNeighborAdj() {
    final EfGridInterface grid = EfTestFactory.buildGrid(10, 10);
    assertNotNull(grid);
    EfNeighborMesh neighbor = EfNeighborMesh.compute(grid, null);
    int[] test = neighbor.getAdjacentMeshes(0, 11);
    assertNotNull(test);
    assertEquals(2, test.length);
    assertTrue(test[0] != test[1]);
    assertTrue(test[0] == 0 || test[0] == 1);
    assertTrue(test[1] == 0 || test[1] == 1);
    test = neighbor.getAdjacentMeshes(0, 1);
    assertNotNull(test);
    assertEquals(2, test.length);
    assertEquals(-1, test[1]);
    assertEquals(0, test[0]);

  }

  public void testAbsCurve() {
    initMaillage();
    mail_.computeBord(null, null);
    final int nbPtOnFr = mail_.getFrontiers().getNbPt(0);
    final double[] data = mail_.getFrontiers().getAbsCurviligne(mail_, 0, 0, nbPtOnFr);
    mail_.getFrontiers().printFullString();
    assertNotNull(data);
    assertEquals(nbPtOnFr, data.length);
    assertEquals(0, data[0], eps_);
    final int[] pt = new int[] { 0, 1, 6, 8, 7, 4 };
    assertEquals(nbPtOnFr, pt.length);
    final double[] resAttendu = new double[pt.length];
    for (int i = 1; i < pt.length; i++) {
      final double x1 = mail_.getPtX(pt[i - 1]);
      final double y1 = mail_.getPtY(pt[i - 1]);
      final double x2 = mail_.getPtX(pt[i]);
      final double y2 = mail_.getPtY(pt[i]);
      resAttendu[i] = resAttendu[i - 1] + CtuluLibGeometrie.getDistance(x1, y1, x2, y2);
      assertEquals("test pour le point " + i, resAttendu[i], data[i], eps_);
    }
  }

  /**
   * Aire CtuluGeometrie.
   */
  public void testAireTriangle() {
    final double x1 = 0;
    final double y1 = 0;
    final double x2 = 5;
    final double y2 = 0;
    double x3 = 0;
    double y3 = 5;
    assertEquals(12.5, CtuluLibGeometrie.aireTriangle(x1, y1, x2, y2, x3, y3), eps_);
    x3 = -15;
    assertEquals(12.5, CtuluLibGeometrie.aireTriangle(x1, y1, x2, y2, x3, y3), eps_);
    x3 = 15;
    y3 = -5;
    assertEquals(12.5, CtuluLibGeometrie.aireTriangle(x1, y1, x2, y2, x3, y3), eps_);
    initMaillage();
    int[] l = mail_.getSmallElement(null, 1);
    assertEquals(1, mail_.getAire(2), eps_);
    assertNotNull(l);
    assertEquals(3, l.length);
    Arrays.sort(l);
    assertEquals(2, l[0]);
    assertEquals(3, l[1]);
    assertEquals(5, l[2]);
    l = mail_.getSmallElement(null, 0.95);
    assertNull(l);

    initMaillageT6();
    assertEquals(1, mail_.getAire(2), eps_);
    l = mail_.getSmallElement(null, 1);
    assertNotNull(l);
    assertEquals(3, l.length);
    Arrays.sort(l);
    assertEquals(2, l[0]);
    assertEquals(3, l[1]);
    assertEquals(5, l[2]);
    l = mail_.getSmallElement(null, 0.95);
    assertNull(l);

  }

  /**
   * @return maillage T3.
   */
  public static EfGrid createMaillage() {
    final EfNode[] pts = new EfNode[9];
    pts[0] = new EfNode(0, 0, 15);
    pts[1] = new EfNode(2, -1, -15);
    pts[2] = new EfNode(1, 2, -5);
    pts[3] = new EfNode(2, 2, 30);
    pts[4] = new EfNode(0, 2, 30);
    pts[5] = new EfNode(1, 4, 30);
    pts[6] = new EfNode(10, 11, 30);
    pts[7] = new EfNode(0, 4, 30);
    pts[8] = new EfNode(2, 20, 30);
    final EfElement[] elt = new EfElement[9];
    elt[0] = new EfElement(new int[] { 0, 2, 1 });
    elt[1] = new EfElement(new int[] { 1, 2, 3 });
    elt[2] = new EfElement(new int[] { 0, 4, 2 });
    elt[3] = new EfElement(new int[] { 4, 5, 2 });
    elt[4] = new EfElement(new int[] { 5, 6, 3 });
    elt[5] = new EfElement(new int[] { 5, 4, 7 });
    elt[6] = new EfElement(new int[] { 1, 3, 6 });
    elt[7] = new EfElement(new int[] { 7, 8, 5 });
    elt[8] = new EfElement(new int[] { 6, 5, 8 });
    return new EfGrid(pts, elt);
  }

  private void initMaillage() {
    mail_ = createMaillage();
  }

  private void initMaillageT6() {
    mail_ = createMaillageT6();
  }

  /**
   * @return le maillage en version T6
   */
  public static EfGrid createMaillageT6() {
    final EfNode[] pts = new EfNode[27];
    pts[0] = new EfNode(0, 0, 15);
    pts[1] = new EfNode(2, -1, -15);
    pts[2] = new EfNode(1, 2, -5);
    pts[3] = new EfNode(2, 2, 30);
    pts[4] = new EfNode(0, 2, 30);
    pts[5] = new EfNode(1, 4, 30);
    pts[6] = new EfNode(10, 11, 30);
    pts[7] = new EfNode(0, 4, 30);
    pts[8] = new EfNode(2, 20, 30);
    // elt 0
    pts[9] = EfNode.createMilieu(pts[0], pts[2]);
    pts[10] = EfNode.createMilieu(pts[2], pts[1]);
    pts[11] = EfNode.createMilieu(pts[1], pts[0]);
    // elt 1
    pts[12] = EfNode.createMilieu(pts[2], pts[3]);
    pts[13] = EfNode.createMilieu(pts[1], pts[3]);
    // pts10
    // elt 2
    pts[14] = EfNode.createMilieu(pts[0], pts[4]);
    pts[15] = EfNode.createMilieu(pts[4], pts[2]);
    // pts 9
    // elt 3
    pts[16] = EfNode.createMilieu(pts[5], pts[4]);
    pts[17] = EfNode.createMilieu(pts[5], pts[2]);
    // pts 15
    // elt 4
    pts[18] = EfNode.createMilieu(pts[5], pts[6]);
    pts[19] = EfNode.createMilieu(pts[3], pts[6]);
    pts[20] = EfNode.createMilieu(pts[5], pts[3]);
    // elt 5
    pts[21] = EfNode.createMilieu(pts[4], pts[7]);
    pts[22] = EfNode.createMilieu(pts[5], pts[7]);
    // pts 16
    // elt 6
    // pts12
    // pts19
    pts[23] = EfNode.createMilieu(pts[6], pts[1]);
    // elt 7
    pts[24] = EfNode.createMilieu(pts[7], pts[8]);
    pts[25] = EfNode.createMilieu(pts[8], pts[5]);
    // pts 21
    // elt 8
    pts[26] = EfNode.createMilieu(pts[6], pts[8]);
    // Pts 25
    // pts 20
    final EfElement[] elt = new EfElement[9];
    elt[0] = new EfElement(new int[] { 0, 9, 2, 10, 1, 11 });
    elt[1] = new EfElement(new int[] { 1, 10, 2, 12, 3, 13 });
    elt[2] = new EfElement(new int[] { 0, 14, 4, 15, 2, 9 });
    elt[3] = new EfElement(new int[] { 4, 16, 5, 17, 2, 15 });
    elt[4] = new EfElement(new int[] { 5, 18, 6, 19, 3, 20 });
    elt[5] = new EfElement(new int[] { 5, 16, 4, 21, 7, 22 });
    elt[6] = new EfElement(new int[] { 1, 13, 3, 19, 6, 23 });
    elt[7] = new EfElement(new int[] { 7, 24, 8, 25, 5, 22 });
    elt[8] = new EfElement(new int[] { 6, 18, 5, 25, 8, 26 });
    return new EfGrid(pts, elt);
  }

  public void testNeighbor() {
    initMaillage();
    final EfNeighborMesh mesh = EfNeighborMesh.compute(mail_, null);
    assertNotNull(mesh);
    int idx = 0;
    // 0
    assertEquals(2, mesh.getNbNeighborMeshes(idx));
    assertEquals(0, mesh.getNeighborMesh(idx, 0));
    assertEquals(2, mesh.getNeighborMesh(idx, 1));

    // 1
    idx++;
    assertEquals(3, mesh.getNbNeighborMeshes(idx));
    assertEquals(0, mesh.getNeighborMesh(idx, 0));
    assertEquals(1, mesh.getNeighborMesh(idx, 1));
    assertEquals(6, mesh.getNeighborMesh(idx, 2));

    // 2
    idx++;
    assertEquals(4, mesh.getNbNeighborMeshes(idx));
    assertEquals(0, mesh.getNeighborMesh(idx, 0));
    assertEquals(1, mesh.getNeighborMesh(idx, 1));
    assertEquals(2, mesh.getNeighborMesh(idx, 2));
    assertEquals(3, mesh.getNeighborMesh(idx, 3));

    // 3
    idx++;
    assertEquals(3, mesh.getNbNeighborMeshes(idx));
    assertEquals(1, mesh.getNeighborMesh(idx, 0));
    assertEquals(4, mesh.getNeighborMesh(idx, 1));
    assertEquals(6, mesh.getNeighborMesh(idx, 2));

    // 4
    idx++;
    assertEquals(3, mesh.getNbNeighborMeshes(idx));
    assertEquals(2, mesh.getNeighborMesh(idx, 0));
    assertEquals(3, mesh.getNeighborMesh(idx, 1));
    assertEquals(5, mesh.getNeighborMesh(idx, 2));

    // 5
    idx++;
    assertEquals(5, mesh.getNbNeighborMeshes(idx));
    assertEquals(3, mesh.getNeighborMesh(idx, 0));
    assertEquals(4, mesh.getNeighborMesh(idx, 1));
    assertEquals(5, mesh.getNeighborMesh(idx, 2));
    assertEquals(7, mesh.getNeighborMesh(idx, 3));
    assertEquals(8, mesh.getNeighborMesh(idx, 4));

    // 6
    idx++;
    assertEquals(3, mesh.getNbNeighborMeshes(idx));
    assertEquals(4, mesh.getNeighborMesh(idx, 0));
    assertEquals(6, mesh.getNeighborMesh(idx, 1));
    assertEquals(8, mesh.getNeighborMesh(idx, 2));

    // 7
    idx++;
    assertEquals(2, mesh.getNbNeighborMeshes(idx));
    assertEquals(5, mesh.getNeighborMesh(idx, 0));
    assertEquals(7, mesh.getNeighborMesh(idx, 1));

    // 8
    idx++;
    assertEquals(2, mesh.getNbNeighborMeshes(idx));
    assertEquals(7, mesh.getNeighborMesh(idx, 0));
    assertEquals(8, mesh.getNeighborMesh(idx, 1));

  }

  /**
   * Orientation trigo.
   */
  public void testTrigo() {
    initMaillage();
    EfElement el = mail_.getElement(0);
    assertEquals(el.getPtIndex(0), 0);
    assertEquals(el.getPtIndex(1), 2);
    assertEquals(el.getPtIndex(2), 1);
    el = mail_.getElement(1);
    assertEquals(el.getPtIndex(0), 1);
    assertEquals(el.getPtIndex(1), 2);
    assertEquals(el.getPtIndex(2), 3);
    assertEquals(mail_.isElementTrigoOriente(0), -1);
    assertEquals(mail_.isElementTrigoOriente(1), -1);
    assertEquals(el.isSegmentInEltOrient(1, 3), -1);
    assertEquals(el.isSegmentInEltOrient(3, 2), -1);
    assertEquals(el.isSegmentInEltOrient(2, 1), -1);
    assertEquals(el.isSegmentInEltOrient(2, 3), 1);
    assertEquals(el.isSegmentInEltOrient(2, 2), 0);
    double d = el.getAngle(mail_, 2) * 180 / Math.PI;
    assertEquals(90, d, eps_);
    d = el.getAngle(mail_, 0) * 180 / Math.PI;
    assertEquals(18.4349, d, 1e-3);
    d = el.getAngle(mail_, 1) * 180 / Math.PI;
    assertEquals(90 - 18.4349, d, 1e-3);
    testCentroide(mail_);
    // on test sur un element a 4 points
    final double[] xyz = new double[3 * 4];
    for (int i = 0; i < xyz.length; i++) {
      xyz[i] = Math.random() * 100;
    }
    final EfElement[] elts = new EfElement[] { new EfElement(new int[] { 0, 1, 2, 3 }) };
    testCentroide(new EfGridArray(xyz, elts));
  }

  private void testCentroide(final EfGridInterface _mail) {
    for (int j = 0; j < _mail.getEltNb(); j++) {
      final EfElement el = _mail.getElement(j);
      final Coordinate[] c = new Coordinate[el.getPtNb() + 1];
      for (int k = 0; k < el.getPtNb(); k++) {
        c[k] = new Coordinate(_mail.getPtX(el.getPtIndex(k)), _mail.getPtY(el.getPtIndex(k)));
      }
      c[c.length - 1] = c[0];
      final LinearRing r = GISGeometryFactory.INSTANCE.createLinearRing(c);
      final Coordinate ci = new Coordinate();
      _mail.getCentreElement(j, ci);
      assertEquals(r.getCentroid().getCoordinate().x, ci.x, 1E-5);
      assertEquals(r.getCentroid().getCoordinate().y, ci.y, 1E-5);
    }
  }

  /**
   * Elt surcontraint.
   */
  public void testOverstressed() {
    initMaillage();
    final int[] overI = mail_.getOverstressedElement(null);
    assertNotNull(overI);
    assertEquals(9, overI.length);
    final EfElement[] nElt = new EfElement[mail_.getEltNb() + 1];
    for (int i = 0; i < mail_.getEltNb(); i++) {
      nElt[i] = new EfElement(mail_.getElement(i));
    }
    nElt[mail_.getEltNb()] = new EfElement(new int[] { 2, 3, 5 });
    final EfNode[] nPt = new EfNode[mail_.getPtsNb() + 1];
    for (int i = 0; i < mail_.getPtsNb(); i++) {
      nPt[i] = new EfNode(mail_.getPt(i));
    }
    final EfGrid n = new EfGrid(nPt, nElt);
    EfLib.orienteGrid(n, null, true, null);
    n.computeBord(null, null);
    assertNull(n.getOverstressedElement(null));

  }

  /**
   * Maillage.contientPoint.
   */
  public void testContains() {
    initMaillage();
    mail_.computeBord(null, null);
    // Polygon l=new Polygon();
    //
    // for(int i=mail_.getFrontiers().getNbPt(0)-1;i>=0;i--){
    // l.addPoint((int)mail_.getPointOnFrontierExtern(i).getX(),
    // (int)mail_.getPointOnFrontierExtern(i).getY());
    // }
    // for(int i=mail_.getFrontiers().getNbPt(0)-1;i>=0;i--){
    // assertTrue(l.contains(mail_.getPointOnFrontierExtern(i).getX(),mail_.getPointOnFrontierExtern(i).getY()));
    // }
    // System.out.println("POLYGON OK");

    for (int i = mail_.getFrontiers().getNbPt(0) - 1; i >= 0; i--) {
      assertTrue(mail_.contientPoint(mail_.getPt(mail_.getPointOnFrontierExtern(i))));
    }

    assertFalse(mail_.contientPoint(0, 5));
    assertFalse(mail_.contientPoint(2.1, 20));
    assertTrue(mail_.contientPoint(1, 12));
    assertEquals(1, mail_.getDistanceFromFrontier(-1, 1), eps_);
    assertEquals(1, mail_.getDistanceFromFrontier(1, 1), eps_);
    assertEquals(0, mail_.getDistanceFromFrontier(1, -0.5), eps_);
  }

  /**
   * Orientation sur maillage T6.
   */
  public void testTrigoT6() {
    initMaillageT6();
    final EfElement el = mail_.getElement(0);
    assertEquals(el.getPtIndex(0), 0);
    assertEquals(el.getPtIndex(1), 9);
    assertEquals(el.getPtIndex(2), 2);
    for (int i = 0; i < 9; i++) {
      assertEquals(mail_.isElementTrigoOriente(i), -1);
    }
    assertEquals(el.isSegmentInEltOrient(0, 9), 1);
    assertEquals(el.isSegmentInEltOrient(9, 2), 1);
    assertEquals(el.isSegmentInEltOrient(2, 10), 1);
    assertEquals(el.isSegmentInEltOrient(10, 1), 1);
    assertEquals(el.isSegmentInEltOrient(1, 11), 1);
    assertEquals(el.isSegmentInEltOrient(11, 0), 1);
    assertEquals(el.isSegmentInEltOrient(0, 11), -1);
    assertEquals(el.isSegmentInEltOrient(0, 12), 0);
    final TIntIntHashMap[] middle = mail_.getMiddleBounds();
    assertNotNull(middle);
    assertEquals(2, middle.length);
    assertNotNull(middle[0]);
    assertNotNull(middle[1]);
    assertTrue(middle[0].contains(9));
    assertTrue(middle[1].contains(9));
    final int idx = middle[0].get(9);
    final int idx2 = middle[1].get(9);
    assertEquals(0, Math.min(idx, idx2));
    assertEquals(2, Math.max(idx, idx2));

  }

  private void internalTestMinMaxCommun() {
    assertEquals(mail_.getMaxX(), 10, eps_);
    assertEquals(mail_.getMaxY(), 20, eps_);
    assertEquals(mail_.getMaxZ(), 30, eps_);
    assertEquals(mail_.getMinX(), 0, eps_);
    assertEquals(mail_.getMinY(), -1, eps_);
    assertEquals(mail_.getMinZ(), -15, eps_);
  }

  /**
   * Min et max du maillage.
   */
  public void testMinMaxCommun() {
    initMaillage();
    internalTestMinMaxCommun();
  }

  /**
   * Min et max du maillage T6.
   */
  public void testMinMaxCommunT6() {
    initMaillageT6();
    internalTestMinMaxCommun();
  }

  protected void internalTestBordCommun() {
    final EfFrontierInterface bord = mail_.getFrontiers();
    assertEquals(2, bord.getNbFrontier());
    final int[] eltContains4 = mail_.getEltIdxContainsPtIdx(4);
    Arrays.sort(eltContains4);
    assertEquals(eltContains4.length, 3);
    assertTrue(Arrays.binarySearch(eltContains4, 2) >= 0);
    assertTrue(Arrays.binarySearch(eltContains4, 3) >= 0);
    assertTrue(Arrays.binarySearch(eltContains4, 5) >= 0);
    int n = bord.getNbPt(0);
    assertEquals(n, 6);
    assertEquals(0, bord.getIdxGlobal(0, 0));
    assertEquals(1, bord.getIdxGlobal(0, 1));
    assertEquals(6, bord.getIdxGlobal(0, 2));
    assertEquals(8, bord.getIdxGlobal(0, 3));
    assertEquals(7, bord.getIdxGlobal(0, 4));
    assertEquals(4, bord.getIdxGlobal(0, 5));
    n = bord.getNbPt(1);
    assertEquals(n, 3);
    assertEquals(2, bord.getIdxGlobal(1, 0));
    assertEquals(5, bord.getIdxGlobal(1, 1));
    assertEquals(3, bord.getIdxGlobal(1, 2));
  }

  /**
   * Test du bord T3.
   */
  public void testBord() {
    initMaillage();
    mail_.computeBord(null, null);
    internalTestBordCommun();
  }

  /**
   * Test du bord T6.
   */
  public void testBordT6() {
    initMaillageT6();
    mail_.computeBord(null, null);
    internalTestBordCommunT6();
  }

  /**
   * Teste les bords du maillage.
   */
  public void internalTestBordCommunT6() {
    final EfFrontierInterface bord = mail_.getFrontiers();
    assertEquals(bord.getNbFrontier(), 2);
    final int[] eltContains4 = mail_.getEltIdxContainsPtIdx(4);
    Arrays.sort(eltContains4);
    assertEquals(eltContains4.length, 3);
    assertTrue(Arrays.binarySearch(eltContains4, 2) >= 0);
    assertTrue(Arrays.binarySearch(eltContains4, 3) >= 0);
    assertTrue(Arrays.binarySearch(eltContains4, 5) >= 0);
    int n = bord.getNbPt(0);
    assertEquals(n, 12);
    assertEquals(0, bord.getIdxGlobal(0, 0));
    assertEquals(11, bord.getIdxGlobal(0, 1));
    assertEquals(1, bord.getIdxGlobal(0, 2));
    assertEquals(23, bord.getIdxGlobal(0, 3));
    assertEquals(6, bord.getIdxGlobal(0, 4));
    assertEquals(26, bord.getIdxGlobal(0, 5));
    assertEquals(8, bord.getIdxGlobal(0, 6));
    assertEquals(24, bord.getIdxGlobal(0, 7));
    assertEquals(7, bord.getIdxGlobal(0, 8));
    assertEquals(21, bord.getIdxGlobal(0, 9));
    assertEquals(4, bord.getIdxGlobal(0, 10));
    assertEquals(14, bord.getIdxGlobal(0, 11));
    n = bord.getNbPt(1);
    assertEquals(n, 6);
    assertEquals(2, bord.getIdxGlobal(1, 0));
    assertEquals(17, bord.getIdxGlobal(1, 1));
    assertEquals(5, bord.getIdxGlobal(1, 2));
    assertEquals(20, bord.getIdxGlobal(1, 3));
    assertEquals(3, bord.getIdxGlobal(1, 4));
    assertEquals(12, bord.getIdxGlobal(1, 5));
    for (int i = 0; i <= 8; i++) {
      assertTrue(mail_.isExtremePoint(i));
      assertFalse(mail_.isMiddlePoint(i));
    }
    for (int i = 9; i < mail_.getPtsNb(); i++) {
      assertTrue(mail_.isMiddlePoint(i));
      assertFalse(mail_.isExtremePoint(i));
    }

  }

  /**
   * Transformation T3->T6.
   */
  public void testT3T6() {
    initMaillage();
    final TObjectIntHashMap segmentPtIdx = new TObjectIntHashMap(mail_.getEltNb() * 3);
    final EfGridInterface temp = EfLibImpl.maillageT3enT6(mail_, progress_, segmentPtIdx);
    assertEquals(temp.getEltNb(), 9);
    assertEquals(27, temp.getPtsNb());
    initMaillageT6();
    assertTrue(temp.isEquivalent(mail_, true, eps_));
    final EfSegmentMutable s = new EfSegmentMutable(0, 1);
    assertEquals(11, segmentPtIdx.get(s));
    s.setPt1Idx(1);
    s.setPt2Idx(2);
    assertEquals(10, segmentPtIdx.get(s));
    s.setPt1Idx(2);
    s.setPt2Idx(3);
    assertTrue(segmentPtIdx.contains(s));
    assertEquals(12, segmentPtIdx.get(s));
    s.setPt1Idx(1);
    s.setPt2Idx(3);
    assertEquals(13, segmentPtIdx.get(s));
    s.setPt1Idx(0);
    s.setPt2Idx(4);
    assertEquals(14, segmentPtIdx.get(s));
    s.setPt1Idx(2);
    s.setPt2Idx(4);
    assertEquals(15, segmentPtIdx.get(s));
    s.setPt1Idx(4);
    s.setPt2Idx(5);
    assertEquals(16, segmentPtIdx.get(s));
    s.setPt1Idx(2);
    s.setPt2Idx(5);
    assertEquals(17, segmentPtIdx.get(s));
    s.setPt1Idx(5);
    s.setPt2Idx(6);
    assertEquals(18, segmentPtIdx.get(s));
    s.setPt1Idx(3);
    s.setPt2Idx(6);
    assertEquals(19, segmentPtIdx.get(s));
    s.setPt1Idx(3);
    s.setPt2Idx(5);
    assertEquals(20, segmentPtIdx.get(s));
    s.setPt1Idx(4);
    s.setPt2Idx(7);
    assertEquals(21, segmentPtIdx.get(s));
    s.setPt1Idx(5);
    s.setPt2Idx(7);
    assertEquals(22, segmentPtIdx.get(s));
    s.setPt1Idx(1);
    s.setPt2Idx(6);
    assertEquals(23, segmentPtIdx.get(s));
    s.setPt1Idx(7);
    s.setPt2Idx(8);
    assertEquals(24, segmentPtIdx.get(s));
    s.setPt1Idx(5);
    s.setPt2Idx(8);
    assertEquals(25, segmentPtIdx.get(s));
    s.setPt1Idx(6);
    s.setPt2Idx(8);
    assertEquals(26, segmentPtIdx.get(s));
    assertTrue(temp.isSameStrict(mail_, null, 0));
  }

  /**
   * transfo T6 en 1 T3.
   */
  public void testT61T3() {
    initMaillageT6();
    final TIntIntHashMap t6T3Corresp = new TIntIntHashMap(mail_.getPtsNb() / 2);
    final EfGridInterface temp = EfLibImpl.maillageT6enT3(mail_, progress_, t6T3Corresp);
    assertNotNull(temp);
    temp.isSameStrict(mail_, null, 0);
    assertEquals(9, t6T3Corresp.size());
    for (int i = 0; i < 9; i++) {
      assertEquals(i, t6T3Corresp.get(i));
    }
    // Autre essai
    EfNode[] pts = new EfNode[15];
    pts[0] = new EfNode(0, 0, 0);
    pts[1] = new EfNode(1, 1, 0);
    pts[2] = new EfNode(2, 2, 0);
    pts[3] = new EfNode(3, 3, 0);
    pts[4] = new EfNode(4, 4, 0);
    pts[5] = new EfNode(5, 3, 0);
    pts[6] = new EfNode(6, 2, 0);
    pts[7] = new EfNode(7, 1, 0);
    pts[8] = new EfNode(8, 0, 0);
    pts[9] = new EfNode(6, 0, 0);
    pts[10] = new EfNode(4, 0, 0);
    pts[11] = new EfNode(2, 0, 0);
    pts[12] = new EfNode(3, 1, 0);
    pts[13] = new EfNode(4, 2, 0);
    pts[14] = new EfNode(5, 1, 0);
    EfElement[] elt = new EfElement[4];
    elt[0] = new EfElement(new int[] { 0, 1, 2, 12, 10, 11 });
    elt[1] = new EfElement(new int[] { 2, 3, 4, 5, 6, 13 });
    elt[2] = new EfElement(new int[] { 6, 7, 8, 9, 10, 14 });
    elt[3] = new EfElement(new int[] { 10, 12, 2, 13, 6, 14 });
    // Attention a bien initialiser la table de correspondance.
    t6T3Corresp.clear();
    final EfGridInterface t3calc = EfLibImpl.maillageT6enT3(new EfGrid(pts, elt), null, t6T3Corresp);
    pts = new EfNode[6];
    pts[0] = new EfNode(0, 0, 0);
    pts[1] = new EfNode(2, 2, 0);
    pts[2] = new EfNode(4, 4, 0);
    pts[3] = new EfNode(6, 2, 0);
    pts[4] = new EfNode(8, 0, 0);
    pts[5] = new EfNode(4, 0, 0);
    elt = new EfElement[4];
    elt[0] = new EfElement(new int[] { 0, 1, 5 });
    elt[1] = new EfElement(new int[] { 1, 2, 3 });
    elt[2] = new EfElement(new int[] { 3, 4, 5 });
    elt[3] = new EfElement(new int[] { 5, 1, 3 });
    final EfGrid t3final = new EfGrid(pts, elt);
    assertTrue(t3calc.isSameStrict(t3final, null, 0));
    for (int i = 0; i < pts.length; i++) {
      assertEquals(i, t6T3Corresp.get(2 * i));
    }
  }

  /**
   * Transfo T6 en 4 T3.
   */
  public void testT6en4T3() {
    initMaillageT6();
    final EfGridInterface temp = EfLibImpl.maillageT6en4T3(mail_, progress_);
    assertNotNull(temp);
    assertEquals(36, temp.getEltNb());
    EfElement t = temp.getElement(0);
    assertEquals(t.getPtIndex(0), 0);
    assertEquals(t.getPtIndex(1), 9);
    assertEquals(t.getPtIndex(2), 11);
    t = temp.getElement(12);
    assertEquals(t.getPtIndex(0), 4);
    assertEquals(t.getPtIndex(1), 16);
    assertEquals(t.getPtIndex(2), 15);
    t = temp.getElement(13);
    assertEquals(t.getPtIndex(0), 16);
    assertEquals(t.getPtIndex(1), 5);
    assertEquals(t.getPtIndex(2), 17);
    t = temp.getElement(14);
    assertEquals(t.getPtIndex(0), 17);
    assertEquals(t.getPtIndex(1), 2);
    assertEquals(t.getPtIndex(2), 15);
    t = temp.getElement(15);
    assertEquals(t.getPtIndex(0), 16);
    assertEquals(t.getPtIndex(1), 17);
    assertEquals(t.getPtIndex(2), 15);
    t = temp.getElement(35);
    assertEquals(t.getPtIndex(0), 18);
    assertEquals(t.getPtIndex(1), 25);
    assertEquals(t.getPtIndex(2), 26);
  }

  /**
   * Frontiere.
   */
  public void testIpobo() {
    initMaillage();
    final int[] ipobo = new int[] { 0, 1, 6, 8, 7, 4, 2, 5, 3 };
    mail_.computeBordFast(ipobo, null, null);
    internalTestBordCommun();
  }

  /**
   * Iterateur sur les frontieres.
   */
  public void testIterator() {
    final TIntArrayList l = new TIntArrayList();
    initMaillage();
    mail_.computeBord(null, null);
    for (final AllFrontierIteratorInterface it = mail_.getFrontiers().getAllFrontierIterator(); it.hasNext();) {
      l.add(it.next());
    }
    assertTrue(mail_.getFrontiers().isSame(l.toNativeArray()));
  }

  private static class GridRef implements SupportLocationI, InterpolationSupportValuesI {

    final EfGridInterface g_;

    GridRef(final EfGridInterface _g) {
      g_ = _g;
    }

    public int getNbValues() {
      return 1;
    }

    public EfGridInterface getGrid() {
      return g_;
    }

    public double getV(final int _i) {
      return g_.getPtZ(_i);
    }

    @Override
    public int getPtsNb() {
      return g_.getPtsNb();
    }

    @Override
    public double getPtX(final int _i) {
      return g_.getPtX(_i);
    }

    @Override
    public double getPtY(final int _i) {
      return g_.getPtY(_i);
    }

    @Override
    public double getV(final CtuluVariable _valueIdx, final int _ptIdx) {
      return g_.getPtZ(_ptIdx);
    }
  }

  /**
   * Iterpolation plus pres voisin.
   */
  public void testInterpRef() {
    initMaillage();
    final InterpolationBilinearSupportSorted ref = InterpolationBilinearSupportSorted.buildSortedSrc(
        new GridRef(mail_), null);
    final int[] d = new int[4];
    assertTrue(ref.getQuadrantIdx(1, -2, d));
    assertEquals(-1, d[0]);
    assertEquals(-1, d[1]);
    assertEquals(1, d[2]);
    assertEquals(0, d[3]);
    assertTrue(ref.getQuadrantIdx(1, 0.5, d));
    assertEquals(0, d[0]);
    assertEquals(1, d[1]);
    assertEquals(2, d[2]);
    assertEquals(4, d[3]);
    assertTrue(ref.getQuadrantIdx(0.5, 3, d));
    assertEquals(4, d[0]);
    assertEquals(2, d[1]);
    assertEquals(5, d[2]);
    assertEquals(7, d[3]);
    // pour verifier que les -1 sont bien remis
    assertTrue(ref.getQuadrantIdx(1, -2, d));
    assertEquals(-1, d[0]);
    assertEquals(-1, d[1]);
    assertEquals(1, d[2]);
    assertEquals(0, d[3]);
  }

  /**
   * Test l'interpolation sur un maillage.
   */
  public void testInterpolation() {
    initMaillage();
    final EfGrid g2 = createMaillage();
    // premier test primaire pour verifier l'interpolation
    final CtuluAnalyze a = new CtuluAnalyze();
    CtuluPermanentList vars = new CtuluPermanentList(new Object[] { H2dVariableType.BATHYMETRIE });
    GridRef src = new GridRef(g2);
    InterpolationParameters params = new InterpolationParameters(vars, new EfInterpolationTargetGridAdapter(mail_,
        false), src);
    params.createDefaultResults();
    InterpolatorBilinear interpolateur = new InterpolatorBilinear(src);
    interpolateur.interpolate(params);
    a.merge(params.getAnalyze());
    assertTrue(a.isEmpty());
    InterpolationResultsHolderI results = params.getResults();
    assertEquals(mail_.getPtsNb(), results.getNbPtResultSet());
    for (int i = 0; i < mail_.getPtsNb(); i++) {
      final double v = results.getValuesForPt(i)[0];
      assertEquals(v, mail_.getPt(i).getZ(), eps_);
    }
    final List l = new ArrayList();
    // le point 0
    l.add(new EfNode(-0.5, -0.5, 3));
    // le point 0
    l.add(new EfNode(0.5, -0.5, 14));
    l.add(new EfNode(0.5, 0.5, 4));
    // le point 4
    l.add(new EfNode(-0.5, 2, -4));
    // le point 4 et 2
    l.add(new EfNode(0.5, 2, 14));
    // le point 7
    l.add(new EfNode(-0.5, 4, 3));
    // le point 7 et 5
    l.add(new EfNode(0.5, 4, 34));
    // le point 5
    l.add(new EfNode(1.5, 4, 21354));
    // le point 2 et 3
    l.add(new EfNode(1.5, 2, 3));
    // le point 3
    l.add(new EfNode(3.5, 2, 45));
    // le point 1
    l.add(new EfNode(1.5, -1.5, 45));
    // le point 1
    l.add(new EfNode(2.5, -1.5, 1));
    // le point 6
    l.add(new EfNode(10, 11.5, 0));
    // le point 6
    l.add(new EfNode(10, 10.5, 14));
    // le point 8 qui ne sera pas pris en compte
    // car les points ref en dehors de la frontiere et trop loin
    l.add(new EfNode(-1, 20, 4));
    l.add(new EfNode(5, 20, 4));
    // les z attendus
    // 0
    final double dist = CtuluLibGeometrie.getDistance(0, 0, 0.5, 0.5);
    double dist2 = CtuluLibGeometrie.getDistance(0, 0, -0.5, 2);
    final EfNode[] list = new EfNode[l.size()];
    l.toArray(list);
    InterpolationSupportPoint support = new InterpolationSupportPoint(list);
    params = new InterpolationParameters(vars, new EfInterpolationTargetGridAdapter(mail_, false), support);
    params.createDefaultResults();
    results=params.getResults();
    interpolateur = new InterpolatorBilinear(support);
    interpolateur.setDistance(1);
    interpolateur.interpolate(params);
    a.merge(params.getAnalyze());
    assertFalse(a.isEmpty());
    assertEquals(params.getResults().getNbPtResultSet(), mail_.getPtsNb());
    // it = map.iterator();
    assertEquals(TestJInterpolation.getInterpolValue(dist, 3, dist2, -4, dist, 14, dist, 4), results.getValuesForPt(0)[0], eps_);
    assertEquals(TestJInterpolation.getInterpolValue(dist, 45, dist, 1, CtuluLibGeometrie.getDistance(2, -1, 3.5, 2), 45,
        CtuluLibGeometrie.getDistance(2, -1, 0.5, -0.5), 14), results.getValuesForPt(1)[0], eps_);

  }

  public void testDistanceEnv() {
    final Envelope env = new Envelope();
    env.expandToInclude(0, 0);
    env.expandToInclude(20, 30);
    assertEquals(1, GISLib.getDistance(-1, 15, env), eps_);
    assertEquals(0, GISLib.getDistance(0, 10, env), eps_);
    assertEquals(1, GISLib.getDistance(0, -1, env), eps_);

  }

  final double minX_ = -100;

  final double maxX_ = 100000;

  final double minY_ = 100;

  final double maxY_ = 1000000;

  final double minXSrc_ = -300;

  final double maxXSrc_ = 150000;

  final double minYSrc_ = 400;

  final double maxYSrc_ = 1400000;

  final double ecartX_ = Math.abs(maxX_ - minX_);

  final double ecartY_ = Math.abs(maxY_ - minY_);

  final double ecartXSrc_ = Math.abs(maxXSrc_ - minXSrc_);

  final double ecartYSrc_ = Math.abs(maxYSrc_ - minYSrc_);

  public double genereX() {
    return minX_ + Math.random() * ecartX_;
  }

  public double genereY() {
    return minY_ + Math.random() * ecartY_;
  }

  public double genereXSrc() {
    return minXSrc_ + Math.random() * ecartXSrc_;
  }

  public double genereYSrc() {
    return minYSrc_ + Math.random() * ecartYSrc_;
  }

  public void testSrcOpt() {
    final GISPoint[] pts = new GISPoint[16];
    for (int i = 0; i < 16; i++) {
      final int col = i % 4;
      final int row = (i - col) / 4;
      pts[i] = new GISPoint(col * 2, row * 2, Math.random());
    }
    final InterpolationBilinearSupportSorted src = InterpolationBilinearSupportSorted.buildSortedSrc(
        new InterpolationSupportPoint(pts), null);
    final int[] quadrant = new int[4];
    src.getQuadrantIdx(1, 1, quadrant);
    assertEquals(0, quadrant[0]);
    assertEquals(1, quadrant[1]);
    assertEquals(5, quadrant[2]);
    assertEquals(4, quadrant[3]);
    src.getQuadrantIdx(1, 5, quadrant);
    assertEquals(8, quadrant[0]);
    assertEquals(9, quadrant[1]);
    assertEquals(13, quadrant[2]);
    assertEquals(12, quadrant[3]);
    src.getQuadrantIdx(-1, 1, quadrant);
    assertEquals(-1, quadrant[0]);
    assertEquals(0, quadrant[1]);
    assertEquals(4, quadrant[2]);
    assertEquals(-1, quadrant[3]);
    src.getQuadrantIdx(1, -1, quadrant);
    assertEquals(-1, quadrant[0]);
    assertEquals(-1, quadrant[1]);
    assertEquals(1, quadrant[2]);
    assertEquals(0, quadrant[3]);
    src.getQuadrantIdx(7, 1, quadrant);
    assertEquals(3, quadrant[0]);
    assertEquals(-1, quadrant[1]);
    assertEquals(-1, quadrant[2]);
    assertEquals(7, quadrant[3]);
    src.getQuadrantIdx(7, 5, quadrant);
    assertEquals(11, quadrant[0]);
    assertEquals(-1, quadrant[1]);
    assertEquals(-1, quadrant[2]);
    assertEquals(15, quadrant[3]);
    src.getQuadrantIdx(5, 7, quadrant);
    assertEquals(14, quadrant[0]);
    assertEquals(15, quadrant[1]);
    assertEquals(-1, quadrant[2]);
    assertEquals(-1, quadrant[3]);
    src.getQuadrantIdx(1, 7, quadrant);
    assertEquals(12, quadrant[0]);
    assertEquals(13, quadrant[1]);
    assertEquals(-1, quadrant[2]);
    assertEquals(-1, quadrant[3]);
    src.getQuadrantIdx(-1, 7, quadrant);
    assertEquals(-1, quadrant[0]);
    assertEquals(12, quadrant[1]);
    assertEquals(-1, quadrant[2]);
    assertEquals(-1, quadrant[3]);
    src.getQuadrantIdx(0, 6, quadrant);
    assertEquals(8, quadrant[0]);
    assertEquals(12, quadrant[1]);
    assertEquals(-1, quadrant[2]);
    assertEquals(-1, quadrant[3]);

  }

  void printTempsMis(final long _deb, final long _fin, final String _cas) {
    System.out.println("temps mis pour " + _cas + ": " + ((_fin - _deb) / 1000D));

  }

  /**
   * Test le decalage des indices dans les elements.
   */
  public void testShiftElement() {
    final EfElement el = new EfElement(new int[] { 1, 2, 3, 4, 5 });
    final EfElement n = EfElement.shiftLeftElementIndex(el);
    assertEquals(n.getPtNb(), 5);
    assertEquals(n.getPtIndex(0), 2);
    assertEquals(n.getPtIndex(1), 3);
    assertEquals(n.getPtIndex(2), 4);
    assertEquals(n.getPtIndex(3), 5);
    assertEquals(n.getPtIndex(4), 1);
  }

  /**
   * fonctions statiques � enlever.
   * 
   * @param _fichierNom
   */
  public static double[][] litPoints(final String _fichierNom) {
    final File fichier = new File(_fichierNom);
    double minX = Double.POSITIVE_INFINITY;
    double minY = Double.POSITIVE_INFINITY;
    List semis;
    LineNumberReader in = null;
    try {
      semis = new ArrayList();
      in = new LineNumberReader(new FileReader(fichier));
      String line = in.readLine();
      StringTokenizer tok = null;
      while (line != null) {
        if (line.trim().startsWith("#")) {
          line = in.readLine();
          continue;
        }
        tok = new StringTokenizer(line);
        final double x = Double.parseDouble(tok.nextToken());
        final double y = Double.parseDouble(tok.nextToken());
        final double z = Double.parseDouble(tok.nextToken());
        if (x < minX) {
          minX = x;
        }
        if (y < minY) {
          minY = y;
        }
        semis.add(new double[] { x, y, z });
        line = in.readLine();
      }
      in.close();
    } catch (final IOException e) {
      semis = null;
      return null;
    } finally {
      if (in != null) {
        try {
          in.close();
        } catch (final IOException e1) {
          e1.printStackTrace();
        }
      }
    }
    FuLog.all("lecture des points sur le client");
    final double[][] pointsTemp = new double[semis.size()][3];
    // recadrage des points par rapport � minX et minY
    for (int i = 0; i < semis.size(); i++) {
      final double[] po = (double[]) semis.get(i);
      po[0] -= minX;
      po[1] -= minY;
      pointsTemp[i] = new double[] { po[0], po[1], po[2] };
    }
    return pointsTemp;
  }
}
