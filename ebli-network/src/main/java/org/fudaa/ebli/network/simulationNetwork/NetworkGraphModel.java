package org.fudaa.ebli.network.simulationNetwork;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource.mxIEventListener;
import com.mxgraph.view.mxGraph;

/**
 * Listener for the graph.
 * Create appropriate operations when adding new object or deleting one.
 * @author Adrien
 *
 */
public class NetworkGraphModel {
	
	private HashMap<mxCell,NetworkUserObject> mappingUserObject = new HashMap<mxCell, NetworkUserObject>();
	
	/**the key is the edge object cell and the value is the surrounding node (gare) values.  */
	private HashMap<mxCell, mxCell[]> mappingNetworkConnection = new HashMap<mxCell, mxCell[]>();
	
	public NetworkDaoFactory dao;
	
	
	public NetworkGraphModel(NetworkDaoFactory daoF) {
		dao = daoF;
	}
	
	
	mxIEventListener listenerRemoveElement = new mxIEventListener() {

		@Override
    public void invoke(Object arg0, mxEventObject arg1) {
			
			removeElement(arg1);
		}
		
	};	
	
	
	
	
	
	mxIEventListener listenerConnectElement = new mxIEventListener() {

		@Override
    public void invoke(Object arg0, mxEventObject arg1) {
			
			connectElement(arg1);
		}
		
	};
	

	private boolean nutralizeDao= false;
	
	
	public void addNewElement(mxCell element) {
		addNewElement(element, null);
	}
	
	
	public void addNewElement(mxCell element, NetworkUserObject userObject) {
		System.out.println("add element " + element);
		
		if(userObject == null && !nutralizeDao)
			userObject = dao.createElement(element);
		
		//-- specify the userobject into network --//
		element.setValue(userObject);
		
		mappingUserObject.put(element, userObject);
		
		
	}

	public void removeElement(mxEventObject event) {
		System.out.println("remove element " + event);
		
		//-- could be multiples elements --//
		if(event.getProperties().containsKey("cells")) {
			Object[] datasToDelete = (Object[]) event.getProperties().get("cells");
			
			removeElement(datasToDelete, true);
			
		}
		
	}
	
	public void removeElement(Object[] datasToDelete, boolean removeDao) {
		for(int i=0 ; i< datasToDelete.length; i++) {
			
			if(datasToDelete[i] instanceof mxCell) {
				mxCell elementToRemove = (mxCell) datasToDelete[i];
				System.out.println("remove element " + elementToRemove);
				
				if(removeDao && !nutralizeDao)
					dao.deleteElement(elementToRemove);
				
				mappingUserObject.remove(elementToRemove);
				mappingNetworkConnection.remove(elementToRemove);
				removeNodeFromConnections(elementToRemove);
				
			}
			
		}
	}
	
	
	private void removeNodeFromConnections(mxCell elementToRemove) {
		for(mxCell cell:mappingNetworkConnection.keySet()) {
			mxCell[] res = mappingNetworkConnection.get(cell);
			if(res !=null && res.length == 2) {
				if(elementToRemove == res[0] )
					res[0] = null;
				if(elementToRemove == res[1] )
					res[1] = null;
				
			}
		}
		
	}

	public void connectElement(mxEventObject event) {
		System.out.println("connect element " + event);
		mxCell terminal = null;
		mxCell edge = null;
		mxCell previous = null;
		
		//-- if there is terminal and edge this is a connection --//
		if(event.getProperties().containsKey("terminal") && event.getProperties().containsKey("edge")) {
			terminal = (mxCell) event.getProperties().get("terminal");
			edge = (mxCell) event.getProperties().get("edge");
			
			if( ! mappingNetworkConnection.containsKey(edge)) {
				mxCell[] connections = new mxCell[2];
				mappingNetworkConnection.put(edge, connections);
			}
			mxCell[] connections = mappingNetworkConnection.get(edge);
			//TODO gestion amont=0 et aval=1 � am�liorer
			if(connections[0] == null){
				connections[0] = terminal;
				if(!nutralizeDao)
					dao.connectElement(edge, terminal, false, true);
			}
			else {
				connections[1] = terminal;
				if(!nutralizeDao)
					dao.connectElement(edge, terminal, false, false);
			}
			
		} else if (event.getProperties().containsKey("previous") && event.getProperties().containsKey("edge")) {
			previous = (mxCell) event.getProperties().get("previous");
			edge = (mxCell) event.getProperties().get("edge");
			
			//-- on retire la connection --//
			if( mappingNetworkConnection.containsKey(edge)) {
				mxCell[] connections = mappingNetworkConnection.get(edge);
				if(connections[0] == previous) {
					connections[0] = null;
					if(!nutralizeDao)
						dao.connectElement(edge, previous, true, true);
				}
				else {
					connections[1] = null;
					if(!nutralizeDao)
						dao.connectElement(edge, previous, true, false);
				}
			}
			
			
		}
		
		
	}

	/**
	 * Validate topology of the network
	 * @return a list of errors if no valid...
	 */
	public List<String> validateNetwork() {
		List<String>  errors = new ArrayList<String>();
		
		//-- check at least 2 gare --//
		if(mappingUserObject.keySet().size()<=1) {
			errors.add("pas assez d'�l�ments. Veuillez en cr�er davantage pour composer votre mod�le");
		}
		
		int countGare= 0;
		for(mxCell cell: mappingUserObject.keySet()){
			if(!(cell.getValue() instanceof NetworkUserObject)) {
				errors.add("Un objet intrus a �t� cr�e, il n'appartient � aucune entit� du mod�le... veuillez recommencer depuis le d�but.");
			}else {
				//-- sanity checks --//
				NetworkUserObject data =   (NetworkUserObject) cell.getValue();
				boolean isNode = data instanceof DefaultNetworkUserObject && ((DefaultNetworkUserObject)data).isNode();
				if(isNode) {
					countGare ++;
					//-- check if the node object is attached to something --//
					boolean isAttached = false;
					for(mxCell edge: mappingNetworkConnection.keySet()) {
						mxCell[] connections = mappingNetworkConnection.get(edge);
						if(connections != null && (connections[0] == cell || connections[1] == cell)) {
							isAttached = true;
						}
					}
					if(!isAttached) {
						errors.add("la gare " + data.getName() + " n'est rattach�e � aucun �l�ment."); 
					}
				} else {
					//-- check non node 2: check if the object is not attached to a node (gare) --//
					if(! mappingNetworkConnection.containsKey(cell))  {
						errors.add("L'�l�ment "+ data.getName() + " n'est rattach� � aucun �l�ments");
					} else {
						mxCell[] connections = mappingNetworkConnection.get(cell);
						if(connections == null || connections.length != 2 || (connections[0] == null && connections[1] == null) ) {
							errors.add("L'�l�ment "+ data.getName() + " n'est rattach� � aucun �l�ments");
						}
						//-- check non node 1: check if  a non node object shouldn't be connected to another non node object --//
						if(connections !=null &&  connections.length == 2 ) {
							NetworkUserObject childAmont =   (NetworkUserObject) connections[0];
							boolean isNodeAmont = childAmont instanceof DefaultNetworkUserObject && ((DefaultNetworkUserObject)childAmont).isNode();
							if(!isNodeAmont) {
								errors.add("L'�l�ment "+ data.getName() + " doit �tre rattach� � une gare et non directement � l'objet " + childAmont.getName());
							}
							NetworkUserObject childAval =   (NetworkUserObject) connections[1];
							boolean isNodeAval = childAval instanceof DefaultNetworkUserObject && ((DefaultNetworkUserObject)childAval).isNode();
							if(!isNodeAval) {
								errors.add("L'�l�ment "+ data.getName() + " doit �tre rattach� � une gare et non directement � l'objet " + childAval.getName());
							}
						}
					}
				}
			}
		}
		
		if( countGare <2) {
			errors.add("il faut au moins 2 gares.");
		}
		return errors;
	}
	
	
	public mxCell findCellByUserObject(NetworkUserObject userObject) {
		
		for(mxCell candidate: mappingUserObject.keySet()) {
			NetworkUserObject result = mappingUserObject.get(candidate);
			if(result == userObject)
				return candidate;
			else if(result instanceof DefaultNetworkUserObject) {
				DefaultNetworkUserObject resDefault = (DefaultNetworkUserObject) result;
				if(resDefault.getName().equals(userObject.getName())){
					return candidate;
				}
			}
		}
		return null;
	}
	
	
	public mxIEventListener getListenerRemoveElement() {
		return listenerRemoveElement;
	}

	public void setListenerRemoveElement(mxIEventListener listenerRemoveElement) {
		this.listenerRemoveElement = listenerRemoveElement;
	}

	public mxIEventListener getListenerConnectElement() {
		return listenerConnectElement;
	}

	public void setListenerConnectElement(mxIEventListener listenerConnectElement) {
		this.listenerConnectElement = listenerConnectElement;
	}

	public HashMap<mxCell, NetworkUserObject> getMappingUserObject() {
		return mappingUserObject;
	}

	public void setMappingUserObject(
			HashMap<mxCell, NetworkUserObject> mappingUserObject) {
		this.mappingUserObject = mappingUserObject;
	}

	public HashMap<mxCell, mxCell[]> getMappingNetworkConnection() {
		return mappingNetworkConnection;
	}

	public void setMappingNetworkConnection(
			HashMap<mxCell, mxCell[]> mappingNetworkConnection) {
		this.mappingNetworkConnection = mappingNetworkConnection;
	}

	public boolean hasChanged() {
		// TODO Auto-generated method stub
		return dao.hasChanged();
	}


	public void clean(mxGraph graph) {
		
		this.nutralizeDao = true;
		Object[] cellsToRemove = mappingUserObject.keySet().toArray();
		graph.removeCells(cellsToRemove);
		((mxGraphModel)graph.getModel()).clear();
		mappingUserObject.clear();
		mappingNetworkConnection.clear();
		this.nutralizeDao = false;
	}


	

	
	
}
