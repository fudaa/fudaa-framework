package org.fudaa.ebli.network.simulationNetwork.shape;



import java.awt.Shape;
import java.awt.geom.GeneralPath;

import com.mxgraph.canvas.mxGraphics2DCanvas;
import com.mxgraph.model.mxCell;
import com.mxgraph.shape.mxBasicShape;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxPoint;
import com.mxgraph.view.mxCellState;
/**
 * Shape specifics for ecluse.
 * @author Adrien
 *
 */
public class ShapeEcluse extends mxBasicShape {
	
	@Override
  public Shape createShape(mxGraphics2DCanvas canvas, mxCellState state){
	    mxCell cell = (mxCell)state.getCell();
	    final GeneralPath p0 = new GeneralPath();
	        
	    if(cell != null && cell.getGeometry() != null) {
	    	
	    	mxPoint A , B;
	    	A =state.getAbsolutePoints().get(0);
	    	B =state.getAbsolutePoints().get(state.getAbsolutePoints().size()-1);
	    	 
	    	System.out.println("The point A:" + A);
	    	System.out.println("The point B:" + B);
	    	System.out.println("scale = " +canvas.getScale());
	    	
	    	
	    	p0.moveTo(A.getX(),A.getY());
	    	p0.lineTo(B.getX(),B.getY());
	    	p0.closePath();
	    	
	    	//-- create losange--//
	    	double coeffDroite = 0;
	    	
	    	if(B.getX() - A.getX() !=0) 
	    		coeffDroite = (B.getY() - A.getY()) / (B.getX() - A.getX()); 
	    	double b = A.getY() - coeffDroite* A.getX(); 
	    	
	    	
	    	
	    	
	    	//mxPoint center =new mxPoint( state.getCenterX(), state.getCenterY());
	    	
	    	//double lineSize = Math.sqrt((A.getX()-B.getX())*(A.getX()-B.getX()) + (A.getY()-B.getY())*(A.getY()-B.getY()));
	    	
	    //	double tailleLosange = lineSize/4;///0.6*canvas.getScale(); 
	    	
	    	
	    	
	    	
	    	//-- draw losange --// 
	    	//TODO not all case are just...
	    /*	mxPoint startLosangeOnLine = new mxPoint();
	    	startLosangeOnLine.setX(center.getX()-tailleLosange);
	    	startLosangeOnLine.setY(coeffDroite * (center.getX()-tailleLosange) +b );
	    	
	    	mxPoint endLosangeOnLine = new mxPoint();
	    	endLosangeOnLine.setX(center.getX()+tailleLosange);
	    	endLosangeOnLine.setY(coeffDroite * (center.getX()+tailleLosange) +b );
	    	
	    	
	    	p0.moveTo(startLosangeOnLine.getX(),startLosangeOnLine.getY());
	    	p0.lineTo(center.getX(),center.getY()-tailleLosange);
	    	p0.lineTo(endLosangeOnLine.getX(),endLosangeOnLine.getY());
	    	p0.closePath();
	    	
	    	p0.moveTo(startLosangeOnLine.getX(),startLosangeOnLine.getY());
	    	p0.lineTo(center.getX(),center.getY()+tailleLosange);
	    	p0.lineTo(endLosangeOnLine.getX(),endLosangeOnLine.getY());
	    	p0.closePath();
	    	*/
	    	
	    	
	    	
	    	
	    }
	   
	    return p0;

	}
	
	


	@Override
  public void paintShape(mxGraphics2DCanvas canvas, mxCellState state)
	{
		

						canvas.getGraphics().draw(createShape(canvas, state));
						
						//-- draw circle --//
					
						mxPoint A , B ;
				    	A = state.getAbsolutePoints().get(0);
				    	B = state.getAbsolutePoints().get(state.getAbsolutePoints().size()-1);
						double lineSize = Math.sqrt((A.getX()-B.getX())*(A.getX()-B.getX()) + (A.getY()-B.getY())*(A.getY()-B.getY()));
				    	int diametre = (int)lineSize/5;
						
						canvas.getGraphics().drawOval((int)state.getCenterX() -diametre/2, (int)state.getCenterY()-diametre/2,diametre, diametre);
					
	}


	/*
	public mxPoint[] createPoints(mxGraphics2DCanvas canvas, mxCellState state)
	{
		
		System.out.println("createPoints()");
		System.out.println("");
		String direction = mxUtils.getString(state.getStyle(),
				mxConstants.STYLE_DIRECTION, mxConstants.DIRECTION_EAST);

		mxPoint p0, pe;

		if (direction.equals(mxConstants.DIRECTION_EAST)
				|| direction.equals(mxConstants.DIRECTION_WEST))
		{
			double mid = state.getCenterY();
			p0 = new mxPoint(state.getX(), mid);
			pe = new mxPoint(state.getX() + state.getWidth(), mid);
		}
		else
		{
			double mid = state.getCenterX();
			p0 = new mxPoint(mid, state.getY());
			pe = new mxPoint(mid, state.getY() + state.getHeight());
		}

		mxPoint[] points = new mxPoint[2];
		points[0] = p0;
		points[1] = pe;

		return points;
	}
*/
	
	
	public int getArcSize(int w, int h)
	{
		int arcSize;

		if (w <= h)
		{
			arcSize = (int) Math.round(h
					* mxConstants.RECTANGLE_ROUNDING_FACTOR);

			if (arcSize > (w / 2))
			{
				arcSize = w / 2;
			}
		}
		else
		{
			arcSize = (int) Math.round(w
					* mxConstants.RECTANGLE_ROUNDING_FACTOR);

			if (arcSize > (h / 2))
			{
				arcSize = h / 2;
			}
		}
		return arcSize;
	}

	
}
