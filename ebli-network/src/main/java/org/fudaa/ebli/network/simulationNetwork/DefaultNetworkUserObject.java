package org.fudaa.ebli.network.simulationNetwork;

import java.io.Serializable;






public class DefaultNetworkUserObject   implements NetworkUserObject, Serializable{

	
	private static final long serialVersionUID = 1L;
	private  String value;	
	private  boolean isNode;	

	public DefaultNetworkUserObject(){
		
	}
	
 public DefaultNetworkUserObject(String value, boolean isNode) {
	 
		this.value = value;
		this.isNode = isNode;
	}
	
	@Override
  public String getName() {
		// TODO Auto-generated method stub
		return value;
	}



	public boolean isNode() {
		return isNode;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setNode(boolean isNode) {
		this.isNode = isNode;
	}

	@Override
	public void setName(String value) {
		// TODO Auto-generated method stub
		this.value = value;
	}
	
}

