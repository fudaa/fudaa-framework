package org.fudaa.ebli.network.simulationNetwork;


import java.util.HashMap;

import com.mxgraph.model.mxCell;
import com.mxgraph.view.mxGraph;

/**
 * Interface that offers methods of synchronization between the network and the user obects CRUD + validation.
 * @author Adrien Hadoux
 *
 */
public interface NetworkDaoFactory {

	
	
	public NetworkUserObject createElement(mxCell element) ;
	
	/**
	 * delete a busines element according to user's manipulation. 
	 * @param element
	 * @return
	 */
	public NetworkUserObject deleteElement(mxCell element) ;
	
	/**
	 * update topology of the network according to user draw's manipulations.
	 * @param element
	 * @param conector
	 * @return
	 */
	public Object connectElement(mxCell edge, mxCell terminal, boolean unconnect, boolean amont) ;
	
	/**
	 * Validate topology of the network
	 * @return
	 */
	//public boolean validateNetwork(final HashMap<mxCell,NetworkUserObject> mappingUserObject, final HashMap<mxCell, mxCell[]> mappingNetworkConnection);
	
	/**
	 * Map the network from jgraph graph load with the  business object + topology.
	 * @return
	 */
	public Object loadNetworkMapping(mxGraph graph, HashMap<mxCell,NetworkUserObject> mappingUserObject,  HashMap<mxCell, mxCell[]> mappingNetworkConnection );

	public boolean hasChanged();
	
	
	
}
