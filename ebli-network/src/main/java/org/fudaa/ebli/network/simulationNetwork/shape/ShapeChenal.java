package org.fudaa.ebli.network.simulationNetwork.shape;


import java.awt.BasicStroke;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.GeneralPath;
import com.mxgraph.canvas.mxGraphics2DCanvas;
import com.mxgraph.model.mxCell;
import com.mxgraph.shape.mxBasicShape;
import com.mxgraph.util.mxPoint;
import com.mxgraph.view.mxCellState;
/**
 * Shape specifics for chenal.
 * @author Adrien
 *
 */
public class ShapeChenal extends mxBasicShape {

	@Override
  public Shape createShape(mxGraphics2DCanvas canvas, mxCellState state){
		mxCell cell = (mxCell)state.getCell();
		final GeneralPath p0 = new GeneralPath();

		if(cell != null && cell.getGeometry() != null) {

			mxPoint A , B ;
			A = state.getAbsolutePoints().get(0);
			B = state.getAbsolutePoints().get(state.getAbsolutePoints().size()-1);

			System.out.println("The point A:" + A);
			System.out.println("The point B:" + B);
			System.out.println("scale = " +canvas.getScale());


			p0.moveTo(A.getX(),A.getY());
			p0.lineTo(B.getX(),B.getY());
			p0.closePath();



		}
		//-- cr�er un double stroke bas� sur la ligne directrice --//
		Stroke stroke = new BasicStroke(10f, BasicStroke.CAP_SQUARE,
				BasicStroke.JOIN_MITER, 10.0f); 
		stroke.createStrokedShape(p0);
		return stroke.createStrokedShape(p0);

	}




	@Override
  public void paintShape(mxGraphics2DCanvas canvas, mxCellState state) {
		canvas.getGraphics().draw(createShape(canvas, state));

	}



}
