package org.fudaa.ebli.network.simulationNetwork;


/**
 *  specify the user object and all method needed by the graph to display properly the object.
 * @author Adrien
 *
 */
public interface NetworkUserObject {

	
	
	public String getName();
	
	public void setName(String value);
}
