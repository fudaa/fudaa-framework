/**
 * $Id: GraphEditor.java,v 1.3 2014/02/08 14:05:58 gaudenz Exp $
 * Copyright (c) 2006-2012, JGraph Ltd */
package org.fudaa.ebli.network.simulationNetwork;

import java.awt.Color;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import javax.swing.ImageIcon;

import org.fudaa.ebli.network.simulationNetwork.shape.ShapeCercleEvitage;
import org.fudaa.ebli.network.simulationNetwork.shape.ShapeChenal;
import org.fudaa.ebli.network.simulationNetwork.shape.ShapeEcluse;
import org.fudaa.ebli.network.simulationNetwork.ui.BasicGraphEditor;
import org.fudaa.ebli.network.simulationNetwork.ui.EditorPalette;
import org.fudaa.ebli.network.simulationNetwork.ui.GraphEditor;
import org.w3c.dom.Document;

import com.mxgraph.canvas.mxGraphics2DCanvas;
import com.mxgraph.io.mxCodec;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.model.mxICell;
import com.mxgraph.model.mxIGraphModel;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.util.mxGraphTransferable;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxXmlUtils;
import com.mxgraph.util.mxEventSource.mxIEventListener;
import com.mxgraph.util.mxPoint;
import com.mxgraph.util.mxResources;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxEdgeStyle;
import com.mxgraph.view.mxGraph;

public class SimulationNetworkEditor extends BasicGraphEditor
{
	
	
	// Adds  template cells for dropping into the graph
			public final static String DEFAULT_VALUE_BASSIN = "Bassin";
			public final static String DEFAULT_VALUE_GARE = "Gare";
			public final static String DEFAULT_VALUE_CHENAL = "Chenal";
			public final static String DEFAULT_VALUE_BIEF = "Tron�on";
			public final static String DEFAULT_VALUE_ECLUSE = "Ecluse";
			public final static String DEFAULT_VALUE_CERCLE = "Cercle";
			public final static String DEFAULT_VALUE_BASSIN_Connecteur = "Connecteur Bassin";
			
				
	
	private final NetworkGraphModel networkModel;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4601740824088314699L;

	/**
	 * Holds the shared number formatter.
	 * 
	 * @see NumberFormat#getInstance()
	 */
	public static final NumberFormat numberFormat = NumberFormat.getInstance();

	/**
	 * Holds the URL for the icon to be used as a handle for creating new
	 * connections. This is currently unused.
	 */
	public static URL url = null;

	//GraphEditor.class.getResource("/org/fudaa/ebli/network/simulationNetwork/images/connector.gif");

	public SimulationNetworkEditor(NetworkGraphModel networkModel)
	{
		this(networkModel, false);
		
	}
	
	public SimulationNetworkEditor(NetworkGraphModel networkModel, boolean isVoixNavigables)
	{
		this("Mod�lisation", new CustomGraphComponent(new CustomGraph(),networkModel),networkModel,isVoixNavigables);
		
		
		
	}
	
	public boolean hasChanged() {
		return getNetworkModel().hasChanged();
	}

	/**
	 * 
	 */
	public SimulationNetworkEditor(String appTitle, mxGraphComponent component,NetworkGraphModel mNetwork, boolean isVoixNavigables)
	{
		super(appTitle, component);
		this.networkModel = mNetwork;
		
		component.setConnectable(false);
		
		//-- add the specificcs shapes --//
		mxGraphics2DCanvas.putShape("shapeChenal", new ShapeChenal());
		mxGraphics2DCanvas.putShape("shapeEcluse", new ShapeEcluse());
		mxGraphics2DCanvas.putShape("shapeCercleEvitage", new ShapeCercleEvitage());
		
		
		final mxGraph graph = graphComponent.getGraph();
		
		Hashtable<String, Object> edgeDirMap = new Hashtable<String, Object>();
		edgeDirMap.put( mxConstants.STYLE_ROUNDED, false );
		edgeDirMap.put( mxConstants.STYLE_ENDARROW, mxConstants.ARROW_BLOCK );
		edgeDirMap.put( mxConstants.STYLE_STARTARROW, mxConstants.ARROW_BLOCK );
		edgeDirMap.put( mxConstants.STYLE_SHAPE, mxConstants.SHAPE_CURVE );
		edgeDirMap.put( mxConstants.STYLE_EDGE, mxEdgeStyle.ElbowConnector );
		edgeDirMap.put( mxConstants.STYLE_LABEL_POSITION, "left" );
		edgeDirMap.put( mxConstants.STYLE_VERTICAL_LABEL_POSITION, "top"  );
		
		graph.getStylesheet().putCellStyle( "ecluseEdge", edgeDirMap );
		
		edgeDirMap = new Hashtable<String, Object>();
		edgeDirMap.put( mxConstants.STYLE_ROUNDED, false );
		edgeDirMap.put( mxConstants.STYLE_ENDARROW, mxConstants.NONE );
		edgeDirMap.put( mxConstants.STYLE_STARTARROW, mxConstants.NONE );
		//edgeDirMap.put( mxConstants.STYLE_SHAPE, mxConstants.shape );
		graph.getStylesheet().putCellStyle( "chenalEdge", edgeDirMap );
		
		
		component.getGraph().addListener(mxEvent.CELLS_REMOVED, networkModel.getListenerRemoveElement());
		component.getGraph().addListener(mxEvent.CELL_CONNECTED, networkModel.getListenerConnectElement());
		//component.getGraph().addListener(mxEvent.CELLS_ADDED, networkModel.getListenerAddElement());

		
		
		
		// Creates the shapes palette
		EditorPalette shapesPalette = insertPalette(mxResources.get("shapes"));
		
		shapesPalette.addListener(mxEvent.SELECT, new mxIEventListener()
		{
			@Override
      public void invoke(Object sender, mxEventObject evt)
			{
				Object tmp = evt.getProperty("transferable");

				if (tmp instanceof mxGraphTransferable)
				{
					mxGraphTransferable t = (mxGraphTransferable) tmp;
					Object cell = t.getCells()[0];

					if (graph.getModel().isEdge(cell))
					{
						((CustomGraph) graph).setEdgeTemplate(cell);
					}
				}
			}

		});

		
		if(!isVoixNavigables) {
		shapesPalette
				.addEdgeTemplate(
						DEFAULT_VALUE_BASSIN,
						new ImageIcon(
								SimulationNetworkEditor.class
										.getResource("/org/fudaa/ebli/network/simulationNetwork/images/doublerectangle.png")),
						getBassinStyle(), 160, 120, DEFAULT_VALUE_BASSIN);
		}
		shapesPalette
				.addTemplate(
						DEFAULT_VALUE_GARE,
						new ImageIcon(
								SimulationNetworkEditor.class
										.getResource("/org/fudaa/ebli/network/simulationNetwork/images/doubleellipse.png")),
						getGareStyle(), 40, 40, DEFAULT_VALUE_GARE);
		
				

		shapesPalette
				.addEdgeTemplate(
						isVoixNavigables?DEFAULT_VALUE_BIEF:DEFAULT_VALUE_CHENAL,
						new ImageIcon(
								SimulationNetworkEditor.class
										.getResource("/org/fudaa/ebli/network/simulationNetwork/images/straight.png")),
						getChenalStyle(), 120, 120, isVoixNavigables?DEFAULT_VALUE_BIEF:DEFAULT_VALUE_CHENAL);
						//"shape=shapeChenal", 120, 120, DEFAULT_VALUE_CHENAL);
	//shape=shapeChenal
		//straight
		shapesPalette
				.addEdgeTemplate(
						DEFAULT_VALUE_ECLUSE,
						new ImageIcon(
								SimulationNetworkEditor.class
										.getResource("/org/fudaa/ebli/network/simulationNetwork/images/arrow.png")),
						getEcluseStyle(), 120, 120, DEFAULT_VALUE_ECLUSE);

		if(!isVoixNavigables) {
		 shapesPalette
		.addEdgeTemplate(
				DEFAULT_VALUE_CERCLE,
				new ImageIcon(
						GraphEditor.class
								.getResource("/org/fudaa/ebli/network/simulationNetwork/images/ellipse.png")),
				getCercleStyle(), 160, 30, DEFAULT_VALUE_CERCLE);
		}
		
		//-- for test only --//
		/*
		shapesPalette
		.addEdgeTemplate(
				"customShape",
				new ImageIcon(
						SimulationNetworkEditor.class
								.getResource("/org/fudaa/ebli/network/simulationNetwork/images/straight.png")),
								"shape=shapeEcluse", 120, 120, "customShape");
		*/
		
		
	}

	/**
	 * 
	 */
	public static class CustomGraphComponent extends mxGraphComponent
	{
		NetworkGraphModel modelSipor;
		/**
		 * 
		 */
		private static final long serialVersionUID = -6833603133512882012L;

		/**
		 * 
		 * @param graph
		 */
		public CustomGraphComponent(mxGraph graph,NetworkGraphModel modelSipor )
		{
			super(graph);
			this.modelSipor = modelSipor;

			// Sets switches typically used in an editor
			setPageVisible(true);
			setGridVisible(true);
			setToolTips(true);
			getConnectionHandler().setCreateTarget(true);

			// Loads the defalt stylesheet from an external file
			mxCodec codec = new mxCodec();
			Document doc = mxUtils.loadDocument(SimulationNetworkEditor.class.getResource(
					"/org/fudaa/ebli/network/simulationNetwork/resources/default-style.xml")
					.toString());
			codec.decode(doc.getDocumentElement(), graph.getStylesheet());

			// Sets the background to white
			getViewport().setOpaque(true);
			getViewport().setBackground(Color.WHITE);
		}

		/**
		 * Overrides drop behaviour to set the cell style if the target
		 * is not a valid drop target and the cells are of the same
		 * type (eg. both vertices or both edges). 
		 */
		@Override
    public Object[] importCells(Object[] cells, double dx, double dy,
                                Object target, Point location)
		{
			if (target == null && cells.length == 1 && location != null)
			{
				target = getCellAt(location.x, location.y);

				if (target instanceof mxICell && cells[0] instanceof mxICell)
				{
					mxICell targetCell = (mxICell) target;
					mxICell dropCell = (mxICell) cells[0];

					if (targetCell.isVertex() == dropCell.isVertex()
							|| targetCell.isEdge() == dropCell.isEdge())
					{
						mxIGraphModel model = graph.getModel();
						model.setStyle(target, model.getStyle(cells[0]));
						graph.setSelectionCell(target);

						return null;
					}
				}
			}

			Object[] results =  super.importCells(cells, dx, dy, target, location);
			
			//-- aha - fire event to notify a new element has been dragged dropped --//
			modelSipor.addNewElement((mxCell) results[0]);
			
			return results;
		}

	}

	/**
	 * A graph that creates new edges from a given template edge.
	 */
	public static class CustomGraph extends mxGraph
	{
		/**
		 * Holds the edge to be used as a template for inserting new edges.
		 */
		protected Object edgeTemplate;

		/**
		 * Custom graph that defines the alternate edge style to be used when
		 * the middle control point of edges is double clicked (flipped).
		 */
		public CustomGraph()
		{
			setAlternateEdgeStyle("edgeStyle=mxEdgeStyle.ElbowConnector;elbow=vertical");
		}

		 
		
		/**
		 * Sets the edge template to be used to inserting edges.
		 */
		public void setEdgeTemplate(Object template)
		{
			edgeTemplate = template;
		}

		/**
		 * Prints out some useful information about the cell in the tooltip.
		 */
		@Override
    public String getToolTipForCell(Object cell)
		{
			String tip = "<html>";
			mxGeometry geo = getModel().getGeometry(cell);
			mxCellState state = getView().getState(cell);

			if (getModel().isEdge(cell))
			{
				tip += "points={";

				if (geo != null)
				{
					List<mxPoint> points = geo.getPoints();

					if (points != null)
					{
						Iterator<mxPoint> it = points.iterator();

						while (it.hasNext())
						{
							mxPoint point = it.next();
							tip += "[x=" + numberFormat.format(point.getX())
									+ ",y=" + numberFormat.format(point.getY())
									+ "],";
						}

						tip = tip.substring(0, tip.length() - 1);
					}
				}

				tip += "}<br>";
				tip += "absPoints={";

				if (state != null)
				{

					for (int i = 0; i < state.getAbsolutePointCount(); i++)
					{
						mxPoint point = state.getAbsolutePoint(i);
						tip += "[x=" + numberFormat.format(point.getX())
								+ ",y=" + numberFormat.format(point.getY())
								+ "],";
					}

					tip = tip.substring(0, tip.length() - 1);
				}

				tip += "}";
			}
			else
			{
				tip += "geo=[";

				if (geo != null)
				{
					tip += "x=" + numberFormat.format(geo.getX()) + ",y="
							+ numberFormat.format(geo.getY()) + ",width="
							+ numberFormat.format(geo.getWidth()) + ",height="
							+ numberFormat.format(geo.getHeight());
				}

				tip += "]<br>";
				tip += "state=[";

				if (state != null)
				{
					tip += "x=" + numberFormat.format(state.getX()) + ",y="
							+ numberFormat.format(state.getY()) + ",width="
							+ numberFormat.format(state.getWidth())
							+ ",height="
							+ numberFormat.format(state.getHeight());
				}

				tip += "]";
			}

			mxPoint trans = getView().getTranslate();

			tip += "<br>scale=" + numberFormat.format(getView().getScale())
					+ ", translate=[x=" + numberFormat.format(trans.getX())
					+ ",y=" + numberFormat.format(trans.getY()) + "]";
			tip += "</html>";

			return tip;
		}

		/**
		 * Overrides the method to use the currently selected edge template for
		 * new edges.
		 * 
		 * @param graph
		 * @param parent
		 * @param id
		 * @param value
		 * @param source
		 * @param target
		 * @param style
		 * @return
		 */
		@Override
    public Object createEdge(Object parent, String id, Object value,
                             Object source, Object target, String style)
		{
			if (edgeTemplate != null)
			{
				mxCell edge = (mxCell) cloneCells(new Object[] { edgeTemplate })[0];
				edge.setId(id);

				return edge;
			}

			return super.createEdge(parent, id, value, source, target, style);
		}

		@Override
		public String convertValueToString(Object arg0) {
			
				if (arg0 instanceof mxCell) {
					mxCell cell = (mxCell) arg0;
					if(cell.getValue() != null && cell.getValue() instanceof NetworkUserObject) {
						 return ((NetworkUserObject)cell.getValue()).getName();
					}
					
				}
			return super.convertValueToString(arg0);
		}



		@Override
		public void cellLabelChanged(Object arg0, Object arg1, boolean arg2) {
			
			if(arg0 == null || !(arg0 instanceof mxCell))
				return;
			if(arg1 == null || ("").equals(arg1.toString()))
				 return ;
			
			mxCell cell = (mxCell) arg0;
			if(cell.getValue() == null || !(cell.getValue() instanceof NetworkUserObject))
				return;
			
			model.beginUpdate();
			try {
				String newName = arg1.toString();
				NetworkUserObject userObject = (NetworkUserObject) cell.getValue();
				
				userObject.setName(newName);
				
				System.out.println("Cell label changed!!");
			
			}finally {
				model.endUpdate();
				this.refresh();
				
			}
			
		}

	}
	
	
	/**
	 * Save graph on jgraph way.
	 * @param fileName
	 * @throws IOException
	 */
	public void saveGraph(String fileName) throws IOException {
		mxCodec codec = new mxCodec();
		String xml = mxXmlUtils.getXml(codec.encode(getGraphComponent().getGraph()
				.getModel()));
		mxUtils.writeFile(xml, fileName);
		
	}
	
	/**
	 * load a jgraph on jgraph way.
	 * Then map the graph loaded to the busineess userobjects.
	 * @param f
	 * @throws IOException
	 */
	public void loadGraph(File f) throws IOException {

		Document document = mxXmlUtils
				.parseXml(mxUtils.readFile(f
						.getAbsolutePath()));

		mxCodec codec = new mxCodec(document);
		codec.decode(
				document.getDocumentElement(),
				getGraphComponent().getGraph().getModel());
		

		
		
		//-- map business objects with data from simulation --//
		networkModel.dao.loadNetworkMapping(getGraphComponent().getGraph(),networkModel.getMappingUserObject(), networkModel.getMappingNetworkConnection());
		
	}

	public NetworkGraphModel getNetworkModel() {
		return networkModel;
	}
	
	
	protected double startingX = 600, startingY= 230;
	protected double startingNodeX = 200, startingNodeY= 230;
	
	
	private String getChenalStyle(){
		return "chenalEdge;verticalLabelPosition=top;labelPosition=left";
		//return "noEdgeStyle=0;shape=curve";
		//return "shape=shapeChenal";
	}
	private String getBassinStyle(){
		return "rectangle;shape=doubleRectangle;";
	}
	private String getGareStyle(){
		return "ellipse;shape=doubleEllipse";
	}
	private String getEcluseStyle(){
		//return "arrow";
		//return "DirectedEdge;noEdgeStyle=0";
		//return "DirectedEdge";
		return "ecluseEdge";
	}
	private String getCercleStyle(){
		return "shape=shapeCercleEvitage";
	}
	
	/**
	 * Add nework element to synchronize with data manipulation.
	 * @param typeElement
	 * @param object
	 */
	public mxCell addNewNetworkElement(String typeElement,NetworkUserObject object) {
		mxCell cell = null;
		
		
		if(!this.DEFAULT_VALUE_GARE.equals(typeElement)) {
			mxGeometry geometry = new mxGeometry(0, 0, 160, 30);
			geometry.setTerminalPoint(new mxPoint(startingX, startingY), true);
			geometry.setTerminalPoint(new mxPoint(startingX + 160, startingY - 30), false);
			geometry.setRelative(true);
			
			
			if(DEFAULT_VALUE_CHENAL.equals(typeElement))
				 cell = new mxCell(object, geometry,getChenalStyle() );
			else if(DEFAULT_VALUE_BIEF.equals(typeElement))
				 cell = new mxCell(object, geometry,getChenalStyle() );	
			else  if(DEFAULT_VALUE_BASSIN.equals(typeElement))
				 cell = new mxCell(object, geometry, getBassinStyle());
			else  if(DEFAULT_VALUE_ECLUSE.equals(typeElement))
				 cell = new mxCell(object, geometry, getEcluseStyle());
			else  if(DEFAULT_VALUE_CERCLE.equals(typeElement))
				 cell = new mxCell(object, geometry, getCercleStyle());
			
			cell.setEdge(true);
			startingY+=50;
			if(startingY> 600) {
				startingX +=180;
				startingY = 230;
			}
			
		} else {
			//-- gare --// 
			mxGeometry geometry = new mxGeometry(startingNodeX,startingNodeY, 40, 40);
			 cell = new mxCell(object, geometry, getGareStyle());
			 cell.setVertex(true);
			 startingNodeX +=180;
				if(startingNodeX> 800) {
					startingNodeX =100;
					startingNodeY+= 100;
				}
		}
		if(cell != null ) {
			cell.setValue(object);
			this.getGraphComponent().getGraph().addCell(cell);
			//-- feed also the model --//
			this.getNetworkModel().addNewElement(cell, object);
			
		}
		return cell;
	}

	/**
	 * Delete an element from nework and update model accordingly.
	 * @param typeElement
	 * @param object
	 */
	public void deleteNetworkElement(String typeElement,NetworkUserObject object) {
		mxCell target = getNetworkModel().findCellByUserObject(object);
		if(target != null) {
			Object[] datasToDelete = new Object[1];
			datasToDelete[0] = target;
			//-- remove from graph --//
			this.getGraphComponent().getGraph().removeCells(datasToDelete);
			
			//-- remove only from mapping not dao because already done --//
			getNetworkModel().removeElement(datasToDelete, false);
		
		}
	}
	
	
	
	public void  connectElement(NetworkUserObject gareAmont, NetworkUserObject gareAval, NetworkUserObject edge) {
		mxCell edgeCell = getNetworkModel().findCellByUserObject(edge);
		mxCell cellGareAmont = getNetworkModel().findCellByUserObject(gareAmont);
		mxCell cellGareAval = null;
		
		if(gareAval != null)
			cellGareAval = getNetworkModel().findCellByUserObject(gareAval);
		
		this.getGraphComponent().getGraph().addEdge(edgeCell, null, cellGareAmont,cellGareAval, null);
		
	}
	
	public void cleanAllNetwork( ) {
		
		this.getNetworkModel().clean(this.getGraphComponent().getGraph());
	}
	
}
