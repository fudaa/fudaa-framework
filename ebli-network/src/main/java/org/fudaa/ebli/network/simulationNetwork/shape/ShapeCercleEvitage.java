package org.fudaa.ebli.network.simulationNetwork.shape;



import java.awt.Shape;
import java.awt.geom.GeneralPath;

import com.mxgraph.canvas.mxGraphics2DCanvas;
import com.mxgraph.model.mxCell;
import com.mxgraph.shape.mxBasicShape;
import com.mxgraph.util.mxPoint;
import com.mxgraph.view.mxCellState;
/**
 * Shape specifics for cercle evitage.
 * @author Adrien
 *
 */
public class ShapeCercleEvitage extends mxBasicShape {

	@Override
  public Shape createShape(mxGraphics2DCanvas canvas, mxCellState state){
		mxCell cell = (mxCell)state.getCell();
		final GeneralPath p0 = new GeneralPath();

		if(cell != null && cell.getGeometry() != null) {

			mxPoint A , B ;
			A = state.getAbsolutePoints().get(0);
			B = state.getAbsolutePoints().get(state.getAbsolutePoints().size()-1);
			p0.moveTo(A.getX(),A.getY());
			p0.lineTo(B.getX(),B.getY());
			p0.closePath();

		}

		return p0;

	}




	@Override
  public void paintShape(mxGraphics2DCanvas canvas, mxCellState state) {
		
		canvas.getGraphics().draw(createShape(canvas, state));
		//-- draw circle --//
		mxPoint A , B ;
		A = state.getAbsolutePoints().get(0);
		B = state.getAbsolutePoints().get(state.getAbsolutePoints().size()-1);
		double lineSize = Math.sqrt((A.getX()-B.getX())*(A.getX()-B.getX()) + (A.getY()-B.getY())*(A.getY()-B.getY()));
		int diametre = (int)lineSize/5;
		canvas.getGraphics().drawOval((int)state.getCenterX() -diametre/2, (int)state.getCenterY()-diametre/2,diametre, diametre);

	}



}
