/*
 GPL 2
 */
package org.fudaa.ebli.converter;

import java.awt.Color;
import junit.framework.TestCase;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 *
 * @author Frederic Deniger
 */
public class TraceLigneModelToStringTransformerTest extends TestCase {

  public void testToString() {
    TraceLigneModelToStringTransformer toStringTransformer = new TraceLigneModelToStringTransformer();
    TraceLigneModel model = new TraceLigneModel(2, 4, Color.RED);
    String toString = toStringTransformer.toString(model);
    assertTrue(toStringTransformer.isValid(toString));
    TraceLigneModel read = toStringTransformer.fromString(toString);
    assertTrue(model.equals(read));

  }
}
