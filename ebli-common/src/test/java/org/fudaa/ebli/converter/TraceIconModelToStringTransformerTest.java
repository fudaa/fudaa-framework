/*
 GPL 2
 */
package org.fudaa.ebli.converter;

import java.awt.Color;
import junit.framework.TestCase;
import org.fudaa.ebli.trace.TraceIconModel;

/**
 *
 * @author Frederic Deniger
 */
public class TraceIconModelToStringTransformerTest extends TestCase {
  
  public void testToStringNullBackground() {
    TraceIconModelToStringTransformer toStringTransformer = new TraceIconModelToStringTransformer();
    TraceIconModel model = new TraceIconModel(2, 4, Color.RED);
    String toString = toStringTransformer.toString(model);
    assertTrue(toStringTransformer.isValid(toString));
    TraceIconModel read = toStringTransformer.fromString(toString);
    assertNull(read.getBackgroundColor());
    assertTrue(read.equals(model));
  }
  
  public void testToStringNotNullBackground() {
    TraceIconModelToStringTransformer toStringTransformer = new TraceIconModelToStringTransformer();
    final Color color = Color.RED;
    final Color backgroundColor = Color.GREEN;
    TraceIconModel model = new TraceIconModel(5, 8, color);
    model.setTypeLigne(10);
    model.setEpaisseurLigne(50);
    model.setBackgroundColor(backgroundColor);
    String toString = toStringTransformer.toString(model);
    assertTrue(toStringTransformer.isValid(toString));
    TraceIconModel read = toStringTransformer.fromString(toString);
    assertTrue(read.equals(model));
  }
}
