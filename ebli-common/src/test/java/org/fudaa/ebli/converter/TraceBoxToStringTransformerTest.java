/*
 GPL 2
 */
package org.fudaa.ebli.converter;

import java.awt.Color;
import junit.framework.TestCase;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.ebli.trace.TraceLigne;

/**
 *
 * @author Frederic Deniger
 */
public class TraceBoxToStringTransformerTest extends TestCase {

  public void testToString() {
    TraceBoxToStringTransformer toStringTransformer = new TraceBoxToStringTransformer();
    TraceBox box = new TraceBox();
    box.setVertical(true);
    box.setDrawBox(false);
    box.setDrawFond(true);
    box.setColorBoite(Color.RED);
    box.setColorText(Color.CYAN);
    box.setColorFond(Color.GRAY);
    box.setHMargin(4);
    box.setVMargin(1);
    box.setTraceLigne(new TraceLigne(6, 3, Color.LIGHT_GRAY));
    String toString = toStringTransformer.toString(box);
    assertTrue(toStringTransformer.isValid(toString));
    TraceBox read = toStringTransformer.fromString(toString);
    assertEquals(read, box);
  }
}
