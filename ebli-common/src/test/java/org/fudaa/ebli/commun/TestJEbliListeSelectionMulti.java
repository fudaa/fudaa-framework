/*
 * @creation     24 f�vr. 2009
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.commun;

import junit.framework.TestCase;

/**
 * Pour tester les listes de selection multi.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class TestJEbliListeSelectionMulti extends TestCase {

  /**
   * Teste les listes issues des op�rations.
   */
  public void testOperation() {
    EbliListeSelectionMulti l;
    
    // and
    l=getListe1();
    l.intersection(getListe2());
    assertNull(l.get(0));
    assertNotNull(l.get(1));
    assertNull(l.get(2));
    assertNotNull(l.get(5));
    assertEquals(l.getNbSelectedItem(),2);
    assertEquals(l.get(5).getNbSelectedIndex(),1);
    
    // or
    l=getListe1();
    l.add(getListe2());
    assertNotNull(l.get(0));
    assertNotNull(l.get(1));
    assertNotNull(l.get(2));
    assertNotNull(l.get(5));
    assertEquals(l.getNbSelectedItem(),8);
    assertEquals(l.get(5).getNbSelectedIndex(),3);
    
    // remove
    l=getListe1();
    l.remove(getListe2());
    assertNotNull(l.get(0));
    assertNull(l.get(1));
    assertNull(l.get(2));
    assertNotNull(l.get(5));
    assertEquals(l.getNbSelectedItem(),3);
    assertEquals(l.get(5).getNbSelectedIndex(),1);
    
    // xor
    l=getListe1();
    l.xor(getListe2());
    assertNotNull(l.get(0));
    assertNull(l.get(1));
    assertNotNull(l.get(2));
    assertNotNull(l.get(5));
    assertEquals(l.getNbSelectedItem(),6);
    assertEquals(l.get(5).getNbSelectedIndex(),2);
  }
  
  /**
   * Teste la modification des listes au cours des op�rations.
   */
  public void testModification() {
    EbliListeSelectionMulti l;
   
    l=getListe1();
    assertFalse(l.add(getListe3()));
    assertFalse(l.add(getListe1()));
    assertTrue(l.add(getListe2()));
    
    l=getListe1();
    assertFalse(l.intersection(getListe1()));
    assertTrue(l.intersection(getListe3()));

    l=getListe3();
    assertFalse(l.intersection(getListe1()));
    assertFalse(l.intersection(getListe3()));
    
    l=getListe1();
    assertTrue(l.remove(getListe3()));
    assertFalse(l.remove(getListe3()));
    
    l=getListe3();
    assertFalse(l.xor(null));
    assertTrue(l.xor(getListe2()));
    assertTrue(l.xor(getListe2()));
    assertFalse(l.add(getListe3()));
    assertFalse(l.intersection(getListe3()));
  }
  
  EbliListeSelectionMulti getListe1() {
    EbliListeSelectionMulti l=new EbliListeSelectionMulti(3);
    l.add(0,1);
    l.add(0,5);
    l.add(1,6);
    l.add(5,12);
    l.add(5,1);
    return l;
  }

  EbliListeSelectionMulti getListe2() {
    EbliListeSelectionMulti l=new EbliListeSelectionMulti(3);
    l.add(2,1);
    l.add(2,5);
    l.add(1,6);
    l.add(5,12);
    l.add(5,7);
    return l;
  }

  EbliListeSelectionMulti getListe3() {
    EbliListeSelectionMulti l=new EbliListeSelectionMulti(2);
    l.add(0,1);
    l.add(5,12);
    l.add(5,1);
    return l;
  }
}
