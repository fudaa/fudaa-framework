/*
 * @creation 12 avr. 2006
 * @modification $Date: 2006-10-19 14:13:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.geometrie;

import junit.framework.TestCase;
import org.fudaa.ebli.commun.VecteurGenerique;

/**
 * @author fred deniger
 * @version $Id: TestJVecteurGenerique.java,v 1.3 2006-10-19 14:13:29 deniger Exp $
 */
public class TestJVecteurGenerique extends TestCase {

  public void testAdd() {
    final VecteurGenerique g = new VecteurGenerique();
    final GrPoint p = new GrPoint(1, 2, 3);
    g.ajoute(p);
    GrPoint p2 = (GrPoint) g.renvoieObj(0);
    assertNotNull(p2);
    final double eps = 1E-15;
    assertEquals(p.x_, p2.x_, eps);
    assertEquals(p.y_, p2.y_, eps);
    assertEquals(p.z_, p2.z_, eps);
    g.setSize(2);
    assertEquals(2, g.nombre());
    assertNull(g.renvoieObj(1));
    p2 = (GrPoint) g.renvoieObj(0);
    assertNotNull(p2);
    assertEquals(p.x_, p2.x_, eps);
    assertEquals(p.y_, p2.y_, eps);
    assertEquals(p.z_, p2.z_, eps);
    g.setSize(1);
    assertEquals(1, g.nombre());
    p2 = (GrPoint) g.renvoieObj(0);
    assertNotNull(p2);
    assertEquals(p.x_, p2.x_, eps);
    assertEquals(p.y_, p2.y_, eps);
    assertEquals(p.z_, p2.z_, eps);
    g.setSize(10);
    assertEquals(10, g.nombre());
    g.setSize(9);
    assertEquals(9, g.nombre());
    g.setSize(9);
    assertEquals(9, g.nombre());
    g.setSize(0);
    assertEquals(0, g.nombre());
  }

}
