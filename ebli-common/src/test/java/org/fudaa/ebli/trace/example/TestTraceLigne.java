/*
 * @file         TestTraceLigne.java
 * @creation     1998-10-13
 * @modification $Date: 2006-10-19 14:13:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.trace.example;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import org.fudaa.ebli.example.ExampleFrame;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.trace.TracePoint;
/**
 * Tests de performance des traces.
 *
 * @version      $Revision: 1.4 $ $Date: 2006-10-19 14:13:24 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class TestTraceLigne extends JPanel {
  public static Dimension DIM= new Dimension(400, 400);
  GrPolygone polygone;
  GrPoint p1;
  GrPoint p2;
  GrPoint p3;
  public TestTraceLigne() {
    super();
    polygone= new GrPolygone();
    p1= new GrPoint(2d, 2d, 0d);
    p2= new GrPoint(11d, 2d, 0d);
    p3= new GrPoint(2d, 11d, 0d);
    polygone.sommets_.ajoute(p1);
    polygone.sommets_.ajoute(p2);
    polygone.sommets_.ajoute(p3);
  }
  @Override
  public Dimension getPreferredSize() {
    return DIM;
  }
  @Override
  public void paintComponent(final Graphics _g) {
    super.paintComponents(_g);
    //    TraceGeometrie tg=new TraceGeometrie(_g,GrMorphisme.identite());
    //    tg.setBackground(Color.RED);
    //    tg.setTypeSurface(TraceSurface.HACHURE_VERTICAL);
    //    p2.x=100d;
    //    p3.y=100d;
    //    tg.dessinePolygone2D(polygone,true,false);
    //    GrPolygone polygoneSimple=new GrPolygone();
    //    GrPoint p1S=new GrPoint(400d-p1.x,p1.y,0d);
    //    GrPoint p2S=new GrPoint(400d-p2.x,p2.y,0d);
    //    GrPoint p3S=new GrPoint(400d-p3.x,p3.y,0d);
    //    polygoneSimple.sommets.ajoute(p1S);
    //    polygoneSimple.sommets.ajoute(p2S);
    //    polygoneSimple.sommets.ajoute(p3S);
    //    tg.dessinePolygone(polygoneSimple,true,false);
    final TracePoint tp= new TracePoint();
    long t1;
    t1= System.currentTimeMillis();
    for (int i= 8; i >= 0; i--) {
      tp.setTypePoint(i);
      for (int j= 10; j >= 0; j--) {
        final double x= 400 - 10 - j * 10;
        final double y= 10 + i * 10;
        tp.dessinePoint((Graphics2D )_g,(int)x, (int)y);
      }
    }
    System.out.println("Temps ANCIEN" + (System.currentTimeMillis() - t1));
    t1= System.currentTimeMillis();
    for (int i= 8; i >= 0; i--) {
      tp.setTypePoint(i);
      for (int j= 10; j >= 0; j--) {
        tp.dessinePoint((Graphics2D )_g,(double) (10 + j * 10), (double) (10 + i * 10));
      }
    }
    System.out.println("Temps double" + (System.currentTimeMillis() - t1));
  }
  public static void main(final String[] _args) {
    final TestTraceLigne panel= new TestTraceLigne();
    final ExampleFrame f= new ExampleFrame(panel);
    f.pack();
    f.setVisible(true);
    f.repaint(0L);
  }
}
