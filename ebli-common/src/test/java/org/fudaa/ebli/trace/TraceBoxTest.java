package org.fudaa.ebli.trace;

import junit.framework.TestCase;

public class TraceBoxTest extends TestCase {

  public void testDuplicate() {
    TraceBox init=new TraceBox();
    init.setVertical(true);
    init.setDrawBox(true);
    init.setDrawFond(true);
    init.setVMargin(2);
    init.setHPosition(5);


    assertEquals(init,init.duplicate());
  }
}