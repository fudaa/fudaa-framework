/*
 *  @file         EbliTestInner.java
 *  @creation     23 d�c. 2002
 *  @modification $Date: 2006-10-19 14:13:24 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.example;
import com.memoire.bu.BuButton;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
/**
 *
 * @author deniger
 * @version $Id: TestEbliInner.java,v 1.2 2006-10-19 14:13:24 deniger Exp $
 */
public class ExampleEbliInner
  implements PropertyChangeListener, VetoableChangeListener, ComponentListener {
  public static void main(final String[] args) {
    final BuButton bt= new BuButton();
    final ExampleEbliInner t= new ExampleEbliInner();
    bt.addPropertyChangeListener(t);
    bt.addVetoableChangeListener(t);
    bt.addComponentListener(t);
    bt.setVisible(true);
    bt.setVisible(false);
    System.out.println("fin");
  }
  @Override
  public void propertyChange(final PropertyChangeEvent evt) {
    System.out.println(evt.getPropertyName());
  }
  @Override
  public void vetoableChange(final PropertyChangeEvent evt)
    throws PropertyVetoException {
    System.out.println(evt.getPropertyName());
  }
  @Override
  public void componentHidden(final ComponentEvent e) {
    System.out.println("cache");
  }
  @Override
  public void componentMoved(final ComponentEvent e) {}
  @Override
  public void componentResized(final ComponentEvent e) {}
  @Override
  public void componentShown(final ComponentEvent e) {
    System.out.println("montre");
  }
}
