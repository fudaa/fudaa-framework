/*
 *  @creation     14 sept. 2005
 *  @modification $Date: 2006-10-19 14:13:24 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.example;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLib;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.io.File;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.text.BadLocationException;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;


public final class ExampleLayout {
  private ExampleLayout(){}

  public static void main(final String[] _args){
//    try {
//      UIManager.setLookAndFeel(new Plastic3DLookAndFeel());
//    }
//    catch (final UnsupportedLookAndFeelException e) {
//      e.printStackTrace();
//    }
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuGridLayout(2, 5, 5, true, false, true, true, true));
    pn.add(new BuLabel("txt 1: "));
    CtuluFileChooserPanel pnChild = new CtuluFileChooserPanel();
    final JTextField tf1=pnChild.getTf();
    tf1.setAutoscrolls(true);
    pnChild.setFile(new File(ExampleLayout.class.getResource("TestLayout.class").getFile()));
    pn.add(pnChild);
    pn.add(new BuLabel("txt 2 le retour: "));
    pnChild = new CtuluFileChooserPanel();
    final JTextField tf2=pnChild.getTf();
    tf2.setAutoscrolls(true);
    pnChild.setFile(new File(ExampleLayout.class.getResource("TestGraphe.class").getFile()));
    pn.add(pnChild);
    final BuComboBox cb = new BuComboBox();
    cb.setModel(new DefaultComboBoxModel(new String[] { "c1", "c2", "c3"}));
    pn.add(new BuLabel("cb 2 le retour: "));
    cb.setPreferredSize(new Dimension(30,20));
    pn.add(cb);
    pn.add(new BuLabel("bt 3: "));
    pn.add(new BuButton("tata"));
    pn.add(new BuLabel("txt 2 le retour: "));
    pnChild = new CtuluFileChooserPanel();
    final JTextField tf3=pnChild.getTf();
    pnChild.setFile(new File(ExampleLayout.class.getResource("TestBGraphe.class").getFile()));
    pn.add(pnChild);
    final Runnable r= new Runnable() {

        /**
         * @see java.lang.Runnable#run()
         */
        @Override
        public void run(){
          Rectangle rect = null;
          try {
            rect = tf1.modelToView(tf1.getText().length());
          }
          catch (BadLocationException e) {
            e.printStackTrace();
          }
          if (rect != null) {
            tf1.scrollRectToVisible(rect);
          }
          if (tf3 != null) {
            rect = null;
            try {
              rect = tf3.modelToView(tf3.getText().length());
            }
            catch (BadLocationException e) {
              e.printStackTrace();
            }
            if (rect != null) {
              tf3.scrollRectToVisible(rect);
            }
          }

        }
      };
      pn.afficheModale(BuLib.HELPER, "test�", r);
  }

}
