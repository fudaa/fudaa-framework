/*
 * @file         Matrix3D.java
 * @creation     1998-12-18
 * @modification $Date: 2006-10-19 14:13:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.example;

/**
 * A conventional 3D matrix object that can transform sets of 3D points and perform a variety of manipulations on the
 * transform
 */
/**
 * @version $Revision: 1.3 $ $Date: 2006-10-19 14:13:24 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class Matrix3D {
  double xx_, xy_, xz_, xo_;
  double yx_, yy_, yz_, yo_;
  double zx_, zy_, zz_, zo_;
  static final double PI = 3.14159265;

  /** Create a new unit matrix. */
  public Matrix3D() {
    xx_ = 1.0f;
    yy_ = 1.0f;
    zz_ = 1.0f;
  }

  /** Scale by f in all dimensions. */
  void scale(final double _f) {
    xx_ *= _f;
    xy_ *= _f;
    xz_ *= _f;
    xo_ *= _f;
    yx_ *= _f;
    yy_ *= _f;
    yz_ *= _f;
    yo_ *= _f;
    zx_ *= _f;
    zy_ *= _f;
    zz_ *= _f;
    zo_ *= _f;
  }

  /** Scale along each axis independently. */
  public void scale(final double _xf, final double _yf, final double _zf) {
    xx_ *= _xf;
    xy_ *= _xf;
    xz_ *= _xf;
    xo_ *= _xf;
    yx_ *= _yf;
    yy_ *= _yf;
    yz_ *= _yf;
    yo_ *= _yf;
    zx_ *= _zf;
    zy_ *= _zf;
    zz_ *= _zf;
    zo_ *= _zf;
  }

  /** Translate the origin. */
  public void translate(final double _x, final double _y, final double _z) {
    xo_ += _x;
    yo_ += _y;
    zo_ += _z;
  }

  /** rotate theta degrees about the y axis. */
  public void yrot(final double _theta) {
    double theta = _theta;
    theta *= (PI / 180);
    final double ct = Math.cos(theta);
    final double st = Math.sin(theta);
    final double nxx = (xx_ * ct + zx_ * st);
    final double nxy = (xy_ * ct + zy_ * st);
    final double nxz = (xz_ * ct + zz_ * st);
    final double nxo = (xo_ * ct + zo_ * st);
    final double nzx = (zx_ * ct - xx_ * st);
    final double nzy = (zy_ * ct - xy_ * st);
    final double nzz = (zz_ * ct - xz_ * st);
    final double nzo = (zo_ * ct - xo_ * st);
    xo_ = nxo;
    xx_ = nxx;
    xy_ = nxy;
    xz_ = nxz;
    zo_ = nzo;
    zx_ = nzx;
    zy_ = nzy;
    zz_ = nzz;
  }

  /** rotate theta degrees about the x axis. */
  public void xrot(final double _theta) {
    double theta = _theta;
    theta *= (PI / 180);
    final double ct = Math.cos(theta);
    final double st = Math.sin(theta);
    final double nyx = (yx_ * ct + zx_ * st);
    final double nyy = (yy_ * ct + zy_ * st);
    final double nyz = (yz_ * ct + zz_ * st);
    final double nyo = (yo_ * ct + zo_ * st);
    final double nzx = (zx_ * ct - yx_ * st);
    final double nzy = (zy_ * ct - yy_ * st);
    final double nzz = (zz_ * ct - yz_ * st);
    final double nzo = (zo_ * ct - yo_ * st);
    yo_ = nyo;
    yx_ = nyx;
    yy_ = nyy;
    yz_ = nyz;
    zo_ = nzo;
    zx_ = nzx;
    zy_ = nzy;
    zz_ = nzz;
  }

  /** rotate theta degrees about the z axis. */
  void zrot(final double _theta) {
    double theta = _theta;
    theta *= (PI / 180);
    final double ct = Math.cos(theta);
    final double st = Math.sin(theta);
    final double nyx = (yx_ * ct + xx_ * st);
    final double nyy = (yy_ * ct + xy_ * st);
    final double nyz = (yz_ * ct + xz_ * st);
    final double nyo = (yo_ * ct + xo_ * st);
    final double nxx = (xx_ * ct - yx_ * st);
    final double nxy = (xy_ * ct - yy_ * st);
    final double nxz = (xz_ * ct - yz_ * st);
    final double nxo = (xo_ * ct - yo_ * st);
    yo_ = nyo;
    yx_ = nyx;
    yy_ = nyy;
    yz_ = nyz;
    xo_ = nxo;
    xx_ = nxx;
    xy_ = nxy;
    xz_ = nxz;
  }

  /** Multiply this matrix by a second: M = M*R. */
  public void mult(final Matrix3D _rhs) {
    final double lxx = xx_ * _rhs.xx_ + yx_ * _rhs.xy_ + zx_ * _rhs.xz_;
    final double lxy = xy_ * _rhs.xx_ + yy_ * _rhs.xy_ + zy_ * _rhs.xz_;
    final double lxz = xz_ * _rhs.xx_ + yz_ * _rhs.xy_ + zz_ * _rhs.xz_;
    final double lxo = xo_ * _rhs.xx_ + yo_ * _rhs.xy_ + zo_ * _rhs.xz_ + _rhs.xo_;
    final double lyx = xx_ * _rhs.yx_ + yx_ * _rhs.yy_ + zx_ * _rhs.yz_;
    final double lyy = xy_ * _rhs.yx_ + yy_ * _rhs.yy_ + zy_ * _rhs.yz_;
    final double lyz = xz_ * _rhs.yx_ + yz_ * _rhs.yy_ + zz_ * _rhs.yz_;
    final double lyo = xo_ * _rhs.yx_ + yo_ * _rhs.yy_ + zo_ * _rhs.yz_ + _rhs.yo_;
    final double lzx = xx_ * _rhs.zx_ + yx_ * _rhs.zy_ + zx_ * _rhs.zz_;
    final double lzy = xy_ * _rhs.zx_ + yy_ * _rhs.zy_ + zy_ * _rhs.zz_;
    final double lzz = xz_ * _rhs.zx_ + yz_ * _rhs.zy_ + zz_ * _rhs.zz_;
    final double lzo = xo_ * _rhs.zx_ + yo_ * _rhs.zy_ + zo_ * _rhs.zz_ + _rhs.zo_;
    xx_ = lxx;
    xy_ = lxy;
    xz_ = lxz;
    xo_ = lxo;
    yx_ = lyx;
    yy_ = lyy;
    yz_ = lyz;
    yo_ = lyo;
    zx_ = lzx;
    zy_ = lzy;
    zz_ = lzz;
    zo_ = lzo;
  }

  /** Reinitialize to the unit matrix. */
  public void unit() {
    xo_ = 0;
    xx_ = 1;
    xy_ = 0;
    xz_ = 0;
    yo_ = 0;
    yx_ = 0;
    yy_ = 1;
    yz_ = 0;
    zo_ = 0;
    zx_ = 0;
    zy_ = 0;
    zz_ = 1;
  }

  /**
   * Transform nvert points from v into tv. v contains the input coordinates in doubleing point. Three successive
   * entries in the array constitute a point. tv ends up holding the transformed points as integers; three successive
   * entries per point
   */
  void transform(final double[] _v, final int[] _tv, final int _nvert) {
    final double lxx = xx_, lxy = xy_, lxz = xz_, lxo = xo_;
    final double lyx = yx_, lyy = yy_, lyz = yz_, lyo = yo_;
    final double lzx = zx_, lzy = zy_, lzz = zz_, lzo = zo_;
    for (int i = _nvert * 3; i >= 0; i -= 3) {
      final double x = _v[i];
      final double y = _v[i + 1];
      final double z = _v[i + 2];
      _tv[i] = (int) (x * lxx + y * lxy + z * lxz + lxo);
      _tv[i + 1] = (int) (x * lyx + y * lyy + z * lyz + lyo);
      _tv[i + 2] = (int) ((x * lzx + y * lzy + z * lzz + lzo) * 1000000.0f);
    }
  }

  @Override
  public String toString() {
    final String virg = ",";
    return ("[" + xo_ + virg + xx_ + virg + xy_ + virg + xz_ + ";" + yo_ + virg + yx_ + virg + yy_ + virg + yz_ + ";"
        + zo_ + virg + zx_ + virg + zy_ + virg + zz_ + "]");
  }
}
