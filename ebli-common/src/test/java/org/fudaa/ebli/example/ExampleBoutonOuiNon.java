/*
 * @creation 12 avr. 2006
 * @modification $Date: 2006-11-14 09:06:26 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.example;

import com.memoire.fu.FuLog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import org.fudaa.ebli.commun.EbliButtonYesNo;

/**
 * @author lacombe
 * @version $Id: TestBoutonOuiNon.java,v 1.3 2006-11-14 09:06:26 deniger Exp $
 */
public final class ExampleBoutonOuiNon {
  private ExampleBoutonOuiNon(){}

  public static void main(final String[] _args){
    final JFrame frame = new JFrame("Test EbliButtonYesNo");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    final EbliButtonYesNo boutonFalse = new EbliButtonYesNo();
    boutonFalse.setText("texte de boutonFalse");
    boutonFalse.setSelected(false);
    boutonFalse.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _e){
        FuLog.debug("boutonFalse e = " + _e);
      }
    });

    final EbliButtonYesNo boutonPrincipal = new EbliButtonYesNo(null, boutonFalse);
    boutonPrincipal.setText("texte de boutonPrincipal");
    boutonPrincipal.setSelected(true);
    boutonPrincipal.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent _e){
        FuLog.debug("boutonFalse e = " + _e);
      }
    });

    frame.getContentPane().add(boutonPrincipal);

    frame.pack();

    frame.setVisible(true);
  }

}
