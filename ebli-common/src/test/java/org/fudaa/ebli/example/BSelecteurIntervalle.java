/*
 * @creation     1998-07-07
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.example;

import com.memoire.bu.BuLightBorder;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JComponent;
import javax.swing.UIManager;

/**
 * Un selecteur d'intervalle.
 *
 * @version $Revision: 1.1 $ $Date: 2007-05-04 13:49:46 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class BSelecteurIntervalle extends JComponent implements MouseListener, MouseMotionListener {
  // donnees membres publiques
  public final static int VERTICAL = 1;
  public final static int HORIZONTAL = 2;
  // donnees membres privees
  private final static int AUCUNE = 0;
  private final static int B_INF = 1;
  private final static int B_SUP = 2;
  private int select_ = AUCUNE;
  private Dimension tailleDefaut_ = new Dimension(128, 16);

  // Constructeur
  public BSelecteurIntervalle() {
    super();
    borneFixInf_ = 0;
    borneInf_ = 30;
    borneFixSup_ = 100;
    borneSup_ = 60;
    setOrientation(VERTICAL);
    setBorder(new BuLightBorder(BuLightBorder.LOWERED, 1));
    setForeground(UIManager.getColor("Slider.foreground"));
    addMouseListener(this);
    addMouseMotionListener(this);
  }

  // paint
  @Override
  public void paint(final Graphics _g) {
    super.paint(_g);
    switch (orientation_) {
    case VERTICAL:
      paintVertical(_g);
      break;
    case HORIZONTAL:
      paintHorizontal(_g);
      break;
    default:
      break;
    }
  }

  private void paintVertical(final Graphics _g) {
    final Dimension d = getSize();
    final Insets f = getInsets();
    final Color coulInterv = getForeground();
    final int h = d.height - f.bottom - f.top;
    final int w = d.width - f.left - f.right;
    final int ys = f.top + h - borneSup_ * h / (borneFixSup_ - borneFixInf_);
    final int yi = f.top + h - borneInf_ * h / (borneFixSup_ - borneFixInf_);
    _g.setColor(coulInterv);
    _g.fill3DRect(f.left, ys, w, yi - ys, true);
  }

  private void paintHorizontal(final Graphics _g) {
    final Dimension d = getSize();
    final Insets f = getInsets();
    final Color coulInterv = getForeground();
    final int h = d.height - f.bottom - f.top;
    final int w = d.width - f.left - f.right;
    final int xs = f.left + w - borneSup_ * w / (borneFixSup_ - borneFixInf_);
    final int xi = f.left + w - borneInf_ * w / (borneFixSup_ - borneFixInf_);
    _g.setColor(coulInterv);
    _g.fill3DRect(xi, f.top, xs - xi, h, true);
  }

  // **********************************************
  // EVENEMENTS
  // **********************************************
  // mouse pressed
  @Override
  public void mousePressed(final MouseEvent _e) {
    switch (orientation_) {
    case VERTICAL:
      mousePressedVertical(_e.getY());
      return;
    case HORIZONTAL:
      mousePressedHorizontal(_e.getX());
      return;
    default:
      return;
    }
  }

  private void mousePressedVertical(final int _y) {
    final Dimension d = getSize();
    final Insets f = getInsets();
    final int h = d.height - f.bottom - f.top;
    if ((_y > f.top + h - borneSup_ * h / (borneFixSup_ - borneFixInf_) - 2)
        && (_y < f.top + h - borneSup_ * h / (borneFixSup_ - borneFixInf_) + 2)) {
      select_ = B_SUP;
    } else if ((_y > f.top + h - borneInf_ * h / (borneFixSup_ - borneFixInf_) - 2)
        && (_y < f.top + h - borneInf_ * h / (borneFixSup_ - borneFixInf_) + 2)) {
      select_ = B_INF;
    }
  }

  private void mousePressedHorizontal(final int _x) {
    final Dimension d = getSize();
    final Insets f = getInsets();
    final int w = d.width - f.left - f.right;
    if ((_x > f.left + w - borneSup_ * w / (borneFixSup_ - borneFixInf_) - 2)
        && (_x < f.left + w - borneSup_ * w / (borneFixSup_ - borneFixInf_) + 2)) {
      select_ = B_SUP;
    } else if ((_x > f.left + w - borneInf_ * w / (borneFixSup_ - borneFixInf_) - 2)
        && (_x < f.left + w - borneInf_ * w / (borneFixSup_ - borneFixInf_) + 2)) {
      select_ = B_INF;
    }
  }

  // mouse entered
  @Override
  public void mouseEntered(final MouseEvent _e) {}

  // mouse exited
  @Override
  public void mouseExited(final MouseEvent _e) {}

  // mouse clicked
  @Override
  public void mouseClicked(final MouseEvent _e) {}

  // mouse released
  @Override
  public void mouseReleased(final MouseEvent _e) {
    select_ = AUCUNE;
  }

  // mouse dragged
  @Override
  public void mouseDragged(final MouseEvent _e) {
    switch (orientation_) {
    case VERTICAL:
      mouseDraggedVertical(_e.getY());
      return;
    case HORIZONTAL:
      mouseDraggedHorizontal(_e.getX());
      return;
    default:
      return;
    }
  }

  private void mouseDraggedVertical(final int _y) {
    final Dimension d = getSize();
    final Insets f = getInsets();
    final int h = d.height - f.bottom - f.top;
    final int vn = (h - _y + f.top) * (borneFixSup_ - borneFixInf_) / h;
    if ((_y < f.top) || (_y > h + f.top)) {
      return;
    }
    if (select_ == B_SUP) {
      if (vn >= borneInf_) {
        setBorneSup(vn);
      }
    } else if (select_ == B_INF) {
      if (vn <= borneSup_) {
        setBorneInf(vn);
      }
    } else {
      return;
    }
    repaint();
  }

  private void mouseDraggedHorizontal(final int _x) {
    final Dimension d = getSize();
    final Insets f = getInsets();
    final int w = d.width - f.left - f.right;
    final int vn = (w - _x + f.left) * (borneFixSup_ - borneFixInf_) / w;
    if ((_x < f.left) || (_x > w + f.left)) {
      return;
    }
    if (select_ == B_SUP) {
      if (vn >= borneInf_) {
        setBorneSup(vn);
      }
    } else if (select_ == B_INF) {
      if (vn <= borneSup_) {
        setBorneInf(vn);
      }
    } else {
      return;
    }
    repaint();
  }

  // mouse moved
  @Override
  public void mouseMoved(final MouseEvent _e) {}
  // **********************************************
  // PROPRIETES INTERNES
  // **********************************************
  // Propriete orientation
  private int orientation_;

  int getOrientation() {
    return orientation_;
  }

  public final void setOrientation(final int _orientation) {
    if (orientation_ != _orientation) {
      final int vp = orientation_;
      orientation_ = _orientation;
      final Dimension d = new Dimension(getHeight(), getWidth());
      tailleDefaut_ = new Dimension(tailleDefaut_.height, tailleDefaut_.width);
      setMinimumSize(tailleDefaut_);
      setPreferredSize(tailleDefaut_);
      setSize(d);
      // repaint();
      firePropertyChange("Orientation", vp, orientation_);
    }
  }
  // Propriete borneFixInf
  private int borneFixInf_;

  int getBorneFixInf() {
    return borneFixInf_;
  }

  public void setBorneFixInf(final int _borneFixInf) {
    if (borneFixInf_ != _borneFixInf) {
      final int vp = borneFixInf_;
      borneFixInf_ = _borneFixInf;
      // repaint();
      firePropertyChange("BorneFixInf", vp, borneFixInf_);
    }
  }
  // Propriete borneFixSup
  private int borneFixSup_;

  int getBorneFixSup() {
    return borneFixSup_;
  }

  public void setBorneFixSup(final int _borneFixSup) {
    if (borneFixSup_ != _borneFixSup) {
      final int vp = borneFixSup_;
      borneFixSup_ = _borneFixSup;
      // repaint();
      firePropertyChange("BorneFixSup", vp, borneFixSup_);
    }
  }
  // **********************************************
  // PROPRIETES UTILISATEUR
  // **********************************************
  // Propriete borneInf
  private int borneInf_;

  int getBorneInf() {
    return borneInf_;
  }

  public void setBorneInf(final int _borneInf) {
    if (borneInf_ != _borneInf) {
      final int vp = borneInf_;
      borneInf_ = _borneInf;
      repaint();
      firePropertyChange("BorneInf", vp, borneInf_);
    }
  }
  // Propriete borneSup
  private int borneSup_;

  int getBorneSup() {
    return borneSup_;
  }

  public void setBorneSup(final int _borneSup) {
    if (borneSup_ != _borneSup) {
      final int vp = borneSup_;
      borneSup_ = _borneSup;
      repaint();
      firePropertyChange("BorneSup", vp, borneSup_);
    }
  }
}
