/*
 * @file         TestRepereEvent.java
 * @creation     1998-07-15
 * @modification $Date: 2006-10-19 14:13:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.example;
import org.fudaa.ebli.repere.RepereEvent;
import org.fudaa.ebli.repere.RepereEventListener;
/**
 * Une classe qui ecoute l'EvenementRepere.
 *
 * @version      $Revision: 1.2 $ $Date: 2006-10-19 14:13:24 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class ExampleRepereEvent implements RepereEventListener {
  @Override
  public void repereModifie(final RepereEvent _evt) {
    System.out.println(_evt.getSource().getClass().getName());
    for (int i= 0; i < 3; i++) {
      for (int j= 0; j < 3; j++) {
        if (_evt.aChange(i, j)) {
          System.out.println(
            "  "
              + i
              + ","
              + j
              + ": "
              + _evt.valeur(i, j)
              + " "
              + (_evt.relatif(i, j) ? "relatif" : "absolu"));
        }
      }
    }
  }
}
