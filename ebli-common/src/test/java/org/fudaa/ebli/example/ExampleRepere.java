/*
 * @creation     1998-07-15
 * @modification $Date: 2006-10-19 14:13:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.example;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import org.fudaa.ebli.controle.BMolette;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrVolume;
import org.fudaa.ebli.repere.BTransformationBouton;
import org.fudaa.ebli.repere.BTransformationGlissiere;
import org.fudaa.ebli.repere.BTransformationMolette;
import org.fudaa.ebli.repere.BTransformationRepere;
import org.fudaa.ebli.repere.BTransformationTexte;
import org.fudaa.ebli.repere.RepereEvent;

/**
 * Un test des Zooms.
 *
 * @version $Revision: 1.5 $ $Date: 2006-10-19 14:13:24 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public final class ExampleRepere {
  private ExampleRepere() {}
  final static int NBI = 8;
  final static int NBJ = 8;

  public static void main(final String[] _args) {
    // Test de surface 3D
    final GrPoint[][] points = new GrPoint[NBI][NBJ];
    final GrVolume surface = new GrVolume();
    computePts(points);
    System.out.println("Construction des polygones:");
    for (int i = 0; i < NBI - 1; i++) {
      System.out.println("ligne " + i);
      for (int j = 0; j < NBJ - 1; j++) {
        final GrPolygone pol = new GrPolygone();
        pol.sommets_.ajoute(points[i][j]);
        pol.sommets_.ajoute(points[i + 1][j]);
        pol.sommets_.ajoute(points[i + 1][j + 1]);
        pol.sommets_.ajoute(points[i][j + 1]);
        surface.polygones_.ajoute(pol);
      }
    }
    final BTransformationRepere t1 = new BTransformationRepere();
    t1.setMinimumSize(new Dimension(500, 500));
    t1.setPreferredSize(new Dimension(500, 500));
    t1.setSize(new Dimension(500, 500));
    t1.setGrVolume(surface);
    final BTransformationBouton z1 = new BTransformationBouton(RepereEvent.ZOOM);
    final BTransformationBouton z2 = new BTransformationBouton(RepereEvent.ZOOM, -1);
    final BTransformationTexte z3 = new BTransformationTexte();
    z3.setTypeTransfo(RepereEvent.ZOOM);
    final BTransformationGlissiere z4 = new BTransformationGlissiere();
    z4.setTypeTransfo(RepereEvent.ZOOM);
    final BTransformationBouton r1 = new BTransformationBouton(RepereEvent.ROT);
    final BTransformationBouton r2 = new BTransformationBouton(RepereEvent.ROT, -1);
    final BTransformationTexte r3 = new BTransformationTexte();
    r3.setTypeTransfo(RepereEvent.ROT);
    final BTransformationGlissiere r4 = new BTransformationGlissiere();
    r4.setTypeTransfo(RepereEvent.ROT_X);
    final BTransformationGlissiere r5 = new BTransformationGlissiere();
    r5.setTypeTransfo(RepereEvent.ROT_Y);
    final BTransformationGlissiere r6 = new BTransformationGlissiere();
    r6.setTypeTransfo(RepereEvent.ROT_Z);
    final BTransformationMolette r7 = new BTransformationMolette();
    r7.setTypeTransfo(RepereEvent.ROT_X);
    final BTransformationMolette r8 = new BTransformationMolette();
    r8.setTypeTransfo(RepereEvent.ROT_Y);
    final BTransformationMolette r9 = new BTransformationMolette();
    r9.setTypeTransfo(RepereEvent.ROT_Z);
    final BTransformationBouton l1 = new BTransformationBouton(RepereEvent.TRANS);
    final BTransformationBouton l2 = new BTransformationBouton(RepereEvent.TRANS, -1);
    final BTransformationTexte l3 = new BTransformationTexte();
    l3.setTypeTransfo(RepereEvent.TRANS);
    final BTransformationGlissiere l4 = new BTransformationGlissiere();
    l4.setTypeTransfo(RepereEvent.TRANS_X);
    final BTransformationGlissiere l5 = new BTransformationGlissiere();
    l5.setTypeTransfo(RepereEvent.TRANS_Y);
    final BTransformationGlissiere l6 = new BTransformationGlissiere();
    l6.setTypeTransfo(RepereEvent.TRANS_Z);
    final BTransformationMolette l7 = new BTransformationMolette();
    l7.setTypeTransfo(RepereEvent.TRANS_X);
    l7.setOrientation(BMolette.HORIZONTAL);
    final BTransformationMolette l8 = new BTransformationMolette();
    l8.setTypeTransfo(RepereEvent.TRANS_Y);
    final BTransformationMolette l9 = new BTransformationMolette();
    l9.setTypeTransfo(RepereEvent.TRANS_Z);
    // BTransformationRepere t1=new BTransformationRepere();
    final ExampleRepereEvent t = new ExampleRepereEvent();
    z1.addRepereEventListener(t);
    z1.addRepereEventListener(z3);
    z1.addRepereEventListener(r3);
    z1.addRepereEventListener(l3);
    z1.addRepereEventListener(t1);
    z2.addRepereEventListener(t);
    z2.addRepereEventListener(z3);
    z2.addRepereEventListener(r3);
    z2.addRepereEventListener(l3);
    z2.addRepereEventListener(t1);
    z3.addRepereEventListener(t);
    z3.addRepereEventListener(r3);
    z3.addRepereEventListener(l3);
    z3.addRepereEventListener(t1);
    z4.addRepereEventListener(t);
    z4.addRepereEventListener(z3);
    z4.addRepereEventListener(r3);
    z4.addRepereEventListener(l3);
    z4.addRepereEventListener(t1);
    r1.addRepereEventListener(t);
    r1.addRepereEventListener(z3);
    r1.addRepereEventListener(r3);
    r1.addRepereEventListener(l3);
    r1.addRepereEventListener(t1);
    r2.addRepereEventListener(t);
    r2.addRepereEventListener(z3);
    r2.addRepereEventListener(r3);
    r2.addRepereEventListener(l3);
    r2.addRepereEventListener(t1);
    r3.addRepereEventListener(t);
    r3.addRepereEventListener(z3);
    r3.addRepereEventListener(l3);
    r3.addRepereEventListener(t1);
    r4.addRepereEventListener(t);
    r4.addRepereEventListener(z3);
    r4.addRepereEventListener(r3);
    r4.addRepereEventListener(l3);
    r4.addRepereEventListener(t1);
    r5.addRepereEventListener(t);
    r5.addRepereEventListener(z3);
    r5.addRepereEventListener(r3);
    r5.addRepereEventListener(l3);
    r5.addRepereEventListener(t1);
    r6.addRepereEventListener(t);
    r6.addRepereEventListener(z3);
    r6.addRepereEventListener(r3);
    r6.addRepereEventListener(l3);
    r6.addRepereEventListener(t1);
    r7.addRepereEventListener(t);
    r7.addRepereEventListener(z3);
    r7.addRepereEventListener(r3);
    r7.addRepereEventListener(l3);
    r7.addRepereEventListener(t1);
    r8.addRepereEventListener(t);
    r8.addRepereEventListener(z3);
    r8.addRepereEventListener(r3);
    r8.addRepereEventListener(l3);
    r8.addRepereEventListener(t1);
    r9.addRepereEventListener(t);
    r9.addRepereEventListener(z3);
    r9.addRepereEventListener(r3);
    r9.addRepereEventListener(l3);
    r9.addRepereEventListener(t1);
    l1.addRepereEventListener(t);
    l1.addRepereEventListener(z3);
    l1.addRepereEventListener(r3);
    l1.addRepereEventListener(l3);
    l1.addRepereEventListener(t1);
    l2.addRepereEventListener(t);
    l2.addRepereEventListener(z3);
    l2.addRepereEventListener(r3);
    l2.addRepereEventListener(l3);
    l2.addRepereEventListener(t1);
    l3.addRepereEventListener(t);
    l3.addRepereEventListener(z3);
    l3.addRepereEventListener(r3);
    l3.addRepereEventListener(t1);
    l4.addRepereEventListener(t);
    l4.addRepereEventListener(z3);
    l4.addRepereEventListener(r3);
    l4.addRepereEventListener(l3);
    l4.addRepereEventListener(t1);
    l5.addRepereEventListener(t);
    l5.addRepereEventListener(z3);
    l5.addRepereEventListener(r3);
    l5.addRepereEventListener(l3);
    l5.addRepereEventListener(t1);
    l6.addRepereEventListener(t);
    l6.addRepereEventListener(z3);
    l6.addRepereEventListener(r3);
    l6.addRepereEventListener(l3);
    l6.addRepereEventListener(t1);
    l7.addRepereEventListener(t);
    l7.addRepereEventListener(z3);
    l7.addRepereEventListener(r3);
    l7.addRepereEventListener(l3);
    l7.addRepereEventListener(t1);
    l8.addRepereEventListener(t);
    l8.addRepereEventListener(z3);
    l8.addRepereEventListener(r3);
    l8.addRepereEventListener(l3);
    l8.addRepereEventListener(t1);
    l9.addRepereEventListener(t);
    l9.addRepereEventListener(z3);
    l9.addRepereEventListener(r3);
    l9.addRepereEventListener(l3);
    l9.addRepereEventListener(t1);
    final JComponent panneauZoom = new JPanel();
    panneauZoom.add(z1);
    panneauZoom.add(z2);
    panneauZoom.add(z3);
    panneauZoom.add(z4);
    final JComponent panneauRot = new JPanel();
    panneauRot.add(r1);
    panneauRot.add(r2);
    panneauRot.add(r3);
    panneauRot.add(r4);
    panneauRot.add(r5);
    panneauRot.add(r6);
    panneauRot.add(r7);
    panneauRot.add(r8);
    panneauRot.add(r9);
    final JComponent panneauTrans = new JPanel();
    panneauTrans.add(l1);
    panneauTrans.add(l2);
    panneauTrans.add(l3);
    panneauTrans.add(l4);
    panneauTrans.add(l5);
    panneauTrans.add(l6);
    panneauTrans.add(l7);
    panneauTrans.add(l8);
    panneauTrans.add(l9);
    final JComponent panneauAffic = new JPanel();
    panneauAffic.add(t1);
    final JComponent[] beans = new JComponent[] { panneauTrans, panneauZoom, panneauRot, panneauAffic };
    final ExampleFrame f = new ExampleFrame(beans);
    panneauTrans.setBorder(new TitledBorder(new BevelBorder(BevelBorder.RAISED), "Translation"));
    panneauZoom.setBorder(new TitledBorder(new BevelBorder(BevelBorder.RAISED), "Zoom"));
    panneauRot.setBorder(new TitledBorder(new BevelBorder(BevelBorder.RAISED), "Rotation"));
    panneauAffic.setBorder(new TitledBorder(new BevelBorder(BevelBorder.RAISED), "Affichage"));
    z1.setBorder(new LineBorder(Color.yellow, 2));
    z2.setBorder(new LineBorder(Color.yellow, 2));
    z3.setBorder(new LineBorder(Color.yellow, 2));
    z4.setBorder(new LineBorder(Color.yellow, 2));
    r1.setBorder(new LineBorder(Color.yellow, 2));
    r2.setBorder(new LineBorder(Color.yellow, 2));
    r3.setBorder(new LineBorder(Color.yellow, 2));
    r4.setBorder(new LineBorder(Color.yellow, 2));
    r5.setBorder(new LineBorder(Color.yellow, 2));
    r6.setBorder(new LineBorder(Color.yellow, 2));
    r7.setBorder(new LineBorder(Color.yellow, 2));
    r8.setBorder(new LineBorder(Color.yellow, 2));
    r9.setBorder(new LineBorder(Color.yellow, 2));
    l1.setBorder(new LineBorder(Color.yellow, 2));
    l2.setBorder(new LineBorder(Color.yellow, 2));
    l3.setBorder(new LineBorder(Color.yellow, 2));
    l4.setBorder(new LineBorder(Color.yellow, 2));
    l5.setBorder(new LineBorder(Color.yellow, 2));
    l6.setBorder(new LineBorder(Color.yellow, 2));
    l7.setBorder(new LineBorder(Color.yellow, 2));
    l8.setBorder(new LineBorder(Color.yellow, 2));
    l9.setBorder(new LineBorder(Color.yellow, 2));
    t1.setBorder(new LineBorder(Color.yellow, 2));
    f.setVisible(true);
  }

  private static void computePts(final GrPoint[][] _points) {
    System.out.println("Calcul des points:");
    for (int i = 0; i < NBI; i++) {
      System.out.println("ligne " + i);
      for (int j = 0; j < NBJ; j++) {
        _points[i][j] = new GrPoint(i, j, -Math.sin(2. * Math.PI * i / NBI) * Math.sin(2. * Math.PI * j / NBJ));
      }
    }
  }
}
