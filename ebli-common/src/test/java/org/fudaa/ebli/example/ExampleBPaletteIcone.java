/*
 * @file         TestBPaletteIcone.java
 * @creation     2000-11-09
 * @modification $Date: 2006-10-19 14:13:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.example;
import javax.swing.Icon;
import javax.swing.JComponent;
import org.fudaa.ebli.palette.BPaletteIconeSimple;
import org.fudaa.ebli.ressource.EbliResource;
/**
 * Un test de BPaletteIcone.
 *
 * @version      $Revision: 1.2 $ $Date: 2006-10-19 14:13:24 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public class ExampleBPaletteIcone {
  public static void main(final String[] args) {
    final BPaletteIconeSimple p= new BPaletteIconeSimple();
    p.setIcones(
      new Icon[] {
        EbliResource.EBLI.getIcon("polygone"),
        EbliResource.EBLI.getIcon("rectangle"),
        EbliResource.EBLI.getIcon("fleche"),
        });
    final JComponent[] beans= new JComponent[] { p };
    final ExampleFrame f= new ExampleFrame(beans);
    f.setVisible(true);
  }
}
