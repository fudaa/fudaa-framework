/*
 * @file         TestSelecteurs.java
 * @creation     1998-06-29
 * @modification $Date: 2006-11-14 09:06:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.example;
import java.awt.Color;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.JLabel;
import org.fudaa.ebli.controle.BSelecteurCouleur;
import org.fudaa.ebli.controle.BSelecteurReduitCouleur;
// import org.fudaa.ebli.trace.*;
/**
 * Un test des selecteurs.
 *
 * @version      $Revision: 1.3 $ $Date: 2006-11-14 09:06:26 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public class ExampleSelecteurs {
  public static void main(final String[] args) {
    final BSelecteurReduitCouleur src1= new BSelecteurReduitCouleur();
    final BSelecteurCouleur src2= new BSelecteurCouleur();
    final JComponent cmp= new JLabel("Hello world");
    cmp.setOpaque(true);
    final Dimension d= new Dimension(128, 128);
    cmp.setMinimumSize(d);
    cmp.setPreferredSize(d);
    src1.addPropertyChangeListener(new CouleurAdaptater(cmp));
    src2.addPropertyChangeListener(new CouleurAdaptater(cmp));
    final JComponent[] beans= new JComponent[] { cmp, src1, src2 };
    final ExampleFrame f= new ExampleFrame(beans);
    f.setVisible(true);
  }
}
class CouleurAdaptater implements PropertyChangeListener {
  JComponent c_;
  boolean b_;
  CouleurAdaptater(final JComponent _c) {
    c_= _c;
  }
  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    if (_evt.getPropertyName().equals("couleur")) {
      System.err.println("COULEUR=" + _evt.getNewValue());
      final Color couleur= (Color)_evt.getNewValue();
      if (b_) {
        c_.setBackground(couleur);
      } else {
        c_.setForeground(couleur);
      }
      b_= !b_;
      c_.repaint();
    }
  }
}
