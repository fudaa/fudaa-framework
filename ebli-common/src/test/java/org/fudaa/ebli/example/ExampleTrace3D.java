/*
 * @file         TestTrace3D.java
 * @creation     1998-07-22
 * @modification $Date: 2006-10-19 14:13:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.example;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrVolume;
import org.fudaa.ebli.trace.Trace3D;

/**
 * Un test de Trace3D.
 *
 * @version $Revision: 1.3 $ $Date: 2006-10-19 14:13:24 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class ExampleTrace3D extends MouseAdapter implements KeyListener {
  boolean running_ = true;

  boolean forward_ = true;

  GrVolume vol_;

  GrVolume vol2_;

  @Override
  public void keyPressed(final KeyEvent _evt) {
    if (!running_) {
      running_ = true;
      System.out.println("running: " + running_);
    }
    switch (_evt.getKeyCode()) {
    case KeyEvent.VK_UP:
      forward_ = true;
      return;
    case KeyEvent.VK_DOWN:
      forward_ = false;
      return;
    default:
      return;
    }
  }

  @Override
  public void keyReleased(final KeyEvent _evt) {}

  @Override
  public void keyTyped(final KeyEvent _evt) {}

  @Override
  public void mouseClicked(final MouseEvent _evt) {
    running_ = !running_;
    System.out.println("running: " + running_);
  }

  public static void main(final String[] _args) {
    new ExampleTrace3D().main();
  }

  public void main() {
    vol_ = GrVolume.creeCube().applique(GrMorphisme.dilatation(100.)).applique(GrMorphisme.translation(100., 100, 0.));
    vol2_ = GrVolume.creeCube().applique(GrMorphisme.dilatation(50.)).applique(GrMorphisme.translation(100., 50, 0.));
    final Frame f = new Frame();
    f.setSize(500, 500);
    final Canvas c = new Canvas() {
      @Override
      public void paint(Graphics _g) {
        Trace3D trace = new Trace3D(_g);
        trace.setCouleur(Color.blue);
        trace.ajouteGrVolume(vol_);
        trace.setCouleur(Color.red);
        trace.ajouteGrVolume(vol2_);
        trace.dessine();
      }
    };
    c.addMouseListener(this);
    c.addKeyListener(this);
    f.add(c);
    f.setVisible(true);
    double a = 0.;
    int sign = 1;
    final GrVolume vi = vol_;
    final GrVolume vi2 = vol2_;
    while (true) {
      if (running_) {
        if (forward_) {
          sign = 1;
        } else {
          sign = -1;
        }
        vol_ = vi.applique(GrMorphisme.translation(-150., -150., -50));
        vol_ = vol_.applique(GrMorphisme.rotation(a, a / 2., a / 4.));
        vol_ = vol_.applique(GrMorphisme.translation(150., 150., 50.));
        vol2_ = vi2.applique(GrMorphisme.translation(-125., -75., -50));
        vol2_ = vol2_.applique(GrMorphisme.rotation(-a, a / 2., -a / 4.));
        vol2_ = vol2_.applique(GrMorphisme.translation(125., 75., 50.));
        c.repaint();
        a += sign * 0.05;
      }
      try {
        Thread.sleep(100);
      } catch (final Exception ex) {}
    }
  }
}
