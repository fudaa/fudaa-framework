/**
 * @creation     1998-07-07
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.example;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.fudaa.ebli.controle.BMolette;
/**
 * Test des controles.
 *
 * @version      $Revision: 1.5 $ $Date: 2007-05-04 13:49:46 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class ExampleControles implements ChangeListener {
  BMolette m_;
  public ExampleControles(final BMolette _m) {
    this.m_= _m;
  }
  @Override
  public void stateChanged(final ChangeEvent _e) {
    System.err.println(
      "change "
        + (m_.getDirection() == BMolette.PLUS ? "+" : "-")
        + " adusting: "
        + m_.getValueIsAdjusting());
  }
  public static void main(final String[] _args) {
    final BSelecteurIntervalle s= new BSelecteurIntervalle();
    //s.setOrientation(BSelecteurIntervalle.HORIZONTAL);
    final BMolette m= new BMolette();
    m.addChangeListener(new ExampleControles(m));
    final JButton b= new JButton("bouton");
    final JComponent[] beans= new JComponent[] { s, m, b };
    final ExampleFrame f= new ExampleFrame(beans);
    //s.setBorder(new LineBorder(Color.yellow, 2));
    //m.setBorder(new LineBorder(Color.yellow, 2));
    //b.setBorder(new LineBorder(Color.yellow, 2));
    f.setVisible(true);
  }
}
