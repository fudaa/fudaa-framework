/*
 * @creation     1998-12-18
 * @modification $Date: 2006-10-19 14:13:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.example;

import com.memoire.fu.FuLog;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.FileReader;
import javax.swing.JComponent;



/**
 * @version $Revision: 1.2 $ $Date: 2006-10-19 14:13:24 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class BVolume3D extends JComponent implements MouseMotionListener {
  OOGLOFF obj_;

  boolean painted_ = true;

  String objname_;

  String message_;

  double xfac_;
  double scaleval_ = 0.1f;

  Matrix3D amat_ = new Matrix3D();

  Matrix3D tmat_ = new Matrix3D();

  // Image bbuffer;
  // Graphics bgc;
  int prevx_;

  int prevy_;

  public BVolume3D() {
    final Dimension d = new Dimension(512, 512);
    setPreferredSize(d);
    setSize(d);
    setDoubleBuffered(true);
    setVisible(true);
    init();
  }

  public void init() {
    objname_ = null;
    if (objname_ == null) {
      objname_ = "essai.off";
    }
    try {
      // OOGL_OFF x = new OOGL_OFF (new URL(getDocumentBase(), objname));
      final FileReader fin = new FileReader(objname_);
      final OOGLOFF x = new OOGLOFF(fin);
      obj_ = x;
      obj_.findBB(); // find bounding box
      double xw = obj_.xmax_ - obj_.xmin_; // so we can scale
      final double yw = obj_.ymax_ - obj_.ymin_; // the object to fit
      final double zw = obj_.zmax_ - obj_.zmin_; // in our window
      if (yw > xw) {
        xw = yw;
      }
      if (zw > xw) {
        xw = zw;
      }
      final double f1 = getSize().width / 2 / xw;
      final double f2 = getSize().height / 2 / xw;
      xfac_ = 0.7f * (f1 < f2 ? f1 : f2) * scaleval_;
    } catch (final Exception ex) {
      obj_ = null;
      message_ = ex.toString();
      FuLog.error(ex);
    }
    addMouseMotionListener(this);
    rotate();
  }

  // Repaint
  class RepaintTask extends Thread {
    private final BVolume3D volume_;

    RepaintTask(final BVolume3D _volume) {
      volume_ = _volume;
    }

    public void stopPainting() {
    // que faire
    }

    @Override
    public void run() {
      volume_.superRepaint();
      repaintTask_ = null;
    }
  }

  RepaintTask repaintTask_ ;

  @Override
  public void repaint() {
    if (repaintTask_ != null) {
      repaintTask_.stopPainting();
    }
    repaintTask_ = new RepaintTask(this);
    repaintTask_.setPriority(Thread.MIN_PRIORITY);
    repaintTask_.start();
  }

  void superRepaint() {
    super.repaint();
  }

  // Rotate
  class RotateTask extends Thread {
    BVolume3D volume_;

    boolean goOn_;

    public RotateTask(final BVolume3D _volume) {
      volume_ = _volume;
      goOn_ = true;
    }

    public void stopRotate() {
      goOn_ = false;
    }

    @Override
    public void run() {
      while (goOn_) {
        do {
          try {
            Thread.sleep(50);
          } catch (final Exception ex) {}
        } while (repaintTask_ != null);
        volume_.amat_.yrot(2.);
        volume_.repaint();
        try {
          Thread.sleep(100);
        } catch (final Exception ex) {}
      }
    }
  }

  private RotateTask rotateTask_;

  public void rotate() {
    if (rotateTask_ != null) {
      rotateTask_.stopRotate();
    }
    rotateTask_ = new RotateTask(this);
    rotateTask_.setPriority(Thread.MIN_PRIORITY);
    rotateTask_.start();
  }

  @Override
  public void mouseDragged(final MouseEvent _ev) {
    final int x = _ev.getX();
    final int y = _ev.getY();
    tmat_.unit();
    final double xtheta = (prevy_ - y) * 360.0f / getSize().width;
    final double ytheta = (prevx_ - x) * 360.0f / getSize().height;
    tmat_.xrot(xtheta);
    tmat_.yrot(ytheta);
    amat_.mult(tmat_);
    if (rotateTask_ == null) {
      repaint();
    }
    prevx_ = x;
    prevy_ = y;
  }

  @Override
  public void mouseMoved(final MouseEvent _ev) {}

  @Override
  public void update(final Graphics _g) {
    paint(_g);
  }

  @Override
  public void paint(final Graphics _g) {
    if (obj_ != null) {
      obj_.mat_.unit();
      obj_.mat_.translate(-(obj_.xmin_ + obj_.xmax_) / 2, -(obj_.ymin_ + obj_.ymax_) / 2, -(obj_.zmin_ + obj_.zmax_) / 2);
      obj_.mat_.mult(amat_);
      final int scale = (int) (xfac_ * getSize().width / 25);
      obj_.mat_.scale(scale, scale, 16 * xfac_ / getSize().width);
      obj_.mat_.translate(getSize().width / 2, getSize().height / 2, 8);
      obj_.transformed_ = false;
      _g.setColor(getBackground());
      _g.fillRect(0, 0, getSize().width, getSize().height);
      obj_.paint(_g);
      Toolkit.getDefaultToolkit().sync();
    }
  }
}
