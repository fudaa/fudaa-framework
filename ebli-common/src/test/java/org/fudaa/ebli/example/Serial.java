package org.fudaa.ebli.example;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.Serializable;

//TODO RENDRE Serializable les classes ci-dessous
//TODO ajouter la m�thode equals permettant de comparer les champs des classes
public class Serial {

    public static abstract class AbstractClient implements Serializable{
        String nom = "Test";

        private static final long serialVersionUID = 1000000;
        
        public AbstractClient() {

        }

        /**
         * @return the nom
         */
        protected String getNom() {
            return nom;
        }

        /**
         * @param nom the nom to set
         */
        protected void setNom(String nom) {
            this.nom = nom;
        }

        @Override
        public String toString() {
            return getClass().getName() + " ,nom=" + nom;
        }

     
    @Override
        public boolean equals(Object obj) {
        	if(! (obj instanceof AbstractClient))
				return false;
			
        	AbstractClient cli=(AbstractClient) obj;
        	if(nom!=null )
        		return nom.equals(cli.nom) ;
        	return false;

        	}
        
        
        
    }

    public static class Client extends AbstractClient {
        /**
		 * 
		 */
		private static final long serialVersionUID = 1000001;
		String pays = "France";

        public Client() {

        }

        /**
         * @return the age
         */
        protected String getPays() {
            return pays;
        }

        /**
         * @param pays the age to set
         */
        protected void setPays(String pays) {
            this.pays = pays;
        }

        @Override
        public String toString() {
            return super.toString() + " ,pays=" + pays;
        }
        
        
    @Override
        public boolean equals(Object obj) {
        	if((obj == null) || (obj.getClass() != this.getClass()))
				return false;
			
        	Client cli=(Client) obj;
        	if(nom!=null && pays!=null)
        		return  super.equals(obj) && pays.equals(cli.pays);
        	return false;

        	}
        		
        
    }

    public static class ClientFrancais extends Client {
        /**
		 * 
		 */
    	private static final long serialVersionUID = 1000002;
		int codePostal = 74000;

        public ClientFrancais() {

        }

        /**
         * @return the codePostal
         */
        protected int getCodePostal() {
            return codePostal;
        }

        /**
         * @param codePostal the codePostal to set
         */
        protected void setCodePostal(int codePostal) {
            this.codePostal = codePostal;
        }

        /**
         *
         */
        @Override
        public String toString() {
            return super.toString() + " ,codePostal=" + codePostal;
        }
        
    @Override
        public boolean equals(Object obj) {
        	if(super.equals(obj)){
        		if(! (obj instanceof ClientFrancais))
    				return false;	
        		 	return codePostal==((ClientFrancais) obj).codePostal;
        		

        	}
        	return false;
        
    }
    }

    public static void main(String[] args) {
        //XXX Ci dessous, voici un moyen pour serialiser/deserialiser un objet
        ObjectOutputStream out = null;
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        ClientFrancais test = new ClientFrancais();
        try {
            out = new ObjectOutputStream(outStream);
            out.writeObject(test);
        } catch (Exception e) {
            try {
                if (out != null) {
                    out.close();
                }
                //TODO d'apres toi est-ce que la ligne suivante est n�cessaire:
                //TODO si non pourquoi
                
                //-- Adrien: pas besoin car c'est out.close(); qui fait le boulot de lib�rer les ressources. --//
                //                outStream.close()
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        ObjectInputStream in = null;
        try {
            in = new ObjectInputStream(new
ByteArrayInputStream(outStream.toByteArray()));
            System.err.println(in.readObject());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //XXX  pour connaitre le SerialVersionUID de de Cient
        ObjectStreamClass desc = ObjectStreamClass.lookup(ClientFrancais.class);
        if (desc == null) {
            System.err.println("la classe n'est pas serializable");
        } else {
            System.err.println("le serial UID de Client est " +
desc.getSerialVersionUID());
            
            
            
        }
        
        ClientFrancais fr = new ClientFrancais();
        Client cl = new Client();
        System.out.println("client est �gale a clientFr " + cl.equals(fr));
        System.out.println("clientFr est �gale a client " + fr.equals(cl));

    }
    
    }
    
   
