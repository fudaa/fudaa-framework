/*
 * @file         TestInApplet.java
 * @creation     1998-12-18
 * @modification $Date: 2006-10-19 14:13:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.example;
import java.awt.Component;
import javax.swing.JApplet;
/**
 * @version      $Revision: 1.2 $ $Date: 2006-10-19 14:13:24 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class ExampleInApplet extends JApplet {
  String name;
  Component bean;
  @Override
  public void init() {
    try {
      name= getParameter("bean");
      bean= (Component)Class.forName(name).newInstance();
      getContentPane().add("Center", bean);
    } catch (final Exception ex) {
      System.err.println("$$$ TEST : " + ex);
    }
  }
}
