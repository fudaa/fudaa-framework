/*
 * @file         TestBPaletteTrait.java
 * @creation     1998-10-20
 * @modification $Date: 2006-10-19 14:13:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.example;
import java.awt.Color;
import javax.swing.JComponent;
import javax.swing.border.LineBorder;
import org.fudaa.ebli.palette.BPaletteTexture;
import org.fudaa.ebli.palette.BPaletteTrait;
/**
 * Un test de BPaletteTrait.
 *
 * @version      $Revision: 1.2 $ $Date: 2006-10-19 14:13:24 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class ExampleBPaletteTrait {
  public static void main(final String[] args) {
    final BPaletteTrait s= new BPaletteTrait();
    final BPaletteTexture t= new BPaletteTexture();
    final JComponent[] beans= new JComponent[] { s, t };
    final ExampleFrame f= new ExampleFrame(beans);
    s.setBorder(new LineBorder(Color.yellow, 2));
    t.setBorder(new LineBorder(Color.yellow, 2));
    f.setVisible(true);
  }
}
