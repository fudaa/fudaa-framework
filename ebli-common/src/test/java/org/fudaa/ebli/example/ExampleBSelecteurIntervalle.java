/*
 * @file         TestBSelecteurIntervalle.java
 * @creation     1998-07-07
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.example;
import java.awt.Color;
import javax.swing.JComponent;
import javax.swing.border.LineBorder;


/**
 * Un test de BSelecteurIntervalle.
 *
 * @version      $Revision: 1.3 $ $Date: 2007-05-04 13:49:46 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class ExampleBSelecteurIntervalle {
  public static void main(final String[] args) {
    final BSelecteurIntervalle s= new BSelecteurIntervalle();
    s.setOrientation(BSelecteurIntervalle.HORIZONTAL);
    final JComponent[] beans= new JComponent[] { s };
    final ExampleFrame f= new ExampleFrame(beans);
    s.setBorder(new LineBorder(Color.yellow, 2));
    f.setVisible(true);
  }
}
