/*
 * @file         TestBVolume3D.java
 * @creation     1998-06-29
 * @modification $Date: 2006-10-19 14:13:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.example;
import javax.swing.JComponent;
import org.fudaa.ebli.palette.BPaletteCouleurSimple;
/**
 * Un test de BVolume3D.
 *
 * @version      $Revision: 1.3 $ $Date: 2006-10-19 14:13:24 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public class ExampleBVolume3D {
  final static int NBI= 20000;
  public static void main(final String[] args) {
    final BPaletteCouleurSimple p= new BPaletteCouleurSimple();
    //     p.setEspace(true);
    //     p.setPaliers(8);
    //     p.setCouleurMax(Color.red);
    //     p.setCouleurMin(Color.black);
    final BVolume3D b= new BVolume3D();
    // b.setValeurs(v);
    // b.setDoubleBuffered(true);
    // b.setPalette(p);
    final JComponent[] beans= new JComponent[] { b, p };
    final ExampleFrame f= new ExampleFrame(beans);
    f.setVisible(true);
  }
}
