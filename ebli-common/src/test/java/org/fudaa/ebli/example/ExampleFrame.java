/*
 * @file         TestFrame.java
 * @creation     1998-06-24
 * @modification $Date: 2006-10-19 14:13:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.example;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.border.LineBorder;
/**
 * Une fenetre d'affichage.
 *
 * @version      $Id: TestFrame.java,v 1.2 2006-10-19 14:13:24 deniger Exp $
 * @author       Guillaume Desnoix
 */
public class ExampleFrame extends JFrame implements WindowListener, KeyListener {
  JComponent content;
  FlowLayout layout;
  public ExampleFrame() {
    super();
    layout= new FlowLayout(FlowLayout.LEFT);
    content= (JComponent)getContentPane();
    content.setLayout(layout);
    final JComboBox b= new JComboBox();
    b.setEditable(true);
    b.getEditor().getEditorComponent().addKeyListener(this);
    content.add(b);
    setTitle("Test");
    addWindowListener(this);
  }
  public ExampleFrame(final JComponent bean) {
    this();
    content.add(bean);
    setTitle("Test: " + bean.getClass().getName());
  }
  public ExampleFrame(final JComponent bean, final String _titre) {
    this();
    content.add(bean);
    setTitle("Test: " + _titre);
  }
  public ExampleFrame(final JComponent[] beans) {
    this();
    for (int i= 0; i < beans.length; i++) {
      if (beans[i].getBorder() == null) {
        beans[i].setBorder(new LineBorder(Color.black, 3));
      }
      content.add(beans[i]);
    }
    setTitle("Test: " + beans.length + " beans");
  }
  @Override
  public void setVisible(final boolean _b) {
    final Dimension d= getPreferredSize();
    setSize(d.width + 50, d.height + 50);
    super.setVisible(_b);
  }
  @Override
  public void windowActivated(final WindowEvent ev) {}
  @Override
  public void windowClosed(final WindowEvent ev) {}
  @Override
  public void windowClosing(final WindowEvent ev) {
    System.exit(0);
  }
  @Override
  public void windowDeactivated(final WindowEvent ev) {}
  @Override
  public void windowDeiconified(final WindowEvent ev) {}
  @Override
  public void windowIconified(final WindowEvent ev) {}
  @Override
  public void windowOpened(final WindowEvent ev) {}
  /**
   *
   */
  @Override
  public void keyPressed(final KeyEvent e) {
    System.out.println(e);
  }
  /**
   *
   */
  @Override
  public void keyReleased(final KeyEvent e) {
    System.out.println(e);
  }
  /**
   *
   */
  @Override
  public void keyTyped(final KeyEvent e) {
    System.out.println(e);
  }
  public static void main(final String[] args) {
    new ExampleFrame().show();
  }
}
