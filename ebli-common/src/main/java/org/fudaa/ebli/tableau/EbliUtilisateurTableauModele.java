/*
 * @creation     2000-09-18
 * @modification $Date: 2007-04-16 16:35:08 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.tableau;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import org.fudaa.ctulu.CtuluLib;

/**
 * @version $Id: EbliUtilisateurTableauModele.java,v 1.5 2007-04-16 16:35:08 deniger Exp $
 * @author Nicolas Maillot
 */
public class EbliUtilisateurTableauModele implements TableModel {
  TableModel table_;
  List formules_; // exemple: D A - 2 +
  List listeners_;

  EbliUtilisateurTableauModele(final TableModel _table) {
    table_ = _table;
    formules_ = new ArrayList();
    listeners_ = new ArrayList();
  }

  public void ajouteFormule(final String _formule) {
    formules_.add(_formule);
    fireTableDataChanged();
  }

  public void enleveFormule(final String _formule) {
    formules_.remove(_formule);
    fireTableDataChanged();
  }

  @Override
  public int getColumnCount() {
    return table_.getColumnCount() + formules_.size();
  }

  @Override
  public int getRowCount() {
    return table_.getRowCount();
  }

  @Override
  public boolean isCellEditable(final int _row, final int _col) {
    if (_row <= table_.getRowCount() && _col <= table_.getColumnCount()) {
      return table_.isCellEditable(_row, _col);
    }
    return false;
  }

  @Override
  public Object getValueAt(final int _row, final int _col) {
    Object ret = null;
    final EbliUtilisateurEvaluateur uc = new EbliUtilisateurEvaluateur();
    int i;
    if (_row < table_.getRowCount() && _col < table_.getColumnCount()) {
      ret = table_.getValueAt(_row, _col);
    }
    // Si on se situe sur les colonnes resultats d'operations sur les autres
    if (_row < table_.getRowCount() && _col >= table_.getColumnCount()
        && _col <= (formules_.size() + table_.getColumnCount())) {
      String form;
      // on met dans une hashtable element de la ligne
      final Hashtable tableSymboles = new Hashtable();
      for (i = 0; i < table_.getColumnCount(); i++) {
        if (i < table_.getColumnCount()) {
          tableSymboles.put("" + (char) ('A' + i), table_.getValueAt(_row, i));
        }
      }
      // Pour mettre les elements des colonnes calcules dans la hashtable, il faut faire appel a evalue
      for (i = table_.getColumnCount(); i < _col; i++) {
        try {
          tableSymboles.put("" + (char) ('A' + i), CtuluLib.getDouble(uc.evalue((String) (formules_.get(i
              - table_.getColumnCount())), tableSymboles)));
        } catch (final IllegalArgumentException ex) {}
      }
      // il ne reste plus qu'a evaluer l'expression selon la colonne ou l'on se trouve
      form = (String) (formules_.get(_col - table_.getColumnCount()));
      try {
        ret = CtuluLib.getDouble(uc.evalue(form, tableSymboles));
      } catch (final IllegalArgumentException ex) {}
    }
    return ret;
  }

  @Override
  public String getColumnName(final int _col) {
    String r = "" + (char) ('A' + _col);
    if (_col < table_.getColumnCount()) {
      r = table_.getColumnName(_col) + " [" + r + "]";
    } else {
      r = " (" + formules_.get(_col - table_.getColumnCount()) + ") " + "[" + r + "]";
    }
    return r;
  }

  @Override
  public void setValueAt(final java.lang.Object _obj, final int _row, final int _col) {}

  @Override
  public Class getColumnClass(final int _col) {
    return Double.class;
  }

  // Gestion du vecteur de Listener
  @Override
  public void addTableModelListener(final TableModelListener _l) {
    listeners_.add(_l);
  }

  @Override
  public void removeTableModelListener(final TableModelListener _l) {
    listeners_.remove(_l);
  }

  // Envoie a tous les listeners un evenement signalant la modification de la structure de la table
  public void fireTableDataChanged() {
    for (int i = 0; i < listeners_.size(); i++) {
      ((TableModelListener) listeners_.get(i))
          .tableChanged(new TableModelEvent(this, TableModelEvent.HEADER_ROW));
    }
  }
}
