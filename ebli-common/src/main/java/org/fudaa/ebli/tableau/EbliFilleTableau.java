/*
 * @creation     1998-10-15
 * @modification $Date: 2006-09-19 14:55:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.tableau;
import com.memoire.bu.*;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.impression.EbliFilleImprimable;
import org.fudaa.ebli.impression.EbliPrinter;
/**
 * Fenetre fille pour l'affichage d'un tableau.
 *
 * @version      $Id: EbliFilleTableau.java,v 1.5 2006-09-19 14:55:56 deniger Exp $
 * @author       Guillaume Desnoix , Nicolas Maillot
 */
public class EbliFilleTableau
  extends EbliFilleImprimable
  implements BuCutCopyPasteInterface {
  protected BuCommonImplementation app_;
  private JComponent content_;
  protected BuButton btAjouter_;
  protected BuButton btEnlever_;
  private BuTable table_;
  private EbliUtilisateurTableauModele fu_;
  protected boolean utiliseFormules_;
  public EbliFilleTableau() {
    this(null, null);
  }
  public EbliFilleTableau(
    final BuCommonImplementation _app,
    final BuInformationsDocument _id) {
    super("", true, true, true, true, null, _id);
    if (_app != null) {
      super.setInformationsSoftware(_app.getInformationsSoftware());
    }
    app_= _app;
    utiliseFormules_= false;
    setName("ifTABLEAU");
    btAjouter_= new BuButton(BuResource.BU.getString("Ajouter"));
    btAjouter_.setToolTipText(EbliLib.getS("Ajouter une formule"));
    //    btAjouter_.setIcon(BuLib.loadToolCommandIcon("ajouter"));
    btAjouter_.setIcon(BuResource.BU.loadMenuCommandIcon("ajouter"));
    btAjouter_.setActionCommand("AJOUTER");
    btAjouter_.addActionListener(this);
    btEnlever_= new BuButton(BuResource.BU.getString("Enlever"));
    btEnlever_.setToolTipText(EbliLib.getS("Enlever une formule"));
    btEnlever_.setIcon(BuResource.BU.loadMenuCommandIcon("enlever"));
    btEnlever_.setActionCommand("ENLEVER");
    btEnlever_.addActionListener(this);
    content_= (JComponent)getContentPane();
    content_.setLayout(new BuBorderLayout());
    setTitle(EbliLib.getS("Tableau"));
    setFrameIcon(BuResource.BU.getIcon("tableau"));
    setLocation(60, 60);
  }
  public void setUtiliseFormules(final boolean _utiliseFormules) {
    utiliseFormules_= _utiliseFormules;
  }
  public BuTable getTable() {
    return table_;
  }
  public void setTable(final BuTable _table) {
    table_= _table;
    if (utiliseFormules_) {
      fu_= new EbliUtilisateurTableauModele(table_.getModel());
      if (!(table_.getModel() instanceof EbliUtilisateurTableauModele)) {
        table_.setModel(fu_);
      }
    }
    content_.removeAll();
    content_.add(new JScrollPane(table_), BuBorderLayout.CENTER);
    final Dimension d= table_.getPreferredSize();
    d.width= Math.min(d.width, 470) + 30;
    d.height= Math.min(d.height, 220) + 30;
    setPreferredSize(d);
    pack();
  }
  @Override
  public void actionPerformed(final ActionEvent _evt) {
    if ("AJOUTER".equals(_evt.getActionCommand())) {
      final BuDialogInput dial=
        new BuDialogInput(
          app_,
          (app_ == null ? null : app_.getInformationsSoftware()),
      EbliLib.getS("Ajouter une formule"),
      EbliLib.getS("Formule") + ":",
          "");
      if (dial.activate() != JOptionPane.CANCEL_OPTION) {
        fu_.ajouteFormule(dial.getValue());
      }
    }
    if ("ENLEVER".equals(_evt.getActionCommand())) {
      final BuDialogInput dial=
        new BuDialogInput(
          app_,
          (app_ == null ? null : app_.getInformationsSoftware()),
          EbliLib.getS("Enlever une formule"),
          EbliLib.getS("Formule") + ":",
          "");
      if (dial.activate() != JOptionPane.CANCEL_OPTION) {
        fu_.enleveFormule(dial.getValue());
      }
    }
  }

  @Override
  public String[] getEnabledActions() {
    return new String[] { "IMPRIMER", "MISEENPAGE", "PREVISUALISER","COPIER" };
  }
  // BuCutCopyPaste
  @Override
  public void cut() {}
  @Override
  public void copy() {
    table_.copy();
  }


  @Override
  public void duplicate() {}
  @Override
  public void paste() {}
  // BuInternalFrame
  @Override
  public JComponent[] getSpecificTools() {
    final JComponent[] r= new JComponent[2];
    if (utiliseFormules_) {
      r[0]= btAjouter_;
      r[1]= btEnlever_;
    }
    return r;
  }
  // BuPrintable
  /*
  public void print(PrintJob _job,Graphics _g)
  {
    BuPrinter.INFO_DOC     =new BuInformationsDocument();
    BuPrinter.INFO_DOC.name=getTitle();
    BuPrinter.INFO_DOC.logo=BuResource.BU.getIcon("tableau");

    BuPrinter.printTable(_job,_g,table_);
  }*/
  @Override
  public int getNumberOfPages() {
    return 1;
  }
  @Override
  public int print(final Graphics _g, final PageFormat _format, final int _numPage) {
    return EbliPrinter.printTable(_g, _format, table_, _numPage);
  }
}
