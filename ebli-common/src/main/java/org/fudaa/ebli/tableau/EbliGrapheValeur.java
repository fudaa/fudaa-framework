/*
 *  @creation     19 juin 2003
 *  @modification $Date: 2006-09-19 14:55:56 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.tableau;

import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author deniger
 * @version $Id: EbliGrapheValeur.java,v 1.5 2006-09-19 14:55:56 deniger Exp $
 */
public class EbliGrapheValeur {
  SortedSet list_;
  boolean sameAbsAuthorized_;

  /**
   *
   */
  public EbliGrapheValeur(final boolean _sameAbsAuthorized) {
    list_ = new TreeSet();
    sameAbsAuthorized_ = _sameAbsAuthorized;
  }

  public boolean containsAbs(final double _d) {
    for (final Iterator it = list_.iterator(); it.hasNext();) {
      final ValeurField f = (ValeurField) it.next();
      if (f.isAbsEquals(_d)) {
        return true;
      }
    }
    return false;
  }

  public boolean addXY(final double _x, final double _y) {
    if (sameAbsAuthorized_ && containsAbs(_x)) {
      return false;
    }
    list_.add(new ValeurField(_x, _y));
    return true;
  }

  public ValeurField[] getValeurField() {
    final ValeurField[] r = new ValeurField[list_.size()];
    list_.toArray(r);
    return r;
  }
  public static class ValeurField implements Comparable {
    private final double abs_;
    private final double coord_;

    public ValeurField(final double _abs, final double _coord) {
      abs_ = _abs;
      coord_ = _coord;
    }

    public double getX() {
      return abs_;
    }

    public double getY() {
      return coord_;
    }

    public boolean isAbsEquals(final ValeurField _f) {
      return abs_ == _f.abs_;
    }

    public boolean isAbsEquals(final double _f) {
      return abs_ == _f;
    }

    @Override
    public int compareTo(final Object _o) {
      if (_o instanceof ValeurField) {
        final ValeurField f = (ValeurField) _o;
        return abs_ > f.abs_ ? 1 : abs_ < f.abs_ ? -1 : coord_ > f.coord_ ? 1 : coord_ > f.coord_ ? -1 : 0;
      }
      throw new IllegalArgumentException("the parameters is not a FudaaGrapheValeur field");
    }
  }
}
