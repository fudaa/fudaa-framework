/*
 * @creation     2000-09-18
 * @modification $Date: 2007-04-16 16:35:08 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
/*Authors: Nicolas Maillot-Guillaume Desnoix
  Statut:stable*/
package org.fudaa.ebli.tableau;
import java.util.Hashtable;
import java.util.Stack;
import java.util.StringTokenizer;
import org.fudaa.ctulu.CtuluLib;
/**
 * @version      $Revision: 1.5 $ $Date: 2007-04-16 16:35:08 $ by $Author: deniger $
 * @author       Nicolas Maillot , Guillaume Desnoix
 */
public class EbliUtilisateurEvaluateur {
  private Stack pile_;
  public EbliUtilisateurEvaluateur() {
    pile_= new Stack();
  }
  public void empile(final Number _valeur) {
    pile_.push(_valeur);
  }
  public double depile() {
    return ((Number)pile_.pop()).doubleValue();
  }
  public double evalue(final String _formule, final Hashtable _vars)
    throws IllegalArgumentException {
    try {
      final StringTokenizer tokenizer= new StringTokenizer(_formule, " ");
      pile_.clear();
      while (tokenizer.hasMoreTokens()) {
        String st;
        double r;
        st= tokenizer.nextToken();
        //Si il s'agit d'un operateur, on effectue l'operation
        if ("+".equals(st)) {
          r= depile() + depile();
        } else if ("-".equals(st)) {
          r= -depile() + depile();
        } else if ("*".equals(st)) {
          r= depile() * depile();
        } else if ("/".equals(st)) {
          r= (1. / depile()) * depile();
        } else if ("^".equals(st)) {
          r= depile();
          r= Math.pow(depile(), r);
        } else          //operateurs unaires
          if (st.equals("abs")) {
            r= Math.abs(depile());
          } else if (st.equals("acos")) {
            r= Math.acos(depile());
          } else if (st.equals("asin")) {
            r= Math.asin(depile());
          } else if (st.equals("atan")) {
            r= Math.atan(depile());
          } else if (st.equals("ceil")) {
            r= Math.ceil(depile());
          } else if (st.equals("floor")) {
            r= Math.ceil(depile());
          } else if (st.equals("cos")) {
            r= Math.cos(depile());
          } else if (st.equals("sin")) {
            r= Math.sin(depile());
          } else if (st.equals("tan")) {
            r= Math.tan(depile());
          } else if (st.equals("log")) {
            r= Math.log(depile());
          } else if (st.equals("exp")) {
            r= Math.exp(depile());
          } else if (st.equals("sqrt")) {
            r= Math.sqrt(depile());
          } else if (st.equals("toDegrees")) {
            r= Math.toDegrees(depile());
          } else if (st.equals("toRadians")) {
            r= Math.toRadians(depile());
          } else if (st.equals("min")) {
            r= Math.min(depile(), depile());
          } else if (st.equals("max")) {
            r= Math.max(depile(), depile());
          } else if (st.equals(">")) {
            r= ((depile() < depile()) ? 1. : 0.);
          } else if (st.equals(">=")) {
            r= (depile() <= depile()) ? 1. : 0.;
          } else if (st.equals("<")) {
            r= (depile() > depile()) ? 1. : 0.;
          } else if (st.equals("<=")) {
            r= (depile() >= depile()) ? 1. : 0.;
          } else {
            if (_vars.get(st) == null) {
              r= (new Double(st)).doubleValue();
            } else {
              r= ((Number)_vars.get(st)).doubleValue();
            }
          }
        empile(CtuluLib.getDouble(r));
      }
      return depile();
    } catch (final Exception ex) {
      throw new IllegalArgumentException(_formule);
    }
  }
}
