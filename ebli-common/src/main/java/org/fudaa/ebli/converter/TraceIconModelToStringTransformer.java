/*
 GPL 2
 */
package org.fudaa.ebli.converter;

import com.memoire.fu.FuLib;
import java.awt.Color;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.ctulu.converter.ColorToStringTransformer;
import org.fudaa.ctulu.converter.PropertyToStringCacheDecorator;
import org.fudaa.ebli.trace.TraceIconModel;

/**
 *
 * @author Frederic Deniger
 */
public class TraceIconModelToStringTransformer extends AbstractPropertyToStringTransformer<TraceIconModel> {

  AbstractPropertyToStringTransformer<Color> colorToStringTransformer;

  public TraceIconModelToStringTransformer(AbstractPropertyToStringTransformer<Color> toStringTransformer) {
    super(TraceIconModel.class);
    this.colorToStringTransformer = toStringTransformer;
  }

  public TraceIconModelToStringTransformer() {
    super(TraceIconModel.class);
    colorToStringTransformer = new PropertyToStringCacheDecorator<Color>(new ColorToStringTransformer());
  }

  @Override
  public String toStringSafe(TraceIconModel in) {
    final String string = ";";
    return in.getType()
            + string + in.getTaille()
            + string + in.getTypeLigne()
            + string + in.getEpaisseurLigne()
            + string + colorToStringTransformer.toString(in.getCouleur())
            + string + colorToStringTransformer.toString(in.getBackgroundColor())
            + string + Boolean.toString(in.isBackgroundColorPainted());
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public TraceIconModel fromStringSafe(String in) {
    String[] split = FuLib.split(in, ';');
    if (split.length != 7) {
      return null;
    }
    try {
      int idx = 0;
      int type = Integer.parseInt(split[idx++]);
      int taille = Integer.parseInt(split[idx++]);
      int typeLigne = Integer.parseInt(split[idx++]);
      float epaisseur = Float.parseFloat(split[idx++]);
      Color c = colorToStringTransformer.fromString(split[idx++]);
      Color background = colorToStringTransformer.fromString(split[idx++]);
      boolean backgroundPainted = Boolean.parseBoolean(split[idx++]);
      final TraceIconModel traceIconModel = new TraceIconModel(type, taille, c);
      traceIconModel.setBackgroundColor(background);
      traceIconModel.setEpaisseurLigne(epaisseur);
      traceIconModel.setTypeLigne(typeLigne);
      traceIconModel.setBackgroundColorPainted(backgroundPainted);
      return traceIconModel;
    } catch (NumberFormatException numberFormatException) {
    }
    return null;
  }
}
