/*
 GPL 2
 */
package org.fudaa.ebli.converter;

import com.memoire.fu.FuLib;
import java.awt.Color;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.ctulu.converter.ColorToStringTransformer;
import org.fudaa.ctulu.converter.PropertyToStringCacheDecorator;
import org.fudaa.ebli.trace.TraceBox;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 *
 * @author Frederic Deniger
 */
public class TraceBoxToStringTransformer extends AbstractPropertyToStringTransformer<TraceBox> {

  AbstractPropertyToStringTransformer<Color> colorToStringTransformer;
  TraceLigneModelToStringTransformer traceLigneModelToStringTransformer;
  char sep = '|';

  public TraceBoxToStringTransformer() {
    this(new PropertyToStringCacheDecorator<Color>(new ColorToStringTransformer()));
  }

  public TraceBoxToStringTransformer(AbstractPropertyToStringTransformer<Color> toStringTransformer) {
    super(TraceBox.class);
    this.colorToStringTransformer = toStringTransformer;
    traceLigneModelToStringTransformer = new TraceLigneModelToStringTransformer();
  }

  @Override
  public String toStringSafe(TraceBox in) {
    StringBuilder builder = new StringBuilder();
    builder.append(colorToStringTransformer.toString(in.getColorBoite()));
    builder.append(sep);
    builder.append(colorToStringTransformer.toString(in.getColorFond()));
    builder.append(sep);
    builder.append(colorToStringTransformer.toString(in.getColorText()));
    builder.append(sep);
    builder.append(Boolean.toString(in.isDrawBox()));
    builder.append(sep);
    builder.append(Boolean.toString(in.isVertical()));
    builder.append(sep);
    builder.append(Boolean.toString(in.isDrawFond()));
    builder.append(sep);
    builder.append(Integer.toString(in.getHMargin()));
    builder.append(sep);
    builder.append(Integer.toString(in.getVMargin()));
    builder.append(sep);
    builder.append(Integer.toString(in.getHPosition()));
    builder.append(sep);
    builder.append(Integer.toString(in.getVPosition()));
    builder.append(sep);
    builder.append(traceLigneModelToStringTransformer.toString(in.getTraceLigne().getModel()));
    return builder.toString();
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public TraceBox fromStringSafe(String in) {
    String[] split = FuLib.split(in, sep);
    if (split.length != 11) {
      return null;
    }
    try {
      int idx = 0;
      TraceBox res = new TraceBox();
      res.setColorBoite(colorToStringTransformer.fromString(split[idx++]));
      res.setColorFond(colorToStringTransformer.fromString(split[idx++]));
      res.setColorText(colorToStringTransformer.fromString(split[idx++]));
      res.setDrawBox(Boolean.parseBoolean(split[idx++]));
      res.setVertical(Boolean.parseBoolean(split[idx++]));
      res.setDrawFond(Boolean.parseBoolean(split[idx++]));
      res.setHMargin(Integer.parseInt(split[idx++]));
      res.setVMargin(Integer.parseInt(split[idx++]));
      res.setHPosition(Integer.parseInt(split[idx++]));
      res.setVPosition(Integer.parseInt(split[idx++]));
      TraceLigneModel ligneModel = traceLigneModelToStringTransformer.fromString(split[idx++]);
      if (ligneModel != null) {
        res.getTraceLigne().setModel(ligneModel);
      }
      return res;
    } catch (NumberFormatException numberFormatException) {
    }
    return null;
  }
}
