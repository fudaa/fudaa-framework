/*
 GPL 2
 */
package org.fudaa.ebli.converter;

import java.awt.Color;
import java.awt.Font;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.ctulu.converter.ColorToStringTransformer;
import org.fudaa.ctulu.converter.FontToStringTransformer;
import org.fudaa.ctulu.converter.PropertyToStringCacheDecorator;

/**
 *
 * @author Frederic Deniger
 */
public class TraceToStringConverter {

  private final AbstractPropertyToStringTransformer<Color> colorToStringTransformer = new PropertyToStringCacheDecorator<Color>(new ColorToStringTransformer());
  private final AbstractPropertyToStringTransformer<Font> fontToStringTransformer = new PropertyToStringCacheDecorator<Font>(new FontToStringTransformer());
  private final TraceIconModelToStringTransformer traceIconModelToStringTransformer = new TraceIconModelToStringTransformer(colorToStringTransformer);
  private final TraceLigneModelToStringTransformer traceLigneModelToStringTransformer = new TraceLigneModelToStringTransformer(colorToStringTransformer);
  private final TraceBoxToStringTransformer traceBoxToStringTransformer = new TraceBoxToStringTransformer(colorToStringTransformer);

  public AbstractPropertyToStringTransformer<Color> getColorToStringTransformer() {
    return colorToStringTransformer;
  }

  public AbstractPropertyToStringTransformer<Font> getFontToStringTransformer() {
    return fontToStringTransformer;
  }

  public TraceIconModelToStringTransformer getTraceIconModelToStringTransformer() {
    return traceIconModelToStringTransformer;
  }

  public TraceLigneModelToStringTransformer getTraceLigneModelToStringTransformer() {
    return traceLigneModelToStringTransformer;
  }

  public TraceBoxToStringTransformer getTraceBoxToStringTransformer() {
    return traceBoxToStringTransformer;
  }
}
