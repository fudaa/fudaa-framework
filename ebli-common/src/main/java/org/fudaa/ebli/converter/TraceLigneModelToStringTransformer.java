/*
 GPL 2
 */
package org.fudaa.ebli.converter;

import com.memoire.fu.FuLib;
import java.awt.Color;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;
import org.fudaa.ctulu.converter.ColorToStringTransformer;
import org.fudaa.ctulu.converter.PropertyToStringCacheDecorator;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 *
 * @author Frederic Deniger
 */
public class TraceLigneModelToStringTransformer extends AbstractPropertyToStringTransformer<TraceLigneModel> {

  AbstractPropertyToStringTransformer<Color> colorToStringTransformer;

  public TraceLigneModelToStringTransformer(AbstractPropertyToStringTransformer<Color> toStringTransformer) {
    super(TraceLigneModel.class);
    this.colorToStringTransformer = toStringTransformer;
  }

  public TraceLigneModelToStringTransformer() {
    super(TraceLigneModel.class);
    colorToStringTransformer = new PropertyToStringCacheDecorator<Color>(new ColorToStringTransformer());
  }

  @Override
  public String toStringSafe(TraceLigneModel in) {
    final String string = ";";
    return in.getTypeTrait() + string + in.getEpaisseur() + string + colorToStringTransformer.toString(in.getCouleur());
  }

  @Override
  public boolean isValidSafe(String in) {
    return fromString(in) != null;
  }

  @Override
  public TraceLigneModel fromStringSafe(String in) {
    String[] split = FuLib.split(in, ';');
    if (split.length != 3) {
      return null;
    }
    try {
      int type = Integer.parseInt(split[0]);
      float epaisseur = Float.parseFloat(split[1]);
      Color c = colorToStringTransformer.fromString(split[2]);
      return new TraceLigneModel(type, epaisseur, c);
    } catch (NumberFormatException numberFormatException) {
    }
    return null;
  }
}
