/*
 GPL 2
 */
package org.fudaa.ebli.converter;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 *
 * @author Frederic Deniger
 */
public class TraceLigneToStringXstreamConverter implements Converter {

  private final TraceLigneModelToStringTransformer toStringTransformer;

  public TraceLigneToStringXstreamConverter(TraceLigneModelToStringTransformer toStringTransformer) {
    this.toStringTransformer = toStringTransformer;
  }

  @Override
  public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
    if (source == null) {
      writer.setValue("");
    } else {
      writer.setValue(toStringTransformer.toString(((TraceLigne) source).getModel()));
    }
  }

  @Override
  public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
    String value = reader.getValue();
    if (CtuluLibString.isEmpty(value)) {
      return null;
    }
    TraceLigneModel fromString = toStringTransformer.fromString(value);
    if (fromString != null) {
      return new TraceLigne(fromString);
    }
    return null;
  }

  @Override
  public boolean canConvert(Class type) {
    return TraceLigne.class.equals(type);
  }
}
