/*
 GPL 2
 */
package org.fudaa.ebli.converter;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import java.util.Collection;
import org.fudaa.ctulu.converter.AbstractPropertyToStringTransformer;

/**
 *
 * Permet d'adapter a Xstream les transformers.
 *
 * @author Frederic Deniger
 */
public class ToStringTransfomerXstreamConverter<T> implements Converter {

  private final AbstractPropertyToStringTransformer<T> toStringTransformer;
  private final Class supportedClass;

  public ToStringTransfomerXstreamConverter(AbstractPropertyToStringTransformer<T> toStringTransformer, Class supportedClass) {
    this.toStringTransformer = toStringTransformer;
    this.supportedClass = supportedClass;
  }

  public AbstractPropertyToStringTransformer<T> getToStringTransformer() {
    return toStringTransformer;
  }

  @Override
  public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
    writer.setValue(toStringTransformer.toString((T) source));
  }

  @Override
  public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
    return toStringTransformer.fromString(reader.getValue());
  }

  @Override
  public boolean canConvert(Class type) {
    return supportedClass.equals(type);
  }

  public static void register(Collection<? extends AbstractPropertyToStringTransformer> transformers, XStream xstream) {
    for (AbstractPropertyToStringTransformer transformer : transformers) {
      xstream.registerConverter(new ToStringTransfomerXstreamConverter(transformer, transformer.getSupportedClass()));

    }
  }
}
