/*
 * @creation     2000-01-12
 * @modification $Date: 2006-04-12 15:28:04 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.dialog;

import javax.swing.JFileChooser;
/**
 * Le JFileChooser de Swing avec une gestion du langage pour les Strings du
 * dialogue. Il s'utilise de la m�me facon que le JFileChooser.
 * <p>
 * Par d�faut, les Strings du dialogue sont en francais. Ils peuvent �tre mis
 * dans toutes les langues souhait�es par l'interm�diaire du fichier
 * ebli_'langage'.txt du package org.fudaa.ebli.ressource.
 *
 * @version      $Id: BFileChooser.java,v 1.9 2006-04-12 15:28:04 deniger Exp $
 * @author       Bertrand Marchand
 */
public class BFileChooser extends JFileChooser {

  /**
   * Pour initialiser les chaines � chaque cr�ation de dialogue.
   */
  //  private boolean init= init();
  /**
   * Initialisation des chaines de caract�res. Les noms d'accessibilit� ne sont
   * pas red�finis.
   */
  /*private boolean init(){
    // Basic L&F
    UIManager.put("FileChooser.fileDescriptionText", EbliResource.EBLI
        .getString("Fichier g�n�rique"));
    //=Generic File
    UIManager
        .put("FileChooser.directoryDescriptionText", EbliResource.EBLI.getString("R�pertoire"));
    //=Directory
    UIManager.put("FileChooser.newFolderErrorText", EbliResource.EBLI
        .getString("Erreur de cr�ation d'un nouveau dossier"));
    // Error creating new folder
    UIManager.put("FileChooser.acceptAllFileFilterText", EbliResource.EBLI
        .getString("Tout fichier (*.*)"));
    //=All Files (*.*)
    UIManager.put("FileChooser.cancelButtonText", EbliResource.EBLI.getString("Annuler"));
    //=Cancel
    UIManager.put("FileChooser.saveButtonText", EbliResource.EBLI.getString("Enregistrer"));
    //=Save
    UIManager.put("FileChooser.openButtonText", EbliResource.EBLI.getString("Ouvrir"));
    //=Open
    UIManager.put("FileChooser.updateButtonText", EbliResource.EBLI.getString("Rafraichir"));
    //=Update
    UIManager.put("FileChooser.helpButtonText", EbliResource.EBLI.getString("Aide"));
    //=Help
    UIManager.put("FileChooser.cancelButtonToolTipText", EbliResource.EBLI
        .getString("Annuler le dialogue"));
    //=Abort file chooser dialog
    UIManager.put("FileChooser.saveButtonToolTipText", EbliResource.EBLI
        .getString("Enregister le fichier s�lectionn�"));
    //=Save selected file
    UIManager.put("FileChooser.openButtonToolTipText", EbliResource.EBLI
        .getString("Ouvrir le fichier s�lectionn�"));
    //=Open selected file
    UIManager.put("FileChooser.updateButtonToolTipText", EbliResource.EBLI
        .getString("Rafraichir la liste des r�pertoires"));
    //=Update directory listing
    UIManager.put("FileChooser.helpButtonToolTipText", EbliResource.EBLI.getString("Aide"));
    // =FileChooser help
    // Motif L&F
    if (UIManager.getLookAndFeel().getName().equals("CDE/Motif")) {
      UIManager.put("FileChooser.acceptAllFileFilterText", "*");
      UIManager.put("FileChooser.pathLabelText", EbliResource.EBLI
          .getString("Chemin ou nom de dossier:"));
      //=Enter path or folder name:
      UIManager.put("FileChooser.filterLabelText", EbliResource.EBLI.getString("Filtre"));
      //=Filter
      UIManager.put("FileChooser.foldersLabelText", EbliResource.EBLI.getString("Dossiers"));
      //=Folders
      UIManager.put("FileChooser.filesLabelText", EbliResource.EBLI.getString("Fichiers"));
      //=Files
      UIManager.put("FileChooser.enterFileNameLabelText", EbliResource.EBLI
          .getString("Nom du fichier:"));
      //=Enter file name:
    }
    // Metal et Windows L&F
    else {
      UIManager.put("FileChooser.lookInLabelText", EbliResource.EBLI.getString("Rechercher dans:"));
      //= Look in:
      UIManager
          .put("FileChooser.fileNameLabelText", EbliResource.EBLI.getString("Nom de fichier:"));
      //=File name:
      UIManager.put("FileChooser.filesOfTypeLabelText", EbliResource.EBLI.getString("Type:"));
      //=Files of type:
      UIManager.put("FileChooser.upFolderToolTipText", EbliResource.EBLI
          .getString("Remonter d'un niveau"));
      //=Up One Level
      UIManager.put("FileChooser.homeFolderToolTipText", EbliResource.EBLI
          .getString("R�pertoire utilisateur"));
      //=Home
      UIManager.put("FileChooser.newFolderToolTipText", EbliResource.EBLI
          .getString("Cr�er un nouveau dossier"));
      //=Create New Folder
      UIManager.put("FileChooser.listViewButtonToolTipText", EbliResource.EBLI.getString("Liste"));
      //=List
      UIManager.put("FileChooser.detailsViewButtonToolTipText", EbliResource.EBLI
          .getString("Liste d�taill�e"));
      //=Details
    }
    this.updateUI();
    return true;
  }*/
}
