/*
 * @creation     2000-01-12
 * @modification $Date: 2006-04-12 15:28:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.dialog;

import java.awt.Component;
import java.awt.Container;
import java.beans.PropertyChangeListener;

/**
 * @version $Id: IDialogInterface.java,v 1.8 2006-04-12 15:28:03 deniger Exp $
 * @author Axel von Arnim
 */
public interface IDialogInterface {
  int DIALOG = 0;
  int INTERNAL = 1;

  void realDispose(boolean _recurse);

  // dispose envoie en fait un propertyChange de changement de type de dialog
  // et ne ferme pas la fenetre
  void dispose();

  void show();

  boolean isShowing();

  void setName(String _n);

  void setModal(boolean _m);

  void setContentPane(Container _c);

  Container getContentPane();

  void setResizable(boolean _b);

  void setClosable(boolean _b);

  void pack();

  void setTitle(String _t);

  void setLocation(int _x, int _y);

  void setSize(int _w, int _h);

  IDialogInterface[] getChildrenDialogs();

  void addChildDialog(IDialogInterface _child);

  void removeChildDialog(IDialogInterface _child);

  void addPropertyChangeListener(PropertyChangeListener _l);

  void removePropertyChangeListener(PropertyChangeListener _l);

  void setDefaultCloseOperation(int _o);

  void setLocationRelativeTo(Component _c);

  Component getComponent();
}
