/*
 * @creation     1999-04-08
 * @modification $Date: 2006-09-19 14:55:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.dialog;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuIcon;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import java.awt.Component;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JComboBox;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

/**
 * @version $Id: BPanneauEditorAction.java,v 1.9 2006-09-19 14:55:56 deniger Exp $
 * @author Axel von Arnim
 */
public class BPanneauEditorAction extends BuPanel {
  public final static int AUCUN = 0;
  public final static int SUPPRIMER = 1 << 1;
  public final static int CREER = 1 << 2;
  public final static int EDITER = 1 << 3;
  public final static int VOIR = 1 << 4;
  public final static int IMPORTER = 1 << 5;
  public final static int EXPORTER = 1 << 6;
  public final static int RESET = 1 << 7;
  BuButton btSupprimer_;
  BuButton btCreer_;
  BuButton btEditer_;
  BuButton btVoir_;
  BuButton btImport_;
  BuButton btExport_;
  BuButton btReset_;
  List btCustom_;
  JComboBox lsCustom_;
  Map listActions_;
  List listeners_;

  public BPanneauEditorAction() {
    this(0);
  }

  public BPanneauEditorAction(final int _mode) {
    btSupprimer_ = null;
    btCreer_ = null;
    btEditer_ = null;
    btVoir_ = null;
    btImport_ = null;
    btExport_ = null;
    btCustom_ = new ArrayList();
    lsCustom_ = null;
    listActions_ = null;
    listeners_ = new ArrayList();
    setBorder(new CompoundBorder(new EmptyBorder(new Insets(0, BDialogContent.INSETS_SIZE, 0,
        BDialogContent.INSETS_SIZE)), new EtchedBorder()));
    configure(_mode);
  }

  public final void configure(final int _mode) {
    configureCreer(_mode);
    configureSuppr(_mode);
    configureEdit(_mode);
    configureVoir(_mode);
    configureImport(_mode);
    configureExport(_mode);
    configureReset(_mode);
  }

  private void configureReset(final int _mode) {
    if ((_mode & RESET) == 0) {
      if (btReset_ != null) {
        remove(btReset_);
      }
      btReset_ = null;
    } else {
      if (btReset_ == null) {
        createBtReset();
      }
    }
  }

  private void configureExport(final int _mode) {
    if ((_mode & EXPORTER) == 0) {
      if (btExport_ != null) {
        remove(btExport_);
      }
      btExport_ = null;
    } else {
      if (btExport_ == null) {
        createBtExport();
      }
    }
  }

  private void configureImport(final int _mode) {
    if ((_mode & IMPORTER) == 0) {
      if (btImport_ != null) {
        remove(btImport_);
      }
      btImport_ = null;
    } else {
      if (btImport_ == null) {
        createBtImport();
      }
    }
  }

  private void configureVoir(final int _mode) {
    if ((_mode & VOIR) == 0) {
      if (btVoir_ != null) {
        remove(btVoir_);
      }
      btVoir_ = null;
    } else {
      if (btVoir_ == null) {
        createBtVoir();
      }
    }
  }

  private void configureEdit(final int _mode) {
    if ((_mode & EDITER) == 0) {
      if (btEditer_ != null) {
        remove(btEditer_);
      }
      btEditer_ = null;
    } else {
      if (btEditer_ == null) {
        createBtEditer();
      }
    }
  }

  private void configureSuppr(final int _mode) {
    if ((_mode & SUPPRIMER) == 0) {
      if (btSupprimer_ != null) {
        remove(btSupprimer_);
      }
      btSupprimer_ = null;
    } else {
      if (btSupprimer_ == null) {
        createBtSuppr();
      }
    }
  }

  private void configureCreer(final int _mode) {
    if ((_mode & CREER) == 0) {
      if (btCreer_ != null) {
        remove(btCreer_);
      }
      btCreer_ = null;
    } else {
      if (btCreer_ == null) {
        createBtCreer();
      }
    }
  }

  private void createBtReset() {
    btReset_ = new BuButton();
    btReset_.setText(BuResource.BU.getString("R�initialiser"));
    btReset_.setIcon(BuResource.BU.getIcon("reinitialiser"));
    btReset_.setName("btRESET");
    btReset_.setActionCommand("RESET");
    add(btReset_);
  }

  private void createBtExport() {
    btExport_ = new BuButton();
    btExport_.setText(BuResource.BU.getString("Exporter"));
    btExport_.setIcon(BuResource.BU.getIcon("exporter"));
    btExport_.setName("btEXPORTER");
    btExport_.setActionCommand("EXPORTER");
    add(btExport_);
  }

  private void createBtImport() {
    btImport_ = new BuButton();
    btImport_.setText(BuResource.BU.getString("Importer"));
    btImport_.setIcon(BuResource.BU.getIcon("importer"));
    btImport_.setName("btIMPORTER");
    btImport_.setActionCommand("IMPORTER");
    add(btImport_);
  }

  private void createBtVoir() {
    btVoir_ = new BuButton();
    btVoir_.setText(BuResource.BU.getString("Voir"));
    btVoir_.setIcon(BuResource.BU.getIcon("voir"));
    btVoir_.setName("btVOIR");
    btVoir_.setActionCommand("VOIR");
    add(btVoir_);
  }

  private void createBtEditer() {
    btEditer_ = new BuButton();
    btEditer_.setText(BuResource.BU.getString("Editer"));
    btEditer_.setIcon(BuResource.BU.getIcon("editer"));
    btEditer_.setName("btEDITER");
    btEditer_.setActionCommand("EDITER");
    add(btEditer_);
  }

  private void createBtSuppr() {
    btSupprimer_ = new BuButton();
    btSupprimer_.setText(BuResource.BU.getString("Supprimer"));
    btSupprimer_.setIcon(BuResource.BU.getIcon("detruire"));
    btSupprimer_.setName("btSUPPRIMER");
    btSupprimer_.setActionCommand("SUPPRIMER");
    add(btSupprimer_);
  }

  private void createBtCreer() {
    btCreer_ = new BuButton();
    btCreer_.setText(BuResource.BU.getString("Cr�er"));
    btCreer_.setIcon(BuResource.BU.getIcon("creer"));
    btCreer_.setName("btCREER");
    btCreer_.setActionCommand("CREER");
    add(btCreer_);
  }

  public BuButton addCustomButton(final String _label, final BuIcon _icon, final String _cmd) {
    final BuButton btNew = new BuButton(_icon, _label);
    btNew.setActionCommand(_cmd);
    btNew.setName("bt" + _cmd);
    add(btNew);
    btCustom_.add(btNew);
    for (int i = 0; i < listeners_.size(); i++) {
      btNew.addActionListener((ActionListener) listeners_.get(i));
    }
    return btNew;
  }

  public BuButton addCustomButton(final String _label, final String _cmd) {
    return addCustomButton(_label, null, _cmd);
  }
  
  public BuButton getCustomAction(final String _cmd) {
    final Component[] comps = getComponents();
    for (int i = 0; i < comps.length; i++) {
      BuButton bt=(BuButton)comps[i];
      if (bt.getActionCommand().equals(_cmd)) {
        return bt;
      }
    }
    
    return null;
  }

  public boolean containsCustomButton(final String _cmd) {
    final Component[] comps = getComponents();
    for (int i = 0; i < comps.length; i++) {
      if (((BuButton) comps[i]).getActionCommand().equals(_cmd)) {
        return true;
      }
    }
    return false;
  }

  public void removeCustomButton(final String _cmd) {
    final Component[] comps = getComponents();
    for (int i = 0; i < comps.length; i++) {
      if (((BuButton) comps[i]).getActionCommand().equals(_cmd)) {
        remove(comps[i]);
        btCustom_.remove(comps[i]);
      }
    }
  }

  public void removeAllCustomButton() {
    final Component[] comps = getComponents();
    for (int i = 0; i < comps.length; i++) {
      remove(comps[i]);
      btCustom_.remove(comps[i]);
    }
  }

  public void addActionList(final String[][] _list) {
    if (_list == null) {
      return;
    }
    if (lsCustom_ == null) {
      listActions_ = new HashMap();
      lsCustom_ = new JComboBox();
      add(lsCustom_);
    }
    for (int i = 0; i < _list.length; i++) {
      listActions_.put(_list[i][0], _list[i][1]);
      lsCustom_.addItem(_list[i][0]);
    }
  }

  @Override
  public void setName(final String _n) {
    if (btSupprimer_ != null) {
      btSupprimer_.setName(_n + ":SUPPRIMER");
    }
    if (btCreer_ != null) {
      btCreer_.setName(_n + ":CREER");
    }
    if (btEditer_ != null) {
      btEditer_.setName(_n + ":EDITER");
    }
    if (btVoir_ != null) {
      btVoir_.setName(_n + ":VOIR");
    }
    if (btImport_ != null) {
      btImport_.setName(_n + ":IMPORTER");
    }
    if (btExport_ != null) {
      btExport_.setName(_n + ":EXPORTER");
    }
    if (btReset_ != null) {
      btReset_.setName(_n + ":RESET");
    }
    for (int i = 0; i < btCustom_.size(); i++) {
      ((BuButton) btCustom_.get(i)).setName(_n + ":CUSTOM" + i);
    }
    if (lsCustom_ != null) {
      lsCustom_.setName(_n + ":LIST");
    }
    super.setName(_n);
  }

  public String getSelectedActionList() {
    if (lsCustom_ == null) {
      return null;
    }
    return (String) listActions_.get(lsCustom_.getSelectedItem());
  }

  public void addActionListener(final ActionListener _l) {
    if (btSupprimer_ != null) {
      btSupprimer_.addActionListener(_l);
    }
    if (btCreer_ != null) {
      btCreer_.addActionListener(_l);
    }
    if (btEditer_ != null) {
      btEditer_.addActionListener(_l);
    }
    if (btVoir_ != null) {
      btVoir_.addActionListener(_l);
    }
    if (btImport_ != null) {
      btImport_.addActionListener(_l);
    }
    if (btExport_ != null) {
      btExport_.addActionListener(_l);
    }
    if (btReset_ != null) {
      btReset_.addActionListener(_l);
    }
    for (int i = 0; i < btCustom_.size(); i++) {
      ((BuButton) btCustom_.get(i)).addActionListener(_l);
    }
    listeners_.add(_l);
  }

  public void removeActionListener(final ActionListener _l) {
    if (btSupprimer_ != null) {
      btSupprimer_.removeActionListener(_l);
    }
    if (btCreer_ != null) {
      btCreer_.removeActionListener(_l);
    }
    if (btEditer_ != null) {
      btEditer_.removeActionListener(_l);
    }
    if (btVoir_ != null) {
      btVoir_.removeActionListener(_l);
    }
    if (btImport_ != null) {
      btImport_.removeActionListener(_l);
    }
    if (btExport_ != null) {
      btExport_.removeActionListener(_l);
    }
    if (btReset_ != null) {
      btReset_.removeActionListener(_l);
    }
    for (int i = 0; i < btCustom_.size(); i++) {
      ((BuButton) btCustom_.get(i)).removeActionListener(_l);
    }
    listeners_.remove(_l);
  }

  public void setEnabledAction(final int _action, final boolean _editable) {
    if ((_action & SUPPRIMER) != 0) {
      btSupprimer_.setEnabled(_editable);
    }
    if ((_action & CREER) != 0) {
      btCreer_.setEnabled(_editable);
    }
    if ((_action & EDITER) != 0) {
      btEditer_.setEnabled(_editable);
    }
    if ((_action & VOIR) != 0) {
      btVoir_.setEnabled(_editable);
    }
    if ((_action & IMPORTER) != 0) {
      btImport_.setEnabled(_editable);
    }
    if ((_action & EXPORTER) != 0) {
      btExport_.setEnabled(_editable);
    }
    if ((_action & RESET) != 0) {
      btReset_.setEnabled(_editable);
    }
  }
}
