/*
 * @creation     2000-01-12
 * @modification $Date: 2007-03-27 16:10:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.dialog;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.WindowConstants;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.impression.EbliPageFormat;
import org.fudaa.ebli.impression.EbliPageable;

/**
 * @version $Id: BInternalDialog.java,v 1.23 2007-03-27 16:10:52 deniger Exp $
 * @author Axel von Arnim
 */
public class BInternalDialog extends BuInternalFrame implements IDialogInterface, EbliPageable, DropTargetListener,
        BuCutCopyPasteInterface, BuUndoRedoInterface, BuSelectFindReplaceInterface, CtuluImageProducer {

  List children_;
  IDialogInterface parent_;
  boolean modal_;
  JComponent[] tools_;
  BuCommonInterface appli_;

  public BInternalDialog(final BuCommonInterface _app) {
    super();
    appli_ = _app;
    children_ = new ArrayList();
    // addMouseListener(this);
    // addMouseMotionListener(this);
    new DropTarget(this, DnDConstants.ACTION_COPY, this);
    parent_ = null;
    // modal_= false;
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    /*
     * BuButton btPop=new BuButton("Pop"); btPop.addActionListener(new ActionListener() { public void
     * actionPerformed(ActionEvent e) { dispose(true); } }); tools_=new JComponent[] { btPop };
     */
    tools_ = new JComponent[0];
    addInternalFrameListener(new InternalFrameAdapter() {
      @Override
      public void internalFrameActivated(final InternalFrameEvent _e) {
        dialogSelected();
      }
    });

    initIcon(_app);
  }

  protected void initIcon(final BuCommonInterface _app) {

    try {
      setFrameIcon(_app.getImplementation().getInformationsSoftware().logo);
    } catch (Exception e) {
    }
  }

  public BInternalDialog(final BuCommonInterface _app, final IDialogInterface _parent) {
    this(_app);
    if (_parent != null) {
      parent_ = _parent;
      parent_.addChildDialog(this);
    }
    initIcon(_app);
  }

  private boolean isContentPaneEbliPageable() {
    return (getContentPane() instanceof EbliPageable);
  }

  private EbliPageable getContentPaneEbliPageable() {
    if (isContentPaneEbliPageable()) {
      return (EbliPageable) getContentPane();
    }
    return null;
  }

  private boolean isContentPaneUndoRedo() {
    return (getContentPane() instanceof BuUndoRedoInterface);
  }

  private BuUndoRedoInterface getContentPaneUndoRedo() {
    if (isContentPaneUndoRedo()) {
      return (BuUndoRedoInterface) getContentPane();
    }
    return null;
  }

  private boolean isContentPaneSelectFindReplace() {
    return (getContentPane() instanceof BuSelectFindReplaceInterface);
  }

  private BuSelectFindReplaceInterface getContentPaneSelectFindReplace() {
    if (isContentPaneSelectFindReplace()) {
      return (BuSelectFindReplaceInterface) getContentPane();
    }
    return null;
  }

  private boolean isContentPaneCutCopyPaste() {
    return (getContentPane() instanceof BuCutCopyPasteInterface);
  }

  private BuCutCopyPasteInterface getContentPaneCutCopyPaste() {
    if (isContentPaneCutCopyPaste()) {
      return (BuCutCopyPasteInterface) getContentPane();
    }
    return null;
  }

  private boolean isContentPaneImageProducer() {
    return (getContentPane() instanceof CtuluImageProducer);
  }

  private CtuluImageProducer getContentPaneImageProducer() {
    if (isContentPaneImageProducer()) {
      return (CtuluImageProducer) getContentPane();
    }
    return null;
  }

  @Override
  public synchronized void addChildDialog(final IDialogInterface _child) {
    children_.add(_child);
  }

  @Override
  public synchronized void removeChildDialog(final IDialogInterface _child) {
    children_.remove(_child);
  }

  @Override
  public void dispose() {
    /*
     * Component cp=getContentPane(); if( cp instanceof BDialogContent ) ((BDialogContent)cp).fermer(); else
     */
    realDispose(true);
  }

  @Override
  public IDialogInterface[] getChildrenDialogs() {
    final IDialogInterface[] res = new IDialogInterface[children_.size()];
    for (int i = 0; i < res.length; i++) {
      res[i] = (IDialogInterface) children_.get(i);
    }
    return res;
  }

  @Override
  public void realDispose(final boolean _recurse) {
    final List c = new ArrayList(children_);
    for (int i = 0; i < c.size(); i++) {
      final IDialogInterface child = (IDialogInterface) c.get(i);
      if (_recurse) {
        child.realDispose(true);
      } else {
        removeChildDialog(child);
      }
    }
    if (parent_ != null) {
      parent_.removeChildDialog(this);
    }
    setModal(false);
    // super.setVisible(false);
    try {
      setClosed(true);
    } catch (final java.beans.PropertyVetoException e) {
    }
    if (appli_ == null) {
      FuLog.warning("EBD: BInternalDialog: null app");
    } else {
      appli_.getImplementation().removeInternalFrame(this);
      removeInternalFrameListener(appli_.getImplementation());
    }
  }

  @Override
  public Component getComponent() {
    return this;
  }

  @Override
  public void setLocationRelativeTo(final Component _c) {
    final Point cloc = _c.getLocationOnScreen();
    final Dimension cdim = _c.getSize();
    final Point loc = new Point();
    final Dimension dim = getPreferredSize();
    loc.x = cloc.x + cdim.width / 2 - dim.width / 2;
    loc.y = cloc.y + cdim.height / 2 - dim.height / 2;
    setBounds(loc.x, loc.y, dim.width, dim.height);
  }

  @Override
  public void show() {
    if (appli_ == null) {
      FuLog.warning("EBD: BInternalDialog: null app");
    } else {
      final BuDesktop dk = appli_.getMainPanel().getDesktop();
      if ((appli_.getFrame() != null) && (!appli_.getFrame().isAncestorOf(this))) {
        // addInternalFrameListener(appli_.getImplementation());
        /* dk. */
        appli_.getImplementation().addInternalFrame(this);
      } else {
        dk.checkInternalFrame(this);
      }
    }
    super.show();
  }

  @Override
  public void setModal(final boolean _m) {
    modal_ = _m;
  }

  public boolean isModal() {
    return modal_;
  }

  @Override
  public JComponent[] getSpecificTools() {
    return tools_;
  }

  @Override
  public String[] getEnabledActions() {
    ArrayList vactions = new ArrayList();
    if (isContentPaneEbliPageable()) {
      vactions.addAll(Arrays.asList(new String[]{"IMPRIMER", "PREVISUALISER", "MISEENPAGE"}));
    }
    if (isContentPaneCutCopyPaste()) {
      vactions.addAll(Arrays.asList(new String[]{"COUPER", "COPIER", "COLLER"}));
    }
    if (isContentPaneUndoRedo()) {
      vactions.addAll(Arrays.asList(new String[]{"DEFAIRE", "REFAIRE"}));
    }
    if (isContentPaneSelectFindReplace()) {
      vactions.addAll(Arrays.asList(new String[]{"TOUTSELECTIONNER", "RECHERCHER", "REMPLACER"}));
    }
    return (String[]) vactions.toArray(new String[0]);
  }

  protected void dialogSelected() {
    firePropertyChange("dialogSelected", null, null);
  }

  /*
   * public void print(PrintJob _job, Graphics _g) { if( getContentPane() instanceof BuPrintable )
   * ((BuPrintable)getContentPane()).print(_job,_g); }
   */
  @Override
  public int print(final Graphics _g, final PageFormat _format, final int _numPage) // throws PrinterException
  {
    if (isContentPaneEbliPageable()) {
      return getContentPaneEbliPageable().print(_g, _format, _numPage);
    }
    FuLog.warning("ContentPane non imprimable");
    return Printable.NO_SUCH_PAGE;
  }

  @Override
  public BuInformationsDocument getInformationsDocument() {
    if (isContentPaneEbliPageable()) {
      return getContentPaneEbliPageable().getInformationsDocument();
    }
    return null;
  }

  @Override
  public BuInformationsSoftware getInformationsSoftware() {
    if (isContentPaneEbliPageable()) {
      return getContentPaneEbliPageable().getInformationsSoftware();
    }
    return null;
  }

  @Override
  public Printable getPrintable(final int _i) {
    if (isContentPaneEbliPageable()) {
      return getContentPaneEbliPageable().getPrintable(_i);
    }
    return null;
  }

  @Override
  public PageFormat getPageFormat(final int _i) {
    if (isContentPaneEbliPageable()) {
      return getContentPaneEbliPageable().getPageFormat(_i);
    }
    return null;
  }

  @Override
  public EbliPageFormat getDefaultEbliPageFormat() {
    if (isContentPaneEbliPageable()) {
      return getContentPaneEbliPageable().getDefaultEbliPageFormat();
    }
    return null;
  }

  @Override
  public int getNumberOfPages() {
    if (isContentPaneEbliPageable()) {
      return getContentPaneEbliPageable().getNumberOfPages();
    }
    return 0;
  }

  // Implementation de BuCutCopyPasteInterface
  @Override
  public void cut() {
    if (isContentPaneCutCopyPaste()) {
      getContentPaneCutCopyPaste().cut();
    }
  }

  @Override
  public void copy() {
    if (isContentPaneCutCopyPaste()) {
      getContentPaneCutCopyPaste().copy();
    }
  }

  @Override
  public void paste() {
    if (isContentPaneCutCopyPaste()) {
      getContentPaneCutCopyPaste().paste();
    }
  }

  @Override
  public void duplicate() {
    if (isContentPaneCutCopyPaste()) {
      getContentPaneCutCopyPaste().duplicate();
    }
  }

  // Implementation de BuUndoRedoInterface
  @Override
  public void undo() {
    if (isContentPaneUndoRedo()) {
      getContentPaneUndoRedo().undo();
    }
  }

  @Override
  public void redo() {
    if (isContentPaneUndoRedo()) {
      getContentPaneUndoRedo().redo();
    }
  }

  @Override
  public void select() {
    if (isContentPaneSelectFindReplace()) {
      getContentPaneSelectFindReplace().select();
    }
  }

  @Override
  public void find() {
    if (isContentPaneSelectFindReplace()) {
      getContentPaneSelectFindReplace().find();
    }
  }

  @Override
  public void replace() {
    if (isContentPaneSelectFindReplace()) {
      getContentPaneSelectFindReplace().replace();
    }
  }

  // Implementation de CtuluImageProducer
  /**
   * @return l'image produite dans la taille courante du composant.
   */
  @Override
  public BufferedImage produceImage(Map _params) {
    if (isContentPaneImageProducer()) {
      return getContentPaneImageProducer().produceImage(_params);
    }
    return CtuluLibImage.produceImageForComponent(this, _params);
    /* } */
  }

  @Override
  public BufferedImage produceImage(int _w, int _h, Map _params) {
    if (isContentPaneImageProducer()) {
      return getContentPaneImageProducer().produceImage(_w, _h, _params);
    }
    return CtuluLibImage.produceImageForComponent(this, _w, _h, _params);
  }

  @Override
  public Dimension getDefaultImageDimension() {
    if (isContentPaneImageProducer()) {
      return getContentPaneImageProducer().getDefaultImageDimension();
    }
    return getSize();
  }

  @Override
  public void dragEnter(final DropTargetDragEvent _dtde) {
    _dtde.rejectDrag();
    if (FuLog.isDebug()) {
      FuLog.debug("EBD: dndEnter");
    }
    getDesktopPane().getDesktopManager().activateFrame(this);
    try {
      setSelected(true);
    } catch (final java.beans.PropertyVetoException ex) {
    }
  }

  @Override
  public void dragExit(final DropTargetEvent _dte) {
  }

  @Override
  public void dragOver(final DropTargetDragEvent _dtde) {
  }

  @Override
  public void drop(final DropTargetDropEvent _dtde) {
    if (FuLog.isDebug()) {
      FuLog.debug("EBD: dndDrop");
    }
    getDesktopPane().getDesktopManager().deactivateFrame(this);
    try {
      setSelected(false);
    } catch (final java.beans.PropertyVetoException ex) {
    }
  }

  @Override
  public void dropActionChanged(final DropTargetDragEvent _dtde) {
  }
}
