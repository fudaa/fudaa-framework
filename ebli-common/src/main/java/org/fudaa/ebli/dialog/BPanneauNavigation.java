/*
 * @creation     1999-04-27
 * @modification $Date: 2006-09-19 14:55:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.dialog;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import javax.swing.border.EmptyBorder;
/**
 * @version      $Id: BPanneauNavigation.java,v 1.13 2006-09-19 14:55:56 deniger Exp $
 * @author       Axel von Arnim
 */
public class BPanneauNavigation extends BuPanel {
  public final static int INSETS_SIZE= 5;
  public final static int AUCUN= 0;
  public final static int VALIDER= 1 << 1;
  public final static int FERMER= 1 << 2;
  public final static int RECULER= 1 << 3;
  public final static int AVANCER= 1 << 4;
  public final static int RECULER_VITE= 1 << 5;
  public final static int AVANCER_VITE= 1 << 6;
  public final static int TERMINER= 1 << 7;
  public final static int ANNULER= 1 << 8;
  public final static int ALIGN_LEFT= 1 << 9;
  public final static int ALIGN_RIGHT= 1 << 10;
  public final static int ALIGN_CENTER= 1 << 11;
  BuButton btNavValider_;
  BuButton btNavFermer_;
  BuButton btNavReculer_;
  BuButton btNavAvancer_;
  BuButton btNavReculerVite_;
  BuButton btNavAvancerVite_;
  BuButton btNavTerminer_;
  BuButton btNavAnnuler_;
  BuPanel pnNav_;
  public BPanneauNavigation() {
    this(ALIGN_LEFT);
  }
  public BPanneauNavigation(final int _mode) {
    btNavValider_= null;
    btNavFermer_= null;
    btNavReculer_= null;
    btNavAvancer_= null;
    btNavTerminer_= null;
    btNavAnnuler_= null;
    pnNav_= new BuPanel();
    setLayout(new BorderLayout());
    setBorder(new EmptyBorder(new Insets(0, 0, INSETS_SIZE, 0)));
    if ((_mode & RECULER_VITE) != 0) {
      btNavReculerVite_= new BuButton();
      btNavReculerVite_.setText(BuResource.BU.getString("Reculer vite"));
      btNavReculerVite_.setIcon(BuResource.BU.getIcon("reculervite"));
      btNavReculerVite_.setName("btNAVRECULERVITE");
      btNavReculerVite_.setActionCommand("RECULERVITE");
      pnNav_.add(btNavReculerVite_);
    }
    if ((_mode & RECULER) != 0) {
      btNavReculer_= new BuButton();
      btNavReculer_.setText(BuResource.BU.getString("Reculer"));
      btNavReculer_.setIcon(BuResource.BU.getIcon("reculer"));
      btNavReculer_.setName("btNAVRECULER");
      btNavReculer_.setActionCommand("RECULER");
      pnNav_.add(btNavReculer_);
    }
    if ((_mode & AVANCER) != 0) {
      btNavAvancer_= new BuButton();
      btNavAvancer_.setText(BuResource.BU.getString("Avancer"));
      btNavAvancer_.setIcon(BuResource.BU.getIcon("avancer"));
      btNavAvancer_.setName("btNAVAVANCER");
      btNavAvancer_.setActionCommand("AVANCER");
      pnNav_.add(btNavAvancer_);
    }
    if ((_mode & AVANCER_VITE) != 0) {
      btNavAvancerVite_= new BuButton();
      btNavAvancerVite_.setText(BuResource.BU.getString("Avancer vite"));
      btNavAvancerVite_.setIcon(BuResource.BU.getIcon("avancervite"));
      btNavAvancerVite_.setName("btNAVAVANCERVITE");
      btNavAvancerVite_.setActionCommand("AVANCERVITE");
      pnNav_.add(btNavAvancerVite_);
    }
    if ((_mode & VALIDER) != 0) {
      btNavValider_= new BuButton();
      btNavValider_.setText(BuResource.BU.getString("Valider"));
      btNavValider_.setIcon(BuResource.BU.getIcon("valider"));
      btNavValider_.setName("btNAVVALIDER");
      btNavValider_.setActionCommand("VALIDER");
      pnNav_.add(btNavValider_);
    }
    if ((_mode & TERMINER) != 0) {
      btNavTerminer_= new BuButton();
      btNavTerminer_.setText(BuResource.BU.getString("Terminer"));
      btNavTerminer_.setIcon(BuResource.BU.getIcon("terminer"));
      btNavTerminer_.setName("btNAVTERMINER");
      btNavTerminer_.setActionCommand("TERMINER");
      pnNav_.add(btNavTerminer_);
    }
    if ((_mode & FERMER) != 0) {
      btNavFermer_= new BuButton();
      btNavFermer_.setText(BuResource.BU.getString("Fermer"));
      btNavFermer_.setIcon(BuResource.BU.getIcon("fermer"));
      btNavFermer_.setName("btNAVFERMER");
      btNavFermer_.setActionCommand("FERMER");
      pnNav_.add(btNavFermer_);
    }
    if ((_mode & ANNULER) != 0) {
      btNavAnnuler_= new BuButton();
      btNavAnnuler_.setText(BuResource.BU.getString("Annuler"));
      btNavAnnuler_.setIcon(BuResource.BU.getIcon("annuler"));
      btNavAnnuler_.setName("btNAVANNULER");
      btNavAnnuler_.setActionCommand("ANNULER");
      pnNav_.add(btNavAnnuler_);
    }
    // Layout
    if ((_mode & ALIGN_LEFT) == 0) {
      if ((_mode & ALIGN_RIGHT) == 0) {
        add(BorderLayout.CENTER, pnNav_);
      } else {
        add(BorderLayout.EAST, pnNav_);
      }
    } else {
      add(BorderLayout.WEST, pnNav_);
    }
  }
  @Override
  public void setName(final String _n) {
    if (btNavValider_ != null) {
      btNavValider_.setName(_n + ":VALIDER");
    }
    if (btNavFermer_ != null) {
      btNavFermer_.setName(_n + ":FERMER");
    }
    if (btNavReculer_ != null) {
      btNavReculer_.setName(_n + ":RECULER");
    }
    if (btNavAvancer_ != null) {
      btNavAvancer_.setName(_n + ":AVANCER");
    }
    if (btNavReculerVite_ != null) {
      btNavReculerVite_.setName(_n + ":RECULERVITE");
    }
    if (btNavAvancerVite_ != null) {
      btNavAvancerVite_.setName(_n + ":AVANCERVITE");
    }
    if (btNavTerminer_ != null) {
      btNavTerminer_.setName(_n + ":TERMINER");
    }
    if (btNavAnnuler_ != null) {
      btNavAnnuler_.setName(_n + ":ANNULER");
    }
    super.setName(_n);
  }
  public void addActionListener(final ActionListener _l) {
    if (btNavValider_ != null) {
      btNavValider_.addActionListener(_l);
    }
    if (btNavFermer_ != null) {
      btNavFermer_.addActionListener(_l);
    }
    if (btNavReculer_ != null) {
      btNavReculer_.addActionListener(_l);
    }
    if (btNavAvancer_ != null) {
      btNavAvancer_.addActionListener(_l);
    }
    if (btNavReculerVite_ != null) {
      btNavReculerVite_.addActionListener(_l);
    }
    if (btNavAvancerVite_ != null) {
      btNavAvancerVite_.addActionListener(_l);
    }
    if (btNavTerminer_ != null) {
      btNavTerminer_.addActionListener(_l);
    }
    if (btNavAnnuler_ != null) {
      btNavAnnuler_.addActionListener(_l);
    }
  }
  public void removeActionListener(final ActionListener _l) {
    if (btNavValider_ != null) {
      btNavValider_.removeActionListener(_l);
    }
    if (btNavFermer_ != null) {
      btNavFermer_.removeActionListener(_l);
    }
    if (btNavReculer_ != null) {
      btNavReculer_.removeActionListener(_l);
    }
    if (btNavAvancer_ != null) {
      btNavAvancer_.removeActionListener(_l);
    }
    if (btNavReculerVite_ != null) {
      btNavReculerVite_.removeActionListener(_l);
    }
    if (btNavAvancerVite_ != null) {
      btNavAvancerVite_.removeActionListener(_l);
    }
    if (btNavTerminer_ != null) {
      btNavTerminer_.removeActionListener(_l);
    }
    if (btNavAnnuler_ != null) {
      btNavAnnuler_.removeActionListener(_l);
    }
  }
  public BuButton getBtNavValider() {
    return btNavValider_;
  }
  public BuButton getBtNavAnnuler() {
    return btNavAnnuler_;
  }
  public BuButton getBtNavAvancer() {
    return btNavAvancer_;
  }
  public BuButton getBtNavAvancerVite() {
    return btNavAvancerVite_;
  }
  public BuButton getBtNavFermer() {
    return btNavFermer_;
  }
  public BuButton getBtNavReculer() {
    return btNavReculer_;
  }
  public BuButton getBtNavReculerVite() {
    return btNavReculerVite_;
  }
  public BuButton getBtNavTerminer() {
    return btNavTerminer_;
  }
}
