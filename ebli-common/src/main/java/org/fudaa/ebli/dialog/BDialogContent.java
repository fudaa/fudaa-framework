/*
 * @creation     2000-01-12
 * @modification $Date: 2007-02-21 16:33:50 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.dialog;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuIcon;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuPanel;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import org.fudaa.ebli.commun.EbliPreferences;

/**
 * @version $Id: BDialogContent.java,v 1.19 2007-02-21 16:33:50 bmarchan Exp $
 * @author Axel von Arnim
 */
public class BDialogContent extends BuPanel implements ActionListener, PropertyChangeListener, KeyListener {
  public static final int INSETS_SIZE = BPanneauNavigation.INSETS_SIZE;
  // Boutons du bas
  private BPanneauNavigation pnNav_;
  // Boutons du haut
  private BPanneauEditorAction pnAction_;
  // Contient les boutons du bas et du haut en bas de fenetre
  private BuPanel pnCtrl_;
  protected int reponse_;
  private final JComponent message_;
  String title_;
  BDialogContent parent_;
  IDialogInterface dialog_;
  boolean modal_, resizable_, closable_;
  // non prive afin qu'il soit accessible depuis les inner-classes
  // La barre de swith du dialogue.
  JPanel pnSwitchMode_;
  // Le panneau principal du dialogue.
  private Container contentPane_;
  private final BuCommonInterface appli_;
  private boolean buildMode_;

  public BDialogContent(final BuCommonInterface _app, final String _title) {
    super();
    appli_ = _app;
    if (appli_ == null) {
      throw createExForNullDialog();
    }
    title_ = _title;
    parent_ = null;
    message_ = null;
    baseInit();
  }

  private RuntimeException createExForNullDialog() {
    return new RuntimeException("BDialogContent: BuCommonInterface nulle");
  }

  public BDialogContent(final BuCommonInterface _app, final String _title, final JComponent _message) {
    super();
    appli_ = _app;
    if (appli_ == null) {
      throw createExForNullDialog();
    }
    title_ = _title;
    parent_ = null;
    message_ = _message;
    baseInit();
  }

  public BDialogContent(final BuCommonInterface _app, final BDialogContent _parent, final String _title) {
    super();
    appli_ = _app;
    if (appli_ == null) {
      throw createExForNullDialog();
    }
    title_ = _title;
    parent_ = _parent;
    message_ = null;
    baseInit();
  }

  public BDialogContent(final BuCommonInterface _app, final BDialogContent _parent, final String _title,
      final JComponent _message) {
    super();
    appli_ = _app;
    if (appli_ == null) {
      throw createExForNullDialog();
    }
    title_ = _title;
    parent_ = _parent;
    message_ = _message;
    baseInit();
  }

  /**
   * Permet d'afficher un message d'information contenant <code>_message</code>.
   *
   * @param _message le message a afficher. Si nul, le dialogue n'est pas affiche.
   */
  public void showBuMessage(final String _message, final boolean _modale) {
    if (_message == null) {
      return;
    }
    final BuDialogMessage message = new BuDialogMessage(appli_, getInformationsSoftware(), _message);
    final Point p = getLocation();
    if (p != null) {
      message.setLocation(p);
    }
    message.setModal(_modale);
    message.activate();
  }

  /**
   * Permet d'afficher un message d'erreur contenant <code>_message</code>.
   *
   * @param _message le message a afficher. Si nul, le dialogue n'est pas affiche.
   */
  public void showBuError(final String _message, final boolean _modale) {
    if (_message == null) {
      return;
    }
    final BuDialogError message = new BuDialogError(appli_, getInformationsSoftware(), _message);
    message.setModal(_modale);
    final Point p = getLocation();
    if (p != null) {
      message.setLocation(p);
    }
    message.activate();
  }

  /*
   * public BuCommonInterface getBuCommonInterface() { return appli_;}
   */
  public BuInformationsSoftware getInformationsSoftware() {
    return appli_.getInformationsSoftware();
  }

  protected void baseInit() {
    // registerKeyboardAction(this, "DIALOGTYPE",
    // KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.ALT_MASK),
    // WHEN_IN_FOCUSED_WINDOW);
    modal_ = false;
    resizable_ = true;
    closable_ = true;
    this.setLayout(new BorderLayout());
    final BorderLayout mlo = new BorderLayout();
    contentPane_ = new JPanel();
    final JComponent mct = (JComponent) contentPane_;
    mct.setLayout(mlo);
    pnSwitchMode_ = new JPanel();
    pnSwitchMode_.setPreferredSize(new Dimension(10, 10));
    pnSwitchMode_.setBorder(new EtchedBorder());
    pnSwitchMode_.addMouseListener(new MouseAdapter() {
      private Color bgcolor_;

      @Override
      public void mousePressed(final MouseEvent _e) {
        bgcolor_ = pnSwitchMode_.getBackground();
        pnSwitchMode_.setBackground(bgcolor_.darker());
      }

      @Override
      public void mouseClicked(final MouseEvent _e) {
        changeDialogType();
      }

      @Override
      public void mouseReleased(final MouseEvent _e) {
        pnSwitchMode_.setBackground(bgcolor_);
      }
    });
    super.add(BorderLayout.NORTH, pnSwitchMode_);
    if (message_ != null) {
      final BorderLayout layout = new BorderLayout();
      layout.setHgap(5);
      layout.setVgap(5);
      final JPanel content = new JPanel();
      content.setBorder(new EmptyBorder(INSETS_SIZE, INSETS_SIZE, INSETS_SIZE, INSETS_SIZE));
      content.setLayout(layout);
      final JScrollPane spm = new JScrollPane(message_);
      final int h = message_.getFont().getSize();
      spm.getHorizontalScrollBar().setUnitIncrement(150);
      spm.getVerticalScrollBar().setUnitIncrement(h + 1);
      content.add(BorderLayout.CENTER, spm);
      mct.add(BorderLayout.CENTER, content);
    }
    super.add(BorderLayout.CENTER, mct);
    reponse_ = JOptionPane.CANCEL_OPTION;
    pnNav_ = null;
    pnAction_ = null;
    pnCtrl_ = new BuPanel();
    pnCtrl_.setLayout(new BorderLayout());
    super.add(pnCtrl_,BorderLayout.SOUTH);
    if (EbliPreferences.DIALOG.TYPE == EbliPreferences.DIALOG.INTERNAL) {
      setToInternalDialog();
    } else {
      setToDialog();
    }
  }

  public void setToDialog() {
    if (dialog_ != null) {
      ((Component) dialog_).removeKeyListener(this);
      removeKeyListener(this);
      ((Component) dialog_).removePropertyChangeListener(this);
    }
    if (parent_ == null) {
      dialog_ = new BDialog(appli_);
    } else {
      dialog_ = new BDialog(appli_, parent_.getDialog());
    }
    dialogInit();
  }

  private void dialogInit() {
    dialog_.setTitle(title_);
    dialog_.setContentPane(this);
    dialog_.setModal(modal_);
    dialog_.setResizable(resizable_);
    dialog_.setClosable(closable_);
    dialog_.addPropertyChangeListener(this);
    if (dialog_ instanceof Dialog) {
      ((Component) dialog_).addKeyListener(this);
    } else {
      addKeyListener(this);
    }
  }

  public void setToInternalDialog() {
    if (dialog_ != null) {
      ((Component) dialog_).removeKeyListener(this);
      removeKeyListener(this);
      ((Component) dialog_).removePropertyChangeListener(this);
    }
    if (parent_ == null) {
      dialog_ = new BInternalDialog(appli_);
    } else {
      dialog_ = new BInternalDialog(appli_, parent_.getDialog());
    }
    dialogInit();
  }

  public IDialogInterface getDialog() {
    return dialog_;
  }

  public void setModal(final boolean _m) {
    modal_ = _m;
    if (dialog_ == null) {
      return;
    }
    dialog_.setModal(_m);
  }

  public void setResizable(final boolean _m) {
    resizable_ = _m;
    if (dialog_ == null) {
      return;
    }
    dialog_.setResizable(_m);
  }

  public void setClosable(final boolean _m) {
    closable_ = _m;
    if (dialog_ == null) {
      return;
    }
    dialog_.setClosable(_m);
  }

  public Container getContentPane() {
    return contentPane_;
  }

  public void setContentPane(final Container _c) {
    remove(contentPane_);
    contentPane_ = _c;
    add(BorderLayout.CENTER, contentPane_);
    doLayout();
  }

  @Override
  public Component add(final Component _comp) {
    return contentPane_.add(_comp);
  }

  @Override
  public Component add(final Component _comp, final int _index) {
    return contentPane_.add(_comp, _index);
  }

  @Override
  public void add(final Component _comp, final Object _constraints) {
    contentPane_.add(_comp, _constraints);
  }

  @Override
  public void add(final Component _comp, final Object _constraints, final int _index) {
    contentPane_.add(_comp, _constraints, _index);
  }

  @Override
  public Component add(final String _name, final Component _comp) {
    return contentPane_.add(_name, _comp);
  }

  public void setTitle(final String _t) {
    if (dialog_ == null) {
      return;
    }
    dialog_.setTitle(_t);
  }

  public String getTitle() {
    return title_;
  }

  public void pack() {
    if (dialog_ == null) {
      return;
    }
    dialog_.pack();
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _e) {
    final String name = _e.getPropertyName();
    if ("dialogType".equals(name)) {
      final Double ov = (Double) _e.getOldValue();
      final Double nv = (Double) _e.getNewValue();
      if (ov.equals(nv)) {
        return;
      }
      changeDialogType();
    } else if ("dialogSelected".equals(name)) {
      requestFocus();
    }
  }

  /**
   * Non prive afin que cette methode soit accessible depuis les inner-classes.
   */
  void changeDialogType() {
    final IDialogInterface[] children = dialog_.getChildrenDialogs();
    dialog_.realDispose(false);
    if (dialog_ instanceof BInternalDialog) {
      setToDialog();
    } else {
      setToInternalDialog();
    }
    for (int i = 0; i < children.length; i++) {
      dialog_.addChildDialog(children[i]);
    }
    show();
  }

  public int activate() {
    final Dimension d = getPreferredSize();
    final Dimension ecran = Toolkit.getDefaultToolkit().getScreenSize();
    d.width += 30;
    d.height += 30;
    // if( message_!=null )
    // {
    // if(d.width <450) d.width =450;
    // if(d.width >550) d.width =550;
    // if(d.height<150) d.height=150;
    // if(d.height>200) d.height=200;
    // }
    if (d.width > ecran.width * 3 / 4) {
      d.width = ecran.width * 3 / 4;
    }
    if (d.height > ecran.height * 3 / 4) {
      d.height = ecran.height * 3 / 4;
    }
    dialog_.setSize(d.width, d.height);
    dialog_.setLocation((ecran.width - d.width) / 2, (ecran.height - d.height) / 2);
    dialog_.show();
    return reponse_;
  }

  @Override
  public void show() {
    activate();
  }

  @Override
  public void setName(final String _n) {
    if (pnAction_ != null) {
      pnAction_.setName(_n + "ACT");
    }
    if (pnNav_ != null) {
      pnNav_.setName(_n + "NAV");
    }
    super.setName(_n);
  }

  public void setActionPanel(final int _mode) {
    // if (pnAction_ != null) {
    // pnAction_.configure(mode);
    // } else {
    pnAction_ = new BPanneauEditorAction(_mode);
    pnAction_.addActionListener(this);
    pnCtrl_.add(pnAction_,BorderLayout.NORTH);
    // }
  }

  public void setActionPanel(final BPanneauEditorAction _p) {
    if (pnAction_ != null) {
      pnAction_.removeActionListener(this);
      pnCtrl_.remove(pnAction_);
    }
    pnAction_ = _p;
    if (pnAction_!=null) {
      pnAction_.addActionListener(this);
      pnCtrl_.add(pnAction_,BorderLayout.NORTH);
    }
  }

  public BPanneauEditorAction getActionPanel() {
    return pnAction_;
  }

  public Component addAction(final String _label, final BuIcon _icon, final String _cmd) {
    if (pnAction_ == null) {
      pnAction_ = new BPanneauEditorAction(0);
      pnAction_.addActionListener(this);
      pnCtrl_.add(pnAction_,BorderLayout.NORTH);
    }
    return pnAction_.addCustomButton(_label, _icon, _cmd);
  }

  public Component addAction(final String _label, final String _cmd) {
    return addAction(_label, null, _cmd);
  }

  public boolean containsAction(final String _cmd) {
    if (pnAction_ == null) {
      return false;
    }
    return pnAction_.containsCustomButton(_cmd);
  }

  public void removeAction(final String _cmd) {
    if (pnAction_ == null) {
      return;
    }
    pnAction_.removeCustomButton(_cmd);
  }

  public void addActionList(final String[][] _list) {
    if (pnAction_ == null) {
      return;
    }
    pnAction_.addActionList(_list);
  }

  public String getSelectedActionList() {
    if (pnAction_ == null) {
      return null;
    }
    return pnAction_.getSelectedActionList();
  }

  public void setNavPanel(final int _mode) {
    if (pnNav_ != null) {
      remove(pnNav_);
    }
    pnNav_ = new BPanneauNavigation(_mode);
    pnNav_.addActionListener(this);
    pnCtrl_.add(pnNav_,BorderLayout.CENTER);
  }

  public BPanneauNavigation getNavPanel() {
    return pnNav_;
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    final String cmd = _evt.getActionCommand();
    if ("FERMER".equals(cmd) || "ANNULER".equals(cmd)) {
      fermer();
    } else if (cmd.startsWith("DIALOGTYPE")) {
      // si on n'a pas le focus, le KeyListener ne marche plus, alors
      // on remplace par ici (KeyStroke event)
      if (!hasFocus()) {
        changeDialogType();
      }
    }
  }

  public void fermer() {
    if (dialog_ == null) {
      return;
    }
    reponse_ = JOptionPane.OK_OPTION;
    dialog_.realDispose(true);
  }

  public void addActionListener(final ActionListener _l) {
    if (pnNav_ != null) {
      pnNav_.removeActionListener(this);
      pnNav_.addActionListener(_l);
      pnNav_.addActionListener(this);
    }
    if (pnAction_ != null) {
      pnAction_.removeActionListener(this);
      pnAction_.addActionListener(_l);
      pnAction_.addActionListener(this);
    }
  }

  public void setEnabledAction(final int _action, final boolean _editable) {
    if (pnAction_ != null) {
      pnAction_.setEnabledAction(_action, _editable);
    }
  }

  @Override
  public void keyPressed(final KeyEvent _e) {
    final int mod = _e.getModifiers();
    if (KeyEvent.VK_A == _e.getKeyCode() && (mod & InputEvent.ALT_MASK) == InputEvent.ALT_MASK) {
      changeDialogType();
    }
  }

  @Override
  public void keyReleased(final KeyEvent _e) {}

  @Override
  public void keyTyped(final KeyEvent _e) {}

  /**
   * Returns the buildMode.
   *
   * @return boolean
   */
  public boolean isBuildMode() {
    return buildMode_;
  }

  /**
   * Sets the buildMode.
   *
   * @param _buildMode The buildMode to set
   */
  public void setBuildMode(final boolean _buildMode) {
    buildMode_ = _buildMode;
  }
}
