/*
 * @creation     2000-01-12
 * @modification $Date: 2006-09-19 14:55:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.dialog;

import com.memoire.bu.BuCommonInterface;
import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.WindowConstants;

/**
 * @version $Id: BDialog.java,v 1.13 2006-09-19 14:55:56 deniger Exp $
 * @author Axel von Arnim
 */
public class BDialog extends JDialog implements IDialogInterface {
  List children_;
  IDialogInterface parent_;
  // BuCommonInterface appli_;
  boolean closable_;

  public BDialog(final BuCommonInterface _app) {
    super(_app == null ? null : _app.getFrame());
    children_ = new ArrayList();
    parent_ = null;
    closable_ = true;
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowActivated(final WindowEvent _e) {
        dialogSelected();
      }
    });
  }

  public BDialog(final BuCommonInterface _app, final IDialogInterface _parent) {
    this(_app);
    if (_parent != null) {
      parent_ = _parent;
      parent_.addChildDialog(this);
    }
    closable_ = true;
  }

  @Override
  public synchronized void addChildDialog(final IDialogInterface _child) {
    children_.add(_child);
  }

  @Override
  public synchronized void removeChildDialog(final IDialogInterface _child) {
    children_.remove(_child);
  }

  @Override
  public void setClosable(final boolean _b) {
    closable_ = _b;
  }

  @Override
  public void dispose() {
    if (!closable_) {
      return;
    }
    /*
     * Component cp=getContentPane(); if( cp instanceof BDialogContent ) ((BDialogContent)cp).fermer(); else
     */
    realDispose(true);
  }

  @Override
  public void hide() {
    if (!closable_) {
      return;
    }
    super.hide();
  }

  @Override
  public IDialogInterface[] getChildrenDialogs() {
    final IDialogInterface[] res = new IDialogInterface[children_.size()];
    for (int i = 0; i < res.length; i++) {
      res[i] = (IDialogInterface) children_.get(i);
    }
    return res;
  }

  @Override
  public void realDispose(final boolean _recurse) {
    final ArrayList c = new ArrayList(children_);
    for (int i = 0; i < c.size(); i++) {
      final IDialogInterface child = (IDialogInterface) c.get(i);
      if (_recurse) {
        child.realDispose(true);
      } else {
        removeChildDialog(child);
      }
    }
    if (parent_ != null) {
      parent_.removeChildDialog(this);
    }
    setModal(false);
    // super.setVisible(false);
    super.dispose();
  }

  @Override
  public Component getComponent() {
    return this;
  }

  @Override
  public void show() {
    this.setLocationRelativeTo(getParent());
    super.show();
  }

  @Override
  public boolean isShowing() {
    return super.isShowing();
  }

  protected void dialogSelected() {
    firePropertyChange("dialogSelected", null, null);
  }
}
