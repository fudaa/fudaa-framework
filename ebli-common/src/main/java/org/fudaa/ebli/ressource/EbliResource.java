/*
 * @creation     1998-09-09
 * @modification $Date: 2006-09-19 14:55:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.ressource;

import com.memoire.bu.BuResource;
import com.memoire.fu.FuLib;
import org.fudaa.ctulu.CtuluResource;

/**
 * @version $Id: EbliResource.java,v 1.9 2006-09-19 14:55:56 deniger Exp $
 * @author Axel von Arnim
 */
public class EbliResource extends BuResource {
  public final static EbliResource EBLI = new EbliResource(CtuluResource.CTULU);

  public EbliResource(final BuResource _parent) {
    setParent(_parent);
  }

  public String getString(final String _s, final String _v0) {
    final String r = getString(_s);
    if (r == null) {
      return r;
    }
    return FuLib.replace(r, "{0}", _v0);
  }

  public String getString(final String _s, final String _v0, final String _v1) {
    String r = getString(_s);
    if (r == null) {
      return r;
    }
    r = FuLib.replace(r, "{0}", _v0);
    return FuLib.replace(r, "{1}", _v1);
  }
}
