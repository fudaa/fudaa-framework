/*
 * @creation 8 nov. 06
 * @modification $Date: 2007-05-22 14:19:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuCheckBox;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JComponent;
import javax.swing.JToggleButton;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author fred deniger
 * @version $Id: BSelecteurCheckBox.java,v 1.3 2007-05-22 14:19:05 deniger Exp $
 */
public class BSelecteurCheckBox extends BSelecteurAbstract implements ItemListener {

  JToggleButton cb_;
  public static final String PROP_VISIBLE = "visible";
  public static final String PROP_USER_VISIBLE = "userVisible";

  private boolean inverseResult_;

  public BSelecteurCheckBox(final String _property, final JToggleButton _cb) {
    super(_property);
    cb_ = _cb;
    cb_.addItemListener(this);
    if (_property == PROP_VISIBLE) {
      setTitle(EbliLib.getS("Visible"));
    }
  }

  public BSelecteurCheckBox(final String _property) {
    this(_property, new BuCheckBox());
  }

  public BSelecteurCheckBox(final String _property, final String _titre) {
    this(_property);
    setTitle(_titre);
  }

  @Override
  public void updateFromTarget() {
    if (cb_ != null) {
      cb_.setEnabled(target_ != null);
      cb_.setSelected(inverseResult_ ? !super.getTargetBooleanValue() : super.getTargetBooleanValue());
    }
  }

  @Override
  public JComponent[] getComponents() {
    return new JComponent[] { cb_ };
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    boolean val = cb_ == null ? false : cb_.isSelected();
    if (inverseResult_) val = !val;
    firePropertyChange(getProperty(), Boolean.valueOf(val));

  }

  public JToggleButton getCb() {
    return cb_;
  }

  public boolean isInverseResult() {
    return inverseResult_;
  }

  public void setInverseResult(boolean _inverseResult) {
    inverseResult_ = _inverseResult;
  }

}
