/*
 * @creation 8 nov. 06
 * @modification $Date: 2006-11-14 09:06:31 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import javax.swing.JComponent;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author fred deniger
 * @version $Id: BSelecteurSlider.java,v 1.1 2006-11-14 09:06:31 deniger Exp $
 */
public class BSelecteurSlider extends BSelecteurAbstract implements ChangeListener {

  public final JSlider slider_;

  public BSelecteurSlider(final String _property, final int _min, final int _max) {
    this(_property, new JSlider(_min, _max));
    slider_.setSnapToTicks(true);
  }

  public BSelecteurSlider(final String _property, final JSlider _slider) {
    super(_property);
    slider_ = _slider;
    slider_.getModel().addChangeListener(this);
  }

  protected int getSliderValueForTarget() {
    return slider_.getValue();
  }

  public int getTargetValueForSlide() {
    return getTargetIntValue();
  }

  @Override
  public void updateFromTarget() {
    slider_.setValue(getTargetValueForSlide());
    slider_.setEnabled(target_ != null);
  }


  @Override
  public JComponent[] getComponents() {
    return new JComponent[] { slider_ };
  }


  @Override
  public void stateChanged(final ChangeEvent _e) {
    if (slider_.getValueIsAdjusting()) {
      return;
    }
    firePropertyChange(getProperty(), new Integer(getSliderValueForTarget()));
  }

}
