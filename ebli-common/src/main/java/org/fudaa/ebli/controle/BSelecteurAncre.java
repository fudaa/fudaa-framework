/*
 * @creation 1999-06-11
 * @modification $Date: 2007-05-04 13:49:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.ebli.controle;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JComponent;

/**
 * Un bean de selection du placement d'ancre.
 * 
 * @version $Revision: 1.3 $ $Date: 2007-05-04 13:49:44 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class BSelecteurAncre extends JComponent implements MouseListener {

  // donnees membres publiques
  public final static int UP_LEFT = 0;
  public final static int UP_RIGHT = 1;
  public final static int BOT_LEFT = 2;
  public final static int BOT_RIGHT = 3;
  public final static int MID_LEFT = 4;
  public final static int MID_RIGHT = 5;
  public final static int UP_CENTRE = 6;
  public final static int MID_CENTRE = 7;
  public final static int BOT_CENTRE = 8;

  /**
   * @param _i l'entier a tester
   * @return true si entier valide
   */
  public static boolean isValideAncre(final int _i) {
    return _i >= 0 && _i <= 8;
  }
  // donnees membres privees
  int ancre_;

  public BSelecteurAncre() {
    this(BOT_LEFT);
  }

  public static boolean isAncreBottom(final int _i) {
    return _i == BOT_CENTRE || _i == BOT_LEFT || _i == BOT_RIGHT;
  }

  public BSelecteurAncre(final int _ancre) {
    ancre_ = _ancre;
    setBackground(Color.white);
    setForeground(new Color(255, 210, 255));
    final Dimension d = new Dimension(51, 51);
    setMinimumSize(d);
    setMaximumSize(d);
    setPreferredSize(d);
    addMouseListener(this);
    // addMouseMotionListener(this);
  }

  public int getAncre() {
    return ancre_;
  }

  @Override
  public void paint(final Graphics _g) {
    super.paint(_g);
    _g.setColor(getBackground());
    _g.fillRect(0, 0, 51, 51);
    _g.setColor(getForeground());
    switch (ancre_) {
    case UP_LEFT:
      _g.fillRect(0, 0, 17, 17);
      break;
    case BOT_LEFT:
      _g.fillRect(0, 34, 17, 17);
      break;
    case UP_RIGHT:
      _g.fillRect(34, 0, 17, 17);
      break;
    case BOT_RIGHT:
      _g.fillRect(34, 34, 17, 17);
      break;
    case UP_CENTRE:
      _g.fillRect(17, 0, 17, 17);
      break;
    case BOT_CENTRE:
      _g.fillRect(17, 34, 17, 17);
      break;
    case MID_CENTRE:
      _g.fillRect(17, 17, 17, 17);
      break;
    case MID_LEFT:
      _g.fillRect(0, 17, 17, 17);
      break;
    case MID_RIGHT:
      _g.fillRect(34, 17, 17, 17);
      break;
    default:
    }
  }

  public void doClick(final int _x, final int _y) {
    final int ancrePrec = ancre_;
    if ((_x >= 0) && (_x < 17) && (_y > 0) && (_y < 17)) {
      ancre_ = UP_LEFT;
    } else if ((_x > 34) && (_x < 52) && (_y >= 0) && (_y < 17)) {
      ancre_ = UP_RIGHT;
    } else if ((_x >= 0) && (_x < 17) && (_y > 34) && (_y < 52)) {
      ancre_ = BOT_LEFT;
    } else if ((_x > 34) && (_x < 52) && (_y > 34) && (_y < 52)) {
      ancre_ = BOT_RIGHT;
    } else if ((_x >= 17) && (_x <= 34) && (_y >= 17) && (_y <= 34)) {
      ancre_ = MID_CENTRE;
    } else if ((_x >= 17) && (_x <= 34) && (_y >= 0) && (_y < 17)) {
      ancre_ = UP_CENTRE;
    } else if ((_x >= 17) && (_x <= 34) && (_y > 34) && (_y < 52)) {
      ancre_ = BOT_CENTRE;
    } else if ((_x >= 0) && (_x < 17) && (_y >= 17) && (_y <= 34)) {
      ancre_ = MID_LEFT;
    } else if ((_x > 34) && (_x < 52) && (_y >= 34) && (_y < 52)) {
      ancre_ = MID_RIGHT;
    }
    repaint();
    if (ancrePrec != ancre_) {
      firePropertyChange("ancre", ancrePrec, ancre_);
    }
  }

  @Override
  public void mouseClicked(final MouseEvent _evt) {

  }

  @Override
  public void mousePressed(final MouseEvent _evt) {}

  @Override
  public void mouseReleased(final MouseEvent _evt) {
    doClick(_evt.getX(), _evt.getY());
  }

  @Override
  public void mouseEntered(final MouseEvent _evt) {}

  @Override
  public void mouseExited(final MouseEvent _evt) {}
}
