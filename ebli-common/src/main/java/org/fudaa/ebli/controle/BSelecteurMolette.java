/*
 * @creation 3 ao�t 2004
 * @modification $Date: 2007-05-04 13:49:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.fudaa.ctulu.CtuluLib;

/**
 * @author Fred Deniger
 * @version $Id: BSelecteurMolette.java,v 1.4 2007-05-04 13:49:44 deniger Exp $
 */
public class BSelecteurMolette extends BSelecteurAbstract implements ChangeListener {

  @Override
  public void stateChanged(final ChangeEvent _e) {
    if (target_ == null) {
      return;
    }
    double d = useMinValue_ ? getMinTargetDoubleValue() : getTargetDoubleValue();
    double inc = d * getPourcentage();
    if (molette_.getDirection() == BMolette.MOINS) {
      inc = -inc;
    }
    d += inc;
    firePropertyChange(getProperty(), CtuluLib.getDouble(d));
  }

  double pourc_ = 0.2;

  public double getPourcentage() {
    return pourc_;
  }

  BMolette molette_;
  boolean useMinValue_;

  /**
   * intialise la molette et le bouton d'initialisation.
   */
  public BSelecteurMolette(final String _prop) {
    super(_prop);
    molette_ = new BMolette();
    molette_.setOrientation(BMolette.HORIZONTAL);
    molette_.addChangeListener(this);
    setAddListenerToTarget(false);
  }

  @Override
  public void updateFromTarget() {}

  @Override
  public JComponent[] getComponents() {
    return super.createComponents(molette_);
  }

  public void setPourcentage(final double _pourc) {
    pourc_ = _pourc;
  }

  public boolean isUseMinValue() {
    return useMinValue_;
  }

  public void setUseMinValue(final boolean _useAverageValue) {
    useMinValue_ = _useAverageValue;
  }

}
