/*
 * @creation     1999-02-18
 * @modification $Date: 2006-11-14 09:06:31 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.controle;
import com.memoire.bu.BuVerticalLayout;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Dictionary;
import java.util.Hashtable;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.fudaa.ebli.trace.BParametresGouraud;
/**
 * Un selecteur de parametres Gouraud.
 *
 * @version      $Revision: 1.1 $ $Date: 2006-11-14 09:06:31 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class BSelecteurReduitParametresGouraud
  extends JComponent
  implements ChangeListener, PropertyChangeListener {
  //private BParametresGouraudCustomEditor editeur_;
  private final BParametresGouraud pg_;
  private final JSlider slidTaille_, slidNiveau_;
  public BSelecteurReduitParametresGouraud() {
    super();
    //editeur_=new BParametresGouraudCustomEditor();
    propertyName_= "parametresGouraud";
    pg_= new BParametresGouraud();
    final Dictionary labels= new Hashtable(8);
    labels.put(new Integer(1), new JLabel("1"));
    labels.put(new Integer(2), new JLabel("2"));
    labels.put(new Integer(3), new JLabel("3"));
    labels.put(new Integer(4), new JLabel("4"));
    labels.put(new Integer(5), new JLabel("5"));
    labels.put(new Integer(6), new JLabel("6"));
    labels.put(new Integer(7), new JLabel("7"));
    labels.put(new Integer(8), new JLabel("8"));
    slidTaille_= new JSlider(SwingConstants.HORIZONTAL, 1, 8, 4);
    slidTaille_.setLabelTable(labels);
    slidTaille_.setMinorTickSpacing(1);
    slidTaille_.setMajorTickSpacing(1);
    slidTaille_.setPaintTicks(true);
    slidTaille_.setSnapToTicks(true);
    slidTaille_.setPaintLabels(true);
    slidTaille_.addChangeListener(this);
    slidNiveau_= new JSlider(SwingConstants.HORIZONTAL, 1, 8, 4);
    slidNiveau_.setLabelTable(labels);
    slidNiveau_.setMinorTickSpacing(1);
    slidNiveau_.setMajorTickSpacing(1);
    slidNiveau_.setPaintTicks(true);
    slidNiveau_.setSnapToTicks(true);
    slidNiveau_.setPaintLabels(true);
    slidNiveau_.addChangeListener(this);
    //editeur_.setObject(pg_);
    //pg_.addPropertyChangeListener(this);
    final BuVerticalLayout lo= new BuVerticalLayout();
    lo.setVgap(5);
    setLayout(lo);
    //add(editeur_);
    add(pg_);
    add(new JLabel("Taille"));
    add(slidTaille_);
    add(new JLabel("Niveau"));
    add(slidNiveau_);
    revalidate();
  }
  // Evenements
  @Override
  public void stateChanged(final ChangeEvent _e) {
    if (_e.getSource() == slidTaille_) {
      pg_.setTaille((int)Math.pow(2., slidTaille_.getValue()));
      if (!slidTaille_.getValueIsAdjusting()) {
        firePropertyChange(getPropertyName(), null, pg_);
      }
    } else if (_e.getSource() == slidNiveau_) {
      pg_.setNiveau((int)Math.pow(2., slidNiveau_.getValue()));
      if (!slidNiveau_.getValueIsAdjusting()) {
        firePropertyChange(getPropertyName(), null, pg_);
      }
    }
  }
  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    // A FAIRE:
    //if( _evt.getPropertyName().equals(pg_.getPropertyName()) ) {
    //  pg_.propertyChange(_evt);
    //}
    //firePropertyChange(getPropertyName(), null, pg_);
  }
  // Proprietes
  private String propertyName_;
  public String getPropertyName() {
    return propertyName_;
  }
  public void setPropertyName(final String _propertyName) {
    propertyName_= _propertyName;
  }
}
