/*
 * @creation 13 nov. 06
 * @modification $Date: 2007-05-04 13:49:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JPopupMenu;
import org.fudaa.ctulu.gui.CtuluButtonForPopup;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluPopupColorChooser;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.trace.TraceIconButton;

/**
 * @author fred deniger
 * @version $Id: BSelecteurColorChooserBt.java,v 1.5 2007-05-04 13:49:44 deniger Exp $
 */
public class BSelecteurColorChooserBt extends BSelecteurAbstract implements ActionListener {

  TraceIconButton bt_;
  boolean useInvisible_;

  protected class PopupButton extends CtuluButtonForPopup {

    @Override
    protected JPopupMenu buildPopup(final JPopupMenu _popup) {
      _popup.setLayout(new BuBorderLayout(0, 0));
      final CtuluPopupColorChooser ctuluPopupColorChooser = new CtuluPopupColorChooser(useInvisible_);
      ctuluPopupColorChooser.setParentPopup(_popup);
      ctuluPopupColorChooser.setIdxSelectedColor((Color) getTargetValue());
      ctuluPopupColorChooser.addPropertyChangeListenerForSelectedColor(new PropertyChangeListener() {
        @Override
        public void propertyChange(final PropertyChangeEvent _evt) {
          BSelecteurColorChooserBt.this.changeColor((Color) _evt.getNewValue());
        }
      });
      _popup.add(ctuluPopupColorChooser);
      return _popup;
    }
  }

  public BSelecteurColorChooserBt() {
    this(false, BSelecteurColorChooser.DEFAULT_PROPERTY);
  }

  public BSelecteurColorChooserBt(final boolean _invisible) {
    this(_invisible, BSelecteurColorChooser.DEFAULT_PROPERTY);
  }
  BuPanel pn_;
  private PopupButton popupButton;

  public BSelecteurColorChooserBt(final String _prop) {
    this(false, _prop);
  }

  public BSelecteurColorChooserBt(final boolean _invisible, final String _prop) {
    super(_prop);
    useInvisible_ = _invisible;
    bt_ = new TraceIconButton();
    bt_.setMaximumSize(new Dimension(20, 20));
    bt_.addActionListener(this);
    pn_ = new BuPanel(new BuHorizontalLayout(0, false, true));
    pn_.add(bt_);
    popupButton = new PopupButton();
    pn_.add(popupButton);
    pn_.addPropertyChangeListener("enabled", new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent evt) {
        bt_.setEnabled(pn_.isEnabled());
        popupButton.setEnabled(pn_.isEnabled());
      }
    });
    setTitle(EbliLib.getS("Couleur"));
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final Color c = JColorChooser.showDialog(bt_, EbliLib.getS("Couleur"), bt_.getSavedColor());
    if (c != null) {
      changeColor(c);
    }

  }

  public void changeColor(final Color _c) {
    bt_.setForeground(_c);
    bt_.repaint();
    firePropertyChange(getProperty(), _c);
  }

  @Override
  public void setEnabled(final boolean _b) {
    CtuluLibSwing.setEnable(pn_, _b);
  }

  public BuPanel getPanel() {
    return pn_;
  }

  @Override
  public JComponent[] getComponents() {
    return createComponents(pn_);
  }

  @Override
  public void updateFromTarget() {
    bt_.setForeground((Color) getTargetValue());

  }

  public BuPanel getPn() {
    return pn_;
  }
}
