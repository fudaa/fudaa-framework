/*
 * @file         BMoletteBeanInfo.java
 * @creation     1999-03-22
 * @modification $Date: 2006-04-12 15:28:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.controle;
import java.beans.SimpleBeanInfo;
/**
 * BeanInfo de BMolette.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-04-12 15:28:01 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class BMoletteBeanInfo extends SimpleBeanInfo {}
