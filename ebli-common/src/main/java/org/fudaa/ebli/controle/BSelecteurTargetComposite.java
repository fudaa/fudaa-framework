/*
 * @creation 9 nov. 06
 * @modification $Date: 2007-05-22 14:19:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import java.beans.PropertyChangeListener;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * @author fred deniger
 * @version $Id: BSelecteurTargetComposite.java,v 1.5 2007-05-22 14:19:05 deniger Exp $
 */
public class BSelecteurTargetComposite implements BSelecteurTargetInterface {

  private final BSelecteurTargetInterface[] targets_;

  public BSelecteurTargetComposite(final BSelecteurTargetInterface[] _targets) {
    super();

    if (_targets == null) {
      targets_ = new BSelecteurTargetInterface[0];
    } else {
      targets_ = _targets;
    }
  }

  @Override
  public Object getMoy(final String _key) {
    if (targets_.length == 0) {
      return null;
    }
    final Object o = targets_[0].getProperty(_key);
    if (o instanceof Number) {
      double res = 0;
      int n = 0;
      for (int i = targets_.length - 1; i >= 0; i--) {
        final Number ni = (Number) targets_[i].getProperty(_key);
        if (ni != null) {
          n++;
          res += ni.doubleValue();
        }
      }
      if (n > 0) {
        res = res / n;
      }
      if (o instanceof Double) {
        return CtuluLib.getDouble(res);
      }
      if (o instanceof Float) {
        return new Float((float) res);
      }
      if (o instanceof Short) {
        return new Short((short) res);
      }
      if (o instanceof Integer) {
        return new Integer((int) res);
      }
      if (o instanceof Long) {
        return new Long((long) res);
      }

    }
    return getProperty(_key);
  }

  @Override
  public Object getMin(final String _key) {
    if (targets_.length == 0) {
      return null;
    }
    final Object o = targets_[0].getProperty(_key);
    if (o instanceof Number) {
      double res = ((Number) o).doubleValue();
      for (int i = targets_.length - 1; i >= 0; i--) {
        final Number ni = (Number) targets_[i].getProperty(_key);
        if (ni != null && ni.doubleValue() < res) {
          res = ni.doubleValue();
        }
      }
      if (o instanceof Double) {
        return CtuluLib.getDouble(res);
      }
      if (o instanceof Float) {
        return new Float((float) res);
      }
      if (o instanceof Short) {
        return new Short((short) res);
      }
      if (o instanceof Integer) {
        return new Integer((int) res);
      }
      if (o instanceof Long) {
        return new Long((long) res);
      }

    }
    return getProperty(_key);
  }

  @Override
  public void addPropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    for (int i = targets_.length - 1; i >= 0; i--) {
      targets_[i].addPropertyChangeListener(_key, _l);
    }
  }

  public TraceLigneModel getLineProperty(final TraceLigneModel _init, final String _key) {
    final TraceLigneModel res = new TraceLigneModel(_init);
    for (int i = targets_.length - 1; i > 0; i--) {
      // si l'objet est diff�rent de l'objet commun on renvoie null
      if (!res.keepCommonValues((TraceLigneModel) targets_[i].getProperty(_key))) {
        return res;
      }
    }
    return res;

  }

  public TraceIconModel getIconProperty(final TraceIconModel _init, final String _key) {
    final TraceIconModel res = new TraceIconModel(_init);
    for (int i = targets_.length - 1; i > 0; i--) {
      // si l'objet est diff�rent de l'objet commun on renvoie null
      if (!res.keepCommonValues((TraceIconModel) targets_[i].getProperty(_key))) {
        return res;
      }
    }
    return res;

  }

  @Override
  public Object getProperty(final String _key) {
    if (targets_.length == 0) {
      return null;
    }
    final Object res = targets_[0].getProperty(_key);
    if (res == null) {
      return null;
      // les cas sp�ciaux
    } else if (res instanceof TraceLigneModel) {
      return getLineProperty((TraceLigneModel) res, _key);
    } else if (res instanceof TraceIconModel) {
      return getIconProperty((TraceIconModel) res, _key);
    }
    // les cas normaux
    for (int i = targets_.length - 1; i > 0; i--) {
      final Object tmp = targets_[i].getProperty(_key);
      // si l'objet est diff�rent de l'objet commun on renvoie null
      if (!CtuluLib.isEquals(tmp, res)) {
        return null;
      }
    }
    return res;
  }

  @Override
  public void removePropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    for (int i = targets_.length - 1; i >= 0; i--) {
      targets_[i].removePropertyChangeListener(_key, _l);
    }

  }

  @Override
  public boolean setProperty(final String _key, final Object _newProp) {
    boolean r = false;
    for (int i = targets_.length - 1; i >= 0; i--) {
      r |= targets_[i].setProperty(_key, _newProp);
    }
    return r;
  }

}
