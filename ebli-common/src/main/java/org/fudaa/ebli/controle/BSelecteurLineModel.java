/*
 * @creation 13 juin 2005
 * 
 * @modification $Date: 2007-05-22 14:19:05 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneChooser;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.trace.TraceTraitRenderer;

/**
 * @author Fred Deniger
 * @version $Id: BSelecteurLineModel.java,v 1.7 2007-05-22 14:19:05 deniger Exp $
 */
public class BSelecteurLineModel extends BSelecteurAbstract implements ItemListener, ChangeListener {

  protected class BSelecteurColorTarget implements BSelecteurTargetInterface {
    Color currentColor_;

    @Override
    public void addPropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    }

    @Override
    public Object getMin(final String _key) {
      return null;
    }

    @Override
    public Object getMoy(final String _key) {
      return null;
    }

    @Override
    public Object getProperty(final String _key) {
      return currentColor_;
    }

    @Override
    public void removePropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    }

    @Override
    public boolean setProperty(final String _key, final Object _newProp) {
      currentColor_ = (Color) _newProp;
      fireDataChanged(false, false, true);
      return true;
    }

  }

  public static final String PROPERTY = "lineModel";

  public static final String getProperty(final int _idx) {
    return _idx == 0 ? PROPERTY : PROPERTY + _idx;
  }

  boolean addColor_ = true;
  BSelecteurColorChooserBt btColor_;

  BuComboBox lineTypeCb_;

  JSpinner spTaille_;

  SpinnerNumberModel tailleModel_;

  TraceLigneChooser typeModel_;

  public BSelecteurLineModel() {
    this(PROPERTY, null);
  }

  public BSelecteurLineModel(final String _prop) {
    this(_prop, null);
  }

  public BSelecteurLineModel(final String _prop, final TraceLigneModel _data) {
    super(_prop);
    typeModel_ = new TraceLigneChooser(true);
    lineTypeCb_ = new BuComboBox();
    lineTypeCb_.setModel(typeModel_);
    lineTypeCb_.addItemListener(this);
    final TraceTraitRenderer r = new TraceTraitRenderer();
    lineTypeCb_.setRenderer(r);
    lineTypeCb_.setPreferredSize(new Dimension(60, 15));

    double val = 1D;
    if (_data != null) {
      val = _data.getEpaisseur();
    }
    tailleModel_ = new SpinnerNumberModel(val, 0, 15, 0.5);
    spTaille_ = new JSpinner(tailleModel_);
    tailleModel_.addChangeListener(this);
    spTaille_.setModel(tailleModel_);
    btColor_ = new BSelecteurColorChooserBt();
    final BSelecteurColorTarget selecteurColorTarget = new BSelecteurColorTarget();
    if (_data != null) {
      typeModel_.setSelectedType(_data.getTypeTrait());
      selecteurColorTarget.currentColor_ = _data.getCouleur();
    }
    btColor_.setSelecteurTarget(selecteurColorTarget);
    title_ = EbliLib.getS("Ligne");
  }

  public BSelecteurLineModel(final TraceLigneModel _data) {
    this(PROPERTY, _data);
  }

  protected void fireDataChanged(final boolean _type, final boolean _epaisseur, final boolean _color) {
    if (isUpdating_) {
      return;
    }
    final TraceLigneModel d = getNewData();
    if (_type) {
      d.setColorIgnored();
      d.setEpaisseurIgnored();
    } else if (_epaisseur) {
      d.setColorIgnored();
      d.setTypeIgnored();
    } else if (_color) {
      d.setTypeIgnored();
      d.setEpaisseurIgnored();
    }
    firePropertyChange(getProperty(), d);
  }

  public JPanel buildPanel() {
    final JPanel r = new BuPanel();
    buildPanel(r, true);
    return r;
  }

  public void buildPanel(final JPanel _pn, final boolean _layout) {
    if (_layout) {
      _pn.setLayout(new BuGridLayout(3, 3, 3, true, true));
    }
    _pn.add(lineTypeCb_);
    _pn.add(spTaille_);
    if (isAddColor()) {
      _pn.add(btColor_.getPn());
    }

  }

  public JComponent getComponent() {
    return buildPanel();
  }

  @Override
  public JComponent[] getComponents() {
    if (isAddColor()) {
      return new JComponent[] { lineTypeCb_, spTaille_, btColor_.getPn() };
    }
    return new JComponent[] { lineTypeCb_, spTaille_ };
  }

  public TraceLigneModel getNewData() {
    final TraceLigneModel r = new TraceLigneModel();
    if (btColor_ != null && btColor_.getTarget() != null && ((BSelecteurColorTarget) btColor_.getTarget()).currentColor_ != null)
      r.setColor(((BSelecteurColorTarget) btColor_.getTarget()).currentColor_);
    r.setTypeTrait(typeModel_.getSelectedType());
    r.setEpaisseur(tailleModel_.getNumber().floatValue());
    return r;
  }

  public final SpinnerNumberModel getTailleModel() {
    return tailleModel_;
  }

  public final TraceLigneChooser getTypeModel() {
    return typeModel_;
  }

  public boolean isAddColor() {
    return addColor_;
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if (_e.getStateChange() == ItemEvent.DESELECTED) {
      return;
    }
    updateEnableStates();
    fireDataChanged(true, false, false);
  }

  private void updateEnableStates() {
    final boolean enable = typeModel_.getSelectedType() != TraceLigne.INVISIBLE && lineTypeCb_.isEnabled();
    btColor_.setEnabled(enable);
    spTaille_.setEnabled(enable);
  }

  public void setAddColor(final boolean _addColor) {
    addColor_ = _addColor;
  }

  @Override
  public void stateChanged(final ChangeEvent _e) {
    fireDataChanged(false, true, false);
  }

  
  public void updateFromModel(final TraceLigneModel ic) {
	  final int type = ic == null ? 0 : ic.getTypeTrait();
	    if (type >= 0) {
	      typeModel_.setSelectedType(type);
	    } else {
	      lineTypeCb_.setSelectedItem(null);
	    }
	    tailleModel_.setValue(CtuluLib.getDouble(Math.max(0D, ic == null ? 1 : ic.getEpaisseur())));
	    final BSelecteurColorTarget colorTarget = (BSelecteurColorTarget) btColor_.getTarget();
	    colorTarget.currentColor_ = (ic == null ? Color.BLACK : ic.getCouleur());
	    updateEnableStates();
	    btColor_.updateFromTarget();
  }
  
  @Override
  public void updateFromTarget() {
    final TraceLigneModel ic = (TraceLigneModel) getTargetValue();
    updateFromModel(ic);

  }
}
