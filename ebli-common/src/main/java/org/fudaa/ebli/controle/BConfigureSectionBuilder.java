/*
 * @creation 10 nov. 06
 * 
 * @modification $Date: 2007-05-04 13:49:45 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;

/**
 * @author fred deniger
 * @version $Id: BConfigureSectionBuilder.java,v 1.5 2007-05-04 13:49:45 deniger Exp $
 */
public class BConfigureSectionBuilder {

  // JLabel[] lbs_;
  JComponent pn_;
  final BSelecteurInterface[] sel_;
  BConfigureSectionBuilder[] ssSectionsBuilder_;
  final BConfigurableInterface[] ssSections_;
  final BConfigurableInterface sect_;

  public static boolean isSame(final BConfigurableInterface _i1, final BConfigurableInterface _i2) {
    if (!isSameTitle(_i1, _i2)) { return false; };
    if (!isSame(_i1.createSelecteurs(), _i2.createSelecteurs())) { return false; }
    final BConfigurableInterface[] c1 = _i1.getSections();
    final BConfigurableInterface[] c2 = _i2.getSections();
    if (!CtuluLibArray.isSameSize(c1, c2)) { return false; }
    if (c1 != null) {
      for (int i = c1.length - 1; i >= 0; i--) {
        if (!isSame(c1[i], c2[i])) { return false; }
      }
    }
    return true;
  }

  private static boolean isSameTitle(final BConfigurableInterface _i1, final BConfigurableInterface _i2) {
    if (_i1 == _i2) { return true; }
    if (_i1 == null || _i2 == null) { return false; }
    return CtuluLib.isEquals(_i1.getTitle(), _i2.getTitle());
  }

  private static boolean isSameTitle(final List _list) {
    if (CtuluLibArray.isEmpty(_list)) { return false; }
    if (_list.size() == 1) { return true; }
    final BConfigurableInterface i1 = (BConfigurableInterface) _list.get(0);
    for (int i = _list.size() - 1; i > 0; i--) {
      if (!isSameTitle(i1, (BConfigurableInterface) _list.get(i))) { return false; }
    }
    return true;
  }

  private static boolean isSame(final BConfigurableInterface[] _list) {
    return isSame(Arrays.asList(_list));
  }

  private static boolean isSame(final List _list) {
    if (CtuluLibArray.isEmpty(_list)) { return false; }
    if (_list.size() == 1) { return true; }
    final BConfigurableInterface i1 = (BConfigurableInterface) _list.get(0);
    for (int i = _list.size() - 1; i > 0; i--) {
      if (!isSame(i1, (BConfigurableInterface) _list.get(i))) { return false; }
    }
    return true;
  }

  public static BConfigurableInterface getCommonConfigurable(final BConfigurableInterface[] _conf) {
    // on se d�barrasse des cas tordus
    if (_conf == null || _conf.length == 0) { return null; }
    if (_conf.length == 1) { return _conf[0]; }
    final BConfigurableInterface first = _conf[0];
    boolean reallySame = true;
    for (int i = 1; i < _conf.length && reallySame; i++) {
      reallySame = isSame(first, _conf[i]);
    }
    if (reallySame) { return new BConfigurableCommon(_conf); }
    final List res = new ArrayList();

    final BConfigurableInterface[][] sameTitle = getCommonSousSections(_conf, true);
    if (sameTitle == null) { return null; }
    for (int i = 0; i < sameTitle.length; i++) {
      if (CtuluLibArray.isEmpty(sameTitle[i])) {
        continue;
      }
      if (isSame(sameTitle[i])) {
        res.add(new BConfigurableCommon(sameTitle[i]));

      } else {
        final BConfigurableInterface[][] sameSsection = getCommonSousSections(sameTitle[i], false);
        if (!CtuluLibArray.isEmpty(sameSsection)) {
          final BConfigurableInterface[] newSS = new BConfigurableInterface[sameSsection.length];
          for (int k = newSS.length - 1; k >= 0; k--) {
            newSS[k] = new BConfigurableCommon(sameSsection[k]);
          }
          res.add(new BConfigurableComposite(newSS, sameTitle[i][0].getTitle()));
        }
      }
    }
    return res.size() == 0 ? null : new BConfigurableComposite((BConfigurableInterface[]) res
        .toArray(new BConfigurableInterface[res.size()]), null);
  }

  private static BConfigurableInterface[][] getCommonSousSections(final BConfigurableInterface[] _conf,
      final boolean _testTitleOnly) {
    final BConfigurableInterface[] ssFirst = _conf[0].getSections();
    if (CtuluLibArray.isEmpty(ssFirst)) { return new BConfigurableInterface[0][0]; }
    int min = ssFirst.length;
    final List[] sections = new ArrayList[min];
    for (int i = 0; i < _conf.length; i++) {
      final BConfigurableInterface[] ssi = _conf[i].getSections();
      if (CtuluLibArray.isEmpty(ssi)) { return null; }
      min = Math.min(min, ssi.length);
      for (int k = 0; k < min; k++) {
        if (sections[k] == null) {
          sections[k] = new ArrayList(_conf.length);
        }
        sections[k].add(ssi[k]);
      }
    }
    final List res = new ArrayList(min);
    for (int i = 0; i < min; i++) {
      if (_testTitleOnly && isSameTitle(sections[i])) {
        res.add(sections[i]);
      } else if (isSame(sections[i])) {
        res.add(sections[i]);
      }
    }
    final BConfigurableInterface[][] resfinal = new BConfigurableInterface[res.size()][];
    for (int i = resfinal.length - 1; i >= 0; i--) {
      final List li = ((List) res.get(i));
      resfinal[i] = (BConfigurableInterface[]) li.toArray(new BConfigurableInterface[li.size()]);
    }
    return resfinal;

  }

  public static boolean isSame(final BSelecteurInterface[] _i1, final BSelecteurInterface[] _i2) {
    if (!CtuluLibArray.isSameSize(_i1, _i2)) { return false; }
    if (_i1 != null) {
      for (int i = _i1.length - 1; i >= 0; i--) {
        if (!isSame(_i1[i], _i2[i])) { return false; }
      }
    }
    return true;
  }

  public static boolean isSame(final BSelecteurInterface _i1, final BSelecteurInterface _i2) {
    return (_i1 == _i2)
        || (CtuluLib.isEquals(_i1.getTitle(), _i2.getTitle()) && CtuluLib
            .isEquals(_i1.getProperty(), _i2.getProperty()));
  }

  public BConfigureSectionBuilder(final BConfigurableInterface _sect) {
    super();
    sect_ = _sect;
    if (sect_ != null) {
      ssSections_ = sect_.getSections();
      sel_ = sect_.createSelecteurs();
      if (ssSections_ != null) {
        ssSectionsBuilder_ = new BConfigureSectionBuilder[ssSections_.length];
        for (int i = ssSections_.length - 1; i >= 0; i--) {
          ssSectionsBuilder_[i] = new BConfigureSectionBuilder(ssSections_[i]);
        }
      }
    } else {
      ssSections_ = null;
      sel_ = null;

    }
  }

  public void doAfterDisplay() {
    if (pn_ != null) {
      // pn_.invalidate();
      // pn_.doLayout();
      pn_.revalidate();
      final Dimension d = pn_.getPreferredSize();
      d.height += 5;
      pn_.setPreferredSize(d);
    }
    if (ssSectionsBuilder_ != null) {
      for (int i = ssSectionsBuilder_.length - 1; i >= 0; i--) {
        ssSectionsBuilder_[i].doAfterDisplay();
      }
    }

  }

  public void setVisible(final boolean _b, final Component _toAvoid) {
    if (pn_ != null) {
      for (int i = pn_.getComponentCount() - 1; i >= 0; i--) {
        if (pn_.getComponent(i) != _toAvoid) {
          pn_.getComponent(i).setVisible(_b);
        }
      }
    }
    if (components_ != null) {
      for (int i = components_.size() - 1; i >= 0; i--) {
        final Component component = (Component) components_.get(i);
        if (component != _toAvoid) {
          (component).setVisible(_b);

        }
      }
    }
  }

  protected void buildPanel(final boolean _thisSelecteur) {
    pn_ = new BuPanel();
    pn_.setBorder(BuBorders.EMPTY3333);
    final GridBagLayout lay = new GridBagLayout();
    pn_.setLayout(lay);
    final GridBagConstraints cl = new GridBagConstraints();
    cl.gridy = 0;
    cl.gridx = 0;
    cl.ipadx = 0;
    cl.ipady = 5;
    if (_thisSelecteur) {
      buildSelecteurs(pn_, cl, lay);
    }
    final Insets insets = new Insets(10, 0, 10, 0);
    final Insets insets0 = new Insets(1, 0, 10, 0);
    if (!CtuluLibArray.isEmpty(ssSections_)) {
      ssSectionsBuilder_ = new BConfigureSectionBuilder[ssSections_.length];
      for (int i = 0; i < ssSections_.length; i++) {
        final BuTransparentToggleButton btVisible = new BuTransparentToggleButton(BuResource.BU.getIcon("bu_expand"),
            BuResource.BU.getIcon("bu_collapse"));
        btVisible.setText(ssSections_[i].getTitle());
        btVisible.setForeground(UIManager.getColor("TitledBorder.titleColor"));
        btVisible.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY.brighter()));
        updateConstraints(cl);
        cl.gridx = 0;
        cl.weightx = 1;
        cl.weighty = 0;

        cl.gridwidth = GridBagConstraints.REMAINDER;
        cl.anchor = GridBagConstraints.FIRST_LINE_START;
        if (i != 0) {
          cl.insets = insets;
        } else {
          cl.insets = insets0;
        }
        lay.setConstraints(btVisible, cl);
        pn_.add(btVisible);
        cl.gridy++;
        ssSectionsBuilder_[i] = new BConfigureSectionBuilder(ssSections_[i]);
        ssSectionsBuilder_[i].buildSelecteurs(pn_, cl, lay);
        final BConfigureSectionBuilder builder = ssSectionsBuilder_[i];
        btVisible.setSelected(true);
        btVisible.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(final ItemEvent _e) {
            builder.setVisible(btVisible.isSelected(), btVisible);
            pn_.revalidate();
            final BuPalette palette = (BuPalette) SwingUtilities.getAncestorOfClass(BuPalette.class, pn_);
            if (palette != null) {
              palette.pack();
            }
          }
        });
        if (!CtuluLibArray.isEmpty(ssSectionsBuilder_[i].ssSections_)) {
          ssSectionsBuilder_[i].buildPanel(false);
          ssSectionsBuilder_[i].pn_.setBorder(BuBorders.EMPTY0505);

          updateConstraints(cl);
          cl.gridx = 0;
          lay.setConstraints(ssSectionsBuilder_[i].pn_, cl);
          pn_.add(ssSectionsBuilder_[i].pn_);
          cl.gridy++;
        }
      }
    }
    // un dernier composant qui ne sert qu'a prendre la place existante
    cl.weighty = 1;
    cl.gridx = 0;
    final JLabel label = new JLabel();
    lay.setConstraints(label, cl);
    pn_.add(label);

  }

  private void updateConstraints(final GridBagConstraints _cl) {
    _cl.weightx = 0;
    _cl.weighty = 0;
    _cl.insets = BuInsets.INSETS1111;
    _cl.gridwidth = 1;
    _cl.gridheight = 1;
    _cl.fill = GridBagConstraints.HORIZONTAL;
    _cl.anchor = GridBagConstraints.LINE_START;
  }

  boolean isEmpty() {
    return CtuluLibArray.isEmpty(sel_) && CtuluLibArray.isEmpty(ssSections_);
  }

  protected void buildTab() {
    if (sel_ != null) {
      FuLog.warning("ESE: les selecteurs ne seront pas affich�s");
    }
    final BuTabbedPane tabbed = new BuTabbedPane();
    pn_ = tabbed;
    if (!CtuluLibArray.isEmpty(ssSections_)) {
      ssSectionsBuilder_ = new BConfigureSectionBuilder[ssSections_.length];
      for (int i = 0; i < ssSections_.length; i++) {
        ssSectionsBuilder_[i] = new BConfigureSectionBuilder(ssSections_[i]);
        ssSectionsBuilder_[i].buildPanel(true);
//        tabbed.addTab(ssSections_[i].getTitle(), new JScrollPane(ssSectionsBuilder_[i].pn_,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
        tabbed.addTab(ssSections_[i].getTitle(), ssSectionsBuilder_[i].pn_);
      }
    }

  }

  protected void buildTab(int i) {
    buildTab();
    if (i >= 0) selectTab(i);
  }

  public void selectTab(int i) {
    if (pn_ == null || !(pn_ instanceof BuTabbedPane)) return;
    ((BuTabbedPane) pn_).setSelectedIndex(i);
  }

  protected void updateTarget() {
    if (!CtuluLibArray.isEmpty(sel_)) {
      for (int i = 0; i < sel_.length; i++) {
        sel_[i].setSelecteurTarget(sect_.getTarget());
      }
    }
    if (!CtuluLibArray.isEmpty(ssSectionsBuilder_)) {
      for (int i = 0; i < ssSectionsBuilder_.length; i++) {
        ssSectionsBuilder_[i].updateTarget();
      }
    }
  }

  List components_;

  public void buildSelecteurs(final JComponent _pn, final GridBagConstraints _cl, final GridBagLayout _lay) {
    components_ = new ArrayList();
    if (CtuluLibArray.isEmpty(sel_)) { return; }
    updateConstraints(_cl);
    for (int i = 0; i < sel_.length; i++) {
      final BSelecteurInterface si = sel_[i];
      final JComponent[] comps = sel_[i].getComponents();
      components_.addAll(Arrays.asList(comps));
      updateConstraints(_cl);

      _cl.gridx = 0;
      _cl.insets = new Insets(1, 8, 1, 3);
      if (si.needAllWidth() && comps.length == 1) {
        _cl.weighty = 2;
        _cl.weightx = 2;
        _cl.fill = GridBagConstraints.BOTH;
        _cl.gridwidth = GridBagConstraints.REMAINDER;
        _lay.setConstraints(comps[0], _cl);
        _pn.add(comps[0]);

      } else {

        final BuLabel lb = new BuLabel(si.getTitle());
        lb.setToolTipText(si.getTooltip());
        // _cl.fill = GridBagConstraints.HORIZONTAL;
        _lay.setConstraints(lb, _cl);
        _pn.add(lb);
        components_.add(lb);
        _cl.insets = BuInsets.INSETS1111;
        for (int j = 0; j < comps.length; j++) {
          _cl.gridx = j + 1;
          final boolean last = j == comps.length - 1;
          // un seul
          _cl.fill = GridBagConstraints.BOTH;
          if (comps.length == 1 && comps[0] instanceof JButton) {
            _cl.fill = GridBagConstraints.VERTICAL;
          }
          _cl.gridwidth = last ? GridBagConstraints.REMAINDER : 1;

          _lay.setConstraints(comps[j], _cl);
          _pn.add(comps[j]);
        }
      }
      _cl.gridy++;
    }

  }
}
