/*
 * @creation 10 nov. 06
 * 
 * @modification $Date: 2007-03-23 17:25:15 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

/**
 * @author fred deniger
 * @version $Id: BConfigurableComposite.java,v 1.2 2007-03-23 17:25:15 deniger Exp $
 */
public class BConfigurableComposite implements BConfigurableInterface {

  final BConfigurableInterface[] childs_;
  final String title_;

  public BConfigurableComposite(final BConfigurableInterface[] _childs, final String _title) {
    super();
    childs_ = _childs;
    title_ = _title;
  }

  public BConfigurableComposite(final BConfigurableInterface _childs, final String _title) {
    super();
    childs_ = new BConfigurableInterface[]{_childs};
    title_ = _title;
  }

  public BConfigurableComposite(final String _title, final BConfigurableInterface... _child1) {
    title_ = _title;
    childs_ = _child1;
  }

  public BConfigurableComposite(final BConfigurableInterface _child1, final BConfigurableInterface _child2,
          final String _title) {
    super();
    childs_ = new BConfigurableInterface[]{_child1, _child2};
    title_ = _title;
  }

  public BConfigurableComposite(final BConfigurableInterface _child1, final BConfigurableInterface _child2,
          final BConfigurableInterface _child3, final String _title) {
    super();
    childs_ = new BConfigurableInterface[]{_child1, _child2, _child3};
    title_ = _title;
  }

  public BConfigurableComposite(final BConfigurableInterface _child1, final BConfigurableInterface _child2,
          final BConfigurableInterface _child3, final BConfigurableInterface _child4, final String _title) {
    super();
    childs_ = new BConfigurableInterface[]{_child1, _child2, _child3, _child4};
    title_ = _title;
  }

  @Override
  public BConfigurableInterface[] getSections() {
    return childs_;
  }

  @Override
  public String getTitle() {
    return title_;
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    return null;
  }

  @Override
  public BSelecteurTargetInterface getTarget() {
    return null;
  }

  @Override
  public void stopConfiguration() {
    if (childs_ != null) {
      for (int i = 0; i < childs_.length; i++) {
        if (childs_[i] != null) {
          childs_[i].stopConfiguration();
        }
      }
    }
  }
}
