/*
 * @creation     1999-01-11
 * @modification $Date: 2006-09-19 14:55:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.controle;

import java.util.Dictionary;
import java.util.Hashtable;
import javax.swing.JLabel;
import javax.swing.JSlider;

/**
 * Un bean de choix de resolution (pour les calques polylignes).
 * 
 * @version $Revision: 1.7 $ $Date: 2006-09-19 14:55:52 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class BSelecteurResolution extends JSlider {
  // donnees membres privees
  private int valeurPrec_;
  private String property_; // "resolution" ou "densite"

  // Constructeur
  public BSelecteurResolution() {
    super(HORIZONTAL, 0, 1000, 1000);
    property_ = "resolution";
    valeurPrec_ = 1000;
    final Dictionary labels = new Hashtable();
    labels.put(new Integer(0), new JLabel("Min"));
    labels.put(new Integer(1000), new JLabel("Max"));
    setLabelTable(labels);
    setPaintLabels(true);
    setMinorTickSpacing(100);
    setPaintTicks(true);
    /*
     * Dimension d=getPreferredSize(); d.width*=2; d.height*=2; setPreferredSize(d);
     */
  }

  public void setPropertyName(final String _property) {
    property_ = _property;
  }

  public String getPropertyName() {
    return property_;
  }

  // Evenements
  @Override
  protected void fireStateChanged() {
    if (!getValueIsAdjusting()) {
      int value = getValue();
      if ((value == 0) && "densite".equals(property_)) {
        value = 1;
      }
      firePropertyChange(property_, valeurPrec_, value);
      valeurPrec_ = getValue();
    }
  }
}
