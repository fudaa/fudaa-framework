/*
 * @creation     1998-12-09
 * @modification $Date: 2007-05-04 13:49:44 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuLightBorder;
import com.memoire.bu.BuVerticalLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;

/**
 * Une palette de fontes.
 * 
 * @version $Revision: 1.2 $ $Date: 2007-05-04 13:49:44 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class BSelecteurReduitFonte extends JComponent implements ActionListener {
  private final JComboBox chPolice_;
  private final JComboBox chTaille_;
  private final JComboBox chStyle_;
  private final JLabel lbExemple_;

  public BSelecteurReduitFonte() {
    super();
    chPolice_ = new JComboBox();
    chPolice_.setMaximumRowCount(6);
    final String[] liste = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
    for (int i = 0; i < liste.length; i++) {
      chPolice_.addItem(liste[i]);
    }
    chPolice_.setName("chFONTE_POLICE");
    chPolice_.addActionListener(this);
    chTaille_ = new JComboBox();
    chTaille_.setMaximumRowCount(6);
    // chTaille.setEditable(true);
    chTaille_.addItem("8");
    chTaille_.addItem("10");
    chTaille_.addItem("12");
    chTaille_.addItem("14");
    chTaille_.addItem("16");
    chTaille_.addItem("18");
    chTaille_.addItem("24");
    chTaille_.addItem("36");
    chTaille_.setName("chFONTE_TAILLE");
    chTaille_.addActionListener(this);
    chStyle_ = new JComboBox();
    chStyle_.setMaximumRowCount(3);
    chStyle_.addItem("Normal");
    chStyle_.addItem("Italique");
    chStyle_.addItem("Gras");
    chStyle_.setName("chFONTE_STYLE");
    chStyle_.addActionListener(this);
    lbExemple_ = new JLabel("ABC abc 123");
    lbExemple_.setBorder(new BuLightBorder(BuLightBorder.LOWERED, 5));
    lbExemple_.setPreferredSize(new Dimension(150, 75));
    final BuVerticalLayout lo = new BuVerticalLayout();
    lo.setVgap(5);
    setLayout(lo);
    add(chPolice_);
    add(chTaille_);
    add(chStyle_);
    add(lbExemple_);
  }

  @Override
  public boolean isFocusCycleRoot() {
    return true;
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    final JComponent source = (JComponent) _evt.getSource();
    valide(source);
  }

  public void valide(final JComponent _source) {
    final String police = (String) chPolice_.getSelectedItem();
    final int taille = Integer.parseInt((String) chTaille_.getSelectedItem());
    final String sstyle = (String) chStyle_.getSelectedItem();
    int istyle = 0;
    if ("Normal".equals(sstyle)) {
      istyle = Font.PLAIN;
    }
    if ("Italique".equals(sstyle)) {
      istyle = Font.ITALIC;
    }
    if ("Gras".equals(sstyle)) {
      istyle = Font.BOLD;
    }
    setFont(new Font(police, istyle, taille));
  }

  @Override
  public Font getFont() {
    return lbExemple_.getFont();
  }

  @Override
  public void setFont(final Font _v) {
    if (!getFont().equals(_v)) {
      final Font vp = getFont();
      lbExemple_.setFont(_v);
      setSize(getPreferredSize());
      revalidate();
      firePropertyChange("font", vp, getFont());
    }
  }
}
