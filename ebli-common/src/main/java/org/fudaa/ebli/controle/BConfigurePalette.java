/*
 * @creation 21 sept. 2004
 * 
 * @modification $Date: 2007-05-04 13:49:44 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author Fred Deniger
 * @version $Id: BConfigurePalette.java,v 1.4 2007-05-04 13:49:44 deniger Exp $
 */
public class BConfigurePalette extends BuPanel implements BPalettePanelInterface {

  /**
   * @param _o l'objet a tester
   * @return true si cet objet est supporte par cette palette
   */
  public static final boolean isTargetValid(final Object _o) {
    return (_o instanceof BConfigurableSectionInterface);

  }

  private final BSelecteurCheckBox cbVisible_;
  final BSelecteurTextField txtTitle_;
  // final BuPanel pnConf_;
  BConfigureSectionBuilder builder_;
  final BuPanel pnTitle_;

  public BConfigurePalette() {
    this(true,null,null);
  }

  public BConfigurePalette(final boolean _showVisible, String titleText, String visibleText) {
    setLayout(new BuBorderLayout(2, 2, true, true));
    setBorder(BorderFactory.createEmptyBorder(5, 8, 5, 8));
    pnTitle_ = new BuPanel();

    txtTitle_ = new BSelecteurTextField("title", new BuTextField());
    txtTitle_.setTitle(titleText == null ? EbliLib.getS("Titre") : titleText);
    pnTitle_.setLayout(new BuGridLayout(2, 3, 3));
    pnTitle_.add(new BuLabel(txtTitle_.getTitle()));
    pnTitle_.add(txtTitle_.getComponents()[0]);
    if (_showVisible) {
      cbVisible_ = new BSelecteurCheckBox(BSelecteurCheckBox.PROP_VISIBLE);
      cbVisible_.setTitle(visibleText == null ? EbliLib.getS("Visible") : visibleText);
      pnTitle_.add(new BuLabel(cbVisible_.getTitle()));
      pnTitle_.add(cbVisible_.getComponents()[0]);
    } else {
      cbVisible_ = null;
    }
    // add(pnConf_, BuBorderLayout.CENTER);
    add(pnTitle_, BuBorderLayout.NORTH);
  }
  
  public void setPanelTitleVisible(boolean visible){
    pnTitle_.setVisible(visible);
  }

  @Override
  public void doAfterDisplay() {
    if (builder_ != null && builder_.pn_ != null) {
      builder_.doAfterDisplay();
    }
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void paletteDeactivated() {}

  @Override
  public boolean setPalettePanelTarget(final Object _target) {
    return setPalettePanelTarget(_target, -1);
  }

  public boolean setPalettePanelTarget(final Object _target, int indiceSelecteurToShow) {
    BSelecteurTargetInterface vis = null;
    boolean enable = false;
    BConfigurableInterface conf = null;
    final boolean res = _target instanceof BConfigurePaletteTargetInterface;
    if (res) {
      final BConfigurableInterface[] cs = ((BConfigurePaletteTargetInterface) _target).getConfigureInterfaces();
      if (cs != null && cs.length == 1) {
        final BConfigurePaletteTargetInterface target = (BConfigurePaletteTargetInterface) _target;
        vis = target.getVisibleTitleTarget();
        enable = target.isTitleModifiable();
        conf = cs[0];

        // -- ajout de la palette en cours dans le cas des objets reload: permet l'ajout en cours de route de
        // bselecteurInterface --//
        if (target instanceof BConfigurePaletteTargetInterfaceReload) {
          ((BConfigurePaletteTargetInterfaceReload) target).setPalette(this);
        }

      }
    }
    setTitleVisibleTarget(vis, enable);
    if (indiceSelecteurToShow != -1) setTargetConf(conf, indiceSelecteurToShow);
    else setTargetConf(conf);
    return res;
  }

  public void setTitleVisibleTarget(final BSelecteurTargetInterface _target, final boolean _txtEnable) {
    txtTitle_.setSelecteurTarget(_target);
    if (cbVisible_ != null) {
      cbVisible_.setSelecteurTarget(_target);
    }
    txtTitle_.tf_.setEnabled(_txtEnable);
  }

  public boolean setTargetConf(final BConfigurableInterface _target) {
    return setTargetConf(_target, -1);
  }

  public boolean setTargetConf(final BConfigurableInterface _target, int tabIndiceToAffiche) {
    // on supprime l'ancienne configuration
    if (builder_ != null) {
      if (builder_.pn_ != null) {
        remove(builder_.pn_);
      }
      if (builder_.sect_ != null) {
        builder_.sect_.stopConfiguration();
      }
    }
    builder_ = new BConfigureSectionBuilder(_target);
    builder_.buildTab(tabIndiceToAffiche);
    builder_.updateTarget();
    if (!builder_.isEmpty()) {
      add(builder_.pn_, BuBorderLayout.CENTER);
    }
    revalidate();
    return true;

  }
}
