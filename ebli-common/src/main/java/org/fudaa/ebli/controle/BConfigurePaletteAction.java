/*
 * @creation 19 sept. 2005
 * 
 * @modification $Date: 2007-05-04 13:49:44 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuResource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.commun.EbliActionPaletteTreeModel;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author Fred Deniger
 * @version $Id: BConfigurePaletteAction.java,v 1.8 2007-05-04 13:49:44 deniger Exp $
 */
public class BConfigurePaletteAction extends EbliActionPaletteTreeModel {

  public BConfigurePaletteAction(final TreeSelectionModel _model) {
    super(EbliLib.getS("Configuration"), BuResource.BU.getToolIcon("configurer"), "CONFIGURE", _model);
    setResizable(true);
  }

  @Override
  protected boolean isTargetValid(final Object _o) {
    return true;
  }

  @Override
  public boolean setPaletteTarget(final Object _target) {
    // FIXME a reprendre avec les EbliWidget
    if (palette_ == null) { return false; }
    if (_target == null) {
      palette_.setPalettePanelTarget(null);
    } else {
      final Object[] o = (Object[]) _target;
      if (o.length == 0) {
        palette_.setPalettePanelTarget(null);
      } else {
        // liste des cibles configurables
        final List tgs = new ArrayList(o.length * 2);
        for (int i = o.length - 1; i >= 0; i--) {
          tgs.addAll(Arrays.asList(((BConfigurePaletteTargetInterface) o[i]).getConfigureInterfaces()));
        }
        // une seule interface de configuration
        if (o.length == 1) {
          palette_.setPalettePanelTarget(o[0]);
        } else {
          palette_.setPalettePanelTarget(new BConfigurePaletteTargetComposite(BConfigureSectionBuilder
              .getCommonConfigurable((BConfigurableInterface[]) tgs.toArray(new BConfigurableInterface[tgs.size()])),
              BConfigurePaletteTargetComposite.getVisTargets(o)));
        }
      }
    }
    doAfterDisplay();
    return true;
  }
  
  protected String txtVisible;
  protected String txtTitre;

  @Override
  public BPalettePanelInterface buildPaletteContent() {
    return new BConfigurePalette(true,txtTitre,txtVisible);
  }

  @Override
  protected void doAfterDisplay() {
    super.doAfterDisplay();
    if (window_ != null) {
      window_.pack();
    }
  }

  @Override
  protected Object getTarget(final TreeSelectionModel _m) {
    final TreePath[] p = _m.getSelectionPaths();

    if (p != null) {
      final Object[] targets = new Object[p.length];
      // p est non null
      for (int i = p.length - 1; i >= 0; i--) {
        targets[i] = p[i] == null ? null : p[i].getLastPathComponent();
      }
      return targets;
    }
    return null;
  }

}
