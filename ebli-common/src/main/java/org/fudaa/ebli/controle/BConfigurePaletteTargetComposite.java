/*
 * @creation 10 nov. 06
 * @modification $Date: 2006-12-05 10:14:37 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import org.fudaa.ebli.commun.EbliLib;

/**
 * @author fred deniger
 * @version $Id: BConfigurePaletteTargetComposite.java,v 1.2 2006-12-05 10:14:37 deniger Exp $
 */
public class BConfigurePaletteTargetComposite implements BConfigurePaletteTargetInterface {

  final BConfigurableInterface conf_;
  final BSelecteurTargetInterface vis_;

  public static BSelecteurTargetInterface[] getVisTargets(final Object[] _o) {
    if (_o == null) {
      return null;
    }
    final BSelecteurTargetInterface[] res = new BSelecteurTargetInterface[_o.length];
    for (int i = 0; i < res.length; i++) {
      res[i] = ((BConfigurePaletteTargetInterface) _o[i]).getVisibleTitleTarget();
    }
    return res;
  }

  public BConfigurePaletteTargetComposite(final BConfigurableInterface _conf, final BSelecteurTargetInterface[] _vis) {
    super();
    conf_ = _conf;
    vis_ = new BSelecteurTargetComposite(_vis) {
      @Override
      public Object getProperty(final String _key) {
        if (BSelecteurTextField.TITLE_PROPERTY == _key) {
          return EbliLib.getS("S�lection multiples");
        }
        return super.getProperty(_key);
      }
    };
  }

  public BConfigurableInterface getConfigureComponent() {
    return conf_;
  }

  @Override
  public BConfigurableInterface[] getConfigureInterfaces() {
    return new BConfigurableInterface[] { getConfigureComponent() };
  }

  @Override
  public BSelecteurTargetInterface getVisibleTitleTarget() {
    return vis_;
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

}
