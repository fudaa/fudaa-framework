/*
 * @creation     1998-09-09
 * @modification $Date: 2006-11-14 09:06:31 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.controle;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
/**
 * Un bean de selection foreground/background.
 *
 * @version      $Id: BSelecteurPlanCouleur.java,v 1.1 2006-11-14 09:06:31 deniger Exp $
 * @author       Axel von Arnim
 */
public class BSelecteurPlanCouleur
  extends JComponent
  implements MouseListener, PropertyChangeListener {
  // donnees membres publiques
  public final static int CONTOUR= 0;
  public final static int REMPLISSAGE= 1;
  // donnees membres privees
  int plan_;
  Color couleurContour_;
  Color couleurRemplissage_;
  Rectangle rectContour_;
  Rectangle rectRemplissage_;
  // Constructeurs
  public BSelecteurPlanCouleur() {
    super();
    plan_= CONTOUR;
    couleurContour_= Color.black;
    couleurRemplissage_= Color.white;
    rectContour_= new Rectangle(10, 10, 20, 20);
    rectRemplissage_= new Rectangle(20, 20, 20, 20);
    final Dimension d= new Dimension(50, 50);
    addMouseListener(this);
    setMinimumSize(d);
    setPreferredSize(d);
  }
  // Methodes publiques
  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    if (_evt.getPropertyName().equals("couleur")) {
      if (plan_ == CONTOUR) {
        firePropertyChange(
          "foreground",
          _evt.getOldValue(),
          _evt.getNewValue());
        couleurContour_= (Color)_evt.getNewValue();
      } else if (plan_ == REMPLISSAGE) {
        firePropertyChange(
          "background",
          _evt.getOldValue(),
          _evt.getNewValue());
        couleurRemplissage_= (Color)_evt.getNewValue();
      }
      repaint();
    }
  }
  @Override
  public void paint(final Graphics _g) {
    super.paint(_g);
    _g.setColor(couleurRemplissage_);
    _g.fillRect(
      rectRemplissage_.x,
      rectRemplissage_.y,
      rectRemplissage_.width,
      rectRemplissage_.height);
    _g.setColor(Color.white);
    if (plan_ == REMPLISSAGE) {
      _g.drawRect(
        rectRemplissage_.x,
        rectRemplissage_.y,
        rectRemplissage_.width,
        rectRemplissage_.height);
    }
    _g.setColor(couleurContour_);
    _g.fillRect(
      rectContour_.x,
      rectContour_.y,
      rectContour_.width,
      rectContour_.height);
    _g.setColor(Color.white);
    if (plan_ == CONTOUR) {
      _g.drawRect(
        rectContour_.x,
        rectContour_.y,
        rectContour_.width,
        rectContour_.height);
    }
  }
  // **********************************************
  // EVENEMENTS
  // **********************************************
  // mouseClicked
  @Override
  public void mouseClicked(final MouseEvent _evt) {
    final int x= _evt.getX();
    final int y= _evt.getY();
    if ((x >= rectContour_.x)
      && (x <= (rectContour_.x + rectContour_.width))
      && (y >= rectContour_.y)
      && (y <= (rectContour_.y + rectContour_.height))) {
      plan_= CONTOUR;
    } else if (
      (x >= rectRemplissage_.x)
        && (x <= (rectRemplissage_.x + rectRemplissage_.width))
        && (y >= rectRemplissage_.y)
        && (y <= (rectRemplissage_.y + rectRemplissage_.height))) {
      plan_= REMPLISSAGE;
    }
    repaint();
  }
  // mousePressed
  @Override
  public void mousePressed(final MouseEvent _e) {}
  // mouseReleased
  @Override
  public void mouseReleased(final MouseEvent _e) {}
  // mouseEntered
  @Override
  public void mouseEntered(final MouseEvent _e) {}
  // mouseExited
  @Override
  public void mouseExited(final MouseEvent _e) {}
}
