/**
 * @creation 21 sept. 2004
 * @modification $Date: 2007-03-09 08:38:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuPanel;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.awt.Dimension;
import javax.swing.JComponent;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreeSelectionModel;
import org.fudaa.ctulu.gui.CtuluComboBoxModelAdapter;
import org.fudaa.ctulu.gui.CtuluListModelEmpty;
import org.fudaa.ebli.commun.BPalettePanelInterface;

/**
 * @author Fred Deniger
 * @version $Id: BSelecteurListComboBox.java,v 1.1 2007-03-09 08:38:20 deniger Exp $
 */
public class BSelecteurListComboBox extends BuPanel implements BPalettePanelInterface, TreeSelectionListener {

  class CbDataListener implements ListDataListener {

    @Override
    public void contentsChanged(final ListDataEvent _e) {

      if (!isUpdatingFromSrc_ && _e.getIndex0() == -1 && _e.getIndex1() == -1 && isTargetUpdatable()) {
        if (Fu.DEBUG && FuLog.isDebug()) {
          FuLog.debug("FMV: palette cb update from cb");
        }
        target_.getListSelectionModel().setSelectionInterval(cb_.getSelectedIndex(), cb_.getSelectedIndex());

      }
    }

    @Override
    public void intervalAdded(final ListDataEvent _e) {}
    @Override
    public void intervalRemoved(final ListDataEvent _e) {}
  }

  class TargetSelectionListener implements ListSelectionListener {

    @Override
    public void valueChanged(final ListSelectionEvent _e) {
      if (isTargetUpdatable()) {
        isUpdatingFromSrc_ = true;
        int index = target_.getListSelectionModel().getMaxSelectionIndex();
        cb_.setSelectedIndex(index);
        isUpdatingFromSrc_ = false;
      }
    }
  }

  /**
   * @param _o l'objet a tester
   * @return true si cet objet est supporte par cette palette
   */
  public static final boolean isTargetValid(final Object _o) {
    return (_o instanceof BSelecteurListTarget);
  }
  CtuluComboBoxModelAdapter adapter_;
  BuComboBox cb_;

  ListDataListener cbListener_;
  ListModel emptyModel_;
  ListSelectionListener listSelectionListener_;
  BSelecteurListTarget target_;

  public boolean isUpdatingFromSrc_;

  public BSelecteurListComboBox() {
   this(null);
    

  }
  
  public BSelecteurListComboBox(Dimension sizeCombo) {
	    setLayout(new BuBorderLayout());
	    cb_ = new BuComboBox();
	    adapter_ = new CtuluComboBoxModelAdapter(CtuluListModelEmpty.EMPTY);
	    cb_.setModel(adapter_);
	    cbListener_ = new CbDataListener();
	    listSelectionListener_ = new TargetSelectionListener();
	    adapter_.addListDataListener(cbListener_);
	    cb_.setEnabled(false);
	    
	    if(sizeCombo!=null){
	    	cb_.setMinimumSize(sizeCombo);
	        cb_.setMaximumSize(sizeCombo);
	        cb_.setSize(sizeCombo);
	    }
	    add(cb_, BuBorderLayout.CENTER);
	    

	  }
  

  boolean isTargetUpdatable() {
    return target_ != null && target_.getListSelectionModel() != null;
  }

  @Override
  public void doAfterDisplay() {}

  public BuComboBox getCb() {
    return cb_;
  }

  @Override
  public void paletteDeactivated() {}

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public boolean setPalettePanelTarget(final Object _target) {
    if (isTargetUpdatable()) {
      target_.getListSelectionModel().removeListSelectionListener(listSelectionListener_);
    }
    boolean ok = false;
    if (isTargetValid(_target)) {
      target_ = (BSelecteurListTarget) _target;
      if (target_.getListModel() != null) {
        adapter_.setModel(target_.getListModel());
        ok = true;
        final ListSelectionModel selection = target_.getListSelectionModel();
        final int idx = selection.getMaxSelectionIndex();
        isUpdatingFromSrc_ = true;
        if (idx >= 0) {
          adapter_.setSelectedItem(target_.getListModel().getElementAt(idx));
        }
        else adapter_.setSelectedItem(null);
        isUpdatingFromSrc_ = false;
        target_.getListSelectionModel().addListSelectionListener(listSelectionListener_);
      }
    } else {
      target_ = null;
    }
    if (!ok) {
      adapter_.setModel(CtuluListModelEmpty.EMPTY);
    }
    cb_.setEnabled(ok);
    cb_.setPreferredSize(cb_.getPreferredSize());
    revalidate();
    return target_ != null;
  }

  @Override
  public void valueChanged(final TreeSelectionEvent _e) {
    final TreeSelectionModel model = (TreeSelectionModel) _e.getSource();
    setPalettePanelTarget(model.isSelectionEmpty() ? null : model.getSelectionPath().getLastPathComponent());

  }

  public static void updateComboPreferredSize(final BSelecteurListComboBox combo) {
    combo.setPreferredSize(new Dimension(Math.max(combo.getPreferredSize().width, 200), combo
        .getPreferredSize().height));
    combo.getCb().setMaximumSize(
        new Dimension(Math.max(combo.getPreferredSize().width, 200), combo.getPreferredSize().height));
    combo.getCb().setMinimumSize(
        new Dimension(Math.max(combo.getPreferredSize().width, 200), combo.getPreferredSize().height));
    combo.getCb().setSize(
        new Dimension(Math.max(combo.getPreferredSize().width, 200), combo.getPreferredSize().height));
    combo.getCb().setPreferredSize(
        new Dimension(Math.max(combo.getPreferredSize().width, 200), combo.getPreferredSize().height));
    combo.revalidate();
  }

}
