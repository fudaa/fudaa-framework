/*
 * @creation 8 nov. 06
 * @modification $Date: 2007-02-15 17:10:11 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;

/**
 * @author fred deniger
 * @version $Id: BSelecteurAbstract.java,v 1.3 2007-02-15 17:10:11 deniger Exp $
 */
public abstract class BSelecteurAbstract implements BSelecteurInterface, PropertyChangeListener {
  public static JComponent[] createComponents(final JComponent _c) {
    return new JComponent[] { _c };
  }

  private final String property_;

  boolean addListenerToTarget_ = true;

  String title_;

  String tooltip_;
  protected boolean isUpdating_;
  protected BSelecteurTargetInterface target_;

  public BSelecteurAbstract(final String _property) {
    super();
    property_ = _property;
  }

  protected boolean getTargetBooleanValue() {
    return getTargetBooleanValue(getProperty());
  }

  protected boolean getTargetBooleanValue(final String _prop) {
    final Object res = getTargetValue(_prop);
    return res == null ? false : ((Boolean) res).booleanValue();
  }

  protected double getTargetDoubleValue() {
    return getTargetDoubleValue(getProperty());
  }

  protected double getAverageTargetDoubleValue() {
    return getAverageTargetDoubleValue(getProperty());
  }

  protected double getMinTargetDoubleValue() {
    return getMinTargetDoubleValue(getProperty());
  }

  protected double getTargetDoubleValue(final String _prop) {
    final Object res = getTargetValue(_prop);
    return res == null ? 0 : ((Number) res).doubleValue();
  }

  protected double getAverageTargetDoubleValue(final String _prop) {
    final Object res = getAverageTargetValue(_prop);
    return res == null ? 0 : ((Number) res).doubleValue();
  }

  protected double getMinTargetDoubleValue(final String _prop) {
    final Object res = getMinTargetValue(_prop);
    return res == null ? 0 : ((Number) res).doubleValue();
  }

  protected float getTargetFloatValue() {
    return getTargetFloatValue(getProperty());
  }

  protected float getTargetFloatValue(final String _prop) {
    final Object res = getTargetValue(_prop);
    return res == null ? 0 : ((Number) res).floatValue();
  }

  protected int getTargetIntValue() {
    return getTargetIntValue(getProperty());
  }

  protected int getTargetIntValue(final String _prop) {
    final Object res = getTargetValue(_prop);
    return res == null ? 0 : ((Number) res).intValue();
  }

  protected String getTargetStringValue() {
    return getTargetStringValue(getProperty());
  }

  protected String getTargetStringValue(final String _prop) {
    return (String) getTargetValue(_prop);
  }

  protected Object getTargetValue() {
    return getTargetValue(getProperty());
  }

  protected Object getTargetValue(final String _prop) {
    return target_ == null ? null : target_.getProperty(_prop);
  }

  protected Object getAverageTargetValue(final String _prop) {
    return target_ == null ? null : target_.getMoy(_prop);
  }

  protected Object getMinTargetValue(final String _prop) {
    return target_ == null ? null : target_.getMin(_prop);
  }

  protected void updateTarget(final BSelecteurTargetInterface _target) {
    final String prop = getProperty();
    if (addListenerToTarget_ && prop != null && target_ != null) {
      target_.removePropertyChangeListener(prop, this);
    }
    target_ = _target;
    if (addListenerToTarget_ && prop != null && _target != null) {
      _target.addPropertyChangeListener(prop, this);
    }
    updateFromTarget();
  }

  /**
   * Methode utilitaire testant si on n'est pas en train de mettre � jour depuis la cible et si la cible est non null
   * avant de lui transmettre la nouvelle valeur.
   * 
   * @param _prop le propri�t�s a modifier
   * @param _newVal la nouvelle valeur.
   */
  public void firePropertyChange(final String _prop, final Object _newVal) {
    if (!isUpdating_ && target_ != null) {
      target_.setProperty(_prop, _newVal);
    }
  }

  @Override
  public final String getProperty() {
    return property_;
  }

  @Override
  public String getTitle() {
    return title_;
  }

  @Override
  public String getTooltip() {
    return tooltip_ == null ? getTitle() : tooltip_;
  }

  public boolean isAddListenerToTarget() {
    return addListenerToTarget_;
  }

  @Override
  public boolean needAllWidth() {
    return false;
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    isUpdating_ = true;
    updateFromTarget();
    isUpdating_ = false;
  }

  @Override
  public void reupdateFromTarget() {
    isUpdating_ = true;
    updateFromTarget();
    isUpdating_ = false;

  }

  public void setAddListenerToTarget(final boolean _addListenerToTarget) {
    addListenerToTarget_ = _addListenerToTarget;
  }

  public BSelecteurTargetInterface getTarget() {
    return target_;
  }

  @Override
  public void setEnabled(final boolean _b) {
    final JComponent[] cs = getComponents();
    if (cs != null) {
      for (int i = 0; i < cs.length; i++) {
        if (cs[i] != null) {
          cs[i].setEnabled(_b);
        }
      }
    }

  }

  @Override
  public void setSelecteurTarget(final BSelecteurTargetInterface _target) {
    if (_target != target_) {
      isUpdating_ = true;
      updateTarget(_target);
      isUpdating_ = false;
    }
  }

  public void setTitle(final String _title) {
    title_ = _title;
  }

  public void setTooltip(final String _tooltip) {
    tooltip_ = _tooltip;
  }

  public abstract void updateFromTarget();

}
