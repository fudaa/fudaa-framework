package org.fudaa.ebli.controle;

import java.util.Map;

/**
 * BConfigurePaletteTargetInterface custom qui permet de rejouer les donn�es de sa palette en fonction des modifications.
 * Utile dans le cas d'ajout de BSelecteurInterface en cours de route.
 * @author Adrien Hadoux
 *
 */
public interface BConfigurePaletteTargetInterfaceReload extends BConfigurePaletteTargetInterface{
	
	public BConfigurePalette getPalette();
	public void setPalette(BConfigurePalette pal);
	
	/**
	 * Recharger la palette en cas de necessit�.
	 * @param infos map qui peut contenir des infos sur le BSelecteurInterface a initialiser...
	 * @return
	 */
	public boolean reloadPalette(Map infos);
	

}
