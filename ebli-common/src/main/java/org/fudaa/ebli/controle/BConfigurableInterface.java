/*
 * @creation 10 nov. 06
 * @modification $Date: 2008-02-27 17:15:40 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

/**
 * Une interface contenant les sections configurables pour la palette de configuration du calque.
 * @author fred deniger
 * @version $Id: BConfigurableInterface.java,v 1.1.6.1 2008-02-27 17:15:40 bmarchan Exp $
 */
public interface BConfigurableInterface extends BConfigurableSectionInterface {

  @Override
  String getTitle();

  BConfigurableInterface[] getSections();

}
