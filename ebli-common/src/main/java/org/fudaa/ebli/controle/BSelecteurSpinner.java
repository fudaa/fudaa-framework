/*
 * @creation 8 nov. 06
 * @modification $Date: 2007-06-05 08:58:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import javax.swing.JComponent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author fred deniger
 * @version $Id: BSelecteurSpinner.java,v 1.5 2007-06-05 08:58:39 deniger Exp $
 */
public class BSelecteurSpinner extends BSelecteurAbstract implements ChangeListener {

  final JSpinner spinner_;

  boolean unableIfNull_;

  public BSelecteurSpinner(final String _property) {
    this(_property, new JSpinner(new SpinnerNumberModel()));
  }

  public BSelecteurSpinner(final String _property, final JSpinner _slider) {
    super(_property);
    spinner_ = _slider;
    spinner_.getModel().addChangeListener(this);
  }

  static final Number DEFAULT_NUMBER = new Integer(0);

  @Override
  public void updateFromTarget() {
    Object targetValue = getTargetValue();
    // spinner_.setEnabled(true);
    boolean isNull = false;
    if (targetValue == null) {
      targetValue = DEFAULT_NUMBER;
      isNull = true;
    }
    spinner_.setValue(targetValue);
    if(unableIfNull_){
      spinner_.setEnabled(!isNull);
    }

  }

  @Override
  public JComponent[] getComponents() {
    return new JComponent[] { spinner_ };
  }

  @Override
  public void stateChanged(final ChangeEvent _e) {
    firePropertyChange(getProperty(), spinner_.getValue());
  }

  public boolean isUnableIfNull() {
    return unableIfNull_;
  }

  public void setUnableIfNull(final boolean _unableIfNull) {
    unableIfNull_ = _unableIfNull;
  }

  public JSpinner getSpinner() {
    return spinner_;
  }
}
