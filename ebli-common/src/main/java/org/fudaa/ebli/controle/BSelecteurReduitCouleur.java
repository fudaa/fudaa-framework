/*
 * @creation     1998-06-26
 * @modification $Date: 2007-05-04 13:49:44 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.controle;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JComponent;
import org.fudaa.ebli.trace.FiltreDesaturation;

/**
 * Un selecteur reduit (dans une palette) de couleur.
 *
 * @version $Revision: 1.3 $ $Date: 2007-05-04 13:49:44 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class BSelecteurReduitCouleur extends JComponent implements MouseListener {
  final static int COLS = 7; // =18;

  final static int ROWS = 9; // =14;

  private static final Color[] COULEURS=new Color[] { new Color(0, 0, 0), new Color(64, 64, 64), new Color(128, 128, 128),
    new Color(160, 160, 160), new Color(192, 192, 192), new Color(224, 224, 224), new Color(255, 255, 255),
    new Color(64, 0, 0), new Color(128, 0, 0), new Color(192, 0, 0), new Color(255, 0, 0),
    new Color(255, 64, 64), new Color(255, 128, 128), new Color(255, 192, 192), new Color(0, 64, 0),
    new Color(0, 128, 0), new Color(0, 192, 0), new Color(0, 255, 0), new Color(64, 255, 64),
    new Color(128, 255, 128), new Color(192, 255, 192), new Color(0, 0, 64), new Color(0, 0, 128),
    new Color(0, 0, 192), new Color(0, 0, 255), new Color(64, 64, 255), new Color(128, 128, 255),
    new Color(192, 192, 255), new Color(64, 64, 0), new Color(128, 128, 0), new Color(192, 192, 0),
    new Color(255, 255, 0), new Color(255, 255, 64), new Color(255, 255, 128), new Color(255, 255, 192),
    new Color(64, 0, 64), new Color(128, 0, 128), new Color(192, 0, 192), new Color(255, 0, 255),
    new Color(255, 64, 255), new Color(255, 128, 255), new Color(255, 192, 255), new Color(0, 64, 64),
    new Color(0, 128, 128), new Color(0, 192, 192), new Color(0, 255, 255), new Color(64, 255, 255),
    new Color(128, 255, 255), new Color(192, 255, 255), new Color(128, 64, 0), new Color(0, 128, 64),
    new Color(64, 0, 128), new Color(128, 0, 64), new Color(64, 128, 0), new Color(0, 64, 128), null,
    new Color(255, 128, 0), new Color(0, 255, 128), new Color(128, 0, 255), new Color(255, 0, 128),
    new Color(128, 255, 0), new Color(0, 128, 255), null, };;

  public BSelecteurReduitCouleur() {
    super();
    indice_ = 0;
    propertyName_ = "couleur";
    final Dimension d = new Dimension(COLS * 12 + 1, ROWS * 12 + 1);
    setMinimumSize(d);
    setPreferredSize(d);
    setDoubleBuffered(true);
    setOpaque(true);
    addMouseListener(this);
  }


  @Override
  public void paint(final Graphics _g) {
    final Dimension dim = getSize();
    final Insets f = getInsets();
    int w, h, x, y;
    final boolean e = isEnabled();
    w = dim.width - f.left - f.right;
    h = dim.height - f.top - f.bottom;
    int i;
    for (i = 0; i < COULEURS.length; i++) {
      x = (i % COLS) * w / COLS;
      y = (i / COLS) * h / ROWS;
      if (COULEURS[i] == null) {
        _g.setColor(getBackground());
      } else {
        _g.setColor(e ? COULEURS[i] : FiltreDesaturation.desatureCouleur(COULEURS[i]));
      }
      _g.fillRect(f.left + x, f.top + y, w / COLS, h / ROWS);
      _g.setColor(e ? Color.black : getBackground().darker());
      _g.drawRect(f.left + x, f.top + y, w / COLS, h / ROWS);
      if (COULEURS[i] == null) {
        _g.drawLine(f.left + x + 2, f.top + y + 2, f.left + x + w / COLS - 2, f.top + y + h / ROWS - 2);
        _g.drawLine(f.left + x + 2, f.top + y + h / ROWS - 2, f.left + x + w / COLS - 2, f.top + y + 2);
      }
    }
    super.paint(_g);
  }

  // Propriete couleur
  private String propertyName_;

  public String getPropertyName() {
    return propertyName_;
  }

  public void setPropertyName(final String _propertyName) {
    propertyName_ = _propertyName;
  }

  private int indice_;

  public Color getCouleur() {
    return COULEURS[indice_];
  }

  public void setCouleur(final Color _couleur) {
    // Color vp=couleurs_[indice_];
    int i;
    for (i = 0; i < COULEURS.length; i++) {
      if ((COULEURS[i] != null) && (COULEURS[i].equals(_couleur))) {
        indice_ = i;
        firePropertyChange(getPropertyName(), null, _couleur);
        break;
      }
    }
  }

  @Override
  public void mouseClicked(final MouseEvent _ev) {
    final Dimension dim = getSize();
    final Insets ins = getInsets();
    int w, h, x, y;
    w = dim.width - ins.left - ins.right - 1;
    h = dim.height - ins.top - ins.bottom - 1;
    x = _ev.getX() - ins.left;
    y = _ev.getY() - ins.top;
    if ((x >= 0) && (x < w) && (y >= 0) && (y < h)) {
      final int i = (x * COLS / w) + (y * ROWS / h) * COLS;
      if ((i < COULEURS.length) && (COULEURS[i] != null)) {
        setCouleur(COULEURS[i]);
      }
    }
  }

  @Override
  public void mouseEntered(final MouseEvent _ev) {}

  @Override
  public void mouseExited(final MouseEvent _ev) {}

  @Override
  public void mousePressed(final MouseEvent _ev) {}

  @Override
  public void mouseReleased(final MouseEvent _ev) {}
}
