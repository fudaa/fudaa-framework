/*
 * @creation 9 ao�t 2004
 * @modification $Date: 2006-09-19 14:55:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * Une checkbox pour controler l'affichage d'un composant. Ce composant recoit les changements de proprietes ("visible")
 * pour se mettre a jour. Une reference est gardee vers le composant cible.
 * 
 * @author Fred Deniger
 * @version $Id: BControleVisible.java,v 1.7 2006-09-19 14:55:53 deniger Exp $
 */
public class BControleVisible extends BuCheckBox implements PropertyChangeListener, ActionListener {

  private JComponent target_;

  public BControleVisible() {
    super();
    addActionListener(this);
  }

  public void setTarget(final JComponent _c) {
    if (_c != target_) {
      if (target_ != null) {
        target_.removePropertyChangeListener(this);
      }
      target_ = _c;
      if (target_ != null) {
        setSelected(target_.isVisible());
        target_.addPropertyChangeListener("visible", this);
      }
    }
    setEnabled(target_ != null);
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (target_ != null) {
      target_.setVisible(isSelected());
    }
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    if (_evt.getSource() == target_) {
      setSelected(target_.isVisible());
    }

  }

  /**
   * Panneau permettant de controler l'etat de visibilite d'un composant.
   * 
   * @return un panel avec un label et une checkbox.
   */
  public static BControleVisible buildPanelVisible(final JPanel _dest) {
    _dest.removeAll();
    _dest.setLayout(new BuGridLayout(2));
    _dest.add(new BuLabel(BuResource.BU.getString("Visible")));
    final BControleVisible r = new BControleVisible();
    _dest.add(r);
    return r;
  }
}
