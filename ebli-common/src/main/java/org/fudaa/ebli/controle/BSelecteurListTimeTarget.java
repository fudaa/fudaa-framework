/**
 * @creation 21 sept. 2004
 * @modification $Date: 2007-03-09 08:38:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import javax.swing.ListModel;
import javax.swing.ListSelectionModel;

/**
 * @author Fred Deniger
 * @version $Id: BSelecteurListTimeTarget.java,v 1.1 2007-03-09 08:38:20 deniger Exp $
 */
public interface BSelecteurListTimeTarget {

  ListModel getTimeListModel();

  ListSelectionModel getTimeListSelectionModel();

}