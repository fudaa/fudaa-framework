/*
 * @creation     1998-06-26
 * @modification $Date: 2006-11-14 09:06:32 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.controle;
import java.beans.SimpleBeanInfo;
/**
 * BeanInfo de BSelecteurCouleur.
 *
 * @version      $Revision: 1.1 $ $Date: 2006-11-14 09:06:32 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class BSelecteurCouleurBeanInfo extends SimpleBeanInfo {}
