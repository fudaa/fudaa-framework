/**
 * @creation 21 sept. 2004
 * @modification $Date: 2007-03-09 08:38:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuPanel;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.Position;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluListModelSelectionEmpty;
import org.fudaa.ctulu.gui.CtuluListModelEmpty;
import org.fudaa.ebli.commun.BPalettePanelInterface;

/**
 * @author Fred Deniger
 * @version $Id: BSelecteurList.java,v 1.1 2007-03-09 08:38:20 deniger Exp $
 */
public class BSelecteurList extends BuPanel implements BPalettePanelInterface {

  /**
   * @param _o l'objet a tester
   * @return true si cet objet est supporte par cette palette
   */
  public static final boolean isTargetValid(final Object _o) {
    return (_o instanceof BSelecteurListTarget);
  }

  ListModel emptyModel_;

  CustomList list_;

  BSelecteurListTarget target_;
  // BControleVisible visible_;

  public BSelecteurListTarget getTarget_() {
    return target_;
  }

  static class SpecificHandler implements KeyListener {

    private String prefix_ = CtuluLibString.EMPTY_STRING;
    private long lastTime_;

    @Override
    public void keyPressed(final KeyEvent _e) {}

    @Override
    public void keyReleased(final KeyEvent _e) {}

    /**
     * Invoked when a key has been typed. Moves the keyboard focus to the first element whose first letter matches the
     * alphanumeric key pressed by the user. Subsequent same key presses move the keyboard focus to the next object that
     * starts with the same letter.
     */
    @Override
    public void keyTyped(final KeyEvent _e) {
      final JList src = (JList) _e.getSource();
      final ListModel model = src.getModel();

      if (model.getSize() == 0 || _e.isAltDown() || _e.isControlDown() || _e.isMetaDown()) {
        // Nothing to select
        return;
      }
      final char c = _e.getKeyChar();
      // les raccourcis pour les vues
      if (c >= 'a' && c <= 'z') {
        return;
      }
      boolean startFrom = true;
      final long time = _e.getWhen();
      int startIndex;
      if (time - lastTime_ < 1000L && (prefix_.length() != 1 || c != prefix_.charAt(0))) {
        prefix_ += c;
        startIndex = src.getSelectedIndex();
      } else {
        prefix_ = "" + c;
        startIndex = src.getSelectedIndex() + 1;
      }
      lastTime_ = time;

      if (startIndex < 0 || startIndex >= model.getSize()) {
        startFrom = false;
        startIndex = 0;
      }
      int index = src.getNextMatch(prefix_, startIndex, Position.Bias.Forward);
      if (index >= 0) {
        src.setSelectedIndex(index);
      } else if (startFrom) { // wrap
        index = src.getNextMatch(prefix_, 0, Position.Bias.Forward);
        if (index >= 0) {
          src.setSelectedIndex(index);
        }
      }
      _e.consume();
    }
  }

  public static class ListUpdater implements Runnable {
    final JList l_;

    public ListUpdater(final JList _l) {
      super();
      l_ = _l;
    }

    @Override
    public void run() {
      l_.ensureIndexIsVisible(l_.getSelectedIndex());
    }
  }

  static class CustomList extends JList implements ListSelectionListener {
    ListUpdater up_;

    @Override
    public void valueChanged(final ListSelectionEvent _e) {
      if (!_e.getValueIsAdjusting()) {
        showSelected();

      }
    }

    protected void showSelected() {
      if (up_ == null) {
        up_ = new ListUpdater(this);
      }
      BuLib.invokeLater(up_);
    }

    private void updateListener() {
      final KeyListener[] l = getKeyListeners();
      if (l != null) {
        for (int i = l.length - 1; i >= 0; i--) {
          removeKeyListener(l[i]);
        }
      }
      addKeyListener(new SpecificHandler());

    }

    @Override
    public void setSelectionModel(final ListSelectionModel _selectionModel) {
      // ne pas mettre getSelectionModel dans une variable !
      if (getSelectionModel() != null) {
        getSelectionModel().removeListSelectionListener(this);
      }
      super.setSelectionModel(_selectionModel);
      if (getSelectionModel() != null) {
        getSelectionModel().addListSelectionListener(this);
      }
    }

    @Override
    public void updateUI() {
      super.updateUI();
      updateListener();
    }

  }

  public BSelecteurList() {
    setLayout(new BuBorderLayout());
    list_ = new CustomList();
    list_.setRequestFocusEnabled(true);
    add(new JScrollPane(list_), BuBorderLayout.CENTER);

  }

  @Override
  public void doAfterDisplay() {
    list_.showSelected();
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public boolean setPalettePanelTarget(final Object _target) {
    list_.setSelectionModel(CtuluListModelSelectionEmpty.EMPTY);
    list_.setModel(CtuluListModelEmpty.EMPTY);
    if (isTargetValid(_target)) {
      target_ = (BSelecteurListTarget) _target;
      if (target_.getListModel() != null) {
        list_.setModel(target_.getListModel());
        list_.setSelectionModel(target_.getListSelectionModel());
      }
      repaint();
    } else {
      target_ = null;
    }
    revalidate();
    return target_ != null;

  }

  @Override
  public void paletteDeactivated() {}

}
