/*
 * @creation 3 ao�t 2004
 * @modification $Date: 2007-05-04 13:49:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuComboBox;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.JComponent;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.ebli.trace.TraceTraitRenderer;

/**
 * Un selecteur pour l'epaisseur des traits. Les modifications/mises a jour sont envoye via les evt
 * PropertyChangeListener la propriete a pour nom "epaisseurTrait" et les type de donnees doivent etre sous forme de
 * float ou String.
 * 
 * @author Fred Deniger
 * @version $Id: BSelecteurEpaisseurTrait.java,v 1.2 2007-05-04 13:49:44 deniger Exp $
 */
public class BSelecteurEpaisseurTrait extends BSelecteurAbstract implements /*PropertyChangeListener, */ItemListener {

  public class TraitList extends AbstractListModel implements ComboBoxModel {

    Object selected_;

    @Override
    public Object getElementAt(final int _index) {
      return taille_ == null ? null : taille_[_index];
    }

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public int getSize() {
      return taille_ == null ? 0 : taille_.length;
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != selected_) {
        selected_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }

    }
  }

  /**
   * La propriete utilisee pour envoyee un evt lorsque l'epaisseur du trait est modifiee.
   */
  public static final String PROPERTY = "epaisseurTrait";

  BuComboBox cb_;
  TraceLigneModel[] taille_;

  public BSelecteurEpaisseurTrait() {
    super(PROPERTY);
    setDefaultTailles();
  }

  private int getDataWithEpaisseur(final float _f) {
    for (int i = taille_.length - 1; i >= 0; i--) {
      if (taille_[i].getEpaisseur() == _f) {
        return i;
      }
    }
    return -1;
  }

  private float getSelectedTaille() {
    return taille_[cb_.getSelectedIndex()].getEpaisseur();
  }

  private boolean isAlreadySet() {
    final int idx = cb_.getSelectedIndex();
    if (idx >= 0) {
      final TraceLigneModel d = taille_[idx];
      if (d.getEpaisseur() == getSelectedTaille()) {
        return true;
      }
    }
    return false;
  }

  private void setDefaultTailles() {
    taille_ = new TraceLigneModel[6];
    for (int i = 0; i < taille_.length; i++) {
      taille_[i] = new TraceLigneModel();
      taille_[i].setEpaisseur(1f + 0.5f * i);
    }
  }

  /**
   * @return l'epaisseur en cours
   */
  protected float getEpaisseurTraitFromTarget() {
    final Object o = getTargetValue(PROPERTY);
    return o == null ? 0 : ((Number) o).floatValue();
  }

  @Override
  public void updateFromTarget() {
    if (cb_ == null) {
      return;
    }
    cb_.setEnabled(target_ != null);
    if (target_ == null || isAlreadySet()) {
      return;
    }
    int idx = getDataWithEpaisseur(getEpaisseurTraitFromTarget());
    if (idx < 0) {
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("EBL: taille non supportee");
      }
      idx = 0;
    }
    cb_.setSelectedIndex(idx);
  }

  @Override
  public JComponent[] getComponents() {
    if (cb_ == null) {
      cb_ = new BuComboBox();
      setDefaultTailles();
      cb_.setModel(new TraitList());
      cb_.addItemListener(this);
      cb_.setRenderer(new TraceTraitRenderer());
      cb_.setPreferredSize(new Dimension(50, 15));
      updateFromTarget();
    }
    return createComponents(cb_);
  }

  /**
   * Mise a jour de la cible.
   */
  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if ((target_ != null) && (_e.getStateChange() == ItemEvent.SELECTED)) {
      super.firePropertyChange(PROPERTY, new Float(getSelectedTaille()));
    }
  }

}
