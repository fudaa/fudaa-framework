/*
 *  @creation     10 Mars 2009
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.trace.TraceSurface;
import org.fudaa.ebli.trace.TraceSurfaceChooser;
import org.fudaa.ebli.trace.TraceSurfaceModel;

/**
 * Un selecteur pour un modele de surface. Seule la couleur de fond du trac� est g�r�e.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class BSelecteurSurfaceModel extends BSelecteurAbstract implements ItemListener, ChangeListener {

  public static final String DEFAULT_PROPERTY = "surfaceModel";

  public static final String getProperty(final int _idx) {
    return _idx == 0 ? DEFAULT_PROPERTY : DEFAULT_PROPERTY + _idx;
  }
  BSelecteurColorChooserBt btColor_;
  BuComboBox cbType_;
  TraceSurfaceModel old_;

  TraceSurfaceChooser typeModel_;

  protected class BSelecteurColorTarget implements BSelecteurTargetInterface {
    Color currentColor_;

    @Override
    public void addPropertyChangeListener(final String _key, final PropertyChangeListener _l) {}

    @Override
    public Object getMin(final String _key) {
      return null;
    }

    @Override
    public Object getMoy(final String _key) {
      return null;
    }

    @Override
    public Object getProperty(final String _key) {
      return currentColor_;
    }

    @Override
    public void removePropertyChangeListener(final String _key, final PropertyChangeListener _l) {}

    @Override
    public boolean setProperty(final String _key, final Object _newProp) {
      currentColor_ = (Color) _newProp;
      fireDataChanged(false, true);
      return true;
    }

  }

  public BSelecteurSurfaceModel() {
    this(DEFAULT_PROPERTY, null);
  }

  public BSelecteurSurfaceModel(final String _prop) {
    this(_prop, null);
  }

  public BSelecteurSurfaceModel(final String _property, final TraceSurfaceModel _tsm) {
    super(_property);
    cbType_ = new BuComboBox();

    typeModel_ = new TraceSurfaceChooser(true);
    cbType_.setModel(typeModel_);
    cbType_.setRenderer(typeModel_.getCellRenderer());
    cbType_.setPreferredSize(new Dimension(60, 15));
    cbType_.addItemListener(this);
    btColor_ = new BSelecteurColorChooserBt();
    final BSelecteurColorTarget selecteurColorTarget = new BSelecteurColorTarget();
    if (_tsm != null) {
      selecteurColorTarget.currentColor_ = _tsm.getBgColor();
      typeModel_.setSelectedType(_tsm.getType());
    }
    btColor_.setSelecteurTarget(selecteurColorTarget);
    title_ = EbliLib.getS("Surface");
  }

  protected void fireDataChanged(final boolean _type, final boolean _color) {
    if (isUpdating_) {
      return;
    }
    if (FuLog.isTrace()) {
      FuLog.trace("ETR: " + getClass().getName() + " data change type=" + _type 
          + "; color=" + _color);
    }
    final TraceSurfaceModel d = getNewData();
    if (_type) {
      d.setBgColorIgnored();
    } else if (_color) {
      d.setTypeIgnored();
    }
    if (!d.equals(old_)) {
      if (FuLog.isTrace()) {
        FuLog.trace("new trace " + d);
      }
      firePropertyChange(getProperty(), d);
    }
  }

  @Override
  public void updateFromTarget() {
    final TraceSurfaceModel tsm = (TraceSurfaceModel) super.getTargetValue();
    isUpdating_ = true;
    old_ = tsm;
    final int type = tsm == null ? 0 : tsm.getType();
    if (type >= 0) {
      typeModel_.setSelectedType(type);
    } else {
      cbType_.setSelectedItem(null);
    }
    final BSelecteurColorTarget colorTarget = (BSelecteurColorTarget) btColor_.getTarget();
    colorTarget.currentColor_ = (tsm == null ? Color.BLACK : tsm.getBgColor());
    btColor_.updateFromTarget();

  }

  public JPanel buildPanel() {
    final JPanel r = new BuPanel();
    buildPanel(r, true);
    return r;
  }

  public void buildPanel(final JPanel _pn, final boolean _layout) {
    if (_layout) {
      _pn.setLayout(new BuGridLayout(2, 3, 3, true, false));
    }
    _pn.add(cbType_);
    _pn.add(btColor_.getPn());

  }

  public JComponent getComponent() {
    return buildPanel();
  }

  @Override
  public JComponent[] getComponents() {
    return new JComponent[] { cbType_, btColor_.getPn() };
  }

  public TraceSurfaceModel getNewData() {
    return new TraceSurfaceModel(typeModel_.getSelectedType(),
        ((BSelecteurColorTarget) btColor_.getTarget()).currentColor_,Color.WHITE);
  }

  public final TraceSurfaceChooser getTypeModel() {
    return typeModel_;
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if (_e.getStateChange() == ItemEvent.DESELECTED) {
      return;
    }
    final boolean enable = typeModel_.getSelectedType() != TraceSurface.INVISIBLE;
    btColor_.setEnabled(enable);
    fireDataChanged(true, false);
  }

  @Override
  public void stateChanged(final ChangeEvent _e) {
    fireDataChanged(false, false);
  }

}
