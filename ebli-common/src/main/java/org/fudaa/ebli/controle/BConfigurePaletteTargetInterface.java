/*
 * @creation 10 nov. 06
 * @modification $Date: 2006-12-05 10:14:37 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

/**
 * @author fred deniger
 * @version $Id: BConfigurePaletteTargetInterface.java,v 1.2 2006-12-05 10:14:37 deniger Exp $
 */
public interface BConfigurePaletteTargetInterface {

  BSelecteurTargetInterface getVisibleTitleTarget();

  boolean isTitleModifiable();

  /**
   * @return les interfaces de configuration
   */
  BConfigurableInterface[] getConfigureInterfaces();

}
