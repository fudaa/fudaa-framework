/*
 * @creation 5 d�c. 2003
 * @modification $Date: 2007-05-04 13:49:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuButtonLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.fu.FuLog;
import ghm.followgui.FontSelectionPanel;
import ghm.followgui.FontSelectionPanel.InvalidFontException;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author deniger
 * @version $Id: BSelecteurReduitFonteNewVersion.java,v 1.4 2007-05-04 13:49:44 deniger Exp $
 */
public class BSelecteurReduitFonteNewVersion extends BSelecteurAbstract implements ActionListener/*, BSelecteurInterface */{

  protected static class ComponentAdapter implements BSelecteurTargetInterface {

    final Component c_;

    /**
     * @param _c
     */
    public ComponentAdapter(final Component _c) {
      super();
      c_ = _c;
    }

    @Override
    public Object getMoy(final String _key) {
      return getProperty(_key);
    }

    @Override
    public Object getMin(final String _key) {
      return getProperty(_key);
    }

    @Override
    public void addPropertyChangeListener(final String _key, final PropertyChangeListener _l) {
      c_.addPropertyChangeListener(_key, _l);
    }

    public Font getFont() {
      return c_.getFont();
    }

    @Override
    public Object getProperty(final String _key) {
      return c_.getFont();
    }

    public boolean isFontModifiable() {
      return true;
    }

    @Override
    public void removePropertyChangeListener(final String _key, final PropertyChangeListener _l) {
      c_.removePropertyChangeListener(_key, _l);

    }

    public void setFont(final Font _f) {
      c_.setFont(_f);
    }

    @Override
    public boolean setProperty(final String _key, final Object _newProp) {
      if (_newProp != null) {
        c_.setFont((Font) _newProp);
        return true;
      }
      return false;
    }

  }

  public static final String PROPERTY = "font";

  BuButton apply_;

  Dialog d_;

  Font default_;

  FontSelectionPanel fontselection_;

  BuPanel pn_;

  /**
   * Constructeur avec font=null.
   */
  public BSelecteurReduitFonteNewVersion() {
    this(null);
  }

  public BSelecteurReduitFonteNewVersion(final Font _f) {
    this(_f, null);
  }

  /**
   * Initialise la font.
   */
  public BSelecteurReduitFonteNewVersion(final Font _f, final Dialog _toDispose) {
    this(PROPERTY, _f, _toDispose);
  }

  public BSelecteurReduitFonteNewVersion(final String _prop, final Font _f, final Dialog _toDispose) {
    super(_prop);
    default_ = _f;
    d_ = _toDispose;

  }

  @Override
  public void updateFromTarget() {
    if (fontselection_ != null) {
      fontselection_.setFont((Font) getTargetValue());
      apply_.setEnabled(target_ != null);
      fontselection_.setEnabled(target_ != null);
    }
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final String c = _e.getActionCommand();
    if ("INIT".equals(c)) {
      if (default_ != null) {
        setSelectedFont(default_);
      }
    } else if ("APPLY".equals(c)) {
      if (target_ != null) {
        firePropertyChange(getProperty(), getSelectedFont());
      }
    } else if ("CLOSE".equals(c) && (d_ != null)) {
      d_.dispose();
    }
  }

  public Font getSelectedFont() {
    try {
      return fontselection_.getSelectedFont();
    } catch (final InvalidFontException _evt) {
      FuLog.error(_evt);
      return (Font) target_.getProperty(getProperty());

    }
  }

  @Override
  public JComponent[] getComponents() {
    return BSelecteurAbstract.createComponents(getComponent());
  }

  public JComponent getComponent() {
    if (pn_ == null) {
      pn_ = new BuPanel(new BuBorderLayout());
      fontselection_ = new FontSelectionPanel(default_, new String[] { EbliLib.getS("Normal"), EbliLib.getS("Gras"),
          EbliLib.getS("Italique"), EbliLib.getS("Gras Italique") }, new int[] { 8, 10, 12, 14, 16, 18, 24, 36 });
      pn_.add(fontselection_, BuBorderLayout.CENTER);
      final BuPanel bt = new BuPanel();
      bt.setLayout(new BuButtonLayout());
      apply_ = new BuButton(BuResource.BU.getString("Appliquer"));
      apply_.setIcon(BuResource.BU.getToolIcon("appliquer"));
      apply_.setActionCommand("APPLY");
      apply_.addActionListener(this);
      /*
       * BuButton init = new BuButton(BuResource.BU.getString("initialiser")); init.setActionCommand("INIT");
       * init.setIcon(BuResource.BU.getToolIcon("effacer")); init.addActionListener(this);
       */
      bt.add(apply_);
      if (d_ != null) {
        final BuButton dispose = new BuButton(BuResource.BU.getString("fermer"));
        dispose.setActionCommand("CLOSE");
        dispose.setIcon(BuResource.BU.getToolIcon("fermer"));
        dispose.addActionListener(this);
        bt.add(dispose);
      }
      // bt.add(init);
      pn_.add(bt, BuBorderLayout.SOUTH);
    }
    return pn_;
  }

  public void selecteurDeactivated() {}

  public void setSelectedFont(final Font _font) {
    fontselection_.setSelectedFont(_font);
    default_ = _font;
  }

  @Override
  public void setSelecteurTarget(final BSelecteurTargetInterface _target) {
    if (target_ != _target) {
      if (_target instanceof Component) {
        super.setSelecteurTarget(new ComponentAdapter((Component) _target));
      } else {
        super.setSelecteurTarget(_target);
      }
    }
  }

  public void setTarget(final Component _c) {
    super.setSelecteurTarget(new ComponentAdapter(_c));
  }

}
