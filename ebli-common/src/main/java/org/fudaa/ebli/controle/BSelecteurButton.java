/*
 * @creation 8 nov. 06
 * @modification $Date: 2007-05-04 13:49:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.JComponent;

/**
 * @author fred deniger
 * @version $Id: BSelecteurButton.java,v 1.4 2007-05-04 13:49:44 deniger Exp $
 */
public class BSelecteurButton extends BSelecteurAbstract implements ActionListener {

  final AbstractButton bt_;
  /**
   * Utiliser pour demander au bouton de r�cup�rer les valeurs par d�faut des cibles et les renvoyer.
   */
  boolean updateWithMinValue_;

  public BSelecteurButton(final String _property, final AbstractButton _bt) {
    super(_property);
    bt_ = _bt;
    bt_.addActionListener(this);
    setAddListenerToTarget(false);
  }

  public BSelecteurButton(final String _property, final String _text) {
    this(_property, new BuButton(_text));

  }

  @Override
  public void updateFromTarget() {}

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (updateWithMinValue_) {
      firePropertyChange(getProperty(), target_ == null ? null : target_.getMin(getProperty()));
    } else {
      firePropertyChange(getProperty(), Boolean.TRUE);
    }

  }

  @Override
  public JComponent[] getComponents() {
    return super.createComponents(bt_);
  }

  public boolean isUpdateWithMinValue() {
    return updateWithMinValue_;
  }

  public void setUpdateWithMinValue(final boolean _updateMoyValue) {
    updateWithMinValue_ = _updateMoyValue;
  }

  public AbstractButton getBt() {
    return bt_;
  }

}
