/*
 *  @creation     13 juin 2005
 *  @modification $Date: 2008-02-27 17:15:40 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

/**
 * Une interface pour une section de configuration contenue dans le panneau de configuration d'un calque.
 * @author Fred Deniger
 * @version $Id: BConfigurableSectionInterface.java,v 1.1.6.1 2008-02-27 17:15:40 bmarchan Exp $
 */
public interface BConfigurableSectionInterface {

  /**
   * @return Les différents sélecteurs de la section de configuration.
   */
  BSelecteurInterface[] createSelecteurs();

  /**
   * @return Le calque cible de la section de configuration.
   */
  BSelecteurTargetInterface getTarget();

  void stopConfiguration();

  /**
   * @return Le titre de la section de configuration.
   */
  String getTitle();

}
