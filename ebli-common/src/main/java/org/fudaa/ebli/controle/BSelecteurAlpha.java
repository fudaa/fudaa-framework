/*
 * @creation 7 ao�t 06
 * 
 * @modification $Date: 2007-05-04 13:49:44 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import java.awt.Dimension;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author fred deniger
 * @version $Id: BSelecteurAlpha.java,v 1.4 2007-05-04 13:49:44 deniger Exp $
 */
public class BSelecteurAlpha extends BSelecteurSlider {

  public static final String DEFAULT_PROPERTY = "alpha";

  public BSelecteurAlpha() {
    this(DEFAULT_PROPERTY);
  }

  public BSelecteurAlpha(final String _prop) {
    super(_prop, 0, 255);
    setTitle(EbliLib.getS("Transparence"));
    Dimension preferredSize = slider_.getPreferredSize();
    super.slider_.setPreferredSize(new Dimension(20, preferredSize.height));
  }

  @Override
  protected int getSliderValueForTarget() {
    return 255 - slider_.getValue();
  }

  @Override
  public int getTargetValueForSlide() {
    final Object o = getTargetValue();
    if (o == null) {
      return 0;
    }
    return 255 - getTargetIntValue();
  }

}
