/*
 * @creation     1998-12-14
 * @modification $Date: 2006-09-19 14:55:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.controle;
import com.memoire.bu.BuToggleButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Un selecteur vrai/faux.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 14:55:52 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class BSelecteurProprieteBooleenne
  extends BuToggleButton
  implements ActionListener {
  public BSelecteurProprieteBooleenne() {
    super();
    propertyName_= "selected";
    addActionListener(this);
  }
  @Override
  public void actionPerformed(final ActionEvent _evt) {
    final boolean b= isSelected();
    firePropertyChange(getPropertyName(), !b, b);
  }
  // Propriete reelle
  private String propertyName_;
  public String getPropertyName() {
    return propertyName_;
  }
  public void setPropertyName(final String _propertyName) {
    propertyName_= _propertyName;
  }
}
