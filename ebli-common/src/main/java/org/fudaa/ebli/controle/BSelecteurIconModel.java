/*
 * @creation 13 juin 2005
 * 
 * @modification $Date: 2007-05-22 14:19:05 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIconChooser;
import org.fudaa.ebli.trace.TraceIconModel;

/**
 * @author Fred Deniger
 * @version $Id: BSelecteurIconModel.java,v 1.5 2007-05-22 14:19:05 deniger Exp $
 */
public class BSelecteurIconModel extends BSelecteurAbstract implements ItemListener, ChangeListener {

  public static final String DEFAULT_PROPERTY = "iconModel";

  public static final String getProperty(final int _idx) {
    return _idx == 0 ? DEFAULT_PROPERTY : DEFAULT_PROPERTY + _idx;
  }

  BSelecteurColorChooserBt btColor_;
  BuComboBox cbType_;
  TraceIconModel old_;
  JSpinner spTaille_;
  SpinnerNumberModel tailleModel_;

  TraceIconChooser typeModel_;

  protected class BSelecteurColorTarget implements BSelecteurTargetInterface {
    Color currentColor_;

    @Override
    public void addPropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    }

    @Override
    public Object getMin(final String _key) {
      return null;
    }

    @Override
    public Object getMoy(final String _key) {
      return null;
    }

    @Override
    public Object getProperty(final String _key) {
      return currentColor_;
    }

    @Override
    public void removePropertyChangeListener(final String _key, final PropertyChangeListener _l) {
    }

    @Override
    public boolean setProperty(final String _key, final Object _newProp) {
      currentColor_ = (Color) _newProp;
      fireDataChanged(false, false, true);
      return true;
    }

  }

  public BSelecteurIconModel() {
    this(DEFAULT_PROPERTY, null);
  }

  public BSelecteurIconModel(final String _prop) {
    this(_prop, null);
  }

  public BSelecteurIconModel(final String _property, final TraceIconModel _ico) {
    super(_property);
    cbType_ = new BuComboBox();

    typeModel_ = new TraceIconChooser(true);
    cbType_.setModel(typeModel_);
    cbType_.setRenderer(typeModel_.getCellRenderer());
    cbType_.setPreferredSize(new Dimension(60, 15));
    cbType_.addItemListener(this);
    int val = 1;
    if (_ico != null) {
      val = _ico.getTaille();
    }
    tailleModel_ = new SpinnerNumberModel(val, 0, 35, 1);
    tailleModel_.addChangeListener(this);
    spTaille_ = new JSpinner(tailleModel_);
    btColor_ = new BSelecteurColorChooserBt();
    final BSelecteurColorTarget selecteurColorTarget = new BSelecteurColorTarget();
    if (_ico != null) {
      selecteurColorTarget.currentColor_ = _ico.getCouleur();
      typeModel_.setSelectedIcone(_ico.getType());
    }
    btColor_.setSelecteurTarget(selecteurColorTarget);
    title_ = EbliLib.getS("Icone");
  }

  protected void fireDataChanged(final boolean _type, final boolean _epaisseur, final boolean _color) {
    if (isUpdating_) {
      return;
    }
    if (FuLog.isTrace()) {
      FuLog.trace("ETR: " + getClass().getName() + " data change type=" + _type + "; epaisseur=" + _epaisseur + "; color=" + _color);
    }
    final TraceIconModel d = getNewData();
    if (_type) {
      d.setColorIgnored();
      d.setEpaisseurIgnored();
    } else if (_epaisseur) {
      d.setColorIgnored();
      d.setTypeIgnored();
    } else if (_color) {
      d.setTypeIgnored();
      d.setEpaisseurIgnored();
    }
    if (!d.equals(old_)) {
      if (FuLog.isTrace()) {
        FuLog.trace("new trace " + d);
      }
      firePropertyChange(getProperty(), d);
    }
  }

  @Override
  public void updateFromTarget() {
    final TraceIconModel ic = (TraceIconModel) super.getTargetValue();
    isUpdating_ = true;
    old_ = ic;
    final int type = ic == null ? 0 : ic.getType();
    if (type >= 0) {
      typeModel_.setSelectedIcone(type);
    } else {
      cbType_.setSelectedItem(null);
    }
    tailleModel_.setValue(new Integer(Math.max(0, ic == null ? 1 : ic.getTaille())));
    final BSelecteurColorTarget colorTarget = (BSelecteurColorTarget) btColor_.getTarget();
    colorTarget.currentColor_ = (ic == null ? Color.BLACK : ic.getCouleur());
    btColor_.updateFromTarget();

  }

  public JPanel buildPanel() {
    final JPanel r = new BuPanel();
    buildPanel(r, true);
    return r;
  }

  public void buildPanel(final JPanel _pn, final boolean _layout) {
    if (_layout) {
      _pn.setLayout(new BuGridLayout(3, 3, 3, true, false));
    }
    if (showType) {
      _pn.add(cbType_);
    }
    _pn.add(spTaille_);
    if (showColor) {
      _pn.add(btColor_.getPn());
    }

  }

  public JComponent getComponent() {
    return buildPanel();
  }

  @Override
  public JComponent[] getComponents() {
    if (showColor && showType) {
      return new JComponent[] { cbType_, spTaille_, btColor_.getPn() };
    }
    List<JComponent> cmps = new ArrayList<JComponent>();
    if (showType) {
      cmps.add(cbType_);
    }
    cmps.add(spTaille_);
    if (showColor) {
      cmps.add(btColor_.getPn());
    }
    return (JComponent[]) cmps.toArray(new JComponent[cmps.size()]);

  }

  public TraceIconModel getNewData() {
    return new TraceIconModel(typeModel_.getSelectedIconeType(), tailleModel_.getNumber().intValue(),
        ((BSelecteurColorTarget) btColor_.getTarget()).currentColor_);
  }

  public final SpinnerNumberModel getTailleModel() {
    return tailleModel_;
  }

  public final TraceIconChooser getTypeModel() {
    return typeModel_;
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if (_e.getStateChange() == ItemEvent.DESELECTED) {
      return;
    }
    final boolean enable = typeModel_.getSelectedIconeType() != TraceIcon.RIEN;
    btColor_.setEnabled(enable);
    spTaille_.setEnabled(enable);
    fireDataChanged(true, false, false);
  }

  @Override
  public void stateChanged(final ChangeEvent _e) {
    fireDataChanged(false, true, false);
  }

  private boolean showColor = true;
  private boolean showType = true;

  public boolean isShowType() {
    return showType;
  }

  public void setShowType(boolean showType) {
    this.showType = showType;
  }

  public void setShowColor(boolean addColor) {
    this.showColor = addColor;

  }

}
