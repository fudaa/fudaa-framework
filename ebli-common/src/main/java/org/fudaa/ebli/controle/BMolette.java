/*
 * @file         BMolette.java
 * @creation     1999-02-10
 * @modification $Date: 2006-09-19 14:55:52 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuLightBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Une molette.
 *
 * @version $Revision: 1.11 $ $Date: 2006-09-19 14:55:52 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class BMolette extends JComponent implements MouseMotionListener, MouseListener, MouseWheelListener {

  public final static int VERTICAL = 1;
  public final static int HORIZONTAL = 2;
  public final static int PLUS = 3;
  public final static int MOINS = 4;
  protected Dimension tailleDefaut_ = new Dimension(128, 16);
  protected ChangeEvent event_;
  private final List changeListeners_;
  private int offset_;
  private int prevMousePos_;
  private boolean adjusting_;
  private int direction_;

  public BMolette() {
    super();
    contraste_ = 0.6;
    luminosite_ = 0.4;
    tickSpacing_ = 5;
    setOrientation(VERTICAL);
    setFocusable(true);
    event_ = new ChangeEvent(this);
    direction_ = PLUS;
    changeListeners_ = new ArrayList();
    setBorder(new BuLightBorder(BuLightBorder.LOWERED, 1));
    addMouseMotionListener(this);
    addMouseListener(this);
    addMouseWheelListener(this);
  }

  /*
   * public void paintComponent(Graphics g) { Dimension size=getSize(); if( orientation_==HORIZONTAL ) { for(int
   * i=-tickSpacing_+offset; i<size.width; i+=tickSpacing_) { g.setColor(darkenBg(i-tickSpacing_)); g.drawLine(i, 0, i,
   * size.height); g.setColor(darkenBg(i+1+tickSpacing_)); g.drawLine(i+1, 0, i+1, size.height); for(int j=2; j<tickSpacing_;
   * j++) { g.setColor(darkenBg(i+j)); g.drawLine(i+j, 0, i+j, size.height); } } } else { for(int
   * i=-tickSpacing_+offset; i<size.height; i+=tickSpacing_) { g.setColor(darkenBg(i-tickSpacing_)); g.drawLine(0, i,
   * size.width, i); g.setColor(darkenBg(i+1+tickSpacing_)); g.drawLine(0, i+1, size.width, i+1); for(int j=2; j<tickSpacing_;
   * j++) { g.setColor(darkenBg(i+j)); g.drawLine(0, i+j, size.width, i+j); } } } }
   */
  @Override
  public void paintComponent(final Graphics _g) {
    final Dimension size = getSize();
    final Color cote = darkenBg(0);
    final Graphics2D g2d = (Graphics2D) _g;
    if (orientation_ == HORIZONTAL) {
      final int milieu = size.width / 2;
      final Color colorMilieu = darkenBg(milieu);
      final Rectangle r = new Rectangle(milieu, size.height);
      GradientPaint gp = new GradientPaint(0f, 0f, cote, milieu, 0f, colorMilieu);
      final Paint oldPaint = g2d.getPaint();
      g2d.setPaint(gp);
      g2d.fill(r);
      r.setLocation(milieu, 0);
      gp = new GradientPaint(milieu, 0f, colorMilieu, size.width, 0f, cote);
      g2d.setPaint(gp);
      g2d.fill(r);
      g2d.setPaint(oldPaint);
      for (int i = -tickSpacing_ + offset_; i < size.width; i += tickSpacing_) {
        g2d.setColor(darkenBg(i - tickSpacing_));
        g2d.drawLine(i, 0, i, size.height);
        g2d.setColor(darkenBg(i + 1 + tickSpacing_));
        g2d.drawLine(i + 1, 0, i + 1, size.height);
      }
    } else {
      final int milieu = size.height / 2;
      final Color colorMilieu = darkenBg(milieu);
      final Rectangle r = new Rectangle(size.width, milieu);
      GradientPaint gp = new GradientPaint(0f, 0f, cote, 0f, milieu, colorMilieu);
      final Paint oldPaint = g2d.getPaint();
      g2d.setPaint(gp);
      g2d.fill(r);
      r.setLocation(0, milieu);
      gp = new GradientPaint(0f, milieu, colorMilieu, 0f, size.height, cote);
      g2d.setPaint(gp);
      g2d.fill(r);
      g2d.setPaint(oldPaint);
      for (int i = -tickSpacing_ + offset_; i < size.height; i += tickSpacing_) {
        g2d.setColor(darkenBg(i - tickSpacing_));
        g2d.drawLine(0, i, size.width, i);
        g2d.setColor(darkenBg(i + 1 + tickSpacing_));
        g2d.drawLine(0, i + 1, size.width, i + 1);
      }
    }
  }

  public int getDirection() {
    return direction_;
  }

  public boolean getValueIsAdjusting() {
    return adjusting_;
  }

  // **********************************************
  // EVENEMENTS
  // **********************************************
  @Override
  public void mouseMoved(final MouseEvent _e) {
  }

  @Override
  public void mouseWheelMoved(MouseWheelEvent e) {
    if (!isEnabled()) {
      return;
    }
    if (e.getWheelRotation() < 0) {
      if (orientation_ == HORIZONTAL) {
        moveForwardHorizontal();
      } else {
        moveForwardVertical();
      }
    } else if (e.getWheelRotation() > 0) {
      if (orientation_ == HORIZONTAL) {
        moveBackwardHorizontal();
      } else {
        moveBackwardVertical();
      }
    }
    repaint();
    fireStateChanged();
  }

  @Override
  public void mouseDragged(final MouseEvent _e) {
    if (!isEnabled()) {
      return;
    }
    if (orientation_ == HORIZONTAL) {
      if ((_e.getPoint().x - prevMousePos_) > 0) {
        moveForwardHorizontal();
      } else {
        moveBackwardHorizontal();
      }
      prevMousePos_ = _e.getPoint().x;
    } else {
      if ((_e.getPoint().y - prevMousePos_) > 0) {
        moveBackwardVertical();
      } else {
        moveForwardVertical();
      }
      prevMousePos_ = _e.getPoint().y;
    }
    repaint();
    fireStateChanged();
  }

  @Override
  public void mouseClicked(final MouseEvent _e) {
  }

  @Override
  public void mouseEntered(final MouseEvent _e) {
  }

  @Override
  public void mouseExited(final MouseEvent _e) {
  }

  @Override
  public void mousePressed(final MouseEvent _e) {
    if (!isEnabled()) {
      return;
    }
    if (orientation_ == HORIZONTAL) {
      prevMousePos_ = _e.getPoint().x;
    } else {
      prevMousePos_ = _e.getPoint().y;
    }
    adjusting_ = true;
  }

  @Override
  public void mouseReleased(final MouseEvent _e) {
    if (!isEnabled()) {
      return;
    }
    adjusting_ = false;
    fireStateChanged();
  }

  protected synchronized void fireStateChanged() {
    for (int i = 0; i < changeListeners_.size(); i++) {
      ((ChangeListener) changeListeners_.get(i)).stateChanged(event_);
    }
  }

  public synchronized void addChangeListener(final ChangeListener _l) {
    changeListeners_.add(_l);
  }

  public synchronized void removeChangeListener(final ChangeListener _l) {
    changeListeners_.remove(_l);
  }
  // **********************************************
  // PROPRIETES INTERNES
  // **********************************************
  // Propriete orientation
  private int orientation_;

  public int getOrientation() {
    return orientation_;
  }

  public final void setOrientation(final int _orientation) {
    if (orientation_ != _orientation) {
      final int vp = orientation_;
      orientation_ = _orientation;
      // Dimension d=new Dimension(getHeight(), getWidth());
      tailleDefaut_ = new Dimension(tailleDefaut_.height, tailleDefaut_.width);
      // setMinimumSize(tailleDefaut_);
      setPreferredSize(tailleDefaut_);
      // setSize(d);
      // repaint();
      firePropertyChange("Orientation", vp, orientation_);
    }
  }
  // Propriete luminosite
  private double luminosite_;

  public double getLuminosite() {
    return luminosite_;
  }

  public void setLuminosite(final double _luminosite) {
    if (luminosite_ != _luminosite) {
      final double vp = luminosite_;
      luminosite_ = _luminosite;
      repaint();
      firePropertyChange("luminosite", vp, luminosite_);
    }
  }
  // Propriete contraste
  private double contraste_;

  public double getContraste() {
    return contraste_;
  }

  public void setContraste(final double _contraste) {
    if (contraste_ != _contraste) {
      final double vp = contraste_;
      contraste_ = _contraste;
      repaint();
      firePropertyChange("contraste", vp, contraste_);
    }
  }
  // Propriete tickSpacing
  private int tickSpacing_;

  public int getTickSpacing() {
    return tickSpacing_;
  }

  public void setTickSpacing(final int _tickSpacing) {
    if (tickSpacing_ != _tickSpacing) {
      final int vp = tickSpacing_;
      tickSpacing_ = _tickSpacing;
      repaint();
      firePropertyChange("tickSpacing", vp, tickSpacing_);
    }
  }

  // Methodes privees
  private Color darkenBg(final int _xI) {
    int x = _xI;
    final Color bg = getBackground();
    int width;
    if (orientation_ == HORIZONTAL) {
      width = getSize().width;
    } else {
      width = getSize().height;
    }
    if (x > width) {
      x = width;
    }
    if (x < 0) {
      x = 0;
    }
    double dev = Math.abs(Math.cos((width / 2 - x) * Math.PI / width));
    dev = dev / (2. - (isEnabled() ? contraste_ : 1.8d)) + (isEnabled() ? luminosite_ : 0.7);
    return new Color((int) (Math.min(bg.getRed() * dev, 255)), (int) (Math.min(bg.getGreen() * dev, 255)), (int) (Math
            .min(bg.getBlue() * dev, 255)));
  }

  private void moveForwardHorizontal() {
    direction_ = PLUS;
    if (offset_ < tickSpacing_) {
      offset_++;
    } else {
      offset_ = 0;
    }
  }

  private void moveBackwardHorizontal() {
    direction_ = MOINS;
    if (offset_ > -tickSpacing_) {
      offset_--;
    } else {
      offset_ = 0;
    }
  }

  private void moveBackwardVertical() {
    direction_ = MOINS; // inversion de direction en y
    if (offset_ < tickSpacing_) {
      offset_++;
    } else {
      offset_ = 0;
    }
  }

  private void moveForwardVertical() {
    direction_ = PLUS;
    if (offset_ > -tickSpacing_) {
      offset_--;
    } else {
      offset_ = 0;
    }
  }
}
