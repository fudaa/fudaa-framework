/*
 * @creation 10 nov. 06
 * @modification $Date: 2006-11-14 09:06:32 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

/**
 * @author fred deniger
 * @version $Id: BConfigurableSingleSection.java,v 1.1 2006-11-14 09:06:32 deniger Exp $
 */
public class BConfigurableSingleSection implements BConfigurableInterface {

  final BConfigurableSectionInterface sect_;

  public BConfigurableSingleSection(final BConfigurableSectionInterface _sect) {
    super();
    sect_ = _sect;
  }

  @Override
  public BConfigurableInterface[] getSections() {
    return null;
  }

  @Override
  public String getTitle() {
    return sect_ == null ? null : sect_.getTitle();
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    return sect_ == null ? null : sect_.createSelecteurs();
  }

  @Override
  public BSelecteurTargetInterface getTarget() {
    return sect_ == null ? null : sect_.getTarget();
  }

  @Override
  public void stopConfiguration() {
    if (sect_ != null) {
      sect_.stopConfiguration();
    }

  }

}
