/*
 *  @creation     21 sept. 2004
 *  @modification $Date: 2006-11-14 09:06:31 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import javax.swing.JComponent;

/**
 * @author Fred Deniger
 * @version $Id: BSelecteurInterface.java,v 1.1 2006-11-14 09:06:31 deniger Exp $
 */
public interface BSelecteurInterface {

  /**
   * @return le composant a dessiner
   */
  JComponent[] getComponents();

  String getTitle();

  String getTooltip();
  
  
  boolean needAllWidth();
  
  
  
  void setEnabled(boolean _b);

  /**
   * @return la propri�t� g�r�e par ce s�lecteur
   */
  String getProperty();

  /**
   * @param target la cible de cette palette
   * @return true si la cible est correcte
   */
  void setSelecteurTarget(BSelecteurTargetInterface _target);
  
  void reupdateFromTarget();

}
