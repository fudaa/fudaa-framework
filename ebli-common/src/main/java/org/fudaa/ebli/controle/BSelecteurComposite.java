/*
 * @creation 21 mai 07
 * @modification $Date: 2007-06-05 08:58:39 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuPanel;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * @author fred deniger
 * @version $Id: BSelecteurComposite.java,v 1.2 2007-06-05 08:58:39 deniger Exp $
 */
public class BSelecteurComposite implements BSelecteurInterface {

  protected BSelecteurInterface[] comps_;
  String title_;
  boolean useOnePn_;

  public BSelecteurComposite(final BSelecteurInterface[] _comps) {
    this(_comps, false);
  }

  public BSelecteurComposite(final BSelecteurInterface[] _comps, final boolean _useOnePanel) {
    useOnePn_ = _useOnePanel;
    comps_ = _comps;
  }

  @Override
  public JComponent[] getComponents() {
    JPanel pn = null;
    List res = null;
    if (useOnePn_) {
      pn = new BuPanel(new BuHorizontalLayout(1, true, true));
    } else {
      res = new ArrayList(4);
    }
    for (int i = 0; i < comps_.length; i++) {
      final JComponent[] cps = comps_[i].getComponents();
      final int nb = cps.length;
      final String tooltip = comps_[i].getTooltip();
      for (int j = 0; j < nb; j++) {
        cps[j].setToolTipText(tooltip);
        if (useOnePn_) {
          pn.add(cps[j]);
        } else
          res.add(cps[j]);
      }

    }
    return useOnePn_ ? BSelecteurAbstract.createComponents(pn) : (JComponent[]) res.toArray(new JComponent[res.size()]);
  }

  @Override
  public String getProperty() {
    return "composed";
  }

  @Override
  public boolean needAllWidth() {
    return false;
  }

  @Override
  public void reupdateFromTarget() {
    for (int i = 0; i < comps_.length; i++) {
      comps_[i].reupdateFromTarget();
    }
  }

  @Override
  public void setEnabled(final boolean _b) {
    for (int i = 0; i < comps_.length; i++) {
      comps_[i].setEnabled(_b);
    }
  }

  @Override
  public void setSelecteurTarget(final BSelecteurTargetInterface _target) {
    for (int i = 0; i < comps_.length; i++) {
      comps_[i].setSelecteurTarget(_target);
    }
  }

  @Override
  public String getTitle() {
    return title_;
  }

  public void setTitle(final String _title) {
    title_ = _title;
  }

  @Override
  public String getTooltip() {
    return comps_[0].getTooltip();
  }

  public boolean isUseOnePn() {
    return useOnePn_;
  }

  public void setUseOnePn(boolean _useOnePn) {
    useOnePn_ = _useOnePn;
  }

}
