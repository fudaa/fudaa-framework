/*
 * @creation 9 nov. 06
 * @modification $Date: 2007-05-04 13:49:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JComponent;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author fred deniger
 * @version $Id: BSelecteurTextField.java,v 1.5 2007-05-04 13:49:44 deniger Exp $
 */
public class BSelecteurTextField extends BSelecteurAbstract implements FocusListener, KeyListener {

  BuTextField tf_;

  public final static String TITLE_PROPERTY = "title";

  public BSelecteurTextField(final String _property) {
    this(_property, BuTextField.createDoubleField());
  }

  public BSelecteurTextField(final String _property, final BuValueValidator _validator) {
    this(_property);
    tf_.setValueValidator(_validator);
  }

  @Override
  public void setTooltip(final String _tooltip) {
    final CtuluHtmlWriter ctuluHtmlWriter = new CtuluHtmlWriter(true, true);
    ctuluHtmlWriter.addTagAndText("p", _tooltip);
    ctuluHtmlWriter.addTag("p");
    ctuluHtmlWriter.addTag("b");
    ctuluHtmlWriter.append(EbliLib.getS("Appuyer sur 'Entrer' pour valider votre saisie"));
    super.setTooltip(ctuluHtmlWriter.getHtml());
  }

  public BSelecteurTextField(final String _property, final BuTextField _tf) {
    this(_property,null,_tf);
  }
  public BSelecteurTextField(final String _property, final String _titre, final BuTextField _tf) {
    super(_property);
    tf_ = _tf;
    tf_.setFocusCycleRoot(false);
    tf_.addFocusListener(this);
    tf_.addKeyListener(this);
    tf_.setColumns(8);
    setTitle(_titre);
  }

  @Override
  public void updateFromTarget() {
    tf_.setValue(getTargetValue());

  }

  @Override
  public JComponent[] getComponents() {
    return super.createComponents(tf_);
  }

  @Override
  public void keyPressed(final KeyEvent _e) {}

  @Override
  public void keyReleased(final KeyEvent _e) {
    if (_e.getKeyCode() == KeyEvent.VK_ENTER) {
      updateTarget();
    }
  }

  @Override
  public void keyTyped(final KeyEvent _e) {}

  @Override
  public void focusGained(final FocusEvent _e) {}

  @Override
  public void focusLost(final FocusEvent _e) {
    updateTarget();

  }

  protected void updateTarget() {
    if (tf_.isEnabled()) {
      firePropertyChange(getProperty(), tf_.getValue());
    }
  }

}
