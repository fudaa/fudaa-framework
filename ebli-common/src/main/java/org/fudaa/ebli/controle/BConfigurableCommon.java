/*
 * @creation 14 nov. 06
 * @modification $Date: 2007-05-04 13:49:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import org.fudaa.ctulu.CtuluLibArray;

/**
 * @author fred deniger
 * @version $Id: BConfigurableCommon.java,v 1.2 2007-05-04 13:49:44 deniger Exp $
 */
public class BConfigurableCommon implements BConfigurableInterface {

  final BConfigurableInterface[] confs_;

  public BConfigurableCommon(final BConfigurableInterface[] _other) {
    super();
    confs_ = _other;
  }

  @Override
  public BConfigurableInterface[] getSections() {
    if (CtuluLibArray.isEmpty(confs_[0].getSections())) {
      return null;
    }
    final int nbSs = confs_[0].getSections().length;

    final BConfigurableInterface[][] other = new BConfigurableInterface[nbSs][confs_.length];
    for (int i = confs_.length - 1; i >= 0; i--) {
      final BConfigurableInterface[] oi = confs_[i].getSections();
      for (int k = oi.length - 1; k >= 0; k--) {
        other[k][i] = oi[k];
      }
    }
    final BConfigurableCommon[] res = new BConfigurableCommon[nbSs];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = new BConfigurableCommon(other[i]);
    }
    return res;
  }

  @Override
  public String getTitle() {
    return confs_[0].getTitle();
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    return confs_[0].createSelecteurs();
  }

  @Override
  public BSelecteurTargetInterface getTarget() {
    final BSelecteurTargetInterface[] trgs = new BSelecteurTargetInterface[confs_.length];
    for (int i = 0; i < trgs.length; i++) {
      trgs[i] = confs_[i].getTarget();
    }
    return new BSelecteurTargetComposite(trgs);
  }

  @Override
  public void stopConfiguration() {
    for (int i = confs_.length - 1; i >= 0; i--) {
      confs_[i].stopConfiguration();
    }
  }

}
