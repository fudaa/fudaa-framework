/*
 *  @creation     21 sept. 2004
 *  @modification $Date: 2006-11-15 09:23:21 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import java.beans.PropertyChangeListener;

/**
 * La cible d'une section de configuration contenue dans un panneau de configuration.
 * @author Fred Deniger
 * @version $Id: BSelecteurTargetInterface.java,v 1.2 2006-11-15 09:23:21 deniger Exp $
 */
public interface BSelecteurTargetInterface {

  /**
   * @param _s la propriete a ecouter
   * @param _l le listener
   */
  void addPropertyChangeListener(String _key, PropertyChangeListener _l);

  /**
   * @param _l le listener a enlever
   */
  void removePropertyChangeListener(String _key, PropertyChangeListener _l);

  /**
   * @param _key La propri�t�
   * @return La valeur de la propri�t� de la cible pour initialiser la section de configuration.
   */
  Object getProperty(String _key);

  Object getMoy(String _key);

  Object getMin(String _key);

  /**
   * Definit la propri�t� sur la cible
   * @param _key La propri�t�
   * @param _newProp La valeur de la propri�t�
   * @return
   */
  boolean setProperty(String _key, Object _newProp);

}
