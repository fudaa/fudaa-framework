/*
 * @creation 30 nov. 06
 * @modification $Date: 2007-05-22 14:19:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JDialog;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author fred deniger
 * @version $Id: BSelecteurFont.java,v 1.4 2007-05-22 14:19:05 deniger Exp $
 */
public class BSelecteurFont extends BSelecteurAbstract implements ActionListener {

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final JDialog d = new JDialog(CtuluLibSwing.getActiveWindow());
    d.setResizable(true);
    final BSelecteurReduitFonteNewVersion selecteur = new BSelecteurReduitFonteNewVersion(getProperty(), bt_.getFont(), d) {
      @Override
      public void firePropertyChange(String _prop, Object _newVal) {
        super.firePropertyChange(_prop, _newVal);
        bt_.setFont(getSelectedFont());
      }
    };
    selecteur.setAddListenerToTarget(false);
    selecteur.setSelecteurTarget(target_);
    d.setContentPane(selecteur.getComponent());

    d.pack();
    d.setLocationRelativeTo(bt_);
    d.setTitle(EbliLib.getS("Choisir la fonte"));
    d.setVisible(true);

  }
  public BuButton bt_ = new BuButton();

  public BSelecteurFont(final String _property) {
    super(_property);
    setTitle(EbliLib.getS("Fonte"));
    bt_.setText(EbliLib.getS("Choisir la fonte"));
    bt_.addActionListener(this);
  }

  public BSelecteurFont() {
    this(BSelecteurReduitFonteNewVersion.PROPERTY);
  }

  @Override
  public void updateFromTarget() {
    final Font f = (Font) getTargetValue();
    if (f != null) {
      bt_.setFont(f);
    }
  }

  public BuButton getButton() {
    return bt_;
  }

  @Override
  public JComponent[] getComponents() {
    return super.createComponents(bt_);
  }
}
