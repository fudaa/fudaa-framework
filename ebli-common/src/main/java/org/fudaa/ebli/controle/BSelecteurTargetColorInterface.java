/*
 *  @creation     21 sept. 2004
 *  @modification $Date: 2006-11-14 09:06:32 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import java.awt.Color;

/**
 * @author Fred Deniger
 * @version $Id: BSelecteurTargetColorInterface.java,v 1.1 2006-11-14 09:06:32 deniger Exp $
 */
public interface BSelecteurTargetColorInterface extends BSelecteurTargetInterface {
  Color getCouleur();

  boolean setCouleur(Color _v);

}
