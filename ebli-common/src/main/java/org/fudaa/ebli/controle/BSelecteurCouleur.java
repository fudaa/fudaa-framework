/*
 * @creation     1998-06-26
 * @modification $Date: 2007-06-05 08:58:39 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.controle;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JComponent;
import org.fudaa.ebli.trace.FiltreDesaturation;

/**
 * Un selecteur de couleur.
 * 
 * @version $Id: BSelecteurCouleur.java,v 1.2 2007-06-05 08:58:39 deniger Exp $
 * @author Guillaume Desnoix
 */
public class BSelecteurCouleur extends JComponent implements MouseListener, MouseMotionListener {
  public BSelecteurCouleur() {
    super();
    final Dimension d = new Dimension(64, 64);
    setMinimumSize(d);
    setPreferredSize(d);
    setDoubleBuffered(true);
    setOpaque(true);
    addMouseListener(this);
    addMouseMotionListener(this);
  }

  @Override
  public void paint(final Graphics _g) {
    final Dimension dim = getSize();
    final Insets f = getInsets();
    final boolean e = isEnabled();
    float t, d, l;
    int w, h, x, y;
    w = dim.width - f.left - f.right;
    h = dim.height - f.top - f.bottom;
    Color c = getCouleur();
    if (!e) {
      c = FiltreDesaturation.desatureCouleur(c);
    }
    _g.setColor(getBackground());
    _g.fillRect(f.left, f.top, 16, 16);
    _g.draw3DRect(f.left + 1, f.top + 1, 13, 13, false);
    _g.setColor(c);
    _g.fillRect(f.left + 2, f.top + 2, 12, 12);
    int i, j;
    for (i = 0; i < 16; i++) {
      for (j = 0; j < 16; j++) {
        x = i * w / 16;
        y = j * h / 16;
        d = ((float) x) / ((float) (w - 16));
        l = ((float) y) / ((float) (h - 16));
        c = Color.getHSBColor(tdl_[0], d, l);
        if (!e) {
          c = FiltreDesaturation.desatureCouleur(c);
        }
        _g.setColor(c);
        _g.fillRect(f.left + 16 + x, f.top + 16 + y, w / 16 + 1, h / 16 + 1);
      }
    }
    for (x = 0; x < w - 16; x++) {
      t = ((float) x) / ((float) (w - 16));
      d = 1.f;
      l = 1.f;
      c = Color.getHSBColor(t, d, l);
      if (!e) {
        c = FiltreDesaturation.desatureCouleur(c);
      }
      _g.setColor(c);
      _g.drawLine(f.left + 16 + x, f.top, f.left + 16 + x, f.top + 15);
    }
    for (x = 0; x < 14; x++) {
      for (y = 0; y < h - 16; y++) {
        final int v = (int) ((255.f * y) / (h - 16));
        final Color c1 = getCouleur();
        Color bg = getBackground();
        if (((y + 1) / 3 + x / 3) % 3 == 0) {
          bg = bg.darker();
        }
        if (((y + 1) / 3 + x / 3) % 3 == 2) {
          bg = bg.brighter();
        }
        final int r1 = c1.getRed();
        final int g1 = c1.getGreen();
        final int b1 = c1.getBlue();
        final int r2 = bg.getRed();
        final int g2 = bg.getGreen();
        final int b2 = bg.getBlue();
        final int r3 = (r1 * v + r2 * (255 - v)) / 255;
        final int g3 = (g1 * v + g2 * (255 - v)) / 255;
        final int b3 = (b1 * v + b2 * (255 - v)) / 255;
        c = new Color(r3, g3, b3);
        if (!e) {
          c = FiltreDesaturation.desatureCouleur(c);
        }
        _g.setColor(c);
        // g.drawLine(f.left,f.top+16+y,f.left+15,f.top+16+y);
        _g.drawLine(f.left + x, f.top + 16 + y, f.left + x, f.top + 16 + y);
      }
    }
    _g.setColor(getBackground());
    _g.drawRect(f.left + 15, f.top, w - 16, 15);
    _g.draw3DRect(f.left + 16, f.top + 1, w - 18, 13, false);
    _g.drawRect(f.left, f.top + 15, w, h - 16);
    _g.draw3DRect(f.left + 1, f.top + 16, 13, h - 18, false);
    _g.drawRect(f.left + 15, f.top + 15, w - 16, h - 16);
    _g.draw3DRect(f.left + 16, f.top + 16, w - 18, h - 18, false);
    c = Color.black;
    if (!e) {
      c = FiltreDesaturation.desatureCouleur(c);
    }
    _g.setColor(c);
    _g.drawRect(f.left + 17 + (int) ((w - 19) * tdl_[0]) - 2, f.top + 6, 4, 4);
    _g.drawRect(f.left + 17 + (int) ((w - 19) * tdl_[1]) - 2, f.top + 17 + (int) ((h - 19) * tdl_[2]) - 2, 4, 4);
    _g.drawRect(f.left + 6, f.top + 16 + (h - 19) * alpha_ / 255 - 2, 4, 4);
    super.paint(_g);
    c = Color.white;
    if (!e) {
      c = FiltreDesaturation.desatureCouleur(c);
    }
    _g.setColor(c);
    _g.fillRect(f.left + 17 + (int) ((w - 19) * tdl_[0]) - 1, f.top + 7, 3, 3);
    _g.fillRect(f.left + 17 + (int) ((w - 19) * tdl_[1]) - 1, f.top + 17 + (int) ((h - 19) * tdl_[2]) - 1, 3, 3);
    _g.fillRect(f.left + 7, f.top + 16 + (h - 19) * alpha_ / 255 - 1, 3, 3);
  }

  static float[] tdl(final Color _c) {
    return Color.RGBtoHSB(_c.getRed(), _c.getGreen(), _c.getBlue(), null);
  }

  static class RepaintTask extends Thread {
    private final BSelecteurCouleur selecteur_;

    RepaintTask(final BSelecteurCouleur _selecteur) {
      selecteur_ = _selecteur;
    }

    public void stopPainting() {
    // que faire ?
    }

    @Override
    public void run() {
      selecteur_.superRepaint();
    }
  }

  private RepaintTask repaintTask_;

  @Override
  public void repaint() {
    if (repaintTask_ != null) {
      repaintTask_.stopPainting();
    }
    repaintTask_ = new RepaintTask(this);
    repaintTask_.setPriority(Thread.MIN_PRIORITY);
    repaintTask_.start();
  }

  void superRepaint() {
    super.repaint();
  }

  // Propriete espace
  /*
   * private boolean espace_; public boolean getEspace() { return espace_; } public void setEspace(boolean _espace) {
   * if(espace_!=_espace) { boolean vp=espace_; espace_=_espace; repaint(); firePropertyChange("Espace",vp,espace_); } }
   */
  // Propriete couleur
  private float[] tdl_ = new float[3];

  private int alpha_ = 255;

  public Color getCouleur() {
    final int v = Color.getHSBColor(tdl_[0], tdl_[1], tdl_[2]).getRGB();
    return new Color((v & 0x00FFFFFF) | (alpha_ << 24), true);
  }

  public void setCouleur(final Color _couleur) {
    final float[] tdl = tdl(_couleur);
    final Color vp = getCouleur();
    tdl_ = tdl;
    // alpha_=(_couleur.getRGB()&0xFF000000)>>24;
    alpha_ = _couleur.getAlpha();
    repaint();
    firePropertyChange("couleur", vp, getCouleur());
  }

  public void selectionne(final MouseEvent _ev) {
    final Dimension dim = getSize();
    final Insets f = getInsets();
    float t, d, l;
    int a, w, h, x, y;
    w = dim.width - f.left - f.right;
    h = dim.height - f.top - f.bottom;
    x = _ev.getX();
    y = _ev.getY();
    a = alpha_;
    t = tdl_[0];
    d = tdl_[1];
    l = tdl_[2];
    if ((x < 16) && (y < 16)) {
      return;
    } else if (y < 16) {
      t = ((float) (x - 16)) / ((float) (w - 16 - 1));
    } else if (x < 16) {
      a = 255 * (y - 16) / (h - 16 - 1);
      if (a < 0) {
        a = 0;
      }
      if (a > 255) {
        a = 255;
        // System.out.println("y="+y+" ;h="+h);
      }
    } else {
      d = ((float) (x - 16)) / ((float) (w - 16));
      l = ((float) (y - 16)) / ((float) (h - 16));
    }
    final int v = Color.getHSBColor(t, d, l).getRGB();
    // setCouleur(new Color((v&0x00FFFFFF)|(alpha_<<24)));
    final Color c = new Color((v & 0x00FFFFFF) | (a << 24), true);
    setCouleur(c);
    // temporaire JDK11
    alpha_ = a;
  }

  @Override
  public void mouseClicked(final MouseEvent _ev) {
    if (!isEnabled()) {
      return;
    }
    selectionne(_ev);
    repaint();
  }

  @Override
  public void mouseDragged(final MouseEvent _ev) {
    if (!isEnabled()) {
      return;
    }
    selectionne(_ev);
    repaint();
  }

  @Override
  public void mouseEntered(final MouseEvent _ev) {}

  @Override
  public void mouseExited(final MouseEvent _ev) {}

  @Override
  public void mousePressed(final MouseEvent _ev) {}

  @Override
  public void mouseReleased(final MouseEvent _ev) {}

  @Override
  public void mouseMoved(final MouseEvent _ev) {}
}
