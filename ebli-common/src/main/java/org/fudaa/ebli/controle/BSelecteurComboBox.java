/*
 * @creation 8 nov. 06
 * 
 * @modification $Date: 2007-05-22 14:19:05 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuComboBox;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JComboBox;
import javax.swing.JComponent;

/**
 * @author fred deniger
 * @version $Id: BSelecteurCheckBox.java,v 1.3 2007-05-22 14:19:05 deniger Exp $
 */
public class BSelecteurComboBox extends BSelecteurAbstract implements ItemListener {

  JComboBox cb_;

  public BSelecteurComboBox(final String _property, final JComboBox _cb) {
    super(_property);
    this.cb_=_cb;
    cb_.addItemListener(this);
  }

  public BSelecteurComboBox(final String _property) {
    this(_property, new BuComboBox());
  }

  public BSelecteurComboBox(final String _property, final String _titre) {
    this(_property);
    setTitle(_titre);
  }

  @Override
  public void updateFromTarget() {
    if (cb_ != null) {
      cb_.setEnabled(target_ != null);
      cb_.setSelectedItem(getTargetValue());
    }
  }

  @Override
  public JComponent[] getComponents() {
    return new JComponent[] { cb_ };
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    firePropertyChange(getProperty(), cb_ == null ? null : cb_.getSelectedItem());

  }

  public JComboBox getCb() {
    return cb_;
  }

}
