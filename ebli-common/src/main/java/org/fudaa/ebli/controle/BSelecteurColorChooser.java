/*
 * @creation 21 sept. 2004
 * @modification $Date: 2007-05-04 13:49:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.controle;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuButtonLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.fudaa.ebli.commun.BPalettePanelInterface;

/**
 * @author Fred Deniger
 * @version $Id: BSelecteurColorChooser.java,v 1.4 2007-05-04 13:49:44 deniger Exp $
 */
public class BSelecteurColorChooser extends BSelecteurAbstract implements ActionListener, /*PropertyChangeListener,*/
    BPalettePanelInterface {

  public static final String DEFAULT_PROPERTY = "foreground";

  protected BuButton apply_;

  protected JColorChooser chooser_;

  protected JPanel pn_;

  public BSelecteurColorChooser() {
    this(DEFAULT_PROPERTY,null);
  }

  public BSelecteurColorChooser(final BuButton[] _bt) {
    this(DEFAULT_PROPERTY,_bt);
  }
    public BSelecteurColorChooser(final String _prop,final BuButton[] _bt) {
    super(_prop);
    pn_ = new BuPanel();
    pn_.setLayout(new BuBorderLayout());
    chooser_ = new JColorChooser();
    pn_.add(chooser_, BuBorderLayout.CENTER);
    final JPanel pb = new BuPanel();
    pb.setLayout(new BuButtonLayout());
    apply_ = new BuButton(BuResource.BU.getToolIcon("appliquer"));
    apply_.addActionListener(this);
    pn_.add(pb, BuBorderLayout.SOUTH);
    pb.add(apply_);
    if (_bt != null) {
      for (int i = 0; i < _bt.length; i++) {
        pb.add(_bt[i]);
      }

    }
  }

  protected Color getTargetColor() {
    return (Color) getTargetValue(super.getProperty());
  }

  @Override
  public void updateFromTarget() {
    apply_.setEnabled(target_ != null);
    chooser_.setEnabled(target_ != null);
    chooser_.setColor(getTargetColor());
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    apply();
  }

  public void apply() {
    if (target_ != null) {
      firePropertyChange(DEFAULT_PROPERTY, getSelectedColor());
    }
  }

  @Override
  public void doAfterDisplay() {}

  @Override
  public JComponent getComponent() {
    return pn_;
  }

  @Override
  public JComponent[] getComponents() {
    return createComponents(pn_);
  }

  public Color getSelectedColor() {
    return chooser_.getColor();
  }

  @Override
  public void paletteDeactivated() {

  }

  @Override
  public boolean setPalettePanelTarget(final Object _target) {
    if (_target instanceof BSelecteurTargetInterface) {
      setSelecteurTarget((BSelecteurTargetInterface) _target);
      return true;
    }
    return false;

  }

}
