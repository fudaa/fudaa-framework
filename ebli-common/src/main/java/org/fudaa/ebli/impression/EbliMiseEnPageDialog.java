/*
 * @creation     2001-10-02
 * @modification $Date: 2006-09-19 14:55:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.impression;

import com.memoire.bu.*;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import javax.swing.JComponent;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Dialogue modifiant la mise en page d'une interface <code>EbliPageable</code>. L'interface doit etre un
 * <code>JComponent</code> pour que les changements soient pris en compte et enregistres dans les ClientPorperty de ce
 * <code>JComponent</code>. La classe <code>EblIpageableDelegate</code> gere ce type de modification.
 *
 * @version $Id: EbliMiseEnPageDialog.java,v 1.12 2006-09-19 14:55:54 deniger Exp $
 * @author Fred Deniger
 */
public class EbliMiseEnPageDialog extends BuDialog {

  /**
   * Le panel principal de ce dialog.
   */
  BuPanel panelPrincipal_;

  /**
   * Le panel charge de l'edition du EbliPageFormat.
   */
  EbliMiseEnPagePanel panelMiseEnPage_;

  /**
   * L'interface concernee par ce diaogue.
   */
  EbliPageable target_;

  /**
   * instancie tous les elements du dialogue.
   *
   * @param _app l'application generant ce dialogue. Peut etre nul.
   * @param _is les infos utilisees pour le dialogue.Peut etre nul.
   * @param _target L'interface a modifier. Ne doit pas etre nul.
   */
  public EbliMiseEnPageDialog(final EbliPageable _target, final BuCommonInterface _app, final BuInformationsSoftware _is) {
    super(_app, _is, EbliResource.EBLI.getString("Entete/Pied de page"), null);
    target_ = _target;
    getComponent();
    panelMiseEnPage_.setEbliPageFormatInit(target_.getDefaultEbliPageFormat());
    panelMiseEnPage_.setEbliPageablePrevisualisation(target_);
  }

  /**
   * Renvoie le panel principal de ce dialogue.
   */
  @Override
  public final JComponent getComponent() {
    if (panelPrincipal_ == null) {
      panelPrincipal_ = new BuPanel();
      panelPrincipal_.setLayout(new BuBorderLayout());
      panelMiseEnPage_ = new EbliMiseEnPagePanel(null, null);
      panelPrincipal_.add(panelMiseEnPage_, BuBorderLayout.CENTER);
      final BuPanel panelBt = new BuPanel();
      panelBt.setLayout(new FlowLayout(FlowLayout.RIGHT));
      final BuButton btAppliquer = new BuButton(BuResource.BU.getString("Appliquer"));
      btAppliquer.setActionCommand("APPLIQUER");
      btAppliquer.setIcon(BuResource.BU.getIcon("appliquer"));
      btAppliquer.addActionListener(this);
      panelBt.add(btAppliquer, 0);
      final BuButton btValider = new BuButton(BuResource.BU.getString("Valider"));
      btValider.setIcon(BuResource.BU.getIcon("valider"));
      btValider.setActionCommand("VALIDER");
      btValider.addActionListener(this);
      panelBt.add(btValider, 1);
      final BuButton btReinit = new BuButton(EbliLib.getS("Initialiser"));
      btReinit.setToolTipText(EbliLib.getS("Initialise le panneau avec les valeurs d'origine"));
      btReinit.setIcon(BuResource.BU.getIcon("defaire"));
      btReinit.setActionCommand("REINIT");
      btReinit.addActionListener(this);
      panelBt.add(btReinit, 2);
      final BuButton btAnnuler = new BuButton(BuResource.BU.getString("Annuler"));
      btAnnuler.setIcon(BuResource.BU.getIcon("annuler"));
      btAnnuler.setActionCommand("ANNULER");
      btAnnuler.addActionListener(this);
      panelBt.add(btAnnuler, 3);

      panelPrincipal_.add(panelBt, BuBorderLayout.SOUTH);
    }
    return panelPrincipal_;
  }

  private void majPageFormat() {
    final EbliPageFormat pageFormatTarget = target_.getDefaultEbliPageFormat();
    final EbliPageFormat pageFormatModif = panelMiseEnPage_.getEbliPageFormatModifie();
    pageFormatTarget.setPiedVisible(pageFormatModif.isPiedVisible());
    pageFormatTarget.setEnteteVisible(pageFormatModif.isEnteteVisible());
    pageFormatTarget.setPageFormat(pageFormatModif.getPageFormat());
    String[] temp = pageFormatModif.getEntete();
    pageFormatTarget.setEntete(temp[0], temp[1], temp[2]);
    temp = pageFormatModif.getPied();
    pageFormatTarget.setPied(temp[0], temp[1], temp[2]);
    if (target_ instanceof JComponent) {
      ((JComponent) target_).revalidate();
    }
  }

  @Override
  public void actionPerformed(final ActionEvent _ae) {
    final String commande = _ae.getActionCommand();
    if ("APPLIQUER".equals(commande)) {
      majPageFormat();
    }
    if ("REINIT".equals(commande)) {
      panelMiseEnPage_.setEbliPageFormatInit(target_.getDefaultEbliPageFormat());
    }
    if ("VALIDER".equals(commande)) {
      majPageFormat();
      dispose();
    }
    if ("ANNULER".equals(commande)) {
      dispose();
    }
  }
}
