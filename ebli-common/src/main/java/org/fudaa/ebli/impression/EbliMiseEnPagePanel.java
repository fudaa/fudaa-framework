/*
 * @creation     2001-10-04
 * @modification $Date: 2006-09-19 14:55:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.impression;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.print.PageFormat;
import java.awt.print.PrinterJob;
import java.text.NumberFormat;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Ce panel permet de modifier le format de page et les entetes et pieds de pages. Les parametres passes en argument ne
 * sont pas modifies et il est possible de recuperer les modifications grace a la methode
 * <code>getEbliPageFormatModife()</code>.
 *
 * @version $Id: EbliMiseEnPagePanel.java,v 1.10 2006-09-19 14:55:54 deniger Exp $
 * @author Fred Deniger
 */
public class EbliMiseEnPagePanel extends BuPanel implements ActionListener {
  /**
   * Format utilise par les labels contenant les dimensions du format de page.
   */
  private final static NumberFormat LABEL_FORMAT = NumberFormat.getInstance();
  /**
   * Initialisation du format des labels.
   */
  static {
    LABEL_FORMAT.setParseIntegerOnly(false);
    LABEL_FORMAT.setMaximumFractionDigits(1);
    LABEL_FORMAT.setMinimumFractionDigits(0);
    LABEL_FORMAT.setMinimumIntegerDigits(1);
  }
  /**
   * Label contenant le type du format :A4,lettre,...
   */
  protected BuLabel lbTypePage_;
  /**
   * Label contenant l'orientation de la page : portrait...
   */
  protected BuLabel lbOrientationPage_;
  /**
   * Label contenant la largeur de la page en cm.
   */
  protected BuLabel lbLargeurPage_;
  /**
   * Label contenant la hauteur de la page en cm.
   */
  protected BuLabel lbHauteurPage_;
  /**
   * Label contenant la hauteur de la marge du haut en cm.
   */
  protected BuLabel lbMargeHautPage_;
  /**
   * Label contenant la marge du bas en cm.
   */
  protected BuLabel lbMargeBasPage_;
  /**
   * Label contenant la largeur de la marge de gauche en cm.
   */
  protected BuLabel lbMargeGauchePage_;
  /**
   * Label contenant la largeur de la marge de droite en cm.
   */
  protected BuLabel lbMargeDroitePage_;
  /**
   * Selectionne si l'entete est visible a l'impression.
   */
  protected BuCheckBox cbEnteteVisible_;
  /**
   * Selectionne si le pied de page est visible a l'impression.
   */
  protected BuCheckBox cbPiedVisible_;
  /**
   * L'editeur concernant la partie gauche de l'entete.
   *
   * @see #EntetePiedEditeur()
   */
  protected EntetePiedEditeur enteteGauche_;
  protected EntetePiedEditeur enteteMilieu_;
  protected EntetePiedEditeur enteteDroit_;
  protected EntetePiedEditeur piedGauche_;
  protected EntetePiedEditeur piedMilieu_;
  protected EntetePiedEditeur piedDroit_;
  /**
   * Sert uniquement a previsualiser les entetes et pieds de pages.
   */
  protected EbliPageable pageablePrevisu_;
  /**
   * Le format de la page.
   */
  protected PageFormat pageFormat_;

  /**
   * instancie tous les elements et les ajoute a ce panel. Les parametres <code>_format</code> et
   * <code>_previsu</code> ne sont pas modifies par ce panel. Si <code>_format</code> null, le format par defaut
   * donne par <code>EbliMisePagePreferencesPanel.getEbliPageFormat()</code> est choisi.
   *
   * @param _format le format initial.
   * @param _previsu si non nul, utiliser pour la previsualisation des entetes et des pieds de page.
   */
  public EbliMiseEnPagePanel(final EbliPageFormat _format, final EbliPageable _previsu) {
    super();
    setLayout(new BuVerticalLayout(10, true, true));
    EbliPageFormat format=_format;
    add(construirePanelFormatPage());
    add(construirePanelEntetePiedDePage());
    if (format == null) {
      format = EbliMiseEnPagePreferencesPanel.getEbliPageFormat();
    }
    pageFormat_ = (PageFormat) (format.getPageFormat().clone());
    pageablePrevisu_ = _previsu;
    majPanelPageFormat();
    majPanelEntetePiedDePage(format);
    majLabelsPrevisu();
  }

  /**
   * formate <code>_d</code>: seul un chiffre apres la virgule est conserve.
   *
   * @param _d le nombre a formater
   * @return le nombre formate sous forme de chaine
   */
  private static String format(final double _d) {
    return LABEL_FORMAT.format(_d);
  }

  /**
   * initialise tous les labels qui afficheront des donnees. La couleur de font est blanche, le texte est aligne a
   * droite et une bordure est ajoutee.
   */
  static final BuLabel initBuLabelDonnee() {
    final BuLabel r = new BuLabel();
    r.setBackground(Color.white);
    r.setOpaque(true);
    r.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BorderFactory.createEmptyBorder(
        2, 2, 2, 2)));
    r.setHorizontalAlignment(SwingConstants.RIGHT);
    return r;
  }

  /**
   * initialise tous les labels qui serviront de Titre. Le texte est aligne a gauche et centre verticalement.
   *
   * @param _titre le texte du label.
   */
  private static BuLabel initBuLabelTitre(final String _titre) {
    final BuLabel r = new BuLabel(EbliResource.EBLI.getString(_titre));
    r.setHorizontalAlignment(SwingConstants.LEFT);
    r.setVerticalAlignment(SwingConstants.CENTER);
    return r;
  }

  /**
   * Initialise la bordure utilisee par les panels principaux: panel du format de page et le panel des entetes et pieds
   * de page.
   *
   * @param _titre le titre ajoute a la bordure.
   */
  private static Border initTitreBorder(final String _titre) {
    return BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(EbliResource.EBLI.getString(_titre)),
        BorderFactory.createEmptyBorder(5, 5, 5, 5));
  }

  /**
   * Construit le panel qui affiche les caracteristiques du format de la page et le bouton de modification.
   */
  private BuPanel construirePanelFormatPage() {
    // le panel general et son border
    final BuPanel pFormatPage = new BuPanel();
    pFormatPage.setLayout(new BuBorderLayout(0, 10));
    pFormatPage.setBorder(initTitreBorder("Taille/Marges de la page"));
    // le panel pour les labels d'information
    final BuPanel pFormatPageInfos = new BuPanel();
    pFormatPageInfos.setLayout(new GridLayout(1, 2, 0, 5));
    final BuGridLayout col1 = new BuGridLayout(2, 4, 5, true, true);
    col1.setRfilled(false);
    col1.setCfilled(false);
    final BuGridLayout col2 = new BuGridLayout(2, 4, 5, true, true);
    col2.setRfilled(false);
    col2.setCfilled(false);
    final BuPanel pFormatPageInfosCol1 = new BuPanel();
    pFormatPageInfosCol1.setLayout(col1);
    final BuPanel pFormatPageInfosCol2 = new BuPanel();
    pFormatPageInfosCol2.setLayout(col2);
    // Les labels pour le format et l'orientation
    pFormatPageInfosCol1.add(initBuLabelTitre("format"));
    lbTypePage_ = initBuLabelDonnee();
    pFormatPageInfosCol1.add(lbTypePage_);
    pFormatPageInfosCol2.add(initBuLabelTitre("orientation"));
    lbOrientationPage_ = initBuLabelDonnee();
    lbOrientationPage_.setHorizontalAlignment(SwingConstants.RIGHT);
    pFormatPageInfosCol2.add(lbOrientationPage_);
    // Les labels pour la largeur et la hauteur de la page
    pFormatPageInfosCol1.add(initBuLabelTitre("largeur"));
    lbLargeurPage_ = initBuLabelDonnee();
    pFormatPageInfosCol1.add(lbLargeurPage_);
    pFormatPageInfosCol2.add(initBuLabelTitre("hauteur"));
    lbHauteurPage_ = initBuLabelDonnee();
    pFormatPageInfosCol2.add(lbHauteurPage_);
    // marge haute et droite
    pFormatPageInfosCol1.add(initBuLabelTitre("marge haut"));
    lbMargeHautPage_ = initBuLabelDonnee();
    pFormatPageInfosCol1.add(lbMargeHautPage_);
    pFormatPageInfosCol2.add(initBuLabelTitre("marge bas"));
    lbMargeBasPage_ = initBuLabelDonnee();
    pFormatPageInfosCol2.add(lbMargeBasPage_);
    // marge gauche et droite
    pFormatPageInfosCol1.add(initBuLabelTitre("marge gauche"));
    lbMargeGauchePage_ = initBuLabelDonnee();
    pFormatPageInfosCol1.add(lbMargeGauchePage_);
    pFormatPageInfosCol2.add(initBuLabelTitre("marge droite"));
    lbMargeDroitePage_ = initBuLabelDonnee();
    pFormatPageInfosCol2.add(lbMargeDroitePage_);
    pFormatPageInfos.add(pFormatPageInfosCol1);
    pFormatPageInfos.add(pFormatPageInfosCol2);
    pFormatPage.add(pFormatPageInfos, BuBorderLayout.CENTER);
    // Le bouton pour modifier le format de la page
    final BuButton btModifier = new BuButton(EbliResource.EBLI.getString("Modifier"));
    btModifier.setActionCommand("MODIFIER");
    btModifier.addActionListener(this);
    pFormatPage.add(btModifier, BuBorderLayout.EAST);
    return pFormatPage;
  }

  /**
   * Construit le panel qui affiche les entetes et pieds de pages.
   */
  private BuPanel construirePanelEntetePiedDePage() {
    // construit le panel et son border
    final BuPanel pEntetePiedDePage = new BuPanel();
    pEntetePiedDePage.setLayout(new BuGridLayout(4, 10, 10, true, true));
    pEntetePiedDePage.setBorder(initTitreBorder("Ent�te/Pied de page"));
    // construit les titres des colonnes
    pEntetePiedDePage.add(new BuLabel(""));
    pEntetePiedDePage.add(initBuLabelTitre("Gauche"));
    pEntetePiedDePage.add(initBuLabelTitre("Centre"));
    pEntetePiedDePage.add(initBuLabelTitre("Droite"));
    // construit le titre de la ligne de l'entete.
    final BuPanel pEntete = new BuPanel();
    pEntete.setLayout(new GridLayout(2, 1, 10, 0));
    pEntete.add(initBuLabelTitre("Ent�te"));
    cbEnteteVisible_ = new BuCheckBox(EbliResource.EBLI.getString("visible"));
    pEntete.add(cbEnteteVisible_);
    // les editeurs concernant l'entete du document.
    enteteGauche_ = new EntetePiedEditeur();
    enteteMilieu_ = new EntetePiedEditeur();
    enteteDroit_ = new EntetePiedEditeur();
    cbEnteteVisible_.addItemListener(enteteGauche_);
    cbEnteteVisible_.addItemListener(enteteMilieu_);
    cbEnteteVisible_.addItemListener(enteteDroit_);
    pEntetePiedDePage.add(pEntete);
    pEntetePiedDePage.add(enteteGauche_);
    pEntetePiedDePage.add(enteteMilieu_);
    pEntetePiedDePage.add(enteteDroit_);
    // construit le titre de la ligne du pied de page.
    final BuPanel pPied = new BuPanel();
    pPied.setLayout(new GridLayout(2, 1, 10, 0));
    pPied.add(initBuLabelTitre("Pied de page"));
    cbPiedVisible_ = new BuCheckBox(EbliResource.EBLI.getString("visible"));
    pPied.add(cbPiedVisible_);
    // les editeurs concernant le pied de page.
    piedGauche_ = new EntetePiedEditeur();
    piedMilieu_ = new EntetePiedEditeur();
    piedDroit_ = new EntetePiedEditeur();
    cbPiedVisible_.addItemListener(piedGauche_);
    cbPiedVisible_.addItemListener(piedMilieu_);
    cbPiedVisible_.addItemListener(piedDroit_);
    pEntetePiedDePage.add(pPied);
    pEntetePiedDePage.add(piedGauche_);
    pEntetePiedDePage.add(piedMilieu_);
    pEntetePiedDePage.add(piedDroit_);
    return pEntetePiedDePage;
  }

  /**
   * Met a jour les labels contenant les caracteristiques du format de la page en fonction de la variable interne
   * <code>pageFormat_</code>.
   */
  private void majPanelPageFormat() {
    // type.
    lbTypePage_.setText(EbliPageFormat.getPageFormatType(pageFormat_));
    // orientation.
    final int orientation = pageFormat_.getOrientation();
    switch (orientation) {
    case PageFormat.LANDSCAPE:
      lbOrientationPage_.setText(EbliResource.EBLI.getString("paysage"));
      break;
    case PageFormat.REVERSE_LANDSCAPE:
      lbOrientationPage_.setText(EbliResource.EBLI.getString("paysage invers�"));
      break;
    case PageFormat.PORTRAIT:
      lbOrientationPage_.setText(EbliResource.EBLI.getString("portrait"));
      break;
    default:
      break;
    }
    // largeur et hauteur
    final double largeur = pageFormat_.getWidth();
    final double hauteur = pageFormat_.getHeight();
    // marges
    final double margeHaut = pageFormat_.getImageableY();
    final double margeBas = hauteur - margeHaut - pageFormat_.getImageableHeight();
    final double margeGauche = pageFormat_.getImageableX();
    final double margeDroite = largeur - margeGauche - pageFormat_.getImageableWidth();
    lbLargeurPage_.setText(format(EbliPageFormat.pixelVersCm(largeur)) + getCmUnit());
    lbHauteurPage_.setText(format(EbliPageFormat.pixelVersCm(hauteur)) + getCmUnit());
    lbMargeHautPage_.setText(format(EbliPageFormat.pixelVersCm(margeHaut)) + getCmUnit());
    lbMargeBasPage_.setText(format(EbliPageFormat.pixelVersCm(margeBas)) + getCmUnit());
    lbMargeGauchePage_.setText(format(EbliPageFormat.pixelVersCm(margeGauche)) + getCmUnit());
    lbMargeDroitePage_.setText(format(EbliPageFormat.pixelVersCm(margeDroite)) + getCmUnit());
  }

  private String getCmUnit() {
    return " cm";
  }

  /**
   * Met a jour le editeurs d'entete et pied de page grace a <code>_epf</code>.
   */
  private void majPanelEntetePiedDePage(final EbliPageFormat _epf) {
    cbEnteteVisible_.setSelected(false);
    cbEnteteVisible_.setSelected(_epf.isEnteteVisible());
    enteteGauche_.setCleSelected(_epf.getEnteteGauche());
    enteteMilieu_.setCleSelected(_epf.getEnteteMilieu());
    enteteDroit_.setCleSelected(_epf.getEnteteDroit());
    cbPiedVisible_.setSelected(false);
    cbPiedVisible_.setSelected(_epf.isPiedVisible());
    piedGauche_.setCleSelected(_epf.getPiedGauche());
    piedMilieu_.setCleSelected(_epf.getPiedMilieu());
    piedDroit_.setCleSelected(_epf.getPiedDroit());
    majLabelsPrevisu();
  }

  /**
   * Met a jour les labels de previsualisation.
   */
  private void majLabelsPrevisu() {
    enteteGauche_.majLabel();
    enteteMilieu_.majLabel();
    enteteDroit_.majLabel();
    piedGauche_.majLabel();
    piedMilieu_.majLabel();
    piedDroit_.majLabel();
  }

  /**
   * Concerne uniquement le bouton de modification du format de la page.
   */
  @Override
  public void actionPerformed(final ActionEvent _ae) {
    final String com = _ae.getActionCommand();
    if ("MODIFIER".equals(com)) {
      pageFormat_ = PrinterJob.getPrinterJob().pageDialog(pageFormat_);
      majPanelPageFormat();
    }
  }

  /**
   * Modifier le format utilise pour initialiser ce panel. Si <code>_epf</code> est null, l'operation est ignoree.
   *
   * @param _epf utilise pour initialiser ce panel
   */
  public final void setEbliPageFormatInit(final EbliPageFormat _epf) {
    if (_epf == null) {
      FuLog.error("EbliPageFormat null");
    } else {
      pageFormat_ = _epf.getPageFormat();
    }
    majPanelPageFormat();
    majPanelEntetePiedDePage(_epf);
  }

  /**
   * Modifier la page utilisee pour previsualiser les entete et pied de page.
   */
  public final void setEbliPageablePrevisualisation(final EbliPageable _ep) {
    pageablePrevisu_ = _ep;
    majLabelsPrevisu();
  }

  /**
   * Renvoie une instance de <code>EbliPageFormat</code> prenant en compte les modifs apportes dans ce panel.
   */
  public EbliPageFormat getEbliPageFormatModifie() {
    final EbliPageFormat r = new EbliPageFormat();
    r.setPageFormat(pageFormat_);
    r.setEnteteVisible(cbEnteteVisible_.isSelected());
    r.setPiedVisible(cbPiedVisible_.isSelected());
    r.setEntete(enteteGauche_.getCleSelected(), enteteMilieu_.getCleSelected(), enteteDroit_.getCleSelected());
    r.setPied(piedGauche_.getCleSelected(), piedMilieu_.getCleSelected(), piedDroit_.getCleSelected());
    return r;
  }
  /**
   * Editeur contenant une comboBox pour selectionner l'element a afficher et un JLabel pour une previsualisation
   * eventuelle. Cet editeur "ecoute" les evenements envoye par les CheckBox controlant la visibilite de l'entete ou du
   * pied de page (<code>piedVisible</code>).
   */
   final class EntetePiedEditeur extends BuPanel implements ItemListener {
    /**
     * ComboBox contenant les differents type d'entete ou pied de page a afficher.
     */
    private final BuComboBox chElement_;
    /**
     * Label de previsualisation.
     */
    private final BuLabel lbPrevisu_;

    /**
     * Construit le panel avec un <code>GridLayout</code>, initialise <code>chElement_</code> avec les valeurs
     * proposees par <code>EbliPageFormat.getValeursTypeEntete()</code> et initialise le label.
     *
     * @see EbliPageFormat#getValeursTypeEntete()
     */
    EntetePiedEditeur() {
      this.setLayout(new GridLayout(2, 1, 2, 5));
      chElement_ = new BuComboBox();
      chElement_.setModel(new DefaultComboBoxModel(EbliPageFormat.getValeursTypeEntete()));
      chElement_.addItemListener(this);
      this.add(chElement_);
      lbPrevisu_ = initBuLabelDonnee();
      lbPrevisu_.setPreferredSize(chElement_.getPreferredSize());
      this.add(lbPrevisu_);
    }

    /**
     * Met a jour le label de previsu en fonction de <code>pageablePrevisu</code>.
     */
    void majLabel() {
      if (pageablePrevisu_ == null) {
        lbPrevisu_.setVisible(false);
      } else {
        lbPrevisu_.setVisible(true);
        lbPrevisu_.setText(EbliPageFormat.getEquivalent(getCleSelected(), pageablePrevisu_, 0));
      }
    }

    /**
     * Change la couleur de fond du label en fonction de <code>_b</code>.
     */
    void setLabelEnabled(final boolean _b) {
      if (_b) {
        lbPrevisu_.setBackground(Color.white);
      } else {
        lbPrevisu_.setBackground(this.getBackground());
      }
    }

    /**
     * initialise l'item selectionne en fonction de la cle <code>_cle</code>.
     *
     * @see EbliPageFormat#getValeurTypeEnteteDepuisCle(String)
     */
    void setCleSelected(final String _cle) {
      chElement_.setSelectedItem(EbliPageFormat.getValeurTypeEnteteDepuisCle(_cle));
      majLabel();
    }

    /**
     * Renvoie la chaine cle correspondante a la valeur selectionnee.
     *
     * @see EbliPageFormat#getCleTypeEnteteDepuisValeur(String)
     */
    String getCleSelected() {
      return EbliPageFormat.getCleTypeEnteteDepuisValeur((String) chElement_.getSelectedItem());
    }

    /**
     * Modifie le contenu du label si l'item selectionne de <code>chElement_</code> a ete modifie ou modifie l'etat du
     * label en fontion de la CheckBox emettrice de l'evenement.
     */
    @Override
    public void itemStateChanged(final ItemEvent _e) {
      final Object source = _e.getSource();
      if (source instanceof BuCheckBox) {
        setLabelEnabled(((BuCheckBox) source).isSelected());
      } else if (source.equals(chElement_) && _e.getStateChange() == ItemEvent.SELECTED) {
        majLabel();
      }
    }
  }
}
