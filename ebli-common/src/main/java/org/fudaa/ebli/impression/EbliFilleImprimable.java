/*
 * @creation     2001-10-16
 * @modification $Date: 2006-09-19 14:55:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.impression;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuInternalFrame;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
/**
 * Cette classe abstraite implemente les fonctions principales pour rendre
 * une BuInternalFrame imprimable. Il reste a implanter les fonctions
 * <code>print</code> et <code>getNumberOfPages</code>.
 *
 * @version      $Id: EbliFilleImprimable.java,v 1.10 2006-09-19 14:55:54 deniger Exp $
 * @author       Fred Deniger
 */
public abstract class EbliFilleImprimable
  extends BuInternalFrame
  implements EbliPageable {
  /**
   * le delegue par defaut qui gere les instances de <code>PageFormat</code> et
   * de <code>Printable</code>.
   */
  private EbliPageableDelegate delegueImpression_;
  /**
   * les informations sur le logiciel utiles pour l'entete et le pied de page.
   */
  private BuInformationsSoftware is_;
  /**
   * les informations sur le document utiles pour l'entete et le pied de page.
   */
  private BuInformationsDocument id_;
  /**
   * cf EbliFilleImprimable("",true,true,true,true,null,null).
   */
  public EbliFilleImprimable() {
    this("", true, true, true, true, null, null);
  }
  /**
   * cf EbliFilleImprimable(_titre,_re,_clo,_max,_ico,null,null).
   */
  public EbliFilleImprimable(
    final String _titre,
    final boolean _re,
    final boolean _clo,
    final boolean _max,
    final boolean _ico) {
    this(_titre, _re, _clo, _max, _ico, null, null);
  }
  /**
   * Appelle le constructeur :<code>BuInternalFrame(_titre,_re,_clo,_max,_ico)
   * </code>. Les <code>BuInformationsSoftware</code> sont recuperees a partir
   * de <code>_appli</code>.
   */
  public EbliFilleImprimable(
    final String _titre,
    final boolean _re,
    final boolean _clo,
    final boolean _max,
    final boolean _ico,
    final BuCommonInterface _appli,
    final BuInformationsDocument _id) {
    super(_titre, _re, _clo, _max, _ico);
    if (_appli != null) {
      is_= _appli.getInformationsSoftware();
    }
    id_= _id;
    //delegueImpression_=new EbliPageableDelegate(this);
  }
  @Override
  public String[] getEnabledActions() {
    return new String[] { "IMPRIMER", "MISEENPAGE", "PREVISUALISER" };
  }
  public void setInformationsSoftware(final BuInformationsSoftware _is) {
    is_= _is;
  }
  public void setInformationsDocument(final BuInformationsDocument _id) {
    id_= _id;
  }
  @Override
  public Printable getPrintable(final int _i) {
    if (delegueImpression_ == null) {
      delegueImpression_= new EbliPageableDelegate(this);
    }
    return delegueImpression_.getPrintable(_i);
  }
  @Override
  public PageFormat getPageFormat(final int _i) {
    if (delegueImpression_ == null) {
      delegueImpression_= new EbliPageableDelegate(this);
    }
    return delegueImpression_.getPageFormat(_i);
  }
  @Override
  public EbliPageFormat getDefaultEbliPageFormat() {
    if (delegueImpression_ == null) {
      delegueImpression_= new EbliPageableDelegate(this);
    }
    return delegueImpression_.getDefaultEbliPageFormat();
  }
  @Override
  public BuInformationsSoftware getInformationsSoftware() {
    if (is_ == null) {
      is_= new BuInformationsSoftware();
    }
    return is_;
  }
  @Override
  public BuInformationsDocument getInformationsDocument() {
    if (id_ == null) {
      id_= new BuInformationsDocument();
    }
    return id_;
  }
}
