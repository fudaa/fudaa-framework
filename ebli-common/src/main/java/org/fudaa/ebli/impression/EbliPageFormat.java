/*
 * @creation 2001-09-27
 * 
 * @modification $Date: 2006-09-19 14:55:54 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.org
 */
package org.fudaa.ebli.impression;

import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInformationsSoftware;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.geom.Rectangle2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterJob;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaSizeName;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Une extension du <code>PageFormat</code> qui gere les entetes et pieds de pages.
 * 
 * @version $Id: EbliPageFormat.java,v 1.14 2006-09-19 14:55:54 deniger Exp $
 * @author Fred Deniger
 */
public class EbliPageFormat {

  /**
   * La font utilisee pour les entetes et pied de page.
   */
  private static final Font FONTBASE = new Font("SansSerif", Font.PLAIN, 10);

  private static String getNth() {
    return "nothing";
  }

  /**
   * Les differents types d'entetes. La premiere colonne correspond aux cles des entetes et la deuxieme aux valeurs
   * affichees.
   */
  static final String[][] TYPE_ENTETE = new String[][] { { getNth(), EbliResource.EBLI.getString("aucun") },
      { "page", EbliResource.EBLI.getString("page") }, { "date", EbliResource.EBLI.getString("date") },
      { "title", EbliResource.EBLI.getString("titre du document") }, { "author", EbliResource.EBLI.getString("auteur") },
      { "organization", EbliResource.EBLI.getString("organisation") }, { "department", EbliResource.EBLI.getString("departement") },
      { "contact", EbliResource.EBLI.getString("email") }, { "application", EbliResource.EBLI.getString("application") },
      { "location", EbliResource.EBLI.getString("localisation du fichier") }, { "comment", EbliResource.EBLI.getString("commentaires") } };
  /**
   * le format de page utilise.
   */
  private PageFormat pageFormat_;
  /**
   * Correspond au type d'entete utilise (la ligne du tableau <code>TYPE_ENTETE</code>. utilise en interne uniquement.
   */
  private int enteteGauche_;
  private int enteteMilieu_;
  private int enteteDroit_;
  private int piedGauche_;
  private int piedMilieu_;
  private int piedDroit_;
  /**
   * <code>true</code>, si l'entete est visible.
   */
  private boolean enteteVisible_;
  /**
   * <code>true</code>, si le pied est visible.
   */
  private boolean piedVisible_;

  /**
   * appelle le constructeur <code>EbliPageFormat(PageFormat)</code> avec comme parametre <code>null</code>.
   */
  public EbliPageFormat() {
    this(null);
  }

  /**
   * Initialise le format avec <code>_format</code> si non null ( un format A4 par defaut est choisi sinon ). L'entete
   * et le pied sont par defaut vide et invisible.
   */
  public EbliPageFormat(final PageFormat _format) {
    if (_format == null) {
      pageFormat_ = construireFormatA4Portrait();
    } else {
      pageFormat_ = _format;
    }
    final int temp = getIndexDepuisCle(getNth());
    enteteGauche_ = temp;
    enteteMilieu_ = temp;
    enteteDroit_ = temp;
    piedGauche_ = temp;
    piedMilieu_ = temp;
    piedDroit_ = temp;
    enteteVisible_ = false;
    piedVisible_ = false;
  }

  /**
   * Construit un format de page correspondant a une feuille A4 Portrait avec des marges de 1cm.
   */
  public static final PageFormat construireFormatA4Portrait() {
    MediaSizeName isoA4 = MediaSizeName.ISO_A4;
    return getPageFormat(isoA4,true);
  }

  public static PageFormat getPageFormat(MediaSizeName isoA4, boolean withMarge) {
    PrintRequestAttributeSet set = new HashPrintRequestAttributeSet(isoA4);
    PageFormat validatePage = PrinterJob.getPrinterJob().validatePage(PrinterJob.getPrinterJob().getPageFormat(set));
    double marge = withMarge ? cmVersPixel(1.5) : 0;
    Paper paper = validatePage.getPaper();
    double x = paper.getImageableX() + marge;
    double y = paper.getImageableY() + marge;
    double w = validatePage.getImageableWidth() - 2 * marge;
    double h = validatePage.getImageableHeight() - 2 * marge;
    paper.setImageableArea(x, y, w, h);
    validatePage.setPaper(paper);
    return validatePage;
  }

  /**
   * renvoie l'index de <code>TYPE_ELEMENT</code> correspondant a la cle <code>_cle</code>( premiere colonne ).
   */
  private static int getIndexDepuisCle(final String _cle) {
    if (_cle == null) {
      return 0;
    }
    for (int i = 0; i < TYPE_ENTETE.length; i++) {
      if (TYPE_ENTETE[i][0].equals(_cle)) {
        return i;
      }
    }
    return 0;
  }

  /**
   * renvoie l'index de <code>TYPE_ELEMENT</code> correspondant a la valeur <code>_valeur</code>( deuxieme colonne ).
   */
  private static int getIndexDepuisValeur(final String _valeur) {
    if (_valeur == null) {
      return 0;
    }
    for (int i = 0; i < TYPE_ENTETE.length; i++) {
      if (TYPE_ENTETE[i][1].equals(_valeur)) {
        return i;
      }
    }
    return 0;
  }

  /**
   * renvoie la cle de <code>TYPE_ELEMENT</code> correspondant a l'index <code>_index</code>( premiere colonne ).
   */
  private static String getCleDepuisIndex(final int _index) {
    if ((_index > -1) && (_index < TYPE_ENTETE.length)) {
      return TYPE_ENTETE[_index][0];
    }
    return getNth();
  }

  /**
   * renvoie la valeur de <code>TYPE_ELEMENT</code> correspondant a l'index <code>_index</code>( deuxieme colonne ).
   */
  private static String getValeurDepuisIndex(final int _index) {
    if ((_index > -1) && (_index < TYPE_ENTETE.length)) {
      return TYPE_ENTETE[_index][1];
    }
    return getValeurDepuisIndex(getIndexDepuisCle(getNth()));
  }

  /**
   * retourne <code>true</code>, si la cle correspondant a l'index <code>_test</code> est <code>"nothing"</code>.
   */
  private static boolean isVide(final int _test) {
    return getNth().equals(getCleDepuisIndex(_test));
  }

  /**
   * retourne <code>true</code>, si les cles correspondant aux index passes en parametres sont toutes egales a
   * <code>"nothing"</code>.
   */
  private static boolean isVide(final int _gauche, final int _milieu, final int _droit) {
    return (isVide(_gauche) && isVide(_milieu) && isVide(_droit));
  }

  /**
   * En fonction de <code>_g</code>, renvoie la hauteur de la Font <code>FONTBASE</code>.
   */
  private static int getHauteurFONTBASE(final Graphics _g) {
    _g.setFont(FONTBASE);
    final FontMetrics metric = _g.getFontMetrics();
    return metric.getMaxAscent() + metric.getMaxDescent() + metric.getLeading();
  }

  /**
   * Renvoie <code>true</code>, si les rectangles de dimensions (<code>_hBase</code>, <code>_wBase</code>) et (
   * <code>_hComp</code>,<code>_wComp</code>) sont equivalents.
   */
  private static boolean isRectanglesEquivalents(final int _hBase, final int _wBase, final int _hComp, final int _wComp) {
    return (((_hBase == _hComp) && (_wBase == _wComp)) || ((_hBase == _wComp) && (_wBase == _hComp)));
  }

  /**
   * Renvoie la cle du type d'entete correpondante a la valeur <code>_valeur</code>. La cle d'entete est utilise en
   * interne pour l'identification et la valeur est l'equivalent affiche.
   */
  public static String getCleTypeEnteteDepuisValeur(final String _valeur) {
    return getCleDepuisIndex(getIndexDepuisValeur(_valeur));
  }

  /**
   * Renvoie la valeur du type d'entete correpondante a la cle <code>_cle</code>.
   */
  public static String getValeurTypeEnteteDepuisCle(final String _cle) {
    return getValeurDepuisIndex(getIndexDepuisCle(_cle));
  }

  /**
   * Les differents types d'entetes possibles.
   */
  public static String[] getValeursTypeEntete() {
    final int taille = TYPE_ENTETE.length;
    final String[] r = new String[taille];
    for (int i = 0; i < taille; i++) {
      r[i] = TYPE_ENTETE[i][1];
    }
    return r;
  }

  /**
   * A partir des informations donnees par <code>_pageable</code> et <code>_pageImprimee</code>, renvoie la chaine
   * correspondante a la cle <code>_cle</code> du tableau <code>TYPE_ENTETE</code>.
   * 
   * @param _cle cle du tableau <code>TYPE_ENTETE</code>.
   * @param _pageable contient les informations du document.
   * @param _pageImprimee le numero de la page a imprimer.
   * @see #TYPE_ENTETE
   */
  public static String getEquivalent(final String _cle, final EbliPageable _pageable, final int _pageImprimee) {
    if (getNth().equals(_cle)) {
      return "";
    }
    if (_pageable == null) {
      return "";
    }
    if ("page".equals(_cle)) {
      return EbliResource.EBLI.getString("page ") + (_pageImprimee + 1) + "/" + _pageable.getNumberOfPages();
    }
    String r = "";
    BuInformationsDocument id = _pageable.getInformationsDocument();
    if (id == null) {
      id = new BuInformationsDocument();
    }
    if (("date".equals(_cle))) {
      r = id.date;
    } else if ("title".equals(_cle)) {
      r = id.name;
    } else if ("author".equals(_cle)) {
      r = id.author;
    } else if ("organization".equals(_cle)) {
      r = id.organization;
    } else if ("department".equals(_cle)) {
      r = id.department;
    } else if ("contact".equals(_cle)) {
      r = id.contact;
    } else if ("location".equals(_cle)) {
      r = id.location;
    } else if ("comment".equals(_cle)) {
      r = id.comment;
    }
    BuInformationsSoftware is = _pageable.getInformationsSoftware();
    if (is == null) {
      is = new BuInformationsSoftware();
    }
    if ("application".equals(_cle)) {
      r = is.name + " " + is.version;
    }
    if (r != null) {
      r = r.trim();
      if (r.length() > 0) {
        return r;
      }
    }
    return "??";
  }

  /**
   * Transforme des centimetres en pixels: un pixel represente 1/72 de inch et un inch=2,54 cm.
   */
  public static double cmVersPixel(final double _cm) {
    return (getScreenResolution() * _cm / 2.54);
  }
  
  
  private static double getScreenResolution(){
    return Toolkit.getDefaultToolkit().getScreenResolution();
  }

  /**
   * pixels en cm.
   */
  public static double pixelVersCm(final double _pix) {
    return (2.54 * _pix / getScreenResolution());
  }

  /**
   * Renvoie l'intitule correspondant au format <code>_type</code>.
   */
  public static String getPageFormatType(final PageFormat _type) {
    //h et w en mm
    final int h = (int) Math.round(pixelVersCm(_type.getHeight()) * 10);
    final int w = (int) Math.round(pixelVersCm(_type.getWidth()) * 10);
    if (isRectanglesEquivalents(297, 210, h, w)) {
      return EbliResource.EBLI.getString("A4");
    } else if (isRectanglesEquivalents(297, 420, h, w)) {
      return EbliResource.EBLI.getString("A3");
    } else if (isRectanglesEquivalents(279, 216, h, w)) {
      return EbliResource.EBLI.getString("Lettre US");
    } else if (isRectanglesEquivalents(356, 216, h, w)) {
      return EbliResource.EBLI.getString("Legal US");
    } else if (isRectanglesEquivalents(176, 250, h, w)) {
      return EbliResource.EBLI.getString("B5");
    }
    return "" + (h / 10) + "x" + (w / 10);
  }

  /**
   * Dessine si necessaire l'entete et le pied de page en fonction des parametres.
   */
  public static void dessinePage(final Graphics _g, final EbliPageable _target, final int _page) {
    final Graphics2D g2d = (Graphics2D) _g;
    //si l'entete et le pied de page sont vides ou invisibles: arret
    final EbliPageFormat ebliPage = _target.getDefaultEbliPageFormat();
    if ((!ebliPage.isEnteteVisible()) && (!ebliPage.isPiedVisible())) {
      return;
    }
    if ((ebliPage.isEnteteVide()) && (ebliPage.isPiedVide())) {
      return;
    }
    //recuperation des caracteristiques initiales.
    final Font initFont = g2d.getFont();
    final Color initColor = g2d.getColor();
    //modification pour l'impression.
    g2d.setColor(Color.black);
    g2d.setFont(FONTBASE);
    //recuperation de la hauteur et de la largeur de la page.
    final PageFormat format = ebliPage.getPageFormat();
    final int largeur = (int) (format.getImageableWidth());
    final int hauteur = (int) (format.getImageableHeight());
    //si entete visible et non vide
    if ((ebliPage.isEnteteVisible()) && (!ebliPage.isEnteteVide())) {
      //recupere clip initial.
      final Shape initShape = g2d.getClip();
      //recupere les 3 parties de l'entete.
      final String[] entete = ebliPage.getEnteteEquivalent(_target, _page);
      //recupere les coordonnees du point en haut a gauche.
      final double x = format.getImageableX();
      final double y = format.getImageableY();
      // la hauteur de l'entete avec 2 de plus par securite.
      final int hauteurEntete = ebliPage.getHauteurEntete(g2d) + 2;
      //On se place au coin haut gauche.
      g2d.translate(x, y);
      final Rectangle2D drawZone = new Rectangle2D.Double(0, 0, largeur, hauteurEntete);
      g2d.clip(drawZone);
      //On dessine les 3 parties.
      dessineChaine(g2d, entete, largeur);
      //On se replace a l'origine initiale et reinitialise le clip
      g2d.translate(-x, -y);
      g2d.setClip(initShape);
    }
    if ((ebliPage.isPiedVisible()) && (!ebliPage.isPiedVide())) {
      final Shape initShape = g2d.getClip();
      final String[] pied = ebliPage.getPiedEquivalent(_target, _page);
      final double x = format.getImageableX();
      final double y = format.getImageableY() + hauteur - ebliPage.getHauteurPied(g2d);
      final int hauteurPied = ebliPage.getHauteurPied(g2d) + 2;
      g2d.translate(x, y);
      final Rectangle2D drawZone = new Rectangle2D.Double(0, 0, largeur, hauteurPied);
      g2d.clip(drawZone);
      dessineChaine(g2d, pied, largeur);
      g2d.translate(-x, -y);
      g2d.setClip(initShape);
    }
    g2d.setFont(initFont);
    g2d.setColor(initColor);
  }

  /**
   * Permet de dessiner les trois chaines <code>_chaines</code> dans la largeur precisee.
   */
  private static void dessineChaine(final Graphics _g2d, final String[] _chaines, final int _largeur) {
    final FontMetrics metric = _g2d.getFontMetrics();
    int styloX = 0;
    final int styloY = metric.getMaxAscent();
    //partie gauche
    if ((_chaines[0] != null) && (_chaines[0].length() > 0)) {
      _g2d.drawString(_chaines[0], styloX, styloY);
    }
    //centre
    if ((_chaines[1] != null) && (_chaines[1].length() > 0)) {
      final double largeurChaine = metric.getStringBounds(_chaines[1], _g2d).getWidth();
      styloX = (int) ((_largeur - largeurChaine) / 2D);
      _g2d.drawString(_chaines[1], styloX, styloY);
    }
    //droit
    if ((_chaines[2] != null) && (_chaines[2].length() > 0)) {
      final double largeurChaine = metric.getStringBounds(_chaines[2], _g2d).getWidth();
      styloX = (int) (_largeur - largeurChaine);
      _g2d.drawString(_chaines[2], styloX, styloY);
    }
  }

  /**
   * renvoie la hauteur en pixel necessaire a l'affichage de l'entete.
   */
  private int getHauteurEntete(final Graphics _g) {
    if ((isEnteteVide()) || (!isEnteteVisible())) {
      return 0;
    }
    return getHauteurFONTBASE(_g);
  }

  private int getHauteurPied(final Graphics _g) {
    if ((isPiedVide()) || (!isPiedVisible())) {
      return 0;
    }
    return getHauteurFONTBASE(_g);
  }

  /**
   * Renvoie <code>true</code> si les 3 parties de l'entetes correspondent a "nothing".
   */
  private boolean isEnteteVide() {
    return isVide(enteteGauche_, enteteMilieu_, enteteDroit_);
  }

  private boolean isPiedVide() {
    return isVide(piedGauche_, piedMilieu_, piedDroit_);
  }

  /**
   * Renvoie le format de page utilise.
   */
  public PageFormat getPageFormat() {
    return pageFormat_;
  }

  /**
   * Modifie le format de page utilise apres validation.
   */
  public void setPageFormat(final PageFormat _format) {
    if (_format == null) {
      return;
    }
    pageFormat_ = PrinterJob.getPrinterJob().validatePage(_format);
  }

  /**
   * Renvoie le format en prenant en compte de la taille de l'entete et du pied de page. Les marges "haut" et "bas" sont
   * augmentees en fonction de la taille de l'entete et du pied de page.
   */
  public PageFormat getDocumentPageFormat(final Graphics _g) {
    final PageFormat r = (PageFormat) pageFormat_.clone();
    final int he = getHauteurEntete(_g);
    final int hp = getHauteurPied(_g);
    final double ixPF = r.getImageableX();
    final double iyPF = r.getImageableY() + he;
    final double iwPF = r.getImageableWidth();
    final double ihPF = r.getImageableHeight() - he - hp;
    final Paper p = r.getPaper();
    if (r.getOrientation() == PageFormat.PORTRAIT) {
      p.setImageableArea(ixPF, iyPF, iwPF, ihPF);
    } else {
      p.setImageableArea(iyPF, ixPF, ihPF, iwPF);
    }
    r.setPaper(p);
    return PrinterJob.getPrinterJob().validatePage(r);
  }

  /**
   * Renvoie <code>true</code>, si l'entete est visible.
   */
  public boolean isEnteteVisible() {
    return enteteVisible_;
  }

  public boolean isPiedVisible() {
    return piedVisible_;
  }

  /**
   * Modifie la visibilite de l'entete.
   */
  public void setEnteteVisible(final boolean _new) {
    enteteVisible_ = _new;
  }

  public void setPiedVisible(final boolean _new) {
    piedVisible_ = _new;
  }

  /**
   * Renvoie la cle correspondante a la partie gauche de l'entete.
   */
  public String getEnteteGauche() {
    return getCleDepuisIndex(enteteGauche_);
  }

  public String getEnteteMilieu() {
    return getCleDepuisIndex(enteteMilieu_);
  }

  public String getEnteteDroit() {
    return getCleDepuisIndex(enteteDroit_);
  }

  public String getPiedGauche() {
    return getCleDepuisIndex(piedGauche_);
  }

  public String getPiedMilieu() {
    return getCleDepuisIndex(piedMilieu_);
  }

  public String getPiedDroit() {
    return getCleDepuisIndex(piedDroit_);
  }

  /**
   * modifie la cle correspondante a la partie gauche de l'entete. Si <code>_new</code> ne correspond pas a une entete,
   * la cle "nothing" est utilisee.
   */
  public void setEnteteGauche(final String _new) {
    enteteGauche_ = getIndexDepuisCle(_new);
  }

  public void setEnteteMilieu(final String _new) {
    enteteMilieu_ = getIndexDepuisCle(_new);
  }

  public void setEnteteDroit(final String _new) {
    enteteDroit_ = getIndexDepuisCle(_new);
  }

  public void setPiedGauche(final String _new) {
    piedGauche_ = getIndexDepuisCle(_new);
  }

  public void setPiedMilieu(final String _new) {
    piedMilieu_ = getIndexDepuisCle(_new);
  }

  public void setPiedDroit(final String _new) {
    piedDroit_ = getIndexDepuisCle(_new);
  }

  /**
   * Modifie les trois parties de l'entete en meme temps.
   */
  public void setEntete(final String _gauche, final String _milieu, final String _droit) {
    setEnteteGauche(_gauche);
    setEnteteMilieu(_milieu);
    setEnteteDroit(_droit);
  }

  public void setPied(final String _gauche, final String _milieu, final String _droit) {
    setPiedGauche(_gauche);
    setPiedMilieu(_milieu);
    setPiedDroit(_droit);
  }

  /**
   * Renvoie les trois partie de l'entete.
   * 
   * @return un tableau a 3 lignes.
   */
  public String[] getEntete() {
    final String[] r = new String[3];
    r[0] = getEnteteGauche();
    r[1] = getEnteteMilieu();
    r[2] = getEnteteDroit();
    return r;
  }

  public String[] getPied() {
    final String[] r = new String[3];
    r[0] = getPiedGauche();
    r[1] = getPiedMilieu();
    r[2] = getPiedDroit();
    return r;
  }

  /**
   * renvoie les valeurs reelles des 3 parties de l'entete en fonctin de <code>_page</code> et de <code>_numPage</code>.
   */
  public String[] getEnteteEquivalent(final EbliPageable _page, final int _numPage) {
    final String[] r = new String[3];
    r[0] = getEquivalent(getEnteteGauche(), _page, _numPage);
    r[1] = getEquivalent(getEnteteMilieu(), _page, _numPage);
    r[2] = getEquivalent(getEnteteDroit(), _page, _numPage);
    return r;
  }

  public String[] getPiedEquivalent(final EbliPageable _page, final int _numPage) {
    final String[] r = new String[3];
    r[0] = getEquivalent(getPiedGauche(), _page, _numPage);
    r[1] = getEquivalent(getPiedMilieu(), _page, _numPage);
    r[2] = getEquivalent(getPiedDroit(), _page, _numPage);
    return r;
  }
}