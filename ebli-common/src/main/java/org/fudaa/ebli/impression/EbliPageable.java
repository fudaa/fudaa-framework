/*
 * @creation     2001-09-27
 * @modification $Date: 2006-04-12 15:28:07 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.impression;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInformationsSoftware;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.Pageable;
/**
 * Une interface qui permet de mieux gerer l'impression des documents. Si
 * l'interface est etendue par un <code>JComponent</code>, il est possible de
 * modifier des parametres grace aux proprietes(cf les constantes KEY) avec
 * la methode <code>JComponent.putClientProperty</code>.Il est conseille de
 * deleguer les operations a <code>EbliPageableDelegate</code>.Il est possible
 * de s'inspirer de <code>BGraphe</code> pour l'utilisation de cette interface.
 *
 * @version      $Id: EbliPageable.java,v 1.7 2006-04-12 15:28:07 deniger Exp $
 * @author       Fred Deniger
 */
public interface EbliPageable extends Pageable {
  /**
   * La chaine utilisee comme cle dans les "client property" des <code>
   * JComponent</code> pour retrouver la valeur du
   * <code>DefaultPageFormat</code>. La valeur correspondante doit donc
   * etre une instance de <code>EbliPageFormat</code>.
   */
  String DEFAULT_PAGE_FORMAT= "DefaultPageFormat";
  /**
   * La methode centrale qui permet d'imprimer (idem que celle de l'interface
   * printable). Le format <code>_format</code> sera celui donne par la methode
   * <code>Pageable.getPageFormat(int)</code>.
   *
   * @return <code>Printable.PAGE_EXISTS</code> si la page existe,
   *         sinon <code>Printable.NO_SUCH_PAGE</code>.
   */
  int print(Graphics _g, PageFormat _format, int _page);
  /**
   * Renvoie les informations sur le document.
   */
  BuInformationsDocument getInformationsDocument();
  /**
   * Renvoie les informations sur l'application.
   */
  BuInformationsSoftware getInformationsSoftware();
  /**
   * Renvoie le format par defaut.Cette methode peut etre deleguee.
   */
  EbliPageFormat getDefaultEbliPageFormat();
}
