/*
 * @creation     2001-06-29
 * @modification $Date: 2006-09-19 14:55:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.impression;

import java.awt.*;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.text.AttributedString;
import javax.swing.JTable;

/**
 * @version $Revision: 1.9 $ $Date: 2006-09-19 14:55:54 $ by $Author: deniger $
 * @author Fred Deniger
 */
public final class EbliPrinter {

  private EbliPrinter() {}
  /**
   * La font utilisee pour ecrire les entete et pied de page.
   */
  public final static Font FONTBASE = new Font("SansSerif", Font.PLAIN, 10);

  /**
   * Initialise la couleur (noire) et la font pour _g.
   * 
   * @param _g graphics a modifier.
   */
  public static void initGraphics(final Graphics _g) {
    _g.setColor(Color.black);
    _g.setFont(FONTBASE);
  }

  /**
   * Appelle la methode initGraphics(_g), translate l'origine sur le coin superieur gauche imprimable de _format puis
   * retourne les coordonnees de ce point.
   * 
   * @param _format le PageFormat a respecter.
   * @param _g2d
   * @return les coordonnees du coin superieur gauche de la zone imprimable.
   */
  public static double[] initGraphics(final Graphics2D _g2d, final PageFormat _format) {
    initGraphics(_g2d);
    final double x = _format.getImageableX();
    final double y = _format.getImageableY();
    _g2d.translate(x, y);
    return new double[] { x, y };
  }

  /**
   * Renvoie le facteur d'echelle maximale qui permet de transformer un objet de taille (_w,_h) en un objet qui tiend
   * dans le format _format.
   * 
   * @param _format le format voulu.
   * @param _w largeur de l'objet a dimensionner.
   * @param _h hauteur de l'objet a dimensionner.
   * @return facteur d'echelle maximal.
   */
  public static double echelle(final PageFormat _format, final double _w, final double _h) {
    // recuperation des dimension de la zone imprimable
    final double hImprimable = _format.getImageableHeight();
    final double wImprimable = _format.getImageableWidth();
    /*
     * calcule et renvoie du facteur maximale pour que le rectangle de taille (_w,_h) "rentre" dans les dimensions
     * precedentes
     */
    return Math.min(wImprimable / _w, hImprimable / _h);
  }

  /**
   * Methode par defaut pour imprimer un composant <code>_c</code>. Calcule le facteur d'echelle entre le component
   * et le format de la page. L'echelle du graphics est modifiee en consequence. Le graphics est finalement restaurer.
   * Il est possible de dessiner une bordure (si <code>_bordure</code> true). Seule une page est imprimee. Si
   * <code>_numPage</code>!= 0 alors la commande est ignoree est la valeur <code>Printable.NO_SUCH_PAGE</code> est
   * renvoyee.
   * 
   * @param _g
   * @param _format
   * @param _c
   * @param _bordure si true une bordure noire est ajoutee
   * @param _numPage le numero de page a imprimer
   */
  public static int printComponent(final Graphics _g, final PageFormat _format, final Component _c,
      final boolean _bordure, final int _numPage) {
    if (_numPage != 0) {
      return Printable.NO_SUCH_PAGE;
    }
    // Sauvegarde de Clip pour restauration finale.
    final Shape s = _g.getClip();
    // Utilisation de methode de Graphics2d.
    final Graphics2D g2d = (Graphics2D) _g;
    // Initialisation du graphics et translation de l 'origine.
    final double[] coord = initGraphics(g2d, _format);
    // Calcul du facteur d'echelle.
    final int wComp = _c.getWidth();
    final int hComp = _c.getHeight();
    final double facteur = echelle(_format, wComp, hComp);
    final double wCompDessine = wComp * facteur;
    final double hCompDessine = hComp * facteur;
    // definition de la zone d'impression
    final double xCentre = (_format.getImageableWidth() - wCompDessine) / 2;
    final double yCentre = (_format.getImageableHeight() - hCompDessine) / 2;
    g2d.translate(xCentre, yCentre);
    g2d.clip(new Rectangle.Double(-5D, -5D, wCompDessine + 10, hCompDessine + 10));
    // mise a l'echelle et impression
    g2d.scale(facteur, facteur);
    _c.printAll(g2d);
    if (_bordure) {
      g2d.draw(new Rectangle.Double(0D, 0D, wComp, hComp));
    }
    // Restauration du graphics.
    g2d.scale(1 / facteur, 1 / facteur);
    g2d.translate(-xCentre, -yCentre);
    g2d.translate(-coord[0], -coord[1]);
    g2d.setClip(s);
    return Printable.PAGE_EXISTS;
  }

  /**
   * Impression d'une image <code>_i</code>.
   * 
   * @param _g
   * @param _format
   * @param _i l'image a imprimer
   */
  public static void printImage(final Graphics _g, final PageFormat _format, final Image _i) {
    final Graphics2D g2d = (Graphics2D) _g;
    final double[] coord = initGraphics(g2d, _format);
    // Calcul du facteur d'echelle.
    final int wComp = _i.getWidth(null);
    final int hComp = _i.getHeight(null);
    final double facteur = echelle(_format, wComp, hComp);
    // si le facteur est inferieur a 1, reduire l'image.
    if (facteur < 1) {
      final int w = (int) Math.ceil(facteur * wComp);
      final int h = (int) Math.ceil(facteur * hComp);
      Image iTemp = _i.getScaledInstance(w, h, Image.SCALE_SMOOTH);
      g2d.drawImage(iTemp, 0, 0, null);
      iTemp = null;
    } else {
      g2d.drawImage(_i, 0, 0, null);
    }
    // Restauration du graphics.
    g2d.translate(-coord[0], -coord[1]);
  }

  /**
   * Impression d'une String justifiee sur la largeur de la page imprimable. La font est choisie par defaut: FONTBASE.
   * Le FontRenderContext peut etre ameliorer: a voir par la suite.
   * 
   * @param _g contexte graphique.
   * @param _format format de la page.
   * @param _s String a imprimer.
   */
  public static void printStringJustifie(final Graphics _g, final PageFormat _format, final String _s) {
    final Graphics2D g2d = (Graphics2D) _g;
    final double[] coord = initGraphics(g2d, _format);
    // x et y represente la position du "stylo"
    final float x = 0;
    float y = 0;
    // la largeur imprimable
    final float w = (float) _format.getImageableWidth();
    /*
     * creation d'un AttributedString car l'iterator est utilise par le LineBreakerMeasurer.
     */
    final AttributedString text = new AttributedString(_s);
    // Initialisation de la font.
    text.addAttribute(TextAttribute.FONT, FONTBASE);
    // Initialisation du LineBreaker
    final LineBreakMeasurer lineBreaker = new LineBreakMeasurer(text.getIterator(), g2d.getFontRenderContext());
    /*
     * 2 layouts sont utilises: le layout en cours qui est traite le layout suivant qui sera traite a la prochaine
     * boucle. Si c'est la derniere ligne (layout suivant est nul), on utilise un layout normal pour afficher la ligne
     * et non pas un layout justifie.
     */
    TextLayout layoutEnCours = lineBreaker.nextLayout(w);
    TextLayout layoutSuivant = lineBreaker.nextLayout(w);
    while (layoutSuivant != null) {
      // layout en cours traite, ce n'est pas la derniere ligne donc justifie.
      layoutEnCours = layoutEnCours.getJustifiedLayout(w);
      // le stylo est positionne sur la BaseLine
      y += layoutEnCours.getAscent();
      // Le texte est dessine
      layoutEnCours.draw(g2d, x, y);
      // Le stylo est positionne en haut de la prochaine ligne
      y += layoutEnCours.getDescent() + layoutEnCours.getLeading();
      // reinitialisation pour la prochaine ligne
      layoutEnCours = layoutSuivant;
      layoutSuivant = lineBreaker.nextLayout(w);
    }
    // derniere ligne
    layoutEnCours.draw(g2d, x, y + layoutEnCours.getAscent());
    g2d.translate(-coord[0], -coord[1]);
  }

  /**
   * Idem que printStringJustifie sauf que ... lignes non justifiees.
   * 
   * @param _g contexte graphique.
   * @param _format format de la page.
   * @param _s String a imprimer.
   */
  public static void printString(final Graphics _g, final PageFormat _format, final String _s) {
    final Graphics2D g2d = (Graphics2D) _g;
    final Font initFont = g2d.getFont();
    final Color initColor = g2d.getColor();
    final double[] coord = initGraphics(g2d, _format);
    // x et y represente la position du "stylo"
    final float x = 0;
    float y = 0;
    // la largeur imprimable
    final float w = (float) _format.getImageableWidth();
    /*
     * creation d'un AttributedString car l'iterator est utilise par le LineBreakerMeasurer.
     */
    final AttributedString text = new AttributedString(_s);
    // Initialisation de la font.
    text.addAttribute(TextAttribute.FONT, FONTBASE);
    // Initialisation du LineBreaker
    final LineBreakMeasurer lineBreaker = new LineBreakMeasurer(text.getIterator(), g2d.getFontRenderContext());
    // Initialisation du layout pour des paragraphes de largeur w
    TextLayout layoutEnCours = lineBreaker.nextLayout(w);
    // Tant qu'il y a des lignes...
    while (layoutEnCours != null) {
      // stylo positionne sur la BaseLine
      y += layoutEnCours.getAscent();
      // Ligne dessinee
      layoutEnCours.draw(g2d, x, y);
      // Stylo repositionne pour la prochaine ligne
      y += layoutEnCours.getDescent() + layoutEnCours.getLeading();
      layoutEnCours = lineBreaker.nextLayout(w);
    }
    g2d.setFont(initFont);
    g2d.setColor(initColor);
    g2d.translate(-coord[0], -coord[1]);
  }

  /**
   * Imprime tout le tableau en le considerant comme un Component. Cette methode est simple mais le rendu n'est pas bon.
   * 
   * @param _g
   * @param _format
   * @param _t
   */
  public static int printTableAll(final Graphics _g, final PageFormat _format, final JTable _t, final int _numPage) {
    return printComponent(_g, _format, _t, true, _numPage);
    // printTable(_g, _format, _t, -1);
  }

  public static int printTable(final Graphics _g, final PageFormat _format, final JTable _t, final int _numPage) {
    return printComponent(_g, _format, _t, true, _numPage);
    // printTable(_g, _format, _t, -1);
  }

  /**
   * ....
   * 
   * @param _g
   * @param _format
   * @param _t
   * @param _height
   */
  public static void printTable(final Graphics _g, final PageFormat _format, final JTable _t, final int _height,
      final int _numPage) {}
}
