/*
 * @creation     2001-09-27
 * @modification $Date: 2006-09-19 14:55:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.impression;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.Pageable;
import java.awt.print.Printable;
/**
 * Classe deleguee permettant de simplifier la construction d'une interface
 * <code>EbliPageable</code>. Ne verifie pas la methode
 * <code>isPageFormatModifiable</code>:c'est au delegateur de le faire.
 *
 * @version      $Id: EbliPageableDelegate.java,v 1.8 2006-09-19 14:55:54 deniger Exp $
 * @author       Fred Deniger
 */
public class EbliPageableDelegate {
  EbliPageable delegateur_;
  EbliPrintable[] printables_;
  EbliPageFormat pageFormat_;
  public EbliPageableDelegate(final EbliPageable _interface) {
    delegateur_= _interface;
    //le tableau de printable est construit des que demande( cf getPrintable ).
    printables_= null;
    //    pageFormat_=EbliMiseEnPagePreferencesPanel.getEbliPageFormat();
  }
  /**
   * Renvoie le nombre de pages du delegateur. Renvoie 1 si le nombre est inconnu
   * @return 1 si nombre inconnu.
   */
  private int nombreDePagesSur() {
    int r= delegateur_.getNumberOfPages();
    if ((r < 1)
      || (r == Pageable.UNKNOWN_NUMBER_OF_PAGES)
      || (r >= Integer.MAX_VALUE)) {
      r= 1;
    }
    return r;
  }
  /**
   * Construit les instances de EbliPrintable en fonction du nombre de page du delegateur.
   */
  private void construitPrintables() {
    final int taille= nombreDePagesSur();
    printables_= new EbliPrintable[taille];
    for (int i= 0; i < taille; i++) {
      printables_[i]= new EbliPrintable(i);
    }
  }
  /**
   * Renvoie le format par defaut.
   */
  public EbliPageFormat getDefaultEbliPageFormat() {
    if (pageFormat_ == null) {
      pageFormat_= EbliMiseEnPagePreferencesPanel.getEbliPageFormat();
    }
    return pageFormat_;
  }
  /**
   *  Renvoie le format des pages (toujours le meme).
   * @param _index inutile dans pour cette classe
   */
  public PageFormat getPageFormat(final int _index) {
    return getDefaultEbliPageFormat().getPageFormat();
  }
  /**
   * Renvoie les instances de printable pour la page <code>_page</code>.
   * @return EbliPrintable(_page)
   */
  public Printable getPrintable(final int _page) {
    if ((_page < 0) || (_page >= nombreDePagesSur())) {
      return null;
    }
    if ((printables_ == null) || (printables_.length != nombreDePagesSur())) {
      construitPrintables();
    }
    return printables_[_page];
  }
  /**
   * Classe permettant d'implanter l'interface Printable. L'impression se fait a partir de
   * la page <code>_page</code> du delegateur.
   */
  private class EbliPrintable implements Printable {
    int pageReelle_;
    EbliPrintable(final int _page) {
      pageReelle_= _page;
    }
    @Override
    public int print(final Graphics _g, final PageFormat _format, final int _pageDemandee) {
      if (_pageDemandee != 0) {
        return Printable.NO_SUCH_PAGE;
      }
      EbliPageFormat.dessinePage(_g, delegateur_, pageReelle_);
      return delegateur_.print(
        _g,
        delegateur_.getDefaultEbliPageFormat().getDocumentPageFormat(_g),
        pageReelle_);
    }
  }
}
