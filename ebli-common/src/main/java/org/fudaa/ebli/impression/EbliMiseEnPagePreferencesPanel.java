/*
 * @creation     2001-10-02
 * @modification $Date: 2006-09-19 14:55:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.impression;
import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuPreferences;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.ressource.EbliResource;
/**
 * Gere les preferences concernant les entete et pied de page utilises
 * pour l'impression. Permet egalement de recuperer le tableau de chaine des
 * entetes et pieds de pages correspondant aux preferences
 * (<code>getEntetePied</code>).
 *
 * @version      $Id$
 * @author       Fred Deniger
 * @see          org.fudaa.ebli.impression.EbliPageable
 */
public class EbliMiseEnPagePreferencesPanel
  extends BuAbstractPreferencesPanel {
  private static final String PORTRAIT_ORIENTATION= "portrait";
  private static final String LANDSCAPE_ORIENTATION= "landscape";
  private static final String REVERSE_LANDSCAPE_ORIENTATION=
    "reverse_landscape";
  /**
   * Les preferences concernees.
   */
  private static final BuPreferences OPTIONS= EbliPreferences.EBLI;
  private static final String ENTETE_GAUCHE_PREF= "impression.entete.gauche";
  private static final String ENTETE_MILIEU_PREF= "impression.entete.milieu";
  private static final String ENTETE_DROIT_PREF= "impression.entete.droite";
  private static final String ENTETE_VISIBLE_PREF= "impression.entete.visible";
  private static final String PIED_GAUCHE_PREF= "impression.pied.gauche";
  private static final String PIED_MILIEU_PREF= "impression.pied.milieu";
  private static final String PIED_DROIT_PREF= "impression.pied.droite";
  private static final String PIED_VISIBLE_PREF= "impression.pied.visible";
  private static final String PAPER_H_PREF= "impression.paper.height";
  private static final String PAPER_W_PREF= "impression.paper.width";
  private static final String PAPER_X_PREF= "impression.paper.x";
  private static final String PAPER_Y_PREF= "impression.paper.y";
  private static final String PAPER_IMAGEABLE_H_PREF=
    "impression.paper.imageable.height";
  private static final String PAPER_IMAGEABLE_W_PREF=
    "impression.paper.imageable.width";
  private static final String ORIENTATION_PREF= "impression.orientation";
  private final EbliMiseEnPagePanel panelMiseEnPage_;
  /**
   * instancie tous les elements et les ajoute a ce panel.
   */
  public EbliMiseEnPagePreferencesPanel() {
    super();
    panelMiseEnPage_= new EbliMiseEnPagePanel(getEbliPageFormat(), null);
    add(panelMiseEnPage_, BuBorderLayout.CENTER);
  }
  private static String getChaineOrientation(final int _orientation) {
    if (_orientation == PageFormat.LANDSCAPE) {
      return LANDSCAPE_ORIENTATION;
    } else if (_orientation == PageFormat.REVERSE_LANDSCAPE) {
      return REVERSE_LANDSCAPE_ORIENTATION;
    }
    return PORTRAIT_ORIENTATION;
  }
  private static int getIntOrientation(final String _orientation) {
    if (LANDSCAPE_ORIENTATION.equals(_orientation)) {
      return PageFormat.LANDSCAPE;
    }
    if (REVERSE_LANDSCAPE_ORIENTATION.equals(_orientation)) {
      return PageFormat.REVERSE_LANDSCAPE;
    }
    return PageFormat.PORTRAIT;
  }
  /**
   * Remplit la table a partir des valeurs des combobox.
   */
  private void fillTable() {
    final EbliPageFormat tempPF= panelMiseEnPage_.getEbliPageFormatModifie();
    //l'entete
    OPTIONS.putStringProperty(ENTETE_GAUCHE_PREF, tempPF.getEnteteGauche());
    OPTIONS.putStringProperty(ENTETE_MILIEU_PREF, tempPF.getEnteteMilieu());
    OPTIONS.putStringProperty(ENTETE_DROIT_PREF, tempPF.getEnteteDroit());
    OPTIONS.putBooleanProperty(ENTETE_VISIBLE_PREF, tempPF.isEnteteVisible());
    //le pied de page
    OPTIONS.putStringProperty(PIED_GAUCHE_PREF, tempPF.getPiedGauche());
    OPTIONS.putStringProperty(PIED_MILIEU_PREF, tempPF.getPiedMilieu());
    OPTIONS.putStringProperty(PIED_DROIT_PREF, tempPF.getPiedDroit());
    OPTIONS.putBooleanProperty(PIED_VISIBLE_PREF, tempPF.isPiedVisible());
    //l'orientation
    OPTIONS.putStringProperty(
      ORIENTATION_PREF,
      getChaineOrientation(tempPF.getPageFormat().getOrientation()));
    //les dimensions du papier
    final Paper tempP= tempPF.getPageFormat().getPaper();
    OPTIONS.putDoubleProperty(PAPER_H_PREF, tempP.getHeight());
    OPTIONS.putDoubleProperty(PAPER_W_PREF, tempP.getWidth());
    OPTIONS.putDoubleProperty(PAPER_X_PREF, tempP.getImageableX());
    OPTIONS.putDoubleProperty(PAPER_Y_PREF, tempP.getImageableY());
    OPTIONS.putDoubleProperty(
      PAPER_IMAGEABLE_H_PREF,
      tempP.getImageableHeight());
    OPTIONS.putDoubleProperty(
      PAPER_IMAGEABLE_W_PREF,
      tempP.getImageableWidth());
  }
  /**
   * le titre des preferences.
   */
  @Override
  public String getTitle() {
    return EbliResource.EBLI.getString("Mise en page");
  }
  @Override
  public boolean isPreferencesValidable() {
    return true;
  }
  @Override
  public boolean isPreferencesCancelable() {
    return true;
  }
  /**
   * Enregistrement des modifications apportees dans ce panel.
   */
  @Override
  public void validatePreferences() {
    fillTable();
    OPTIONS.writeIniFile();
  }
  /**
   * Annuler les modifications (relit les preferences).
   */
  @Override
  public void cancelPreferences() {
    OPTIONS.readIniFile();
    panelMiseEnPage_.setEbliPageFormatInit(getEbliPageFormat());
  }
  public static EbliPageFormat getEbliPageFormat() {
    //construit un EbliPageFormat par defaut
    final EbliPageFormat r= new EbliPageFormat(null);
    //l'entete
    final String nth = "nothing";
    r.setEntete(
      OPTIONS.getStringProperty(ENTETE_GAUCHE_PREF, nth),
      OPTIONS.getStringProperty(ENTETE_MILIEU_PREF, nth),
      OPTIONS.getStringProperty(ENTETE_DROIT_PREF, nth));
    r.setEnteteVisible(OPTIONS.getBooleanProperty(ENTETE_VISIBLE_PREF, false));
    //le pied de page
    r.setPied(
      OPTIONS.getStringProperty(PIED_GAUCHE_PREF, nth),
      OPTIONS.getStringProperty(PIED_MILIEU_PREF, nth),
      OPTIONS.getStringProperty(PIED_DROIT_PREF, nth));
    r.setPiedVisible(OPTIONS.getBooleanProperty(PIED_VISIBLE_PREF, false));
    //l'orientation
    final int o=
      getIntOrientation(
        OPTIONS.getStringProperty(ORIENTATION_PREF, PORTRAIT_ORIENTATION));
    //les dimensions du papier
    final double h= OPTIONS.getDoubleProperty(PAPER_H_PREF, 0);
    final double w= OPTIONS.getDoubleProperty(PAPER_W_PREF, 0);
    final double x= OPTIONS.getDoubleProperty(PAPER_X_PREF, 0);
    final double y= OPTIONS.getDoubleProperty(PAPER_Y_PREF, 0);
    final double ih= OPTIONS.getDoubleProperty(PAPER_IMAGEABLE_H_PREF, 0);
    final double iw= OPTIONS.getDoubleProperty(PAPER_IMAGEABLE_W_PREF, 0);
    /*si l'une des dimensions est incorrecte, aucun changement sur le format
     n'est effectue.*/
    if ((h != 0)
      && (w != 0)
      && (x != 0)
      && (y != 0)
      && (ih != 0)
      && (iw != 0)) {
      final PageFormat pf= new PageFormat();
      pf.setOrientation(o);
      final Paper p= new Paper();
      p.setSize(w, h);
      p.setImageableArea(x, y, iw, ih);
      pf.setPaper(p);
      r.setPageFormat(pf);
    }
    return r;
  }
}
