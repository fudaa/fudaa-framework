/*
 * @creation 2001-11-23
 * @modification $Date: 2007-05-04 13:49:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.impression;

import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.dja.DjaFrame;
import com.memoire.dja.DjaGrid;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.util.Map;
import org.fudaa.ctulu.image.CtuluImageProducer;

/**
 * Une "surcharge" de <code>DjaFrame</code> qui gere l'impression et l'exportation d'image.
 * 
 * @version $Id: EbliDjaFrame.java,v 1.18 2007-05-04 13:49:46 deniger Exp $
 * @author Fred Deniger
 */
public class EbliDjaFrame extends DjaFrame implements EbliPageable, CtuluImageProducer {

  /**
   * Le delegue charge de l'impression.
   */
  private EbliPageableDelegate delegueImpression_;
  /**
   * Les informations sur le logiciel utiles pour les entetes et pied de pages.
   */
  private BuInformationsSoftware is_;
  /**
   * Les informations sur le document utiles pour les entetes et pied de pages.
   */
  private BuInformationsDocument id_;

  /**
   * cf <code>EbliDjaFrame(_app, _file, _grid,true,null)</code>.
   */
  public EbliDjaFrame(final BuCommonImplementation _app, final String _file, final DjaGrid _grid) {
    this(_app, _file, _grid, true, null);
  }

  /**
   * cf <code>EbliDjaFrame(_app, _file, _grid,_tools,null)</code>.
   */
  public EbliDjaFrame(final BuCommonImplementation _app, final String _file, final DjaGrid _grid, final boolean _tools) {
    this(_app, _file, _grid, _tools, null);
  }

  /**
   * Appelle le constructeur de <code>DjaFrame(_app,_file,_grid,_tools</code>. Si <code>_app</code> est non nulle,
   * recupere les <code>BuInformationsSoftware</code> ;sinon instanciation de nouvelles infos. De meme pour les
   * <code>BuInformationsDocument</code>. Ces infos sont utilisees pour les entetes et pied de page (<code>getInformations..</code>.
   * 
   * @param _app
   */
  public EbliDjaFrame(final BuCommonImplementation _app, final String _file, final DjaGrid _grid, final boolean _tools,
      final BuInformationsDocument _id) {
    super(_app, _file, _grid, _tools);
    if (_app != null) {
      is_ = _app.getInformationsSoftware();
    } else {
      is_ = new BuInformationsSoftware();
    }
    if (_id == null) {
      id_ = new BuInformationsDocument();
    } else {
      id_ = _id;
    }
    delegueImpression_ = new EbliPageableDelegate(this);
  }

  /**
   * Modifications des informations utilisees pour les entetes et pieds de pages.
   */
  public void setInformationsSoftware(final BuInformationsSoftware _is) {
    is_ = _is;
  }

  /**
   * Modifications des informations utilisees pour les entetes et pieds de pages.
   */
  public void setInformationsSoftware(final BuInformationsDocument _id) {
    id_ = _id;
  }

  /**
   * Renvoie une instance de <code>Printable</code> pour la page <code>_i</code>.
   */
  @Override
  public Printable getPrintable(final int _i) {
    return delegueImpression_.getPrintable(_i);
  }

  /**
   * Renvoie une instance de <code>PageeFormat</code> pour la page <code>_i</code>.
   */
  @Override
  public PageFormat getPageFormat(final int _i) {
    return delegueImpression_.getPageFormat(_i);
  }

  /**
   * Le format de page utilise par defaut.
   */
  @Override
  public EbliPageFormat getDefaultEbliPageFormat() {
    return delegueImpression_.getDefaultEbliPageFormat();
  }

  /**
   * Renvoie les informations utilisees pour les entetes et pieds de pages.
   */
  @Override
  public BuInformationsSoftware getInformationsSoftware() {
    return is_;
  }

  /**
   * Renvoie les informations utilisees pour les entetes et pieds de pages.
   */
  @Override
  public BuInformationsDocument getInformationsDocument() {
    return id_;
  }

  /**
   * Utilise la methode <code>EbliPrinter.printComponent</code> sur l'objet <code>DjaGrid</code>.
   */
  @Override
  public int print(final Graphics _g, final PageFormat _format, final int _numPage) {
    return EbliPrinter.printComponent(_g, _format, grid_, true, _numPage);
  }

  /**
   * Renvoie 1.
   */
  @Override
  public int getNumberOfPages() {
    return 1;
  }

  /**
   * @return l'image produite dans la taille courante du composant.
   */
  @Override
  public BufferedImage produceImage(final Map _params) {
    final BufferedImage i = new BufferedImage(grid_.getWidth(), grid_.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
    grid_.paint(i.createGraphics());
    return i;
  }

  @Override
  public BufferedImage produceImage(final int _w, final int _h, final Map _params) {
    return null;
  }

  @Override
  public Dimension getDefaultImageDimension() {
    return grid_.getSize();
  }
}
