/*
 *  @creation     13 mai 2004
 *  @modification $Date: 2007-03-30 15:36:29 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import java.awt.Color;
import org.fudaa.ctulu.CtuluNumberFormatI;

/**
 * @author Fred Deniger
 * @version $Id: BPlageInterface.java,v 1.7 2007-03-30 15:36:29 deniger Exp $
 */
public interface BPlageInterface {

  /**
   * @return Returns the couleur.
   */
  Color getCouleur();

  /**
   * @return true si l'utilisateur � modifier la l�gende.
   */
  public boolean isLegendCustomized();

  /**
   * @return une copie de cette plage
   */
  BPlageInterface copy();

  /**
   * @return Returns the iconeTaille.
   */
  int getIconeTaille();

  /**
   * @return Returns the iconeType.
   */
  int getIconeType();

  /**
   * @return Returns the legende.
   */
  String getLegende();

  /**
   * @return l'identifiant. peut etre null
   */
  Object getIdentifiant();

  /**
   * @return Returns the max.
   */
  double getMax();

  /**
   * @return Returns the min.
   */
  double getMin();

  /**
   * Ajuste le contenu de la legende de cette plage.
   * 
   * @param _formatter le formatteur de nomber
   * @param _sep le separateur de nombre
   */
  boolean ajusteLegendes(CtuluNumberFormatI _formatter, String _sep);

  /**
   * @return la chaine utilis�e pour la partie max de la plage
   */
  public String getMaxString();

  /**
   * @return la chaine utilis�e pour la partie min de la plage
   */
  public String getMinString();

  public String getSep();

  /**
   * @param _newLeg la nouvelle legende
   */
  boolean setLegende(String _newLeg);

}