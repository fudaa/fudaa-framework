/*
 *  @creation     10 Mars 2009
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.ListCellRenderer;

/**
 * Un chooser de types de trac�s de surfaces.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class TraceSurfaceChooser extends AbstractListModel implements ComboBoxModel {

  private TraceSurfaceModel[] surfaces_;

  private Object selected_;

  public TraceSurfaceChooser() {
    this(true);
  }

  public TraceSurfaceChooser(final boolean _addInvisible) {
    List<TraceSurfaceModel> lsurf=new ArrayList<TraceSurfaceModel>();

    if (_addInvisible)
      lsurf.add(new TraceSurfaceModel(TraceSurface.INVISIBLE, Color.BLACK, Color.WHITE));

    lsurf.add(new TraceSurfaceModel(TraceSurface.UNIFORME, Color.BLACK, Color.WHITE));
    lsurf.add(new TraceSurfaceModel(TraceSurface.HACHURE_DROITE, Color.BLACK, Color.WHITE));
    lsurf.add(new TraceSurfaceModel(TraceSurface.HACHURE_GAUCHE, Color.BLACK, Color.WHITE));
    lsurf.add(new TraceSurfaceModel(TraceSurface.HACHURE_HORIZONTAL, Color.BLACK, Color.WHITE));
    lsurf.add(new TraceSurfaceModel(TraceSurface.HACHURE_VERTICAL, Color.BLACK, Color.WHITE));
    lsurf.add(new TraceSurfaceModel(TraceSurface.POINTILLE, Color.BLACK, Color.WHITE));

    surfaces_=lsurf.toArray(new TraceSurfaceModel[0]);
  }

  @Override
  public Object getSelectedItem() {
    return selected_;
  }

  public void setCouleur(final Color _c) {
    if (_c == null) {
      return;
    }
    if (!_c.equals(surfaces_[0].getBgColor())) {
      for (int i = surfaces_.length - 1; i >= 0; i--) {
        surfaces_[i].setBgColor(_c);
      }
    }
    fireContentsChanged(this, 0, getSize());
  }

  /**
   * @return le type de surface a selectionner
   */
  public int getSelectedType() {
    if (selected_ == null) {
      return TraceSurface.INVISIBLE;
    }
    return ((TraceSurfaceModel) selected_).getType();
  }

  /**
   * @param _i : le type de surface a selectionner
   */
  public void setSelectedType(final int _i) {
    for (int i = surfaces_.length - 1; i >= 0; i--) {
      if (surfaces_[i].getType() == _i) {
        setSelectedItem(surfaces_[i]);
        return;
      }
    }
    setSelectedItem(null);

  }

  @Override
  public void setSelectedItem(final Object _anItem) {
    if (_anItem != selected_) {
      selected_ = _anItem;
      fireContentsChanged(this, -1, -1);
    }
  }

  @Override
  public Object getElementAt(final int _index) {
    return surfaces_[_index];
  }

  @Override
  public int getSize() {
    return surfaces_.length;
  }

  /**
   * @return un renderer pour ce model
   */
  public ListCellRenderer getCellRenderer() {
    return new TraceSurfaceRenderer();
  }
}
