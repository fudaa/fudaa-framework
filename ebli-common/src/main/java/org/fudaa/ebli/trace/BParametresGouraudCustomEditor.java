/*
 * @creation     1998-06-30
 * @modification $Date: 2006-09-19 14:55:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.trace;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.Customizer;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
/**
 * Un editeur de parametres Gouraud.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 14:55:48 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class BParametresGouraudCustomEditor
  extends JComponent
  implements Customizer, ActionListener {
  private BParametresGouraud objet_;
  GridLayout layout_;
  JComboBox chTaille_;
  JComboBox chNiveau_;
  public BParametresGouraudCustomEditor() {
    super();
    layout_= new GridLayout(2, 0);
    setLayout(layout_);
    add(new JLabel(" Taille : ", SwingConstants.RIGHT), 0);
    chTaille_= new JComboBox();
    chTaille_.addItem("2");
    chTaille_.addItem("4");
    chTaille_.addItem("8");
    chTaille_.addItem("16");
    chTaille_.addItem("32");
    chTaille_.addItem("64");
    chTaille_.addItem("128");
    chTaille_.addItem("256");
    chTaille_.addActionListener(this);
    add(chTaille_, 1);
    add(new JLabel(" Niveau : ", SwingConstants.RIGHT), 2);
    chNiveau_= new JComboBox();
    chNiveau_.addItem("2");
    chNiveau_.addItem("4");
    chNiveau_.addItem("8");
    chNiveau_.addItem("16");
    chNiveau_.addItem("32");
    chNiveau_.addItem("64");
    chNiveau_.addItem("128");
    chNiveau_.addItem("256");
    chNiveau_.addActionListener(this);
    add(chNiveau_, 3);
  }
  @Override
  public void setObject(final Object _objet) {
    if (_objet instanceof BParametresGouraud) {
      objet_= (BParametresGouraud)_objet;
      chTaille_.setSelectedItem("" + objet_.getTaille());
      chNiveau_.setSelectedItem("" + objet_.getNiveau());
    }
  }
  @Override
  public void actionPerformed(final ActionEvent _ev) {
    final Object source= _ev.getSource();
    if (source == chTaille_) {
      final int vp= objet_.getTaille();
      final int v= Integer.parseInt((String)chTaille_.getSelectedItem());
      if (v != vp) {
        objet_.setTaille(v);
      }
    }
    if (source == chNiveau_) {
      final int vp= objet_.getNiveau();
      final int v= Integer.parseInt((String)chNiveau_.getSelectedItem());
      if (v != vp) {
        objet_.setNiveau(v);
      }
    }
  }
}
