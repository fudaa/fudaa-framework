/*
 *  @creation     13 juin 2005
 *  @modification $Date: 2006-09-19 14:55:49 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import java.awt.Color;

/**
 * @author Fred Deniger
 * @version $Id: TraceIconImmutable.java,v 1.3 2006-09-19 14:55:49 deniger Exp $
 */
public class TraceIconImmutable extends TraceIcon {

  public TraceIconImmutable(final TraceIconModelImmutable _model) {
    super(_model);
  }

  @Override
  public void setCouleur(final Color _couleur) {
    throw new IllegalArgumentException();
  }

  @Override
  public void setModel(final TraceIconModel _data) {
    throw new IllegalArgumentException();
  }

  @Override
  public void setTaille(final int _taille) {
    throw new IllegalArgumentException();
  }

  @Override
  public void setType(final int _type) {
    throw new IllegalArgumentException();
  }
}
