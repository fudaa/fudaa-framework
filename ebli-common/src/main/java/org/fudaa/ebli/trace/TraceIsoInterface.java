/*
 * @creation 24 avr. 07
 * @modification $Date: 2007-04-26 14:34:36 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import java.awt.Graphics2D;
import java.awt.Polygon;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.geometrie.GrBoite;

/**
 * @author fred deniger
 * @version $Id: TraceIsoInterface.java,v 1.1 2007-04-26 14:34:36 deniger Exp $
 */
public interface TraceIsoInterface {

  /**
   * @param _g le graphique support
   * @param _p le polygon a dessine
   * @param _v les valeurs aux points du poly
   */
  void draw(final Graphics2D _g, final Polygon _p, final double[] _v);
  
  void setRapide(boolean rapide);
  
  /**
   * 
   * @param w
   * @param h
   * @param sharedMemory la m�moire des points s�lectionn�. si null, une interne devrait �tre cr��.
   */
  void setDimension(int w, int h,CtuluListSelection sharedMemory);

  public void setClipEcran(GrBoite clipEcran);

}