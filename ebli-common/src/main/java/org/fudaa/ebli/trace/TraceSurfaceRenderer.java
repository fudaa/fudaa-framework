/*
 * @creation 10 Mars 2009
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import com.memoire.bu.BuLabel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellRenderer;

/**
 * Un renderer pour les trac�s de surfaces.
 * 
 * @author Bertrand Marchand
 * @version $Id$
 */
public final class TraceSurfaceRenderer extends JComponent implements TableCellRenderer, ListCellRenderer {

  private final TraceSurface ts_;
  BuLabel lbNo_;

  /**
   * Initialisation variables internes.
   */
  public TraceSurfaceRenderer() {
    ts_ = new TraceSurface();
  }

  @Override
  protected void paintComponent(final Graphics _g) {
    if (ts_.getTypeSurface()==TraceSurface.INVISIBLE) {
      return;
    }

    final Dimension d = getSize();
    int[] x=new int[4];
    int[] y=new int[4];
    x[0]=2;
    y[0]=2;
    x[1]=d.width-4;
    y[1]=2;
    x[2]=d.width-4;
    y[2]=d.height-4;
    x[3]=2;
    y[3]=d.height-4;
    
    final Color old = _g.getColor();
    _g.setColor(getBackground());
    ((Graphics2D) _g).fill(_g.getClip());
    Graphics2D g2d=(Graphics2D)_g;
    ts_.remplitPolygone(g2d, x, y);
    _g.setColor(old);
  }

  private void buildLb() {
    if (lbNo_ == null) {
      lbNo_ = new BuLabel();
      lbNo_.setOpaque(true);
    }
  }

  @Override
  public Component getListCellRendererComponent(final JList _list, final Object _value, final int _index,
                                                final boolean _isSelected, final boolean _cellHasFocus) {
    JComponent r = this;
    if (_value == null) {
      buildLb();
      lbNo_.setText(CtuluLibString.ESPACE);
      r = lbNo_;
    } else {
      ts_.setModel((TraceSurfaceModel) _value);
      if (ts_.getTypeSurface() == TraceSurface.INVISIBLE) {
        buildLb();
        lbNo_.setText(CtuluLibString.ESPACE);
        r = lbNo_;
      }
    }
    if (_isSelected) {
      r.setForeground(_list.getSelectionForeground());
      r.setBackground(_list.getSelectionBackground());
    } else {
      r.setBackground(_list.getBackground());
      r.setForeground(_list.getForeground());
    }
    r.setBorder((_cellHasFocus) ? UIManager.getBorder("List.focusCellHighlightBorder")
        : CtuluCellRenderer.BORDER_NO_FOCUS);
    return r;
  }

  @Override
  public Dimension getPreferredSize() {
    final Dimension d = super.getPreferredSize();
    final int s = 15;
    d.width = 15;
    if (d.height < s) {
      d.height = s;
    }
    return d;
  }

  @Override
  public Component getTableCellRendererComponent(final JTable _table, final Object _value, final boolean _isSelected,
                                                 final boolean _hasFocus, final int _row, final int _column) {
    JComponent r = this;
    if (_value == null) {
      if (lbNo_ == null) {
        lbNo_ = new BuLabel(CtuluLibString.ESPACE);
        lbNo_.setOpaque(true);
      }
      r = lbNo_;
    }
    if (_isSelected) {
      r.setForeground(_table.getSelectionForeground());
      r.setBackground(_table.getSelectionBackground());
    }
    if (_hasFocus) {
      r.setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
      r.setForeground(UIManager.getColor("Table.focusCellForeground"));
      r.setBackground(UIManager.getColor("Table.focusCellBackground"));
    } else {
      r.setBorder(CtuluCellRenderer.BORDER_NO_FOCUS);
      r.setForeground(_table.getForeground());
      r.setBackground(_table.getBackground());
    }
    if (_value != null) {
      ts_.setModel((TraceSurfaceModel) _value);
    }
    return r;
  }
}
