/*
 *  @creation     26 janv. 2005
 *  @modification $Date: 2007-02-15 17:10:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import java.awt.Color;
import javax.swing.JButton;

/**
 * @author Fred Deniger
 * @version $Id: TraceIconButton.java,v 1.4 2007-02-15 17:10:09 deniger Exp $
 */
public class TraceIconButton extends JButton {

  public TraceIconButton() {
    final TraceIcon ic = new TraceIcon(TraceIcon.CARRE, 7, getForeground());
    setIcon(ic);
  }

  public Color getSavedColor() {
    final TraceIcon ic = (TraceIcon) getIcon();
    if (ic.getType() == TraceIcon.CARRE) {
      return null;
    }
    return ic.getCouleur();
  }

  @Override
  public void setForeground(final Color _fg) {
    final TraceIcon ic = (TraceIcon) getIcon();
    if (ic == null) {
      return;
    }
    if (_fg == null) {
      ic.setCouleur(Color.GRAY);
      ic.setType(TraceIcon.CARRE);
    } else {
      ic.setCouleur(_fg);
      ic.setType(TraceIcon.CARRE_PLEIN);
    }
    super.setForeground(_fg);

  }
}
