/*
 * @creation     2002-11-15
 * @modification $Date: 2006-04-12 15:28:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@ebli.org
 */
package org.fudaa.ebli.trace;

import java.awt.Color;

/**
 * @version $Id: IPaletteCouleur.java,v 1.8 2006-04-12 15:28:01 deniger Exp $
 * @author Fred Deniger
 */
public interface IPaletteCouleur {
  Color couleur(double _z);
}
