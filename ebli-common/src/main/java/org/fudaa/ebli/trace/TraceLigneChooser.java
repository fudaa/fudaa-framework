/*
 *  @creation     26 janv. 2005
 *  @modification $Date: 2006-09-19 14:55:49 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import java.awt.Color;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 * @author Fred Deniger
 * @version $Id: TraceLigneChooser.java,v 1.5 2006-09-19 14:55:49 deniger Exp $
 */
public class TraceLigneChooser extends AbstractListModel implements ComboBoxModel {

  private TraceLigneModel[] lignes_;

  private Object selected_;

  public TraceLigneChooser() {
    this(true);
  }

  public TraceLigneChooser(final boolean _addInvisible) {
    if (_addInvisible) {

      lignes_ = new TraceLigneModel[] { new TraceLigneModel(TraceLigne.INVISIBLE, 1, Color.BLACK),
          new TraceLigneModel(TraceLigne.LISSE, 1, Color.BLACK),
          new TraceLigneModel(TraceLigne.TIRETE, 1, Color.BLACK),
          new TraceLigneModel(TraceLigne.MIXTE, 1, Color.BLACK),
          new TraceLigneModel(TraceLigne.POINTILLE, 1, Color.BLACK) };

    } else {
      lignes_ = new TraceLigneModel[] { new TraceLigneModel(TraceLigne.LISSE, 1, Color.BLACK),
          new TraceLigneModel(TraceLigne.TIRETE, 1, Color.BLACK),
          new TraceLigneModel(TraceLigne.MIXTE, 1, Color.BLACK),
          new TraceLigneModel(TraceLigne.POINTILLE, 1, Color.BLACK) };
    }

  }

  @Override
  public Object getSelectedItem() {
    return selected_;
  }

  public void setCouleur(final Color _c) {
    if (_c == null) {
      return;
    }
    if (!_c.equals(lignes_[0].getCouleur())) {
      for (int i = lignes_.length - 1; i >= 0; i--) {
        lignes_[i].setCouleur(_c);
      }
    }
    fireContentsChanged(this, 0, getSize());
  }

  /**
   * @return le type de trait a selectionner
   */
  public int getSelectedType() {
    if (selected_ == null) {
      return TraceLigne.INVISIBLE;
    }
    return ((TraceLigneModel) selected_).getTypeTrait();
  }

  /**
   * @param _i : le type de trait a selectionner
   */
  public void setSelectedType(final int _i) {
    for (int i = lignes_.length - 1; i >= 0; i--) {
      if (lignes_[i].getTypeTrait() == _i) {
        setSelectedItem(lignes_[i]);
        return;
      }
    }
    setSelectedItem(null);

  }

  @Override
  public void setSelectedItem(final Object _anItem) {
    if (_anItem != selected_) {
      selected_ = _anItem;
      fireContentsChanged(this, -1, -1);
    }
  }

  @Override
  public Object getElementAt(final int _index) {
    return lignes_[_index];
  }

  @Override
  public int getSize() {
    return lignes_.length;
  }
}
