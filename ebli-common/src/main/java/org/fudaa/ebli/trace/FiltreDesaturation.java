/*
 * @creation     1998-10-13
 * @modification $Date: 2006-09-19 14:55:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.trace;

import java.awt.Color;
import java.awt.image.RGBImageFilter;

/**
 * Une classe rassemblant la desaturation des couleurs.
 *
 * @version $Revision: 1.7 $ $Date: 2006-09-19 14:55:49 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class FiltreDesaturation extends RGBImageFilter {
  public static final int desatureCouleur(final int _rgb) {
    final int a = (_rgb & 0xff000000) >> 24;
    int r = (_rgb & 0x00ff0000) >> 16;
    int g = (_rgb & 0x0000ff00) >> 8;
    int b = (_rgb & 0x000000ff);
    if (a == -1) {
      r = (576 + r + g + b) / 6;
      g = r;
      b = r;
    }
    if (r > 255) {
      r = 255;
    }
    if (r < 0) {
      r = 0;
    }
    if (g > 255) {
      g = 255;
    }
    if (g < 0) {
      g = 0;
    }
    if (b > 255) {
      b = 255;
    }
    if (b < 0) {
      b = 0;
    }
    return (a << 24) | (r << 16) | (g << 8) | b;
  }

  public static final Color desatureCouleur(final Color _c) {
    return new Color(desatureCouleur(_c.getRGB()));
  }

  @Override
  public int filterRGB(final int _x, final int _y, final int _rgb) {
    return desatureCouleur(_rgb);
  }
}
