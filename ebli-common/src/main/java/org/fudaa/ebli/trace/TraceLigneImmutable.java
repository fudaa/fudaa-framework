/*
 *  @creation     31 mars 2005
 *  @modification $Date: 2006-09-19 14:55:49 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import java.awt.Color;


/**
 * @author Fred Deniger
 * @version $Id: TraceLigneImmutable.java,v 1.4 2006-09-19 14:55:49 deniger Exp $
 */
public class TraceLigneImmutable extends TraceLigne {

  public TraceLigneImmutable(final TraceLigneModelImmutable _d) {
    super(new TraceLigneModelImmutable(_d));
  }
  @Override
  public final void setModel(final TraceLigneModel _model){
    throw new IllegalArgumentException();
  }


  @Override
  public boolean setCouleur(final Color _c){
    throw new IllegalArgumentException();
  }
  @Override
  public boolean setEpaisseur(final float _f){
    throw new IllegalArgumentException();
  }
  @Override
  public boolean setTypeTrait(final int _type){
    throw new IllegalArgumentException();
  }
}
