/*
 *  @creation     12 d�c. 2003
 *  @modification $Date: 2008-03-18 15:24:21 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.trace;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import javax.swing.Icon;

/**
 * @author deniger
 * @version $Id: TraceIcon.java,v 1.6.6.1 2008-03-18 15:24:21 bmarchan Exp $
 */
public class TraceIcon implements Icon {

  public final static int RIEN = 0;
  // public final static int POINT = 1;
  public final static int PLUS = 2;
  public final static int CROIX = 3;
  public final static int LIGNE_OBLIQUE = 15;
  public final static int LIGNE_HORIZONTAL = 16;
  public final static int CARRE = 4;
  public final static int CARRE_PLEIN = 5;
  public final static int LOSANGE = 6;
  public final static int CERCLE = 7;
  public final static int DISQUE = 8;
  public final static int CARRE_SELECTION = 9;
  public final static int CARRE_SELECTION_ELEMENT = 10;
  public final static int LOSANGE_PLEIN = 11;
  public final static int CROIX_DOUBLE = 12;
  public final static int PLUS_DOUBLE = 13;
  public final static int CARRE_ARRONDI = 14;
  private TraceIconModel model_;
  private TraceLigneModel ligneModel = new TraceLigneModel();
  private TraceLigne traceLigne = new TraceLigne(ligneModel);

  public final TraceIconModel getModel() {
    return model_;
  }

  public void setModel(final TraceIconModel _data) {
    model_ = _data;
  }

  public int getTaille() {
    return model_.getTaille();
  }

  public int getType() {
    return model_.getType();
  }

  public void setCouleur(final Color _couleur) {
    model_.setCouleur(_couleur);
  }

  public void setTaille(final int _taille) {
    model_.setTaille(_taille);
  }

  public void setType(final int _type) {
    model_.setType(_type);
  }

  /**
   *
   */
  public TraceIcon() {
    model_ = new TraceIconModel();
  }

  /**
   * @param _type
   * @param _taille
   */
  public TraceIcon(final int _type, final int _taille) {
    model_ = new TraceIconModel(_type, _taille, Color.BLACK);
  }

  public TraceIcon(final int _type, final int _taille, final Color _c) {
    model_ = new TraceIconModel(_type, _taille, _c);
  }

  public TraceIcon(final TraceIconModel _data) {
    model_ = _data;
  }

  public TraceIcon(final TraceIcon _ic) {
    model_ = new TraceIconModel(_ic.model_);
  }

  public Color getCouleur() {
    return model_.couleur_;
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    Color old = null;
    if (model_.couleur_ != null) {
      old = _g.getColor();
      _g.setColor(model_.couleur_);
    }
    final Graphics2D g2d = (Graphics2D) _g;
    Stroke oldStroke = (g2d).getStroke();

    if (model_.typeLigne_ > 0 && model_.epaisseurLigne_ > 0) {
      ligneModel.setEpaisseur(model_.epaisseurLigne_);
      ligneModel.setTypeTrait(model_.typeLigne_);
      ligneModel.setColor(g2d.getColor());
      traceLigne.initialisationGraphics(g2d);
    }
    final int x = _x;
    final int y = _y;
    final int taille = model_.taille_ * 2 - 1;
    final int mid = model_.taille_ - 1;
    if (taille == 1) {
      _g.drawLine(x, y, x, y);
    } else if (taille != 0) {
      traceData(_g, x, y, mid, taille);
    }
    if (old != null) {
      _g.setColor(old);
    }
    g2d.setStroke(oldStroke);
  }

  public void paintIconCentre(final Component _c, final Graphics _g, final double _x, final double _y) {
    paintIconCentre(_c, _g, (int) _x, (int) _y);
  }

  public void paintIconCentre(final Graphics _g, final double _x, final double _y) {
    paintIconCentre(null, _g, (int) _x, (int) _y);
  }

  public void paintIconCentre(final Graphics _g, final int _x, final int _y) {
    paintIconCentre(null, _g, _x, _y);
  }

  @Override
  public int getIconWidth() {
    return 2 * model_.taille_ - 1;
  }

  @Override
  public int getIconHeight() {
    return 2 * model_.taille_ - 1;
  }

  public void paintIconCentre(final Component _c, final Graphics _g, final int _x, final int _y) {
    paintIcon(_c, _g, _x - model_.taille_ + 1, _y - model_.taille_ + 1);
  }

  protected void traceData(final Graphics _g, final int x, final int y, final int mid, final int taille) {
    switch (model_.type_) {
      case RIEN:
        break;
      /*
       * case POINT: _g.drawLine(x, y, x, y); break;
       */
      case PLUS:
        _g.drawLine(x, y + mid, x + taille - 1, y + mid);
        _g.drawLine(x + mid, y, x + mid, y + taille - 1);
        break;
      case PLUS_DOUBLE:
        final Color oldC2 = _g.getColor();
        _g.setColor(Color.WHITE);
        _g.drawLine(x, y + mid - 1, x + taille - 1, y + mid - 1);
        _g.drawLine(x, y + mid + 1, x + taille - 1, y + mid + 1);
        _g.drawLine(x + mid - 1, y, x + mid - 1, y + taille - 1);
        _g.drawLine(x + mid + 1, y, x + mid + 1, y + taille - 1);
        _g.setColor(oldC2);
        _g.drawLine(x, y + mid, x + taille - 1, y + mid);
        _g.drawLine(x + mid, y, x + mid, y + taille - 1);
        _g.drawLine(x + mid - 1, y + mid - 1, x + mid + 1, y + mid + 1);
        _g.drawLine(x + mid + 1, y + mid - 1, x + mid - 1, y + mid + 1);
        break;
      case CROIX_DOUBLE:
        final Color oldC1 = _g.getColor();
        _g.setColor(Color.WHITE);
        _g.drawLine(x, y + 1, x + taille - 2, y + taille - 1);
        _g.drawLine(x + 1, y, x + taille - 1, y + taille - 2);
        _g.drawLine(x, y + taille - 2, x + taille - 2, y);
        _g.drawLine(x + 1, y + taille - 1, x + taille - 1, y + 1);
        _g.setColor(oldC1);
        _g.drawLine(x, y, x + taille - 1, y + taille - 1);
        _g.drawLine(x, y + taille - 1, x + taille - 1, y);
        break;
      case CROIX:
        _g.drawLine(x, y, x + taille - 1, y + taille - 1);
        _g.drawLine(x, y + taille - 1, x + taille - 1, y);
        break;
      case LIGNE_OBLIQUE:
        _g.drawLine(x, y, x + taille - 1, y + taille - 1);
        break;
      case LIGNE_HORIZONTAL:
        _g.drawLine(x, y + mid, x + taille - 1, y + mid);
        break;
      case CARRE:
        if (model_.backgroundColor != null && model_.isBackgroundColorPainted()) {
          Color current = _g.getColor();
          _g.setColor(model_.backgroundColor);
          _g.fillRect(x, y, taille - 1, taille - 1);
          _g.setColor(current);
        }
        _g.drawRect(x, y, taille - 1, taille - 1);
        break;
      case CARRE_ARRONDI:
        int arc = Math.max(1, (int) (taille * 0.3));
        if (model_.backgroundColor != null && model_.isBackgroundColorPainted()) {
          Color current = _g.getColor();
          _g.setColor(model_.backgroundColor);
          _g.fillRoundRect(x, y, taille - 1, taille - 1, arc, arc);
          _g.setColor(current);
        }
        _g.drawRoundRect(x, y, taille - 1, taille - 1, arc, arc);
        break;
      case CARRE_SELECTION:
        _g.drawRect(x, y, taille - 1, taille - 1);
        final Color oldC = _g.getColor();
        _g.setColor(Color.WHITE);
        _g.drawRect(x + 1, y + 1, taille - 3, taille - 3);
        _g.setColor(oldC);
        break;
      case CARRE_SELECTION_ELEMENT:
        _g.fillRect(x, y, taille, taille);
        break;
      case CARRE_PLEIN:
        _g.fillRect(x, y, taille, taille);
        break;
      case LOSANGE:
        if (model_.backgroundColor != null && model_.isBackgroundColorPainted()) {
          Color current = _g.getColor();
          _g.setColor(model_.backgroundColor);
          _g.fillPolygon(new int[]{x, x + mid, x + taille - 1, x + mid}, new int[]{y + mid, y, y + mid,
                    y + taille - 1}, 4);
          _g.setColor(current);
        }
        _g.drawLine(x, y + mid, x + mid, y);
        _g.drawLine(x + mid, y, x + taille - 1, y + mid);
        _g.drawLine(x + taille - 1, y + mid, x + mid, y + taille - 1);
        _g.drawLine(x + mid, y + taille - 1, x, y + mid);
        break;
      case LOSANGE_PLEIN:
        _g.drawLine(x, y + mid, x + mid, y);
        _g.drawLine(x + mid, y, x + taille - 1, y + mid);
        _g.drawLine(x + taille - 1, y + mid, x + mid, y + taille - 1);
        _g.drawLine(x + mid, y + taille - 1, x, y + mid);
        _g.fillPolygon(new int[]{x, x + mid, x + taille - 1, x + mid}, new int[]{y + mid, y, y + mid,
                  y + taille - 1}, 4);
        break;
      case CERCLE:
        if (model_.backgroundColor != null && model_.isBackgroundColorPainted()) {
          Color current = _g.getColor();
          _g.setColor(model_.backgroundColor);
          _g.fillArc(x, y, taille - 1, taille - 1, 0, 360);
          _g.setColor(current);
        }
        _g.drawArc(x, y, taille - 1, taille - 1, 0, 360);
        break;
      case DISQUE:
        _g.fillArc(x, y, taille - 1, taille - 1, 0, 360);
        break;
      default:
        break;
    }
  }
}
