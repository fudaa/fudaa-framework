/*
 *  @creation     26 janv. 2005
 *  @modification $Date: 2007-05-22 14:19:05 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import java.awt.Color;

/**
 * Une classe permettant de stocker les donnees essentielles pour le trac� de surfaces.
 * @author Bertrand Marchand
 */
public class TraceSurfaceModel {

  int typeSurface_;
  Color bgColor_;
  Color fgColor_;

  public TraceSurfaceModel() {
    this(null);
  }

  public TraceSurfaceModel(final int _typeSurface, final Color _bgColor, final Color _fgColor) {
    typeSurface_ = _typeSurface;
    bgColor_ =  _bgColor;
    fgColor_ = _fgColor;
  }

  public TraceSurfaceModel(final TraceSurfaceModel _m) {
    if (_m == null) {
      setDefault();
    } else {
      typeSurface_ = _m.typeSurface_;
      bgColor_ = _m.bgColor_;
      fgColor_ = _m.fgColor_;
    }
  }

  public TraceSurface buildCopy() {
    return new TraceSurface(new TraceSurfaceModel(this));
  }

  private void setDefault() {
    typeSurface_ = TraceSurface.UNIFORME;
    bgColor_ = Color.white;
    fgColor_ = Color.black;
  }

  @Override
  public boolean equals(final Object _obj) {
    if (_obj == this) {
      return true;
    }
    if (_obj == null) {
      return false;
    }
    if (_obj.getClass().equals(getClass())) {
      final TraceSurfaceModel d = (TraceSurfaceModel) _obj;
      return d.typeSurface_ == typeSurface_ && 
             (d.bgColor_ == bgColor_ || (d.bgColor_!=null && d.bgColor_.equals(bgColor_)) &&
             (d.fgColor_ == fgColor_ || (d.fgColor_!=null && d.fgColor_.equals(fgColor_))));
    }
    return false;
  }

  public final Color getFgColor() {
    return fgColor_;
  }

  public final Color getBgColor() {
    return bgColor_;
  }

  public final int getType() {
    return typeSurface_;
  }

  @Override
  public int hashCode() {
    return 13 * typeSurface_+(fgColor_ == null ? 0 : fgColor_.hashCode())+(bgColor_ == null ? 0 : bgColor_.hashCode());
  }

  public boolean isBgColorIgnored() {
    return bgColor_==null;
  }

  public boolean isFgColorIgnored() {
    return fgColor_==null;
  }

  public boolean isTypeIgnored() {
    return typeSurface_ < 0;
  }

  /**
   * @param _d
   * @return false si toutes les donn�es ne sont diff�rentes
   */
  public boolean keepCommonValues(final TraceSurfaceModel _d) {
    if (typeSurface_ != _d.typeSurface_) {
      typeSurface_ = -1;
    }
    if (fgColor_ != null && !fgColor_.equals(_d.fgColor_)) {
      fgColor_ = null;
    }
    if (bgColor_ != null && !bgColor_.equals(_d.bgColor_)) {
      bgColor_ = null;
    }

    return typeSurface_ >= 0 || fgColor_ !=null && bgColor_ != null;
  }

  public boolean setBgColor(final Color _c) {
    if (_c != bgColor_) {
      bgColor_ = _c;
      return true;
    }
    return false;
  }

  public void setBgColorIgnored() {
    bgColor_ = null;
  }

  public boolean setFgColor(final Color _c) {
    if (_c != fgColor_) {
      fgColor_ = _c;
      return true;
    }
    return false;
  }

  public void setFgColorIgnored() {
    fgColor_ = null;
  }

  public void setTypeIgnored() {
    typeSurface_ = -1;
  }

  public boolean setType(final int _type) {
    if (_type != typeSurface_) {
      typeSurface_ = _type;
      return true;
    }
    return false;
  }

  @Override
  public String toString() {
    return "trace surface type=" + typeSurface_ + ", foreground color " + fgColor_.toString() +
           ", background color=" + bgColor_.toString();
  }

  public boolean updateData(final TraceSurfaceModel _model) {
    boolean r = false;
    if (!_model.isFgColorIgnored() && (!_model.getFgColor().equals(getFgColor()))) {
      setFgColor(_model.getFgColor());
      r = true;
    }
    if (!_model.isBgColorIgnored() && (!_model.getBgColor().equals(getBgColor()))) {
      setBgColor(_model.getBgColor());
      r = true;
    }
    if (!_model.isTypeIgnored() && _model.getType()!=getType()) {
      setType(_model.getType());
      r = true;
    }
    return r;
  }

}