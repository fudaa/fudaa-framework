/*
 *  @creation     25 janv. 2005
 *  @modification $Date: 2006-12-05 10:14:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import javax.swing.Icon;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;


public class TraceIconCellRenderer extends CtuluCellTextRenderer {

  @Override
  protected void setValue(final Object _value){
    setIcon(null);
    setText(CtuluLibString.ESPACE);
    if (_value == null) {
      return;

    }
    else if (((TraceIcon) _value).getType()== TraceIcon.RIEN) {
      //setText(EbliLib.getS("Aucun"));
    }
    else {
      setIcon((Icon) _value);
    }

  }
}