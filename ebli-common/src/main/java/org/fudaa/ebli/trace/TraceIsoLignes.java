/*
 * @creation     1999-01-08
 * @modification $Date: 2006-09-19 14:55:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.trace;
import java.awt.Graphics;
import java.awt.Polygon;
/**
 * Trace d'iso-lignes.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 14:55:49 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class TraceIsoLignes {
  double dz_;
  // BPaletteCouleur pal_;
  IPaletteCouleur pal_;
  public TraceIsoLignes(final double _dz, final IPaletteCouleur _pal) {
    dz_= _dz;
    pal_= _pal;
  }
  public void draw(final Graphics _g, final Polygon _p, final double[] _v) {
    drawX(_g, _p, _v);
  }
  private void drawX(final Graphics _g, final Polygon _p, final double[] _v) {
    if (_p.npoints == 3) {
      draw3(_g, _p, _v);
    } else if (_p.npoints == 4) {
      draw4(_g, _p, _v);
    } else if (_p.npoints >= 5) {
      draw5(_g, _p, _v);
    }
  }
  private void draw5(final Graphics _g, final Polygon _p, final double[] _v) {
    final Polygon p1= new Polygon();
    final Polygon p2= new Polygon();
    final double[] v1= new double[3];
    final double[] v2= new double[_p.npoints - 1];
    p1.addPoint(_p.xpoints[0], _p.ypoints[0]);
    p1.addPoint(_p.xpoints[1], _p.ypoints[1]);
    p1.addPoint(_p.xpoints[_p.npoints - 1], _p.ypoints[_p.npoints - 1]);
    v1[0]= _v[0];
    v1[1]= _v[1];
    v1[2]= _v[_p.npoints - 1];
    for (int i= 1; i < _p.npoints; i++) {
      p2.addPoint(_p.xpoints[i], _p.ypoints[i]);
      v2[i - 1]= _v[i];
    }
    draw3(_g, p1, v1);
    drawX(_g, p2, v2);
  }
  private void draw4(final Graphics _g, final Polygon _p, final double[] _v) {
    final Polygon p1= new Polygon();
    final Polygon p2= new Polygon();
    p1.addPoint(_p.xpoints[0], _p.ypoints[0]);
    p1.addPoint(_p.xpoints[1], _p.ypoints[1]);
    p1.addPoint(_p.xpoints[3], _p.ypoints[3]);
    p2.addPoint(_p.xpoints[1], _p.ypoints[1]);
    p2.addPoint(_p.xpoints[2], _p.ypoints[2]);
    p2.addPoint(_p.xpoints[3], _p.ypoints[3]);
    final double[] v1= new double[3];
    final double[] v2= new double[3];
    v1[0]= _v[0];
    v1[1]= _v[1];
    v1[2]= _v[3];
    v2[0]= _v[1];
    v2[1]= _v[2];
    v2[2]= _v[3];
    draw3(_g, p1, v1);
    draw3(_g, p2, v2);
  }
  private void draw3(final Graphics _g, final Polygon _p, final double[] _v) {
    final double vmin= Math.min(_v[0], Math.min(_v[1], _v[2]));
    final double vmax= Math.max(_v[0], Math.max(_v[1], _v[2]));
    int nmin= 0;
    if (vmin == _v[1]) {
      nmin= 1;
    }
    if (vmin == _v[2]) {
      nmin= 2;
    }
    int nmax= 0;
    if (vmax == _v[1]) {
      nmax= 1;
    }
    if (vmax == _v[2]) {
      nmax= 2;
    }
    int nmoy= 0;
    while ((nmoy == nmin) || (nmoy == nmax)) {
      nmoy++;
    }
    final double vmoy= _v[nmoy];
    for (double z= 0.; z <= vmax; z += dz_) {
      if (z > vmoy) {
        final int pn= _p.npoints;
        final int[] px= _p.xpoints;
        final int[] py= _p.ypoints;
        final int n0= (nmax - 1 + pn) % pn;
        final int n1= (nmax + 0) % pn;
        final int n2= (nmax + 1) % pn;
        final double d0= (z - _v[n0]) / (_v[n1] - _v[n0]);
        final int x0= (int) (d0 * px[n1] + (1. - d0) * px[n0]);
        final int y0= (int) (d0 * py[n1] + (1. - d0) * py[n0]);
        final double d1= (z - _v[n2]) / (_v[n1] - _v[n2]);
        final int x1= (int) (d1 * px[n1] + (1. - d1) * px[n2]);
        final int y1= (int) (d1 * py[n1] + (1. - d1) * py[n2]);
        _g.setColor(pal_.couleur(z));
        _g.drawLine(x0, y0, x1, y1);
      }
    }
    for (double z= 1.; z >= vmin; z -= dz_) {
      if (z < vmoy) {
        final int pn= _p.npoints;
        final int[] px= _p.xpoints;
        final int[] py= _p.ypoints;
        final int n0= (nmin - 1 + pn) % pn;
        final int n1= (nmin + 0) % pn;
        final int n2= (nmin + 1) % pn;
        final double d0= (z - _v[n1]) / (_v[n0] - _v[n1]);
        final int x0= (int) (d0 * px[n0] + (1. - d0) * px[n1]);
        final int y0= (int) (d0 * py[n0] + (1. - d0) * py[n1]);
        final double d1= (z - _v[n1]) / (_v[n2] - _v[n1]);
        final int x1= (int) (d1 * px[n2] + (1. - d1) * px[n1]);
        final int y1= (int) (d1 * py[n2] + (1. - d1) * py[n1]);
        _g.setColor(pal_.couleur(z));
        _g.drawLine(x0, y0, x1, y1);
      }
    }
  }
}
