/*
 *  @creation     25 janv. 2005
 *  @modification $Date: 2006-09-19 14:55:48 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import java.awt.Color;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.ListCellRenderer;

/**
 * @author Fred Deniger
 * @version $Id: TraceIconChooser.java,v 1.4 2006-09-19 14:55:48 deniger Exp $
 */
public class TraceIconChooser extends AbstractListModel implements ComboBoxModel {

  final TraceIcon[] ic_;

  public TraceIconChooser() {
    this(true);
  }

  public TraceIconChooser(final boolean _addRien) {
    if (_addRien) {
      ic_ = new TraceIcon[]{new TraceIcon(TraceIcon.RIEN, 6, Color.BLACK),
        new TraceIcon(TraceIcon.DISQUE, 6, Color.BLACK), new TraceIcon(TraceIcon.CARRE_PLEIN, 6, Color.BLACK),
        new TraceIcon(TraceIcon.LOSANGE_PLEIN, 6, Color.BLACK), new TraceIcon(TraceIcon.CERCLE, 6, Color.BLACK),
        new TraceIcon(TraceIcon.CARRE, 6, Color.BLACK), new TraceIcon(TraceIcon.LOSANGE, 6, Color.BLACK),
        new TraceIcon(TraceIcon.CROIX, 6, Color.BLACK), new TraceIcon(TraceIcon.PLUS, 6, Color.BLACK), new TraceIcon(TraceIcon.LIGNE_HORIZONTAL, 6, Color.BLACK)};
    } else {
      ic_ = new TraceIcon[]{new TraceIcon(TraceIcon.DISQUE, 6, Color.BLACK),
        new TraceIcon(TraceIcon.CARRE_PLEIN, 6, Color.BLACK), new TraceIcon(TraceIcon.LOSANGE_PLEIN, 6, Color.BLACK),
        new TraceIcon(TraceIcon.CERCLE, 6, Color.BLACK), new TraceIcon(TraceIcon.CARRE, 6, Color.BLACK),
        new TraceIcon(TraceIcon.LOSANGE, 6, Color.BLACK), new TraceIcon(TraceIcon.CROIX, 6, Color.BLACK),
        new TraceIcon(TraceIcon.PLUS, 6, Color.BLACK), new TraceIcon(TraceIcon.LIGNE_HORIZONTAL, 6, Color.BLACK)};
    }

  }
  Object selected_;

  @Override
  public Object getSelectedItem() {
    return selected_;
  }

  public void setCouleur(final Color _c) {
    if (_c == null) {
      return;
    }
    if (!_c.equals(ic_[0].getCouleur())) {
      for (int i = ic_.length - 1; i >= 0; i--) {
        ic_[i].setCouleur(_c);
      }
    }
    fireContentsChanged(this, 0, getSize());
  }

  private TraceIcon getSelectedIcone() {
    return (TraceIcon) selected_;
  }

  public int getSelectedIconeType() {
    final TraceIcon r = getSelectedIcone();
    if (r == null) {
      return TraceIcon.RIEN;
    }
    return r.getType();
  }

  @Override
  public void setSelectedItem(final Object _anItem) {
    if (_anItem != selected_) {
      selected_ = _anItem;
      fireContentsChanged(this, -1, -1);
    }
  }

  /**
   * @param _iconeType le type d'icone a selectionner
   */
  public void setSelectedIcone(final int _iconeType) {
    for (int i = 1; i < getSize(); i++) {
      if (ic_[i].getType() == _iconeType) {
        setSelectedItem(ic_[i]);
        return;
      }
    }
    setSelectedItem(ic_[0]);
  }

  @Override
  public Object getElementAt(final int _index) {
    return ic_[_index];
  }

  @Override
  public int getSize() {
    return ic_.length;
  }

  /**
   * @return un renderer pour ce model
   */
  public ListCellRenderer getCellRenderer() {
    return new TraceIconCellRenderer();
  }
}
