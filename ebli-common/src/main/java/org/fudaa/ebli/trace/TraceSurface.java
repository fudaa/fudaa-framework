/*
 *  @creation     1998-09-07
 *  @modification $Date: 2007-02-02 11:21:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.trace;

import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.util.List;
import javax.swing.Icon;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ebli.mathematiques.MatriceHermite;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Une classe de trace de surfaces.
 * 
 * @version $Id: TraceSurface.java,v 1.15 2007-02-02 11:21:55 deniger Exp $
 * @author Axel von Arnim
 */
public class TraceSurface {

  private final static int TEXTURE_PAINT_TAILLE = 4;
  private final static Rectangle TEXTURE_RECTANGLE = new Rectangle(0, 0, TEXTURE_PAINT_TAILLE, TEXTURE_PAINT_TAILLE);
  private static TextureIcone[] ICONES;
  private final static double C_MAGIC = Math.PI / 4d;
//  private int styleSurface_;
//  private Color couleurFond_;
//  private Color couleurPremierPlan_;
  
  /** Type invisible */
  public final static int INVISIBLE = 0;
  /** Type uniforme (couleur background utilis�) */
  public final static int UNIFORME = 1;
  /** Type hachure horizontal */
  public final static int HACHURE_HORIZONTAL = 2;
  /** Type hachure verticale */
  public final static int HACHURE_VERTICAL = 3;
  /** Type hachure haut gauche vers bas droit */
  public final static int HACHURE_DROITE = 4;
  /** Type hachure haut droit vers bas gauche */
  public final static int HACHURE_GAUCHE = 5;
  /** Type pointill� */
  public final static int POINTILLE = 6;
  
  /** Noms des textures */
  public final static List TEXTURE_NOM = new CtuluPermanentList(new String[] {
      EbliResource.EBLI.getString("invisible"), EbliResource.EBLI.getString("uniforme"),
      EbliResource.EBLI.getString("hachures horizontales"), EbliResource.EBLI.getString("hachures verticales"),
      EbliResource.EBLI.getString("hachures droites"), EbliResource.EBLI.getString("hachures gauches"),
      EbliResource.EBLI.getString("pointill�s") });
  
  /** Modele de surface */
  private TraceSurfaceModel model_;

  /**
   * Recupere le graphics 2D. Initialise le style de surface a uniforme, la couleur de fond est blanche et le premiere
   * plan est noire.
   */
  public TraceSurface() {
    model_ = new TraceSurfaceModel();
  }

  /**
   * @param _type le type de surface
   * @param _fg la couleur de dessus
   * @param _bg la couleur de fond
   */
  public TraceSurface(final int _type, final Color _fg, final Color _bg) {
    model_ = new TraceSurfaceModel(_type, _bg, _fg);
  }

  /**
   * @param _d les donn�es d'initialisation
   */
  public TraceSurface(final TraceSurfaceModel _d) {
    model_ = _d;
  }

  /**
   * @param _d les donn�es d'initialisation
   */
  public TraceSurface(final TraceSurface _d) {
    model_ = new TraceSurfaceModel(_d.model_);
  }

  public void setModel(final TraceSurfaceModel _model) {
    model_ = _model;
  }

  /**
   * Renvoie le texturePaint correspondant.
   * 
   * @return TexturePaint
   */
  private TexturePaint getTexturePaint() {
    return getTexturePaint(model_.typeSurface_, model_.fgColor_, model_.bgColor_);
  }

  private Polygon arc2polygon(final int _x1, final int _y1, final int _x2, final int _y2, final int _vx1,
      final int _vy1, final int _vx2, final int _vy2) {
    final int nb = (Math.abs(_x2 - _x1) + Math.abs(_y2 - _y1)) / 2;
    Point pf;
    final int[] x = new int[nb + 1];
    final int[] y = new int[nb + 1];
    x[0] = _x1;
    y[0] = _y1;
    for (int i = 1; i < nb + 1; i++) {
      pf = MatriceHermite.hermite((double) i / nb, _x1, _y1, _x2, _y2, _vx1, _vy1, _vx2, _vy2);
      x[i] = pf.x;
      y[i] = pf.y;
    }
    return new Polygon(x, y, x.length);
  }

  /**
   * @return TextureNumber
   */
  public static int getTextureNombre() {
    return TEXTURE_NOM.size();
  }

  /**
   * Cree un texturePaint avec les parametres donnees.
   * 
   * @param _type le type de surface
   * @param _premierPlan la couleur du premier plan
   * @param _fond la couleur de fond
   * @return TexturePaint le texture paint correspondant aux parametres
   */
  public static TexturePaint getTexturePaint(final int _type, final Color _premierPlan, final Color _fond) {
    if ((_type == INVISIBLE) || (_type == UNIFORME)) {
      return null;
    }
    final TexturePaint texturePaint = new TexturePaint(new BufferedImage(TEXTURE_PAINT_TAILLE, TEXTURE_PAINT_TAILLE,
        BufferedImage.TYPE_INT_RGB), TEXTURE_RECTANGLE);
    final Graphics2D gBI = texturePaint.getImage().createGraphics();
    gBI.setColor(_fond);
    gBI.fill(texturePaint.getAnchorRect());
    gBI.setColor(_premierPlan);
    switch (_type) {
    case HACHURE_HORIZONTAL:
      gBI.drawLine(0, TEXTURE_PAINT_TAILLE / 2, TEXTURE_PAINT_TAILLE - 1, TEXTURE_PAINT_TAILLE / 2);
      return texturePaint;

    case HACHURE_VERTICAL:
      gBI.drawLine(TEXTURE_PAINT_TAILLE / 2, 0, TEXTURE_PAINT_TAILLE / 2, TEXTURE_PAINT_TAILLE - 1);
      return texturePaint;

    case HACHURE_DROITE:
      gBI.drawLine(0, 0, TEXTURE_PAINT_TAILLE - 1, TEXTURE_PAINT_TAILLE - 1);
      return texturePaint;

    case HACHURE_GAUCHE:
      gBI.drawLine(0, TEXTURE_PAINT_TAILLE - 1, TEXTURE_PAINT_TAILLE - 1, 0);
      return texturePaint;

    case POINTILLE:
      gBI.fill(new Ellipse2D.Float(TEXTURE_PAINT_TAILLE / 2f, TEXTURE_PAINT_TAILLE / 2f, 1f, 1f));
      return texturePaint;

    default:
      FuLog.error("Type surface inconnue");
      return null;
    }
  }

  /**
   * @return Icones
   */
  public static Icon[] getIcones() {
    return getIcones(16, 16);
  }

  /**
   * @param _w
   * @param _h
   * @return Icones
   */
  public static Icon[] getIcones(final int _w, final int _h) {
    final int nb = getTextureNombre();
    if (ICONES == null) {
      ICONES = new TextureIcone[nb];
      for (int i = nb - 1; i >= 0; i--) {
        ICONES[i] = new TextureIcone(i, _w, _h);
      }
    } else {
      for (int i = nb - 1; i >= 0; i--) {
        ICONES[i].setIconHeight(_h);
        ICONES[i].setIconWidth(_w);
      }
    }
    return ICONES;
  }

  /**
   * Affectation de la propriete <I>typeSurface</I> .
   * 
   * @param _styleSurface TypeSurface
   */
  public void setTypeSurface(final int _styleSurface) {
    model_.typeSurface_ = _styleSurface;
  }

  /**
   * Affectation de la propriete <I>couleur de fond</I> .
   * 
   * @param _c CouleurFond
   */
  public void setCouleurFond(final Color _c) {
    model_.bgColor_ = _c;
  }

  /**
   * Affectation de la propriete <I>couleur de fond</I> .
   * 
   * @param _c CouleurPremierPlan
   */
  public void setCouleurPremierPlan(final Color _c) {
    model_.fgColor_ = _c;
  }

  // Methodes publiques
  /**
   * Accesseur de la propriete <I>typeSurface</I> . Elle definit le style de remplissage (hachure, pointille, ...), en
   * prenant ses valeurs dans les champs statiques de cette classe. Par defaut, elle vaut <I>UNIFORME</I> .
   * 
   * @return TypeSurface
   */
  public int getTypeSurface() {
    return model_.typeSurface_;
  }

  /**
   * Accesseur de la propriete <I>couleur de fond</I> . Couleur des lignes tracees.
   * 
   * @return CouleurFond
   */
  public Color getCouleurFond() {
    return model_.bgColor_;
  }

  /**
   * Accesseur de la propriete <I>couleur de fond</I> . Couleur des lignes tracees.
   * 
   * @return CouleurPremierPlan
   */
  public Color getCouleurPremierPlan() {
    return model_.fgColor_;
  }

  public void remplitEllipse(final Graphics2D _g, final double _x1, final double _y1, final double _x2,
      final double _y2, final double _x3, final double _y3, final double _x4, final double _y4) {
    remplitEllipse(_g, (int) _x1, (int) _y1, (int) _x2, (int) _y2, (int) _x3, (int) _y3, (int) _x4, (int) _y4);
  }

  public void remplitEllipse(final Graphics2D _g2d, final int _x1, final int _y1, final int _x2, final int _y2,
      final int _x3, final int _y3, final int _x4, final int _y4) {
    if (model_.typeSurface_ == INVISIBLE) {
      return;
    }
    int i;
    final Polygon arc1 = arc2polygon((_x1 + _x2) / 2, (_y1 + _y2) / 2, (_x2 + _x3) / 2, (_y2 + _y3) / 2,
        (int) ((_x2 - _x1) * C_MAGIC), (int) ((_y2 - _y1) * C_MAGIC), (int) ((_x3 - _x2) * C_MAGIC),
        (int) ((_y3 - _y2) * C_MAGIC));
    final Polygon arc2 = arc2polygon((_x2 + _x3) / 2, (_y2 + _y3) / 2, (_x3 + _x4) / 2, (_y3 + _y4) / 2,
        (int) ((_x3 - _x2) * C_MAGIC), (int) ((_y3 - _y2) * C_MAGIC), (int) ((_x4 - _x3) * C_MAGIC),
        (int) ((_y4 - _y3) * C_MAGIC));
    final Polygon arc3 = arc2polygon((_x3 + _x4) / 2, (_y3 + _y4) / 2, (_x4 + _x1) / 2, (_y4 + _y1) / 2,
        (int) ((_x4 - _x3) * C_MAGIC), (int) ((_y4 - _y3) * C_MAGIC), (int) ((_x1 - _x4) * C_MAGIC),
        (int) ((_y1 - _y4) * C_MAGIC));
    final Polygon arc4 = arc2polygon((_x4 + _x1) / 2, (_y4 + _y1) / 2, (_x1 + _x2) / 2, (_y1 + _y2) / 2,
        (int) ((_x1 - _x4) * C_MAGIC), (int) ((_y1 - _y4) * C_MAGIC), (int) ((_x2 - _x1) * C_MAGIC),
        (int) ((_y2 - _y1) * C_MAGIC));
    final int[] ellx = new int[arc1.npoints + arc2.npoints + arc3.npoints + arc4.npoints];
    final int[] elly = new int[arc1.npoints + arc2.npoints + arc3.npoints + arc4.npoints];
    for (i = 0; i < arc1.npoints; i++) {
      ellx[i] = arc1.xpoints[i];
      elly[i] = arc1.ypoints[i];
    }
    for (i = 0; i < arc2.npoints; i++) {
      ellx[i + arc1.npoints] = arc2.xpoints[i];
      elly[i + arc1.npoints] = arc2.ypoints[i];
    }
    for (i = 0; i < arc3.npoints; i++) {
      ellx[i + arc1.npoints + arc2.npoints] = arc3.xpoints[i];
      elly[i + arc1.npoints + arc2.npoints] = arc3.ypoints[i];
    }
    for (i = 0; i < arc4.npoints; i++) {
      ellx[i + arc1.npoints + arc2.npoints + arc3.npoints] = arc4.xpoints[i];
      elly[i + arc1.npoints + arc2.npoints + arc3.npoints] = arc4.ypoints[i];
    }
    remplitPolygone(_g2d, ellx, elly);
  }

  /**
   * Remplissage de polygone.
   */
  public void remplitPolygone(final Graphics2D _g2d, final int[] _x, final int[] _y) {
    if (model_.typeSurface_ == INVISIBLE) {
      return;
    }
    remplitPolygone(_g2d, new Polygon(_x, _y, _x.length));
  }

  /**
   * Remplissage de polygone. Le style de remplissage est defini par <I> typeSurface</I> .
   * 
   * @param _pol polygone a remplir
   * @see #setTypeSurface(int)
   */
  public void remplitPolygone(final Graphics2D _g2d, final Polygon _pol) {
    if (model_.typeSurface_ == INVISIBLE) {
      return;
    }
    final Paint oldPaint = _g2d.getPaint();
    _g2d.setColor(model_.bgColor_);
    if (model_.typeSurface_ == UNIFORME) {
      _g2d.setColor(model_.bgColor_);
    } else {
      _g2d.setPaint(getTexturePaint());
    }
    _g2d.fillPolygon(_pol);
    _g2d.setPaint(oldPaint);
  }

  /**
   * @version $Id: TraceSurface.java,v 1.15 2007-02-02 11:21:55 deniger Exp $
   * @author deniger
   */
  static class TextureIcone implements Icon {

    private final Color fond_ = Color.white;
    private final Color couleur_ = Color.black;
    private int width_;
    private int height_;
    private int type_;

    /**
     * @param _type
     */
    public TextureIcone(final int _type) {
      this(_type, 16, 16);
    }

    /**
     * @param _type
     * @param _w
     * @param _h
     */
    public TextureIcone(final int _type, final int _w, final int _h) {
      type_ = _type;
      width_ = _w;
      height_ = _h;
    }

    /**
     * @param _e IconWidth
     */
    public void setIconWidth(final int _e) {
      width_ = _e;
    }

    /**
     * @param _e IconHeight
     */
    public void setIconHeight(final int _e) {
      height_ = _e;
    }

    /**
     * @param _e Type
     */
    public void setType(final int _e) {
      if ((type_ < TraceSurface.getTextureNombre()) && (type_ >= 0)) {
        type_ = _e;
      }
    }

    /**
     * @return IconWidth
     */
    @Override
    public int getIconWidth() {
      return width_;
    }

    /**
     * @return IconHeight
     */
    @Override
    public int getIconHeight() {
      return height_;
    }

    /**
     * @return Type
     */
    public int getType() {
      return type_;
    }

    /**
     * @param _c
     * @param _g
     * @param _x
     * @param _y
     */
    @Override
    public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
      if (type_ == TraceSurface.INVISIBLE) {
        _g.setColor(couleur_);
        _g.fillRect(_x, _y, getIconWidth(), getIconHeight());
        return;
      } else if (type_ == TraceSurface.UNIFORME) {
        _g.setColor(fond_);
        _g.fillRect(_x, _y, getIconWidth(), getIconHeight());
        return;
      }
      final Graphics2D g2d = (Graphics2D) _g;
      final Paint oldPaint = g2d.getPaint();
      g2d.setPaint(TraceSurface.getTexturePaint(type_, couleur_, fond_));
      g2d.fillRect(_x, _y, getIconWidth(), getIconHeight());
      g2d.setPaint(oldPaint);
    }
  }
}
