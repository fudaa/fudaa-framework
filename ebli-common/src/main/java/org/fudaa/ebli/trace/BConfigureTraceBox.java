/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.ebli.trace;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.controle.BSelecteurReduitFonteNewVersion;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Configurer un tracefox
 *
 * @author Adrien Hadoux
 */
public class BConfigureTraceBox {

    private TraceBox target;
    private JComponent compAssociated;
    private static int COLOR_TEXT = 0;
    private static int COLOR_FOND = 1;
    private static int COLOR_BORDER = 2;
    private static int SHOULD_DRAW_BOX = 3;
    private static int SHOULD_BE_VERTICAL = 4;
    private static int SHOULD_DRAW_BACKGROUND = 5;
    private static int SHOULD_TOP = 6;
    private static int SHOULD_LEFT = 8;
    private static int SHOULD_RIGHT = 9;
    private static int SHOULD_BOTTOM = 7;

    public void setTarget(TraceBox box, JComponent comp) {
        target = box;
        compAssociated = comp;
    }

    public void buildUi() {
        final JDialog m = new JDialog();
        m.setTitle(EbliResource.EBLI.getString("Configuration  du rendu"));
        m.setResizable(true);
        m.setPreferredSize(new Dimension(250, 400));
        m.setMinimumSize(new Dimension(250, 400));
        m.setMaximumSize(new Dimension(250, 400));
       m.setModal(true);
        final List<JCheckBox> listeCheckBoxes = new ArrayList<JCheckBox>();
       BuPanel conteneur = new BuPanel(new GridLayout(12, 1));
       m.setContentPane(conteneur);

        conteneur.add(buildFontEntry());
        conteneur.add(buildColorEntry(COLOR_TEXT, "Couleur texte"));
        conteneur.add(buildColorEntry(COLOR_FOND, "Couleur Fond"));
        conteneur.add(buildColorEntry(COLOR_BORDER, "Couleur Bordure"));
        conteneur.add(buildCheckboxEntry(SHOULD_DRAW_BOX, "Draw box?",listeCheckBoxes));
        conteneur.add(buildCheckboxEntry(SHOULD_BE_VERTICAL, "vertical or horizontal?",listeCheckBoxes));
        conteneur.add(buildCheckboxEntry(SHOULD_DRAW_BACKGROUND, "Draw background?",listeCheckBoxes));
        conteneur.add(buildCheckboxEntry(SHOULD_TOP, "Position haut?",listeCheckBoxes));
        conteneur.add(buildCheckboxEntry(SHOULD_BOTTOM, "Position bas?",listeCheckBoxes));
        conteneur.add(buildCheckboxEntry(SHOULD_LEFT, "Position gauche?",listeCheckBoxes));
        conteneur.add(buildCheckboxEntry(SHOULD_RIGHT, "Position droite?",listeCheckBoxes));

        conteneur.add(buildValidateEntry());
        m.pack();
        m.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        SwingUtilities.invokeLater( new Runnable() { 
            @Override
            public void run() {
                m.setVisible(true);
            }
    });
    }

    private BuPanel buildCheckboxEntry(final int action, final String title, final List<JCheckBox> liste) {
        BuPanel panel = new BuPanel(new FlowLayout(FlowLayout.LEFT));
        final JCheckBox button = new JCheckBox(EbliResource.EBLI.getString(title));
        panel.add(button);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (action == SHOULD_DRAW_BOX) {
                    target.setDrawBox(button.isSelected());
                    
                } else if (action == SHOULD_BE_VERTICAL) {
                    target.setVertical(button.isSelected());
                } else if (action == SHOULD_BOTTOM) {
                    target.setVPosition(button.isSelected() ? SwingConstants.TOP : SwingConstants.CENTER);
                    liste.get(SHOULD_TOP-SHOULD_DRAW_BOX).setSelected(!button.isSelected());
                } else if (action == SHOULD_TOP) {
                    target.setVPosition(button.isSelected() ? SwingConstants.BOTTOM : SwingConstants.CENTER);
                    liste.get(SHOULD_BOTTOM-SHOULD_DRAW_BOX).setSelected(!button.isSelected());
            
                } else if (action == SHOULD_RIGHT) {
                    target.setHPosition(button.isSelected() ? SwingConstants.LEFT : SwingConstants.CENTER);
                     liste.get(SHOULD_LEFT-SHOULD_DRAW_BOX).setSelected(!button.isSelected());
                   } else if (action == SHOULD_LEFT) {
                    target.setHPosition(button.isSelected() ? SwingConstants.RIGHT : SwingConstants.CENTER);
                           liste.get(SHOULD_RIGHT-SHOULD_DRAW_BOX).setSelected(!button.isSelected());
           
                } else {
                    target.setDrawFond(button.isSelected());

                }
                compAssociated.repaint();
            }
        });

        if (target != null) {
            if (action == SHOULD_DRAW_BOX) {
                button.setSelected(target.isDrawBox());
            } else if (action == SHOULD_BE_VERTICAL) {
                button.setSelected(target.isVertical());
            } else if (action == SHOULD_BOTTOM) {
                button.setSelected(target.getVPosition() == SwingConstants.TOP);

            } else if (action == SHOULD_TOP) {
                button.setSelected(target.getVPosition() == SwingConstants.BOTTOM);

            } else if (action == SHOULD_RIGHT) {
                button.setSelected(target.getHPosition() == SwingConstants.RIGHT);
            } else if (action == SHOULD_LEFT) {
                button.setSelected(target.getHPosition() == SwingConstants.LEFT);
            } else {
                button.setSelected(target.isDrawFond());
            }

        }
        liste.add(button);

        return panel;
    }

    private BuPanel buildColorEntry(final int action, String title) {
        BuPanel panel = new BuPanel(new FlowLayout(FlowLayout.LEFT));

        BuButton button = new BuButton(EbliResource.EBLI.getIcon("theme_20"), EbliResource.EBLI.getString(title));
        button.setSize(new Dimension(20, 20));

        panel.add(button);
        panel.add(new JLabel(EbliResource.EBLI.getString(title)));

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                colorChangeTracebox(target, action);
            }
        });
        return panel;
    }

    public void colorChangeTracebox(final TraceBox box, final int action) {

        Color initColor = box.getColorBoite();
        if (action == COLOR_TEXT) {
            initColor = box.getColorText();
        } else if (action == COLOR_FOND) {
            initColor = box.getColorFond();
        }

        final JColorChooser ch = new JColorChooser(initColor);

        final CtuluDialogPanel p = new CtuluDialogPanel(false) {

            @Override
            public boolean apply() {
                if (action == COLOR_TEXT) {
                    box.setColorText(ch.getColor());
                } else if (action == COLOR_FOND) {
                    box.setColorFond(ch.getColor());
                } else {
                    box.setColorBoite(ch.getColor());
                }
                compAssociated.repaint();
                return true;
            }
        };
        p.add(ch);
        p.afficheModale(null, "", CtuluDialog.OK_CANCEL_APPLY_OPTION);
    }

    private BuPanel buildValidateEntry() {
        BuPanel panel = new BuPanel();
        BuButton button = new BuButton(EbliResource.EBLI.getIcon("valider"), EbliResource.EBLI.getString("Valider"));
        button.setSize(new Dimension(20, 20));

        panel.add(button);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                compAssociated.repaint();
            }
        });
        return panel;
    }

    private BuPanel buildFontEntry() {
        BuPanel panel = new BuPanel(new FlowLayout(FlowLayout.LEFT));
        BuButton button = new BuButton(EbliResource.EBLI.getIcon("text"), EbliResource.EBLI.getString("Font"));
        panel.add(button);
        panel.add(new JLabel(EbliResource.EBLI.getString("Font")));
        button.setSize(new Dimension(20, 20));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fontChange("font", compAssociated);
            }
        });
        return panel;
    }

    public void fontChange(final String _titre, final JComponent _c) {
        final JDialog m = new JDialog();
        m.setTitle(EbliResource.EBLI.getString(_titre));
        final BSelecteurReduitFonteNewVersion s = new BSelecteurReduitFonteNewVersion(_c.getFont(), m);
        s.setTarget(_c);
        m.setResizable(true);
        m.setContentPane(s.getComponent());
        m.setModal(true);
        m.pack();
         m.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        SwingUtilities.invokeLater( 
        new Runnable() { 
            @Override
            public void run() {
                m.setVisible(true);
            }
    }
);
       
    }

    public static void main(String[] args) {
        BConfigureTraceBox config = new BConfigureTraceBox();
        config.setTarget(new TraceBox(), new BuPanel());
        config.buildUi();
    }
}
