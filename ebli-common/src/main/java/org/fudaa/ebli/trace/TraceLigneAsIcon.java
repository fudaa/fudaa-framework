/*
 GPL 2
 */
package org.fudaa.ebli.trace;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.Icon;

/**
 *
 * @author Frederic Deniger
 */
public class TraceLigneAsIcon implements Icon {

  int width = 15;
  int height = 10;
  TraceLigne traceLigne;

  public TraceLigneAsIcon() {
  }

  public TraceLigneAsIcon(TraceLigneModel traceLigneModel) {
    setTraceLigne(traceLigneModel);
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public final void setTraceLigne(TraceLigneModel traceLigneModel) {
    this.traceLigne = traceLigneModel == null ? null : new TraceLigne(traceLigneModel);
  }

  public void setWidth(int width) {
    this.width = width;
  }

  @Override
  public void paintIcon(Component c, Graphics g, int x, int y) {
    if (traceLigne != null) {
      traceLigne.dessineTrait((Graphics2D) g, x, y + height / 2, x + width, y + height / 2);
    }
  }

  @Override
  public int getIconWidth() {
    return width;
  }

  @Override
  public int getIconHeight() {
    return height;
  }
}
