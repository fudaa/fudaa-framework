/*
 *  @creation     26 janv. 2005
 *  @modification $Date: 2007-05-22 14:19:05 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import java.awt.Color;

/**
 * Une classe permettant de stocker les donnees essentielles pour traceLigne.
 */
public class TraceLigneModel {

  public static final String PROPERTY_EPAISSEUR = "epaisseur";
  public static final String PROPERTY_COULEUR = "couleur";
  public static final String PROPERTY_TYPE = "typeTrait";
  Color couleur_;
  float epaisseur_;
  int typeTrait_;

  public TraceLigneModel() {
    setDefault();
  }

  public TraceLigne buildCopy() {
    return new TraceLigne(new TraceLigneModel(this));
  }

  public TraceLigneModel(final int _typeTrait, final float _epaisseur, final Color _c) {
    typeTrait_ = _typeTrait;
    epaisseur_ = _epaisseur;
    couleur_ = _c;
  }

  public TraceLigneModel(final TraceLigneModel _m) {
    if (_m == null) {
      setDefault();
    } else {
      typeTrait_ = _m.typeTrait_;
      epaisseur_ = _m.epaisseur_;
      couleur_ = _m.couleur_;
    }
  }

  private void setDefault() {
    typeTrait_ = TraceLigne.LISSE;
    epaisseur_ = 1f;
    couleur_ = Color.BLACK;
  }

  @Override
  public boolean equals(final Object _obj) {
    if (_obj == this) {
      return true;
    }
    if (_obj == null) {
      return false;
    }
    if (_obj.getClass().equals(getClass())) {
      final TraceLigneModel d = (TraceLigneModel) _obj;
      return d.typeTrait_ == typeTrait_ && d.epaisseur_ == epaisseur_
             && (d.couleur_ == couleur_ || (d.couleur_ != null && d.couleur_.equals(couleur_)));
    }
    return false;
  }

  public final Color getCouleur() {
    return couleur_;
  }

  public final float getEpaisseur() {

    return epaisseur_;
  }

  public final int getTypeTrait() {
    return typeTrait_;
  }

  @Override
  public int hashCode() {
    return 13 * typeTrait_ + Float.floatToIntBits(epaisseur_) + ((couleur_ == null ? 0 : couleur_.hashCode()));
  }

  public boolean isColorIgnored() {
    return couleur_ == null;
  }

  public boolean isEpaisseurIgnored() {
    return epaisseur_ < 0;
  }

  public boolean isTypeIgnored() {
    return typeTrait_ < 0;
  }

  /**
   * @param _d
   * @return false si toutes les donn�es ne sont diff�rentes
   */
  public boolean keepCommonValues(final TraceLigneModel _d) {
    if (typeTrait_ != _d.typeTrait_) {
      typeTrait_ = -1;
    }
    if (epaisseur_ != _d.epaisseur_) {
      epaisseur_ = -1;
    }
    if (couleur_ != null && !couleur_.equals(_d.couleur_)) {
      couleur_ = null;
    }
    return typeTrait_ >= 0 || epaisseur_ >= 0 && couleur_ != null;

  }

  public boolean setColor(final Color _c) {
    if (_c != couleur_) {
      couleur_ = _c;
      return true;
    }
    return false;
  }

  public void setColorIgnored() {
    couleur_ = null;
  }

  public boolean setCouleur(final Color _c) {
    return setColor(_c);
  }

  public boolean setEpaisseur(final float _f) {
    if ((_f != epaisseur_) && (_f > 0)) {
      epaisseur_ = _f;
      return true;
    }
    return false;
  }

  public void setEpaisseurIgnored() {
    epaisseur_ = -1;
  }

  public void setTypeIgnored() {
    typeTrait_ = -1;
  }

  public boolean setTypeTrait(final int _type) {
    if (_type != typeTrait_) {
      typeTrait_ = _type;
      return true;
    }
    return false;
  }

  @Override
  public String toString() {
    return "trace ligne type=" + typeTrait_ + ", epaisseur " + epaisseur_ + ", color=" + couleur_.toString();
  }

  public boolean updateData(final TraceLigneModel _model) {
    boolean r = false;
    if (!_model.isColorIgnored() && (!_model.getCouleur().equals(getCouleur()))) {
      setCouleur(_model.getCouleur());
      r = true;
    }
    if (!_model.isTypeIgnored() && _model.getTypeTrait() != getTypeTrait()) {
      setTypeTrait(_model.getTypeTrait());
      r = true;
    }
    if (!_model.isEpaisseurIgnored() && _model.getEpaisseur() != getEpaisseur()) {
      setEpaisseur(_model.getEpaisseur());
      r = true;
    }
    return r;
  }
}