/*
 * @creation     1998-10-13
 * @modification $Date: 2006-09-19 14:55:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.trace;

import com.memoire.bu.BuIcon;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.FilteredImageSource;
import java.awt.image.RGBImageFilter;
import javax.swing.ImageIcon;

/**
 * Une classe rassemblant l'attenuation des couleurs.
 *
 * @version $Id: FiltreAttenuation.java,v 1.7 2006-09-19 14:55:48 deniger Exp $
 * @author Guillaume Desnoix
 */
public class FiltreAttenuation extends RGBImageFilter {
  public final static FiltreAttenuation INSTANCE = new FiltreAttenuation();

  @Override
  public int filterRGB(final int _x, final int _y, final int _rgb) {
    return attenueCouleur(_rgb);
  }

  public final static int attenueCouleur(final int _rgb) {
    final int a = (_rgb & 0xff000000) >> 24;
    int r = (_rgb & 0x00ff0000) >> 16;
    int g = (_rgb & 0x0000ff00) >> 8;
    int b = (_rgb & 0x000000ff);
    if (a == -1) {
      r += (256 - r) >> 1;
      g += (256 - g) >> 1;
      b += (256 - b) >> 1;
    }
    if (r > 255) {
      r = 255;
    }
    if (r < 0) {
      r = 0;
    }
    if (g > 255) {
      g = 255;
    }
    if (g < 0) {
      g = 0;
    }
    if (b > 255) {
      b = 255;
    }
    if (b < 0) {
      b = 0;
    }
    return (a << 24) | (r << 16) | (g << 8) | b;
  }

  public final static Color attenueCouleur(final Color _c) {
    return new Color(attenueCouleur(_c.getRGB()));
  }

  private static BuIcon attenue0(final Image _image) {
    return new BuIcon(Toolkit.getDefaultToolkit().createImage(new FilteredImageSource(_image.getSource(), INSTANCE)));
  }

  public final static Image attenueImage(final Image _image) {
    return attenue0(_image).getImage();
  }

  public final static ImageIcon attenueIcone(final ImageIcon _icone) {
    return attenue0(_icone.getImage());
  }

  public final static BuIcon attenueIcone(final BuIcon _icone) {
    final BuIcon r = attenue0(_icone.getImage());
    r.setHotSpot(_icone.getHotSpot());
    return r;
  }
}
