/*
 *  @creation     13 juin 2005
 *  @modification $Date: 2006-11-14 09:06:22 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import java.awt.Color;

/**
 * @author Fred Deniger
 * @version $Id: TraceIconModelImmutable.java,v 1.4 2006-11-14 09:06:22 deniger Exp $
 */
public class TraceIconModelImmutable extends TraceIconModel {

  /**
   * @param _type
   * @param _taille
   * @param _c
   */
  public TraceIconModelImmutable(final int _type, final int _taille, final Color _c) {
    super(_type, _taille, _c);
  }

  /**
   * @param _d
   */
  public TraceIconModelImmutable(final TraceIconModel _d) {
    super(_d);
  }

  @Override
  public boolean keepCommonValues(final TraceIconModel _d) {
    return false;
  }

  @Override
  public void setColorIgnored() {}

  @Override
  public boolean setCouleur(final Color _couleur) {
    return false;
  }

  @Override
  public void setEpaisseurIgnored() {}

  @Override
  public void setTaille(final int _taille) {}

  @Override
  public void setType(final int _type) {}

  @Override
  public void setTypeIgnored() {}

  @Override
  public boolean updateData(final TraceIconModel _model) {
    return false;
  }
}
