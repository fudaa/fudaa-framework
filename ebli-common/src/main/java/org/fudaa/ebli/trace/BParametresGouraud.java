/*
 * @creation     1998-06-30
 * @modification $Date: 2006-09-19 14:55:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.trace;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.JComponent;
/**
 * Un ensemble de parametres pour affichage selon Gouraud.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 14:55:49 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class BParametresGouraud extends JComponent {
  public BParametresGouraud() {
    super();
    niveau_= 16;
    taille_= 16;
    final Dimension d= new Dimension(64, 64);
    setMinimumSize(d);
    setPreferredSize(d);
  }
  @Override
  public void paint(final Graphics _g) {
    super.paint(_g);
    final Dimension d= getSize();
    final Insets f= getInsets();
    final int[] xp1= { f.left, d.width - 1 - f.right, d.width - 1 - f.right, f.left };
    final int[] yp1=
      { f.top, f.top, d.height - 1 - f.bottom, d.height - 1 - f.bottom };
    final int[] rc1= { 0, 0, 255, 255 };
    final int[] vc1= { 0, 255, 255, 0 };
    final int[] bc1= { 255, 0, 255, 0 };
    final Gouraud grd= new Gouraud(_g, niveau_, taille_);
    grd.fillRectangle(xp1, yp1, rc1, bc1, vc1);
  }
  // Propriete niveau
  private int niveau_;
  public int getNiveau() {
    return niveau_;
  }
  public void setNiveau(final int _niveau) {
    if (niveau_ != _niveau) {
      final int vp= niveau_;
      niveau_= _niveau;
      if (niveau_ < 2) {
        niveau_= 2;
      }
      repaint();
      firePropertyChange("Niveau", vp, niveau_);
    }
  }
  // Propriete taille
  private int taille_;
  public int getTaille() {
    return taille_;
  }
  public void setTaille(final int _taille) {
    if (taille_ != _taille) {
      final int vp= taille_;
      taille_= _taille;
      if (taille_ < 2) {
        taille_= 2;
      }
      repaint();
      firePropertyChange("Taille", vp, taille_);
    }
  }
}
