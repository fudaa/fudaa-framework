/*
 *  @creation     13 mai 2004
 *  @modification $Date: 2007-04-26 14:34:36 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import gnu.trove.TDoubleArrayList;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;

/**
 * @author Fred Deniger
 * @version $Id: TraceIsoAvecPlage.java,v 1.8 2007-04-26 14:34:36 deniger Exp $
 */
public abstract class TraceIsoAvecPlage implements TraceIsoInterface {

  /**
   * Niveaux de limite des plages. Si 2 plages cons�cutives ne sont pas contig�es, un niveau est ajout� avec comme couleur correspondante la couleur
   * "Autres" de la palette, ou la couleur <i>null</i> si la palette n'affiche pas les couleurs "autres".
   */
  protected double[] vniv_;
  /**
   * Couleur associ�e � chaque niveau. Les valeurs inf�rieures au niveau <i>n</i> ont pour couleur la couleur <i>n</i>. La couleur peut �tre
   * <i>null</i> si les plages de la palette ne sont pas contig�es.
   */
  protected Color[] cniv_;
  protected boolean rapide;
  int h = -1;
  int w = -1;
  CtuluListSelection memory;
  GrBoite clipEcran;

  @Override
  public void setClipEcran(GrBoite clipEcran) {
    this.clipEcran = clipEcran;
  }

  @Override
  public void setDimension(int w, int h, CtuluListSelection sharedMemory) {
    this.h = h;
    this.w = w + 1;
    memory = sharedMemory;
    if (memory == null) {
      memory = new CtuluListSelection(this.w * h);
    }
  }

  @Override
  public void setRapide(boolean rapide) {
    this.rapide = rapide;
  }

  /**
   *
   */
  public TraceIsoAvecPlage() {
    super();
  }

  /**
   * @param _pal la palette a partir de laquelle le trace sera fait
   */
  protected final void updateFromPlageData(final TraceIsoPlageInterface _pal, final int _alpha) {
    if (_pal == null) {
      return;
    }
    // D�finition des niveaux.
    final int nbPlages = _pal.getNbPlages();
    final TDoubleArrayList vvniv = new TDoubleArrayList(nbPlages + 3);
    final List vcniv = new ArrayList(nbPlages + 3);
    final Color colAutres = _pal.isAutresVisible() ? _pal.getPlageAutresInterface().getCouleur() : null;
    double maxOld = Double.NEGATIVE_INFINITY;
    BPlageInterface pi;
    final boolean useAlpha = !rapide && EbliLib.isAlphaChanged(_alpha);
    for (int i = 0; i < nbPlages; i++) {
      pi = _pal.getPlageInterface(i);
      final double min = pi.getMin();
      // Ajout eventuel d'un niveau hors plages
      if (min > maxOld) {
        vvniv.add(min);
        vcniv.add(colAutres);
      }
      maxOld = pi.getMax();
      // Ajout de la plage
      vvniv.add(maxOld);
      vcniv.add(useAlpha ? EbliLib.getAlphaColor(pi.getCouleur(), _alpha) : pi.getCouleur());
    }
    // Ajout d'un niveau +Infini
    vvniv.add(Double.POSITIVE_INFINITY);
    vcniv.add(useAlpha ? EbliLib.getAlphaColor(colAutres, _alpha) : colAutres);
    vniv_ = vvniv.toNativeArray();
    cniv_ = new Color[vcniv.size()];
    vcniv.toArray(cniv_);

  }
}
