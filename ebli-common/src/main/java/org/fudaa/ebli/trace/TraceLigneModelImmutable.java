/*
 *  @creation     13 juin 2005
 *  @modification $Date: 2006-09-19 14:55:49 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import java.awt.Color;

public class TraceLigneModelImmutable extends TraceLigneModel {

  public TraceLigneModelImmutable(final TraceLigneModel _m) {
    super(_m);
  }

  public TraceLigneModelImmutable(final int _type,final float _epaisseur,final Color _c) {
    super(_type,_epaisseur,_c);
  }

  @Override
  public boolean updateData(final TraceLigneModel _model){
    return false;
  }

  @Override
  public boolean keepCommonValues(final TraceLigneModel _d){
    return false;
  }

  @Override
  public boolean setColor(final Color _c){
    return false;
  }

  @Override
  public void setColorIgnored(){}

  @Override
  public boolean setCouleur(final Color _c){
    return false;
  }

  @Override
  public boolean setEpaisseur(final float _f){
    return false;
  }

  @Override
  public void setEpaisseurIgnored(){}

  @Override
  public void setTypeIgnored(){}

  @Override
  public boolean setTypeTrait(final int _type){
    return false;
  }
}