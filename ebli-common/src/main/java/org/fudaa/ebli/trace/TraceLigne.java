/*
 *  @creation     1998-08-26
 *  @modification $Date: 2007-03-15 17:00:29 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.trace;

import com.memoire.fu.FuEmptyArrays;
import java.awt.*;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ebli.mathematiques.MatriceHermite;

/**
 * Une classe de trace de lignes.
 *
 * @version $Id: TraceLigne.java,v 1.20 2007-03-15 17:00:29 deniger Exp $
 * @author Axel von Arnim
 */
public class TraceLigne {

  private final static float[] POINTILLE_DASH = {2.0f, 2.0f};
  /**
   *    */
  private final static float[] TIRETE_DASH = {6.0f, 3.0f};
  /**
   *    */
  /**
   *    */
  private final static float[] MIXTE_DASH = {6.0f, 3.0f, 3.0f, 3.0f};
  private final static BasicStroke DEFAULT_MIXTE_STROKE = new BasicStroke(1f, BasicStroke.CAP_BUTT,
                                                                          BasicStroke.JOIN_MITER, 10.0f, MIXTE_DASH, 0.0f);
  /**
   *    */
  private final static BasicStroke DEFAULT_POINTILLE_STROKE = new BasicStroke(1f, BasicStroke.CAP_BUTT,
                                                                              BasicStroke.JOIN_MITER, 10.0f, POINTILLE_DASH, 0.0f);
  /**
   *    */
  private final static BasicStroke DEFAULT_STROKE = new BasicStroke(1f);
  /**
   *    */
  private final static BasicStroke DEFAULT_TIRETE_STROKE = new BasicStroke(1f, BasicStroke.CAP_BUTT,
                                                                           BasicStroke.JOIN_MITER, 10.0f, TIRETE_DASH, 0.0f);
  final static double C_MAGIC = Math.PI / 4.;
  /**
   *    */
  public final static Font FONTBASE = new Font("SansSerif", Font.PLAIN, 20);
  /**
   * Ligne non dessinee.
   */
  public final static int INVISIBLE = 0;
  /**
   * Ligne lisse.
   */
  public final static int LISSE = 1;
  /**
   * ligne mixte.
   */
  public final static int MIXTE = 5;
  /**
   * les points remarquables sont mis en valeur.
   */
  public final static int POINT_MARQUE = 2;
  /**
   * ligne pointillee.
   */
  public final static int POINTILLE = 3;
  /**
   * ligne tiretee.
   */
  public final static int TIRETE = 4;
  private TraceLigneModel model_;

  public final TraceLigneModel getModel() {
    return model_;
  }

  public void setModel(final TraceLigneModel _model) {
    model_ = _model;
  }

  /**
   * Initialisation par defaut: taille 1, noir, trait lisse.
   */
  public TraceLigne() {
    model_ = new TraceLigneModel();
  }

  /**
   * @param _type le type du trait
   * @param _taille la taille
   * @param _c la couleur
   */
  public TraceLigne(final int _type, final float _taille, final Color _c) {
    model_ = new TraceLigneModel(_type, _taille, _c);
  }

  /**
   * @param _d les donn�es d'initialisation
   */
  public TraceLigne(final TraceLigneModel _d) {
    model_ = _d;
  }

  /**
   * @param _d les donn�es d'initialisation
   */
  public TraceLigne(final TraceLigne _d) {
    model_ = new TraceLigneModel(_d.model_);
  }

  /**
   * @return le polygone correspondant
   */
  private Polygon arc2polygon(final int _x1, final int _y1, final int _x2, final int _y2, final int _vx1,
                              final int _vy1, final int _vx2, final int _vy2) {
    final int nb = (Math.abs(_x2 - _x1) + Math.abs(_y2 - _y1)) / 2;
    Point pf;
    final int[] x = new int[nb + 1];
    final int[] y = new int[nb + 1];
    x[0] = _x1;
    y[0] = _y1;
    for (int i = 1; i < nb + 1; i++) {
      pf = MatriceHermite.hermite((double) i / nb, _x1, _y1, _x2, _y2, _vx1, _vy1, _vx2, _vy2);
      x[i] = pf.x;
      y[i] = pf.y;
    }
    return new Polygon(x, y, x.length);
  }

  /**
   * @return tableau de float correspondant au style de trait choisi
   */
  private float[] getDashArray() {
    switch (model_.typeTrait_) {
      case POINTILLE:
        return POINTILLE_DASH;
      case TIRETE:
        return TIRETE_DASH;
      case MIXTE:
        return MIXTE_DASH;
      default:
        return FuEmptyArrays.FLOAT0;
    }
  }

  /**
   * Initialise le graphics2D g_.
   *
   * @return true si l'initialisation de g_ a ete effectuee false si le type de trait est invisible
   */
  protected boolean initialisationGraphics(final Graphics2D _g) {
    if (model_.typeTrait_ == INVISIBLE) {
      return false;
    }
    if (model_.couleur_ != null) {
      _g.setColor(model_.couleur_);
    }
    _g.setStroke(getStroke());
    return true;
  }

  public Polygon creeArc(final double _x1, final double _y1, final double _x2, final double _y2, final double _vx1,
                         final double _vy1, final double _vx2, final double _vy2) {
    return creeArc((int) _x1, (int) _y1, (int) _x2, (int) _y2, (int) _vx1, (int) _vy1, (int) _vx2, (int) _vy2);
  }

  /**
   * Cree un arc sous forme d'un polygone. Voir <i>dessineArc </i> pour les parametres.
   *
   * @return l'arc
   */
  public Polygon creeArc(final int _x1, final int _y1, final int _x2, final int _y2, final int _vx1, final int _vy1,
                         final int _vx2, final int _vy2) {
    final int nb = (Math.abs(_x2 - _x1) + Math.abs(_y2 - _y1)) / 2;
    Point pf;
    final int[] x = new int[nb + 2];
    final int[] y = new int[nb + 2];
    x[0] = _x1;
    y[0] = _y1;
    for (int i = 1; i < nb + 1; i++) {
      pf = MatriceHermite.hermite((double) i / nb, _x1, _y1, _x2, _y2, _vx1, _vy1, _vx2, _vy2);
      x[i] = pf.x;
      y[i] = pf.y;
    }
    final double k1 = _vx2 * _x2 + _vy2 * _y2;
    final double k2 = _vx1 * _x1 + _vy1 * _y1;
    final double ytmp = (k2 * _vx2 - k1 * _vx1) / (_vy1 * _vx2 - _vy2 * _vx1);
    x[nb + 1] = (int) ((k1 - _vy2 * ytmp) / _vx2);
    y[nb + 1] = (int) ytmp;
    return new Polygon(x, y, x.length);
  }

  public void dessineArc(final Graphics2D _g, final double _x1, final double _y1, final double _x2, final double _y2,
                         final double _vx1, final double _vy1, final double _vx2, final double _vy2) {
    final Color oldColor = _g.getColor();
    final Stroke oldStroke = _g.getStroke();
    if (!initialisationGraphics(_g)) {
      return;
    }
    Point2D.Double pi = new Point2D.Double(_x1, _y1);
    Point2D.Double pf;
    final double nb = (Math.abs(_x2 - _x1) + Math.abs(_y2 - _y1)) / 2;
    for (int i = 1; i < nb + 1; i++) {
      pf = MatriceHermite.hermite(i / nb, _x1, _y1, _x2, _y2, _vx1, _vy1, _vx2, _vy2);
      _g.draw(new Line2D.Double(pi.x, pi.y, pf.x, pf.y));
      pi = pf;
    }
    _g.setColor(oldColor);
    _g.setStroke(oldStroke);
  }

  /**
   * Trace un arc. Il est trace avec le type de trait definit par <i>typeTrait </i>.
   *
   * @see #setTypeTrait(int)
   */
  public void dessineArc(final Graphics2D _g, final int _x1, final int _y1, final int _x2, final int _y2,
                         final int _vx1, final int _vy1, final int _vx2, final int _vy2) {
    final Color oldColor = _g.getColor();
    final Stroke oldStroke = _g.getStroke();
    if (!initialisationGraphics(_g)) {
      return;
    }
    Point pi = new Point(_x1, _y1);
    Point pf;
    final int nb = (Math.abs(_x2 - _x1) + Math.abs(_y2 - _y1)) / 2;
    for (int i = 1; i < nb + 1; i++) {
      pf = MatriceHermite.hermite((double) i / nb, _x1, _y1, _x2, _y2, _vx1, _vy1, _vx2, _vy2);
      _g.drawLine(pi.x, pi.y, pf.x, pf.y);
      pi = pf;
    }
    _g.setColor(oldColor);
    _g.setStroke(oldStroke);
  }

  /**
   * Meme fonctionnement que dessineArc. La courbure est plus sensible.
   *
   * @param _x1
   * @param y1
   * @param x2
   * @param y2
   * @param vx1
   * @param vy1
   * @param vx2
   * @param vy2
   */
  public void dessineArc2D(final Graphics2D _g, final double _x1, final double _y1, final double _x2, final double _y2,
                           final double _vx1, final double _vy1, final double _vx2, final double _vy2) {
    final Color oldColor = _g.getColor();
    final Stroke oldStroke = _g.getStroke();
    if (!initialisationGraphics(_g)) {
      return;
    }
    _g.draw(new CubicCurve2D.Double(_x1, _y1, _x1 + _vx1, _y1 + _vy1, _x2 + _vx2, _y2 + _vy2, _x2, _y2));
    _g.setColor(oldColor);
    _g.setStroke(oldStroke);
  }

  /**
   * Dessine une ellipse passant par les points x1,x2,x3 et x4.
   */
  public void dessineEllipse(final Graphics2D _g, final int _x1, final int _y1, final int _x2, final int _y2,
                             final int _x3, final int _y3, final int _x4, final int _y4) {
    int i;
    final Polygon arc1 = arc2polygon((_x1 + _x2) / 2, (_y1 + _y2) / 2, (_x2 + _x3) / 2, (_y2 + _y3) / 2,
                                     (int) ((_x2 - _x1) * C_MAGIC), (int) ((_y2 - _y1) * C_MAGIC), (int) ((_x3 - _x2) * C_MAGIC),
                                     (int) ((_y3 - _y2) * C_MAGIC));
    final Polygon arc2 = arc2polygon((_x2 + _x3) / 2, (_y2 + _y3) / 2, (_x3 + _x4) / 2, (_y3 + _y4) / 2,
                                     (int) ((_x3 - _x2) * C_MAGIC), (int) ((_y3 - _y2) * C_MAGIC), (int) ((_x4 - _x3) * C_MAGIC),
                                     (int) ((_y4 - _y3) * C_MAGIC));
    final Polygon arc3 = arc2polygon((_x3 + _x4) / 2, (_y3 + _y4) / 2, (_x4 + _x1) / 2, (_y4 + _y1) / 2,
                                     (int) ((_x4 - _x3) * C_MAGIC), (int) ((_y4 - _y3) * C_MAGIC), (int) ((_x1 - _x4) * C_MAGIC),
                                     (int) ((_y1 - _y4) * C_MAGIC));
    final Polygon arc4 = arc2polygon((_x4 + _x1) / 2, (_y4 + _y1) / 2, (_x1 + _x2) / 2, (_y1 + _y2) / 2,
                                     (int) ((_x1 - _x4) * C_MAGIC), (int) ((_y1 - _y4) * C_MAGIC), (int) ((_x2 - _x1) * C_MAGIC),
                                     (int) ((_y2 - _y1) * C_MAGIC));
    final int[] ellx = new int[arc1.npoints + arc2.npoints + arc3.npoints + arc4.npoints];
    final int[] elly = new int[arc1.npoints + arc2.npoints + arc3.npoints + arc4.npoints];
    for (i = 0; i < arc1.npoints; i++) {
      ellx[i] = arc1.xpoints[i];
      elly[i] = arc1.ypoints[i];
    }
    for (i = 0; i < arc2.npoints; i++) {
      ellx[i + arc1.npoints] = arc2.xpoints[i];
      elly[i + arc1.npoints] = arc2.ypoints[i];
    }
    for (i = 0; i < arc3.npoints; i++) {
      ellx[i + arc1.npoints + arc2.npoints] = arc3.xpoints[i];
      elly[i + arc1.npoints + arc2.npoints] = arc3.ypoints[i];
    }
    for (i = 0; i < arc4.npoints; i++) {
      ellx[i + arc1.npoints + arc2.npoints + arc3.npoints] = arc4.xpoints[i];
      elly[i + arc1.npoints + arc2.npoints + arc3.npoints] = arc4.ypoints[i];
    }
    dessinePolygone(_g, ellx, elly);
  }

  /**
   * Dessine une eclipse dans le rectangle dont le coin a gauche est (_x,_y), la largeur _w et la hauteur _h.
   *
   * @param _x abscisse point en haut a gauche
   * @param _y abscisse point en haut a gauche
   * @param _w largeur
   * @param _h hauteur
   */
  public void dessineEllipse2D(final Graphics2D _g, final double _x, final double _y, final double _w, final double _h) {
    final Color oldColor = _g.getColor();
    final Stroke oldStroke = _g.getStroke();
    if (!initialisationGraphics(_g)) {
      return;
    }
    _g.draw(new Ellipse2D.Double(_x, _y, _w, _h));
    _g.setColor(oldColor);
    _g.setStroke(oldStroke);
  }

  /**
   * @param _g le graphics support
   * @param x1 x premier point
   * @param y1 y premier point
   * @param x2 x deuxieme point
   * @param y2 y deuxieme point
   * @see #dessineFleche(Graphics2D, int, int, int, int, int, int)
   */
  public void dessineFleche(final Graphics2D _g, final int _x1, final int _y1, final int _x2, final int _y2) {
    dessineFleche(_g, _x1, _y1, _x2, _y2, 3, 3);
  }

  /**
   * Trace une fleche. La fleche est tracee avec le type definit par <i>typeTrait </i>. \ | \ ecart \ | ------------ / /
   * /<-->retour
   *
   * @param _x1 premier point x
   * @param _y1 premier point y
   * @param _x2 deuxieme point x
   * @param _y2 deuxieme point y
   * @param _ecart la distance des extremites de la fleche par rapport au segment
   * @param _retour la longueur du retour de la fleche
   * @see #setTypeTrait(int)
   */
  public void dessineFleche(final Graphics2D _g, final int _x1, final int _y1, final int _x2, final int _y2,
                            final int _retour, final int _ecart) {
    final Color oldColor = _g.getColor();
    final Stroke oldStroke = _g.getStroke();
    if (!initialisationGraphics(_g)) {
      return;
    }
    _g.drawLine(_x1, _y1, _x2, _y2);
    _g.setColor(oldColor);
    _g.setStroke(oldStroke);

    dessinePointe(_g, _x1, _y1, _x2, _y2, _retour, _ecart);
  }

  /**
   * Trac� d'une pointe de fleche uniquement, centr�e sur (xpointe,ypointe). La direction est donn�e par le deuxieme point
   * (xorig,yorig).
   * @param _g Le contexte graphique
   * @param _xorig Le x du point d'origine, donnant la direction.
   * @param _yorig Le y du point d'origine, donnant la direction.
   * @param _xpointe Le x de la pointe de fleche.
   * @param _ypointe Le y de la pointe de fleche.
   * @param _ecart la distance des extremites de la fleche par rapport au segment
   * @param _retour la longueur du retour de la fleche
   */
  public void dessinePointe(final Graphics2D _g, final int _xorig, final int _yorig, final int _xpointe, final int _ypointe,
                            final int _retour, final int _ecart) {
    final Color oldColor = _g.getColor();
    final Stroke oldStroke = _g.getStroke();
    if (!initialisationGraphics(_g)) {
      return;
    }
//    _g.drawLine(_xorig, _yorig, _xpointe, _ypointe);
    if (model_.typeTrait_ == POINT_MARQUE) {
      _g.drawRect(_xorig - 1, _yorig - 1, 2, 2);
      _g.drawRect(_xpointe - 1, _ypointe - 1, 2, 2);
    } else {
      // la norme du vecteur
      final double norme = CtuluLibGeometrie.getDistance(_xorig, _yorig, _xpointe, _ypointe);
      // Si le retour de la fleche est trop grand on le diminue au 3 de la fleche
      if (norme == 0) {
        return;
      }
      final double coef = (norme - _retour) / norme;
      // xd et yd sont les points du traits correspondant a l'extremite des retours de la fleche
      final double xd = coef * (_xpointe - _xorig) + _xorig;
      final double yd = coef * (_ypointe - _yorig) + _yorig;
      // vecteur normal au trait
      final double xn = (_yorig - _ypointe) / norme;
      final double yn = (_xpointe - _xorig) / norme;
      // Xe et Ye sont les coordonnees d'une extremite de la fleche
      double xe = _ecart * xn + xd;
      double ye = _ecart * yn + yd;
      _g.drawLine(_xpointe, _ypointe, (int) xe, (int) ye);
      // Xe et Ye sont les coordonnees de l'autre extremite
      xe = _ecart * (-xn) + xd;
      ye = _ecart * (-yn) + yd;
      _g.drawLine(_xpointe, _ypointe, (int) xe, (int) ye);
    }
    _g.setColor(oldColor);
    _g.setStroke(oldStroke);
  }

  /**
   * Trace un polygone. Il est trace avec le type definit par <i>typeTrait </i>.
   *
   * @param _x polygone x
   * @param _y polygone y
   * @see #setTypeTrait(int)
   */
  public void dessinePolygone(final Graphics2D _g, final int[] _x, final int[] _y) {
    final Color oldColor = _g.getColor();
    final Stroke oldStroke = _g.getStroke();
    if (!initialisationGraphics(_g)) {
      return;
    }
    if (model_.typeTrait_ == POINT_MARQUE) {
      _g.drawRect(_x[0] - 1, _y[0] - 1, 2, 2);
      for (int i = 1; i < _x.length; i++) {
        _g.drawLine(_x[i - 1], _y[i - 1], _x[i], _y[i]);
        _g.drawRect(_x[i] - 1, _y[i] - 1, 2, 2);
      }
      _g.drawLine(_x[_x.length - 1], _y[_x.length - 1], _x[0], _y[0]);
    } else {
      _g.drawPolygon(_x, _y, _x.length);
    }
    _g.setColor(oldColor);
    _g.setStroke(oldStroke);
  }

  /**
   * Trace une polyligne. Elle est tracee avec le type defini par <i>typeTrait </i>.
   *
   * @param _x polyligne x
   * @param _y polyligne y
   * @see #setTypeTrait(int)
   */
  public void dessinePolyligne(final Graphics2D _g, final int[] _x, final int[] _y) {
    final Color oldColor = _g.getColor();
    final Stroke oldStroke = _g.getStroke();
    if (!initialisationGraphics(_g)) {
      return;
    }
    if (model_.typeTrait_ == POINT_MARQUE) {
      final Rectangle r = _g.getClipBounds();
      final int rx2 = r.x + r.width;
      final int ry2 = r.y + r.height;
      // int dehors=0;
      // A FAIRE
      _g.drawRect(_x[0] - 1, _y[0] - 1, 2, 2);
      for (int i = 1; i < _x.length; i++) {
        if ((_x[i] > r.x) && (_x[i] < rx2) && (_y[i] > r.y) && (_y[i] < ry2)) {
          _g.drawLine(_x[i - 1], _y[i - 1], _x[i], _y[i]);
          _g.drawRect(_x[i] - 1, _y[i] - 1, 2, 2);
        }
      }
    } else {
      _g.drawPolyline(_x, _y, _x.length);
    }
    _g.setColor(oldColor);
    _g.setStroke(oldStroke);
  }

  public void dessineRectangle(final Graphics2D _g2d, final double _x, final double _y, final double _largeur,
                               final double _hauteur) {
    dessineRectangle(_g2d, (int) _x, (int) _y, (int) _largeur, (int) _hauteur);
  }

  /**
   * Trace un rectangle. Il est trace avec le type de trait <i>typetrait </i>.
   *
   * @param _x position x
   * @param _y position y
   * @param _largeur largeur
   * @param _hauteur hauteur
   * @see #setTypeTrait(int)
   */
  public void dessineRectangle(final Graphics2D _g, final int _x, final int _y, final int _largeur, final int _hauteur) {
    dessineRoundRectangle(_g, _x, _y, _largeur, _hauteur, 0);
  }

  public void dessineRoundRectangle(final Graphics2D _g, final int _x, final int _y, final int _largeur,
                                    final int _hauteur, final int _rayon) {
    final Color oldColor = _g.getColor();
    final Stroke oldStroke = _g.getStroke();
    if (!initialisationGraphics(_g)) {
      return;
    }
    if (model_.typeTrait_ == POINT_MARQUE) {
      final Rectangle2D.Double r = new Rectangle2D.Double(_x - 1d, _y - 1d, 2d, 2d);
      _g.draw(r);
      r.setRect(_x + _largeur - 1d, _y - 1d, 2d, 2d);
      _g.draw(r);
      r.setRect(_x + _largeur - 1d, _y + _hauteur - 1d, 2d, 2d);
      _g.draw(r);
      r.setRect(_x - 1d, _y + _hauteur - 1d, 2d, 2d);
      _g.draw(r);
    }
    if (_rayon > 0) {
      _g.drawRoundRect(_x, _y, _largeur - 1, _hauteur - 1, _rayon, _rayon);
    } else {
      _g.drawRect(_x, _y, _largeur - 1, _hauteur - 1);
    }
    _g.setColor(oldColor);
    _g.setStroke(oldStroke);
  }

  /**
   * @param _s
   */
  public void dessineShape(final Graphics2D _g, final Shape _s) {
    final Color oldColor = _g.getColor();
    final Stroke oldStroke = _g.getStroke();
    if (!initialisationGraphics(_g)) {
      return;
    }
    _g.draw(_s);
    _g.setColor(oldColor);
    _g.setStroke(oldStroke);
  }

  /**
   * Trace un texte.
   *
   * @param _t le texte
   * @param _x position x
   * @param _y position y
   */
  public void dessineTexte(final Graphics2D _g, final String _t, final Font _f, final int _x, final int _y) {
    final Color oldColor = _g.getColor();
    final Stroke oldStroke = _g.getStroke();
    final Font oldFont = _g.getFont();
    _g.setColor(model_.couleur_);
    if (_f != null) {
      _g.setFont(_f);
    }
    _g.drawString(_t, _x, _y);
    _g.setColor(oldColor);
    _g.setStroke(oldStroke);
    _g.setFont(oldFont);
  }

  public void dessineTrait(final Graphics2D _g, final double _x1, final double _y1, final double _x2, final double _y2) {
    dessineTrait(_g, (int) _x1, (int) _y1, (int) _x2, (int) _y2);
  }

  /**
   * Trace un trait. Le trait est trace avec le type definit par <i>typeTrait </i>.
   *
   * @param _x1 premier point x
   * @param _y1 premier point y
   * @param _x2 deuxieme point x
   * @param _y2 deuxieme point y
   * @see #setTypeTrait(int)
   */
  public void dessineTrait(final Graphics2D _g, final int _x1, final int _y1, final int _x2, final int _y2) {
    if (model_.epaisseur_ <= 0) {
      return;
    }
    final Color oldColor = _g.getColor();
    final Stroke oldStroke = _g.getStroke();
    if (!initialisationGraphics(_g)) {
      return;
    }
    dessineTraitCore(_g, _x1, _y1, _x2, _y2);
    _g.setColor(oldColor);
    _g.setStroke(oldStroke);
  }

  public Color getCouleur() {
    return model_.getCouleur();
  }

  public float getEpaisseur() {
    return model_.getEpaisseur();
  }

  /**
   * @return le basicstroke a utiliser selon l'epaisseur et le type de trait specifie
   */
  public Stroke getStroke() {
    if (model_.typeTrait_ == INVISIBLE) {
      return null;
    }
    if (model_.epaisseur_ == 1f) {
      if (model_.typeTrait_ == LISSE || (model_.typeTrait_ == POINT_MARQUE)) {
        return DEFAULT_STROKE;
      }
      switch (model_.typeTrait_) {
        case POINTILLE:
          return DEFAULT_POINTILLE_STROKE;
        case TIRETE:
          return DEFAULT_TIRETE_STROKE;
        case MIXTE:
          return DEFAULT_MIXTE_STROKE;
        default:
      }
    }
    if (model_.typeTrait_ == LISSE || (model_.typeTrait_ == POINT_MARQUE)) {
      return new BasicStroke(model_.epaisseur_);
    }
    return new BasicStroke(model_.epaisseur_, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, getDashArray(), 0.0f);
  }

  public int getTypeTrait() {
    return model_.getTypeTrait();
  }

  public boolean setCouleur(final Color _c) {
    return model_.setCouleur(_c);
  }

  public boolean setEpaisseur(final float _f) {
    return model_.setEpaisseur(_f);
  }

  public boolean setTypeTrait(final int _type) {
    return model_.setTypeTrait(_type);
  }

  /**
   * Dessine uniquement le trait sans modifier la le graphics.
   * Ne doit �tre appele que par des calques cherchant les perf,
   * @param _g
   * @param _x1
   * @param _y1
   * @param _x2
   * @param _y2 
   */
  public void dessineTraitCore(final Graphics2D _g, final int _x1, final int _y1, final int _x2, final int _y2) {
    if (model_.typeTrait_ == POINT_MARQUE) {
      _g.drawLine(_x1, _y1, _x2, _y2);
      _g.drawRect(_x1 - 1, _y1 - 1, 2, 2);
      _g.drawRect(_x2 - 1, _y2 - 1, 2, 2);
    } else {
      try {
        _g.drawLine(_x1, _y1, _x2, _y2);
      } catch (Throwable e) {
        e.printStackTrace();
      }
    }
  }
}