/*
 GPL 2
 */
package org.fudaa.ebli.trace;

import java.awt.Graphics;

/**
 *
 * @author Frederic Deniger
 */
public class TraceIconFleche extends TraceIcon {

  int dx1;
  int dy1;
  int dy2;
  int dx2;

  public int getDx1() {
    return dx1;
  }

  public void setDx1(int dx1) {
    this.dx1 = dx1;
  }

  public int getDy1() {
    return dy1;
  }

  public void setDy1(int dy1) {
    this.dy1 = dy1;
  }

  public int getDy2() {
    return dy2;
  }

  public void setDy2(int dy2) {
    this.dy2 = dy2;
  }

  public int getDx2() {
    return dx2;
  }

  public void setDx2(int dx2) {
    this.dx2 = dx2;
  }

  @Override
  protected void traceData(Graphics _g, int x, int y, int mid, int taille) {
    _g.drawLine(x + mid, y + mid, x + mid + dx1, y + mid + dy1);
    _g.drawLine(x + mid, y + mid, x + mid + dx2, y + mid + dy2);
  }
}
