/*
 *  @creation     23 juin 2004
 *  @modification $Date: 2006-09-19 14:55:49 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import com.memoire.fu.FuLib;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import javax.swing.SwingConstants;

/**
 * Une classe permettant de tracer un texte � une position (x,y) donn�e. Le texte peut �tre trac� : <ul> <li>Centr�/� gauche/� droite/au milieu/en
 * bas/en haut de la position suivant les constantes {@link SwingConstants}.</li> <li>Avec une d�coration (fond/contour rectangle visible)</li> </ul>
 *
 * @author Fred Deniger
 * @version $Id: TraceBox.java,v 1.5 2006-09-19 14:55:49 deniger Exp $
 */
public class TraceBox extends AbstractTraceBoxedData {

  private Color colorFond_;
  private Color colorBoite_;
  private Color colorText_;
  private boolean drawBox_;
  private boolean vertical;
  private boolean drawFond_;
  private int hMargin_;
  private TraceLigne traceLigne_;
  private int vMargin_;

  /**
   *
   */
  public TraceBox() {
    traceLigne_ = new TraceLigne();
    drawBox_ = true;
    drawFond_ = true;
    hMargin_ = 1;
    vMargin_ = 1;
  }

  public TraceBox(TraceBox from) {
    super(from);
    if (from == null) {
      return;
    }
    colorBoite_ = from.colorBoite_;
    colorFond_ = from.colorFond_;
    colorText_ = from.colorText_;
    drawBox_ = from.drawBox_;
    drawFond_ = from.drawFond_;
    hMargin_ = from.hMargin_;
    hPosition_ = from.hPosition_;
    traceLigne_ = new TraceLigne(from.traceLigne_);
    vMargin_ = from.vMargin_;
    vPosition_ = from.vPosition_;
    vertical=from.vertical;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 41 * hash + (this.colorFond_ != null ? this.colorFond_.hashCode() : 0);
    hash = 41 * hash + (this.colorBoite_ != null ? this.colorBoite_.hashCode() : 0);
    hash = 41 * hash + (this.colorText_ != null ? this.colorText_.hashCode() : 0);
    hash = 41 * hash + (this.drawBox_ ? 1 : 0);
    hash = 41 * hash + (this.vertical ? 1 : 0);
    hash = 41 * hash + (this.drawFond_ ? 1 : 0);
    hash = 41 * hash + this.hMargin_;
    hash = 41 * hash + (this.traceLigne_ != null ? this.traceLigne_.hashCode() : 0);
    hash = 41 * hash + this.vMargin_;
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final TraceBox other = (TraceBox) obj;
    if (this.colorFond_ != other.colorFond_ && (this.colorFond_ == null || !this.colorFond_.equals(other.colorFond_))) {
      return false;
    }
    if (this.colorBoite_ != other.colorBoite_ && (this.colorBoite_ == null || !this.colorBoite_.equals(other.colorBoite_))) {
      return false;
    }
    if (this.colorText_ != other.colorText_ && (this.colorText_ == null || !this.colorText_.equals(other.colorText_))) {
      return false;
    }
    if (this.drawBox_ != other.drawBox_) {
      return false;
    }
    if (this.vertical != other.vertical) {
      return false;
    }
    if (this.drawFond_ != other.drawFond_) {
      return false;
    }
    if (this.hMargin_ != other.hMargin_) {
      return false;
    }
    if (this.vMargin_ != other.vMargin_) {
      return false;
    }
    final TraceLigneModel model = traceLigne_.getModel();
    final TraceLigneModel otherModel = other.traceLigne_.getModel();
    return model == otherModel || (model != null && model.equals(otherModel));
  }

  public Color getColorFond() {
    if (colorFond_ == null) {
      colorFond_ = Color.yellow;
    }
    return colorFond_;
  }

  public boolean isVertical() {
    return vertical;
  }

  public void setVertical(boolean vertical) {
    this.vertical = vertical;
  }

  public Color getColorBoite() {
    if (colorBoite_ == null) {
      colorBoite_ = Color.BLACK;
    }
    return colorBoite_;
  }

  public Color getColorText() {
    if (colorText_ == null) {
      colorText_ = Color.BLACK;
    }
    return colorText_;
  }

  public int getHMargin() {
    return hMargin_;
  }

  public TraceLigne getTraceLigne() {
    return traceLigne_;
  }

  public int getVMargin() {
    return vMargin_;
  }

  public boolean isDrawBox() {
    return drawBox_;
  }

  public boolean isDrawFond() {
    return drawFond_;
  }

  public int currentWidth_ = -1;

  public void paintBox(final Graphics2D _g2d, final int _x, final int _y, final String _s) {
    paintBox(_g2d, _x, _y, _s, null);
  }

  public int getTotalHeightMargin() {
    return 2 * hMargin_ + (int) (2 * traceLigne_.getEpaisseur());
  }

  public int getTotalWidthMargin() {
    return 2 * hMargin_ + (int) (2 * traceLigne_.getEpaisseur());
  }

  public Dimension getDimension(final Graphics2D _g2d, final String _s) {
    if (traceLigne_ == null) {
      traceLigne_ = new TraceLigne();
    }
    final FontMetrics fm = _g2d.getFontMetrics();
    String[] lines = FuLib.split(_s, '\n');
    final int fontHeight = fm.getHeight();
    final int hString = (fontHeight) * lines.length + (lines.length - 1) * vMargin_;
    int wString = getMaxStringWith(fm, lines);
    int hOuter = hString + getTotalHeightMargin();
    int wOuter = wString + getTotalWidthMargin();
    if (vertical) {
      int tmp = hOuter;
      hOuter = wOuter;
      wOuter = tmp;
    }
    return new Dimension(wOuter, hOuter);
  }

  public void paintBox(final Graphics2D _g2d, final int _x, final int _y, final String _s, Rectangle view) {
    if (_s == null) {
      return;
    }
    Dimension dimension = getDimension(_g2d, _s);
    int hOuter = dimension.height;
    int wOuter = dimension.width;
    if (traceLigne_ == null) {
      traceLigne_ = new TraceLigne();
    }
    final FontMetrics fm = _g2d.getFontMetrics();
    String[] lines = FuLib.split(_s, '\n');
    final int fontHeight = fm.getHeight();
    final int hString = (fontHeight) * lines.length + (lines.length - 1) * vMargin_;
    int wString = getMaxStringWith(fm, lines);
    int xTopLeft = _x;
    if (hPosition_ == SwingConstants.CENTER) {
      xTopLeft -= wOuter / 2;
    } else if (hPosition_ == SwingConstants.RIGHT) {
      xTopLeft -= wOuter;
    }
    int yTopLeft = _y;
    if (vPosition_ == SwingConstants.CENTER) {
      yTopLeft -= hOuter / 2;
    } else if (vPosition_ == SwingConstants.BOTTOM) {
      yTopLeft -= hOuter;
    }
    if (view != null) {
      xTopLeft = Math.min(view.x + view.width - wOuter, xTopLeft);
      yTopLeft = Math.min(view.y + view.height - hOuter, yTopLeft);
      xTopLeft = Math.max(xTopLeft, view.x);
      yTopLeft = Math.max(yTopLeft, view.y);
    }
    if (drawFond_) {
      _g2d.setColor(getColorFond());
      _g2d.fillRect(xTopLeft + 1, yTopLeft + 1, wOuter - 2, hOuter - 2);
    }
    if (drawBox_) {
      traceLigne_.setCouleur(getColorBoite());
      traceLigne_.dessineRectangle(_g2d, xTopLeft, yTopLeft, wOuter, hOuter);
      currentWidth_ = wOuter;
    }
    Color old = _g2d.getColor();
    _g2d.setColor(getColorText());
    final int x = xTopLeft + wOuter / 2 - wString / 2;
    final int stringTopLeft = yTopLeft + hOuter / 2 - hString / 2;
    AffineTransform save = null;
    if (vertical) {
      save = _g2d.getTransform();
      _g2d.rotate(-Math.PI / 2, xTopLeft + wOuter / 2, yTopLeft + hOuter / 2);
    }
    for (int i = 0; i < lines.length; i++) {
      _g2d.drawString(lines[i], x, stringTopLeft + (i + 1) * fontHeight + (i - 1) * vMargin_ - fm.getDescent());
    }
    if (save != null) {
      _g2d.setTransform(save);
    }
    _g2d.setColor(old);
  }

  public void setColorFond(final Color _colorFond) {
    colorFond_ = _colorFond;
  }

  public void setColorBoite(final Color _colorBoite) {
    colorBoite_ = _colorBoite;
  }

  public void setColorText(final Color _colorText) {
    colorText_ = _colorText;
  }

  public void setDrawBox(final boolean _drawBox) {
    drawBox_ = _drawBox;
  }

  public void setDrawFond(final boolean _drawFond) {
    drawFond_ = _drawFond;
  }

  public void setHMargin(final int _margin) {
    hMargin_ = _margin;
  }

  public void setTraceLigne(final TraceLigne _traceLigne) {
    traceLigne_ = _traceLigne;
  }

  public void setVMargin(final int _margin) {
    vMargin_ = _margin;
  }

  public TraceBox duplicate() {
    return new TraceBox(this);
  }


  private int getMaxStringWith(final FontMetrics fm, String[] lines) {
    int wString = fm.stringWidth(lines[0]);
    for (int i = 0; i < lines.length; i++) {
      final int stringWidth = fm.stringWidth(lines[i]);
      wString = Math.max(stringWidth, wString);
    }
    return wString;
  }
}
