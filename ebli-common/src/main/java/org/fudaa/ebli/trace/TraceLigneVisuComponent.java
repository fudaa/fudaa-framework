/*
 *  @file         BSelecteurTraitComponent.java
 *  @creation     8 oct. 2003
 *  @modification $Date: 2006-11-14 09:06:22 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.trace;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JComponent;

/**
 * @author deniger
 * @version $Id: TraceLigneVisuComponent.java,v 1.1 2006-11-14 09:06:22 deniger Exp $
 */
public class TraceLigneVisuComponent extends JComponent {
  TraceLigne l_;

  public TraceLigneVisuComponent(final TraceLigneModel _d) {
    l_ = new TraceLigne(_d);
    setPreferredSize(new Dimension(40, 10));
  }

  @Override
  public void paint(final Graphics _g) {
    _g.setColor(Color.white);
    _g.fillRect(0, 0, getWidth(), getHeight());
    final int h = getHeight() / 2;
    l_.dessineTrait((Graphics2D) _g, 0, h, getWidth(), h);
    super.paintBorder(_g);
  }
}
