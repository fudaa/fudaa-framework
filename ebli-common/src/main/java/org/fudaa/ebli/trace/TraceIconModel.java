/*
 *  @creation     13 juin 2005
 *  @modification $Date: 2007-05-22 14:19:05 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import java.awt.Color;
import org.fudaa.ctulu.CtuluLib;

/**
 * @author Fred Deniger
 * @version $Id: TraceIconModel.java,v 1.7 2007-05-22 14:19:05 deniger Exp $
 */
public class TraceIconModel {

  /**
   * Ces propri�t�s permettent d'acc�der par introspection aux donn�es. Mettre � jour ces derni�res si on modifie les propri�t�s.
   */
  public static final String PROPERTY_TAILLE = "taille";
  public static final String PROPERTY_COULEUR = "couleur";
  public static final String PROPERTY_TYPE = "type";
  public static final String PROPERTY_EPAISSEUR_LIGNE = "epaisseurLigne";
  public static final String PROPERTY_BACKGROUND_COLOR = "backgroundColor";
  public static final String PROPERTY_BACKGROUND_COLOR_PAINTED = "backgroundColorPainted";
  Color couleur_;
  Color backgroundColor;
  boolean backgroundColorPainted;
  int taille_;
  int type_;
  float epaisseurLigne_ = 1f;
  int typeLigne_ = TraceLigne.LISSE;

  public TraceIconModel() {
    setDefaults();
  }

  public boolean isBackgroundColorPainted() {
    return backgroundColorPainted;
  }

  public void setBackgroundColorPainted(boolean backgroundColorPainted) {
    this.backgroundColorPainted = backgroundColorPainted;
  }

  public TraceIconModel(final int _type, final int _taille, final Color _c) {
    type_ = _type;
    taille_ = _taille;
    couleur_ = _c;

  }

  @Override
  public boolean equals(Object _obj) {
    if (_obj == this) {
      return true;
    }
    if (_obj instanceof TraceIconModel) {
      TraceIconModel other = (TraceIconModel) _obj;
      return taille_ == other.taille_
              && typeLigne_ == other.typeLigne_
              && epaisseurLigne_ == other.epaisseurLigne_
              && type_ == other.type_
              && CtuluLib.isEquals(couleur_, other.couleur_)
              && backgroundColorPainted == other.backgroundColorPainted
              && CtuluLib.isEquals(backgroundColor, other.backgroundColor);
    }
    return false;

  }

  public void setEpaisseurLigne(float epaisseurLigne) {
    this.epaisseurLigne_ = epaisseurLigne;
  }

  public float getEpaisseurLigne() {
    return epaisseurLigne_;
  }

  public void setTypeLigne(int typeLigne) {
    this.typeLigne_ = typeLigne;
  }

  public void setBackgroundColor(Color backgroundColor) {
    this.backgroundColor = backgroundColor;
  }

  @Override
  public int hashCode() {
    return type_ + taille_ * 10 + typeLigne_ + Float.floatToIntBits(epaisseurLigne_) + (couleur_ == null ? 0 : couleur_.hashCode()) + (backgroundColorPainted ? 1 : 0) + (backgroundColor == null ? 0 : backgroundColor.hashCode());
  }

  public TraceIconModel(final TraceIconModel _d) {
    setDefaults();
    if (_d != null) {
      if (!_d.isTypeIgnored()) {
        type_ = _d.type_;
      }
      if (!_d.isTailleIgnored()) {
        taille_ = _d.taille_;
      }
      if (!_d.isColorIgnored()) {
        couleur_ = _d.couleur_;
      }
      backgroundColor=_d.backgroundColor;
      backgroundColorPainted=_d.backgroundColorPainted;
    }

  }

  public TraceIcon buildCopy() {
    return new TraceIcon(new TraceIconModel(this));
  }

  private void setDefaults() {
    couleur_ = Color.BLACK;
    type_ = TraceIcon.CARRE_PLEIN;
    taille_ = 3;
  }

  public final TraceIconModel cloneData() {
    return new TraceIconModel(this);
  }

  public final Color getCouleur() {
    return couleur_;
  }

  public Color getBackgroundColor() {
    return backgroundColor;
  }

  public final void getData(final TraceIconModel _ic) {
    _ic.type_ = type_;
    _ic.taille_ = taille_;
    _ic.couleur_ = couleur_;
  }

  public final int getTaille() {
    return taille_;
  }

  public final int getType() {
    return type_;
  }

  public int getTypeLigne() {
    return typeLigne_;
  }

  public boolean isColorIgnored() {
    return couleur_ == null;
  }

  public boolean isInitialized() {
    return taille_ > 0 && type_ >= 0 && couleur_ != null;
  }

  public boolean isTailleIgnored() {
    return taille_ < 0;
  }

  public boolean isEpaisseurIgnored() {
    return epaisseurLigne_ < 0;
  }

  public boolean isTypeIgnored() {
    return type_ < 0;
  }

  public boolean isTypeLigneIgnored() {
    return typeLigne_ < 0;
  }

  public boolean keepCommonValues(final TraceIconModel _d) {
    if (type_ != _d.type_) {
      type_ = -1;
    }
    if (taille_ != _d.taille_) {
      taille_ = -1;
    }
    if (couleur_ != null && !couleur_.equals(_d.couleur_)) {
      couleur_ = null;
    }
    if (typeLigne_ != _d.typeLigne_) {
      typeLigne_ = -1;
    }
    if (epaisseurLigne_ != _d.epaisseurLigne_) {
      epaisseurLigne_ = -1;
    }
    if (backgroundColor != null && !backgroundColor.equals(_d.backgroundColor)) {
      backgroundColor = null;
    }
    return type_ >= 0 || taille_ >= 0 && couleur_ != null && typeLigne_ > 0 && epaisseurLigne_ > 0;

  }

  public void setColorIgnored() {
    couleur_ = null;
  }

  public boolean setCouleur(final Color _couleur) {
    if (_couleur == couleur_) {
      return false;
    }
    couleur_ = _couleur;
    return true;
  }

  public void setEpaisseurIgnored() {
    taille_ = -1;
  }

  public void setTaille(final int _taille) {
    taille_ = _taille;
  }

  public void setType(final int _type) {
    type_ = _type;
  }

  public void setTypeIgnored() {
    type_ = -1;
  }

  @Override
  public String toString() {
    return "trace icone type=" + type_ + ", epaisseur " + taille_ + ", color=" + couleur_;
  }

  /**
   * @param _model le nouveau modele pour le trac�
   * @return true si modif. SI _model est null, renvoie false
   */
  public boolean updateData(final TraceIconModel _model) {

    if (_model == null) {
      return false;
    }
    boolean r = false;
    if (!_model.isColorIgnored() && (!_model.getCouleur().equals(getCouleur()))) {
      setCouleur(_model.getCouleur());
      r = true;
    }
    if (!_model.isTypeIgnored() && _model.getType() != getType()) {
      setType(_model.getType());
      r = true;
    }
    if (!_model.isTailleIgnored() && _model.getTaille() != getTaille()) {
      setTaille(_model.getTaille());
      r = true;
    }
    if (!_model.isEpaisseurIgnored() && _model.epaisseurLigne_ != epaisseurLigne_) {
      epaisseurLigne_ = _model.epaisseurLigne_;
      r = true;
    }
    if (!_model.isTypeLigneIgnored() && _model.typeLigne_ != typeLigne_) {
      typeLigne_ = _model.typeLigne_;
      r = true;
    }
    backgroundColor = _model.backgroundColor;
    backgroundColorPainted = _model.backgroundColorPainted;
    return r;
  }
}
