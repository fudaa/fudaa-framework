/*
 * @creation     1999-01-11
 * @modification $Date: 2007-05-04 13:49:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.trace;

import java.awt.Graphics2D;
import java.awt.Polygon;

/**
 * Trace d'iso-lignes avec une palette d�finie par des plages de valeurs. <p> Les plages peuvent �tre r�parties de mani�re non homog�ne. Le trac�
 * accepte �galement une palette � plages non contig�es (avec des trous), ou dont les bornes sont incluses dans l'intervalle min/max des valeurs �
 * tracer.
 *
 * @version $Revision: 1.10 $ $Date: 2007-05-04 13:49:47 $ by $Author: deniger $
 * @author Bertrand Marchand
 */
public class TraceIsoLignesAvecPlages extends TraceIsoAvecPlage {

  final TraceLigne ligne_;

  /**
   * Cr�ation du trac� d'isosurface. <p> <b>Remarque :</b> Lors de la cr�ation, le trac� d�fini les niveaux. Il doit �tre recr�� chaque fois que le
   * contenu de la palette est modifi�.
   *
   * @param _pal la palette en question
   */
  public TraceIsoLignesAvecPlages(final TraceIsoPlageInterface _pal, final int _alpha, TraceLigneModel _ligne) {
    super.updateFromPlageData(_pal, _alpha);
    if (_ligne == null) {
      ligne_ = new TraceLigne();
    } else {
      ligne_ = new TraceLigne(_ligne);
    }
  }

  /**
   * @param _g le graphique support
   * @param _p le polygon a dessine
   * @param _v les valeurs aux points du poly
   */
  @Override
  public void draw(final Graphics2D _g, final Polygon _p, final double[] _v) {
    drawX(_g, _p, _v);
  }

  private void drawX(final Graphics2D _g, final Polygon _p, final double[] _v) {
    if (_p.npoints == 3) {
      draw3(_g, _p, _v);
    } else if (_p.npoints == 4) {
      draw4(_g, _p, _v);
    } else if (_p.npoints >= 5) {
      draw5(_g, _p, _v);
    }
  }

  private void draw5(final Graphics2D _g, final Polygon _p, final double[] _v) {
    final Polygon p1 = new Polygon();
    final Polygon p2 = new Polygon();
    final double[] v1 = new double[3];
    final double[] v2 = new double[_p.npoints - 1];
    p1.addPoint(_p.xpoints[0], _p.ypoints[0]);
    p1.addPoint(_p.xpoints[1], _p.ypoints[1]);
    p1.addPoint(_p.xpoints[_p.npoints - 1], _p.ypoints[_p.npoints - 1]);
    v1[0] = _v[0];
    v1[1] = _v[1];
    v1[2] = _v[_p.npoints - 1];
    for (int i = 1; i < _p.npoints; i++) {
      p2.addPoint(_p.xpoints[i], _p.ypoints[i]);
      v2[i - 1] = _v[i];
    }
    draw3(_g, p1, v1);
    drawX(_g, p2, v2);
  }

  private void draw4(final Graphics2D _g, final Polygon _p, final double[] _v) {
    final Polygon p1 = new Polygon();
    final Polygon p2 = new Polygon();
    p1.addPoint(_p.xpoints[0], _p.ypoints[0]);
    p1.addPoint(_p.xpoints[1], _p.ypoints[1]);
    p1.addPoint(_p.xpoints[3], _p.ypoints[3]);
    p2.addPoint(_p.xpoints[1], _p.ypoints[1]);
    p2.addPoint(_p.xpoints[2], _p.ypoints[2]);
    p2.addPoint(_p.xpoints[3], _p.ypoints[3]);
    final double[] v1 = new double[3];
    final double[] v2 = new double[3];
    v1[0] = _v[0];
    v1[1] = _v[1];
    v1[2] = _v[3];
    v2[0] = _v[1];
    v2[1] = _v[2];
    v2[2] = _v[3];
    draw3(_g, p1, v1);
    draw3(_g, p2, v2);
  }

  /**
   * Trac� d'un triangle.
   */
  private void draw3(final Graphics2D _g, final Polygon _p, final double[] _v) {
    // int[] xp=new int[5];
    // int[] yp=new int[5];
    final int[] xp = new int[2];
    final int[] yp = new int[2];
    // int np;
    // int pn=_p.npoints;
    final int[] px = _p.xpoints;
    final int[] py = _p.ypoints;
    double d;
    // Ordonnancement des valeurs aux 3 points.
    int nmin;
    int nmoy;
    int nmax;
    if (_v[0] > _v[1]) {
      if (_v[0] > _v[2]) {
        nmax = 0;
        if (_v[1] > _v[2]) {
          nmoy = 1;
          nmin = 2;
        } else {
          nmoy = 2;
          nmin = 1;
        }
      } else {
        nmin = 1;
        nmoy = 0;
        nmax = 2;
      }
    } else {
      if (_v[0] < _v[2]) {
        nmin = 0;
        if (_v[1] < _v[2]) {
          nmoy = 1;
          nmax = 2;
        } else {
          nmoy = 2;
          nmax = 1;
        }
      } else {
        nmin = 2;
        nmoy = 0;
        nmax = 1;
      }
    }
    boolean nminInt = false;
    boolean nmoyInt = false;
    boolean nmaxInt = false;
    int n11 = -1; // Pour acceptation compil.
    int n12 = -1; // Pour acceptation compil.
    int n21 = -1; // Pour acceptation compil.
    int n22 = -1; // Pour acceptation compil.
    // np=-1; // Pour acceptation compil.
    for (int i = 0; i < vniv_.length; i++) {
      if (nmaxInt) {
        break; // Le point de valeur max est pass� => Fin de trac�.
      }
      if (_v[nmin] < vniv_[i] && !nminInt) {
        // xp[np]=px[nmin];
        // yp[np]=py[nmin];
        // np++;
        nminInt = true;
        n11 = nmin;
        n12 = nmoy;
        n21 = nmin;
        n22 = nmax;
      }
      if (_v[nmoy] < vniv_[i] && !nmoyInt) {
        // xp[np]=px[nmoy];
        // yp[np]=py[nmoy];
        // np++;
        nmoyInt = true;
        n11 = nmoy;
        n12 = nmax;
      }
      if (_v[nmax] < vniv_[i] && !nmaxInt) {
        // xp[np]=px[nmax];
        // yp[np]=py[nmax];
        // np++;
        nmaxInt = true;
      }
      if (nminInt && !nmaxInt) {
        // Points d'intersection
        d = (vniv_[i] - _v[n11]) / (_v[n12] - _v[n11]);
        xp[0] = (int) ((px[n12] - px[n11]) * d + px[n11]);
        yp[0] = (int) ((py[n12] - py[n11]) * d + py[n11]);
        // np++;
        d = (vniv_[i] - _v[n21]) / (_v[n22] - _v[n21]);
        xp[1] = (int) ((px[n22] - px[n21]) * d + px[n21]);
        yp[1] = (int) ((py[n22] - py[n21]) * d + py[n21]);
        // np++;
      }
      if (nminInt && cniv_[i] != null) {
        if (clipEcran == null || clipEcran.intersectXYBoite(xp[0], yp[0], xp[1], yp[1])) {
          double dx = xp[1] - xp[0];
          double dy = yp[1] - yp[0];
          _g.setColor(cniv_[i]);
          boolean painted = false;
          int idx0 = xp[0] + yp[0] * w;
          int idx1 = xp[1] + yp[1] * w;
          if (memory != null) {
            if (dx == 0 && dy == 0) {
              if (!memory.isSelected(idx0)) {
                if (idx0 >= 0) {
                  memory.add(idx0);
                }
                painted = true;
                _g.drawLine(xp[0], yp[0], xp[1], yp[1]);
              }
            } else if (Math.abs(dy) <= 1 && Math.abs(dx) <= 1) {
              if (!memory.isSelected(idx0) || !memory.isSelected(idx1)) {
                painted = true;
                _g.drawLine(xp[0], yp[0], xp[1], yp[1]);
                if (idx0 >= 0) {
                  memory.add(idx0);
                }
                if (idx1 >= 0) {
                  memory.add(idx1);
                }
              }


            }
          }
          if (!painted) {
            ligne_.setCouleur(cniv_[i]);
            if (memory != null) {
              if (idx0 >= 0) {
                memory.add(idx0);
              }
              if (idx1 >= 0) {
                memory.add(idx1);
              }
            }
            ligne_.dessineTrait(_g, xp[0], yp[0], xp[1], yp[1]);
          }
        }
      }
    }
  }
}
