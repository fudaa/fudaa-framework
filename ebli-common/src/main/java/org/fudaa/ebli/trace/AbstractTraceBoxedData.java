/*
 GPL 2
 */
package org.fudaa.ebli.trace;

import java.awt.Point;
import javax.swing.SwingConstants;

/**
 *
 * @author Frederic Deniger
 */
public class AbstractTraceBoxedData {

  int hPosition_ = SwingConstants.LEFT;
  int vPosition_ = SwingConstants.TOP;

  public AbstractTraceBoxedData(){

  }

  public AbstractTraceBoxedData(AbstractTraceBoxedData initFrom){
    if(initFrom!=null){
      this.hPosition_=initFrom.hPosition_;
      this.vPosition_=initFrom.vPosition_;
    }
  }

  /**
   * @param _position Position horizontale du texte.
   * @see SwingConstants#LEFT
   * @see SwingConstants#CENTER
   * @see SwingConstants#RIGHT
   */
  public void setHPosition(final int _position) {
    hPosition_ = _position;
  }

  /**
   * @param _position Position verticale du texte.
   * @see SwingConstants#TOP
   * @see SwingConstants#CENTER
   * @see SwingConstants#BOTTOM
   */
  public void setVPosition(final int _position) {
    vPosition_ = _position;
  }

  public int getHPosition() {
    return hPosition_;
  }

  public int getVPosition() {
    return vPosition_;
  }

  /**
   * Positionne HPosition et VPosition pour repondre � la position. ptToModify et le point final du trace. Il sera modifi� pour respecter le delta
   * deamnde
   *
   * @param position in SwingConstants.CENTER, SwingConstants.EAST,SwingConstants.NORTH_EAST,....
   * @param delta le delta entre le centre du trace et le point d'ancrage
   * @param ptToModify
   */
  public void configureFor(int position, int delta, Point ptToModify) {
    if (position == SwingConstants.CENTER) {
      setHPosition(SwingConstants.CENTER);
      setVPosition(SwingConstants.CENTER);
    }
    if (position == SwingConstants.EAST) {
      setHPosition(SwingConstants.LEFT);
      setVPosition(SwingConstants.CENTER);
      ptToModify.x += delta;
    }
    if (position == SwingConstants.WEST) {
      setHPosition(SwingConstants.RIGHT);
      setVPosition(SwingConstants.CENTER);
      ptToModify.x -= delta;
    }
    if (position == SwingConstants.NORTH_EAST) {
      setHPosition(SwingConstants.LEFT);
      setVPosition(SwingConstants.BOTTOM);
      ptToModify.x += delta;
      ptToModify.y -= delta;
    }
    if (position == SwingConstants.NORTH) {
      setHPosition(SwingConstants.CENTER);
      setVPosition(SwingConstants.BOTTOM);
      ptToModify.y -= delta;
    }
    if (position == SwingConstants.NORTH_WEST) {
      setHPosition(SwingConstants.RIGHT);
      setVPosition(SwingConstants.BOTTOM);
      ptToModify.y -= delta;
      ptToModify.x -= delta;
    }
    if (position == SwingConstants.SOUTH_EAST) {
      setHPosition(SwingConstants.LEFT);
      setVPosition(SwingConstants.TOP);
      ptToModify.x += delta;
      ptToModify.y += delta;
    }
    if (position == SwingConstants.SOUTH) {
      setHPosition(SwingConstants.CENTER);
      setVPosition(SwingConstants.TOP);
      ptToModify.y += delta;
    }
    if (position == SwingConstants.SOUTH_WEST) {
      setHPosition(SwingConstants.RIGHT);
      setVPosition(SwingConstants.TOP);
      ptToModify.y += delta;
      ptToModify.x -= delta;
    }

  }
}
