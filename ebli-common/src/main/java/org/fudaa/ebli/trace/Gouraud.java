/*
 * @creation     1998-02-18
 * @modification $Date: 2006-09-19 14:55:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.trace;

import java.awt.Color;
import java.awt.Graphics;

/**
 * @version $Revision: 1.9 $ $Date: 2006-09-19 14:55:49 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class Gouraud {
  int niveau_ = 32;

  int taille_ = 100;

  Graphics g_;

  public Gouraud(final Graphics _g) {
    g_ = _g;
  }

  public Gouraud(final Graphics _g, final int _niveau, final int _taille) {
    g_ = _g;
    niveau_ = _niveau;
    taille_ = _taille;
  }

  // annexes
  static int abs(final int _v) {
    return _v < 0 ? -_v : _v;
  }

  static int max(final int _v1, final int _v2) {
    return _v1 < _v2 ? _v2 : _v1;
  }

  static int max(final int _v1, final int _v2, final int _v3) {
    return max(_v1, max(_v2, _v3));
  }

  static int plat(final int[] _v) {
    int r = 0;
    for (int i = 0; i < _v.length; i++) {
      for (int j = i + 1; j < _v.length; j++) {
        r = max(r, abs(_v[i] - _v[j]));
      }
    }
    return r;
  }

  // accesseurs
  public Graphics getGraphics() {
    return g_;
  }

  // drawRectangle
  public void drawRectangle(final int[] _xp, final int[] _yp, final Color[] _cc) {
    final int[] rc = { _cc[0].getRed(), _cc[1].getRed(), _cc[2].getRed(), _cc[3].getRed() };
    final int[] vc = { _cc[0].getGreen(), _cc[1].getGreen(), _cc[2].getGreen(), _cc[3].getGreen() };
    final int[] bc = { _cc[0].getBlue(), _cc[1].getBlue(), _cc[2].getBlue(), _cc[3].getBlue() };
    drawRectangle(_xp, _yp, rc, vc, bc);
  }

  public void drawRectangle(final int[] _xp, final int[] _yp, final int[] _rc, final int[] _vc, final int[] _bc) {
    final int w = plat(_xp);
    final int h = plat(_yp);
    final int l = max(plat(_rc), plat(_vc), plat(_bc));
    if (((w <= taille_) && (h <= taille_)) || (l < niveau_)) {
      final int r = (_rc[0] + _rc[1] + _rc[2] + _rc[3]) / 4;
      final int v = (_vc[0] + _vc[1] + _vc[2] + _vc[3]) / 4;
      final int b = (_bc[0] + _bc[1] + _bc[2] + _bc[3]) / 4;
      final Color c = new Color(r, v, b);
      g_.setColor(c);
      g_.drawPolygon(_xp, _yp, 4);
    } else {
      final int x01 = (_xp[0] + _xp[1]) / 2;
      final int y01 = (_yp[0] + _yp[1]) / 2;
      final int x12 = (_xp[1] + _xp[2]) / 2;
      final int y12 = (_yp[1] + _yp[2]) / 2;
      final int x23 = (_xp[2] + _xp[3]) / 2;
      final int y23 = (_yp[2] + _yp[3]) / 2;
      final int x30 = (_xp[3] + _xp[0]) / 2;
      final int y30 = (_yp[3] + _yp[0]) / 2;
      final int xmm = (x01 + x12 + x23 + x30) / 4;
      final int ymm = (y01 + y12 + y23 + y30) / 4;
      final int r01 = (_rc[0] + _rc[1]) / 2;
      final int v01 = (_vc[0] + _vc[1]) / 2;
      final int b01 = (_bc[0] + _bc[1]) / 2;
      final int r12 = (_rc[1] + _rc[2]) / 2;
      final int v12 = (_vc[1] + _vc[2]) / 2;
      final int b12 = (_bc[1] + _bc[2]) / 2;
      final int r23 = (_rc[2] + _rc[3]) / 2;
      final int v23 = (_vc[2] + _vc[3]) / 2;
      final int b23 = (_bc[2] + _bc[3]) / 2;
      final int r30 = (_rc[3] + _rc[0]) / 2;
      final int v30 = (_vc[3] + _vc[0]) / 2;
      final int b30 = (_bc[3] + _bc[0]) / 2;
      final int rmm = (r01 + r12 + r23 + r30) / 4;
      final int vmm = (v01 + v12 + v23 + v30) / 4;
      final int bmm = (b01 + b12 + b23 + b30) / 4;
      final int[] x0s = { _xp[0], x01, xmm, x30 };
      final int[] y0s = { _yp[0], y01, ymm, y30 };
      final int[] r0s = { _rc[0], r01, rmm, r30 };
      final int[] v0s = { _vc[0], v01, vmm, v30 };
      final int[] b0s = { _bc[0], b01, bmm, b30 };
      drawRectangle(x0s, y0s, r0s, v0s, b0s);
      final int[] x1s = { _xp[1], x12, xmm, x01 };
      final int[] y1s = { _yp[1], y12, ymm, y01 };
      final int[] r1s = { _rc[1], r12, rmm, r01 };
      final int[] v1s = { _vc[1], v12, vmm, v01 };
      final int[] b1s = { _bc[1], b12, bmm, b01 };
      drawRectangle(x1s, y1s, r1s, v1s, b1s);
      final int[] x2s = { _xp[2], x23, xmm, x12 };
      final int[] y2s = { _yp[2], y23, ymm, y12 };
      final int[] r2s = { _rc[2], r23, rmm, r12 };
      final int[] v2s = { _vc[2], v23, vmm, v12 };
      final int[] b2s = { _bc[2], b23, bmm, b12 };
      drawRectangle(x2s, y2s, r2s, v2s, b2s);
      final int[] x3s = { _xp[3], x30, xmm, x23 };
      final int[] y3s = { _yp[3], y30, ymm, y23 };
      final int[] r3s = { _rc[3], r30, rmm, r23 };
      final int[] v3s = { _vc[3], v30, vmm, v23 };
      final int[] b3s = { _bc[3], b30, bmm, b23 };
      drawRectangle(x3s, y3s, r3s, v3s, b3s);
    }
  }

  // fillRectangle
  public void fillRectangle(final int[] _xp, final int[] _yp, final Color[] _cc) {
    final int[] rc = { _cc[0].getRed(), _cc[1].getRed(), _cc[2].getRed(), _cc[3].getRed() };
    final int[] vc = { _cc[0].getGreen(), _cc[1].getGreen(), _cc[2].getGreen(), _cc[3].getGreen() };
    final int[] bc = { _cc[0].getBlue(), _cc[1].getBlue(), _cc[2].getBlue(), _cc[3].getBlue() };
    fillRectangle(_xp, _yp, rc, vc, bc);
  }

  public void fillRectangle(final int[] _xp, final int[] _yp, final int[] _rc, final int[] _vc, final int[] _bc) {
    final int w = plat(_xp);
    final int h = plat(_yp);
    final int l = max(plat(_rc), plat(_vc), plat(_bc));
    if (((w <= taille_) && (h <= taille_)) || (l < niveau_)) {
      final int r = (_rc[0] + _rc[1] + _rc[2] + _rc[3]) / 4;
      final int v = (_vc[0] + _vc[1] + _vc[2] + _vc[3]) / 4;
      final int b = (_bc[0] + _bc[1] + _bc[2] + _bc[3]) / 4;
      final Color c = new Color(r, v, b);
      g_.setColor(c);
      g_.drawPolygon(_xp, _yp, 4);
      g_.fillPolygon(_xp, _yp, 4);
    } else {
      final int x01 = (_xp[0] + _xp[1]) / 2;
      final int y01 = (_yp[0] + _yp[1]) / 2;
      final int x12 = (_xp[1] + _xp[2]) / 2;
      final int y12 = (_yp[1] + _yp[2]) / 2;
      final int x23 = (_xp[2] + _xp[3]) / 2;
      final int y23 = (_yp[2] + _yp[3]) / 2;
      final int x30 = (_xp[3] + _xp[0]) / 2;
      final int y30 = (_yp[3] + _yp[0]) / 2;
      final int xmm = (_xp[0] + _xp[1] + _xp[2] + _xp[3]) / 4;
      final int ymm = (_yp[0] + _yp[1] + _yp[2] + _yp[3]) / 4;
      // int xmm=(x01+x12+x23+x30)/4;
      // int ymm=(y01+y12+y23+y30)/4;
      final int r01 = (_rc[0] + _rc[1]) / 2;
      final int v01 = (_vc[0] + _vc[1]) / 2;
      final int b01 = (_bc[0] + _bc[1]) / 2;
      final int r12 = (_rc[1] + _rc[2]) / 2;
      final int v12 = (_vc[1] + _vc[2]) / 2;
      final int b12 = (_bc[1] + _bc[2]) / 2;
      final int r23 = (_rc[2] + _rc[3]) / 2;
      final int v23 = (_vc[2] + _vc[3]) / 2;
      final int b23 = (_bc[2] + _bc[3]) / 2;
      final int r30 = (_rc[3] + _rc[0]) / 2;
      final int v30 = (_vc[3] + _vc[0]) / 2;
      final int b30 = (_bc[3] + _bc[0]) / 2;
      final int rmm = (r01 + r12 + r23 + r30) / 4;
      final int vmm = (v01 + v12 + v23 + v30) / 4;
      final int bmm = (b01 + b12 + b23 + b30) / 4;
      final int[] x0s = { _xp[0], x01, xmm, x30 };
      final int[] y0s = { _yp[0], y01, ymm, y30 };
      final int[] r0s = { _rc[0], r01, rmm, r30 };
      final int[] v0s = { _vc[0], v01, vmm, v30 };
      final int[] b0s = { _bc[0], b01, bmm, b30 };
      fillRectangle(x0s, y0s, r0s, v0s, b0s);
      final int[] x1s = { _xp[1], x12, xmm, x01 };
      final int[] y1s = { _yp[1], y12, ymm, y01 };
      final int[] r1s = { _rc[1], r12, rmm, r01 };
      final int[] v1s = { _vc[1], v12, vmm, v01 };
      final int[] b1s = { _bc[1], b12, bmm, b01 };
      fillRectangle(x1s, y1s, r1s, v1s, b1s);
      final int[] x2s = { _xp[2], x23, xmm, x12 };
      final int[] y2s = { _yp[2], y23, ymm, y12 };
      final int[] r2s = { _rc[2], r23, rmm, r12 };
      final int[] v2s = { _vc[2], v23, vmm, v12 };
      final int[] b2s = { _bc[2], b23, bmm, b12 };
      fillRectangle(x2s, y2s, r2s, v2s, b2s);
      final int[] x3s = { _xp[3], x30, xmm, x23 };
      final int[] y3s = { _yp[3], y30, ymm, y23 };
      final int[] r3s = { _rc[3], r30, rmm, r23 };
      final int[] v3s = { _vc[3], v30, vmm, v23 };
      final int[] b3s = { _bc[3], b30, bmm, b23 };
      fillRectangle(x3s, y3s, r3s, v3s, b3s);
    }
  }

  // draw
  public void drawTriangle(final int[] _xp, final int[] _yp, final Color[] _cc) {
    final int[] rc = { _cc[0].getRed(), _cc[1].getRed(), _cc[2].getRed() };
    final int[] vc = { _cc[0].getGreen(), _cc[1].getGreen(), _cc[2].getGreen() };
    final int[] bc = { _cc[0].getBlue(), _cc[1].getBlue(), _cc[2].getBlue() };
    drawTriangle(_xp, _yp, rc, vc, bc);
  }

  public void drawTriangle(final int[] _xp, final int[] _yp, final int[] _rc, final int[] _vc, final int[] _bc) {
    final int w = plat(_xp);
    final int h = plat(_yp);
    final int l = max(plat(_rc), plat(_vc), plat(_bc));
    if (((w <= taille_) && (h <= taille_)) || (l <= niveau_)) {
      final int r = (_rc[0] + _rc[1] + _rc[2]) / 3;
      final int v = (_vc[0] + _vc[1] + _vc[2]) / 3;
      final int b = (_bc[0] + _bc[1] + _bc[2]) / 3;
      final Color c = new Color(r, v, b);
      g_.setColor(c);
      g_.drawPolygon(_xp, _yp, 3);
    } else {
      final int x01 = (_xp[0] + _xp[1]) / 2;
      final int y01 = (_yp[0] + _yp[1]) / 2;
      final int x12 = (_xp[1] + _xp[2]) / 2;
      final int y12 = (_yp[1] + _yp[2]) / 2;
      final int x20 = (_xp[2] + _xp[0]) / 2;
      final int y20 = (_yp[2] + _yp[0]) / 2;
      final int r01 = (_rc[0] + _rc[1]) / 2;
      final int v01 = (_vc[0] + _vc[1]) / 2;
      final int b01 = (_bc[0] + _bc[1]) / 2;
      final int r12 = (_rc[1] + _rc[2]) / 2;
      final int v12 = (_vc[1] + _vc[2]) / 2;
      final int b12 = (_bc[1] + _bc[2]) / 2;
      final int r20 = (_rc[2] + _rc[0]) / 2;
      final int v20 = (_vc[2] + _vc[0]) / 2;
      final int b20 = (_bc[2] + _bc[0]) / 2;
      final int[] x0s = { x01, x20, _xp[0] };
      final int[] y0s = { y01, y20, _yp[0] };
      final int[] r0s = { r01, r20, _rc[0] };
      final int[] v0s = { v01, v20, _vc[0] };
      final int[] b0s = { b01, b20, _bc[0] };
      drawTriangle(x0s, y0s, r0s, v0s, b0s);
      final int[] x1s = { x12, x01, _xp[1] };
      final int[] y1s = { y12, y01, _yp[1] };
      final int[] r1s = { r12, r01, _rc[1] };
      final int[] v1s = { v12, v01, _vc[1] };
      final int[] b1s = { b12, b01, _bc[1] };
      drawTriangle(x1s, y1s, r1s, v1s, b1s);
      final int[] x2s = { x12, x20, _xp[2] };
      final int[] y2s = { y12, y20, _yp[2] };
      final int[] r2s = { r12, r20, _rc[2] };
      final int[] v2s = { v12, v20, _vc[2] };
      final int[] b2s = { b12, b20, _bc[2] };
      drawTriangle(x2s, y2s, r2s, v2s, b2s);
      final int[] x4s = { x01, x12, x20 };
      final int[] y4s = { y01, y12, y20 };
      final int[] r4s = { r01, r12, r20 };
      final int[] v4s = { v01, v12, v20 };
      final int[] b4s = { b01, b12, b20 };
      drawTriangle(x4s, y4s, r4s, v4s, b4s);
    }
  }

  // fill
  public void fillTriangle(final int[] _xp, final int[] _yp, final Color[] _cc) {
    final int[] rc = { _cc[0].getRed(), _cc[1].getRed(), _cc[2].getRed() };
    final int[] vc = { _cc[0].getGreen(), _cc[1].getGreen(), _cc[2].getGreen() };
    final int[] bc = { _cc[0].getBlue(), _cc[1].getBlue(), _cc[2].getBlue() };
    fillTriangle(_xp, _yp, rc, vc, bc);
  }

  public void fillTriangle(final int[] _xp, final int[] _yp, final int[] _rc, final int[] _vc, final int[] _bc) {
    final int w = plat(_xp);
    final int h = plat(_yp);
    final int l = max(plat(_rc), plat(_vc), plat(_bc));
    if (((w <= taille_) && (h <= taille_)) || (l <= niveau_)) {
      final int r = (_rc[0] + _rc[1] + _rc[2]) / 3;
      final int v = (_vc[0] + _vc[1] + _vc[2]) / 3;
      final int b = (_bc[0] + _bc[1] + _bc[2]) / 3;
      final Color c = new Color(r, v, b);
      g_.setColor(c);
      g_.fillPolygon(_xp, _yp, 3);
      g_.drawPolygon(_xp, _yp, 3);
    } else {
      final int x01 = (_xp[0] + _xp[1]) / 2;
      final int y01 = (_yp[0] + _yp[1]) / 2;
      final int x12 = (_xp[1] + _xp[2]) / 2;
      final int y12 = (_yp[1] + _yp[2]) / 2;
      final int x20 = (_xp[2] + _xp[0]) / 2;
      final int y20 = (_yp[2] + _yp[0]) / 2;
      final int r01 = (_rc[0] + _rc[1]) / 2;
      final int v01 = (_vc[0] + _vc[1]) / 2;
      final int b01 = (_bc[0] + _bc[1]) / 2;
      final int r12 = (_rc[1] + _rc[2]) / 2;
      final int v12 = (_vc[1] + _vc[2]) / 2;
      final int b12 = (_bc[1] + _bc[2]) / 2;
      final int r20 = (_rc[2] + _rc[0]) / 2;
      final int v20 = (_vc[2] + _vc[0]) / 2;
      final int b20 = (_bc[2] + _bc[0]) / 2;
      final int[] x0s = { x01, x20, _xp[0] };
      final int[] y0s = { y01, y20, _yp[0] };
      final int[] r0s = { r01, r20, _rc[0] };
      final int[] v0s = { v01, v20, _vc[0] };
      final int[] b0s = { b01, b20, _bc[0] };
      fillTriangle(x0s, y0s, r0s, v0s, b0s);
      final int[] x1s = { x12, x01, _xp[1] };
      final int[] y1s = { y12, y01, _yp[1] };
      final int[] r1s = { r12, r01, _rc[1] };
      final int[] v1s = { v12, v01, _vc[1] };
      final int[] b1s = { b12, b01, _bc[1] };
      fillTriangle(x1s, y1s, r1s, v1s, b1s);
      final int[] x2s = { x12, x20, _xp[2] };
      final int[] y2s = { y12, y20, _yp[2] };
      final int[] r2s = { r12, r20, _rc[2] };
      final int[] v2s = { v12, v20, _vc[2] };
      final int[] b2s = { b12, b20, _bc[2] };
      fillTriangle(x2s, y2s, r2s, v2s, b2s);
      final int[] x4s = { x01, x12, x20 };
      final int[] y4s = { y01, y12, y20 };
      final int[] r4s = { r01, r12, r20 };
      final int[] v4s = { v01, v12, v20 };
      final int[] b4s = { b01, b12, b20 };
      fillTriangle(x4s, y4s, r4s, v4s, b4s);
    }
  }
}
