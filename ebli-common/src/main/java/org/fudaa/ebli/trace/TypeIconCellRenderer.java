/*
 *  @creation     25 janv. 2005
 *  @modification $Date: 2006-12-05 10:14:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import java.awt.Color;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;

/**
 * Cell renderer affichant le type d'un icon.
 * @author deniger ( genesis)
 */
public class TypeIconCellRenderer extends CtuluCellTextRenderer {

  TraceIcon defaultIcon = new TraceIcon(TraceIcon.RIEN, 6, Color.BLACK);

  public void setDefaultIcon(TraceIcon defaultIcon) {
    this.defaultIcon = defaultIcon;
  }

  public TraceIcon getDefaultIcon() {
    return defaultIcon;
  }

  @Override
  protected void setValue(final Object _value) {
    defaultIcon.setType(TraceIcon.RIEN);
    setIcon(defaultIcon);
    setText(CtuluLibString.ESPACE);
    if (_value != null) {
      Integer type = (Integer) _value;
      defaultIcon.setType(type);
    }

  }
}
