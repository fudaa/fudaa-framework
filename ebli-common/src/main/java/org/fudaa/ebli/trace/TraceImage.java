/*
 *  @creation     23 juin 2004
 *  @modification $Date: 2006-09-19 14:55:49 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.ImageObserver;
import javax.swing.Icon;
import javax.swing.SwingConstants;
import org.fudaa.ebli.geometrie.GrBoite;

/**
 * Une classe permettant de dessiner un icone externe en le positionnant horizontalement/verticalement
 *
 * @author Fred Deniger
 * @version $Id: TraceBox.java,v 1.5 2006-09-19 14:55:49 deniger Exp $
 */
public class TraceImage extends AbstractTraceBoxedData {

  /**
   *
   */
  public TraceImage() {
  }

  public void paintIcon(final Graphics2D _g2d, final int _x, final int _y, Icon icon, Rectangle view) {
    final int hInner = icon.getIconHeight();
    final int wInner = icon.getIconWidth();
    int xTopLeft = getXTopLeft(_x, wInner, view);
    int yTopLeft = getYTopLeft(_y, hInner, view);
    icon.paintIcon(null, _g2d, xTopLeft, yTopLeft);

  }

  public void paintImage(final Graphics2D _g2d, final int _x, final int _y, Image image, Rectangle view, ImageObserver observer) {
    final int hInner = image.getHeight(observer);
    final int wInner = image.getWidth(observer);
    int xTopLeft = getXTopLeft(_x, wInner, view);
    int yTopLeft = getYTopLeft(_y, hInner, view);
    _g2d.drawImage(image, xTopLeft, yTopLeft, observer);
  }

  /**
   *
   * @param iconBoite une boite a initialiser. si null, une nouvelle instance est cr��e
   * @param icon l'icon pour avoir largeur/hauteur
   * @param x x dessin�
   * @param y y dessine
   * @return la boite dans laquelle l'icone sera dessin� en coordonn�es �cran
   */
  public GrBoite getBoite(GrBoite iconBoite, Icon icon, int x, int y) {
    GrBoite res = iconBoite;
    if (res == null) {
      res = new GrBoite();
    }
    res.e_ = null;
    res.o_ = null;
    final int hInner = icon.getIconHeight();
    final int wInner = icon.getIconWidth();
    int xTopLeft = getXTopLeft(x, wInner, null);
    int yTopLeft = getYTopLeft(y, hInner, null);
    res.ajuste(xTopLeft, yTopLeft, 0);
    res.ajuste(xTopLeft + icon.getIconWidth(), yTopLeft + icon.getIconHeight(), 0);
    return res;
  }

  public TraceImage duplicate() {
    TraceImage duplic = new TraceImage();
    duplic.hPosition_ = this.hPosition_;
    duplic.vPosition_ = this.vPosition_;
    return duplic;
  }

  public int getXTopLeft(final int _x, final int wInner, Rectangle view) {
    int xTopLeft = _x;
    if (hPosition_ == SwingConstants.CENTER) {
      xTopLeft -= wInner / 2;
    } else if (hPosition_ == SwingConstants.RIGHT) {
      xTopLeft -= wInner;
    }
    if (view != null) {
      xTopLeft = Math.min(view.x + view.width - wInner, xTopLeft);
      xTopLeft = Math.max(xTopLeft, view.x);
    }
    return xTopLeft;
  }

  public int getYTopLeft(final int _y, final int hInner, Rectangle view) {
    int yTopLeft = _y;
    if (vPosition_ == SwingConstants.CENTER) {
      yTopLeft -= hInner / 2;
    } else if (vPosition_ == SwingConstants.BOTTOM) {
      yTopLeft -= hInner;
    }
    if (view != null) {
      yTopLeft = Math.min(view.y + view.height - hInner, yTopLeft);
      yTopLeft = Math.max(yTopLeft, view.y);
    }
    return yTopLeft;
  }
}
