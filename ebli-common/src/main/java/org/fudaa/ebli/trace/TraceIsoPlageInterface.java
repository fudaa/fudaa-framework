/*
 *  @creation     13 mai 2004
 *  @modification $Date: 2007-01-19 13:09:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.trace;


/**
 * @author Fred Deniger
 * @version $Id: TraceIsoPlageInterface.java,v 1.1 2007-01-19 13:09:50 deniger Exp $
 */
public interface TraceIsoPlageInterface {

  /**
   * Retourne le nombre de plages de la palette.
   * 
   * @return Le nombre de plages
   */
  int getNbPlages();

  /**
   * @param _idx l'indice
   * @return la plage d'indice _idx
   */
  BPlageInterface getPlageInterface(int _idx);

  /**
   * @return la plage utilisee pour les valeur en dehors des plages definies
   */
  BPlageInterface getPlageAutresInterface();

  boolean isAutresVisible();

}