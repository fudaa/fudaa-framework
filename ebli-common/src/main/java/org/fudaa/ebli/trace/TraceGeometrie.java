/*
 *  @creation     1998-08-31
 *  @modification $Date: 2007-05-04 13:49:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.trace;

import com.memoire.fu.FuLog;
import org.fudaa.ebli.geometrie.*;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;

/**
 * Un trace pour le package geometrie. Il trace des structures specifiques a ce package.
 *
 * @author Axel von Arnim
 * @version $Id: TraceGeometrie.java,v 1.15 2007-05-04 13:49:47 deniger Exp $
 */
public class TraceGeometrie {
  /**
   * Tracer les points.
   */
  private final TracePoint tp_;
  /**
   * Tracer les ligne.
   */
  private final TraceLigne tl_;
  /**
   * Remplir les surfaces.
   */
  private final TraceSurface ts_;
  /**
   *
   */
  private final GrMorphisme versEcran_;
  /** */
  /**
   * Alignement � gauche pour trac� de texte.
   */
  public final static int TEXTE_GAUCHE = 0;
  /**
   * Alignement centr� pour trac� de texte.
   */
  public final static int TEXTE_CENTRE = 1;
  /**
   * Alignement � droite pour trac� de texte.
   */
  public final static int TEXTE_DROITE = 2;

  /**
   * @param _versEcran le morphisme qui convertit en coordonnees ecran
   */
  public TraceGeometrie(final GrMorphisme _versEcran) {
    versEcran_ = _versEcran;
    tp_ = new TracePoint();
    tl_ = new TraceLigne();
    ts_ = new TraceSurface();
  }

  /**
   * Transformation du type <I>GrPolyligne</I> en type primitif. En mode rapide, seuls le log10 du nombre de points
   * sont pris en compte.
   */
  private int[][] polyligneVersPrimitif(final GrPolyligne _l, final int _resolution, final boolean _rapide) {
    final int[][] res = new int[2][];
    final int totNombre = _l.nombre();
    int redNombre;
    if (_rapide) {
      redNombre = Math.max(2, Math.min(totNombre * _resolution / 1000 - 1, 10 + (int) Math.log(totNombre * _resolution
        / 1000D)));
    } else if (totNombre > 10) {
      redNombre = Math.max(10, (int) (totNombre * _resolution / 1000D));
    } else {
      redNombre = totNombre;
    }
    final double interv = (double) totNombre / (double) redNombre;
    try {
      res[0] = new int[redNombre + 1];
      res[1] = new int[redNombre + 1];
    } catch (final Exception _e) {
      FuLog.warning("totNombre=" + totNombre + "\nresolution=" + _resolution + "\nrednombre=" + redNombre + "\nlog="
        + (10 + (int) Math.abs(Math.log(totNombre * _resolution / 1000))));
    }
    GrPoint sommet;
    for (int j = 0; j < redNombre; j++) {
      sommet = _l.sommets_.renvoie((int) (j * interv)).applique(versEcran_);
      res[0][j] = (int) sommet.x_;
      res[1][j] = (int) sommet.y_;
    }
    sommet = _l.sommets_.renvoie(totNombre - 1).applique(versEcran_);
    res[0][redNombre] = (int) sommet.x_;
    res[1][redNombre] = (int) sommet.y_;
    return res;
  }

  /**
   * Change le style de remplissage du trace.
   *
   * @param _styleSurface TypeSurface
   * @see TraceSurface#setTypeSurface(int)
   */
  public void setTypeSurface(final int _styleSurface) {
    ts_.setTypeSurface(_styleSurface);
  }

  /**
   * Change le style de trait du trace.
   *
   * @param _styleTrait TypeTrait
   * @see TraceLigne#setTypeTrait(int)
   */
  public void setTypeTrait(final int _styleTrait) {
    setTypeTrait(_styleTrait, 1f);
  }

  /**
   * @param _styleTrait TypeTrait
   * @param _epaisseur TypeTrait
   */
  public void setTypeTrait(final int _styleTrait, final int _epaisseur) {
    setTypeTrait(_styleTrait, (float) _epaisseur);
  }

  /**
   * @param _styleTrait TypeTrait
   * @param _epaisseur TypeTrait
   */
  public void setTypeTrait(final int _styleTrait, final float _epaisseur) {
    tl_.setTypeTrait(_styleTrait);
    tl_.setEpaisseur(_epaisseur);
  }

  /**
   * Change le style de point du trace.
   *
   * @param _stylePoint TypePoint
   * @see TracePoint#setTypePoint(int)
   */
  public void setTypePoint(final int _stylePoint) {
    tp_.setTypePoint(_stylePoint);
  }

  /**
   * Change la taille de point du trace.
   *
   * @param _taillePoint TaillePoint
   * @see TracePoint#setTaillePoint(int)
   */
  public void setTaillePoint(final int _taillePoint) {
    tp_.setTaillePoint(_taillePoint);
  }

  /**
   * Change la couleur de contour du trace.
   *
   * @param _c CouleurContour
   * @see TracePoint#setCouleur(Color)
   * @see TraceLigne#setCouleur(Color)
   * @deprecated
   */
  public void setCouleurContour(final Color _c) {
    setForeground(_c);
  }

  /**
   * @param _c Foreground
   */
  public void setForeground(final Color _c) {
    tp_.setCouleur(_c);
    tl_.setCouleur(_c);
  }

  /**
   * Change la couleur de remplissage du trace.
   *
   * @param _c CouleurRemplissage
   * @see TraceSurface#setCouleurFond(Color)
   */
  public void setCouleurRemplissage(final Color _c) {
    setBackground(_c);
  }

  /**
   * Change la couleur de remplissage du trace.
   *
   * @param _c Background
   * @see TraceSurface#setCouleurFond(Color)
   */
  public void setBackground(final Color _c) {
    ts_.setCouleurFond(_c);
  }

  /**
   * Change la couleur utilisee pour dessiner les textures: les hachures et les pointilles.
   *
   * @param _c TextureCouleur
   */
  public void setTextureCouleur(final Color _c) {
    ts_.setCouleurPremierPlan(_c);
  }

  private Font ft_;

  /**
   * Change la police.
   *
   * @param _police Font
   */
  public void setFont(final Font _police) {
    ft_ = _police;
  }

  /**
   * @param _icon Icon
   */
  public void setIcon(final Icon _icon) {
    tp_.setIcon(_icon);
  }

  /**
   * @return Icon
   */
  public Icon getIcon() {
    return tp_.getIcon();
  }

  /**
   * Dessine un point.
   *
   * @param _p le point
   * @param _rapide mode rapide: si oui, l'affichage est degrade pour l'accelerer
   */
  public void dessinePoint(final Graphics2D _g, final GrPoint _p, final boolean _rapide) {
    final GrPoint p = _p.applique(versEcran_);
    if (_rapide) {
      final int ptp = tp_.getTypePoint();
      tp_.setTypePoint(TracePoint.POINT);
      tp_.dessinePoint(_g, p.x_, p.y_);
      tp_.setTypePoint(ptp);
    } else {
      tp_.dessinePoint(_g, p.x_, p.y_);
    }
  }

  /**
   * @param _p
   * @param _rapide
   */
  public void dessinePoint(final Graphics2D _g, final GrPositionRelativePoint _p, final boolean _rapide) {
    final GrPositionRelativePoint p = _p.applique(versEcran_);
    if (_rapide) {
      final int ptp = tp_.getTypePoint();
      tp_.setTypePoint(TracePoint.POINT);
      tp_.dessinePoint(_g, p.x(), p.y());
      tp_.setTypePoint(ptp);
    } else {
      tp_.dessinePoint(_g, p.x(), p.y());
    }
  }

  /**
   * @param _p
   * @param _rapide
   */
  public void dessinePoint(final Graphics2D _g, final GrPositionRelativeSegment _p, final boolean _rapide) {
    final GrPositionRelativeSegment p = _p.applique(versEcran_);
    if (_rapide) {
      final int ptp = tp_.getTypePoint();
      tp_.setTypePoint(TracePoint.POINT);
      tp_.dessinePoint(_g, p.x(), p.y());
      tp_.setTypePoint(ptp);
    } else {
      tp_.dessinePoint(_g, p.x(), p.y());
    }
  }

  /**
   * Dessine un trait.
   *
   * @param _p1 premier point
   * @param _p2 deuxieme point
   * @param _rapide mode rapide: si oui, l'affichage est degrade pour l'accelerer
   */
  public void dessineTrait(final Graphics2D _g, final GrPoint _p1, final GrPoint _p2, final boolean _rapide) {
    final GrPoint p1 = _p1.applique(versEcran_);
    final GrPoint p2 = _p2.applique(versEcran_);
    if (_rapide) {
      final int ptl = tl_.getTypeTrait();
      tl_.setTypeTrait(TraceLigne.LISSE);
      tl_.dessineTrait(_g, p1.x_, p1.y_, p2.x_, p2.y_);
      tl_.setTypeTrait(ptl);
    } else {
      tl_.dessineTrait(_g, p1.x_, p1.y_, p2.x_, p2.y_);
    }
  }

  /**
   * Dessine un segment.
   *
   * @param _s un segment
   * @param _rapide mode rapide: si oui, l'affichage est degrade pour l'accelerer
   */
  public void dessineSegment(final Graphics2D _g, final GrSegment _s, final boolean _rapide) {
    final GrSegment s = _s.applique(versEcran_);
    if (_rapide) {
      final int ptl = tl_.getTypeTrait();
      tl_.setTypeTrait(TraceLigne.LISSE);
      tl_.dessineTrait(_g, s.o_.x_, s.o_.y_, s.e_.x_, s.e_.y_);
      tl_.setTypeTrait(ptl);
    } else {
      tl_.dessineTrait(_g, s.o_.x_, s.o_.y_, s.e_.x_, s.e_.y_);
    }
  }

  /**
   * @param _p1
   * @param _p2
   * @param _rapide
   */
  public void dessineTrait(final Graphics2D _g, final GrPositionRelativePoint _p1, final GrPositionRelativePoint _p2,
                           final boolean _rapide) {
    final GrPositionRelativePoint p1 = _p1.applique(versEcran_);
    final GrPositionRelativePoint p2 = _p2.applique(versEcran_);
    if (_rapide) {
      final int ptl = tl_.getTypeTrait();
      tl_.setTypeTrait(TraceLigne.LISSE);
      tl_.dessineTrait(_g, p1.x(), p1.y(), p2.x(), p2.y());
      tl_.setTypeTrait(ptl);
    } else {
      tl_.dessineTrait(_g, p1.x(), p1.y(), p2.x(), p2.y());
    }
  }

  /**
   * @param _p1
   * @param _p2
   * @param _rapide
   */
  public void dessineTrait(final Graphics2D _g, final GrPositionRelativeSegment _p1,
                           final GrPositionRelativeSegment _p2, final boolean _rapide) {
    final GrPositionRelativeSegment p1 = _p1.applique(versEcran_);
    final GrPositionRelativeSegment p2 = _p2.applique(versEcran_);
    if (_rapide) {
      final int ptl = tl_.getTypeTrait();
      tl_.setTypeTrait(TraceLigne.LISSE);
      tl_.dessineTrait(_g, p1.x(), p1.y(), p2.x(), p2.y());
      tl_.setTypeTrait(ptl);
    } else {
      tl_.dessineTrait(_g, p1.x(), p1.y(), p2.x(), p2.y());
    }
  }

  /**
   * Dessine une polyligne.
   *
   * @param _l la polyligne
   * @param _rapide mode rapide: si oui, l'affichage est degrade pour l'accelerer
   */
  public void dessinePolyligne(final Graphics2D _g, final GrPolyligne _l, final boolean _rapide) {
    dessinePolyligne(_g, _l, 1000, _rapide);
  }

  /**
   * Dessine une polyligne.
   *
   * @param _l la polyligne
   * @param _resolution la resolution
   * @param _rapide mode rapide: si oui, l'affichage est degrade pour l'accelerer
   */
  public void dessinePolyligne(final Graphics2D _g, final GrPolyligne _l, final int _resolution, final boolean _rapide) {
    if (_l.sommets_.nombre() == 0) {
      return;
    }
    final int[][] prim = polyligneVersPrimitif(_l, _resolution, _rapide);
    if (_rapide) {
      final int ptl = tl_.getTypeTrait();
      tl_.setTypeTrait(TraceLigne.LISSE);
      tl_.dessinePolyligne(_g, prim[0], prim[1]);
      tl_.setTypeTrait(ptl);
    } else {
      tl_.dessinePolyligne(_g, prim[0], prim[1]);
    }
  }

  /**
   * Dessine un polygone.
   *
   * @param _l le polygone
   * @param _rapide mode rapide: si oui, l'affichage est degrade pour l'accelerer
   * @param _remplissage
   */
  public void dessinePolygone(final Graphics2D _g, final GrPolyligne _l, final boolean _remplissage,
                              final boolean _rapide) {
    dessinePolygone(_g, _l, 1000, _remplissage, _rapide);
  }

  /**
   * @param _l
   * @param _resolution
   * @param _remplissage
   * @param _rapide
   */
  public void dessinePolygone(final Graphics2D _g, final GrPolyligne _l, final int _resolution,
                              final boolean _remplissage, final boolean _rapide) {
    if (_l.sommets_.nombre() == 0) {
      return;
    }
    final int[][] prim = polyligneVersPrimitif(_l, _resolution, _rapide);
    if (_rapide) {
      final int ptl = tl_.getTypeTrait();
      tl_.setTypeTrait(TraceLigne.LISSE);
      tl_.dessinePolygone(_g, prim[0], prim[1]);
      tl_.setTypeTrait(ptl);
    } else {
      if (_remplissage) {
        ts_.remplitPolygone(_g, prim[0], prim[1]);
      }
      tl_.dessinePolygone(_g, prim[0], prim[1]);
    }
  }

  /**
   * @param _l
   * @param _remplissage
   * @param _rapide
   */
  public void dessinePolygone(final Graphics2D _g, final GrPolygone _l, final boolean _remplissage,
                              final boolean _rapide) {
    dessinePolygone(_g, _l, 1000, _remplissage, _rapide);
  }

  /**
   * @param _l
   * @param _resolution
   * @param _remplissage
   * @param _rapide
   */
  public void dessinePolygone(final Graphics2D _g, final GrPolygone _l, final int _resolution,
                              final boolean _remplissage, final boolean _rapide) {
    final GrPolyligne l = new GrPolyligne();
    l.sommets_ = _l.sommets_;
    dessinePolygone(_g, l, _resolution, _remplissage, _rapide);
  }

  /**
   * Dessine un texte.
   *
   * @param _t le texte
   * @param _p position
   * @param _rapide mode rapide: si oui, l'affichage est degrade pour l'accelerer
   */
  public void dessineTexte(final Graphics2D _g, final String _t, final GrPoint _p, final boolean _rapide) {
    dessineTexte(_g, _t, _p, TraceGeometrie.TEXTE_GAUCHE, _rapide);
  }

  /**
   * Dessine un texte align�.
   *
   * @param _t Le texte
   * @param _p Point d'accrochage
   * @param _align Alignement (TEXTE_DROITE, TEXTE_CENTRE, TEXTE_GAUCHE)
   * @param _rapide <i>true</i> l'affichage est degrade pour accelerer le trac�
   */
  public void dessineTexte(final Graphics2D _g, final String _t, final GrPoint _p, final int _align,
                           final boolean _rapide) {
    final GrPoint p = _p.applique(versEcran_);
    int xdecal = 0;
    if (_align == TEXTE_CENTRE) {
      xdecal = _g.getFontMetrics().stringWidth(_t) / 2;
    } else if (_align == TEXTE_DROITE) {
      xdecal = _g.getFontMetrics().stringWidth(_t);
    }
    tl_.dessineTexte(_g, _t, ft_, (int) p.x_ - xdecal, (int) p.y_);
  }

  /**
   * Dessine un symbole.
   *
   * @param _s le symbole
   * @param _remplissage Le symbole est remplit avec la couleur de fond.
   * @param _rapide mode rapide: si oui, l'affichage est degrade pour l'accelerer
   */
  public void dessineSymbole(final Graphics2D _g, final GrSymbole _s, final boolean _remplissage, final boolean _rapide) {
    if (_rapide) {
      dessinePoint(_g, _s.position_, _rapide);
    } else {
      _g.setStroke(tl_.getStroke());
      final AffineTransform at = new AffineTransform();
      GrSymbole s;
      s = _s.applique(versEcran_);
      at.setToTranslation(s.position_.x_, s.position_.y_);
      if (s.suitRotationZ_) {
        at.rotate(Math.atan2(s.direction_.y_, s.direction_.x_));
      } else {
        at.rotate(Math.atan2(_s.direction_.y_, _s.direction_.x_));
      }
      at.scale(s.echelle_, s.echelle_);
      at.translate(-s.xAncre_, -s.yAncre_);
      final Shape sh = at.createTransformedShape(s.trace_);
      if (_remplissage) {
        _g.setColor(ts_.getCouleurFond());
        _g.fill(sh);
      }
      final Color c = tl_.getCouleur();
      _g.setColor(c);
      _g.draw(sh);
    }
  }

  /**
   * Dessine un arc.
   *
   * @param _p1 premier point
   * @param _p2 deuxieme point
   * @param _v1 vecteur direction du premier point
   * @param _v2 vecteur direction du second point
   * @param _remplissage remplissage oui/non
   * @param _rapide mode rapide: si oui, l'affichage est degrade pour l'accelerer
   */
  public void dessineArc(final Graphics2D _g, final GrPoint _p1, final GrPoint _p2, final GrVecteur _v1,
                         final GrVecteur _v2, final boolean _remplissage, final boolean _rapide) {
    final GrPoint p1 = _p1.applique(versEcran_);
    final GrPoint p2 = _p2.applique(versEcran_);
    final GrVecteur v1 = _v1.applique(versEcran_);
    final GrVecteur v2 = _v2.applique(versEcran_);
    if (_rapide) {
      final int ptl = tl_.getTypeTrait();
      tl_.setTypeTrait(TraceLigne.LISSE);
      tl_.dessineTrait(_g, p1.x_, p1.y_, p2.x_, p2.y_);
      tl_.setTypeTrait(ptl);
    } else {
      if (_remplissage) {
        ts_.remplitPolygone(_g, tl_.creeArc(p1.x_, p1.y_, p2.x_, p2.y_, v1.x_, v1.y_, v2.x_, v2.y_));
      }
      tl_.dessineArc(_g, p1.x_, p1.y_, p2.x_, p2.y_, v1.x_, v1.y_, v2.x_, v2.y_);
    }
  }

  /**
   * @param _p1
   * @param _p2
   * @param _p3
   * @param _p4
   * @param _remplissage
   * @param _rapide
   */
  public void dessineEllipse(final Graphics2D _g, final GrPoint _p1, final GrPoint _p2, final GrPoint _p3,
                             final GrPoint _p4, final boolean _remplissage, final boolean _rapide) {
    final GrPoint p1 = _p1.applique(versEcran_);
    final GrPoint p2 = _p2.applique(versEcran_);
    final GrPoint p3 = _p3.applique(versEcran_);
    final GrPoint p4 = _p4.applique(versEcran_);
    if (_rapide) {
      final int ptl = tl_.getTypeTrait();
      tl_.setTypeTrait(TraceLigne.LISSE);
      tl_.dessineRectangle(_g, p1.x_, p1.y_, (p3.x_ - p1.x_), (p3.y_ - p1.y_));
      tl_.setTypeTrait(ptl);
    } else {
      if (_remplissage) {
        ts_.remplitEllipse(_g, p1.x_, p1.y_, p2.x_, p2.y_, p3.x_, p3.y_, p4.x_, p4.y_);
      }
      tl_.dessineEllipse(_g, (int) p1.x_, (int) p1.y_, (int) p2.x_, (int) p2.y_, (int) p3.x_, (int) p3.y_, (int) p4.x_,
        (int) p4.y_);
    }
  }

  /**
   * @param _p1
   * @param _p2
   * @param _rapide
   */
  public void dessineRectangle(final Graphics2D _g, final GrPoint _p1, final GrPoint _p2, final boolean _rapide) {
    dessineRectangle(_g, _p1, _p2, false, _rapide);
  }

  /**
   * Trace d'un rectangle depuis 2 points oppos�s.
   *
   * @param _rapide Sans action, le rendu est toujours un rectangle.
   * @param _p1 le point min
   * @param _p2 le point max
   * @param _remplissage
   */
  public void dessineRectangle(final Graphics2D _g, final GrPoint _p1, final GrPoint _p2, final boolean _remplissage,
                               final boolean _rapide) {
    if (_p1 != null && _p2 != null) {
      final GrPoint p1 = _p1.applique(versEcran_);
      final GrPoint p2 = _p2.applique(versEcran_);
      final double x = Math.min(p1.x_, p2.x_);
      final double y = Math.min(p1.y_, p2.y_);
      final double w = Math.abs(p2.x_ - p1.x_);
      final double h = Math.abs(p2.y_ - p1.y_);
      tl_.dessineRectangle(_g, x, y, w, h);
    }
  }
}
