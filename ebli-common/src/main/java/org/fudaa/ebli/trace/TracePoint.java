/*
 * @creation     1998-08-26
 * @modification $Date: 2008-03-26 16:30:19 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.trace;

import java.awt.Color;
import java.awt.Graphics2D;
import javax.swing.Icon;
/**
 * Une classe de trace de points.
 * @version $Revision: 1.10.8.1 $ $Date: 2008-03-26 16:30:19 $ by $Author: bmarchan $
 * @author Axel von Arnim
 */
public class TracePoint {

  // constantes publiques
  public final static int RIEN = 0;
  public final static int POINT = 1;
  public final static int PLUS = 2;
  public final static int CROIX = 3;
  public final static int CARRE = 4;
  public final static int CARRE_PLEIN = 5;
  public final static int LOSANGE = 6;
  public final static int CERCLE = 7;
  public final static int DISQUE = 8;
  public final static int ICONE = 9;
  //  private static BPaletteIcone palette=null;
  // donnees membres privees
  private int stylePoint_;
  private int taillePoint_;
  private Color couleur_;

  // Constructeur
  public TracePoint() {
    stylePoint_ = POINT;
    taillePoint_ = 3;
    couleur_ = Color.black;
  }
  
  public TracePoint(int _type, int _taille, Color _color) {
    stylePoint_=_type;
    taillePoint_=_taille;
    couleur_=_color;
  }

  /*public static Icon getIcone(int _typePoint, int _taillePoint, Color _couleur) {
    final int type = _typePoint;
    final int taille = _taillePoint;
    final Color couleur = _couleur;
    Icon icone = new Icon() {

      public int getIconHeight() {
        return taille;
      }

      public int getIconWidth() {
        return taille;
      }

      public void paintIcon(Component c, Graphics g, int x, int y) {
        TracePoint t = new TracePoint();
        t.setTypePoint(type);
        t.setCouleur(couleur);
        t.setTypePoint(taille);
        t.dessinePoint((Graphics2D) g, x, y);
      }
    };
    return icone;
  }

  public static Icon getIcone(int _typePoint, int _taillePoint) {
    final int type = _typePoint;
    final int taille = _taillePoint;
    Icon icone = new Icon() {

      public int getIconHeight() {
        return taille;
      }

      public int getIconWidth() {
        return taille;
      }

      public void paintIcon(Component c, Graphics g, int x, int y) {
        TracePoint t = new TracePoint();
        t.setCouleur(g.getColor());
        t.setTypePoint(type);
        t.setTypePoint(taille);
        t.dessinePoint((Graphics2D )g,x, y);
      }
    };
    return icone;
  }

  public static Icon getIconePoint() {
    Icon icone = new Icon() {

      public int getIconHeight() {
        return 1;
      }

      public int getIconWidth() {
        return 1;
      }

      public void paintIcon(Component c, Graphics g, int x, int y) {
        g.drawLine(x, y, x, y);
      }
    };
    return icone;
  }*/

  //  public static
  // Methodes publiques
  /**
   * Affectation de la propriete <I>typePoint </I>.
   */
  public void setTypePoint(final int _stylePoint) {
    stylePoint_ = _stylePoint;
  }

  /**
   * Accesseur de la propriete <I>typePoint </I>. Elle definit le style de trace de point. Les
   * valeurs possibles sont les champs statiques de cette classe. Par defaut, elle vaut <I>POINT
   * </I>.
   */
  public int getTypePoint() {
    return stylePoint_;
  }

  public void setTaillePoint(final int _taillePoint) {
    taillePoint_ = _taillePoint;
  }

  public int getTaillePoint() {
    return taillePoint_;
  }

  /**
   * Accesseur de la propriete <I>couleur </I>. Couleur des points traces.
   */
  public Color getCouleur() {
    return couleur_;
  }

  /**
   * Affectation de la propriete <I>couleur </I>.
   */
  public void setCouleur(final Color _c) {
    couleur_ = _c;
  }

  private Icon icon_;

  public Icon getIcon() {
    return icon_;
  }

  public void setIcon(final Icon _icon) {
    icon_ = _icon;
  }

  /**
   * Trace un point a la position specifiee. Le point est trace avec le style definit par
   * <I>typePoint </I>.
   * @param _x position x
   * @param _y position y
   * @see #setTypePoint(int)
   */
  public void dessinePoint(final Graphics2D _g, final int _x, final int _y) {
    final Color oldColor = _g.getColor();
    _g.setColor(couleur_);
    switch (stylePoint_) {
    case RIEN:
      break;
    case POINT:
      _g.drawLine(_x, _y, _x, _y);
      break;
    case PLUS:
      _g.drawLine(_x - taillePoint_, _y, _x + taillePoint_, _y);
      _g.drawLine(_x, _y - taillePoint_, _x, _y + taillePoint_);
      break;
    case CROIX:
      _g.drawLine(_x - taillePoint_, _y - taillePoint_, _x + taillePoint_, _y + taillePoint_);
      _g.drawLine(_x + taillePoint_, _y - taillePoint_, _x - taillePoint_, _y + taillePoint_);
      break;
    case CARRE:
      _g.drawRect(_x - taillePoint_, _y - taillePoint_, 2 * taillePoint_ - 1, 2 * taillePoint_ - 1);
      break;
    case CARRE_PLEIN:
      _g.fillRect(_x - taillePoint_, _y - taillePoint_, 2 * taillePoint_ - 1, 2 * taillePoint_ - 1);
      break;
    case LOSANGE:
      _g.drawLine(_x - taillePoint_, _y, _x, _y - taillePoint_);
      _g.drawLine(_x, _y - taillePoint_, _x + taillePoint_, _y);
      _g.drawLine(_x + taillePoint_, _y, _x, _y + taillePoint_);
      _g.drawLine(_x, _y + taillePoint_, _x - taillePoint_, _y);
      break;
    case CERCLE:
      _g.drawArc(_x - taillePoint_, _y - taillePoint_, 2 * taillePoint_ + 1, 2 * taillePoint_ + 1, 0,
          360);
      break;
    case DISQUE:
      _g.fillArc(_x - taillePoint_, _y - taillePoint_, 2 * taillePoint_ + 1, 2 * taillePoint_ + 1, 0,
          360);
      break;
    case ICONE:
      if (icon_ != null) {
        //int wi=icon_.getIconWidth();
        final int hi = icon_.getIconHeight();
        icon_.paintIcon(null, _g, _x, _y - hi);
      }
      break;
    default:
      break;
    }
    _g.setColor(oldColor);
  }

  public void dessinePoint(final Graphics2D _g, final double _x, final double _y) {
    dessinePoint(_g, (int) _x, (int) _y);
  }
}