/*
 * @creation     1998-07-21
 * @modification $Date: 2006-09-19 14:55:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.trace;

import java.awt.Color;
import java.awt.Graphics;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrVecteur;
import org.fudaa.ebli.geometrie.GrVolume;
import org.fudaa.ebli.geometrie.VecteurGrPoint;

/**
 * Un graphics 3D.
 *
 * @version $Revision: 1.9 $ $Date: 2006-09-19 14:55:48 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class Trace3D {
  // donnees membres privees
  private GrPolygone[] facettes_;
  private int[] facIndices_;
  private boolean[] dissimulees_;
  private double[] zbuff_;
  private Color[] couleurs_;
  private final Graphics g_;
  private Color couleur_;
  private final GrVecteur dirLumiere_;

  // Constructeur
  public Trace3D(final Graphics _g) {
    facettes_ = new GrPolygone[0];
    g_ = _g;
    couleur_ = g_.getColor();
    dirLumiere_ = new GrVecteur(0., 0., 1.).normalise();
  }

  // accesseurs
  /**
   * Retourne le <I>Graphics</I> associe a ce trace.
   */
  public Graphics getGraphics() {
    return g_;
  }

  /**
   * Accesseur de la propriete <I>couleur</I> de ce trace. Elle definit la couleur du prochain <I>GrVolume</I> ajoute.
   *
   * @see #ajouteGrVolume(GrVolume)
   */
  public Color getCouleur() {
    return couleur_;
  }

  /**
   * Affectation de la propriete <I>couleur</I>.
   */
  public void setCouleur(final Color _c) {
    couleur_ = _c;
  }

  // ajouteGrVolume
  /**
   * Ajoute un nouveau volume a dessiner. Il aura la couleur definie precedemment par <I>setCouleur</I>.
   *
   * @see #setCouleur(Color)
   */
  public void ajouteGrVolume(final GrVolume _v) {
    final GrPolygone[] facPrec = facettes_;
    final Color[] coulPrec = couleurs_;
    final int taille = _v.nombre() + facPrec.length;
    facettes_ = new GrPolygone[taille];
    couleurs_ = new Color[taille];
    for (int i = 0; i < facPrec.length; i++) {
      facettes_[i] = facPrec[i];
      couleurs_[i] = coulPrec[i];
    }
    for (int i = facPrec.length; i < taille; i++) {
      final GrPolygone pol = _v.polygone(i);
      facettes_[i] = pol;
      couleurs_[i] = couleur_;
    }
  }

  // dessine (flush)
  /**
   * Dessine tous les volumes de ce calque.
   */
  public void dessine() {
    facIndices_ = new int[facettes_.length];
    zbuff_ = new double[facettes_.length];
    dissimulees_ = new boolean[facettes_.length];
    for (int i = 0; i < facettes_.length; i++) {
      facIndices_[i] = i;
      zbuff_[i] = facettes_[i].barycentre().z_;
      // zbuff[i]=minZ(facettes[i]);
      dissimulees_[i] = false;
    }
    classeFacettes(0, facettes_.length - 1);
    trieFacettes();
    for (int i = 0; i < facIndices_.length; i++) {
      if (!dissimulees_[facIndices_[i]]) {
        final VecteurGrPoint projete = facettes_[facIndices_[i]].sommets_;
        final int[] xPoints = new int[projete.nombre()];
        final int[] yPoints = new int[projete.nombre()];
        for (int j = 0; j < projete.nombre(); j++) {
          final GrPoint ptCour = projete.renvoie(j);
          xPoints[j] = (int) ptCour.x_;
          yPoints[j] = (int) ptCour.y_;
        }
        couleur_ = couleurs_[facIndices_[i]];
        double coeffLuminosite = dirLumiere_.produitScalaire(facettes_[facIndices_[i]].normale());
        coeffLuminosite = coeffLuminosite * 2.;
        if (coeffLuminosite < 0.2) {
          coeffLuminosite = 0.2;
        }
        int r = couleur_.getRed();
        int v = couleur_.getGreen();
        int b = couleur_.getBlue();
        r = (int) (coeffLuminosite * (r + 64));
        v = (int) (coeffLuminosite * (v + 64));
        b = (int) (coeffLuminosite * (b + 64));
        if (r < 0) {
          r = 0;
        }
        if (r > 255) {
          r = 255;
        }
        if (v < 0) {
          v = 0;
        }
        if (v > 255) {
          v = 255;
        }
        if (b < 0) {
          b = 0;
        }
        if (b > 255) {
          b = 255;
        }
        g_.setColor(new Color(r, v, b));
        g_.fillPolygon(xPoints, yPoints, xPoints.length);
        g_.setColor(couleur_);
        g_.drawPolygon(xPoints, yPoints, xPoints.length);
      }
    }
  }

  // Methodes privees
  /**
   * Classement des facettes par Z-Buffer.
   */
  private void classeFacettes(final int _gauche, final int _droit) {
    int i, j;
    double z;
    i = _gauche;
    j = _droit;
    z = zbuff_[facIndices_[(_gauche + _droit) / 2]];
    do {
      while (zbuff_[facIndices_[i]] > z && i < _droit) {
        i++;
      }
      while (z > zbuff_[facIndices_[j]] && j > _gauche) {
        j--;
      }
      if (i <= j) {
        final int tmpi = facIndices_[i];
        facIndices_[i] = facIndices_[j];
        facIndices_[j] = tmpi;
        i++;
        j--;
      }
    } while (i <= j);
    if (_gauche < j) {
      classeFacettes(_gauche, j);
    }
    if (i < _droit) {
      classeFacettes(i, _droit);
    }
  }

  /**
   * Elimination des facettes du demi-espace avant l'ecran.
   */
  private void trieFacettes() {
    for (int i = 0; i < facettes_.length; i++) {
      if (zbuff_[facIndices_[i]] < 0) {
        dissimulees_[facIndices_[i]] = true;
      }
    }
  }
  /**
   * Renvoie le plus petit Z d'un polygone. Sert pour appliquer un recalage ou detecter les volumes intersectant
   * l'ecran.
   */
  // private double minZ(GrPolygone fac)
  // {
  // double minz=fac.sommets.renvoie(0).z;
  // double z;
  // if( fac.sommets.nombre()>0 )
  // for(int i=1; i<fac.sommets.nombre(); i++) {
  // z=fac.sommets.renvoie(i).z;
  // if( z<minz ) minz=z;
  // }
  // return minz;
  // }
}
