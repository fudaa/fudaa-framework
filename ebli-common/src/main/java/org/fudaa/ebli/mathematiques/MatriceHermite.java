/*
 * @creation     1999-02-15
 * @modification $Date: 2006-09-19 14:55:56 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.mathematiques;

import java.awt.Point;
import java.awt.geom.Point2D;

/**
 * Mini class matrice pour l'interpolation de Hermite.
 *
 * @version $Revision: 1.8 $ $Date: 2006-09-19 14:55:56 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class MatriceHermite {
  public final static MatriceHermite HERMITE = new MatriceHermite(new double[][] { { 2, -2, 1, 1 }, { -3, 3, -2, -1 },
      { 0, 0, 1, 0 }, { 1, 0, 0, 0 } });
  private double[][] a_;

  public MatriceHermite(final int _ni, final int _nj) {
    a_ = new double[_ni][_nj];
  }

  public MatriceHermite(final double[][] _a) {
    a_ = _a;
  }

  public double a(final int _i, final int _j) {
    return a_[_i][_j];
  }

  public void a(final int _i, final int _j, final double _ia) {
    a_[_i][_j] = _ia;
  }

  public MatriceHermite multiplication(final MatriceHermite _m) {
    final int ni = a_.length;
    final int nj = _m.a_[0].length;
    final MatriceHermite r = new MatriceHermite(ni, nj);
    for (int i = 0; i < ni; i++) {
      for (int j = 0; j < nj; j++) {
        for (int k = 0; k < a_[0].length; k++) {
          r.a_[i][j] += a_[i][k] * _m.a_[k][j];
        }
      }
    }
    return r;
  }

  /**
   * Interpolation de Hermite.
   */
  public static Point hermite(final double _t, final int _x1, final int _y1, final int _x2, final int _y2, final int _vx1, final int _vy1, final int _vx2, final int _vy2) {
    final MatriceHermite h = MatriceHermite.HERMITE;
    final MatriceHermite t = new MatriceHermite(new double[][] { { _t * _t * _t, _t * _t, _t, 1 } });
    final MatriceHermite x = new MatriceHermite(new double[][] { { _x1, _y1, 0, 1 }, { _x2, _y2, 0, 1 },
        { _vx1, _vy1, 0, 0 }, { _vx2, _vy2, 0, 0 } });
    final MatriceHermite q = t.multiplication(h).multiplication(x);
    return new Point((int) q.a(0, 0), (int) q.a(0, 1));
  }

  public static Point2D.Double hermite(final double _t, final double _x1, final double _y1, final double _x2, final double _y2, final double _vx1, final double _vy1,
      final double _vx2, final double _vy2) {
    final MatriceHermite h = MatriceHermite.HERMITE;
    final MatriceHermite t = new MatriceHermite(new double[][] { { _t * _t * _t, _t * _t, _t, 1d } });
    final MatriceHermite x = new MatriceHermite(new double[][] { { _x1, _y1, 0d, 1d }, { _x2, _y2, 0d, 1d },
        { _vx1, _vy1, 0d, 0d }, { _vx2, _vy2, 0d, 0d } });
    final MatriceHermite q = t.multiplication(h).multiplication(x);
    return new Point2D.Double(q.a(0, 0), q.a(0, 1));
  }
}
