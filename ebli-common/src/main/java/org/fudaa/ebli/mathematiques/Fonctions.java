/*
 * @creation     1998-03-25
 * @modification $Date: 2006-04-12 15:28:07 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.mathematiques;
/**
 * Fonctions.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-04-12 15:28:07 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public final class Fonctions {

  private Fonctions(){}

  public static double sqr(final double _d) {
    return _d * _d;
  }
  public static double sign(final double _d) {
    return _d < 0. ? -1. : _d > 0. ? 1. : 0.;
  }
}
