/*
 * @creation     1998-12-15
 * @modification $Date: 2007-05-04 13:49:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.palette;

import com.memoire.bu.*;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;


/**
 * Une palette de palettes ;-).
 * 
 * @version $Revision: 1.2 $ $Date: 2007-05-04 13:49:42 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class BPaletteSelecteurReduitCouleur extends JPanel implements ListSelectionListener, PropertyChangeListener,
    ActionListener {
  private final DefaultListModel model_;
  private final BuEmptyList list_;
  private final BPaletteCouleurSimpleCustomEditor editeur_;
  private final JButton creer_;
  private final JButton detruire_;
  private final JButton appliquer_;

  public BPaletteSelecteurReduitCouleur() {
    super();
    model_ = new DefaultListModel();
    list_ = new BuEmptyList();
    list_.setCellRenderer(new PaletteCouleurCellRenderer());
    list_.setModel(model_);
    list_.setEmptyText("Aucune palette d�finie");
    list_.setFont(new Font("Dialog", Font.PLAIN, 10));
    final BPaletteCouleurSimple[] pals = new BPaletteCouleurSimple[8];
    for (int i = 0; i < pals.length; i++) {
      pals[i] = new BPaletteCouleurSimple();
      model_.addElement(pals[i]);
      pals[i].addPropertyChangeListener(this);
    }
    pals[1].setCycles(5);
    pals[1].setCouleurMax(Color.getHSBColor(0.995f, 0.995f, 0.995f));
    pals[2].setPaliers(10);
    pals[3].setCycles(5);
    pals[3].setPaliers(10);
    pals[3].setCouleurMax(Color.getHSBColor(0.995f, 0.995f, 0.995f));
    pals[4].setEspace(true);
    pals[4].setCouleurMin(Color.black);
    pals[4].setCouleurMax(Color.white);
    pals[5].setEspace(true);
    pals[5].setCouleurMin(Color.black);
    pals[5].setCouleurMax(Color.white);
    pals[5].setPaliers(10);
    pals[6].setEspace(true);
    pals[6].setCouleurMin(Color.red);
    pals[6].setCouleurMax(Color.green);
    pals[7].setEspace(true);
    pals[7].setCouleurMin(Color.blue);
    pals[7].setCouleurMax(Color.red);
    // list_.setSelectedIndex(0);
    list_.setVisibleRowCount(4);
    list_.addListSelectionListener(this);
    list_.setBorder(new EmptyBorder(3, 3, 3, 3));
    final JScrollPane sp = new JScrollPane(list_);
    editeur_ = new BPaletteCouleurSimpleCustomEditor();
    editeur_.setObject(pals[0]);
    editeur_.setBorder(new BuLightBorder(BuLightBorder.LOWERED, 5));
    BuLib.setRecursiveEnabled(editeur_, false);
    creer_ = new BuButton(BuResource.BU.getIcon("creer"));
    creer_.setName("CREER_PALETTE");
    creer_.setToolTipText(BuResource.BU.getString("Cr�er"));
    creer_.setActionCommand("CREER_PALETTE");
    creer_.addActionListener(this);
    detruire_ = new BuButton(BuResource.BU.getIcon("detruire"));
    detruire_.setName("DETRUIRE_PALETTE");
    detruire_.setToolTipText(BuResource.BU.getString("D�truire"));
    detruire_.setActionCommand("DETRUIRE_PALETTE");
    detruire_.addActionListener(this);
    appliquer_ = new BuButton(BuResource.BU.getIcon("appliquer"));
    appliquer_.setName("APPLIQUER_PALETTE");
    appliquer_.setToolTipText(BuResource.BU.getString("Appliquer"));
    appliquer_.setActionCommand("APPLIQUER_PALETTE");
    appliquer_.addActionListener(this);
    final JPanel pb = new JPanel();
    pb.setLayout(new BuHorizontalLayout(5, false, true));
    pb.add(creer_);
    pb.add(detruire_);
    pb.add(appliquer_);
    final BuVerticalLayout lo = new BuVerticalLayout();
    lo.setVgap(5);
    setLayout(lo);
    add(sp);
    add(pb);
    add(editeur_);
  }

  @Override
  public boolean isFocusCycleRoot() {
    return true;
  }

  public void setSelectedPalette(final BPaletteCouleur _s) {
    if (model_.contains(_s)) {
      list_.setSelectedValue(_s, true);
    } else {
      list_.getSelectionModel().clearSelection();
    }
  }

  @Override
  public void valueChanged(final ListSelectionEvent _evt) {
    final Object v = list_.getSelectedValue();
    if (v == null) {
      detruire_.setEnabled(false);
      appliquer_.setEnabled(false);
      creer_.requestFocus();
      BuLib.setRecursiveEnabled(editeur_, false);
    } else {
      detruire_.setEnabled(true);
      appliquer_.setEnabled(true);
      BuLib.setRecursiveEnabled(editeur_, true);
      editeur_.setObject(v);
      firePropertyChange(getPropertyName(), null, v);
    }
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    list_.repaint();
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    final String action = _evt.getActionCommand();
    if ("CREER_PALETTE".equals(action)) {
      final BPaletteCouleurSimple p = new BPaletteCouleurSimple();
      model_.addElement(p);
      p.addPropertyChangeListener(this);
      list_.setSelectedValue(p, true);
    }
    if ("DETRUIRE_PALETTE".equals(action)) {
      final Object v = list_.getSelectedValue();
      if (v != null) {
        model_.removeElement(v);
      }
    }
    if ("APPLIQUER_PALETTE".equals(action)) {
      final Object v = list_.getSelectedValue();
      if (v != null) {
        firePropertyChange(getPropertyName(), null, v);
      }
    }
  }
  // Proprietes
  private String propertyName_ = "paletteCouleur";

  public String getPropertyName() {
    return propertyName_;
  }

  public void setPropertyName(final String _propertyName) {
    propertyName_ = _propertyName;
  }
  // Rendu
  static class PaletteCouleurCellRenderer extends BPaletteCouleurSimple implements ListCellRenderer {
    @Override
    public Component getListCellRendererComponent(final JList _list, final Object _value, final int _index,
                                                  final boolean _isSelected, final boolean _cellHasFocus) {
      final BPaletteCouleurSimple v = (BPaletteCouleurSimple) _value;
      setEspace(v.getEspace());
      setCouleurMin(v.getCouleurMin());
      setCouleurMax(v.getCouleurMax());
      setPaliers(v.getPaliers());
      setCycles(v.getCycles());
      // setFont(list.getFont());
      if (_isSelected) {
        this.setOpaque(true);
        this.setBackground(_list.getSelectionBackground());
        this.setForeground(_list.getSelectionForeground());
        this.setBorder(new BuLightBorder(BuLightBorder.RAISED, 3));
      } else {
        this.setOpaque(false);
        this.setBackground(_list.getBackground());
        this.setForeground(_list.getForeground());
        this.setBorder(new EmptyBorder(3, 3, 3, 3));
      }
      return this;
    }
  }
}
