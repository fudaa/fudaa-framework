/*
 * @creation     1998-06-26
 * @modification $Date: 2006-04-12 15:28:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.palette;
import java.beans.SimpleBeanInfo;
/**
 * BeanInfo de BPaletteCouleurSimple.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-04-12 15:28:02 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class BPaletteCouleurSimpleBeanInfo extends SimpleBeanInfo {}
