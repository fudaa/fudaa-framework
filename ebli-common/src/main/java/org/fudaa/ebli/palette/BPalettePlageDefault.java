/*
 * @creation 27 mai 2005
 * 
 * @modification $Date: 2007-05-04 13:49:42 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import java.awt.Color;
import javax.swing.JComponent;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.trace.BPlageInterface;

/**
 * @author Fred Deniger
 * @version $Id: BPalettePlageDefault.java,v 1.2 2007-05-04 13:49:42 deniger Exp $
 */
public class BPalettePlageDefault implements BPalettePlageInterface {

  public BPlageInterface autre_;
  public CtuluValueEditorI editor_;
  public CtuluNumberFormatI formatter_;
  public boolean isAutreVisible_;
  public BPlageInterface[] pls_;
  public String sep_;
  public String ssTitre_;
  public String subTitleLabel;
  public boolean subTitleVisible;

  @Override
  public String getSubTitleLabel() {
    return subTitleLabel;
  }

  public void setSubTitleLabel(String subTitleTitleLabel) {
    this.subTitleLabel = subTitleTitleLabel;
  }

  @Override
  public boolean isSubTitleVisible() {
    return subTitleVisible;
  }

  @Override
  public void setSubTitleVisible(boolean subTitleTitleVisible) {
    this.subTitleVisible = subTitleTitleVisible;
  }
  public String titre_;
  private boolean logScale;

  public BPalettePlageDefault() {
  }
  boolean reduit;

  @Override
  public JComponent createReduit() {
    return null;
  }

  @Override
  public boolean isReduit() {
    return reduit;
  }

  @Override
  public boolean isLogScale() {
    return logScale;
  }

  @Override
  public void setLogScale(boolean selected) {
    this.logScale = selected;

  }

  @Override
  public boolean isReduitEnable() {
    return true;
  }

  @Override
  public boolean setReduit(final boolean _b) {
    reduit = _b;
    return true;
  }

  @Override
  public void load(final BPalettePlageProperties _prop) {
  }

  /**
   *
   */
  public BPalettePlageDefault(final BPalettePlageInterface _palette) {
    if (_palette != null) {
      isAutreVisible_ = _palette.isAutresVisible();
      editor_ = _palette.getValueEditor();
      sep_ = _palette.getSep();
      ssTitre_ = _palette.getSousTitre();
      titre_ = _palette.getTitre();
      formatter_ = _palette.getFormatter();
      autre_ = _palette.getPlageAutresInterface();
      pls_ = _palette.getPlages();
      logScale = _palette.isLogScale();
      subTitleVisible = _palette.isSubTitleVisible();
      reduit = _palette.isReduit();
    }
    if (sep_ == null) {
      sep_ = EbliLib.getS("�");
    }
  }

  @Override
  public BPlageInterface createPlage(final BPlageInterface _src) {
    return null;
  }

  @Override
  public CtuluNumberFormatI getFormatter() {
    return formatter_;
  }

  @Override
  public int getNbPlages() {
    return pls_ == null ? 0 : pls_.length;
  }

  @Override
  public BPlageInterface getPlageAutresInterface() {
    return autre_;
  }

  @Override
  public void setPlageListener(final BPalettePlageListener _l) {
  }

  @Override
  public BPlageInterface getPlageInterface(final int _idx) {
    return pls_[_idx];
  }

  @Override
  public BPlageInterface[] getPlages() {
    if (pls_ == null) {
      return null;
    }
    final BPlageInterface[] res = new BPlageInterface[pls_.length];
    System.arraycopy(pls_, 0, res, 0, res.length);
    return res;
  }

  @Override
  public String getSep() {
    return sep_;
  }

  @Override
  public String getSousTitre() {
    return ssTitre_;
  }

  @Override
  public String getTitre() {
    return titre_;
  }

  @Override
  public CtuluValueEditorI getValueEditor() {
    return editor_;
  }

  @Override
  public boolean isAutresVisible() {
    return isAutreVisible_;
  }

  /**
   * Si _z est compris entre 0 et 9, une couleur standard (variable statique de Color) est renvoyee. Sinon, une couleur est choisie au hasard entre le
   * rouge et le magenta.
   *
   * @param _z un entier
   * @return une couleur correspondante.
   */
  public static Color getColor(final int _z) {
    if (_z == 0) {
      return Color.black;
    }
    if (_z == 1) {
      return Color.orange;
    }
    if (_z == 2) {
      return Color.green;
    }
    if (_z == 3) {
      return Color.red;
    }
    if (_z == 4) {
      return Color.pink;
    }
    if (_z == 5) {
      return Color.yellow;
    }
    if (_z == 6) {
      return Color.blue;

    }
    if (_z == 7) {
      return Color.darkGray;
    }
    if (_z == 8) {
      return Color.gray;

    }
    if (_z == 9) {
      return Color.cyan;

    }
    return BPalettePlageAbstract.getCouleur(Color.red, Color.magenta, Math.random());
  }
}
