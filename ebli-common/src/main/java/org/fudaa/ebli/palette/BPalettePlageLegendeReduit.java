/*
 * @creation 23 mars 07
 * @modification $Date: 2007-06-13 12:57:46 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JComponent;

/**
 * @author fred deniger
 * @version $Id: BPalettePlageLegendeReduit.java,v 1.3 2007-06-13 12:57:46 deniger Exp $
 */
public class BPalettePlageLegendeReduit extends JComponent {
  BPalettePlage model_;

  public BPalettePlageLegendeReduit() {
    super();
    setPreferredSize(new Dimension(75, 90));
  }

  @Override
  protected void paintComponent(final Graphics _g) {
    if (model_ == null || model_.getNbPlages() == 0) {
      return;
    }
    final Graphics2D g2d = (Graphics2D) _g;
    final int w = 25;
    final int x = 2;
    g2d.setFont(getFont());
    final FontMetrics fm = g2d.getFontMetrics();
    final int y = 2 + fm.getAscent() / 2;
    final int xMax = x + w;
    final int h = getHeight() - 2 * y;
    final int yMax = y + h;
    final double valueMax = model_.getMaxPalette();
    final double valueMin = model_.getMinPalette();
    final double deltaEcran = h;
    final double deltaReel = valueMax - valueMin;
    for (int i = y; i <= yMax; i++) {
      final double v = valueMax - ((i - y) / deltaEcran) * deltaReel;
      final Color c = model_.getNearestPlage(v).getCouleur();
      g2d.setColor(c);
      g2d.drawLine(x, i, xMax, i);
    }
    g2d.setColor(getForeground());

    g2d.drawString(model_.getFormatter().format(model_.getMaxPalette()), xMax + 5, y + fm.getAscent() / 2);
    g2d.drawString(model_.getFormatter().format(model_.getMinPalette()), xMax + 5, yMax);
  }

  public BPalettePlageInterface getModel() {
    return model_;
  }

  public void setModel(final BPalettePlage _model) {
    model_ = _model;
  }

}
