/*
 * @creation     1998-06-25
 * @modification $Date: 2006-09-19 14:55:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.palette;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyEditor;
import javax.swing.JComponent;
import org.fudaa.ctulu.CtuluLibString;

/**
 * Un editeur de palette de couleur simple.
 *
 * @version $Revision: 1.8 $ $Date: 2006-09-19 14:55:51 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class BPaletteCouleurSimplePropertyEditor extends JComponent implements PropertyEditor {
  private BPaletteCouleurSimple objet_;
  private PropertyChangeSupport changeListeners_;

  public BPaletteCouleurSimplePropertyEditor() {
    super();
  }

  @Override
  public void setValue(final Object _value) {
    if ((objet_ != _value) && (_value instanceof BPaletteCouleurSimple)) {
      final Object vp = objet_;
      objet_ = (BPaletteCouleurSimple) _value;
      firePropertyChange(null, vp, objet_);
    }
  }

  @Override
  public Object getValue() {
    return objet_;
  }

  @Override
  public boolean isPaintable() {
    return true;
  }

  @Override
  public void paintValue(final Graphics _gfx, final Rectangle _box) {
    objet_.paint(_gfx);
  }

  @Override
  public String getJavaInitializationString() {
    return "new org.fudaa.ebli.palette.BPaletteCouleurSimple()";
  }

  @Override
  public String getAsText() {
    String r = "";
    final boolean rgb = objet_.getEspace();
    if (rgb) {
      r += "RGB";
    } else {
      r += "TDL";
    }
    final Color cmin = objet_.getCouleurMin();
    final Color cmax = objet_.getCouleurMax();
    final String vir = CtuluLibString.VIR;
    if (rgb) {
      r += vir + cmin.getRed() + vir + cmin.getGreen() + vir + cmin.getBlue();
      r += vir + cmax.getRed() + vir + cmax.getGreen() + vir + cmax.getBlue();
    } else {
      final float[] tdlmin = BPaletteCouleurSimple.tdl(cmin);
      final float[] tdlmax = BPaletteCouleurSimple.tdl(cmax);
      r += vir + tdlmin[0] + vir + tdlmin[1] + vir + tdlmin[2];
      r += vir + tdlmax[0] + vir + tdlmax[1] + vir + tdlmax[2];
    }
    return r + vir + objet_.getPaliers();
  }

  @Override
  public void setAsText(final String _texte) throws IllegalArgumentException {
    throw new IllegalArgumentException();
  }

  @Override
  public String[] getTags() {
    return null;
  }

  @Override
  public Component getCustomEditor() {
    return null;
  }

  @Override
  public boolean supportsCustomEditor() {
    return false;
  }

  @Override
  public void addPropertyChangeListener(final PropertyChangeListener _l) {
    if (changeListeners_ == null) {
      changeListeners_ = new PropertyChangeSupport(this);
    }
    changeListeners_.addPropertyChangeListener(_l);
  }

  @Override
  public void removePropertyChangeListener(final PropertyChangeListener _l) {
    if (changeListeners_ == null) {
      return;
    }
    changeListeners_.removePropertyChangeListener(_l);
  }

  @Override
  protected void firePropertyChange(final String _dsc, final Object _avant, final Object _apres) {
    if (changeListeners_ == null) {
      return;
    }
    changeListeners_.firePropertyChange(_dsc, _avant, _apres);
  }
}
