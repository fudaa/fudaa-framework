/*
 * @creation     1998-09-10
 * @modification $Date: 2006-09-19 14:55:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.palette;
import com.memoire.bu.BuButton;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.JPanel;
import org.fudaa.ebli.trace.TraceSurface;
/**
 * Une palette de TraceSurface.textures.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 14:55:51 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public class BPaletteTexture extends JPanel implements ActionListener {
  // donnees membres publiques
  // donnees membres privees
  //  GridLayout layout_;
  // Constructeurs
  public BPaletteTexture() {
    super();
    //layout_=new GridLayout(5, nb/5+((nb%5)==0?0:1));
    final Icon[] icones= TraceSurface.getIcones();
    final int nb= icones.length;
    setLayout(new GridLayout(5, nb / 5 + ((nb % 5) == 0 ? 0 : 1)));
    for (int i= nb - 1; i >= 0; i--) {
      final BuButton tmp= new BuButton();
      tmp.setToolTipText((String)TraceSurface.TEXTURE_NOM.get(i));
      tmp.setActionCommand("" + i);
      tmp.setIcon(icones[i]);
      tmp.addActionListener(this);
      add(tmp);
    }
  }
  /* for(int i=0; i<boutons_.length; i++) {
    if( TraceSurface.textures[i].equals("") )
      icon=new BuColorIcon(Color.white);
    icon=EbliResource.EBLI.getIcon(TraceSurface.textures[i]);
    if( icon!=null )
      boutons_[i]=new JButton(icon);
    else
      boutons_[i]=new JButton(" ");
    boutons_[i].setBackground(Color.white);
    boutons_[i].setActionCommand(""+i);
    add(boutons_[i]);
    boutons_[i].addActionListener(this);
  } */
  //Dimension d=new Dimension(35,150);
  //setMinimumSize(d);
  //}
  // Methodes publiques
  // **********************************************
  // EVENEMENTS
  // **********************************************
  // ActionPerformed
  @Override
  public void actionPerformed(final ActionEvent _evt) {
    setTypeSurface(Integer.parseInt(_evt.getActionCommand()));
  }
  // **********************************************
  // PROPRIETES EXTERNES
  // **********************************************
  // Propriete typeForme
  private int typeSurface_;
  public int getTypeSurface() {
    return typeSurface_;
  }
  public void setTypeSurface(final int _typeSurface) {
    if (typeSurface_ != _typeSurface) {
      final int vp= typeSurface_;
      typeSurface_= _typeSurface;
      firePropertyChange("TypeSurface", vp, typeSurface_);
    }
  }
}
