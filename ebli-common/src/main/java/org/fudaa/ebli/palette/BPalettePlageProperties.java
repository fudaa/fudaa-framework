/*
 * @creation 15 sept. 2005
 *
 * @modification $Date: 2007-04-30 14:22:22 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.fudaa.ctulu.CtuluNumberFormatBuilder;
import org.fudaa.ebli.trace.BPlageInterface;

/**
 * @author Fred Deniger
 * @version $Id: BPalettePlageProperties.java,v 1.8 2007-04-30 14:22:22 deniger Exp $
 */
@XStreamAlias("Palette-Properties")
public class BPalettePlageProperties {

  @XStreamAlias("SubTitle")
  private String ssTitre_;
  @XStreamAlias("SubTitleVisible")
  private boolean ssTitreVisible = true;
  @XStreamAlias("Title")
  private String titre_;
  @XStreamAlias("Formatter")
  private String formatterPattern_;
  @XStreamAlias("SepCharacters")
  private String sep_;
  @XStreamAlias("Entries")
  private BPlageInterface[] plages_;
  @XStreamAlias("Small")
  private boolean isReduit_;
  @XStreamAlias("Log")
  private boolean logScale;

  protected BPalettePlageProperties() {
  }

  protected BPalettePlageProperties(final BPalettePlageAbstract _plage) {
    ssTitre_ = _plage.getSousTitre();
    ssTitreVisible = _plage.isSubTitleVisible();
    sep_ = _plage.getSep();
    this.logScale = _plage.isLogScale();
    titre_ = _plage.getTitre();
    formatterPattern_ = _plage.getFormatLocalizedPattern();
    plages_ = _plage.getPlages();
    isReduit_ = _plage.isReduit();
  }

  protected BPalettePlageProperties(final BPalettePlageProperties from) {
    if (from != null) {
      ssTitre_ = from.ssTitre_;
      ssTitreVisible = from.ssTitreVisible;
      sep_ = from.sep_;
      this.logScale = from.logScale;
      titre_ = from.titre_;
      formatterPattern_ = from.formatterPattern_;
      if (from.plages_ != null) {
        plages_ = new BPlageInterface[from.plages_.length];
        for (int i = 0; i < plages_.length; i++) {
          plages_[i] = from.plages_[i].copy();
        }
      }
      isReduit_ = from.isReduit_;
    }
  }

  public BPalettePlageProperties copy() {
    return new BPalettePlageProperties(this);
  }


  public void setSsTitre(String ssTitre) {
    this.ssTitre_ = ssTitre;
  }

  protected void updatePalette(final BPalettePlageAbstract _target) {
    if (ssTitre_ != null) {
      _target.setSousTitre(ssTitre_);
    }
    if (titre_ != null) {
      _target.setTitre(titre_);
    }
    if (sep_ != null) {
      _target.setSep(sep_);
    }
    if (formatterPattern_ != null) {
      _target.setFormatter(CtuluNumberFormatBuilder.restoreFromPattern(formatterPattern_));
    }
    if (plages_ != null) {
      _target.setPlages(plages_);
    }
    if (_target.isReduitEnable()) {
      _target.setReduit(isReduit_);
    }
    _target.setLogScale(logScale);
    _target.setSubTitleVisible(ssTitreVisible);

  }
}
