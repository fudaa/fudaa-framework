/*
 GPL 2
 */
package org.fudaa.ebli.palette;

import javax.swing.AbstractListModel;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 *
 * @author Frederic Deniger
 */
public class ReversedListModel extends AbstractListModel implements ListDataListener {

  private final ListModel init;
  int oldSize;

  public ReversedListModel(ListModel init) {
    this.init = init;
    extractOldSize(init);
    init.addListDataListener(this);
  }

  @Override
  public void intervalAdded(ListDataEvent e) {
    extractOldSize(init);
    int min = getReversedIdx(e.getIndex0());
    int max = getReversedIdx(e.getIndex1());
    fireIntervalAdded(e.getSource(), Math.min(min, max), Math.max(min, max));
  }

  @Override
  public void intervalRemoved(ListDataEvent e) {
    if (oldSize == 0) {
      return;
    }
    int min = getReversedIdx(e.getIndex0());
    int max = getReversedIdx(e.getIndex1());
    fireIntervalRemoved(e.getSource(), Math.min(min, max), Math.max(min, max));
    extractOldSize(init);
  }

  @Override
  public void contentsChanged(ListDataEvent e) {
    int min = getReversedIdx(e.getIndex0());
    int max = getReversedIdx(e.getIndex1());
    fireContentsChanged(e.getSource(), Math.min(min, max), Math.max(min, max));
  }

  @Override
  public int getSize() {
    return init.getSize();
  }

  @Override
  public Object getElementAt(int index) {
    return init.getElementAt(getSize() - index - 1);
  }

  protected int getReversedIdx(int index) {
    return Math.min(Math.max(0, oldSize - index - 1), getSize());
  }

  protected void extractOldSize(ListModel init) {
    oldSize = init.getSize();
  }
}
