/**
 *  @creation     24 janv. 2005
 *  @modification $Date: 2006-11-14 09:06:07 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;



/**
 * @author Fred Deniger
 * @version $Id: PaletteTitleComparator.java,v 1.1 2006-11-14 09:06:07 deniger Exp $
 */
public class PaletteTitleComparator implements java.util.Comparator {

  @Override
  public int compare(final Object _a, final Object _b){
    if (_a == _b) {
      return 0;
    }
    if (_a == null) {
      return -1;
    }
    if (_b == null) {
      return 1;
    }
    final int i = (((PaletteCouleur) _a).getTitle()).compareTo(((PaletteCouleur) _b).getTitle());
    if (i == 0) {
      return _a.hashCode() - _b.hashCode();
    }
    return i;
  }
}
