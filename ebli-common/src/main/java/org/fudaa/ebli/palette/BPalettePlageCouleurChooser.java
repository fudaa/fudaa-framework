/*
 *  @creation     25 janv. 2005
 *  @modification $Date: 2006-04-12 15:28:02 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import java.util.List;


/**
 * @author Fred Deniger
 * @version $Id: BPalettePlageCouleurChooser.java,v 1.4 2006-04-12 15:28:02 deniger Exp $
 */
public interface BPalettePlageCouleurChooser {

  /**
   * @param _pls liste de  BPlageAbstract
   */
  void initPaletteCouleurs(List _pls);

}
