/*
 * @creation     1999-02-23
 * @modification $Date: 2006-04-12 15:28:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;
import javax.swing.JComponent;
import org.fudaa.ebli.trace.IPaletteCouleur;
/**
 * Une classe de base pour les palettes de couleur.
 *
 * @version      $Revision: 1.10 $ $Date: 2006-04-12 15:28:02 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public abstract class BPaletteCouleur
  extends JComponent
  implements IPaletteCouleur {}
