/*
 *  @creation     15 d�c. 2004
 *  @modification $Date: 2006-09-19 14:55:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.awt.Color;
import org.fudaa.ebli.trace.BPlageInterface;
import org.fudaa.ebli.trace.TraceIcon;

/**
 * @author Fred Deniger
 * @version $Id: BPlageAbstract.java,v 1.4 2006-09-19 14:55:51 deniger Exp $
 */
public abstract class BPlageAbstract implements BPlageInterface, Comparable {

  @XStreamAlias("Color")
  protected Color couleur_;
  @XStreamAlias("IconSize")
  protected int iconeTaille_ = 5;
  @XStreamAlias("IconType")
  protected int iconeType_ = TraceIcon.CARRE_PLEIN;

  /**
   * @return Returns the couleur.
   */
  @Override
  public Color getCouleur() {
    return couleur_;
  }

  /**
   * @return Returns the iconeTaille.
   */
  @Override
  public int getIconeTaille() {
    return iconeTaille_;
  }

  /**
   * @return Returns the iconeType.
   */
  @Override
  public int getIconeType() {
    return iconeType_;
  }

  /**
   * Modifie si necessaire la couleur et envoie un evt.
   *
   * @param _c la nouvelle taille
   */
  public void setCouleur(final Color _c) {
    if (couleur_ != _c) {
      couleur_ = _c;
    }
  }

  /**
   * Modifie si necessaire la taille et envoie un evt.
   *
   * @param _taille la nouvelle taille
   */
  public void setTailleIcone(final int _taille) {
    if (iconeTaille_ != _taille) {
      iconeTaille_ = _taille;
      // go
    }
  }

  /**
   * Modifie si necessaire le type et envoie un evt.
   *
   * @param _type la nouvelle taille
   */
  public void setTypeIcone(final int _type) {
    if (iconeType_ != _type) {
      iconeType_ = _type;
      // go
    }
  }
}
