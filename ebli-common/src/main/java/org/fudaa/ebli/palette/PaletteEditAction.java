/*
 * @file ActionSelectionPalette.java
 * @creation 13 mai 2004
 * @modification $Date: 2008-02-22 16:23:45 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import javax.swing.Action;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.commun.EbliActionPaletteTreeModel;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Une action affichant une palette pour le choix des couleurs, reagissant au changement de selection de calque.
 * 
 * @author Fred Deniger
 * @version $Id: PaletteEditAction.java,v 1.1.6.1 2008-02-22 16:23:45 bmarchan Exp $
 */
public class PaletteEditAction extends EbliActionPaletteTreeModel {

  public static PaletteEditAction build(final TreeSelectionModel _m) {
    return new PaletteEditAction(_m);
  }

  /**
   * Constructeur par defaut.
   */
  public PaletteEditAction(final TreeSelectionModel _m) {
    super(EbliResource.EBLI.getString("Palette"), EbliResource.EBLI.getIcon("palettecouleur"), "CHOOSE_COLOR_PALET", _m);
    putValue(Action.SHORT_DESCRIPTION, EbliResource.EBLI.getString("Choix de la palette de couleurs"));

  }

  @Override
  public BPalettePanelInterface buildPaletteContent() {
    return new PaletteSelecteurCouleurPlage();
  }

  @Override
  protected Object getTarget(final TreeSelectionModel _m) {
    final TreePath[] p = _m.getSelectionPaths();

    if (p != null) {
      final Object[] targets = new Object[p.length];
      // p est non null
      for (int i = p.length - 1; i >= 0; i--) {
        targets[i] = p[i] == null ? null : p[i].getLastPathComponent();
      }
      return targets;
    }
    return null;
  }

  @Override
  protected boolean isTargetValid(final Object _o) {
    return PaletteSelecteurCouleurPlage.isTargetValid(_o);
  }
}
