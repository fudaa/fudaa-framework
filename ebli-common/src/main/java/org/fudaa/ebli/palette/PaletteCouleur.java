/**
 *  @creation     24 janv. 2005
 *  @modification $Date: 2006-11-14 09:06:04 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import java.util.List;
import org.fudaa.ebli.trace.IPaletteCouleur;


/**
 * @author Fred Deniger
 * @version $Id: PaletteCouleur.java,v 1.1 2006-11-14 09:06:04 deniger Exp $
 */
public interface PaletteCouleur extends IPaletteCouleur {
  
  
  void updatePlages(List _plages);
  
  String getTitle();

}
