/*
 *  @creation     24 janv. 2005
 *  @modification $Date: 2007-05-04 13:49:42 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import org.fudaa.ebli.trace.FiltreDesaturation;
import org.fudaa.ebli.trace.IPaletteCouleur;

/**
 * @author Fred Deniger
 * @version $Id: PaletteCouleurRenderer.java,v 1.2 2007-05-04 13:49:42 deniger Exp $
 */
public class PaletteCouleurRenderer extends JComponent implements ListCellRenderer, Icon {

  IPaletteCouleur palette_;

  final void setPalette(final IPaletteCouleur _palette) {
    palette_ = _palette;
  }

  public PaletteCouleurRenderer() {
    super();
  }

  @Override
  public Component getListCellRendererComponent(final JList _list, final Object _value, final int _index,
                                                final boolean _isSelected, final boolean _cellHasFocus) {
    palette_ = (IPaletteCouleur) _value;
    return this;
  }

  @Override
  public void paintComponent(final Graphics _g) {
    if (palette_ == null) {
      return;
    }
    // super.paintComponent(g);
    final Insets f = getInsets();
    double z;
    final boolean e = isEnabled();
    final int w = getWidth() - f.left - f.right;
    final int h = getHeight() - f.top - f.bottom;
    _g.translate(f.left, f.top);
    for (int x = 0; x < w; x++) {
      z = (double) x / (double) w;
      _g.setColor(e ? palette_.couleur(z) : FiltreDesaturation.desatureCouleur(palette_.couleur(z)));
      _g.drawLine(x, 0, x, h - 1);
    }
    _g.translate(-f.left, -f.top);
  }

  @Override
  public int getIconHeight() {
    return 16;
  }

  @Override
  public int getIconWidth() {
    return 16;
  }

  @Override
  public void paintIcon(final Component _c, final Graphics _g, final int _x, final int _y) {
    if (palette_ == null) {
      return;
    }
    final int w = getIconWidth();
    final int h = getIconHeight();
    final Color old = _g.getColor();
    for (int i = 0; i < w; i++) {
      final double z = (double) i / (double) w;
      _g.setColor(palette_.couleur(z));
      _g.drawLine(_x + i, _y, _x + i, _y + h);
    }
    _g.setColor(old);

  }
}
