/*
 *  @creation     26 janv. 2005
 *  @modification $Date: 2006-09-19 14:55:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import java.awt.event.KeyEvent;
import javax.swing.Action;
import javax.swing.KeyStroke;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * @author Fred Deniger
 * @version $Id: BPaletteInfoAbstractAction.java,v 1.10 2006-09-19 14:55:51 deniger Exp $
 */
public abstract class BPaletteInfoAbstractAction extends EbliActionPaletteAbstract {

  public BPaletteInfoAbstractAction() {
    super(EbliLib.getS("Propriétés"), EbliResource.EBLI.getIcon("info"), "INFOS");
    setPaletteResizable(true);
    putValue(Action.SHORT_DESCRIPTION, EbliLib.getS("Propriétés des objets sélectionnés"));
    putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke('i'));
    putValue(EbliActionInterface.SECOND_KEYSTROKE, KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0));
  }

  protected abstract boolean mustBeUpdated();

  protected BPaletteInfo getPaletteInfo() {
    if (window_ == null) {
      return null;
    }
    return (BPaletteInfo) getPaletteContent();
  }

  public void updateState() {
    if (getPaletteInfo() != null) {
      getPaletteInfo().updateState();
    }
  }

  @Override
  public void updateBeforeShow() {
    if (getPaletteInfo() != null) {
      getPaletteInfo().setAvailable(true);
      if (mustBeUpdated()) {
        ((BPaletteInfo) getPaletteContent()).updateState();
      }
    }
  }

  @Override
  protected void hideWindow() {
    super.hideWindow();
    if (getPaletteInfo() != null) {
      getPaletteInfo().setAvailable(false);
    }
  }
}