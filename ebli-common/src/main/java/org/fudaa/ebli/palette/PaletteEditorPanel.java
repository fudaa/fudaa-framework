/**
 *  @creation     24 janv. 2005
 *  @modification $Date: 2007-06-28 09:26:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextField;
import com.memoire.fu.FuLib;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JColorChooser;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import org.fudaa.ctulu.gui.CtuluCellButtonEditor;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluPopupListener;
import org.fudaa.ctulu.gui.CtuluPopupMenu;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author Fred Deniger
 * @version $Id: PaletteEditorPanel.java,v 1.3 2007-06-28 09:26:47 deniger Exp $
 */
public class PaletteEditorPanel implements ActionListener, CtuluPopupListener.PopupReceiver, ListSelectionListener,
    ChangeListener {

  private static class ColorCellEditor extends CtuluCellButtonEditor {

    public ColorCellEditor() {
      super(null);
      setOpaque(true);
      super.setDoubleClick(true);
    }

    @Override
    protected void doAction() {
      value_ = JColorChooser.showDialog(this, EbliLib.getS("Modifier la couleur"), getBackground());
    }

    @Override
    public void setValue(final Object _o) {
      if (_o == null) {
        return;
      }
      setBackground((Color) _o);
    }

  }

  static class ColorCellRender extends CtuluCellTextRenderer {

    Border bFocused_ = BorderFactory.createLineBorder(Color.BLACK, 2);
    Border bSelected_ = BorderFactory.createLineBorder(Color.BLACK, 1);

    @Override
    public Component getTableCellRendererComponent(final JTable _table, final Object _value, final boolean _isSelected,
        final boolean _hasFocus, final int _row, final int _column) {
      if (_value == null) {
        setOpaque(false);
      } else {
        setOpaque(true);
        setBackground((Color) _value);
      }
      if (_hasFocus) {
        setBorder(bFocused_);
      } else if (_isSelected) {
        setBorder(bSelected_);
      } else {
        setBorder(BORDER_NO_FOCUS);
      }
      return this;
    }
  }

  final class ColorTable extends JTable {

    private final TableCellEditor cellE_ = new ColorCellEditor();
    private final TableCellRenderer cellR_ = new ColorCellRender();

    ColorTable() {
      setModel(model_);
      setColumnSelectionAllowed(true);
      setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
      final TableColumnModel col = getColumnModel();
      for (int i = col.getColumnCount() - 1; i >= 0; i--) {
        col.getColumn(i).setMaxWidth(10);
      }
      setRowHeight(10);
      getSelectionModel().addListSelectionListener(PaletteEditorPanel.this);
      getColumnModel().getSelectionModel().addListSelectionListener(PaletteEditorPanel.this);
      new CtuluPopupListener(PaletteEditorPanel.this, this);
    }

    @Override
    public TableCellEditor getCellEditor(final int _row, final int _column) {
      return cellE_;
    }

    @Override
    public TableCellRenderer getCellRenderer(final int _row, final int _column) {
      return cellR_;
    }

  }

  private JColorChooser ch_;

  private transient JTable table_;

  PaletteManagerTableModel model_;

  BuPanel r_;

  BuTextField tftTitle_;

  String title_;

  public PaletteEditorPanel(final String _title, final Color[] _init) {
    model_ = new PaletteManagerTableModel(_init);
    title_ = _title;
  }

  private void insert() {
    final ListSelectionModel rowModel = table_.getSelectionModel();
    if (rowModel.isSelectionEmpty()) {
      return;
    }
    final int minRow = rowModel.getMinSelectionIndex();
    final int maxRow = rowModel.getMaxSelectionIndex();
    if (minRow != maxRow) {
      return;
    }
    final ListSelectionModel colModel = table_.getColumnModel().getSelectionModel();
    final int minCol = colModel.getMinSelectionIndex();
    final int maxCol = colModel.getMaxSelectionIndex();
    if (minCol != maxCol) {
      return;
    }
    model_.insertColor(minRow, minCol);
  }

  private void remove() {
    final ListSelectionModel colModel = table_.getColumnModel().getSelectionModel();
    final ListSelectionModel rowModel = table_.getSelectionModel();
    final int minRow = rowModel.getMinSelectionIndex();
    final int maxRow = rowModel.getMaxSelectionIndex();
    if (!colModel.isSelectionEmpty()) {
      final List idx = new ArrayList();
      final int max = colModel.getMaxSelectionIndex();
      for (int col = colModel.getMinSelectionIndex(); col <= max; col++) {
        if (colModel.isSelectedIndex(col)) {
          for (int rowIdx = minRow; rowIdx <= maxRow; rowIdx++) {
            if (table_.isCellSelected(rowIdx, col)) {
              final Object o = model_.getValueAt(rowIdx, col);
              if (o != null) {
                idx.add(o);
              }
            }
          }
        }
      }
      if (idx.size() > 0) {
        model_.remove(idx);
      }
    }

  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if ("REMOVE".equals(_e.getActionCommand())) {
      remove();
    } else {
      insert();
    }

  }

  public JPanel buildPanel() {
    if (r_ != null) {
      return r_;
    }
    r_ = new BuPanel();
    r_.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    r_.setLayout(new BuHorizontalLayout(5, true, true));
    final BuPanel top = new BuPanel();
    table_ = new ColorTable();
    final BuPanel pnTop = new BuPanel();
    pnTop.setLayout(new BuBorderLayout(5, 5, true, true));
    tftTitle_ = new BuTextField();
    tftTitle_.setColumns(10);
    tftTitle_.setText(title_);
    pnTop.add(tftTitle_, BuBorderLayout.NORTH);
    final BuScrollPane p = new BuScrollPane(table_);
    p.setPreferredWidth(table_.getPreferredSize().width);
    p.setColumnHeaderView(null);
    pnTop.add(p, BuBorderLayout.CENTER);
    pnTop.setBorder(BorderFactory.createEtchedBorder());
    top.add(pnTop);
    ch_ = new JColorChooser();

    ch_.getSelectionModel().addChangeListener(this);
    r_.add(top);
    r_.add(ch_);
    table_.setColumnSelectionInterval(0, 0);
    table_.setRowSelectionInterval(0, 0);
    valueChanged(null);
    return r_;
  }

  /**
   * @return les couleurs du modele
   */
  public Color[] getColors() {
    final Color[] r = new Color[model_.color_.size()];
    model_.color_.toArray(r);
    return r;
  }

  public String getTitle() {
    final String r = tftTitle_.getText();
    final String rf = FuLib.clean(r);
    if (!r.equals(rf)) {
      tftTitle_.setText(rf);

    }
    return rf;
  }

  @Override
  public void popup(MouseEvent _evt) {
    final CtuluPopupMenu menu = new CtuluPopupMenu();
    menu.addMenuItem("Enlever", "REMOVE", true, this);
    menu.addMenuItem("Ins�rer", "INSERT", true, this);
    menu.show(table_, _evt.getX(), _evt.getY());
  }

  @Override
  public void stateChanged(final ChangeEvent _e) {
    final Color c = ch_.getSelectionModel().getSelectedColor();
    if (c == null) {
      return;
    }
    final ListSelectionModel rowModel = table_.getSelectionModel();
    if (!rowModel.isSelectionEmpty()) {
      final int row = rowModel.getLeadSelectionIndex();
      final int col = table_.getColumnModel().getSelectionModel().getLeadSelectionIndex();
      final Color n = model_.getColor(row, col);
      if (!c.equals(n)) {
        model_.setValueAt(c, row, col);
      }
    }
  }

  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    final ListSelectionModel rowModel = table_.getSelectionModel();
    if (rowModel.isSelectionEmpty()) {
      ch_.setColor(null);
    }
    final int row = rowModel.getLeadSelectionIndex();
    final int col = table_.getColumnModel().getSelectionModel().getLeadSelectionIndex();
    final Color c = model_.getColor(row, col);
    ch_.setColor(c);
  }

}
