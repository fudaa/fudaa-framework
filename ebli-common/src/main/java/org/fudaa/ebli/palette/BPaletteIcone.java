/*
 * @creation     2000-11-09
 * @modification $Date: 2006-04-12 15:28:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.palette;
import javax.swing.Icon;
import javax.swing.JComponent;
/**
 * Une classe de base pour les palettes d'icone.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-04-12 15:28:02 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public abstract class BPaletteIcone extends JComponent {
  /**
   * @param _z valeur entre 0 et 1
   * @return renvoie l'icone correspondant
   */
  public abstract Icon icone(double _z);
}
