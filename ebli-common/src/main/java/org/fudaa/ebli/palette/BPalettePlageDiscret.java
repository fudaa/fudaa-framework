/*
 * @creation 15 d�c. 2004
 * 
 * @modification $Date: 2006-09-19 14:55:51 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import com.memoire.fu.FuLog;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.fudaa.ebli.trace.BPlageInterface;

/**
 * Une palette de valeurs discretes. Cette palette manipule des plages discretes uniquement.
 * 
 * @author Fred Deniger
 * @version $Id: BPalettePlageDiscret.java,v 1.8 2006-09-19 14:55:51 deniger Exp $
 */
public class BPalettePlageDiscret extends BPalettePlageAbstract {

  List plage_;

  /**
   * @param _o les objets a prendre en compte (peut �tre <tt>null</tt>)
   */
  public BPalettePlageDiscret(final Object[] _o) {
    this(_o, Color.BLUE, Color.RED);
  }

  /**
   * @param _o les objets a prendre en compte
   * @param _min la couleur min
   * @param _max la couleur max
   */
  public BPalettePlageDiscret(final Object[] _o, final Color _min, final Color _max) {
    plage_ = new ArrayList(_o == null ? 0 : _o.length);
    if (_o != null) {
      final double taille = _o.length;
      for (int i = 0; i < _o.length; i++) {
        final BPlageDiscret d = new BPlageDiscret(_o[i]);
        d.setCouleur(getCouleur(_min, _max, i / taille));
        plage_.add(d);
      }
    }
  }

  /**
   * Enleve toutes les palettes.
   */
  public final void clear() {
    plage_.clear();
    fireAllPaletteModified();
  }

  @Override
  public BPlageInterface createPlage(final BPlageInterface _src) {
    if (_src == null) {
      return new BPlageDiscret();
    }
    return new BPlageDiscret(_src);
  }

  @Override
  public int getNbPlages() {
    return plage_ == null ? 0 : plage_.size();
  }

  public BPlageDiscret getPlage(final int _i) {
    return (BPlageDiscret) plage_.get(_i);
  }

  @Override
  public BPlageInterface getPlageAutresInterface() {
    return null;
  }

  public BPlageInterface getPlageFor(final Object _o) {
    for (int i = getNbPlages() - 1; i >= 0; i--) {
      if (getPlage(i).getIdentifiant().equals(_o)) {
        return getPlage(i);
      }
    }
    return null;

  }

  @Override
  public BPlageInterface getPlageInterface(final int _idx) {
    return getPlage(_idx);
  }

  @Override
  public boolean isAutresVisible() {
    return false;
  }

  @Override
  public boolean isLogScale() {
    return false;
  }

  @Override
  public void setLogScale(boolean selected) {
  }

  /**
   * Important : M�me si la m�thode supporte des plages abstraites en argument, elle ne sait traiter que des plages
   * discretes.
   * 
   * @param _p les nouvelles plages qui seront copiees.
   */
  @Override
  public final void setPlages(final BPlageInterface[] _p) {
    plage_.clear();
    final int nb = _p.length;
    for (int i = 0; i < nb; i++) {
      final BPlageDiscret p = new BPlageDiscret(_p[i]);
      if (p.getIdentifiant() == null) {
        FuLog.error("EPA: Identifiant is null");
      } else {
        plage_.add(p);
      }
    }
    Collections.sort(plage_);
    fireAllPaletteModified();
  }
}
