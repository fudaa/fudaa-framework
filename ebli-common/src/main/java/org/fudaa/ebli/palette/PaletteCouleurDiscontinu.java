/**
 *  @creation     24 janv. 2005
 *  @modification $Date: 2006-11-14 09:06:05 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import java.awt.Color;
import java.util.List;


/**
 * @author Fred Deniger
 * @version $Id: PaletteCouleurDiscontinu.java,v 1.1 2006-11-14 09:06:05 deniger Exp $
 */
public class PaletteCouleurDiscontinu implements PaletteCouleur {

  private Color[] cs_;
  private String title_;

  final void setTitle(final String _title){
    title_ = _title;
  }
  /**
   * @param _cs
   */
  public PaletteCouleurDiscontinu(final Color[] _cs, final String _title) {
    super();
    cs_ = _cs;
    title_ = _title;
  }
  
  

  @Override
  public String getTitle(){
    return title_;
  }
  final Color[] getCs(){
    return cs_;
  }
  final void setCs(final Color[] _cs){
    cs_=_cs;
  }


  @Override
  public void updatePlages(final List _plages){
    if (cs_ == null || cs_.length == 0) {
      return;
    }
    int max = _plages.size();
    if (max > cs_.length) {
      max = cs_.length;
    }
    for (int i = 0; i < max; i++) {
      ((BPlageAbstract)_plages.get(i)).setCouleur(cs_[i]);
    }
    Color last = cs_[cs_.length - 1].darker();
    for (int i = max; i < _plages.size(); i++) {
      ((BPlageAbstract)_plages.get(i)).setCouleur(last);
      last = last.darker();
    }

  }

  @Override
  public Color couleur(final double _z){
    int i=(int)(_z*cs_.length);
    if(i>=cs_.length) {
      i=cs_.length-1;
    }
    if(i<0) {
      i=0;
    }
    return cs_[i];
  }
}
