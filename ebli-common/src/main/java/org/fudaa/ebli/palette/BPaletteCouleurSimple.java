/*
 * @creation     1998-06-24
 * @modification $Date: 2007-02-02 11:21:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.trace.FiltreDesaturation;

/**
 * Une palette de couleur.
 *
 * @version $Id: BPaletteCouleurSimple.java,v 1.13 2007-02-02 11:21:54 deniger Exp $
 * @author Guillaume Desnoix
 */
public class BPaletteCouleurSimple extends BPaletteCouleur {
  final static public Color TDL_MIN = Color.getHSBColor(0.005f, 0.995f, 0.995f);
  final static public Color TDL_MAX = Color.getHSBColor(0.645f, 0.995f, 0.995f);
  final static public boolean HORIZONTAL = true;
  final static public boolean VERTICAL = false;

  public BPaletteCouleurSimple() {
    super();
    setEspace(false);
    setCouleurMin(TDL_MIN);
    setCouleurMax(TDL_MAX);
    setOrientation(true);
    setPaliers(0);
    setCycles(1);
  }

  @Override
  public void paintComponent(final Graphics _g) {
    // super.paintComponent(g);
    final Insets f = getInsets();
    double z;
    final boolean e = isEnabled();
    final int w = getWidth() - f.left - f.right;
    final int h = getHeight() - f.top - f.bottom;
    _g.translate(f.left, f.top);
    if (getOrientation()) {
      for (int x = 0; x < w; x++) {
        z = (double) x / (double) w;
        _g.setColor(e ? couleur(z) : FiltreDesaturation.desatureCouleur(couleur(z)));
        _g.drawLine(x, 0, x, h - 1);
      }
    } else {
      for (int y = 0; y < h; y++) {
        z = (double) y / (double) h;
        _g.setColor(e ? couleur(1. - z) : FiltreDesaturation.desatureCouleur(couleur(z)));
        _g.drawLine(0, y, w - 1, y);
      }
    }
    _g.translate(-f.left, -f.top);
  }

  static float[] tdl(final Color _c) {
    return Color.RGBtoHSB(_c.getRed(), _c.getGreen(), _c.getBlue(), null);
  }

  @Override
  public Color couleur(final double _c) {
    double z=_c;
    Color c = Color.black;
    if (z < 0.) {
      z = 0.;
    }
    if (z >= 1.) {
      z = 0.99999999;
    }
    z *= cycles_;
    z -= Math.floor(z);
    final double paliers = paliers_;
    if (paliers_ != 0) {
      z = Math.floor(z * paliers) / paliers;
    }
    if (espace_) {
      int n, r, v, b, a;
      int rmin, vmin, bmin, amin;
      int rmax, vmax, bmax, amax;
      n = (int) (z * 255.);
      rmin = couleurMin_.getRed();
      vmin = couleurMin_.getGreen();
      bmin = couleurMin_.getBlue();
      amin = couleurMin_.getAlpha();
      rmax = couleurMax_.getRed();
      vmax = couleurMax_.getGreen();
      bmax = couleurMax_.getBlue();
      amax = couleurMax_.getAlpha();
      r = rmin + (rmax - rmin) * n / 255;
      v = vmin + (vmax - vmin) * n / 255;
      b = bmin + (bmax - bmin) * n / 255;
      a = amin + (amax - amin) * n / 255;
      c = new Color(r, v, b, a);
    } else {
      float t, d, l;
      int n, a, amin, amax;
      float[] tdlmin, tdlmax;
      tdlmin = tdl(couleurMin_);
      tdlmax = tdl(couleurMax_);
      amin = couleurMin_.getAlpha();
      amax = couleurMax_.getAlpha();
      n = (int) (z * 255.);
      a = amin + (amax - amin) * n / 255;
      t = tdlmin[0] + (tdlmax[0] - tdlmin[0]) * (float) z;
      d = tdlmin[1] + (tdlmax[1] - tdlmin[1]) * (float) z;
      l = tdlmin[2] + (tdlmax[2] - tdlmin[2]) * (float) z;
      final int v = Color.getHSBColor(t, d, l).getRGB();
      c = new Color((v & 0x00FFFFFF) | (a << 24), true);
    }
    return c;
  }

  @Override
  public String toString() {
    final Color cmin = getCouleurMin();
    final Color cmax = getCouleurMax();
    final int pi = getPaliers();
    String ps = "";
    if (pi > 0) {
      ps = "" + pi + " paliers ";
    }
    final String vir = CtuluLibString.VIR;
    return "Palette (" + cmin.getRed() + vir + cmin.getGreen() + vir + cmin.getBlue() + ")->(" + cmax.getRed() + vir
        + cmax.getGreen() + vir + cmax.getBlue() + ") " + ps + (getEspace() ? "RVB" : "TDL");
  }
  // Propriete espace
  private boolean espace_;

  public boolean getEspace() {
    return espace_;
  }

  public final void setEspace(final boolean _espace) {
    if (espace_ != _espace) {
      final boolean vp = espace_;
      espace_ = _espace;
      repaint();
      firePropertyChange("espace", vp, espace_);
    }
  }
  // Propriete couleurMin
  private Color couleurMin_;

  public Color getCouleurMin() {
    return couleurMin_;
  }

  public final void setCouleurMin(final Color _couleurMin) {
    if (!_couleurMin.equals(couleurMin_)) {
      final Color vp = couleurMin_;
      couleurMin_ = _couleurMin;
      repaint();
      firePropertyChange("couleurMin", vp, couleurMin_);
    }
  }
  // Propriete orientation
  private boolean orientation_;

  public boolean getOrientation() {
    return orientation_;
  }

  public final void setOrientation(final boolean _orientation) {
    if (orientation_ != _orientation) {
      final boolean vp = orientation_;
      orientation_ = _orientation;
      Dimension d = null;
      if (_orientation) {
        d = new Dimension(128, 16);
      } else {
        d = new Dimension(16, 128);
      }
      setMinimumSize(d);
      setPreferredSize(d);
      revalidate();
      repaint();
      firePropertyChange("orientation", vp, orientation_);
    }
  }
  // Propriete couleurMax
  private Color couleurMax_;

  public Color getCouleurMax() {
    return couleurMax_;
  }

  public final void setCouleurMax(final Color _couleurMax) {
    if (!_couleurMax.equals(couleurMax_)) {
      final Color vp = couleurMax_;
      couleurMax_ = _couleurMax;
      repaint();
      firePropertyChange("couleurMax", vp, couleurMax_);
    }
  }
  // Propriete paliers
  private int paliers_;

  public int getPaliers() {
    return paliers_;
  }

  public final void setPaliers(final int _p) {
    int p=_p;
    if (p < 0) {
      p = 0;
    }
    if (paliers_ != p) {
      final int vp = paliers_;
      paliers_ = p;
      repaint();
      firePropertyChange("paliers", vp, paliers_);
    }
  }
  // Propriete cycles
  private int cycles_;

  public int getCycles() {
    return cycles_;
  }

  public final void setCycles(final int _c) {
    int c=_c;
    if (c < 1) {
      c = 1;
    }
    if (cycles_ != c) {
      final int vp = cycles_;
      cycles_ = c;
      repaint();
      firePropertyChange("cycles", vp, cycles_);
    }
  }
}
