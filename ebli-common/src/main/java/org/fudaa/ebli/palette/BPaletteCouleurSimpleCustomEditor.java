/*
 * @creation     1998-06-24
 * @modification $Date: 2007-05-04 13:49:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.palette;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuGridLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.Customizer;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import org.fudaa.ebli.controle.BSelecteurCouleur;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Un editeur de palette de couleur.
 * 
 * @version $Revision: 1.11 $ $Date: 2007-05-04 13:49:42 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class BPaletteCouleurSimpleCustomEditor extends JComponent implements Customizer, ActionListener,
    PropertyChangeListener {
  private BPaletteCouleurSimple objet_;
  // private PropertyChangeSupport changeListeners;
  private final JComboBox chEspace_;
  private final JTextField tfPaliers_;
  private final JTextField tfCycles_;
  private final BSelecteurCouleur bcSelecteurCouleurMin_;
  private final BSelecteurCouleur bcSelecteurCouleurMax_;

  public BPaletteCouleurSimpleCustomEditor() {
    super();
    // changeListeners=new PropertyChangeSupport(this);
    setLayout(new BuBorderLayout());
    final JPanel pp = new JPanel();
    final BuGridLayout ppl = new BuGridLayout();
    ppl.setColumns(2);
    ppl.setVgap(5);
    ppl.setHgap(5);
    ppl.setCfilled(true);
    ppl.setRfilled(false);
    pp.setLayout(ppl);
    final String s = ":";
    pp.add(new JLabel(EbliResource.EBLI.getString("Espace") + s, SwingConstants.RIGHT), 0);
    chEspace_ = new JComboBox();
    chEspace_.addItem("TDL");
    chEspace_.addItem("RVB");
    chEspace_.addActionListener(this);
    pp.add(chEspace_, 1);
    pp.add(new JLabel(EbliResource.EBLI.getString("Paliers") + s, SwingConstants.RIGHT), 2);
    tfPaliers_ = new JTextField("0");
    tfPaliers_.addActionListener(this);
    pp.add(tfPaliers_, 3);
    pp.add(new JLabel(EbliResource.EBLI.getString("Cycles") + s, SwingConstants.RIGHT), 4);
    tfCycles_ = new JTextField("1");
    tfCycles_.addActionListener(this);
    pp.add(tfCycles_, 5);
    add(pp, BuBorderLayout.NORTH);
    bcSelecteurCouleurMin_ = new BSelecteurCouleur();
    bcSelecteurCouleurMin_.setCouleur(Color.getHSBColor(0.5f, 0.5f, 0.5f));
    bcSelecteurCouleurMin_.addPropertyChangeListener(this);
    bcSelecteurCouleurMax_ = new BSelecteurCouleur();
    bcSelecteurCouleurMax_.setCouleur(Color.getHSBColor(0.5f, 0.5f, 0.5f));
    bcSelecteurCouleurMax_.addPropertyChangeListener(this);
    final JPanel scp = new JPanel();
    final BuGridLayout scl = new BuGridLayout();
    scl.setColumns(2);
    scl.setHgap(2);
    scl.setVgap(2);
    scl.setCfilled(false);
    scl.setRfilled(true);
    scp.setLayout(scl);
    scp.setBorder(new EmptyBorder(2, 2, 2, 2));
    scp.add(new JLabel("Min", SwingConstants.CENTER), 0);
    scp.add(new JLabel("Max", SwingConstants.CENTER), 1);
    scp.add(bcSelecteurCouleurMin_, 2);
    scp.add(bcSelecteurCouleurMax_, 3);
    add(scp, BuBorderLayout.CENTER);
  }

  @Override
  public void setObject(final Object _objet) {
    if (_objet instanceof BPaletteCouleurSimple) {
      objet_ = (BPaletteCouleurSimple) _objet;
      tfPaliers_.setText("" + objet_.getPaliers());
      tfCycles_.setText("" + objet_.getCycles());
      chEspace_.setSelectedItem(objet_.getEspace() ? "RVB" : "TDL");
      bcSelecteurCouleurMin_.setCouleur(objet_.getCouleurMin());
      bcSelecteurCouleurMax_.setCouleur(objet_.getCouleurMax());
    }
  }

  @Override
  public void actionPerformed(final ActionEvent _ev) {
    final Object source = _ev.getSource();
    if (source == chEspace_) {
      final boolean vp = objet_.getEspace();
      final boolean v = ((String) chEspace_.getSelectedItem()).equals("RVB");
      if (v != vp) {
        objet_.setEspace(v);
      }
    }
    if (source == tfPaliers_) {
      final int vp = objet_.getPaliers();
      final int v = Integer.parseInt(tfPaliers_.getText());
      if (v != vp) {
        objet_.setPaliers(v);
      }
    }
    if (source == tfCycles_) {
      final int vp = objet_.getCycles();
      final int v = Integer.parseInt(tfCycles_.getText());
      if (v != vp) {
        objet_.setCycles(v);
      }
    }
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _ev) {
    final Object source = _ev.getSource();
    final String prop = _ev.getPropertyName();
    if ("couleur".equals(prop) && (source == bcSelecteurCouleurMin_)) {
      final Color vp = objet_.getCouleurMin();
      final Color v = (Color) _ev.getNewValue();
      if (!v.equals(vp)) {
        objet_.setCouleurMin(v);
      }
    }
    if ("couleur".equals(prop) && (source == bcSelecteurCouleurMax_)) {
      final Color vp = objet_.getCouleurMax();
      final Color v = (Color) _ev.getNewValue();
      if (!v.equals(vp)) {
        objet_.setCouleurMax(v);
      }
    }
  }
}
