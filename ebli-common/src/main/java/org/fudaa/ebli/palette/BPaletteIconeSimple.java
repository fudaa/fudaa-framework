/*
 * @creation     2000-11-09
 * @modification $Date: 2006-09-19 14:55:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.palette;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.Icon;

/**
 * Une palette d'icone.
 *
 * @version $Revision: 1.11 $ $Date: 2006-09-19 14:55:51 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class BPaletteIconeSimple extends BPaletteIcone {
  final static public boolean HORIZONTAL = true;
  final static public boolean VERTICAL = false;

  public BPaletteIconeSimple() {
    super();
    setBackground(Color.white);
    setOpaque(true);
    setOrientation(true);
  }

  @Override
  public void paintComponent(final Graphics _g) {
    super.paintComponent(_g);
    if (isOpaque()) {
      _g.setColor(getBackground());
      _g.fillRect(0, 0, getWidth(), getHeight());
    }
    final Icon[] icons = getIcones();
    if (icons == null) {
      return;
    }
    final boolean horizontal = getOrientation();
    final double dz = 1. / icons.length;
    int x = 0;
    for (double z = 0.; z < 1.; z += dz) {
      final Icon icon = icone(z);
      if (icon == null) {
        x += 16;
      } else {
        icon.paintIcon(this, _g, (horizontal ? x : 0), (horizontal ? 0 : x));
        x += icon.getIconWidth() + 2;
      }
    }
  }

  @Override
  public Icon icone(final double _z) {
    double z = _z;
    if (z < 0.) {
      z = 0.;
    }
    if (z >= 1.) {
      z = 0.99999999;
    }
    Icon r = null;
    final Icon[] icons = getIcones();
    if (icons != null) {
      r = icons[(int) (z * icons.length)];
    }
    return r;
  }

  @Override
  public String toString() {
    return "PaletteIconeSimple()";
  }
  // Propriete icones
  private Icon[] icones_;

  public Icon[] getIcones() {
    return icones_;
  }

  public void setIcones(final Icon[] _icones) {
    if (icones_ != _icones) {
      final Icon[] vp = icones_;
      icones_ = _icones;
      final Dimension d = computeSize(getOrientation());
      setMinimumSize(d);
      setPreferredSize(d);
      revalidate();
      repaint();
      firePropertyChange("icones", vp, icones_);
    }
  }
  // Propriete orientation
  private boolean orientation_;

  public boolean getOrientation() {
    return orientation_;
  }

  public final void setOrientation(final boolean _orientation) {
    if (orientation_ != _orientation) {
      final boolean vp = orientation_;
      orientation_ = _orientation;
      final Dimension d = computeSize(_orientation);
      setMinimumSize(d);
      setPreferredSize(d);
      revalidate();
      repaint();
      firePropertyChange("orientation", vp, orientation_);
    }
  }

  protected final Dimension computeSize(final boolean _orientation) {
    Dimension r = null;
    final Icon[] icons = getIcones();
    if (icons == null) {
      r = new Dimension(128, 16);
    } else {
      int w = 0;
      int h = 0;
      final double dz = 1. / icons.length;
      for (double z = 0.; z < 1.; z += dz) {
        final Icon icon = icone(z);
        if (icon == null) {
          w += 16;
          h = Math.max(h, 16);
        } else {
          w += icon.getIconWidth() + 2;
          h = Math.max(h, icon.getIconHeight());
        }
      }
      final Insets f = getInsets();
      r = new Dimension(w + f.left + f.right, h + f.top + f.bottom);
    }
    if (!_orientation) {
      final int tmp = r.width;
      r.width = r.height;
      r.height = tmp;
    }
    return r;
  }
}
