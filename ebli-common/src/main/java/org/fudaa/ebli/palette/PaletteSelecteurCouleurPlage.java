/*
 * @creation 13 mai 2004
 * 
 * @modification $Date: 2007-06-05 08:58:38 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.editor.CtuluValueEditorDefaults;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ctulu.gui.*;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.palette.PaletteXmlReaderWriter.PaletteFileFilter;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.BPlageInterface;
import org.fudaa.ebli.trace.TraceIcon;
import org.fudaa.ebli.trace.TraceIsoPlageInterface;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Fred Deniger
 */
public class PaletteSelecteurCouleurPlage extends JPanel implements PropertyChangeListener, ListSelectionListener,
    ActionListener, MouseListener, BPalettePanelInterface, BuBorders {

  public CtuluNumberFormatI getDefaultFormatter() {
    CtuluNumberFormatI ctuluNumberFormatI = default_ == null ? null : default_.getFormatter();
    return ctuluNumberFormatI;
  }

  class PlageTableModel extends AbstractListModel implements TraceIsoPlageInterface {

    public PlageTableModel() {
      plages_ = new ArrayList();
    }

    protected void fireAdd(final int _min, final int _max) {
      fireIntervalAdded(this, _min, _max);
    }

    protected void fireAllSupr(final int _oldSize) {
      if (_oldSize > 0) {
        fireIntervalRemoved(this, 0, _oldSize - 1);
      }
    }

    protected void fireModified() {
      final int i = list_.getSelectedIndex();
      fireContentsChanged(this, i, i);
    }

    protected void fireModified(int _min, int _max) {
      fireContentsChanged(this, _min, _max);
    }

    protected void fireAllModified() {
      fireContentsChanged(this, 0, list_.getModel().getSize() - 1);
    }

    protected void fireModified(final Object _o) {
      final int i = plages_.indexOf(_o);
      if (i >= 0) {
        fireContentsChanged(this, i, i);
      }
    }

    protected void fireRemoved(final int _idx) {
      fireIntervalRemoved(this, _idx, _idx);
    }

    @Override
    public Object getElementAt(final int _index) {
      return plages_.get(_index);
    }

    @Override
    public int getSize() {
      return getRealSize();
    }

    private int getRealSize() {
      return plages_ == null ? 0 : plages_.size();
    }

    public void modifyColors(final PaletteCouleur _palette) {
      if (_palette != null) {
        _palette.updatePlages(plages_);
        plagesUpdated();
      }
    }

    public void plagesUpdated() {
      final boolean enable = plages_.size() > 0;
      btApply_.setEnabled(true);
      btColor_.setEnabled(enable);
      txtMax_.setEnabled(enable);
      txtMin_.setEnabled(enable);

    }

    /**
     * @param _pal la palette qui sert pour initialiser le composant
     */
    public void setPlages(final BPalettePlageInterface _pal) {
      plageEnCours_ = null;
      isAdjusting_ = true;

      if (_pal == null || _pal.getNbPlages() == 0) {
        final int oldSize = plages_.size();
        plages_.clear();
        fireAllSupr(oldSize);
        updatePanel();
      }
      else {
        final int nb = _pal.getNbPlages();
        final int oldSize = plages_.size();
        plages_.clear();
        fireAllSupr(oldSize);
        // for (int i = 0; i < nb; i++) {
        // plages_.add(_pal.getPlageInterface(i).copy());
        // }
        for (int i = nb - 1; i >= 0; i--) {
          plages_.add(_pal.getPlageInterface(i).copy());
        }
        fireAdd(0, nb - 1);
        isAdjusting_ = false;
        list_.setSelectedIndex(0);

      }
      isAdjusting_ = false;

      plagesUpdated();
    }

    @Override
    public int getNbPlages() {
      return getSize();
    }

    @Override
    public BPlageInterface getPlageAutresInterface() {
      return null;
    }

    @Override
    public BPlageInterface getPlageInterface(int idx) {
      return (BPlageInterface) getElementAt(idx);
    }

    @Override
    public boolean isAutresVisible() {
      return false;
    }
  }

  /**
   * @param _o la cible a tester. La cible est une liste de calques.
   * @return true si les calques ont une palette modifiable 
   */
  public static boolean isTargetValid(final Object _o) {
    Object[] layers = (Object[])_o;
    for (Object layer : layers) {
      if (!(layer instanceof BPalettePlageTarget) || !((BPalettePlageTarget) layer).isPaletteModifiable())
        return false;
    }
    
    return true;
  }

  private transient BPalettePlageDefault default_;
  JButton btApply_;
  BuToolButton btAssombrir_;
  JButton btColor_;
  BuToolButton btEcl_;
  BuButton btFormat_;
  BuButton btExport_;
  BuButton btImport_;
  JButton btRefresh_;
  BuCheckBox cbChangedLeg_;
  TraceIcon ic_;
  boolean isAdjusting_;
  BuMenuItem itemRemove_;
  BuMenuItem itemSplit_;
  JLabel lbPalTitle_;
  JCheckBox subTitleVisible;
  JCheckBox cbReduit;
  JList list_;
  CtuluPopupMenu menu_;
  PlageTableModel model_;
  JPanel panelData_;
  BPlageAbstract plageEnCours_;
  List plages_;
  BuScrollPane sp_;
  Object[] layers_;
  JTextField tfPlageLeg_;
  JTextField tfTitlePalette_;
  JComponent txtMax_;
  JComponent txtMin_;
  BuLabel lbMax_;
  BuLabel lbMin_;
  CtuluValueEditorI valueEditor_ = CtuluValueEditorDefaults.DOUBLE_EDITOR;
  protected boolean isDiscreteTarget_;
  private BuPanel btpn;
  final BuPanel pnTitle;

  public PaletteSelecteurCouleurPlage() {
    super();
    setLayout(new BuBorderLayout(2, 2));
    final BuPanel top = new BuPanel(new BuVerticalLayout(2));
    top.setOpaque(false);
    lbPalTitle_ = new BuLabel();
    lbPalTitle_.setHorizontalTextPosition(SwingConstants.CENTER);
    lbPalTitle_.setHorizontalAlignment(SwingConstants.CENTER);
    lbPalTitle_.setFont(BuLib.deriveFont(lbPalTitle_.getFont(), Font.BOLD, 0));
    subTitleVisible = new JCheckBox();
    subTitleVisible.setVisible(false);
    cbReduit = new JCheckBox();
    cbReduit.setOpaque(false);
    cbReduit.setText(EbliLib.getS("L�gende r�duite"));

    tfTitlePalette_ = new BuTextField(10);
    pnTitle = new BuPanel(new BuGridLayout(2));
    pnTitle.setOpaque(false);
    pnTitle.add(new BuLabel(EbliLib.getS("Titre:")));
    pnTitle.add(tfTitlePalette_);
    top.add(lbPalTitle_);
    top.add(pnTitle);
    top.add(subTitleVisible);
    top.add(cbReduit);
    top.setBorder(EMPTY3333);
    add(top, BuBorderLayout.NORTH);
    list_ = new BuEmptyList();
    model_ = new PlageTableModel();
    if (BPalettePlage.isDisplayReversed()) {
      list_.setModel(new ReversedListModel(model_));
    }
    else {
      list_.setModel(model_);
    }
    list_.setCellRenderer(new BPalettePlageLegende.PlageCellRenderer());
    list_.getSelectionModel().addListSelectionListener(this);
    list_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    list_.addMouseListener(this);
    list_.setFocusable(false);
    sp_ = new BuScrollPane(list_);
    sp_.setPreferredWidth(70);
    add(sp_, BuBorderLayout.CENTER);
    final BuPanel east = new BuPanel();
    east.setOpaque(false);
    east.setLayout(new BuBorderLayout(2, 2));
    final BuPanel pnColor = new BuPanel();
    pnColor.setOpaque(false);
    pnColor.setLayout(new BuButtonLayout(1, SwingConstants.LEFT));
    btAssombrir_ = new BuToolButton();
    btAssombrir_.setIcon(EbliResource.EBLI.getIcon("assombrir"));
    btAssombrir_.setToolTipText(EbliLib.getS("Assombrir"));
    btAssombrir_.addActionListener(this);
    btEcl_ = new BuToolButton();
    btEcl_.setIcon(EbliResource.EBLI.getIcon("eclaircir"));
    btEcl_.setToolTipText(EbliLib.getS("Eclaircir"));
    btEcl_.addActionListener(this);
    pnColor.add(btAssombrir_);
    pnColor.add(btEcl_);
    east.add(pnColor, BuBorderLayout.SOUTH);
    updatePanelData(east);
    add(east, BorderLayout.EAST);
    btApply_ = new BuButton(BuResource.BU.getIcon("appliquer"));
    btApply_.setText(BuResource.BU.getString("Appliquer"));
    btApply_.setToolTipText(EbliLib.getS("Appliquer les modifications"));
    btApply_.addActionListener(this);
    btApply_.setEnabled(false);
    btRefresh_ = new BuButton(BuResource.BU.getIcon("rafraichir"));
    btRefresh_.setText(EbliLib.getS("initialiser"));
    btRefresh_.setToolTipText(EbliLib.getS("Initialiser les plages des couleurs"));
    btRefresh_.addActionListener(this);
    btRefresh_.setEnabled(false);
    btFormat_ = new BuButton();
    btFormat_.setText(EbliLib.getS("Format des labels"));
    btFormat_.setToolTipText(EbliLib.getS("Permet de modifier le formattage des nombres et le s�parateur de valeurs"));
    btFormat_.addActionListener(this);
    btFormat_.setEnabled(false);
    btExport_ = new BuButton();
    btExport_.setText(EbliLib.getS("Exporter"));
    btExport_.addActionListener(this);
    btExport_.setToolTipText(EbliLib.getS("Permet d'exporter les seuils et les couleurs de cette palette"));
    btImport_ = new BuButton();
    btImport_.setText(EbliLib.getS("Importer"));
    btImport_.addActionListener(this);
    btImport_.setToolTipText(EbliLib.getS("Permet d'importer les seuils et les couleurs d'une palette"));
    btpn = new BuPanel();
    btpn.setLayout(new FlowLayout(SwingConstants.RIGHT));
    final BuPanel pnImport = new BuPanel();
    pnImport.setOpaque(false);
    btpn.setOpaque(false);
    pnImport.setLayout(new FlowLayout(FlowLayout.LEFT));
    pnImport.add(btExport_);
    pnImport.add(btImport_);

    top.add(pnImport, BorderLayout.NORTH);

    btpn.add(btFormat_);
    btpn.add(btRefresh_);
    btpn.add(btApply_);
    add(btpn, BuBorderLayout.SOUTH);
  }

  public void addButton(JButton bt) {
    btpn.add(bt);
  }

  public JButton getBtApply() {
    return btApply_;
  }

  public JTextField getTfTitlePalette() {
    return tfTitlePalette_;
  }

  public void setTitlePanelVisible(boolean b) {
    pnTitle.setVisible(b);
    lbPalTitle_.setVisible(b);
    subTitleVisible.setVisible(b);
    cbReduit.setVisible(b);
  }

  public void actionApply() {
    savePanel();
    final BPlageInterface[] vp = new BPlageInterface[plages_.size()];
    plages_.toArray(vp);
    if (layers_ != null) {
      default_.pls_ = vp;
      default_.titre_ = tfTitlePalette_.getText();
      if (subTitleVisible.isVisible()) {
        default_.setSubTitleVisible(subTitleVisible.isSelected());
      }
      if (cbReduit.isEnabled()) {
        default_.setReduit(cbReduit.isSelected());
      }

      for (Object layer : layers_) {
        if (layer instanceof BPalettePlageTarget) {
          ((BPalettePlageTarget) layer).setPaletteCouleurPlages(default_);
        }
      }
    }
  }

  /**
   * @param _bt
   */
  private void actionDarken(final Object _bt) {
    final boolean dark = (_bt == btAssombrir_);
    for (int i = plages_.size() - 1; i >= 0; i--) {
      final BPlageAbstract p = (BPlageAbstract) plages_.get(i);
      p.setCouleur(dark ? p.getCouleur().darker() : p.getCouleur().brighter());
    }
    updatePanel();
    model_.fireAllModified();
  }

  private void actionFormat() {
    CtuluNumberFormatI ctuluNumberFormatI = getDefaultFormatter();
    /*
     * CtuluNumberFormatI fmt = getDefaultFormat(); if (fmt == null) { fmt = CtuluLib.DEFAULT_NUMBER_FORMAT; }
     */
    final CtuluDecimalFormatEditPanel fmtSelect = new CtuluDecimalFormatEditPanel(ctuluNumberFormatI);
    fmtSelect.setErrorTextUnable();
    final CtuluDialogPanel pn = new CtuluDialogPanel() {
      @Override
      public boolean isDataValid() {
        return fmtSelect.isDataValid();
      }
    };
    pn.setLayout(new BuVerticalLayout(4));
    final JPanel pnSep = new BuPanel(new BuGridLayout(2, 2, 2));
    pnSep.add(new BuLabel(EbliLib.getS("S�parateur")));
    final BuTextField tf = new BuTextField();
    tf.setText(default_.getSep());
    pnSep.add(tf);
    pn.add(pnSep);

    if (ctuluNumberFormatI == null || ctuluNumberFormatI.isDecimal()) {
      fmtSelect.setBorder(BorderFactory.createTitledBorder(EbliLib.getS("Format d�cimal")));
      pn.add(fmtSelect);
    }
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(this, EbliLib.getS("Format")))) {
      default_.formatter_ = fmtSelect.getCurrentFmt();
      default_.sep_ = tf.getText();
      ajusteAllLegendes();

    }
  }

  protected BPalettePlageDefault getPlageDefault() {
    return default_;
  }

  private void actionRemove() {
    // pour garder la bonne selection
    int i = plages_.indexOf(plageEnCours_);
    plages_.remove(i);
    model_.fireRemoved(i);
    plageEnCours_ = null;
    final int nb = plages_.size();
    if (i >= nb) {
      i = nb - 1;
    }
    if (nb != 0) {
      list_.setSelectedValue(plages_.get(i), false);
      plageEnCours_ = getSelectedPlage();
      updatePanel();
    }
    // setModified();
  }

  private void actionSplit() {
    if (plageEnCours_ == null) {
      return;
    }
    JSpinner spinner = new JSpinner(new SpinnerNumberModel(2, 2, 100, 1));
    CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuHorizontalLayout());
    pn.addLabel(EbliLib.getS("Nombre de plages � cr�er"));
    pn.add(spinner);
    if (!pn.afficheModaleOk(this)) {
      return;
    }
    int nb = ((Number) spinner.getValue()).intValue();
    if (nb < 2) {
      return;
    }
    final double min = plageEnCours_.getMin();
    final double max = plageEnCours_.getMax();
    double step = (max - min) / nb;
    ((BPlage) plageEnCours_).setMax(Double.parseDouble(default_.formatter_.format(min + step)));
    plageEnCours_.ajusteLegendes(default_.formatter_, default_.sep_);
    final int idx = plages_.indexOf(plageEnCours_);
    ((BPlage) plages_.get(idx)).setMax(Double.parseDouble(default_.formatter_.format(min + step)));
    ((BPlage) plages_.get(idx)).ajusteLegendes(default_.formatter_, default_.sep_);
    Color sup = Color.MAGENTA;
    boolean isFirst = idx == 0;
    if (!isFirst) {
      sup = ((BPlage) plages_.get(idx - 1)).getCouleur();
    }
    // au cas ou la couleur est prise
    if (plageEnCours_.getCouleur() == sup) {
      sup = Color.gray;
    }
    Color minColor = plageEnCours_.getCouleur();
    for (int i = 1; i < nb; i++) {
      final BPlage nplage = new BPlage(plageEnCours_);
      double mini = min + i * step;
      double maxi = min + (i + 1) * step;
      if (i == nb - 1) {
        maxi = max;
      }
      nplage.setMinMax(Double.parseDouble(default_.formatter_.format(mini)),
          Double.parseDouble(default_.formatter_.format(maxi)));
      nplage.ajusteLegendes(default_.formatter_, default_.sep_);
      nplage.setCouleur(BPalettePlageAbstract.getCouleur(minColor, sup, (double) i / nb));
      plages_.add(idx, nplage);
    }
    updatePanel();
    model_.fireAdd(idx + 1, idx + nb - 1);
    model_.fireModified(idx, idx);
    // setModified();
  }

  private void reinitPlages() {
    final PaletteRefreshPanel pn = new PaletteRefreshPanel(this);
    final Frame f = CtuluLibSwing.getFrameAncestor(this);
    final CtuluDialog dial = new CtuluDialog(f, pn);
    pn.setOwner(dial);
    dial.setTitle(EbliLib.getS("Initialiser"));
    dial.afficheDialogModal();
  }

  /**
   * @param _pal la palette qui sert pour initialiser le composant
   */
  private void setPlages(final BPalettePlageTarget _layer) {
    BPalettePlageInterface _pal = _layer == null ? null : _layer.getPaletteCouleur();

    model_.setPlages(_pal);
    btApply_.setEnabled(_pal != null);
    btRefresh_.setEnabled(_layer != null && (_layer.isDonneesBoiteAvailable()));
    btColor_.setEnabled(_pal != null);

    if (_layer == null) {
      setTitre(CtuluLibString.EMPTY_STRING);
      cbReduit.setSelected(false);
      cbReduit.setEnabled(false);
    }
    else {
      final String dataDesc = _layer.getDataDescription();
      setTitre(dataDesc == null ? CtuluLibString.EMPTY_STRING : dataDesc);
      cbReduit.setSelected(_layer.getPaletteCouleur() != null && _layer.getPaletteCouleur().isReduit());
      cbReduit.setEnabled(_layer.getPaletteCouleur() != null && _layer.getPaletteCouleur().isReduitEnable());
    }
  }

  /*
   * void setModified(){ btApply_.setEnabled(true); }
   */
  /**
   * Met a jour le panneau des donn�es.
   */
  private void updatePanelData(final BuPanel _dest) {
    // boolean add = false;
    final String suff = ": ";
    if (panelData_ == null) {
      panelData_ = new BuPanel();
      panelData_.setLayout(new BuGridLayout(2));
      // add = true;
    }
    else {
      panelData_.removeAll();
    }
    panelData_.add(new BuLabel(EbliLib.getS("Couleur") + suff));
    btColor_ = new JButton();
    btColor_.addActionListener(this);
    ic_ = new TraceIcon();
    btColor_.setIcon(ic_);
    panelData_.add(btColor_);
    panelData_.add(lbMin_ = new BuLabel(EbliLib.getS("Min") + suff));
    txtMin_ = valueEditor_.createEditorComponent();
    panelData_.add(txtMin_);
    panelData_.add(lbMax_ = new BuLabel(EbliLib.getS("Max") + suff));
    txtMax_ = valueEditor_.createEditorComponent();
    panelData_.add(txtMax_);
    if (!isDiscreteTarget_) {
      if (cbChangedLeg_ == null) {
        cbChangedLeg_ = new BuCheckBox(EbliLib.getS("Label") + suff);
        cbChangedLeg_.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(final ItemEvent _e) {
            if (cbChangedLeg_.isSelected()) {
              if (plageEnCours_ != null && tfPlageLeg_ != null) {
                ajusteSelectedPlageLegende(true);
                tfPlageLeg_.setText(plageEnCours_.getLegende());
              }
            }
            else if (tfPlageLeg_ != null) {
              tfPlageLeg_.setText(CtuluLibString.EMPTY_STRING);
            }
            if (tfPlageLeg_ != null) {
              tfPlageLeg_.setEnabled(cbChangedLeg_.isSelected());
            }

          }
        });

      }
      panelData_.add(cbChangedLeg_);
      tfPlageLeg_ = new BuTextField();
      tfPlageLeg_.setColumns(10);
      tfPlageLeg_.setEnabled(false);
      panelData_.add(tfPlageLeg_);
    }

    if (_dest == null) {
      panelData_.revalidate();
    }
    else {
      _dest.add(panelData_, BuBorderLayout.CENTER);
    }

  }

  BPlageAbstract getMaxPlage() {
    if (list_.getModel().getSize() == 0) {
      return null;
    }
    return (BPlageAbstract) model_.getElementAt(0);
  }

  double[] getMinMax() {
    final double[] r = new double[2];
    if ((plages_ == null) || (plages_.size() == 0)) {
      return null;
    }
    BPlage p = (BPlage) plages_.get(0);
    r[0] = p.getMin();
    r[1] = p.getMax();
    for (int i = plages_.size() - 1; i > 0; i--) {
      p = (BPlage) plages_.get(i);
      double d = p.getMin();
      if (d < r[0]) {
        r[0] = d;
      }
      d = p.getMax();
      if (d > r[1]) {
        r[1] = d;
      }
    }
    return r;
  }

  BPlageAbstract getMinPlage() {
    if (list_.getModel().getSize() == 0) {
      return null;
    }
    return (BPlageAbstract) model_.getElementAt(list_.getModel().getSize() - 1);
  }

  BPlageAbstract getSelectedPlage() {
    return (BPlageAbstract) list_.getSelectedValue();
  }

  void savePanel() {
    boolean modified = ajusteSelectedPlageLegende(false);
    if (plageEnCours_ != null) {
      final BPlageInterface p = (BPlageInterface) plageEnCours_;
      if (cbChangedLeg_ != null && cbChangedLeg_.isSelected()) {
        modified |= p.setLegende(tfPlageLeg_.getText());
        // Fred deja effectue par la premiere ligne
      } /*
         * else { modified |= p.ajusteLegendes(default_.getFormatter(), default_.getSep()); }
         */
    }
    if (modified) {
      model_.fireModified(plageEnCours_);
    }
  }

  boolean useFormatter;

  public boolean isUseFormatter() {
    return useFormatter;
  }

  public void setUseFormatter(boolean useFormatter) {
    this.useFormatter = useFormatter;
  }

  void updatePanel() {
    if (plageEnCours_ == null) {
      ic_.setCouleur(btColor_.getForeground());
      valueEditor_.setValue(CtuluLibString.EMPTY_STRING, txtMax_);
      valueEditor_.setValue(CtuluLibString.EMPTY_STRING, txtMin_);
      tfPlageLeg_.setText(CtuluLibString.EMPTY_STRING);
    }
    else {
      ic_.setCouleur(plageEnCours_.getCouleur());
      ic_.setTaille(plageEnCours_.getIconeTaille());
      ic_.setType(plageEnCours_.getIconeType());
      // final CtuluNumberFormatI fmt = plageEnCours_.getgetDefaultFormat();
      String s = Double.toString(plageEnCours_.getMin());
      if (useFormatter) {
        CtuluNumberFormatI ctuluNumberFormatI = getDefaultFormatter();
        if (ctuluNumberFormatI != null) {
          s = ctuluNumberFormatI.format(plageEnCours_.getMin());
        }
      }
      valueEditor_.setValue(s, txtMin_);
      txtMin_.setToolTipText(s);
      s = Double.toString(plageEnCours_.getMax());
      if (useFormatter) {
        CtuluNumberFormatI ctuluNumberFormatI = getDefaultFormatter();
        if (ctuluNumberFormatI != null) {
          s = ctuluNumberFormatI.format(plageEnCours_.getMax());
        }
      }
      valueEditor_.setValue(s, txtMax_);
      txtMax_.setToolTipText(s);
      tfPlageLeg_.setText(plageEnCours_.getLegende());
    }
    if (cbChangedLeg_ != null && plageEnCours_ != null) {
      cbChangedLeg_.setSelected(plageEnCours_.isLegendCustomized());
    }
    btColor_.repaint();
  }

  protected void ajusteAllLegendes() {
    ajusteAllLegendes(true);
  }

  protected void ajusteAllLegendes(boolean fireEvent) {
    BPalettePlageAbstract.updatePlageLegendes(model_, default_.formatter_, default_.sep_);
    if (fireEvent) {
      model_.fireAllModified();
    }
  }

  protected final boolean ajusteSelectedPlageLegende(final boolean _fireEvent) {
    if (!isDiscreteTarget_ && plageEnCours_ != null) {
      final BPlage p = (BPlage) plageEnCours_;
      if (p != null && (valueEditor_.isValueValidFromComponent(txtMin_))
          && (valueEditor_.isValueValidFromComponent(txtMax_))) {
        
        double nmin = Double.parseDouble(valueEditor_.getStringValue(txtMin_));
        double nmax = Double.parseDouble(valueEditor_.getStringValue(txtMax_));
        
//        if (cbChangedLeg_.isSelected()) {
//          modified |= p.setLegende(tfPlageLeg_.getText());
//        }
//        else {
//          
//        }
        if ((nmin != p.getMin()) || (nmax != p.getMax())) {
          if (nmin > nmax) {
            final double t = nmin;
            nmin = nmax;
            nmax = t;
          }
          if (p.setMinMax(nmin, nmax)) {
            ajusteAllLegendes(false);
            if (_fireEvent) {
              model_.fireModified(plageEnCours_);
            }
            return true;
          }
          return false;

        }
      }
    }
    return false;
  }

  protected void updateDiscreteTarget() {
    txtMax_.setVisible(!isDiscreteTarget_);
    txtMin_.setVisible(!isDiscreteTarget_);
    lbMax_.setVisible(!isDiscreteTarget_);
    lbMin_.setVisible(!isDiscreteTarget_);
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    final Object s = _e.getSource();
    if ((s == btColor_) && (getSelectedPlage() != null)) {
      final Color n = JColorChooser.showDialog(this, EbliResource.EBLI.getString("Couleur"), ic_.getCouleur());
      if ((n != null) && (n != ic_.getCouleur())) {
        // setModified();
        ic_.setCouleur(n);
        plageEnCours_.setCouleur(n);
        model_.fireModified();
      }
    }
    else if (s == btApply_) {
      actionApply();
    }
    else if (s == btRefresh_) {
      reinitPlages();
    }
    else if (s == btExport_) {
      exportPalette();
    }
    else if (s == btImport_) {
      importPalette();
    }
    else if (s == btAssombrir_ || s == btEcl_) {
      actionDarken(s);
    }
    else if (!isDiscreteTarget_ && s == itemRemove_) {
      actionRemove();

    }
    else if (!isDiscreteTarget_ && s == itemSplit_) {
      actionSplit();
    }
    else if (btFormat_ == _e.getSource()) {
      actionFormat();
    }
  }

  public BuButton getBtFormat() {
    return btFormat_;
  }

  private void importPalette() {
    BuFileChooser chooser = new BuFileChooser();
    PaletteFileFilter filter = new PaletteFileFilter();

    chooser.setDialogType(JFileChooser.OPEN_DIALOG);
    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    chooser.setFileFilter(filter);

    if (chooser.showDialog(this, null) == JFileChooser.APPROVE_OPTION) {
      File file = filter.getFileWithExtension(chooser.getSelectedFile());
      PaletteXmlReaderWriter reader = new PaletteXmlReaderWriter();
      CtuluLog analyser = new CtuluLog();

      reader.read(file, this, analyser);
    }
  }

  private void exportPalette() {
    BuFileChooser chooser = new BuFileChooser();
    PaletteFileFilter filter = new PaletteFileFilter();

    chooser.setDialogType(JFileChooser.SAVE_DIALOG);
    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    chooser.setFileFilter(filter);

    if (chooser.showDialog(this, null) == JFileChooser.APPROVE_OPTION) {
      File file = filter.getFileWithExtension(chooser.getSelectedFile());
      PaletteXmlReaderWriter writer = new PaletteXmlReaderWriter();
      CtuluLog analyser = new CtuluLog();

      writer.write(file, this, analyser);
    }
  }

  @Override
  public void doAfterDisplay() {
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void mouseClicked(final MouseEvent _e) {
  }

  @Override
  public void mouseEntered(final MouseEvent _e) {
  }

  @Override
  public void mouseExited(final MouseEvent _e) {
  }

  @Override
  public void mousePressed(final MouseEvent _e) {
  }

  @Override
  public void mouseReleased(final MouseEvent _e) {
    if (!isDiscreteTarget_ && EbliLib.isPopupMouseEvent(_e)) {
      if (menu_ == null) {
        menu_ = new CtuluPopupMenu();
        // pas besoin de listener: le popup recupere l'invoker (this)
        itemSplit_ = menu_.addMenuItem(EbliLib.getS("scinder"), "DECOUPER", BuResource.BU.getIcon("couper"), true);
        itemSplit_.addActionListener(this);
        itemRemove_ = menu_.addMenuItem(EbliLib.getS("enlever"), "DELETE", BuResource.BU.getIcon("enlever"), true);
        itemRemove_.addActionListener(this);
      }
      final boolean enable = !list_.isSelectionEmpty();
      itemSplit_.setEnabled(enable);
      itemRemove_.setEnabled(enable);
      menu_.show(this, _e.getX(), _e.getY());
    }
  }

  @Override
  public void paletteDeactivated() {
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    setPalettePanelTarget(layers_, true);
  }

  /**
   * La target est une liste de calques. Le premier calque selectionn� d�finit la palette qui sera appliqu�e � tous les calques de la liste.
   */
  @Override
  public boolean setPalettePanelTarget(final Object _layers) {
    return setPalettePanelTarget((Object[]) _layers, true);
  }

  public boolean setTargetPalette(final BPalettePlageTarget target) {
    return setPalettePanelTarget(new BPalettePlageTarget[]{target}, true);
  }

  public BPalettePlageTarget getMainTarget() {
    BPalettePlageTarget mainLayer = null;
    if (layers_ != null && layers_.length > 0 && layers_[0] instanceof BPalettePlageTarget) {
      mainLayer = (BPalettePlageTarget) layers_[0];
    }
    
    return mainLayer;
  }
  
  public BPalettePlageTarget[] getTargets() {
    ArrayList<BPalettePlageTarget> targets = new ArrayList<>();
    
    for (Object layer : layers_ != null ? layers_ : new Object[0]) {
      if (layer instanceof BPalettePlageTarget) {
        targets.add((BPalettePlageTarget) layer);
      }
    }

    return targets.toArray(new BPalettePlageTarget[0]);
  }
  
  /**
   * La palette s'applique � une liste de calques.
   * 
   * @param _layers le noveau modele
   * @return true si la cible est prise en compte
   */
  private boolean setPalettePanelTarget(Object[] _layers, final boolean _forceUpdate) {
    layers_ = _layers;
    
//    if (_forceUpdate || _layers != target_) {
    // Suppression des listeners
    for (Object layer : _layers != null ? _layers : new Object[0]) {
      if (layer instanceof BPalettePlageTarget) {
        ((BPalettePlageTarget) layer).removePropertyChangeListener("paletteCouleur", this);
      }
    }
    
    // Le calque principal, celui utilis� pour afficher la palette (le premier de la selection).
    BPalettePlageTarget mainLayer = getMainTarget();


//      if (_layers != null && !_layers.isPaletteModifiable()) {
//        layers_ = null;
//      }

    if (mainLayer == null) {
      setPlages(null);
      setTitre(CtuluLibString.EMPTY_STRING);
      cbReduit.setSelected(false);
      cbReduit.setEnabled(false);
      tfTitlePalette_.setText(CtuluLibString.EMPTY_STRING);
      btFormat_.setEnabled(false);
    }

    else {
      final CtuluValueEditorI old = valueEditor_;
      valueEditor_ = CtuluValueEditorDefaults.DOUBLE_EDITOR;
      default_ = new BPalettePlageDefault(mainLayer.getPaletteCouleur());
      if (mainLayer.getPaletteCouleur() == null) {
        tfTitlePalette_.setText(mainLayer.getDataDescription());
      }
      else {
        valueEditor_ = mainLayer.getPaletteCouleur().getValueEditor();
        tfTitlePalette_.setText(mainLayer.getPaletteCouleur().getTitre());
      }
      if (valueEditor_ != old) {
        updatePanelData(null);
      }
      isDiscreteTarget_ = mainLayer.isDiscrete();
      if (!isDiscreteTarget_) {
        btFormat_.setEnabled(true);
      }
      setPlages(mainLayer);
      final String dataDesc = mainLayer.getDataDescription();
      setTitre(
          mainLayer.getTitle() + dataDesc == null ? CtuluLibString.EMPTY_STRING : (CtuluLibString.ESPACE + dataDesc));
      cbReduit.setSelected(mainLayer.getPaletteCouleur() != null && mainLayer.getPaletteCouleur().isReduit());
      cbReduit.setEnabled(mainLayer.getPaletteCouleur() != null && mainLayer.getPaletteCouleur().isReduitEnable());
      String subTitleLabel = null;
      if (mainLayer.getPaletteCouleur() != null) {
        subTitleLabel = mainLayer.getPaletteCouleur().getSubTitleLabel();
      }
      if (subTitleLabel != null) {
        subTitleVisible.setVisible(true);
        subTitleVisible.setText(EbliLib.getS("Afficher") + ": " + subTitleLabel);
        subTitleVisible.setSelected(mainLayer.getPaletteCouleur().isSubTitleVisible());
      }
      else {
        subTitleVisible.setVisible(false);
      }
    }
    updateDiscreteTarget();

    for (Object layer : _layers != null ? _layers : new Object[0]) {
      if (layer instanceof BPalettePlageTarget) {
        ((BPalettePlageTarget) layer).addPropertyChangeListener("paletteCouleur", this);
      }
    }

    return layers_ != null;

  }

  /**
   * @param _t le nouveau titre
   */
  public void setTitre(final String _t) {
    lbPalTitle_.setText(_t);
  }

  @Override
  public void valueChanged(final ListSelectionEvent _e) {

    if (!isAdjusting_ && !_e.getValueIsAdjusting()) {
      savePanel();
      final BPlageAbstract old = plageEnCours_;
      plageEnCours_ = getSelectedPlage();
      if (plageEnCours_ != old) {
        updatePanel();
      }
    }
  }
}
