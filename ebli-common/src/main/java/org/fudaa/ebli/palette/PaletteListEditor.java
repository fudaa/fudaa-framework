/**
 *  @creation     24 janv. 2005
 *  @modification $Date: 2006-11-14 09:06:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuButtonLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import javax.swing.JFileChooser;
import javax.swing.SwingConstants;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFavoriteFiles;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author Fred Deniger
 * @version $Id: PaletteListEditor.java,v 1.1 2006-11-14 09:06:17 deniger Exp $
 */
public class PaletteListEditor extends CtuluDialogPanel implements ActionListener {

  class PaletteListEditorModel extends CtuluListEditorModel {

    PaletteListEditorModel() {
      super(new ArrayList(Arrays.asList(PaletteManager.INSTANCE.getAllPaletteCopy())), true);
    }

    protected boolean isTitleUsed(final String _t) {
      for (int i = v_.size() - 1; i >= 0; i--) {
        final PaletteCouleurDiscontinu vi = (PaletteCouleurDiscontinu) v_.get(i);
        if (vi.getTitle().equals(_t)) {
          return true;
        }
      }
      return false;
    }

    @Override
    public boolean canRemove() {
      return super.getRowCount() > 1;
    }

    @Override
    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      return false;
    }

    PaletteCouleurDiscontinu[] getPalettes() {
      final PaletteCouleurDiscontinu[] r = new PaletteCouleurDiscontinu[v_.size()];
      v_.toArray(r);
      return r;
    }

    @Override
    public Object createNewObject() {
      final PaletteEditorPanel panel = new PaletteEditorPanel(EbliLib.getS("palette") + CtuluLibString.ESPACE
          + (v_.size() + 1), null);
      final CtuluDialogPanel pn = new CtuluDialogPanel() {

        @Override
        public boolean isDataValid() {

          if (panel.getColors().length == 0) {
            setErrorText(EbliLib.getS("D�finir au moins une couleur"));
            return false;
          }
          final String newTitle = panel.getTitle();
          if (isTitleUsed(newTitle)) {
            setErrorText(EbliLib.getS("Nom d�j� utilis�"));
            return false;
          }

          return true;
        }
      };
      pn.add(panel.buildPanel());
      if (CtuluDialogPanel.isOkResponse(pn.afficheModale(PaletteListEditor.this))) {
        // mise a jour de la plage
        return new PaletteCouleurDiscontinu(panel.getColors(), panel.getTitle());

      }
      return null;
    }

    @Override
    public void edit(final int _r) {
      final PaletteCouleurDiscontinu vi = (PaletteCouleurDiscontinu) v_.get(_r);
      final PaletteEditorPanel panel = new PaletteEditorPanel(vi.getTitle(), vi.getCs());
      final CtuluDialogPanel pn = new CtuluDialogPanel() {

        @Override
        public boolean isDataValid() {

          if (panel.getColors().length == 0) {
            setErrorText(EbliLib.getS("D�finir au moins une couleur"));
            return false;
          }
          final String newTitle = panel.getTitle();
          if (!newTitle.equals(vi.getTitle()) && isTitleUsed(newTitle)) {
            setErrorText(EbliLib.getS("Nom d�j� utilis�"));
            return false;
          }

          return true;
        }
      };
      pn.add(panel.buildPanel());
      if (CtuluDialogPanel.isOkResponse(pn.afficheModale(PaletteListEditor.this))) {
        vi.setCs(panel.getColors());
        vi.setTitle(panel.getTitle());
        fireTableCellUpdated(_r, 1);

      }
    }

  }

  PaletteListEditorModel model_;

  PaletteListEditor() {
    model_ = new PaletteListEditorModel();
    final CtuluListEditorPanel ed = new CtuluListEditorPanel(model_, true, true, true, true, true);
    ed.setValueListCellRenderer(new PaletteCouleurRendererTitle());
    setLayout(new BuBorderLayout());
    add(ed, BuBorderLayout.CENTER);

    final BuPanel btPanel = new BuPanel();
    btPanel.setLayout(new BuButtonLayout(5, SwingConstants.CENTER));
    BuButton bt = new BuButton();
    bt.setIcon(BuResource.BU.getIcon("exporter"));
    bt.setText(EbliLib.getS("Exporter toutes les palettes"));
    bt.setActionCommand("EXPORT_PALETTES");
    bt.addActionListener(this);
    btPanel.add(bt);
    bt = new BuButton();
    bt.setIcon(BuResource.BU.getIcon("importer"));
    bt.setText(EbliLib.getS("Importer des palettes"));
    bt.setActionCommand("IMPORT_PALETTES");
    bt.addActionListener(this);
    btPanel.add(bt);
    add(btPanel, BuBorderLayout.SOUTH);
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    if ("EXPORT_PALETTES".equals(_evt.getActionCommand())) {
      export();
    } else if ("IMPORT_PALETTES".equals(_evt.getActionCommand())) {
      importer();
    }
  }

  void export() {
    final CtuluFileChooser fileChooser = new CtuluFileChooser(false);
    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fileChooser.setSelectedFile(new File(CtuluFavoriteFiles.getLastDir(), "palette-colors.txt"));
    fileChooser.setFileHidingEnabled(true);
    fileChooser.setMultiSelectionEnabled(false);
    if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
      final File f = fileChooser.getSelectedFile();
      final Properties p = PaletteManager.INSTANCE.exportPalette();
      FileOutputStream fileOutputStream =null;
      try {
        fileOutputStream = new FileOutputStream(f);
        p.store(fileOutputStream, CtuluLibString.EMPTY_STRING);
      } catch (final FileNotFoundException _e) {
        FuLog.error(_e);
      } catch (final IOException _e) {
        FuLog.error(_e);
      }
      finally{
        if(fileOutputStream!=null) {
          try {
            fileOutputStream.close();
          } catch (final IOException _evt) {
            FuLog.error(_evt);
            
          }
        }
      }
    }
  }

  void importer() {
    final CtuluFileChooser fileChooser = new CtuluFileChooser(true);
    fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    fileChooser.setFileHidingEnabled(true);
    fileChooser.setMultiSelectionEnabled(false);
    if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
      final Properties p = new Properties();
      FileInputStream fileInputStream =null;
      try {
        fileInputStream = new FileInputStream(fileChooser.getSelectedFile());
        p.load(fileInputStream);
        if (!p.isEmpty()) {
          final Map newProp = PaletteManager.INSTANCE.loadPaletteFromExtFile(p);
          for (final Iterator it = newProp.values().iterator(); it.hasNext();) {
            final PaletteCouleurDiscontinu d = (PaletteCouleurDiscontinu) it.next();
            if (model_.isTitleUsed(d.getTitle())) {
              String newName = FuLib.clean("import_" + d.getTitle());
              if (model_.isTitleUsed(newName)) {
                final int max = 21;
                int idx = 1;
                newName = newName + "_" + idx;
                while (model_.isTitleUsed(newName) && (idx < max)) {
                  idx++;
                  newName = newName + "_" + idx;
                }
                if (!model_.isTitleUsed(newName)) {
                  d.setTitle(newName);
                  model_.addElement(d);
                }
              } else {
                d.setTitle(newName);
                model_.addElement(d);
              }
            } else {
              model_.addElement(d);
            }
          }
        }
      } catch (final FileNotFoundException _e) {
        FuLog.error(_e);
      } catch (final IOException _e) {
        FuLog.error(_e);
      }
      finally{
        if(fileInputStream!=null) {
          try {
            fileInputStream.close();
          } catch (final IOException _evt) {
            FuLog.error(_evt);
            
          }
        }
      }
    }

  }

  @Override
  public boolean apply() {
    PaletteManager.INSTANCE.setPalette(model_.getPalettes());
    PaletteManager.INSTANCE.savePref();
    return true;
  }

}
