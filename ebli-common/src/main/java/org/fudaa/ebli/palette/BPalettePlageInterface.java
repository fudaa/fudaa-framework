/*
 *  @creation     13 mai 2004
 *  @modification $Date: 2007-03-30 15:36:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import javax.swing.JComponent;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ebli.trace.BPlageInterface;
import org.fudaa.ebli.trace.TraceIsoPlageInterface;

/**
 * @author Fred Deniger
 * @version $Id: BPalettePlageInterface.java,v 1.3 2007-03-30 15:36:28 deniger Exp $
 */
public interface BPalettePlageInterface extends TraceIsoPlageInterface{


  /**
   * @return le sÚparateur a utiliser
   */
  String getSep();

  void setPlageListener(BPalettePlageListener _l);

  /**
   * @return les plages dans l'ordre
   */
  BPlageInterface[] getPlages();
  
  boolean isReduitEnable();
  boolean isReduit();
  
  boolean setReduit(boolean _b);
  
  JComponent createReduit();
  
  boolean isLogScale();
  void setLogScale(boolean selected);


  CtuluValueEditorI getValueEditor();

  /**
   * @param _src la source a copier
   * @return la nouvelle plage
   */
  BPlageInterface createPlage(BPlageInterface _src);

  /**
   * @return le formatter pour les legendes des plages
   */
  CtuluNumberFormatI getFormatter();


  /**
   * Retourne le sous titre de la palette.
   * 
   * @return Le sous titre.
   */
  String getSousTitre();

  /**
   * Retourne le titre de la palette.
   * 
   * @return Le titre.
   */
  String getTitre();


  void load(final BPalettePlageProperties _prop);

  /**
   * @return a label used to display configuration dialog about subtitle.
   */
  String getSubTitleLabel();

  boolean isSubTitleVisible();
  void setSubTitleVisible(boolean visible);
}