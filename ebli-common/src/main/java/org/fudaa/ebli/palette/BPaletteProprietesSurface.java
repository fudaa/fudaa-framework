/*
 * @creation     1998-01-13
 * @modification $Date: 2007-05-04 13:49:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.palette;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Une palette de fontes.
 * 
 * @version $Id: BPaletteProprietesSurface.java,v 1.10 2007-05-04 13:49:42 deniger Exp $
 * @author Guillaume Desnoix , Axel von Arnim
 */
public class BPaletteProprietesSurface extends JComponent implements ActionListener {
  private final JCheckBox cbContour_;
  private final JCheckBox cbSurface_;
  private final JCheckBox cbIsolignes_;
  private final JCheckBox cbIsosurfaces_;
  private final JCheckBox cbPaletteLocale_;
  private final JCheckBox cbBornes_;
  private final BuTextField tfEcart_;
  private final BuTextField tfMinV_;
  private final BuTextField tfMaxV_;

  public BPaletteProprietesSurface() {
    super();
    cbContour_ = new JCheckBox(EbliResource.EBLI.getString("Contour"));
    cbContour_.setSelected(false);
    cbContour_.addActionListener(this);
    cbSurface_ = new JCheckBox(EbliResource.EBLI.getString("Surface"));
    cbSurface_.setSelected(false);
    cbSurface_.addActionListener(this);
    cbIsolignes_ = new JCheckBox(EbliResource.EBLI.getString("Isolignes"));
    cbIsolignes_.setSelected(true);
    cbIsolignes_.addActionListener(this);
    cbIsosurfaces_ = new JCheckBox(EbliResource.EBLI.getString("Isosurfaces"));
    cbIsosurfaces_.setSelected(true);
    cbIsosurfaces_.addActionListener(this);
    tfEcart_ = BuTextField.createDoubleField();
    tfEcart_.addActionListener(this);
    cbBornes_ = new JCheckBox();
    cbBornes_.setSelected(false);
    cbBornes_.addActionListener(this);
    tfMinV_ = BuTextField.createDoubleField();
    tfMinV_.setEnabled(false);
    tfMinV_.addActionListener(this);
    tfMaxV_ = BuTextField.createDoubleField();
    tfMaxV_.setEnabled(false);
    tfMaxV_.addActionListener(this);
    cbPaletteLocale_ = new JCheckBox(EbliResource.EBLI.getString("Palette locale"));
    cbPaletteLocale_.setSelected(false);
    cbPaletteLocale_.setEnabled(false);
    cbPaletteLocale_.addActionListener(this);
    final BuGridLayout lo = new BuGridLayout();
    lo.setColumns(2);
    lo.setHgap(5);
    lo.setVgap(5);
    setLayout(lo);
    add(cbContour_);
    add(cbIsolignes_);
    add(cbSurface_);
    add(cbIsosurfaces_);
    final String s = ":";
    add(new JLabel(EbliResource.EBLI.getString("Ecart") + s, SwingConstants.RIGHT));
    add(tfEcart_);
    add(new JLabel(EbliResource.EBLI.getString("Bornes") + s, SwingConstants.RIGHT));
    add(cbBornes_);
    add(new JLabel(EbliResource.EBLI.getString("Valeur min") + s, SwingConstants.RIGHT));
    add(tfMinV_);
    add(new JLabel(EbliResource.EBLI.getString("Valeur max" + s), SwingConstants.RIGHT));
    add(tfMaxV_);
    add(cbPaletteLocale_);
    setBorder(new EmptyBorder(3, 3, 3, 3));
  }

  @Override
  public boolean isFocusCycleRoot() {
    return true;
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    final JComponent source = (JComponent) _evt.getSource();
    valide(source);
  }

  public void valide(final JComponent _source) {
    if (_source == cbContour_) {
      firePropertyChange("contour", !cbContour_.isSelected(), cbContour_.isSelected());
    }
    if (_source == cbSurface_) {
      firePropertyChange("surface", !cbSurface_.isSelected(), cbSurface_.isSelected());
    }
    if (_source == cbIsolignes_) {
      firePropertyChange("isolignes", !cbIsolignes_.isSelected(), cbIsolignes_.isSelected());
    }
    if (_source == cbIsosurfaces_) {
      firePropertyChange("isosurfaces", !cbIsosurfaces_.isSelected(), cbIsosurfaces_.isSelected());
    }
    if (_source == tfEcart_) {
      final Double v = (Double) tfEcart_.getValue();
      if (v == null) {
        return;
      }
      firePropertyChange("ecart", 0., v.doubleValue());
    }
    if (_source == cbBornes_) {
      tfMinV_.setValue(null);
      tfMinV_.setEnabled(cbBornes_.isSelected());
      tfMaxV_.setValue(null);
      tfMaxV_.setEnabled(cbBornes_.isSelected());
      if (!cbBornes_.isSelected()) {
        firePropertyChange("minValeur", 0., Double.NaN);
        firePropertyChange("maxValeur", 0., Double.NaN);
      }
    }
    if (_source == tfMinV_) {
      final Double v = (Double) tfMinV_.getValue();
      double dv = 0.;
      if (v == null) {
        dv = Double.NaN;
      } else {
        dv = v.doubleValue();
      }
      firePropertyChange("minValeur", 0., dv);
    }
    if (_source == tfMaxV_) {
      final Double v = (Double) tfMaxV_.getValue();
      double dv = 0.;
      if (v == null) {
        dv = Double.NaN;
      } else {
        dv = v.doubleValue();
      }
      firePropertyChange("maxValeur", 0., dv);
    }
    if (_source == cbPaletteLocale_) {
      firePropertyChange("paletteLocale", !cbPaletteLocale_.isSelected(), cbPaletteLocale_.isSelected());
    }
  }
}
