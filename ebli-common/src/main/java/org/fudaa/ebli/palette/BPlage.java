/*
 * @creation 13 mai 2004
 * 
 * @modification $Date: 2007-04-30 14:22:22 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.text.NumberFormat;
import java.util.Locale;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ebli.trace.BPlageInterface;

/**
 * Un classe représentant une plage.
 */
@XStreamAlias("Range")
public class BPlage extends BPlageAbstract {

  public final static CtuluNumberFormatDefault PLAGE_NUMBER_FORMAT;

  static {
    NumberFormat fmt = NumberFormat.getInstance(Locale.US);
    fmt.setMaximumFractionDigits(2);
    fmt.setGroupingUsed(false);
    fmt.setMinimumFractionDigits(2);
    PLAGE_NUMBER_FORMAT = new CtuluNumberFormatDefault(fmt);
  }

  public static String getText(double val, final CtuluNumberFormatI _formatter) {
    if (val == Double.POSITIVE_INFINITY) {
      return "+" + CtuluLibString.getInfiniSymbol();
    } else if (val == Double.NEGATIVE_INFINITY) {
      return "-" + CtuluLibString.getInfiniSymbol();
    }
    CtuluNumberFormatI fmt = _formatter;
    if (_formatter == null) {
      fmt = PLAGE_NUMBER_FORMAT;
    }
    return fmt.format(val);
  }
  @XStreamAlias("Legend")
  protected String legende_;
  @XStreamAlias("Max")
  private double max_; // Maximum des valeurs de la
  @XStreamAlias("MaxAsString")
  String maxText;
  // plage
  @XStreamAlias("Min")
  double min_; // Minimum des valeurs de la
  @XStreamAlias("MinAsText")
  String minText;
  @XStreamAlias("Separator-Characters")
  String sep;
  @XStreamAlias("Customized")
  private boolean isLegendCustomized;

  public BPlage() {
  }

  /**
   * @param _r la plage a copier
   */
  public BPlage(final BPlageInterface _r) {
    if (_r != null) {
      couleur_ = _r.getCouleur();
      max_ = _r.getMax();
      min_ = _r.getMin();
      maxText = _r.getMaxString();
      minText = _r.getMinString();
      sep = _r.getSep();
      legende_ = _r.getLegende();
      iconeTaille_ = _r.getIconeTaille();
      isLegendCustomized = _r.isLegendCustomized();
      iconeType_ = _r.getIconeType();
    }
  }

  public boolean ajusteLegendes(final CtuluNumberFormatI _formatter) {
    return ajusteLegendes(_formatter, sep);
  }

  /**
   * Ajuste la legende de la plage en fonction de min et max.
   */
  @Override
  public boolean ajusteLegendes(final CtuluNumberFormatI _formatter, final String _sep) {
    isLegendCustomized = false;
    final StringBuilder bf = new StringBuilder();
    minText = getText(min_, _formatter);
    this.sep = _sep;
    maxText = getText(max_, _formatter);
    bf.append(minText);
    // Rajout du deuxieme intervalle si la valeur du min<>valeur du max
    if (min_ != max_) {
      bf.append(CtuluLibString.ESPACE).append(_sep).append(CtuluLibString.ESPACE);
      bf.append(maxText);
    }
    final String newL = bf.toString();
    if (newL.equals(legende_)) {
      return false;
    }
    legende_ = newL;
    return true;
  }

  /**
   * Compare seulement les bornes min et max.
   */
  @Override
  public int compareTo(final Object _o) {
    if (_o instanceof BPlage) {
      final BPlage p = (BPlage) _o;
      if (min_ < p.min_) {
        return -1;
      } else if (min_ > p.min_) {
        return 1;
      } else if (max_ < p.max_) {
        return -1;
      } else if (max_ > p.max_) {
        return 1;
      } else {
        return 0;
      }

    }
    throw new IllegalArgumentException("pas une plage!");
  }

  /**
   * @param _v la valeur a considerer
   * @return true si la valeur est comprise dans les bornes de la plage
   */
  public boolean containsValue(final double _v) {
    return ((_v >= min_) && (_v <= max_));
  }

  @Override
  public BPlageInterface copy() {
    return new BPlage(this);
  }

  @Override
  public Object getIdentifiant() {
    return null;
  }

  /**
   * @return Returns the legende.
   */
  @Override
  public String getLegende() {
    return legende_;
  }

  /**
   * @return Returns the max.
   */
  @Override
  public double getMax() {
    return max_;
  }

  @Override
  public String getMaxString() {
    return maxText;
  }

  /**
   * @return Returns the min.
   */
  @Override
  public double getMin() {
    return min_;
  }

  @Override
  public String getMinString() {
    return minText;
  }

  @Override
  public String getSep() {
    return sep;
  }

  /**
   * @return the isLegendCustomized
   */
  @Override
  public boolean isLegendCustomized() {
    return isLegendCustomized;
  }

  @Override
  public boolean setLegende(final String _newLeg) {
    isLegendCustomized = true;
    if (legende_ == _newLeg || (legende_ != null && legende_.equals(_newLeg))) {
      return false;
    }
    legende_ = _newLeg;
    return true;
  }

  /**
   * @param _max la nouvelle valeur
   */
  public void setMax(final double _max) {
    if (_max != max_) {
      max_ = _max;
      // go
    }
  }

  /**
   * Modifie si nécessaire la valeur min de la plage et envoie un evt.
   *
   * @param _min la nouvelle valeur
   */
  public void setMin(final double _min) {
    if (_min != min_) {
      min_ = _min;
    }
  }

  /**
   * Modifie si necessaire les bornes de la plage et envoie un evt.
   *
   * @param _min la nouvelle valeur min
   * @param _max la nouvelle valeur max
   */
  public boolean setMinMax(final double _min, final double _max) {
    if ((min_ == _min) && (max_ == _max)) {
      return false;
    }
    max_ = _max;
    min_ = _min;
    return true;
  }
}
