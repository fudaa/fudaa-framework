/*
 *  @creation     13 mai 2004
 *  @modification $Date: 2007-01-19 13:09:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;


/**
 * @author Fred Deniger
 * @version $Id: BPalettePlageListener.java,v 1.6 2007-01-19 13:09:50 deniger Exp $
 */
public interface BPalettePlageListener {

  /**
   * Appelee lorsque la structure de la palette a ete modifiee.
   * @param _src la source de l'evt
   */
  void allPaletteModified(BPalettePlageInterface _src);

  /**
   * Appelee lorsque le titre a ete modifie.
   * @param _src la source de l'evt
   */
  void paletteTitreModified(BPalettePlageInterface _src);

  /**
   * Appele lorsque une plage a ete modifiee.
   *
   * @param _i l'indice de la plage modifiee
   * @param _src la source de l'evt
   */
  void plageModified(BPalettePlageInterface _src,int _i);
}