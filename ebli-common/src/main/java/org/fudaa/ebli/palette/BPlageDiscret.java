/*
 * @creation 15 d�c. 2004
 * @modification $Date: 2007-03-30 15:36:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ebli.trace.BPlageInterface;

/**
 * @author Fred Deniger
 * @version $Id: BPlageDiscret.java,v 1.8 2007-03-30 15:36:28 deniger Exp $
 */
public class BPlageDiscret extends BPlageAbstract {

  Object id_;
  /** La l�gende customis�e */
  String legende_;

  public BPlageDiscret() {

  }

  @Override
  public String getSep() {
    return null;
  }

  /**
   * @param _r la plage utilisee pour l'initialisation
   */
  public BPlageDiscret(final BPlageInterface _r) {
    initFrom(_r);
  }

  public void initFrom(BPlageInterface _r) {
    couleur_ = _r.getCouleur();
    id_ = _r.getIdentifiant();
    iconeTaille_ = _r.getIconeTaille();
    iconeType_ = _r.getIconeType();
    legende_=_r.getLegende();
  }

  @Override
  public BPlageInterface copy() {
    return new BPlageDiscret(this);
  }

  @Override
  public boolean isLegendCustomized() {
    return legende_!=null;
  }

  @Override
  public boolean setLegende(final String _newLeg) {
    legende_=_newLeg;
    return true;
  }

  /**
   * @param _id l'identifiant
   */
  public BPlageDiscret(final Object _id) {
    id_ = _id;

  }

  @Override
  public int compareTo(final Object _o) {
    if (_o instanceof BPlageDiscret) {
      final int r = getLegende().compareTo(((BPlageDiscret) _o).getLegende());
      if (r == 0 && !equals(_o)) { return hashCode() - _o.hashCode(); }
      return r;
    }
    throw new IllegalArgumentException("pas une plage!");
  }

  @Override
  public Object getIdentifiant() {
    return id_;
  }

  @Override
  public boolean ajusteLegendes(CtuluNumberFormatI formatter, String sep) {
    return false;
  }

  @Override
  public String getMaxString() {
    return CtuluLibString.EMPTY_STRING;
  }

  @Override
  public String getMinString() {
    return CtuluLibString.EMPTY_STRING;
  }

  @Override
  public String getLegende() {
    if (legende_!=null)
      return legende_;
    else if(id_ == null)
      return CtuluLibString.EMPTY_STRING;
    else
      return id_.toString();
  }

  @Override
  public double getMax() {
    return 0;
  }

  @Override
  public double getMin() {
    return 0;
  }
}
