/*
 * @creation 2000-05-15
 * 
 * @modification $Date: 2007-06-13 12:57:46 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JComponent;
import org.fudaa.ebli.trace.BPlageInterface;

import com.memoire.fu.FuLog;

/**
 * Une palette de couleurs associ�es � des plages de valeurs. Les valeurs hors plages sont consid�r�es comme "autres".
 *
 * @version $Id: BPalettePlage.java,v 1.22 2007-06-13 12:57:46 deniger Exp $
 * @author Bertrand Marchand
 */
public class BPalettePlage extends BPalettePlageAbstract implements Cloneable {

  public static void initPlages(final int _nb, final double _min, final double _max, final List _l, final BPalettePlage _model) {
    initPlages(_nb, _min, _max, _l, Color.CYAN.darker(), Color.RED, _model);
  }

  public static boolean isDisplayReversed() {
    return "true".equals(System.getProperty("palettePlage.display.reversed"));
  }

  public static void setDisplayReversed() {
    System.setProperty("palettePlage.display.reversed", "true");
  }

  /**
   * D�finition du nombre de plages.
   * <p>
   * Les plages sont red�finies automatiquement entre les 2 bornes min. et max., de mani�re r�guli�re. Les couleurs sont red�finies, les symboles et
   * les tailles, les l�gendes �galement.
   *
   * @param _nb Le nombre de plages, sup�rieur ou �gal � 1. Si inf�rieur, palette par d�faut.
   * @param _min la borne min des plages
   * @param _max la borne max des plages
   * @param _l la liste a modifier
   * @param _colorMin la couleur pour le min
   * @param _colorMax la couleur pour le max
   */
  public static void initPlages(final int _nb, final double _min, final double _max, final List _l, final Color _colorMin, final Color _colorMax,
          final BPalettePlage _model) {
    int nb = Math.max(_nb, 1);
    if (_min == _max) {
      nb = 1;
    }

    _l.clear();
    // initialisation des plages
    for (int i = 0; i < nb; i++) {
      _l.add(_model.createPlage(null));
    }
    double max = _max;
    final double min = _min;
    if (max <= min) {
      max = min;
    }
    // pour eviter la division par 0
    if (nb == 1) {
      final BPlage p0 = (BPlage) _l.get(0);
      p0.setMin(min);
      p0.setMax(max);
      p0.setCouleur(_colorMin);
    } else {
      // par construction les plages sont dans le bon ordre !
      for (int i = 0; i < nb; i++) {
        final BPlage pi = (BPlage) _l.get(i);
        pi.setMin(min + i * (max - min) / nb);
        pi.setMax(min + (i + 1) * (max - min) / nb);
        
        // BM 14/05/2019 Pour eviter les pb d'arrondis.
        if (i == 0) {
          pi.setMin(_min);
        }
        if (i == nb-1) {
          pi.setMax(_max);
        }
        
        pi.setCouleur(getCouleur(_colorMin, _colorMax, (double) i / ((double) (nb - 1))));
        // ajuste la legende
      }
      ((BPlage) _l.get(0)).setCouleur(_colorMin);
      ((BPlage) _l.get(nb - 1)).setCouleur(_colorMax);
    }
    _model.updatePlageLegendes();
  }
  /**
   * Plage pour les valeur en dehors des plages d�finies.
   */
  BPlage autres_;
  private boolean autresVisible_;
  private boolean logScale;
  /**
   * La liste des plages utilis�es.
   */
  List plages_;
  boolean reduit_;

  // Le symbole par d�faut pour les plages
  /**
   * Cr�ation d'une palette vide.
   */
  public BPalettePlage() {
    plages_ = new ArrayList(20);
    final BPlage pg = new BPlage();
    pg.setCouleur(Color.blue);
    pg.setMin(0);
    pg.setMax(1);
    plages_.add(pg);
  }

  public BPalettePlage(final BPalettePlage _p) {
    final int nb = _p.getNbPlages();
    plages_ = new ArrayList(nb + 1);
    for (int i = 0; i < nb; i++) {
      plages_.add(new BPlage(_p.getPlageInterface(i)));
    }
  }

  public BPalettePlage(final BPalettePlageProperties _p) {
    plages_ = new ArrayList(20);
    _p.updatePalette(this);
  }

  public BPalettePlage(final BPlageInterface[] _p) {
    this();
    setPlages(_p);
  }

  @Override
  public final Object clone() throws CloneNotSupportedException {
    final BPalettePlage r = (BPalettePlage) super.clone();
    final int nb = getNbPlages();
    r.plages_ = new ArrayList(nb);
    for (int i = 0; i < nb; i++) {
      r.plages_.add(createPlage(getPlage(i)));
    }
    r.setTitre(getTitre());
    r.setSousTitre(getSousTitre());
    r.setSubTitleLabel(getSubTitleLabel());
    r.setSubTitleVisible(isSubTitleVisible());
    return r;
  }

  @Override
  public BPlageInterface createPlage(final BPlageInterface _src) {
    if (_src != null) {
      return new BPlage(_src);
    }
    return new BPlage();

  }

  @Override
  public JComponent createReduit() {
    final BPalettePlageLegendeReduit res = new BPalettePlageLegendeReduit();
    res.setModel(this);
    res.setSize(50, 75);
    return res;
  }

  /**
   * @param _val la valeur a considerer
   * @return la couleur correspondante ou null si aucun
   */
  public Color getColorFor(final double _val) {
    final BPlage r = getPlageEnglobante(_val);
    if (r != null) {
      return r.getCouleur();
    }
    return null;
  }

  /**
   * @return la derniere plage
   */
  public final BPlage getLastPlage() {
    if (getNbPlages() == 0) {
      return null;
    }
    return getPlage(getNbPlages() - 1);
  }

  /**
   * Retourne la borne max. de la palette.
   *
   * @return La borne max.
   */
  public double getMaxPalette() {
    return getLastPlage().getMax();
  }

  /**
   * Retourne la borne min. de la palette.
   *
   * @return La borne min.
   */
  public double getMinPalette() {
    return getPlage(0).min_;
  }

  /**
   * Retourne le nombre de plages de la palette.
   *
   * @return Le nombre de plages
   */
  @Override
  public int getNbPlages() {
    return plages_.size();
  }

  public BPlage getNearestPlage(final double _val) {
    BPlage r = getPlageEnglobante(_val);
    if (r == null) {
      double ecart = -1;
      int idx = -1;
      final int nb = getNbPlages();
      for (int i = 0; i < nb; i++) {
        final BPlage pi = getPlage(i);
        final double ecarti = Math.min(Math.abs(pi.getMax() - _val), Math.abs(pi.getMin() - _val));
        if (ecart < 0 || ecarti < ecart) {
          ecart = ecarti;
          idx = i;
        }
      }
      if (idx >= 0) {
        r = getPlage(idx);
      }

    }
    return r;
  }

  /**
   * @param _idx l'indice
   * @return la plage d'indice _idx
   */
  public final BPlage getPlage(final int _idx) {
    return (BPlage) plages_.get(_idx);
  }

  /**
   * @return la plage utilisee pour les valeur en dehors des plages definies
   */
  public BPlage getPlageAutres() {
    return autres_;
  }

  @Override
  public BPlageInterface getPlageAutresInterface() {
    return getPlageAutres();
  }

  /**
   * Retourne la plage associ�e � une valeur.
   * <p>
   * Si la valeur est en dehors des plages, la plage retourn�e est ou bien la plage "autres" si les valeurs "autres" doivent �tre repr�sent�es, ou
   * bien <i>null </i> si les valeurs "autres" ne doivent pas �tre repr�sent�es.
   *
   * @param _val la valeur a considerer
   * @return La plage associ�e � la valeur.
   */
  public BPlage getPlageEnglobante(final double _val) {
    final int nb = getNbPlages();
    BPlage pi;
    final double val = _val;
    for (int i = 0; i < nb; i++) {
      pi = getPlage(i);
      if (pi.containsValue(val)) {
        return pi;
      }
    }
    FuLog.trace("Value " + _val);
    return isAutresVisible() ? autres_ : null;
  }

  @Override
  public BPlageInterface getPlageInterface(final int _idx) {
    return getPlage(_idx);
  }

  /**
   * @param _c l'intialiseur de couleurs
   */
  public void initCouleurs(final BPalettePlageCouleurChooser _c) {
    _c.initPaletteCouleurs(plages_);
  }

  public void initPlages(final int _nb, final double _min, final double _max) {
    this.initPlages(_nb, _min, _max, false);
  }

  /**
   * D�finition du nombre de plages.
   * <p>
   * Les plages sont red�finies automatiquement entre les 2 bornes min. et max., de mani�re r�guli�re. Les couleurs sont red�finies, les symboles et
   * les tailles, les l�gendes �galement.
   *
   * @param _nb Le nombre de plages, sup�rieur ou �gal � 1. Si inf�rieur, palette par d�faut.
   * @param _min la borne min des plages
   * @param _max la borne max des plages
   */
  public void initPlages(final int _nb, final double _min, final double _max, boolean isLog) {
    initPlages(_nb, _min, _max, plages_, this);
    fireAllPaletteModified();
  }

  public void initPlages(final int _nb, final double _min, final double _max, final Color _minC, final Color _maxC) {
    initPlages(_nb, _min, _max, plages_, _minC, _maxC, this);
    fireAllPaletteModified();
  }

  /**
   * Retourne si les valeurs "autres" sont visibles.
   *
   * @return <i>true </i> Les valeurs hors plages sont visibles.
   */
  @Override
  public boolean isAutresVisible() {
    return autresVisible_;
  }

  @Override
  public boolean isLogScale() {
    return logScale;
  }

  @Override
  public void setLogScale(boolean selected) {
    this.logScale = selected;
  }

  @Override
  public boolean isReduit() {
    return reduit_;
  }

  @Override
  public boolean isReduitEnable() {
    return true;
  }

  /**
   * Propager les tailles de symboles. Les tailles sont interpol�es d'une taille vers une autre pour toutes les plages de la palette.
   * <p>
   * La taille <i>_tmin </i> correspond � la plage 0, la taille <i>_tmax </i> � la plage getNbPlages()-1.
   *
   * @param _tmin Taille pour la plage 0
   * @param _tmax Taille pour la plage getNbPlage()-1
   */
  protected void propagerTailles(final int _tmin, final int _tmax) {
    final int nb = getNbPlages();
    for (int i = 0; i < nb; i++) {
      getPlage(i).setTailleIcone(_tmin + (int) (((double) i / (double) (nb - 1)) * (_tmax - _tmin)));
    }
  }

  public final void setAutres(final BPlage _autres) {
    autres_ = _autres;
  }

  /**
   * D�finit si les valeurs "autres" sont rendues visibles.
   *
   * @param _visible <i>true </i> Les valeurs "autres" sont rendues visibles.
   */
  public void setAutresVisible(final boolean _visible) {
    if (autresVisible_ != _visible) {
      autresVisible_ = _visible;
    }
  }

  public void setLogPlages(boolean logPlages) {
    this.logScale = logPlages;
  }

  /**
   * D�finition de la borne max. de la palette.
   * <p>
   * Les bornes des plages ne sont pas red�finies automatiquement. Le r�ajustement peut �tre effectu� par ajustePlages().
   *
   * @param _max la nouvelle borne max
   */
  public void setMaxPalette(final double _max) {
    getLastPlage().setMax(_max);
  }

  /**
   * D�finition de la borne min. de la palette.
   * <p>
   * Les bornes des plages ne sont pas red�finies automatiquement. Le r�ajustement peut �tre effectu� par ajustePlages().
   *
   * @param _min la nouvelle borne min
   */
  public void setMinPalette(final double _min) {
    getPlage(0).setMin(_min);
  }

  /**
   * @param _p les nouvelles plages qui seront copiees.
   */
  @Override
  public final void setPlages(final BPlageInterface[] _p) {
    plages_.clear();
    final int nb = _p.length;
    for (int i = 0; i < nb; i++) {
      plages_.add(createPlage(_p[i]));
    }
    Collections.sort(plages_);
    if (plages_.size() > 15) {
      setReduit(true);
    }
    fireAllPaletteModified();
  }

  @Override
  public boolean setReduit(final boolean _b) {
    if (_b == reduit_) {
      return false;
    }
    reduit_ = _b;
    fireAllPaletteModified();
    return true;
  }
}
