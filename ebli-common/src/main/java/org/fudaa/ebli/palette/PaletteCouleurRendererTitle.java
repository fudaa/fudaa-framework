/*
 *  @creation     24 janv. 2005
 *  @modification $Date: 2006-11-14 09:06:13 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuSeparator;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.table.TableCellRenderer;


/**
 * @author Fred Deniger
 * @version $Id: PaletteCouleurRendererTitle.java,v 1.1 2006-11-14 09:06:13 deniger Exp $
 */
public class PaletteCouleurRendererTitle extends BuPanel implements ListCellRenderer,
    TableCellRenderer {

  PaletteCouleurRenderer renderer_;
  BuLabel title_;

  public PaletteCouleurRendererTitle() {
    setLayout(new BuGridLayout(2, 5, 0));
    setOpaque(true);
    title_ = new BuLabel();
    title_.setOpaque(false);
    renderer_ = new PaletteCouleurRenderer();
    renderer_.setPreferredSize(new Dimension(50, 8));
    add(renderer_);
    add(title_);
  }

  BuSeparator nullRenderer_;

  @Override
  public Component getTableCellRendererComponent(final JTable _table, final Object _value, final boolean _isSelected,
                                                 final boolean _hasFocus, final int _row, final int _column){
    if (_value == null) {
      if (nullRenderer_ == null) {
        nullRenderer_ = new BuSeparator();
      }
      return nullRenderer_;
    }
    final PaletteCouleur p = (PaletteCouleur) _value;
    title_.setText(p.getTitle());
    renderer_.setPalette(p);
    if (_isSelected) {
      title_.setForeground(_table.getSelectionForeground());
      super.setBackground(_table.getSelectionBackground());
    }
    else {
      title_.setForeground(_table.getForeground());
      super.setBackground(_table.getBackground());
    }
    return this;
  }

  @Override
  public Component getListCellRendererComponent(final JList _list, final Object _value, final int _index,
                                                final boolean _isSelected, final boolean _cellHasFocus){
    if (_value == null) {
      if (nullRenderer_ == null) {
        nullRenderer_ = new BuSeparator();
      }
      return nullRenderer_;
    }
    final PaletteCouleur p = (PaletteCouleur) _value;
    title_.setText(p.getTitle());
    renderer_.setPalette(p);
    if (_isSelected) {
      title_.setForeground(_list.getSelectionForeground());
      super.setBackground(_list.getSelectionBackground());
    }
    else {
      title_.setForeground(_list.getForeground());
      super.setBackground(_list.getBackground());
    }
    return this;
  }
}
