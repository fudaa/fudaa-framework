/*
 * @creation 2 ao�t 2004
 * 
 * @modification $Date: 2007-01-19 13:09:50 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import java.beans.PropertyChangeListener;
import org.fudaa.ctulu.CtuluRange;

/**
 * @author Fred Deniger
 * @version $Id: BPalettePlageTarget.java,v 1.8 2007-01-19 13:09:50 deniger Exp $
 */
public interface BPalettePlageTarget {

  /**
   * @param _s la propriete a ecouter
   * @param _l le listener
   */
  void addPropertyChangeListener(String _key, PropertyChangeListener _l);

  /**
   * @param _l le listener a enlever
   */
  void removePropertyChangeListener(String _key, PropertyChangeListener _l);

  /**
   * @return le titre de la cible
   */
  String getTitle();

  /**
   * Permet de connaitre les bornes des donnees affichees.
   * 
   * @param _b la boite a modifier
   * @return false si non adapte.
   */
  boolean getRange(CtuluRange _b);

  /**
   * @param _b les bornes a modifier
   * @return true si adapte
   */
  boolean getTimeRange(CtuluRange _b);

  /**
   * Permet de cr�er une palette de couleurs. Si {@link #isDiscrete()}, la palette cr��e est discrete, sinon elle est
   * suivant des niveaux.
   * 
   * @return La palette cr�ee.
   */
  BPalettePlageInterface createPaletteCouleur();

  /**
   * @return la palette en cours d'utilisation
   */
  BPalettePlageInterface getPaletteCouleur();

  /**
   * Permet de mettre a jour les plages a utiliser par le calque. Attention: ne pas changer la signature de cette
   * methode. Elle est appelee par reflexion par le BArbreModel
   * 
   * @param _newPlage les nouvelles plages
   */
  void setPaletteCouleurPlages(BPalettePlageInterface _newPlage);

  /**
   * @return true si l'outil palette peut-etre utilise pour ce calque
   */
  boolean isPaletteModifiable();

  /**
   * @return true si cette une palette non continu
   */
  boolean isDiscrete();


  /**
   * Un calque peut avoir comme titre surface et peut dessiner une bathymetrie. getTitle renverre surface et
   * getDataDescription renverra "bathymetrie"
   * 
   * @return une description des donnees
   */
  String getDataDescription();

  /**
   * @return true si contient des donnees et si la methode getDonnees est implant�es
   */
  boolean isDonneesBoiteAvailable();

  /**
   * @return true si contient des donnees variables avec le temps
   */
  boolean isDonneesBoiteTimeAvailable();
 

}
