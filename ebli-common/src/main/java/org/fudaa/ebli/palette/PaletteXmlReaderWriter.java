/**
 * 
 */
package org.fudaa.ebli.palette;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.ConversionException;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.XmlFriendlyReplacer;
import com.thoughtworks.xstream.mapper.CannotResolveClassException;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.filechooser.FileFilter;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluNumberFormatFixedFigure;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ebli.trace.BPlageInterface;

/**
 * @author CANEL Christophe (Genesis)
 */
public class PaletteXmlReaderWriter {
  public static class PaletteFileFilter extends FileFilter {

    private final static String EXTENSION = ".palette.xml";

    public PaletteFileFilter() {}

    @Override
    public String getDescription() {
      return "Fichier de palette (*.palette.xml)";
    }

    @Override
    public boolean accept(File f) {
      if ((f == null) || f.isHidden()) { return false; }

      return this.hasCorrectExtension(f);
    }

    public File getFileWithExtension(File file) {
      if (this.hasCorrectExtension(file)) { return file; }

      return new File(file.getAbsolutePath() + EXTENSION);
    }

    private boolean hasCorrectExtension(File file) {
      final int extLength = EXTENSION.length();
      final String fileName = file.getName();
      final int fileNameLength = fileName.length();

      if (fileNameLength < (extLength + 1)) { return false; }

      return fileName.substring(fileNameLength - extLength, fileNameLength).equalsIgnoreCase(EXTENSION);
    }
  }

  private static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
  private static final XmlFriendlyReplacer REPLACER = new XmlFriendlyReplacer("#", "_");
  private static final String ENCODING = "UTF-8";
  private final static Logger LOGGER = Logger.getLogger(PaletteXmlReaderWriter.class.getName());

  private static class HeaderDao {
    public String xmlns = "http://www.fudaa.fr/xsd/crue";
    public String xmlnsxsi = "http://www.w3.org/2001/XMLSchema-instance";
    public String xsischemaLocation = "http://www.fudaa.fr/xsd/crue http://www.fudaa.fr/xsd/crue/header.xsd";
  }

  private static class PaletteDao extends HeaderDao {
    public FormatDao Format;
    public List<PlageDao> Plages;

    public static PaletteDao from(PaletteSelecteurCouleurPlage metier) {
      PaletteDao dao = new PaletteDao();

      dao.Format = FormatDao.fromFormat(metier.getPlageDefault());
      dao.Plages = new ArrayList<PaletteXmlReaderWriter.PlageDao>(metier.plages_.size());

      for (BPlageInterface plage : (List<BPlageInterface>) metier.plages_) {
        dao.Plages.add(PlageDao.fromPlage(plage));
      }

      return dao;
    }

    public static void update(PaletteSelecteurCouleurPlage metier, PaletteDao dao) {
      FormatDao.update(metier.getPlageDefault(), dao.Format);

      metier.plages_.clear();

      for (PlageDao plageDao : dao.Plages) {
        BPlageInterface plage = PlageDao.toPlage(plageDao);

        if (!plage.isLegendCustomized()) {
          plage.ajusteLegendes(metier.getPlageDefault().formatter_, metier.getPlageDefault().sep_);
        }

        metier.plages_.add(plage);
      }

      metier.model_.fireAllModified();
    }
  }

  private static class FormatDao {
    public String Separateur;
    public FormatterDao Formatter;

    public static FormatDao fromFormat(BPalettePlageDefault metier) {
      FormatDao dao = new FormatDao();

      dao.Separateur = metier.sep_;
      dao.Formatter = FormatterDao.fromFormatter(metier.formatter_);

      return dao;
    }

    public static void update(BPalettePlageDefault metier, FormatDao dao) {
      metier.sep_ = dao.Separateur;
      metier.formatter_ = FormatterDao.toFormatter(dao.Formatter);
    }
  }

  private static class FormatterDao {
    public Integer NbDigits;
    public Integer NbMaxDigits;
    public Integer NbMinDigits;
    public Boolean NotationScientifique;

    public static FormatterDao fromFormatter(CtuluNumberFormatI metier) {
      FormatterDao dao = new FormatterDao();

      if (metier instanceof CtuluNumberFormatDefault) {
        DecimalFormat formatter = ((CtuluNumberFormatDefault) metier).cloneDecimalFormat();

        dao.NbMaxDigits = formatter.getMaximumFractionDigits();
        dao.NbMinDigits = formatter.getMinimumFractionDigits();
        dao.NotationScientifique = (formatter.toPattern().indexOf('E') > -1);
      } else if (metier instanceof CtuluNumberFormatFixedFigure) {
        dao.NbDigits = ((CtuluNumberFormatFixedFigure) metier).getMaxChar();
      }

      return dao;
    }

    public static CtuluNumberFormatI toFormatter(FormatterDao dao) {
      CtuluNumberFormatI metier;

      if (dao.NbDigits == null) {
        DecimalFormat formatter = new DecimalFormat();

        if (dao.NotationScientifique) {
          formatter.applyPattern("0.#####E0");
        } else {
          formatter.applyPattern("0.##");
        }

        formatter.setMaximumFractionDigits(dao.NbMaxDigits);
        formatter.setMinimumFractionDigits(dao.NbMinDigits);

        metier = new CtuluNumberFormatDefault(formatter);
      } else {
        metier = new CtuluNumberFormatFixedFigure(dao.NbDigits);
      }

      return metier;
    }
  }

  private static class PlageDao {
    public int Couleur;
    public double Max;
    public double Min;
    public String Label;

    public static PlageDao fromPlage(BPlageInterface metier) {
      PlageDao dao = new PlageDao();

      dao.Couleur = metier.getCouleur().getRGB();
      dao.Max = metier.getMax();
      dao.Min = metier.getMin();

      if (metier.isLegendCustomized()) {
        dao.Label = metier.getLegende();
      } else {
        dao.Label = null;
      }

      return dao;
    }

    public static BPlageInterface toPlage(PlageDao dao) {
      BPlage metier = new BPlage();

      metier.setCouleur(new Color(dao.Couleur));
      metier.setMax(dao.Max);
      metier.setMin(dao.Min);

      if (dao.Label != null) {
        metier.setLegende(dao.Label);
      }

      return metier;
    }
  }

  public void read(final File fichier, PaletteSelecteurCouleurPlage palette, final CtuluLog analyser) {
    PaletteDao dao = readDao(fichier, analyser);

    if (dao != null) {
      PaletteDao.update(palette, dao);
    }
  }

  private PaletteDao readDao(final File fichier, final CtuluLog analyser) {
    //    analyser.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    //    analyser.setDesc(BusinessMessages.getString("read.file", f.getName()));

    FileInputStream in = null;
    PaletteDao newData = null;
    try {
      in = new FileInputStream(fichier);
      newData = readDao(in, analyser);
    } catch (final FileNotFoundException e) {
      LOGGER.log(Level.FINE, "readDao", e);
      final String path = fichier == null ? "null" : fichier.getAbsolutePath();
      analyser.addSevereError("io.FileNotFoundException.error", path);
    } finally {
      CtuluLibFile.close(in);
    }
    return newData;
  }

  private PaletteDao readDao(final InputStream in, final CtuluLog analyser) {
    PaletteDao newData = null;
    BufferedReader contentRead = null;
    try {
      final XStream parser = initXmlParser(analyser);

      // The unicodeInputStream is used to eliminate the BOM bug from java:
      //      UnicodeInputStream unicodeStream = new UnicodeInputStream(in, ENCODING);
      //      unicodeStream.init();
      //      contentRead = new BufferedReader(new InputStreamReader(unicodeStream, ENCODING));
      contentRead = new BufferedReader(new InputStreamReader(in, ENCODING));

      newData = (PaletteDao) parser.fromXML(contentRead);
    } catch (ConversionException conversionException) {
      LOGGER.log(Level.FINE, "io.unknown.bsalise", conversionException);
      //TODO Voir si garder.
      //analyser.addFatalError("io.unknown.bsalise", StringUtils.substringBefore(conversionException.getShortMessage(), " "));

    } catch (CannotResolveClassException cannotResolveException) {
      LOGGER.log(Level.FINE, "io.unknown.bsalise", cannotResolveException);
      analyser.addSevereError("io.unknown.bsalise", cannotResolveException.getMessage());

    } catch (final Exception e) {
      LOGGER.log(Level.FINE, "io.xml.error", e);
      analyser.addSevereError("io.xml.error", e.getMessage());
    } finally {
      CtuluLibFile.close(contentRead);
    }
    return newData;
  }

  public void write(final File file, PaletteSelecteurCouleurPlage palette, final CtuluLog analyser) {
    PaletteDao dao = PaletteDao.from(palette);

    writeDAO(file, dao, analyser);
  }

  private boolean writeDAO(final File file, final PaletteDao dao, final CtuluLog analyser) {
    //analyser.setDefaultResourceBundle(BusinessMessages.RESOURCE_BUNDLE);
    //analyser.setDesc(BusinessMessages.getString("write.file", f.getName()));

    FileOutputStream out = null;
    boolean ok = true;
    try {
      out = new FileOutputStream(file);
      //      out.write(new byte[]{(byte)0xEF, (byte)0xBB, (byte)0xBF});
      //      out.flush();
      ok = writeDAO(out, dao, analyser);
    } catch (final IOException e) {
      LOGGER.log(Level.SEVERE, "writeDAO " + file.getName(), e);
      ok = false;
    } finally {
      CtuluLibFile.close(out);
    }
    return ok;

  }

  private boolean writeDAO(final OutputStream out, final PaletteDao dao, final CtuluLog analyser) {
    boolean isOk = true;
    try {
      final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, ENCODING));
      writer.write(XML_HEADER + CtuluLibString.LINE_SEP);

      final XStream parser = initXmlParser(analyser);
      parser.marshal(dao, new PrettyPrintWriter(writer, new char[] { ' ', ' ' }, CtuluLibString.LINE_SEP, REPLACER));
      // parser.toXML(dao, writer);

    } catch (final IOException e) {
      LOGGER.log(Level.SEVERE, "writeDAO", e);
      analyser.addSevereError("file.write.error");
      isOk = false;
    } finally {
      CtuluLibFile.close(out);
    }
    return isOk;
  }

  private XStream initXmlParser(final CtuluLog analyse) {
    final DomDriver domDriver = new DomDriver(ENCODING, REPLACER);
    final XStream xstream = new XStream(domDriver);
    xstream.setMode(XStream.NO_REFERENCES);

    configureXStreamHeader(xstream, analyse);
    configureXStreamPalette(xstream, analyse);
    configureXStreamPlage(xstream, analyse);

    return xstream;
  }

  private void configureXStreamHeader(final XStream xstream, final CtuluLog analyse) {
    xstream.aliasAttribute("xmlns:xsi", "xmlnsxsi");
    xstream.aliasAttribute("xsi:schemaLocation", "xsischemaLocation");

    xstream.useAttributeFor(HeaderDao.class, "xmlns");
    xstream.useAttributeFor(HeaderDao.class, "xmlnsxsi");
    xstream.useAttributeFor(HeaderDao.class, "xsischemaLocation");
  }

  private void configureXStreamPalette(final XStream xstream, final CtuluLog analyse) {
    xstream.alias("Palette", PaletteDao.class);
  }

  private void configureXStreamPlage(final XStream xstream, final CtuluLog analyse) {
    xstream.alias("Plage", PlageDao.class);
  }
}
