/**
 *  @creation     24 janv. 2005
 *  @modification $Date: 2007-03-15 17:00:31 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import java.awt.Color;
import java.util.List;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author Fred Deniger
 * @version $Id: PaletteCouleurContinu.java,v 1.2 2007-03-15 17:00:31 deniger Exp $
 */
public class PaletteCouleurContinu implements PaletteCouleur {

  public final static Color DEFAULT_MAX = Color.RED.darker();
  public final static Color DEFAULT_MIN = Color.CYAN.brighter();

  Color max_ = DEFAULT_MAX;
  Color min_ = DEFAULT_MIN;

  /**
   * @param _min
   * @param _max
   */
  public PaletteCouleurContinu(final Color _min, final Color _max) {
    super();
    min_ = _min;
    max_ = _max;
    if (min_ == null) min_ = DEFAULT_MIN;
    if (max_ == null) max_ = DEFAULT_MAX;
  }

  @Override
  public Color couleur(final double _z) {
    return BPalettePlageAbstract.getCouleur(min_, max_, _z);
  }

  public final Color getMax() {
    return max_;
  }

  public final Color getMin() {
    return min_;
  }

  public final void setMax(final Color _max) {
    if (_max != null) max_ = _max;
  }

  public final void setMin(final Color _min) {
    if (_min != null) min_ = _min;
  }

  @Override
  public String getTitle() {
    return EbliLib.getS("Lin�aire");
  }

  @Override
  public void updatePlages(final List _plages) {
    final double max = (_plages.size() - 1);
    for (int i = 0; i < _plages.size(); i++) {
      final double d = i / max;
      ((BPlageAbstract) _plages.get(i)).setCouleur(couleur(d));
    }
  }
}
