/*
 * @creation 15 d�c. 2004
 * 
 * @modification $Date: 2007-05-22 14:19:03 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import com.memoire.fu.FuEmptyArrays;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import java.awt.Color;
import javax.swing.JComponent;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.editor.CtuluValueEditorDefaults;
import org.fudaa.ctulu.editor.CtuluValueEditorI;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.trace.BPlageInterface;
import org.fudaa.ebli.trace.TraceIsoPlageInterface;

/**
 * @author Fred Deniger
 * @version $Id: BPalettePlageAbstract.java,v 1.20 2007-05-22 14:19:03 deniger Exp $
 */
public abstract class BPalettePlageAbstract implements BPalettePlageInterface {

  private static float[] tdl(final Color _c) {
    if (_c == null) {
      return FuEmptyArrays.FLOAT0;
    }
    return Color.RGBtoHSB(_c.getRed(), _c.getGreen(), _c.getBlue(), null);
  }

  public String getFormatLocalizedPattern() {
    return formatter_ == null ? null : formatter_.toLocalizedPattern();
  }

  @Override
  public JComponent createReduit() {
    return null;
  }

  @Override
  public boolean isReduit() {
    return false;
  }

  @Override
  public boolean isReduitEnable() {
    return false;
  }

  @Override
  public boolean setReduit(final boolean _b) {
    return false;
  }

  /**
   * Retourne une couleur interpol�e dans une palette d�finie par 2 couleurs extr�mes.
   */
  public static Color getCouleur(final Color _couleurMin, final Color _couleurMax, final double _z) {
    double z = _z;
    if (z < 0.) {
      z = 0.;
    }
    if (z >= 1.) {
      return _couleurMax;
    }
    z -= Math.floor(z);
    float t, d, l;
    int n, a, amin, amax;
    float[] tdlmin, tdlmax;
    tdlmin = tdl(_couleurMin);
    tdlmax = tdl(_couleurMax);
    amin = _couleurMin.getAlpha();
    amax = _couleurMax.getAlpha();
    n = (int) (z * 255.);
    a = amin + (amax - amin) * n / 255;
    t = tdlmin[0] + (tdlmax[0] - tdlmin[0]) * (float) z;
    d = tdlmin[1] + (tdlmax[1] - tdlmin[1]) * (float) z;
    l = tdlmin[2] + (tdlmax[2] - tdlmin[2]) * (float) z;
    final int v = Color.getHSBColor(t, d, l).getRGB();
    return new Color((v & 0x00FFFFFF) | (a << 24), true);
  }
  private transient BPalettePlageListener legende_;
  /**
   * Le sous-titre de la palette.
   */
  @XStreamAlias("SubTitle")
  private String ssTitre_;
  /**
   * Le titre de la palette.
   */
  @XStreamAlias("Title")
  private String titre_;
  @XStreamOmitField
  private CtuluNumberFormatI formatter_;
  @XStreamAlias("Separator-Characters")
  String sep_ = EbliLib.getS("�");

  public BPalettePlageAbstract() {
  }

  public BPalettePlageAbstract(final BPalettePlageAbstract _plage) {
    if (_plage != null) {
      ssTitre_ = _plage.ssTitre_;
      titre_ = _plage.titre_;
      if (_plage.sep_ != null) {
        sep_ = _plage.sep_;
      }
      if (_plage.formatter_ != null) {
        formatter_ = _plage.formatter_.getCopy();
      }
    }
  }

  public BPalettePlageAbstract(final BPalettePlageProperties _plage) {
    if (_plage != null) {
      _plage.updatePalette(this);
    }
  }

  protected void fireAllPaletteModified() {
    if (legende_ != null) {
      legende_.allPaletteModified(this);
    }
  }

  protected void firePlageModified(final int _i) {
    if (legende_ != null) {
      legende_.plageModified(this, _i);
    }
  }

  protected void fireTitlePaletteModified() {
    if (legende_ != null) {
      legende_.paletteTitreModified(this);
    }
  }

  @Override
  public void setPlageListener(final BPalettePlageListener _l) {
    legende_ = _l;
  }

  @Override
  public CtuluNumberFormatI getFormatter() {
    return formatter_ == null ? BPlage.PLAGE_NUMBER_FORMAT : formatter_;
  }

  @Override
  public BPlageInterface[] getPlages() {
    final BPlageInterface[] ps = new BPlageInterface[getNbPlages()];
    for (int i = 0; i < ps.length; i++) {
      ps[i] = getPlageInterface(i);
    }
    return ps;
  }

  @Override
  public final String getSep() {
    return sep_;
  }

  /**
   * Retourne le sous titre de la palette.
   *
   * @return Le sous titre.
   */
  @Override
  public String getSousTitre() {
    return ssTitre_;
  }

  /**
   * Retourne le titre de la palette.
   *
   * @return Le titre.
   */
  @Override
  public String getTitre() {
    return titre_;
  }

  @Override
  public CtuluValueEditorI getValueEditor() {
    return CtuluValueEditorDefaults.DOUBLE_EDITOR;
  }

  public void initFrom(final BPalettePlageInterface _a) {
    if (_a == null) {
      return;
    }
    setTitre(_a.getTitre());
    setSousTitre(_a.getSousTitre());
    setSubTitleVisible(_a.isSubTitleVisible());
    setPlages(_a.getPlages());
    setSep(_a.getSep());
    setFormatter(_a.getFormatter());
    setLogScale(_a.isLogScale());
    if (isReduitEnable()) {
      setReduit(_a.isReduit());
    }
  }

  public final void setFormatter(final CtuluNumberFormatI _formatter) {
    if (_formatter != null && !_formatter.equals(formatter_)) {
      formatter_ = _formatter;
      fireAllPaletteModified();
    }
  }

  public BPalettePlageProperties save() {
    return new BPalettePlageProperties(this);
  }

  @Override
  public void load(final BPalettePlageProperties _prop) {
    if (_prop != null) {
      _prop.updatePalette(this);
    }
  }

  /**
   * @param _p les nouvelles plages
   */
  public abstract void setPlages(BPlageInterface[] _p);

  public final void setSep(final String _sep) {
    sep_ = _sep;
  }

  /**
   * D�finition du sous titre de la palette.
   *
   * @param _ssTitre Le sous titre.
   */
  public void setSousTitre(final String _ssTitre) {
    if (_ssTitre == null) {
      return;
    }
    if (!_ssTitre.equals(ssTitre_)) {
      ssTitre_ = _ssTitre;
      fireTitlePaletteModified();
    }
  }
  private String subTitleLabel;
  private boolean subTitleVisible = true;

  @Override
  public boolean isSubTitleVisible() {
    return subTitleVisible;
  }

  @Override
  public void setSubTitleVisible(boolean subTitleVisible) {
    if (subTitleVisible != this.subTitleVisible) {
      this.subTitleVisible = subTitleVisible;
      fireTitlePaletteModified();
    }
  }

  @Override
  public String getSubTitleLabel() {
    return subTitleLabel;
  }

  public void setSubTitleLabel(String sousTitreTitle) {
    this.subTitleLabel = sousTitreTitle;
  }

  /**
   * D�finition du titre de la palette.
   *
   * @param _titre Le titre.
   */
  public void setTitre(final String _titre) {
    if (_titre == null) {
      return;
    }
    if (!_titre.equals(titre_)) {
      titre_ = _titre;
      fireTitlePaletteModified();
    }
  }

  /**
   * Ajuste les legendes.
   */
  public void updatePlageLegendes() {
    updatePlageLegendes(this, getFormatter(), sep_);
  }

  public static void updatePlageLegendes(final TraceIsoPlageInterface plages, final CtuluNumberFormatI fmt,
          final String sep) {
    for (int i = plages.getNbPlages() - 1; i >= 0; i--) {
      plages.getPlageInterface(i).ajusteLegendes(fmt, sep);
    }
  }
}
