/**
 *  @creation     21 janv. 2005
 *  @modification $Date: 2006-11-14 09:06:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuResource;
import com.memoire.fu.FuLib;
import java.awt.Color;
import java.util.*;
import org.fudaa.ebli.commun.EbliPreferences;

/**
 * @author Fred Deniger
 * @version $Id: PaletteManager.java,v 1.1 2006-11-14 09:06:17 deniger Exp $
 */
public final class PaletteManager implements BPalettePlageCouleurChooser{

  /**
   * Singleton.
   */
  public static final PaletteManager INSTANCE = new PaletteManager();

  //final int COLUMN = 10;

  Map namePalette_;

  /**
   * @param _t le nom a tester
   * @return si une palette existe avec le meme nom
   */
  public boolean containsPaletteWithTitle(final String _t){
    return namePalette_.containsKey(_t);
  }

  private PaletteManager() {
    namePalette_ = loadPaletteFromPref();
    if (namePalette_ == null) {
      namePalette_ = new HashMap();
    }
    if (namePalette_.size() == 0) {
      final Color[] cs = new Color[30];
      final double max = cs.length;
      for (int i = 0; i < cs.length; i++) {
        cs[i] = BPalettePlageAbstract.getCouleur(Color.BLUE, Color.RED, i / max);
      }
      final  String name = BuResource.BU.getString("Utilisateur");
      namePalette_.put(name, new PaletteCouleurDiscontinu(cs, name));
    }
  }

  /**
   * Nettoie les preferences.
   */
  private void cleanPref(){
    final     BuPreferences pref = getPref();
    final String prefix = getPrefPrefix();
    final List m = new ArrayList();
    for (final Enumeration e = pref.keys(); e.hasMoreElements();) {
      final String key = (String) e.nextElement();
      if (key.startsWith(prefix)) {
        m.add(key);
      }
    }
    for (int i = m.size() - 1; i >= 0; i--) {
      pref.removeProperty((String) m.get(i));
    }
  }

  /**
   * @param _c le tableau des couleurs a copier
   * @return le tableau copie
   */
  public static Color[] copy(final Color[] _c){
    if (_c == null) {
      return null;
    }
    final Color[] r = new Color[_c.length];
    System.arraycopy(_c, 0, r, 0, r.length);
    return r;
  }

  private BuPreferences getPref(){
    return EbliPreferences.EBLI;
  }

  private Color[] loadColor(final String _value){
    final StringTokenizer tk = new StringTokenizer(_value, ";");
    final  Color[] r = new Color[tk.countTokens()];
    int idx = 0;
    try {
      while (tk.hasMoreTokens()) {
        final    Color ri = getColorProperty(tk.nextToken());
        r[idx++] = ri;
      }
    }
    catch (final Exception ex) {
      return null;
    }
    if (idx != r.length) {
      return null;
    }
    return r;
  }

  private Map loadPaletteFromPref(){
    final  BuPreferences pref = getPref();
    final  String prefix = getPrefPrefix();
    final   Map m = new HashMap();
    for (final Enumeration e = pref.keys(); e.hasMoreElements();) {
      final String key = (String) e.nextElement();
      if (key.startsWith(prefix)) {
        final  String name = key.substring(prefix.length());
        if (!m.containsKey(name)) {
          final   Color[] cs = loadColor(pref.getStringProperty(key, null));
          if (cs != null) {
            m.put(name, new PaletteCouleurDiscontinu(cs, name));
          }
        }
      }
    }
    return m;
  }

  /**
   * @param _cs le tableau des couleurs
   * @return la chaine représentant ce tableau
   */
  private String toString(final Color[] _cs){
    if (_cs == null || _cs.length == 0) {
      return null;
    }
    final StringBuffer buf = new StringBuffer();
    final String sep = ";";
    for (int i = 0; i < _cs.length; i++) {
      if (i > 0) {
        buf.append(sep);
      }
      buf.append(getColorProperty(_cs[i]));
    }
    return buf.toString();
  }

  String getPrefPrefix(){
    return "palette.color.";
  }

  String getLastPalettePrefKey(){
    return "palette.last.color";
  }

  /**
   * Permet d'initialiser les plages avec la dernière palette sélectionnée.
   * @param _pls les plages a initialiser
   */
  @Override
  public void initPaletteCouleurs(final List _pls){
    final  String lastPal=getLastChoosePaletteName();
    if(lastPal!=null){
      final    PaletteCouleurDiscontinu p=getPalette(lastPal);
      if(p!=null) {
        p.updatePlages(_pls);
      }
    }
  }

  /**
   * @return le nom de la derniere palette selectionnee. null si continue
   */
  public String getLastChoosePaletteName(){
    return getPref().getStringProperty(getLastPalettePrefKey(), null);
  }

  /**
   * @param _name la nom de la derniere palette discontinue utilisee.
   */
  public void setLastChoosePaletteName(final String _name){
    if (_name == null) {
      getPref().removeProperty(getLastPalettePrefKey());
    } else{
      getPref().putStringProperty(getLastPalettePrefKey(), _name);
    }
    savePref();
  }

  /**
   * @return les propriétés pouvant etre exportées.
   */
  public Properties exportPalette(){
    final  Properties r = new Properties();
    final  String prefix = getPrefPrefix();
    for (final Iterator it = namePalette_.entrySet().iterator(); it.hasNext();) {
      final Map.Entry e = (Map.Entry) it.next();
      final String s = toString(((PaletteCouleurDiscontinu) e.getValue()).getCs());
      if (s != null) {
        //juste pour etre sur que la cle est une chaine.
        r.put(prefix + ((String) e.getKey()), s);
      }
    }
    return r;
  }

  /**
   * @param _color la couleur
   * @return la chaine representant cette couleur
   */
  public String getColorProperty(final Color _color){
    final String s = Integer.toHexString(_color.getRGB());
    return ("000000" + s).substring(s.length());
  }

  /**
   * @param _color la chaine
   * @return la couleur correspondante ou null si chaine non correct.
   */
  public Color getColorProperty(final String _color){

    try {
      return new Color(Integer.parseInt(_color, 16));
    }
    catch (final Exception ex) {}

    return null;
  }

  /**
   * @param _name le nom de la palette
   * @return copie des couleurs des couleurs correspondantes. null si aucune
   */
  public PaletteCouleurDiscontinu getPalette(final String _name){
    return (PaletteCouleurDiscontinu) namePalette_.get(_name);
  }

  /**
   * @param _f les propriétés lues dans un fichier. Dans ce cas, pas de préfix
   * @return nom->Color[]
   */
  public Map loadPaletteFromExtFile(final Properties _f){
    final Map m = new HashMap();
    final String prefix = getPrefPrefix();
    for (final Enumeration e = _f.keys(); e.hasMoreElements();) {
      final String key = (String) e.nextElement();
      if (key.startsWith(prefix)) {
        final  String name = key.substring(prefix.length());
        if (!m.containsKey(name)) {
          final  Color[] cs = loadColor(_f.getProperty(key, null));
          if (cs != null) {
            m.put(name, new PaletteCouleurDiscontinu(cs, name));
          }
        }
      }
    }
    return m;
  }

  /**
   * @param _name le nom de la palette a ne plus sauvegardee/utilisee.
   */
  public void removePalette(final String _name){
    if(namePalette_.size()==1) {
      return;
    }
    namePalette_.remove(_name);
  }

  /**
   * @return toutes les palettes sauvegardees.
   */
  public PaletteCouleurDiscontinu[] getAllPalette(){
    final PaletteCouleurDiscontinu[] r = new PaletteCouleurDiscontinu[namePalette_.size()];
    namePalette_.values().toArray(r);
    Arrays.sort(r, new PaletteTitleComparator());
    return r;
  }

  void setPalette(final PaletteCouleurDiscontinu[] _cs){
    namePalette_.clear();
    for (int i = _cs.length - 1; i >= 0; i--) {
      namePalette_.put(_cs[i].getTitle(), _cs[i]);
    }

  }

  PaletteCouleurDiscontinu[] getAllPaletteCopy(){
    final  PaletteCouleurDiscontinu[] r = new PaletteCouleurDiscontinu[namePalette_.size()];
    int i = 0;
    for (final Iterator it = namePalette_.values().iterator(); it.hasNext();) {
      final  PaletteCouleurDiscontinu p = (PaletteCouleurDiscontinu) it.next();
      r[i++] = new PaletteCouleurDiscontinu(copy(p.getCs()), p.getTitle());
    }
    Arrays.sort(r, new PaletteTitleComparator());
    return r;
  }

  /**
   * Sauve les palettes dans les préferences.
   */
  public void savePref(){
    cleanPref();
    final  BuPreferences preferences = getPref();
    final  String prefix = getPrefPrefix();
    for (final Iterator it = namePalette_.entrySet().iterator(); it.hasNext();) {
      final Map.Entry e = (Map.Entry) it.next();
      final  String s = toString(((PaletteCouleurDiscontinu) e.getValue()).getCs());
      if (s != null) {
        preferences.putStringProperty(prefix + (String) e.getKey(), s);
      }
    }
    //preferences.writeIniFile();
  }

  /**
   * @param _d la palette modifiee
   * @param _title le nouveau titre
   */
  public void modifyPalette(final PaletteCouleurDiscontinu _d,final String _title){
    if (!_d.getTitle().equals(_title)) {
      namePalette_.remove(_d.getTitle());
      _d.setTitle(_title);
      namePalette_.put(_title, _d);
    }
  }

  /**
   * Ajoute la palette avec le titre _name si aucune palette avec le meme nom.
   * @param _name le nom de la palette
   * @param _cs les couleurs
   */
  public void setPalette(final String _name,final Color[] _cs){
    final  String name = FuLib.clean(_name);
    if (namePalette_.containsKey(_name)) {
      return;
    }
    namePalette_.put(name, new PaletteCouleurDiscontinu(copy(_cs), _name));
  }

}
