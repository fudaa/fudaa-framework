/*
 * @creation 2000-05-15
 *
 * @modification $Date: 2007-05-04 13:49:42 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.trace.BPlageInterface;
import org.fudaa.ebli.trace.TraceIcon;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

/**
 * Une palette de couleurs associ�es � des plages de valeurs. Les valeurs hors plages sont consid�r�es comme "autres".
 *
 * @author Bertrand Marchand TODO refaire en widget
 * @version $Id: BPalettePlageLegende.java,v 1.23 2007-05-04 13:49:42 deniger Exp $
 */
public class BPalettePlageLegende extends JPanel implements BPalettePlageListener {
  @Override
  public void setFont(final Font _font) {
    boolean first = super.getFont() == null;
    int oldSize = super.getFont() == null ? 0 : super.getFont().getSize();
    super.setFont(_font);
    int delta = oldSize == 0 ? 0 : _font.getSize() - oldSize;

    if (pnMainPanel != null) {
      internSetFont(pnMainPanel, _font);
    }
    if (pnTitre_ != null) {
      internSetFont(pnTitre_, _font);
    }
    if (reduit_ != null) {
      reduit_.setFont(_font);
    }
    if (!first) {
      int componentCount = pnPlages.getComponentCount();
      for (int i = 0; i < componentCount; i++) {
        Component component2 = pnPlages.getComponent(i);
        Font oldFont = component2.getFont();
        int newSize = Math.max(6, oldFont.getSize() + delta);
        component2.setFont(new Font(oldFont.getName(), _font.getStyle(), newSize));
      }
    }
  }

  public void addUserComponent(final JComponent _c) {
    if (_c != null) {
      pnTitre_.add(_c);
      pnTitre_.doLayout();
    }
  }

  public void removeUserComponent(final JComponent _c) {
    if (_c != null) {
      pnTitre_.remove(_c);
      pnTitre_.doLayout();
    }
  }

  private void internSetFont(final JComponent _c, final Font _font) {
    for (int i = _c.getComponentCount() - 1; i >= 0; i--) {
      _c.getComponent(i).setFont(_font);
    }
  }

  BPalettePlageInterface model_;
  // Le symbole par d�faut pour les plages
  private final JLabel lbTitre_ = new JLabel();
  private final JPanel pnMainPanel = new JPanel();
  private JLabel lbAutres_ = new JLabel();
  BuPanel pnTitre_;
  private final JLabel lbSSTitre_ = new JLabel();
  final BuPanel pnPlages;

  /**
   * Cr�ation d'une palette vide.
   */
  public BPalettePlageLegende() {
    setLayout(new BuBorderLayout(0, 0, true, false));
    final Border bdPlages = BorderFactory.createEmptyBorder(5, 5, 5, 5);
    pnTitre_ = new BuPanel();
    pnTitre_.setOpaque(false);
    pnTitre_.setLayout(new BuVerticalLayout(4, true, true));
    lbTitre_.setHorizontalAlignment(SwingConstants.CENTER);
    lbTitre_.setFont(getFont());
    lbTitre_.setAlignmentX((float) 0.5);
    final BuGridLayout lyPlages = new BuGridLayout();
    lyPlages.setColumns(1);
    lyPlages.setHgap(5);
    pnMainPanel.setLayout(lyPlages);
    pnMainPanel.setBorder(bdPlages);
    pnMainPanel.setOpaque(false);
    pnPlages = new BuPanel(new GridBagLayout());
    pnPlages.setOpaque(false);
    pnPlages.setFont(getFont());

    pnMainPanel.add(pnPlages);
    lbSSTitre_.setHorizontalAlignment(SwingConstants.CENTER);
    lbSSTitre_.setFont(getFont());
    lbSSTitre_.setAlignmentX((float) 0.5);
    this.setOpaque(false);

    pnTitre_.add(lbTitre_, null);
    pnTitre_.add(lbSSTitre_, null);
    this.add(pnTitre_, BorderLayout.NORTH);
    add(pnMainPanel, BorderLayout.CENTER);
  }

  JComponent reduit_;

  /**
   * Construit la legende et la met a jour.
   *
   * @param _model le modele
   */
  public BPalettePlageLegende(final BPalettePlageAbstract _model) {
    this();
    setModel(_model);
  }

  class PlageTableModel extends AbstractListModel {
    @Override
    public Object getElementAt(final int _index) {
      return model_.getPlageInterface(_index);
    }

    public void fireAllRemoved(final int _oldSize) {

      if (_oldSize > 0) {
        fireIntervalRemoved(this, 0, _oldSize - 1);
      }
    }

    public void fireAllAdd() {

      if (model_ == null) {
        fireIntervalAdded(this, 0, 0);
      } else {
        fireIntervalAdded(this, 0, model_.getNbPlages());
      }
    }

    public void fireModified(final int _i) {

      fireContentsChanged(this, _i, _i);
    }

    @Override
    public int getSize() {
      return model_ == null ? 0 : model_.getNbPlages();
    }
  }

  /**
   * Un cell renderer pour afficher une plage.
   *
   * @author Fred Deniger
   * @version $Id: BPalettePlageLegende.java,v 1.23 2007-05-04 13:49:42 deniger Exp $
   */
  public static class PlageCellRenderer extends JLabel implements ListCellRenderer {
    TraceIcon icone_;

    public PlageCellRenderer() {
      icone_ = new TraceIcon();
      setIcon(icone_);
      setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(final JList _list, final Object _value, final int _index, final boolean _isSelected,
                                                  final boolean _cellHasFocus) {
      if (_isSelected) {
        super.setForeground(_list.getSelectionForeground());
        super.setBackground(_list.getSelectionBackground());
      } else {
        super.setForeground(_list.getForeground());
        super.setBackground(_list.getBackground());
      }
      setFont(_list.getFont());
      setHorizontalTextPosition(JLabel.LEFT);
      if (_value instanceof BPlageInterface) {
        final BPlageInterface p = (BPlageInterface) _value;
        icone_.setCouleur(p.getCouleur());
        icone_.setTaille(p.getIconeTaille());
        icone_.setType(p.getIconeType());
        setText(p.getLegende());
        setToolTipText(p.getLegende());
      } else {
        final String val = _value == null ? CtuluLibString.EMPTY_STRING : _value.toString();
        setText(val);
        setToolTipText(val);
        icone_.setType(TraceIcon.RIEN);
      }

      return this;
    }
  }

  /**
   * @param _model le nouveau modele utilise pour dessiner la legende
   */
  public final void setModel(final BPalettePlageInterface _model) {

    if (model_ != _model) {
      model_ = _model;
      if (_model != null) {
        _model.setPlageListener(this);
      }
      if (reduit_ != null) {
        pnMainPanel.remove(reduit_);
      }
      pnMainPanel.remove(pnPlages);
      JComponent cp = pnPlages;
      if (model_ != null && (model_.isReduit() || (model_.isReduitEnable() && model_.getNbPlages() > 15))) {
        cp = model_.createReduit();
      }
      if (cp == null) {
        cp = pnPlages;
      } else {
        cp.setFont(getFont());
      }

      pnMainPanel.add(cp, 0);
      updateAllLegende();
    }
  }

  public boolean isReduitEnable() {
    return model_ != null && model_.isReduitEnable();
  }

  public boolean isReduit() {
    return model_ != null && model_.isReduit();
  }

  /**
   * @return le model utilise
   */
  public BPalettePlageInterface getModel() {
    return model_;
  }

  /**
   * Redessine enti�rment la legende. Appele lorsque la structure de la palette a ete modifiee
   */
  public final void updateAllLegende() {
    if (model_ != null) {
      updateTitres();
      updatePlages();
      if (model_.isAutresVisible()) {
        lbAutres_ = buildLabel(model_.getPlageAutresInterface(), true);
        pnMainPanel.add(lbAutres_);
      } else {
        pnMainPanel.remove(lbAutres_);
      }
    }
    revalidate();
    doLayout();
    repaint();
  }

  private void updatePlages() {
    pnPlages.removeAll();
    GridBagConstraints c = new GridBagConstraints();
    int nb = model_.getNbPlages();
    for (int i = 0; i < nb; i++) {
      c.gridx = 0;
      c.gridy = i;
      c.ipadx = 3;
      c.anchor = GridBagConstraints.LINE_END;
      BPlageInterface p = model_.getPlageInterface(i);
      pnPlages.add(buildLabel(p, false), c);
      c.gridx++;
      if (p.isLegendCustomized()) {
        c.gridwidth = 3;
        c.anchor = GridBagConstraints.LINE_START;
        pnPlages.add(buildLabel(p.getLegende()), c);
        c.gridwidth = 1;
      } else {
        c.anchor = GridBagConstraints.LINE_START;
        pnPlages.add(buildLabel(p.getMinString()), c);
        c.gridx++;
        c.anchor = GridBagConstraints.CENTER;

        pnPlages.add(buildLabel(p.getSep()), c);
        // on remet dans l'etat
        c.anchor = GridBagConstraints.LINE_END;
        c.gridx++;
        pnPlages.add(buildLabel(p.getMaxString()), c);
      }
    }
  }

  private JLabel buildLabel(String txt) {
    JLabel res = new JLabel(txt);
    res.setFont(getFont());
    res.setOpaque(false);
    return res;
  }

  private JLabel buildLabel(final BPlageInterface _p, boolean setText) {
    final JLabel r = new JLabel();
    r.setFont(getFont());
    final TraceIcon ic = new TraceIcon(_p.getIconeType(), _p.getIconeTaille());
    ic.setCouleur(_p.getCouleur());
    r.setIcon(ic);
    if (setText) {
      r.setText(_p.getLegende());
    }
    r.setToolTipText(_p.getLegende());
    return r;
  }

  protected final void updateTitres() {
    if (model_ == null) {
      return;
    }
    if (lbTitre_ != null) {
      lbTitre_.setText(model_.getTitre());
    }
    if (lbSSTitre_ != null) {
      lbSSTitre_.setText(model_.getSousTitre());
      lbSSTitre_.setVisible(model_.isSubTitleVisible());
    }
  }

  protected final void updateLabel(final int _idx) {
    updatePlages();
  }

  protected final void updateLabelAutre() {
    if (lbAutres_ != null) {
      updateLabel(lbAutres_, model_.getPlageAutresInterface());
    }
  }

  protected final void updateLabel(final JLabel _lb, final BPlageInterface _p) {
    final TraceIcon ic = (TraceIcon) _lb.getIcon();
    ic.setCouleur(_p.getCouleur());
    ic.setTaille(_p.getIconeTaille());
    ic.setType(_p.getIconeType());
    _lb.setText(_p.getLegende());
    _lb.repaint();
  }

  @Override
  public void allPaletteModified(final BPalettePlageInterface _src) {
    if (_src == model_) {
      pnMainPanel.removeAll();
      reduit_ = null;
      if (_src != null && _src.isReduitEnable() && model_.isReduit()) {
        reduit_ = model_.createReduit();
        pnMainPanel.add(reduit_);
      } else {
        pnMainPanel.add(pnPlages);
      }
      updateAllLegende();
    }
  }

  @Override
  public void plageModified(final BPalettePlageInterface _src, final int _i) {

    if (_src == model_) {
      updateLabel(_i);
    }
  }

  @Override
  public void paletteTitreModified(final BPalettePlageInterface _src) {

    if (_src == model_) {
      updateTitres();
    }
  }

  public void setUseReduit(final boolean _useReduit) {
    if (reduit_ != null) {
      pnMainPanel.remove(reduit_);
    }
    pnMainPanel.remove(pnPlages);
    JComponent cp = null;
    reduit_ = null;
    if (model_ != null && model_.isReduitEnable()) {
      model_.setReduit(_useReduit);
      if (_useReduit) {
        reduit_ = model_.createReduit();
        reduit_.setFont(getFont());
        cp = reduit_;
      } else {
        cp = pnPlages;
      }
    }
    if (cp == null) {
      cp = pnPlages;
    }
    pnMainPanel.add(cp, 0);
    pnMainPanel.invalidate();
    allPaletteModified(model_);
  }
}
