/*
 * @creation     1998-10-20
 * @modification $Date: 2006-09-19 14:55:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.palette;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ebli.trace.TraceLigne;

/**
 * Une palette de trait.
 *
 * @version $Revision: 1.11 $ $Date: 2006-09-19 14:55:51 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class BPaletteTrait extends JPanel implements ListSelectionListener {
  // donnees membres publiques
  // donnees membres privees
  JList combo_;

  // Constructeurs
  public BPaletteTrait() {
    super();
    final Object[] traits = new Integer[4];
    traits[0] = (new Integer(TraceLigne.LISSE));
    traits[1] = (new Integer(TraceLigne.POINT_MARQUE));
    traits[2] = (new Integer(TraceLigne.POINTILLE));
    traits[3] = (new Integer(TraceLigne.TIRETE));
    // traits[4]=(new Integer(TraceLigne.MIXTE));
    combo_ = new JList(traits);
    combo_.setSelectionMode(0);
    combo_.setCellRenderer(new PaletteTraitCellRenderer());
    combo_.addListSelectionListener(this);
    add(combo_);
  }

  // Methodes publiques
  // **********************************************
  // EVENEMENTS
  // **********************************************
  // ActionPerformed
  @Override
  public void valueChanged(final ListSelectionEvent _evt) {
    if (!_evt.getValueIsAdjusting()) {
      setTypeTrait(((Integer) combo_.getSelectedValue()).intValue());
    }
  }
  // **********************************************
  // PROPRIETES EXTERNES
  // **********************************************
  // Propriete typeTrait
  private int typeTrait_;

  public int getTypeTrait() {
    return typeTrait_;
  }

  public void setTypeTrait(final int _typeTrait) {
    if (typeTrait_ != _typeTrait) {
      final int vp = typeTrait_;
      typeTrait_ = _typeTrait;
      firePropertyChange("TypeTrait", vp, typeTrait_);
    }
  }
}

/**
 * Le ListCellRenderer de BPaletteTrait.
 *
 * @version $Id: BPaletteTrait.java,v 1.11 2006-09-19 14:55:51 deniger Exp $
 * @author Axel von Arnim
 */
class PaletteTraitCellRenderer implements ListCellRenderer {
  @Override
  public Component getListCellRendererComponent(final JList _list, final Object _value, final int _index, final boolean _isSelected,
                                                final boolean _cellHasFocus) {
    return new PaletteTraitCellComponent(((Integer) _value).intValue(), _isSelected, _list);
  }
}

/**
 * Le ListCellComponent de BPaletteTrait.
 *
 * @version $Id: BPaletteTrait.java,v 1.11 2006-09-19 14:55:51 deniger Exp $
 * @author Axel von Arnim
 */
class PaletteTraitCellComponent extends JComponent {
  int typeTrait_;

  public PaletteTraitCellComponent(final int _typeTrait, final boolean _selected, final JList _list) {
    super();
    typeTrait_ = _typeTrait;
    if (_selected) {
      setOpaque(true);
      setBackground(_list.getSelectionBackground());
      setForeground(_list.getSelectionForeground());
    }
    setPreferredSize(new Dimension(50, 20));
  }

  @Override
  public void paint(final Graphics _g) {
    _g.setColor(getBackground());
    _g.fillRect(0, 0, getWidth(), getHeight());
    final TraceLigne t = new TraceLigne();
    t.setTypeTrait(typeTrait_);
    t.setCouleur(getForeground());
    t.dessineTrait((Graphics2D) _g, 10, 10, 40, 10);
    super.paintBorder(_g);
  }
}
