/*
 * @creation     1998-03-25
 * @modification $Date: 2006-09-19 14:55:50 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;
import java.awt.Point;
/**
 * Un noeud 3D.
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 14:55:50 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public final class GrNoeud extends GrObjet implements GrContour {
  public GrPoint point_;
  public GrNoeud() {
    this(0., 0., 0.);
  }

  public GrNoeud(final GrPoint _pt) {
    point_= _pt;
  }
  public GrNoeud(final double _x, final double _y, final double _z) {
    this(new GrPoint(_x, _y, _z));
  }
  @Override
  public  String toString() {
    return "GrNoeud(" + point_.x_ + ", " + point_.y_ + ", " + point_.z_ + ")";
  }
  public  GrNoeud addition(final GrVecteur _v) {
    return new GrNoeud(point_.addition(_v));
  }
  public  GrNoeud soustraction(final GrVecteur _v) {
    return new GrNoeud(point_.soustraction(_v));
  }
  public  GrVecteur soustraction(final GrNoeud _n) {
    return point_.soustraction(_n.point_);
  }
  public  boolean comparaison(final GrNoeud _n) {
    return point_.comparaison(_n.point_);
  }
  public  double distance(final GrNoeud _n) {
    return point_.distance(_n.point_);
  }
  public  double distanceXY(final GrNoeud _n) {
    return point_.distanceXY(_n.point_);
  }
  public  GrNoeud applique(final GrMorphisme _t) {
    return new GrNoeud(point_.applique(_t));
  }
  @Override
  public  void autoApplique(final GrMorphisme _t) {
    point_.autoApplique(_t);
  }
  /**
   * Implémentation GrObjet.
   */
  @Override
  public  GrBoite boite() {
    return point_.boite();
  }
  @Override
  public  GrPoint[] contour() {
    return point_.contour();
  }
  /**
   * Indique si l'objet est sélectionné pour un point donné.<p>
   *
   *
   * @author b.marchand Le 26/10/2000
   */
  @Override
  public  boolean estSelectionne(
    final GrMorphisme _ecran,
    final double _dist,
    final Point _pt) {
    return point_.estSelectionne(_ecran, _dist, _pt);
  }
}
