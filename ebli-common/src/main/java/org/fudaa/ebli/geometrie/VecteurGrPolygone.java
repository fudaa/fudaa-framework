/*
 *  @creation     2002-06-25
 *  @modification $Date: 2006-09-19 14:55:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;

import org.fudaa.ebli.commun.VecteurGenerique;

/**
 * vecteur de GrPolygone. Cette classe peut �tre utilisee � la place de ListeGrPolygone. Moins performant pour les
 * insertions mais moins gourmand en m�moire : des noeuds de liaisons ne sont pas cr�es.
 *
 * @version $Id: VecteurGrPolygone.java,v 1.7 2006-09-19 14:55:50 deniger Exp $
 * @author Fred Deniger
 */
public final class VecteurGrPolygone extends VecteurGenerique {
  /** */
  public VecteurGrPolygone() {
    super();
  }

  /**
   * Renvoie l'element d'index <code>_j</code>.
   *
   * @param _j l'index de l'objet a renvoyer.
   * @return l'objet d'index <code>_j</code>.
   */
  public GrPolygone renvoie(final int _j) {
    return (GrPolygone) internRenvoie(_j);
  }

  /**
   * @return le tableau correspondant
   */
  public GrPolygone[] tableau() {
    final GrPolygone[] r = new GrPolygone[nombre()];
    v_.toArray(r);
    return r;
  }

  public void tableau(final GrPolygone[] _o) {
    super.internTableau(_o);
  }

  public void ajoute(final GrPolygone _o) {
    super.ajoute(_o);
  }

  public void insere(final GrPolygone _o, final int _j) {
    super.insere(_o, _j);
  }

  public void remplace(final GrPolygone _o, final int _j) {
    super.remplace(_o, _j);
  }

  public void enleve(final GrPolygone _o) {
    super.enleve(_o);
  }

  public int indice(final GrPolygone _o) {
    return super.indice(_o);
  }

  public boolean contient(final GrPolygone _o) {
    return super.contient(_o);
  }
}
