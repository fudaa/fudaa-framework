/*
 * @creation     1998-03-25
 * @modification $Date: 2006-09-19 14:55:50 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;
import org.fudaa.ebli.mathematiques.Fonctions;
/**
 * Un triangle 3D.
 *
 * @version      $Id: GrTriangle.java,v 1.7 2006-09-19 14:55:50 deniger Exp $
 * @author       Guillaume Desnoix
 */
public final class GrTriangle extends GrPolygone {
  public GrTriangle() {}
  public GrTriangle(final GrPoint _p1, final GrPoint _p2, final GrPoint _p3) {
    sommets_.ajoute(_p1);
    sommets_.ajoute(_p2);
    sommets_.ajoute(_p3);
  }
  @Override
  public  int nombre() {
    return 3;
  }
  @Override
  public  String toString() {
    return "GrTriangle("
      + sommet(0)
      + ", "
      + sommet(1)
      + ", "
      + sommet(2)
      + ")";
  }
  @Override
  public  boolean estConvexe() {
    return true;
  }
  @Override
  public  boolean estSimple() {
    return true;
  }
  public  boolean estPlat() {
    return Math.abs(arete(0).normalise().produitScalaire(arete(1).normalise()))
      == 1.;
  }
  public  double equilateraliteXY() {
    final double p= perimetreXY();
    return 10.39 * aireXY() / (p * p);
  }
  public  boolean estDedansXY(final GrPoint _p) {
    double s0= 0.;
    for (int i= 0; i < 3; i++) {
      final double s=
        Fonctions.sign(arete(i).produitSinusXY(_p.soustraction(sommet(i))));
      if (s != 0.) {
        s0= s;
        break;
      }
    }
    if (s0 == 0.) {
      return false;
    }
    for (int i= 0; i < 3; i++) {
      final double s=
        Fonctions.sign(arete(i).produitSinusXY(_p.soustraction(sommet(i))));
      if ((s != 0.) && (s != s0)) {
        return false;
      }
    }
    return true;
  }
  @Override
  public  double distanceXY(final GrPoint _p) {
    if (estDedansXY(_p)) {
      return 0.;
    }
    return Math.min(
      cote(0).distanceXY(_p),
      Math.min(cote(1).distanceXY(_p), cote(2).distanceXY(_p)));
  }
  public  double aireXY() {
    final double p= arete(0).produitSinusXY(arete(2));
    return Math.abs(p) / 2.;
  }
  public  GrVecteur coordonneesXY(final GrPoint _p) {
    if (!estDedansXY(_p)) {
      return null;
    }
    final double a= aireXY();
    final double u= (new GrTriangle(_p, sommet(1), sommet(2))).aireXY() / a;
    final double v= (new GrTriangle(_p, sommet(0), sommet(2))).aireXY() / a;
    final double w= (new GrTriangle(_p, sommet(0), sommet(1))).aireXY() / a;
    return new GrVecteur(u, v, w);
  }
  @Override
  public  GrPolygone applique(final GrMorphisme _t) {
    final GrPoint a= sommet(0).applique(_t);
    final GrPoint b= sommet(1).applique(_t);
    final GrPoint c= sommet(2).applique(_t);
    return new GrTriangle(a, b, c);
  }
  @Override
  public  void autoApplique(final GrMorphisme _t) {
    sommet(0).autoApplique(_t);
    sommet(1).autoApplique(_t);
    sommet(2).autoApplique(_t);
  }
}
