#!/bin/sh

# Liste

jgen ../general/Liste.jgen GrPoint
jgen ../general/Liste.jgen GrPolygone
jgen ../general/Liste.jgen GrPolyligne
jgen ../general/Liste.jgen GrSegment
jgen ../general/Liste.jgen GrTriangle
jgen ../general/Liste.jgen GrVecteur

# Bertrand

jgen ../general/Liste.jgen GrContour
jgen ../general/Liste.jgen GrElement
jgen ../general/Liste.jgen GrNoeud
jgen ../general/Liste.jgen GrSymbole
jgen ../general/Liste.jgen GrPositionRelativePoint
jgen ../general/Liste.jgen GrPositionRelativeSegment
