/*
 * @creation     1998-03-25
 * @modification $Date: 2006-11-20 08:40:16 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;

import org.locationtech.jts.geom.CoordinateSequence;
import java.awt.Point;
import javax.swing.DefaultListSelectionModel;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISCoordinateSequenceContainerInterface;
import org.fudaa.ctulu.gis.GISCoordinateSequenceFactory;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPolyligne;

/**
 * Une polyligne 3D.
 * 
 * @version $Id: GrPolyligne.java,v 1.12 2006-11-20 08:40:16 deniger Exp $
 * @author Guillaume Desnoix
 */
public class GrPolyligne extends GrObjet implements GrContour, Cloneable {

  // public ListeGrPoint sommets;
  public VecteurGrPoint sommets_;

  public GrPolyligne() {
    sommets_ = new VecteurGrPoint();
  }
  
  @Override
  public GrPolyligne clone() {
    try {
      GrPolyligne o=(GrPolyligne)super.clone();
      o.sommets_=(VecteurGrPoint)sommets_.clone();
      return o;
    }
    // Ne doit pas se produire.
    catch (CloneNotSupportedException e) {
      throw new InternalError();
    }
  }



  /**
   * Cr�ation d'une polyligne � partir d'une sequence.
   * @param _seq La sequence.
   */
  public GrPolyligne(CoordinateSequence _seq) {
    this();
    boolean bz=_seq.getDimension()>2;
    
    for (int i=0; i<_seq.size(); i++) {
      sommets_.ajoute(new GrPoint(_seq.getOrdinate(i, 0), _seq.getOrdinate(i, 1), bz ? _seq.getOrdinate(i, 2):0));
    }
  }

  public GrPolyligne(GISCoordinateSequenceContainerInterface _geom) {
    this(_geom.getCoordinateSequence());
  }

  public final int nombre() {
    return sommets_.nombre();
  }

  public boolean estSimple() {
    final int n = nombre();
    if (n < 4) {
      return true;
    }
    for (int i = 0; i < n - 2; i++) {
      for (int j = i + 2; j < n - 1; j++) {
        if (segment(i).intersectXY(segment(j))) {
          return false;
        }
      }
    }
    return true;
  }

  public final GrPoint sommet(final int _i) {
    final int n = nombre();
    if (n == 0) {
      return null;
    }
    return sommets_.renvoie(_i);
  }

  public final boolean copieSommet(final GrPoint _p, final int _i) {
    if ((_i < 0) || (_i >= nombre())) {
      return false;
    }
    return sommets_.copie(_p, _i);
  }

  public final GrVecteur vecteur(final int _i) {
    return sommet(_i + 1).soustraction(sommet(_i));
  }

  public final void copieSegment(final GrSegment _s, final int _i) {
    if (_s.o_ == null) {
      _s.o_ = new GrPoint();
    }
    copieSommet(_s.o_, _i);
    if (_s.e_ == null) {
      _s.e_ = new GrPoint();
    }
    copieSommet(_s.e_, _i + 1);
  }

  public final GrSegment segment(final int _i) {
    return new GrSegment(sommet(_i), sommet(_i + 1));
  }

  public final double angle(final int _i) {
    return Math.asin(vecteur(_i).normalise().produitSinusXY(vecteur(_i - 1).normalise()));
  }

  @Override
  public final String toString() {
    return "GrPolyligne(" + nombre() + " sommets)";
  }

  /**
   * Impl�mentation GrObjet.
   */
  @Override
  public final GrBoite boite() {
    return sommets_.boite();
  }

  /**
   * Copie dans <code>_b</code> la boite de cette polyligne.
   */
  public final GrBoite boite(final GrBoite _b) {
    return sommets_.boite(_b);
  }

  public final double longueurXY() {
    double p = 0.;
    final int n = nombre();
    for (int i = 0; i < n - 1; i++) {
      p += vecteur(i).normeXY();
    }
    return p;
  }

  public final double longueur() {
    double p = 0.;
    final int n = nombre();
    for (int i = 0; i < n - 1; i++) {
      p += vecteur(i).norme();
    }
    return p;
  }

  /*
   * public final boolean comparaison(GrPolyligne p) { return centre().comparaison(p.centre()); }
   */
  public final GrPolyligne applique(final GrMorphisme _t) {
    final GrPolyligne r = new GrPolyligne();
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      r.sommets_.ajoute(sommet(i).applique(_t));
    }
    return r;
  }

  @Override
  public final void autoApplique(final GrMorphisme _t) {
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      sommet(i).autoApplique(_t);
    }
  }

  public final GrPolyligne fragmente(final double _ds) {
    final GrPolyligne r = new GrPolyligne();
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      final GrPoint p = sommet(i);
      final GrVecteur v = vecteur(i);
      final int s = (int) (v.norme() / _ds);
      r.sommets_.ajoute(p);
      for (int j = 1; j < s; j++) {
        r.sommets_.ajoute(p.addition(v.multiplication(((double) j) / ((double) s))));
      }
    }
    return r;
  }

  /**
   * Retourne le point milieu de la polyligne.
   * 
   * @author Bertrand Marchand
   */
  public GrPoint pointMilieu() {
    return pointDAbscisse(0.5);
  }

  /**
   * Retourne un point situ� � une abscisse curviligne donn�e du debut de la polyligne.
   * 
   * @author Bertrand Marchand
   * @param _abscisse L'abscisse sur la polyligne, comprise entre [0,1].
   * @return Le point. <I>null </I> si l'abscisse n'est pas comprise entre [0,1]
   */
  public GrPoint pointDAbscisse(final double _abscisse) {
    if (_abscisse < 0 || _abscisse > 1) {
      return null;
    }
    int i;
    double s = _abscisse * longueur();
    double norme;
    GrPoint p1;
    GrPoint p2;
    for (i = 0; i < nombre() - 1; i++) {
      norme = vecteur(i).norme();
      if (norme >= s || i == nombre() - 2) {
        s /= norme;
        break;
      }
      s -= norme;
    }
    p1 = sommet(i);
    p2 = sommet(i + 1);
    return new GrPoint(p1.x_ + (p2.x_ - p1.x_) * s, p1.y_ + (p2.y_ - p1.y_) * s, p1.z_ + (p2.z_ - p1.z_) * s);
  }

  /**
   * Retourne l'abscisse du sommet _i.
   * 
   * @author Bertrand Marchand
   * @param _i Sommet sur la polyligne
   * @param _norme True : L'abscisse retourn� est comprise entre [0,1], false : suivant la longueur de la polyligne
   * @return L'abscisse sur la polyligne.
   */
  public double abscisseDe(final int _i, boolean _norme) {
    double abscisse = 0;
    for (int i = 0; i < _i; i++) {
      abscisse += vecteur(i).norme();
    }
    if (_norme)
      return Math.min(1, abscisse / longueur());
    else
      return abscisse;
  }
  
  /**
   * Retourne l'abscisse norm� du sommet _i.
   * 
   * @author Bertrand Marchand
   * @param _i Sommet sur la polyligne
   * @return L'abscisse sur la polyligne, comprise entre [0,1].
   */
  public double abscisseDe(final int _i) {
    return abscisseDe(_i, true);
  }

  /**
   * Retourne l'abscisse d'un point donn�. Si le point n'est pas sur la polyligne, l'abscisse retourn� est celui du
   * point de la polyligne dont la distance est minimale avec le point donn�.
   * 
   * @param _p Point
   * @return L'abscisse sur la polyligne, comprise entre [0,1].
   */
  public double abscisseDe(final GrPoint _p) {
    double abscisse = 0;
    double abscur = 0;
    double dmin = Double.MAX_VALUE;
    final int n = nombre();
    for (int i = 0; i < n - 1; i++) {
      final GrPoint ppp = segment(i).pointPlusProche(_p);
      final double d = ppp.distance(_p);
      if (d < dmin) {
        dmin = d;
        abscisse = abscur + ppp.soustraction(sommet(i)).norme();
        if (dmin == 0.) {
          break;
        }
      }
      abscur += vecteur(i).norme();
    }
    return Math.min(1, abscisse / longueur());
  }

  /**
   * Renvoie la distance entre la polyligne et un point.
   * 
   * @param _p un point XY
   * @return la distance
   */
  public final double distanceXY(final GrPoint _p) {
    double dmin = Double.MAX_VALUE;
    final int n = nombre();
    final double x = _p.x_;
    final double y = _p.y_;
    for (int i = n - 2; i >= 0; i--) {
      final double d = CtuluLibGeometrie.distanceFromSegment(sommet(i).x_, sommet(i).y_, sommet(i + 1).x_,
          sommet(i + 1).y_, x, y);

      // segment(i).distanceXY(p);
      if (d < dmin) {
        dmin = d;
        if (dmin == 0.) {
          break;
        }
      }
    }
    return dmin;
  }

  /**
   * Retourne le point appartenant � la polyligne le plus proche du point donn�.
   */
  public final GrPoint pointPlusProche(final GrPoint _p) {
    GrPoint r = null;
    double dmin = Double.MAX_VALUE;
    final int n = nombre();
    for (int i = 0; i < n - 1; i++) {
      final GrPoint q = segment(i).pointPlusProche(_p);
      final double d = q.distance(_p);
      if (d < dmin) {
        dmin = d;
        r = q;
        if (dmin == 0.) {
          break;
        }
      }
    }
    return r;
  }

  /**
   * Retourne le point appartenant � la polyligne le plus proche du point donn� dans le plan x,y.
   */
  public final GrPoint pointPlusProcheXY(final GrPoint _p) {
    GrPoint r = null;
    double dmin = Double.MAX_VALUE;
    final int n = nombre();
    for (int i = 0; i < n - 1; i++) {
      final GrPoint q = segment(i).pointPlusProcheXY(_p);
      final double d = q.distanceXY(_p);
      if (d < dmin) {
        dmin = d;
        r = q;
        if (dmin == 0.) {
          break;
        }
      }
    }
    return r;
  }

  /**
   * Renvoie la premiere intersection entre la polyligne et un segment.
   * 
   * @param _s un segment
   * @return un point ou null
   */
  public final GrPoint intersectionXY(final GrSegment _s) {
    final int n = nombre();
    for (int i = 0; i < n - 1; i++) {
      final GrPoint r = segment(i).intersectionXY(_s);
      if (r != null) {
        return r;
      }
    }
    return null;
  }

  /**
   * Renvoie la premiere intersection entre deux polylignes.
   * 
   * @param _p une polyligne
   * @return un point ou null
   */
  public final GrPoint intersectionXY(final GrPolyligne _p) {
    final int ni = nombre();
    final int nj = _p.nombre();
    for (int i = 0; i < ni - 1; i++) {
      for (int j = 0; j < nj - 1; j++) {
        final GrPoint r = segment(i).intersectionXY(_p.segment(j));
        if (r != null) {
          return r;
        }
      }
    }
    return null;
  }

  /**
   * Check if the 2 polylines have a common point, regarding the precision.
   * @author Adrien Hadoux
   */
  public GrPoint pointCommonWith(final GrPolyligne _p, double _tol) {
	  for(int i=0;i<_p.sommets_.nombre();i++){			
		  GrPoint point = _p.sommets_.renvoie(i);
		  for(int j=0;j<this.sommets_.nombre();j++){
			  GrPoint point2 = this.sommets_.renvoie(j);
			  if(Math.abs(point.x_ - point2.x_)<=_tol && Math.abs(point.y_ - point2.y_)<=_tol)
				  return point;
			  
	   }
	  }
		  return null;
  }
  
  /**
   * Renvoie la premiere intersection entre 2 polylignes, avec une tol�rance.
   * Attention : La tol�rance ne concerne que les points en bout de polyligne.
   * @param _p une polyligne
   * @param _tol La tol�rance.
   * @return un point ou null
   */
  public final GrPoint intersectionXY(final GrPolyligne _p, double _tol) {
	//-- ahx - step1 on cherche si il existe un point en commun entre pt et _p --//
	GrPoint pt = pointCommonWith(_p,_tol);    
	 if (pt!=null) return pt;
	 
	//-- step 2 on cherche une intersection entre pt et _P --//
	//-- ahx: Attention! intersectionXY() retourne null si les 2 polyligne ont exactement un point commun... --//   
	pt=intersectionXY(_p);
    if (pt!=null) return pt;

    
    
    // On calcule un point au plus proche en bout de polyligne.
    pt=pointPlusProcheXY(_p.sommet(0));
    if (_p.sommet(0).distanceXY(pt)<=_tol) return pt;
    pt=pointPlusProcheXY(_p.sommet(_p.sommets_.nombre()-1));
    if (_p.sommet(_p.sommets_.nombre()-1).distanceXY(pt)<=_tol) return pt;
    pt=_p.pointPlusProcheXY(sommet(0));
    if (sommet(0).distanceXY(pt)<=_tol) return pt;
    pt=_p.pointPlusProcheXY(sommet(sommets_.nombre()-1));
    if (sommet(sommets_.nombre()-1).distanceXY(pt)<=_tol) return pt;

    return null;
  }

  /**
   * Retourne un objet GrPolygone �quivalent.
   * 
   * @return Un GrPolygone equivalent.
   * @author b.marchand Le 28/08/2001
   */
  public final GrPolygone toGrPolygone() {
    final GrPolygone r = new GrPolygone();
    final int nb = nombre();
    for (int i = 0; i < nb; i++) {
      r.sommets_.ajoute(sommet(i));
    }
    return r;
  }
  
  /**
   * @return La s�quence de coordonn�es �quivalente
   */
  public final CoordinateSequence toCoordinateSequence() {
    CoordinateSequence seq=new GISCoordinateSequenceFactory().create(sommets_.nombre(), 3);
    int nb=seq.size();
    for (int i=0; i<nb; i++) {
      seq.setOrdinate(i, 0, sommets_.renvoieX(i));
      seq.setOrdinate(i, 1, sommets_.renvoieY(i));
      seq.setOrdinate(i, 2, sommets_.renvoieZ(i));
    }
    return seq;
  }
  
  /**
   * @return Une GIS polyligne �quivalente. Pas de contr�le sur le nombre de points de this.
   */
  public final GISPolyligne toGIS() {
    return GISGeometryFactory.INSTANCE.createLineString(toCoordinateSequence());
  }

  @Override
  public final GrPoint[] contour() {
    return sommets_.tableau();
  }

  /**
   * Indique si l'objet est s�lectionn� pour un point donn�.
   * 
   * @author b.marchand Le 26/10/2000
   * @param _ecran Le morphisme pour la transformation en coordonn�es �cran.
   * @param _dist La tol�rence en coordonn�es �cran pour laquelle on consid�re l'objet s�lectionn�.
   * @param _pt Le point de s�lection en coordonn�es �cran.
   * @return <I>true </I> L'objet est s�lectionn�. <I>false </I> L'objet n'est pas s�lectionn�.
   */
  @Override
  public final boolean estSelectionne(final GrMorphisme _ecran, final double _dist, final Point _pt) {
    final GrPolyligne p = applique(_ecran);
    final GrPoint pt = new GrPoint(_pt.getX(), _pt.getY(), 0);
    return p.distanceXY(pt) < _dist;
    // La m�thode Line2D.ptLineDist() ne semble pas fonctionner ???
    /*
     * for (int i=0; i <p.nombre()-1; i++) { GrPoint p1=p.sommet(i); GrPoint p2=p.sommet(i+1); if
     * (Line2D.ptLineDist(p1.x,p1.y,p2.x,p2.y,_pt.getX(),_pt.getY()) <_dist) return true; } return false;
     */
  }

  public final boolean estSelectionne(final GrMorphisme _ecran, final int _dist, final GrPoint _pt) {
    final GrPolyligne p = applique(_ecran);
    return p.distanceXY(_pt) < _dist;
  }

  public final static boolean estSelectionne(final GrPolyligne _p, final int _dist, final GrPoint _pt) {
    return _p.distanceXY(_pt) < _dist;
  }

  /**
   * Inverse le sens de parcours de la polyligne.
   */
  public final void inverse() {
    int nb = sommets_.nombre();
    for (int i=0; i<nb/2; i++) {
      GrPoint pt1=sommets_.renvoie(i);
      GrPoint pt2=sommets_.renvoie(nb-1-i);
      sommets_.remplace(pt2, i);
      sommets_.remplace(pt1, nb-1-i);
    }
  }
  
  /**
   * Renvoie la liste de points de <code>_p </code> contenus dans le polygone <code>_polyRef</code>.
   * 
   * @return null si aucun point n'appartient a <code>_polyRef</code>.
   */
  public static final DefaultListSelectionModel pointsSelectionnes(final GrPolyligne _poly, final GrPolygone _polyRef) {
    final GrBoite polyRefBoite = _polyRef.boite();
    if (!polyRefBoite.intersectXY(_poly.boite())) {
      return null;
    }
    final int nb = _poly.nombre() - 1;
    GrPoint p;
    final DefaultListSelectionModel r = new DefaultListSelectionModel();
    r.setValueIsAdjusting(true);
    for (int i = nb; i >= 0; i--) {
      p = _poly.sommet(i);
      if (p == null) {
        continue;
      }
      if (polyRefBoite.contientXY(p) && _polyRef.contientXY(p)) {
        r.addSelectionInterval(i, i);
      }
    }
    r.setValueIsAdjusting(false);
    return r.isSelectionEmpty() ? null : r;
  }
}
