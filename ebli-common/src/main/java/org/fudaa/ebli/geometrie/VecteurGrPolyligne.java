/*
 *  @creation     2002-06-25
 *  @modification $Date: 2006-09-19 14:55:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;

import org.fudaa.ebli.commun.VecteurGenerique;

/**
 * vecteur de GrPolygone. Cette classe peut �tre utilisee � la place de ListeGrPolygone. Moins performant pour les
 * insertions mais moins gourmand en m�moire : des noeuds de liaisons ne sont pas cr�es.
 *
 * @version $Id: VecteurGrPolyligne.java,v 1.1 2006-09-19 14:55:50 deniger Exp $
 * @author Fred Deniger
 */
public final class VecteurGrPolyligne extends VecteurGenerique {
  /** */
  public VecteurGrPolyligne() {
    super();
  }

  /**
   * Renvoie l'element d'index <code>_j</code>.
   *
   * @param _j l'index de l'objet a renvoyer.
   * @return l'objet d'index <code>_j</code>.
   */
  public GrPolyligne renvoie(final int _j) {
    return (GrPolyligne) internRenvoie(_j);
  }

  /**
   * @return le tableau correspondant
   */
  public GrPolyligne[] tableau() {
    final GrPolyligne[] r = new GrPolyligne[nombre()];
    v_.toArray(r);
    return r;
  }

  public void tableau(final GrPolyligne[] _o) {
    super.internTableau(_o);
  }

  public void ajoute(final GrPolyligne _o) {
    super.ajoute(_o);
  }

  public void insere(final GrPolyligne _o, final int _j) {
    super.insere(_o, _j);
  }

  public void remplace(final GrPolyligne _o, final int _j) {
    super.remplace(_o, _j);
  }

  public void enleve(final GrPolyligne _o) {
    super.enleve(_o);
  }

  public int indice(final GrPolyligne _o) {
    return super.indice(_o);
  }

  public boolean contient(final GrPolyligne _o) {
    return super.contient(_o);
  }
}
