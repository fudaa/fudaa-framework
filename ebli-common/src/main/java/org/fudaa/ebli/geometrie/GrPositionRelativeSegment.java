/*
 * @creation     1999-05-07
 * @modification $Date: 2006-09-19 14:55:50 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;

import java.awt.Point;

/**
 * Une position relative (en %) sur un GrSegment.
 *
 * @version $Revision: 1.9 $ $Date: 2006-09-19 14:55:50 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public final class GrPositionRelativeSegment extends GrObjet implements GrContour {
  public GrSegment origine_;
  public double position_;

  public GrPositionRelativeSegment() {
    origine_ = new GrSegment();
    position_ = 0.;
  }

  @Override
  public String toString() {
    return "GrPositionRelativeSegment(" + origine_ + ", " + position_ + ")";
  }

  public double x() {
    return (origine_.e_.x_ - origine_.o_.x_) * position_ + origine_.o_.x_;
  }

  public double y() {
    return (origine_.e_.y_ - origine_.o_.y_) * position_ + origine_.o_.y_;
  }

  public double z() {
    return (origine_.e_.z_ - origine_.o_.z_) * position_ + origine_.o_.z_;
  }

  public GrPoint toGrPoint() {
    return new GrPoint(x(), y(), z());
  }

  public GrPositionRelativeSegment applique(final GrMorphisme _t) {
    final GrPositionRelativeSegment r = new GrPositionRelativeSegment();
    r.position_ = position_;
    r.origine_ = origine_.applique(_t);
    return r;
  }

  @Override
  public void autoApplique(final GrMorphisme _t) {
    origine_.autoApplique(_t);
  }

  @Override
  public GrPoint[] contour() {
    return new GrPoint[] { new GrPoint(x(), y(), z()) };
  }

  /**
   * Indique si l'objet est s�lectionn� pour un point donn�.
   * <p>
   * La m�thode a �t� impl�ment�e dans le but de satisfaire l'interface GrContour. Son fonctionnement n'est peut �tre
   * pas exactement celui attendu.
   *
   * @author b.marchand Le 26/10/2000
   */
  @Override
  public boolean estSelectionne(final GrMorphisme _ecran, final double _dist, final Point _pt) {
    final GrPoint p = new GrPoint(x(), y(), z());
    return p.estSelectionne(_ecran, _dist, _pt);
  }
}
