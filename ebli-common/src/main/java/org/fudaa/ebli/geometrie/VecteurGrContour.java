/*
 *  @creation     2002-06-25
 *  @modification $Date: 2006-09-19 14:55:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;

import org.fudaa.ebli.commun.VecteurGenerique;

/**
 * vecteur de GrContour. Cette classe peut �tre utilisee � la place de ListeGrContour. Moins performant pour les
 * insertions mais moins gourmand en m�moire : des noeuds de liaisons ne sont pas cr�es.
 *
 * @version $Id: VecteurGrContour.java,v 1.7 2006-09-19 14:55:51 deniger Exp $
 * @author Fred Deniger
 */
public final class VecteurGrContour extends VecteurGenerique {
  public VecteurGrContour() {
    super();
  }

  /**
   * Renvoie l'element d'index <code>_j</code>.
   *
   * @param _j l'index de l'objet a renvoyer.
   * @return l'objet d'index <code>_j</code>.
   */
  public GrContour renvoie(final int _j) {
    return (GrContour) internRenvoie(_j);
  }

  /**
   * @return le tableau correspondant
   */
  public GrContour[] tableau() {
    final GrContour[] r = new GrContour[nombre()];
    v_.toArray(r);
    return r;
  }

  public void tableau(final GrContour[] _o) {
    super.internTableau(_o);
  }

  public void ajoute(final GrContour _o) {
    super.ajoute(_o);
  }

  public void insere(final GrContour _o, final int _j) {
    super.insere(_o, _j);
  }

  public void remplace(final GrContour _o, final int _j) {
    super.remplace(_o, _j);
  }

  public void enleve(final GrContour _o) {
    super.enleve(_o);
  }

  public int indice(final GrContour _o) {
    return super.indice(_o);
  }

  public boolean contient(final GrContour _o) {
    return super.contient(_o);
  }
}
