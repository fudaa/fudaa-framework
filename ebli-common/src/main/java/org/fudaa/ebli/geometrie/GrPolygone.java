/*
 * @creation     1998-03-25
 * @modification $Date: 2007-02-02 11:21:54 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;

import org.locationtech.jts.geom.CoordinateSequence;
import java.awt.Point;
import java.awt.Polygon;
import javax.swing.DefaultListSelectionModel;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.gis.GISCoordinateSequenceContainerInterface;
import org.fudaa.ctulu.gis.GISCoordinateSequenceFactory;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPolygone;
import org.fudaa.ebli.mathematiques.Fonctions;

/**
 * Un polygone 3D.
 * 
 * @version $Id: GrPolygone.java,v 1.20 2007-02-02 11:21:54 deniger Exp $
 * @author Guillaume Desnoix
 */
public class GrPolygone extends GrObjet implements GrContour {
  // public ListeGrPoint sommets;
  public VecteurGrPoint sommets_;

  public GrPolygone() {
    sommets_ = new VecteurGrPoint();
  }

  /**
   * Cr�ation d'un polygone � partir d'une sequence. La s�quence peut �tre ferm�e ou non.
   * @param _seq La sequence.
   */
  public GrPolygone(CoordinateSequence _seq) {
    this();
    
    boolean bz=_seq.getDimension()>2;
    boolean isClosed=bz ? 
        _seq.getCoordinate(0).equals3D(_seq.getCoordinate(_seq.size()-1)) : 
        _seq.getCoordinate(0).equals2D(_seq.getCoordinate(_seq.size()-1));
    int nb=_seq.size();
    if (isClosed)
      nb--;
    
    for (int i=0; i<nb; i++) {
      sommets_.ajoute(new GrPoint(_seq.getOrdinate(i, 0), _seq.getOrdinate(i, 1), bz ? _seq.getOrdinate(i, 2):0));
    }
  }

  public GrPolygone(GISCoordinateSequenceContainerInterface _geom) {
    this(_geom.getCoordinateSequence());
  }

  public int nombre() {
    return sommets_.nombre();
  }

  public final GrPoint sommet(final int _i) {
    int i = _i;
    final int n = nombre();
    if (n == 0) {
      return null;
    }
    while (i < 0) {
      i += n;
    }
    while (i >= n) {
      i -= n;
    }
    return sommets_.renvoie(i);
  }

  public final GrPoint sommetQuick(final int _i) {
    return sommets_.renvoie(_i);
  }

  public final boolean copieSommet(final GrPoint _p, final int _i) {
    int i = _i;
    final int n = nombre();
    if (n == 0) {
      return false;
    }
    while (i < 0) {
      i += n;
    }
    while (i >= n) {
      i -= n;
    }
    return sommets_.copie(_p, i);
  }

  public final GrVecteur arete(final int _i) {
    return sommet(_i + 1).soustraction(sommet(_i));
  }

  public final GrSegment cote(final int _i) {
    return new GrSegment(sommet(_i), sommet(_i + 1));
  }

  public final void copieCote(final GrSegment _s, final int _i) {
    if (_s.o_ == null) {
      _s.o_ = new GrPoint();
    }
    copieSommet(_s.o_, _i);
    if (_s.e_ == null) {
      _s.e_ = new GrPoint();
    }
    copieSommet(_s.e_, _i + 1);
  }

  public final double angle(final int _i) {
    return Math.asin(arete(_i).normalise().produitSinusXY(arete(_i - 1).normalise()));
  }

  @Override
  public String toString() {
    return "GrPolygone(" + nombre() + " sommets)";
  }

  /**
   * Impl�mentation GrObjet.
   */
  @Override
  public final GrBoite boite() {
    return sommets_.boite();
  }

  /**
   * Copie dans <code>_b</code> la boite de cette polyligne.
   */
  public final GrBoite boite(final GrBoite _b) {
    return sommets_.boite(_b);
  }

  public final GrVecteur normale() {
    return arete(0).produitVectoriel(arete(1)).normalise();
  }

  public final GrPoint barycentre() {
    return new GrPoint(sommets_.barycentre());
  }

  public final GrPoint centre() {
    final GrPoint r = new GrPoint();
    centre(r);
    return r;
  }

  public final void centre(final GrPoint _p) {
    double x = 0., y = 0., z = 0.;
    double qt = 0.;
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      final GrPoint p = sommet(i);
      final double q = arete(i - 1).norme() + arete(i).norme();
      x += q * p.x_;
      y += q * p.y_;
      z += q * p.z_;
      qt += q;
    }
    _p.setCoordonnees(x / qt, y / qt, z / qt);
  }

  public boolean estConvexe() {
    final int n = nombre();
    if (n < 4) {
      return true;
    }
    double s0 = 0.;
    for (int i = 0; i < n; i++) {
      final double s = Fonctions.sign(arete(i).produitSinusXY(arete(i + 1)));
      if (s != 0.) {
        s0 = s;
        break;
      }
    }
    if (s0 == 0.) {
      return false;
    }
    for (int i = 0; i < n; i++) {
      final double s = Fonctions.sign(arete(i).produitSinusXY(arete(i + 1)));
      if ((s != 0.) && (s != s0)) {
        return false;
      }
    }
    return true;
  }

  public boolean estSimple() {
    final int n = nombre();
    if (n < 4) {
      return true;
    }
    for (int i = 0; i < n - 2; i++) {
      for (int j = i + 2; j < n - 1; j++) {
        if (cote(i).intersectXY(cote(j))) {
          return false;
        }
      }
    }
    return true;
  }

  public boolean estCorrect() {
    final int n = nombre();
    if (n < 3) {
      return true;
    }
    // GrPoint c=centre();
    double a = 0.;
    for (int i = 0; i < n; i++) {
      double b = angle(i);
      while (b > Math.PI) {
        b -= Math.PI * 2.;
      }
      a += b;
    }
    return a >= 0.;
  }

  public final double perimetreXY() {
    double p = 0.;
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      p += arete(i).normeXY();
    }
    return p;
  }

  public final double diametreXY() {
    double dmax = 0.;
    final int n = nombre();
    for (int i = 0; i < n - 1; i++) {
      for (int j = i + 1; j < n; j++) {
        final double d = sommet(i).distanceXY(sommet(j));
        if (d > dmax) {
          dmax = d;
        }
      }
    }
    return dmax;
  }

  public final boolean comparaison(final GrPolygone _p) {
    return centre().comparaison(_p.centre());
  }

  public GrPolygone applique(final GrMorphisme _t) {
    final GrPolygone r = new GrPolygone();
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      r.sommets_.ajoute(sommet(i).applique(_t));
    }
    return r;
  }

  @Override
  public void autoApplique(final GrMorphisme _t) {
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      sommetQuick(i).autoApplique(_t);
    }
  }

  public final GrPolygone fragmente(final double _ds) {
    final GrPolygone r = new GrPolygone();
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      final GrPoint p = sommet(i);
      final GrVecteur v = arete(i);
      final int s = (int) (v.norme() / _ds);
      r.sommets_.ajoute(p);
      for (int j = 1; j < s; j++) {
        r.sommets_.ajoute(p.addition(v.multiplication(((double) j) / ((double) s))));
      }
    }
    return r;
  }

  /**
   * Calcule l'intersection en XY du Segment <code>s</code> avec cette boite.
   * 
   * @return null si aucune intersection
   */
  public final GrPoint intersectionXY(final GrSegment _s) {
    for (int i = nombre() - 1; i >= 0; i--) {
      final GrPoint r = cote(i).intersectionXY(_s);
      if (r != null) {
        return r;
      }
    }
    return null;
  }

  /**
   * @param _s le segment a tester
   * @return true si intersection
   */
  public final boolean intersectXY(final GrSegment _s) {
    for (int i = nombre() - 1; i >= 0; i--) {
      if (cote(i).intersectXY(_s)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Retourne un polygone AWT.
   */
  public final Polygon polygon() {
    final Polygon r = new Polygon();
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      final GrPoint p = sommetQuick(i);
      r.addPoint((int) p.x_, (int) p.y_);
    }
    return r;
  }

  /**
   * Retourne un objet GrPolyligne �quivalent.
   * 
   * @return Un GrPolyligne equivalent.
   * @author b.marchand Le 28/08/2001
   */
  public final GrPolyligne toGrPolyligne() {
    final GrPolyligne r = new GrPolyligne();
    final int nb = nombre();
    for (int i = 0; i < nb; i++) {
      r.sommets_.ajoute(sommetQuick(i));
    }
    return r;
  }
  
  /**
   * @return La s�quence de coordonn�es �quivalente, avec le 1er et dernier point egaux.
   */
  public final CoordinateSequence toCoordinateSequence() {
    CoordinateSequence seq=new GISCoordinateSequenceFactory().create(sommets_.nombre()+1, 3);
    int nb=seq.size();
    for (int i=0; i<nb; i++) {
      seq.setOrdinate(i, 0, sommets_.renvoieX(i%(nb-1)));
      seq.setOrdinate(i, 1, sommets_.renvoieY(i%(nb-1)));
      seq.setOrdinate(i, 2, sommets_.renvoieZ(i%(nb-1)));
    }
    return seq;
  }
  
  /**
   * @return Un GIS polygone �quivalent. Pas de contr�le sur le nombre de points de this. Si le nombre est<4,
   * peut poser pb.
   */
  public final GISPolygone toGIS() {
    return GISGeometryFactory.INSTANCE.createLinearRing(toCoordinateSequence());
  }

  @Override
  public final GrPoint[] contour() {
    return sommets_.tableau();
  }

  /**
   * Indique si l'objet est s�lectionn� pour un point donn�.
   * <p>
   * <B>Remarque</B> : La distance de tol�rance n'est pas prise en compte en testant sur la polyligne �quivalente au
   * polygone si le point trouv� en dehors du polygone.
   * 
   * @param _ecran Le morphisme pour la transformation en coordonn�es �cran.
   * @param _dist La tol�rence en coordonn�es �cran pour laquelle on consid�re l'objet s�lectionn�.
   * @param _pt Le point de s�lection en coordonn�es �cran.
   * @return <I>true</I> L'objet est s�lectionn�. <I>false</I> L'objet n'est pas s�lectionn�.
   * @author b.marchand Le 26/10/2000
   */
  @Override
  public final boolean estSelectionne(final GrMorphisme _ecran, final double _dist, final Point _pt) {
    final GrPolygone p = applique(_ecran);
    final Polygon pl = p.polygon();
    if (pl.contains(_pt)) {
      return true;
    }
    // Pour le controle de distance de tol�rance autour du polygone.
    return toGrPolyligne().estSelectionne(_ecran, _dist, _pt);
  }

  public final boolean estSelectionne(final GrMorphisme _ecran, final int _dist, final GrPoint _pt) {
    return estSelectionne(applique(_ecran), _dist, _pt);
  }

  /**
   * @param _p le polygone a tester
   * @param _dist la tolerance : distance max entre le polygone et le point
   * @param _pt le point de selection
   * @return true si le polygone _p est selectionne par le point _pt
   */
  public static final boolean estSelectionne(final GrPolygone _p, final double _dist, final GrPoint _pt) {
    if (_p.contientXY(_pt)) {
      return true;
    }
    // Pour le controle de distance de tol�rance autour du polygone.
    return _p.distanceXY(_pt) < _dist;
  }

  /**
   * Renvoie true si <code>_poly</code> est ENTIEREMENT dans le polygone de ref <code>_polyRef</code>.
   * 
   * @param _poly le polygone a tester.
   */
  public static final boolean estSelectionne(final GrPolygone _poly, final GrPolygone _polyRef) {
    /*
     * GrBoite polyRefBoite= _polyRef.boite(); if (!polyRefBoite.contientXY(_poly.boite())) return false;
     */
    final int nb = _poly.nombre() - 1;
    GrPoint p;
    // GrSegment segment= new GrSegment();
    for (int i = nb; i >= 0; i--) {
      p = _poly.sommet(i);
      if (p == null) {
        continue;
      }
      if (!_polyRef.contientXY(p)) {
        return false;
        /*
         * segment= _poly.cote(i); if (!_polyRef.intersectXY(segment)) return false;
         */
      }
    }
    return true;
  }

  /**
   * Renvoie la liste de points de <code>_p </code> contenus dans le polygone <code>_polyRef</code>.
   * 
   * @return null si aucun point n'appartient a <code>_polyRef</code>.
   */
  public static final DefaultListSelectionModel pointsSelectionnes(final GrPolygone _poly, final GrPolygone _polyolyRef) {
    final GrBoite polyRefBoite = _polyolyRef.boite();
    if (!polyRefBoite.intersectXY(_poly.boite())) {
      return null;
    }
    final int nb = _poly.nombre() - 1;
    GrPoint p;
    final DefaultListSelectionModel r = new DefaultListSelectionModel();
    r.setValueIsAdjusting(true);
    for (int i = nb; i >= 0; i--) {
      p = _poly.sommet(i);
      if (p == null) {
        continue;
      }
      if (polyRefBoite.contientXY(p) && _polyolyRef.contientXY(p)) {
        r.addSelectionInterval(i, i);
      }
    }
    r.setValueIsAdjusting(false);
    return r.isSelectionEmpty() ? null : r;
  }

  // Creation de formes elementaires
  public final static GrPolygone creeCarre() {
    final GrPolygone r = new GrPolygone();
    r.sommets_.ajoute(new GrPoint(0., 0., 0.));
    r.sommets_.ajoute(new GrPoint(1., 0., 0.));
    r.sommets_.ajoute(new GrPoint(1., 1., 0.));
    r.sommets_.ajoute(new GrPoint(0., 1., 0.));
    return r;
  }

  /**
   * Teste, en XY, si le point <code>_p</code> est contenu dans ce polygone.
   * 
   * @return true si ce polygone contient <code>_p</code>.
   */
  public boolean contientXY(final GrPoint _p) {
    return contientXY(_p.x_, _p.y_);
  }

  /**
   * Renvoie la distance minimale entre <code>p</code> et ce polygone.
   * 
   * @return <code>Double.MAX_VALUE</code> si aucun cote.
   */
  public double distanceXY(final GrPoint _p) {
    return distanceXY(_p.x_, _p.y_);
  }

  public double distanceXY(final double _x, final double _y) {
    final int n = nombre();
    if (n == 0) {
      return 0;
    }
    final double x = _x;
    final double y = _y;
    double dmin = CtuluLibGeometrie.distanceFromSegment(sommet(0).x_, sommet(0).y_, sommet(n - 1).x_, sommet(n - 1).y_,
        x, y);
    double d;
    for (int i = n - 1; i > 0; i--) {
      d = CtuluLibGeometrie.distanceFromSegment(sommet(i).x_, sommet(i).y_, sommet(i - 1).x_, sommet(i - 1).y_, x, y);
      if (d < dmin) {
        dmin = d;
        if (dmin == 0.) {
          break;
        }
      }
    }
    return dmin;
  }

  /**
   * Teste, en XY, si le point <code>(_x,_y)</code> est contenu dans ce polygone.
   * 
   * @return true si ce polygone contient <code>(_x,_y)</code>.
   */
  public boolean contientXY(final double _x, final double _y) {
    final int nb = nombre();
    if ((nb <= 2) || (!boite().contientXY(_x, _y))) {
      return false;
    }
    int recu = 0;
    GrPoint p = sommets_.renvoie(nb - 1);
    double finx = p.x_;
    double finy = p.y_;
    double xi, yi;
    for (int i = 0; i < nb; finx = xi, finy = yi, i++) {
      p = sommets_.renvoie(i);
      xi = p.x_;
      yi = p.y_;
      if (yi == finy) {
        continue;
      }
      double lx;
      if (xi < finx) {
        if (_x >= finx) {
          continue;
        }
        lx = xi;
      } else {
        if (_x >= xi) {
          continue;
        }
        lx = finx;
      }
      double test1, test2;
      if (yi < finy) {
        if (_y < yi || _y >= finy) {
          continue;
        }
        if (_x < lx) {
          recu++;
          continue;
        }
        test1 = _x - xi;
        test2 = _y - yi;
      } else {
        if (_y < finy || _y >= yi) {
          continue;
        }
        if (_x < lx) {
          recu++;
          continue;
        }
        test1 = _x - finx;
        test2 = _y - finy;
      }
      if (test1 < (test2 / (finy - yi) * (finx - xi))) {
        recu++;
      }
    }
    return ((recu & 1) != 0);
  }
  
  @Override
  public boolean equals(Object _o) {
    if (!(_o instanceof GrPolygone)) return false;
    GrPolygone o=(GrPolygone)_o;
    
    if (o.nombre()!=nombre())
      return false;
    
    for (int i=0; i<o.sommets_.nombre(); i++) {
      if (!o.sommet(i).equals(sommet(i)))
        return false;
    }
    
    return true;
  }
}
