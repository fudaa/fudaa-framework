/*
 * @creation     1997-03-25
 * @modification $Date: 2007-05-04 13:49:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;

/**
 * Un vecteur 3D.
 * 
 * @version $Id: GrVecteur.java,v 1.11 2007-05-04 13:49:47 deniger Exp $
 * @author Guillaume Desnoix
 */
public final class GrVecteur extends GrObjet {
  public double x_;
  public double y_;
  public double z_;

  public GrVecteur() {}

  public GrVecteur(final GrVecteur _v) {
    this(_v.x_, _v.y_, _v.z_);

  }

  public GrVecteur(final double _x, final double _y, final double _z) {
    x_ = _x;
    y_ = _y;
    z_ = _z;
  }

  @Override
  public String toString() {
    return "GrVecteur(" + x_ + ", " + y_ + ", " + z_ + ")";
  }

  // operateurs unaires
  public double norme() {
    return Math.sqrt(produitScalaire(this));
  }

  public double normeXY() {
    return Math.sqrt(produitScalaireXY(this));
  }

  public GrVecteur normalise() {
    final double n = norme();
    if (n == 0) {
      return new GrVecteur();
    }
    return new GrVecteur(x_ / n, y_ / n, z_ / n);
  }

  public GrVecteur normaliseXY() {
    final double n = normeXY();
    if (n == 0) {
      return new GrVecteur();
    }
    return new GrVecteur(x_ / n, y_ / n, z_);
  }

  public void autoNormaliseXY() {
    final double n = normeXY();
    if (n == 0) {
      return;
    }
    x_ = x_ / n;
    y_ = y_ / n;
    z_ = z_ / n;
  }

  public GrVecteur equilibre() {
    final double n = x_ + y_ + z_;
    return new GrVecteur(x_ / n, y_ / n, z_ / n);
  }

  public GrVecteur equilibreXY() {
    final double n = x_ + y_;
    return new GrVecteur(x_ / n, y_ / n, z_);
  }

  public GrVecteur rotationZ90() {
    return new GrVecteur(-y_, x_, z_);
  }

  public GrVecteur projectionXY() {
    return new GrVecteur(x_, y_, 0.);
  }

  public GrVecteur applique(final GrMorphisme _t) {
    final GrVecteur r = new GrVecteur();
    r.x_ = x_ * _t.a_[0][0] + y_ * _t.a_[1][0] + z_ * _t.a_[2][0];
    r.y_ = x_ * _t.a_[0][1] + y_ * _t.a_[1][1] + z_ * _t.a_[2][1];
    r.z_ = x_ * _t.a_[0][2] + y_ * _t.a_[1][2] + z_ * _t.a_[2][2];
    return r;
  }

  @Override
  public void autoApplique(final GrMorphisme _t) {
    x_ = x_ * _t.a_[0][0] + y_ * _t.a_[1][0] + z_ * _t.a_[2][0];
    y_ = x_ * _t.a_[0][1] + y_ * _t.a_[1][1] + z_ * _t.a_[2][1];
    z_ = x_ * _t.a_[0][2] + y_ * _t.a_[1][2] + z_ * _t.a_[2][2];
  }

  // Operateurs binaires
  public GrVecteur multiplication(final double _d) {
    final GrVecteur res = new GrVecteur(this);
    res.autoMultiplication(_d);
    return res;
  }

  public void autoMultiplication(final double _d) {
    x_ = _d * x_;
    y_ = _d * y_;
    z_ = _d * z_;
  }

  public GrVecteur division(final double _d) {
    return new GrVecteur(x_ / _d, y_ / _d, z_ / _d);
  }

  public double division(final GrVecteur _v) {
    final GrVecteur w = projection(_v);
    if (_v.x_ != 0.) {
      return w.x_ / _v.x_;
    } else if (_v.y_ != 0.) {
      return w.y_ / _v.y_;
    } else if (_v.z_ != 0.) {
      return w.z_ / _v.z_;
    }
    return 0.;
  }

  public GrVecteur addition(final GrVecteur _v) {
    return new GrVecteur(x_ + _v.x_, y_ + _v.y_, z_ + _v.z_);
  }

  public void autoAddition(final GrVecteur _v) {
    x_ = x_ + _v.x_;
    y_ = y_ + _v.y_;
    z_ = z_ + _v.z_;
  }

  public GrPoint addition(final GrPoint _p) {
    return new GrPoint(x_ + _p.x_, y_ + _p.y_, z_ + _p.z_);
  }

  public GrVecteur soustraction(final GrVecteur _v) {
    return new GrVecteur(x_ - _v.x_, y_ - _v.y_, z_ - _v.z_);
  }

  public double produitScalaire(final GrVecteur _v) {
    return x_ * _v.x_ + y_ * _v.y_ + z_ * _v.z_;
  }

  public double produitScalaireXY(final GrVecteur _v) {
    return x_ * _v.x_ + y_ * _v.y_;
  }

  public double produitSinus(final GrVecteur _v) {
    return rotationZ90().produitScalaire(_v);
  }

  public double produitSinusXY(final GrVecteur _v) {
    return _v.x_ * -y_ + _v.y_ * x_;
  }

  public GrVecteur produitVectoriel(final GrVecteur _v) {
    return new GrVecteur(y_ * _v.z_ - z_ * _v.y_, z_ * _v.x_ - x_ * _v.z_, x_ * _v.y_ - y_ * _v.x_);
  }

  public GrVecteur produitVectorielXY(final GrVecteur _v) {
    return new GrVecteur(0., 0., x_ * _v.y_ - y_ * _v.x_);
  }

  public GrVecteur projection(final GrVecteur _v) {
    return _v.normalise().multiplication(produitScalaire(_v.normalise()));
  }

  // Operateurs ternaires
  public double produitTripleScalaire(final GrVecteur _v1, final GrVecteur _v2) {
    return produitVectoriel(_v1).produitScalaire(_v2);
  }

  public GrVecteur produitTripleVectoriel(final GrVecteur _v1, final GrVecteur _v2) {
    return produitVectoriel(_v1).produitVectoriel(_v2);
  }

  /**
   * Calcule dans le plan XY l'angle entre <code>this</code> et un autre vecteur.
   * L'angle calcul� est sign�, tenant compte de l'ordre de <code>this</code> et autre.<p>
   * Exemples :<br>
   * <code>
   * Si this(1,0,0) et autre(0,-1,0), angle=-PI/2<br>
   * Si this(0,-1,0) et autre(1,0,0), angle=PI/2
   * </code>
   * @param _v Le vecteur autre. 
   * @return L'angle en radians, sign�.
   */
  public double getAngleXY(GrVecteur _v) {
    GrVecteur v1=this.normalise();
    GrVecteur v2=_v.normalise();
    return Math.atan2(v1.produitVectoriel(v2).z_,v1.produitScalaire(v2));
  }
}
