/*
 * @creation     2000-05-12
 * @modification $Date: 2006-04-12 15:28:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;
import java.io.Serializable;
/**
 * Interface permettant de stocker un objet contenant des informations spécifiques
 * de l'application.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-04-12 15:28:02 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public interface GrData extends Serializable {}
