/*
 * @creation     1998-09-02
 * @modification $Date: 2006-04-12 15:28:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;
import java.awt.Point;
/**
 * Interface pour un objet s�lectionnable.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-04-12 15:28:02 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public interface GrContour {
  /**
   * Methode retournant les points du contour de l'objet.<p>
   *
   * Par exemple, pour un GrElement, ces points sont les noeuds du GrElement.
   * Ces points servent a determiner si un objet est dans la selection pour une
   * s�lection par rectangle ou polygone (tous ses points sont dans la
   * s�lection) ou non.<p>
   *
   * Ils servent �galement � afficher les poign�es si l'objet s�lectionn�.
   *
   * @see org.fudaa.ebli.calque.BCalqueSelectionInteraction
   * @see org.fudaa.ebli.calque.BCalqueSelection
   */
  GrPoint[] contour();
  /**
   * Indique si l'objet est s�lectionn� pour un point donn�.<p>
   *
   * Dans le cadre de la s�lection ponctuelle.
   *
   * @param _ecran Le morphisme pour la transformation en coordonn�es �cran.
   * @param _dist  La tol�rence en coordonn�es �cran pour laquelle on consid�re
   *               l'objet s�lectionn�.
   * @param _pt    Le point de s�lection en coordonn�es �cran.
   *
   * @return <I>true</I>  L'objet est s�lectionn�.
   *         <I>false</I> L'objet n'est pas s�lectionn�.
   *
   * @see org.fudaa.ebli.calque.BCalqueSelectionInteraction
   */
  boolean estSelectionne(GrMorphisme _ecran, double _dist, Point _pt);
  /**
   * Indique si l'objet est s�lectionn� pour un rectangle donn�.<p>
   *
   * Dans le cadre de la s�lection par rectangle. Pour l'instant, cette m�thode
   * n'est pas impl�ment�e dans les objets, la s�lection/non se fait dans le
   * calque <i>BCalqueSelectionInteraction</i> par l'interm�diaire des points de
   * contours.
   *
   * @param _ecran Le morphisme pour la transformation en coordonn�es �cran.
   * @param _dist  La tol�rence en coordonn�es �cran pour laquelle on consid�re
   *               l'objet s�lectionn�.
   * @param _pt    Le point de s�lection en coordonn�es �cran.
   *
   * @return <I>true</I> L'objet est s�lectionn�.
   *         <I>false</I> L'objet n'est pas s�lectionn�.
   *
   * @see org.fudaa.ebli.calque.BCalqueSelectionInteraction
   */
  //  boolean estSelectionne(GrMorphisme _ecran, int _dist, Rectangle _pt);
  /**
   * Indique si l'objet est s�lectionn� pour un polygone donn�.<p>
   *
   * Dans le cadre de la s�lection par polygone. Pour l'instant, cette m�thode
   * n'est pas impl�ment�e dans les objets, la s�lection/non se fait dans le
   * calque <i>BCalqueSelectionInteraction</i> par l'interm�diaire des points de
   * contours.
   *
   * @param _ecran Le morphisme pour la transformation en coordonn�es �cran.
   * @param _dist  La tol�rence en coordonn�es �cran pour laquelle on consid�re
   *               l'objet s�lectionn�.
   * @param _pt    Le point de s�lection en coordonn�es �cran.
   *
   * @return <I>true</I> L'objet est s�lectionn�.
   *         <I>false</I> L'objet n'est pas s�lectionn�.
   *
   * @see org.fudaa.ebli.calque.BCalqueSelectionInteraction
   */
  //  boolean estSelectionne(GrMorphisme _ecran, int _dist, Polygone _pt);
}
