/*
 * @creation     1998-09-02
 * @modification $Date: 2007-06-05 08:58:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.geometrie;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Envelope;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.geom.Point2D;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;

/**
 * GrBoite.
 *
 * @version $Revision: 1.30 $ $Date: 2007-06-05 08:58:38 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public final class GrBoite extends GrObjet implements GrContour, Cloneable {

  /**
   * Le point des minimum.
   */
  public GrPoint o_;
  /**
   * Le point des maximum.
   */
  public GrPoint e_;

  public GrBoite() {
  }

  public GrBoite(final Envelope _e) {
    if (_e != null && !_e.isNull()) {
      e_ = new GrPoint(_e.getMaxX(), _e.getMaxY(), 0);
      o_ = new GrPoint(_e.getMinX(), _e.getMinY(), 0);
    }
  }

  public double getMinX() {
    if (isIndefinie()) {
      return -Double.MAX_VALUE;
    }
    return o_.x_;
  }

  public void translate(final GrVecteur _v) {
    if (isIndefinie()) {
      return;
    }
    o_.autoAddition(_v);
    e_.autoAddition(_v);
  }

  public void factor(final double _d) {
    final double xMid = getMidX();
    final double yMid = getMidY();
    final double zMid = getMidZ();
    final double dx = Math.abs(getDeltaX() * _d / 2);
    final double dy = Math.abs(getDeltaY() * _d / 2);
    final double dz = Math.abs(getDeltaZ() * _d / 2);
    setMinX(xMid - dx);
    setMaxX(xMid + dx);
    setMinY(yMid - dy);
    setMaxY(yMid + dy);
    setMinZ(zMid - dz);
    setMaxZ(zMid + dz);

  }

  public void translate(final double _x, final double _y) {
    if (isIndefinie()) {
      return;
    }
    o_.autoAddition(_x, _y);
    e_.autoAddition(_x, _y);
  }

  public double getMaxX() {
    if (isIndefinie()) {
      return Double.MAX_VALUE;
    }
    return e_.x_;
  }

  public double getMinY() {
    if (isIndefinie()) {
      return -Double.MAX_VALUE;
    }
    return o_.y_;
  }

  public double getMinZ() {
    if (isIndefinie()) {
      return -Double.MAX_VALUE;
    }
    return o_.z_;
  }

  public double getMaxZ() {
    if (isIndefinie()) {
      return Double.MAX_VALUE;
    }
    return e_.z_;
  }

  public double getMaxY() {
    if (isIndefinie()) {
      return Double.MAX_VALUE;
    }
    return e_.y_;
  }

  public void setMinX(final double _x) {
    if (o_ == null) {
      o_ = new GrPoint();
    }
    o_.x_ = _x;
  }

  public void setMinY(final double _y) {
    if (o_ == null) {
      o_ = new GrPoint();
    }
    o_.y_ = _y;
  }

  public void setMaxX(final double _x) {
    if (e_ == null) {
      e_ = new GrPoint();
    }
    e_.x_ = _x;
  }

  public void setMaxY(final double _y) {
    if (e_ == null) {
      e_ = new GrPoint();
    }
    e_.y_ = _y;
  }

  public void setMaxZ(final double _z) {
    if (e_ == null) {
      e_ = new GrPoint();
    }
    e_.z_ = _z;
  }

  public void setMinZ(final double _z) {
    if (o_ == null) {
      o_ = new GrPoint();
    }
    o_.z_ = _z;
  }

  /**
   * @param _min le point des min
   * @param _max le point des max
   */
  public GrBoite(final GrPoint _min, final GrPoint _max) {
    ajuste(_min);
    ajuste(_max);
  }

  public GrBoite(final GrBoite _b) {
    if (_b != null && !_b.isIndefinie()) {
      o_ = new GrPoint(_b.o_);
      e_ = new GrPoint(_b.e_);
    }
  }

  public void ajuste(final Point2D _p) {
    if (_p == null) {
      return;
    }
    ajuste(_p.getX(), _p.getY(), 0);
  }

  public void ajuste(final GrPoint _p) {
    if (_p == null) {
      return;
    }
    if (o_ == null) {
      o_ = new GrPoint(_p.x_, _p.y_, _p.z_);
    } else {
      o_.x_ = Math.min(o_.x_, _p.x_);
      o_.y_ = Math.min(o_.y_, _p.y_);
      o_.z_ = Math.min(o_.z_, _p.z_);
    }
    if (e_ == null) {
      e_ = new GrPoint(_p.x_, _p.y_, _p.z_);
    } else {
      e_.x_ = Math.max(e_.x_, _p.x_);
      e_.y_ = Math.max(e_.y_, _p.y_);
      e_.z_ = Math.max(e_.z_, _p.z_);
    }
  }

  public void ajusteZ(final double _z) {
    if (o_ != null) {
      o_.z_ = Math.min(o_.z_, _z);
    }
    if (e_ != null) {
      e_.z_ = Math.max(e_.z_, _z);
    }

  }

  public void ajuste(final double _x, final double _y, final double _z) {
    if (o_ == null) {
      o_ = new GrPoint(_x, _y, _z);
    }
    if (e_ == null) {
      e_ = new GrPoint(_x, _y, _z);
    }
    o_.x_ = Math.min(o_.x_, _x);
    o_.y_ = Math.min(o_.y_, _y);
    o_.z_ = Math.min(o_.z_, _z);
    e_.x_ = Math.max(e_.x_, _x);
    e_.y_ = Math.max(e_.y_, _y);
    e_.z_ = Math.max(e_.z_, _z);
  }

  /**
   * Ajuste la boite avec les points min/max d'une autre boite.
   *
   * @param _b Boite d'ajustement
   */
  public void ajuste(final GrBoite _b) {
    if (_b != null) {
      ajuste(_b.e_);
      ajuste(_b.o_);
    }
  }

  /**
   * Ajoste � partir d'une enveloppe.
   *
   * @param _e L'enveloppe
   */
  public void ajuste(final Envelope _e) {
    if (_e != null && !_e.isNull()) {
      if (o_ == null) {
        o_ = new GrPoint(_e.getMinX(), _e.getMinY(), 0);
      }
      if (e_ == null) {
        e_ = new GrPoint(_e.getMaxX(), _e.getMaxY(), 0);
      }
      o_.x_ = Math.min(o_.x_, _e.getMinX());
      o_.y_ = Math.min(o_.y_, _e.getMinY());
      e_.x_ = Math.max(e_.x_, _e.getMaxX());
      e_.y_ = Math.max(e_.y_, _e.getMaxY());
    }
  }

  public boolean isIndefinie() {
    return (o_ == null) || (e_ == null) || getDeltaX() < 0 || getDeltaY() < 0 || getDeltaZ() < 0;
  }

  @Override
  public String toString() {
    return "GrBoite(" + o_ + CtuluLibString.VIR + e_ + ")";
  }

  @Override
  public Object clone() throws CloneNotSupportedException {
    final GrBoite r = (GrBoite) super.clone();
    if (o_ != null) {
      r.o_ = new GrPoint(o_.x_, o_.y_, o_.z_);
    }
    if (e_ != null) {
      r.e_ = new GrPoint(e_.x_, e_.y_, e_.z_);
    }
    return r;
  }

  public GrPoint barycentre() {
    return (new GrSegment(o_, e_)).milieu();
  }

  /**
   * Calcul le barycentre en x,y et stocke les valeurs dans p. Ne fait aucun test sur les points.
   */
  public void barycentreXY(final GrPoint _p) {
    _p.x_ = (o_.x_ + e_.x_) / 2;
    _p.y_ = (o_.y_ + e_.y_) / 2;
  }

  public double volume() {
    return Math.abs(o_.x_ - e_.x_) * Math.abs(o_.x_ - e_.x_) * Math.abs(o_.x_ - e_.x_);
  }

  /**
   * @deprecated utiliser #contientXY(GrPoint _p)
   */
  public boolean estDedansXY(final GrPoint _p) {
    return contientXY(_p);
  }

  /**
   * Teste en X,Y si le point _p appartient a cette boite.
   *
   * @return true si _p appartient a cette boite.
   */
  public boolean contientXY(final GrPoint _p) {
    return contientXY(_p.x_, _p.y_);
  }

  /**
   * Teste en X,Y si le boite
   * <code>_b </code> appartient a cette boite. En fait, teste les points _b.e et _b.o appartienne a cette boite.
   *
   * @return true si _b appartient a cette boite.
   */
  public boolean contientXY(final GrBoite _b) {
    return (contientXY(_b.o_)) && (contientXY(_b.e_));
  }

  /**
   * @param _b la boite a tester
   * @return true si _b est contenue en y dans cette boite
   */
  public boolean contientY(final GrBoite _b) {
    return (contientY(_b.o_)) && (contientY(_b.e_));
  }

  /**
   * @param _b la boite a tester
   * @return true si _b est contenue en x dans cette boite
   */
  public boolean contientX(final GrBoite _b) {
    return (contientX(_b.o_)) && (contientX(_b.e_));
  }

  public boolean contientY(final GrPoint _b) {
    return contientY(_b.y_);
  }

  public boolean contientY(final double _y) {
    return _y <= e_.y_ && _y >= o_.y_;
  }

  public boolean contientX(final GrPoint _b) {
    return contientX(_b.x_);
  }

  public boolean contientX(final double _x) {
    return _x <= e_.x_ && _x >= o_.x_;
  }

  public double distanceXY(final GrPoint _p) {
    return distanceXY(_p.x_, _p.y_);
  }

  public double distanceXY(final double _x, final double _y) {
    double r = CtuluLibGeometrie.distanceFromSegment(o_.x_, o_.y_, e_.x_, o_.y_, _x, _y);
    double r1 = CtuluLibGeometrie.distanceFromSegment(e_.x_, e_.y_, e_.x_, o_.y_, _x, _y);
    if (r1 < r) {
      r = r1;
    }
    r1 = CtuluLibGeometrie.distanceFromSegment(e_.x_, e_.y_, o_.x_, e_.y_, _x, _y);
    if (r1 < r) {
      r = r1;
    }
    r1 = CtuluLibGeometrie.distanceFromSegment(o_.x_, o_.y_, o_.x_, e_.y_, _x, _y);
    if (r1 < r) {
      r = r1;
    }
    return r;
  }

  /**
   * Teste en X,Y,Z si le boite
   * <code>_b </code> appartient a cette boite. En fait, teste les points _b.e et _b.o appartienne a cette boite.
   *
   * @return true si _b appartient a cette boite.
   */
  public boolean contient(final GrBoite _b) {
    return (contient(_b.o_)) && (contient(_b.e_));
  }

  /**
   * Teste en X,Y si le point (_x,_y) appartient a cette boite.
   *
   * @return true si (_x,_y) appartient a cette boite.
   */
  public boolean contientXY(final double _x, final double _y) {
    return (o_.x_ <= _x) && (_x <= e_.x_) && (o_.y_ <= _y) && (_y <= e_.y_);
  }

  /**
   * Teste en X,Y,Z si le point
   * <code>_p</code> appartient a cette boite.
   *
   * @return true si _p appartient a cette boite.
   */
  public boolean contient(final GrPoint _p) {
    return contient(_p.x_, _p.y_, _p.z_);
  }

  /**
   * @deprecated utiliser #contient(GrPoint _p)
   */
  public boolean estDedans(final GrPoint _p) {
    return contient(_p);
  }

  /**
   * Teste en X,Y,Z si le point (_x,_y,_z) appartient a cette boite.
   *
   * @return true si (_x,_y,_z) appartient a cette boite.
   */
  public boolean contient(final double _x, final double _y, final double _z) {
    return (o_.x_ <= _x) && (_x <= e_.x_) && (o_.y_ <= _y) && (_y <= e_.y_) && (o_.z_ <= _z) && (_z <= e_.z_);
  }

  public boolean comparaison(final GrBoite _b) {
    return o_.comparaison(_b.o_);
  }

  public boolean intersectXY(final GrBoite _b) {
    if ((e_.x_ < _b.o_.x_) || (o_.x_ > _b.e_.x_) || (e_.y_ < _b.o_.y_) || (o_.y_ > _b.e_.y_)) {
      return false;
    }
    return true;
  }

  public boolean intersectXY(final Envelope e) {
    if (e == null || e.isNull()) {
      return false;
    }
//      e_ = new GrPoint(_e.getMaxX(), _e.getMaxY(), 0);
//      o_ = new GrPoint(_e.getMinX(), _e.getMinY(), 0);
    if ((e_.x_ < e.getMinX()) || (o_.x_ > e.getMaxX()) || (e_.y_ < e.getMinY()) || (o_.y_ > e.getMaxY())) {
      return false;
    }
    return true;
  }

  /**
   * @param _b le segment
   * @return true si la boite du segment intersecte cette boite
   */
  public boolean intersectXYBoite(final GrSegment _b) {

    double ox = 0;
    double oy = 0;
    double ex = 0;
    double ey = 0;
    if (_b.e_.x_ < _b.o_.x_) {
      ox = _b.e_.x_;
      ex = _b.o_.x_;
    } else {
      ox = _b.o_.x_;
      ex = _b.e_.x_;
    }
    if (_b.e_.y_ < _b.o_.y_) {
      oy = _b.e_.y_;
      ey = _b.o_.y_;
    } else {
      oy = _b.o_.y_;
      ey = _b.e_.y_;
    }
    return !((e_.x_ < ox) || (o_.x_ > ex) || (e_.y_ < oy) || (o_.y_ > ey));
  }

   /**
   * Le segment et (x1,y1) (x2,y2).
   * @return true si la boite du segment intersecte cette boite
   */
  public boolean intersectXYBoite(double x1, double y1, double x2, double y2) {

    double ox = 0;
    double oy = 0;
    double ex = 0;
    double ey = 0;
    if (x1 < x2) {
      ox = x1;
      ex = x2;
    } else {
      ox = x2;
      ex = x1;
    }
    if (y1 < y2) {
      oy = y1;
      ey = y2;
    } else {
      oy = y2;
      ey = y1;
    }
    return !((e_.x_ < ox) || (o_.x_ > ex) || (e_.y_ < oy) || (o_.y_ > ey));
  }

  public GrBoite intersectionXY(final GrBoite _b2d) {
    if (_b2d == null || isIndefinie()) {
      return null;
    }
    if ((e_.x_ < _b2d.o_.x_) || (o_.x_ > _b2d.e_.x_) || (e_.y_ < _b2d.o_.y_) || (o_.y_ > _b2d.e_.y_)) {
      return null;
    }
    final GrBoite r = new GrBoite(this);
    if (_b2d.o_.x_ > r.o_.x_) {
      r.o_.x_ = _b2d.o_.x_;
    }
    if (_b2d.o_.y_ > r.o_.y_) {
      r.o_.y_ = _b2d.o_.y_;
    }
    if (_b2d.e_.x_ < r.e_.x_) {
      r.e_.x_ = _b2d.e_.x_;
    }
    if (_b2d.e_.y_ < r.e_.y_) {
      r.e_.y_ = _b2d.e_.y_;
    }
    r.o_.z_ = 0.;
    r.e_.z_ = 0.;
    return r;
  }

  public boolean autoIntersectionXY(final GrBoite _b2d) {
    if (_b2d == null) {
      setToNill();
      return false;
    }
    if ((e_.x_ < _b2d.o_.x_) || (o_.x_ > _b2d.e_.x_) || (e_.y_ < _b2d.o_.y_) || (o_.y_ > _b2d.e_.y_)) {
      setToNill();
      return false;
    }
    if (_b2d.o_.x_ > o_.x_) {
      o_.x_ = _b2d.o_.x_;
    }
    if (_b2d.o_.y_ > o_.y_) {
      o_.y_ = _b2d.o_.y_;
    }
    if (_b2d.e_.x_ < e_.x_) {
      e_.x_ = _b2d.e_.x_;
    }
    if (_b2d.e_.y_ < e_.y_) {
      e_.y_ = _b2d.e_.y_;
    }
    o_.z_ = 0.;
    e_.z_ = 0.;
    return true;
  }

  public GrBoite intersection(final GrBoite _b) {
    if ((e_.x_ < _b.o_.x_) || (o_.x_ > _b.e_.x_) || (e_.y_ < _b.o_.y_) || (o_.y_ > _b.e_.y_) || (e_.z_ < _b.o_.z_)
            || (o_.z_ > _b.e_.z_)) {
      return null;
    }
    final GrBoite r = new GrBoite(this);
    r.intersect(_b);
    return r;
  }

  public void intersect(final GrBoite _b) {
    if ((e_.x_ < _b.o_.x_) || (o_.x_ > _b.e_.x_) || (e_.y_ < _b.o_.y_) || (o_.y_ > _b.e_.y_) || (e_.z_ < _b.o_.z_)
            || (o_.z_ > _b.e_.z_)) {
      return;
    }
    if (_b.o_.x_ > o_.x_) {
      o_.x_ = _b.o_.x_;
    }
    if (_b.o_.y_ > o_.y_) {
      o_.y_ = _b.o_.y_;
    }
    if (_b.o_.z_ > o_.z_) {
      o_.z_ = _b.o_.z_;
    }
    if (_b.e_.x_ < e_.x_) {
      e_.x_ = _b.e_.x_;
    }
    if (_b.e_.y_ < e_.y_) {
      e_.y_ = _b.e_.y_;
    }
    if (_b.e_.z_ < e_.z_) {
      e_.z_ = _b.e_.z_;
    }
  }

  public GrBoite unionXY(final GrBoite _b) {
    final GrBoite r = union(_b);
    r.o_.z_ = 0.;
    r.e_.z_ = 0.;
    return r;
  }

  public GrBoite union(final GrBoite _b) {
    GrBoite r = null;
    try {
      r = (GrBoite) clone();
    } catch (final CloneNotSupportedException _evt) {
      FuLog.error(_evt);

    }
    if (r != null) {
      r.ajuste(_b.o_);
      r.ajuste(_b.e_);
    }
    return r;
  }

  public GrBoite applique(final GrMorphisme _t) {
    final GrBoite r = new GrBoite();
    r.ajuste(o_.applique(_t));
    r.ajuste(e_.applique(_t));
    r.ajuste(new GrPoint(o_.x_, e_.y_, o_.z_).applique(_t));
    r.ajuste(new GrPoint(e_.x_, o_.y_, o_.z_).applique(_t));
    r.ajuste(new GrPoint(e_.x_, e_.y_, o_.z_).applique(_t));
    r.ajuste(new GrPoint(o_.x_, e_.y_, e_.z_).applique(_t));
    r.ajuste(new GrPoint(e_.x_, o_.y_, e_.z_).applique(_t));
    r.ajuste(new GrPoint(o_.x_, o_.y_, e_.z_).applique(_t));
    return r;
  }

  @Override
  public void autoApplique(final GrMorphisme _t) {
    final GrBoite r = applique(_t);
    o_ = null;
    e_ = null;
    ajuste(r.o_);
    ajuste(r.e_);
  }

  /**
   * Impl�mentation GrObjet.
   */
  @Override
  public GrBoite boite() {
    return this;
  }

  public GrPolygone enPolygoneXY() {
    final GrPolygone r = new GrPolygone();
    if (o_ != null) {
      r.sommets_.ajoute(new GrPoint(o_.x_, o_.y_, 0.));
      r.sommets_.ajoute(new GrPoint(o_.x_, e_.y_, 0.));
    }
    if (e_ != null) {
      r.sommets_.ajoute(new GrPoint(e_.x_, e_.y_, 0.));
      r.sommets_.ajoute(new GrPoint(e_.x_, o_.y_, 0.));
    }
    return r;
  }

  public Polygon polygon() {
    final Polygon r = new Polygon();
    r.addPoint((int) o_.x_, (int) o_.y_);
    r.addPoint((int) o_.x_, (int) e_.y_);
    r.addPoint((int) e_.x_, (int) e_.y_);
    r.addPoint((int) e_.x_, (int) o_.y_);
    return r;
  }

  @Override
  public GrPoint[] contour() {
    final GrPoint[] r = new GrPoint[4];
    r[0] = o_;
    r[1] = new GrPoint(o_.x_, e_.y_, o_.z_);
    r[2] = e_;
    r[3] = new GrPoint(e_.x_, o_.y_, e_.z_);
    return r;
  }

  public void setToNill() {
    if (e_ != null && o_ != null) {
      o_.setCoordonnees(0, 0, 0);
      e_.setCoordonnees(-1D, -1D, 0);
    }
  }

  public double getDeltaX() {
    return e_.x_ - o_.x_;
  }

  public double getDeltaY() {
    return e_.y_ - o_.y_;
  }

  public double getDeltaZ() {
    return e_.z_ - o_.z_;
  }

  public double getMidX() {
    return (e_.x_ + o_.x_) / 2;
  }

  public double getMidZ() {
    return (e_.z_ + o_.z_) / 2;
  }

  public double getMidY() {
    return (e_.y_ + o_.y_) / 2;
  }

  public Envelope getEnv() {
    final Envelope r = new Envelope();
    if (e_ != null) {
      r.expandToInclude(e_.x_, e_.y_);
    }
    if (o_ != null) {
      r.expandToInclude(o_.x_, o_.y_);
    }
    return r;
  }

  /**
   * Indique si l'objet est s�lectionn� pour un point donn�. <p> La m�thode a �t� impl�ment�e dans le but de satisfaire l'interface GrContour. Son
   * fonctionnement n'est peut �tre pas exactement celui attendu.
   *
   * @see GrContour
   * @author b.marchand Le 26/10/2000
   */
  @Override
  public boolean estSelectionne(final GrMorphisme _ecran, final double _dist, final Point _pt) {
    final GrBoite b = applique(_ecran);
    final Polygon pl = b.polygon();
    return pl.contains(_pt);
  }
}
