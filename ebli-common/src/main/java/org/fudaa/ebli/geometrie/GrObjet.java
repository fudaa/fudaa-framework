/*
 * @creation     1999-05-06
 * @modification $Date: 2006-09-29 13:45:32 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;

import java.io.Serializable;

/**
 * @version $Revision: 1.9 $ $Date: 2006-09-29 13:45:32 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public abstract class GrObjet implements Serializable {
  /*
   * Accesseur au champ data de stockage des informations sp�cifiques � l'application
   */
  private transient GrData data_;

  // a surcharger
  public void autoApplique(final GrMorphisme _t) {}

  /**
   * Retourne la boite englobante de l'objet (� surcharger).
   */
  public GrBoite boite() {
    return null;
  }

  public GrData data() {
    return data_;
  }

  public void data(final GrData _data) {
    data_ = _data;
  }
}
