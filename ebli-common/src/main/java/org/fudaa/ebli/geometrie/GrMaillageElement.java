/*
 * @creation     1999-10-22
 * @modification $Date: 2007-06-05 08:58:38 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;
import java.util.*;
/**
 * Une classe maillage sous forme d'�l�ments.<p>
 * Les �l�ments ne sont par n�cessairement orient�s dans le m�me sens.
 *
 * @version      $Id: GrMaillageElement.java,v 1.16 2007-06-05 08:58:38 deniger Exp $
 * @author       Guillaume Desnoix
 */
public class GrMaillageElement   extends GrObjet {
  private VecteurGrNoeud noeuds_;
  private VecteurGrElement elements_;
  private GrElement[][] elsPeau_;
  /**
   * Cr�ation d'un maillage vide.
   */
  public GrMaillageElement() {
    noeuds_= new VecteurGrNoeud();
    elements_= new VecteurGrElement();
    elsPeau_= null;
  }

  /**
   * @param _c la collection qui contiendra tous les noeuds du maillage
   */
  public void ajouteTousLesNoeuds(final Collection _c) {
    noeuds_.addAll(_c);
  }
  /**
   * @param _c la collection qui contiendra tous les elements du maillage
   */
  public void ajouteTousLesElements(final Collection _c) {
    elements_.addAll(_c);
  }
  /**
   * Cr�ation d'un maillage � partir des �l�ments.
   */
  public GrMaillageElement(final GrElement[] _elements) {
    noeuds_= null;
    elements_= new VecteurGrElement();
    elements_.setTableau(_elements);
    elsPeau_= null;
  }
  /**
   * Cr�ation d'un maillage � partir des �l�ments et de la table des noeuds.
   * associ�e
   */
  public GrMaillageElement(final GrElement[] _elements, final GrNoeud[] _noeuds) {
    noeuds_= new VecteurGrNoeud();
    noeuds_.tableau(_noeuds);
    elements_= new VecteurGrElement();
    elements_.setTableau(_elements);
    elsPeau_= null;
  }
  public final GrElement[] elements() {
    return elements_.tableau();
  }

  public final int getNbElement() {
    return elements_.nombre();
  }
  //  public void elements(GrElement[] _elements) {
  //    elements_.tableau(_elements);
  //    noeuds_=null;
  //  }
  //  public void noeuds(GrNoeud[] _noeuds) {
  //    noeuds_.tableau(_noeuds);
  //  }
  public final GrNoeud[] noeuds() {
    return lnds().tableau();
  }
  public final void ajoute(final GrNoeud _noeud) {
    lnds().ajoute(_noeud);
    elsPeau_= null;
  }
  public final void ajoute(final GrElement _element) {
    for (int i= 0; i < _element.noeuds_.length; i++) {
      if (!lnds().contient(_element.noeuds_[i])) {
        lnds().ajoute(_element.noeuds_[i]);
      }
    }
    elsPeau_= null;
  }
  public final void enleve(final GrElement _element) {
    elements_.enleve(_element);
    elsPeau_= null;
  }
  /**
   * Enl�ve les noeuds non connect�s.
   * <b>Remarque</b> : L'ordre des noeuds dans la liste des noeuds peut �tre
   * modifi�e suite � l'appel de cette m�thode.
   */
  public final void enleveNoeudsNonConnectes() {
    noeuds_= null;
    lnds();
  }
  public final int indice(final GrNoeud _noeud) {
    return lnds().indice(_noeud);
  }
  public final int indice(final GrElement _element) {
    return elements_.indice(_element);
  }
  public final GrElement element(final int _i) {
    return elements_.renvoie(_i);
  }
  public final GrNoeud noeud(final int _i) {
    return lnds().renvoie(_i);
  }

  public final int getNbNoeud() {
    return lnds().nombre();
  }
  @Override
  public final String toString() {
    return "GrMaillageElement("
      + lnds().nombre()
      + " noeuds, "
      + elements_.nombre()
      + " connectivites)";
  }
  /**
   * Impl�mentation GrObjet.
   */
  @Override
  public final GrBoite boite() {
    final GrBoite r= new GrBoite();
    final int n= lnds().nombre();
    for (int i= 0; i < n; i++) {
      r.ajuste(lnds().renvoie(i).point_);
    }
    return r;
  }
  public final GrMaillageElement applique(final GrMorphisme _t) {
    GrNoeud oNd;
    final GrNoeud[] oNds= lnds().tableau();
    GrNoeud[] ndsEle;
    GrElement oEle;
    GrNoeud[] oNdsEle;
    int oType;
    final GrElement[] els= new GrElement[elements_.nombre()];
    final Hashtable ht= new Hashtable(oNds.length);
    for (int i= 0; i < oNds.length; i++) {
      oNd= oNds[i];
      oNds[i]= oNd.applique(_t);
      ht.put(oNd, oNds[i]);
    }
    for (int i= 0; i < elements_.nombre(); i++) {
      oEle= elements_.renvoie(i);
      oNdsEle= oEle.noeuds_;
      oType= oEle.type_;
      ndsEle= new GrNoeud[oNdsEle.length];
      for (int j= 0; j < oNdsEle.length; j++) {
        ndsEle[j]= (GrNoeud)ht.get(oNdsEle[j]);
      }
      els[i]= new GrElement(ndsEle, oType);
    }
    return new GrMaillageElement(els, oNds);
  }
  @Override
  public final void autoApplique(final GrMorphisme _t) {
    int i, n;
    n= lnds().nombre();
    for (i= 0; i < n; i++) {
      lnds().renvoie(i).autoApplique(_t);
    }
  }
  public final int getNombre() {
    return elements_.nombre();
  }
  /**
   * Retourne la liste des ar�tes de contours sous forme d'�l�ments lin�iques
   * ordonn�s par contours.
   * DEBUG: cette methode est dangereuse car elle renvoie le tableau des elements de contours
   * N'importe qui peut le modifier.
   *
   * L'orientation des �lements peut �tre quelconque sans avoir d'influence
   * sur les fronti�res (contours) trouv�es.
   */
  public GrElement[][] aretesContours() {
    if (elsPeau_ != null) {
      return elsPeau_;
    }
    final GrElement[] elsOrient= new GrElement[elements_.nombre()];
    // Orientation des �l�ments tous dans l'ordre trigo
    final int nb=elements_.nombre();
    for (int i= 0; i < nb; i++) {
      final GrElement el= elements_.renvoie(i);
      final GrNoeud[] nds= new GrNoeud[el.noeuds_.length];
      System.arraycopy(el.noeuds_, 0, nds, 0, el.noeuds_.length);
      elsOrient[i]= new GrElement(nds, el.type_);
      elsOrient[i].orienteTrigo(true);
    }
    // D�composition des �l�ments en ar�tes de contours
    final Map acs= new HashMap(elsOrient.length * 3);
    for (int i= 0; i < elsOrient.length; i++) {
      final GrElement[] as= elsOrient[i].aretes();
      // Les ar�tes de contours pointent vers les �l�ments non orient�s.
      for (int j= 0; j < as.length; j++) {
        as[j].elementsSupport_[0]= elements_.renvoie(i);
      }
      for (int j= 0; j < as.length; j++) {
        int ndDeb;
        int ndFin;
        Integer key;
        AreteChainee ac;
        ndDeb= as[j].noeuds_[0].hashCode();
        ndFin= as[j].noeuds_[as[j].nombre() - 1].hashCode();
        if (ndDeb > ndFin) {
          key= new Integer(ndDeb);
        } else {
          key= new Integer(ndFin);
        }
        ac= (AreteChainee)acs.get(key);
        if (ac == null) {
          ac= new AreteChainee(as[j]);
          acs.put(key, ac);
        } else {
          while (ac.suivant_ != null) {
            ac= ac.suivant_;
          }
          ac.suivant_= new AreteChainee(as[j]);
        }
      }
    }
    // Suppression des doublons : seules les ar�tes restantes constituent les
    // fronti�res
    final List aretes= new ArrayList(acs.size());
    for (final Iterator e= acs.values().iterator(); e.hasNext();) {
      AreteChainee ac= (AreteChainee)e.next();
      AreteChainee acc;
      NEXTARETE : while (ac != null) {
        acc= ac;
        while ((acc.suivant_) != null) {
          // Doublon => suppression du doublon
          if (sontEgaux(ac.arete_, acc.suivant_.arete_)) {
            acc.suivant_= acc.suivant_.suivant_;
            ac= ac.suivant_;
            continue NEXTARETE;
          }
          acc= acc.suivant_;
        }
        // Pas de doublons => Stockage de l'ar�te
        aretes.add(ac.arete_);
        ac= ac.suivant_;
      }
    }
    // Ordonnancement des ar�tes par contours
    final Hashtable nd2Arete= new Hashtable(aretes.size());
    for (final Iterator e= aretes.iterator(); e.hasNext();) {
      final GrElement ar= (GrElement)e.next();
      nd2Arete.put(ar.noeuds_[0], ar);
    }
    final List cntrs= new ArrayList();
    while (nd2Arete.size() > 0) {
      final List cntr= new ArrayList();
      GrElement ar;
      GrNoeud nd;
      nd= ((GrElement)nd2Arete.elements().nextElement()).noeuds_[0];
      while ((ar= (GrElement)nd2Arete.remove(nd)) != null) {
        //        for (int i=0; i<ar.length-1; i++) cntr.add(ar[i]);
        cntr.add(ar);
        nd= ar.noeuds_[ar.nombre() - 1];
      }
      cntrs.add(cntr);
    }
    // Transfert vers un tableau d'ar�tes
    elsPeau_= new GrElement[cntrs.size()][];
    for (int i= 0; i < elsPeau_.length; i++) {
      final List cntr= (List)cntrs.get(i);
      elsPeau_[i]= new GrElement[cntr.size()];
      cntr.toArray(elsPeau_[i]);
    }
    return elsPeau_;
  }
  /**
   * Retourne les contours sous forme de noeuds ordonn�s. Le premier contour
   * est toujours le contour ext�rieur
   */
  public GrNoeud[][] noeudsContours() {
    if (aretesContours() == null) {
      return null;
    }
    final GrNoeud[][] noeuds= new GrNoeud[elsPeau_.length][];
    GrNoeud[] noeudsArete;
    int nbNoeuds;
    for (int i= 0; i < elsPeau_.length; i++) {
      // D�compte du nombre de noeuds sur le contour courant
      nbNoeuds= 0;
      for (int j= 0; j < elsPeau_[i].length; j++) {
        noeudsArete= elsPeau_[i][j].noeuds_;
        nbNoeuds += noeudsArete.length - 1;
      }
      noeuds[i]= new GrNoeud[nbNoeuds];
      // Stockage des noeuds pour le contour courant
      nbNoeuds= 0;
      for (int j= 0; j < elsPeau_[i].length; j++) {
        noeudsArete= elsPeau_[i][j].noeuds_;
        for (int k= 0; k < noeudsArete.length - 1; k++) {
          noeuds[i][nbNoeuds]= noeudsArete[k];
          nbNoeuds++;
        }
      }
    }
    return noeuds;
  }
  /**
   * Cr�e un maillage quadratique � partir d'un maillage lin�aire ou autre.
   */
  public GrMaillageElement versQuadratique() throws NoSuchMethodException {
    throw new NoSuchMethodException("Methode GrMaillageElement.versQuadratique() non implementee");
  }
  /**
   * Cr�e un maillage lin�aire � partir d'un maillage quadratique par
   * suppression des noeuds milieux ou d�coupage des �lements.
   *
   * @param _mode Le mode d'obtention du maillage lin�aire depuis le maillage
   *              quadratique.
   * <pre>
   *   1 : Le maillage est obtenu par suppression des noeuds milieux. Les noeuds
   *       sommets restent commun aux 2 maillages, le nombre d'�l�ments ne
   *       varie pas.
   *   2 : Le maillage est obtenu par d�coupage des �l�ments. Les noeuds sont
   *       communs entre les 2 maillages, le nombre d'�l�ments est modifi�.
   * </pre>
   *
   * @return Le maillage lin�aire.
   */
  public GrMaillageElement versLineaire(final int _mode) {
    // Suppression des noeuds milieux
    if (_mode == 1) {
      final GrElement[] els= new GrElement[elements_.nombre()];
      for (int i= 0; i < elements_.nombre(); i++) {
        els[i]= elements_.renvoie(i).versLineaire();
      }
      return new GrMaillageElement(els);
    }
    final List vels= new ArrayList();
    for (int i= 0; i < elements_.nombre(); i++) {
      final GrElement[] elsLin= elements_.renvoie(i).versLineaires();
      for (int j= 0; j < elsLin.length; j++) {
        vels.add(elsLin[j]);
      }
    }
    final GrElement[] els= (GrElement[])vels.toArray(new GrElement[vels.size()]);
    return new GrMaillageElement(els, noeuds());
  }
  /**
   * Cr�e un maillage lin�aire � partir d'un maillage quadratique.
   * Seuls les noeuds sommets sont conserv�s. Les noeuds restant sont communs
   * d'avec le maillage <I>this</I>.
   *
   * @deprecated Utiliser plut�t versLineaire(int _type).
   * @return Le maillage lin�aire.
   */
  public GrMaillageElement versLineaire() {
    return versLineaire(1);
    //    int       type;
    //    int       numero;
    //    GrNoeud[] noeuds;
    //
    //    GrElement[] els=new GrElement[elements_.nombre()];
    //
    //    for (int i=0; i<elements_.nombre(); i++) {
    //      type=elements_.renvoie(i).type;
    //      if      (type==GrElement.L3) type=GrElement.L2;
    //      else if (type==GrElement.T6) type=GrElement.T3;
    //      else if (type==GrElement.Q8) type=GrElement.Q4;
    //
    //      noeuds=elements_.renvoie(i).noeudsSommets();
    //
    //      els[i]=new GrElement(noeuds,type);
    //    }
    //
    //    return new GrMaillageElement(els,noeuds());
  }
  /**
   * Cr�e la liste des noeuds � partir des �l�ments. L'ordre des noeuds est
   * quelconque, et 2 maillages avec la m�me liste ordonn�e d'�l�ments pourront
   * avoir une liste de noeuds ordonn�e de mani�re diff�rente.
   */
  private  VecteurGrNoeud lnds() {
    if (noeuds_ == null) {
      noeuds_= new VecteurGrNoeud();
      final HashSet hNds= new HashSet(elements_.nombre() * 3);
      // Taille approximative
      for (int i= 0; i < elements_.nombre(); i++) {
        final GrNoeud[] nds= elements_.renvoie(i).noeuds_;
        for (int j= 0; j < nds.length; j++) {
          hNds.add(nds[j]);
        }
      }
      noeuds_.tableau((GrNoeud[])hNds.toArray(new GrNoeud[noeuds_.nombre()]));
    }
    return noeuds_;
  }
  //----------------------------------------------------------------------------
  // Une classe de chainage des ar�tes d'�l�ment
  //----------------------------------------------------------------------------
  static final class AreteChainee {
    GrElement arete_;
    AreteChainee suivant_;
    public AreteChainee(final GrElement _arete) {
      arete_= _arete;
      suivant_= null;
    }
  }
  /**
   * Une m�thode de comparaison de 2 �l�ments lin�iques. 2 �l�ments sont �gaux
   * s'ils ont les m�mes noeuds dans le m�me ordre ou dans l'ordre invers�
   */
  public static final boolean sontEgaux(final GrElement _a, final GrElement _b) {
    boolean egal;
    if (_a.noeuds_.length != _b.noeuds_.length) {
      return false;
    }
    final int n= _a.noeuds_.length;
    egal= true;
    for (int i= 0; i < n; i++) {
      if (_a.noeuds_[i] != _b.noeuds_[i]) {
        egal= false;
        break;
      }
    }
    if (!egal) {
      egal= true;
      for (int i= 0; i < n; i++) {
        if (_a.noeuds_[n - i - 1] != _b.noeuds_[i]) {
          egal= false;
          break;
        }
      }
    }
    return egal;
  }
}
