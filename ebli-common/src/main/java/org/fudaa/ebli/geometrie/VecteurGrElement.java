/*
 *  @creation     2002-06-25
 *  @modification $Date: 2006-09-19 14:55:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.geometrie;
import org.fudaa.ebli.commun.VecteurGenerique;
/**
 * vecteur de GrNoeud. Cette classe peut �tre utilisee � la place de
 * ListeGrNoeud. Moins performant pour les insertions mais moins gourmand en
 * m�moire : des noeuds de liaisons ne sont pas cr�es.
 *
 * @version   $Id: VecteurGrElement.java,v 1.7 2006-09-19 14:55:51 deniger Exp $
 * @author    Fred Deniger
 */
public final class VecteurGrElement extends VecteurGenerique {
  public VecteurGrElement() {
    super();
  }
  /**
   * Renvoie l'element d'index <code>_j</code>.
   * @param _j l'index de l'objet a renvoyer.
   * @return l'objet d'index <code>_j</code>.
   */
  public GrElement renvoie(final int _j) {
    return (GrElement)internRenvoie(_j);
  }
  /**
   * @return le tableau correspondant
   */
  public GrElement[] tableau() {
    final GrElement[] r= new GrElement[nombre()];
    v_.toArray(r);
    return r;
  }
  /**
   * Vide le vecteur et l'initialise avec <code>_o</code>.
   * @param _o le tableau des nouveaux elements.
   */
  public void setTableau(final GrElement[] _o) {
    super.internTableau(_o);
  }
  public void ajoute(final GrElement _o) {
    super.ajoute(_o);
  }
  public void insere(final GrElement _o, final int _j) {
    super.insere(_o, _j);
  }
  public void remplace(final GrElement _o, final int _j) {
    super.remplace(_o, _j);
  }
  public void enleve(final GrElement _o) {
    super.enleve(_o);
  }
  public int indice(final GrElement _o) {
    return super.indice(_o);
  }
  public boolean contient(final GrElement _o) {
    return super.contient(_o);
  }
}
