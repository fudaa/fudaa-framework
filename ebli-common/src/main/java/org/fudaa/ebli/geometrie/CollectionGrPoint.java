/*
 *  @creation     2002-09-16
 *  @modification $Date: 2006-04-12 15:28:02 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;
/**
 * vecteur de GrPoint. Cette classe peut �tre utilisee � la place de
 * ListeGrPoint. Moins performant pour les insertions mais moins gourmand en
 * m�moire : des noeuds de liaisons ne sont pas cr�es.
 *
 * @version   $Id: CollectionGrPoint.java,v 1.6 2006-04-12 15:28:02 deniger Exp $
 * @author    Fred Deniger
 */
public interface CollectionGrPoint {
  int nombre();
  boolean renvoie(GrPoint _p, int _i);
  double renvoieX(int _i);
  double renvoieY(int _i);
  double renvoieZ(int _i);
}
