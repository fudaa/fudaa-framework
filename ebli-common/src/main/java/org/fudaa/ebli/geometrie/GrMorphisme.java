/*
 * @creation 1998-03-25
 * 
 * @modification $Date: 2006-09-19 14:55:50 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;

import java.awt.geom.AffineTransform;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;

/**
 * Un morphisme 3D.
 * 
 * @version $Id: GrMorphisme.java,v 1.13 2006-09-19 14:55:50 deniger Exp $
 * @author Guillaume Desnoix
 */
public final class GrMorphisme {

  public final double[][] a_;

  public GrMorphisme() {
    a_ = new double[4][4];
    a_[0][0] = 1.;
    a_[1][1] = 1.;
    a_[2][2] = 1.;
    a_[3][3] = 1.;
  }

  public boolean isSame(final GrMorphisme _m) {
    if (_m == this) { return true; }
    for (int i = a_.length - 1; i >= 0; i--) {
      if (!CtuluLibArray.isDoubleEquals(a_[i], _m.a_[i], 1E-3d)) { return false; }
    }
    return true;
  }
  
//  GrMorphisme [10.238227282204456, 0.0, 0.0, 0.0][0.0, -10.238227282204456, 0.0, 0.0][0.0, 1.253821227009479E-15, 0.0, 0.0][-100.08227286972827, 3020.170497076333, 0.0, 1.0
//  GrMorphisme [10.238227282204456, 0.0, 0.0, 0.0][0.0, -10.238227282204456, 0.0, 0.0][0.0, 1.253821227009479E-15, 0.0, 0.0][-100.08227286972827, 3009.9322697941284, 0.0, 1.0

  @Override
  public int hashCode() {
    double val = 0;
    for (int i = 0; i < a_.length; i++) {
      val += CtuluLibArray.getSum(a_[i]);

    }
    return (int)val;
  }

  @Override
  public String toString() {
    return "GrMorphisme [" + CtuluLibString.arrayToString(a_[0]) + "][" + CtuluLibString.arrayToString(a_[1]) + "]["
        + CtuluLibString.arrayToString(a_[2]) + "][" + CtuluLibString.arrayToString(a_[3]);
  }

  @Override
  public boolean equals(Object obj) {
    if (obj != null && obj.getClass().equals(getClass())) { return isSame((GrMorphisme) obj); }
    return false;
  }

  /*
   * public final String toString() { return m.toString(); }
   */
  public static GrMorphisme nul() {
    final GrMorphisme t = new GrMorphisme();
    t.a_[0][0] = 0.;
    t.a_[1][1] = 0.;
    t.a_[2][2] = 0.;
    t.a_[3][3] = 0.;
    return t;
  }

  /**
   * Methode testee dans TestJGr.
   * 
   * @param _mToModify l'AffineTransform a modifier avec ce morphisme
   */
  public void creeAffineTransorm2D(final AffineTransform _mToModify) {
    _mToModify.setTransform(a_[0][0], a_[0][1], a_[1][0], a_[1][1], a_[3][0], a_[3][1]);
  }

  /**
   * @return l'AffineTransform correspondante ce morphisme
   */
  public AffineTransform creeAffineTransorm2D() {
    final AffineTransform r = new AffineTransform();
    creeAffineTransorm2D(r);
    return r;
  }

  public static GrMorphisme identite() {
    return new GrMorphisme();
  }

  public static GrMorphisme translationX(final double _tx) {
    final GrMorphisme t = new GrMorphisme();
    t.a_[3][0] = _tx;
    return t;
  }

  public static GrMorphisme translationY(final double _ty) {
    final GrMorphisme t = new GrMorphisme();
    t.a_[3][1] = _ty;
    return t;
  }

  public static GrMorphisme translationZ(final double _tz) {
    final GrMorphisme t = new GrMorphisme();
    t.a_[3][2] = _tz;
    return t;
  }

  public static GrMorphisme translation(final double _tx, final double _ty, final double _tz) {
    final GrMorphisme t = new GrMorphisme();
    t.a_[3][0] = _tx;
    t.a_[3][1] = _ty;
    t.a_[3][2] = _tz;
    return t;
  }

  public static GrMorphisme translation(final GrVecteur _v) {
    final GrMorphisme t = new GrMorphisme();
    t.a_[3][0] = _v.x_;
    t.a_[3][1] = _v.y_;
    t.a_[3][2] = _v.z_;
    return t;
  }

  public static GrMorphisme dilatation(final double _e) {
    final GrMorphisme t = new GrMorphisme();
    t.a_[0][0] = _e;
    t.a_[1][1] = _e;
    t.a_[2][2] = _e;
    return t;
  }

  public static GrMorphisme dilatation(final double _ex, final double _ey, final double _ez) {
    final GrMorphisme t = new GrMorphisme();
    t.a_[0][0] = _ex;
    t.a_[1][1] = _ey;
    t.a_[2][2] = _ez;
    return t;
  }

  public static GrMorphisme rotationX(final double _rx) {
    final GrMorphisme t = new GrMorphisme();
    t.a_[1][1] = Math.cos(_rx);
    t.a_[1][2] = Math.sin(_rx);
    t.a_[2][2] = Math.cos(_rx);
    t.a_[2][1] = Math.sin(-_rx);
    return t;
  }

  public static GrMorphisme rotationY(final double _ry) {
    final GrMorphisme t = new GrMorphisme();
    t.a_[0][0] = Math.cos(_ry);
    t.a_[0][2] = Math.sin(-_ry);
    t.a_[2][2] = Math.cos(_ry);
    t.a_[2][0] = Math.sin(_ry);
    return t;
  }

  public static GrMorphisme rotationZ(final double _rz) {
    final GrMorphisme t = new GrMorphisme();
    t.a_[0][0] = Math.cos(_rz);
    t.a_[0][1] = Math.sin(_rz);
    t.a_[1][1] = Math.cos(_rz);
    t.a_[1][0] = Math.sin(-_rz);
    return t;
  }

  public static GrMorphisme rotation(final double _rx, final double _ry, final double _rz) {
    return rotationX(_rx).composition(rotationY(_ry).composition(rotationZ(_rz)));
  }

  public static GrMorphisme rotation(final double _rx, final double _ry, final double _rz, final GrPoint _p) {
    return translation(-_p.x_, -_p.y_, -_p.z_).composition(
        rotation(_rx, _ry, _rz).composition(translation(_p.x_, _p.y_, _p.z_)));
  }

  public static GrMorphisme cisaillement(final double _sx, final double _sy, final double _sz) {
    final GrMorphisme t = new GrMorphisme();
    t.a_[0][3] = _sx;
    t.a_[1][3] = _sy;
    t.a_[2][3] = _sz;
    return t;
  }

  public static GrMorphisme projectionXY() {
    final GrMorphisme t = new GrMorphisme();
    t.a_[2][2] = 0.;
    return t;
  }

  public static GrMorphisme projectionPerspective(final double _d) {
    final GrMorphisme t = new GrMorphisme();
    t.a_[2][2] = 0.;
    t.a_[2][3] = 1. / _d;
    return t;
  }

  public static GrMorphisme projectionCavaliere(final double _a) {
    final GrMorphisme t = new GrMorphisme();
    t.a_[2][0] = Math.cos(-_a);
    t.a_[2][1] = Math.sin(-_a);
    return t;
  }

  public GrMorphisme composition(final GrMorphisme _o) {
    final double[][] b = new double[4][4];
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 4; j++) {
        for (int k = 0; k < 4; k++) {
          b[i][j] += a_[i][k] * _o.a_[k][j];
        }
      }
    }
    CtuluLibArray.copyIn(b, a_);
    return this;
  }

  public GrMorphisme initFrom(final GrMorphisme _o) {
    if (_o != null) {
      CtuluLibArray.copyIn(_o.a_, a_);
    }
    return this;
  }

  public GrMorphisme initFrom(final double[][] _o) {
    if (_o != null) {
      CtuluLibArray.copyIn(_o, a_);
    }
    return this;
  }

  public static double convertDistanceXY(final GrMorphisme _t, final double _initDist) {
    final double tx2 = _initDist * _t.a_[1][0];
    final double ty2 = _initDist * _t.a_[1][1];
    return Math.hypot(tx2, ty2);

  }
}