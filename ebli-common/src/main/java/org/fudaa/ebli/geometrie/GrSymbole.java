/*
 * @creation     1999-09-23
 * @modification $Date: 2006-09-19 14:55:50 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;
import java.awt.Point;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
/**
 * Un symboles. Les symboles sont ancr�s � un point 3D,
 * peuvent ou non suivre la rotation suivant Z. Leur taille est fix�e.
 * Le symbole sera dessine entre [0,1] suivant x et y.
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 14:55:50 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public final class GrSymbole extends GrObjet implements GrContour {
  public static final int CARRE= 0;
  public static final int CERCLE= 1;
  public static final int LOSANGE= 2;
  public static final int FLECHE= 3;
  public static final int FLECHE_LIEE= 4;
  public static final int FLECHE_DIFFRACTEE= 5;
  public static final int TRIANGLE= 6;
  public GrPoint position_;
  public GrVecteur direction_;
  public Shape trace_;
  public double echelle_;
  public double xAncre_;
  public double yAncre_;
  public boolean suitRotationZ_;
  /**
   * Cr�ation d'un symbole par d�faut.
   */
  public GrSymbole() {
    this(CARRE, new GrPoint(0., 0., 0.), 0., 1.);
  }
  /**
   * Cr�ation d'un symbole suivant un trace utilisateur.
   */
  public GrSymbole(
    final Shape _trace,
    final GrPoint _position,
    final double _xAncre,
    final double _yAncre,
    final double _rotZ,
    final double _echelle) {
    init(_trace, _position, _xAncre, _yAncre, _rotZ, _echelle);
  }
  /**
   * Cr�ation d'un symbole suivant un type pr�d�fini.
   * @param _type     Type de symbole (CARRE, CERCLE, LOSANGE, etc.).
   * @param _position Point 3D d'affichage du symbole.
   * @param _rotZ     Rotation en degr�s du symbole par rapport � l'axe Z.
   * @param _echelle  Echelle du symbole. 1. repr�sente 1 pixel � l'�cran.
   */
  public GrSymbole(
    final int _type,
    final GrPoint _position,
    final double _rotZ,
    final double _echelle) {
    //Shape trace;
    //double xAncre;
    //double yAncre;
    if (_type == CARRE) {
      final Rectangle2D r= new Rectangle2D.Double(0., 0., 1., 1.);
      xAncre_= 0.5;
      yAncre_= 0.5;
      trace_= r;
    } else if (_type == CERCLE) {
      final Ellipse2D e= new Ellipse2D.Double(0., 0., 1., 1.);
      xAncre_= 0.5;
      yAncre_= 0.5;
      trace_= e;
    } else if (_type == LOSANGE) {
      final GeneralPath gp= new GeneralPath();
      gp.moveTo(0.5f, 0.0f);
      gp.lineTo(1.0f, 0.5f);
      gp.lineTo(0.5f, 1.0f);
      gp.lineTo(0.0f, 0.5f);
      gp.closePath();
      xAncre_= 0.5;
      yAncre_= 0.5;
      trace_= gp;
    } else if (_type == FLECHE) {
      final GeneralPath gp= new GeneralPath();
      gp.moveTo((float) (4.0 / 15.), (float) (2.0 / 15.));
      gp.lineTo((float) (4.0 / 15.), (float) (15.0 / 15.));
      gp.moveTo((float) (0.0 / 15.), (float) (11.0 / 15.));
      gp.lineTo((float) (4.0 / 15.), (float) (15.0 / 15.));
      gp.lineTo((float) (8.0 / 15.), (float) (11.0 / 15.));
      xAncre_= 4. / 15.;
      yAncre_= 2. / 15.;
      trace_= gp;
    } else if (_type == FLECHE_LIEE) {
      final GeneralPath gp= new GeneralPath();
      gp.moveTo((float) (2.0 / 15.), (float) (0.0 / 15.));
      gp.lineTo((float) (6.0 / 15.), (float) (0.0 / 15.));
      gp.lineTo((float) (6.0 / 15.), (float) (4.0 / 15.));
      gp.lineTo((float) (2.0 / 15.), (float) (4.0 / 15.));
      gp.closePath();
      gp.moveTo((float) (4.0 / 15.), (float) (4.0 / 15.));
      gp.lineTo((float) (4.0 / 15.), (float) (15.0 / 15.));
      gp.moveTo((float) (0.0 / 15.), (float) (11.0 / 15.));
      gp.lineTo((float) (4.0 / 15.), (float) (15.0 / 15.));
      gp.lineTo((float) (8.0 / 15.), (float) (11.0 / 15.));
      xAncre_= 4. / 15.;
      yAncre_= 2. / 15.;
      trace_= gp;
    } else if (_type == FLECHE_DIFFRACTEE) {
      final GeneralPath gp= new GeneralPath();
      gp.moveTo((float) (4.0 / 15.), (float) (2.0 / 15.));
      gp.lineTo((float) (4.0 / 15.), (float) (15.0 / 15.));
      gp.moveTo((float) (0.0 / 15.), (float) (11.0 / 15.));
      gp.lineTo((float) (4.0 / 15.), (float) (15.0 / 15.));
      gp.lineTo((float) (8.0 / 15.), (float) (11.0 / 15.));
      gp.moveTo((float) (0.0 / 15.), (float) (5.0 / 15.));
      gp.lineTo((float) (2.0 / 15.), (float) (7.0 / 15.));
      gp.lineTo((float) (6.0 / 15.), (float) (7.0 / 15.));
      gp.lineTo((float) (8.0 / 15.), (float) (5.0 / 15.));
      gp.moveTo((float) (2.0 / 15.), (float) (4.0 / 15.));
      gp.lineTo((float) (3.0 / 15.), (float) (5.0 / 15.));
      gp.lineTo((float) (5.0 / 15.), (float) (5.0 / 15.));
      gp.lineTo((float) (6.0 / 15.), (float) (4.0 / 15.));
      xAncre_= 4. / 15.;
      yAncre_= 2. / 15.;
      trace_= gp;
    } else {
      final GeneralPath gp= new GeneralPath();
      gp.moveTo(0.0f, 0.067f);
      gp.lineTo(1.0f, 0.067f);
      gp.lineTo(0.5f, 0.933f);
      gp.closePath();
      xAncre_= 0.5;
      yAncre_= 0.5;
      trace_= gp;
    }
    init(trace_, _position, xAncre_, yAncre_, _rotZ, _echelle);
  }
  private  void init(
    final Shape _trace,
    final GrPoint _position,
    final double _xAncre,
    final double _yAncre,
    final double _rotZ,
    final double _echelle) {
    trace_= _trace;
    position_= _position;
    xAncre_= _xAncre;
    yAncre_= _yAncre;
    setRotationZInitiale(_rotZ);
    echelle_= _echelle;
    suitRotationZ_= true;
  }
  /**
   * D�finit l'angle de rotation du symbole autour de Z �cran.
   * @param _rotZ Angle de rotation autour de Z �cran en degr�s.
   */
  public  void setRotationZInitiale( final double _rotZ) {
    double rotZ=_rotZ;
    rotZ= rotZ * Math.PI / 180.;
    direction_= new GrVecteur(Math.cos(rotZ), Math.sin(rotZ), 0.);
  }
  /**
   * Retourne l'angle de rotation du symbole autour de Z �cran.
   * @return Angle de rotation autour de Z �cran en degr�s.
   */
  public  double getRotationZInitiale() {
    return Math.atan2(direction_.y_, direction_.x_) * 180. / Math.PI;
  }
  @Override
  public  String toString() {
    return "GrSymbole("
      + position_.x_
      + ", "
      + position_.y_
      + ", "
      + position_.z_
      + ")";
  }
  public  GrSymbole applique(final GrMorphisme _t) {
    final GrSymbole r= new GrSymbole();
    r.trace_= trace_;
    r.position_= position_.applique(_t);
    r.xAncre_= xAncre_;
    r.yAncre_= yAncre_;
    r.direction_= direction_.applique(_t);
    r.echelle_= echelle_;
    r.suitRotationZ_= suitRotationZ_;
    return r;
  }
  @Override
  public  void autoApplique(final GrMorphisme _t) {
    position_.applique(_t);
    direction_.applique(_t);
  }
  /**
   * Impl�mentation GrObjet.
   */
  @Override
  public  GrBoite boite() {
    return position_.boite();
  }
  @Override
  public  GrPoint[] contour() {
    return new GrPoint[] { this.position_ };
  }
  /**
   * Indique si l'objet est s�lectionn� pour un point donn�.<p>
   *
   * @see GrContour
   *
   * @author b.marchand Le 26/10/2000
   */
  @Override
  public  boolean estSelectionne(
    final GrMorphisme _ecran,
    final double _dist,
    final Point _pt) {
    return position_.estSelectionne(_ecran, _dist, _pt);
  }
}
