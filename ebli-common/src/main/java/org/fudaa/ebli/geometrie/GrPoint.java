/*
 * @creation 1998-03-25
 * 
 * @modification $Date: 2006-12-20 16:08:24 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.geometrie;

import java.awt.Point;
import java.awt.geom.Point2D;

import org.locationtech.jts.geom.Coordinate;

/**
 * Un point 3D.
 *
 * @version $Revision: 1.19 $ $Date: 2006-12-20 16:08:24 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public final class GrPoint extends GrObjet implements GrContour {

  public double x_;
  public double y_;
  public double z_;

  public GrPoint() {
  }

  public GrPoint(final GrPoint _p) {
    initialiseAvec(_p);
  }

  public GrPoint(final Coordinate _p) {
    initialiseAvec(_p);
  }

  public GrPoint(final double _x, final double _y, final double _z) {
    x_ = _x;
    y_ = _y;
    z_ = _z;
  }

  public GrPoint(final double[] _c) {
    if (_c.length > 0) {
      x_ = _c[0];
    }
    if (_c.length > 1) {
      y_ = _c[1];
    }
    if (_c.length > 2) {
      z_ = _c[2];
    }
  }

  public GrPoint(final Point _p) {
    x_ = _p.x;
    y_ = _p.y;
  }

  public void initialiseAvec(final GrPoint _p) {
    x_ = _p.x_;
    y_ = _p.y_;
    z_ = _p.z_;
  }

  public void initialiseAvec(final Coordinate _p) {
    x_ = _p.x;
    y_ = _p.y;
  }

  public void initialiseAvec(final Point2D.Double _p) {
    x_ = _p.x;
    y_ = _p.y;
    z_ = 0;
  }

  public void setCoordonnees(final double _x, final double _y, final double _z) {
    x_ = _x;
    y_ = _y;
    z_ = _z;
  }

  @Override
  public String toString() {
    return "GrPoint(" + x_ + ", " + y_ + ", " + z_ + ")";
  }

  public GrPoint addition(final GrVecteur _v) {
    return new GrPoint(x_ + _v.x_, y_ + _v.y_, z_ + _v.z_);
  }

  public void autoAddition(final GrVecteur _v) {
    x_ += _v.x_;
    y_ += _v.y_;
    z_ += _v.z_;
  }

  public void autoAddition(final double _x, final double _y) {
    x_ += _x;
    y_ += _y;
  }

  public GrPoint soustraction(final GrVecteur _v) {
    return new GrPoint(x_ - _v.x_, y_ - _v.y_, z_ - _v.z_);
  }

  public GrVecteur soustraction(final GrPoint _p) {
    return new GrVecteur(x_ - _p.x_, y_ - _p.y_, z_ - _p.z_);
  }

  public boolean comparaison(final GrPoint _p) {
    return x_ < _p.x_ ? true : x_ > _p.x_ ? false : y_ < _p.y_ ? true : y_ > _p.y_ ? false : z_ < _p.x_;
  }

  public double distance(final GrPoint _p) {
    //
    return Math.sqrt((x_ - _p.x_) * (x_ - _p.x_) + (y_ - _p.y_) * (y_ - _p.y_) + (z_ - _p.z_) * (z_ - _p.z_));
  }

  public double distanceXY(final GrPoint _p) {
    return Math.sqrt((x_ - _p.x_) * (x_ - _p.x_) + (y_ - _p.y_) * (y_ - _p.y_));
  }

  public double distanceAuCarre(final GrPoint _p) {
    double dx = x_ - _p.x_;
    double dy = y_ - _p.y_;
    return dx * dx + dy * dy;
  }

  public GrPoint applique(final GrMorphisme _t) {
    final GrPoint r = new GrPoint();
    r.x_ = x_ * _t.a_[0][0] + y_ * _t.a_[1][0] + z_ * _t.a_[2][0] + _t.a_[3][0];
    r.y_ = x_ * _t.a_[0][1] + y_ * _t.a_[1][1] + z_ * _t.a_[2][1] + _t.a_[3][1];
    r.z_ = x_ * _t.a_[0][2] + y_ * _t.a_[1][2] + z_ * _t.a_[2][2] + _t.a_[3][2];
    return r;
  }

  @Override
  public void autoApplique(final GrMorphisme _t) {
    final double tx = x_ * _t.a_[0][0] + y_ * _t.a_[1][0] + z_ * _t.a_[2][0] + _t.a_[3][0];
    final double ty = x_ * _t.a_[0][1] + y_ * _t.a_[1][1] + z_ * _t.a_[2][1] + _t.a_[3][1];
    final double tz = x_ * _t.a_[0][2] + y_ * _t.a_[1][2] + z_ * _t.a_[2][2] + _t.a_[3][2];
    x_ = tx;
    y_ = ty;
    z_ = tz;
  }

  public void autoApplique2D(final GrMorphisme _t) {
    final double tx = x_ * _t.a_[0][0] + y_ * _t.a_[1][0] + z_ * _t.a_[2][0] + _t.a_[3][0];
    final double ty = x_ * _t.a_[0][1] + y_ * _t.a_[1][1] + z_ * _t.a_[2][1] + _t.a_[3][1];
    x_ = tx;
    y_ = ty;
  }

  @Override
  public GrPoint[] contour() {
    return new GrPoint[]{this};
  }

  /**
   * Impl�mentation GrObjet.
   */
  @Override
  public GrBoite boite() {
    final GrBoite r = new GrBoite();
    r.ajuste(this);
    return r;
  }

  /**
   * Retourne un point AWT. b.marchand Le 26/10/2000
   */
  public Point point() {
    return new Point((int) x_, (int) y_);
  }

  public Point2D.Double getPoint2D() {
    return new Point2D.Double(x_, y_);
  }

  /**
   * Indique si l'objet est s�lectionn� pour un point donn�.
   *
   * @param _ecran Le morphisme pour la transformation en coordonn�es �cran.
   * @param _dist La tol�rence en coordonn�es �cran pour laquelle on consid�re l'objet s�lectionn�.
   * @param _pt Le point de s�lection en coordonn�es �cran.
   * @return <I>true</I> L'objet est s�lectionn�. <I>false</I> L'objet n'est pas s�lectionn�.
   * @author b.marchand Le 26/10/2000
   */
  @Override
  public boolean estSelectionne(final GrMorphisme _ecran, final double _dist, final Point _pt) {
    final GrPoint p = applique(_ecran);
    return _pt.distance(p.point()) < _dist;
  }

  /**
   * Indique si l'objet
   * <code>_pTest</code> est s�lectionn� pour un point donn�
   * <code>_pRef</code>.
   *
   * @param _dist La tol�rence en coordonn�es �cran pour laquelle on consid�re l'objet s�lectionn�.
   * @param _pRef Le point de s�lection en coordonn�es �cran (il estmodifie ).
   * @param _pTest Le point a teste.
   * @return <I>true</I> L'objet est s�lectionn�. <I>false</I> L'objet n'est pas s�lectionn�.
   */
  public static boolean estSelectionne(final GrPoint _pTest, final double _dist, final GrPoint _pRef) {
    return _pTest.distanceXY(_pRef) < _dist;
  }
  
  @Override
  public boolean equals(Object _o) {
    if (!(_o instanceof GrPoint)) return false;
    GrPoint o=(GrPoint)_o;
    
    return x_==o.x_ && y_==o.y_ && z_==o.z_;
  }
}
