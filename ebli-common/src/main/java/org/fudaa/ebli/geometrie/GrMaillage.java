/*
 * @modification $Date: 2006-09-19 14:55:50 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;

import java.awt.Polygon;
import java.util.ArrayList;
import java.util.List;

/**
 * Maillage.
 *
 * @version $Id: GrMaillage.java,v 1.8 2006-09-19 14:55:50 deniger Exp $
 * @author Guillaume Desnoix
 */
public class GrMaillage extends GrObjet {
  /**
   * Liste des noeuds du maillage ordonn�e.
   */
  public VecteurGrPoint noeuds_;
  /**
   * Liste des connectivit�s des �l�ments. La connectivit� pour chaque �l�ment est un tableau d'entiers (int[]) dont
   * chaque valeur repr�sente un indice sur la liste des noeuds, indice d�butant � 0.
   */
  public List connectivites_;

  public GrMaillage() {
    noeuds_ = new VecteurGrPoint();
    connectivites_ = new ArrayList();
  }

  @Override
  public String toString() {
    return "GrMaillage(" + noeuds_.nombre() + " noeuds, " + connectivites_.size() + " connectivites)";
  }

  /**
   * Impl�mentation GrObjet.
   */
  @Override
  public GrBoite boite() {
    final GrBoite r = new GrBoite();
    final int n = noeuds_.nombre();
    for (int i = 0; i < n; i++) {
      r.ajuste(noeuds_.renvoie(i));
    }
    return r;
  }

  public GrMaillage applique(final GrMorphisme _t) {
    final GrMaillage r = new GrMaillage();
    int i, n;
    n = noeuds_.nombre();
    for (i = 0; i < n; i++) {
      r.noeuds_.ajoute(noeuds_.renvoie(i).applique(_t));
    }
    r.connectivites_ = connectivites_;
    return r;
  }

  @Override
  public void autoApplique(final GrMorphisme _t) {
    int i, n;
    n = noeuds_.nombre();
    for (i = 0; i < n; i++) {
      noeuds_.renvoie(i).autoApplique(_t);
    }
  }

  public int nombre() {
    return connectivites_.size();
  }

  public Polygon polygon(final int _i) {
    final Polygon r = new Polygon();
    final int[] c = (int[]) connectivites_.get(_i);
    for (int j = 0; j < c.length; j++) {
      final GrPoint p = noeuds_.renvoie(c[j]);
      r.addPoint((int) p.x_, (int) p.y_);
    }
    return r;
  }

  public GrPolygone polygone(final int _i) {
    final GrPolygone r = new GrPolygone();
    final int[] c = (int[]) connectivites_.get(_i);
    for (int j = 0; j < c.length; j++) {
      r.sommets_.ajoute(noeuds_.renvoie(c[j]));
    }
    return r;
  }
}
