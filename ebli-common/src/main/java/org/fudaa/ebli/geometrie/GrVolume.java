/*
 * @creation     1998-07-21
 * @modification $Date: 2006-09-19 14:55:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;

import org.fudaa.ctulu.CtuluLibString;

/**
 * Un volume 3D.
 *
 * @version $Id: GrVolume.java,v 1.7 2006-09-19 14:55:51 deniger Exp $
 * @author Guillaume Desnoix
 */
public class GrVolume extends GrObjet {
  public VecteurGrPolygone polygones_;

  public GrVolume() {
    polygones_ = new VecteurGrPolygone();
  }

  public int nombre() {
    return polygones_.nombre();
  }

  public GrPolygone polygone(final int _idx) {
    int idx=_idx;
    final int n = nombre();
    if (n == 0) {
      return null;
    }
    while (idx < 0) {
      idx += n;
    }
    while (idx >= n) {
      idx -= n;
    }
    return polygones_.renvoie(idx);
  }

  @Override
  public String toString() {
    final int n = nombre();
    return "\t" + getClass().getName() + " (" + n + " polygones)\n" + "\tBarycentre=" + barycentre()
        + CtuluLibString.LINE_SEP + "\tCentre=" + centre() + CtuluLibString.LINE_SEP
        // r+="\tPerimetre=" +perimetreXY()+CtululibString.LINE_SEP;
        // r+="\tDiametre=" +diametreXY() +CtululibString.LINE_SEP;
        + "\tSimple=" + estSimple() + CtuluLibString.LINE_SEP + "\tConvexe=" + estConvexe() + CtuluLibString.LINE_SEP;
  }

  /**
   * Implémentation GrObjet.
   */
  @Override
  public GrBoite boite() {
    final GrBoite r = new GrBoite();
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      final GrPolygone g = polygone(i);
      final int np = g.nombre();
      for (int j = 0; j < np; j++) {
        r.ajuste(g.sommet(i));
      }
    }
    return r;
  }

  public GrPoint barycentre() {
    double x = 0., y = 0., z = 0.;
    final int n = nombre();
    final double qt = n;
    for (int i = 0; i < n; i++) {
      final GrPoint p = polygone(i).centre();
      x += p.x_;
      y += p.y_;
      z += p.z_;
    }
    return new GrPoint(x / qt, y / qt, z / qt);
  }

  public GrPoint centre() {
    return barycentre();
  }

  public boolean estConvexe() {
    boolean r = true;
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      r &= polygone(i).estConvexe();
    }
    return r;
  }

  public boolean estSimple() {
    boolean r = true;
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      r &= polygone(i).estSimple();
    }
    return r;
  }

  public boolean comparaison(final GrVolume _p) {
    return centre().comparaison(_p.centre());
  }

  public GrVolume applique(final GrMorphisme _t) {
    final GrVolume r = new GrVolume();
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      r.polygones_.ajoute(polygone(i).applique(_t));
    }
    return r;
  }

  @Override
  public void autoApplique(final GrMorphisme _t) {
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      polygone(i).autoApplique(_t);
    }
  }

  public static GrVolume creeCube() {
    GrVolume vol;
    GrPolygone pol;
    vol = new GrVolume();
    // face dessus
    pol = new GrPolygone();
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(0., 1., 0.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(1., 1., 0.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(1., 1., 1.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(0., 1., 1.));
    vol.polygones_.ajoute(pol);
    // face derriere
    pol = new GrPolygone();
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(1., 1., 1.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(1., 0., 1.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(0., 0., 1.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(0., 1., 1.));
    vol.polygones_.ajoute(pol);
    // face dessous
    pol = new GrPolygone();
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(0., 0., 0.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(0., 0., 1.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(1., 0., 1.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(1., 0., 0.));
    vol.polygones_.ajoute(pol);
    // face devant
    pol = new GrPolygone();
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(0., 0., 0.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(1., 0., 0.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(1., 1., 0.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(0., 1., 0.));
    vol.polygones_.ajoute(pol);
    // face droite
    pol = new GrPolygone();
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(1., 0., 0.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(1., 0., 1.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(1., 1., 1.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(1., 1., 0.));
    vol.polygones_.ajoute(pol);
    // face gauche
    pol = new GrPolygone();
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(0., 0., 0.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(0., 1., 0.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(0., 1., 1.));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(0., 0., 1.));
    vol.polygones_.ajoute(pol);
    return vol;
  }

  public static GrVolume creeBoite(final GrBoite _b) {
    GrVolume vol;
    GrPolygone pol;
    vol = new GrVolume();
    final double ox = _b.o_.x_;
    final double oy = _b.o_.y_;
    final double oz = _b.o_.z_;
    final double ex = _b.e_.x_;
    final double ey = _b.e_.y_;
    final double ez = _b.e_.z_;
    // face dessus
    pol = new GrPolygone();
    pol.sommets_.ajoute(_b.o_);
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ex, oy, oz));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ex, oy, oz));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ex, oy, ez));
    vol.polygones_.ajoute(pol);
    // face derriere
    pol = new GrPolygone();
    pol.sommets_.ajoute(_b.e_);
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ox, ey, ez));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ox, oy, ez));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ex, oy, ez));
    vol.polygones_.ajoute(pol);
    // face dessous
    pol = new GrPolygone();
    pol.sommets_.ajoute(_b.e_);
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ex, ey, oz));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ex, oy, ez));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ox, ey, ez));
    vol.polygones_.ajoute(pol);
    // face devant
    pol = new GrPolygone();
    pol.sommets_.ajoute(_b.o_);
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ox, ey, oz));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ex, ey, oz));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ex, oy, oz));
    vol.polygones_.ajoute(pol);
    // face droite
    pol = new GrPolygone();
    pol.sommets_.ajoute(_b.e_);
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ex, oy, ez));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ex, oy, oz));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ex, ey, oz));
    vol.polygones_.ajoute(pol);
    // face gauche
    pol = new GrPolygone();
    pol.sommets_.ajoute(_b.o_);
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ox, oy, ez));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ox, ey, ez));
    pol.sommets_.ajoute(new org.fudaa.ebli.geometrie.GrPoint(ox, ey, oz));
    vol.polygones_.ajoute(pol);
    return vol;
  }
}
