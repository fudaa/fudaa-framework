/*
 * @creation     1997-03-25
 * @modification $Date: 2006-11-20 08:40:16 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;

import java.awt.Point;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;

/**
 * Un segment 3D.
 *
 * @version $Revision: 1.16 $ $Date: 2006-11-20 08:40:16 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public final class GrSegment extends GrObjet implements GrContour {

  public GrPoint o_;
  public GrPoint e_;

  public GrSegment() {
  }

  public GrSegment(final GrPoint _io, final GrPoint _ie) {
    o_ = _io;
    e_ = _ie;
  }

  public GrVecteur vecteur() {
    return e_.soustraction(o_);
  }

  @Override
  public String toString() {
    return "GrSegment(" + o_ + ", " + e_ + ")";
  }

  public GrPoint milieu() {
    return new GrPoint(getMidX(), getMidY(), (o_.z_ + e_.z_) / 2.);
  }

  public double getMidX() {
    return (o_.x_ + e_.x_) / 2D;
  }

  public double getMidY() {
    return (o_.y_ + e_.y_) / 2D;
  }

  public double getVx() {
    return e_.x_ - o_.x_;
  }

  public double getVy() {
    return e_.y_ - o_.y_;
  }

  public void milieu(final GrPoint _p) {
    _p.setCoordonnees((o_.x_ + e_.x_) / 2., (o_.y_ + e_.y_) / 2., (o_.z_ + e_.z_) / 2.);
  }

  /**
   * Impl�mentation GrObjet.
   */
  @Override
  public GrBoite boite() {
    final GrBoite r = new GrBoite();
    boite(r);
    return r;
  }

  /**
   * Initialise la boite passee en parametre a partir de la boite de ce segment.
   */
  public void boite(final GrBoite _b) {
    if (_b.e_ == null) {
      _b.e_ = new GrPoint(e_);
    } else {
      _b.e_.initialiseAvec(e_);
    }
    if (_b.o_ == null) {
      _b.o_ = new GrPoint(e_);
    } else {
      _b.o_.initialiseAvec(e_);
    }
    if (o_.x_ < e_.x_) {
      _b.o_.x_ = o_.x_;
    }
    if (o_.y_ < e_.y_) {
      _b.o_.y_ = o_.y_;
    }
    if (o_.z_ < e_.z_) {
      _b.o_.z_ = o_.z_;
    }
    if (o_.x_ > e_.x_) {
      _b.e_.x_ = o_.x_;
    }
    if (o_.y_ > e_.y_) {
      _b.e_.y_ = o_.y_;
    }
    if (o_.z_ > e_.z_) {
      _b.e_.z_ = o_.z_;
    }
  }

  public double longueurXY() {
    return o_.distanceXY(e_);
  }

  public double longueurAuCarre() {
    return o_.distanceAuCarre(e_);
  }

  public double longueur() {
    return o_.distance(e_);
  }

  /**
   * Calcul la distance avec le point _p.
   */
  public double distanceXY(final GrPoint _p) {
    return CtuluLibGeometrie.distanceFromSegment(e_.x_, e_.y_, o_.x_, o_.y_, _p.x_, _p.y_);
  }

  /**
   * Calcul de la distance d'un point au segment. Cherche le point le plus proche sur le segment pour le calcul de cette distance.
   */
  public double distance(final GrPoint _p) {
    /*
     * double d=p.soustraction(o).division(vecteur()); double r; if(d<=0.) r=o.distance(p); else if(d>=1.)
     * r=e.distance(p); else r=o.addition(vecteur().multiplication(d)).distance(p); return r;
     */
    return pointPlusProche(_p).distance(_p);
  }

  /**
   * Retourne le point appartenant au segment le plus proche du point donne.
   */
  public GrPoint pointPlusProche(final GrPoint _p) {
    final double d = _p.soustraction(o_).division(vecteur());
    GrPoint r;
    if (d <= 0.) {
      r = new GrPoint(o_.x_, o_.y_, o_.z_);
    } else if (d >= 1.) {
      r = new GrPoint(e_.x_, e_.y_, e_.z_);
    } else {
      r = o_.addition(vecteur().multiplication(d));
    }
    return r;
  }

  /**
   * Retourne le point appartenant au segment le plus proche du point donne dans le plan x,y.
   */
  public GrPoint pointPlusProcheXY(final GrPoint _p) {
    final double d = _p.soustraction(o_).projectionXY().division(vecteur().projectionXY());
    GrPoint r;
    if (d <= 0.) {
      r = new GrPoint(o_.x_, o_.y_, o_.z_);
    } else if (d >= 1.) {
      r = new GrPoint(e_.x_, e_.y_, e_.z_);
    } else {
      r = o_.addition(vecteur().multiplication(d));
    }
    r.z_ = 0.;
    return r;
  }

  public GrPoint projectionXY(final GrPoint _p) {
    final double d = _p.soustraction(o_).projectionXY().division(vecteur().projectionXY());

    if (d <= 0. || d >= 1.) {
      return null;
    } else {
      GrPoint r = o_.addition(vecteur().multiplication(d));
      r.z_ = 0.;
      return r;
    }
  }

  /**
   * Retourne le point appartenant a la droite passant par ce segment le plus proche du point donn� dans le plan x,y.
   *
   * @param _p Le point donn�.
   * @return Le point le plus proche sur la droite.
   */
  public GrPoint pointPlusProcheSurDroiteXY(final GrPoint _p) {
    final double d = _p.soustraction(o_).projectionXY().division(vecteur().projectionXY());
    GrPoint r;
    r = o_.addition(vecteur().multiplication(d));
    r.z_ = 0.;
    return r;
  }

  public GrPoint intersectionXY(final GrSegment _s) {
    if (boite().intersectionXY(_s.boite()) == null) {
      return null;
    }
    final GrVecteur txy = vecteur().projectionXY();
    final GrVecteur sxy = _s.vecteur().projectionXY();
    final GrVecteur n = txy.produitVectoriel(sxy);
    if ((_s.o_.soustraction(o_).projectionXY().produitTripleScalaire(txy, n)
            * _s.e_.soustraction(o_).projectionXY().produitTripleScalaire(txy, n) < 0)
            && (o_.soustraction(_s.o_).projectionXY().produitTripleScalaire(sxy, n)
            * e_.soustraction(_s.o_).projectionXY().produitTripleScalaire(sxy, n) < 0)) {
      final double t = o_.soustraction(_s.o_).projectionXY().produitTripleScalaire(txy, n)
              / sxy.produitTripleScalaire(txy, n);
      final GrPoint p = _s.o_.addition(sxy.multiplication(t));
      p.z_ = 0.;
      return p;
    }
    return null;
  }

  public boolean intersectXY(final GrSegment _s) {
    if (!boite().intersectXY(_s.boite())) {
      return false;
    }
    final GrVecteur txy = vecteur().projectionXY();
    final GrVecteur sxy = _s.vecteur().projectionXY();
    final GrVecteur n = txy.produitVectoriel(sxy);
    if ((_s.o_.soustraction(o_).projectionXY().produitTripleScalaire(txy, n)
            * _s.e_.soustraction(o_).projectionXY().produitTripleScalaire(txy, n) < 0)
            && (o_.soustraction(_s.o_).projectionXY().produitTripleScalaire(sxy, n)
            * e_.soustraction(_s.o_).projectionXY().produitTripleScalaire(sxy, n) < 0)) {
      return true;
    }
    return false;
  }

  /**
   * Retourne le segment le plus court joignant deux segments.
   */
  public GrSegment segmentPlusCourtXY(final GrSegment _s) {
    GrSegment r = null;
    final GrPoint i = intersectionXY(_s);
    if (i == null) {
      GrPoint p;
      GrSegment t;
      p = pointPlusProcheXY(_s.o_); // p E this
      r = new GrSegment(p, _s.o_);
      p = pointPlusProcheXY(_s.e_); // p E this
      t = new GrSegment(p, _s.e_);
      if (t.longueurXY() < r.longueurXY()) {
        r = t;
      }
      p = _s.pointPlusProcheXY(o_); // p E s
      t = new GrSegment(p, o_);
      if (t.longueurXY() < r.longueurXY()) {
        r = t;
      }
      p = _s.pointPlusProcheXY(e_); // p E s
      t = new GrSegment(p, e_);
      if (t.longueurXY() < r.longueurXY()) {
        r = t;
      }
    } else {
      r = new GrSegment(i, i);
    }
    return r;
  }

  public GrSegment applique(final GrMorphisme _t) {
    final GrSegment res = new GrSegment();
    res.o_ = o_.applique(_t);
    res.e_ = e_.applique(_t);
    return res;
  }

  @Override
  public void autoApplique(final GrMorphisme _t) {
    o_.autoApplique(_t);
    e_.autoApplique(_t);
  }

  /*
   * public GrPoint intersection(GrSegment s) { if(boite().intersection(s.boite())==null) return null; GrVecteur
   * n=vecteur().produitVectoriel(s.vecteur()); if( ( s.o.soustraction(o).produitTripleScalaire(vecteur(),n)
   * s.e.soustraction(o).produitTripleScalaire(vecteur(),n) <0) &&(
   * o.soustraction(s.o).produitTripleScalaire(s.vecteur(),n) e.soustraction(s.o).produitTripleScalaire(s.vecteur(),n)
   * <0)) { double t=o.soustraction(s.o).produitTripleScalaire(vecteur(),n)
   * /s.vecteur().produitTripleScalaire(vecteur(),n); return s.o.addition(s.vecteur().multiplication(t)); } else return
   * null; }
   */
  @Override
  public GrPoint[] contour() {
    return new GrPoint[]{o_, e_};
  }

  /**
   * Indique si l'objet est s�lectionn� pour un point donn�.
   * <p>
   * La m�thode a �t� impl�ment�e dans le but de satisfaire l'interface GrContour. Son fonctionnement n'est peut �tre pas exactement celui attendu.
   *
   * @author b.marchand Le 26/10/2000
   */
  @Override
  public boolean estSelectionne(final GrMorphisme _ecran, final double _dist, final Point _pt) {
    return estSelectionne(_ecran, _dist, new GrPoint(_pt));
  }

  public boolean estSelectionne(final GrMorphisme _ecran, final double _dist, final GrPoint _pt) {
    final GrSegment p = applique(_ecran);
    return p.distanceXY(_pt) < _dist;
  }

  public boolean estSelectionne(final double _dist, final GrPoint _pt) {
    return distanceXY(_pt) < _dist;
  }
}
