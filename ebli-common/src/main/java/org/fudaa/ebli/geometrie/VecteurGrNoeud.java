/*
 *  @creation     2002-06-25
 *  @modification $Date: 2006-09-19 14:55:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;

import org.fudaa.ebli.commun.VecteurGenerique;

/**
 * vecteur de GrNoeud. Cette classe peut �tre utilisee � la place de ListeGrNoeud. Moins performant pour les insertions
 * mais moins gourmand en m�moire : des noeuds de liaisons ne sont pas cr�es.
 *
 * @version $Id: VecteurGrNoeud.java,v 1.7 2006-09-19 14:55:51 deniger Exp $
 * @author Fred Deniger
 */
public final class VecteurGrNoeud extends VecteurGenerique {
  public VecteurGrNoeud() {
    super();
  }

  /**
   * Renvoie l'element d'index <code>_j</code>.
   *
   * @param _j l'index de l'objet a renvoyer.
   * @return l'objet d'index <code>_j</code>.
   */
  public GrNoeud renvoie(final int _j) {
    return (GrNoeud) internRenvoie(_j);
  }

  /**
   * @return le tableau correspondant
   */
  public GrNoeud[] tableau() {
    final GrNoeud[] r = new GrNoeud[nombre()];
    v_.toArray(r);
    return r;
  }

  /**
   * Vide le vecteur et l'initialise avec <code>_o</code>.
   *
   * @param _o le tableau des nouveaux elements.
   */
  public void tableau(final GrNoeud[] _o) {
    super.internTableau(_o);
  }

  public void ajoute(final GrNoeud _o) {
    super.ajoute(_o);
  }

  public void insere(final GrNoeud _o, final int _j) {
    super.insere(_o, _j);
  }

  public void remplace(final GrNoeud _o, final int _j) {
    super.remplace(_o, _j);
  }

  public void enleve(final GrNoeud _o) {
    super.enleve(_o);
  }

  public int indice(final GrNoeud _o) {
    return super.indice(_o);
  }

  public boolean contient(final GrNoeud _o) {
    return super.contient(_o);
  }
}
