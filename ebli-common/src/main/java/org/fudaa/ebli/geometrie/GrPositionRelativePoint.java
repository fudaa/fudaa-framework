/*
 * @creation     1999-05-07
 * @modification $Date: 2006-09-19 14:55:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;
import java.awt.Point;
/**
 * Une position relative par rapport a un GrPoint.
 *
 * @version      $Revision: 1.9 $ $Date: 2006-09-19 14:55:51 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public final class GrPositionRelativePoint
  extends GrObjet
  implements GrContour {
  public GrPoint origine_;
  public GrVecteur position_;
  public GrPositionRelativePoint() {
    origine_= new GrPoint();
    position_= new GrVecteur();
  }
  @Override
  public  String toString() {
    return "GrPositionRelativePoint(" + origine_ + ", " + position_ + ")";
  }
  public  double x() {
    return origine_.x_ + position_.x_;
  }
  public  double y() {
    return origine_.y_ + position_.y_;
  }
  public  double z() {
    return origine_.z_ + position_.z_;
  }
  public  GrPoint toGrPoint() {
    return new GrPoint(x(), y(), z());
  }
  public  GrPositionRelativePoint applique(final GrMorphisme _t) {
    final GrPositionRelativePoint r= new GrPositionRelativePoint();
    r.origine_= origine_.applique(_t);
    r.position_= position_.applique(_t);
    return r;
  }
  @Override
  public  void autoApplique(final GrMorphisme _t) {
    origine_.autoApplique(_t);
    position_.autoApplique(_t);
  }
  @Override
  public  GrPoint[] contour() {
    return new GrPoint[] { new GrPoint(x(), y(), z())};
  }
  /**
   * Indique si l'objet est s�lectionn� pour un point donn�.<p>
   *
   * La m�thode a �t� impl�ment�e dans le but de satisfaire l'interface
   * GrContour. Son fonctionnement n'est peut �tre pas exactement celui attendu.
   *
   * @author b.marchand Le 26/10/2000
   */
  @Override
  public  boolean estSelectionne(
    final GrMorphisme _ecran,
    final double _dist,
    final Point _pt) {
    final GrPoint p= new GrPoint(x(), y(), z());
    return p.estSelectionne(_ecran, _dist, _pt);
  }
}
