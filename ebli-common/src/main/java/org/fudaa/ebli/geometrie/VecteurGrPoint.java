/*
 *  @creation     2002-06-25
 *  @modification $Date: 2007-05-04 13:49:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;

import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ebli.commun.EbliSelectionMode;
import org.fudaa.ebli.commun.VecteurGenerique;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.LinearRing;

/**
 * vecteur de GrPoint. Cette classe peut �tre utilisee � la place de ListeGrPoint. Moins performant pour les insertions
 * mais moins gourmand en m�moire : des noeuds de liaisons ne sont pas cr�es.
 *
 * @author Fred Deniger
 * @version $Id: VecteurGrPoint.java,v 1.15 2007-05-04 13:49:47 deniger Exp $
 */
public final class VecteurGrPoint extends VecteurGenerique {
  /**
   * Initialise le vecteur.
   */
  public VecteurGrPoint() {
    super();
  }

  public VecteurGrPoint(final int _nb) {
    super(_nb);
  }

  public void setX(final double _x, final int _j) {
    final GrPoint pt = (GrPoint) super.internRenvoie(_j);
    pt.x_ = _x;
  }

  public void setY(final double y, final int _j) {
    final GrPoint pt = (GrPoint) super.internRenvoie(_j);
    pt.y_ = y;
  }

  public void setZ(final double z, final int _j) {
    final GrPoint pt = (GrPoint) super.internRenvoie(_j);
    pt.z_ = z;
  }

  @Override
  public void remplace(Object _o, int _j) {
    super.remplace(_o, _j);
  }

  @Override
  public void insere(Object _o, int _j) {
    super.insere(_o, _j);
  }

  @Override
  public void enleve(Object _o) {
    super.enleve(_o);
  }

  @Override
  public int indice(Object _o) {
    return super.indice(_o);
  }

  public Coordinate barycentre() {
    return barycentre(null);
  }

  public Coordinate barycentre(final Coordinate coordinate) {
    double x = 0.;
    double y = 0.;
    double z = 0.;
    final int n = nombre();
    final double qt = n;
    for (int i = 0; i < n; i++) {
      x += renvoieX(i);
      y += renvoieY(i);
      z += renvoieZ(i);
    }
    Coordinate res = coordinate;
    if (res == null) {
      res = new Coordinate();
    }
    res.x = x / qt;
    res.y = y / qt;
    return res;
  }

  public GrBoite boite(final GrBoite grBoite) {
    final int nb = nombre();
    if (nb <= 0) {
      grBoite.e_ = null;
      grBoite.o_ = null;
      return grBoite;
    }
    if (grBoite.e_ == null) {
      grBoite.e_ = new GrPoint();
    }
    copie(grBoite.e_, 0);
    if (grBoite.o_ == null) {
      grBoite.o_ = new GrPoint();
    }
    copie(grBoite.o_, 0);
    for (int i = nb - 1; i > 0; i--) {
      grBoite.ajuste(renvoie(i));
    }
    return grBoite;
  }

  public GrBoite boite() {
    return boite(new GrBoite());
  }

  /**
   * @param _ferme true si ferme -> derniere coordonnees =Premiere
   * @return la liste des coordonnees.
   */
  public Coordinate[] createCoordinateSequence(final boolean _ferme) {
    final int nb = nombre();
    final Coordinate[] c = new Coordinate[_ferme ? nb + 1 : nb];
    for (int i = 0; i < nb; i++) {
      final GrPoint temp = (GrPoint) internRenvoie(i);
      c[i] = new Coordinate(temp.x_, temp.y_, temp.z_);
    }
    if (_ferme) {
      c[nb] = c[0];
    }
    return c;
  }

  /**
   * Renvoie l'element d'index <code>_j</code>.
   *
   * @param _j l'index de l'objet a renvoyer.
   * @return l'objet d'index <code>_j</code>.
   */
  public GrPoint renvoie(final int _j) {
    return (GrPoint) v_.get(_j);
  }

  public double renvoieX(final int idx) {
    final GrPoint temp = (GrPoint) internRenvoie(idx);
    return temp.x_;
  }

  public double renvoieY(final int idx) {
    final GrPoint temp = (GrPoint) internRenvoie(idx);
    return temp.y_;
  }

  public double renvoieZ(final int idx) {
    final GrPoint temp = (GrPoint) internRenvoie(idx);
    return temp.z_;
  }

  /**
   * Copie le point <code>_i</code> dans <code>_t</code>.
   *
   * @return true si le point a ete copie et false sinon.
   */
  public boolean copie(final GrPoint grPoint, final int idx) {
    final GrPoint temp = (GrPoint) internRenvoie(idx);
    if (temp == null) {
      return false;
    }
    grPoint.setCoordonnees(temp.x_, temp.y_, temp.z_);
    return true;
  }

  /**
   * @return le tableau correspondant
   */
  public GrPoint[] tableau() {
    final GrPoint[] r = new GrPoint[nombre()];
    v_.toArray(r);
    return r;
  }

  public double[] tableauDouble() {
    final int nb = v_.size();
    final double[] r = new double[nb * 3];
    GrPoint p;
    for (int i = 0; i < nb; i++) {
      p = renvoie(i);
      r[3 * i] = p.x_;
      r[3 * i + 1] = p.y_;
      r[3 * i + 2] = p.z_;
    }
    return r;
  }

  public void ajoute(final double _x, final double _y, final double _z) {
    super.ajoute(new GrPoint(_x, _y, _z));
  }

  public void tableau(final GrPoint[] _o) {
    super.internTableau(_o);
  }

  /**
   * Renvoie true si <code>_p</code> est ENTIEREMENT dans le polygone de ref <code>_polyRef</code>.
   *
   * @param coordinate coordonnees temporaires
   * @param testeur le testeur
   * @param envelope l'enveloppe de _r
   * @param polyToTest la liste des point a tester
   * @return true si tous le points de _polyToTest sont compris dans le testeur
   */
  public static boolean estSelectionneEnv(final Coordinate coordinate, final Envelope envelope, final LinearRing linearRing,
                                          final PointOnGeometryLocator testeur, final VecteurGrPoint polyToTest, final int mode) {
    if (mode == EbliSelectionMode.MODE_ONE) {
      final int nb = polyToTest.nombre() - 1;
      final Coordinate c = coordinate;
      for (int i = nb; i >= 0; i--) {
        c.x = polyToTest.renvoieX(i);
        c.y = polyToTest.renvoieY(i);
        if (GISLib.isSelectedEnv(c, linearRing, envelope, testeur)) {
          return true;
        }
      }

      return false;
    } else if (mode == EbliSelectionMode.MODE_CENTER) {
      polyToTest.barycentre(coordinate);
      return GISLib.isSelectedEnv(coordinate, linearRing, envelope, testeur);
    }

    final int nb = polyToTest.nombre() - 1;
    final Coordinate c = coordinate;
    for (int i = nb; i >= 0; i--) {
      c.x = polyToTest.renvoieX(i);
      c.y = polyToTest.renvoieY(i);
      if (!GISLib.isSelectedEnv(c, linearRing, envelope, testeur)) {
        return false;
      }
    }
    return true;
  }
}
