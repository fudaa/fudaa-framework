/*
 * @creation     1999-09-23
 * @modification $Date: 2006-09-19 14:55:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;

import java.awt.Point;
import java.awt.Polygon;

/**
 * Un element.
 *
 * @version $Revision: 1.11 $ $Date: 2006-09-19 14:55:51 $ by $Author: deniger $
 * @author Bertrand Marchand
 */
public final class GrElement extends GrObjet implements GrContour {
  /**
   * Le type "Lin��que" � 2 noeuds.
   */
  public static final int L2 = 0;
  /**
   * Le type "Lin��que" � 3 noeuds.
   */
  public static final int L3 = 1;
  /**
   * Le type "Triangle" � 3 noeuds.
   */
  public static final int T3 = 2;
  /**
   * Le type "Triangle" � 6 noeuds.
   */
  public static final int T6 = 3;
  /**
   * Le type "Quadrilat�re" � 4 noeuds.
   */
  public static final int Q4 = 4;
  /**
   * Le type "Quadrilat�re" � 8 noeuds.
   */
  public static final int Q8 = 5;
  /**
   * Les noeuds de l'�l�ment.
   */
  public GrNoeud[] noeuds_;
  /**
   * Le type de l'�l�ment.
   */
  public int type_;
  /**
   * Les �l�ments support de l'�l�ment s'il est de type ar�te. Ces �l�ments sont au nombre de 0 si l'�l�ment n'est pas
   * une ar�te, de 1 si l'ar�te n'appartient qu'� un seul �lement, de 2 si elle est � la jonction de 2 �l�ments.
   */
  public GrElement[] elementsSupport_;

  /**
   * Un �l�ment � initialiser par la suite.
   */
  public GrElement() {
    this(new GrNoeud[0], -1);
  }

  /**
   * Un �l�ment de type ar�te appartenant � 1 seul �l�ment.
   */
  public GrElement(final GrNoeud[] _nds, final int _type, final GrElement _support) {
    this(_nds, _type, new GrElement[] { _support });
  }

  /**
   * Un �l�ment de type ar�te appartenant � 2 �l�ments. DEBUG : c'est dangereux de faire ainsi: les _nds et _supports
   * peuvent �tre modifies par un prog externe. A voir....
   *
   * @param _nds les noeuds
   * @param _type le type de l'element
   * @param _supports les elements support ?
   */
  public GrElement(final GrNoeud[] _nds, final int _type, final GrElement[] _supports) {
    type_ = _type;
    noeuds_ = _nds;
    elementsSupport_ = _supports;
  }

  /**
   * Un �l�ment.
   */
  public GrElement(final GrNoeud[] _nds, final int _type) {
    this(_nds, _type, new GrElement[0]);
  }

  public int nombre() {
    return noeuds_.length;
  }

  @Override
  public String toString() {
    return "GrElement(" + nombre() + " noeuds)";
  }

  /**
   * Impl�mentation GrObjet.
   */
  @Override
  public GrBoite boite() {
    final GrBoite r = new GrBoite();
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      r.ajuste(noeuds_[i].point_);
    }
    return r;
  }

  public GrPoint barycentre() {
    double x = 0., y = 0., z = 0.;
    final int n = nombre();
    final double qt = n;
    for (int i = 0; i < n; i++) {
      final GrPoint p = noeuds_[i].point_;
      x += p.x_;
      y += p.y_;
      z += p.z_;
    }
    return new GrPoint(x / qt, y / qt, z / qt);
  }

  public GrElement applique(final GrMorphisme _t) {
    final GrNoeud[] nds = new GrNoeud[noeuds_.length];
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      nds[i] = noeuds_[i].applique(_t);
    }
    return new GrElement(nds, type_, elementsSupport_);
  }

  @Override
  public void autoApplique(final GrMorphisme _t) {
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      noeuds_[i].autoApplique(_t);
    }
  }

  @Override
  public GrPoint[] contour() {
    final GrPoint[] r = new GrPoint[noeuds_.length];
    for (int i = 0; i < noeuds_.length; i++) {
      r[i] = noeuds_[i].point_;
    }
    return r;
  }

  /**
   * Renvoie les ar�tes de l'�l�ment sous forme d'un tableau d'�l�ments lin�iques.
   */
  public GrElement[] aretes() {
    GrElement[] r;
    switch (type_) {
    case L2:
      r = new GrElement[] { new GrElement(noeuds_, type_, this) };
      break;
    case L3:
      r = new GrElement[] { new GrElement(noeuds_, type_, this) };
      break;
    case T3:
      r = new GrElement[] { new GrElement(new GrNoeud[] { noeuds_[0], noeuds_[1] }, L2, this),
          new GrElement(new GrNoeud[] { noeuds_[1], noeuds_[2] }, L2, this),
          new GrElement(new GrNoeud[] { noeuds_[2], noeuds_[0] }, L2, this) };
      break;
    case T6:
      r = new GrElement[] { new GrElement(new GrNoeud[] { noeuds_[0], noeuds_[1], noeuds_[2] }, L3, this),
          new GrElement(new GrNoeud[] { noeuds_[2], noeuds_[3], noeuds_[4] }, L3, this),
          new GrElement(new GrNoeud[] { noeuds_[4], noeuds_[5], noeuds_[0] }, L3, this) };
      break;
    case Q4:
      r = new GrElement[] { new GrElement(new GrNoeud[] { noeuds_[0], noeuds_[1] }, L2, this),
          new GrElement(new GrNoeud[] { noeuds_[1], noeuds_[2] }, L2, this),
          new GrElement(new GrNoeud[] { noeuds_[2], noeuds_[3] }, L2, this),
          new GrElement(new GrNoeud[] { noeuds_[3], noeuds_[0] }, L2, this) };
      break;
    case Q8:
      r = new GrElement[] { new GrElement(new GrNoeud[] { noeuds_[0], noeuds_[1], noeuds_[2] }, L3, this),
          new GrElement(new GrNoeud[] { noeuds_[2], noeuds_[3], noeuds_[4] }, L3, this),
          new GrElement(new GrNoeud[] { noeuds_[4], noeuds_[5], noeuds_[6] }, L3, this),
          new GrElement(new GrNoeud[] { noeuds_[6], noeuds_[7], noeuds_[0] }, L3, this) };
      break;
    default:
      r = new GrElement[] {};
      break;
    }
    return r;
  }

  /**
   * Retourne les noeuds sommets de l'�l�ment.
   *
   * @return Les noeuds sommets de l'�l�ment
   */
  public GrNoeud[] noeudsSommets() {
    GrNoeud[] r;
    switch (type_) {
    case L2:
      r = new GrNoeud[] { noeuds_[0], noeuds_[1] };
      break;
    case L3:
      r = new GrNoeud[] { noeuds_[0], noeuds_[2] };
      break;
    case T3:
      r = new GrNoeud[] { noeuds_[0], noeuds_[1], noeuds_[2] };
      break;
    case T6:
      r = new GrNoeud[] { noeuds_[0], noeuds_[2], noeuds_[4] };
      break;
    case Q4:
      r = new GrNoeud[] { noeuds_[0], noeuds_[1], noeuds_[2], noeuds_[3] };
      break;
    case Q8:
      r = new GrNoeud[] { noeuds_[0], noeuds_[2], noeuds_[4], noeuds_[6] };
      break;
    default:
      r = new GrNoeud[] {};
      break;
    }
    return r;
  }

  /**
   * Retourne les noeuds milieux de l'�l�ment.
   *
   * @return Les noeuds milieux de l'�l�ment (tableau de longueur 0 si l'�l�ment est de type lin�aire)
   */
  public GrNoeud[] noeudsMilieux() {
    GrNoeud[] r;
    switch (type_) {
    case L3:
      r = new GrNoeud[] { noeuds_[1] };
      break;
    case T6:
      r = new GrNoeud[] { noeuds_[1], noeuds_[3], noeuds_[5] };
      break;
    case Q8:
      r = new GrNoeud[] { noeuds_[1], noeuds_[3], noeuds_[5], noeuds_[7] };
      break;
    default:
      r = new GrNoeud[] {};
      break;
    }
    return r;
  }

  /**
   * Cr�e un �l�ment lin�aire � partir d'un �l�ment quadratique par suppression des noeuds milieux.
   *
   * @return L'�l�ment lin�aire.
   */
  public GrElement versLineaire() {
    GrNoeud[] nds;
    int tp = type_;
    if (type_ == GrElement.L3) {
      tp = GrElement.L2;
    } else if (type_ == GrElement.T6) {
      tp = GrElement.T3;
    } else if (type_ == GrElement.Q8) {
      tp = GrElement.Q4;
    }
    nds = noeudsSommets();
    return new GrElement(nds, tp);
  }

  /**
   * Cr�e des �l�ments lin�aires � partir d'un �l�ment quadratique par d�coupage de l'�lement.
   *
   * @return L'�l�ment lin�aire.
   */
  public GrElement[] versLineaires() {
    int tp;
    GrElement[] r;
    switch (type_) {
    case L3:
      tp = GrElement.L2;
      r = new GrElement[2];
      r[0] = new GrElement(new GrNoeud[] { noeuds_[0], noeuds_[1] }, tp);
      r[1] = new GrElement(new GrNoeud[] { noeuds_[1], noeuds_[2] }, tp);
      break;
    case T6:
      tp = GrElement.T3;
      r = new GrElement[4];
      r[0] = new GrElement(new GrNoeud[] { noeuds_[5], noeuds_[0], noeuds_[1] }, tp);
      r[1] = new GrElement(new GrNoeud[] { noeuds_[1], noeuds_[2], noeuds_[3] }, tp);
      r[2] = new GrElement(new GrNoeud[] { noeuds_[3], noeuds_[4], noeuds_[5] }, tp);
      r[3] = new GrElement(new GrNoeud[] { noeuds_[5], noeuds_[1], noeuds_[3] }, tp);
      break;
    case Q8:
      tp = GrElement.T3;
      r = new GrElement[6];
      r[0] = new GrElement(new GrNoeud[] { noeuds_[7], noeuds_[0], noeuds_[1] }, tp);
      r[1] = new GrElement(new GrNoeud[] { noeuds_[1], noeuds_[2], noeuds_[3] }, tp);
      r[2] = new GrElement(new GrNoeud[] { noeuds_[3], noeuds_[4], noeuds_[5] }, tp);
      r[3] = new GrElement(new GrNoeud[] { noeuds_[5], noeuds_[6], noeuds_[7] }, tp);
      r[4] = new GrElement(new GrNoeud[] { noeuds_[7], noeuds_[1], noeuds_[3] }, tp);
      r[5] = new GrElement(new GrNoeud[] { noeuds_[3], noeuds_[5], noeuds_[7] }, tp);
      break;
    default:
      r = new GrElement[] { this };
      break;
    }
    return r;
  }

  /**
   * Orientation de l'�l�ment suivant le sens trigonom�trique. Les �l�ments lin�iques et les elements plats ne sont par
   * r�orient�s.
   *
   * @param _trigo <i>true</i> Orient� dans le sens trigo <i>false</i> Orient� dans le sens horaire
   */
  public void orienteTrigo(final boolean _trigo) {
    // Rien pour les �l�ments lin�iques
    if (type_ == -1 || type_ == L2 || type_ == L3) {
      return;
    }
    // Pour les autres, calcul du produit vectoriel depuis les noeuds sommets
    final GrNoeud[] nds = noeudsSommets();
    final GrVecteur v1 = new GrVecteur(nds[1].point_.x_ - nds[0].point_.x_, nds[1].point_.y_ - nds[0].point_.y_, 0);
    final GrVecteur v2 = new GrVecteur(nds[2].point_.x_ - nds[1].point_.x_, nds[2].point_.y_ - nds[1].point_.y_, 0);
    final double sens = v1.x_ * v2.y_ - v2.x_ * v1.y_;
    // Inversion de l'ordre des noeuds
    if (sens < 0 && _trigo || sens > 0 && !_trigo) {
      for (int i = 0; i < noeuds_.length / 2; i++) {
        final GrNoeud nd = noeuds_[i];
        noeuds_[i] = noeuds_[noeuds_.length - i - 1];
        noeuds_[noeuds_.length - i - 1] = nd;
      }
    }
  }

  public Polygon polygon() {
    final Polygon r = new Polygon();
    final int n = nombre();
    for (int i = 0; i < n; i++) {
      final GrPoint p = noeuds_[i].point_;
      r.addPoint((int) p.x_, (int) p.y_);
    }
    return r;
  }

  /**
   * Retourne le GrPolygone �quivalent ordonn� dans le m�me ordre que les noeuds de l'�l�ment.
   */
  public GrPolygone polygone() {
    final GrPolygone r = new GrPolygone();
    for (int i = 0; i < nombre(); i++) {
      r.sommets_.ajoute(noeuds_[i].point_);
    }
    return r;
  }

  /**
   * Indique si l'objet est s�lectionn� pour un point donn�.
   * <p>
   *
   * @author b.marchand Le 26/10/2000
   */
  @Override
  public boolean estSelectionne(final GrMorphisme _ecran, final double _dist, final Point _pt) {
    final GrElement e = applique(_ecran);
    final Polygon pl = e.polygon();
    return pl.contains(_pt);
  }
}
