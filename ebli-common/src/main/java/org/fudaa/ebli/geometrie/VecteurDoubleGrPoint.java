/*
 *  @creation     2002-06-25
 *  @modification $Date: 2006-09-19 14:55:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.geometrie;
import com.memoire.fu.FuVectordouble;
/**
 * vecteur de GrPoint : les GrPoint sont stockes sous la forme
 * d'un seul tableau de double. Pour modifier un point, il faut
 * explictement appeler une methode d'affectation.
 *
 * @version   $Id: VecteurDoubleGrPoint.java,v 1.8 2006-09-19 14:55:51 deniger Exp $
 * @author    Fred Deniger
 */
public final class VecteurDoubleGrPoint {
  private FuVectordouble vecteur_;
  /**
   * Initialise le vecteur pour 11 points.
   */
  public VecteurDoubleGrPoint() {
    this(11);
  }
  /**
   * Intialise le vecteur pour <code>_size</code> elements.
   */
  public VecteurDoubleGrPoint(final int _size) {
    vecteur_= new FuVectordouble(_size * 3);
  }
  /**
   * Ajoute le point <code>_x,_y,_z</code> a la fin du vecteur.
   */
  public void ajoute(final double _x, final double _y, final double _z) {
    vecteur_.addElement(_x);
    vecteur_.addElement(_y);
    vecteur_.addElement(_z);
  }
  /**
   * Ajoute les points de <code>_v</code> a la fin.
   */
  public void ajoute(final VecteurDoubleGrPoint _v) {
    ajoute(_v.vecteur_.data_);
  }
  public void setSize(final int _i) {
    vecteur_.setSize(3 * _i);
  }
  /**
   * Ajoute une serie de points qui sont sous la forme.
   * [x0,Y0,Z0,X1,......]
   */
  public void ajoute(final FuVectordouble _v) {
    ajoute(_v.data_);
  }
  /**
   * Ajoute une serie de points qui sont sous la forme.
   * [x0,Y0,Z0,X1,......]
   */
  public void ajoute(final double[] _pts) {
    if (_pts == null) {
      return;
    }
    final int nInit= vecteur_.size();
    final int nNew= _pts.length;
    final int nFinal= nInit + nNew;
    vecteur_.ensureCapacity(nFinal + 15);
    vecteur_.setSize(nFinal);
    System.arraycopy(_pts, 0, vecteur_.data_, nInit, nNew);
  }
  /**
   * Ajoute les coordonnees du point <code>_p</code>
   * au vecteur de double.
   */
  public void ajoute(final GrPoint _p) {
    ajoute(_p.x_, _p.y_, _p.z_);
  }
  /**
   * Affecte a <code>_p</code> une copie du point <code>_i</code>.
   */
  public boolean renvoie(final GrPoint _p, final int _i) {
    final int i= 3 * _i;
    if ((i < 0) || (i > vecteur_.size() - 3)) {
      return false;
    }
    _p.setCoordonnees(
      vecteur_.elementAt(i),
      vecteur_.elementAt(i + 1),
      vecteur_.elementAt(i + 2));
    return true;
  }
  public void ensureCapacity(final int _i) {
    vecteur_.ensureCapacity(_i * 3);
  }
  /**
   * Renvoie une copie du point <code>_i</code>.
   */
  public GrPoint renvoie(final int _i) {
    final GrPoint r= new GrPoint();
    renvoie(r, _i);
    return r;
  }
  public double renvoieX(final int _i) {
    return vecteur_.elementAt(3 * _i);
  }
  public double renvoieY(final int _i) {
    return vecteur_.elementAt(3 * _i + 1);
  }
  public double renvoieZ(final int _i) {
    return vecteur_.elementAt(3 * _i + 2);
  }
  public boolean enleve(final int _i) {
    final int i= 3 * _i;
    if ((i < 0) || (i > vecteur_.size() - 3)) {
      return false;
    }
    vecteur_.removeElementAt(i);
    vecteur_.removeElementAt(i);
    vecteur_.removeElementAt(i);
    return true;
  }
  public void insere(final GrPoint _p, final int _i) {
    insere(_p.x_, _p.y_, _p.z_, _i);
  }
  public boolean insere(final double _x, final double _y, final double _z, final int _i) {
    final int i= 3 * _i;
    if ((i < 0) || (i > vecteur_.size() - 3)) {
      return false;
    }
    vecteur_.insertElementAt(_z, i);
    vecteur_.insertElementAt(_y, i);
    vecteur_.insertElementAt(_x, i);
    return true;
  }
  public void remplace(final GrPoint _p, final int _i) {
    remplace(_p.x_, _p.y_, _p.z_, _i);
  }
  public boolean remplace(final double _x, final double _y, final double _z, final int _i) {
    final int i= 3 * _i;
    if ((i < 0) || (i > vecteur_.size() - 3)) {
      return false;
    }
    vecteur_.setElementAt(_x, i);
    vecteur_.setElementAt(_y, i + 1);
    vecteur_.setElementAt(_z, i + 2);
    return true;
  }
  public int nombre() {
    return (vecteur_.size() / 3);
  }
  public void capacite(final int _i) {
    vecteur_.ensureCapacity(_i * 3);
  }
  public void vide() {
    vecteur_.removeAllElements();
  }
  public boolean contient(final double _x, final double _y, final double _z) {
    final int index= 0;
    final int indexF= vecteur_.size() - 2;
    for (int i= index; i < indexF; i += 3) {
      if ((_x == vecteur_.elementAt(i))
        && (_y == vecteur_.elementAt(i + 1))
        && (_z == vecteur_.elementAt(i + 2))) {
        return true;
      }
    }
    return false;
  }
}
