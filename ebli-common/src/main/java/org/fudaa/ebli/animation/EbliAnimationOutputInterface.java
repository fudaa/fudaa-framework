/**
 * @creation 15 nov. 2004
 * @modification $Date: 2007-01-26 14:58:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.animation;

import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.File;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.image.CtuluImageProducer;

/**
 * @author Fred Deniger
 * @version $Id: EbliAnimationOutputInterface.java,v 1.7 2007-01-26 14:58:09 deniger Exp $
 */
public interface EbliAnimationOutputInterface {

  /**
   * @param _p le producteur d'image (utiliser pour connaitre la taille des images).
   * @param _parent le parent pour afficher des messages
   * @param _nbImg le nombre d'images attendues
   * @return true si initialise
   */
  boolean init(CtuluImageProducer _p, Component _parent, int _nbImg);

  void setWarnIfFileExists(boolean warnIfFileExists);

  EbliAnimationOutputInterface copy();

  /**
   * @return le nom de la sortie
   */
  String getName();

  /**
   * @return le nom court de la sortie
   */
  String getShortName();

  /**
   * @return true si active
   */
  boolean isActivated();

  /**
   * @param _new la nouvelle valeur d'activite
   */
  void setActivated(boolean _new);

  /**
   * @return null si valide. message d'erreur sinon
   */
  String isValid(boolean _testDim);

  /**
   * @param _im l'image a ajouter
   * @param _last
   */
  void appendFrame(BufferedImage _im, boolean _last);

  /**
   * Ajouter a la fin.
   */
  void finish();

  /**
   * @param _nbFrame le nombre de pas de temps
   * @return le panneau de config.
   */
  CtuluDialogPanel getConfigurePanel(int _nbFrame);

  void setDelay(float _secPerImage);

  float getDelay();

  String getHtmlDesc();

  void setFile(File _dest);

  File getDestFile();

  String getExt();

  void setWidth(int _w);

  void setHeight(int _h);
}