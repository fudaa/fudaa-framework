/*
 * @creation 16 mai 2006
 * @modification $Date: 2007-03-23 17:25:14 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.animation;

import com.memoire.bu.BuProgressBar;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;

class EbliAnimationTask extends Observable implements Runnable {

  BuProgressBar bar_;

  private int firstIdx_ = -1;

  private int inc_ = 1;

  private int lastIdx_;

  EbliAnimationTask() {}

  protected void setInc(final int _i) {
    if (_i == 0) {
      inc_ = 1;
    } else if (_i < 0) {
      inc_ = -_i;
    } else {
      inc_ = _i;
    }
  }

  public int getFirstTimeStep() {
    return firstIdx_ < 0 ? 0 : firstIdx_;
  }

  /**
   * @return l'incr�ment utilise
   */
  public int getInc() {
    return inc_;
  }

  public int getLastTimeStep() {
    return firstIdx_ < 0 ? (src_ == null ? 0 : src_.getNbTimeStep() - 1) : lastIdx_;
  }

  /**
   * @return le nombre d'images qui seront affich�es lors de l'animation
   */
  public int getNbDisplayedAnimStep() {
    computeNbStep();
    return (int) Math.ceil((double) nbStep_ / (double) inc_);
  }

  /**
   * @return le nomnbre total de pas de temps contenus dans la source
   */
  public int getNbTotalAnimStep() {
    if (src_ == null || (src_.getNbTimeStep() == 0)) {
      return 0;
    }
    return src_.getNbTimeStep();
  }

  int currentIdx_;
  int realLastIdx_;
  int nbStep_;
  int width_;
  int height_;

  EbliAnimationOutputInterface[] outs_;
  EbliAnimationOutputInterface[] outsActivated_;

  public String init(final int _width, final int _height) {
    src_.setVideoMode(true);
    width_ = _width;
    height_ = _height;
    computeNbStep();
    outsActivated_ = getSelectedOuptut(_width, _height);
    saveOut_ = !CtuluLibArray.isEmpty(outsActivated_);
    if (saveOut_) {
      for (int i = outsActivated_.length - 1; i >= 0; i--) {
        final String err = outsActivated_[i].isValid(true);
        if (err != null) {
          return err;
        }
      }

    }
    isFinished_ = false;
    return null;
  }

  private void computeNbStep() {
    currentIdx_ = firstIdx_;
    realLastIdx_ = lastIdx_;
    if (currentIdx_ < 0) {
      currentIdx_ = 0;
      realLastIdx_ = getNbTotalAnimStep() - 1;
    }
    if (currentIdx_ > realLastIdx_) {
      final int tmp = currentIdx_;
      currentIdx_ = realLastIdx_;
      realLastIdx_ = tmp;
    }
    nbStep_ = realLastIdx_ - currentIdx_ + 1;
  }

  EbliAnimationSourceInterface src_;

  boolean isFinished_;

  boolean saveOut_;

  public boolean doAnim() {
    if (isFinished_) {
      throw new IllegalAccessError("must be stopped");
    }
    if (currentIdx_ > realLastIdx_) {
      finished();
      return false;
    }

    src_.setTimeStep(currentIdx_);
    if (saveOut_) {
      final BufferedImage image = src_.produceImage(width_, height_, null);
      if (image == null) {
        src_.produceImage(null);
      }
      for (int j = outsActivated_.length - 1; j >= 0; j--) {
        outsActivated_[j].appendFrame(image, currentIdx_ == realLastIdx_);
      }
      if (image != null) {
        image.flush();
      }
    }
    bar_.setValue(100 - ((int) (((double) (realLastIdx_ - currentIdx_)) / ((double) nbStep_) * 100)));
    bar_.setString(CtuluLib.getS("Pas de temps:") + CtuluLibString.ESPACE + src_.getTimeStep(currentIdx_));
    currentIdx_ += inc_;
    return currentIdx_ <= realLastIdx_;

  }

  public void finished() {
    if (saveOut_) {
      bar_.setValue(20);
      for (int j = outsActivated_.length - 1; j >= 0; j--) {
        outsActivated_[j].finish();
      }
      bar_.setValue(80);
    }
    setChanged();
    outsActivated_ = null;
    bar_.setValue(0);
    isFinished_ = true;
    notifyObservers();
  }

  /**
   * @param _outs
   */
  void setAvailableOuts(final EbliAnimationOutputInterface[] _outs) {
    outs_ = _outs;

  }

  protected EbliAnimationOutputInterface[] getSelectedOuptut(final int _width, final int _height) {
    if (outs_ != null && outs_.length > 0) {
      // updateCompDim();
      final int nbProducedImg = getNbDisplayedAnimStep();
      final List l = new ArrayList(outs_.length);
      for (int i = 0; i < outs_.length; i++) {
        final EbliAnimationOutputInterface outi = outs_[i];
        if (outi.isActivated() && outi.isValid(false) == null) {
          outi.setHeight(_height);
          outi.setWidth(_width);
          if (outi.init(getSrc(), src_.getComponent(), nbProducedImg)) l.add(outi);
        }
      }
      return (EbliAnimationOutputInterface[]) l.toArray(new EbliAnimationOutputInterface[l.size()]);
    }
    return null;
  }

  protected boolean isDimensionBad() {
    final Dimension d = src_.getDefaultImageDimension();
    final int w = d.width;
    int r = w % 8;
    if (r != 0) {
      return true;
    }
    final int h = d.height;
    r = h % 8;
    return r != 0;
  }

  EbliAnimationSourceInterface getSrc() {
    return src_;
  }

  void setSrc(final EbliAnimationSourceInterface _src) {
    src_ = _src;
  }

  BuProgressBar getBar() {
    return bar_;
  }

  void setBar(final BuProgressBar _bar) {
    bar_ = _bar;
  }

  int getFirstIdx() {
    return firstIdx_;
  }

  void setFirstIdx(final int _firstIdx) {
    firstIdx_ = _firstIdx;
  }

  int getLastIdx() {
    return lastIdx_;
  }

  void setLastIdx(final int _lastIdx) {
    lastIdx_ = _lastIdx;
  }

  boolean isFinished() {
    return isFinished_;
  }

  @Override
  public void run() {
    doAnim();
  }
}
