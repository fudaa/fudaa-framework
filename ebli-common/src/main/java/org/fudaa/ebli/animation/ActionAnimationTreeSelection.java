/**
 *  @creation     12 nov. 2004
 *  @modification $Date: 2007-02-07 09:55:50 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.animation;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.awt.Window;
import java.util.ArrayList;
import java.util.List;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

/**
 * @author Fred Deniger
 * @version $Id: ActionAnimationTreeSelection.java,v 1.6 2007-02-07 09:55:50 deniger Exp $
 */
@SuppressWarnings("serial")
public class ActionAnimationTreeSelection extends EbliAnimationAction implements TreeSelectionListener {
  
  

  /**
   * @param _a
   * @param _f
   */
  public ActionAnimationTreeSelection(final EbliAnimationSourceAbstract _a, final Window _f) {
    super(_a, null,_f);
  }


  public ActionAnimationTreeSelection(final EbliAnimationSourceAbstract _a) {
    super(_a);
  }

  public ActionAnimationTreeSelection(final EbliAnimationSourceAbstract _a, final EbliAnimation _f) {
    super(_a, _f);
  }

  @Override
  public void valueChanged(final TreeSelectionEvent _e) {
    final TreeSelectionModel model = (TreeSelectionModel) _e.getSource();

    updateFromModel(model);
  }

  public void updateFromModel(final TreeSelectionModel _model) {
    if (_model.isSelectionEmpty()) {
      setAnimAdapter(null);
      return;
    }

    if (_model.getSelectionCount() == 1) {
      setAnimAdapter(_model.getSelectionPath().getLastPathComponent());
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("EAC: selected " + _model.getSelectionPath().getClass().getName());
      }
    } else {
      final TreePath[] p = _model.getSelectionPaths();
      final List anim = new ArrayList();
      for (int i = p.length - 1; i >= 0; i--) {
        final Object o = p[i].getLastPathComponent();
        if (o instanceof EbliAnimationAdapterInterface) {
          anim.add(o);
        }
      }
      if (anim.size() == 0) {
        setAnimAdapter(null);
      } else if (anim.size() == 1) {
        setAnimAdapterInterface((EbliAnimationAdapterInterface) anim.get(0));
      } else {
        setAnimAdapterInterface(new EbliAnimationComposite((EbliAnimationAdapterInterface[]) anim
            .toArray(new EbliAnimationAdapterInterface[anim.size()])));
      }
    }
  }
}
