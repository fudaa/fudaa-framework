/**
 * @creation 15 nov. 2004
 * @modification $Date: 2007-03-23 17:25:14 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.animation;

import com.memoire.fu.FuLog;
import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.IOException;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ctulu.video.CtuluAviWriter;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author Fred Deniger
 * @version $Id: EbliAnimationOutputAvi.java,v 1.16 2007-03-23 17:25:14 deniger Exp $
 */
public class EbliAnimationOutputAvi extends EbliAnimationOutputAbstract {

  int coefFrameRate_;

  CtuluAviWriter writer_;

  public EbliAnimationOutputAvi() {
    super(".avi");
  }

  public EbliAnimationOutputAvi(EbliAnimationOutputAvi from) {
    super(from);
    if(from==null){
      return;
    }
    this.coefFrameRate_=from.coefFrameRate_;
  }

  @Override
  public EbliAnimationOutputInterface copy() {
    return new EbliAnimationOutputAvi(this);
  }

  @Override
  public void appendFrame(final BufferedImage _im, final boolean _last) {
    final BufferedImage im = CtuluLibImage.resize(_im, width_, height_);
    try {
      for (int i = 0; i < coefFrameRate_; i++) {
        writer_.writeRGBFrame(im);
      }
    } catch (final IOException _e) {
      FuLog.warning(_e);
    }

  }

  @Override
  public void finish() {
    try {
      if (!writer_.writeEnd()) {
        CtuluLibMessage.info(EbliLib.getS("Le fichier avi n'a pas �t� �crit"));
      }
    } catch (final IOException _e) {
      FuLog.warning(_e);
    } finally {
      try {
        writer_.close();
      } catch (final IOException _e1) {
        FuLog.warning(_e1);
      }
    }
    warnIfFileExists_ = true;
  }

  @Override
  public String getName() {
    return "video avi";
  }

  @Override
  public boolean init(final CtuluImageProducer _p, final Component _parent, final int _nbImg) {
    if (isActivated()) {
      if (!canOverwrittenFile(_parent)) {
        return false;
      }
      coefFrameRate_ = getNbRepeatForAvi();
      destFile_.delete();
      writer_ = new CtuluAviWriter(destFile_, _nbImg * coefFrameRate_, width_, height_, getGoodFPSForAvi());
      try {
        writer_.writeImageBegin();
      } catch (final IOException _e) {
        CtuluLibDialog.showError(_parent, CtuluResource.CTULU.getString("erreur"), _e.getLocalizedMessage());
        try {
          writer_.close();
        } catch (final IOException _e1) {
          FuLog.warning(_e1);
        }
      }
    }
    return true;
  }

}
