/*
 * @creation 25 janv. 07
 * @modification $Date: 2007-06-28 09:26:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.animation;

import java.awt.Component;
import java.io.File;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author fred deniger
 * @version $Id: EbliAnimationOutputAbstract.java,v 1.3 2007-06-28 09:26:48 deniger Exp $
 */
public abstract class EbliAnimationOutputAbstract implements EbliAnimationOutputInterface {

  public static String getHtmlDes(final EbliAnimationOutputInterface _out) {
    final CtuluHtmlWriter res = new CtuluHtmlWriter(true);
    if (_out.getDestFile() != null) {
      res.addTagAndText("b", CtuluLib.getS("Fichier:")).append(CtuluLibString.ESPACE).append(
              _out.getDestFile().getAbsolutePath()).append("<br>");
    }
    res.addTagAndText("b", EbliLib.getS("Dur�e d'affichage d'une image:")).append(CtuluLibString.ESPACE).append(
            Float.toString(_out.getDelay()));
    return res.getHtml();
  }
  private boolean isActived_;
  final String ext_;
  boolean warnIfFileExists_ = true;
  protected float delay_ = 0.5F;
  protected File destFile_;
  protected int height_;
  protected int width_;

  public EbliAnimationOutputAbstract(final String _ext) {
    super();
    ext_ = _ext;
  }
  public EbliAnimationOutputAbstract(final EbliAnimationOutputAbstract from) {
    super();
    this.ext_=from==null?null:from.ext_;
    if(from==null){
      return;
    }
    this.isActived_=from.isActived_;
    this.warnIfFileExists_=from.warnIfFileExists_;
    this.delay_=from.delay_;
    this.height_=from.height_;
    this.width_=from.width_;
    this.destFile_=from.destFile_;
  }



  @Override
  public void setWarnIfFileExists(boolean warnIfFileExists) {
    this.warnIfFileExists_ = warnIfFileExists;
  }

  protected boolean canOverwrittenFile(final Component _parent) {
    if (warnIfFileExists_) {
      if (destFile_.exists()) {
        String yesText = EbliLib.getS("Enregistrer et ecraser");
        String noText = EbliLib.getS("Ne pas Enregistrer");
        if (!CtuluLibDialog.showConfirmation(_parent, EbliLib.getS("Enregistrement animation"), EbliLib.getS(
                "Le fichier de sortie {0} existe. Il sera ecras� � la fin de l'animation.", destFile_.getName())
                + "\n" + EbliLib.getS("Voulez-vous sauvegarder l'animation ?"), yesText, noText)) {
          return false;
        }
      }
    }
    return true;
  }

  public CtuluDialogPanel createConfigurePanel(final int _nbFrame) {
    return new EbliAnimationVideoEditPanel(_nbFrame, this);
  }

  /**
   * @return le fps (nombre d'image par seconde) demand? par l'utilisateur
   */
  public int getAskedFps() {
    return (int) Math.floor(1F / delay_);
  }

  @Override
  public final CtuluDialogPanel getConfigurePanel(final int _nbFrame) {
    final CtuluDialogPanel res = createConfigurePanel(_nbFrame);
    warnIfFileExists_ = false;
    return res;
  }

  @Override
  public float getDelay() {
    return delay_;
  }

  @Override
  public File getDestFile() {
    return destFile_;
  }

  @Override
  public String getExt() {
    return ext_;
  }

  public int getGoodFps() {
    final int fps = getAskedFps();
    return fps < 1 ? 1 : fps;
  }

  /**
   * @return le fps a utiliser pour avi (>=20)
   */
  public int getGoodFPSForAvi() {
    final int realFPS = getAskedFps();
    return realFPS < 20 ? 20 : realFPS;
  }

  @Override
  public String getHtmlDesc() {
    return getHtmlDes(this);
  }

  /**
   * Si l'utilisateur veut qu'une image soit affichee plus longtemps, elle doit etre ajoutee plusieurs fois au flux (sauf pour la sortie gif).
   *
   * @return le nombre de fois que l'on doit reajouter une image pour avoir le bon delai.
   */
  public int getNbRepeat() {
    final int fps = getAskedFps();
    if (fps < 1) {
      return (int) Math.ceil(1 / fps);
    }
    return 1;
  }

  /**
   * @return le nombre de fois que l'on doit ajouter une frame au flux de sortie afin d'avoir un bon fps pour une sortie avi.
   */
  public int getNbRepeatForAvi() {
    final int fps = getAskedFps();
    final int goodfps = getGoodFPSForAvi();
    if (fps >= goodfps) {
      return 1;
    }
    return (int) Math.ceil(goodfps / fps);
  }

  @Override
  public String getShortName() {
    return ext_.substring(1);
  }

  @Override
  public boolean isActivated() {
    return isActived_;
  }

  @Override
  public String isValid(final boolean _testDim) {

    if (destFile_ == null) {
      return EbliLib.getS("Le fichier de sortie n'est pas pr�cis�");
    }
    if (destFile_.isDirectory()) {
      return EbliLib.getS("Le fichier de sortie est un r�pertoire");
    }
    if (_testDim) {
      if (width_ <= 0) {
        return CtuluLib.getS("La largeur de l'image est n�gative ou nulle");
      }
      if (height_ <= 0) {
        return CtuluLib.getS("La hauteur de l'image est n�gative ou nulle");
      }
    }
    return CtuluLibFile.canWrite(destFile_,false);
  }

  @Override
  public void setActivated(final boolean _new) {
    isActived_ = _new;
  }

  @Override
  public void setDelay(final float _delay) {
    final float d = 1F / 25F;
    delay_ = _delay < d ? d : _delay;
  }

  @Override
  public final void setFile(final File _f) {
    if (destFile_ == _f) {
      return;
    } else if (_f != null && !_f.isDirectory()) {
      destFile_ = _f;
      if (!destFile_.getAbsolutePath().endsWith(ext_)) {
        final File pare = destFile_.getParentFile();
        final String name = CtuluLibFile.getSansExtension(destFile_.getName());
        destFile_ = new File(pare, name + ext_);
      }
    } else {
      destFile_ = null;
    }
  }

  @Override
  public void setHeight(final int _h) {
    if (_h > 0) {
      height_ = _h;
    }

  }

  @Override
  public void setWidth(final int _w) {
    if (_w > 0) {
      width_ = _w;
    }

  }
}
