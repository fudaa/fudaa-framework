/*
 * @creation 25 janv. 07
 * @modification $Date: 2007-05-04 13:49:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.animation;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;
import com.memoire.fu.FuLib;
import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.video.CtuluMencoderFinder;
import org.fudaa.ebli.commun.EbliLib;

/**
 * Un panneau permettant d'�diter le fichier de sortie et la duree de la vid�o (le frame rate...).
 * 
 * @author fred deniger
 * @version $Id: EbliAnimationVideoEditPanel.java,v 1.3 2007-05-04 13:49:44 deniger Exp $
 */
public class EbliAnimationVideoEditPanel extends CtuluDialogPanel implements DocumentListener {

  BuTextField ftDuree_;
  JTextField ftFile_;
  BuTextField ftDelay_;
  boolean isUpdating_;
  int nbFrame_;
  File destFile_;
  final NumberFormat fmtDelay_ = new DecimalFormat();
  final EbliAnimationOutputInterface dest_;
  /**
   * Temps en sec pour chaque image.
   */
  float delay_;
  BuValueValidator vDuree_;
  BuValueValidator vRate_;

  public static class Mencoder extends EbliAnimationVideoEditPanel {

    JTextField tfMencoder_;

    public Mencoder(final int _nbFrame, final EbliAnimationOutputInterface _dest) {
      super(_nbFrame, _dest);
      final String initPath = new CtuluMencoderFinder().getMencoderInPathOrDistrib();
      if (initPath == null) {
        tfMencoder_ = addLabelFileChooserPanel(CtuluLib.getS("l'ex�cutable 'mencoder'"), null, false, false);
        tfMencoder_.setToolTipText(CtuluLib.getS("Renseigner le chemin vers 'mencoder'"));
        tfMencoder_.setText(CtuluMencoderFinder.getMencoderInPref());
        final CtuluHtmlWriter writer = new CtuluHtmlWriter(true, true);
        writer.setDefaultResource(CtuluResource.CTULU);
        if (FuLib.isLinux()) {
          writer.addi18n("'mencoder' peut �tre install� � partir de la distribution de Linux").append("<br>");
        }
        final String web = "http://www.mplayerhq.hu/design7/dload.html";
        writer.append(
            CtuluLib.getS("Vous pouvez t�l�charger 'mplayer' et 'mencoder' depuis {0}", "<a href=\"" + web + "\">"
                + web + "</a>")).append("<br>");
        writer.addTag("b");
        writer.addi18n("Installer 'mencoder' et r�afficher cette boite de dialogue pour v�rifier");
        setHelpText(writer.getHtml());
      }
    }

    @Override
    public boolean isDataValid() {
      if (tfMencoder_ != null) {
        final String absolutePath = new File(tfMencoder_.getText()).getAbsolutePath();
        final boolean valide = CtuluLibFile.exists(new File(tfMencoder_.getText()))
            && CtuluMencoderFinder.isMencoderOk(absolutePath);
        if (!valide) {
          setErrorText(CtuluLib.getS("Renseigner le chemin vers 'mencoder'"));
          return false;
        }
        CtuluMencoderFinder.setMencoderInPref(absolutePath);
      }
      return super.isDataValid();
    }
  }

  /**
   * @param _nbFrame le nombre de pas
   */
  public EbliAnimationVideoEditPanel(final int _nbFrame, final EbliAnimationOutputInterface _dest) {
    super(true);
    dest_ = _dest;
    nbFrame_ = _nbFrame;
    delay_ = _dest.getDelay();
    setLayout(new BuGridLayout(2, 5, 5));
    destFile_ = _dest.getDestFile();
    ftFile_ = addLabelFileChooserPanel(EbliLib.getS("Fichier de sortie"), destFile_, false, true);
    ftDelay_ = addLabelDoubleText(EbliLib.getS("Dur�e d'affichage d'une image") + " (sec)");
    fmtDelay_.setMaximumFractionDigits(2);
    ftDelay_.setText(fmtDelay_.format(delay_));

    ftDuree_ = addLabelIntegerText(EbliLib.getS("Dur�e animation") + " (sec)");
    vDuree_ = new CtuluValueValidator.DoubleMin(1D);
    ftDuree_.setValueValidator(vDuree_);

    ftDuree_.setText(CtuluLibString.getString((int) Math.ceil(delay_ * nbFrame_)));
    
    vRate_ = new CtuluValueValidator.DoubleMin(1D / 25D);
    ftDelay_.setValueValidator(vRate_);
    ftDelay_.getDocument().addDocumentListener(this);
    ftDuree_.getDocument().addDocumentListener(this);
  }

  protected void updateRate(final Document _src) {
    if (isUpdating_) {
      return;
    }
    isUpdating_ = true;
    // on met a jour le framerate
    if (_src == ftDuree_.getDocument()) {
      final Integer i = (Integer) ftDuree_.getValue();
      if (i != null) {
        final String txt = ftDelay_.getText().trim();
        final String newTxt = fmtDelay_.format(CtuluLibImage.getRatio(i.intValue(), nbFrame_));
        if (!txt.equals(newTxt)) {
          ftDelay_.setText(newTxt);
        }
      }
    } else if (_src == ftDelay_.getDocument()) {
      final Double i = (Double) ftDelay_.getValue();
      if (i != null) {
        final String txt = ftDuree_.getText().trim();
        final String newTxt = Integer.toString((int) Math.ceil(nbFrame_ * i.doubleValue())).trim();
        if (!txt.equals(newTxt)) {
          ftDuree_.setText(newTxt);
        }
      }
    }
    isUpdating_ = false;
  }

  @Override
  public void changedUpdate(final DocumentEvent _e) {
    updateRate(_e.getDocument());
  }

  @Override
  public boolean apply() {
    dest_.setFile(EbliAnimationVideoEditPanel.getDestFile(new File(ftFile_.getText().trim()), dest_.getExt()));
    dest_.setDelay(((Double) ftDelay_.getValue()).floatValue());
    return true;
  }

  @Override
  public void insertUpdate(final DocumentEvent _e) {
    updateRate(_e.getDocument());
  }

  @Override
  public void removeUpdate(final DocumentEvent _e) {
    updateRate(_e.getDocument());
  }

  @Override
  public boolean isDataValid() {
    super.cancelErrorText();
    boolean r = vDuree_.isValueValid(ftDuree_.getValue());
    if (r) {
      r = vRate_.isValueValid(ftDelay_.getValue());
    }
    if (r) {
      final String txt = ftFile_.getText().trim();
      if (txt.length() == 0) {
        setErrorText(EbliLib.getS("Le fichier de sortie n'est pas pr�cis�"));
        return false;
      }
      final File f = new File(txt);
      if (f.isDirectory()) {
        setErrorText(EbliLib.getS("Le fichier de sortie est un r�pertoire"));
        return false;
      }
      final File avi = EbliAnimationVideoEditPanel.getDestFile(f, dest_.getExt());
      final String err = CtuluLibFile.canWrite(avi);
      if (err != null) {
        setErrorText(err);
        return false;
      }
      if (avi.exists() && !CtuluLibDialog.confirmeOverwriteFile(this, avi.getName())) {
        return false;
      }
    }
    return r;
  }

  protected static File getDestFile(final File _f, final String _ext) {
    if (_f == null) {
      return null;
    }
    final String path = _f.getAbsolutePath();
    if (path.endsWith(_ext)) {
      return _f;
    }
    final File pare = _f.getParentFile();
    final String name = CtuluLibFile.getSansExtension(_f.getName());
    return new File(pare, name + _ext);
  }
}
