/**
 *  @creation     12 nov. 2004
 *  @modification $Date: 2006-09-19 14:55:52 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.animation;

/**
 * Cette interface d�finit la s�quence des images de l'animation par leur nombre, le nom des images,
 * les intervalles de temps entre chaque image.
 * Elle est a impl�menter et � affecter � la source de l'animation.
 * 
 * @author Fred Deniger
 * @version $Id: EbliAnimationAdapterInterface.java,v 1.5 2006-09-19 14:55:52 deniger Exp $
 */
public interface EbliAnimationAdapterInterface {

  /**
   * @return le nombre de pas de temps de la s�quence
   */
  int getNbTimeStep();

  /**
   * @param _idx indice du pas de temps [0;getNbTimeStep()[
   * @return L'intitul� du pas de temps visualis� dans le composant d'animation.
   */
  String getTimeStep(int _idx);

  /**
   * @param _idx indice du pas de temps [0;getNbTimeStep()[
   * @return la valeur en sec de ce pas de temps
   */
  double getTimeStepValueSec(int _idx);

  /**
   * @param _idx L'image que doit afficher la source pour l'indice donn�.
   */
  void setTimeStep(int _idx);

  /**
   * @return le titre de la s�quence
   */
  String getTitle();

}
