/**
 * @creation 12 nov. 2004
 * @modification $Date: 2007-01-26 14:58:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.animation;

import java.awt.Component;
import org.fudaa.ctulu.image.CtuluImageProducer;

/**
 * @author Fred Deniger
 * @version $Id: EbliAnimationSourceInterface.java,v 1.9 2007-01-26 14:58:09 deniger Exp $
 */
public interface EbliAnimationSourceInterface extends CtuluImageProducer, EbliAnimationAdapterInterface {

  /**
   * Permet d'avertir le client que la video est en cours.
   * 
   * @param _b true si video en cours
   */
  void setVideoMode(boolean _b);

  /**
   * @return le composant contenant l'affichage (optionnel)
   */
  Component getComponent();


}