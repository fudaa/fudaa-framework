/**
 * @creation 12 nov. 2004
 * @modification $Date: 2006-09-19 14:55:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.animation;

import org.fudaa.ctulu.CtuluLibString;

/**
 * @author Fred Deniger
 * @version $Id: EbliAnimationSourceAbstract.java,v 1.4 2006-09-19 14:55:52 deniger Exp $
 */
public abstract class EbliAnimationSourceAbstract implements EbliAnimationSourceInterface {

  EbliAnimationAdapterInterface adapter_;

  public final EbliAnimationAdapterInterface getAdapter() {
    return adapter_;
  }

  @Override
  public String getTitle() {
    return adapter_ == null ? CtuluLibString.EMPTY_STRING : adapter_.getTitle();
  }

  /**
   * @param _adapter le nouvel adapteur
   */
  public final void setAdapter(final EbliAnimationAdapterInterface _adapter) {
    adapter_ = _adapter;
  }

  @Override
  public int getNbTimeStep() {
    return adapter_ == null ? 0 : adapter_.getNbTimeStep();
  }

  @Override
  public String getTimeStep(final int _idx) {
    return adapter_ == null ? CtuluLibString.EMPTY_STRING : adapter_.getTimeStep(_idx);
  }

  @Override
  public double getTimeStepValueSec(int _idx) {
    return adapter_ == null ? 0D : adapter_.getTimeStepValueSec(_idx);
  }

  @Override
  public void setTimeStep(final int _idx) {
    if (adapter_ != null) {
      adapter_.setTimeStep(_idx);
    }
  }
}
