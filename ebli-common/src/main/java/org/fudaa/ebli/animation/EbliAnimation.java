/*
 * @creation 26 mars 2004
 * @modification $Date: 2008-04-22 14:30:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.animation;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.InvocationTargetException;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluRoundEtchBorder;
import org.fudaa.ctulu.gui.CtuluTitledPanelCheckBox;
import org.fudaa.ctulu.image.CtuluImageDimensionChooserPanel;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Le composant d'animation, sous forme de palette. Il permet de controler le deroulement de l'animation.
 *
 * @author Fred Deniger
 * @version $Id: EbliAnimation.java,v 1.29 2008-04-22 14:30:27 jm_lacombe Exp $
 */
public class EbliAnimation {

  private final class LauncherPanel extends JPanel implements ActionListener, Observer, ItemListener, DocumentListener {

    JCheckBox partialTs_;
    BuProgressBar bar_;
    BuButton btOut_;
    BuComboBox cb_;
    transient JComboBox cbFirst_;
    transient JComboBox cbLast_;
    BuComboBox cbOut_;
    JCheckBox cbSaveVideo_;
    BuToggleButton configure_;
    BuLabel lbInfo_;
    BuLabel lbSize_;
    ComponentListener listener_;
    Component old_;
    JToggleButton pause_;
    BuPanel pnConfigure_;
    CtuluImageDimensionChooserPanel pnDimImages_;
    JButton start_;
    JButton stop_;
    BuTextField txtInc_;

    public LauncherPanel() {
      bar_ = new BuProgressBar();
      bar_.setStringPainted(true);
      task_.setBar(bar_);
      // BuPanel main = new BuPanel();
      setLayout(new BuBorderLayout(5, 5, true, true));
      setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
      final BuPanel pnTop = new BuPanel();
      pnTop.setLayout(new BuBorderLayout(0, 3, true, false));
      final BuPanel pnButton = new BuPanel();
      pnButton.setLayout(new BuButtonLayout(2, SwingConstants.CENTER));
      final BuIcon icon = EbliResource.EBLI.getIcon("anim-play.png");
      start_ = new JButton(icon);
      start_.setBorderPainted(false);
      start_.setContentAreaFilled(false);
      start_.setToolTipText(EbliLib.getS("Lancer"));
      start_.setActionCommand("GOGOGO_ETREGO");
      start_.addActionListener(this);
      pnButton.add(start_);
      pause_ = new JToggleButton(EbliResource.EBLI.getIcon("anim-pause.png"));
      pause_.setBorderPainted(false);
      pause_.setContentAreaFilled(false);
      pause_.setToolTipText(EbliLib.getS("Pause"));
      pause_.setActionCommand("PAUSE");
      pause_.addActionListener(this);
      pause_.setEnabled(false);
      pnButton.add(pause_);
      stop_ = new JButton(EbliResource.EBLI.getIcon("anim-stop.png"));
      stop_.setBorderPainted(false);
      stop_.setContentAreaFilled(false);
      stop_.setToolTipText(EbliLib.getS("Arr�ter"));
      stop_.setActionCommand("STOP");
      stop_.addActionListener(this);
      stop_.setEnabled(false);
      pnButton.add(stop_);
      final Font ft = BuLib.deriveFont("Label", Font.PLAIN, -2);
      configure_ = new BuToggleButton(EbliLib.getS("Options") + " >>");
      configure_.setFont(ft);
      configure_.addItemListener(this);
      configure_.setHorizontalAlignment(SwingConstants.LEFT);
      final BuPanel pnConfigure = new BuPanel(new BuButtonLayout(0, SwingConstants.LEFT));
      if (cmpOptions_ != null) {
        pnConfigure.add(cmpOptions_);
      }
      pnConfigure.add(configure_);
      pnButton.setBorder(new CtuluRoundEtchBorder(30));
      lbTitle_ = new JLabel();
      lbTitle_.setFont(BuLib.deriveFont(lbTitle_.getFont(), Font.BOLD, 0));
      lbTitle_.setHorizontalTextPosition(SwingConstants.CENTER);
      pnTop.add(lbTitle_, BuBorderLayout.NORTH);
      pnTop.add(pnButton);
      pnTop.add(pnConfigure, BuBorderLayout.SOUTH);

      final BuPanel pnCenter = new BuPanel();
      pnCenter.setLayout(new BuVerticalLayout(5, true, false));
      pnConfigure_ = new BuPanel();
      pnConfigure_.setVisible(false);
      pnCenter.add(pnConfigure_);

      final BuPanel pn = new BuPanel();
      pn.setLayout(new BuBorderLayout(1, 0, true, true));
      lbInfo_ = new BuLabel();
      lbInfo_.setBorder(BorderFactory.createLoweredBevelBorder());
      lbInfo_.setFont(ft);
      lbSize_ = new BuLabel();
      lbSize_.setToolTipText(EbliLib.getS("Dimension source"));
      lbSize_.setFont(ft);
      bar_.setMaximum(100);
      bar_.setSize(100, 10);
      pn.add(lbSize_, BuBorderLayout.WEST);
      pn.add(lbInfo_, BuBorderLayout.EAST);
      pn.add(bar_, BuBorderLayout.SOUTH);

      add(pnTop, BuBorderLayout.NORTH);
      add(pnCenter, BuBorderLayout.CENTER);
      add(pn, BuBorderLayout.SOUTH);
      srcChanged();
    }
    JLabel lbTitle_;

    private void buildConfigure() {
      if (pnConfigure_.getComponentCount() == 0) {
        pnConfigure_.setLayout(new BuVerticalLayout(5, true, false));
        cb_ = new BuComboBox();
        cb_.setModel(createVitesseModel());
        cb_.setSelectedIndex(1);
        cb_.addItemListener(this);
        BuPanel pn = new BuPanel();
        pn.setLayout(new BuGridLayout(2, 5, 5));
        pn.add(new BuLabel(EbliLib.getS("Vitesse animation:")));
        pn.add(cb_);
        pnConfigure_.add(pn);
        pn = new BuPanel();
        pn.setLayout(new BuVerticalLayout(1, true, true));
        final CtuluTitledPanelCheckBox timePn = CtuluLibSwing.createTitleCheckBox(EbliLib.getS("Animation partielle:"));
        partialTs_ = timePn.getTitleCb();
        partialTs_.setSelected(false);
        partialTs_.addItemListener(this);
        timePn.setLayout(new BuGridLayout(2, 5, 5));
        cbFirst_ = new JComboBox();
        cbFirst_.setModel(new TimeCbModel());
        if (cbFirst_.getModel().getSize() > 0) {
          cbFirst_.setSelectedIndex(0);
        }
        cbLast_ = new JComboBox();
        cbLast_.setModel(new TimeCbModel());
        if (cbLast_.getModel().getSize() > 0) {
          cbLast_.setSelectedIndex(task_.getSrc().getNbTimeStep() - 1);
        }
        cbLast_.setEnabled(false);
        cbFirst_.setEnabled(false);
        cbFirst_.addItemListener(this);
        cbLast_.addItemListener(this);
        timePn.add(new BuLabel(EbliLib.getS("d�but")));
        timePn.add(cbFirst_);
        timePn.add(new BuLabel(EbliLib.getS("fin")));
        timePn.add(cbLast_);
        final BuLabel lb = new BuLabel(EbliLib.getS("inc"));
        lb.setToolTipText(EbliLib.getS("Incr�ment"));
        timePn.add(lb);
        txtInc_ = BuTextField.createIntegerField();
        txtInc_.setText(CtuluLibString.UN);
        txtInc_.setColumns(5);
        txtInc_.getDocument().addDocumentListener(this);
        txtInc_.setEnabled(false);
        timePn.add(txtInc_);
        pn.add(timePn);
        pnConfigure_.add(pn);
        pn = new BuPanel(new BuBorderLayout(2, 2));
        buildOuts();
        cbOut_ = new BuComboBox(outs_);
        cbOut_.setSelectedIndex(-1);
        cbOut_.setRenderer(new CtuluCellTextRenderer() {
          @Override
          protected void setValue(final Object _value) {
            super.setValue(_value == null ? CtuluLib.getS("Aucun") : ((EbliAnimationOutputInterface) _value).getName());
          }
        });
        btOut_ = new BuButton(BuResource.BU.getIcon("configurer")) {
          @Override
          public void setEnabled(final boolean _b) {
            super.setEnabled(_b && cbOut_.getSelectedIndex() >= 0);
          }
        };
        btOut_.addActionListener(this);
        pn.add(cbOut_, BuBorderLayout.CENTER);
        pn.add(btOut_, BuBorderLayout.EAST);
        final CtuluTitledPanelCheckBox savePn = CtuluLibSwing.createTitleCheckBox(EbliLib.getS("Enregistrer"));

        savePn.setLayout(new BuVerticalLayout(4, true, false));
        cbSaveVideo_ = savePn.getTitleCb();
        savePn.add(pn);
        cbSaveVideo_.addItemListener(this);
        final BuLabel buLabel = new BuLabel(EbliLib.getS("Dimension des images en sortie"));
        buLabel.setForeground(CtuluLibSwing.getDefaultTitleBorderColor());
        savePn.add(buLabel);
        pnDimImages_ = new CtuluImageDimensionChooserPanel();
        savePn.add(pnDimImages_);
        cbSaveVideo_.setSelected(false);

        cbOut_.addItemListener(this);
        pnConfigure_.add(savePn);
        updateSizeLabel();
        if (task_.getSrc() != null) {
          pnDimImages_.setInitDimension(task_.getSrc().getDefaultImageDimension(), true);
        }
      }

    }

    protected int getFirstIdx() {
      return cbFirst_ == null ? -1 : cbFirst_.getSelectedIndex();
    }

    protected int getLastIdx() {
      return cbLast_ == null ? -1 : cbLast_.getSelectedIndex();
    }

    protected ComponentListener getListener() {
      if (listener_ == null) {
        listener_ = new ComponentListener() {
          @Override
          public void componentHidden(final ComponentEvent _e) {
          }

          @Override
          public void componentMoved(final ComponentEvent _e) {
          }

          @Override
          public void componentResized(final ComponentEvent _e) {
            updateSizeLabel();
          }

          @Override
          public void componentShown(final ComponentEvent _e) {
          }
        };
      }
      return listener_;
    }

    protected void incChanged() {
      final Integer integer = (Integer) txtInc_.getValue();
      if (integer != null) {
        task_.setInc(integer.intValue());
        majInfo();
      }
    }

    protected void majInfo() {
      final StringBuffer bf = new StringBuffer(CtuluLibString.ESPACE).append(EbliLib.getS("pas:")).append(
              task_.getNbDisplayedAnimStep());
      if (outs_ != null) {
        for (int i = 0; i < outs_.length; i++) {
          if (outs_[i].isActivated() && outs_[i].isValid(false) == null) {
            bf.append(" | ");
            bf.append(outs_[i].getShortName());
          }
        }
      }
      // TODO A CONTINUER POUR LES SORTIES ACTIVES
      lbInfo_.setText(bf.toString());
    }

    protected void majTimeStep() {
      if (task_.getSrc().getNbTimeStep() == 0) {
        bar_.setToolTipText("");
        return;
      }
      if (task_.getLastTimeStep() >= 0 && task_.getFirstTimeStep() >= 0) {
        final String txt = task_.getSrc().getTimeStep(task_.getFirstTimeStep());
        final String fin = task_.getSrc().getTimeStep(task_.getLastTimeStep());
        String text = EbliLib.getS("De t={0} � t={1}", txt, fin);
        if (task_.getInc() > 1) {
          text += ' ' + EbliLib.getS("incr�ment:") + task_.getInc();
        }
        bar_.setToolTipText(text);
      }
    }

    protected void majTimeStepInfo() {
      majTimeStep();
      majInfo();
    }

    protected void setRun(final boolean _r) {
      if (partialTs_ != null) {
        final boolean enable = !_r;
        partialTs_.setEnabled(enable);
        /*
         * if (!allTs_.isSelected()) { cbLast_.setEnabled(enable); cbFirst_.setEnabled(enable);
         * txtInc_.setEnabled(enable); }
         */
      }
    }

    protected void setVitesse(final int _t) {
      updateAnimVitesse();
    }

    /**
     * Met a jour le controlleur de s�quence suite a une modification de source.
     */
    protected void srcChanged() {
      final int maxStep = task_.getSrc() == null ? 0 : task_.getSrc().getNbTimeStep();
      if (cbFirst_ != null || cbLast_ != null) {
        // On tente de r�cuperer les items selectionn�s s'ils existent dans la nouvelle s�quence.
        if (cbFirst_ != null) {
          ((TimeCbModel) cbFirst_.getModel()).updateSelectIdx();
        }
        if (cbLast_ != null) {
          ((TimeCbModel) cbLast_.getModel()).updateSelectIdx();
        }
        if (maxStep > 0) {
          if ((partialTs_ != null && !partialTs_.isSelected())
                  || (task_.getLastIdx() > maxStep) || (task_.getLastIdx() < 0)
                  || (task_.getFirstIdx() > maxStep) || (task_.getFirstIdx() < 0)) {
            cbFirst_.setSelectedIndex(0);
            cbLast_.setSelectedIndex(maxStep - 1);
            txtInc_.setText(CtuluLibString.UN);
          }
        }
      }
      if (old_ != task_.getSrc().getComponent()) {
        if (old_ != null && listener_ != null) {
          old_.removeComponentListener(listener_);
        }
        if (task_.getSrc().getComponent() != null) {
          old_ = task_.getSrc().getComponent();
          old_.addComponentListener(getListener());
        }
        updateSizeLabel();
      }
      final boolean b = task_.getSrc().getNbTimeStep() > 0;
      start_.setEnabled(b);
      if (cbSaveVideo_ != null) {
        cbSaveVideo_.setSelected(b);
      }
      majTimeStepInfo();
    }

    protected void updateSizeLabel() {
      final Dimension d = task_.getSrc().getDefaultImageDimension();
      if (d == null) {
        lbSize_.setText(CtuluLibString.EMPTY_STRING);
      } else {
        lbSize_.setText(d.width + " x " + d.height);
      }
      if (pnDimImages_ != null) {
        pnDimImages_.setInitDimension(d, false);
      }
    }

    @Override
    public void actionPerformed(final ActionEvent _e) {
      if (start_ == _e.getSource()) {
        stop_.setEnabled(true);
        pause_.setEnabled(true);
        setRun(true);
        start_.setEnabled(false);
        animStart();

      } else if (stop_ == _e.getSource()) {
        animStop();

      } else if (pause_ == _e.getSource()) {
        animPause();

      } else if (btOut_ == _e.getSource()) {
        final int i = cbOut_.getSelectedIndex();
        if (i >= 0) {
          outs_[i].getConfigurePanel(task_.getNbDisplayedAnimStep()).afficheModale(this);
          outs_[i].setHeight(pnDimImages_.getImageHeight());
          outs_[i].setWidth(pnDimImages_.getImageWidth());
          final String valide = outs_[i].isValid(false);
          if (valide == null) {
            btOut_.setForeground(Color.BLACK);
            btOut_.setToolTipText(outs_[i].getHtmlDesc());
          } else {
            btOut_.setForeground(Color.RED);
            btOut_.setToolTipText(valide);
          }
          majInfo();

        }
      }
    }

    @Override
    public void changedUpdate(final DocumentEvent _e) {
      incChanged();
    }

    public int getVitesse() {
      return getTimeForAnim(cb_ == null ? 1 : cb_.getSelectedIndex());

    }

    @Override
    public void insertUpdate(final DocumentEvent _e) {
      incChanged();
    }

    protected void repackFrame() {
      final BuPalette parent = (BuPalette) SwingUtilities.getAncestorOfClass(BuPalette.class, pn_);
      if (parent != null) {
        parent.pack();
      } else {
        final Window winParent = (Window) SwingUtilities.getAncestorOfClass(Window.class, pn_);
        if (winParent != null) {
          winParent.pack();
        }
      }
    }

    @Override
    public void itemStateChanged(final ItemEvent _e) {
      final Object src = _e.getSource();
      int idx = -1;
      if (src == cbSaveVideo_) {
        if (cbSaveVideo_.isSelected()) {
          updateOutActivated(cbOut_.getSelectedIndex());

        } else {
          updateOutActivated(-1);
        }
      } else if (src == configure_) {
        // le panneau n'etait pas visible on le construit
        if (!pnConfigure_.isVisible() && _e.getStateChange() == ItemEvent.SELECTED) {
          buildConfigure();
        }
        // on le rend (in)visible
        pnConfigure_.setVisible(!pnConfigure_.isVisible());
        repackFrame();
      } else if (src == partialTs_) {
        if (!partialTs_.isSelected()) {
          cbFirst_.setSelectedIndex(0);
          cbLast_.setSelectedIndex(task_.getSrc().getNbTimeStep() - 1);
          txtInc_.setText(CtuluLibString.UN);
        }
        majTimeStepInfo();
      } else if (src == cbOut_) {
        idx = cbOut_.getSelectedIndex();
        btOut_.setEnabled(idx >= 0);
        if (idx >= 0) {
          final String val = outs_[idx].isValid(false);
          if (val == null) {
            btOut_.setForeground(Color.BLACK);
            btOut_.setToolTipText(outs_[idx].getHtmlDesc());
          } else {
            btOut_.setForeground(Color.RED);
            btOut_.setToolTipText(val);
            btOut_.doClick();
          }
        }
        updateOutActivated(idx);
        majInfo();
      } else if (_e.getStateChange() == ItemEvent.SELECTED) {
        if ((src == cbFirst_) || (src == cbLast_)) {
          task_.setFirstIdx(cbFirst_.getSelectedIndex());
          task_.setLastIdx(cbLast_.getSelectedIndex());
          majTimeStepInfo();

        } else {
          setVitesse(cb_.getSelectedIndex());
        }
      }
    }

    private void updateOutActivated(final int _idx) {
      for (int i = outs_.length - 1; i >= 0; i--) {
        outs_[i].setActivated(i == _idx);
      }
    }

    @Override
    public void removeUpdate(final DocumentEvent _e) {
      incChanged();
    }

    @Override
    public void update(final Observable _o, final Object _arg) {
      animFinished();
      stop_.setEnabled(false);
      pause_.setEnabled(false);
      pause_.setSelected(false);
      start_.setEnabled(true);
      task_.getSrc().setVideoMode(false);
      // configure_.setEnabled(true);
      setRun(false);
      if (mustUpdateAfterPlay_) {
        if (newSrc_ == null) {
          majEnableState();
        } else {
          setSrc(newSrc_);
        }
        newSrc_ = null;
        mustUpdateAfterPlay_ = false;

      }
      initTimers();

    }
  }

  protected class TimeCbModel extends AbstractListModel implements ComboBoxModel {

    transient int idx_ = -1;
    transient Object select_;

    @Override
    public Object getElementAt(final int _index) {
      return task_.getSrc().getTimeStep(_index);
    }

    @Override
    public Object getSelectedItem() {
      return select_;
    }

    @Override
    public int getSize() {
      return task_.getSrc().getNbTimeStep();
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != select_) {
        select_ = _anItem;
        idx_ = -1;
        for (int i = getSize() - 1; i >= 0; i--) {
          final Object o = getElementAt(i);
          if ((o == select_) || (o != null && getElementAt(i).equals(select_))) {
            idx_ = i;
            break;
          }
        }
        fireContentsChanged(this, -1, -1);
      }
    }

    /**
     * Met a jour l'indexdans la nouvelle sequence correspondant � l'ancien item
     */
    public void updateSelectIdx() {
      if (idx_ >= 0) {
        Object old = select_;
        select_ = null;
        setSelectedItem(old);
      }
    }
  }

  public final static int getOffset() {
    return 10;
  }

  public final static int getTimeForAnim(final int _i) {
    return getOffset() + 500 * _i;
  }
  // JDialog d_;
  boolean mustUpdateAfterPlay_;
  EbliAnimationSourceInterface newSrc_;
  EbliAnimationOutputInterface[] outs_;
  LauncherPanel pn_;
  TimerTask swingTimer_;
  EbliAnimationTask task_;
  Timer timerContainer_;
  /**
   * Composant optionel ajouter par l'utilisateur
   */
  JComponent cmpOptions_;

  public EbliAnimation() {
    task_ = new EbliAnimationTask();
  }

  /**
   * @return the optionCp
   */
  public JComponent getOptionCp() {
    return cmpOptions_;
  }

  /**
   * @param _optionCp the optionCp to set
   */
  public void setOptionCp(JComponent _optionCp) {
    cmpOptions_ = _optionCp;
  }

  private void createTimer() {
    if (swingTimer_ == null) {
      final Runnable r = new Runnable() {
        @Override
        public void run() {
          if (task_.isFinished_) {
            return;
          }
          if (isPaused()) {
            return;
          }
          task_.doAnim();
        }
      };
      swingTimer_ = new TimerTask() {
        @Override
        public void run() {
          try {
            EventQueue.invokeAndWait(r);
          } catch (final InterruptedException _evt) {
            FuLog.error(_evt);
          } catch (final InvocationTargetException _evt) {
            FuLog.error(_evt);

          }

        }
      };
    }
  }

  // TrIsoModel m_;
  // SwingRepainter repainter_;
  private void updateTitle() {
    if (pn_ != null) {
      pn_.lbTitle_
              .setText(EbliLib.getS("animation")
              + (task_.getSrc() == null ? CtuluLibString.EMPTY_STRING : (CtuluLibString.ESPACE + task_.getSrc()
              .getTitle())));
    }
  }

  void initTimers() {
    if (swingTimer_ != null) {
      swingTimer_.cancel();
      swingTimer_ = null;

    }
    if (timerContainer_ != null) {
      timerContainer_.cancel();

      timerContainer_ = null;
    }
  }

  boolean isPaused() {
    return pn_.pause_.isSelected();
  }

  protected void animPause() {
    // if (!isPaused() && !task_.isFinished_) swingTimer_.start();
  }

  protected void animStart() {
    initTimers();
    int width = 0;
    int height = 0;
    if (pn_.pnDimImages_ != null) {
      width = pn_.pnDimImages_.getImageWidth();
      height = pn_.pnDimImages_.getImageHeight();
    }
    final String err = task_.init(width, height);
    if (err != null) {
      CtuluLibDialog.showError(pn_, CtuluLib.getS("Vid�o"), err);
      task_.finished();
      return;
    }
    createTimer();
    timerContainer_ = new Timer();
    timerContainer_.schedule(swingTimer_, 0, pn_.getVitesse());
  }

  public void animStop() {
    if (isPlaying() || isPaused()) {
      swingTimer_.cancel();
      swingTimer_ = null;
      timerContainer_.cancel();
      timerContainer_ = null;
      task_.finished();
    }

  }

  protected EbliAnimationOutputInterface[] buildOuts() {
    if (outs_ == null) {
      outs_ = new EbliAnimationOutputInterface[4];
      int idx=0;
      outs_[idx++] = new EbliAnimationOutputAvi();
      outs_[idx++] = new EbliAnimationOutputAviMencoder();
      outs_[idx++] = new EbliAnimationOutputGIF();
      outs_[idx++] = new EbliAnimationOutputImage();
    }
    return outs_;
  }

  protected ComboBoxModel createVitesseModel() {
    final String[] vals = new String[8];
    for (int i = 0; i < vals.length; i++) {
      vals[i] = (getTimeForAnim(i) - getOffset()) + " ms";
    }
    vals[0] += " ( " + EbliLib.getS("rapide") + " )";
    vals[3] += " ( " + EbliLib.getS("moyen") + " )";
    vals[7] += " ( " + EbliLib.getS("lent") + " )";
    return new DefaultComboBoxModel(vals);
  }

  protected void updateAnimVitesse() {

    if (isPlaying()) {
      swingTimer_.cancel();
      timerContainer_.cancel();
      timerContainer_ = new Timer();
      swingTimer_ = null;
      createTimer();
      timerContainer_.schedule(swingTimer_, 0, pn_.getVitesse());
      // createTimer();
    } /*
     * else swingTimer_ = null;
     */

  }

  public void active() {
    if (task_.getSrc() == null) {
      return;
    }

    getPanel();
  }

  public void animFinished() {
    if (isPlaying()) {
      swingTimer_.cancel();
      timerContainer_.cancel();
      timerContainer_ = null;
    }
  }

  public JToggleButton getConfigureBt() {
    if (pn_ != null) {
      return pn_.configure_;
    }
    return null;
  }

  public JPanel getPanel() {
    if (pn_ == null) {
      pn_ = new LauncherPanel();
      task_.addObserver(pn_);
      task_.setAvailableOuts(buildOuts());
    }
    return pn_;
  }

  public final EbliAnimationSourceInterface getSrc() {
    return task_.getSrc();
  }

  public boolean isActivated() {
    return pn_ != null;
  }

  /**
   * @return true si en cours
   */
  public boolean isPlaying() {
    return timerContainer_ != null;
  }

  public final void majEnableState() {
    if (FuLog.isTrace()) {
      FuLog.trace("EAN: update state " + getClass().getName());
    }
    if (isPlaying()) {
      mustUpdateAfterPlay_ = true;
      return;
    }
    if (pn_ != null) {
      pn_.srcChanged();
      updateTitle();
    }
  }

  public final void setSrc(final EbliAnimationSourceInterface _src) {
    if (isPlaying()) {
      newSrc_ = _src;
      mustUpdateAfterPlay_ = true;
    } else {
      task_.setSrc(_src);
      majEnableState();
    }
  }

  public JComponent getComponent() {
    getPanel();
    updateTitle();
    return pn_;
  }
}
