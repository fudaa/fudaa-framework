/*
 * @creation 15 nov. 2004
 * @modification $Date: 2007-05-04 13:49:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.animation;

import com.memoire.fu.FuLog;

import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.gif.CtuluGif89EncoderImmediateSave;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author Fred Deniger
 * @version $Id: EbliAnimationOutputGIF.java,v 1.4 2007-05-04 13:49:44 deniger Exp $
 */
public class EbliAnimationOutputGIF extends EbliAnimationOutputAbstract {

  public EbliAnimationOutputGIF() {
    super(".gif");
  }

  public EbliAnimationOutputGIF(EbliAnimationOutputGIF from) {
    super(from);
  }

  @Override
  public EbliAnimationOutputInterface copy() {
    return new EbliAnimationOutputGIF(this);
  }

  @Override
  public void appendFrame(final BufferedImage _im, final boolean _last) {
    try {
      encoder_.addFrame(CtuluLibImage.resize(_im, width_, height_));
    } catch (final IOException _evt) {
      FuLog.error(_evt);

    }
  }

  @Override
  public void finish() {
    try {
      encoder_.finish();

    } catch (final IOException _evt) {
      FuLog.error(_evt);

    }
    warnIfFileExists_ = true;
  }

  @Override
  public String getName() {
    return EbliLib.getS("Gif anim�");
  }

  CtuluGif89EncoderImmediateSave encoder_;

  @Override
  public boolean init(final CtuluImageProducer _p, final Component _parent, final int _nbImg) {
    if (isActivated()) {
      if (!canOverwrittenFile(_parent)) {
        return false;
      }
      destFile_.delete();
      try {
        encoder_ = new CtuluGif89EncoderImmediateSave((int) Math.ceil(delay_ * 100), new FileOutputStream(destFile_));
        encoder_.setLoopCount(0);
      } catch (final FileNotFoundException _evt) {
        FuLog.error(_evt);
        encoder_ = null;
      }
      if (encoder_ == null) {
        return false;
      }
    }
    return true;
  }

}
