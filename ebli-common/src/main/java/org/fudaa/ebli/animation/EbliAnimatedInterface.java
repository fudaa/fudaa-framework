/*
 * @creation 6 f�vr. 07
 * @modification $Date: 2007-02-07 09:55:50 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.animation;

/**
 * @author fred deniger
 * @version $Id: EbliAnimatedInterface.java,v 1.1 2007-02-07 09:55:50 deniger Exp $
 */
public interface EbliAnimatedInterface {
  
  EbliAnimationSourceInterface getAnimationSrc();

}
