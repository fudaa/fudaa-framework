/**
 * @creation 16 nov. 2004
 * @modification $Date: 2007-06-28 09:26:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.animation;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuTextField;
import com.memoire.fu.FuLog;

import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileFilter;
import java.text.NumberFormat;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;

import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author Fred Deniger
 * @version $Id: EbliAnimationOutputImage.java,v 1.17 2007-06-28 09:26:48 deniger Exp $
 */
public class EbliAnimationOutputImage implements EbliAnimationOutputInterface {

  private boolean isActivated_;
  File dir_;
  String pref_ = "image-";
  String fmt_ = "png";
  NumberFormat numFmt_;
  int idx_;
  int width_;
  int height_;


  @Override
  public EbliAnimationOutputInterface copy() {
    return new EbliAnimationOutputImage(this);
  }

  @Override
  public float getDelay() {
    return 0;
  }

  @Override
  public File getDestFile() {
    return dir_;
  }

  @Override
  public String getExt() {
    return null;
  }

  @Override
  public void setDelay(final float _secPerImage) {
  }

  @Override
  public void setFile(final File _dest) {
  }

  @Override
  public void setHeight(final int _h) {
    height_ = _h;

  }

  @Override
  public void setWidth(final int _w) {
    width_ = _w;

  }


  public EbliAnimationOutputImage() {
    if (!CtuluImageExport.FORMAT_LIST.contains("png")) {
      FuLog.warning("EBL: png not supported");
      fmt_ = (String) CtuluImageExport.FORMAT_LIST.get(0);
    }
  }

  public EbliAnimationOutputImage(EbliAnimationOutputImage from) {
    this();
    if (from == null) return;
    this.dir_ = from.dir_;
    this.pref_ = from.pref_;
    this.fmt_ = from.fmt_;
    this.ask_ = from.ask_;
    this.width_ = from.width_;
    this.height_ = from.height_;
    this.idx_ = from.idx_;
    this.isActivated_ = from.isActivated_;
    if (from.numFmt_ != null) {
      this.numFmt_ = (NumberFormat) from.numFmt_.clone();
    }
  }


  @Override
  public void setWarnIfFileExists(boolean warnIfFileExists) {
    ask_ = warnIfFileExists;
  }

  @Override
  public void appendFrame(final BufferedImage _im, final boolean _last) {
    if (isActivated()) {
      idx_++;
      final File dest = new File(dir_, pref_ + numFmt_.format(idx_) + CtuluLibString.DOT + fmt_);
      CtuluImageExport.export(CtuluLibImage.resize(_im, width_, height_), dest, fmt_, null);
      _im.flush();

    }
  }

  @Override
  public CtuluDialogPanel getConfigurePanel(final int _nbFrame) {
    return new ImageEditPanel();
  }

  private class ImageEditPanel extends CtuluDialogPanel {

    JTextField txtDir_;
    BuTextField txtPrefix_;
    BuComboBox cbFmt_;

    public ImageEditPanel() {
      super(true);
      setLayout(new BuGridLayout(2, 5, 5));
      txtDir_ = addLabelFileChooserPanel(EbliLib.getS("R�pertoire de sortie:"), dir_, true, true);
      txtPrefix_ = addLabelStringText(EbliLib.getS("Pr�fixe des fichiers de sortie:"));
      if (pref_ != null) {
        txtPrefix_.setText(pref_);
      }
      add(new BuLabel(EbliLib.getS("Format des images:")));
      cbFmt_ = new BuComboBox();
      cbFmt_.setModel(new DefaultComboBoxModel(CtuluImageExport.FORMAT_LIST.toArray()));
      cbFmt_.setSelectedItem(fmt_);
      add(cbFmt_);
    }

    @Override
    public boolean apply() {
      dir_ = new File(txtDir_.getText());
      fmt_ = (String) cbFmt_.getSelectedItem();
      pref_ = txtPrefix_.getText();
      return true;
    }

    @Override
    public boolean isDataValid() {
      if (txtDir_.getText().trim().length() == 0) {
        setErrorText(EbliLib.getS("Le r�pertoire de sortie n'est pas d�fini"));
        return false;
      }
      final File dir = new File(txtDir_.getText());

      if (dir.exists() && dir.isFile()) {
        setErrorText(EbliLib.getS("Le r�pertoire de sortie utilis� est un fichier"));
        return false;
      }
      final String err = CtuluLibFile.canWrite(dir);
      if (err != null) {
        setErrorText(err);
        return false;
      }
      if (txtPrefix_.getText().trim().length() == 0) {
        txtPrefix_.setText("img-");
      }
      final File[] fileToDelete = getFileWith(dir, txtPrefix_.getText(), (String) cbFmt_.getSelectedItem());
      if (fileToDelete != null && (fileToDelete.length > 0)) {
        final String mes = EbliLib.getS("Le r�pertoire de sortie contient d�j� des fichiers images de type {0}."
            + " Ils seront tous effac�s lors de l'animation.", pref_ + "*." + fmt_);
        CtuluLibDialog.showWarn(this, EbliLib.getS("sortie images"), mes);
        ask_ = false;
      }
      return true;
    }
  }

  @Override
  public void finish() {
    ask_ = true;
  }

  @Override
  public String getName() {
    return EbliLib.getS("Images");
  }

  @Override
  public String getShortName() {
    return fmt_;
  }

  boolean ask_;

  public static File[] getFileWith(final File _dir, final String _pref, final String _fmt) {
    if (!CtuluLibFile.exists(_dir)) {
      return null;
    }
    final FileFilter ft = new FileFilter() {
      String end_ = CtuluLibString.DOT + _fmt;

      @Override
      public boolean accept(File _pathname) {
        if (_pathname.isFile()) {
          final String name = _pathname.getName();
          return name.startsWith(_pref) && name.endsWith(end_);
        }
        return false;
      }
    };
    return _dir.listFiles(ft);
  }

  @Override
  public String getHtmlDesc() {
    final CtuluHtmlWriter res = new CtuluHtmlWriter(true);
    if (dir_ != null) {
      res.append(CtuluLib.getS("Dossier:")).append(CtuluLibString.ESPACE).append(dir_.getAbsolutePath());
    }
    return res.getHtml();
  }

  @Override
  public boolean init(final CtuluImageProducer _p, final Component _parent, final int _nbImg) {
    idx_ = 0;
    numFmt_ = CtuluLibString.getFormatForIndexingInteger(_nbImg);
    final File[] fs = getFileWith(dir_, pref_, fmt_);

    if (ask_ && fs != null && fs.length > 0) {
      String yesText = EbliLib.getS("Enregistrer et ecraser");
      String noText = EbliLib.getS("Ne pas Enregistrer");
      String mes = EbliLib.getS("Le r�pertoire de sortie contient d�j� des fichiers images de type {0}."
          + " Ils seront tous effac�s lors de l'animation.", pref_ + "*." + fmt_);
      mes += CtuluLibString.LINE_SEP;
      mes += EbliLib.getS("Voulez-vous continuer l'enregistrement des images ?");
      final boolean b = CtuluLibDialog.showConfirmation(_parent, EbliLib.getS("sortie images"), mes, yesText, noText);
      if (b) {
        for (int i = fs.length - 1; i >= 0; i--) {
          fs[i].delete();
        }
      } else {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean isActivated() {
    return isActivated_;
  }

  @Override
  public String isValid(final boolean _testDim) {
    if (_testDim) {
      if (width_ <= 0) {
        return CtuluLib.getS("La largeur de l'image est n�gative ou nulle");
      }
      if (height_ <= 0) {
        return CtuluLib.getS("La hauteur de l'image est n�gative ou nulle");
      }
    }
    if (dir_ == null) {
      return EbliLib.getS("Le r�pertoire de sortie n'est pas d�fini");
    }
    if (dir_.exists() && dir_.isFile()) {
      return EbliLib.getS("Le r�pertoire de sortie utilis� est un fichier");
    }
    return CtuluLibFile.canWrite(dir_);
  }

  @Override
  public void setActivated(final boolean _new) {
    isActivated_ = _new;
  }
}
