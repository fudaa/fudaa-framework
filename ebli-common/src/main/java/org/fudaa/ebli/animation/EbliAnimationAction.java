/**
 * @creation 12 nov. 2004
 * @modification $Date: 2007-05-04 13:49:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.animation;

import com.memoire.bu.BuResource;
import java.awt.Window;
import javax.swing.JComponent;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author Fred Deniger
 * @version $Id: EbliAnimationAction.java,v 1.14 2007-05-04 13:49:44 deniger Exp $
 */
public class EbliAnimationAction extends EbliActionPaletteAbstract {

  transient EbliAnimation anim_;

  transient EbliAnimationAdapterInterface target_;

  private EbliAnimationAction() {
    super(EbliLib.getS("Animation"), BuResource.BU.getToolIcon("video"), "ANIM");
    setEnabled(false);
    updateStateBeforeShow();
  }

  public EbliAnimationAction(final EbliAnimationSourceInterface _a, final EbliAnimation _animation, final Window _parent) {
    this();
    parent_ = _parent;
    anim_ = _animation;
    if (anim_ == null) anim_ = new EbliAnimation();
    anim_.setSrc(_a);
  }

  public EbliAnimationAction(final EbliAnimationSourceInterface _a) {
    this(_a, null, null);
  }

  public EbliAnimationAction(final EbliAnimationSourceInterface _a, final EbliAnimation _animation) {
    this(_a, _animation, null);

  }

  private void updateAnimAdapterInterface(final EbliAnimationAdapterInterface _o) {
    target_ = _o;
    if (isPlaying()) {
      return;
    }
    ((EbliAnimationSourceAbstract) anim_.getSrc()).setAdapter(_o);
    if (anim_.isActivated()) {
      anim_.majEnableState();
    }

  }

  protected boolean isPlaying() {
    return anim_.isPlaying();
  }

  public void setAnimAdapter(final Object _o) {
    setAnimAdapterInterface(_o instanceof EbliAnimationAdapterInterface ? (EbliAnimationAdapterInterface) _o : null);
  }

  /**
   * Notifie le composant d'animation que la s�quence a chang�.
   * @param _o La nouvelle s�quence d'animation. Peut etre <tt>null</tt>
   */
  public void setAnimAdapterInterface(final EbliAnimationAdapterInterface _o) {
    if (isPlaying()) {
      return;
    }
    if (_o == null) {
      setEnabled(false);
      updateAnimAdapterInterface(null);

    } else {
      setEnabled(true);
      updateAnimAdapterInterface(_o);
    }
  }

  @Override
  public JComponent buildContentPane() {
    return anim_.getComponent();
  }

  public EbliAnimation getAnim() {
    return anim_;
  }

  public EbliAnimationSourceInterface getSrc() {
    return anim_.getSrc();
  }

  @Override
  public void updateStateBeforeShow() {
    if (anim_ != null && anim_.getSrc() != null) {
      if (anim_.getSrc() instanceof EbliAnimationSourceAbstract) {
        ((EbliAnimationSourceAbstract) anim_.getSrc()).setAdapter(target_);
      }
      anim_.majEnableState();
    }
  }
}
