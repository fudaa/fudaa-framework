/*
 *  @creation     21 sept. 2005
 *  @modification $Date: 2006-04-12 15:28:02 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.find;

import javax.swing.ComboBoxModel;
import javax.swing.ListCellRenderer;
import org.fudaa.ebli.geometrie.GrBoite;

/**
 * Une interface a implementer pour une recherche sur des calques.
 * @author Fred Deniger
 * @version $Id$
 */
public interface EbliFindable {

  ComboBoxModel getItemModel();

  ListCellRenderer getComboRender();

  /**
   * Definit un zoom sur le panneau des calques.
   * @param _env
   */
  void zoom(GrBoite _env);

  /**
   * Selectionne le calque sur lequel faire une recherche.
   * @param _item Le calque
   * @return
   */
  boolean setSelected(EbliFindableItem _item);
}
