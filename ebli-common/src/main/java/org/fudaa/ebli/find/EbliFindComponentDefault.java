/*
 *  @creation     8 d�c. 2003
 *  @modification $Date: 2007-05-04 13:49:44 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.find;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author deniger
 * @version $Id: EbliFindComponentDefault.java,v 1.6 2007-05-04 13:49:44 deniger Exp $
 */
public class EbliFindComponentDefault extends BuPanel implements EbliFindComponent {

  JTextField txtFind_;

  public EbliFindComponentDefault(final String _lb) {
    txtFind_ = new BuTextField(10);
    txtFind_.setToolTipText(getIndiceHelpComplete());
    setLayout(new BuGridLayout(2, 5, 5));
    final JLabel lb = new JLabel(_lb);
    lb.setLabelFor(txtFind_);
    lb.setDisplayedMnemonic(_lb.charAt(0));
    add(lb);
    add(txtFind_);
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  public String getFindText() {
    return txtFind_.getText();
  }

  @Override
  public Object getFindQuery() {
    return getFindText();
  }

  @Override
  public String getSearchId() {
    return "IDX";
  }

  public static String getIndiceHelp() {
    return EbliLib.getS("Vous pouvez entrer plusieurs indices (s�par�s par des ;)") + "<br>"
        + EbliLib.getS("La cha�ne '1-10' s�lectionnera tous les indices entre 1 et 10");
  }

  public static String getIndiceHelpComplete() {
    return "<html>" + getIndiceHelp() + "</html>";
  }

}
