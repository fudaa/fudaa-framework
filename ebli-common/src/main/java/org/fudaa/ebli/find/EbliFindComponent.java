/*
 *  @creation     8 d�c. 2003
 *  @modification $Date: 2006-12-06 09:03:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.find;

import javax.swing.JComponent;

/**
 * @author deniger
 * @version $Id: EbliFindComponent.java,v 1.2 2006-12-06 09:03:37 deniger Exp $
 */
public interface EbliFindComponent {

  /**
   * @return le texte entre par l'utilisateur
   */
  Object getFindQuery();

  /**
   * @return le composant
   */
  JComponent getComponent();

  /**
   * @return le type de recherche
   */
  String getSearchId();

}