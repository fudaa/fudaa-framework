/**
 *  @file         MvFindAction.java
 *  @creation     8 d�c. 2003
 *  @modification $Date: 2006-12-06 09:03:36 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.find;

import org.fudaa.ebli.geometrie.GrBoite;

/**
 * Une action de recherche. Cette action de recherche est associ�e � un calque.
 * Elle construit les composants panneaux de recherche.
 * @author deniger
 * @version $Id$
 */
public interface EbliFindActionInterface {

  /**
   * Recherche et effectue la selection.
   * @param _s la chaine a chercher
   * @param _findId l'action
   * @param _selOption l'option pour la selection
   * @return true si trouve
   */
  boolean find(Object _s,String _findId,int _selOption,EbliFindable _parent);

  /**
   * @param _s la chaine a chercher
   * @param _findId l'action
   * @return null si pas d'erreur. erreur sinon
   */
  String erreur(Object _s,String _findId,EbliFindable _parent);

  /**
   * @param _parent TODO
   * @return le composant de recherche
   */
  EbliFindComponent createFindComponent(EbliFindable _parent);

  /**
   * @param _searchId  l'option pour la recherche
   * @return true si on peut editer
   */
  boolean isEditableEnable(String _searchId,EbliFindable _parent);

  /**
   * @return la boite de zoom
   */
  GrBoite getZoomOnSelected();

  /**
   * @return le calque en question
   */
  EbliFindableItem getCalque();
  

  /**
   * Editer la selection.
   * @return un message d'erreur �ventuel
   */
  String editSelected(EbliFindable _parent);
}