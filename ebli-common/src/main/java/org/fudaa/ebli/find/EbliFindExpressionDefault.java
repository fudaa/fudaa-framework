/*
 *  @creation     15 juin 2005
 *  @modification $Date: 2007-04-16 16:35:08 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.find;

import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ebli.commun.EbliLib;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class EbliFindExpressionDefault implements EbliFindExpressionContainerInterface {

  final Double nbObject_;
  Variable i_;

  public EbliFindExpressionDefault(final int _nbObject) {
    nbObject_ = CtuluLib.getDouble(_nbObject);
  }

  protected Variable createIndexVariable(final CtuluExpr _expr) {
    return _expr.addVar("i", EbliLib.getS("l'indice de l'objet"));
  }

  protected Variable createNbObjectVariable(final CtuluExpr _expr) {
    return _expr.addVar("nb", EbliLib.getS("Nombre d'objets"));
  }

  @Override
  public void initialiseExpr(final CtuluExpr _expr) {
    _expr.initVar();
    _expr.getParser().getFunctionTable().remove("str");
    i_ = createIndexVariable(_expr);
    final Variable nb = createNbObjectVariable(_expr);
    nb.setValue(nbObject_);
    nb.setIsConstant(true);

  }

  @Override
  public void majVariable(final int _idx, final Variable[] _varToUpdate) {
    // Integer : Important pour obtenir un nombre entier lors de l'opération Add()
    // avec un String.
    i_.setValue(new Integer(_idx + 1));
  }

}
