/*
 * @creation 8 d�c. 2003
 * 
 * @modification $Date: 2007-06-05 08:58:39 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.find;

import com.memoire.bu.*;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.*;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluEnhancedDialog;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliSelectionState;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * @author deniger
 * @version $Id$
 */
public class EbliFindDialog extends BuPanel implements ActionListener, ItemListener, PropertyChangeListener {

  static class SelectComboboxRenderer extends CtuluCellTextRenderer {

    private static final String AND = EbliLib.getS("Rechercher dans la s�lection") + " (and)";
    private static final String REPLACE = EbliLib.getS("Remplacer la s�lection courante");
    private static final String REPLACE_SONDE = EbliLib.getS("Remplacer la sonde courante");
    private static final String DEL = EbliLib.getS("Enlever de la s�lection courante") + " (-)";
    private static final String ADD = EbliLib.getS("Ajouter � la s�lection courante") + " (+)";
    private static final String ADD_SONDE = EbliLib.getS("Ajouter aux sondes courantes") + " (+)";
    private final SelectionModeModel selectModeModel;

    /**
     * @param selectModeModel
     */
    public SelectComboboxRenderer(SelectionModeModel selectModeModel) {
      super();
      this.selectModeModel = selectModeModel;
    }
    BuIcon add_ = BuResource.BU.getToolIcon("ajouter");
    BuIcon and_ = EbliResource.EBLI.getToolIcon("selection-and");
    BuIcon remove_ = BuResource.BU.getToolIcon("enlever");
    BuIcon rien_ = BuResource.BU.getToolIcon("aucun");
    BuIcon set_ = EbliResource.EBLI.getToolIcon("new-selection");

    @Override
    protected void setValue(final Object _value) {
      String txt = CtuluLibString.EMPTY_STRING;
      BuIcon ic = rien_;
      if (_value != null) {
        final Integer i = (Integer) _value;
        switch (i.intValue()) {
          case EbliSelectionState.ACTION_ADD:
            txt = selectModeModel.isValueSonde() ? ADD_SONDE : ADD;
            ic = add_;
            break;
          case EbliSelectionState.ACTION_DEL:
            txt = DEL;
            ic = remove_;
            break;
          case EbliSelectionState.ACTION_REPLACE:
            txt = selectModeModel.isValueSonde() ? REPLACE_SONDE : REPLACE;
            ic = set_;
            break;
          case EbliSelectionState.ACTION_AND:
            txt = AND;
            ic = and_;
            break;
          default:
        }
        setText(txt);
        setIcon(ic);
      }
    }
  }

  private static class SelectionModeModel extends AbstractListModel implements ComboBoxModel {

    Object selected_;
    Integer[] values_;
    Integer[] valuesSonde_;
    Integer[] valuesNormal_;
    Integer lastSondeValue = Integer.valueOf(0);
    Integer lastNormalValue = Integer.valueOf(0);

    public SelectionModeModel() {
      valuesNormal_ = new Integer[]{new Integer(EbliSelectionState.ACTION_REPLACE),
        new Integer(EbliSelectionState.ACTION_ADD), new Integer(EbliSelectionState.ACTION_DEL),
        new Integer(EbliSelectionState.ACTION_AND)};
      values_ = valuesNormal_;
      selected_ = values_[0];
    }

    public boolean isValueSonde() {
      return values_ == valuesSonde_;
    }

    public void setSondeMode(boolean b) {
      if (b && values_ != valuesSonde_) {
        lastNormalValue = (Integer) getSelectedItem();
        if (valuesSonde_ == null) {
          valuesSonde_ = new Integer[]{new Integer(EbliSelectionState.ACTION_REPLACE),
            new Integer(EbliSelectionState.ACTION_ADD)};
        }
        values_ = valuesSonde_;
        fireIntervalRemoved(this, valuesSonde_.length, valuesNormal_.length - 1);
        fireContentsChanged(this, 0, valuesSonde_.length - 1);
      } else if (!b && values_ != valuesNormal_) {
        values_ = valuesNormal_;
        fireContentsChanged(this, 0, valuesSonde_.length - 1);
        fireIntervalAdded(this, valuesSonde_.length, valuesNormal_.length - 1);

      }

    }

    @Override
    public Object getElementAt(final int _index) {
      return values_[_index];
    }

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public int getSize() {
      return values_.length;
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != selected_) {
        selected_ = _anItem;
        fireContentsChanged(this, -1, -1);

      }
    }
  }

  public final static boolean isCalqueFindable(final Object _findable) {
    return (_findable instanceof EbliFindableItem) && ((EbliFindableItem) _findable).getFinder() != null;
  }
  JComponent active_;
  BuButton btSearch_;
  JCheckBox cbEdit_;
  SelectionModeModel cbMode_;
  JComboBox cbSelectMode_;
  ComboBoxModel cbSelectModeModel_;
  JCheckBox cbZoom_;
  EbliFindActionInterface currentAct_;
  CtuluEnhancedDialog dial_;
  KeyListener enterListener_;
  EbliFindComponent findCmp_;
  BuPanel pnCenter_;
  EbliFindable visu_;
  final JLabel lbResults_;

  /**
   * @param _visu le panneau de visu
   */
  public EbliFindDialog(final EbliFindable _visu) {
    // ne pas changer le nom!
    setName("EbliFindDialogPanel");
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    visu_ = _visu;
    setLayout(new BuBorderLayout(10, 10));
    // act_ = _act;
    cbSelectModeModel_ = visu_.getItemModel();
    final JComboBox cbCalque = new JComboBox(cbSelectModeModel_);
    final ListCellRenderer renderer = visu_.getComboRender();
    cbCalque.setRenderer(renderer);
    ((JComponent) renderer).setPreferredSize(new Dimension(300, 35));
    cbCalque.setPreferredSize(new Dimension(300, 35));
    cbCalque.addItemListener(this);
    add(cbCalque, BuBorderLayout.NORTH);
    pnCenter_ = new BuPanel();
    pnCenter_.setLayout(new BuVerticalLayout(10, true, false));
    active_ = new BuLabel(BuResource.BU.getString("Rechercher ..."));
    active_.setPreferredSize(new Dimension(400, 200));
    pnCenter_.add(active_);
    cbZoom_ = new BuCheckBox(EbliLib.getS("Zoom"));
    cbZoom_.setMnemonic('z');
    final JPanel pn = new JPanel();
    pn.setLayout(new BuButtonLayout(3, SwingConstants.LEFT));
    pn.add(cbZoom_);
    cbEdit_ = new BuCheckBox(BuResource.BU.getString("Editer"));
    cbEdit_.setMnemonic('e');
    pn.add(cbEdit_);
    pnCenter_.add(pn);
    cbMode_ = new SelectionModeModel();
    cbSelectMode_ = new JComboBox(cbMode_);
    cbSelectMode_.setRenderer(new SelectComboboxRenderer(cbMode_));
    pnCenter_.add(cbSelectMode_);
    cbSelectMode_.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(final ItemEvent _e) {
        if (currentAct_ != null && _e.getStateChange() == ItemEvent.SELECTED) {
          afficheInutileTest();
        }
      }
    });
    lbResults_ = new BuLabel("R�sultat recherche");
    lbResults_.setMinimumSize(new Dimension(30, 15));
    lbResults_.setHorizontalTextPosition(SwingConstants.CENTER);
    lbResults_.setHorizontalAlignment(SwingConstants.CENTER);
    pnCenter_.add(lbResults_);
    add(pnCenter_, BuBorderLayout.CENTER);
    final BuPanel btpn = new BuPanel();
    btpn.setLayout(new BuButtonLayout(5, SwingConstants.RIGHT));
    final String s = BuResource.BU.getString("Rechercher");
    btSearch_ = new BuButton(BuResource.BU.getToolIcon("rechercher"), s);
    btSearch_.setActionCommand("SEARCH");
    btSearch_.setMnemonic(s.charAt(0));
    btSearch_.addActionListener(this);
    btpn.add(btSearch_);
    final BuButton bt = new BuButton(BuResource.BU.getToolIcon("annuler"), BuResource.BU.getString("Annuler"));
    bt.setActionCommand("CANCEL");
    bt.addActionListener(this);
    btpn.add(bt);
    add(btpn, BuBorderLayout.SOUTH);
  }

  /**
   * Appel� quand il y a eu changement de calque de recherche.
   */
  private void updateAll() {
    pnCenter_.remove(active_);
    final Object cq = cbSelectModeModel_.getSelectedItem();
    if (active_ != null) {
      active_.removePropertyChangeListener(this);
    }
    active_ = null;
    currentAct_ = null;
    findCmp_ = null;
    if (cq instanceof EbliFindableItem) {
      final EbliFindableItem mv = (EbliFindableItem) cq;
      currentAct_ = mv.getFinder();
      if (currentAct_ != null) {
        findCmp_ = currentAct_.createFindComponent(visu_);
        active_ = findCmp_.getComponent();
      }
    }
    if (active_ == null) {
      active_ = new BuLabel(EbliLib.getS("Pas de recherche"));
      btSearch_.setEnabled(false);
    } else {
      btSearch_.setEnabled(true);
      final List v = BuLib.getAllSubComponents(active_);
      for (int i = v.size() - 1; i > +0; i--) {
        final Object o = v.get(i);
        if (o instanceof JTextField) {
          setEnterAction((JComponent) o);
        }
      }
      active_.addPropertyChangeListener("find_state", this);
    }
    pnCenter_.add(active_, 0);
    updateEditState();
    updateSelectModel();
    cbZoom_.setEnabled(currentAct_ != null);
    pnCenter_.invalidate();
    if (dial_ == null) {
      doLayout();
      revalidate();
      repaint();
    } else {

      revalidate();
      dial_.pack();
    }
  }

  private void updateEditState() {
    cbEdit_.setEnabled(currentAct_ != null && currentAct_.isEditableEnable(findCmp_.getSearchId(), visu_));
  }

  private void updateSelectModel() {
    if (findCmp_ == null) {
      cbSelectMode_.setEnabled(false);
    } else {
      cbMode_.setSondeMode("SONDE".equals(findCmp_.getSearchId()));
    }
  }

  protected KeyListener getEnterAction() {
    if (enterListener_ == null) {
      enterListener_ = new KeyListener() {
        @Override
        public void keyPressed(final KeyEvent _e) {
        }

        @Override
        public void keyReleased(final KeyEvent _e) {
          if (_e.getKeyCode() == KeyEvent.VK_ENTER) {
            find();
          }
        }

        @Override
        public void keyTyped(final KeyEvent _e) {
        }
      };
    }
    return enterListener_;
  }

  protected void setEnterAction(final JComponent _cp) {
    final KeyListener l = getEnterAction();
    // on ajoute le listener que s'il n'est pas pr�sent.
    if (CtuluLibArray.findObject(_cp.getKeyListeners(), l) < 0) {
      _cp.addKeyListener(getEnterAction());
    }
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    final String com = _evt.getActionCommand();
    if (("CANCEL".equals(com)) && (dial_ != null)) {
      dial_.setVisible(false);
    } else if ("SEARCH".equals(com)) {
      find();
    }
  }

  public void affiche(final Component _parent) {
    if (dial_ == null) {
      dial_ = new CtuluEnhancedDialog(CtuluLibSwing.getFrameAncestorHelper(_parent)) {
        @Override
        public void cancel() {
          setVisible(false);
        }
      };
      dial_.setModal(false);

      dial_.setContentPane(this);
      // setEnterAction(dial_.getRootPane(), true);
      final List v = BuLib.getAllSubComponents(this);
      for (int i = v.size() - 1; i >= 0; i--) {
        if (v.get(i) instanceof JComponent) {
          setEnterAction((JComponent) v.get(i));
        }

      }
      dial_.setTitle(BuResource.BU.getString("Rechercher"));
      dial_.setName("mvFindDialog");
      clearResult();
      dial_.pack();
    }
    clearResult();
    final Dimension dialogSize = dial_.getSize();
    final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    dial_.setLocation((screenSize.width - dialogSize.width) / 2, (screenSize.height - dialogSize.height) / 2);
    dial_.setVisible(true);
  }

  public void afficheInutileTest() {
    if (isInutileSearch()) {
      setErrorText(EbliLib.getS("La s�lection courante est vide"));
    }
  }

  protected void setErrorText(final String _s) {
    lbResults_.setForeground(Color.RED);
    lbResults_.setText(_s);
  }

  protected void clearResult() {
    lbResults_.setText(CtuluLibString.ESPACE);
  }

  protected void setResultText(final String _s) {
    lbResults_.setForeground(CtuluLibSwing.getDefaultLabelForegroundColor());
    lbResults_.setText(_s);
    if (dial_ != null) {
      dial_.pack();
    }
  }

  public void find() {
    clearResult();
    if (currentAct_ == null) {
      return;
    }
    if (!"SONDE".equals(findCmp_.getSearchId()) && isInutileSearch()) {
      afficheInutileTest();
      return;
    }
    final String err = currentAct_.erreur(findCmp_.getFindQuery(), findCmp_.getSearchId(), visu_);
    visu_.setSelected((EbliFindableItem) cbSelectModeModel_.getSelectedItem());
    if (currentAct_.find(findCmp_.getFindQuery(), findCmp_.getSearchId(), ((Integer) cbMode_.getSelectedItem())
            .intValue(), visu_)) {
      if (err != null) {
        setErrorText(BuResource.BU.getString("Avertissement:") + CtuluLibString.ESPACE + err);
      }
    } else {
      if (err == null) {
        setResultText(EbliLib.getS("Non trouv�..."));
      } else {
        setErrorText(err);
      }
      return;
    }

    if (cbZoom_.isSelected()) {
      final GrBoite b = currentAct_.getZoomOnSelected();
      if (b != null) {
        visu_.zoom(b);
      }
    }
    setResultText(EbliLib.getS("S�lection effectu�e..."));
    if ((cbEdit_.isEnabled()) && (cbEdit_.isSelected())) {
      final String errEdit = currentAct_.editSelected(visu_);
      if (errEdit != null) {
        setErrorText(EbliLib.getS("Edition impossible:") + CtuluLibString.ESPACE + errEdit);
      }
    }

  }

  public boolean isInutileSearch() {
    final int i = ((Integer) cbMode_.getSelectedItem()).intValue();
    return (i == EbliSelectionState.ACTION_AND || i == EbliSelectionState.ACTION_DEL)
            && (currentAct_.getCalque()).isSelectionEmpty();
  }

  @Override
  public void itemStateChanged(final ItemEvent _evt) {
    if (_evt.getStateChange() == ItemEvent.SELECTED) {
      updateAll();
    }

  }

  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    updateEditState();
    updateSelectModel();
  }

  /**
   * @param _c le calque a selectionne
   */
  public void setSelectedFindAction(final EbliFindableItem _c) {
    cbSelectModeModel_.setSelectedItem(_c);
  }

  public void updateBeforeShow() {
    updateAll();
  }

  public JComponent getActive() {
    return active_;
  }
}
