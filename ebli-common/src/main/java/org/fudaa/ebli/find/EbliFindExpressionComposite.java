/*
 *  @creation     8 ao�t 2005
 *  @modification $Date: 2006-07-13 13:35:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.find;

import org.fudaa.ctulu.CtuluExpr;
import org.nfunk.jep.Variable;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class EbliFindExpressionComposite implements EbliFindExpressionContainerInterface {

  final EbliFindExpressionContainerInterface normalSupplier_;
  final EbliFindExpressionContainerInterface postSupplier_;

  /**
   * @param _supplier
   * @param _supplier2
   */
  public EbliFindExpressionComposite(final EbliFindExpressionContainerInterface _supplier,
      final EbliFindExpressionContainerInterface _supplier2) {
    super();
    normalSupplier_ = _supplier;
    postSupplier_ = _supplier2;
  }

  @Override
  public void initialiseExpr(final CtuluExpr _exp){
    normalSupplier_.initialiseExpr(_exp);
    postSupplier_.initialiseExpr(_exp);

  }

  @Override
  public void majVariable(final int _idx, final Variable[] _varToUpdate){
    normalSupplier_.majVariable(_idx, _varToUpdate);
    postSupplier_.majVariable(_idx, _varToUpdate);

  }

}
