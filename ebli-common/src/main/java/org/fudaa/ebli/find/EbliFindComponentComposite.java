/*
 *  @creation     17 juin 2005
 *  @modification $Date: 2006-12-06 09:03:36 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.find;

import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Un composant de recherche composite, pouvant gerer plusieurs panneaux de recherche.
 * @author Fred Deniger
 * @version $Id$
 */
public class EbliFindComponentComposite extends JTabbedPane implements EbliFindComponent {

  final EbliFindComponent[] fdComp_;
  final JComponent[] comp_;

  public EbliFindComponentComposite(final EbliFindComponent[] _comp, final String[] _title, final Icon[] _ic) {
    fdComp_ = _comp;
    comp_ = new JComponent[fdComp_.length];
    for (int i = 0; i < comp_.length; i++) {
      final JComponent ci = fdComp_[i].getComponent();
      comp_[i] = ci;
      addTab(_title[i], _ic[i], ci, ci.getToolTipText());
    }
    doLayout();
    addChangeListener(new ChangeListener() {

      @Override
      public void stateChanged(final ChangeEvent _e) {
        EbliFindComponentComposite.this.firePropertyChange("find_state", true, false);
      }
    });
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  

  @Override
  public Object getFindQuery() {
    return fdComp_[getSelectedIndex()].getFindQuery();
  }

  @Override
  public String getSearchId() {
    return fdComp_[getSelectedIndex()].getSearchId();
  }
}
