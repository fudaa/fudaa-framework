/*
 *  @creation     8 d�c. 2003
 *  @modification $Date: 2007-05-04 13:49:44 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.find;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import javax.swing.JComponent;
import javax.swing.JTextField;
import org.fudaa.ebli.commun.EbliLib;

/**
 * @author deniger
 * @version $Id: EbliFindComponentSonde.java,v 1.4 2007-05-04 13:49:44 deniger Exp $
 */
@SuppressWarnings("serial")
public class EbliFindComponentSonde extends BuPanel implements EbliFindComponent {

  JTextField x_;
  JTextField y_;

  public EbliFindComponentSonde() {
    super(new BuGridLayout(2, 5, 5));
    x_ = BuTextField.createDoubleField();
    x_.setColumns(10);
    y_ = BuTextField.createDoubleField();
    y_.setColumns(10);
    setLayout(new BuGridLayout(2, 5, 5));
    add(new BuLabel("X:"));
    add(x_);
    add(new BuLabel("Y:"));
    add(y_);
    add(new BuLabel(""));
    add(new BuLabel(EbliLib.getS("<html><body>Il est possible d'utiliser plusieurs sondes<br>en utilisant l'option ajouter ci-dessous")));
  }

  @Override
  public JComponent getComponent(){
    return this;
  }

  @Override
  public Object getFindQuery() {
    return x_.getText() + ';' + y_.getText();
  }
  

  @Override
  public String getSearchId(){
    return "SONDE";
  }

}
