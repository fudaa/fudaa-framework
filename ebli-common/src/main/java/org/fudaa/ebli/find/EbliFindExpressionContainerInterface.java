/*
 *  @creation     15 juin 2005
 *  @modification $Date: 2006-04-12 15:28:02 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.find;

import org.fudaa.ctulu.CtuluExpr;
import org.nfunk.jep.Variable;

/**
 * Une classe a implementer pour chaque calque qui autorise une recherche par la boite de dialogue rechercher. 
 * @author Fred Deniger
 * @version $Id$
 */
public interface EbliFindExpressionContainerInterface {

  /**
   * Initialise le container d'expression avec les variables pouvant �tre trait�es par le calque.
   * @param _exp Le container d'expression
   */
  void initialiseExpr(CtuluExpr _exp);
  
  /**
   * Initialise la valeur de chaque variable pour l'indice d'objet donn�. Cette m�thode est appel�e pour chaque objet lors du
   * lancement de la selection.
   * @param _idx L'indice de l'objet dans le calque.
   * @param _varToUpdate Les variables dont la valeur est a intialiser.
   */
  void majVariable(int _idx,Variable[] _varToUpdate);
}
