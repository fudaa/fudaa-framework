/*
 *  @file         TrFindComponent.java
 *  @creation     8 d�c. 2003
 *  @modification $Date: 2007-05-04 13:49:44 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.find;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import gnu.trove.TIntHashSet;
import java.util.StringTokenizer;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliSelectionState;
import org.nfunk.jep.Variable;

/**
 * @author deniger
 * @version $Id$
 */
public abstract class EbliFindActionAbstract implements EbliFindActionInterface {

  public static int[] getIndex(final String _idx) {
    if ((_idx == null) || (_idx.length() == 0)) {
      return null;
    }
    final StringTokenizer tk = new StringTokenizer(_idx, ";");
    final TIntHashSet l = new TIntHashSet(tk.countTokens());
    while (tk.hasMoreTokens()) {
      final String tok = tk.nextToken();
      final int idxTiret = tok.indexOf('-');
      if (idxTiret > 0 && idxTiret < tok.length() - 1) {
        try {
          int i1 = Integer.parseInt(tok.substring(0, idxTiret));
          int i2 = Integer.parseInt(tok.substring(idxTiret + 1));
          if (i1 > i2) {
            final int tmp = i2;
            i2 = i1;
            i1 = tmp;
          }
          for (int i = i1; i <= i2; i++) {
            if (i > 0) {
              l.add(i - 1);
            }
          }
        } catch (final NumberFormatException e) {
          if (Fu.DEBUG && FuLog.isDebug()) {
            FuLog.debug("EFI: format is wrong");
          }
        }
      } else {
        int i;
        try {
          i = Integer.parseInt(tok);
        } catch (final NumberFormatException e) {
          i = -1;
        }
        if (i > 0) {
          l.add(i - 1);
        }
      }
    }
    return l.toArray();
  }

  public static String testVide(final int[] _idx) {
    if (_idx == null || _idx.length == 0) {
      return EbliLib.getS("Entr�e vide");
    }
    return null;
  }

  protected EbliFindableItem layer_;

  protected CtuluExpr expr_;
  protected EbliFindExpressionContainerInterface exprFiller_;

  public EbliFindActionAbstract(final EbliFindableItem _layer) {
    layer_ = _layer;
  }
  
  

  protected void buildExpr() {
    expr_ = null;
    exprFiller_ = layer_.getExpressionContainer();
    if (exprFiller_ != null) {
      expr_ = new CtuluExpr();
      exprFiller_.initialiseExpr(expr_);
    }
  }

  protected EbliFindComponent buildExprComp() {
    return new EbliFindComponentExpr(EbliLib.getS("Rechercher:"), expr_);
  }

  protected EbliFindComponent buildSimpleComp() {
    return new EbliFindComponentDefault(EbliLib.getS("Rechercher:"));
  }

  protected abstract boolean changeSelection(CtuluListSelection _l, int _selOption, String _action);

  protected abstract int getNbValuesToTest();

  protected abstract int getNbValuesInModel();

  protected abstract CtuluListSelection getOldSelection();

  protected CtuluListSelection createNewSelection(final String _idx, final String _action, final int _selOption) {
    CtuluListSelection selection = null;
    if ("IDX".equals(_action)) {
      final int[] idx = getIndex(_idx);
      if (idx != null) {
        selection = new CtuluListSelection(idx);
      }
    } else if ("EXPR".equals(_action)) {
      if (FuLog.isTrace()) {
        FuLog.trace("find from expression");
      }
      if (expr_.getParser().hasError()) {
        return null;
      }
      final int nb = getNbValuesToTest();
      selection = new CtuluListSelection(nb);
      final Variable[] vars =expr_.findUsedVar();
      CtuluListSelection old = null;
      if (_selOption == EbliSelectionState.ACTION_AND || _selOption == EbliSelectionState.ACTION_DEL) {
        old = getOldSelection();
      }
      final int min = old == null ? 0 : old.getMinIndex();
      final int max = old == null ? nb - 1 : old.getMaxIndex();
      for (int i = max; i >= min; i--) {
        if (old == null || old.isSelected(i)) {
          if (vars != null) {
            exprFiller_.majVariable(i, vars);
          }
          final int val = (int) expr_.getParser().getValue();
          if (val == 1) {
            selection.add(i);
          }
        }
      }
    }
    return selection;
  }

  @Override
  public EbliFindComponent createFindComponent(final EbliFindable _parent) {
    buildExpr();
    if (expr_ != null) {
      return buildExprComp();
    }
    return buildSimpleComp();
  }

  @Override
  public String editSelected(final EbliFindable _parent) {
    return null;
  }

  @Override
  public String erreur(final Object _s, final String _action, final EbliFindable _parent) {
    final String exp=(String)_s;
    if (exp == null || exp.length() == 0) {
      return EbliLib.getS("Entr�e vide");
    }
    String r = null;
    if ("IDX".equals(_action)) {
      final int[] idx = getIndex(exp);
      r = EbliFindActionAbstract.testVide(idx);
      if (r == null) {
        boolean err = false;
        final int max = getNbValuesInModel();
        for (int i = idx.length - 1; i >= 0 && (!err); i--) {
          if (idx[i] < 0 || idx[i] >= max) {
            err = true;
          }
        }
        if (err) {
          r = EbliLib.getS("Des indices sont en dehors des limites");
        }
      }
    } else if ("EXPR".equals(_action)) {
      exprFiller_.initialiseExpr(expr_);
      expr_.getParser().parseExpression(exp);
      if (expr_.getParser().hasError()) {
        r = expr_.getParser().getError(0);
      }
    }
    return r;
  }

  @Override
  public boolean find(final Object _idx, final String _action, final int _selOption, final EbliFindable _parent) {
    final String exp=(String)_idx;
    final CtuluListSelection selection = createNewSelection(exp, _action, _selOption);
    return selection == null ? false : changeSelection(selection, _selOption, _action);
  }

  @Override
  public boolean isEditableEnable(final String _searchId, final EbliFindable _parent) {
    return false;
  }

}
