/*
 *  @creation     8 d�c. 2003
 *  @modification $Date: 2007-05-04 13:49:44 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.find;

import com.memoire.bu.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.*;
import org.fudaa.ctulu.CtuluExpr;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.editor.CtuluExprGUI;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Un panneau pour la recherche suivant une expression (formule).
 * @author deniger
 * @version $Id$
 */
public class EbliFindComponentExpr extends BuPanel implements EbliFindComponent, ActionListener {

  CtuluExpr expr_;
  JTextField txtFind_;
  protected BuRadioButton btIdx_;
  protected BuRadioButton btExpr_;
  String txtIndices_;
  String txtFormule_;

  final void buildTxtIndices() {
    if (txtIndices_ == null) {
      txtIndices_ = EbliFindComponentDefault.getIndiceHelpComplete();
    }
  }

  final void buildTxtFormule() {
    if (txtFormule_ == null) {
      final CtuluHtmlWriter html = new CtuluHtmlWriter(true);
      html.setDefaultResource(EbliResource.EBLI);
      html.addi18n("Vous pouvez entrer une expression pour une recherche avanc�e").nl().addi18n(
          "Par exemple, l'expression 'i>=10 && z<=100' s�lectionnera<br>"
              + " tous les objets d'indices sup�rieur � 10<br>et dont la valeur z est inf�rieurs � 100").nl().addi18n(
          "L'assistant peut �tre activ� � partir du bouton � droite de la zone de saisie").nl().toString();
      txtFormule_ = html.getHtml();
    }
  }

  BuButton btAss_;

  public EbliFindComponentExpr(final String _lb, final CtuluExpr _expr) {
    super(new BuBorderLayout());
    /*
     * setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BuBorders.EMPTY5555));
     */
    expr_ = _expr;
    txtFind_ = new BuTextField(15);
    txtFind_.setToolTipText("<html>" + EbliFindComponentDefault.getIndiceHelp() + "<br>"
        + EbliLib.getS("Vous pouvez �galement entrer des expressions") + "</html>");
    final BuPanel search = new BuPanel(new BuGridLayout(3, 5, 5));
    final JLabel lb = new JLabel(_lb);
    lb.setLabelFor(txtFind_);
    lb.setDisplayedMnemonic(_lb.charAt(0));
    search.add(lb);
    search.add(txtFind_);
    btAss_ = new BuButton();
    btAss_.setActionCommand("ASSISTANT");
    btAss_.addActionListener(this);
    btAss_.setIcon(CtuluResource.CTULU.getIcon("formule"));
    btAss_.setToolTipText(EbliLib.getS(""));
    search.add(btAss_);
    btAss_.setEnabled(false);
    add(search);
    final ButtonGroup bg = new ButtonGroup();
    final ItemListener listener = new ItemListener() {

      @Override
      public void itemStateChanged(ItemEvent _e) {
        if (_e.getStateChange() == ItemEvent.SELECTED) {
          if (btIdx_.isSelected()) {
            buildTxtIndices();
            btAss_.setEnabled(false);
            txtFind_.setToolTipText(txtIndices_);
          } else {
            buildTxtFormule();
            btAss_.setEnabled(true);
            txtFind_.setToolTipText(txtFormule_);
          }
        }
      }
    };
    btIdx_ = new BuRadioButton(EbliLib.getS("Indice(s)"));
    btExpr_ = new BuRadioButton(EbliLib.getS("Formule"));
    bg.add(btIdx_);
    bg.add(btExpr_);
    btIdx_.addItemListener(listener);
    btExpr_.addItemListener(listener);
    btIdx_.setSelected(true);
    buildTxtFormule();
    buildTxtIndices();
    buildOptionsPanel();
  }

  protected void buildOptionsPanel() {
    final BuPanel pn = new BuPanel(new BuVerticalLayout());
    pn.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), CtuluLib.getS("Options")));
    pn.add(btIdx_);
    pn.add(btExpr_);
    add(pn, BuBorderLayout.SOUTH);
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    expr_.getParser().parseExpression(txtFind_.getText());
    final CtuluExprGUI gui = new CtuluExprGUI(expr_);
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuBorderLayout());
    pn.add(gui);
    final EbliFindDialog imp = (EbliFindDialog) SwingUtilities.getAncestorNamed("EbliFindDialogPanel", this);

    final JDialog f = imp == null ? null : imp.dial_;
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(f, EbliLib.getS("Formule:")))) {
      txtFind_.setText(gui.getExprText());
    }
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public Object getFindQuery() {
    return txtFind_.getText();
  }

  @Override
  public String getSearchId() {
    return btIdx_.isSelected() ? "IDX" : "EXPR";
  }

  /**
   * Redefinit l'expression. L'expression conditionne les variables utilisables dans le panneau.
   * @param _expr L'expression.
   */
  public void setExpr(CtuluExpr _expr) {
    expr_=_expr;
    txtFind_.setText("");
  }
  
  /**
   * Pour une recherche uniquement sur des atomiques. Certaines options sont gris�es.
   * @param _b True : Recherche sur des atomiques.
   */
  public void setFindOnAtomic(boolean _b) {
    btIdx_.setEnabled(!_b);
    if (_b) btExpr_.setSelected(true);
  }
}
