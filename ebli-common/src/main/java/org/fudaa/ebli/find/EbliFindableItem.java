/*
 *  @creation     21 sept. 2005
 *  @modification $Date: 2006-04-12 15:28:02 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.find;

/**
 * Un calque sur lequel on peut faire une recherche.
 * @author Fred Deniger
 * @version $Id$
 */
public interface EbliFindableItem {
  /**
   * @return le composant de recherche pour ce calque
   */
  EbliFindActionInterface getFinder();

  /**
   * @return les variables disponible pour ce calque
   */
  EbliFindExpressionContainerInterface getExpressionContainer();
  
  /**
   * @return true si la selection courante est vide
   */
  boolean isSelectionEmpty();
}
