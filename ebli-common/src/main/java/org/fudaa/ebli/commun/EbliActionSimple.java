/**
 *  @creation     2 oct. 2003
 *  @modification $Date: 2006-09-27 14:31:21 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.commun;
import javax.swing.AbstractButton;
import javax.swing.Icon;
/**
 * @author deniger
 * @version $Id: EbliActionSimple.java,v 1.5 2006-09-27 14:31:21 deniger Exp $
 */
public  class EbliActionSimple extends EbliActionAbstract {
  public EbliActionSimple(final String _name, final Icon _ic, final String _ac) {
    super(_name, _ic, _ac);
  }

  @Override
  public final AbstractButton buildButton(final EbliComponentFactory _f) {
    return _f.buildButton(this);
  }
  @Override
  public final AbstractButton buildMenuItem(final EbliComponentFactory _f) {
    return _f.buildMenuItem(this);
  }
  @Override
  public  AbstractButton buildToolButton(final EbliComponentFactory _f) {
    return _f.buildToolButton(this);
  }
}
