/*
 * @creation 2 oct. 2003
 * @modification $Date: 2006-11-14 09:06:35 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.commun;

import com.memoire.bu.BuLib;
import com.memoire.bu.BuPalette;
import javax.swing.Icon;
import javax.swing.JComponent;


/**
 * @author deniger
 * @version $Id: EbliActionPaletteSpecAbstract.java,v 1.6 2006-11-14 09:06:35 deniger Exp $
 */
public abstract class EbliActionPaletteSpecAbstract extends EbliActionPaletteAbstract {

  protected transient BPalettePanelInterface palette_;
  /** L'action est d�sactiv�e sitot que la palette est cach�e. */
  protected boolean setDisableWhenHide_;

  protected Object target_;

  public EbliActionPaletteSpecAbstract(final String _name, final Icon _icon, final String _actionName) {
    super(_name, _icon, _actionName);
  }

  public EbliActionPaletteSpecAbstract(final String _name, final Icon _icon, final String _actionName,
      final boolean _resizable) {
    super(_name, _icon, _actionName);
    resizable_ = _resizable;
  }

  @Override
  public JComponent buildContentPane() {
    palette_ = buildPaletteContent();
    return palette_.getComponent();
  }

  protected abstract BPalettePanelInterface buildPaletteContent();

  @Override
  protected void doAfterDisplay() {
    if (palette_ != null) {
      palette_.doAfterDisplay();
    }
  }

  @Override
  protected BuPalette getWindow() {
    return window_;
  }

  @Override
  protected void hideWindow() {
    super.hideWindow();
    if (setDisableWhenHide_) {
      setEnabled(false);
      setDisableWhenHide_ = false;
    }
    if (palette_ != null) {
      setPaletteTarget(null);
    }
  }

  @Override
  protected void paletteDeactivated() {
    if (palette_ != null) {
      palette_.paletteDeactivated();
    }
  }

  protected boolean setPaletteTarget(final Object _target) {
    if (palette_ != null) {
      return palette_.setPalettePanelTarget(_target);
    }
    return false;
  }

  /**
   * D�finit le calque actif.
   * @param _o Le calque, null si pas de selection.
   */
  protected void setTarget(final Object _o) {
    final boolean isValid = isTargetValid(_o);
    if (target_ != _o) {
      //target mise a null si non valid
      target_ = isValid?_o:null;
      if ((palette_ != null) && isSelected()) {
        setPaletteTarget(target_);
        if (window_ != null) {
          BuLib.invokeLater(new Runnable() {

            @Override
            public void run() {
              getWindow().pack();
            }
          });
        }

      } else {
        setEnabled(isValid);
      }
    }
    setDisableWhenHide_ = !isValid;
  }

  /**
   * Met a jour la cible de la palette.
   */
  @Override
  public void updateBeforeShow() {
    if (palette_ != null) {
      setPaletteTarget(target_);
      if (window_ != null) {
        window_.pack();
      }
    }
  }

}
