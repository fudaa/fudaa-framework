/**
 *  @creation     3 nov. 2003
 *  @modification $Date: 2008-02-05 17:02:26 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;

import gnu.trove.TIntObjectIterator;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;

/**
 * Une liste de selection. Cette liste associe a chaque geometrie selectionn�e une liste de sous objets selectionn�s.
 * On acc�de aux diff�rentes listes de sous objets par l'indice de la geometrie selectionnee.
 * 
 * @author deniger
 * @version $Id$
 */
public interface EbliListeSelectionMultiInterface {
  /**
   * @return le nombre de geometries selectionn�es
   */
  int getNbListSelected();

  /**
   * @return L'iterateur sur les listes de selections.
   */
  TIntObjectIterator getIterator();
  
  /**
   * @param _i l'indice de geometrie.
   * @return la liste de selection pour cette geometrie
   */
  CtuluListSelectionInterface getSelection(int _idxGeom);
  /**
   * @return Les indices de geometries, stock�s sous la forme d'un tableau. L'ordre des indices retourn�s est
   * quelconque.
   */
  int[] getIdxSelected();
  
  /**
   * @return Les indices de g�ometries stock�s sous la forme d'une liste de selection.
   */
  CtuluListSelection getIdxSelection();

  /**
   * @return true si aucune g�om�trie selectionn�e.
   */
  boolean isEmpty();
  
  /**
   * @return l'indice de la seule g�ometrie selectionn�e. sinon -1.
   */
  int isSelectionInOneBloc();
  
  /**
   * @return le nombre total de sous objets selectionnes.
   */
  int getNbSelectedItem();
}
