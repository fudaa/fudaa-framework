/**
 * @creation 23 mai 2005
 * @modification $Date: 2006-11-14 09:06:35 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2005 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;

import com.memoire.fu.FuLog;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.swing.AbstractListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.fudaa.ctulu.CtuluLibString;


/**
 * Un nouveau mod�le de liste (JList) � utiliser pour afficher une grande chaine
 * texte sans utiliser un JTextArea qui consomme trop de m�moire.
 * @author JM Lacombe
 * @version $Id: EbliListModelLargeString.java,v 1.1 2006-11-14 09:06:35 deniger Exp $
 */
public final class EbliListModelLargeString extends AbstractListModel {
  private ArrayList listeString_;

  /**
   * Constructeur par d�faut.
   */
  public EbliListModelLargeString() {
  }

  /**
   * Contruit le mod�le � partir d'une grande chaine contenant des fin de lignes.
   * @param _largeString String
   */
  public EbliListModelLargeString(final String _largeString) {
    setLargeString(_largeString);
  }

  /**
   * Contruit le mod�le � partir d'une grande chaine contenant des fin de
   * lignes sous la forme d'un tableau d'octets.
   * @param _largeTabByte Le tableau d'octets repr�sentant une grande chaine
   * contenant des fin de lignes.
   */
  public EbliListModelLargeString(final byte[] _largeTabByte) {
    setLargeTabByte(_largeTabByte);
  }

  /**
   * Initialise le mod�le � partir d'une grande chaine contenant des fin de lignes.
   * @param _largeString String
   */
  public void setLargeString(final String _largeString) {
    //douteux ...
    System.gc();
    listeString_ = new ArrayList(_largeString.length()/80);
    final StringTokenizer st = new StringTokenizer(_largeString,"\n\r\f");
    while (st.hasMoreTokens()) {
      listeString_.add(st.nextToken());
    }
    //Fred inutile ici
    //_largeString = null;
    listeString_.trimToSize();
    System.gc();
  }

  /**
   * Initialise le mod�le � partir d'une grande chaine contenant des fin de
   * lignes sous la forme d'un tableau d'octets.
   * @param _largeTabByte byte[]
   */
  public void setLargeTabByte(final byte[] _largeTabByte) {
    if (_largeTabByte == null) {
      setLargeString(CtuluLibString.EMPTY_STRING);
    } else {
      setLargeString( new String(_largeTabByte));
    }
  }

  /**
   * Returns the value at the specified index.
   * @param _index the requested index
   * @return the value at <code>index</code>
   */
  @Override
  public Object getElementAt(final int _index) {
    return listeString_.get(_index);
  }

  /**
   * Returns the length of the list.
   * @return the length of the list
   */
  @Override
  public int getSize() {
    return listeString_.size();
  }
  public static void main(final String[] _args) {
    StringBuffer largeStringBuffer = new StringBuffer(100000);
    for (int i = 0; i < 1000000; i++) {
      largeStringBuffer.append("------------------------ La ligne num�ro ");
      largeStringBuffer.append(i+1);
      largeStringBuffer.append("-------------------------\n");
    }
    String largeString = largeStringBuffer.toString();
    largeStringBuffer = null;
    System.out.println("----Apr�s allocation string()------");
    afficheMemoire();

    final EbliListModelLargeString listeModele = new EbliListModelLargeString();
    final byte[] tabBytes = largeString.getBytes();
    largeString = null;
    FuLog.debug("apr�s largeString.getBytes()");
    afficheMemoire();
    listeModele.setLargeTabByte(tabBytes);
//    listeModele.setLargeString(largeString);

    final JFrame frame = new JFrame("Test ListModelLargeString");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    final JList liste = new JList(listeModele);
    final JPanel content = new JPanel(new BorderLayout());
    content.add(new JScrollPane(liste),  BorderLayout.CENTER);

    final JButton memoire = new JButton("M�moire");
    memoire.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        afficheMemoire();
      }
    });

    content.add(memoire,  BorderLayout.SOUTH);

    frame.getContentPane().add(content);
    frame.pack();

    frame.setVisible(true);

  }

    static void afficheMemoire() {
    System.gc();
    final long menFree = Runtime.getRuntime().freeMemory();
    final long menTot = Runtime.getRuntime().maxMemory();
    FuLog.debug("M�moire utilis�e : "+((menTot-menFree)/1000)+" Ko");
  }

}
