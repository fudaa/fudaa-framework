/*
 *  @creation     2002-08-26
 *  @modification $Date: 2007-05-04 13:49:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.commun;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Classe generique pour manipuler des vecteurs d'objets Geometrie.
 * 
 * @version $Id: VecteurGenerique.java,v 1.15 2007-05-04 13:49:47 deniger Exp $
 * @author Fred Deniger
 */
public class VecteurGenerique extends Object implements Serializable, Cloneable {
  /** Le vecteur contenant les donnees. */
  protected List v_;

  /** Initialise le vecteur. */
  public VecteurGenerique() {
    v_ = new ArrayList();
  }

  protected VecteurGenerique(final int _nb) {
    v_ = new ArrayList(_nb);
  }

  /**
   * @param _target la collection regroupant tous les elements de cette liste.
   */
  public void addAll(final Collection _target) {
    _target.addAll(v_);
  }

  /**
   * @param _o
   */
  public void ajoute(final Object _o) {
    v_.add(_o);
  }

  /**
   * L'objet <code>o</code> aura l'indice <code>j</code> et les objets d'indices sup ou egal seront augmentes d'1.
   * 
   * @param _o :l'objet a inserer
   * @param _j : l'indice
   */
  protected void insere(final Object _o, final int _j) {
    v_.add(_j, _o);
  }

  /**
   * @param _o le nouvel objet d'indice j
   * @param _j l'indice
   */
  protected void remplace(final Object _o, final int _j) {
    v_.set(_j, _o);
  }

  /**
   * Supprime la premiere occurence de l'objet <code>o</code>.
   * 
   * @param _o : l'objet a eneleve du vecteur.
   */
  protected void enleve(final Object _o) {
    v_.remove(_o);
  }

  /**
   * @param _o
   * @return l'indice du premier objet <code>o</code> trouve.
   */
  protected int indice(final Object _o) {
    return v_.indexOf(_o);
  }

  /**
   * @param _o
   * @return true si le vecteur contient <code>o</code>.
   */
  protected boolean contient(final Object _o) {
    return v_.contains(_o);
  }

  /**
   * @param _j
   * @return l'objet d'indice <code>j</code>
   */
  protected Object internRenvoie(final int _j) {
    return v_.get(_j);
  }

  public Object renvoieObj(final int _j) {
    return internRenvoie(_j);
  }

  /**
   * @return le tableau correspondant.
   */
  protected Object[] internTableau() {
    return v_.toArray();
  }

  /**
   * Initialise le vecteur et ajoute tous les elements du tableau <code>_o
   *</code>.
   * 
   * @param _o le nouveau tableau d'objet du vecteur
   */
  protected void internTableau(final Object[] _o) {
    if ((_o == null) || (_o.length <= 0)) {
      return;
    }
    v_.clear();
    v_.addAll(Arrays.asList(_o));
  }

  /**
   * @return un Vecteur de meme type. Les objets internes sont les memes.
   */
  @Override
  public final Object clone() throws CloneNotSupportedException {
    final VecteurGenerique r = (VecteurGenerique) super.clone();
    // Obligatoire sinon le clone pointe sur la m�me liste.
    r.v_=new ArrayList();
    for (int i = 0; i < nombre(); i++) {
      r.ajoute(internRenvoie(i));
    }
    return r;
  }

  @Override
  public String toString() {
    return getClass().getName() + "(" + nombre() + " elements)";
  }

  /**
   * @return la taille du vecteur
   */
  public final int nombre() {
    return v_.size();
  }

  /** Vide entierement le vecteur. */
  public final void vide() {
    v_.clear();
  }

  /**
   * Supprime le premier element du vecteur ( tous les indices sup�rieurs sont modifies).
   */
  public final void enleve() {
    v_.remove(0);
  }

  public final void enleveDernier() {
    v_.remove(v_.size() - 1);
  }

  /**
   * La capacite du vecteur interne est portee a <code>_i</code>.
   * 
   * @param _newSize
   */
  // public final void capacite(int _i) {
  // v_.ensureCapacity(_i);
  // }
  public void setSize(final int _newSize) {
    final int oldSize = v_.size();
    if (oldSize != _newSize) {
      if (_newSize == 0) {
        v_.clear();
      } else if (oldSize > _newSize) {
        for (int i = oldSize - 1; i >= _newSize; i--) {
          v_.remove(i);
        }
      } else {
        v_.addAll(Arrays.asList(new Object[_newSize - oldSize]));
      }

    }
  }

  /**
   * Supprime l'objet d'indice <code>i</code> et modifie les indices superieurs en consequence.
   * 
   * @param _i
   */
  public final void enleve(final int _i) {
    v_.remove(_i);
  }

  /**
   * @return true si le vecteur est vide.
   */
  public final boolean estVide() {
    return v_.isEmpty();
  }
}
