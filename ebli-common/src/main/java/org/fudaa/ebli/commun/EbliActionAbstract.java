/**
 * @file EbliAbstractAction.java
 * @creation 2 oct. 2003
 * @modification $Date: 2008-02-22 16:25:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.ebli.commun;

import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.action.ActionsInstaller;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;

/**
 * Une action avec tooltip adapt� � l'�tat du bouton. Si le bouton est d�sactiv�, un tooltip expliquant commant le rendre actif est affich�.
 *
 * @author deniger
 * @version $Id: EbliActionAbstract.java,v 1.14.6.1 2008-02-22 16:25:43 bmarchan Exp $
 */
public abstract class EbliActionAbstract extends AbstractAction implements EbliActionInterface {
  /**
   * @param _name le nom de l'action
   * @param _icon l'icone
   * @param _command la commande
   */
  public EbliActionAbstract(final String _name, final Icon _icon, final String _command) {
    super(_name, _icon);
    setToolTip(getDefaultTooltip());
    putValue(Action.ACTION_COMMAND_KEY, _command);
  }

  public Icon getIcon() {
    return (Icon) super.getValue(Action.SMALL_ICON);
  }

  protected void setIcon(final Icon _ic) {
    putValue(Action.SMALL_ICON, _ic);
  }

  /**
   * attention: doit etre appele pour faire le menage uniquement !
   */
  @Override
  public void cleanListener() {
    PropertyChangeListener[] propertyChangeListeners = getPropertyChangeListeners();
    for (PropertyChangeListener propertyChangeListener : propertyChangeListeners) {
      removePropertyChangeListener(propertyChangeListener);
    }
  }

  public String getTitle() {
    return (String) super.getValue(Action.NAME);
  }

  public void setKey(final KeyStroke _stroke) {
    super.putValue(ACCELERATOR_KEY, _stroke);
  }
  
  public KeyStroke getKey() {
    return (KeyStroke)super.getValue(ACCELERATOR_KEY);
  }

  public static String getAcceleratorText(final KeyStroke _accelerator) {
    return ActionsInstaller.getAcceleratorText(_accelerator);
  }

  @Override
  public final String getDefaultTooltip() {
    final StringBuilder r = new StringBuilder(50);
    final String actionName = (String) super.getValue(Action.NAME);
    r.append("<p style=\"margin:3px;white-space:nowrap;\">").append(actionName);
    final String accelerator = getAcceleratorText((KeyStroke) getValue(Action.ACCELERATOR_KEY));
    if (StringUtils.isNotBlank(accelerator)) {
      r.append("  (<i>").append(accelerator).append("</i>)");
    }
    r.append("</p>");
    final String def = (String) getValue(DEFAULT_TOOLTIP);
    if (def != null && !def.equalsIgnoreCase(actionName)) {
      r.append("<p style=\"margin:3px\">").append(def).append("</p><br><br>");
    }
    return r.toString();
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
  }

  @Override
  public AbstractButton buildButton(final EbliComponentFactory _f) {
    return null;
  }

  @Override
  public AbstractButton buildMenuItem(final EbliComponentFactory _f) {
    return null;
  }

  @Override
  public AbstractButton buildToolButton(final EbliComponentFactory _f) {
    return null;
  }

  private String buildUnableString(final String _message, final String _endMessage) {
    final StringBuffer r = new StringBuffer("<p style=\"margin:2px\">").append(getDefaultTooltip()).append("</p>");
    if (_endMessage != null) {
      r.append("<p style=\"margin:5px;margin-top:2px;\"><u color=\"red\">").append(
        CtuluResource.CTULU.getString("Message:")).append("</u>").append(_endMessage).append("</p>");
    }
    r.append("<p style=\"margin:5px;margin-top:2px\"><u>").append(EbliLib.getS("Condition:")).append("</u><br>")
      .append(_message).append("</p>");
    return r.toString();
  }

  @Override
  public void setEnabled(final boolean _newValue) {
    super.setEnabled(_newValue);
    updateTooltip();
  }

  protected void updateTooltip() {
    if (isEnabled()) {
      setToolTip(getDefaultTooltip());
    } else {
      setToolTip(getUnableTooltip());
    }
  }

  protected String getUserMessage() {
    return (String) super.getValue("EnableCondition");
  }

  protected void setUserMessage(final String _s) {
    super.putValue("EnableCondition", _s);
  }

  @Override
  public String getEnableCondition() {
    return (String) super.getValue(UNABLE_TOOLTIP);
  }

  @Override
  public final void putValue(final String _key, final Object _newValue) {
    super.putValue(_key, _newValue);
    if (_key == Action.ACCELERATOR_KEY || _key == Action.NAME) {
      updateTooltip();
    }
    if (_key == Action.SHORT_DESCRIPTION) {
      setDefaultToolTip((String) _newValue);
    }
  }

  protected final String getUnableTooltip() {
    final String condtion = getEnableCondition();
    if (condtion != null) {
      return buildUnableString(condtion, getUserMessage());
    }
    return getDefaultTooltip();
  }

  @Override
  public void updateStateBeforeShow() {
  }

  @Override
  public final void setDefaultToolTip(final String _s) {
    putValue(DEFAULT_TOOLTIP, _s);
    updateTooltip();
  }

  public final void setUnableToolTip(final String _s) {
    putValue(UNABLE_TOOLTIP, _s);
  }

  private void setToolTip(final String _s) {
    super.putValue(SHORT_DESCRIPTION, "<html><body>" + _s + "</body></html>");
  }

  public String getTooltip() {
    return (String) getValue(SHORT_DESCRIPTION);
  }

  /**
   * @param acts la liste des actions a parcourir
   * @param _actionCmd la commande a rechercher
   * @return l'action trouvee ou null sinon.
   */
  public static Action findAction(Collection<EbliActionInterface> acts, String _actionCmd) {
    if ((acts == null) || (_actionCmd == null)) {
      return null;
    }
    for (Action action : acts) {
      if (action != null && action.getValue(Action.ACTION_COMMAND_KEY).equals(_actionCmd)) {
        return action;
      }
    }
    return null;
  }
}
