/*
 GPL 2
 */
package org.fudaa.ebli.commun;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCheckBoxMenuItem;
import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuToggleButton;
import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.JMenuItem;

/**
 *
 * @author Frederic Deniger
 */
public class EbliBasicComponentFactory {

  public AbstractButton createButton() {
    return new BuButton();
  }

  public AbstractButton createToggleButton() {
    return new BuToggleButton();
  }

  public JMenuItem createCheckBoxMenuItem() {
    return new BuCheckBoxMenuItem();
  }

  public JMenuItem createMenuItem() {
    return new BuMenuItem();
  }

  public AbstractButton createToolToggleButton() {
    return new BuToggleButton();
  }

  public AbstractButton createCheckBox() {
    return new JCheckBox();
  }

  public AbstractButton createPaletteButton(EbliActionPaletteAbstract _a) {
    return new EbliPaletteButton(_a);
  }

  public AbstractButton createToolButton(EbliActionSimple _a) {
    return new EbliButtonTool(_a);
  }

}
