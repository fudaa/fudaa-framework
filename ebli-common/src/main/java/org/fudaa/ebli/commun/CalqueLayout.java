/**
 * @creation     1998-08-25
 * @modification $Date: 2006-09-19 14:55:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.commun;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.awt.Rectangle;
/**
 * Un overlay layout pour les calques.
 * Les calques sont dessines les uns par-dessus les autres, chacun etant
 * a fond transparent.
 *
 * @version      $Revision: 1.3 $ $Date: 2006-09-19 14:55:55 $ by $Author: deniger $
 * @author       Guillaume Desnoix , Axel von Arnim
 */
public class CalqueLayout implements LayoutManager2 {
  // Vector components;
  public CalqueLayout() {
    // components=new Vector();
  }
  /*
  public Vector getComponents()
  {
    return components;
  }
  */
  /**
    * Ajoute un composant.
    * @param _comp composant
    * @param _cstr contraintes
    */
  @Override
  public void addLayoutComponent(final Component _comp, final Object _cstr) {
  }
  /**
    * Ajoute un composant sans contraintes.
    * @param _name nom du composant (ignore)
    * @param _comp composant
    */
  @Override
  public void addLayoutComponent(final String _name, final Component _comp) {
  }
  /**
    * Retire un composant.
    * @param _comp composant
    */
  @Override
  public void removeLayoutComponent(final Component _comp) {
  }
  /**
    * Calcule la taille minimum pour ce layout.
    * A partir des tailles des composants.
    */
  @Override
  public Dimension minimumLayoutSize(final Container _parent) {
    synchronized (_parent.getTreeLock()) {
      // int nbc=components.size();
      final int nbc= _parent.getComponentCount();
      int w= 0;
      int h= 0;
      for (int i= 0; i < nbc; i++) {
        // Dimension d=((Component)components.elementAt(i)).getMinimumSize();
        final Dimension d= _parent.getComponent(i).getMinimumSize();
        w= Math.max(w, d.width);
        h= Math.max(h, d.height);
      }
      final Insets insets= _parent.getInsets();
      return new Dimension(
        w + insets.left + insets.right,
        h + insets.top + insets.bottom);
    }
  }
  /**
    * Calcule la taille ideale pour ce layout.
    * A partir des tailles des composants.
    */
  @Override
  public Dimension preferredLayoutSize(final Container _parent) {
    synchronized (_parent.getTreeLock()) {
      // int nbc=components.size();
      final int nbc= _parent.getComponentCount();
      int w= 0;
      int h= 0;
      for (int i= 0; i < nbc; i++) {
        // Dimension d=((Component)components.elementAt(i)).getPreferredSize();
        final Dimension d= _parent.getComponent(i).getPreferredSize();
        w= Math.max(w, d.width);
        h= Math.max(h, d.height);
      }
      final Insets insets= _parent.getInsets();
      return new Dimension(
        w + insets.left + insets.right,
        h + insets.top + insets.bottom);
    }
  }
  /**
    * Calcule la taille maximale pour ce layout.
    * A partir des tailles des composants.
    */
  @Override
  public Dimension maximumLayoutSize(final Container _parent) {
    synchronized (_parent.getTreeLock()) {
      // int nbc=components.size();
      final int nbc= _parent.getComponentCount();
      int w= 0;
      int h= 0;
      for (int i= 0; i < nbc; i++) {
        // Dimension d=((Component)components.elementAt(i)).getMaximumSize();
        final Dimension d= _parent.getComponent(i).getMaximumSize();
        w= Math.max(w, d.width);
        h= Math.max(h, d.height);
      }
      final Insets insets= _parent.getInsets();
      return new Dimension(
        w + insets.left + insets.right,
        h + insets.top + insets.bottom);
    }
  }
  @Override
  public void invalidateLayout(final Container _parent) {}
  @Override
  public float getLayoutAlignmentX(final Container _parent) {
    return 0.5f;
  }
  @Override
  public float getLayoutAlignmentY(final Container _parent) {
    return 0.5f;
  }
  /**
    * Applique ce layout a un container.
    * Les composants sont redimensionnes et places.
    */
  @Override
  public void layoutContainer(final Container _parent) {
    synchronized (_parent.getTreeLock()) {
      final Insets insets= _parent.getInsets();
      final Rectangle bounds=
        new Rectangle(
          insets.left,
          insets.top,
          _parent.getSize().width - (insets.left + insets.right),
          _parent.getSize().height - (insets.top + insets.bottom));
      // int nbc=components.size();
      final int nbc= _parent.getComponentCount();
      for (int i= 0; i < nbc; i++) {
        // Component c=(Component)components.elementAt(i);
        final Component c= _parent.getComponent(i);
        final Rectangle actual=
          new Rectangle(bounds.x, bounds.y, bounds.width, bounds.height);
        c.setBounds(actual.x, actual.y, actual.width, actual.height);
      }
    }
  }
}
