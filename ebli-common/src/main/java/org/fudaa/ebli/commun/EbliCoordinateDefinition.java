package org.fudaa.ebli.commun;

/**
 * Une definition pour une coordonn�e, c'est � dire son unit�, son type (entier
 * ou double), son nom.
 *
 * @author marchand@deltacad.fr
 * @version $Id$
 */
public class EbliCoordinateDefinition {
  String name_;
  EbliFormatterInterface fmt_;

  public EbliCoordinateDefinition(String _name, EbliFormatterInterface _fmt) {
    name_=_name;
    fmt_=_fmt;
  }

  /**
   * @return Le nom de la coordonn�e.
   */
  public String getName() {
    return name_;
  }

  /**
   * @return Le formatteur pour cette coordonn�e.
   */
  public EbliFormatterInterface getFormatter() {
    return fmt_;
  }

  /**
   * Definit le formatteur.
   * @param _fmt Le formatteur pour l'unit�.
   */
  public void setFormatter(EbliFormatterInterface _fmt) {
    fmt_=_fmt;
  }

  /**
   * D�finit le nom pour l'unit�.
   * @param _name Le nom de l'unit�.
   */
  void setName(String _name) {
    name_=_name;
  }
}
