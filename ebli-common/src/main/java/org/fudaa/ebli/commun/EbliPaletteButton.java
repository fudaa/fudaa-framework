/**
 *  @creation     2 oct. 2003
 *  @modification $Date: 2006-09-19 14:55:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.commun;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuToggleButton;
/**
 * @author deniger
 * @version $Id: EbliPaletteButton.java,v 1.5 2006-09-19 14:55:55 deniger Exp $
 */
public class EbliPaletteButton extends BuToggleButton {
  EbliActionPaletteAbstract ac_;
  /**
   * 
   */
  public EbliPaletteButton(final EbliActionPaletteAbstract _ac) {
    ac_= _ac;
//    super.setAction(ac_);
  }
  @Override
  public final void setVisible(final boolean _b) {
    if (_b != isVisible()) {
      ac_.setMainButtonVisible(_b);
      super.setVisible(_b);
    }

  }
  
  public void setDesktop(final BuDesktop _d) {
    ac_.setDesktop(_d);
  }
}
