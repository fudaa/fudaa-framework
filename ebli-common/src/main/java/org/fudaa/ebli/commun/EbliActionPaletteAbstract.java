/*
 * @creation 2 oct. 2003
 * @modification $Date$
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.commun;

import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuPalette;
import org.fudaa.ctulu.action.ActionsInstaller;
import org.fudaa.ctulu.gui.CtuluLibSwing;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Une action d�clenchant l'affichage d'une palette. Une palette est un panneau non modal. Elle peut �tre associ�e :
 * <p>
 * - Soit � la JInternalFrame active du Desktop donn� par {@link #setDesktop(BuDesktop)}. Elle apparait et disparait en m�me temps qu'elle. Elle est
 * visualis�e sous forme de Palette interne.<p>
 * - Soit a l'application par {@link #setParent(Component)}. Dans ce cas, elle est visualis�e sous forme de dialogue externe.
 *
 * @author deniger
 * @version $Id$
 */
public abstract class EbliActionPaletteAbstract extends EbliActionChangeState implements InternalFrameListener {
  private static int nbr_ = -1;
  JDialog dial_;
  Point loc_;
  /**
   * Le desktop sur lequel est affich� la palette si non null.
   */
  protected BuDesktop desktop_;
  /**
   * Le parent utilis� pour une palette externe
   */
  protected Component parent_;
  /**
   * La palette est resizable
   */
  protected boolean resizable_;
  /**
   * La palette, contenant un composant sp�cifique pour l'action
   */
  protected transient BuPalette window_;
  protected boolean usedAsTab;
  private boolean dialogIsModal = true;

  /**
   * Construction de l'action.
   *
   * @param _name Le nom de l'action.
   * @param _icon L'icone de l'action
   * @param _actionName La commande de l'action.
   */
  public EbliActionPaletteAbstract(final String _name, final Icon _icon, final String _actionName) {
    super(_name, _icon, _actionName);
  }

  /**
   * Construction de l'action en pr�cisant le caract�re "resizable" la palette.
   *
   * @param _name Le nom de l'action.
   * @param _icon L'icone de l'action
   * @param _actionName La commande de l'action.
   * @param _resizable True Retaillable, false sinn.
   */
  public EbliActionPaletteAbstract(final String _name, final Icon _icon, final String _actionName,
                                   final boolean _resizable) {
    super(_name, _icon, _actionName);
    resizable_ = _resizable;
  }

  /**
   * Construit la palette, en incluant le composant sp�cifique de l'action. L'action �coute les evenements sur la palette.
   */
  protected void buildWindow() {
    if (window_ == null) {
      window_ = new BuPalette() {
        @Override
        protected void fireInternalFrameEvent(final int _i) {
          if (_i == InternalFrameEvent.INTERNAL_FRAME_CLOSED) {
            EbliActionPaletteAbstract.this.setSelected(false);
            hideWindow();
          } else {
            super.fireInternalFrameEvent(_i);
          }
        }
      };
      window_.setName((String) getValue(Action.ACTION_COMMAND_KEY));
      window_.setTitle((String) getValue(Action.NAME));
      window_.setResizable(resizable_);
      window_.setClosable(true);
      final JComponent buildContentPane = getPaletteContent();
      if (buildContentPane instanceof MouseWheelListener) {
        window_.addMouseWheelListener((MouseWheelListener) buildContentPane);
      }
      window_.setContent(buildContentPane);
      window_.getContentPane().setFocusable(true);
      window_.pack();
      window_.addInternalFrameListener(this);
    }
  }

  /**
   * A surcharger pour une op�ration faite apr�s que la palette apparaisse.
   */
  protected void doAfterDisplay() {
  }

  /**
   * @return La palette associ�e � l'action.
   */
  protected BuPalette getWindow() {
    return window_;
  }

  /**
   * Fait disparaitre la palette.
   */
  protected void hideWindow() {
    if (dial_ != null) {
      dial_.dispose();
      dial_ = null;
    }
    if ((window_ == null) || !window_.isVisible()) {
      return;
    }
    loc_ = window_.getLocation();
    window_.setVisible(false);
    window_.getContent().setVisible(false);
    window_.revalidate();
    if (desktop_ != null) {
      if (SwingUtilities.isEventDispatchThread()) {
        desktop_.removeInternalFrame(window_);
      } else {
        SwingUtilities.invokeLater(new Runnable() {
          @Override
          public void run() {
            desktop_.removeInternalFrame(window_);
          }
        });
      }
    }
  }

  /**
   * Controle que la cible est acceptable par l'action/la palette. N'affecte pas la cible courante.
   *
   * @param _o La cible
   * @return true si cible acceptable.
   */
  protected boolean isTargetValid(final Object _o) {
    return true;
  }

  protected void paletteDeactivated() {
  }

  /**
   * Fait apparaitre la palette, apr�s l'avoir construite si elle n'existe pas encore.
   */
  protected void showWindow() {
    final boolean first = window_ == null;
    if (!usedAsTab) {
      buildWindow();
      if (desktop_ != null && loc_ == null) {
        loc_ = window_.getLocation();
        final JInternalFrame fr = desktop_.getCurrentInternalFrame();
        if (fr == null) {
          if ((loc_.x == 0) && (loc_.y == 0)) {
            nbr_ = (nbr_ + 1) % 8;
            loc_ = new Point(5 + 20 * nbr_, 5 + 20 * nbr_);
          }
        } else {
          fr.getLocation(loc_);
          loc_.x -= 5;
          loc_.y -= 5;
        }
      }
    }
    updateBeforeShow();

    final Icon i = (Icon) getValue(Action.SMALL_ICON);
    if (!usedAsTab) {
      if (i != null) {
        window_.setFrameIcon(i);
        // SwingUtilities.updateComponentTreeUI(window_);
      }
      if (desktop_ == null) {
        Window d = CtuluLibSwing.getActiveWindow();// (Window) SwingUtilities.getAncestorOfClass(Window.class, parent_);
        if (d != null) {
          dial_ = (d instanceof Frame) ? new JDialog((Frame) d) : new JDialog((Dialog) d);
          ActionsInstaller.install(dial_);
          dial_.setLocation(d.getLocationOnScreen());
        } else {
          dial_ = new JDialog(CtuluLibSwing.getFrameAncestor(parent_));
          ActionsInstaller.install(dial_);
          dial_.setLocationRelativeTo(parent_);
        }
        dial_.setModal(dialogIsModal);
        dial_.setContentPane(window_.getContentPane());
        dial_.setTitle(window_.getTitle());
        dial_.pack();
        dial_.addWindowListener(new WindowAdapter() {
          @Override
          public void windowClosing(WindowEvent _e) {
            setSelected(false);
            internalFrameClosing(null);
          }
        });
        dial_.setVisible(true);
        doAfterDisplay();
      } else {
        BuLib.invokeLater(() -> {
          window_.getContent().setVisible(true);
          desktop_.addInternalFrame(window_);
          window_.setLocation(loc_);
          doAfterDisplay();
          if (first) {
            desktop_.arrangePalettes();
          } else {
            // pour que les palettes s'affichent correctement.
            window_.pack();
          }
        });
      }
    }
  }

  @Override
  public void setSelected(final boolean _b) {
    super.setSelected(_b);
    if (!_b && dial_ != null && !dialogIsModal) {
      dial_.setVisible(false);
    }
  }

  protected JComponent content;

  public JComponent getPaletteContent() {
    if (content == null) {
      content = buildContentPane();
    }
    return content;
  }

  /**
   * @return le composant a ajouter a la palette
   */
  protected abstract JComponent buildContentPane();

  @Override
  public final AbstractButton buildToolButton(final EbliComponentFactory _f) {
    return _f.buildToolButton(this);
  }

  @Override
  public void changeAction() {
    if (isSelected()) {
      showWindow();
    } else {
      hideWindow();
    }
  }

  public BuDesktop getDesktop() {
    return desktop_;
  }

  public Dimension getPalettePreferredSize() {
    return window_.getPreferredSize();
  }

  public final boolean isResizable() {
    return resizable_;
  }

  /**
   * Definit le desktop pour une palette associ�e � l'InternalFrame active.
   *
   * @param _desktop Le desktop, null si pas d'association avec une fenetre active.
   */
  public void setDesktop(final BuDesktop _desktop) {
    desktop_ = _desktop;
  }

  /**
   * Methode appele lorsque le bouton de cette palette change d'�tat (visible ou non).
   */
  public void setMainButtonVisible(final boolean _b) {
    if (_b && isSelected()) {
      showWindow();
    } else {
      hideWindow();
    }
  }

  public void setPaletteLocation(final int _x, final int _y) {
    loc_ = new Point(_x, _y);
    window_.setLocation(loc_);
  }

  /**
   * Sets whether the associated palette is resizable.
   *
   * @param _b true if the palette is resizable; false otherwise
   */
  public void setPaletteResizable(final boolean _b) {
    resizable_ = _b;
    if (window_ != null) {
      window_.setResizable(resizable_);
    }
  }

  /**
   * Le parent de la palette pour une palette externe. Ne sert pas si la palette est interne.
   *
   * @param _parent Le parent. Peut �tre null.
   */
  public void setParent(Component _parent) {
    parent_ = _parent;
  }

  public final void setResizable(final boolean _resizable) {
    resizable_ = _resizable;
  }

  /**
   * Met a jour la cible de la palette.
   */
  public void updateBeforeShow() {
  }

  @Override
  public void updateStateBeforeShow() {
  }

  /*
   * (non-Javadoc) @see javax.swing.event.InternalFrameListener#internalFrameActivated(javax.swing.event.InternalFrameEvent)
   */
  @Override
  public void internalFrameActivated(InternalFrameEvent e) {
  }

  /*
   * (non-Javadoc) @see javax.swing.event.InternalFrameListener#internalFrameClosed(javax.swing.event.InternalFrameEvent)
   */
  @Override
  public void internalFrameClosed(InternalFrameEvent e) {
  }

  /*
   * (non-Javadoc) @see javax.swing.event.InternalFrameListener#internalFrameClosing(javax.swing.event.InternalFrameEvent)
   */
  @Override
  public void internalFrameClosing(InternalFrameEvent e) {
  }

  /*
   * (non-Javadoc) @see javax.swing.event.InternalFrameListener#internalFrameDeactivated(javax.swing.event.InternalFrameEvent)
   */
  @Override
  public void internalFrameDeactivated(InternalFrameEvent e) {
  }

  /*
   * (non-Javadoc) @see javax.swing.event.InternalFrameListener#internalFrameDeiconified(javax.swing.event.InternalFrameEvent)
   */
  @Override
  public void internalFrameDeiconified(InternalFrameEvent e) {
  }

  /*
   * (non-Javadoc) @see javax.swing.event.InternalFrameListener#internalFrameIconified(javax.swing.event.InternalFrameEvent)
   */
  @Override
  public void internalFrameIconified(InternalFrameEvent e) {
  }

  /*
   * (non-Javadoc) @see javax.swing.event.InternalFrameListener#internalFrameOpened(javax.swing.event.InternalFrameEvent)
   */
  @Override
  public void internalFrameOpened(InternalFrameEvent e) {
  }

  /**
   * @return the dialogIsModal
   */
  public boolean isDialogIsModal() {
    return dialogIsModal;
  }

  /**
   * @param dialogIsModal the dialogIsModal to set
   */
  public void setDialogIsModal(boolean dialogIsModal) {
    this.dialogIsModal = dialogIsModal;
  }

  /**
   * @return the usedAsTab
   */
  public boolean isUsedAsTab() {
    return usedAsTab;
  }

  /**
   * @param usedAsTab the usedAsTab to set
   */
  public void setUsedAsTab(boolean usedAsTab) {
    this.usedAsTab = usedAsTab;
  }
}
