/*
 GPL 2
 */
package org.fudaa.ebli.commun;

import com.thoughtworks.xstream.XStream;

/**
 *
 * @author Frederic Deniger
 */
public interface XstreamCustomizer {

  String PARAMETER_ID = "parameterId";

  void configureXstream(XStream xstream);
}
