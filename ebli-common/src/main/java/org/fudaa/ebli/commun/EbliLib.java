/*
 * @file EbliLib.java
 * 
 * @creation 2002-12-18
 * 
 * @modification $Date: 2007-05-04 13:49:47 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.org
 */
package org.fudaa.ebli.commun;

import com.memoire.bu.BuDesktop;
import com.thoughtworks.xstream.XStream;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.KeyStroke;
import org.fudaa.ctulu.action.ActionsInstaller;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Ensemble de methodes utilitaires non classees.
 *
 * @author Fred Deniger
 * @version $Id: EbliLib.java,v 1.37 2007-05-04 13:49:47 deniger Exp $
 */
public final class EbliLib {

  private EbliLib() {
  }

  public static ObjectInputStream createObjectInpuStream(final File file, XStream parser) throws IOException,
          FileNotFoundException {
    return parser.createObjectInputStream(new InputStreamReader(new FileInputStream(file), Charset.forName("UTF-8")));
  }

  public static ObjectOutputStream createObjectOutpuStream(final File file, XStream parser) throws IOException,
          FileNotFoundException {
    return parser
            .createObjectOutputStream(new OutputStreamWriter(new FileOutputStream(file), Charset.forName("UTF-8")));
  }

  public static boolean isAlphaChanged(final int _alpha) {
    return _alpha >= 0 && _alpha < 255;
  }

  public static Color getAlphaColor(final Color _init, final int _alpha) {
    if (_init == null || _alpha <= 0 || _alpha >= 255) {
      return _init;
    }
    return new Color(_init.getRed(), _init.getGreen(), _init.getBlue(), _alpha);
  }

  /**
   * @param _initValue la valeur initiale
   * @param _min la valeur min autorisee
   * @param _max la valeur max autorisee
   * @return la valeur _initValue si elle est comprise entre les bornes min,max. Sinon, min si valeur inferieur et max si max
   */
  private static int setIn(int _initValue, int _min, int _max) {
    if (_initValue < _min) {
      return _min;
    }
    if (_initValue > _max) {
      return _max;
    }
    return _initValue;
  }

  /**
   * @param _p Le point a modifier pour qu'il soi a l'interieur de jc
   * @param jc le composant a utiliser
   */
  public static void setIn(Point _p, JComponent _jc) {
    _p.x = setIn(_p.x, 0, _jc.getWidth());
    _p.y = setIn(_p.y, 0, _jc.getHeight());

  }

  public static String getZoomDesc() {
    return "<table cellpadding='1'><tr><td>" + EbliLib.getS("Un clic gauche") + "</td><td>:"
            + EbliLib.getS("Zoom centr� sur le point cliqu�") + "</tr><tr><td>" + EbliLib.getS("Rectangle dessin�")
            + "</td><td>:" + EbliLib.getS("Zoom sur le rectangle dessin�") + "</tr><tr><td<i>Shift</i>+"
            + EbliLib.getS("un clic gauche") + "</td><td>:" + EbliLib.getS("Zoom arri�re centr� sur le point cliqu�")
            + "</table>";
  }

  /**
   * @param _sortedArray le tableau des indices dans l'ordre croissant
   * @param _nbItem le nombre d'item
   * @return -1 if no contiguous selection 0 if one selection without loop of index i>0 if one selection with loop of index (the first non selected
   * idx).
   */
  public static int isSelectionContiguous(final int[] _sortedArray, final int _nbItem) {
    final int n = _sortedArray.length;
    int begun = 0;
    int idxJump = 0;
    for (int i = n - 2; i >= 0; i--) {
      if (_sortedArray[i] != (_sortedArray[i + 1] - 1)) {
        if (begun == 0) {
          if ((_sortedArray[n - 1] == (_nbItem - 1)) && (_sortedArray[0] == 0)) {
            begun = 1;
            idxJump = _sortedArray[i] + 1;
          } else {
            return -1;
          }
        } else {
          return -1;
        }
      }
    }
    return begun == 0 ? 0 : idxJump;
  }

  public static void updateMapKeyStroke(final JDialog _c, final Action[] _ac) {
    if (_ac != null) {
      updateMapKeyStroke(_c, Arrays.asList(_ac));
    }
  }

  public static void updateMapKeyStroke(final JDialog _c, final List _ac) {
    if (_ac != null) {
      final InputMap thisMap = _c.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
      final ActionMap m = _c.getRootPane().getActionMap();
      for (int i = _ac.size() - 1; i >= 0; i--) {
        if (_ac.get(i) != null) {
          final Action action = ((Action) _ac.get(i));
          KeyStroke k = (KeyStroke) action.getValue(Action.ACCELERATOR_KEY);
          if (k != null) {
            final Object o = action.getValue(Action.ACTION_COMMAND_KEY);
            thisMap.put(k, o);
            m.put(o, action);
            k = (KeyStroke) action.getValue(EbliActionInterface.SECOND_KEYSTROKE);
            if (k != null) {
              thisMap.put(k, o);
            }
          }
        }
      }
    }
  }

  public static void updateMapKeyStroke(final JComponent _c, final Action[] _ac) {
    updateMapKeyStroke(_c, Arrays.asList(_ac), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
  }

  public static void updateMapKeyStroke(final JComponent _c, final List _acs) {
    updateMapKeyStroke(_c, _acs, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
  }

  public static void updateMapKeyStroke(final JComponent _c, final Action[] _ac, final int _when) {
    updateMapKeyStroke(_c, Arrays.asList(_ac), _when);

  }

  public static void updateMapKeyStroke(final JComponent _c, final List _ac, final int _when) {
    if (_ac != null) {
      final InputMap thisMap = _c.getInputMap(_when);
      final ActionMap m = _c.getActionMap();
      for (int i = _ac.size() - 1; i >= 0; i--) {
        if (_ac.get(i) != null) {
          final Action aci = (Action) _ac.get(i);
          KeyStroke k = (KeyStroke) aci.getValue(Action.ACCELERATOR_KEY);
          if (k != null) {
            final Object o = aci.getValue(Action.ACTION_COMMAND_KEY);
            thisMap.put(k, o);
            m.put(o, aci);
            k = (KeyStroke) aci.getValue(EbliActionInterface.SECOND_KEYSTROKE);
            if (k != null) {
              thisMap.put(k, o);
            }

          }
        }
      }
    }
  }

  public static void updateMapKeyStrokeForButton(final JComponent _c, final Action _ac, final int _when) {
    ActionsInstaller.updateMapKeyStrokeForButton(_c, _ac, _when);
  }

  /**
   * Renvoie le bouton dont l'actionCommand est egale a
   * <code>_actionCommand</code>. Renvoie null si un des deux parametres est nul.
   *
   * @return le bouton de <code>_buttons</code> tel que <code>_actionCommand.equals(_buttons.getActionCommand)</code>.
   */
  public static AbstractButton getAbstractButton(final String _actionCommand, final AbstractButton[] _buttons) {
    if ((_actionCommand == null) || (_buttons == null)) {
      return null;
    }
    final int n = _buttons.length - 1;
    AbstractButton b;
    for (int i = n; i >= 0; i--) {
      b = _buttons[i];
      if (b != null && _actionCommand.equals(b.getActionCommand())) {
        return b;
      }
    }
    return null;
  }

  public static int getAbstractButtonIndex(final String _actionCommand, final AbstractButton[] _buttons) {
    if ((_actionCommand == null) || (_buttons == null)) {
      return -1;
    }
    final int n = _buttons.length - 1;
    AbstractButton b;
    for (int i = n; i >= 0; i--) {
      b = _buttons[i];
      if (b != null && _actionCommand.equals(b.getActionCommand())) {
        return i;
      }
    }
    return -1;
  }

  public static boolean isPopupMouseEvent(final MouseEvent _evt) {
    return _evt.isPopupTrigger();
  }

  /**
   * @param _ac les actions dont l'etat "enable" sera modifie
   * @param _b la nouvelle valeur
   */
  public static void setActionEnable(final Action[] _ac, final boolean _b) {
    if ((_ac != null) && (_ac.length > 0)) {
      for (int i = _ac.length - 1; i >= 0; i--) {
        if (_ac[i] != null) {
          _ac[i].setEnabled(_b);
        }
      }
    }
  }

  /**
   * A "shortcut" to get i18n String (FudaaResource.FUDAA.getString).
   */
  public static String getS(final String _s) {
    return EbliResource.EBLI.getString(_s);
  }

  /**
   * A "shortcut" to get i18n String (FudaaResource.FUDAA.getString).
   */
  public static String getS(final String _s, final String _v0) {
    return EbliResource.EBLI.getString(_s, _v0);
  }

  /**
   * A "shortcut" to get i18n String (FudaaResource.FUDAA.getString).
   */
  public static String getS(final String _s, final String _v0, final String _v1) {
    return EbliResource.EBLI.getString(_s, _v0, _v1);
  }

  public static void cleanListener(EbliActionInterface[] actions) {
    if (actions != null) {
      for (EbliActionInterface ebliActionInterface : actions) {
        cleanListener(ebliActionInterface);
      }
    }
  }

  public static void cleanListener(EbliActionInterface action) {
    if (action != null) {
      action.cleanListener();
    }
  }

  public static List<JComponent> updateToolButtons(final List<? extends EbliActionInterface> _dest, final BuDesktop _bu) {
    if (_dest != null) {
      final List<JComponent> res = new ArrayList<JComponent>(_dest.size());
      for (final Iterator<? extends EbliActionInterface> it = _dest.iterator(); it.hasNext();) {
        final EbliActionInterface ac = it.next();
        if (ac == null) {
          res.add(null);
        } else {
          final JComponent c = ac.buildToolButton(EbliComponentFactory.INSTANCE);
          res.add(c);
          if ((_bu != null) && (ac instanceof EbliActionPaletteAbstract)) {
            ((EbliActionPaletteAbstract) ac).setDesktop(_bu);
          }
        }
      }
      return res;
    }
    return null;
  }
}