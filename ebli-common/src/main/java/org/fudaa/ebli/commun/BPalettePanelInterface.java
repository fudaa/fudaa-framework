/*
 *  @creation     21 sept. 2004
 *  @modification $Date: 2006-11-14 09:06:35 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;

import javax.swing.JComponent;

/**
 * @author Fred Deniger
 * @version $Id: BPalettePanelInterface.java,v 1.1 2006-11-14 09:06:35 deniger Exp $
 */
public interface BPalettePanelInterface {

  /**
   * @return le composant a dessiner
   */
  JComponent getComponent();

  /**
   * @param _target la cible de cette palette. La cible peut �tre un calque, une liste de calques, n'importe quoi,
   * qui va permettre de configurer la palette ou faire les traitements.
   * @return true si la cible est correcte
   */
  boolean setPalettePanelTarget(Object _target);

  /**
   * appele lorsque le selecteur n'est plus activ�e.
   * BM: A priori n'est pas appel�, donc ne pas se crever � implementer.
   */
  void paletteDeactivated();

  /**
   * BM: A priori appel� apr�s que la palette soit visible, en cas de chgt de cible. Par exemple pour refaire un pack().
   */
  void doAfterDisplay();

}
