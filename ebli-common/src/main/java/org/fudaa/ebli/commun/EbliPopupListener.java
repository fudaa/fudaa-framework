/*
 * @creation 29 sept. 06
 * @modification $Date: 2006-10-19 14:13:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author fred deniger
 * @version $Id: EbliPopupListener.java,v 1.2 2006-10-19 14:13:25 deniger Exp $
 */
public abstract class EbliPopupListener extends MouseAdapter {

  @Override
  public void mousePressed(final MouseEvent _e) {
    if (EbliLib.isPopupMouseEvent(_e)) {
      popup(_e.getComponent(),_e.getX(), _e.getY());
    }
  }

  @Override
  public void mouseReleased(final MouseEvent _e) {
    if (EbliLib.isPopupMouseEvent(_e)) {
      popup(_e.getComponent(),_e.getX(), _e.getY());
    }
  }

  public abstract void popup(Component _src,int _x, int _y);

}
