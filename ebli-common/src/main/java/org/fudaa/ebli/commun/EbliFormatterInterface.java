/*
 *  @creation     6 avr. 2005
 *  @modification $Date: 2006-04-12 15:28:07 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;

import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.editor.CtuluValueEditorI;

/**
 * @author Fred Deniger
 * @version $Id: EbliFormatterInterface.java,v 1.5 2006-04-12 15:28:07 deniger Exp $
 */
public interface EbliFormatterInterface extends CtuluValueEditorI{

  CtuluNumberFormatI getXYFormatter();
  String getUnit();
}