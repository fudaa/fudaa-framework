/*
 * @file         BToggleButton3States.java
 * @creation     1999-02-10
 * @modification $Date: 2006-11-14 09:06:35 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.commun;

import com.memoire.bu.BuFilters;
import com.memoire.bu.BuIcon;
import com.memoire.bu.BuToggleButton;
import java.awt.event.MouseEvent;
import java.awt.image.FilteredImageSource;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * Un ToggleBouton a trois etats (selected, reselected, deselected).
 * 
 * @version $Revision: 1.1 $ $Date: 2006-11-14 09:06:35 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class BToggleButton3States extends BuToggleButton {

  private boolean is3states_;
  private boolean reselected_;
  private java.awt.Color bg_;
  private Icon reselIcon_, icon_;

  public BToggleButton3States() {
    super();
  }

  public BToggleButton3States(final BuIcon _icon) {
    super(_icon);
    icon_ = _icon;
    reselIcon_ = _icon;
  }

  public BToggleButton3States(final String _label) {
    super(_label);
  }

  public BToggleButton3States(final BuIcon _icon, final String _label) {
    super(_icon, _label);
    icon_ = _icon;
    reselIcon_ = _icon;
  }

  @Override
  public void setIcon(final Icon _icon) {
    super.setIcon(_icon);
    icon_ = _icon;
    if (_icon instanceof ImageIcon) {
      reselIcon_ = new BuIcon(createImage(new FilteredImageSource(((ImageIcon) _icon).getImage().getSource(),
          BuFilters.RED)));
    } else {
      reselIcon_ = _icon;
    }
  }

  public void set3States(final boolean _s) {
    if (_s == is3states_) {
      return;
    }
    final boolean vp = is3states_;
    is3states_ = _s;
    firePropertyChange("3States", vp, is3states_);
  }

  public boolean is3States() {
    return is3states_;
  }

  public boolean isReselected() {
    return reselected_;
  }

  public void setReselected(final boolean _s) {
    if (!is3states_) {
      return;
    }
    if (_s == reselected_) {
      return;
    }
    final boolean vp = reselected_;
    reselected_ = _s;
    if (reselected_) {
      setSelected(true);
    }
    firePropertyChange("reselected", vp, reselected_);
  }

  // Surcharge de la m�thode setSelected pour remise � l'�tat initial du bouton
  @Override
  public void setSelected(final boolean _b) {
    reselected_ = false;
    if (bg_ != null) {
      setForeground(bg_);
      if (getIcon() != null) {
        super.setIcon(icon_);
      }
      bg_ = null;
    }
    super.setSelected(_b);
  }

  @Override
  protected void processMouseEvent(final MouseEvent _evt) {
    if (_evt.getID() == MouseEvent.MOUSE_RELEASED && is3states_ && isSelected()) {
      if (reselected_) {
        reselected_ = false;
        if (bg_ != null) {
          setForeground(bg_);
          if (getIcon() != null) {
            super.setIcon(icon_);
          }
          bg_ = null;
        }
      } else {
        setSelected(false);
        reselected_ = true;
        bg_ = getForeground();
        setForeground(java.awt.Color.red.darker());
        if (getIcon() != null) {
          super.setIcon(reselIcon_);
        }
      }
    }
    super.processMouseEvent(_evt);
  }
}
