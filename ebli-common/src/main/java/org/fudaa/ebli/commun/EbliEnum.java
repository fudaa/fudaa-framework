/*
 *  @file         EbliEnum.java
 *  @creation     3 oct. 2003
 *  @modification $Date: 2006-09-19 14:55:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.commun;
/**
 * @author deniger
 * @version $Id: EbliEnum.java,v 1.4 2006-09-19 14:55:55 deniger Exp $
 */
public class EbliEnum {
  private final String desc_;
  public EbliEnum(final String _name) {
    desc_= _name;
  }
  public String getDesc() {
    return desc_;
  }
  @Override
  public String toString() {
    return "EbliEnum " + desc_;
  }
}
