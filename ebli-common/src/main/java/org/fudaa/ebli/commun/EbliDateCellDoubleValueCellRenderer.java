/*
GPL 2
 */
package org.fudaa.ebli.commun;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.table.CtuluTableCellDoubleValue;

/**
 *
 * @author Frederic Deniger
 */
public class EbliDateCellDoubleValueCellRenderer extends CtuluCellTextRenderer {

  @Override
  protected void setValue(final Object _value) {
    setHorizontalTextPosition(RIGHT);
    setHorizontalAlignment(RIGHT);
    setText((_value == null) ? CtuluLibString.EMPTY_STRING : ((CtuluTableCellDoubleValue) _value).getFormattedValue());
  }

}
