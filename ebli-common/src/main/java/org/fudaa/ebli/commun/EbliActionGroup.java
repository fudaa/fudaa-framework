/**
 *  @creation     24 juin 2004
 *  @modification $Date: 2006-09-19 14:55:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: EbliActionGroup.java,v 1.4 2006-09-19 14:55:55 deniger Exp $
 */
public class EbliActionGroup implements PropertyChangeListener {

  private final EbliActionChangeState[] actions_;

  public EbliActionGroup(final List _l) {
    actions_ = new EbliActionChangeState[_l.size()];
    _l.toArray(actions_);
    for (int i = actions_.length - 1; i >= 0; i--) {
      actions_[i].setSelected(false);
      actions_[i].addPropertyChangeListener(this);
    }
  }

  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    if ("selected".equals(_evt.getPropertyName())) {
      final EbliActionChangeState s = (EbliActionChangeState) _evt.getSource();
      final boolean newVal = ((Boolean) _evt.getNewValue()).booleanValue();
      if (newVal) {
        for (int i = actions_.length - 1; i >= 0; i--) {

          if ((actions_[i] != s) && (actions_[i].isSelected())) {
            actions_[i].changeAll();
          }
        }
        s.setSelected(true);
      }
    }
  }

}
