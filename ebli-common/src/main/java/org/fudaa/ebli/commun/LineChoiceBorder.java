/**
 * @creation     2000-11-28
 * @modification $Date: 2006-11-14 09:06:35 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.border.AbstractBorder;
/**
 * @version      $Revision: 1.1 $ $Date: 2006-11-14 09:06:35 $ by $Author: deniger $
 * @author       Jean-Marc Lacombe
 */
public class LineChoiceBorder extends AbstractBorder {
  private final boolean high_, bottom_, right_, left_, middleVert_, middleHor_;
  public LineChoiceBorder(
    final boolean _high,
    final boolean _bottom,
    final boolean _right,
    final boolean _left,
    final boolean _middleVert,
    final boolean _middleHor) {
    high_= _high;
    bottom_= _bottom;
    right_= _right;
    left_= _left;
    middleVert_= _middleVert;
    middleHor_= _middleHor;
  }
  @Override
  public void paintBorder(
    final Component _c,
    final Graphics _g,
    final int _x,
    final int _y,
    final int _w,
    final int _h) {
    final Color old= _g.getColor();
    _g.translate(_x, _y);
    _g.setColor(_c.getForeground());
    if (high_) {
      _g.drawLine(0, 0, _w, 0);
    }
    if (bottom_) {
      _g.drawLine(0, _h, _w, _h);
    }
    if (right_) {
      _g.drawLine(_w - 1, 0, _w, _h);
    }
    if (left_) {
      _g.drawLine(0, 0, 0, _h);
    }
    if (middleVert_) {
      _g.drawLine(_w / 2, 0, _w / 2, _h);
    }
    if (middleHor_) {
      _g.drawLine(0, _h / 2, _w, _h / 2);
    }
    //    _g.draw3DRect(0,0,_w-1,_h-1,true);
    _g.translate(-_x, -_y);
    _g.setColor(old);
  }
  @Override
  public Insets getBorderInsets(final Component _c) {
    return new Insets(3, 3, 3, 3);
  }
  @Override
  public boolean isBorderOpaque() {
    return true;
  }
}
