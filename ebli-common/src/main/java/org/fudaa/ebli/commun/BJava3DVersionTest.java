/*
 * @creation 31 mai 2006
 * @modification $Date: 2007-02-02 11:21:54 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;

import com.memoire.fu.FuLog;
import java.lang.reflect.Method;
import java.util.Map;
import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Une classe permettant de tester si java3D est install�.
 * 
 * @author fred deniger
 * @version $Id: BJava3DVersionTest.java,v 1.3 2007-02-02 11:21:54 deniger Exp $
 */
public final class BJava3DVersionTest {

  private BJava3DVersionTest() {}

  /**
   * true si le teste a d�j� �t� effectu�.
   */
  private static boolean isInit_;

  /**
   * true si java est install� mais ne peut pas se lancer: cela arrive si la carte graphique n'est pas support� par
   * Java3D.
   */
  private static boolean isWrongVersion_;
  /**
   * La version de Java 3D.
   */
  private static String version_;

  private static void init() {
    if (isInit_) {
      return;
    }
    isInit_ = true;
    Class z = null;
    try {
      z = Class.forName("javax.media.j3d.VirtualUniverse");
      final Method m = z.getMethod("getProperties", null);
      if (m != null) {
        final Map res = (Map) m.invoke(z.newInstance(), null);
        if (res != null) {
          version_ = (String) res.get("j3d.version");
        }
      }
    } catch (final ClassNotFoundException _evt) {} catch (final Throwable _ex) {
      FuLog.error(_ex);
      _ex.printStackTrace();
      isWrongVersion_ = true;

    }
  }

  public static boolean isJava3DFound() {
    init();
    return version_ != null;
  }

  public static boolean isWrongVersion() {
    init();
    return isWrongVersion_;
  }

  public static String getVersion() {
    init();
    return version_;
  }

  public static String getNewVersion() {
    return "https://java3d.dev.java.net/binary-builds.html";
  }

  public static String getOldVersion() {
    return "https://java3d.dev.java.net/binary-builds-old.html";
  }

  public static void showVersionDialog(final CtuluUI _ui) {
    if (isJava3DFound()) {
      _ui.message("3D", getHtmlInstallMessage(true), false);
    } else {
      _ui.error("3D", getHtmlInstallMessage(true), false);
    }
  }

  public static String getHtmlInstallMessage(final boolean _complete) {
    final CtuluHtmlWriter writer = new CtuluHtmlWriter(true, true);
    writer.setDefaultResource(EbliResource.EBLI);
    init();
    boolean ok = false;
    if (isWrongVersion_) {

      writer
          .addi18n("Java 3D est install� sur votre poste mais il n'est pas op�rationnel.")
          .nl()
          .addi18n("Il se peut que votre carte graphique ne soit pas ou plus support�e par cette version de Java 3D.")
          .nl()
          .addi18n(
              "Si votre carte est 'ancienne', vous pouvez essayer de d�sinstaller cette version de Java 3D et d'installer une version moins r�cente (1.3.2 par exemple)");
    } else if (isJava3DFound()) {
      ok = true;
      writer.addi18n("Java 3D est install� sur votre poste").nl().addi18n("Version utilis�e:").append(
          CtuluLibString.ESPACE).append(getVersion());
    } else {
      writer.addi18n("Java 3D n'est pas install� sur votre poste");
    }
    if (!ok && _complete) {
      writer.nl().nl().addTagAndText("b", EbliLib.getS("T�l�chargements"));
      final String nbsp = "&nbsp;";

      writer.nl().addTag("ul");
      final CtuluHtmlWriter.Tag li = writer.addTag("li");
      writer.addi18n("les derni�res versions:").append(nbsp).addLink(getNewVersion());
      li.close();
      writer.addTag("li");
      writer.addi18n("les anciennes versions:").append(nbsp).addLink(getOldVersion());
    }
    return writer.getHtml();
  }

}
