/*
 * @creation 11 mars 2005
 * 
 * @modification $Date: 2007-04-16 16:35:09 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.ObjectUtils;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;

/**
 * Classe decrivant les propri�t�s d'un calque. NE MODIFIER QUE SI VRAIMENT NECESSAIRE: utiliser pour serialiser l'etat d'un calque
 *
 * @author Fred Deniger
 * @version $Id: EbliUIProperties.java,v 1.7 2007-04-16 16:35:09 deniger Exp $
 */
public final class EbliUIProperties {

  @XStreamAlias("Name")
  private String name_;
  @XStreamAlias("Values")
  private final Map values_;

  public EbliUIProperties() {
    values_ = new HashMap();
  }

  public void addAll(EbliUIProperties other) {
    if (other == null) {
      return;
    }
    for (Object object : other.values_.entrySet()) {
      java.lang.Object key = ((Map.Entry) object).getKey();
      java.lang.Object value = ((Map.Entry) object).getValue();
      values_.put(key, value);
    }
  }

  /**
   *
   * @param prefix le prefix � ajouter � toutes les propri�t�s
   * @return une nouvelles instance de EbliUIProperties
   */
  public EbliUIProperties cloneWithPrefix(String prefix) {
    EbliUIProperties res = new EbliUIProperties(name_);
    for (Object object : values_.entrySet()) {
      java.lang.Object key = ((Map.Entry) object).getKey();
      java.lang.Object value = ((Map.Entry) object).getValue();
      res.values_.put(prefix + key, value);
    }
    return res;
  }

  /**
   *
   * @param prefix
   * @return une map de propri�t� contenant uniquement celles commencant par le prefix.
   */
  public EbliUIProperties extractSubset(String prefix) {
    EbliUIProperties res = new EbliUIProperties(name_);
    int prefixSize = prefix.length();
    for (Object object : values_.entrySet()) {
      String key = ObjectUtils.toString(((Map.Entry) object).getKey());
      java.lang.Object value = ((Map.Entry) object).getValue();
      if (key != null && key.startsWith(prefix)) {
        res.values_.put(key.substring(prefixSize), value);
      }
    }
    return res;
  }

  public EbliUIProperties(final String _name) {
    this();
    name_ = _name;
  }

  public void put(final String _key, final boolean _val) {
    values_.put(_key, CtuluLibString.toString(_val));
  }

  public void remove(final String _key) {
    values_.remove(_key);
  }

  public void put(final String _key, final double _val) {
    values_.put(_key, CtuluLib.getDouble(_val));
  }

  public void put(final String _key, final int _val) {
    values_.put(_key, new Integer(_val));
  }

  public void put(final String _key, final Object _val) {
    values_.put(_key, _val);
  }

  public String getTitle() {
    return (String) values_.get("calque.title");
  }

  public void setTitle(final String _title) {
    values_.put("calque.title", _title);
  }

  public Object get(final String _key) {
    return values_.get(_key);
  }

  public boolean isTitleDefined() {
    return values_.containsKey("calque.title");
  }

  public boolean isDefined(final String _key) {
    return values_.containsKey(_key);
  }

  public boolean getBoolean(final String _key) {
    final Object o = values_.get(_key);
    return o == null ? false : CtuluLibString.toBoolean(o.toString());
  }

  public double getDouble(final String _key) {
    final Object o = values_.get(_key);
    return o == null ? 0 : ((Double) o).doubleValue();
  }

  public Double getDoubleObject(final String _key) {
    final Object o = values_.get(_key);
    return o == null ? null : ((Double) o).doubleValue();
  }

  public int getInteger(final String _key) {
    return getInteger(_key, 0);
  }

  public int getInteger(final String _key, final int _defaultValue) {
    final Object o = values_.get(_key);
    return o == null ? _defaultValue : ((Integer) o).intValue();
  }

  public String getString(final String _key) {
    return (String) values_.get(_key);
  }

  public String getLayerName() {
    return name_;
  }

  public void setLayerName(final String _name) {
    name_ = _name;
  }
}