/*
 GPL 2
 */
package org.fudaa.ebli.commun;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import javax.swing.Icon;

/**
 *
 * @author Frederic Deniger
 */
public class EbliComposedIcon implements Icon {

  private Color background;
  private Icon icon;

  public EbliComposedIcon(Color background, Icon icon) {
    this.background = background;
    this.icon = icon;
  }

  public Color getBackground() {
    return background;
  }

  public void setBackground(Color background) {
    this.background = background;
  }

  public Icon getIcon() {
    return icon;
  }

  public void setIcon(Icon icon) {
    this.icon = icon;
  }


  @Override
  public void paintIcon(Component c, Graphics g, int x, int y) {
    g.setColor(background);
    g.fillRect(x, y, icon.getIconWidth(), icon.getIconHeight());
    icon.paintIcon(c, g, x, y);
  }

  @Override
  public int getIconWidth() {
    return icon.getIconWidth();
  }

  @Override
  public int getIconHeight() {
    return icon.getIconHeight();
  }

}
