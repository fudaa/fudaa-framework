/*
 * @creation 2 oct. 2003
 * @modification $Date: 2008-01-17 11:42:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.ebli.commun;

import javax.swing.AbstractButton;
import javax.swing.Action;
import org.fudaa.ctulu.action.ActionsInstaller;

/**
 * Une interface Action au sens Ebli. Cette interface sait creer les boutons correspondants � l'action.
 *
 * @author deniger
 * @version $Id: EbliActionInterface.java,v 1.7.8.1 2008-01-17 11:42:52 bmarchan Exp $
 */
public interface EbliActionInterface extends Action {

  String DEFAULT_TOOLTIP = "DefaultTooltip";
  String UNABLE_TOOLTIP = "UnableTooltip";
  public final static String SECOND_KEYSTROKE = ActionsInstaller.SECOND_KEYSTROKE;

  void updateStateBeforeShow();

  /**
   * @param _f l'usine des JComponent
   * @return un bouton adapte aux toolbars
   */
  AbstractButton buildToolButton(EbliComponentFactory _f);

  void setDefaultToolTip(String _s);

  /**
   * attention: doit etre appele pour faire le menage uniquement !
   */
  void cleanListener();

  /**
   * @param _f l'usine des JComponent
   * @return un bouton pour les panels normaux
   */
  AbstractButton buildButton(EbliComponentFactory _f);

  /**
   * @param _f l'usine des JComponent
   * @return un bouton adapte pour les menus
   */
  AbstractButton buildMenuItem(EbliComponentFactory _f);

  /**
   * @return le tooltip par defaut
   */
  String getDefaultTooltip();

  /**
   * @return condition d'utilisation
   */
  String getEnableCondition();
}