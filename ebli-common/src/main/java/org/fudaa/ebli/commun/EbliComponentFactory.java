/*
 * @creation 2 oct. 2003
 * 
 * @modification $Date: 2006-09-19 14:55:55 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.commun;

import java.awt.Color;
import javax.swing.*;

/**
 * @author deniger
 * @version $Id: EbliComponentFactory.java,v 1.12 2006-09-19 14:55:55 deniger Exp $
 */
public final class EbliComponentFactory {

  public final static EbliComponentFactory INSTANCE = new EbliComponentFactory();

  private Color useSelectedIcon;

  EbliBasicComponentFactory componentFactory = new EbliBasicComponentFactory();

  /**
   *
   */
  private EbliComponentFactory() {
    super();
  }

  public EbliBasicComponentFactory getComponentFactory() {
    return componentFactory;
  }

  public void setComponentFactory(EbliBasicComponentFactory componentFactory) {
    this.componentFactory = componentFactory;
  }

  public Color getUseSelectedIcon() {
    return useSelectedIcon;
  }

  public void setUseSelectedIcon(Color useSelectedIcon) {
    this.useSelectedIcon = useSelectedIcon;
  }

  private void decoreStateButton(final AbstractButton _r, final EbliActionChangeState _a) {
    _r.setAction(_a);
    _r.setSelected(_a.isSelected());
    _a.addPropertyChangeListener(new EbliSelectedChangeListener(_r));
  }

  public void addActionsToMenu(final EbliActionInterface[] _acs, final JMenu _dest) {
    if ((_acs != null) && (_dest != null)) {
      final int n = _acs.length;
      for (int i = 0; i < n; i++) {
        if (_acs[i] == null) {
          _dest.addSeparator();
        } else {
          _acs[i].updateStateBeforeShow();
          _dest.add(_acs[i].buildMenuItem(this));
        }
      }
    }
  }

  public void addActionsToMenu(final EbliActionInterface[] _acs, final JPopupMenu _dest) {
    if ((_acs != null) && (_dest != null)) {
      final int n = _acs.length;
      for (int i = 0; i < n; i++) {
        if (_acs[i] == null) {
          _dest.addSeparator();
        } else {
          _acs[i].updateStateBeforeShow();
          _dest.add(_acs[i].buildMenuItem(this));
        }
      }
    }
  }

  public AbstractButton buildButton(final EbliActionChangeState _a) {
    final AbstractButton r = componentFactory.createToggleButton();
    decoreStateButton(r, _a);
    return r;
  }

  public AbstractButton buildButton(final EbliActionSimple _a) {
    final AbstractButton r = componentFactory.createButton();
    r.setAction(_a);
    return r;
  }

  public JMenuItem buildMenuItem(final EbliActionChangeState _a) {
    final JMenuItem r = componentFactory.createCheckBoxMenuItem();
    decoreStateButton(r, _a);
    return r;
  }

  public JMenuItem buildMenuItem(final EbliActionSimple _a) {
    final JMenuItem r = componentFactory.createMenuItem();
    r.setAction(_a);
    return r;
  }

  public AbstractButton buildToolButton(final EbliActionChangeState _a) {
    final AbstractButton r = componentFactory.createToolToggleButton();
    decoreStateButton(r, _a);
    decoreToolBarButton(r);
    if (useSelectedIcon != null && _a.getIcon() != null) {
      EbliComposedIcon selectedIcon = new EbliComposedIcon(useSelectedIcon, _a.getIcon());
      r.setSelectedIcon(selectedIcon);
    }
    _a.updateTooltip();
    r.setToolTipText((String) _a.getValue(Action.SHORT_DESCRIPTION));
    return r;
  }

  public AbstractButton buildCheckBox(final EbliActionChangeState _a) {
    final AbstractButton r = componentFactory.createCheckBox();
    decoreStateButton(r, _a);
    decoreToolBarButton(r);
    r.setText(_a.getTitle());
    return r;
  }

  public AbstractButton buildToolButton(final EbliActionPaletteAbstract _a) {
    final AbstractButton r = componentFactory.createPaletteButton(_a);
    decoreStateButton(r, _a);
    decoreToolBarButton(r);
    return r;
  }

  public AbstractButton buildToolButton(final EbliActionSimple _a) {
    final AbstractButton r = componentFactory.createToolButton(_a);
    _a.updateTooltip();
    r.setToolTipText(_a.getTooltip());
    decoreToolBarButton(r);
    return r;
  }

  public void decoreToolBarButton(final AbstractButton _r) {
    _r.setText("");
    _r.setRolloverEnabled(true);
    _r.setBorderPainted(false);
  }
}
