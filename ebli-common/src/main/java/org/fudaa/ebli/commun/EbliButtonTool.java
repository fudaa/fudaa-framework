/*
 *  @creation     29 mars 2005
 *  @modification $Date: 2006-09-19 14:55:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;

import com.memoire.bu.BuToolButton;
import java.awt.event.KeyEvent;
import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * @author Fred Deniger
 * @version $Id: EbliButtonTool.java,v 1.6 2006-09-19 14:55:55 deniger Exp $
 */
public class EbliButtonTool extends BuToolButton {

  KeyStroke key_;

  public EbliButtonTool(final EbliActionInterface _action) {
    super(null, null);
    setAction(_action);
    setFocusable(true);
    key_ = (KeyStroke) _action.getValue(Action.ACCELERATOR_KEY);
    if(key_!=null)
    {
      final KeyStroke accelerator =  key_;
      final String acceleratorDelimiter =
      UIManager.getString("MenuItem.acceleratorDelimiter");
      String acceleratorText = "";
      if (accelerator != null) {
          final int modifiers = accelerator.getModifiers();
          if (modifiers > 0) {
              acceleratorText = KeyEvent.getKeyModifiersText(modifiers);
              //acceleratorText += "-";
              acceleratorText += acceleratorDelimiter;
      }

          final int keyCode = accelerator.getKeyCode();
          if (keyCode == 0) {
              acceleratorText += accelerator.getKeyChar();
          } else {
              acceleratorText += KeyEvent.getKeyText(keyCode);
          }
      }
      setToolTipText(super.getToolTipText()+" "+acceleratorText);
    }
  }

  @Override
  protected boolean processKeyBinding(final KeyStroke _ks,final KeyEvent _e,final int _condition,final boolean _pressed){
    if (isEnabled() && key_ != null && key_.equals(_ks)) {
      return SwingUtilities.notifyAction(getAction(), _ks, _e, this, _e.getModifiers());
    }
    return super.processKeyBinding(_ks, _e, _condition, _pressed);
  }

}
