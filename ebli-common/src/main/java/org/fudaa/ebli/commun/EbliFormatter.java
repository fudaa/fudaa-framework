/*
 *  @creation     6 avr. 2005
 *  @modification $Date: 2006-09-19 14:55:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;

import java.text.NumberFormat;
import si.uom.SI;
import javax.measure.Unit;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;

/**
 * @author Fred Deniger
 * @version $Id: EbliFormatter.java,v 1.8 2006-09-19 14:55:55 deniger Exp $
 */
public class EbliFormatter extends CtuluValueEditorDouble implements EbliFormatterInterface {

  private Unit unit;

  public EbliFormatter(Unit unit) {
    this();
    setUnit(unit);
  }

  public EbliFormatter() {
    super();
  }

  @Override
  public CtuluNumberFormatI getXYFormatter() {
    if (formatter_ == null) {
      buildDefaultXYFormatter();
    }
    return formatter_;
  }

  @Override
  public String getUnit() {
    return unit == null ? null : unit.toString();
  }

  public void setFmt(final NumberFormat numberFormat) {
    buildDefaultXYFormatter();
    ((CtuluNumberFormatDefault) formatter_).setFmt(numberFormat);
  }
  
  public NumberFormat getFmt() {
    buildDefaultXYFormatter();
    return ((CtuluNumberFormatDefault) formatter_).getFmt();
  }

  protected void buildDefaultXYFormatter() {
    if (formatter_ == null) {
      formatter_ = new CtuluNumberFormatDefault();
      unit = SI.METRE;
      final NumberFormat fmt = CtuluLib.getDecimalFormat();
      fmt.setMaximumFractionDigits(2);
      ((CtuluNumberFormatDefault) formatter_).setFmt(fmt);
    }

  }
  
  

  /**
   * Definit l'unit� des valeurs a formatter.
   * @param unit L'unit�.
   */
  public void setUnit(final Unit unit) {
    buildDefaultXYFormatter();
    this.unit = unit;
  }
}
