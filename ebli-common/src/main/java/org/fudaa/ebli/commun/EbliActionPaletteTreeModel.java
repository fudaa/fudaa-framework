/*
 *  @file         EbliPaletteInteracAction.java
 *  @creation     13 mai 2004
 *  @modification $Date: 2007-05-04 13:49:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;

import javax.swing.Icon;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

/**
 * Une action qui r�agit au calque selectionne.
 * 
 * @author Fred Deniger
 * @version $Id: EbliActionPaletteTreeModel.java,v 1.3 2007-05-04 13:49:47 deniger Exp $
 */
public abstract class EbliActionPaletteTreeModel extends EbliActionPaletteSpecAbstract implements TreeSelectionListener {

  /**
   * @param _name
   * @param _icon
   * @param _actionName
   * @param _m l'arbre calque
   */
  public EbliActionPaletteTreeModel(final String _name, final Icon _icon, final String _actionName,
      final TreeSelectionModel _m) {
    super(_name, _icon, _actionName);
    if (_m != null) {
      _m.addTreeSelectionListener(this);
      target_ = getTarget(_m);
    }
    setEnabled(isTargetValid(target_));
    setTarget(target_);
  }

  /**
   * Retourne le calque selectionn� dans l'arbre.
   * @param _m Le modele de selection.
   * @return Le calque selectionn�. Normalement de type BCalque ??
   */
  protected Object getTarget(final TreeSelectionModel _m) {
    final TreePath path = _m.getSelectionPath();
    return path == null ? null : path.getLastPathComponent();
  }

  public EbliActionPaletteTreeModel(final String _name, final Icon _icon, final String _actionName) {
    this(_name, _icon, _actionName, null);
  }

  @Override
  public void valueChanged(final TreeSelectionEvent _e) {
    setTarget(getTarget((TreeSelectionModel) _e.getSource()));
  }
}
