/*
 * @file         EbliPreferences.java
 * @creation     2000-02-04
 * @modification $Date: 2006-04-12 15:28:07 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.commun;

import com.memoire.bu.BuPreferences;
/**
 * @version      $Revision: 1.9 $ $Date: 2006-04-12 15:28:07 $ by $Author: deniger $
 * @author       Christophe Delhorbe
 */
public class EbliPreferences extends BuPreferences {
  /**
   * singleton pour les prefs.
   */
  public final static BuPreferences EBLI;
  
  static {
    if (root_==null) {
      EBLI= new EbliPreferences();
    }
    else
      EBLI=BU;
  }

  public final static boolean OUTILS_CALQUES_EN_COLONNE=
    "in_column".equals(
      EbliPreferences.EBLI.getStringProperty("internalframe.tools.position"));
  /**
   * @author AVA
   * @version $Id$
   */
  public static final class DIALOG {
    public final static int EXTERNAL= 1;
    public final static int INTERNAL= 0;
    public final static int TYPE=
      ("yes".equals(EbliPreferences.EBLI.getStringProperty("dialog.external"))
        ? EXTERNAL
        : INTERNAL);
    public final static int AUCUN= 0;
    public final static int SUPPRIMER= 1 << 1;
    public final static int CREER= 1 << 2;
    public final static int EDITER= 1 << 3;
    public final static int VOIR= 1 << 4;
    public final static int IMPORTER= 1 << 5;
    public final static int EXPORTER= 1 << 6;
    public final static int RESET= 1 << 7;
    public final static int VALIDER= 1 << 1;
    public final static int FERMER= 1 << 2;
    public final static int RECULER= 1 << 3;
    public final static int AVANCER= 1 << 4;
    public final static int RECULER_VITE= 1 << 5;
    public final static int AVANCER_VITE= 1 << 6;
    public final static int TERMINER= 1 << 7;
    public final static int ANNULER= 1 << 8;
    public final static int ALIGN_LEFT= 1 << 9;
    public final static int ALIGN_RIGHT= 1 << 10;
    public final static int ALIGN_CENTER= 1 << 11;
  }
}
