/*
 * @creation 9 ao�t 2004
 * @modification $Date: 2006-09-19 14:55:55 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;

import java.awt.event.InputEvent;

/**
 * Permet de gerer les modes de selections selon les touches pressees.
 * @author Fred Deniger
 * @version $Id: EbliSelectionState.java,v 1.5 2006-09-19 14:55:55 deniger Exp $
 */
public class EbliSelectionState {

  /**
   * union des selection.
   */
  public final static int ACTION_ADD = 1;
  /**
   * enlever.
   */
  public final static int ACTION_DEL = 2;

  /**
   * set.
   */
  public final static int ACTION_REPLACE = 0;
  /**
   * special.
   */
  public final static int ACTION_SPECIAL = 4;
  /**
   * Intersection.
   */
  public final static int ACTION_AND= 5;
  /**
   * xor.
   */
  public final static int ACTION_XOR = 3;
  private String controleDesc_;
  private int modificateur_;
  private boolean special_;
  private boolean xor_;

  public EbliSelectionState() {
    super();
  }

  public void majControleDesc(final InputEvent _e){
    if (_e.isShiftDown()) {
      if (xor_ && _e.isControlDown()) {
        controleDesc_ = " (xor)";
        modificateur_ = EbliSelectionState.ACTION_XOR;
      }
      else {
        controleDesc_ = " (+)";
        modificateur_ = EbliSelectionState.ACTION_ADD;
      }
    }
    else if (_e.isControlDown()) {
      if (special_ && (_e.isAltDown())) {
        controleDesc_ = " (special)";
        modificateur_ = EbliSelectionState.ACTION_SPECIAL;
      }
      else {
        controleDesc_ = " (-)";
        modificateur_ = EbliSelectionState.ACTION_DEL;
      }

    }
    else {
      controleDesc_ = null;
      modificateur_ = EbliSelectionState.ACTION_REPLACE;
    }
  }


  public final String getControleDesc(){
    return controleDesc_;
  }

  public final int getModificateur(){
    return modificateur_;
  }

  public final boolean isSpecial(){
    return special_;
  }

  public final boolean isXor(){
    return xor_;
  }

  /**
   * @return true si mode ACTION_REPLACE
   */
  public boolean isReplaceMode(){
    return modificateur_==ACTION_REPLACE;
  }
  /**
   * @return true si mode ACTION_ADD
   */
  public boolean isAddMode(){
    return modificateur_==ACTION_ADD;
  }
  /**
   * @return true si mode ACTION_DEL
   */
  public boolean isDelMode(){
    return modificateur_==ACTION_DEL;
  }
  /**
   * @return true si mode ACTION_SPECIAL
   */
  public boolean isSpecialMode(){
    return modificateur_==ACTION_SPECIAL;
  }
  /**
   * @return true si mode ACTION_XOR
   */
  public boolean isXorMode(){
    return modificateur_==ACTION_XOR;
  }

  public final void setModificateur(final int _modificateur){
    modificateur_ = _modificateur;
  }

  public final void setSpecial(final boolean _special){
    special_ = _special;
  }

  public final void setXor(final boolean _xor){
    xor_ = _xor;
  }

}