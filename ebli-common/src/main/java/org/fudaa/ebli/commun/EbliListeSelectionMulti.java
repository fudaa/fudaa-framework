/**
 * @creation 24 oct. 2003
 * @modification $Date: 2008-02-05 17:09:58 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;

import gnu.trove.TIntObjectHashMap;
import gnu.trove.TIntObjectIterator;
import gnu.trove.TIntObjectProcedure;
import gnu.trove.TIntProcedure;
import org.fudaa.ctulu.CtuluListSelection;
import org.fudaa.ctulu.CtuluListSelectionInterface;

/**
 * Une liste de selection de sommets de g�om�tries contenues par un calque sous forme de map. La map contient des listes de selection de sommets (une
 * liste par g�om�trie du calque). Chaque liste de selection de sommets est associ�e � une cl� qui est l'indice de la g�om�trie s�lectionn�e dans le
 * calque.
 *
 * @author deniger
 * @version $Id$
 */
public class EbliListeSelectionMulti implements EbliListeSelectionMultiInterface {

  protected TIntObjectHashMap idxListSelection_;

  public EbliListeSelectionMulti(final int _n) {
    idxListSelection_ = new TIntObjectHashMap(_n);
  }

  /**
   * Une op�ration (and) d'intersection de la liste
   * <code>this</code> avec une liste donn�e.
   *
   * @param _selection La liste donn�e.
   * @return true : Si la liste <code>this</code> est modifi�e par l'op�ration.
   */
  public boolean intersection(final EbliListeSelectionMultiInterface _selection) {
    if ((_selection == null) || _selection.isEmpty()) {
      final boolean r = !isEmpty();
      clear();
      return r;
    }
    boolean r = false;
    final TIntObjectIterator it = idxListSelection_.iterator();
    for (int i = idxListSelection_.size(); i-- > 0;) {
      it.advance();
      final int idx = it.key();
      final CtuluListSelection thisSelection = (CtuluListSelection) it.value();
      final CtuluListSelection selection = (CtuluListSelection) _selection.getSelection(idx);
      r |= thisSelection.intersection(selection);
      if (thisSelection.isEmpty()) {
        it.remove();
      }
    }
    return r;

  }

  /**
   *
   * @param _selection
   * @return true si intersection.
   */
  public boolean intersects(final EbliListeSelectionMultiInterface _selection) {
    if ((_selection == null) || _selection.isEmpty()) {
      return false;
    }
    final TIntObjectIterator it = idxListSelection_.iterator();
    for (int i = idxListSelection_.size(); i-- > 0;) {
      it.advance();
      final int idx = it.key();
      final CtuluListSelection thisSelection = (CtuluListSelection) it.value();
      final CtuluListSelection selection = (CtuluListSelection) _selection.getSelection(idx);
      if (thisSelection != null && thisSelection.intersects(selection)) {
        return true;
      }
    }
    return false;

  }

  public void clear() {
    idxListSelection_.clear();
  }

  public void set(final int _i, final CtuluListSelection _l) {
    idxListSelection_.put(_i, _l);
  }

  public void forEachKey(final TIntProcedure _p) {
    idxListSelection_.forEachKey(_p);
  }

  public void forEachEntry(final TIntObjectProcedure _p) {
    idxListSelection_.forEachEntry(_p);
  }

  /**
   * @todo Pas sur que cette implementation donne le r�sultat attendu dans l'inteface !!!
   */
  @Override
  public int isSelectionInOneBloc() {
    return (idxListSelection_.size() == 1 ? idxListSelection_.keys()[0] : -1);
  }

  @Override
  public int[] getIdxSelected() {
    return idxListSelection_.keys();
  }

  @Override
  public CtuluListSelection getIdxSelection() {
    return new CtuluListSelection(getIdxSelected());
  }

  public CtuluListSelection get(final int _idxSelected) {
    return (CtuluListSelection) idxListSelection_.get(_idxSelected);
  }

  @Override
  public CtuluListSelectionInterface getSelection(final int _idxSelected) {
    return (CtuluListSelection) idxListSelection_.get(_idxSelected);
  }

  @Override
  public boolean isEmpty() {
    if (idxListSelection_.isEmpty()) {
      return true;
    }
    final TIntObjectIterator it = idxListSelection_.iterator();
    for (int i = idxListSelection_.size(); i-- > 0;) {
      it.advance();
      if (!(((CtuluListSelection) it.value()).isEmpty())) {
        return false;
      }
    }
    return true;
  }

  /**
   * Une op�ration (xor) d'ajout si non existant et suppression si existant sur la liste
   * <code>this</code> depuis une liste donn�e.
   *
   * @param _selection La liste donn�e.
   * @return true : Si la liste <code>this</code> est modifi�e par l'op�ration.
   */
  public boolean xor(final EbliListeSelectionMultiInterface _m) {
    if ((_m == null) || _m.isEmpty()) {
      return false;
    }
    boolean r = false;
    final TIntObjectIterator itxor = _m.getIterator();
    for (int i = _m.getNbListSelected(); i-- > 0;) {
      itxor.advance();
      final int idx = itxor.key();
      final CtuluListSelection e = (CtuluListSelection) itxor.value();
      if (!e.isEmpty()) {
        CtuluListSelection thisSelection = get(idx);
        if (thisSelection == null) {
          thisSelection = new CtuluListSelection(e);
          idxListSelection_.put(idx, thisSelection);
          r = true;
        } else {
          if (thisSelection.xor((CtuluListSelection) itxor.value())) {
            r = true;
            if (thisSelection.isEmpty()) {
              idxListSelection_.remove(idx);
            }
          }
        }
      }
    }
    return r;
  }

  /**
   * Une op�ration (or) d'ajout de la liste donn�e � la liste
   * <code>this</code>.
   *
   * @param _m La liste donn�e.
   * @return true : Si la liste <code>this</code> est modifi�e par l'op�ration.
   */
  public boolean add(final EbliListeSelectionMultiInterface _m) {
    if ((_m == null) || _m.isEmpty()) {
      return false;
    }
    boolean r = false;
    final TIntObjectIterator it = _m.getIterator();
    for (int i = _m.getNbListSelected(); i-- > 0;) {
      it.advance();
      final int idx = it.key();
      final CtuluListSelection e = (CtuluListSelection) it.value();
      if (!e.isEmpty()) {
        CtuluListSelection thisSelection = get(idx);
        if (thisSelection == null) {
          thisSelection = new CtuluListSelection(e);
          idxListSelection_.put(idx, thisSelection);
          r = true;
        } else {
          r |= thisSelection.add(e);
        }
      }
    }
    return r;
  }

  /**
   * Return the total number of selected index.
   */
  @Override
  public int getNbSelectedItem() {
    int r = 0;
    final TIntObjectIterator it = idxListSelection_.iterator();
    for (int i = idxListSelection_.size(); i-- > 0;) {
      it.advance();
      r += ((CtuluListSelection) it.value()).getNbSelectedIndex();
    }
    return r;
  }

  /**
   * Une op�ration de suppression de la liste donn�e � la liste
   * <code>this</code>.
   *
   * @param _m La liste donn�e.
   * @return true : Si la liste <code>this</code> est modifi�e par l'op�ration.
   */
  public boolean remove(final EbliListeSelectionMultiInterface _m) {
    if ((_m == null) || _m.isEmpty()) {
      return false;
    }
    boolean r = false;
    final TIntObjectIterator it = _m.getIterator();
    for (int i = _m.getNbListSelected(); i-- > 0;) {
      it.advance();
      final int idx = it.key();
      final CtuluListSelection thisSelection = get(idx);
      if (thisSelection != null && thisSelection.remove((CtuluListSelection) it.value())) {
        r = true;
        if (thisSelection.isEmpty()) {
          idxListSelection_.remove(idx);
        }

      }
    }
    return r;
  }

  /**
   * Une op�ration de remplacement de la liste
   * <code>this</code> par la liste donn�e.
   *
   * @param _m La liste donn�e.
   * @return true dans tous les cas.
   */
  public boolean setSelection(final EbliListeSelectionMultiInterface _m) {
    clear();
    if (_m == null) {
      return true;
    }
    final TIntObjectIterator it = _m.getIterator();
    for (int i = _m.getNbListSelected(); i-- > 0;) {
      it.advance();
      final CtuluListSelection newS = new CtuluListSelection((CtuluListSelection) it.value());
      if (!newS.isEmpty()) {
        idxListSelection_.put(it.key(), newS);
      }

    }
    return true;
  }

  @Override
  public int getNbListSelected() {
    return idxListSelection_.size();
  }

  @Override
  public TIntObjectIterator getIterator() {
    return idxListSelection_.iterator();
  }

  /**
   * Efface la liste avant de definir l'unique sommet.
   *
   * @param _idxGlobal L'indice global
   * @param _idxSelected L'indice de sommet.
   */
  public void set(final int _idxGlobal, final int _idxSelected) {
    idxListSelection_.clear();
    final CtuluListSelection l = new CtuluListSelection();
    l.setSelectionInterval(_idxSelected, _idxSelected);
    idxListSelection_.put(_idxGlobal, l);
  }

  /**
   * Ajoute un sommet a la liste.
   *
   * @param _idxGlobal L'indice global
   * @param _idxSelected L'indice de sommet.
   */
  public void add(final int _idxGlobal, final int _idxSelected) {
    CtuluListSelection l = (CtuluListSelection) idxListSelection_.get(_idxGlobal);
    if (l == null) {
      l = new CtuluListSelection();
      idxListSelection_.put(_idxGlobal, l);
    }
    l.add(_idxSelected);
  }
}
