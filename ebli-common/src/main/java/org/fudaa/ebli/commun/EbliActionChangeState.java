/*
 *  @creation     2 oct. 2003
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.ebli.commun;

import java.awt.event.ActionEvent;
import javax.swing.AbstractButton;
import javax.swing.Icon;

/**
 * @author deniger
 * @version $Id$
 */
public abstract class EbliActionChangeState extends EbliActionAbstract {

  private boolean selected_;

  public EbliActionChangeState(final String _name, final Icon _ic, final String _action) {
    super(_name, _ic, _action);

  }

  /**
   * Change the selection state and call the method changeSelectedState.
   */
  @Override
  public final void actionPerformed(final ActionEvent _ae) {
    changeAll();
  }

  public void changeAll() {
    setSelected(!isSelected());
    changeAction();
  }

  /**
   * Change only the state of the component : do not call the methode changeSelectedState.
   */
  public void setSelected(final boolean _b) {
    if (_b != selected_) {
      selected_ = _b;
      firePropertyChange("selected", selected_ ? Boolean.FALSE : Boolean.TRUE, selected_ ? Boolean.TRUE : Boolean.FALSE);
    }
  }

  /**
   * Appel� quand l'�tat s�lectionn� ou non de l'action est modifi�
   */
  public abstract void changeAction();

  public boolean isSelected() {
    return selected_;
  }

  @Override
  public final AbstractButton buildButton(final EbliComponentFactory _f) {
    return _f.buildButton(this);
  }
  
  public final AbstractButton buildCheckBox(final EbliComponentFactory _f) {
    return _f.buildCheckBox(this);
  }

  @Override
  public final AbstractButton buildMenuItem(final EbliComponentFactory _f) {
    return _f.buildMenuItem(this);
  }

  @Override
  public AbstractButton buildToolButton(final EbliComponentFactory _f) {
    return _f.buildToolButton(this);
  }
}