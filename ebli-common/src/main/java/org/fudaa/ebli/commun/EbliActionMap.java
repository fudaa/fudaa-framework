/*
 * @creation     17 mars 08
 * @modification $Date: 2008-03-18 15:26:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ebli.commun;

import com.memoire.fu.FuLog;
import java.util.HashMap;
import javax.swing.Action;

/**
 * Classe singleton detenant les actions de l'application. Cette classe est un point d'entree a toutes les
 * actions.
 * 
 * @author Bertrand Marchand
 * @version $Id: EbliActionMap.java,v 1.1.2.1 2008-03-18 15:26:05 bmarchan Exp $
 */
public class EbliActionMap extends HashMap {
  private EbliActionMap() {}
  private static EbliActionMap map_=new EbliActionMap();
  
  public static EbliActionMap getInstance() {
    return map_;
  }
  
  /**
   * Retourne l'action par son nom (ActionCommand).
   * @param _actionName Le nom de l'action.
   * @return L'action, ou null si inconnue.
   */
  public EbliActionAbstract getAction(String _actionName) {
    return (EbliActionAbstract)map_.get(_actionName);
  }
  
  /**
   * Attention:que faire pour des applications avec des multiples fenetres- La map est statique !
   * Ajoute une action.
   */
  public void addAction(EbliActionAbstract _action) {
   if (!map_.containsKey(_action.getValue(Action.ACTION_COMMAND_KEY))) {
     map_.put(_action.getValue(Action.ACTION_COMMAND_KEY), _action);
   }
   else {
     // Erreur destin�e au programmeur => Pas de traduction
     FuLog.error("Command "+_action.getValue(Action.ACTION_COMMAND_KEY)+" is already registered");
   }
  }
  
  /**
   * Supprime l'action.
   */
  public void removeAction(EbliActionAbstract _action) {
    map_.remove(_action.getValue(Action.ACTION_COMMAND_KEY));
  }
  
  /**
   * Supprime l'action.
   */
  public void removeAction(String _actionName) {
    map_.remove(_actionName);
  }
}
