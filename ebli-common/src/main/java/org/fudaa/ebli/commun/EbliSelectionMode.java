/*
 * @creation 17 janv. 07
 * @modification $Date: 2007-01-19 13:09:50 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;

/**
 * @author fred deniger
 * @version $Id: EbliSelectionMode.java,v 1.1 2007-01-19 13:09:50 deniger Exp $
 */
public final class EbliSelectionMode {
  
  /**
   * Tous les points d'un objet doivent etre selectionnés.
   */
  public static final int MODE_ALL = 0;
  /**
   * Au moins une points d'un objet doit etre selectionné.
   */
  public static final int MODE_ONE = 1;
  /**
   * Le centre de l'objet doit etre selectionne.
   */
  public static final int MODE_CENTER = 2;

  private EbliSelectionMode() {}

}
