/*
 *  @creation     16 sept. 2005
 *  @modification $Date: 2006-10-26 07:33:06 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;

import com.memoire.bu.BuTable;


/**
 * @author Fred Deniger
 * @version $Id: EbliTableInfoTarget.java,v 1.1 2006-10-26 07:33:06 deniger Exp $
 */
public interface EbliTableInfoTarget {

  /**
   * @return le titre du calque
   */
  String getTitle();
  /**
   * @return la table afficher
   */
  BuTable createValuesTable();
  /**
   * @return les lignes selectionnees
   */
  int[] getSelectedObjectInTable();
  /**
   * @return true si les donn�es sont disponibles
   */
  boolean isValuesTableAvailable();

}
