/**
 * @creation 19 oct. 2004
 * @modification $Date: 2007-05-04 13:49:47 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.commun;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.*;
import org.fudaa.ctulu.table.CtuluTableCellDoubleValue;
import org.fudaa.ctulu.table.CtuluTableColumnHeader;
import org.fudaa.ctulu.table.CtuluTableModelInterface;
import org.fudaa.ctulu.table.CtuluTableSortModel;
import si.uom.SI;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Arrays;

/**
 * @author Fred Deniger
 * @version $Id: EbliTableInfoPanel.java,v 1.3 2007-05-04 13:49:47 deniger Exp $
 */
public class EbliTableInfoPanel extends BuPanel implements ActionListener, ItemListener {

  /**
   * Le modele de table utilise pour afficher les valeurs des indices selectionné uniquement.
   *
   * @author Fred Deniger
   * @version $Id: EbliTableInfoPanel.java,v 1.3 2007-05-04 13:49:47 deniger Exp $
   */
  private class SelectedTableModel extends AbstractTableModel implements TableModelListener {

    protected SelectedTableModel() {
      tableModel.addTableModelListener(this);
    }

    @Override
    public Class getColumnClass(final int columnIndex) {
      return tableModel.getColumnClass(columnIndex);
    }

    @Override
    public int getColumnCount() {
      return tableModel.getColumnCount();
    }

    @Override
    public String getColumnName(final int columnIndex) {
      return tableModel.getColumnName(columnIndex);
    }

    @Override
    public int getRowCount() {
      return selectedLine.length;
    }

    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
      return tableModel.getValueAt(selectedLine[rowIndex], columnIndex);
    }

    @Override
    public boolean isCellEditable(final int rowIndex, final int columnIndex) {
      return tableModel.isCellEditable(rowIndex, columnIndex);
    }

    @Override
    public void removeTableModelListener(final TableModelListener listener) {
      tableModel.removeTableModelListener(listener);
    }

    @Override
    public void tableChanged(final TableModelEvent tableModelEvent) {
      if (tableSortModel != null) {
        tableSortModel.fireTableChanged(tableModelEvent);
      }

    }
  }

  public static Component containsComponent(final BuTable buTable) {
    return (Component) buTable.getClientProperty("TABLE_COMPONENT");
  }

  public static String containsTitle(final BuTable buTable) {
    return (String) buTable.getClientProperty("TABLE_TITLE");
  }

  public static boolean showSelectedRows(final BuTable buTable) {
    final Boolean res = (Boolean) buTable.getClientProperty("TABLE_SHOW_SELECTED_ROW");
    return res == null ? true : res;
  }

  /**
   * Enregistre le titre dans les propriete (putClientProperty) de la table.
   *
   * @param buTable le composant a modifier
   * @param component le titre quie sera utilise dans ce panneau
   */
  public static void setComponent(final BuTable buTable, final Component component) {
    buTable.putClientProperty("TABLE_COMPONENT", component);
  }

  /**
   * Enregistre le titre dans les propriete (putClientProperty) de la table.
   *
   * @param buTable le composant a modifier
   * @param title le titre quie sera utilise dans ce panneau
   */
  public static void setTitle(final BuTable buTable, final String title) {
    buTable.putClientProperty("TABLE_TITLE", title);
  }

  public static void setJMenuBarComponents(final BuTable buTable, final JComponent[] jComponents) {
    buTable.putClientProperty("TABLE_JMENUBAR_COMP", jComponents);
  }

  public static JComponent[] getJMenuBarComponents(final BuTable table) {
    return (JComponent[]) table.getClientProperty("TABLE_JMENUBAR_COMP");
  }

  public static void setShowSelectedRow(final BuTable table, final boolean show) {
    table.putClientProperty("TABLE_SHOW_SELECTED_ROW", Boolean.valueOf(show));
  }

  private final TableColumnModel tableColumnModel;
  private JDialog dialog;
  private boolean displaySelected;
  private TableModel tableModel;
  private EbliTableInfoTarget target;
  private int[] selectedLine;
  private CtuluTableSortModel tableSortModel;
  private BuTable table;
  private final CtuluUI ctuluUI;

  /**
   * Un constructeur, avec les formats pour les colonnes X et Y et Z. Le format de Z est aussi utilisé pour les autres colonnes Double.
   *
   * @param _defs Les definitions de coordonnées.
   * @param _layer Le calque cible.
   */
  public EbliTableInfoPanel(final CtuluUI _ui, final EbliTableInfoTarget _layer, EbliCoordinateDefinition[] _defs) {
    super();

    if (_defs == null) {
      _defs = new EbliCoordinateDefinition[]{
        new EbliCoordinateDefinition("X", new EbliFormatter(SI.METRE)),
        new EbliCoordinateDefinition("Y", new EbliFormatter(SI.METRE)),
        new EbliCoordinateDefinition("Z", new EbliFormatter(SI.METRE))
      };
    }

    target = _layer;
    ctuluUI = _ui;
    tableSortModel = new CtuluTableSortModel();
    setLayout(new BuBorderLayout(5, 5, true, true));
    table = target.createValuesTable();

    if (table == null) {
      setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEtchedBorder(), BuBorders.EMPTY5555));
      add(new JLabel(CtuluLib.getS("Pas de valeurs disponibles"), UIManager.getIcon("OptionPane.warningIcon"),
              SwingConstants.CENTER));
      tableColumnModel = null;
    } else {
      tableColumnModel = new DefaultTableColumnModel();
      final TableColumnModel init = table.getColumnModel();
      final int nb = init.getColumnCount();
      // TODO a continuer
      EbliDateCellDoubleValueCellRenderer doubleValueRenderer = new EbliDateCellDoubleValueCellRenderer();
      CtuluCellTextRenderer textRenderer = new CtuluCellTextRenderer();
      for (int i = 0; i < nb; i++) {
        TableColumn column = init.getColumn(i);
        int modelIdx = column.getModelIndex();
        final Class<?> columnClass = table.getModel().getColumnClass(modelIdx);
        if (columnClass.equals(Double.class)) {
          CtuluCellTextDecimalRenderer renderer = new CtuluCellTextDecimalRenderer();
          if (_defs[0].getName().equalsIgnoreCase(table.getModel().getColumnName(modelIdx))) {
            renderer.setFormatter(_defs[0].getFormatter().getXYFormatter());
          } else if (_defs[1].getName().equalsIgnoreCase(table.getModel().getColumnName(modelIdx))) {
            renderer.setFormatter(_defs[1].getFormatter().getXYFormatter());
          } else {
            renderer.setFormatter(_defs[2].getFormatter().getXYFormatter());
          }
          column.setCellRenderer(renderer);
        } else if (columnClass.equals(CtuluTableCellDoubleValue.class)) {
          column.setCellRenderer(doubleValueRenderer);
        } else if (columnClass.equals(String.class)) {
          column.setCellRenderer(textRenderer);
        }
        tableColumnModel.addColumn(column);
      }
      tableModel = table.getModel();
      tableSortModel.setModel(tableModel);
      tableSortModel.setSortOnUpdate(true);
      table.setModel(tableSortModel);
      table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
      table.getActionMap().remove("copy");
      String t = (String) table.getClientProperty("TABLE_TITLE");
      if (t == null) {
        t = target.getTitle();
      }
      final BuLabel lb = new BuLabel(t);
      lb.setHorizontalAlignment(SwingConstants.CENTER);
      final Component c = containsComponent(table);
      if (c == null) {
        add(lb, BuBorderLayout.NORTH);
      } else {
        final BuPanel pnNorth = new BuPanel(new BuBorderLayout(5, 5, false, false));
        pnNorth.add(lb, BuBorderLayout.NORTH);
        pnNorth.add(c);
        add(pnNorth, BuBorderLayout.NORTH);
      }
      selectedLine = target.getSelectedObjectInTable();
      table.setColumnSelectionAllowed(true);
      if (selectedLine != null) {
        Arrays.sort(selectedLine);
        tableSortModel.setModel(new SelectedTableModel());
        table.setModel(tableSortModel);
        displaySelected = true;
      }
      add(new BuScrollPane(table), BuBorderLayout.CENTER);
      // l'ordre de ces 2 lignes est important
      CtuluTableColumnHeader header = new CtuluTableColumnHeader(tableColumnModel);
      table.setTableHeader(header);

      tableSortModel.install(table);
      table.setColumnModel(tableColumnModel);
      table.setCellSelectionEnabled(true);
      for (int i = 0; i < tableColumnModel.getColumnCount(); i++) {
        header.adjustWidth(tableColumnModel.getColumn(i));
      }
    }

  }

  protected void changeSelectedIndexDisplayed() {
    displaySelected = !displaySelected;
    if (displaySelected) {
      tableSortModel.setModel(new SelectedTableModel());
    } else {
      final int idx = tableSortModel.getSortingColumn();
      tableSortModel.setModel(tableModel);
      if (idx >= 0) {
        tableSortModel.setSortingColumn(idx);
      }
    }
    tableSortModel.fireTableDataChanged();
    table.setColumnModel(tableColumnModel);
  }

  protected void copy() {
    table.copy();
  }

  public void showInDialog() {
    final Frame f = CtuluLibSwing.getFrameAncestorHelper(ctuluUI.getParentComponent());
    dialog = new JDialog(f);

    dialog.setResizable(true);
    dialog.setContentPane(this);
    if (table != null) {
      final BuMenuBar b = new BuMenuBar();
      final BuMenu menu = new BuMenu(EbliLib.getS("Export"), "EXPORT");
      menu.setIcon(BuResource.BU.getMenuIcon("exporter"));
      final BuMenuItem it = menu.addMenuItem(EbliLib.getS("Exporter table (csv, excel)"), "EXPORT_CSV");
      it.addActionListener(this);
      it.setIcon(BuResource.BU.getIcon("exporter"));
      menu.addMenuItem(BuResource.BU.getString("Copier"), "COPIER", BuResource.BU.getIcon("copier"), true,
              KeyEvent.VK_C, this);
      b.add(menu);
      if (showSelectedRows(table)) {
        final BuCheckBox bc = new BuCheckBox();
        bc.setText(EbliLib.getS("Afficher les objets sélectionnés uniquement"));
        bc.setActionCommand("CHANGE_SELECT");
        bc.setEnabled(selectedLine != null);
        bc.setSelected(selectedLine != null);
        bc.addItemListener(this);
        b.add(bc);
      }
      final JComponent[] otherTool = getJMenuBarComponents(table);
      if (otherTool != null) {
        for (int i = 0; i < otherTool.length; i++) {
          b.add(otherTool[i]);
        }
      }

      dialog.setJMenuBar(b);
    }

    dialog.setModal(true);
    dialog.pack();
    dialog.setTitle(target.getTitle());
    dialog.setLocationRelativeTo(CtuluLibSwing.getFrameAncestor(ctuluUI.getParentComponent()));
    CtuluDialogPreferences.loadComponentLocationAndDimension(dialog, EbliPreferences.EBLI, "graphe.table");
    CtuluDialogPreferences.ensureComponentWillBeVisible(dialog, dialog.getLocation());
    dialog.setVisible(true);
    dialog.dispose();
    CtuluDialogPreferences.saveComponentLocationAndDimension(dialog, EbliPreferences.EBLI, "graphe.table");
    EbliPreferences.EBLI.writeIniFile();
    dialog = null;
  }
  private ExportTableCommentSupplier exportTableCommentSupplier;

  public void setExportTableCommentSupplier(ExportTableCommentSupplier exportTableCommentSupplier) {
    this.exportTableCommentSupplier = exportTableCommentSupplier;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if ("COPIER".equals(_e.getActionCommand())) {
      copy();
    } else if ("EXPORT_CSV".equals(_e.getActionCommand())) {
      exportTable();
    }
  }
  
  /**
   * exporte les valeurs de la table vers un fichier Excel ou Csv.
   */
  public void exportTable() {
    table.putClientProperty(CtuluTableModelInterface.EXPORT_COMMENT_PROPERTY, null);
    try {
      if (exportTableCommentSupplier != null) {
        table.putClientProperty(CtuluTableModelInterface.EXPORT_COMMENT_PROPERTY, exportTableCommentSupplier.getComments());
      }
    } catch (NullPointerException ex) {
      FuLog.error(ex);
    }
    CtuluTableExportPanel.doExport(';', table, ctuluUI);
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    changeSelectedIndexDisplayed();
  }
}
