/*
 * @creation     1999-02-09
 * @modification $Date: 2007-05-04 13:49:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.repere;

import com.memoire.bu.BuBorders;
import com.memoire.bu.BuLightBorder;
import com.memoire.bu.BuResource;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluPopupMenu;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrVecteur;

/**
 * Un controle de repere.
 * 
 * @version $Id: ZTransformationDomaine.java,v 1.9 2007-05-04 13:49:45 deniger Exp $
 * @author Guillaume Desnoix
 */
public class ZTransformationDomaine extends JComponent implements MouseListener, MouseMotionListener,
    PropertyChangeListener, ActionListener, MouseWheelListener {
  // Proprietes
  private AbstractVueCalque vueCalque_;

  Cursor defaultCursor_ = new Cursor(Cursor.DEFAULT_CURSOR);

  BufferedImage im_;

  GrBoite initBoite_;

  double initXEcran_;

  double initYEcran_;

  boolean isOut_;

  final Cursor moveCursor_ = new Cursor(Cursor.MOVE_CURSOR);

  boolean rapideIsDone_;

  GrBoite totale_;

  GrVecteur v_;

  GrPolygone zoneEcran_;

  public ZTransformationDomaine() {
    super();
    setBorder(new CompoundBorder(new BuLightBorder(BuLightBorder.LOWERED, 1), new EmptyBorder(1, 1, 1, 1)));
    // setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
    final Dimension d = new Dimension(120, 120);
    setBorder(BuBorders.EMPTY3333);
    setSize(d);
    setMinimumSize(d);
    setPreferredSize(d);
    addMouseListener(this);
    addMouseMotionListener(this);
    addMouseWheelListener(this);
    setFocusable(true);
    setEnabled(true);
    // setCursor(moveCursor_);
  }

  public ZTransformationDomaine(final AbstractVueCalque _vue) {
    this();
    setVueCalque(_vue);
  }

  private void endRapide(final boolean _cursor) {
    if (rapideIsDone_) {
      rapideIsDone_ = false;
    }
    if (_cursor) {
      updateCursor(defaultCursor_);
    }
  }

  public static int getHPaint(final Dimension _size, final Insets _insets) {
    return _size.height - _insets.top - _insets.bottom;
  }

  private static double getOffsetX(final GrBoite _total, final int _w, final double _e) {
    return (-_total.getDeltaX() * _e + _w) / 2.;
  }

  private static double getOffsetY(final GrBoite _total, final int _h, final double _e) {
    return (-_total.getDeltaY() * _e + _h) / 2.;
  }

  private GrMorphisme getVersEcran() {
    if (totale_ == null) {
      totale_ = getReelDomaine();
    }
    return getVersEcran(totale_, getSize(), getInsets());
  }

  public static GrMorphisme getVersEcran(final GrBoite _domaine, final Dimension _targetDim, final Insets _targetInset) {
    final int w = getWPaint(_targetDim, _targetInset);
    final int h = getHPaint(_targetDim, _targetInset);
    final double e = getDilatation(_domaine, _targetDim, _targetInset);
    final double tx = getOffsetX(_domaine, w, e) + _targetInset.left;
    final double ty = getOffsetY(_domaine, h, e) + _targetInset.top;
    final GrMorphisme m = GrMorphisme.identite();
    m.composition(GrMorphisme.translation(-_domaine.o_.x_, -_domaine.o_.y_, 0.));
    m.composition(GrMorphisme.dilatation(e, e, 0.));
    m.composition(GrMorphisme.rotation(Math.PI, 0., 0.));
    m.composition(GrMorphisme.translation(0., h, 0.));
    m.composition(GrMorphisme.translation(tx, -ty, 0.));
    return m;

  }

  private GrMorphisme getVersReel() {
    if (totale_ == null) {
      totale_ = getReelDomaine();
    }
    return getVersReel(totale_, getSize(), getInsets());
  }

  public static GrMorphisme getVersReel(final GrBoite _b, final Dimension _dim, final Insets _insets) {
    final int w = getWPaint(_dim, _insets);
    final int h = getHPaint(_dim, _insets);
    final double e = getDilatation(_b, _dim, _insets);
    final double tx = getOffsetX(_b, w, e) + _insets.left;
    final double ty = getOffsetY(_b, h, e) + _insets.top;
    final GrMorphisme m = GrMorphisme.identite();
    m.composition(GrMorphisme.translation(-tx, ty, 0.));
    m.composition(GrMorphisme.translation(0., -h, 0.));
    m.composition(GrMorphisme.rotation(-Math.PI, 0., 0.));
    m.composition(GrMorphisme.dilatation(1 / e, 1 / e, 0.));
    m.composition(GrMorphisme.translation(_b.o_.x_, _b.o_.y_, 0.));
    return m;
  }

  public static int getWPaint(final Dimension _size, final Insets _insets) {
    return _size.width - _insets.left - _insets.right;
  }

  private boolean isInVue(final MouseEvent _e) {
    return zoneEcran_ != null
        && (zoneEcran_.contientXY(_e.getX(), _e.getY()) || zoneEcran_.distanceXY(_e.getX(), _e.getY()) < 2);
  }

  private void popup(final MouseEvent _evt) {
    final CtuluPopupMenu bp = new CtuluPopupMenu();
    bp.addMenuItem(EbliLib.getS("Rafra�chir"), "REFRESH", true, this).setIcon(BuResource.BU.getToolIcon("rafraichir"));
    bp.show(this, _evt.getX(), _evt.getY());
  }

  private void updateTooltip() {
    final GrBoite b = vueCalque_.getViewBoite();
    setToolTipText("<html><body><b><u>" + EbliLib.getS("Zone affich�e:") + "</u></b><br>" + EbliLib.getS("Point min:")
        + CtuluLibString.ESPACE + CtuluLib.DEFAULT_NUMBER_FORMAT.format(b.o_.x_) + " ,"
        + CtuluLib.DEFAULT_NUMBER_FORMAT.format(b.o_.y_) + "<br>" + EbliLib.getS("Point max:") + CtuluLibString.ESPACE
        + CtuluLib.DEFAULT_NUMBER_FORMAT.format(b.e_.x_) + " ," + CtuluLib.DEFAULT_NUMBER_FORMAT.format(b.e_.y_));
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if ("REFRESH".equals(_e.getActionCommand())) {
      updateIm();
      repaint();

    }

  }

  public final synchronized void addRepereEventListener(final RepereEventListener _listener) {
    listenerList.add(RepereEventListener.class, _listener);
  }

  @Override
  public void doLayout() {
    updateIm();
    super.doLayout();
  }

  // fireRepereEvent
  public synchronized void fireRepereEvent(final RepereEvent _evt) {
    final Object[] listeners = listenerList.getListenerList();
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == RepereEventListener.class) {
        ((RepereEventListener) listeners[i + 1]).repereModifie(_evt);
      }
    }
  }

  public double getDilatation() {
    if (totale_ == null) {
      totale_ = getReelDomaine();
    }
    return getDilatation(totale_, getSize(), getInsets());
  }

  public static double getDilatation(final GrBoite _domaine, final Dimension _dim, final Insets _insets) {
    final int w = getWPaint(_dim, _insets);
    final int h = getHPaint(_dim, _insets);
    final double deltaX = _domaine.getDeltaX();
    final double deltaY = _domaine.getDeltaY();
    return Math.min(w / deltaX, h / deltaY);
  }

  public Cursor getMoveCursor() {

    return moveCursor_;
  }

  public GrBoite getReelDomaine() {
    return vueCalque_.getAbstractCalque().getDomaine();
  }

  public GrPolygone getReelZoneDisplayed() {
    return vueCalque_.getViewPolygone();
  }

  public GrBoite getReelBoiteDisplayed() {
    return vueCalque_.getViewBoite();
  }

  public AbstractVueCalque getVueCalque() {
    return vueCalque_;
  }

  @Override
  public void mouseClicked(final MouseEvent _evt) {
    if (EbliLib.isPopupMouseEvent(_evt)) {
      popup(_evt);
    }
  }

  @Override
  public void mouseDragged(final MouseEvent _e) {

    if (_e.getModifiers() != InputEvent.BUTTON1_MASK) { return; }
    if (isOut_) { return; }
    if (!rapideIsDone_ && initBoite_ != null) {
      updateCursor(getMoveCursor());
      rapideIsDone_ = true;
    }
    translate(_e.getX(), _e.getY(), true);
    initXEcran_ = _e.getX();
    initYEcran_ = _e.getY();
    _e.consume();
  }

  @Override
  public void mouseEntered(final MouseEvent _evt) {
    isOut_ = false;

  }

  @Override
  public void mouseExited(final MouseEvent _evt) {
    isOut_ = true;
    endRapide(true);
  }

  Cursor oldCursor_;

  @Override
  public void mouseMoved(final MouseEvent _e) {
    updateCursor(isInVue(_e) ? moveCursor_ : defaultCursor_);
  }

  @Override
  public void mousePressed(final MouseEvent _e) {
    requestFocus();
    if (!isInVue(_e)) { return; }
    initBoite_ = getReelBoiteDisplayed();
    initXEcran_ = _e.getX();
    initYEcran_ = _e.getY();

    _e.consume();
  }

  @Override
  public void mouseReleased(final MouseEvent _e) {
    if (EbliLib.isPopupMouseEvent(_e)) {
      popup(_e);
    } else if (vueCalque_ != null && SwingUtilities.isMiddleMouseButton(_e)) {
      final GrPoint p = new GrPoint(_e.getPoint());
      p.autoApplique(getVersReel());
      vueCalque_.zoomOnRealPoint(p.x_, p.y_, _e.isShiftDown());
      return;
    }
    if (_e.getModifiers() != InputEvent.BUTTON1_MASK) { return; }
    if (isOut_) { return; }
    if (initBoite_ == null && _e.getClickCount() == 1) {
      final GrBoite b = getReelBoiteDisplayed();
      final GrPoint pClicked = new GrPoint(_e.getPoint());
      pClicked.autoApplique(getVersReel());
      vueCalque_.translation(pClicked.x_ - b.getMidX(), pClicked.y_ - b.getMidY(), false);
    }
    translate(_e.getX(), _e.getY(), false);
    initBoite_ = null;
    if (rapideIsDone_) {
      vueCalque_.setRapide(false);
    }
    endRapide(!isInVue(_e));
    updateIm();
  }

  @Override
  public void mouseWheelMoved(final MouseWheelEvent _e) {
    if (vueCalque_ != null) {
      vueCalque_.getRepereController().mouseWheelMoved(_e);
    }
  }

  @Override
  public void paintComponent(final Graphics _g) {
    if (totale_ == null) {
      totale_ = getReelDomaine();
    }
    final Color old = _g.getColor();
    final Dimension size = getSize();

    if (im_ != null) {
      ((Graphics2D) _g).drawImage(im_, 0, 0, size.width, size.height, null, null);
    }
    final GrMorphisme versEcran = getVersEcran();
    zoneEcran_ = getReelZoneDisplayed();

    zoneEcran_.autoApplique(versEcran);
    /*
     * zoneEcran_.translate(1, 1); _g.setColor(Color.GRAY);
     */
    final Color zoneColor = Color.BLUE.brighter();
    final Polygon pz = zoneEcran_.polygon();
    _g.setColor(new Color(zoneColor.getRed(), zoneColor.getGreen(), zoneColor.getBlue(), 80));
    _g.fillPolygon(pz);
    _g.setColor(zoneColor);
    _g.drawPolygon(pz);
    _g.setColor(old);
  }

  // Evenements
  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    if ("rapide".equals(_evt.getPropertyName()) || "ajustement".equals(_evt.getPropertyName())
        || "versReel".equals(_evt.getPropertyName())) { return; }
    if ("versEcran".equals(_evt.getPropertyName())) {
      totale_ = null;
      updateTooltip();
    } else {
      updateIm();
    }
    repaint();

  }

  public final synchronized void removeRepereEventListener(final RepereEventListener _listener) {
    listenerList.remove(RepereEventListener.class, _listener);
  }

  public final void setVueCalque(final AbstractVueCalque _v) {
    if (vueCalque_ != _v) {
      final AbstractVueCalque vp = vueCalque_;
      if (vp != null) {
        vp.getAbstractCalque().removePropertyChangeListener(this);
        removeRepereEventListener(vp);
        removeKeyListener(vp.getRepereController());
      }
      vueCalque_ = _v;
      if (vueCalque_ != null) {
        vueCalque_.getAbstractCalque().addPropertyChangeListener(this);
        addRepereEventListener(vueCalque_);
        addKeyListener(vueCalque_.getRepereController());
      }
      firePropertyChange("vueCalque", vp, vueCalque_);
      updateIm();
      updateTooltip();
    }
  }

  private void updateCursor(final Cursor _c) {
    if (_c == oldCursor_) { return; }
    oldCursor_ = _c;
    setCursor(_c);
  }

  public void translate(final double _x, final double _y, final boolean _ajustement) {
    if (initBoite_ == null) { return; }
    if (v_ == null) {
      v_ = new GrVecteur();
    }
    v_.x_ = _x - initXEcran_;
    v_.y_ = _y - initYEcran_;
    v_.autoApplique(getVersReel());
    vueCalque_.translation(v_.x_, v_.y_, _ajustement);
  }

  public void updateIm() {
    final Dimension d = getSize();
    im_ = new BufferedImage(d.width, d.height, BufferedImage.TYPE_INT_RGB);
    final Graphics g = im_.createGraphics();
    g.setColor(vueCalque_.getBackground());
    g.fillRect(0, 0, d.width, d.height);
    vueCalque_.paintAllInImage(g, getVersEcran(), getVersReel());
    g.dispose();
    im_.flush();
  }
}
