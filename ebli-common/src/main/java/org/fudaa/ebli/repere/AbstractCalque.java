/*
 *  @creation     6 janv. 2003
 *  @modification $Date: 2006-06-13 15:46:37 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.repere;

import javax.swing.JComponent;
import org.fudaa.ebli.geometrie.GrBoite;

/**
 * @author deniger
 * @version $Id: AbstractCalque.java,v 1.9 2006-06-13 15:46:37 deniger Exp $
 */
public abstract class AbstractCalque extends JComponent {
  /**
   * Constructor for AbstractCalque.
   */
  public AbstractCalque() {
    super();
  }

  public abstract GrBoite getDomaine();

  
}
