/*
 * @creation     1998-07-15
 * @modification $Date: 2006-09-19 14:55:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.repere;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * Un controle de transformations absolues.
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 14:55:51 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class BTransformationTexte extends JComponent implements ActionListener, RepereEventListener {
  // donnees membres privees
  JTextField[] text_;

  JLabel[] label_;

  NumberFormat nf1_, nf2_;

  double limitePrecision_;

  JCheckBox symBouton_;

  GridLayout layout_;

  List repereEventListeners_;

  double[] valeur_;

  double valeurNeutre_;

  String texteLabel_;

  int indiceTransfo_;

  // Constructeur
  public BTransformationTexte() {
    super();
    layout_ = new GridLayout(4, 2, 2, 2);
    setLayout(layout_);
    typeTransfo_ = RepereEvent.ZOOM;
    valeurNeutre_ = 1.;
    texteLabel_ = " Echelle";
    indiceTransfo_ = 0;
    valeur_ = new double[3];
    text_ = new JTextField[3];
    label_ = new JLabel[3];
    nf1_ = NumberFormat.getInstance(Locale.FRENCH);
    nf1_.setMaximumFractionDigits(2);
    limitePrecision_ = Math.pow(10, -nf1_.getMaximumFractionDigits());
    nf2_ = NumberFormat.getInstance(Locale.FRENCH);
    nf2_.setMaximumFractionDigits(6);
    label_[0] = new JLabel(texteLabel_ + " en X : ", SwingConstants.RIGHT);
    label_[1] = new JLabel(texteLabel_ + " en Y : ", SwingConstants.RIGHT);
    label_[2] = new JLabel(texteLabel_ + " en Z : ", SwingConstants.RIGHT);
    symBouton_ = new JCheckBox();
    add(new JLabel(" Symetrique : ", SwingConstants.RIGHT));
    add(symBouton_);
    symBouton_.addActionListener(this);
    for (int i = 0; i < 3; i++) {
      valeur_[i] = valeurNeutre_;
      text_[i] = new JTextField(nf1_.format(valeur_[i]), 5);
      add(label_[i]);
      add(text_[i]);
      text_[i].addActionListener(this);
    }
    repereEventListeners_ = new ArrayList();
  }

  // **********************************************
  // EVENEMENTS
  // **********************************************
  // Action event
  @Override
  public void actionPerformed(final ActionEvent _e) {
    final Object source = _e.getSource();
    if (source == symBouton_) {
      return;
    }
    final RepereEvent evt = new RepereEvent(this, false);
    double valeur;
    try {
      valeur = Double.valueOf(((JTextField) source).getText()).doubleValue();
    } catch (final NumberFormatException e1) {
      try {
        valeur = nf2_.parse(((JTextField) source).getText()).doubleValue();
      } catch (final ParseException _e2) {
        valeur = valeurNeutre_;
      }
    }
    String text;
    if (valeur < limitePrecision_) {
      text = nf2_.format(valeur);
    } else {
      text = nf1_.format(valeur);
    }
    if (symBouton_.isSelected()) {
      Arrays.fill(valeur_, valeur);
      text_[0].setText(text);
      text_[1].setText(text);
      text_[2].setText(text);
      evt.ajouteTransformation(typeTransfo_, valeur_[0], RepereEvent.ABSOLU);
    } else {
      for (int i = 0; i < 3; i++) {
        if (source == text_[i]) {
          valeur_[i] = valeur;
          text_[i].setText(text);
          evt.ajouteTransformation(typeTransfo_ + i + 1, valeur_[i], RepereEvent.ABSOLU);
        }
      }
    }
    fireRepereEvent(evt);
  }

  // RepereEvent
  @Override
  public void repereModifie(final RepereEvent _evt) {
    String text;
    for (int i = 0; i < 3; i++) {
      if (_evt.aChange(indiceTransfo_, i)) {
        if (typeTransfo_ == RepereEvent.ZOOM) {
          valeur_[i] = (_evt.relatif(indiceTransfo_, i) ? valeur_[i] : valeurNeutre_) * _evt.valeur(indiceTransfo_, i);
        } else {
          valeur_[i] = (_evt.relatif(indiceTransfo_, i) ? valeur_[i] : valeurNeutre_) + _evt.valeur(indiceTransfo_, i);
        }
        if (valeur_[i] < limitePrecision_) {
          text = nf2_.format(valeur_[i]);
        } else {
          text = nf1_.format(valeur_[i]);
        }
        text_[i].setText(text);
      }
    }
  }

  // RepereEvent listeners
  public synchronized void addRepereEventListener(final RepereEventListener _listener) {
    repereEventListeners_.add(_listener);
  }

  public synchronized void removeRepereEventListener(final RepereEventListener _listener) {
    repereEventListeners_.remove(_listener);
  }

  // fireRepereEvent
  public synchronized void fireRepereEvent(final RepereEvent _evt) {
    for (int i = 0; i < repereEventListeners_.size(); i++) {
      ((RepereEventListener) repereEventListeners_.get(i)).repereModifie(_evt);
    }
  }

  // **********************************************
  // PROPRIETES INTERNES
  // **********************************************
  // Propriete typeTransfo
  private int typeTransfo_;

  int getTypeTransfo() {
    return typeTransfo_;
  }

  public void setTypeTransfo(final int _typeTransfo) {
    if (typeTransfo_ != _typeTransfo) {
      switch (_typeTransfo) {
      case RepereEvent.ZOOM:
        valeurNeutre_ = 1.;
        texteLabel_ = " Echelle";
        indiceTransfo_ = 0;
        break;
      case RepereEvent.ROT:
        valeurNeutre_ = 0.;
        texteLabel_ = " Rotation";
        indiceTransfo_ = 2;
        break;
      case RepereEvent.TRANS:
        valeurNeutre_ = 0.;
        texteLabel_ = " Translation";
        indiceTransfo_ = 1;
        break;
      default:
        return;
      }
      final int vp = typeTransfo_;
      typeTransfo_ = _typeTransfo;
      Arrays.fill(valeur_, valeurNeutre_);
      text_[0].setText(nf1_.format(valeur_[0]));
      text_[1].setText(nf1_.format(valeur_[1]));
      text_[2].setText(nf1_.format(valeur_[2]));
      label_[0].setText(texteLabel_ + " en X : ");
      label_[1].setText(texteLabel_ + " en Y : ");
      label_[2].setText(texteLabel_ + " en Z : ");
      // repaint();
      firePropertyChange("TypeTransfo", vp, typeTransfo_);
    }
  }
}
