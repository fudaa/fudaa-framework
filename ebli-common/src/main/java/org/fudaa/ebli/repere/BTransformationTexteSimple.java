/*
 * @creation     2000-04-27
 * @modification $Date: 2007-04-16 16:35:08 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.repere;

import com.memoire.bu.BuCharValidator;
import com.memoire.bu.BuStringValidator;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLib;

/**
 * Un controle de transformations absolues.
 * 
 * @version $Revision: 1.10 $ $Date: 2007-04-16 16:35:08 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class BTransformationTexteSimple extends BuTextField implements ActionListener, RepereEventListener {
  // donnees membres privees
  List repereEventListeners_;
  double valeur_;
  double valeurNeutre_;
  int indiceTransfo_;
  int indiceAxe_;

  // Constructeur
  public BTransformationTexteSimple() {
    super();
    setCharValidator(BuCharValidator.DOUBLE);
    setStringValidator(BuStringValidator.DOUBLE);
    setValueValidator(BuValueValidator.DOUBLE);
    typeTransfo_ = RepereEvent.ZOOM;
    valeurNeutre_ = 1.;
    indiceTransfo_ = 0;
    indiceAxe_ = 0;
    valeur_ = valeurNeutre_;
    setValue(CtuluLib.getDouble(valeur_));
    addActionListener(this);
    repereEventListeners_ = new ArrayList();
  }

  // **********************************************
  // EVENEMENTS
  // **********************************************
  // Action event
  @Override
  public void actionPerformed(final ActionEvent _e) {
    final Object source = _e.getSource();
    if (source != this) {
      return;
    }
    final RepereEvent evt = new RepereEvent(this, false);
    valeur_ = ((Double) getValue()).doubleValue();
    if ((typeTransfo_ == RepereEvent.ROT) || (typeTransfo_ == RepereEvent.ROT_X) || (typeTransfo_ == RepereEvent.ROT_Y)
        || (typeTransfo_ == RepereEvent.ROT_Z)) {
      valeur_ = valeur_ * Math.PI / 180.;
    }
    evt.ajouteTransformation(typeTransfo_, valeur_, RepereEvent.ABSOLU);
    fireRepereEvent(evt);
  }

  // RepereEvent
  @Override
  public void repereModifie(final RepereEvent _evt) {
    if ((typeTransfo_ == RepereEvent.ZOOM)) {
      valeur_ = (_evt.relatif(indiceTransfo_, indiceAxe_) ? valeur_ : valeurNeutre_)
          * _evt.valeur(indiceTransfo_, indiceAxe_);
    } else {
      valeur_ = (_evt.relatif(indiceTransfo_, indiceAxe_) ? valeur_ : valeurNeutre_)
          + _evt.valeur(indiceTransfo_, indiceAxe_);
    }
    double v = 0.;
    if ((typeTransfo_ == RepereEvent.ROT) || (typeTransfo_ == RepereEvent.ROT_X) || (typeTransfo_ == RepereEvent.ROT_Y)
        || (typeTransfo_ == RepereEvent.ROT_Z)) {
      v = valeur_ * 180. / Math.PI;
    } else {
      v = valeur_;
    }
    setValue(CtuluLib.getDouble(v));
  }

  // RepereEvent listeners
  public synchronized void addRepereEventListener(final RepereEventListener _listener) {
    repereEventListeners_.add(_listener);
  }

  public synchronized void removeRepereEventListener(final RepereEventListener _listener) {
    repereEventListeners_.remove(_listener);
  }

  // fireRepereEvent
  public synchronized void fireRepereEvent(final RepereEvent _evt) {
    for (int i = 0; i < repereEventListeners_.size(); i++) {
      ((RepereEventListener) repereEventListeners_.get(i)).repereModifie(_evt);
    }
  }
  // **********************************************
  // PROPRIETES INTERNES
  // **********************************************
  // Propriete typeTransfo
  private int typeTransfo_;

  int getTypeTransfo() {
    return typeTransfo_;
  }

  public void setTypeTransfo(final int _typeTransfo) {
    if (typeTransfo_ != _typeTransfo) {
      switch (_typeTransfo) {
      case RepereEvent.ZOOM:
      case RepereEvent.ZOOM_X:
        valeurNeutre_ = 1.;
        indiceTransfo_ = 0;
        indiceAxe_ = 0;
        break;
      case RepereEvent.ZOOM_Y:
        valeurNeutre_ = 1.;
        indiceTransfo_ = 0;
        indiceAxe_ = 1;
        break;
      case RepereEvent.ZOOM_Z:
        valeurNeutre_ = 1.;
        indiceTransfo_ = 0;
        indiceAxe_ = 2;
        break;
      case RepereEvent.ROT:
      case RepereEvent.ROT_X:
        valeurNeutre_ = 0.;
        indiceTransfo_ = 2;
        indiceAxe_ = 0;
        break;
      case RepereEvent.ROT_Y:
        valeurNeutre_ = 0.;
        indiceTransfo_ = 2;
        indiceAxe_ = 1;
        break;
      case RepereEvent.ROT_Z:
        valeurNeutre_ = 0.;
        indiceTransfo_ = 2;
        indiceAxe_ = 2;
        break;
      case RepereEvent.TRANS:
      case RepereEvent.TRANS_X:
        valeurNeutre_ = 0.;
        indiceTransfo_ = 1;
        indiceAxe_ = 0;
        break;
      case RepereEvent.TRANS_Y:
        valeurNeutre_ = 0.;
        indiceTransfo_ = 1;
        indiceAxe_ = 1;
        break;
      case RepereEvent.TRANS_Z:
        valeurNeutre_ = 0.;
        indiceTransfo_ = 1;
        indiceAxe_ = 2;
        break;
      default:
        return;
      }
      final int vp = typeTransfo_;
      typeTransfo_ = _typeTransfo;
      valeur_ = valeurNeutre_;
      setValue(CtuluLib.getDouble(valeur_));
      firePropertyChange("typeTransfo", vp, typeTransfo_);
    }
  }
}
