/*
 * @creation     1998-10-16
 * @modification $Date: 2006-04-12 15:28:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.repere;
import java.util.EventListener;
/**
 * Interface auditeur de l'evenement "coordonnees".
 *
 * @version      $Revision: 1.6 $ $Date: 2006-04-12 15:28:02 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public interface CoordonneesListener extends EventListener {
  void coordonneesModifiees(CoordonneesEvent _evt);
}
