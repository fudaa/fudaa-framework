/*
 * @creation     1999-02-10
 * @modification $Date: 2006-09-19 14:55:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.repere;

import org.fudaa.ebli.controle.BMolette;

/**
 * Un controle de transformation par molette.
 *
 * @version $Id: BTransformationMolette.java,v 1.7 2006-09-19 14:55:51 deniger Exp $
 * @author Axel von Arnim
 */
public class BTransformationMolette extends BMolette {
  // private Vector repereEventListeners_;
  public BTransformationMolette() {
    super();
    typeTransfo_ = RepereEvent.ZOOM;
    sensibilite_ = 1.1;
    // repereEventListeners_=new Vector();
  }

  // **********************************************
  // EVENEMENTS
  // **********************************************
  // FireStateChanged
  @Override
  protected void fireStateChanged() {
    final boolean adjusting = getValueIsAdjusting();
    final RepereEvent evt = new RepereEvent(this, adjusting);
    if (adjusting) {
      switch (typeTransfo_) {
      case RepereEvent.ZOOM:
      case RepereEvent.ZOOM_X:
      case RepereEvent.ZOOM_Y:
      case RepereEvent.ZOOM_Z:
        evt.ajouteTransformation(typeTransfo_, Math.pow(sensibilite_, (getDirection() == PLUS ? 1 : -1)),
            RepereEvent.RELATIF);
        break;
      default:
        evt.ajouteTransformation(typeTransfo_, sensibilite_ * (getDirection() == PLUS ? 1 : -1), RepereEvent.RELATIF);
        break;
      }
    } else {
      // fake evt pour forcer le repaint des calques (en mettant aChange(0, 0)
      // a true)
      evt.ajouteTransformation(0, 0, 1., RepereEvent.RELATIF);
    }
    fireRepereEvent(evt);
  }

  public synchronized void addRepereEventListener(final RepereEventListener _listener) {
    listenerList.add(RepereEventListener.class, _listener);
  }

  public synchronized void removeRepereEventListener(final RepereEventListener _listener) {
    listenerList.remove(RepereEventListener.class, _listener);
  }

  public synchronized void fireRepereEvent(final RepereEvent _evt) {
    final Object[] listeners = listenerList.getListenerList();
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == RepereEventListener.class) {
        ((RepereEventListener) listeners[i + 1]).repereModifie(_evt);
      }
    }
  }
  // **********************************************
  // PROPRIETES INTERNES
  // **********************************************
  // Propriete sensibilite
  private double sensibilite_;

  double getSensibilite() {
    return sensibilite_;
  }

  public void setSensibilite(final double _sensibilite) {
    if ((sensibilite_ != _sensibilite) && (_sensibilite != 0)) {
      final double vp = sensibilite_;
      sensibilite_ = _sensibilite;
      firePropertyChange("Sensibilite", vp, sensibilite_);
    }
  }
  // Propriete typeTransfo
  private int typeTransfo_;

  int getTypeTransfo() {
    return typeTransfo_;
  }

  public void setTypeTransfo(final int _typeTransfo) {
    if (typeTransfo_ != _typeTransfo) {
      switch (_typeTransfo) {
      case RepereEvent.ZOOM:
      case RepereEvent.ZOOM_X:
      case RepereEvent.ZOOM_Y:
      case RepereEvent.ZOOM_Z:
        sensibilite_ = 1.1;
        break;
      case RepereEvent.ROT:
      case RepereEvent.ROT_X:
      case RepereEvent.ROT_Y:
      case RepereEvent.ROT_Z:
        sensibilite_ = 2 * Math.PI / 100;
        break;
      case RepereEvent.TRANS:
      case RepereEvent.TRANS_X:
      case RepereEvent.TRANS_Y:
      case RepereEvent.TRANS_Z:
        sensibilite_ = 10.;
        break;
      default:
        return;
      }
      final int vp = typeTransfo_;
      typeTransfo_ = _typeTransfo;
      // repaint();
      firePropertyChange("TypeTransfo", vp, typeTransfo_);
    }
  }
}
