/*
 * @creation     1999-02-09
 * @modification $Date: 2006-09-19 14:55:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.repere;

import com.memoire.bu.BuLightBorder;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Polygon;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.geometrie.GrVecteur;

/**
 * Un controle de repere.
 * 
 * @version $Id: BTransformationDomaine.java,v 1.12 2006-09-19 14:55:51 deniger Exp $
 * @author Guillaume Desnoix
 */
public class BTransformationDomaine extends JComponent implements MouseListener, MouseMotionListener,
    PropertyChangeListener {
  public BTransformationDomaine() {
    super();
    setBorder(new CompoundBorder(new BuLightBorder(BuLightBorder.LOWERED, 1), new EmptyBorder(1, 1, 1, 1)));
    setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
    final Dimension d = new Dimension(48, 48);
    setMinimumSize(d);
    setPreferredSize(d);
    setSize(d);
    addMouseListener(this);
    addMouseMotionListener(this);
  }

  GrBoite totale_;

  private GrMorphisme getVersEcran() {
    final Dimension size = getSize();
    final Insets insets = getInsets();
    final int w = size.width - insets.left - insets.right;
    final int h = size.height - insets.top - insets.bottom;
    if (totale_ == null) {
      final GrPolygone domaine = getDomaine();
      if (domaine == null) {
        return GrMorphisme.identite();
      }
      final GrPolygone zone = getZone();
      totale_ = zone.boite().union(domaine.boite());
      // totale=getDomaine().boite();
    }
    final double e = Math.min(w / (totale_.e_.x_ - totale_.o_.x_), h / (totale_.e_.y_ - totale_.o_.y_));
    final double tx = ((totale_.e_.x_ - totale_.o_.x_) * e - w) / 2.;
    final double ty = ((totale_.e_.y_ - totale_.o_.y_) * e - h) / 2.;
    final GrMorphisme m = GrMorphisme.identite();
    m.composition(GrMorphisme.translation(-totale_.o_.x_, -totale_.o_.y_, 0.));
    m.composition(GrMorphisme.dilatation(e, e, 0.));
    m.composition(GrMorphisme.rotation(Math.PI, 0., 0.));
    m.composition(GrMorphisme.translation(0., h, 0.));
    m.composition(GrMorphisme.translation(-tx, ty, 0.));
    m.composition(GrMorphisme.translation(insets.left, insets.top, 0.));
    return m;
  }

  private GrMorphisme getVersReel() {
    final Dimension size = getSize();
    final Insets insets = getInsets();
    final int w = size.width - insets.left - insets.right;
    final int h = size.height - insets.top - insets.bottom;
    if (totale_ == null) {
      final GrPolygone domaine = getDomaine();
      if (domaine == null) {
        return GrMorphisme.identite();
      }
      final GrPolygone zone = getZone();
      totale_ = zone.boite().union(domaine.boite());
    }
    final double e = Math.min(w / (totale_.e_.x_ - totale_.o_.x_), h / (totale_.e_.y_ - totale_.o_.y_));
    final double tx = ((totale_.e_.x_ - totale_.o_.x_) * e - w) / 2.;
    final double ty = ((totale_.e_.y_ - totale_.o_.y_) * e - h) / 2.;
    final GrMorphisme m = GrMorphisme.identite();
    m.composition(GrMorphisme.translation(-insets.left, -insets.top, 0.));
    m.composition(GrMorphisme.translation(tx, -ty, 0.));
    m.composition(GrMorphisme.translation(0., -h, 0.));
    m.composition(GrMorphisme.rotation(-Math.PI, 0., 0.));
    m.composition(GrMorphisme.dilatation(1 / e, 1 / e, 0.));
    m.composition(GrMorphisme.translation(totale_.o_.x_, totale_.o_.y_, 0.));
    return m;
  }

  // paint
  @Override
  public void paint(final Graphics _g) {
    final GrPolygone domaine = getDomaine();
    if (domaine == null) {
      super.paint(_g);
      return;
    }
    final GrMorphisme versEcran = getVersEcran();
    final GrPolygone zone = getZone();
    final Polygon pd = domaine.applique(versEcran).polygon();
    final Polygon pz = zone.applique(versEcran).polygon();
    _g.setColor(new Color(224, 240, 255));
    _g.fillPolygon(pd);
    _g.setColor(new Color(128, 160, 192));
    _g.drawPolygon(pd);
    _g.setColor(Color.white);
    _g.fillPolygon(pz);
    _g.setColor(new Color(255, 192, 192));
    _g.drawPolygon(pz);
    final int xp = (int) getZone().centre().applique(versEcran).x_;
    final int yp = (int) getZone().centre().applique(versEcran).y_;
    _g.setColor(Color.red);
    _g.drawLine(xp - 1, yp - 1, xp + 1, yp + 1);
    _g.drawLine(xp + 1, yp - 1, xp - 1, yp + 1);
    super.paint(_g);
  }

  // Evenements
  @Override
  public void propertyChange(final PropertyChangeEvent _evt) {
    totale_ = null;
    repaint();
  }

  public void translate(final int _x, final int _y, final boolean _ajustement) {
    final GrMorphisme versReel = getVersReel();
    final GrPoint cz = getZone().centre();
    final GrPoint pz = new GrPoint(_x, _y, 0.).applique(versReel);
    // Modification importante:les coordonnees doivent etre des
    // coordonnees ecran
    final GrVecteur v = pz.soustraction(cz);
    v.autoApplique(vueCalque_.getVersEcran());
    final RepereEvent re = new RepereEvent(this, _ajustement);
    re.ajouteTransformation(RepereEvent.TRANS_X, v.x_, RepereEvent.RELATIF);
    re.ajouteTransformation(RepereEvent.TRANS_Y, -v.y_, RepereEvent.RELATIF);
    fireRepereEvent(re);
  }

  @Override
  public void mouseClicked(final MouseEvent _evt) {}

  @Override
  public void mouseEntered(final MouseEvent _evt) {}

  @Override
  public void mouseExited(final MouseEvent _evt) {}

  @Override
  public void mousePressed(final MouseEvent _e) {
    if (_e.getModifiers() != InputEvent.BUTTON1_MASK) {
      return;
    }
    totale_ = null;
  }

  @Override
  public void mouseReleased(final MouseEvent _e) {
    if (_e.getModifiers() != InputEvent.BUTTON1_MASK) {
      return;
    }
    totale_ = null;
    translate(_e.getX(), _e.getY(), false);
  }

  @Override
  public void mouseDragged(final MouseEvent _e) {
    if (_e.getModifiers() != InputEvent.BUTTON1_MASK) {
      return;
    }
    translate(_e.getX(), _e.getY(), true);
  }

  @Override
  public void mouseMoved(final MouseEvent _e) {}

  public synchronized void addRepereEventListener(final RepereEventListener _listener) {
    listenerList.add(RepereEventListener.class, _listener);
  }

  public synchronized void removeRepereEventListener(final RepereEventListener _listener) {
    listenerList.remove(RepereEventListener.class, _listener);
  }

  // fireRepereEvent
  public synchronized void fireRepereEvent(final RepereEvent _evt) {
    final Object[] listeners = listenerList.getListenerList();
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == RepereEventListener.class) {
        ((RepereEventListener) listeners[i + 1]).repereModifie(_evt);
      }
    }
  }

  // Proprietes
  private AbstractVueCalque vueCalque_;

  public AbstractVueCalque getVueCalque() {
    return vueCalque_;
  }

  public void setVueCalque(final AbstractVueCalque _v) {
    if (vueCalque_ != _v) {
      final AbstractVueCalque vp = vueCalque_;
      if (vp != null) {
        vp.getAbstractCalque().removePropertyChangeListener(this);
        removeRepereEventListener(vp);
      }
      vueCalque_ = _v;
      if (vueCalque_ != null) {
        // vueCalque_.getAbstractCalque().addPropertyChangeListener(this);
        vueCalque_.getAbstractCalque().addPropertyChangeListener("versEcran", this);
        addRepereEventListener(vueCalque_);
      }
      firePropertyChange("vueCalque", vp, vueCalque_);
    }
  }

  public GrPolygone getDomaine() {
    final AbstractCalque calque = vueCalque_.getAbstractCalque();
    final GrBoite d = calque.getDomaine();
    if (d == null) {
      return null;
    }
    return d.enPolygoneXY();
  }

  public GrPolygone getZone() {
    final AbstractCalque calque = vueCalque_.getAbstractCalque();
    final int w = calque.getSize().width;
    final int h = calque.getSize().height;
    final GrBoite r = new GrBoite();
    r.ajuste(new GrPoint(0., 0., 0.));
    r.ajuste(new GrPoint(w, h, 0.));
    final GrMorphisme m = vueCalque_.getVersReel();
    return r.enPolygoneXY().applique(m);
  }
}
