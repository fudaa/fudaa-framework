/*
 * @creation     1998-07-21
 * @modification $Date: 2006-09-19 14:55:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.repere;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrVolume;
import org.fudaa.ebli.trace.Trace3D;

/**
 * Un controle de repere.
 *
 * @version $Revision: 1.10 $ $Date: 2006-09-19 14:55:51 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class BTransformationRepere extends JComponent implements MouseListener, MouseMotionListener,
    RepereEventListener {
  // donnees membres privees
  private final List repereEventListeners_;

  private double[][] repere_;



  private GrVolume cube_;

  private boolean volumeAChange_;

  private Image imgBuf_;

  // Constructeur
  public BTransformationRepere() {
    super();
    repereEventListeners_ = new ArrayList();
    repere_ = new double[4][3];
    for (int j = 0; j < 3; j++) {
      // Echelle
      repere_[0][j] = 1.;
    }
    for (int i = 1; i < 4; i++) {
      // Autres
      for (int j = 0; j < 3; j++) {
        repere_[i][j] = 0.;
      }
    }
    imgBuf_ = null;
    cube_ = GrVolume.creeCube().applique(GrMorphisme.dilatation(50.));
    volumeRef_ = cube_;
    volumeAChange_ = true;
    final Dimension tailleDefaut= new Dimension(100, 100);
    setMinimumSize(tailleDefaut);
    setPreferredSize(tailleDefaut);
    setSize(tailleDefaut);
    // volumeRef_=recentre(volumeRef_);
    addMouseListener(this);
    addMouseMotionListener(this);
  }

  // paint
  @Override
  public void paint(final Graphics _g) {
    super.paint(_g);
    final Insets bordure = getInsets();
    if (volumeAChange_) {
      final Dimension fenetre = getSize();
      imgBuf_ = createImage(fenetre.width - bordure.left - bordure.right, fenetre.height - bordure.top - bordure.bottom);
      final Graphics gImg = imgBuf_.getGraphics();
      final Trace3D trace = new Trace3D(gImg);
      trace.setCouleur(Color.blue);
      trace.ajouteGrVolume(cube_);
      trace.dessine();
    }
    volumeAChange_ = false;
    _g.drawImage(imgBuf_, bordure.left, bordure.top, this);
  }

  // **********************************************
  // EVENEMENTS
  // **********************************************
  // mouse pressed
  @Override
  public void mousePressed(final MouseEvent _evt) {}

  // mouse entered
  @Override
  public void mouseEntered(final MouseEvent _evt) {}

  // mouse exited
  @Override
  public void mouseExited(final MouseEvent _evt) {}

  // mouse clicked
  @Override
  public void mouseClicked(final MouseEvent _evt) {}

  // mouse released
  @Override
  public void mouseReleased(final MouseEvent _evt) {}

  // mouse dragged
  @Override
  public void mouseDragged(final MouseEvent _evt) {}

  // mouse moved
  @Override
  public void mouseMoved(final MouseEvent _evt) {}

  // RepereEvent
  @Override
  public void repereModifie(final RepereEvent _evt) {
    // on calcule la boite et on la convertit en volume
    final GrVolume volGrBoite = GrVolume.creeBoite(volumeRef_.boite());
    for (int j = 0; j < 3; j++) {
      if (_evt.aChange(0, j)) {
        repere_[0][j] = (_evt.relatif(0, j) ? repere_[0][j] : 1.) * _evt.valeur(0, j);
      }
    }
    for (int i = 1; i < 4; i++) {
      for (int j = 0; j < 3; j++) {
        if (_evt.aChange(i, j)) {
          repere_[i][j] = (_evt.relatif(i, j) ? repere_[i][j] : 0.) + _evt.valeur(i, j);
        }
      }
    }
    final GrMorphisme m = GrMorphisme.identite();
    m.composition(GrMorphisme.dilatation(repere_[0][0], repere_[0][1], repere_[0][2]));
    m.composition(GrMorphisme.rotation(repere_[2][0], repere_[2][1], repere_[2][2], volGrBoite.applique(m).centre()));
    m.composition(GrMorphisme.translation(repere_[1][0], repere_[1][1], repere_[1][2]));
    // on applique le tout au volume
    cube_ = volumeRef_.applique(m);
    volumeAChange_ = true;
    repaint();
  }

  // RepereEvent listeners
  public synchronized void addRepereEventListener(final RepereEventListener _listener) {
    repereEventListeners_.add(_listener);
  }

  public synchronized void removeRepereEventListener(final RepereEventListener _listener) {
    repereEventListeners_.remove(_listener);
  }

  // fireRepereEvent
  public synchronized void fireRepereEvent(final RepereEvent _evt) {
    for (int i = 0; i < repereEventListeners_.size(); i++) {
      ((RepereEventListener) repereEventListeners_.get(i)).repereModifie(_evt);
    }
  }

  // **********************************************
  // PROPRIETES INTERNES
  // **********************************************
  // Propriete volume
  private GrVolume volumeRef_;

  public GrVolume getGrVolume() {
    return volumeRef_;
  }

  public void setGrVolume(final GrVolume _volumeRef) {
    if (volumeRef_ != _volumeRef) {
      final GrVolume vp = volumeRef_;
      cube_ = _volumeRef;
      volumeRef_ = cube_ ;
      // volumeRef_=recentre(volumeRef_);
      repereModifie(new RepereEvent(this, false));
      firePropertyChange("GrVolume", vp, volumeRef_);
    }
  }
}
