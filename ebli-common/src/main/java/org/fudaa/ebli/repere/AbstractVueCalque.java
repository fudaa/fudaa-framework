/*
 *  @creation     6 janv. 2003
 *  @modification $Date: 2006-12-05 10:14:36 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.ebli.repere;

import java.awt.Graphics;
import javax.swing.JComponent;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPolygone;

/**
 * @author deniger
 * @version $Id: AbstractVueCalque.java,v 1.9 2006-12-05 10:14:36 deniger Exp $
 */
public abstract class AbstractVueCalque extends JComponent implements RepereEventListener {
  /**
   * Constructor for AbstractVueCalque.
   */
  public AbstractVueCalque() {
    super();
  }

  public abstract RepereMouseKeyController getRepereController();

  public abstract AbstractCalque getAbstractCalque();

  public abstract void paintAllInImage(Graphics _g, GrMorphisme _versEcran, GrMorphisme _versReel);

  public abstract void translation(double _realDx, double _realDy, boolean _rapide);

  public abstract void setRapide(boolean _b);

  public abstract GrPolygone getViewPolygone();

  public abstract GrBoite getViewBoite();

  public abstract void zoomOnRealPoint(double _x, double _y, boolean _out);

  public abstract GrMorphisme getVersEcran();

  public abstract GrMorphisme getVersReel();
}
