/*
 * @creation     1998-10-16
 * @modification $Date: 2006-09-19 14:55:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.repere;
import java.util.EventObject;
/**
 * Un evenement "coordonnees".
 *
 * @version      $Id: CoordonneesEvent.java,v 1.8 2006-09-19 14:55:51 deniger Exp $
 * @author       Guillaume Desnoix
 */
public class CoordonneesEvent extends EventObject {
  public final static int X= 0;
  public final static int Y= 1;
  public final static int Z= 2;
  boolean[] modifie_;
  double[] valeur_;
  boolean[] defini_;
  boolean ajustement_;
  public CoordonneesEvent(
    final Object _source,
    final double[] _valeur,
    final boolean[] _modifie,
    final boolean[] _defini,
    final boolean _ajustement) {
    super(_source);
    valeur_= _valeur;
    modifie_= _modifie;
    defini_= _defini;
    ajustement_= _ajustement;
  }
  public boolean[] modifie() {
    return modifie_;
  }
  public double[] valeur() {
    return valeur_;
  }
  public boolean[] defini() {
    return defini_;
  }
  public boolean ajustement() {
    return ajustement_;
  }
}
