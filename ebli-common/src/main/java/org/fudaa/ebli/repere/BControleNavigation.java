/*
 * @creation     2000-04-27
 * @modification $Date: 2007-06-28 09:26:48 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.repere;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.fudaa.ctulu.gui.CtuluPopupListener;
import org.fudaa.ctulu.gui.CtuluPopupMenu;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliPreferences;
import org.fudaa.ebli.controle.BMolette;
import org.fudaa.ebli.ressource.EbliResource;

/**
 * Un bean de controle de navigation.
 * 
 * @version $Id: BControleNavigation.java,v 1.15 2007-06-28 09:26:48 deniger Exp $
 * @author Bertrand Marchand
 */
public class BControleNavigation extends JPanel implements CtuluPopupListener.PopupReceiver {

  private final BTransformationDomaine td_;
  private String typeAffichage_;
  public final static String MOLETTE = "molette";
  public final static String BOUTON = "bouton";
  public final static String DOMAINE = "domaine";
  private final static String PREFERENCE_KEY = "btransformationdomaine.affichage";
  private final CtuluPopupListener listener_;

  public BControleNavigation(final AbstractVueCalque _vue) {
    super();
    final BorderLayout l2 = new BorderLayout();
    l2.setHgap(2);
    l2.setVgap(2);
    setLayout(l2);
    td_ = new BTransformationDomaine();
    td_.setVueCalque(_vue);
    td_.setPreferredSize(new Dimension(46, 66));
    listener_ = new CtuluPopupListener(this, this);
    td_.addMouseListener(listener_);
    setAffichage(EbliPreferences.EBLI.getStringProperty(PREFERENCE_KEY, MOLETTE));
  }

  public final void setAffichage(final String _id) {
    if (_id == null) {
      return;
    }
    if (_id.equals(typeAffichage_)) {
      return;
    }
    typeAffichage_ = _id;
    clean();
    if (BOUTON.equals(typeAffichage_)) {
      buildDomaineBouton();
    } else if (DOMAINE.equals(typeAffichage_)) {
      buildDomaine();
    } else {
      buildDomaineMolette();
    }
    EbliPreferences.EBLI.putStringProperty(PREFERENCE_KEY, typeAffichage_);
    end();
  }

  private void clean() {
    removeAll();
  }

  private void end() {
    revalidate();
    // repaint();
  }

  private void buildDomaine() {
    add(td_, BuBorderLayout.CENTER);
  }

  private void buildDomaineBouton() {
    final AbstractVueCalque vue = td_.getVueCalque();
    final Dimension dm = new Dimension(16, 16);
    final BTransformationBouton btz = new BTransformationBouton(RepereEvent.ZOOM, -1, false);
    btz.setPreferredSize(dm);
    btz.setToolTipText(EbliLib.getS("Agrandir"));
    btz.addRepereEventListener(vue);
    btz.setIcon(EbliResource.EBLI.getIcon("plus_10"));
    final BTransformationBouton btzM = new BTransformationBouton(RepereEvent.ZOOM, false);
    btzM.setToolTipText(EbliLib.getS("R�duire"));
    btzM.addRepereEventListener(vue);
    btzM.setPreferredSize(dm);
    btzM.setIcon(EbliResource.EBLI.getIcon("moins_10"));
    GridBagLayout grille = new GridBagLayout();
    final GridBagConstraints cl = new GridBagConstraints();
    final BuPanel zoom = new BuPanel();
    zoom.setLayout(grille);
    cl.gridx = 0;
    cl.gridy = 0;
    cl.anchor = GridBagConstraints.SOUTH;
    grille.setConstraints(btz, cl);
    zoom.add(btz);
    cl.gridx = 0;
    cl.gridy = 1;
    cl.anchor = GridBagConstraints.NORTH;
    grille.setConstraints(btzM, cl);
    zoom.add(btzM);
    final BTransformationBouton btx = new BTransformationBouton(RepereEvent.TRANS_X, false);
    btx.setIcon(EbliResource.EBLI.getIcon("flechedroite_10"));
    btx.setToolTipText(EbliLib.getS("Translation positive en X"));
    btx.setPreferredSize(dm);
    btx.addRepereEventListener(vue);
    final BTransformationBouton btxM = new BTransformationBouton(RepereEvent.TRANS_X, -1, false);
    btxM.setPreferredSize(dm);
    btxM.setToolTipText(EbliLib.getS("Translation n�gative en X"));
    btxM.setIcon(EbliResource.EBLI.getIcon("flechegauche_10"));
    btxM.addRepereEventListener(vue);
    final BuPanel transX = new BuPanel();
    transX.setLayout(new FlowLayout(FlowLayout.CENTER, 1, 0));
    transX.add(btxM);
    transX.add(btx);
    final BTransformationBouton bty = new BTransformationBouton(RepereEvent.TRANS_Y, false);
    bty.setIcon(EbliResource.EBLI.getIcon("flechehaut_10"));
    bty.setToolTipText(EbliLib.getS("Translation positive en Y"));
    bty.setVerticalAlignment(SwingConstants.CENTER);
    bty.setHorizontalAlignment(SwingConstants.CENTER);
    bty.setPreferredSize(dm);
    bty.addRepereEventListener(vue);
    final BTransformationBouton btyM = new BTransformationBouton(RepereEvent.TRANS_Y, -1, false);
    btyM.setIcon(EbliResource.EBLI.getIcon("flechebas_10"));
    btyM.setPreferredSize(dm);
    btyM.setToolTipText(EbliLib.getS("Translation n�gative en Y"));
    btyM.addRepereEventListener(vue);
    final BuPanel transY = new BuPanel();
    grille = new GridBagLayout();
    transY.setLayout(grille);
    cl.gridx = 0;
    cl.gridy = 0;
    cl.anchor = GridBagConstraints.SOUTH;
    grille.setConstraints(bty, cl);
    transY.add(bty);
    cl.gridx = 0;
    cl.gridy = 1;
    cl.anchor = GridBagConstraints.NORTH;
    grille.setConstraints(btyM, cl);
    transY.add(btyM);
    final BTransformationBouton btrz = new BTransformationBouton(RepereEvent.ROT_Z, -1, false);
    btrz.setToolTipText(EbliLib.getS("Rotation positive"));
    btrz.setIcon(EbliResource.EBLI.getIcon("rotationplus_10"));
    btrz.setPreferredSize(dm);
    btrz.addRepereEventListener(vue);
    final BTransformationBouton btrzM = new BTransformationBouton(RepereEvent.ROT_Z, false);
    btrzM.setToolTipText(EbliLib.getS("Rotation n�gative"));
    btrzM.setIcon(EbliResource.EBLI.getIcon("rotationmoins_10"));
    btrzM.setPreferredSize(dm);
    btrzM.addRepereEventListener(vue);
    final BuPanel rotZ = new BuPanel();
    rotZ.setLayout(new FlowLayout(FlowLayout.CENTER, 1, 0));
    rotZ.add(btrzM);
    rotZ.add(btrz);
    add(rotZ, BuBorderLayout.NORTH);
    add(zoom, BuBorderLayout.WEST);
    add(transX, BuBorderLayout.SOUTH);
    add(transY, BuBorderLayout.EAST);
    add(td_, BuBorderLayout.CENTER);
  }

  private void buildDomaineMolette() {
    final AbstractVueCalque vue = td_.getVueCalque();
    final Dimension dmh = new Dimension(66, 10);
    final Dimension dmv = new Dimension(10, 66);
    final BTransformationMolette tx = new BTransformationMolette();
    tx.setTypeTransfo(RepereEvent.TRANS_X);
    tx.setOrientation(BMolette.HORIZONTAL);
    tx.setPreferredSize(dmh);
    tx.setBackground(new Color(192, 128, 192));
    tx.setToolTipText(EbliResource.EBLI.getString("Translation en X"));
    tx.addRepereEventListener(vue);
    final BTransformationMolette ty = new BTransformationMolette();
    ty.setTypeTransfo(RepereEvent.TRANS_Y);
    ty.setPreferredSize(dmv);
    ty.setBackground(new Color(128, 192, 128));
    ty.setToolTipText(EbliResource.EBLI.getString("Translation en Y"));
    ty.addRepereEventListener(vue);
    final BTransformationMolette rz = new BTransformationMolette();
    rz.setTypeTransfo(RepereEvent.ROT_Z);
    rz.setOrientation(BMolette.HORIZONTAL);
    rz.setPreferredSize(dmh);
    rz.setBackground(new Color(128, 192, 192));
    rz.setToolTipText(EbliResource.EBLI.getString("Rotation en Z"));
    rz.addRepereEventListener(vue);
    final BTransformationMolette exy = new BTransformationMolette();
    exy.setTypeTransfo(RepereEvent.ZOOM);
    exy.setPreferredSize(dmv);
    exy.setBackground(new Color(192, 192, 128));
    exy.setToolTipText(EbliResource.EBLI.getString("Echelle en XY"));
    exy.addRepereEventListener(vue);
    add(td_, BuBorderLayout.CENTER);
    add(rz, BuBorderLayout.NORTH);
    add(ty, BuBorderLayout.EAST);
    add(tx, BuBorderLayout.SOUTH);
    add(exy, BuBorderLayout.WEST);
    rz.addMouseListener(listener_);
    tx.addMouseListener(listener_);
    ty.addMouseListener(listener_);
    exy.addMouseListener(listener_);
  }

  @Override
  public void popup(MouseEvent _evt) {
    final String prefix = "AFF_";
    final CtuluPopupMenu bp = new CtuluPopupMenu();
    final ActionListener l = new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent _e) {
        String com = _e.getActionCommand();
        if (com.startsWith(prefix)) {
          setAffichage(com.substring(prefix.length()).toLowerCase());

        }
      }
    };
    bp.addRadioButton(EbliLib.getS(MOLETTE), prefix + "MOLETTE", null, true, MOLETTE.equals(typeAffichage_))
        .addActionListener(l);
    bp.addRadioButton(EbliLib.getS("bouton"), prefix + "BOUTON", null, true, "bouton".equals(typeAffichage_))
        .addActionListener(l);
    bp.addRadioButton(EbliLib.getS("domaine uniquement"), prefix + "DOMAINE", null, true,
        "domaine".equals(typeAffichage_)).addActionListener(l);
    bp.show((JComponent) _evt.getSource(), _evt.getX(), _evt.getY());
  }
}
