/*
 * @creation     1998-07-15
 * @modification $Date: 2006-04-12 15:28:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.repere;
import java.util.EventListener;
/**
 * Interface auditeur de l'evenement "repere".
 *
 * @version      $Revision: 1.7 $ $Date: 2006-04-12 15:28:02 $ by $Author: deniger $
 * @author       Axel von Arnim
 */
public interface RepereEventListener extends EventListener {
  void repereModifie(RepereEvent _evt);
}
