/*
 * @creation     1998-07-15
 * @modification $Date: 2006-09-19 14:55:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.repere;

import java.text.DecimalFormat;
import java.util.EventObject;

/**
 * Un evenement "repere" avec des coordonnees ecran pour les translations.
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 14:55:51 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class RepereEvent extends EventObject {
  public final static int ZOOM = 0;
  public final static int ZOOM_X = 1;
  public final static int ZOOM_Y = 2;
  public final static int ZOOM_Z = 3;
  public final static int ROT = 4;
  public final static int ROT_X = 5;
  public final static int ROT_Y = 6;
  public final static int ROT_Z = 7;
  public final static int TRANS = 8;
  public final static int TRANS_X = 9;
  public final static int TRANS_Y = 10;
  public final static int TRANS_Z = 11;
  public final static int IDENT = 12;
  public final static boolean ABSOLU = false;
  public final static boolean RELATIF = true;
  final boolean[][] aChange_;
  final double[][] valeur_;
  final boolean[][] relatif_;
  final boolean ajustement_;

  public RepereEvent(final Object _source, final boolean _ajustement) {
    super(_source);
    ajustement_ = _ajustement;
    /* Matrice des transformations */
    /* [ Ex Ey Ez ] */
    /* [ Tx Ty Tz ] */
    /* [ Rx Ry Rz ] */
    /* [ Sx Sy Sz ] */
    aChange_ = new boolean[4][3];
    valeur_ = new double[4][3];
    relatif_ = new boolean[4][3];
    for (int j = 0; j < 3; j++) { // Echelle
      aChange_[0][j] = false;
      valeur_[0][j] = 1.;
      relatif_[0][j] = false;
    }
    for (int i = 1; i < 4; i++) {
      // Autres
      for (int j = 0; j < 3; j++) {
        aChange_[i][j] = false;
        valeur_[i][j] = 0.;
        relatif_[i][j] = false;
      }
    }
  }

  @Override
  public String toString() {
    final DecimalFormat nf = new DecimalFormat("##.###E0");
    nf.setMinimumFractionDigits(2);
    nf.setMaximumFractionDigits(2);
    nf.setMinimumIntegerDigits(1);
    nf.setMaximumIntegerDigits(1);
    nf.setGroupingUsed(false);
    final StringBuffer res = new StringBuffer(100);
    res.append("RepereEvent()\n[(val, rel, chg) (val, rel, chg) (val, rel, chg)]\n");
    for (int i = 0; i < 4; i++) {
      res.append('[');
      for (int j = 0; j < 3; j++) {
        res.append(" (").append(nf.format(valeur_[i][j])).append(", ").append((relatif_[i][j] ? "1" : "0"))
            .append(", ").append((aChange_[i][j] ? "1" : "0")).append(") ");
      }
      res.append("]\n");
    }
    return res.toString();
  }

  public void ajouteTransformation(final int _i, final int _j, final double _valeur, final boolean _relatif) {
    if (_i == 0) {
      valeur_[_i][_j] = (_relatif ? valeur_[_i][_j] : 1.) * _valeur;
    } else {
      // Autres
      valeur_[_i][_j] = (_relatif ? valeur_[_i][_j] : 0.) + _valeur;
    }
    relatif_[_i][_j] = _relatif;
    aChange_[_i][_j] = true;
  }

  public void ajouteTransformation(final int _type, final double _valeur, final boolean _relatif) {
    switch (_type) {
    case ZOOM_X:
      ajouteTransformation(0, 0, _valeur, _relatif);
      return;
    case ZOOM_Y:
      ajouteTransformation(0, 1, _valeur, _relatif);
      return;
    case ZOOM_Z:
      ajouteTransformation(0, 2, _valeur, _relatif);
      return;
    case ZOOM:
      ajouteTransformation(0, 0, _valeur, _relatif);
      ajouteTransformation(0, 1, _valeur, _relatif);
      ajouteTransformation(0, 2, _valeur, _relatif);
      return;
    case ROT_X:
      ajouteTransformation(2, 0, _valeur, _relatif);
      return;
    case ROT_Y:
      ajouteTransformation(2, 1, _valeur, _relatif);
      return;
    case ROT_Z:
      ajouteTransformation(2, 2, _valeur, _relatif);
      return;
    case ROT:
      ajouteTransformation(2, 0, _valeur, _relatif);
      ajouteTransformation(2, 1, _valeur, _relatif);
      ajouteTransformation(2, 2, _valeur, _relatif);
      return;
    case TRANS_X:
      ajouteTransformation(1, 0, _valeur, _relatif);
      return;
    case TRANS_Y:
      ajouteTransformation(1, 1, _valeur, _relatif);
      return;
    case TRANS_Z:
      ajouteTransformation(1, 2, _valeur, _relatif);
      return;
    case TRANS:
      ajouteTransformation(1, 0, _valeur, _relatif);
      ajouteTransformation(1, 1, _valeur, _relatif);
      ajouteTransformation(1, 2, _valeur, _relatif);
      return;
    case IDENT:
      if (!_relatif) {
        for (int i = 0; i < 4; i++) {
          for (int j = 0; j < 3; j++) {
            aChange_[i][j] = true;
          }
        }
      }
      return;
    default:
    }
  }

  public boolean aChange(final int _i, final int _j) {
    return aChange_[_i][_j];
  }

  public double valeur(final int _i, final int _j) {
    return valeur_[_i][_j];
  }

  public boolean relatif(final int _i, final int _j) {
    return relatif_[_i][_j];
  }

  public boolean ajustement() {
    return ajustement_;
  }
}
