/*
 * @creation     2000-04-27
 * @modification $Date: 2007-05-04 13:49:45 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.repere;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import org.fudaa.ebli.geometrie.GrVecteur;

/**
 * Un bean de controle de repere par textfields.
 * 
 * @version $Revision: 1.10 $ $Date: 2007-05-04 13:49:45 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class BControleRepereTexte extends JPanel implements RepereEventListener {
  private final BTransformationTexteSimple tfTrX_, tfTrY_, tfEch_;
  private final List repereEventListeners_;
  private final AbstractVueCalque vue_;

  public BControleRepereTexte(final AbstractVueCalque _vue) {
    super();
    vue_ = _vue;
    repereEventListeners_ = new ArrayList();
    setLayout(new BuGridLayout(2, 5, 5));
    setBorder(new EmptyBorder(5, 5, 5, 5));
    int n = 0;
    tfEch_ = new BTransformationTexteSimple();
    tfEch_.setTypeTransfo(RepereEvent.ZOOM);
    tfEch_.addRepereEventListener(this);
    add(new BuLabel("Echelle"), n++);
    add(new BuLabel("Rotation"), n++);
    add(tfEch_, n++);
    BTransformationTexteSimple tfRot = new BTransformationTexteSimple();
    tfRot.setTypeTransfo(RepereEvent.ROT_Z);
    tfRot.addRepereEventListener(this);
    add(tfRot, n++);
    tfTrX_ = new BTransformationTexteSimple();
    tfTrX_.setTypeTransfo(RepereEvent.TRANS_X);
    tfTrX_.addRepereEventListener(this);
    add(new BuLabel("Translation X"), n++);
    add(new BuLabel("Translation Y"), n++);
    add(tfTrX_, n++);
    tfTrY_ = new BTransformationTexteSimple();
    tfTrY_.setTypeTransfo(RepereEvent.TRANS_Y);
    tfTrY_.addRepereEventListener(this);
    add(tfTrY_, n++);
  }

  // RepereEvent listeners
  public synchronized void addRepereEventListener(final RepereEventListener _listener) {
    repereEventListeners_.add(_listener);
  }

  public synchronized void removeRepereEventListener(final RepereEventListener _listener) {
    repereEventListeners_.remove(_listener);
  }

  // fireRepereEvent
  public synchronized void fireRepereEvent(final RepereEvent _evt) {
    final GrVecteur v = new GrVecteur(_evt.valeur(1, 0), _evt.valeur(1, 1), _evt.valeur(1, 2));
    v.autoApplique(vue_.getVersEcran());
    if (_evt.aChange(1, 0)) {
      _evt.ajouteTransformation(1, 0, v.x_ - vue_.getInsets().left, RepereEvent.ABSOLU);
    }
    if (_evt.aChange(1, 1)) {
      _evt.ajouteTransformation(1, 1, -v.y_ + vue_.getInsets().bottom, RepereEvent.ABSOLU);
    }
    if (_evt.aChange(1, 2)) {
      _evt.ajouteTransformation(1, 2, v.z_, RepereEvent.ABSOLU);
    }
    for (int i = 0; i < repereEventListeners_.size(); i++) {
      ((RepereEventListener) repereEventListeners_.get(i)).repereModifie(_evt);
    }
  }

  // repere event
  @Override
  public void repereModifie(final RepereEvent _e) {
    fireRepereEvent(_e);
  }
}
