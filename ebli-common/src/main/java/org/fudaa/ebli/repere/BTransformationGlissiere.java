/*
 * @creation     1998-07-16
 * @modification $Date: 2006-09-19 14:55:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.repere;

import javax.swing.JSlider;

/**
 * Un controle de transformation par glissiere.
 *
 * @version $Revision: 1.7 $ $Date: 2006-09-19 14:55:51 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class BTransformationGlissiere extends JSlider {
  // donnees membres privees
  int valeurPrec_;

  // Vector repereEventListeners_;
  // Constructeur
  public BTransformationGlissiere() {
    super(VERTICAL, 0, 100, 50);
    typeTransfo_ = RepereEvent.ZOOM;
    valeurPrec_ = 50;
    sensibilite_ = 1.1;
    // repereEventListeners_=new Vector();
  }

  // **********************************************
  // EVENEMENTS
  // **********************************************
  // FireStateChanged
  @Override
  protected void fireStateChanged() {
    final RepereEvent evt = new RepereEvent(this, getValueIsAdjusting());
    switch (typeTransfo_) {
    case RepereEvent.ZOOM:
    case RepereEvent.ZOOM_X:
    case RepereEvent.ZOOM_Y:
    case RepereEvent.ZOOM_Z:
      evt.ajouteTransformation(typeTransfo_, Math.pow(sensibilite_, getValue() - valeurPrec_), RepereEvent.RELATIF);
      break;
    default:
      evt.ajouteTransformation(typeTransfo_, sensibilite_ * (getValue() - valeurPrec_), RepereEvent.RELATIF);
      break;
    }
    fireRepereEvent(evt);
    valeurPrec_ = getValue();
  }

  // RepereEvent listeners
  public synchronized void addRepereEventListener(final RepereEventListener _listener) {
    listenerList.add(RepereEventListener.class, _listener);
  }

  public synchronized void removeRepereEventListener(final RepereEventListener _listener) {
    listenerList.remove(RepereEventListener.class, _listener);
  }

  public synchronized void fireRepereEvent(final RepereEvent _evt) {
    final Object[] listeners = listenerList.getListenerList();
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == RepereEventListener.class) {
        ((RepereEventListener) listeners[i + 1]).repereModifie(_evt);
      }
    }
  }
  // **********************************************
  // PROPRIETES INTERNES
  // **********************************************
  // Propriete sensibilite
  private double sensibilite_;

  double getSensibilite() {
    return sensibilite_;
  }

  public void setSensibilite(final double _sensibilite) {
    if ((sensibilite_ != _sensibilite) && (_sensibilite != 0)) {
      final double vp = sensibilite_;
      sensibilite_ = _sensibilite;
      firePropertyChange("Sensibilite", vp, sensibilite_);
    }
  }
  // Propriete typeTransfo
  private int typeTransfo_;

  int getTypeTransfo() {
    return typeTransfo_;
  }

  public void setTypeTransfo(final int _typeTransfo) {
    if (typeTransfo_ != _typeTransfo) {
      switch (_typeTransfo) {
      case RepereEvent.ZOOM:
      case RepereEvent.ZOOM_X:
      case RepereEvent.ZOOM_Y:
      case RepereEvent.ZOOM_Z:
        valeurPrec_ = 50;
        sensibilite_ = 1.1;
        break;
      case RepereEvent.ROT:
      case RepereEvent.ROT_X:
      case RepereEvent.ROT_Y:
      case RepereEvent.ROT_Z:
        valeurPrec_ = 50;
        sensibilite_ = 2 * Math.PI / 100;
        break;
      case RepereEvent.TRANS:
      case RepereEvent.TRANS_X:
      case RepereEvent.TRANS_Y:
      case RepereEvent.TRANS_Z:
        valeurPrec_ = 50;
        sensibilite_ = 10.;
        break;
      default:
        return;
      }
      final int vp = typeTransfo_;
      typeTransfo_ = _typeTransfo;
      // repaint();
      firePropertyChange("TypeTransfo", vp, typeTransfo_);
    }
  }
}
