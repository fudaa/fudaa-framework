/*
 * @creation     1998-07-13
 * @modification $Date: 2006-09-19 14:55:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.ebli.repere;

import com.memoire.bu.BuButton;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Un controle de transformation par bouton.
 * 
 * @version $Id: BTransformationBouton.java,v 1.7 2006-09-19 14:55:51 deniger Exp $
 * @author Axel von Arnim
 */
public class BTransformationBouton extends BuButton implements MouseListener {
  double valeurNeutre_;
  boolean boutonEnfonce_;

  // Constructeur
  public BTransformationBouton() {
    this(RepereEvent.ZOOM, true);
  }

  public BTransformationBouton(final int _typeTransfo) {
    this(_typeTransfo, true);
  }

  public BTransformationBouton(final int _typeTransfo, final boolean _text) {
    this(_typeTransfo, +1, _text);
  }

  public BTransformationBouton(final int _typeTransfo, final int _signe) {
    this(_typeTransfo, _signe, true);
  }

  public BTransformationBouton(final int _typeTransfo, final int _signe, final boolean _text) {
    super();
    typeTransfo_ = -1;
    setTypeTransfo(_typeTransfo);
    boutonEnfonce_ = false;
    addMouseListener(this);
    setSigne(_signe);
    if (_text) {
      setText(valeur_ >= 0 ? "+" : "-");
    }
  }

  // **********************************************
  // EVENEMENTS
  // **********************************************
  // Mouse
  @Override
  public void mousePressed(final MouseEvent _e) {
    boutonEnfonce_ = true;
    final RepereEvent evt = new RepereEvent(this, false);
    evt.ajouteTransformation(typeTransfo_, valeur_, RepereEvent.RELATIF);
    fireRepereEvent(evt);
    new RepeteurBouton(evt).start();
  }

  @Override
  public void mouseReleased(final MouseEvent _e) {
    boutonEnfonce_ = false;
  }

  @Override
  public void mouseClicked(final MouseEvent _e) {}

  @Override
  public void mouseEntered(final MouseEvent _e) {}

  @Override
  public void mouseExited(final MouseEvent _e) {}
  class RepeteurBouton extends Thread {
    RepereEvent evt_;

    public RepeteurBouton(final RepereEvent _evt) {
      evt_ = _evt;
    }

    @Override
    public void run() {
      try {
        sleep(1000);
        while (boutonEnfonce_) {
          fireRepereEvent(evt_);
          sleep(100);
        }
      } catch (final InterruptedException e) {}
    }
  }

  // RepereEvent listeners
  public synchronized void addRepereEventListener(final RepereEventListener _listener) {
    listenerList.add(RepereEventListener.class, _listener);
  }

  public synchronized void removeRepereEventListener(final RepereEventListener _listener) {
    listenerList.remove(RepereEventListener.class, _listener);
  }

  public synchronized void fireRepereEvent(final RepereEvent _evt) {
    final Object[] listeners = listenerList.getListenerList();
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == RepereEventListener.class) {
        ((RepereEventListener) listeners[i + 1]).repereModifie(_evt);
      }
    }
  }
  // **********************************************
  // PROPRIETES INTERNES
  // **********************************************
  // Propriete valeur
  private double valeur_;

  double getValeur() {
    return valeur_;
  }

  public final void setValeur(final double _valeur) {
    // double v=_valeur>=0?_valeur:-_valeur;
    if (valeur_ != _valeur) {
      if ((typeTransfo_ == RepereEvent.ZOOM) || (typeTransfo_ == RepereEvent.ZOOM_X)
          || (typeTransfo_ == RepereEvent.ZOOM_Y) || (typeTransfo_ == RepereEvent.ZOOM_Z)) {
        if (_valeur == 0.) {
          return;
        }
      }
      final double vp = valeur_;
      valeur_ = _valeur;
      // setText((valeur_>valeurNeutre_)?"+":"-");
      firePropertyChange("Valeur", vp, valeur_);
    }
  }

  // Propriete signe
  int getSigne() {
    return (valeur_ >= valeurNeutre_) ? 1 : -1;
  }

  private void setSigne(final int _signe) {
    if (_signe != ((valeur_ >= valeurNeutre_) ? 1 : -1)) {
      switch (typeTransfo_) {
      case RepereEvent.ZOOM:
      case RepereEvent.ZOOM_X:
      case RepereEvent.ZOOM_Y:
      case RepereEvent.ZOOM_Z:
        setValeur(1. / valeur_);
        break;
      default:
        setValeur(-valeur_);
      }
      final int vp = (valeur_ >= valeurNeutre_) ? 1 : -1;
      firePropertyChange("Signe", vp, (valeur_ >= valeurNeutre_) ? 1 : -1);
    }
  }
  // Propriete typeTransfo
  private int typeTransfo_;

  int getTypeTransfo() {
    return typeTransfo_;
  }

  private void setTypeTransfo(final int _typeTransfo) {
    if (typeTransfo_ != _typeTransfo) {
      switch (_typeTransfo) {
      case RepereEvent.ZOOM:
      case RepereEvent.ZOOM_X:
      case RepereEvent.ZOOM_Y:
      case RepereEvent.ZOOM_Z:
        valeurNeutre_ = 1.;
        setValeur(2.);
        break;
      case RepereEvent.ROT:
      case RepereEvent.ROT_X:
      case RepereEvent.ROT_Y:
      case RepereEvent.ROT_Z:
        valeurNeutre_ = 0.;
        setValeur(0.1);
        break;
      case RepereEvent.TRANS:
      case RepereEvent.TRANS_X:
      case RepereEvent.TRANS_Y:
      case RepereEvent.TRANS_Z:
        valeurNeutre_ = 0.;
        setValeur(10.);
        break;
      default:
        return;
      }
      final int vp = typeTransfo_;
      typeTransfo_ = _typeTransfo;
      // repaint();
      firePropertyChange("TypeTransfo", vp, typeTransfo_);
    }
  }
}
