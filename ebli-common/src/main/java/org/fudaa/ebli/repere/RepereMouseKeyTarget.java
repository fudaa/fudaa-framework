/*
 * @creation 1 d�c. 06
 * @modification $Date: 2006-12-05 10:14:36 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.repere;

import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelListener;

/**
 * @author fred deniger
 * @version $Id: RepereMouseKeyTarget.java,v 1.1 2006-12-05 10:14:36 deniger Exp $
 */
public interface RepereMouseKeyTarget {

  void translateXLeft(final boolean _largeTranslate, final boolean _rapide);

  void translateXRight(final boolean _large, final boolean _rapide);

  void translateYUp(final boolean _largeTranslate, final boolean _rapide);

  void translateYDown(final boolean _largeTranslate, final boolean _rapide);

  void zoomOnMouse(final MouseEvent _evt, boolean _isOut);

  void zoomIn();

  void zoomOut();

  void addMouseListener(MouseListener _l);

  void addMouseWheelListener(MouseWheelListener _l);

  void addKeyListener(KeyListener _l);

  void setRapide(boolean _b);

  void requestFocus();

}
