/*
 * @creation 1 d�c. 06
 * @modification $Date: 2007-05-04 13:49:45 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ebli.repere;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import javax.swing.SwingUtilities;

/**
 * @author fred deniger
 * @version $Id: RepereMouseKeyController.java,v 1.2 2007-05-04 13:49:45 deniger Exp $
 */
public class RepereMouseKeyController extends MouseAdapter implements KeyListener, MouseWheelListener {

  long nextTime_;
  long nextTimeRapide_;
  final RepereMouseKeyTarget target_;

  public RepereMouseKeyController(final RepereMouseKeyTarget _target) {
    super();
    target_ = _target;
    target_.addMouseListener(this);
    target_.addMouseWheelListener(this);
    target_.addKeyListener(this);
  }

  @Override
  public void mouseReleased(final MouseEvent _evt) {
    if (SwingUtilities.isMiddleMouseButton(_evt)) {
//      target_.zoomOnMouse(_evt, _evt.isShiftDown());
    }
  }

  @Override
  public void mouseClicked(final MouseEvent _e) {
    target_.requestFocus();
  }

  @Override
  public void keyPressed(final KeyEvent _e) {
    final long current = System.currentTimeMillis();
    if (current >= nextTime_) {
      final boolean fast = nextTime_ > 0 && current > nextTimeRapide_;
      if (_e.getKeyCode() == KeyEvent.VK_SPACE) {
        if (_e.isShiftDown()) {
          target_.zoomOut();
        } else {
          target_.zoomIn();
        }
      } else if (_e.getKeyCode() == KeyEvent.VK_RIGHT) {
        target_.translateXRight(_e.isControlDown(), fast);
      } else if (_e.getKeyCode() == KeyEvent.VK_LEFT) {
        target_.translateXLeft(_e.isControlDown(), fast);
      } else if (_e.getKeyCode() == KeyEvent.VK_UP) {
        target_.translateYUp(_e.isControlDown(), fast);
      } else if (_e.getKeyCode() == KeyEvent.VK_DOWN) {
        target_.translateYDown(_e.isControlDown(), fast);
      }
      if (nextTime_ <= 0) {
        // 1/2 sec
        nextTimeRapide_ = current + 500;
      }

      nextTime_ = current + 200;
    }
  }

  @Override
  public void keyReleased(final KeyEvent _e) {
    nextTime_ = 0;
    target_.setRapide(false);

  }

  @Override
  public void keyTyped(final KeyEvent _e) {
  }

  @Override
  public void mouseWheelMoved(final MouseWheelEvent _e) {
    if (_e.isConsumed()) {
      return;
    }
    if (_e.isControlDown()) {
      if (_e.getWheelRotation() > 0) {
        target_.zoomOnMouse(_e, true);
      } else {
        target_.zoomOnMouse(_e, false);
      }
    } else {
      if (_e.getWheelRotation() < 0) {
        if (_e.isShiftDown()) {
          target_.translateXRight(false, false);
        } else {
          target_.translateYUp(false, false);
        }
      } else if (_e.isShiftDown()) {
        target_.translateXLeft(false, false);
      } else {
        target_.translateYDown(false, false);
      }
    }

  }
}
