

package com.memoire.mst;

// MstHandlerBase.java: Simple base class for AElfred processors.
// NO WARRANTY! See README, and copyright below.
// $Id: MstHandlerBase.java,v 1.2 2006-09-19 14:35:21 deniger Exp $







/**
  * Convenience base class for AElfred handlers.
  * <p>This base class implements the MstXmlHandler interface with
  * (mostly empty) default handlers.  You are not required to use this,
  * but if you need to handle only a few events, you might find
  * it convenient to extend this class rather than implementing
  * the entire interface.  This example overrides only the
  * <code>charData</code> method, using the defaults for the others:
  * <pre>
  * 
  *
  * public class MyHandler extends MstHandlerBase {
  *   public void charData (char ch[], int start, int length)
  *   {
  *     System.out.println("Data: " + new String (ch, start, length));
  *   }
  * }
  * </pre>
  * <p>This class is optional, but if you use it, you must also
  * include the <code>MstXmlException</code> class.
  * <p>Do not extend this if you are using SAX; extend
  * <code>org.xml.sax.MstHandlerBase</code> instead.
  * @author Copyright (c) 1998 by Microstar Software Ltd.
  * @author written by David Megginson &lt;dmeggins@microstar.com&gt;
  * @version 1.1
  * @see MstXmlHandler
  * @see MstXmlException
  * @see org.xml.sax.MstHandlerBase
  */
public class MstHandlerBase implements MstXmlHandler {

  /**
    * Handle the start of the document.
    * <p>The default implementation does nothing.
    * @see MstXmlHandler#startDocument
    * @exception java.lang.Exception Derived methods may throw exceptions.
    */
  @Override
  public void startDocument () 
    throws java.lang.Exception
  {
  }

  /**
    * Handle the end of the document.
    * <p>The default implementation does nothing.
    * @see MstXmlHandler#endDocument
    * @exception java.lang.Exception Derived methods may throw exceptions.
    */
  @Override
  public void endDocument ()
    throws java.lang.Exception
  {
  }

  /**
    * Resolve an external entity.
    * <p>The default implementation simply returns the supplied
    * system identifier.
    * @see MstXmlHandler#resolveEntity
    * @exception java.lang.Exception Derived methods may throw exceptions.
    */
  @Override
  public Object resolveEntity (String publicId, String systemId) 
    throws java.lang.Exception
  {
    return null;
  }


  /**
    * Handle the start of an external entity.
    * <p>The default implementation does nothing.
    * @see MstXmlHandler#startExternalEntity
    * @exception java.lang.Exception Derived methods may throw exceptions.
    */
  @Override
  public void startExternalEntity (String systemId)
    throws java.lang.Exception
  {
  }

  /**
    * Handle the end of an external entity.
    * <p>The default implementation does nothing.
    * @see MstXmlHandler#endExternalEntity
    * @exception java.lang.Exception Derived methods may throw exceptions.
    */
  @Override
  public void endExternalEntity (String systemId)
    throws java.lang.Exception
  {
  }

  /**
    * Handle a document type declaration.
    * <p>The default implementation does nothing.
    * @see MstXmlHandler#doctypeDecl
    * @exception java.lang.Exception Derived methods may throw exceptions.
    */
  @Override
  public void doctypeDecl (String name, String publicId, String systemId)
    throws java.lang.Exception
  {
  }

  /**
    * Handle an attribute assignment.
    * <p>The default implementation does nothing.
    * @see MstXmlHandler#attribute
    * @exception java.lang.Exception Derived methods may throw exceptions.
    */
  @Override
  public void attribute (String aname, String value, boolean isSpecified)
    throws java.lang.Exception
  {
  }

  /**
    * Handle the start of an element.
    * <p>The default implementation does nothing.
    * @see MstXmlHandler#startElement
    * @exception java.lang.Exception Derived methods may throw exceptions.
    */
  @Override
  public void startElement (String elname)
    throws java.lang.Exception
  {
  }

  /**
    * Handle the end of an element.
    * <p>The default implementation does nothing.
    * @see MstXmlHandler#endElement
    * @exception java.lang.Exception Derived methods may throw exceptions.
    */
  @Override
  public void endElement (String elname)
    throws java.lang.Exception
  {
  }

  /**
    * Handle character data.
    * <p>The default implementation does nothing.
    * @see MstXmlHandler#charData
    * @exception java.lang.Exception Derived methods may throw exceptions.
    */
  @Override
  public void charData (char ch[], int start, int length)
    throws java.lang.Exception
  {
  }

  /**
    * Handle ignorable whitespace.
    * <p>The default implementation does nothing.
    * @see MstXmlHandler#ignorableWhitespace
    * @exception java.lang.Exception Derived methods may throw exceptions.
    */
  @Override
  public void ignorableWhitespace (char ch[], int start, int length)
    throws java.lang.Exception
  {
  }

  /**
    * Handle a processing instruction.
    * <p>The default implementation does nothing.
    * @see MstXmlHandler#processingInstruction
    * @exception java.lang.Exception Derived methods may throw exceptions.
    */
  @Override
  public void processingInstruction (String target, String data)
    throws java.lang.Exception
  {
  }

  /**
    * Throw an exception for a fatal error.
    * <p>The default implementation throws <code>MstXmlException</code>.
    * @see MstXmlHandler#error
    * @exception MstXmlException A specific parsing error.
    * @exception java.lang.Exception Derived methods may throw exceptions.
    */
  @Override
  public void error (String message, String systemId, int line, int column)
    throws /*MstXmlException,*/ java.lang.Exception
  {
    throw new MstXmlException(message, systemId, line, column);
  }

}
