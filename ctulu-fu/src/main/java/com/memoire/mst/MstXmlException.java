

package com.memoire.mst;

// MstXmlException.java: Simple base class for AElfred processors.
// NO WARRANTY! See README, and copyright below.
// $Id: MstXmlException.java,v 1.2 2006-09-19 14:35:21 deniger Exp $




/**
  * Convenience exception class for reporting XML parsing errors.
  * <p>This is an exception class that you can use to encapsulate all
  * of the information from &AElig;lfred's <code>error</code> callback.
  * This is not necessary for routine use of &AElig;lfred, but it
  * is used by the optional <code>MstHandlerBase</code> class.
  * <p>Note that the core &AElig;lfred classes do <em>not</em>
  * use this exception.
  * @author Copyright (c) 1998 by Microstar Software Ltd.
  * @author written by David Megginson &lt;dmeggins@microstar.com&gt;
  * @version 1.1
  * @see MstXmlHandler#error
  * @see MstHandlerBase
  */
public class MstXmlException extends Exception
{
  private String message;
  private String systemId;
  private int line;
  private int column;


  /**
    * Construct a new XML parsing exception.
    * @param _message The error message from the parser.
    * @param _systemId The URI of the entity containing the error.
    * @param _line The line number where the error appeared.
    * @param _column The column number where the error appeared.
    */
  public MstXmlException (String _message, String _systemId, int _line, int _column)
  {
    this.message = _message;
    this.systemId = _systemId;
    this.line = _line;
    this.column = _column;
  }


  /**
    * Get the error message from the parser.
    * @return A string describing the error.
    */
  @Override
  public String getMessage ()
  {
    return message;
  }


  /**
    * Get the URI of the entity containing the error.
    * @return The URI as a string.
    */
  public String getSystemId ()
  {
    return systemId;
  }


  /**
    * Get the line number containing the error.
    * @return The line number as an integer.
    */
  public int getLine ()
  {
    return line;
  }

  /**
    * Get the column number containing the error.
    * @return The column number as an integer.
    */
  public int getColumn ()
  {
    return column;
  }

}
