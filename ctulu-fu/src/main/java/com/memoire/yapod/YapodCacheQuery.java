/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodCacheQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Vector;

public class YapodCacheQuery
       implements YapodQuery
{
  private Vector cache_;
  private YapodQuery previous_;

  public YapodCacheQuery
    (YapodQuery _previous)
  {
    cache_=null;
    setPrevious(_previous);
  }

  public final YapodQuery getPrevious()
    { return previous_; }

  public final synchronized void setPrevious(YapodQuery _query)
  {
    if(_query==null)
      throw new IllegalArgumentException("_query is null");

    previous_=_query;
  }

  @Override
  public Enumeration getResult()
  {
    if(cache_==null)
      cache_=YapodLib.toVector(getPrevious().getResult());

    return cache_.elements();
  }

  public synchronized void clean()
    { cache_=null; }

  @Override
  public String toString()
    { return "cache("+getPrevious()+")"; }
}
