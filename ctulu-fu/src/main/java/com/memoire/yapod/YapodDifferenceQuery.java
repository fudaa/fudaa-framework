/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         YapodDifferenceQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Vector;

public class YapodDifferenceQuery
       implements YapodQuery
{
  private YapodQuery q1_,q2_;

  public YapodDifferenceQuery
    (YapodQuery _q1, YapodQuery _q2)
  {
    q1_=_q1;
    q2_=_q2;
  }

  @Override
  public Enumeration getResult()
  {
    Vector r=new Vector();
    for(Enumeration e=q1_.getResult(); e.hasMoreElements(); )
    {
      Object o=e.nextElement();
      r.addElement(o);
    }
    for(Enumeration e=q2_.getResult(); e.hasMoreElements(); )
    {
      Object o=e.nextElement();
      if(r.contains(o)) r.removeElement(o);
    }
    return r.elements();
  }

  @Override
  public String toString()
    { return "difference("+q1_+","+q2_+")"; }
}
