/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut unstable
 * @file YapodCollector.java
 * @version 0.17
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 */
package com.memoire.yapod;

import com.memoire.fu.FuLog;

import java.io.*;
import java.nio.file.Files;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class YapodCollector
    implements Serializable {
  protected String file_;     // path to file
  protected int delay_;    // delay_ in seconds between automatic savings
  protected Vector objects_;  // list of collected objects
  protected Hashtable names_;    // name   -> object
  protected Hashtable binds_;    // object -> name
  transient Autosave as_;

  private YapodCollector() {
  }

  private YapodCollector(String _file) {
    file_ = _file;
    delay_ = 10;
    objects_ = new Vector();
    names_ = new Hashtable();
    binds_ = new Hashtable();
  }

  // autosave

  public void startAutoSave() {
    if (as_ == null) {
      as_ = new Autosave();
      as_.start();
    }
  }

  public void stopAutoSave() {
    if (as_ != null) {
      as_.active = false;
      as_ = null;
    }
  }

  public int getAutoSaveDelay() {
    return delay_;
  }

  public void setAutoSaveDelay(int _delay) {
    if (delay_ < 0) {
      throw new IllegalArgumentException
          ("delay must be equal or greater than 1 (" +
              _delay + " is not valid)");
    }
    delay_ = _delay;
  }

  // management

  public synchronized void add(Object _object) {
    if (_object == null) {
      throw new IllegalArgumentException("_object is null");
    }

    if (!objects_.contains(_object)) {
      objects_.addElement(_object);
    }
  }

  public synchronized void remove(Object _object) {
    if (_object == null) {
      throw new IllegalArgumentException("_object is null");
    }

    if (!objects_.contains(_object)) {
      throw new IllegalArgumentException("_object is not in collector");
    }

    objects_.removeElement(_object);

    Object name = binds_.get(_object);
    if (name != null) {
      binds_.remove(_object);
      names_.remove(name);
    }
  }

  public synchronized boolean contains(Object _object) {
    if (_object == null) {
      throw new IllegalArgumentException("_object is null");
    }

    return objects_.contains(_object);
  }

  public synchronized Enumeration list() {
    return objects_.elements();
  }

  // naming

  public synchronized void bind(String _name, Object _object) {
    if (_name == null) {
      throw new IllegalArgumentException("_name is null");
    }

    if (_object == null) {
      throw new IllegalArgumentException("_object is null");
    }

    if (!objects_.contains(_object)) {
      throw new IllegalArgumentException("_object is not in collector");
    }

    Object o = names_.get(_name);
    if ((o != null) && (o != _object)) {
      throw new IllegalArgumentException
          ("_name \"" + _name + "\" is already used");
    }

    names_.put(_name, _object);
    binds_.put(_object, _name);
  }

  public synchronized void unbind(String _name, Object _object) {
    if (_name == null) {
      throw new IllegalArgumentException("_name is null");
    }

    if (_object == null) {
      throw new IllegalArgumentException("_object is null");
    }

    if (!objects_.contains(_object)) {
      throw new IllegalArgumentException("_object is not in collector");
    }

    Object o = names_.get(_name);
    if ((o != null) && (o != _object)) {
      throw new IllegalArgumentException
          ("_name \"" + _name + "\" is already used");
    }

    names_.remove(_name);
    binds_.remove(_object);
  }

  public synchronized Object find(String _name) {
    if (_name == null) {
      throw new IllegalArgumentException("_name is null");
    }

    Object o = names_.get(_name);
    if (o == null) {
      throw new IllegalArgumentException
          ("_name \"" + _name + "\" is not used in collector");
    }

    return o;
  }

  // query

  public YapodQuery createQuery() {
    return new YapodQuery() {
      @Override
      public Enumeration getResult() {
        return list();
      }

      @Override
      public String toString() {
        return "collector(\"" + file_ + "\")";
      }
    };
  }



  public static YapodDeserializer getDeserializer(String _file) {
    YapodDeserializer r = null;
    if (_file.endsWith(".ser")) {
      r = new YapodSerDeserializer();
    } else if (_file.endsWith(".sgz")) {
      r = new YapodSgzDeserializer();
    } else if (_file.endsWith(".xml")) {
      r = new YapodXmlDeserializer();
    } else if (_file.endsWith(".xgz")) {
      r = new YapodXgzDeserializer();
    }
    return r;
  }

  public static YapodSerializer getSerializer(String _file) {
    YapodSerializer r = null;
    if (_file.endsWith(".ser")) {
      r = new YapodSerSerializer();
    } else if (_file.endsWith(".sgz")) {
      r = new YapodSgzSerializer();
    } else if (_file.endsWith(".xml")) {
      r = new YapodXmlSerializer();
    } else if (_file.endsWith(".xgz")) {
      r = new YapodXgzSerializer();
    } else if (_file.endsWith(".yss")) {
      r = new YapodSimpleSerializer();
    }
    return r;
  }

  public static synchronized boolean exists(String _file) {
    return new File(_file).canRead();
  }

  public static synchronized YapodCollector create(String _file) {
    if (exists(_file)) {
      throw new RuntimeException
          ("Can't create collector " + _file + ", already exists");
    }

    return new YapodCollector(_file);
  }

  public static synchronized void destroy(String _file) {
    if (!exists(_file)) {
      throw new RuntimeException
          ("Can't destroy collector " + _file + ", doesn't exist");
    }

    new File(_file).delete();
  }

  public static synchronized YapodCollector open(String _file) {
    YapodDeserializer in = getDeserializer(_file);

    if (in == null) {
      throw new RuntimeException
          ("Can't find a deserializer");
    }

    YapodCollector r = null;

    try {
      in.open(new FileInputStream(_file));
      r = (YapodCollector) in.read();
      in.close();
    } catch (Exception ex) {
      FuLog.error(ex);
      throw new RuntimeException("Couldn't save collector " + _file);
    }

    return r;
  }

  public synchronized void close() {
    stopAutoSave();
    if (canSave()) {
      save();
    }
  }

  public synchronized boolean canSave() {
    boolean r = false;

    File f = new File(file_);
    if (!f.exists()) {
      r = true;
      try {
        try (FileOutputStream out = new FileOutputStream(f)) {
          out.write(0);
        }
      } catch (Exception ex) {
        r = false;
      } finally {
        try {
          Files.delete(f.toPath());
        } catch (IOException e) {
          FuLog.error(e);
        }
      }
    } else {
      r = f.canWrite();
    }

    return r;
  }

  public synchronized void save() {
    YapodSerializer out = getSerializer(file_);

    if (out == null) {
      throw new RuntimeException("Can't find a serializer");
    }

    if (!canSave()) {
      throw new RuntimeException("Can't write " + file_);
    }

    try {
      if (exists(file_)) {
        new File(file_).renameTo(new File(file_ + ".cpy"));
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new RuntimeException("Couldn't make backup " + file_);
    }

    try {
      out.open(new FileOutputStream(file_));
      out.write(this);
      out.close();
      System.err.println(file_ + " saved.");
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new RuntimeException("Couldn't save collector " + file_);
    }
  }

  private final class Autosave extends Thread {
    public boolean active;

    public Autosave() {
      setPriority(Thread.MIN_PRIORITY);
      //setDaemon(true);
      active = true;
    }

    @Override
    public void run() {
      while (active) {
        if (!canSave()) {
          throw new RuntimeException("Can't write " + file_);
        }
        save();

        try {
          sleep(1000l * delay_);
        } catch (Exception ex) {
        }
        Thread.yield();
      }
    }
  }
}

