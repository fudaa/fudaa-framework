/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         YapodSgzDeserializer.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

public class YapodSgzDeserializer
       extends YapodSerDeserializer
{
  public YapodSgzDeserializer()
  {
  }

  @Override
  public String extension()
  {
    return "sgz";
  }

  public String indente()
  {
    return "";
  }

  @Override
  public synchronized void open(InputStream _in) throws IOException
  {
    InputStream in=_in;
    if(!(in instanceof BufferedInputStream))
      in=new BufferedInputStream(in,BUFFER_SIZE);
    super.open(new GZIPInputStream(in));
  }
}
