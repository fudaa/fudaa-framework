/**
 * @modification $Date: 2004-05-14 10:30:42 $
 * @statut       unstable
 * @file         YapodDeserializer.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;

public interface YapodDeserializer
{
  int BUFFER_SIZE=1048576; // 1Mb

  String extension();
  void   open (InputStream in) throws IOException;
  void   close() throws IOException;
  Object read () throws IOException, ClassNotFoundException;
  Object retrieve
    (String _id, Object dbcx_, Hashtable _ref, Hashtable _obj, Hashtable _nmo)
    throws Exception;
}
