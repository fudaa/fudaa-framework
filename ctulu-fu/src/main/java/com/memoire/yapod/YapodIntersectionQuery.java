/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodIntersectionQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Hashtable;

public class YapodIntersectionQuery
       implements YapodQuery
{
  private YapodQuery[] q_;

  public YapodIntersectionQuery
    (YapodQuery _q1, YapodQuery _q2)
  {
    q_=new YapodQuery[] { _q1, _q2 };
  }

  public YapodIntersectionQuery
    (YapodQuery[] _queries)
  {
    if(_queries==null)
      throw new IllegalArgumentException("_queries is null");

    q_=_queries;
  }

  @Override
  public Enumeration getResult()
  {
    Hashtable table=new Hashtable();

    for(int i=0;i<q_.length;i++)
    {
      for(Enumeration e=q_[i].getResult(); e.hasMoreElements(); )
      {
	Object v=e.nextElement();
	Integer n=(Integer)table.get(v);
	if(n==null) n=new Integer(1);
	else        n=new Integer(n.intValue()+1);
	table.put(v,n);
      }
    }

    for(Enumeration e=table.keys(); e.hasMoreElements(); )
    {
      Object k=e.nextElement();
      if(((Integer)table.get(k)).intValue()<q_.length)
	table.remove(k);
    }

    return table.elements();
  }

  @Override
  public String toString()
  {
    String r="intersection(";
    for(int i=0;i<q_.length;i++)
      r+= (i==0 ? "" : ",")+q_[i];
    r+=")";
    return r;
  }
}
