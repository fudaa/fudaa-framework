/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         YapodMinQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Vector;

public class YapodMinQuery
       extends YapodAbstractQuery
{
  public YapodMinQuery
    (YapodQuery _previous)
  {
    super(_previous);
  }

  @Override
  protected final Enumeration query(Enumeration e)
  {
    Number min_double=null;
    String min_string=null;

    while(e.hasMoreElements())
    {
      Object o=e.nextElement();
      if(o instanceof Number)
      {
	Number v=(Number)o;
	if(min_double==null) min_double=v;
	else if(v.doubleValue()<min_double.doubleValue()) min_double=v;
      }
      if(o instanceof String)
      {
	String v=(String)o;
	if(min_string==null) min_string=v;
	else if(min_string.compareTo(v)>0) min_string=v;
      }
    }

    Vector r=new Vector(1);
         if((min_string!=null)&&(min_double!=null)) ;
    else if(min_string!=null) r.addElement(min_string);
    else                      r.addElement(min_double);
    return r.elements();
  }

  @Override
  public String toString()
    { return "min("+getPrevious()+")"; }
}
