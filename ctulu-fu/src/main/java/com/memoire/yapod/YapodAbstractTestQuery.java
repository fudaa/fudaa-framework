/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodAbstractTestQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public abstract class YapodAbstractTestQuery
                extends YapodAbstractQuery
{
  public YapodAbstractTestQuery
    (YapodQuery _previous)
  {
    super(_previous);
  }

  protected abstract boolean test(Object _object);

  @Override
  protected final Enumeration query(Enumeration e)
    { return new Enumerator(e); }

  /*
  protected final Enumeration query(Enumeration e)
  {
    Vector r=new Vector();
    while(e.hasMoreElements())
    {
      Object o=e.nextElement();
      if(test(o)) r.addElement(o);
    }
    return r.elements();
  }
  */

  private final class Enumerator implements Enumeration
  {
    private Enumeration e_;
    private Object      next_;

    public Enumerator(Enumeration _e)
    {
      e_=_e;
      next();
    }

    private final void next()
    {
      boolean t;
      Object  o;

      do
      {
	if(!e_.hasMoreElements())
	  { next_=FAKE; return; }

	o=e_.nextElement();
	t=test(o);
	if(t) next_=o;
      } while(!t);
    }

    @Override
    public final boolean hasMoreElements()
      { return next_!=FAKE; }

    @Override
    public final Object nextElement()
    {
      if(next_==FAKE)
	throw new NoSuchElementException();

      Object r=next_;
      next();
      return r;
    }
  }

  public String getText()
    { return "internal"; }

  @Override
  public String toString()
    { return "test(\""+getText()+"\","+getPrevious()+")"; }
}
