/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         YapodSumQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Vector;

public class YapodSumQuery
       extends YapodAbstractQuery
{
  public YapodSumQuery
    (YapodQuery _previous)
  {
    super(_previous);
  }

  @Override
  protected final Enumeration query(Enumeration e)
  {
    double sum=0.;

    while(e.hasMoreElements())
    {
      Object o=e.nextElement();
      if(o instanceof Number)
	sum+=((Number)o).doubleValue();
    }

    Vector r=new Vector(1);
    r.addElement(new Double(sum));
    return r.elements();
  }

  @Override
  public String toString()
    { return "sum("+getPrevious()+")"; }
}
