/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodConstants.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;


public interface YapodConstants
{
  Object     FAKE =new Object();
  YapodQuery DUMMY=new YapodDummyQuery();
}
