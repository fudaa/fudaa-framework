/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodXgzDeserializer.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

public class YapodXgzDeserializer
       extends YapodXmlDeserializer
{
  public YapodXgzDeserializer()
  {
  }

  @Override
  public String extension()
  {
    return "xgz";
  }

  public String indente()
  {
    return "";
  }

  @Override
  public synchronized void open(InputStream _in) throws IOException
  {
    InputStream tmpIn=_in;
    if(!(_in instanceof BufferedInputStream))
      tmpIn=new BufferedInputStream(_in,BUFFER_SIZE);
    super.open(new GZIPInputStream(tmpIn));
  }
}
