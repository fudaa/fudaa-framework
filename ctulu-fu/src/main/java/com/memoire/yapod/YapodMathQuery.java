/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodMathQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;

public class YapodMathQuery
       extends YapodAbstractComputeQuery
{
  private static String[] OPERATORS=new String[]
  {
    "+","-","*","/","&","|","^"
  };

  private int    op_;
  private Object field_;
  private Object value_;

  public YapodMathQuery
    (Object _field, String _op, Object _value, YapodQuery _previous)
  {
    super(_previous);

    if(_field==null)
      throw new IllegalArgumentException
	("_field is null");

    /*
    if(_value==null)
      throw new IllegalArgumentException
	("_value is null");
    */

    int i=OPERATORS.length-1;
    while(i>=0)
    {
      if(OPERATORS[i].equals(_op)) break;
      i--;
    }

    if(i<0)
      throw new IllegalArgumentException
	("operator "+_op+" not reconized");

    op_   =i;
    field_=_field;
    value_=_value;
  }

  @Override
  protected Object compute(Object o)
  {
    Object v=field_;
    if(field_ instanceof String) v=YapodLib.getValue(o,(String)field_);
    if(v==FAKE) v=field_;
    if(v==null) v="";

    Object w=value_;
    if(value_ instanceof String) w=YapodLib.getValue(o,(String)value_);
    if(w==FAKE) w=value_;
    if(w==null) w="";	

    Object r=FAKE;

    switch(op_)
    {
    case 0:
      if((w instanceof Number)&&(v instanceof Number))
	r=new Double(((Number)v).doubleValue()+((Number)w).doubleValue());
      else
      if((w instanceof String)||(v instanceof String))
	r=""+v+w;
      break;
    case 1:
      if((w instanceof Number)&&(v instanceof Number))
	r=new Double(((Number)v).doubleValue()-((Number)w).doubleValue());
      else
      if((w instanceof String)||(v instanceof String))
      {
	r=""+v;
	int i=((String)r).lastIndexOf(""+w);
	if(i>=0) r=((String)r).substring(0,i);
      }
      break;
    case 2:
      if((w instanceof Number)&&(v instanceof Number))
	r=new Double(((Number)v).doubleValue()*((Number)w).doubleValue());
      break;
    case 3:
      if((w instanceof Number)&&(v instanceof Number))
      {
	double d=((Number)w).doubleValue();
	if(d!=0.) r=new Double(((Number)v).doubleValue()/d);
	else      r=new Double(Double.NaN);
      }
      break;
    case 4:
      if((w instanceof Boolean)&&(v instanceof Boolean))
	r=Boolean.valueOf(  ((Boolean)w).booleanValue()
		      &&((Boolean)v).booleanValue());
      break;
    case 5:
      if((w instanceof Boolean)&&(v instanceof Boolean))
	r=Boolean.valueOf(  ((Boolean)w).booleanValue()
		      ||((Boolean)v).booleanValue());
      break;
    case 6:
      if((w instanceof Boolean)&&(v instanceof Boolean))
	r=Boolean.valueOf(  ((Boolean)w).booleanValue()
		      ^ ((Boolean)v).booleanValue());
      break;
    }

    return r;
  }

  @Override
  public String toString()
    { return "math(\""+field_+OPERATORS[op_]+value_+"\","+getPrevious()+")"; }
}
