/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         YapodMapQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Vector;

public class YapodMapQuery
       extends YapodAbstractComputeQuery
{
  private YapodAbstractQuery query_;

  public YapodMapQuery
    (YapodAbstractQuery _query, YapodQuery _previous)
  {
    super(_previous);

    if(_query==null)
      throw new IllegalArgumentException("_query is null");

    query_=_query;
  }

  @Override
  protected Object compute(Object _o)
  {
    Object o=_o;
    Object r=FAKE;

    if(o instanceof Enumeration)
      o=new YapodStaticVectorQuery(YapodLib.toVector((Enumeration)o));
    if(o instanceof Vector)
      o=new YapodStaticVectorQuery((Vector)o);
    if(o instanceof Object[])
      o=new YapodStaticArrayQuery((Object[])o);
    if(o instanceof double[])
      o=new YapodStaticDoubleArrayQuery((double[])o);

    if(o instanceof YapodQuery)
    {
      query_.setPrevious((YapodQuery)o);
      r=YapodLib.toVector(query_.getResult());
    }

    return r;
  }

  @Override
  public String toString()
  {
    return "map("+query_+","+getPrevious()+")";
  }
}
