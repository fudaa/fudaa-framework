/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodNumberQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;

public class YapodNumberQuery
       extends YapodClassQuery
{
  public YapodNumberQuery
    (YapodQuery _previous)
  {
    super(Number.class,_previous);
  }

  @Override
  public String toString()
  {
    return "number("+getPrevious()+")";
  }
}
