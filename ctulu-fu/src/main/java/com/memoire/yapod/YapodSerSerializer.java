/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodSerSerializer.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Hashtable;

public class YapodSerSerializer
       implements YapodSerializer
{
  private ObjectOutputStream out_;

  public YapodSerSerializer()
  {
  }

  @Override
  public String extension()
  {
    return "ser";
  }

  @Override
  public synchronized void open(OutputStream _out)
       throws IOException
  {
    OutputStream out=_out;
    if(!(out instanceof BufferedOutputStream))
      out=new BufferedOutputStream(out,BUFFER_SIZE);
    out_=new ObjectOutputStream(out);
  }

  @Override
  public synchronized void close()
       throws IOException
  {
    out_.close();
  }

  @Override
  public synchronized void write(Object _o)
       throws IOException
  {
    out_.writeObject(_o);
  }

  @Override
  public synchronized void store
  (Object _o, Object _dbcx, Hashtable _ref, Hashtable _obj)
  {
  }
}
