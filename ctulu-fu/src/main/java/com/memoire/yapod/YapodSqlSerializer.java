/**
 * @modification 14/03/01
 * @statut unstable
 * @file YapodSqlSerializer.java
 * @version 0.17
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 */
package com.memoire.yapod;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.StringTokenizer;

public class YapodSqlSerializer
    implements YapodSerializer {
  private static final String NULL = "\'\\N\'";
  private static final char Q = '\'';
  PrintWriter out;
  Hashtable ref; // (obj,id)
  Hashtable obj; // (id,obj)
  Object root;
  int indentation;

  public YapodSqlSerializer() {
  }

  @Override
  public String extension() {
    return "sql";
  }

  public String indente() {
    String r = "";
    for (int i = 0; i < indentation; i++) r += "  ";
    return r;
  }

  public void warning(String _s) {
    System.err.println("Yapod SQL warning: " + _s);
  }

  @Override
  public synchronized void open(OutputStream _out) throws IOException {
    ref = new Hashtable();
    obj = new Hashtable();
    out = new PrintWriter(_out, true);

    StringBuffer r = new StringBuffer();
    indentation = 0;
    writeHeader(r);
    out.print(r.toString());

  }

  @Override
  public synchronized void close() throws IOException {
    StringBuffer r = new StringBuffer();
    writeFooter(r);
    out.print(r.toString());


    out.close();
  }

  @Override
  public synchronized void store
      (Object _o, Object _dbcx, Hashtable _ref, Hashtable _obj)
      throws SQLException {
    String k = (String) _ref.get(_o);
    if (k == null) {
      throw new IllegalArgumentException
          ("object not referenced: " + _o);
    }

    Hashtable old_ref = ref;
    Hashtable old_obj = obj;
    ref = _ref;
    obj = _obj;
    root = _o;
    indentation = 0;

    StringBuffer r = new StringBuffer();
    System.err.println("storing " + getId(_o));
    write0(_o, r);
    StringTokenizer tok = new StringTokenizer(new String(r), "\n");
    while (tok.hasMoreTokens()) {
      String l = tok.nextToken();
      l = l.substring(0, l.length() - 1);
      try (Statement stmt = ((Connection) _dbcx).createStatement()) {
        stmt.executeUpdate(l);
      }
    }

    root = null;
    //ref.put(_o,k);
    //obj.put(k,_o);
    ref = old_ref;
    obj = old_obj;
  }

  protected String getId(Object _o) {
    if (_o == null) {
      return NULL;
    }

    String r = (String) ref.get(_o);

    if (r == null) {
      int i = 1;
      do {
        /*
        r=_o.getClass().getName();
        int j=r.lastIndexOf('.');
        if(j>=0) r=r.substring(j+1);
        r=r.toLowerCase()+"-"+i;
        */
        if (_o.getClass().isArray()) {
          r = "RT-" + i;
        } else {
          r = "RO-" + i;
        }
        i++;
      } while (obj.get(r) != null);

      ref.put(_o, r);
      obj.put(r, _o);
    }

    return Q + r + Q;
  }

  @Override
  public final synchronized void write(Object _o) throws IOException {
    StringBuffer r = new StringBuffer();
    write0(_o, r);
    out.print(r.toString());
  }

  // Write parts into StringBuffer

  protected synchronized void writeHeader(StringBuffer _sb) {
    _sb.append("-- Yapod SQL DB mapping\n\n");
    _sb.append("USE yapod;\n");
  }

  protected synchronized void writeFooter(StringBuffer _sb) {
    _sb.append("\n-- End Yapod SQL\n");
  }

  protected synchronized void writeNull(StringBuffer _sb) {
    _sb.append(NULL + ',' + NULL + ',' + NULL + ");\n");
  }

  protected synchronized void writeBoolean(Boolean _o, StringBuffer _sb) {
    _sb.append(NULL + ',' +
        getTypeAttr(_o) + ',' +
        Q + _o.toString() + Q + ");\n");
  }

  protected synchronized void writeNumber(Number _o, StringBuffer _sb) {
    _sb.append(NULL + ',' +
        getTypeAttr(_o) + ',' +
        Q + _o.toString() + Q + ");\n");
  }

  protected synchronized void writeCharacter(Character _o, StringBuffer _sb) {
    _sb.append(NULL + ',' +
        getTypeAttr(_o) + ',' +
        Q + (int) _o.charValue() + Q + ");\n");
  }

  protected synchronized void writeString(String _o, StringBuffer _sb) {
    _sb.append(NULL + ',' +
        getTypeAttr(_o) + ',' +
        Q + toXmlCharset(_o) + Q + ");\n");
  }

  protected synchronized void writeArray(Object _o, StringBuffer _sb) {
    _sb.append(getId(_o) + ',' +
        NULL + ',' +
        NULL + ");\n");
    _sb.append("INSERT INTO object VALUES (" +
        getId(_o) + ',' +
        getTypeAttr(_o) + ',' +
        getDepthAttr(_o) + ',' +
        Q + java.lang.reflect.Array.getLength(_o) + Q + ");\n");

    for (int i = 0; i < java.lang.reflect.Array.getLength(_o); i++)
      writeArrayElement(_o, i, java.lang.reflect.Array.get(_o, i), _sb);
  }

  protected synchronized void writeObject(Object _o, StringBuffer _sb) {
    Class c = _o.getClass();

    if (!(_o instanceof Serializable)) {
      warning("Not serializable " + c.getName());
    }

    try {
      c.getMethod
          ("writeObject", new Class[]{ObjectOutputStream.class});
      warning("Specific serialization for " + c.getName());
    } catch (NoSuchMethodException ex) {
    }

    synchronized (this) {
      _sb.append("INSERT INTO object VALUES (" +
          getId(_o) + ',' +
          getTypeAttr(_o) + ',' +
          NULL + ',' +
          NULL + ");\n");
      Field[] fields = YapodLib.getAllFields(_o.getClass());
      for (int i = 0; i < fields.length; i++)
        writeField(_o, fields[i], _sb);
    }
  }

  protected synchronized void writeReference(Object _o, StringBuffer _sb) {
    _sb.append(getId(_o) + ',' +
        NULL + ',' +
        NULL + ");\n");
  }

  protected void writeArrayElement(Object _array, int _index, Object _value, StringBuffer _sb) {
    _sb.append("INSERT INTO field VALUES (" +
        getId(_array) + ',' +
        Q + _index + Q + ',');
    write(_value, _sb);
  }

  protected void writeField(String _name, Object _value, StringBuffer _sb) {
    _sb.append(
        Q + _name + Q + ",");
    write(_value, _sb);
  }

  // General write

  protected synchronized void write(Object _o, StringBuffer _sb) {
    if (_o == null) {
      writeNull(_sb);
    } else if (_o instanceof Boolean) {
      writeBoolean((Boolean) _o, _sb);
    } else if (_o instanceof Number) {
      writeNumber((Number) _o, _sb);
    } else if (_o instanceof Character) {
      writeCharacter((Character) _o, _sb);
    } else if (_o instanceof String) {
      writeString((String) _o, _sb);
    } else if (_o.getClass().isArray()) {
      writeArray(_o, _sb);
    } else {
      if (ref.get(_o) == null) {
        writeReference(_o, _sb);
        if (root == _o) {
          writeObject(_o, _sb);
        }
      } else {
        writeReference(_o, _sb);
      }
    }
  }

  protected synchronized void write0(Object _o, StringBuffer _sb) {
    if (_o == null) {
      throw new RuntimeException("write0: not object type : " + _o);
    } else if (_o instanceof Boolean) {
      throw new RuntimeException("write0: not object type : " + _o);
    } else if (_o instanceof Number) {
      throw new RuntimeException("write0: not object type : " + _o);
    } else if (_o instanceof Character) {
      throw new RuntimeException("write0: not object type : " + _o);
    } else if (_o instanceof String) {
      throw new RuntimeException("write0: not object type : " + _o);
    } else if (_o.getClass().isArray()) {
      ;
    }
    //     writeArray(_o,_sb);
    else {
      writeObject(_o, _sb);
    }
  }

  protected void writeField(Object _o, Field _f, StringBuffer _sb) {
    if (!YapodLib.isValid(_f)) {
      return;
    }

    YapodLib.setAccessible(_f, true);

    Object FAKE_VALUE = new Object();
    Object value = FAKE_VALUE;

    try {
      value = _f.get(_o);
    } catch (IllegalAccessException ex1) {
      String fn = _f.getName();

      Method m = null;

      try {
        m = _o.getClass().getMethod(fn, new Class[0]);
      } catch (NoSuchMethodException ex2) {
      }

      if ((m == null) && (fn.endsWith("_"))) {
        fn = fn.substring(0, fn.length() - 1);
        try {
          m = _o.getClass().getMethod(fn, new Class[0]);
        } catch (NoSuchMethodException ex3) {
        }
      }

      if (m == null) {
        fn = "get" + fn.substring(0, 1).toUpperCase() + fn.substring(1);
        try {
          m = _o.getClass().getMethod(fn, new Class[0]);
        } catch (NoSuchMethodException ex4) {
        }
      }

      if (m != null) {
        try {
          value = m.invoke(_o, new Object[0]);
        } catch (IllegalAccessException ex5) {
          //nothing to do
        } catch (InvocationTargetException ex6) {
          //nothing to do
        }
      }
    }

    if (value == FAKE_VALUE) {
      warning("No access to field " + _f.getName() +
          " in " + _o.getClass());
      value = null;
    }

    _sb.append("INSERT INTO field VALUES (" +
        getId(_o) + ',');
    writeField(_f.getName(), value, _sb);
  }

  private String getTypeAttr(Object _value) {
    if (_value == null) {
      return NULL;
    }

    StringBuffer r = new StringBuffer(30);

    Class c = _value.getClass();
    while (c.isArray()) c = c.getComponentType();

    r.append(Q + c.getName() + Q);
    return r.toString();
  }

  private String getDepthAttr(Object _value) {
    if (_value == null) {
      return NULL;
    }

    Class c = _value.getClass();
    while (!c.isArray()) return NULL;

    StringBuffer r = new StringBuffer(3);

    int n = 0;
    while (c.isArray()) {
      c = c.getComponentType();
      n++;
    }
    r.append("" + Q + n + Q);

    return r.toString();
  }

  protected static final String toXmlCharset(char _c) {
    String r;

    if (_c == '<') {
      r = "&lt;";
    } else if (_c == '>') {
      r = "&gt;";
    } else if (_c == ' ') {
      r = "&#32;";
    } else if (_c == '&') {
      r = "&amp;";
    } else if (_c == '"') {
      r = "&quot;";
    } else if (Character.isISOControl(_c) || (_c == '\'') || (_c == '\"') || (_c > 254)) {
      r = "&#" + (int) _c + ";";
    } else {
      r = "" + _c;
    }
    return r;
  }

  protected static final String toXmlCharset(Character _c) {
    return toXmlCharset(_c.charValue());
  }

  protected static final String toXmlCharset(String _s) {
    StringBuffer r = new StringBuffer();
    int l = _s.length();

    for (int i = 0; i < l; i++) {
      char c = _s.charAt(i);
      r.append(toXmlCharset(c));
    }

    return r.toString();
  }


}
