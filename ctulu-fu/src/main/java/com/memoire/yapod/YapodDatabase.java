/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut unstable
 * @file YapodDatabase.java
 * @version 0.17
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 */
package com.memoire.yapod;

import com.memoire.fu.FuLog;

import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.LineNumberReader;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class YapodDatabase
    // implements Serializable
{
  protected String dir_;      // path to directory
  protected int delay_;    // delay_ in seconds between automatic savings
  protected boolean opened_;
  protected transient Vector objects_;  // list of collected objects
  protected transient Hashtable names_;    // name   -> object
  protected transient Hashtable binds_;    // object -> name
  protected transient String protocol_;
  protected transient YapodSerializer serializer_;
  protected transient YapodDeserializer deserializer_;
  protected transient Object dbcx_;
  transient Autosave as_;

  private YapodDatabase() {
  }

  private YapodDatabase(String _dir) {
    dir_ = _dir;
    delay_ = 10;
    protocol_ = extractProtocol(_dir);
    if ("jdbc".equals(protocol_)) {
      serializer_ = new YapodSqlSerializer();
      deserializer_ = null;
      try {
        Class.forName("org.gjt.mm.mysql.Driver").newInstance();
        dbcx_ = DriverManager.getConnection(dir_);
        opened_ = true;
      } catch (Throwable t) {
        opened_ = false;
      }
      // MEMO: dir_="jdbc:mysql://marina:3306/yapod?user=boony&password=doebfu";
    } else if ("xml".equals(protocol_)) {
      serializer_ = new YapodXmlSerializer();
      deserializer_ = new YapodXmlDeserializer();
      dbcx_ = dir_.substring(dir_.indexOf(':') + 1);
      opened_ = true;
    }
    objects_ = new Vector();
    names_ = new Hashtable();
    binds_ = new Hashtable();
  }

  // autosave

  public void startAutoSave() {
    if (!opened_) {
      throw new RuntimeException
          ("database " + dir_ + " is closed");
    }

    if (as_ == null) {
      as_ = new Autosave();
      as_.start();
    }
  }

  public void stopAutoSave() {
    if (!opened_) {
      throw new RuntimeException
          ("database " + dir_ + " is closed");
    }

    if (as_ != null) {
      as_.active = false;
      as_ = null;
    }
  }

  public int getAutoSaveDelay() {
    return delay_;
  }

  public void setAutoSaveDelay(int _delay) {
    if (delay_ < 0) {
      throw new IllegalArgumentException
          ("delay must be equal or greater than 1 (" +
              _delay + " is not valid)");
    }
    delay_ = _delay;
  }

  // graph

  private Hashtable graph() {
    int l = objects_.size();
    Hashtable t = new Hashtable(3 * l + 1);

    for (int i = 0; i < l; i++)
      __graph0(objects_.elementAt(i), t);

    return t;
  }

  private void __graph0(Object _o, Hashtable _table) {
    if (_o == null) {
      return;
    }
    if (_table.get(_o) != null) {
      return;
    }
    if (_o instanceof Boolean) {
      return;
    }
    if (_o instanceof Character) {
      return;
    }
    if (_o instanceof Number) {
      return;
    }
    if (_o instanceof String) {
      return;
    }

    String k = (String) binds_.get(_o);
    if (k == null) {
      int n = 0;
      do {
        k = "oid-" + (System.currentTimeMillis() % 10000000000l);
        n++;
      }
      while (names_.get(k) != null);
      names_.put(k, _o);
      binds_.put(_o, k);
    }

    Class c = _o.getClass();
    if (c.isArray()) {
      int l = java.lang.reflect.Array.getLength(_o);
      for (int i = 0; i < l; i++)
        __graph0(java.lang.reflect.Array.get(_o, i), _table);
    } else {
      _table.put(_o, k);

      Field[] fields = YapodLib.getAllFields(c);
      for (int i = 0; i < fields.length; i++) {
        if (!YapodLib.isValid(fields[i])) {
          break;
        }
        try {
          YapodLib.setAccessible(fields[i], true);
          __graph0(fields[i].get(_o), _table);
        } catch (Exception ex) {
        }
      }
    }
  }

  // management

  public synchronized void add(Object _object) {
    if (_object == null) {
      throw new IllegalArgumentException("_object is null");
    }

    if (!opened_) {
      throw new RuntimeException
          ("database " + dir_ + " is closed");
    }

    if (!objects_.contains(_object)) {
      objects_.addElement(_object);
    }
  }

  /*
  public synchronized void remove(Object _object)
  {
    if(_object==null)
      throw new IllegalArgumentException("_object is null");

    if(!objects_.contains(_object))
      throw new IllegalArgumentException("_object is not in database");

    objects_.removeElement(_object);

    Object name=binds_.get(_object);
    if(name!=null)
    {
      binds_.remove(_object);
      names_.remove(name);
    }
  }
  */

  /*
  public synchronized boolean contains(Object _object)
  {
    if(_object==null)
      throw new IllegalArgumentException("_object is null");

    return objects_.contains(_object);
  }
  */

  // naming

  public synchronized void bind(String _name, Object _object) {
    if (_name == null) {
      throw new IllegalArgumentException("_name is null");
    }

    if (_object == null) {
      throw new IllegalArgumentException("_object is null");
    }

    if (!opened_) {
      throw new RuntimeException
          ("database " + dir_ + " is closed");
    }

    if (!objects_.contains(_object)) {
      throw new IllegalArgumentException("_object is not in database");
    }

    Object o = find(_name);
    if ((o != null) && (o != _object)) {
      throw new IllegalArgumentException
          ("_name \"" + _name + "\" is already used");
    }

    names_.put(_name, _object);
    binds_.put(_object, _name);
  }

  /*
  public synchronized void unbind(String _name, Object _object)
  {
    if(_name==null)
      throw new IllegalArgumentException("_name is null");

    if(_object==null)
      throw new IllegalArgumentException("_object is null");

    if(!opened_)
      throw new RuntimeException
	("database "+dir_+" is closed");

    if(!objects_.contains(_object))
      throw new IllegalArgumentException("_object is not in database");

    Object o=find(_name);
    if((o!=null)&&(o!=_object))
      throw new IllegalArgumentException
	("_name \""+_name+"\" is already used");

    names_.remove(_name);
    binds_.remove(_object);
  }
  */

  public synchronized Object find(String _name) {
    if (_name == null) {
      throw new IllegalArgumentException("_name is null");
    }

    if (!opened_) {
      throw new RuntimeException
          ("database " + dir_ + " is closed");
    }

    Object o = names_.get(_name);

    if (o == null) {
      Hashtable nmo = new Hashtable();

      try {
        o = deserializer_.retrieve(_name, dbcx_, binds_, names_, nmo);
      } catch (Exception ex) {
      }

      //if(o!=null) add(o);
      Enumeration e = nmo.keys();
      while (e.hasMoreElements()) {
        String k = (String) e.nextElement();
        Object v = nmo.get(k);
        add(v);
        bind(k, v);
      }
    }

    /*
    if(o==null)
      throw new IllegalArgumentException
	("_name \""+_name+"\" is not used in database");
    */

    return o;
  }

  public String guess(String _id) {
    String r = null;
    Object o = names_.get(_id);

    if (o != null) {
      r = o.getClass().getName();
    } else {
      if ("jdbc".equals(protocol_)) {
        try (Statement stmt = ((Connection) dbcx_).createStatement()) {

          try (ResultSet rs = stmt.executeQuery(
              "SELECT type FROM object WHERE oid=" + _id)) {
            if (rs.next()) {
              r = rs.getString("type");
            }
          }
        } catch (Exception ex) {
          FuLog.warning(ex);
        }
      } else if ("xml".equals(protocol_)) {
        try (LineNumberReader f = new LineNumberReader
            (new FileReader(dbcx_ + File.separator + _id + ".xml"))) {
          String l = null;
          for (int i = 0; i < 5; i++) l = f.readLine();
          int i = l == null ? -1 : l.indexOf("<object type=\"");
          if (i >= 0 && l != null) {
            l = l.substring(i + 14);
            r = l.substring(0, l.indexOf('"'));
          }
        } catch (Exception ex) {
          FuLog.warning(ex);
        }
      }
    }

    return r;
  }

  // query

  public synchronized String[] index() {
    if (!opened_) {
      throw new RuntimeException
          ("database " + dir_ + " is closed");
    }
    String[] r = new String[0];

    if ("jdbc".equals(protocol_)) {
      try (Statement stmt = ((Connection) dbcx_).createStatement()) {
        Vector vr = new Vector();
        try (ResultSet rs = stmt.executeQuery(
            "SELECT oid FROM object")) {

          while (rs.next())
            vr.addElement(rs.getString("oid"));
        }
        r = new String[vr.size()];
        for (int i = 0; i < r.length; i++)
          r[i] = (String) vr.elementAt(i);
      } catch (Exception ex) {
      }
    } else if ("xml".equals(protocol_)) {
      File d = new File((String) dbcx_);
      FilenameFilter f = (_dir, _name) -> _name.endsWith(".xml");
      r = d.list(f);
      for (int i = 0; i < r.length; i++)
        r[i] = r[i].substring(0, r[i].length() - 4);
    }

    return r;
  }

  public synchronized Vector references() {
    String[] n = index();
    Vector r = new Vector(n.length);

    for (int i = 0; i < n.length; i++) {
      YapodReference o = new YapodReference(this, n[i]);
      if (o.isValid()) {
        r.addElement(o);
      }
    }

    return r;
  }

  public YapodQuery createQuery() {
    return new YapodQuery() {
      @Override
      public Enumeration getResult() {
        return references().elements();
      }

      @Override
      public String toString() {
        return "database(\"" + dir_ + "\")";
      }
    };
  }

  // static

  public static synchronized boolean exists(String _dir) {
    boolean r = false;
    String protocol = extractProtocol(_dir);

    if ("jdbc".equals(protocol)) {
      try {
        Class.forName("org.gjt.mm.mysql.Driver").newInstance();
        try (Connection dbcx = DriverManager.getConnection(_dir)) {
          r = (dbcx != null);
        }
      } catch (Exception ex) {
        FuLog.error(ex);
      }
    } else if ("xml".equals(protocol)) {
      File d = new File(_dir.substring(_dir.indexOf(':') + 1));
      r = d.isDirectory() && d.canRead();
    }

    return r;
  }

  public static synchronized YapodDatabase open(String _dir) {
    if (!exists(_dir)) {
      throw new RuntimeException
          ("Can't open database " + _dir + ", doesn't exist");
    }

    return new YapodDatabase(_dir);
  }

  // not static

  public synchronized boolean isOpened() {
    return opened_;
  }

  public synchronized void close() {
    if (!opened_) {
      throw new RuntimeException
          ("database " + dir_ + " is closed");
    }

    stopAutoSave();
    if (canSave()) {
      save();
    }
    if ("jdbc".equals(protocol_)) {
      try {
        ((Connection) dbcx_).close();
      } catch (Exception ex) {
        FuLog.warning(ex);
      }
    }
    opened_ = false;
  }

  public synchronized boolean canSave() {
    if ("jdbc".equals(protocol_)) {
      return opened_;
    } else if ("xml".equals(protocol_)) {
      File d = new File((String) dbcx_);
      return opened_ && d.isDirectory() && d.canWrite();
    }
    return false;
  }

  public synchronized void save() {
    if (!opened_) {
      throw new RuntimeException
          ("database " + dir_ + " is closed");
    }

    if (!canSave()) {
      throw new RuntimeException("Can't write " + dir_);
    }

    Hashtable dat = graph();
    Hashtable ref = new Hashtable(dat.size());
    Hashtable obj = new Hashtable(dat.size());

    for (Enumeration e = dat.keys(); e.hasMoreElements(); ) {
      Object o = e.nextElement();
      String k = (String) dat.get(o);
      ref.put(o, k);
      obj.put(k, o);
    }

    for (Enumeration e = dat.keys(); e.hasMoreElements(); ) {
      Object o = e.nextElement();

      try {
        serializer_.store(o, dbcx_, ref, obj);
      } catch (Exception ex) {
        FuLog.warning("Couldn't save " + o, ex);
      }
    }

    FuLog.warning
        ("Database " + dir_ + " saved (" + dat.size() + " objects)");
  }

  private final class Autosave extends Thread {
    public boolean active;

    public Autosave() {
      setPriority(Thread.MIN_PRIORITY);
      active = true;
    }

    @Override
    public void run() {
      while (active) {
        if (!opened_) {
          throw new RuntimeException
              ("database " + dir_ + " is closed");
        }

        if (!canSave()) {
          throw new RuntimeException("Can't write " + dir_);
        }
        save();

        try {
          sleep(1000l * delay_);
        } catch (Exception ex) {
          FuLog.warning(ex);
        }
        Thread.yield();
      }
    }
  }

  private static String extractProtocol(String _dir) {
    String r = "xml";

    if (_dir != null) {
      int i = _dir.indexOf(':');
      if (i >= 0) {
        r = _dir.substring(0, i);
      }
    }

    return r;
  }
}
