/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         YapodDummyQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public class YapodDummyQuery
       implements YapodQuery, Enumeration
{
  @Override
  public final Enumeration getResult()
    { return this; }

  @Override
  public final boolean hasMoreElements()
    { return false; }

  @Override
  public final Object nextElement()
    { throw new NoSuchElementException(); }
}
