/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodFlattenQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Vector;

public class YapodFlattenQuery
       implements YapodQuery
{
  private YapodQuery previous_;

  public YapodFlattenQuery
    (YapodQuery _previous)
  {
    setPrevious(_previous);
  }

  public final YapodQuery getPrevious()
    { return previous_; }

  public final synchronized void setPrevious(YapodQuery _query)
  {
    if(_query==null)
      throw new IllegalArgumentException("_query is null");

    previous_=_query;
  }

  @Override
  public Enumeration getResult()
  {
    Vector r=new Vector();
    for(Enumeration e=getPrevious().getResult(); e.hasMoreElements(); )
    {
      Object o=e.nextElement();

      if(o instanceof Vector)
      {
	Vector v=(Vector)o;
	int    l=v.size();
	for(int i=0;i<l;i++)
	  r.addElement(v.elementAt(i));
      }
      else
      if(o instanceof Object[])
      {
	Object[] v=(Object[])o;
	for(int i=0;i<v.length;i++)
	  r.addElement(v[i]);
      }
      else r.addElement(o);
    }
    return r.elements();
  }

  @Override
  public String toString()
  {
    return "flatten("+getPrevious()+")";
  }
}
