/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodCrossQuery.java
 * @version      0.17
 * @author       Nicolas Chevalier
 * @email        nicolas.chavalier@etu.utc.fr
 * @license      GNU General Public License 2 (GPL2)
 * (c)1999-2001 Nicolas Chevalier
 * nicolas.chavalier@etu.utc.fr
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Vector;

public class YapodCrossQuery
       implements YapodQuery
{
  private YapodQuery[] q_;
  
  // Constructeur
  public YapodCrossQuery(YapodQuery[] _queries)
  {
    if(_queries==null)
      throw new IllegalArgumentException("_queries is null");
    q_=_queries;
  }
  
  @Override
  public Enumeration getResult()
  {
    int         i =0;
    Vector      v1=new Vector();
    Vector      vInter;
    Enumeration e;
    Object      obj;
    
    // Pour chaque element du tableau q_
    for(int j=0; j<q_.length; j++)
    {
      i=0;
      e=q_[j].getResult();
      
      // Le resultats de chaque element j de q_ (pointe par e) est
      // explore par l'indice i.
      while(e.hasMoreElements())
      {
        // Ajoute un nouveau vecteur dans v1 si necessaire et
        // vInter pointe vers l'element i de e.
        if(v1.size()<=i)
	{
          vInter=new Vector();
          v1.addElement(vInter);
        }
        else
	  vInter=(Vector)v1.elementAt(i);

        // Exploration de l'element i de e pour ajouter chacun de
	// ses elements dans vInter
        obj=e.nextElement();
        
        // Si obj est un vecteur : ajoute chacun de ses elements dans vInter
        if(obj instanceof Vector)
	{
	  Vector prov=(Vector)obj;
	  int    l=prov.size();
	  for(int k=0; k<l; k++)
	    vInter.addElement(prov.elementAt(k));
        }
        else
        // Si obj est un tableau d'objets : ajoute chacun de ses
	// elements dans vInter
        if(obj instanceof Object[])
	{
          Object[] prov=(Object[])obj;
          for(int k=0; k<prov.length; k++)
	    vInter.addElement(prov[k]);
        }
        else
	  // Sinon ajoute obj dans vInter.
	  vInter.addElement(obj);
        
        i++;
      }
    }
  
    return v1.elements();
  }
  
  public static void main(String[] args)
  {
    YapodQuery[] queries = new YapodQuery[2];
    Object[] tab1 = new Object[3];
    Object[] tab2 = new Object[2];
    
    Object[] tabTab1 = { tab1 };
    Object[] tabTab2 = { tab2 };
    
    for ( int i=0 ; i<3 ; i++ ) tab1[i]=new Integer((i+1));
    for ( int i=0 ; i<2 ; i++ ) tab2[i]=new Integer((i+1));
    
    YapodQuery qTabTab1 = new YapodStaticArrayQuery(tabTab1);
    YapodQuery qTabTab2 = new YapodStaticArrayQuery(tabTab2);
    
    queries[0] = new YapodUnionQuery(qTabTab1, qTabTab2);
    queries[1] = new YapodUnionQuery(qTabTab2, qTabTab1);
    
    YapodQuery q0 = new YapodCrossQuery(queries);
    YapodLib.print(q0.getResult());    
  }
}
