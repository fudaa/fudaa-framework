/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodSerDeserializer.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.Hashtable;

public class YapodSerDeserializer
       implements YapodDeserializer
{
  private ObjectInputStream in_;

  public YapodSerDeserializer()
  {
  }

  @Override
  public String extension()
  {
    return "ser";
  }

  @Override
  public void open(InputStream _in)
       throws IOException
  {
    InputStream in=_in;
    if(!(in instanceof BufferedInputStream))
      in=new BufferedInputStream(in,BUFFER_SIZE);
    in_=new ObjectInputStream(in);
  }

  @Override
  public void close()
       throws IOException
  {
    in_.close();
  }

  @Override
  public Object read()
       throws ClassNotFoundException, IOException
  {
    return in_.readObject();
  }

  @Override
  public synchronized Object retrieve
  (String _id, Object dbcx_, Hashtable _ref, Hashtable _obj, Hashtable _nmo)
  {
    return null;
  }
}
