/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodLib.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Enumeration;
import java.util.Vector;

public class YapodLib
       implements YapodConstants
{
  public static final String replace(String _s, String _a, String _b)
  {
    String r=_s;
    int i=0;
    while((i=r.indexOf(_a,i))>=0)
    {
      r=r.substring(0,i)+_b+r.substring(i+_a.length());
      i=i+_b.length();
    }
    //System.err.println("replace: "+r);
    return r;
  }

  public static final Object first(Enumeration _e)
  {
    Object r=null;
    if(_e.hasMoreElements()) r=_e.nextElement();
    return r;
  }

  public static final Object first(Vector _v)
  {
    Object r=null;
    if(_v.size()>0) r=_v.elementAt(0);
    return r;
  }

  public static final Object first(Object[] _a)
  {
    Object r=null;
    if(_a.length>0) r=_a[0];
    return r;
  }

  public static final Object[] toArray(Enumeration _e)
  {
    return toArray(toVector(_e));
  }

  public static final Vector toVector(Enumeration _e)
  {
    Vector r=new Vector();
    while(_e.hasMoreElements())
    {
      Object o=_e.nextElement();
      if(o!=FAKE) r.addElement(o);
    }
    return r;
  }

  public static final Object[] toArray(Vector _v)
  {
    int      t=_v.size();
    Object[] r=new Object[t];
    for(int i=0;i<t;i++) r[i]=_v.elementAt(i);
    return r;
  }

  public static final boolean isValid(Field _field)
  {
    int modifiers=_field.getModifiers ();
    return (!Modifier.isTransient(modifiers) &&
	    !Modifier.isStatic   (modifiers) &&
	    !Modifier.isFinal    (modifiers));
  }

  public static final Object getValue(Object _o, String _field)
  {
    if(_o==null) return _o;
    if("this".equals(_field)) return _o;

    if(_o instanceof YapodAssoc)
      return ((YapodAssoc)_o).get(_field);

    Object value=FAKE;
    Class  c    =_o.getClass();

    try
    {
      Field f=getField(c,_field);
      value=f.get(_o);
    }
    catch(Exception ex1)
    {
      String fn=_field;
      // System.err.println("Access error: "+fn+" in "+c);

      Method m=null;

      try { m=c.getMethod(fn,new Class[0]); }
      catch(NoSuchMethodException ex2) { }

      if((m==null)&&(fn.endsWith("_")))
      {
	fn=fn.substring(0,fn.length()-1);
	try { m=c.getMethod(fn,new Class[0]); }
	catch(NoSuchMethodException ex3) { }
      }

      if(m==null)
      {
	fn="get"+fn.substring(0,1).toUpperCase()+fn.substring(1);
	try { m=c.getMethod(fn,new Class[0]); }
	catch(NoSuchMethodException ex4) { }
      }

      if(m!=null)
      {
	try
	{
	  value=m.invoke(_o,new Object[0]);
	  // System.err.println("Using method "+fn+"()");
	}
        catch(IllegalAccessException ex5) { }
        catch(InvocationTargetException ex6) { }
      }
    }

    return value;
  }

  // accessible

  public static final boolean isAccessible(Member _m)
  {
    boolean r=false;

    // JDK12
    // _m.isAccessible();
    try
    {
      Method m=_m.getClass().getMethod
	("isAccessible",new Class[] { });
      Object o=m.invoke(_m,new Object[] { });
      r=Boolean.TRUE.equals(o);
    }
    catch(Exception ex)
    {
      //System.err.println("### can not invoke "+ex.getMessage());
      //System.err.println(""+ex.getClass().getName());
    }

    return r;
  }

  public static final void setAccessible(Member _m, boolean _b)
  {
    // JDK12
    // _m.setAccessible(true);
    try
    {
      Method m=_m.getClass().getMethod
	("setAccessible",new Class[] { Boolean.TYPE } );
      m.invoke(_m,new Object[] { (_b ? Boolean.TRUE : Boolean.FALSE) } );
    }
    catch(Exception ex)
    {
      //System.err.println("### can not invoke "+ex.getMessage());
      //System.err.println(""+ex.getClass().getName());
    }
  }

  private static final void getFields0(Class _class, Vector _v)
  {
    if(_class!=null)
    {
      getFields0(_class.getSuperclass(),_v);
      Field[] f=_class.getDeclaredFields();
      if(f!=null)
	for(int i=0;i<f.length;i++)
	  _v.addElement(f[i]);
    }
  }

  public static final Field[] getAllFields(Class _class)
  {
    Vector v=new Vector();
    getFields0(_class,v);

    int     l=v.size();
    Field[] r=new Field[l];
    for(int i=0;i<l;i++)
      r[i]=(Field)v.elementAt(i);

    return r;
  }

  public static final Field getField(Class _class, String _name)
  {
    Field[] f=getAllFields(_class);
    for(int i=f.length-1; i>=0; i--)
      if(f[i].getName().equals(_name))
	return f[i];
    return null;
  } 

  // debug

  public static final void print(YapodQuery q)
  {
    System.out.println(q);
  }

  public static final void print(String s,Enumeration e)
  {
    System.out.print(s);
    print(e);
  }

  public static final void print(Enumeration e)
  {
    StringBuffer r=new StringBuffer();
    boolean      p=true;

    r.append('[');
    while(e.hasMoreElements())
    {
      if(p) p=false; 
      else r.append(", ");
      r.append(e.nextElement());
    }
    r.append(']');

    System.out.println(r);
  }
}

