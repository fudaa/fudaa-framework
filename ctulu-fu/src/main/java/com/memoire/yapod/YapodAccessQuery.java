/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodAccessQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;

public class YapodAccessQuery
       extends YapodAbstractComputeQuery
{
  private String field_;

  public YapodAccessQuery
    (String _field, YapodQuery _previous)
  {
    super(_previous);

    if(_field==null)
      throw new IllegalArgumentException
	("_field is null");

    field_=_field;
  }

  @Override
  protected Object compute(Object _object)
  {
    return YapodLib.getValue(_object,field_);
  }

  @Override
  public String toString()
    { return "access(\""+field_+"\","+getPrevious()+")"; }
}
