/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         YapodClassQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;

public class YapodClassQuery
       extends YapodAbstractTestQuery
{
  private Class class_;

  public YapodClassQuery
    (Class _class, YapodQuery _previous)
  {
    super(_previous);
    class_=_class;
  }

  @Override
  protected boolean test(Object _object)
    { return class_.isInstance(_object); }

  @Override
  public String toString()
  {
    String n=class_.getName();
    int    i=n.lastIndexOf('.');
    if(i>=0) n=n.substring(i+1);
    return "class(\""+n+"\","+getPrevious()+")";
  }
}
