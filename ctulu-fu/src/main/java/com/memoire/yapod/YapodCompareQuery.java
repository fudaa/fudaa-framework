/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodCompareQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;

public class YapodCompareQuery
       extends YapodAbstractTestQuery
{
  private static String[] OPERATORS=new String[]
  {
    "==","!=","<",">","<=",">="
  };

  private int    op_;
  private Object field_;
  private Object value_;

  public YapodCompareQuery
    (Object _field, String _op, Object _value, YapodQuery _previous)
  {
    super(_previous);

    if(_field==null)
      throw new IllegalArgumentException
	("_field is null");

    /*
    if(_value==null)
      throw new IllegalArgumentException
	("_value is null");
    */

    int i=OPERATORS.length-1;
    while(i>=0)
    {
      if(OPERATORS[i].equals(_op)) break;
      i--;
    }

    if(i<0)
      throw new IllegalArgumentException
	("operator "+_op+" not reconized");

    op_   =i;
    field_=_field;
    value_=_value;
  }

  @Override
  protected boolean test(Object o)
  {
    Object v=field_;
    if(field_ instanceof String) v=YapodLib.getValue(o,(String)field_);
    if(v==FAKE) v=field_;

    Object w=value_;
    if(value_ instanceof String) w=YapodLib.getValue(o,(String)value_);
    if(w==FAKE) w=value_;

    boolean b=false;

    if(w!=null)
    {
      switch(op_)
      {
      case 0:
	if((w instanceof Number)&&(v instanceof Number))
	  b=((Number)w).doubleValue()==((Number)v).doubleValue();
	else
	if((w instanceof String)&&(v instanceof String))
	  b=((String)w).compareTo((String)v)==0;
	break;
      case 1:
	if((w instanceof Number)&&(v instanceof Number))
	  b=((Number)w).doubleValue()!=((Number)v).doubleValue();
	else
	if((w instanceof String)&&(v instanceof String))
	  b=((String)w).compareTo((String)v)!=0;
	break;
      case 2:
	if((w instanceof Number)&&(v instanceof Number))
	  b=((Number)w).doubleValue()>((Number)v).doubleValue();
	else
	if((w instanceof String)&&(v instanceof String))
	  b=((String)w).compareTo((String)v)>0;
	break;
      case 3:
	if((w instanceof Number)&&(v instanceof Number))
	  b=((Number)w).doubleValue()<((Number)v).doubleValue();
	else
	if((w instanceof String)&&(v instanceof String))
	  b=((String)w).compareTo((String)v)<0;
	break;
      case 4:
	if((w instanceof Number)&&(v instanceof Number))
	  b=((Number)w).doubleValue()>=((Number)v).doubleValue();
	else
	if((w instanceof String)&&(v instanceof String))
	  b=((String)w).compareTo((String)v)>=0;
	break;
      case 5:
	if((w instanceof Number)&&(v instanceof Number))
	  b=((Number)w).doubleValue()<=((Number)v).doubleValue();
	else
	  if((w instanceof String)&&(v instanceof String))
	    b=((String)w).compareTo((String)v)<=0;
	break;
      }
    }
    else // w==null
    {
      switch(op_)
      {
      case 0: b=(v==null); break;
      case 1: b=(v!=null); break;
      }
    }

    return b;
  }

  @Override
  public String toString()
    { return "compare(\""+field_+OPERATORS[op_]+value_+"\","+getPrevious()+")"; }
}
