/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodSimpleSerializer.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Hashtable;

public class YapodSimpleSerializer
       implements YapodSerializer
{
  PrintWriter out;
  Hashtable   ref;

  public YapodSimpleSerializer()
  {
  }

  @Override
  public String extension()
  {
    return "yss";
  }

  @Override
  public synchronized void open(OutputStream _out)
  {
    OutputStream outStr=_out;
    if(!(outStr instanceof BufferedOutputStream))
      outStr=new BufferedOutputStream(outStr,BUFFER_SIZE);
    ref=new Hashtable();
    out=new PrintWriter(outStr,true);
  }

  @Override
  public synchronized void close()
  {
    out.close();
  }

  private String getId(Object _o)
  {
    String r=(String)ref.get(_o);

    if(r==null)
    {
      int i=1;
      do
      {
	r=_o.getClass().getName();
	int j=r.lastIndexOf('.');
	if(j>=0) r=r.substring(j+1);
	r=r.toLowerCase()+i;
	i++;
      } while(ref.get(_o)!=null);
      ref.put(_o,r);
    }

    return r;
  }

  @Override
  public synchronized void write(Object _o)
  {
    StringBuffer r=new StringBuffer();

    synchronized(_o)
    {
      r.append("{\n");
      r.append("  id:    ");
      r.append(getId(_o));
      r.append('\n');
      r.append("  class: ");
      r.append(_o.getClass().getName());
      r.append('\n');

      Field[] fields=YapodLib.getAllFields(_o.getClass());
      for(int i=0;i<fields.length;i++)
	write(_o,fields[i],r);
    }

    r.append("}\n");
    out.print(r.toString());
  }

  protected void write(Object _o, Field _f, StringBuffer _b)
  {
    //boolean isPrivate  =Modifier.isPrivate  (_f.getModifiers());
    boolean isStatic   =Modifier.isStatic   (_f.getModifiers());
    boolean isTransient=Modifier.isTransient(_f.getModifiers());

    if(isStatic   ) return;
    if(isTransient) return;

    /*
    if(isPrivate)
    {
      System.err.println("### private field "+_f.getName()+
			 " in "+_o.getClass());
      return;
    }
    */

    Object FAKE_VALUE=new Object();
    Object value=FAKE_VALUE;

    try
    {
      value=_f.get(_o);
    }
    catch(IllegalAccessException ex1)
    {
      String fn=_f.getName();
      // System.err.println("Access error: "+fn+" in "+_o.getClass());

      Method m=null;

      try { m=_o.getClass().getMethod(fn,new Class[0]); }
      catch(NoSuchMethodException ex2) { }

      if((m==null)&&(fn.endsWith("_")))
      {
	fn=fn.substring(0,fn.length()-1);
	try { m=_o.getClass().getMethod(fn,new Class[0]); }
	catch(NoSuchMethodException ex3) { }
      }

      if(m==null)
      {
	fn="get"+fn.substring(0,1).toUpperCase()+fn.substring(1);
	try { m=_o.getClass().getMethod(fn,new Class[0]); }
	catch(NoSuchMethodException ex4) { }
      }

      if(m!=null)
      {
	try
	{
	  value=m.invoke(_o,new Object[0]);
	  // System.err.println("Using method "+fn+"()");
	}
        catch(IllegalAccessException ex5) { }
        catch(InvocationTargetException ex6) { }
      }
    }

    StringBuffer r=new StringBuffer(100);

    r.append("  field: ");
    r.append(_f.getType().getName());
    r.append(' ');
    r.append(_f.getName());

    if(value!=FAKE_VALUE)
    {
      r.append('=');
      if(value==null)
	r.append("null");
      else
      if(value instanceof Number)
	r.append(""+value);
      else
      if(value instanceof Character)
	r.append("'"+toAscii((Character)value)+"'");
      else
      if(value instanceof String)
	r.append("\""+toAscii((String)value)+"\"");
      else
	{ write(value); r.append("@"+getId(value)); }
    }

    r.append(";\n");
    _b.append(r);
  }

  @Override
  public synchronized void store
  (Object _o, Object _dbcx, Hashtable _ref, Hashtable _obj)
  {
  }

  private static final String toAscii(char _c)
  {
    String r;
    if(Character.isISOControl(_c)||(_c=='\'')||(_c=='\"')||(_c>254))
    {
      r="0000"+Integer.toHexString(_c).toUpperCase();
      r=r.substring(r.length()-4,r.length());
      r="\\u"+r;
    }
    else
      r=""+_c;
    return r;
  }
    
  private static final String toAscii(Character _c)
  {
    return toAscii(_c.charValue());
  }

  private static final String toAscii(String _s)
  {
    StringBuffer r=new StringBuffer();
    int          l=_s.length();

    for(int i=0;i<l;i++)
    {
      char c=_s.charAt(i);
      r.append(toAscii(c));
    }
    
    return r.toString();
  }

 /* public static void main(String[] args)
  {
    try
    {
      YapodSerializer out=new YapodSimpleSerializer();
      out.open(System.out);
      out.write(new YapodTestC2());
      out.close();
    }
    catch(IOException ex) { ex.printStackTrace(); }
  }*/
}
