/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodExternalQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.lang.reflect.Method;

public class YapodExternalQuery
       extends YapodAbstractComputeQuery
{
  private String   function_;
  private Method   method_;
  private Object[] params_;

  public YapodExternalQuery
    (String _function, YapodQuery _previous)
    { this(_function,new Object[0],_previous); }

  public YapodExternalQuery
    (String _function, Object[] _params, YapodQuery _previous)
  {
    super(_previous);

    if(_params==null)
      throw new IllegalArgumentException
	("_params is null");

    int i=_function.lastIndexOf('.');
    if(i<0)
      throw new IllegalArgumentException
	("function "+_function+" not reconized");

    function_=_function;
    String cn=function_.substring(0,i);
    String mn=function_.substring(i+1);

    Class c=null;
    try { c=Class.forName(cn); }
    catch(Exception ex1) { }
    if(c==null)
      throw new IllegalArgumentException
	("class "+cn+" not found");

    params_=new Object[_params.length+1];
    System.arraycopy(_params,0,params_,1,_params.length);

    Class[] cp=new Class[params_.length];
    for(i=0;i<params_.length;i++) cp[i]=Object.class;

    method_=null;
    try { method_=c.getMethod(mn,cp); }
    catch(NoSuchMethodException ex) { }
    if(method_==null)
      throw new IllegalArgumentException
	("method "+mn+" not found");
  }

  @Override
  protected Object compute(Object _o)
  {
    Object r=FAKE;
    params_[0]=_o;
    try { r=method_.invoke(null,params_); }
    catch(Exception ex) { }
    return r;
  }

  @Override
  public String toString()
  {
    return "external(\""+function_+"\","+getPrevious()+")";
  }
}
