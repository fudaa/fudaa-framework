/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodInQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Vector;

public class YapodInQuery
       extends YapodAbstractTestQuery
{
  private String field_;
  private Vector values_;

  public YapodInQuery
    (String _field, Vector _values, YapodQuery _previous)
  {
    super(_previous);

    if(_field==null)
      throw new IllegalArgumentException
	("_field is null");

    if(_values==null)
      throw new IllegalArgumentException
	("_values is null");

    field_ =_field;
    values_=_values;
  }

  @Override
  protected boolean test(Object o)
  {
    Object v=YapodLib.getValue(o,field_);
    if(v==FAKE) v=field_;
    if(v==null) v="";

    return values_.contains(v);
  }

  @Override
  public String toString()
    { return "in(\""+field_+"\","+values_+","+getPrevious()+")"; }
}

