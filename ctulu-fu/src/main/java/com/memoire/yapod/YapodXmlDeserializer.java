/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodXmlDeserializer.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;

import com.memoire.mst.MstHandlerBase;
import com.memoire.mst.MstXmlHandler;
import com.memoire.mst.MstXmlParser;
import java.io.*;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Stack;
import java.util.Vector;

public class YapodXmlDeserializer
    implements YapodDeserializer {
  final Object NULL_VALUE = new Object();
  public static final String ENCODING = "UTF-8"; //"ISO-8859-1"

  Reader in;
  Hashtable ref; // (obj,id)
  Hashtable obj; // (id,obj)
  Hashtable nmo; // only for retrieve

  Stack objects;
  Stack fields;
  Stack arrays;
  Vector singles;

  String single_class;

  public YapodXmlDeserializer() {
  }

  @Override
  public String extension() {
    return "xml";
  }

  @Override
  public synchronized void open(InputStream _in) throws IOException {
    InputStream bufferedIn=_in;
    if (! (bufferedIn instanceof BufferedInputStream)) {
      bufferedIn = new BufferedInputStream(_in, BUFFER_SIZE);

    }
    in = new InputStreamReader(bufferedIn, ENCODING);
    ref = new Hashtable();
    obj = new Hashtable();
  }

  @Override
  public synchronized void close() throws IOException {
    in.close();
  }

  private String dir = null;
  private String root = null;

  @Override
  public synchronized Object retrieve(String _id, Object dbcx_, Hashtable _ref,
                                      Hashtable _obj, Hashtable _nmo) throws
      IOException, ClassNotFoundException {
    String _dir = (String) dbcx_;
    String old_dir = dir;
    Hashtable old_ref = ref;
    Hashtable old_obj = obj;
    Hashtable old_nmo = nmo;
    dir = _dir;
    ref = _ref;
    obj = _obj;
    nmo = _nmo;
//ref.remove(_o);
//obj.remove(k);

    root = _id;
    Object r = getObj(_id);
    root = null;

//ref.put(_o,k);
//obj.put(k,_o);
    nmo = old_nmo;
    ref = old_ref;
    obj = old_obj;
    dir = old_dir;

    return r;
  }

  Object getObj(String _id) {
    if ("null".equals(_id)) {
      return null;
    }

    Object r = obj.get(_id);
    if ( (r == null) && (dir != null)) {
      try {
        if (_id.equals(root)) {
          Reader old_in = in;
          in = new FileReader(dir + File.separator + _id + ".xml");
          r = read();
          in.close();
          in = old_in;
          nmo.put(_id, r);
        }
        else {
          YapodXmlDeserializer yxd = new YapodXmlDeserializer();
          r = yxd.retrieve(_id, dir, ref, obj, nmo);
        }
      }
      catch (Exception ex) {
        System.err.println("object " + _id + " not found in " + dir);
      }
    }

    return r;
  }

  /*
   private String getId(Object _o)
   {
   if(_o==null) return "null";
   String r=(String)ref.get(_o);
   if(r==null)
   {
   int i=1;
   do
   {
   r="RO-"+i;
   i++;
   } while(obj.get(r)!=null);
   ref.put(_o,r);
   obj.put(r,_o);
   }
   return r;
   }
   */

  @Override
  public synchronized Object read() throws IOException, ClassNotFoundException {
    if (objects == null) {
      readAll();

    }
    if (singles.size() == 0) {
      throw new NoSuchElementException();
    }

    Object r = singles.elementAt(0);
    if (r == NULL_VALUE) {
      r = null;
    }
    singles.removeElementAt(0);
    return r;
  }

  private synchronized void readAll() throws IOException,
      ClassNotFoundException {
    objects = new Stack();
    fields = new Stack();
    arrays = new Stack();
    singles = new Vector();

    Handler handler = new Handler(this, "");
    MstXmlParser parser = new MstXmlParser();
    parser.setHandler(handler);

    /*
     String url=fichier;
     if(url.indexOf(':')<0) url="file://"+new File(url).getAbsolutePath();
     try { parser.parse(url,null,null,null); }
     catch (Exception ex) { } // ex.printStackTrace();
     */

    try {
      parser.parse(null, null, in);
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }

    /*
     if(singles.size()!=1)
     {
     handler.warning("singles.size!=1 ("+singles.size()+")");
     }
     Object r=singles.elementAt(0);
     if(r==NULL_VALUE) r=null;
     return r;
     */
  }

  private static final class CorbaEnum {
    private int __value;
    private Class clazz_;

    public CorbaEnum(Class _clazz) {
      clazz_ = _clazz;
      __value = -1;
    }

    public Object build() {
      Object o = null;
      try {
        Method m = clazz_.getDeclaredMethod("from_int",
                                            new Class[] {Integer.TYPE});
        YapodLib.setAccessible(m, true);
        o = m.invoke(null, new Object[] {new Integer(__value)});
      }
      catch (NoSuchMethodException ex) {}
      catch (IllegalAccessException ex) {}
      catch (IllegalArgumentException ex) {}
      catch (InvocationTargetException ex) {}
      return o;
    }

    @Override
    public String toString() {
      return "enum " + clazz_.getName() +
          (__value != -1 ? "(" + __value + ")" : "");
    }
  }


  static final boolean isCorbaEnum(Class _c) {
    boolean r = false;
    try {
      Method m = _c.getDeclaredMethod("from_int", new Class[] {Integer.TYPE});

      r = ( (m.getModifiers() & Modifier.STATIC) != 0) &&
          (m.getReturnType() == _c);
    }
    catch (NoSuchMethodException ex) {}
    return r;
  }

  private class Handler
      extends MstHandlerBase
      implements MstXmlHandler {
    Properties options_ = new Properties();
    String fichier_ = null;
    YapodXmlDeserializer parse_ = null;
    StringBuffer data_ = new StringBuffer(32);

    public Handler(YapodXmlDeserializer _parse, String _fichier) {
      fichier_ = _fichier;
      parse_ = _parse;
    }

    public void trace(Object _o) {
// try { System.err.println("Yapod XML trace  : "+_o); }
// catch(Throwable th) { }
    }

    public void warning(String _s) {
      System.err.println("Yapod XML warning: " + _s);
    }

    @Override
    public void error(String message, String systemId, int line, int column) throws
        /*MstXmlException,*/ Exception {
      System.err.println("Yapod XML error  : " + message);
      super.error(message, systemId, line, column);
    }

    @Override
    public void charData(char ch[], int start, int length) throws Exception {
//for(int i=0; i<length; i++)
//data_+=ch[start+i];
      data_.append(ch, start, length);
    }

    @Override
    public void startElement(String _element) throws Exception {
      trace(_element);

      if ("yapod".equals(_element)) {
        //on ignore le tag root
      }
      else if ("object".equals(_element)) {
        String type = (String) options_.get("type");
        String id = (String) options_.get("id");

        Object o = NULL_VALUE;
        Class c = null;

        try {
          c = Class.forName(type);
        }
        catch (ClassNotFoundException ex) {
          warning("class not found " + type);
        }

        if (c != null) {
          try {
            Constructor x = c.getDeclaredConstructor(new Class[] {});

            YapodLib.setAccessible(x, true);
            o = x.newInstance(new Object[] {});
            trace(o);
          }
          catch (NoSuchMethodException ex) {}

          // CORBA Enum ?
          if (o == NULL_VALUE) {
            if (isCorbaEnum(c)) {
              o = new CorbaEnum(c);
              trace(o);
            }
          }

          /*
           if(o==NULL_VALUE)
           {
           try
           {
           Constructor x=c.getDeclaredConstructor
           (new Class[] { Integer.TYPE });
           YapodLib.setAccessible(x,true);
           o=x.newInstance(new Object[] { new Integer(0) });
           trace(o);
           }
           catch(NoSuchMethodException ex) { }
           }
           */

          if (o == NULL_VALUE) {
            warning("default constructor not found for class " + type);
          }
        }

        if (id != null) {
          ref.put(o, id);
          obj.put(id, o);
        }

        objects.push(o);
      }
      else if ("field".equals(_element)) {
        String name = (String) options_.get("name");

        Object o = objects.peek();
        Class c = o.getClass();
        Field f = YapodLib.getField(c, name);
        YapodLib.setAccessible(f, true);
        trace(f);

        fields.push(singles);
        fields.push(f);
        singles = new Vector();

        if (f == null) {
          System.err.println("### field " + name + " removed in " +
                             objects.peek().getClass());
        }
      }
      else if ("array-byte".equals(_element)) {
      }
      /*
       else
       if("array-int".equals(_element))
       {
       }
       */
      else if ("null".equals(_element)) {
      }
      else if ("single".equals(_element)) {
        single_class = (String) options_.get("type");
      }
      else if ("array".equals(_element)) {
        String type = (String) options_.get("type");
        String id = (String) options_.get("id");
        int length = Integer.parseInt( (String) options_.get("length"));
        int depth = Integer.parseInt( (String) options_.get("depth"));

        Class c = normalize(type);

        int[] dims = new int[depth];
        dims[0] = length;
        Object a = Array.newInstance(c, dims);
        trace(a);

        if (id != null) {
          ref.put(a, id);
          obj.put(id, a);
        }

        arrays.push(singles);
        arrays.push(a);
        singles = new Vector(10);
      }
      else if ("reference".equals(_element)) {
        String id = (String) options_.get("idref");

        Object o = getObj(id);
        if (o == null) {
          o = NULL_VALUE;
        }
        singles.addElement(o);
      }
      else {
        warning("unreconized tag: " + _element);

      // TMP @GDX
      //if(!"".equals(data_.toString().trim()))
      //warning("data should be empty: "+data_);

      // Slow on 1.4
      //data_.setLength(0);
      // Change to:
      }
      data_ = new StringBuffer(32);
      options_.clear();
    }

    @Override
    public void endElement(String _element) throws Exception {
      if ("array-byte".equals(_element)) {
        //byte[] o=data_.toString().getBytes(ENCODING);
        int l = data_.length();
        char[] c = new char[l];
        data_.getChars(0, l, c, 0);
        byte[] o = new byte[l];
        for (int k = 0; k < l; k++) {
          o[k] = (byte) c[k];
        //trace(o);
        }
        singles.addElement(o);
        //data_.setLength(0);
        return;
      }
      /*
       else
       if("array-int".equals(_element))
       {
       int    l=data_.length();
       char[] c=new char[l];
       data_.getChars(0,l,c,0);
       int[] o=new int[l];
       for(int k=0;k<l;k++) o[k]=(int)c[k];
//trace(o);
       singles.addElement(o);
       return;
       }
       */

      String data = data_.toString().trim();

      if (!"".equals(data)) {
        trace(data);
      }
      trace("/" + _element);

      if ("yapod".equals(_element)) {
      }
      else if ("object".equals(_element)) {
        Object o = objects.pop();
        if (o instanceof CorbaEnum) {
          Object id = ref.get(o);

          Object n = ( (CorbaEnum) o).build();
          if (n == null) {
            System.err.println("### failed to build enum " + n);
            n = NULL_VALUE;
          }
          o = n;
          ref.put(o, id);
          obj.put(id, o);
        }
      }
      else if ("field".equals(_element)) {
        Field f = (Field) fields.pop();

        if (singles.size() != 1) {
          warning("singles.size!=1 (" + singles.size() + ")");

        }
        Object r = singles.elementAt(0);
        if (r == NULL_VALUE) {
          r = null;

        }
        if (f != null) {
          try {
            f.set(objects.peek(), r);
          }
          catch (IllegalAccessException ex) {
            System.err.println("### no access to field " + f.getName() + " in " +
                               objects.peek().getClass());
          }
        }

        singles = (Vector) fields.pop();
      }
      else if ("null".equals(_element)) {
        singles.addElement(NULL_VALUE);
      }
      else if ("single".equals(_element)) {
        Object o = NULL_VALUE;
        Class c = normalize(single_class);

        if (c == Boolean.class) {
          o = new Boolean(data);
        }
        else if (c == Character.class) {
          try {
            o = new Character( (char)new Integer(data).intValue());
          }
          catch (NumberFormatException ex) {
            warning("invalid char value " + data);
          }
        }
        else if (c == String.class) {
          o = fromXmlCharset(data);
        }
        else { // Number
          try {
            Constructor x = c.getDeclaredConstructor(new Class[] {String.class});
            YapodLib.setAccessible(x, true);
            o = x.newInstance(new Object[] {data});
          }
          catch (NoSuchMethodException ex) {
            warning("constructor(string) not found for class " + single_class);
          }
        }

        trace(o);
        singles.addElement(o);
//data="";
      }
      else if ("array".equals(_element)) {
        Object a = arrays.pop();
        int l = Array.getLength(a);
        if (singles.size() != l) {
          warning("singles.size!=array.length (" + singles.size() + ")");

        }
        for (int i = 0; i < l; i++) {
          Object r = singles.elementAt(i);
          if (r == NULL_VALUE) {
            r = null;
          }
          Array.set(a, i, r);
        }

        singles = (Vector) arrays.pop();
        singles.addElement(a);
      }
      else if ("reference".equals(_element)) {
      }
      else {
        warning("unreconized tag: " + _element);

        /*
         if(!"".equals(data))
         {
         warning("data is not empty: "+data);
         data="";
         }
         data_.setLength(0);
         */
      }
    }

    @Override
    public void attribute(String _attribut, String _valeur, boolean _specifie) throws
        Exception {
      if (_specifie) {
        options_.put(_attribut, _valeur);
      }
    }

    private Class normalize(String type) {
      Class r = Object.class;

      if ("boolean".equals(type)) {
        r = Boolean.TYPE;
      }
      else if ("char".equals(type)) {
        r = Character.TYPE;
      }
      else if ("byte".equals(type)) {
        r = Byte.TYPE;
      }
      else if ("short".equals(type)) {
        r = Short.TYPE;
      }
      else if ("int".equals(type)) {
        r = Integer.TYPE;
      }
      else if ("long".equals(type)) {
        r = Long.TYPE;
      }
      else if ("float".equals(type)) {
        r = Float.TYPE;
      }
      else if ("double".equals(type)) {
        r = Double.TYPE;
      }
      else if ("void".equals(type)) {
        r = Void.TYPE;
      }
      else {
        try {
          r = Class.forName(type);
        }
        catch (ClassNotFoundException ex) {
          warning("class not found for " + type);
        }
      }

      return r;
    }

    /*
     private final String fromXmlCharset(String _s)
     {
     String r=_s;
     r=YapodLib.replace(r,"&#32;"," ");
     int i,j;
     while((i=r.indexOf("&#"))>=0)
     {
     j=r.indexOf(";",i);
     if(j<0) break;
     String s=r.substring(i+2,j);
     try
     {
     String c=""+(char)Integer.parseInt(s);
//System.err.println("S="+s+" --> "+c);
     r=YapodLib.replace(r,"&#"+s+";",c);
     }
     catch(NumberFormatException ex)
     {
     warning("invalid char value &#"+s+";");
     break;
     }
     }
     r=YapodLib.replace(r,"&lt;" ,"<");
     r=YapodLib.replace(r,"&gt;" ,">");
     r=YapodLib.replace(r,"&quot;","\"");
     r=YapodLib.replace(r,"&amp;","&");
     return r;
     }
     */

    private final String fromXmlCharset(String _s) {
      StringBuffer r = new StringBuffer(_s.length());

      int i = 0;
      int j = 0;
      while ( (j = _s.indexOf("&", i)) >= 0) {
        if (i != j) {
          r.append(_s.substring(i, j));
        }
        i = j;

        int k = _s.indexOf(";", i);
        if (k <= 0) {
          warning("suspicious single &");
          i++;
          r.append('&');
          continue;
        }

        String t = _s.substring(i + 1, k);
//System.err.println("T="+t);

        if ("#32".equals(t)) {
          r.append(' ');
        }
        else if ("lt".equals(t)) {
          r.append('<');
        }
        else if ("gt".equals(t)) {
          r.append('>');
        }
        else if ("quot".equals(t)) {
          r.append('"');
        }
        else if ("amp".equals(t)) {
          r.append('&');
        }
        else if (t.charAt(0) == '#') {
          try {
            char c = (char) Integer.parseInt(t.substring(1));
            r.append(c);
          }
          catch (NumberFormatException ex) {
            warning("invalid char value &" + t + ";");
//System.err.println("invalid char value &"+t+";");
            r.append('&');
            r.append(t);
            r.append(';');
          }
        }
        else {
          warning("invalid entity &" + t + ";");
//System.err.println("invalid entity &"+t+";");
          r.append('&');
          r.append(t);
          r.append(';');
        }

        i = k + 1;
      }

      r.append(_s.substring(i));
      return r.toString();
    }
  }


  public static void main(String[] args) {
    /*
     String s="Hello&quot;&amp;x&gt;x&lt;&lt;x&#32;&#64;&apos;xyz";
     System.out.println(s);
     System.out.println(fromXmlCharset(s));
     */

    try {
      String f = (args.length > 0 ? args[0] : "xxx.xml");
      YapodDeserializer in = new YapodXmlDeserializer();
      InputStream is = new FileInputStream(f);
     // InputStream xxx=System.in;
      in.open(is);
      Object o1 = in.read();
      Object o2 = in.read();
      Object o3 = in.read();
      in.close();

      YapodSerializer out = new YapodXmlSerializer();
      OutputStream os = new FileOutputStream(f + ".2");
      // OutputStream xxx=System.out;
      out.open(os);
      out.write(o1);
      out.write(o2);
      out.write(o3);
      out.close();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}