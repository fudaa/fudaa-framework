/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut unstable
 * @file YapodSecurityManager.java
 * @version 0.17
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 */
package com.memoire.yapod;

import java.io.FileDescriptor;
import java.net.InetAddress;

public class YapodSecurityManager
  extends SecurityManager {
  @Override
  public void checkCreateClassLoader() {
  }

  @Override
  public void checkAccess(Thread g) {
  }

  @Override
  public void checkAccess(ThreadGroup g) {
  }

  @Override
  public void checkExit(int status) {
  }

  @Override
  public void checkExec(String cmd) {
  }

  @Override
  public void checkLink(String lib) {
  }

  @Override
  public void checkRead(FileDescriptor fd) {
  }

  @Override
  public void checkRead(String file) {
  }

  @Override
  public void checkRead(String file, Object context) {
  }

  @Override
  public void checkWrite(FileDescriptor fd) {
  }

  @Override
  public void checkWrite(String file) {
  }

  @Override
  public void checkDelete(String file) {
  }

  @Override
  public void checkConnect(String host, int port) {
  }

  @Override
  public void checkConnect(String host, int port, Object context) {
  }

  @Override
  public void checkListen(int port) {
  }

  @Override
  public void checkAccept(String host, int port) {
  }

  @Override
  public void checkMulticast(InetAddress maddr) {
  }

  @Override
  public void checkMulticast(InetAddress maddr, byte ttl) {
  }

  @Override
  public void checkPropertiesAccess() {
  }

  @Override
  public void checkPropertyAccess(String key) {
  }

  public void checkPropertyAccess(String key, String def) {
  }

  public boolean checkTopLevelWindow(Object window) {
    return true;
  }

  @Override
  public void checkPrintJobAccess() {
  }

  public void checkSystemClipboardAccess() {
  }

  public void checkAwtEventQueueAccess() {
  }

  @Override
  public void checkPackageAccess(String pkg) {
  }

  @Override
  public void checkPackageDefinition(String pkg) {
  }

  @Override
  public void checkSetFactory() {
  }

  public void checkMemberAccess(Class clazz, int which) {
  }

  @Override
  public void checkSecurityAccess(String provider) {
  }


/*
  private SecurityManager old_;

  public YapodSecurityManager()
  {
    old_=System.getSecurityManager();
    System.err.println("old_="+old_);
  }

  public void checkCreateClassLoader()
  { if(old_!=null) old_.checkCreateClassLoader(); }

  public void checkAccess(Thread _thread)
  { if(old_!=null) old_.checkAccess(_thread); }

  public void checkAccess(ThreadGroup _group)
  { if(old_!=null) old_.checkAccess(_group); }

  public void checkExit(int _status)
  { if(old_!=null) old_.checkExit(_status); }

  public void checkExec(String _cmd)
  { if(old_!=null) old_.checkExec(_cmd); }

  public void checkLink(String _lib)
  { if(old_!=null) old_.checkLink(_lib); }

  public void checkRead(FileDescriptor _fd)
  { if(old_!=null) old_.checkRead(_fd); }

  public void checkWrite(FileDescriptor _fd)
  { if(old_!=null) old_.checkWrite(_fd); }

  public void checkDelete(String _file)
  { if(old_!=null) old_.checkDelete(_file); }

  public void checkConnect(String _host, int _port)
  { if(old_!=null) old_.checkConnect(_host,_port); }

  public void checkConnect(String _host, int _port, Object _context)
  { if(old_!=null) old_.checkConnect(_host,_port,_context); }

  public void checkListen(int _port)
  { if(old_!=null) old_.checkListen(_port); }

  public void checkAccept(String _host, int _port)
  { if(old_!=null) old_.checkAccept(_host,_port); }

  public void checkMulticast(InetAddress _maddr)
  { if(old_!=null) old_.checkMulticast(_maddr); }

  public void checkMulticast(InetAddress _maddr, byte _ttl)
  { if(old_!=null) old_.checkMulticast(_maddr,_ttl); }

  public void checkPropertiesAccess()
  { if(old_!=null) old_.checkPropertiesAccess(); }

  public void checkPropertyAccess(String _key)
  { if(old_!=null) old_.checkPropertyAccess(_key); }

  public boolean checkTopLevelWindow(Object _window)
  {
    if(old_!=null) return old_.checkTopLevelWindow(_window);
    else return super.checkTopLevelWindow(_window);
  }

  public void checkPrintJobAccess()
  { if(old_!=null) old_.checkPrintJobAccess(); }

  public void checkSystemClipboardAccess()
  { if(old_!=null) old_.checkSystemClipboardAccess(); }

  public void checkAwtEventQueueAccess()
  { if(old_!=null) old_.checkAwtEventQueueAccess(); }

  public void checkPackageAccess(String _pkg)
  { if(old_!=null) old_.checkPackageAccess(_pkg); }

  public void checkPackageDefinition(String _pkg)
  { if(old_!=null) old_.checkPackageDefinition(_pkg); }

  public void checkSetFactory()
  { if(old_!=null) old_.checkSetFactory(); }

  public void checkSecurityAccess(String _action)
  { if(old_!=null) old_.checkSecurityAccess(_action); }

  public void checkMemberAccess(Class _class, int _field)
  { }
*/
}
