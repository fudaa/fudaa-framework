/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         YapodElementQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Vector;

public class YapodElementQuery
       extends YapodAbstractQuery
{
  private int num_;

  public YapodElementQuery
    (int _num, YapodQuery _previous)
  {
    super(_previous);
    num_=_num;
  }

  @Override
  protected final Enumeration query(Enumeration e)
  {
    int    n=0;
    Object o=FAKE;

    while(e.hasMoreElements())
    {
      o=e.nextElement();
      if(n==num_) break;
      n++;
    }

    Vector r=new Vector(1);
    if(o!=FAKE) r.addElement(o);
    return r.elements();
  }

  @Override
  public String toString()
    { return "element("+num_+","+getPrevious()+")"; }
}
