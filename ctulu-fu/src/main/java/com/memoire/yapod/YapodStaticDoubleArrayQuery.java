/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodStaticDoubleArrayQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public class YapodStaticDoubleArrayQuery
       implements YapodQuery
{
  private double[] values_;

  public YapodStaticDoubleArrayQuery(double[] _values)
  {
    if(_values==null)
      throw new IllegalArgumentException("_values is null");

    values_=_values;
  }

  @Override
  public final Enumeration getResult()
    { return new Enumerator(values_); }

  @Override
  public String toString()
  {
    StringBuffer r=new StringBuffer();

    r.append('[');
    for(int i=0;i<values_.length;i++)
    {
      if(i!=0) r.append(", ");
      r.append(values_[i]);
    }
    r.append(']');

    return r.toString();
  }

  final class Mutable
                extends Number implements Cloneable
  {
    private double value_;
    public final double getValue() { return value_; }
    public final void   setValue(double _value) { value_=_value; }

    @Override
    public final int    intValue   () { return (int)  value_; }
    @Override
    public final long   longValue  () { return (long) value_; }
    @Override
    public final float  floatValue () { return (float)value_; }
    @Override
    public final double doubleValue() { return        value_; }

    @Override
    public String toString()
      { return "mutable("+value_+")"; }
    @Override
    public Object clone()
      { Mutable r=new Mutable(); r.setValue(value_); return r; }
  }

  private final class Enumerator implements Enumeration
  {
    private double[] enumValues_;
    private int      index_;
    private Mutable  mutan_;

    public Enumerator(double[] _values)
    {
      enumValues_=_values;
      index_ =0;
      mutan_ =new Mutable();
    }

    @Override
    public final boolean hasMoreElements()
      { return index_<enumValues_.length; }

    @Override
    public final Object nextElement()
    {
      if(index_>=enumValues_.length)
	throw new NoSuchElementException();
      // return new Double(values_[index_++]);
      mutan_.setValue(enumValues_[index_++]);
      return mutan_;
    }
  }
}
