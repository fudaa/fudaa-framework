/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodSetQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Vector;

public class YapodSetQuery
       implements YapodQuery
{
  private YapodQuery[] q_;

  public YapodSetQuery
    (YapodQuery _q1, YapodQuery _q2)
  {
    this(new YapodQuery[] { _q1, _q2 });
  }

  public YapodSetQuery
    (YapodQuery[] _queries)
  {
    if(_queries==null)
      throw new IllegalArgumentException("_queries is null");

    for(int i=0;i<_queries.length;i++)
      if(_queries[i]==null)
	throw new IllegalArgumentException("_queries["+i+"] is null");

    q_=_queries;
  }

  @Override
  public Enumeration getResult()
  {
    Vector r=new Vector();
    for(int i=0;i<q_.length;i++)
      // r.addElement(q_[i]);
      r.addElement(YapodLib.toVector(q_[i].getResult()));
    return r.elements();
  }

  @Override
  public String toString()
  {
    String r="set(";
    for(int i=0;i<q_.length;i++)
      r+= (i==0 ? "" : ",")+q_[i];
    r+=")";
    return r;
  }
}
