/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodAvgQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Vector;

public class YapodAvgQuery
       extends YapodAbstractQuery
{
  public YapodAvgQuery
    (YapodQuery _previous)
  {
    super(_previous);
  }

  @Override
  protected final Enumeration query(Enumeration e)
  {
    double sum=0.;
    int    nbr=0;

    while(e.hasMoreElements())
    {
      Object o=e.nextElement();
      if(o instanceof Number)
      {
	sum+=((Number)o).doubleValue();
	nbr++;
      }
    }

    Vector r=new Vector(1);
    if(nbr!=0) r.addElement(new Double(sum/nbr));
    return r.elements();
  }

  @Override
  public String toString()
    { return "avg("+getPrevious()+")"; }
}
