/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         YapodStaticArrayQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.NoSuchElementException;

public class YapodStaticArrayQuery
       implements YapodQuery
{
  private Object[] values_;

  public YapodStaticArrayQuery(Object[] _values)
  {
    if(_values==null)
      throw new IllegalArgumentException("_values is null");

    values_=_values;
  }

  @Override
  public final Enumeration getResult()
    { return new Enumerator(values_); }

  @Override
  public String toString()
    { return values_.toString(); }

  private final class Enumerator implements Enumeration
  {
    private Object[] enumValues_;
    private int      index_;

    public Enumerator(Object[] _values)
    {
      enumValues_=_values;
      index_=0;
    }

    @Override
    public final boolean hasMoreElements()
      { return index_<enumValues_.length; }

    @Override
    public final Object nextElement()
    {
      if(index_>=enumValues_.length)
	throw new NoSuchElementException();
      return enumValues_[index_++];
    }
  }
}
