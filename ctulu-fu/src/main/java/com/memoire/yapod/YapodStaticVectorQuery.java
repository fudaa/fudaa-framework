/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodStaticVectorQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Vector;

public class YapodStaticVectorQuery
       implements YapodQuery
{
  private Vector values_;

  public YapodStaticVectorQuery(Vector _values)
  {
    if(_values==null)
      throw new IllegalArgumentException("_values is null");

    values_=_values;
  }

  @Override
  public final Enumeration getResult()
    { return values_.elements(); }

  @Override
  public String toString()
    { return values_.toString(); }
}
