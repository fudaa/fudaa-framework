/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodAbstractQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;

public abstract class YapodAbstractQuery
                implements YapodQuery
{
  private YapodQuery previous_;

  public YapodAbstractQuery(YapodQuery _query)
  {
    setPrevious(_query);
  }

  protected abstract Enumeration query(Enumeration e);

  public final YapodQuery getPrevious()
    { return previous_; }

  public final synchronized void setPrevious(YapodQuery _query)
  {
    if(_query==null)
      throw new IllegalArgumentException("_query is null");

    previous_=_query;
  }

  @Override
  public final Enumeration getResult()
    { return query(previous_.getResult()); }

  @Override
  public String toString()
    { return "query("+previous_+")"; }
}

