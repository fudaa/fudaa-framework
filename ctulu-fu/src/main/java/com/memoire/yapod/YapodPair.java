/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodPair.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;


public final class YapodPair
{
  public Object key;
  public Object value;

  public YapodPair(Object _key, Object _value)
    { key=_key; value=_value; }

  @Override
  public String toString()
    { return "pair("+key+","+value+")"; }
}
