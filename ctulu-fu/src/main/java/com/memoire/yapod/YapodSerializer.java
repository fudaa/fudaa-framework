/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodSerializer.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Hashtable;

public interface YapodSerializer
{
  int BUFFER_SIZE=1048576; // 1Mb

  String extension();
  void   open (OutputStream out) throws IOException;
  void   close()                 throws IOException;
  void   write(Object _o)        throws IOException;
  void   store
    (Object _o, Object dbcx_, Hashtable _ref, Hashtable _obj)
    throws Exception;
}


