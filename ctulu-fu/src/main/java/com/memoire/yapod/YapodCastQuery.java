/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodCastQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;

public class YapodCastQuery
       extends YapodAbstractComputeQuery
{
  private static String[] OPERATORS=new String[]
  {
    "byte","short","int","long","float","double","char","string"
  };

  private int op_;

  public YapodCastQuery
    (String _op, YapodQuery _previous)
  {
    super(_previous);

    int i=OPERATORS.length-1;
    while(i>=0)
    {
      if(OPERATORS[i].equals(_op)) break;
      i--;
    }

    if(i<0)
      throw new IllegalArgumentException
	("operator "+_op+" not reconized");

    op_=i;
  }

  @Override
  protected Object compute(Object _o)
  {
    Object r=FAKE;

    try
    {
      switch(op_)
      {
      case 0:
	if(_o instanceof Number) r=new Byte(((Number)_o).byteValue());
	if(_o instanceof String) r=new Byte((String)_o);
	break;
      case 1:
	if(_o instanceof Number) r=new Short(((Number)_o).shortValue());
	if(_o instanceof String) r=new Short((String)_o);
	break;
      case 2:
	if(_o instanceof Number) r=new Integer(((Number)_o).intValue());
	if(_o instanceof String) r=new Integer((String)_o);
	break;
      case 3:
	if(_o instanceof Number) r=new Long(((Number)_o).longValue());
	if(_o instanceof String) r=new Long((String)_o);
	break;
      case 4:
	if(_o instanceof Number) r=new Float(((Number)_o).floatValue());
	if(_o instanceof String) r=new Float((String)_o);
	break;
      case 5:
	if(_o instanceof Number) r=new Double(((Number)_o).doubleValue());
	if(_o instanceof String) r=new Double((String)_o);
	break;
      case 6:
	if(_o instanceof Number) r=new Character
				   ((char)((Number)_o).intValue());
	if(_o instanceof String)
	{
	  String s=(String)_o;
	  if(s.length()>0) r=new Character(s.charAt(0));
	  else             r=new Character('\0');
	}
	break;
      case 7:
	if(_o==null) r="";
	else         r=""+_o;
	break;
      }
    }
    catch(Exception ex) { }
    return r;
  }

  @Override
  public String toString()
  {
    return "cast(\""+OPERATORS[op_]+"\","+getPrevious()+")";
  }
}

