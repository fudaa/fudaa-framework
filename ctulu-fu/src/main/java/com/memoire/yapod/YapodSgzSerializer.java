/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodSgzSerializer.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

public class YapodSgzSerializer
       extends YapodSerSerializer
{
  public YapodSgzSerializer()
  {
  }

  @Override
  public String extension()
  {
    return "sgz";
  }

  public String indente()
  {
    return "";
  }

  @Override
  public synchronized void open(OutputStream _out) throws IOException
  {
    OutputStream out=_out;
    if(!(out instanceof BufferedOutputStream))
      out=new BufferedOutputStream(out,BUFFER_SIZE);
    super.open(new GZIPOutputStream(out));
  }
}
