/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         YapodReference.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;

public class YapodReference
       // implements Serializable
{
  protected YapodDatabase db_;   // database
  protected String        id_;   // object id

  // not public
  YapodReference(YapodDatabase _db, String _id)
  {
    if(_db==null)
      throw new IllegalArgumentException("_db is null");

    if(_id==null)
      throw new IllegalArgumentException("_id is null");

    db_=_db;
    id_=_id;
  }

  public synchronized boolean isValid()
  {
    return db_.isOpened()&&(db_.guess(id_)!=null);
  }

  public synchronized String getId()
  {
    return id_;
  }

  public synchronized Object getObject()
  {
    if(!isValid())
      throw new RuntimeException
	("invalid reference");

    return db_.find(id_);
  }

  public synchronized String getType()
  {
    if(!isValid())
      throw new RuntimeException
	("invalid reference");

    return db_.guess(id_);
  }
}
