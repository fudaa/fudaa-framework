/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         YapodIntervalQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Vector;

public class YapodIntervalQuery
       extends YapodAbstractQuery
{
  private int begin_,end_;

  public YapodIntervalQuery
    (int _begin, int _end, YapodQuery _previous)
  {
    super(_previous);
    begin_=_begin;
    end_  =_end;
  }

  @Override
  protected final Enumeration query(Enumeration e)
  {
    Vector r=new Vector();

    if(end_>begin_)
    {
      int n=0;

      while(e.hasMoreElements())
      {
	if(n<end_)
	{
	  Object o=e.nextElement();
	  if(n>=begin_) r.addElement(o);
	}
	else break;

	n++;
      }
    }

    return r.elements();
  }

  @Override
  public String toString()
    { return "interval("+begin_+","+end_+","+getPrevious()+")"; }
}




