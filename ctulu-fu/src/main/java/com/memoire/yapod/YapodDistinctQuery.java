/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         YapodDistinctQuery.java
 * @version      0.17
 * @author       Nicolas Chevalier
 * @email        nicolas.chavalier@etu.utc.fr
 * @license      GNU General Public License 2 (GPL2)
 * (c)1999-2001 Nicolas Chevalier
 * nicolas.chavalier@etu.utc.fr
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Remove multiple values.
 * Equivalent to the unix command uniq.
 * @author Nicolas Chevalier
 */

public class YapodDistinctQuery extends YapodAbstractQuery
{
  public YapodDistinctQuery(YapodQuery _previous)
  {
    super(_previous);
  }
  
  @Override
  public Enumeration query(Enumeration _enum)
  {
    Hashtable table = new Hashtable();

    while(_enum.hasMoreElements())
    { 
      Object elt=_enum.nextElement();
      table.put(elt,elt);
    }
    
    return table.elements();
  }
  
  public static void main(String[] args)
  {
    Integer i1 = new Integer(1);
    Integer i2 = new Integer(2);
    Integer i3 = new Integer(3);
    Integer i4 = new Integer(1);
    
    Integer[] tab1 = {i1,i2,i3,i2,i1,i4};
    YapodQuery q0 = new YapodStaticArrayQuery(tab1);
    YapodDistinctQuery q1 = new YapodDistinctQuery(q0);
    System.out.println("\nq0");
    YapodLib.print(q0.getResult());
    System.out.println("\nq1");
    YapodLib.print(q1.getResult());
  }
}
