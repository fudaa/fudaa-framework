/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         YapodAssoc.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Hashtable;

public final class YapodAssoc
             implements YapodConstants
{
  private static Object NULL=new Object();

  private Hashtable table;

  public YapodAssoc()
  {
    table=new Hashtable(1,1);
  }

  public void put(String _key, Object _value)
  {
    Object value=_value;
    if((_key==null)||(_key==FAKE))
      return;

    if(value==null) value=NULL;
    table.put(_key,value);
  }

  public Object get(String _key)
  {
    if((_key==null)||(_key==FAKE))
      return FAKE;

    Object r=table.get(_key);
    if(r==NULL) r=null;
    return r;
  }

  @Override
  public String toString()
  {
    StringBuffer r=new StringBuffer();
    r.append('{');

    boolean p=true;
    for(Enumeration e=table.keys(); e.hasMoreElements(); )
    {
      if(p) p=false;
      else  r.append(", ");
      String k=(String)e.nextElement();
      r.append(k+":"+table.get(k));
    }

    r.append('}');
    return r.toString();
  }
}
