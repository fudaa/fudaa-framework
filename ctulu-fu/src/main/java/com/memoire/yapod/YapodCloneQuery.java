/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodCloneQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.lang.reflect.Method;

public class YapodCloneQuery
       extends YapodAbstractComputeQuery
{
  public YapodCloneQuery
    (YapodQuery _previous)
  {
    super(_previous);
  }

  @Override
  protected Object compute(Object _o)
  {
    Object r=FAKE;
    try
    {
      Method m=_o.getClass().getMethod("clone",new Class[0]);
      r=m.invoke(_o,new Object[0]);
    }
    catch(Exception ex) { ex.printStackTrace(); }
    return r;
  }

  @Override
  public String toString()
  {
    return "clone("+getPrevious()+")";
  }
}

