/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodUnionQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Vector;

public class YapodUnionQuery
       implements YapodQuery
{
  private YapodQuery[] q_;

  public YapodUnionQuery
    (YapodQuery _q1, YapodQuery _q2)
  {
    q_=new YapodQuery[] { _q1, _q2 };
  }

  public YapodUnionQuery
    (YapodQuery[] _queries)
  {
    if(_queries==null)
      throw new IllegalArgumentException("_queries is null");

    q_=_queries;
  }

  @Override
  public Enumeration getResult()
  {
    Vector r=new Vector();
    for(int i=0;i<q_.length;i++)
    {
      for(Enumeration e=q_[i].getResult(); e.hasMoreElements(); )
      {
	Object o=e.nextElement();
	if(!r.contains(o)) r.addElement(o);
      }
    }
    return r.elements();
  }

  @Override
  public String toString()
  {
    String r="union(";
    for(int i=0;i<q_.length;i++)
      r+= (i==0 ? "" : ",")+q_[i];
    r+=")";
    return r;
  }
}
