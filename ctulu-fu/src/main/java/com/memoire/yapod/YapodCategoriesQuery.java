/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         YapodCategoriesQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class YapodCategoriesQuery
       extends YapodAbstractQuery
{
  private String field_;

  public YapodCategoriesQuery
    (String _field, YapodQuery _previous)
  {
    super(_previous);

    if(_field==null)
      throw new IllegalArgumentException
	("_field is null");

    field_=_field;
  }

  @Override
  protected Enumeration query(Enumeration e)
  {
    Hashtable table=new Hashtable();

    while(e.hasMoreElements())
    {
      Object o=e.nextElement();

      try
      {
	Object v=YapodLib.getValue(o,field_);
	if(v==FAKE) v=field_;
	if(v==null) v="";

	Integer n=(Integer)table.get(v);
	if(n==null) n=new Integer(1);
	else        n=new Integer(n.intValue()+1);
	table.put(v,n);
      }
      catch(Exception ex) { }
    }

    Vector r=new Vector(table.size());
    for(Enumeration f=table.keys(); f.hasMoreElements(); )
    {
      Object k=f.nextElement();
      r.addElement(new YapodPair(k,table.get(k)));
    }
    return r.elements();
  }

  @Override
  public String toString()
    { return "categories(\""+field_+"\","+getPrevious()+")"; }
}
