/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         YapodExplodeQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.lang.reflect.Field;

public class YapodExplodeQuery
       extends YapodAbstractComputeQuery
{
  public YapodExplodeQuery
    (YapodQuery _previous)
  {
    super(_previous);
  }

  @Override
  protected Object compute(Object _object)
  {
    YapodAssoc r=new YapodAssoc();

    Field[] fields=YapodLib.getAllFields(_object.getClass());

    for(int i=0;i<fields.length;i++)
    {
      String k=fields[i].getName();
      Object v=YapodLib.getValue(_object,k);
      r.put(k,v);
    }

    return r;
  }

  @Override
  public String toString()
    { return "explode("+getPrevious()+")"; }
}
