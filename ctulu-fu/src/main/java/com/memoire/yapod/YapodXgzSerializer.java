/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodXgzSerializer.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

public class YapodXgzSerializer
       extends YapodXmlSerializer
{
  public YapodXgzSerializer()
  {
  }

  @Override
  public String extension()
  {
    return "xgz";
  }

  @Override
  public String indente()
  {
    return "";
  }

  @Override
  public synchronized void open(OutputStream _out) throws IOException
  {
    OutputStream tmpOut=_out;
    if(!(tmpOut instanceof BufferedOutputStream))
      tmpOut=new BufferedOutputStream(tmpOut,BUFFER_SIZE);
    super.open(new GZIPOutputStream(tmpOut));
  }
}
