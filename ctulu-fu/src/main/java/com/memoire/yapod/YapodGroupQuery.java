/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodGroupQuery.java
 * @version      0.17
 * @author       Nicolas Chevalier
 * @email        nicolas.chavalier@etu.utc.fr
 * @license      GNU General Public License 2 (GPL2)
 * (c)1999-2001 Nicolas Chevalier
 * nicolas.chavalier@etu.utc.fr
 */

package com.memoire.yapod;
import java.awt.Point;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

/**
 *
 * Regroupe les �l�ments r�sultants de _previous par valeurs du champ _field.
 * Ex :    YapodQuery q = new YapodGroupQuery("x", previous)
 *         avec resultat de previous =  [ [x=1 y=1], [x=1 y=2], [x=2 y=1], [x=2 y=2] ]
 *
 *         q donne [ 
 *                   pair(1, [ [x=1 y=1], [x=1 y=2] ], 
 *                   pair(2, [ [x=2 y=1], [x=2 y=2] ]
 *                 ]
 *
 */
public class YapodGroupQuery
       extends YapodAbstractQuery
{
  private String field_;

  public YapodGroupQuery(String _field, YapodQuery _previous)
  {
    super(_previous);

    if(_field==null) 
      throw new IllegalArgumentException ("_field is null");

    field_=_field;
  }

  @Override
  protected Enumeration query(Enumeration e)
  {
    Hashtable table=new Hashtable();

    while(e.hasMoreElements())
    {
      Object o=e.nextElement();

      try
      {
        // Recherche de la valeur du champ demand� dans o.
        Object v = YapodLib.getValue(o,field_);
	if(v==FAKE) v=field_;   // Le champ demand� n'existe pas.
	if(v==null) v="";       // La valeur du champ demand� est null.

	// Ajoute un vecteur pour une valeur inconnue du champ.
        Vector vect=(Vector)table.get(v);
	if(vect==null)
	{
          vect=new Vector();
          table.put(v,vect);
        }
      
        // Ajout de l'�l�ment dans le vecteur correspondant � la valeur du champ demand�.
        vect.addElement(o);
      }
      catch(Exception ex) { }
    }
    // table contient un l'identifiant "" pour les valeurs nulles du champ demand�.
    // table contient un identifiant �gal au champ demand� pour les �l�ments qui ne le contiennent pas.

    Vector r=new Vector(table.size());
    for(Enumeration f=table.keys(); f.hasMoreElements(); )
    {
      Object k=f.nextElement();
      r.addElement(new YapodPair(k,table.get(k)));
    }
    return r.elements();
  }

  @Override
  public String toString()
    { return "group(\""+field_+"\","+getPrevious()+")"; }

  public static void main(String[] args)
  {
    Vector vect=new Vector();
    vect.addElement(new Point(1,1));
    vect.addElement(new Point(1,2));
    vect.addElement(new Point(2,1));
    vect.addElement(new Point(2,2));
    
    YapodQuery q0 = new YapodStaticVectorQuery(vect);
    System.out.println("\nq0");
    YapodLib.print(q0.getResult());
    
    YapodQuery q1 = new YapodGroupQuery ("x", q0);
    System.out.println("\nq1");
    YapodLib.print(q1.getResult());
  }    
}
