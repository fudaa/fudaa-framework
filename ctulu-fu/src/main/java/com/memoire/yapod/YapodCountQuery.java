/**
 * @modification $Date: 2006-09-19 14:34:57 $
 * @statut       unstable
 * @file         YapodCountQuery.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;
import java.util.Enumeration;
import java.util.Vector;

public class YapodCountQuery
       extends YapodAbstractQuery
{
  public YapodCountQuery
    (YapodQuery _previous)
  {
    super(_previous);
  }

  @Override
  protected final Enumeration query(Enumeration e)
  {
    int n=0;
    while(e.hasMoreElements())
      { n++; e.nextElement(); }

    Vector r=new Vector(1);
    r.addElement(new Integer(n));
    return r.elements();
  }

  @Override
  public String toString()
    { return "count("+getPrevious()+")"; }
}
