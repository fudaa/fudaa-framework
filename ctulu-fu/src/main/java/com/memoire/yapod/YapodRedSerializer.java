/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         YapodRedSerializer.java
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package com.memoire.yapod;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Field;

public class YapodRedSerializer
       extends YapodXmlSerializer
{
  public YapodRedSerializer()
  {
  }

  @Override
  public String extension()
  {
    return "red";
  }

  // Write parts into a OutputStream

  @Override
  protected synchronized void writeHeader(OutputStream _w)
      throws IOException
  {
    output(_w,"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
    output(_w,"<?xml-stylesheet href=\"red.css\" type=\"text/css\"?>\n");
    output(_w,"<!DOCTYPE red>\n");
    output(_w,"<red>");
  }

  @Override
  protected synchronized void writeFooter(OutputStream _w)
      throws IOException
  {
    output(_w,"\n</red>");
  }

  @Override
  protected synchronized void writeNull(OutputStream _w)
      throws IOException
  {
  }

  @Override
  protected synchronized void writeNumber(Number _o, OutputStream _w)
      throws IOException
  {
    output(_w,_o.toString());
  }

  @Override
  protected synchronized void writeCharacter(Character _o, OutputStream _w)
      throws IOException
  {
    output(_w,toXmlCharset(_o.toString()));
  }

  @Override
  protected synchronized void writeString(String _o, OutputStream _w)
      throws IOException
  {
    output(_w,toXmlCharset(_o));
  }

  @Override
  protected synchronized void writeArray(Object _o, OutputStream _w)
      throws IOException
  {
    output(_w,"\n"+indente()+"<array>");
    indentation_++;
    for(int i=0;i<Array.getLength(_o);i++)
    {
      if(i>0) output(_w,",");
      write(Array.get(_o,i),_w);
    }
    indentation_--;
    output(_w,"</array>");
  }

  @Override
  protected synchronized void writeObject(Object _o, OutputStream _w)
      throws IOException
  {
    synchronized(_o)
    {
      String cn=_o.getClass().getName();
      int j=cn.lastIndexOf('.');
      if(j>=0) cn=cn.substring(j+1);
      cn=cn.toLowerCase();

      output(_w,"\n"+indente()+"<"+cn+
		 " id=\""+getId(_o)+"\">");
      indentation_++;
      Field[] fields=YapodLib.getAllFields(_o.getClass());
      for(int i=0;i<fields.length;i++)
	writeField(_o,fields[i],_w);
      indentation_--;
      output(_w,"\n"+indente()+"</"+cn+">");
    }
  }

  @Override
  protected synchronized void writeReference(Object _o, OutputStream _w)
      throws IOException
  {
    output(_w,getId(_o));
  }

  @Override
  protected void writeField(String _name, Object _value, OutputStream _w)
      throws IOException
  {
    String fn=_name.toLowerCase();

    output(_w,"\n"+indente()+"<"+fn+">");
    write(_value,_w);
    output(_w,"</"+fn+">");
  }

  /*public static void main(String[] args)
  {
    try
    {
      YapodSerializer out=new YapodRedSerializer();
      OutputStream xxx=new FileOutputStream("xxx.red");
      // OutputStream xxx=System.out;
      out.open(xxx);
      out.write(new YapodTestC2());
      out.close();
    }
    catch(IOException ex) { ex.printStackTrace(); }
  }*/
}
