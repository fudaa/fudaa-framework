/**
 * @modification $Date: 2006-04-05 09:59:14 $
 * @statut       unstable
 * @file         FuConstants.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

public interface FuEmptyArrays
{
  boolean[] BOOLEAN0=new boolean[0];
  byte   [] BYTE0   =new byte   [0];
  int    [] INT0    =new int    [0];
  double [] DOUBLE0 =new double [0];
  float  [] FLOAT0  =new float  [0];

  Class [] CLASS0   =new Class  [0];
  Object[] OBJECT0  =new Object [0];
  String[] STRING0  =new String [0];
}
