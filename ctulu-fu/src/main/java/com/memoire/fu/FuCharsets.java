/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuCharset.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    2004-2005 Guillaume Desnoix
 */

package com.memoire.fu;

import java.nio.charset.Charset;

public final class FuCharsets
{
  private static final Object[] CHARSETS=
    Charset.availableCharsets().values().toArray();

  public static final Charset[] getCharsets()
  {
    int       l=CHARSETS.length;
    Charset[] r=new Charset[l];
    System.arraycopy(CHARSETS,0,r,0,l);
    return r;
  }
}
