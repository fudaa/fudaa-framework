/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuHashtableFast.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * A fast hashtable not synchronized and using == as test.
 */
public final class FuHashtableFast
    // implements Serializable
{
  static final class Entry
      // implements Serializable
  {
    public int    hash;
    public Object key;
    public Object value;
    public Entry  next;
  }

  private Entry table[];
  private int   count;
  private int   threshold;
  private float factor;

  public FuHashtableFast(int _capacity, float _factor)
  {
    if((_capacity<=0)||(_factor<=0.0))
      throw new IllegalArgumentException();

    factor=_factor;
    table     =new Entry[_capacity];
    threshold =(int)(_capacity*_factor);
  }

  public FuHashtableFast(int _capacity)
  {
    this(_capacity,0.75f);
  }

  public FuHashtableFast()
  {
    this(21,0.75f);
  }

  public final int size()
  {
    return count;
  }

  public final boolean isEmpty()
  {
    return (count==0);
  }

  public final Enumeration keys()
  {
    return new Enumerator(table,true);
  }

  public final Enumeration elements()
  {
    return new Enumerator(table,false);
  }

  public final boolean contains(Object _value)
  {
    if(_value==null) throw new NullPointerException();

    Entry[] tab=table;
    for(int i=tab.length; i-->0; )
    {
      for(Entry e=tab[i]; e!=null; e=e.next)
	if(e.value==_value)
	  return true;
    }
    return false;
  }

  public final boolean containsKey(Object _key)
  {
    Entry[] tab  =table;
    int     hash =_key.hashCode();
    int     index=(hash&0x7FFFFFFF)%tab.length;

    for(Entry e=tab[index]; e!=null; e=e.next)
      if((e.hash==hash)&&(e.key==_key))
	return true;

    return false;
  }

  public final Object get(Object _key)
  {
    Entry[] tab  =table;
    int     hash =_key.hashCode();
    int     index=(hash&0x7FFFFFFF)%tab.length;

    for(Entry e=tab[index]; e!=null; e=e.next)
      if((e.hash==hash)&&(e.key==_key))
	return e.value;

    return null;
  }

  private final void rehash()
  {
    int     oldCapacity=table.length;
    Entry[] oldTable   =table;

    int   newCapacity=oldCapacity*3+1;
    Entry newTable[] =new Entry[newCapacity];

    threshold=(int)(newCapacity*factor);
    table    =newTable;

    for(int i=oldCapacity; i-->0; )
    {
      for(Entry old=oldTable[i]; old!=null; )
      {
	Entry e=old;
	old=old.next;

	int index=(e.hash&0x7FFFFFFF)%newCapacity;
	e.next=newTable[index];
	newTable[index]=e;
      }
    }
  }

  public final Object put(Object _key, Object _value)
  {
    if(_value==null) throw new NullPointerException();

    Entry[] tab  =table;
    int     hash =_key.hashCode();
    int     index=(hash&0x7FFFFFFF)%tab.length;

    for(Entry e=tab[index]; e!=null; e=e.next)
    {
      if((e.hash==hash)&&(e.key==_key))
      {
	Object old=e.value;
	e.value=_value;
	return old;
      }
    }

    if(count>=threshold)
    {
      rehash();
      return put(_key,_value);
    } 

    Entry e   =new Entry();
    e.hash    =hash;
    e.key     =_key;
    e.value   =_value;
    e.next    =tab[index];
    tab[index]=e;
    count++;
    return null;
  }

  public final Object remove(Object key)
  {
    Entry[] tab=table;
    int hash =key.hashCode();
    int index=(hash & 0x7FFFFFFF) % tab.length;

    for(Entry e=tab[index], prev=null; e!=null; prev=e, e=e.next)
    {
      if((e.hash==hash)&&(e.key==key))
      {
	if(prev!=null)
	  prev.next =e.next;
	else
	  tab[index]=e.next;

	count--;
	return e.value;
      }
    }

    return null;
  }

  public final void clear()
  {
    Entry[] tab=table;
    for(int index=tab.length; --index>= 0; )
      tab[index]=null;
    count=0;
  }

  @Override
  public final String toString()
  {
    return "FuHashtableFast("+count+")";
  }

  private static final class Enumerator
    implements Enumeration
  {
    private boolean keys;
    private int     index;
    private Entry[] table;
    private Entry   entry;

    Enumerator(Entry[] _table, boolean _keys)
    {
      table=_table;
      keys =_keys;
      index=_table.length;
    }
	
    @Override
    public final boolean hasMoreElements()
    {
      if(entry!=null) return true;

      while(index-->0)
	if((entry=table[index])!=null)
	  return true;

      return false;
    }

    @Override
    public final Object nextElement()
    {
      if(entry==null)
	while((index>=0)&&((entry=table[index])==null))
	  index--;

      if(entry!=null)
      {
	Entry e=entry;
	entry=e.next;
	return (keys ? e.key : e.value);
      }
      throw new NoSuchElementException("FuHashtableFast.Enumerator");
    }
  }

  /*
  public static void main(String[] args)
  {
    final int NB=200000;

    Hashtable   nv=new Hashtable();
    FuHashtableFast fv=new FuHashtableFast();
    long debut,fin;

    Integer[]    te=new Integer[NB];
    for(int i=0;i<NB;i++)
      te[i]=FuFactoryInteger.get(i);

    debut=System.currentTimeMillis();
    for(int i=0;i<NB;i++)
      nv.put(te[i],te[i]);
    fin=System.currentTimeMillis();
    System.out.println("duree Stnd Hashtable: "+(fin-debut)+" ms");

    debut=System.currentTimeMillis();
    for(int i=0;i<NB;i++)
      fv.put(te[i],te[i]);
    fin=System.currentTimeMillis();
    System.out.println("duree Fast Hashtable: "+(fin-debut)+" ms");

    debut=System.currentTimeMillis();
    for(int i=0;i<NB/100;i++)
      nv.get(te[i]);
    fin=System.currentTimeMillis();
    System.out.println("duree Stnd Hashtable: "+(fin-debut)+" ms");

    debut=System.currentTimeMillis();
    for(int i=0;i<NB/100;i++)
      fv.get(te[i]);
    fin=System.currentTimeMillis();
    System.out.println("duree Fast Hashtable: "+(fin-debut)+" ms");
  }
  */
}
