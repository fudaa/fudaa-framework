/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuText.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

/*
import java.beans.*;
import java.io.*;
import java.net.*;
import java.lang.ref.*;
import java.lang.reflect.*;
import java.util.*;
import java.text.*;
import com.memoire.re.*;
*/

/**
 * Utility class with only static methods relative to plain text.
 */
public final class FuText
{
   static final String[] ENTITIES=
  {
    //html       char     mc    sc    accents
    "&nbsp;"    ,"\u00A0"," "  ," "  ,null,
    "&iexcl;"   ,"\u00A1","!"  ,"!"  ,null,
    "&cent;"    ,"\u00A2","|c" ,"c"  ,null,
    "&pound;"   ,"\u00A3","-L" ,"L"  ,null,
    "&curren;"  ,"\u00A4","=o" ,""   ,null,
    "&yen;"     ,"\u00A5","=Y" ,"Y"  ,null,
    "&brvbar;"  ,"\u00A6","|"  ,"|"  ,null,
    "&sect;"    ,"\u00A7","|S" ,""   ,null,
    "&uml;"     ,"\u00A8","|:" ,""   ,null,
    "&copy;"    ,"\u00A9","(c)","(c)",null,
    "&ordf;"    ,"\u00AA","_a" ,""   ,null,
    "&laquo;"   ,"\u00AB","\"" ,"\"" ,null,
    "&not;"     ,"\u00AC","-," ,"-"  ,null,
    "&shy;"     ,"\u00AD","-"  ,"-"  ,null,
    "&reg;"     ,"\u00AE","(r)","(r)",null,
    "&macr;"    ,"\u00AF","|-" ,"-"  ,null,
    "&deg;"     ,"\u00B0","|o" ,"o"  ,null,
    "&plusmn;"  ,"\u00B1","+-" ,"+-" ,null,
    "&sup2;"    ,"\u00B2","^2" ,"^2" ,null,
    "&sup3;"    ,"\u00B3","^3" ,"^3" ,null,
    "&acute;"   ,"\u00B4","\'" ,"\'" ,null,
    "&micro;"   ,"\u00B5",",u" ,"u"  ,null,
    "&para;"    ,"\u00B6","P|" ,""   ,null,
    "&middot;"  ,"\u00B7","."  ,"."  ,null,
    "&cedil;"   ,"\u00B8",","  ,""   ,null,
    "&sup1;"    ,"\u00B9","/1" ,""   ,null,
    "&ordm;"    ,"\u00BA","_o" ,""   ,null,
    "&raquo;"   ,"\u00BB","\"" ,"\"" ,null,
    "&frac14;"  ,"\u00BC","1/4","1/4",null,
    "&frac12;"  ,"\u00BD","1/2","1/2",null,
    "&frac34;"  ,"\u00BE","3/4","3/4",null,
    "&iquest;"  ,"\u00BF","?"  ,"?"  ,null,
    "&Agrave;"  ,"\u00C0","`A" ,"A"  ,"A" ,
    "&Aacute;"  ,"\u00C1","'A" ,"A"  ,"A" ,
    "&Acirc;"   ,"\u00C2","^A" ,"A"  ,"A" ,
    "&Atilde;"  ,"\u00C3","~A" ,"A"  ,"A" ,
    "&Auml;"    ,"\u00C4",":A" ,"A"  ,"A" ,
    "&Aring;"   ,"\u00C5","|A" ,"A"  ,"A" ,
    "&AElig;"   ,"\u00C6","AE" ,"AE" ,"A" ,
    "&Ccedil;"  ,"\u00C7",",C" ,"C"  ,"C" ,
    "&Egrave;"  ,"\u00C8","`E" ,"E"  ,"E" ,
    "&Eacute;"  ,"\u00C9","'E" ,"E"  ,"E" ,
    "&Ecirc;"   ,"\u00CA","^E" ,"E"  ,"E" ,
    "&Euml;"    ,"\u00CB",":E" ,"E"  ,"E" ,
    "&Igrave;"  ,"\u00CC","`I" ,"I"  ,"I" ,
    "&Iacute;"  ,"\u00CD","'I" ,"I"  ,"I" ,
    "&Icirc;"   ,"\u00CE","^I" ,"I"  ,"I" ,
    "&Iuml;"    ,"\u00CF",":I" ,"I"  ,"I" ,
    "&ETH;"     ,"\u00D0","-D" ,"D"  ,"D" ,
    "&Ntilde;"  ,"\u00D1","~N" ,"N"  ,"N" ,
    "&Ograve;"  ,"\u00D2","`O" ,"O"  ,"O" ,
    "&Oacute;"  ,"\u00D3","'O" ,"O"  ,"O" ,
    "&Ocirc;"   ,"\u00D4","^O" ,"O"  ,"O" ,
    "&Otilde;"  ,"\u00D5","~O" ,"O"  ,"O" ,
    "&Ouml;"    ,"\u00D6",":O" ,"O"  ,"O" ,
    "&times;"   ,"\u00D7","x"  ,"x"  ,null,
    "&Oslash;"  ,"\u00D8","/O" ,"O"  ,"O" ,
    "&Ugrave;"  ,"\u00D9","`U" ,"U"  ,"U" ,
    "&Uacute;"  ,"\u00DA","'U" ,"U"  ,"U" ,
    "&Ucirc;"   ,"\u00DB","^U" ,"U"  ,"U" ,
    "&Uuml;"    ,"\u00DC",":U" ,"U"  ,"U" ,
    "&Yacute;"  ,"\u00DD","'Y" ,"Y"  ,"Y" ,
    "&THORN;"   ,"\u00DE","|P" ,"P"  ,null,
    "&szlig;"   ,"\u00DF","ss" ,"B"  ,"ss",
    "&agrave;"  ,"\u00E0","`a" ,"a"  ,"a" ,
    "&aacute;"  ,"\u00E1","'a" ,"a"  ,"a" ,
    "&acirc;"   ,"\u00E2","^a" ,"a"  ,"a" ,
    "&atilde;"  ,"\u00E3","~a" ,"a"  ,"a" ,
    "&auml;"    ,"\u00E4",":a" ,"a"  ,"a" ,
    "&aring;"   ,"\u00E5","|a" ,"a"  ,"a" ,
    "&aelig;"   ,"\u00E6","ae" ,"ae" ,"ae",
    "&ccedil;"  ,"\u00E7","c," ,"c"  ,"c" ,
    "&egrave;"  ,"\u00E8","`e" ,"e"  ,"e" ,
    "&eacute;"  ,"\u00E9","'e" ,"e"  ,"e" ,
    "&ecirc;"   ,"\u00EA","^e" ,"e"  ,"e" ,
    "&euml;"    ,"\u00EB",":e" ,"e"  ,"e" ,
    "&igrave;"  ,"\u00EC","`i" ,"i"  ,"i" ,
    "&iacute;"  ,"\u00ED","'i" ,"i"  ,"i" ,
    "&icirc;"   ,"\u00EE","^i" ,"i"  ,"i" ,
    "&iuml;"    ,"\u00EF",":i" ,"i"  ,"i" ,
    "&eth;"     ,"\u00F0","-d" ,"d"  ,"d" ,
    "&ntilde;"  ,"\u00F1","~n" ,"n"  ,"n" ,
    "&ograve;"  ,"\u00F2","`o" ,"o"  ,"o" ,
    "&oacute;"  ,"\u00F3","'o" ,"o"  ,"o" ,
    "&ocirc;"   ,"\u00F4","^o" ,"o"  ,"o" ,
    "&otilde;"  ,"\u00F5","~o" ,"o"  ,"o" ,
    "&ouml;"    ,"\u00F6",":o" ,"o"  ,"o" ,
    "&divide;"  ,"\u00F7",":-" ,"/"  ,null,
    "&oslash;"  ,"\u00F8","/o" ,"o"  ,"o" ,
    "&ugrave;"  ,"\u00F9","`u" ,"u"  ,"u" ,
    "&uacute;"  ,"\u00FA","'u" ,"u"  ,"u" ,
    "&ucirc;"   ,"\u00FB","^u" ,"u"  ,"u" ,
    "&uuml;"    ,"\u00FC","�"  ,"u"  ,"u" ,
    "&yacute;"  ,"\u00FD","'y" ,"y"  ,"y" ,
    "&thorn;"   ,"\u00FE","|p" ,"t"  ,null,
    "&yuml;"    ,"\u00FF",":y" ,"y"  ,"y" ,
    /*
    "&OElig;"   ,"\u0152","OE" ," ",
    "&oelig;"   ,"\u0153",
    "&Scaron;"  ,"\u0160",
    "&scaron;"  ,"\u0161",
    "&Yuml;"    ,"\u0178",
    "&circ;"    ,"\u02C6",
    "&tilde;"   ,"\u02DC",
    "&ensp;"    ,"\u2002",
    "&emsp;"    ,"\u2003",
    "&thinsp;"  ,"\u2009",
    "&zwnj;"    ,"\u200C",
    "&zwj;"     ,"\u200D",
    "&lrm;"     ,"\u200E",
    "&rlm;"     ,"\u200F",
    "&ndash;"   ,"\u2013",
    "&mdash;"   ,"\u2014",
    "&lsquo;"   ,"\u2018",
    "&rsquo;"   ,"\u2019",
    "&sbquo;"   ,"\u201A",
    "&ldquo;"   ,"\u201C",
    "&rdquo;"   ,"\u201D",
    "&bdquo;"   ,"\u201E",
    "&dagger;"  ,"\u2020",
    "&Dagger;"  ,"\u2021",
    "&permil;"  ,"\u2030",
    "&lsaquo;"  ,"\u2039",
    "&rsaquo;"  ,"\u203A",
    "&euro;"    ,"\u20AC",
    "&fnof;"    ,"\u0192",
    "&Alpha;"   ,"\u0391",
    "&Beta;"    ,"\u0392",
    "&Gamma;"   ,"\u0393",
    "&Delta;"   ,"\u0394",
    "&Epsilon;" ,"\u0395",
    "&Zeta;"    ,"\u0396",
    "&Eta;"     ,"\u0397",
    "&Theta;"   ,"\u0398",
    "&Iota;"    ,"\u0399",
    "&Kappa;"   ,"\u039A",
    "&Lambda;"  ,"\u039B",
    "&Mu;"      ,"\u039C",
    "&Nu;"      ,"\u039D",
    "&Xi;"      ,"\u039E",
    "&Omicron;" ,"\u039F",
    "&Pi;"      ,"\u03A0",
    "&Rho;"     ,"\u03A1",
    "&Sigma;"   ,"\u03A3",
    "&Tau;"     ,"\u03A4",
    "&Upsilon;" ,"\u03A5",
    "&Phi;"     ,"\u03A6",
    "&Chi;"     ,"\u03A7",
    "&Psi;"     ,"\u03A8",
    "&Omega;"   ,"\u03A9",
    "&alpha;"   ,"\u03B1",
    "&beta;"    ,"\u03B2",
    "&gamma;"   ,"\u03B3",
    "&delta;"   ,"\u03B4",
    "&epsilon;" ,"\u03B5",
    "&zeta;"    ,"\u03B6",
    "&eta;"     ,"\u03B7",
    "&theta;"   ,"\u03B8",
    "&iota;"    ,"\u03B9",
    "&kappa;"   ,"\u03BA",
    "&lambda;"  ,"\u03BB",
    "&mu;"      ,"\u03BC",
    "&nu;"      ,"\u03BD",
    "&xi;"      ,"\u03BE",
    "&omicron;" ,"\u03BF",
    "&pi;"      ,"\u03C0",
    "&rho;"     ,"\u03C1",
    "&sigmaf;"  ,"\u03C2",
    "&sigma;"   ,"\u03C3",
    "&tau;"     ,"\u03C4",
    "&upsilon;" ,"\u03C5",
    "&phi;"     ,"\u03C6",
    "&chi;"     ,"\u03C7",
    "&psi;"     ,"\u03C8",
    "&omega;"   ,"\u03C9",
    "&thetasym;","\u03D1",
    "&upsih;"   ,"\u03D2",
    "&piv;"     ,"\u03D6",
    "&bull;"    ,"\u2022",
    "&hellip;"  ,"\u2026",
    "&prime;"   ,"\u2032",
    "&Prime;"   ,"\u2033",
    "&oline;"   ,"\u203E",
    "&frasl;"   ,"\u2044",
    "&weierp;"  ,"\u2118",
    "&image;"   ,"\u2111",
    "&real;"    ,"\u211C",
    "&trade;"   ,"\u2122",
    "&alefsym;" ,"\u2135",
    "&larr;"    ,"\u2190",
    "&uarr;"    ,"\u2191",
    "&rarr;"    ,"\u2192",
    "&darr;"    ,"\u2193",
    "&harr;"    ,"\u2194",
    "&crarr;"   ,"\u21B5",
    "&lArr;"    ,"\u21D0",
    "&uArr;"    ,"\u21D1",
    "&rArr;"    ,"\u21D2",
    "&dArr;"    ,"\u21D3",
    "&hArr;"    ,"\u21D4",
    "&forall;"  ,"\u2200",
    "&part;"    ,"\u2202",
    "&exist;"   ,"\u2203",
    "&empty;"   ,"\u2205",
    "&nabla;"   ,"\u2207",
    "&isin;"    ,"\u2208",
    "&notin;"   ,"\u2209",
    "&ni;"      ,"\u220B",
    "&prod;"    ,"\u220F",
    "&sum;"     ,"\u2211",
    "&minus;"   ,"\u2212",
    "&lowast;"  ,"\u2217",
    "&radic;"   ,"\u221A",
    "&prop;"    ,"\u221D",
    "&infin;"   ,"\u221E",
    "&ang;"     ,"\u2220",
    "&and;"     ,"\u2227",
    "&or;"      ,"\u2228",
    "&cap;"     ,"\u2229",
    "&cup;"     ,"\u222A",
    "&int;"     ,"\u222B",
    "&there4;"  ,"\u2234",
    "&sim;"     ,"\u223C",
    "&cong;"    ,"\u2245",
    "&asymp;"   ,"\u2248",
    "&ne;"      ,"\u2260",
    "&equiv;"   ,"\u2261",
    "&le;"      ,"\u2264",
    "&ge;"      ,"\u2265",
    "&sub;"     ,"\u2282",
    "&sup;"     ,"\u2283",
    "&nsub;"    ,"\u2284",
    "&sube;"    ,"\u2286",
    "&supe;"    ,"\u2287",
    "&oplus;"   ,"\u2295",
    "&otimes;"  ,"\u2297",
    "&perp;"    ,"\u22A5",
    "&sdot;"    ,"\u22C5",
    "&lceil;"   ,"\u2308",
    "&rceil;"   ,"\u2309",
    "&lfloor;"  ,"\u230A",
    "&rfloor;"  ,"\u230B",
    "&lang;"    ,"\u2329",
    "&rang;"    ,"\u232A",
    "&loz;"     ,"\u25CA",
    "&spades;"  ,"\u2660",
    "&clubs;"   ,"\u2663",
    "&hearts;"  ,"\u2665",
    "&diams;"   ,"\u2666",

    "&quot;"    ,"\\u0022", // quote quote
    "&lt;"      ,"\u003C",
    "&gt;"      ,"\u003E",
    "&apos;"    ,"\u0027",
    "&amp;"     ,"\u0026",   // put at the end to avoid double decoding
    */
  };

  public static final String textToAscii(String _s)
  {
    String s=_s;
    for(int i=0;i<ENTITIES.length;i+=5)
        s=FuLib.replace(s,ENTITIES[i+1],ENTITIES[i+2]);
    return s;
  }

  public static final String textToSingleAscii(String _s)
  {
    String s=_s;
    for(int i=0;i<ENTITIES.length;i+=5)
        s=FuLib.replace(s,ENTITIES[i+1],ENTITIES[i+3]);
    return s;
  }

  /**
   * Remove accents while keeping any non-letter char.
   * @param _s the initial string
   * @return the modified string
   */
  public static final String removeAccents(String _s)
  {
    String s=_s;
    for(int i=0;i<ENTITIES.length;i+=5)
      if(ENTITIES[i+4]!=null)
        s=FuLib.replace(s,ENTITIES[i+1],ENTITIES[i+4]);
    return s;
  }

  /**
   * Clean the string by replacing invalid chars by the given char.
   * Valid chars are 0-9, a-z, A-Z and dot.
   * @param _s the initial string
   * @return the modified string
   */
  public static final String clean(String _s,char _c)
  {
    if(_s==null) return null;

    String s=removeAccents(_s);

    int          len=s.length();
    StringBuffer tmp=new StringBuffer(len);
    boolean      us =true;
    for(int i=0;i<len;i++)
    {
      char c=s.charAt(i);
      if((c=='.')||
         ((c>='0')&&(c<='9'))||
         ((c>='A')&&(c<='Z'))||
         ((c>='a')&&(c<='z')))
	{ tmp.append(c); us=false; }
      else
	if(!us) { tmp.append(_c); us=true; }
    }

    s=tmp.toString();
    len=s.length();

    int j=len;
    while((j>1)&&(s.charAt(j-1)==_c)) j--;
    if(j!=len) s=s.substring(0,j);

    return s;
  }
}
