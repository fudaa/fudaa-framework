/**
 * @modification $Date: 2007-05-04 13:42:01 $
 * @statut       unstable
 * @file         FuPreferences.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

/**
 * A class to manage user preferences in a file of properties.<p>
 * 
 * <b>Important</b> : Les propri�t�s sont r�parties dans autant de fichiers qu'il y a de classes filles de 
 * {@link FuPreferences} instanci�es. Pour eviter ca, il est necessaire <b>AU TOUT DEBUT</b> d'une application de d�finir le
 * fichier de ressources par la m�thode {@link #setRoot(String)}.
 * 
 * @author G.Desnoix
 * @version $Id$
 */

public class FuPreferences
{
  private static final boolean TRACE=false;

//  public static final FuPreferences FU=new FuPreferences();

  private   boolean    dirty_;
  protected Properties values_;
  protected Vector     listeners_;
  /** Le nom du fichier des ressources pour qu'il soit unique. */
  protected static String root_;
    
  // Constructeur "interdit"
  
  protected FuPreferences()
  {
    dirty_    =false;
    values_   =new Properties();
    listeners_=new Vector();

    readIniFile();
  }

  /** 
   * Definit la racine du nom du fichier de pr�f�rences. 
   * @param _root Le nom du fichier de pr�f�rences. Peut �tre null.
   */
  public static void setRoot(String _root) {
    root_=_root;
  }
  
  public final String getRoot()
  {
    if (root_!=null) return root_;
    
    Class  c=getClass();
    String f=c.getName();

    int i=f.lastIndexOf('.');
    if(i>=0) f=f.substring(i+1);

    if(f.endsWith("Preferences")) f=f.substring(0,f.length()-11);
    f=f.toLowerCase();

    return f;
  }

  protected String getPath()
  {
    String f=getRoot();
    String p=FuLib.getUserHome();

    if(!"".equals(p)) p+=File.separator;

         if(FuLib.isWindows()) f=p+f+".ini";
    else if(FuLib.isUnix   ()) f=p+"."+f+"rc";
    else                       f=p+f+".preferences";

    //FuLog.debug("getPath -> "+f);
    return f;
  }

  public final synchronized void readIniFile()
  {
    if(TRACE&&FuLog.isTrace())
      FuLog.trace("FPR: "+getRoot()+": reading preferences");
    FileInputStream fileInputStream =null;
    try
    {
      fileInputStream = new FileInputStream(getPath());
      values_.load(fileInputStream);
    }
    catch(Throwable th)
    {
    }
    finally{
      FuLib.safeClose(fileInputStream);
    }
    addDefaultValues();
    dirty_=false;
  }
  
  public final synchronized void writeIniFile()
  {
    if(!dirty_) return;

    if(TRACE&&FuLog.isTrace())
      FuLog.trace("FPR: "+getRoot()+": writing preferences");

    removeDefaultValues();
    FileOutputStream fileOutputStream =null;
    try
    {
      fileOutputStream = new FileOutputStream(getPath());
      values_.store(fileOutputStream,
		  "Preferences for "+getRoot());
    }
    catch(Throwable th)
    {
      FuLog.warning("FPR: couldn't write "+getPath());
    }
    finally{
      FuLib.safeClose(fileOutputStream);
    }
    addDefaultValues();
    dirty_=false;
  }

  protected void addDefaultValues() { }
  protected void removeDefaultValues() { }

  // Basic

  public final Enumeration keys()
  {
    return values_.keys();
  }

  public final int size()
  {
    return values_.size();
  }

  public final Object[][] getArray()
  {
    return FuLib.convertHashtableToArray(values_);
  }

  private final String get(String _key)
  {
    return values_.getProperty(_key);
  }

  private final void remove(String _key)
  {
    String old=get(_key);
    values_.remove(_key);

    dirty_=true;
    firePropertyChange(_key,old,null);
  }

  private final void put(String _key, String _value)
  {
    String old=getStringProperty(_key);
    if(_value==null) values_.remove(_key);
    else             values_.put(_key,_value);

    dirty_=true;
    firePropertyChange(_key,old,_value);
  }

  protected void firePropertyChange(String _p, String _o, String _n)
  {
    PropertyChangeEvent e=new PropertyChangeEvent(this,_p,_o,_n);
    Enumeration         l=listeners_.elements();
    while(l.hasMoreElements())
      ((PropertyChangeListener)l.nextElement()).propertyChange(e);
  }

  public void addPropertyChangeListener(PropertyChangeListener _l)
  { listeners_.addElement(_l); }

  public void removePropertyChangeListener(PropertyChangeListener _l)
  { listeners_.removeElement(_l); }

  // GET
  
  /*
  public final String getProperty(String _key)
    { return getProperty(_key,""); }

  public final String getProperty(String _key, String _default)
  {
    String r=get(_key);
    if(r==null) r=_default;
    return r;
  }
  */

  
  public final String getStringProperty(String _key)
    { return getStringProperty(_key,""); }

  public final String getStringProperty(String _key, String _default)
  {
    String r=get(_key);
    if(r==null) r=_default;
    return r;
  }
  
  public final boolean getBooleanProperty(String _key)
    { return getBooleanProperty(_key,false); }

  public final boolean getBooleanProperty(String _key, boolean _default)
  {
    String  s=get(_key);
    boolean r=_default;

    if("false".equals(s)) r=false;
    if("true" .equals(s)) r=true;
    if("no"   .equals(s)) r=false;
    if("yes"  .equals(s)) r=true;
    if("0"    .equals(s)) r=false;
    if("1"    .equals(s)) r=true;

    return r;
  }

  public final int getIntegerProperty(String _key)
    { return getIntegerProperty(_key,0); }

  public final int getIntegerProperty(String _key, int _default)
  {
    String  s=get(_key);
    int     r=_default;

    try { r=Integer.parseInt(s); }
    catch(Exception ex) { }

    return r;
  }

  public final double getDoubleProperty(String _key)
    { return getDoubleProperty(_key,0.); }

  public final double getDoubleProperty(String _key, double _default)
  {
    String s=get(_key);
    double r=_default;

    try { r=new Double(s).doubleValue(); } // Double.parseDouble
    catch(Exception ex) { }

    return r;
  }

  // PUT

  public void putStringProperty(String _key, String _value)
  {
    put(_key,_value);
  }

  public void putBooleanProperty(String _key, boolean _value)
  {
    put(_key,""+_value);
  }

  public void putIntegerProperty(String _key, int _value)
  {
    put(_key,""+_value);
  }

  public void putDoubleProperty(String _key, double _value)
  {
    put(_key,""+_value);
  }

  // REMOVE

  public final void removeProperty(String _key)
  {
    if(_key!=null) remove(_key);
  }

  /**
   * Renvoie toutes les proprietes du projet y compris les valeurs par defaut.
   */
  public Enumeration allKeysWithDefaults(){
    return values_.propertyNames();
  }
}

