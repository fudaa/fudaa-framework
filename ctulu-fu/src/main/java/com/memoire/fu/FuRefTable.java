/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuRefTable.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * A cache using weak references for keys and
 * soft references for values on 1.2+.
 */
public class FuRefTable
{
  public static final int STRONG=0;
  public static final int SOFT  =1;
  public static final int WEAK  =2;

  private static final Object createWeakReference(Object _o2)
  {
    class WK extends WeakReference
    {
      public WK(Object _o)
      {
        super(_o);
      }

      @Override
      public boolean equals(Object _o)
      {
        Object oInit=_o;
        if(!(oInit instanceof WK))
        {
          FuLog.warning("not a WK: "+oInit);
          return false;
        }

        oInit=((WK)oInit).get();
        
        Object o=get();
        if(o==null) return oInit==null;
        
        return o.equals(oInit);
      }
      
      @Override
      public int hashCode()
      {
        Object o=get();
        return o==null ? 0 : o.hashCode();
      }
    }

    return new WK(_o2);
  }

  private static final Object createSoftReference(Object _o2)
  {
    class SK extends SoftReference
    {
      public SK(Object _o)
      {
        super(_o);
      }

      @Override
      public boolean equals(Object _o)
      {
        Object oInit=_o;
        if(!(oInit instanceof SK))
        {
          FuLog.warning("not a WK: "+oInit);
          return false;
        }

        oInit=((SK)oInit).get();
        
        Object o=get();
        if(o==null) return oInit==null;
        
        return o.equals(oInit);
      }
      
      @Override
      public int hashCode()
      {
        Object o=get();
        return o==null ? 0 : o.hashCode();
      }
    }

    return new SK(_o2);
  }

  private int       keyType_;
  private int       valType_;
  private Hashtable table_;

  public FuRefTable(int _keyType,int _valType)
  {
    keyType_=_keyType;
    valType_=_valType;
    table_  =new Hashtable();
  }

  private Object createKeyReference(Object _o)
  {
    Object r;

    switch(keyType_)
    {
    case SOFT: r=createSoftReference(_o); break;
    case WEAK: r=createWeakReference(_o); break;
    default:   r=_o;
    }
    return r;
  }

  private Object createValueReference(Object _o)
  {
    Object r;

    switch(valType_)
    {
    case SOFT: r=createSoftReference(_o); break;
    case WEAK: r=createWeakReference(_o); break;
    default:   r=_o;
    }
    return r;
  }

  public Object get(Object _key)
  {
    Object key=_key;
    if(FuLib.jdk()>=1.2)
      key=createKeyReference(key);

    Object v=table_.get(key);
    if(v!=null)
    {
      if(FuLib.jdk()>=1.2)
      {
        if(v instanceof Reference)
          v=((Reference)v).get();
        if(v==null) table_.remove(key);
      }
    }
    return v;
  }

  public void put(Object _key,Object _value)
  {
    Object key=_key;
    Object value=_value;
    if(FuLib.jdk()>=1.2)
      key=createKeyReference(key);

    if(value==null)
    {
      table_.remove(key);
    }
    else
    {
      if(FuLib.jdk()>=1.2)
        value=createValueReference(value);
      table_.put(key,value);
    }
  }

  public void clear()
  {
    table_.clear();
  }

  public boolean trim()
  {
    boolean done=false;
    if(FuLib.jdk()>=1.2)
    {
      Enumeration e=table_.keys();
      while(e.hasMoreElements())
      {
        Object o=e.nextElement();
        if(o instanceof Reference)
        {
          Reference r=(Reference)o;
          o=r.get();
          if(o==null)
          {
            table_.remove(r);
            done=true;
          }
        }
      }
    }
    return done;
  }

  public Object[] keys()
  {
    int l=table_.size();
    FuVectorFast v=new FuVectorFast(l);
    Enumeration e=table_.keys();
    while(e.hasMoreElements())
    {
      Object o=e.nextElement();
      if(FuLib.jdk()>=1.2)
      {
        if(o instanceof Reference)
        {
          Reference r=(Reference)o;
          o=r.get();
          if(o==null) table_.remove(r);
        }
      }
      if(o!=null) v.addElement(o);
    }
    l=v.size();
    Object[] r=new Object[l];
    v.copyInto(r);
    return r;
  }
}
