/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuUnicodeFilterInputStream.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

import java.io.IOException;
import java.io.Writer;

/**
 * An UnicodeFilterWriter converts any non-ASCII character
 * into a \\u#### sequence where # is an hexa number.
 * This class is thread-safe.
 */
public class FuUnicodeFilterWriter
  extends Writer
{
  private static final char[] HEXA=
  { '0','1','2','3',
    '4','5','6','7',
    '8','9','a','b',
    'c','d','e','f' };

  private Writer writer_;
  private char[] buf_;

  public FuUnicodeFilterWriter(Writer _writer)
  {
    super();
    writer_=_writer;
    buf_   =new char[6];
    buf_[0]='\\';
    buf_[1]='u';
  }

  public FuUnicodeFilterWriter(Writer _writer,Object _lock)
  {
    super(_lock);
    writer_=_writer;
    buf_   =new char[6];
    buf_[0]='\\';
    buf_[1]='u';
  }

  @Override
  public void write(int _b)
    throws IOException
  {
    synchronized(lock)
    {
      if((_b>=0)&&(_b<=127))
      {
        writer_.write(_b);
      }
      else
      {
        buf_[2]=HEXA[(_b>>12)&0x000F];
        buf_[3]=HEXA[(_b>> 8)&0x000F];
        buf_[4]=HEXA[(_b>> 4)&0x000F];
        buf_[5]=HEXA[(_b    )&0x000F];
        writer_.write(buf_,0,6);
      }
    }
  }

  @Override
  public void write(char _buffer[],int _offset,int _length)
    throws IOException
  {
    if(_buffer==null) throw new NullPointerException();

    if((_offset<0)||(_offset>_buffer.length)||(_length<0)||
       ((_offset+_length)>_buffer.length)||((_offset+_length)<0))
      throw new IndexOutOfBoundsException();

    if(_length==0) return;

    synchronized(lock)
    {
      for(int i=0;i<_length;i++)
        write(_buffer[_offset+i]);
    }
  }

  @Override
  public void flush()
    throws IOException
  {
    synchronized(lock)
    {
      writer_.flush();
    }
  }

  @Override
  public void close()
    throws IOException
  {
    synchronized(lock)
    {
      writer_.close();
    }
  }

  /*
  public static void main(String[] _args)
    throws IOException
  {
    Writer out=new FuUnicodeFilterWriter(new OutputStreamWriter(System.out));
    int c;
    while((c=System.in.read())!=-1)
    {
      out.write(c);
      if(c=='\n') out.flush();
    }
  }
  */
}

