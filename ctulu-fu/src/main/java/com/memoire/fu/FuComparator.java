/**
 * @modification $Date: 2006-04-26 08:44:42 $
 * @statut       unstable
 * @file         FuComparator.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

import java.util.Comparator;

public interface FuComparator extends Comparator
{
  @Override
  int compare(Object _a, Object _b);

  class Reverse implements FuComparator
  {
    private FuComparator c_;

    public Reverse(FuComparator _c)
    {
      c_=_c;
    }

    @Override
    public int compare(Object _a, Object _b)
    {
      return -c_.compare(_a,_b);
    }
  }

  FuComparator STRING_COMPARATOR=new FuComparator()
  {
    @Override
    public int compare(Object _a, Object _b)
    {
      return _a.toString().compareTo(_b.toString());
    }
  };

  FuComparator STRING_IGNORE_CASE_COMPARATOR=new FuComparator()
  {
    @Override
    public int compare(Object _a, Object _b)
    {
      int r;
//      if(FuLib.jdk()>=1.2)
	r=_a.toString().compareToIgnoreCase(_b.toString());
//      else
//	r=_a.toString().toLowerCase().compareTo(_b.toString().toLowerCase());

      if(r==0) r=_a.toString().compareTo(_b.toString());
      return r;
    }
  };

  FuComparator REVERSE_STRING_COMPARATOR=new FuComparator()
  {
    @Override
    public int compare(Object _a, Object _b)
    {
      return _b.toString().compareTo(_a.toString());
    }
  };
}
