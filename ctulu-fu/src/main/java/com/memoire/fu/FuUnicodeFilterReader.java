/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuUnicodeFilterReader.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

import java.io.CharConversionException;
import java.io.IOException;
import java.io.Reader;

/**
 * An UnicodeFilterReader converts any \\u#### sequence
 * (where # is an hexa number) into the corresponding character.
 * This class is thread-safe.
 */
public class FuUnicodeFilterReader
  extends Reader
{
  private int          p_=-1;
  //private StringBuffer b_=new StringBuffer(4);
  private Reader       reader_;

  public FuUnicodeFilterReader(Reader _reader)
  {
    super();
    reader_=_reader;
  }

  public FuUnicodeFilterReader(Reader _reader,Object _lock)
  {
    super(_lock);
    reader_=_reader;
  }

  @Override
  public int read() throws IOException
  {
    int i;
    synchronized(lock)
    {
      if(p_!=-1)
      {
        i=p_;
        p_=-1;
      }
      else
      {  
        i=reader_.read();
        if(i=='\\')
        {
          int j=reader_.read();
          if(j!='u')
	  {
            p_=j;
          }
          else
	  {
            i=0;
            for(int k=0;k<4;k++)
            {
              j=reader_.read();
              if(j==-1)
                throw new CharConversionException
                  ("invalid unicode sequence at the end of stream");
                   if((j>='0')&&(j<='9')) j-='0';
              else if((j>='a')&&(j<='f')) j-='a'-10;
              else if((j>='A')&&(j<='F')) j-='A'-10;
              else throw new CharConversionException
                     ("invalid unicode sequence");
              i=(i<<4)|j;
            }

            /*
            b_.setLength(0);
            for(int k=0;k<4;k++)
            {
              j=reader_.read();
              if(j==-1)
                throw new CharConversionException
                  ("invalid unicode sequence at the end \\u"+b_);
              b_.append((char)j);
            }
            try
            {
              i=Integer.parseInt(b_.toString(),16);
            }
            catch(NumberFormatException ex)
            {
              throw new CharConversionException
                ("invalid unicode sequence \\u"+b_);
            }
            */
          }
        }
      }
    }
    return i;
  }

  @Override
  public int read(char _buffer[],int _offset, int _length)
    throws IOException
  {
    if(_buffer==null) throw new NullPointerException();

    if((_offset<0)||(_offset>_buffer.length)||(_length<0)||
       ((_offset+_length)>_buffer.length)||((_offset+_length)<0))
      throw new IndexOutOfBoundsException();

    if(_length==0) return 0;

    int c,i;
    synchronized(lock)
    {
      c=read();
      if(c==-1) return -1;

      _buffer[_offset]=(char)c;
    
      i=1;
      try
      {
        for(;i<_length;i++)
        {
          c=read();
          if(c==-1) break;
          _buffer[_offset+i]=(char)c;
        }
      }
      catch (IOException ex)
      {
      }
    }
    return i;
  }

  @Override
  public void close()
    throws IOException
  {
    synchronized(lock)
    {
      reader_.close();
    }
  }
}

