/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuLimitedInputStream.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

import java.io.IOException;
import java.io.InputStream;

public class FuLimitedInputStream
  extends InputStream
{
  private long        offset_;
  private long        length_;
  private InputStream input_;

  public FuLimitedInputStream(InputStream _input,long _offset,long _length)
  {
    input_=_input;
    offset_=_offset;
    length_=_length;
  }

  @Override
  public int read() throws IOException
  {
    if(offset_>0)
    {
      FuLib.skipFully(input_,offset_);
      offset_=0L;
    }

    if(length_<=0) return -1;

    int r=input_.read();
    if(r==-1) length_=0;
    else length_--;
    return r;
  }
}
