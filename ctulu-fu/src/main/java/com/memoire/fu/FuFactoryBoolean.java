/**
 * @modification $Date: 2005-08-16 12:58:08 $
 * @statut       unstable
 * @file         FuFactoryBoolean.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2004 Guillaume Desnoix
 */

package com.memoire.fu;

public final class FuFactoryBoolean
{
  public static final Boolean get(boolean _b)
  {
    return _b ? Boolean.TRUE : Boolean.FALSE;
  }
}
