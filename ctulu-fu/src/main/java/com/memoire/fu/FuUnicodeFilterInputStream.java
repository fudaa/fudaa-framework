/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuUnicodeFilterInputStream.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

import java.io.CharConversionException;
import java.io.IOException;
import java.io.InputStream;

/**
 * An UnicodeFilterInputStream converts any \\u#### sequence
 * (where # is an hexa number) into the corresponding character.
 * This class is not thread-safe.
 */
public class FuUnicodeFilterInputStream
  extends InputStream
{
  private int          p_=-1;
  //private StringBuffer b_=new StringBuffer(4);
  private InputStream  input_;

  public FuUnicodeFilterInputStream(InputStream _input)
  {
    input_=_input;
  }

  @Override
  public int read() throws IOException
  {
    int i;
    if(p_!=-1)
    {
      i=p_;
      p_=-1;
    }
    else
    {  
      i=input_.read();
      if(i=='\\')
      {
	int j=input_.read();
	if(j!='u')
	{
	  p_=j;
	}
	else
	{
          i=0;
	  for(int k=0;k<4;k++)
          {
            j=input_.read();
            if(j==-1)
              throw new CharConversionException
                ("invalid unicode sequence at the end of stream");
                 if((j>='0')&&(j<='9')) j-='0';
            else if((j>='a')&&(j<='f')) j-='a'-10;
            else if((j>='A')&&(j<='F')) j-='A'-10;
            else throw new CharConversionException
                   ("invalid unicode sequence");
            i=(i<<4)|j;
          }
          
          /*
          b_.setLength(0);
	  for(int k=0;k<4;k++)
          {
            j=input_.read();
            if(j==-1)
              throw new CharConversionException
                ("invalid unicode sequence at the end \\u"+b_);
            b_.append((char)j);
          }
          try
          {
            i=Integer.parseInt(b_.toString(),16);
          }
          catch(NumberFormatException ex)
          {
            throw new CharConversionException
              ("invalid unicode sequence \\u"+b_);
          }
          */
	}
      }
    }
    return i;
  }

  /*
  public static void main(String[] _args)
    throws IOException
  {
    InputStream in=new FuUnicodeFilterInputStream(System.in);
    int c;
    while((c=in.read())!=-1)
    {
      System.out.print((char)c);
    }
  }
  */
}

