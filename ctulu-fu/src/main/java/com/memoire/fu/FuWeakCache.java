/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuWeakCache.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;


/**
 * A cache using weak references for keys and
 * soft references for values on 1.2+.
 */
public class FuWeakCache
  extends FuRefTable
{
  public FuWeakCache()
  {
    super(WEAK,SOFT);
  }

  /*
  private static final Object createReference(Object _o)
  {
    class WK extends WeakReference
    {
      public WK(Object _o)
      {
        super(_o);
      }

      public boolean equals(Object _o)
      {
        if(!(_o instanceof WK)) return false;
        _o=((WK)_o).get();
        
        Object o=get();
        if(o==null) return _o==null;
        
        return o.equals(_o);
      }
      
      public int hashCode()
      {
        Object o=get();
        return o==null ? 0 : o.hashCode();
      }
    }

    return new WK(_o);
  }

  private Hashtable table_;

  public FuWeakCache()
  {
    table_=new Hashtable();
  }

  public Object get(Object _key)
  {
    if(FuLib.jdk()>=1.2)
      _key=createReference(_key);

    Object v=table_.get(_key);
    if(v!=null)
    {
      if(FuLib.jdk()>=1.2)
      {
        SoftReference r=(SoftReference)v;
        v=r.get();
        if(v==null) table_.remove(_key);
      }
    }
    return v;
  }

  public void put(Object _key,Object _value)
  {
    if(FuLib.jdk()>=1.2)
      _key=createReference(_key);

    if(_value==null)
    {
      table_.remove(_key);
    }
    else
    {
      if(FuLib.jdk()>=1.2)
        _value=new SoftReference(_value);
      table_.put(_key,_value);
    }
  }

  public void clear()
  {
    table_.clear();
  }

  public boolean trim()
  {
    boolean done=false;
    if(FuLib.jdk()>=1.2)
    {
      Enumeration e=table_.keys();
      while(e.hasMoreElements())
      {
        Object o=e.nextElement();
        if(o instanceof Reference)
        {
          Reference r=(Reference)o;
          o=r.get();
          if(o==null)
          {
            table_.remove(r);
            done=true;
          }
        }
      }
    }
    return done;
  }

  public Object[] keys()
  {
    int l=table_.size();
    FuVectorFast v=new FuVectorFast(l);
    Enumeration e=table_.keys();
    while(e.hasMoreElements())
    {
      Object o=e.nextElement();
      if(FuLib.jdk()>=1.2)
      {
        if(o instanceof Reference)
        {
          Reference r=(Reference)o;
          o=r.get();
          if(o==null) table_.remove(r);
        }
      }
      if(o!=null) v.addElement(o);
    }
    l=v.size();
    Object[] r=new Object[l];
    v.copyInto(r);
    return r;
  }
  */
}
