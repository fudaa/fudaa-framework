/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuHashtablePublic.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * A simple hashtable with public fields.
 * Needed for Yapod serialization.
 */
public class FuHashtablePublic
       implements Cloneable, Serializable
{
  static final class Entry
          implements Cloneable, Serializable
  {
    public transient int hash;

    public Object key;
    public Object value;
    public Entry  next;

    @Override
    public Object clone()
    {
      Entry r=new Entry();
      r.hash =hash;
      r.key  =key;
      r.value=value;
      r.next =((next!=null) ? (Entry)next.clone() : null);
      return r;
    }
  }

  private transient boolean rehashed_=false;

  public Entry table[];
  public int   count;
  public int   threshold;
  public float factor;

  public FuHashtablePublic(int _capacity, float _factor)
  {
    if((_capacity<=0)||(_factor<=0.0))
      throw new IllegalArgumentException();

    factor=_factor;
    table     =new Entry[_capacity];
    threshold =(int)(_capacity*_factor);
  }

  public FuHashtablePublic(int _capacity)
  {
    this(_capacity,0.75f);
  }

  public FuHashtablePublic()
  {
    this(11,0.75f);
  }

  public int size()
  {
    return count;
  }

  public boolean isEmpty()
  {
    return (count==0);
  }

  public Enumeration keys()
  {
    if(!rehashed_) rehash1();
    return new Enumerator(table,true);
  }

  public Enumeration elements()
  {
    if(!rehashed_) rehash1();
    return new Enumerator(table,false);
  }

  public boolean contains(Object _value)
  {
    if(_value==null) throw new NullPointerException();

    if(!rehashed_) rehash1();

    Entry[] tab=table;
    for(int i=tab.length; i-->0; )
    {
      for(Entry e=tab[i]; e!=null; e=e.next)
	if(e.value.equals(_value))
	  return true;
    }
    return false;
  }

  public boolean containsKey(Object _key)
  {
    if(!rehashed_) rehash1();

    Entry[] tab  =table;
    int     hash =_key.hashCode();
    int     index=(hash&0x7FFFFFFF)%tab.length;

    for(Entry e=tab[index]; e!=null; e=e.next)
      if((e.hash==hash)&&(e.key.equals(_key)))
	return true;

    return false;
  }

  public Object get(Object _key)
  {
    if(!rehashed_) rehash1();

    Entry[] tab  =table;
    int     hash =_key.hashCode();
    int     index=(hash&0x7FFFFFFF)%tab.length;

    for(Entry e=tab[index]; e!=null; e=e.next)
      if((e.hash==hash)&&(e.key.equals(_key)))
	return e.value;

    return null;
  }

  protected void rehash1()
  {
    rehash(table.length);
  }

  protected void rehash2()
  {
    rehash(table.length*2+1);
  }

  protected void rehash(int newCapacity)
  {
    rehashed_=true;

    int     oldCapacity=table.length;
    Entry[] oldTable   =table;

    Entry newTable[] =new Entry[newCapacity];

    threshold=(int)(newCapacity*factor);
    table    =newTable;

    for(int i=oldCapacity; i-->0; )
    {
      for(Entry old=oldTable[i]; old!=null; )
      {
	Entry e=old;
	old=old.next;

	e.hash=e.key.hashCode();
	int index=(e.hash&0x7FFFFFFF)%newCapacity;
	e.next=newTable[index];
	newTable[index]=e;
      }
    }
  }

  public Object put(Object _key, Object _value)
  {
    if(_value==null) throw new NullPointerException();

    if(!rehashed_) rehash1();

    Entry[] tab  =table;
    int     hash =_key.hashCode();
    int     index=(hash&0x7FFFFFFF)%tab.length;

    for(Entry e=tab[index]; e!=null; e=e.next)
    {
      if((e.hash==hash)&&(e.key.equals(_key)))
      {
	Object old=e.value;
	e.value=_value;
	return old;
      }
    }

    if(count>=threshold)
    {
      rehash2();
      return put(_key,_value);
    } 

    Entry e   =new Entry();
    e.hash    =hash;
    e.key     =_key;
    e.value   =_value;
    e.next    =tab[index];
    tab[index]=e;
    count++;
    return null;
  }

  public Object remove(Object key)
  {
    if(!rehashed_) rehash1();

    Entry[] tab=table;
    int hash =key.hashCode();
    int index=(hash & 0x7FFFFFFF) % tab.length;

    for(Entry e=tab[index], prev=null; e!=null; prev=e, e=e.next)
    {
      if((e.hash==hash)&&(e.key.equals(key)))
      {
	if(prev!=null)
	  prev.next =e.next;
	else
	  tab[index]=e.next;

	count--;
	return e.value;
      }
    }

    return null;
  }

  public synchronized void clear()
  {
    Entry[] tab=table;
    for(int i=tab.length; i-->0; )
      tab[i]=null;
    count=0;
  }

  @Override
  public final synchronized Object clone()
  {
    if(!rehashed_) rehash1();

    FuHashtablePublic r=null;

    try
    { 
      r=(FuHashtablePublic)super.clone();
      r.table=new Entry[table.length];
      for(int i=table.length; i-->0 ; )
      {
	r.table[i]=
	  (table[i]!=null) 
	  ? (Entry)table[i].clone()
	  : null;
      }
    }
    catch(CloneNotSupportedException ex)
    { 
      throw new InternalError();
    }

    return r;
  }

  @Override
  public final String toString()
  {
    return "FuHashtablePublic("+count+")";
  }

  private static final class Enumerator
    implements Enumeration
  {
    private boolean keys;
    private int     index;
    private Entry[] table;
    private Entry   entry;

    Enumerator(Entry[] _table, boolean _keys)
    {
      table=_table;
      keys =_keys;
      index=_table.length;
    }
	
    @Override
    public boolean hasMoreElements()
    {
      if(entry!=null) return true;

      while(index-->0)
	if((entry=table[index])!=null)
	  return true;

      return false;
    }

    @Override
    public Object nextElement()
    {
      if(entry==null)
	while((index>=0)&&((entry=table[index])==null))
	  index--;

      if(entry!=null)
      {
	Entry e=entry;
	entry=e.next;
	return (keys ? e.key : e.value);
      }

      throw new NoSuchElementException("FuHashtablePublic.Enumerator");
    }
  }
}
