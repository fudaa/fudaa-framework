/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuResource.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

import java.io.InputStream;
import java.util.Locale;
import java.util.Properties;

/**
 * Utility class to manage resources.
 * As localized strings, ...
 */
public class FuResource
{
  private static boolean defaultToEnglish_=false;

  public static final boolean isDefaultToEnglish()
  {
    return defaultToEnglish_;
  }

  public static final void setDefaultToEnglish(boolean _b)
  {
    defaultToEnglish_=_b;
  }

  public static final FuResource FU=new FuResource();

  // Strings

  /*
  public final String _(String _s)
  {
    return getString(_s);
  }
  */

  protected String getRoot(Class _c,Locale _l)
  {
    String f=_c.getName();

    int i=f.lastIndexOf('.');
    if(i>=0) f=f.substring(i+1);

    if(f.endsWith("Resource")) f=f.substring(0,f.length()-8);
    f=f.toLowerCase()+"_"+_l.getLanguage()+".txt";

    return f;
  }

  private void loadTable(Properties _pp,Locale _l)
  {
    Class  c=getClass();
    String f=getRoot(c,_l);

    InputStream sin=null;
    try
    {
      sin=c.getResourceAsStream(f);
      if(FuLib.jdk()<1.2) sin=new FuUnicodeFilterInputStream(sin);
      _pp.load(sin);
    }
    catch(Exception ex)
    {
    }
    finally
    {
      FuLib.safeClose(sin);
      //try { if(sin!=null) sin.close(); }
      //catch(IOException ex) { }
    }
    sin=null;
  }

  private Properties pp_=null;
  private Locale     pl_=null;

  public String getString(final String _s)
  {
    if(_s==null)      return null;
    if("".equals(_s)) return "";

    Locale l=Locale.getDefault();

    if("fr".equals(l.getLanguage()))
    {
      String v;

      int i=_s.lastIndexOf('[');
      if(i<=0)
      {
        v=_s;
      }
      else
      {
        int j=_s.indexOf(']',i+1);
        v=(j<=0) ? _s : getString(_s.substring(0,i)+_s.substring(j+1));
      }

      return v;
    }

    if(l!=pl_) pp_=null;

    if(pp_==null)
    {
      pp_=new Properties();
      pl_=l;

      if(isDefaultToEnglish()) loadTable(pp_,Locale.ENGLISH);
      loadTable(pp_,l);
    }

    boolean deux_points      =false;
    boolean points_suspension=false;
    boolean point_simple     =false;
    boolean souligne         =false;
    int     espaces          =0;
    String  k=_s;
    String  v=pp_.getProperty(k);

    if(v==null)
    {
      while(k.endsWith(" "))
      {
        k=k.substring(0,k.length()-1);
        espaces++;
      }
      v=pp_.getProperty(k);
    }

    if(v==null)
    {
      if(k.endsWith(":"))
      {
        k=k.substring(0,k.length()-1);
        deux_points=true;
      }

      if(k.endsWith("..."))
      {
        k=k.substring(0,k.length()-3);
        points_suspension=true;
      }

      if(k.endsWith("."))
      {
        k=k.substring(0,k.length()-1);
        point_simple=true;
      }

      if(deux_points||points_suspension||point_simple)
        v=pp_.getProperty(k);
    }

    if(v==null)
    {
      k=k.replace(' ','_');
      souligne=true;
      v=pp_.getProperty(k);
    }

    if((v==null)&&k.equals(k.toUpperCase()))
    {
      String t=k.substring(0,1)+k.substring(1).toLowerCase();
      v=pp_.getProperty(t);
      if(v!=null)
        v=v.substring(0,1)+v.substring(1).toUpperCase();
    }

    if((v==null)&&k.equals(k.toLowerCase()))
    {
      String t=k.substring(0,1).toUpperCase()+k.substring(1);
      v=pp_.getProperty(t);
      if(v!=null)
        v=v.substring(0,1).toLowerCase()+v.substring(1);
    }

    if(v!=null)
    {
      if(souligne||(v.indexOf('_')>=0))
        v=v.replace('_',' ');

      if(point_simple)      v+=".";
      if(points_suspension) v+="...";
      if(deux_points)       v+=":";

      if(espaces>0)
      {
        int n=_s.length();
        while(v.length()<n) v+=' ';
      }

      if(l.getLanguage().equals("eo"))
        v=v.replace('^','x');

      if(k!=_s) pp_.put(_s,v); //setProperty
    }
    else
    {
      int i=_s.lastIndexOf('[');
      if(i<=0)
      {
        v=_s;
      }
      else
      {
        int j=_s.indexOf(']',i+1);
        v=(j<=0) ? _s : getString(_s.substring(0,i)+_s.substring(j+1));
      }
    }

    return v;
  }

  public final String getString(final String _s,final Object... _p)
  {
    String r=getString(_s);
    for(int i=0;i<_p.length;i++)
      r=FuLib.replace(r,"{"+i+"}",_p[i]==null ? "null" : _p[i].toString());
    return r;
  }
}
