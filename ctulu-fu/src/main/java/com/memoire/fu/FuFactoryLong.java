/**
 * @modification $Date: 2005-08-16 12:58:08 $
 * @statut       unstable
 * @file         FuFactoryLong.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2004 Guillaume Desnoix
 */

package com.memoire.fu;

public final class FuFactoryLong
{
  private static final int    SIZE =0x80;
  private static final Long[] SMALL=new Long[SIZE];
  private static final Long[] TABLE=new Long[SIZE];

  static
  {
    for(int i=0;i<SIZE;i++)
      SMALL[i]=new Long(i);
    for(int i=0;i<SIZE;i++)
      TABLE[i]=new Long(SIZE+i);
  }

  public static final Long get(long _i)
  {
    Long r;

    if((_i>=0)&&(_i<SIZE))
      r=SMALL[(int)_i];
    else
    if(_i<0)
      r=new Long(_i);
    else
    {
      int n=((int)_i)&(SIZE-1);
      r=TABLE[n];
      if(r.intValue()!=_i)
      {
        r=new Long(_i);
        TABLE[n]=r;
      }
    }

    return r;
  }
}
