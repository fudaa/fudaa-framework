/**
 * @modification $Date: 2007-01-08 15:36:48 $
 * @statut       unstable
 * @file         Fu.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

public final class Fu {
  public static final boolean TRACE = false;
  public static final boolean DEBUG = false;

  private Fu() {}
}
