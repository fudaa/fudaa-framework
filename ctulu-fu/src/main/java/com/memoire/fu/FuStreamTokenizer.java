/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuStreamTokenizer.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StreamTokenizer;

public class FuStreamTokenizer
  extends StreamTokenizer
{
  private String origin_;

  public FuStreamTokenizer(InputStream _input,String _origin)
  {
    this(new InputStreamReader(_input),_origin);
  }

  public FuStreamTokenizer(Reader _reader,String _origin)
  {
    super(new FuUnicodeFilterReader(_reader));
    origin_=_origin;
  }

  public String origin()
  { return origin_; }
}
