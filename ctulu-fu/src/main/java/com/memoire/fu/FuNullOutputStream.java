/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuNullOutputStream.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2003 Guillaume Desnoix
 */

package com.memoire.fu;

import java.io.OutputStream;

public final class FuNullOutputStream extends OutputStream
{
  public FuNullOutputStream()
  {
  }

  @Override
  public void flush() { }
  @Override
  public void close() { }
  @Override
  public void write(int _c) { }
  @Override
  public void write(byte[] _b) { }
  @Override
  public void write(byte[] _b, int _off, int _len) { }
}
