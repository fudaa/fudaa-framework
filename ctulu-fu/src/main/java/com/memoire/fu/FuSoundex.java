/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuSoundex.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

public final class FuSoundex
{
  /*
    1       B, F, P, V
    2       C, G, J, K, Q, S, X, Z
    3       D, T
    4       L
    5       M, N    
    6       R
  */
  /*
    1       B, P
    2       C, K, Q
    3       D, T
    4       L
    5       M, N
    6       R
    7       G, J
    8       X, Z, S
    9       F, V
  */

  private static final int[] ENGLISH=new int[]
    { 0,1,2,3,0,1,2,0,0,2,2,4,5,5,0,1,2,6,2,3,0,1,0,2,0,2 };

  private static final int[] FRENCH=new int[]
    { 0,1,2,3,0,9,7,0,0,7,2,4,5,5,0,1,2,6,8,3,0,9,0,8,0,8 };


  /*
  private static String clean(String _s)
  {
    if(_s==null) return "";

    _s=FuText.removeAccents(_s);
    _s=_s.toUpperCase();
    int l=_s.length();
    StringBuffer r=new StringBuffer(l);
    for(int i=0;i<l;i++)
    {
      char c=_s.charAt(i);
      if((c>='A')&&(c<='Z'))
        if(ENGLISH[c-'A']!=0)
          r.append(c);
    }
    System.out.println("R="+r);
    return r.toString();
  }
  */
  
  public static String englishSoundex(String _s)
  {
    return soundex(_s,ENGLISH);
  }

  public static String frenchSoundex(String _s)
  {
   String s=FuText.removeAccents(_s.toUpperCase());
    return soundex(s,FRENCH);
  }

  protected static String soundex(String _s,int[] _data)
  {
    StringBuffer r=new StringBuffer(4);
    int l=_s.length();
    int n=0;
    int p=-1;
    for(int i=0;i<l;i++)
    {
      char c=_s.charAt(i);
      if(i==0)
      {
        r.append(c);
        continue;
      }

      int  b=-1;
      if((c>='A')&&(c<='Z')) b=_data[c-'A'];
      if((c>='a')&&(c<='z')) b=_data[c-'a'];
      if((b>0)&&(p!=b))
      {
        p=b;
        r.append((char)(b+'0'));//r|=b<<(8*n);
        n++;
        if(n>=3) break;
      }
    }
    //System.out.println("N="+r);
    return FuLib.rightpad(r.toString(),4,' ');
  }

  public static int englishCompare(String _a,String _b)
  {
    return compare(_a,_b,ENGLISH);
  }

  public static int frenchCompare(String _a,String _b)
  {
    String a=FuText.removeAccents(_a.toUpperCase());
    String b=FuText.removeAccents(_b.toUpperCase());
    return compare(a,b,FRENCH);
  }

  public static int levenshtein(String _s,String _t)
  {
    int n=_s.length();
    int m=_t.length();
 
    if(n==0) return m;
    if(m==0) return n;
 
    int[][] d=new int[n+1][m+1];
 
    for(int i=0;i<=n;i++) d[i][0]=i;
    for(int j=1;j<=m;j++) d[0][j]=j;
 
    for(int i=1;i<=n;i++)
    {
      char sc=_s.charAt(i-1);
      for(int j=1;j<=m;j++)
      {
        int v=d[i-1][j-1];
        if(_t.charAt(j-1)!=sc) v++;
        d[i][j]=Math.min(Math.min(d[i-1][j]+1,d[i][j-1]+1),v);
      }
    }
    return d[n][m];
  }

  /*
  private static boolean same(int _a,int _b)
  {
    return (_a==_b)&&(_a!=0);
  }
  */

  protected static int compare(String _a,String _b,int[] _data)
  {
    String a=soundex(_a,_data);
    String b=soundex(_b,_data);
    int n=39*levenshtein(a,b)+13*levenshtein(_a,_b);

    int v=100-n/3;
    if(v<  0) v=0;
    if(v>100) v=100;
    return v;

    /*
    int a=code(_a,_data);
    int b=code(_b,_data);
    int _nbb=8;
    int mask=0;
    for(int i=0;i<_nbb;i++)
      mask|=1<<i;
    int n=0;
    for(int p=0;p+_nbb<=32;p+=_nbb)
    {
      if((a&(mask<<p))!=(b&(mask<<p)))
        n+=_dn;
      if(p>0)
      {
        if(same((a&(mask<<(p-_nbb))),(b&(mask<<p))))
          n--;
        if(same((a&(mask<<p)),(b&(mask<<(p-_nbb)))))
          n--;
      }
      if(p+2*_nbb<=32)
      {
        if(same((a&(mask<<(p+_nbb))),(b&(mask<<p))))
          n--;
        if(same((a&(mask<<p)),(b&(mask<<(p+_nbb)))))
          n--;
      }
    }

    int v=100-(100*n/17);
    if(v<  0) v=0;
    if(v>100) v=100;
    return v;
    */
  }  

  public static void main(String[] _args)
  {
    int l=_args.length;
    for(int i=0;i<l;i++)
      for(int j=0;j<l;j++)
        if(i!=j)
        {
          String a=_args[i];
          String b=_args[j];
          System.out.println(a+" "+englishSoundex(a)+" "+b+" "+englishSoundex(b)+" D:"+levenshtein(a,b));

          int ve=englishCompare(a,b);
          int vf=frenchCompare(a,b);
          //System.out.println(Integer.toHexString(englishCode(a))+" "+Integer.toHexString(englishCode(b)));
          System.out.println
            (FuLib.rightpad(a,15,' ')+
             FuLib.rightpad(b,15,' ')+
             "en:"+
             FuLib.leftpad (""+ve,3,' ')+
             "% "+
             "fr:"+
             FuLib.leftpad (""+vf,3,' ')+
             "%");
        }
  }
}
