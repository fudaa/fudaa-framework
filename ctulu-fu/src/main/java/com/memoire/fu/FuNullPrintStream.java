/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuNullPrintStream.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2003 Guillaume Desnoix
 */

package com.memoire.fu;

import java.io.PrintStream;

public final class FuNullPrintStream extends PrintStream
{
  public FuNullPrintStream()
  {
    super(new FuNullOutputStream());
  }

  //public void flush() { }
  //public void close() { }
  //public boolean checkError() { return false; }

  @Override
  public void write(int _c) { }
  @Override
  public void write(byte[] _b) { }
  @Override
  public void write(byte[] _b, int _off, int _len) { }
  @Override
  public void print(boolean _x) { }
  @Override
  public void print(char _x) { }
  @Override
  public void print(int _x) { }
  @Override
  public void print(long _x) { }
  @Override
  public void print(float _x) { }
  @Override
  public void print(double _x) { }
  @Override
  public void print(char[] _x) { }
  @Override
  public void print(String _x) { }
  @Override
  public void print(Object _x) { }
  @Override
  public void println() { }
  @Override
  public void println(boolean _x) { }
  @Override
  public void println(char _x) { }
  @Override
  public void println(int _x) { }
  @Override
  public void println(long _x) { }
  @Override
  public void println(float _x) { }
  @Override
  public void println(double _x) { }
  @Override
  public void println(char[] _x) { }
  @Override
  public void println(String _x) { }
  @Override
  public void println(Object _x) { }
}
