/**
 * @modification $Date: 2005-08-16 12:58:08 $
 * @statut       unstable
 * @file         FuCompletor.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

public interface FuCompletor
{
  /**
   * Returns the completing sequence.
   * @param _s the whole buffer
   * @param _p the current cursor position
   * @return the characters to insert
   */
  String complete(String _s,int _p);
}

