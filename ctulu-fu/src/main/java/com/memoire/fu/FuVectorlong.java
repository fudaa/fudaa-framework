/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuVectorlong.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

import java.io.Serializable;
import java.util.NoSuchElementException;

/**
 * A vector for long values.
 * This a vector for primitive types.
 * This source code for this class is generated from FuVector.jgen.
 * Public fields needed for Yapod serialization.
 */
public class FuVectorlong
       implements Cloneable, Serializable
{
  public long data_[];
  public int count_;
  public int increment_;

  public FuVectorlong(int _capacity,int _increment)
  {
    super();
    data_     =new long[_capacity];
    count_    =0;
    increment_=_increment;
  }

  public FuVectorlong(int _capacity)
  {
    this(_capacity,0);
  }

  public FuVectorlong()
  {
    this(11,0);
  }

  public FuVectorlong(long[] _data)
  {
    super();
    data_     =_data;
    count_    =_data.length;
    increment_=1;
  }

  public final synchronized void copyInto(long[] _array)
  {
    for(int i=0;i<count_;i++)
      _array[i]=data_[i];
  }

  public final synchronized long[] toArray()
  {
    long[] r=new long[count_];

    for(int i=0;i<count_;i++)
      r[i]=data_[i];

    return r;
  }

  public final synchronized void trimToSize()
  {
    int old_capacity=data_.length;
    if(count_<old_capacity)
    {
      long[] old_data=data_;
      data_=new long[count_];
      System.arraycopy(old_data,0,data_,0,count_);
    }
  }

  public final synchronized void ensureCapacity(int _min)
  {
    if(_min>data_.length)
      resize0(_min);
  }

  private void resize0(int _min)
  {
    int   old_capacity=data_.length;
    long[] old_data    =data_;

    int new_capacity=
      (  (increment_>0)
       ? (old_capacity+increment_)
       : (old_capacity*2));

    if(new_capacity<_min)
      new_capacity=_min;

    data_=new long[new_capacity];
    System.arraycopy(old_data,0,data_,0,count_);
  }
    
  public final synchronized void setSize(int _new)
  {
    if((_new>count_)&&(_new>data_.length))
      resize0(_new);
    else
      for(int i=_new;i<count_;i++)
	data_[i]=0;

    count_=_new;
  }

  public final int capacity()
  {
    return data_.length;
  }

  public final int size()
  {
    return count_;
  }

  public final boolean isEmpty()
  {
    return count_==0;
  }

  public final synchronized Enumerator elements()
  {
    return new Enumerator(this);
  }
    
  public final boolean contains(long _o)
  {
    return (indexOf(_o,0)>=0);
  }

  public final int indexOf(long _o)
  {
    return indexOf(_o,0);
  }

  public final synchronized int indexOf(long _o, int _index)
  {
    for(int i=_index; i<count_; i++)
      if(_o==data_[i]) // equals
	return i;
      
    return -1;
  }

  public final int lastIndexOf(long _o)
  {
    return lastIndexOf(_o,count_-1);
  }

  public final synchronized int lastIndexOf(long _o, int _index)
  {
    for(int i=_index; i>=0; i--)
      if(_o==data_[i]) // equals
	return i;

    return -1;
  }

  public final synchronized long elementAt(int _index)
  {
    return data_[_index];
  }

  public final synchronized long firstElement()
  {
    if(count_==0) throw new NoSuchElementException();
    return data_[0];
  }

  public final synchronized long lastElement()
  {
    if(count_==0) throw new NoSuchElementException();
    return data_[count_-1];
  }

  public final synchronized void setElementAt(long _o,int _index)
  {
    if(_index>=count_) throw new ArrayIndexOutOfBoundsException();
    data_[_index]=_o;
  }

  public final synchronized void removeElementAt(int _index)
  {
    int j=count_-_index-1;
    if(j>0)
      System.arraycopy(data_,_index+1,data_,_index,j);

    count_--;
    data_[count_]=0;
  }

  public final synchronized void insertElementAt(long _o, int _index)
  {
    if(_index==count_)
      addElement(_o);
    else
    {
      if(count_>=data_.length)
	resize0(count_+1);
      count_++;
      System.arraycopy(data_,_index,data_,
		       _index+1,count_-1-_index);
      data_[_index]=_o;
    }
  }

  public final synchronized void addElement(long _o)
  {
    if(count_>=data_.length)
      resize0(count_+1);
    data_[count_]=_o;
    count_++;
  }

  public final synchronized boolean removeElement(long _o)
  {
    int i=indexOf(_o);
    if(i>=0)
    {
      removeElementAt(i);
      return true;
    }
    return false;
  }

  public final synchronized void removeAllElements()
  {
    for(int i=0; i<count_; i++)
      data_[i]=0;
    count_=0;
  }

  @Override
  public final synchronized Object clone()
  {
    FuVectorlong r=null;

    try
    { 
      r=(FuVectorlong)super.clone();
      r.data_=new long[count_];
      System.arraycopy(data_,0,r.data_,0,count_);
    }
    catch(CloneNotSupportedException ex)
    { 
      throw new InternalError();
    }

    return r;
  }

  @Override
  public final String toString()
  {
    return "FuVectorlong("+count_+")";
  }

  public static final class Enumerator
  {
    private FuVectorlong vector;
    private int         count;

    Enumerator(FuVectorlong _v)
    {
      vector=_v;
      count =0;
    }

    public final boolean hasMoreElements()
    {
      return count<vector.count_;
    }

    public final long nextElement()
    {
      if(count<vector.count_)
	return vector.data_[count++];
      throw new NoSuchElementException("FuVectorlong.Enumerator");
    }
  }
}
