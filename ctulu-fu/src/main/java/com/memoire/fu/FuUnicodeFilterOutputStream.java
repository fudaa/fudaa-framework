/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuUnicodeFilterOutputStream.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

import java.io.IOException;
import java.io.OutputStream;

/**
 * An UnicodeFilterOutputStream converts any non-ASCII character
 * into a \\u#### sequence where # is an hexa number.
 * This class is not thread-safe.
 */
public class FuUnicodeFilterOutputStream
  extends OutputStream
{
  private static final byte[] HEXA=
  { (byte)'0',(byte)'1',(byte)'2',(byte)'3',
    (byte)'4',(byte)'5',(byte)'6',(byte)'7',
    (byte)'8',(byte)'9',(byte)'a',(byte)'b',
    (byte)'c',(byte)'d',(byte)'e',(byte)'f' };

  private OutputStream  output_;
  private byte[]        buf_;

  public FuUnicodeFilterOutputStream(OutputStream _output)
  {
    output_=_output;
    buf_   =new byte[6];
    buf_[0]=(byte)'\\';
    buf_[1]=(byte)'u';
  }

  @Override
  public void write(int _b) throws IOException
  {
    if((_b>=0)&&(_b<=127))
    {
      output_.write(_b);
    }
    else
    {
      buf_[2]=HEXA[(_b>>12)&0x000F];
      buf_[3]=HEXA[(_b>> 8)&0x000F];
      buf_[4]=HEXA[(_b>> 4)&0x000F];
      buf_[5]=HEXA[(_b    )&0x000F];
      output_.write(buf_,0,6);
    }
  }

  /*
  public static void main(String[] _args)
    throws IOException
  {
    OutputStream out=new FuUnicodeFilterOutputStream(System.out);
    int c;
    while((c=System.in.read())!=-1)
    {
      out.write(c);
    }
  }
  */
}

