/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuFactoryByteArray.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2004 Guillaume Desnoix
 */

package com.memoire.fu;

import java.lang.ref.SoftReference;
import java.util.Arrays;

public final class FuFactoryByteArray
  implements FuEmptyArrays
{
  private static final int      MAX    =32;
  private static final Object   SOFT   =createReference(BYTE0);
  private static final Object[] LIST;
  //private static final Runtime  RUNTIME=Runtime.getRuntime();
  private static final int      ZLN    =8192;
  private static final byte[]   ZER    =new byte[ZLN];
  private static final int      BORDER =2048;
  
  static
  {
    LIST=new Object[MAX];
    if(FuLib.jdk()>=1.2)
      Arrays.fill(LIST,SOFT);
  }

  private static final Object createReference(Object _o)
  {
    Object r;
    if(FuLib.jdk()>=1.2)
      r=new SoftReference(_o);
    else
      r=_o;
    return r;
  }

  private static final void zero(byte[] _a)
  {
    int i=_a.length;
    while(i>=ZLN)
    {
      i-=ZLN;
      System.arraycopy(ZER,0,_a,i,ZLN);
    }
    if(i>0)
      System.arraycopy(ZER,0,_a,0,i);
  }

  private static final byte[] list(int _i)
  {
    Object s=LIST[_i];
    byte[] r;

    if(s!=SOFT)
    {
      if(FuLib.jdk()>=1.2)
        r=(byte[])((SoftReference)s).get();
      else
        r=(byte[])s;
    }
    else r=BYTE0;

    if(r==null) { LIST[_i]=SOFT; r=BYTE0; }
    return r;
  }

  private static final byte[] search(int _minsize,int _maxsize)
  {
    int    p=-1;
    int    m=Integer.MAX_VALUE;
    byte[] r=null;

    for(int i=0;i<MAX;i++)
    {
      byte[] b=list(i);
      if(b!=BYTE0)
      {
        int    l=b.length;
        if((l>=_minsize)&&(l<=_maxsize))
        {
          if((p==-1)||(l<m))
          {
            r=b;
            p=i;
            m=l;
          }
        }
      }
    }

    if(p>=0) LIST[p]=SOFT;
    return r;
  }

  public static final byte[] get(final int _size,final boolean _zeros)
  {
    return _size<BORDER ? new byte[_size] : get0(_size,_size,_zeros);
  }

  public static final byte[] get(final int _minsize,final int _maxsize,
                                 final boolean _zeros)
  {
    //return _minsize==0 ? FuEmptyArrays.BYTE0 : get0(_minsize,_maxsize,_zeros);
    return _minsize<BORDER ? new byte[_minsize] : get0(_minsize,_maxsize,_zeros);
    //return get0(_minsize,_maxsize,_zeros);
    //return new byte[_minsize];
  }

  private static final byte[] get0(final int _minsize,int _maxsize,
                                   final boolean _zeros)
  {
    int maxsize=_maxsize;
    if(maxsize<_minsize)
      maxsize=(_minsize<<2)|(ZLN-1);
    //Math.max(_minsize,
    //(int)Math.min(RUNTIME.freeMemory(),(_minsize<<2)|(ZLN-1)));


    byte[] r=search(_minsize,maxsize);

    if(r==null)
    {
      int l=_minsize;
      //int l=_maxsize;//Math.min(//min;
      //if(_maxsize>_minsize) l=Math.min(max,((max+min)>>1)|(ZLN-1));
      r=new byte[l];
    }
    else if(_zeros) zero(r);

    return r;
  }

  private static int cpt_=0;

  public static final void release(byte[] _array)
  {
    if(_array==null) return;

    final int l=_array.length;
    if(l<BORDER) return;

    for(int i=0;i<MAX;i++)
      if(list(i)==BYTE0)
      {
        LIST[i]=createReference(_array);
        return;
      }

    int n=cpt_+1;
    if(n>=MAX) n=0;
    LIST[n]=createReference(_array);
    cpt_=n;
  }

  // Test
  /*
  private static final void test(int i)
  {
    long before,after;
    long n1,n2,n3,t1,t2,t3;
    byte[] b=null;
    boolean z;
    //int c=0;

    t1=t2=t3=0L;
    long start=System.currentTimeMillis();
    while(System.currentTimeMillis()<start+10000)
    {
      //int l=i/10;
      //int l=i%10000;
      int l=(4096+(int)(i*Math.random()*Math.random()))/2;//*Math.random()))/2;
      //int l=100;
      if((l>1024)&&(Math.random()<0.6)) l|=0x3FF;

      before=System.nanoTime();
      for(int j=0;j<10;j++)
      {
        b=new byte[l];
      }
      after =System.nanoTime();
      n1=(after-before);
      t1+=n1;
      if(b.length>0) b[0]++;

      z=Math.random()<0.9;
      before=System.nanoTime();
      for(int j=0;j<10;j++)
      {
        b=get(l,z);
        release(b);
      }
      after =System.nanoTime();
      n2=(after-before);
      t2+=n2;


      z=Math.random()<0.1;
      before=System.nanoTime();
      for(int j=0;j<10;j++)
      {
        b=get(l,-1,z);
        release(b);
      }
      after =System.nanoTime();
      n3=(after-before);
      t3+=n3;

      //if(n2<n1) c++; else c=0;
      //if(c>2) { System.out.println(i); c=0; }
    }
    System.out.println(FuLib.leftpad(""+i        ,10,' ')+
                       FuLib.leftpad(""+(t1/1000),10,' ')+
                       FuLib.leftpad(""+(t2/1000),10,' ')+
                       FuLib.leftpad(""+(t3/1000),10,' ')+
                       FuLib.leftpad((100*(t1-t2)/t1)+"%",10,' ')+
                       FuLib.leftpad((100*(t1-t3)/t1)+"%",10,' '));
  }

  public static void main(String[] _args)
  {
    System.out.println("Size      New       Factory   Range     Gain21     Gain31     ");
    for(int i=128;i<10000000;i=i*180/100+1)
      test(i);
  }
  */
}
