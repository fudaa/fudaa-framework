/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuEmptyInputStream.java
 * @version      0.32
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    2003-2004 Guillaume Desnoix
 */

package com.memoire.fu;

import java.io.IOException;
import java.io.InputStream;

public class FuEmptyInputStream
  extends InputStream
{
  private boolean close_;

  public FuEmptyInputStream()
  {
  }

  @Override
  public void close() throws IOException
  {
    close_=true;
    super.close();
  }

  @Override
  public int available() throws IOException
  {
    if(close_) throw new IOException("empty stream is close");
    return 1;
  }

  @Override
  public int read() throws IOException
  {
    if(close_) throw new IOException("empty stream is close");
    close_=true;
    return -1;
  }
}
