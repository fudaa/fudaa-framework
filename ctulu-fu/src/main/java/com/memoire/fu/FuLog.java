/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut unstable
 * @file FuLog.java
 * @version 0.43
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 * @copyright 1998-2005 Guillaume Desnoix
 */
package com.memoire.fu;

// import java.lang.ref.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class FuLog {

  private static final int SW = 80; // screen width
  // private and final are temporary
  /*
   private static final int NOTHING=0;
   private static final int DEBUG  =1;
   private static final int TRACE  =2;
   private static final int WARNING=3;
   private static final int ERROR  =4;
   private static final int FAILURE=5;
   */
  public final static int ALL = 0;
  public final static int DEBUG = 1;
  public final static int TRACE = 2;
  public final static int WARNING = 3;
  public final static int ERROR = 4;
  public final static int FAILURE = 5;
  public final static int NOTHING = 6;
  static final String[] LEVELS = {"     ", "DEBUG", "TRACE", "WARNG", "ERROR", "FAILR"};
  private static final int level_ = getLogLevel();
  static final boolean date_ = getLogOption("date", false);
  static final boolean time_ = getLogOption("time", false);
  static final boolean location_ = getLogOption("location", false);
  static final boolean cut_ = false/**
           * getLogOption("cut" ,false)
           */
          ;
  private static boolean useJavaUtilLogging = false;

  public static boolean isUseJavaUtilLogging() {
    return useJavaUtilLogging;
  }

  public static void setUseJavaUtilLogging(boolean useJavaUtilLogging) {
    FuLog.useJavaUtilLogging = useJavaUtilLogging;
  }
  public static PrintStream LOGSTREAM;
  private static final Thread THREAD;
  static final Vector QUEUE; //LinkedList

  public static final boolean isTrace() {
    return level_ <= TRACE;
  }

  public static final String getStackTrace() {
    String r = "<<no trace>>";
    try {
      throw new RuntimeException("getStackTrace");
    } catch (RuntimeException ex) {
      r = getStackTrace(ex);
    }
    return r;
  }

  public static final boolean isDebug() {
    return level_ <= DEBUG;
  }

  public static final boolean isWarning() {
    return level_ <= WARNING;
  }

  static final String getStackTrace(Throwable _th) {
    String r = "<<no trace>>";
    try {
      StringWriter out = new StringWriter();
      _th.printStackTrace(new PrintWriter(out));
      r = out.getBuffer().toString().trim();
    } catch (Throwable th) {
    }
    return r;
  }

  public static final void printStackTrace() {
    String r = getStackTrace();
    int p = r.indexOf("com.memoire.fu.FuLog.printStackTrace");
    if (p >= 0) {
      p = r.indexOf('\n', p);
    }
    if (p >= 0) {
      r = r.substring(p + 1);
    }
    r = FuLib.replace(r, "        at ", "  ");
    r = FuLib.replace(r, "\tat ", "  ");
    System.err.println("STK: ==== STACK TRACE ====");
    System.err.println(r);
  }

  public static final void debug(String _msg) {
    log(DEBUG, _msg, null);
  }

  public static final void debug(String _msg, Throwable _th) {
    log(DEBUG, _msg, _th);
  }

  public static final void trace(String _msg) {
    log(TRACE, _msg, null);
  }

  public static final void all(String _msg) {
    log(ALL, _msg, null);
  }

  /*
   public static final void trace(String _msg,Throwable _th)
   {
   log(TRACE,_msg,_th);
   }
   */
  public static final void warning(String _msg) {
    log(WARNING, _msg, null);
  }

  public static final void warning(Throwable _th) {
    log(WARNING, null, _th);
  }

  public static final void warning(String _msg, Throwable _th) {
    log(WARNING, _msg, _th);
  }

  public static final void error(String _msg) {
    log(ERROR, _msg, null);
  }

  public static final void error(Throwable _th) {
    log(ERROR, null, _th);
  }

  public static final void error(String _msg, Throwable _th) {
    log(ERROR, _msg, _th);
  }

  /*
   public static final void failure(String _msg)
   {
   log(FAILURE,_msg,null);
   }
   */
  public static final void failure(String _msg, Throwable _th) {
    log(FAILURE, _msg, _th);
  }

  private static final PrintStream createLogStream() {
    PrintStream r = null;
    try {
      String log = System.getProperty("fulog.file");
      if (log == null) {
        log = System.getProperty("user.home") + File.separator + ".jdistro" + File.separator + "log";
      }
      r = new PrintStream(new FileOutputStream(log), false);
    } catch (IOException ex) {
    }
    return r;
  }

  private static final int getLogLevel() {
    int r = DEBUG;
    String level = System.getProperty("fulog.level");
    if ("all".equals(level)) {
      r = ALL;
    }
    if ("debug".equals(level)) {
      r = DEBUG;
    }
    if ("trace".equals(level)) {
      r = TRACE;
    }
    if ("warning".equals(level)) {
      r = WARNING;
    }
    if ("error".equals(level)) {
      r = ERROR;
    }
    if ("failure".equals(level)) {
      r = FAILURE;
    }
    if ("nothing".equals(level)) {
      r = NOTHING;
    }
    return r;
  }

  private static final boolean getLogOption(String _key, boolean _default) {
    boolean r = _default;
    String o = System.getProperty("fulog." + _key);
    if ("true".equals(o) || "yes".equals(o)) {
      r = true;
    }
    if ("false".equals(o) || "no".equals(o)) {
      r = false;
    }
    return r;
  }

  static {
    if (level_ >= NOTHING) {
      LOGSTREAM = null;
      QUEUE = null;
      THREAD = null;
    } else {
      LOGSTREAM = createLogStream();

      QUEUE = new Vector();//LinkedList();

      THREAD = new Thread(new Runnable() {
        @Override
        public void run() {
          for (;;) {
            Record r = null;

            synchronized (QUEUE) {
              while (QUEUE.isEmpty()) {
                try {
                  QUEUE.wait();
                } catch (InterruptedException ex) {
                }
              }

              //r=(Record)QUEUE.removeFirst();
              r = (Record) QUEUE.elementAt(0);
              QUEUE.removeElementAt(0);
            }

            boolean header = (r.msg_ != null) && (r.msg_.length() >= 4) && (r.msg_.charAt(3) == ':');

            StringBuilder  sb = new StringBuilder(256);

            sb.append(header ? r.msg_.substring(0, 4) : "???:");
            sb.append(' ');
            sb.append(LEVELS[r.recLevel_]);
            if (date_) {
              String s = FuLib.date(r.recTime_, DateFormat.SHORT);
              int l = s.length();
              while (l < 9) {
                sb.append(' ');
                l++;
              }
              sb.append(s);
            }
            if (time_) {
              String s = FuLib.time(r.recTime_, DateFormat.MEDIUM);
              int l = s.length();
              while (l < 9) {
                sb.append(' ');
                l++;
              }
              sb.append(s);
            }

            if ((r.msg_ != null) && (r.msg_.length() > 0) && (r.msg_.charAt(0) != ' ')) {
              sb.append(' ');
            }
            String msg = (header ? r.msg_.substring(4) : r.msg_);
            if (cut_) {

              msg = msg.replace('\n', ' ');
              msg = msg.replace('\r', ' ');
              msg = msg.replace('\t', ' ');
            }
            sb.append(msg);

            if (r.th_ != null) {
              msg = r.th_.getMessage();
              if ((msg != null) && !"".equals(msg)) {
                sb.append(" -- ");
                sb.append(msg);
              }
            }

            if (cut_) {
              for (int i = SW - 2; i < sb.length(); i += SW - 2) {
                sb.insert(i, "\n           ");
              }
            }

            String st = null;

            if (r.th_ != null) {
              if (location_) {
                st = getStackTrace(r.th_);

                String loc = st;
                int i = loc.lastIndexOf("FuLog");
                if (i >= 0) {
                  loc = st.substring(i);
                  i = loc.indexOf('\n');
                  if (i >= 0) {
                    loc = loc.substring(i + 1);
                  }
                }
                i = loc.indexOf('(');
                if (i >= 0) {
                  loc = loc.substring(i + 1);
                }
                i = loc.indexOf(')');
                if (i >= 0) {
                  loc = loc.substring(0, i);
                } else {
                  loc = "unknown";
                }

                if (cut_) {
                  int l = sb.length();
                  int p = SW - 2 - loc.length();
                  if (l % (SW - 1) > p) {
                    while (l % (SW - 1) != 0) {
                      sb.append(' ');
                      l++;
                    }
                    sb.append('\n');
                  }
                  while (l % (SW - 1) < p) {
                    sb.append(' ');
                    l++;
                  }
                } else {
                  sb.append(' ');
                }

                sb.append(loc);
              }
            }

            System.out.println(sb.toString());
            if (LOGSTREAM != null) {
              LOGSTREAM.println(sb.toString());
            }

            if ((r.th_ != null) && (r.recLevel_ >= ERROR)) {
              if (st == null) {
                st = getStackTrace(r.th_);
              }

              String[] s = FuLib.split(st, '\n', false, true);
              for (int j = 0; j < s.length; j++) {
                sb.setLength(0);
                sb.append("     ");
                sb.append(s[j]);
                if (cut_) {
                  for (int i = SW - 2; i < sb.length(); i += SW - 2) {
                    sb.insert(i, "\n         ");
                  }
                }
                System.err.println(sb.toString());
                if (LOGSTREAM != null) {
                  LOGSTREAM.println(sb.toString());
                }
              }
            }

            System.err.flush();
            if (LOGSTREAM != null) {
              LOGSTREAM.flush();
            }

            if (r.recLevel_ >= FAILURE) {
              System.exit(-127);
            }
          }
        }
      }, "Fu Logger");

      THREAD.setDaemon(true);
      THREAD.setPriority(Thread.MAX_PRIORITY);
      THREAD.start();
    }
  }

  private static final class Record {

    int recLevel_;
    String msg_;
    Throwable th_;
    long recTime_;

    public Record(int _level, String _msg, Throwable _th) {
      recLevel_ = _level;
      msg_ = _msg;
      th_ = _th;
      recTime_ = System.currentTimeMillis();
    }
  }
  private static final Logger LOGGER = Logger.getLogger(FuLog.class.getName());

  private static Level getLevel(int _level) {
    if (_level == DEBUG) {
      return Level.FINE;
    }
    if (_level == FAILURE) {
      return Level.SEVERE;
    }
    if (_level == ALL) {
      return Level.ALL;
    }
    if (_level == NOTHING) {
      return Level.OFF;
    }
    if (_level == TRACE) {
      return Level.OFF;
    }
    return Level.WARNING;

  }

  private static final void logWithUtilLogging(int _level, String _msg, Throwable _th) {
    Level level = getLevel(_level);
    if (LOGGER.isLoggable(level)) {
      LOGGER.log(level, _msg, _th);
    }

  }

  private static final void log(int _level, String _msg, Throwable _th) {
    if (useJavaUtilLogging) {
      logWithUtilLogging(_level, _msg, _th);
      return;
    }
    String msg = _msg;
    Throwable th = _th;
    if (_level >= level_ || _level == ALL) {
      if (th == null) {
        if (location_ || (_level >= ERROR)) {
          try {
            throw new RuntimeException("");
          } catch (RuntimeException _ex) {
            th = _ex;
          }
        }
      }

      if ((msg == null) && (th != null)) {
        msg = th.getClass().getName();
      }

      if (th instanceof InvocationTargetException) {
        th = ((InvocationTargetException) th).getTargetException();
      }
      if (QUEUE != null) {
        synchronized (QUEUE) {
          QUEUE.addElement(new Record(_level, msg, th)); //add
          QUEUE.notifyAll();
        }
      }
    }
  }
}
