/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuVectorString.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

import java.io.Serializable;
import java.util.NoSuchElementException;

/**
 * A vector for String values.
 * This a vector for primitive types.
 * Public fields needed for Yapod serialization.
 */
public class FuVectorString
       implements Cloneable, Serializable
{
  public String[] data_;
  public int      count_;
  public int      increment_;

  public FuVectorString(int _capacity,int _increment)
  {
    super();
    data_     =new String[_capacity];
    count_    =0;
    increment_=_increment;
  }

  public FuVectorString(int _capacity)
  {
    this(_capacity,0);
  }

  public FuVectorString()
  {
    this(11,0);
  }

  public FuVectorString(String[] _data)
  {
    super();
    data_     =_data;
    count_    =_data.length;
    increment_=1;
  }

  public final synchronized void sort()
  {
    FuSort.sort(data_,0,count_,FuComparator.STRING_COMPARATOR);
  }

  public final synchronized void sort(FuComparator _c)
  {
    FuSort.sort(data_,0,count_,_c);
  }

  public final synchronized void sortIgnoreCase()
  {
    FuSort.sort(data_,0,count_,FuComparator.STRING_IGNORE_CASE_COMPARATOR);
  }

  public final synchronized void uniq()
  {
    if(count_==0) return;

    int j=0;
    while((j<count_)&&(data_[j]==null)) j++;
    if(j==count_) { count_=0; return; }

    if(j>0) { data_[0]=data_[j]; data_[j]=null; }

    int i=0;
    for(j=1; j<count_; j++)
    {
      if(data_[j]==null) continue;
      if(!data_[i].equals(data_[j]))
      {
	i++;
	if(i!=j) { data_[i]=data_[j]; data_[j]=null; }
      }
    }
    count_=i+1;
  }

  public final synchronized void uniqIgnoreCase()
  {
    if(count_==0) return;

    int j=0;
    while((j<count_)&&(data_[j]==null)) j++;
    if(j==count_) { count_=0; return; }

    if(j>0) { data_[0]=data_[j]; data_[j]=null; }

    int i=0;
    for(j=1; j<count_; j++)
    {
      if(data_[j]==null) continue;
      if(!data_[i].equalsIgnoreCase(data_[j]))
      {
	i++;
	if(i!=j) { data_[i]=data_[j]; data_[j]=null; }
      }
    }
    count_=i+1;
  }

  public final synchronized void copyInto(String[] _array)
  {
    for(int i=0;i<count_;i++)
      _array[i]=data_[i];
  }

  public final synchronized String[] toArray()
  {
    String[] r=new String[count_];

    for(int i=0;i<count_;i++)
      r[i]=data_[i];

    return r;
  }

  public final synchronized void trimToSize()
  {
    int old_capacity=data_.length;
    if(count_<old_capacity)
    {
      String[] old_data=data_;
      data_=new String[count_];
      System.arraycopy(old_data,0,data_,0,count_);
    }
  }

  public final synchronized void ensureCapacity(int _min)
  {
    if(_min>data_.length)
      resize0(_min);
  }

  private void resize0(int _min)
  {
    int   old_capacity=data_.length;
    String[] old_data    =data_;

    int new_capacity=
      (  (increment_>0)
       ? (old_capacity+increment_)
       : (old_capacity*2));

    if(new_capacity<_min)
      new_capacity=_min;

    data_=new String[new_capacity];
    System.arraycopy(old_data,0,data_,0,count_);
  }
    
  public final synchronized void setSize(int _new)
  {
    if((_new>count_)&&(_new>data_.length))
      resize0(_new);
    else
      for(int i=_new;i<count_;i++)
	data_[i]=null;

    count_=_new;
  }

  public final int capacity()
  {
    return data_.length;
  }

  public final int size()
  {
    return count_;
  }

  public final boolean isEmpty()
  {
    return count_==0;
  }

  public final synchronized Enumerator elements()
  {
    return new Enumerator(this);
  }
    
  public final boolean contains(String _o)
  {
    return (indexOf(_o,0)>=0);
  }

  public final int indexOf(String _o)
  {
    return indexOf(_o,0);
  }

  public final synchronized int indexOf(String _o, int _index)
  {
    for(int i=_index; i<count_; i++)
      if(_o.equals(data_[i]))
	return i;
      
    return -1;
  }

  public final int lastIndexOf(String _o)
  {
    return lastIndexOf(_o,count_-1);
  }

  public final synchronized int lastIndexOf(String _o, int _index)
  {
    for(int i=_index; i>=0; i--)
      if(_o.equals(data_[i]))
	return i;

    return -1;
  }

  public final synchronized String elementAt(int _index)
  {
    return data_[_index];
  }

  public final synchronized String firstElement()
  {
    if(count_==0) throw new NoSuchElementException();
    return data_[0];
  }

  public final synchronized String lastElement()
  {
    if(count_==0) throw new NoSuchElementException();
    return data_[count_-1];
  }

  public final synchronized void setElementAt(String _o,int _index)
  {
    if(_index>=count_) throw new ArrayIndexOutOfBoundsException();
    data_[_index]=_o;
  }

  public final synchronized void removeElementAt(int _index)
  {
    int j=count_-_index-1;
    if(j>0)
      System.arraycopy(data_,_index+1,data_,_index,j);

    count_--;
    data_[count_]=null;
  }

  public final synchronized void insertElementAt(String _o, int _index)
  {
    if(_index==count_)
      addElement(_o);
    else
    {
      if(count_>=data_.length)
	resize0(count_+1);
      count_++;
      System.arraycopy(data_,_index,data_,
		       _index+1,count_-1-_index);
      data_[_index]=_o;
    }
  }

  public final synchronized void addElement(String _o)
  {
    if(count_>=data_.length)
      resize0(count_+1);
    data_[count_]=_o;
    count_++;
  }

  public final synchronized void addElements(String[] _o)
  {
    int l=_o.length;
    if(count_+l>data_.length)
      resize0(count_+l);
    for(int i=0;i<l;i++)
      data_[count_+i]=_o[i];
    count_+=l;
  }

  public final synchronized boolean removeElement(String _o)
  {
    int i=indexOf(_o);
    if(i>=0)
    {
      removeElementAt(i);
      return true;
    }
    return false;
  }

  public final synchronized void removeAllElements()
  {
    for(int i=0; i<count_; i++)
      data_[i]=null;
    count_=0;
  }

  @Override
  public final synchronized Object clone()
  {
    FuVectorString r=null;

    try
    { 
      r=(FuVectorString)super.clone();
      r.data_=new String[count_];
      System.arraycopy(data_,0,r.data_,0,count_);
    }
    catch(CloneNotSupportedException ex)
    { 
      throw new InternalError();
    }

    return r;
  }

  @Override
  public final String toString()
  {
    return "FuVectorString("+count_+")";
  }

  public static final class Enumerator
  {
    private FuVectorString vector;
    private int            count;

    Enumerator(FuVectorString _v)
    {
      vector=_v;
      count =0;
    }

    public final boolean hasMoreElements()
    {
      return count<vector.count_;
    }

    public final String nextElement()
    {
      if(count<vector.count_)
	return vector.data_[count++];
      throw new NoSuchElementException("FuVectorString.Enumerator");
    }
  }

  /*
  public static void main(String[] _args)
  {
    String s=
      "Acto,Foo,Jsh,-,Abcl,Beanshell,Dawn,Fiji,Pnuts,Python,Rhino,Ruby,Silk";
    s=s+","+s;
    FuVectorString v=new FuVectorString(FuLib.split(s,',',false,true));
    v.sort();
    v.uniq();
    v.removeElement("-");
    String[] a=v.toArray();
    for(int i=0;i<a.length;i++)
      System.out.println(a[i]);
  }
  */
}
