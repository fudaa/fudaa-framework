/**
 * @modification $Date: 2006-09-19 14:34:58 $
 * @statut       unstable
 * @file         FuVectordouble.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.fu;

import java.io.Serializable;
import java.util.NoSuchElementException;

/**
 * A vector for double values.
 * This a vector for primitive types.
 * This source code for this class is generated from FuVector.jgen.
 * Public fields needed for Yapod serialization.
 */
public class FuVectordouble
       implements Cloneable, Serializable
{
  public double data_[];
  public int count_;
  public int increment_;

  public FuVectordouble(int _capacity,int _increment)
  {
    super();
    data_     =new double[_capacity];
    count_    =0;
    increment_=_increment;
  }

  public FuVectordouble(int _capacity)
  {
    this(_capacity,0);
  }

  public FuVectordouble()
  {
    this(11,0);
  }

  public FuVectordouble(double[] _data)
  {
    super();
    data_     =_data;
    count_    =_data.length;
    increment_=1;
  }

  public final synchronized void copyInto(double[] _array)
  {
    for(int i=0;i<count_;i++)
      _array[i]=data_[i];
  }

  public final synchronized double[] toArray()
  {
    double[] r=new double[count_];

    for(int i=0;i<count_;i++)
      r[i]=data_[i];

    return r;
  }

  public final synchronized void trimToSize()
  {
    int old_capacity=data_.length;
    if(count_<old_capacity)
    {
      double[] old_data=data_;
      data_=new double[count_];
      System.arraycopy(old_data,0,data_,0,count_);
    }
  }

  public final synchronized void ensureCapacity(int _min)
  {
    if(_min>data_.length)
      resize0(_min);
  }

  private void resize0(int _min)
  {
    int   old_capacity=data_.length;
    double[] old_data    =data_;

    int new_capacity=
      (  (increment_>0)
       ? (old_capacity+increment_)
       : (old_capacity*2));

    if(new_capacity<_min)
      new_capacity=_min;

    data_=new double[new_capacity];
    System.arraycopy(old_data,0,data_,0,count_);
  }
    
  public final synchronized void setSize(int _new)
  {
    if((_new>count_)&&(_new>data_.length))
      resize0(_new);
    else
      for(int i=_new;i<count_;i++)
	data_[i]=0;

    count_=_new;
  }

  public final int capacity()
  {
    return data_.length;
  }

  public final int size()
  {
    return count_;
  }

  public final boolean isEmpty()
  {
    return count_==0;
  }

  public final synchronized Enumerator elements()
  {
    return new Enumerator(this);
  }
    
  public final boolean contains(double _o)
  {
    return (indexOf(_o,0)>=0);
  }

  public final int indexOf(double _o)
  {
    return indexOf(_o,0);
  }

  public final synchronized int indexOf(double _o, int _index)
  {
    for(int i=_index; i<count_; i++)
      if(_o==data_[i]) // equals
	return i;
      
    return -1;
  }

  public final int lastIndexOf(double _o)
  {
    return lastIndexOf(_o,count_-1);
  }

  public final synchronized int lastIndexOf(double _o, int _index)
  {
    for(int i=_index; i>=0; i--)
      if(_o==data_[i]) // equals
	return i;

    return -1;
  }

  public final synchronized double elementAt(int _index)
  {
    return data_[_index];
  }

  public final synchronized double firstElement()
  {
    if(count_==0) throw new NoSuchElementException();
    return data_[0];
  }

  public final synchronized double lastElement()
  {
    if(count_==0) throw new NoSuchElementException();
    return data_[count_-1];
  }

  public final synchronized void setElementAt(double _o,int _index)
  {
    if(_index>=count_) throw new ArrayIndexOutOfBoundsException();
    data_[_index]=_o;
  }

  public final synchronized void removeElementAt(int _index)
  {
    int j=count_-_index-1;
    if(j>0)
      System.arraycopy(data_,_index+1,data_,_index,j);

    count_--;
    data_[count_]=0;
  }

  public final synchronized void insertElementAt(double _o, int _index)
  {
    if(_index==count_)
      addElement(_o);
    else
    {
      if(count_>=data_.length)
	resize0(count_+1);
      count_++;
      System.arraycopy(data_,_index,data_,
		       _index+1,count_-1-_index);
      data_[_index]=_o;
    }
  }

  public final synchronized void addElement(double _o)
  {
    if(count_>=data_.length)
      resize0(count_+1);
    data_[count_]=_o;
    count_++;
  }

  public final synchronized boolean removeElement(double _o)
  {
    int i=indexOf(_o);
    if(i>=0)
    {
      removeElementAt(i);
      return true;
    }
    return false;
  }

  public final synchronized void removeAllElements()
  {
    for(int i=0; i<count_; i++)
      data_[i]=0;
    count_=0;
  }

  @Override
  public final synchronized Object clone()
  {
    FuVectordouble r=null;

    try
    { 
      r=(FuVectordouble)super.clone();
      r.data_=new double[count_];
      System.arraycopy(data_,0,r.data_,0,count_);
    }
    catch(CloneNotSupportedException ex)
    { 
      throw new InternalError();
    }

    return r;
  }

  @Override
  public final String toString()
  {
    return "FuVectordouble("+count_+")";
  }

  public static final class Enumerator
  {
    private FuVectordouble vector;
    private int         count;

    Enumerator(FuVectordouble _v)
    {
      vector=_v;
      count =0;
    }

    public final boolean hasMoreElements()
    {
      return count<vector.count_;
    }

    public final double nextElement()
    {
      if(count<vector.count_)
	return vector.data_[count++];
      throw new NoSuchElementException("FuVectordouble.Enumerator");
    }
  }
}
