/**
 * @modification $Date: 2005-08-16 12:58:08 $
 * @statut       unstable
 * @file         FuFactoryInteger.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2004 Guillaume Desnoix
 */

package com.memoire.fu;

public final class FuFactoryInteger
{
  private static final int       SIZE =0x100;
  private static final Integer[] SMALL=new Integer[SIZE];
  private static final Integer[] TABLE=new Integer[SIZE];

  static
  {
    for(int i=0;i<SIZE;i++)
      SMALL[i]=new Integer(i);
    for(int i=0;i<SIZE;i++)
      TABLE[i]=new Integer(SIZE+i);
  }

  public static final Integer get(int _i)
  {
    Integer r;

    if((_i>=0)&&(_i<SIZE))
      r=SMALL[_i];
    else
    if(_i<0)
      r=new Integer(_i);
    else
    {
      int n=_i&(SIZE-1);
      r=TABLE[n];
      if(r.intValue()!=_i)
      {
        r=new Integer(_i);
        TABLE[n]=r;
      }
    }

    return r;
  }
}
