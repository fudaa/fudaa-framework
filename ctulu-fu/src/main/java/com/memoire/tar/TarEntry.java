
package com.memoire.tar;

import java.io.IOException;

public class TarEntry
{
  private long   pos_;
  private String name_;
  private String mode_;
  private int    uid_;
  private int    gid_;
  private long   size_;
  private long   time_;
  //private String check_;
  private byte   type_;
  private String link_;
  private String user_;
  private String group_;

  TarEntry(byte[] _data,long _pos,String _longname)
    throws IOException
  {
    if(_data==null)
      throw new IllegalArgumentException("data is null");
    if(_data.length<512)
      throw new IllegalArgumentException("data is too short");
    if(!convertString(_data,257,8).startsWith("ustar"))
    {
      //System.out.println("MAGIC="+convertString(_data,257,8));
      throw new IOException("not a tar format");
    }

    pos_   =_pos;
    name_  =(_longname!=null) ? _longname : convertString(_data,0,100);
    mode_  =removeZeros (convertString(_data,100,8));
    uid_   =convertOctal(convertString(_data,108,8));
    gid_   =convertOctal(convertString(_data,116,8));
    size_  =convertOctal(convertString(_data,124,12));
    time_  =1000L*convertOctal(convertString(_data,136,12));
    //check_ =convertString(_data,148,8);
    type_  =_data[156];
    link_  =convertString(_data,157,100);
    user_  =convertString(_data,264,32);
    group_ =convertString(_data,296,32);
    /*
    major_ =convertString(_data,328,8);
    minor_ =convertString(_data,336,8);
    */
  }

  public long getPosition()
  {
    return pos_;
  }

  public String getName()
  {
    return name_;
  }

  public String getMode()
  {
    return mode_;
  }

  public int getUID()
  {
    return uid_;
  }

  public int getGID()
  {
    return gid_;
  }

  public long getSize()
  {
    return size_;
  }

  public long getTime()
  {
    return time_;
  }

  public byte getType()
  {
    return type_;
  }

  public String getLink()
  {
    return link_;
  }

  public String getUser()
  {
    return user_;
  }

  public String getGroup()
  {
    return group_;
  }

  public boolean isFile()
  {
    return (type_==0)||(type_=='0');
  }

  public boolean isDirectory()
  {
    return (type_=='5')||name_.endsWith("/");
  }

  @Override
  public String toString()
  {
    return name_;
  }

  private static String convertString(byte[] _b,int _o,int _l)
  {
    int i;
    for(i=_o;i<_o+_l;i++)
        if(_b[i]==0) break;
    return new String(_b,_o,i-_o);
  }

  private static String removeZeros(String _s)
  {
    int i;
    //System.err.println("'"+_s+"'");
    for(i=0;i<_s.length()-1;i++)
      if(_s.charAt(i)!='0') break;
    return _s.substring(i);
  }

  private static int convertOctal(String _s)
  {
    String s=removeZeros(_s.trim());
    if("".equals(s)) return 0;
    return Integer.parseInt(s,8);
  }
}
