
package com.memoire.tar;

import com.memoire.bzip.BZip2InputStream;
import com.memoire.fu.FuFactoryByteArray;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLimitedInputStream;
import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.util.zip.GZIPInputStream;

public class TarFile
{
  public static final int OPEN_READ=0x1;

  private File      file_;
  private Vector    list_;
  private Hashtable table_;

  public TarFile(String _name)
  {
    this(new File(_name));
  }

  public TarFile(File _file)
  {
    this(_file,OPEN_READ);
  }

  public TarFile(File _file,int _mode)
  {
    if(_file==null)
      throw new IllegalArgumentException("file is null");

    file_ =_file;
    list_ =null;
    table_=null;
  }

  public TarEntry getEntry(String _name)
    throws IOException
  {
    if(_name==null)
      throw new IllegalArgumentException("name is null");

    ensureOpen();
    return (TarEntry)table_.get(_name);
  }

  public InputStream getInputStream(TarEntry _entry)
    throws IOException
  {
    if(_entry==null) return null;

    InputStream in=new BufferedInputStream(new FileInputStream(file_));
    if(FuLib.isBzip2ed(in)) in=new BZip2InputStream(in);
    if(FuLib.isGziped (in)) in=new GZIPInputStream(in);
    return new FuLimitedInputStream(in,_entry.getPosition(),_entry.getSize());
  }

  public InputStream getInputStream(String _name)
    throws IOException
  {
    return getInputStream(getEntry(_name));
  }

  public String getName()
  {
    return file_.getName();
  }

  public Enumeration entries()
    throws IOException
  {
    ensureOpen();
    return list_.elements();
  }

  protected void ensureOpen()
    throws IOException
  {
    if(list_!=null) return;

    synchronized(this)
    {
      table_=new Hashtable(11);
      list_ =new Vector(11);
      
      InputStream in=new BufferedInputStream(new FileInputStream(file_));
      if(FuLib.isBzip2ed(in)) in=new BZip2InputStream(in);
      if(FuLib.isGziped (in)) in=new GZIPInputStream(in);

      byte[] h  =FuFactoryByteArray.get(512,-1,false);
      long   pos=0L;
      String longname=null;

      for(;;)
      {
        boolean eof=false;
        try { FuLib.readFully(in,h,0,512); }
        catch(EOFException ex) { eof=true; }
        //if((n<0)||(h[0]==0))
        if(h[0]==0) eof=true;

        if(eof)
        {
          FuLib.safeClose(in);
          FuFactoryByteArray.release(h);
          return;
        }

        pos+=512L;
        TarEntry e=new TarEntry(h,pos,longname);
        long size=e.getSize();
        long n;

        if(e.getName().startsWith("././@LongLink"))
        {
          n=size;
          byte[] b=null;
          try
          {
            b=FuFactoryByteArray.get((int)n,-1,false); //b=new byte[(int)n];
            FuLib.readFully(in,b,0,(int)n);
            int i;
            for(i=0;i<(int)n;i++)
              if(b[i]==0) break;
            longname=new String(b,0,i,"ASCII");
            //longname=new String(b,"ASCII").trim();
            //System.err.println("LONGNAME='"+longname+"'");
          }
          finally
          {
            FuFactoryByteArray.release(b);
          }
        }
        else
        {
          table_.put(e.getName(),e);
          list_.addElement(e);
          longname=null;

          n=size;
          pos+=n;
          //while(n>0) n-=in.skip(n);
          FuLib.skipFully(in,n);
        }

        n=size%512;
        if(n!=0)
        {
          n=512-n;
          //System.err.println("padding "+n);
          pos+=n;
          //while(n>0) n-=in.skip(n);
          FuLib.skipFully(in,n);
        }
      }
    }
  }

  public void close()
  {
    list_ =null;
    table_=null;
  }

  // Test
  public static void main(String[] _args)
    throws IOException
  {
    TarFile f=new TarFile(_args[0]);
    Enumeration e=f.entries();
    while(e.hasMoreElements())
    {
      TarEntry t=(TarEntry)e.nextElement();
      String s=t.getName();
      if(s.length()>40)
      {
        //System.err.println("S1='"+s+"'");
        s="..."+s.substring(s.length()-37);
        //System.err.println("S2='"+s+"' "+s.length());
      }
      System.out.println
        (FuLib.rightpad(s,40,' ')+
         FuLib.leftpad ("  "+t.getSize(),15,' ')+
         FuLib.rightpad("  "+new Date(t.getTime()),21,' '));
    }

    /*
    TarEntry t=f.getEntry("myMileage/README");
    if(t!=null)
    {
      System.out.println("pos="+t.getPosition());
      InputStream in=f.getInputStream(t);
      int c;
      while((c=in.read())>0)
        System.out.print((char)c);
    }

    t=f.getEntry("myMileage/src/Mileage.java");
    if(t!=null)
    {
      System.out.println("pos="+t.getPosition());
      InputStream in=f.getInputStream(t);
      int c;
      while((c=in.read())>0)
        System.out.print((char)c);
    }
    */
  }
}
