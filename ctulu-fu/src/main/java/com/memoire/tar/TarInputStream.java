
package com.memoire.tar;

import com.memoire.bzip.BZip2InputStream;
import com.memoire.fu.FuFactoryByteArray;
import com.memoire.fu.FuLib;

import java.io.*;
import java.util.Date;
import java.util.zip.GZIPInputStream;

public class TarInputStream
    extends FilterInputStream {
  private boolean closed_;
  private String longname_;
  private TarEntry entry_;
  private long remaining_;
  private byte[] buf1 = new byte[1];

  public TarInputStream(InputStream _in)
      throws IOException {
    super(zipCheck(_in));
  }

  private static final InputStream zipCheck(InputStream _in)
      throws IOException {
    InputStream in = _in;
    if (!(in instanceof BufferedInputStream)) {
      in = new BufferedInputStream(in);
    }

    if (FuLib.isBzip2ed(in)) {
      in = new BZip2InputStream(in);
    }
    if (FuLib.isGziped(in)) {
      in = new GZIPInputStream(in);
    }

    return in;
  }

  private void ensureOpen()
      throws IOException {
    if (closed_) {
      throw new IOException("tar input stream is closed");
    }
  }

  public TarEntry getNextEntry()
      throws IOException {
    ensureOpen();
    if (entry_ != null) {
      closeEntry();
    }

    byte[] buf = null;
    TarEntry r = null;

    try {
      buf = FuFactoryByteArray.get(512, -1, false);
      boolean eof = false;
      try {
        FuLib.readFully(in, buf, 0, 512);
      } catch (EOFException ex) {
        eof = true;
      }
      if (buf[0] == 0) {
        eof = true;
      }

      if (!eof) {
        r = new TarEntry(buf, 0, longname_);
        //System.out.println("NAME="+r.getName());
        longname_ = null;
        if (r.getName().startsWith("././@LongLink")) {
          entry_ = r;
          remaining_ = entry_.getSize();
          long n = r.getSize();
          byte[] b = null;
          try {
            b = FuFactoryByteArray.get((int) n, -1, false);
            FuLib.readFully(this, b, 0, (int) n);
            int i;
            for (i = 0; i < (int) n; i++)
              if (b[i] == 0) {
                break;
              }
            longname_ = new String(b, 0, i, "ASCII");
          } finally {
            FuFactoryByteArray.release(b);
          }
          //System.out.println("LG="+longname_);
          return getNextEntry();
        }
      }
    } finally {
      FuFactoryByteArray.release(buf);
    }

    entry_ = r;
    if (entry_ != null) {
      remaining_ = entry_.getSize();
    }
    return r;
  }

  public void closeEntry()
      throws IOException {
    ensureOpen();
    if (entry_ == null) {
      throw new IOException("no entry is open");
    }

    long n = entry_.getSize() % 512L;
    if (n != 0) {
      n = 512L - n;
    }
    n += remaining_;
    FuLib.skipFully(in, n);

    entry_ = null;
    remaining_ = 0L;
  }

  @Override
  public int available()
      throws IOException {
    ensureOpen();
    return (entry_ == null) ? 0 :
        Math.max(1, (int) Math.min(remaining_, in.available()));
  }

  @Override
  public int read()
      throws IOException {
    ensureOpen();
    return (read(buf1, 0, 1) == -1) ? -1 : buf1[0] & 0xff;
  }

  @Override
  public int read(byte[] _buffer, int _offset, int _length)
      throws IOException {
    int length = _length;
    ensureOpen();
    if ((_offset < 0) || (length < 0) || (_offset > _buffer.length - length)) {
      throw new IndexOutOfBoundsException();
    } else if (length == 0) {
      return 0;
    }

    if (entry_ == null) {
      return -1;
    }

    if (remaining_ <= 0L) {
      closeEntry();
      return -1;
    }

    if (length > remaining_) {
      length = (int) remaining_;
    }

    int nr = in.read(_buffer, _offset, length);
    if (nr == -1) {
      throw new IOException("unexpected end of tar data");
    }

    remaining_ -= nr;
    return nr;
  }

  @Override
  public long skip(long _length)
      throws IOException {
    long length = _length;
    if (length < 0L) {
      throw new IllegalArgumentException("negative skip length");
    }

    ensureOpen();

    if (length == 0L) {
      return 0L;
    }

    long r = 0L;
    byte[] buf = null;
    try {
      int min = (int) Math.min(4096, Math.min(remaining_, length));
      buf = FuFactoryByteArray.get(min, -1, false);

      while (length > 0L) {
        int nr = (int) Math.min(buf.length, Math.min(remaining_, length));
        nr = in.read(buf, 0, nr);
        if (nr == -1) {
          break;
        }
        remaining_ -= nr;
        length -= nr;
        r += nr;
      }
    } finally {
      FuFactoryByteArray.release(buf);
    }

    return r;
  }

  @Override
  public void close()
      throws IOException {
    if (!closed_) {
      in.close();
      closed_ = true;
    }
  }

  @Override
  public boolean markSupported() {
    return false;
  }

  @Override
  public synchronized void mark(int _limit) {
  }

  @Override
  public synchronized void reset()
      throws IOException {
    throw new IOException("mark/reset not supported");
  }

  // Test
  public static void main(String[] _args)
      throws IOException {
    try (TarInputStream in = new TarInputStream(new FileInputStream(_args[0]))) {
      TarEntry e;
      while ((e = in.getNextEntry()) != null) {
        String s = e.getName();
        if (s.length() > 40) {
          s = "..." + s.substring(s.length() - 37);
        }
        System.out.println
            (FuLib.rightpad(s, 40, ' ') +
                FuLib.leftpad("  " + e.getSize(), 15, ' ') +
                FuLib.rightpad("  " + new Date(e.getTime()), 21, ' '));
      }
    }
  }
}
