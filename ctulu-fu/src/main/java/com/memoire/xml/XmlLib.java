/**
 * @modification 2004-04-28
 * @statut       unstable
 * @file         XmlLib.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.xml;

public final class XmlLib
{
  /**
   * Return JRE version. Copy of FuLib.jdk().
   * @return the double release number of the JVM.
   */
  public static final double jdk()
  {
    return JDK;
  }

  private static final double JDK=computeJDK();

  private static final double computeJDK()
  {
    double r=1.1;
    try
    {
      r=new Double(System.getProperty("java.version")
		   .substring(0,3)).doubleValue();
    }
    catch(Exception ex)
    {
      System.err.println("Unknown JDK version. Switched to 1.1.");
      System.err.println("Version found: "+System.getProperty("java.version"));
    }
    return r;
  }
}
