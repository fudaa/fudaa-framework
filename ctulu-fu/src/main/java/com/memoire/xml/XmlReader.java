/**
 * @modification 2001-10-25
 * @statut       unstable
 * @file         XmlReader.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.xml;

import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Stack;

public class XmlReader
  implements XmlListener
{
  private static Hashtable ORDER;

  static {
    ORDER=new Hashtable();

    Integer I90=new Integer(90);
    Integer I95=new Integer(95);
    Integer I80=new Integer(80);
    Integer I75=new Integer(75);
    Integer I70=new Integer(70);
    Integer I65=new Integer(65);
    Integer I60=new Integer(60);
    Integer I55=new Integer(55);
    Integer I50=new Integer(50);
    Integer I45=new Integer(45);
    Integer I44=new Integer(44);
    Integer I43=new Integer(43);
    Integer I42=new Integer(42);
    Integer I40=new Integer(40);
    //Integer I30=new Integer(30);
    Integer I20=new Integer(20);
    Integer I15=new Integer(15);
    Integer I10=new Integer(10);
    Integer I05=new Integer(05);

    ORDER.put("SCRIPT"    ,I95);

    ORDER.put("HTML"      ,I90);

    ORDER.put("HEAD"      ,I80);
    ORDER.put("BODY"      ,I80);

    ORDER.put("FRAMESET"  ,I75);
    ORDER.put("NOFRAMES"  ,I75);
    ORDER.put("NOSCRIPT"  ,I75);

    ORDER.put("FRAME"     ,I70);

    ORDER.put("APPLET"    ,I65);

    ORDER.put("DIV"       ,I60);
    ORDER.put("BLOCKQUOTE",I60);

    ORDER.put("TABLE"     ,I55);

    ORDER.put("TR"        ,I50);

    ORDER.put("TD"        ,I45);

    ORDER.put("FONT"      ,I44);

    ORDER.put("H1",I43);
    ORDER.put("H2",I43);
    ORDER.put("H3",I43);
    ORDER.put("H4",I43);
    ORDER.put("H5",I43);
    ORDER.put("H6",I43);

    ORDER.put("DL",I43);
    ORDER.put("P" ,I42);

    ORDER.put("UL",I40);
    ORDER.put("OL",I40);

    ORDER.put("LI",I20);
    ORDER.put("DT",I20);
    ORDER.put("DD",I20);

    ORDER.put("A"   ,I15);

    ORDER.put("EM"  ,I10);
    ORDER.put("B"   ,I10);
    ORDER.put("I"   ,I10);
    ORDER.put("CODE",I10);

    ORDER.put("IMG"  ,I05);
    ORDER.put("PARAM",I65);
  }

  private int       mode_;

  private XmlNode   current_,root_;
  private Stack     nodes_     =new Stack();
  private Hashtable attributes_=new Hashtable(11);

  private int       errors_;
  private String    origin_;
  private int       lineno_;
  private int       charno_;

  public XmlReader()
  {
    mode_=XmlParser.XML;
  }
  
  public XmlReader(int _mode)
  {
    mode_=_mode;
  }
  
  @Override
  public void location(String _origin, int _lineno, int _charno)
  {
    origin_=_origin;
    lineno_=_lineno;
    charno_=_charno;
  }

  @Override
  public void startElement(String _tag)
  {
    //System.err.println("XRD: start element "+_tag+" #"+lineno_);

    XmlNode n=new XmlNode();
    n.tag_=_tag;
    n.attributes_=attributes_;
    attributes_=new Hashtable(11);

    if(current_!=null)
    {
      if(  (mode_==XmlParser.HTML)
	 &&current_.tag_.equals(_tag)
	 &&!_tag.equals("FONT")
	 &&!_tag.equals("FRAMESET"))
	missingTag(_tag);
    }

    if(current_!=null) current_.children_.addElement(n);
    current_=n;
    if(root_==null) root_=n;
    nodes_.push(n);
  }

  private void missingTag(String _tag)
  {
    if(  !_tag.equals("P")
       &&!_tag.equals("BLOCKQUOTE")
       &&!_tag.equals("LI")
       &&!_tag.equals("DT")
       &&!_tag.equals("DD")
       &&!_tag.equals("OPTION")
       &&!_tag.equals("PARAM")
       &&!_tag.equals("A"))
    {
      error("missing end "+_tag);
      //System.err.println(current_);
    }
    else current_.children_.addElement(" ");

    endElement(_tag);
  }

  @Override
  public void endElement(String _tag)
  {
    //System.err.println("XRD: end element "+_tag+" #"+lineno_);

    if(nodes_.size()==0)
    {
      error("end needed "+_tag);
      return;
    }

    XmlNode n=(XmlNode)nodes_.peek();

    //if(n.tag_.equals("item")) System.err.println(n);

    if(_tag.equals(n.tag_))
    {
      nodes_.pop();
    }
    else
    {
      if(mode_==XmlParser.HTML)
      {
	boolean done=false;

	while(!done)
	{
	  /*
	    if(  n.tag_.equals("P")
	    ||n.tag_.equals("LI")
	    ||n.tag_.equals("OPTION")
	    ||n.tag_.equals("PARAM")
	    ||_tag.equals("FONT"))
	  */
	  Integer ni=(Integer)ORDER.get(n.tag_);
	  Integer ti=(Integer)ORDER.get(_tag);
	  if((ni!=null)&&(ti!=null)&&(ni.intValue()<ti.intValue()))
	  {
	    current_=n;
	    nodes_.pop();
	    //System.err.println("=== "+_tag+" (remove "+n.tag_+")");

	    if(nodes_.size()>0)
	    {
	      n=(XmlNode)nodes_.peek();
	      done=_tag.equals(n.tag_);
              //System.err.println("PEEK="+n.tag_);
              if(done)
              {
                nodes_.pop();
                n=(XmlNode)nodes_.peek();
                //System.err.println("PEEK="+n.tag_);
              }
	    }
	    else done=true;
	  }
	  else
	  {
	    //System.err.println("=== "+_tag+" (keep "+n.tag_+")");
	    done=true;
	  }
	}
      }
      else
      {
        String s="block "+n.tag_+" /"+_tag;
        try
        {
          if(!s.equals(new String(s.getBytes("ASCII")))||
             (s.length()>50))
            s="block (...)";
        }
        catch(UnsupportedEncodingException ex) { ex.printStackTrace(); }
	error(s);
	//dump();
      }
    }

    if(nodes_.size()>0) current_=(XmlNode)nodes_.peek();
    //else current_=null;
    //if(current_!=null) System.err.println("+++ current="+current_.tag_);
  }

  /*private void dump()
  {
    String t="";
    while(!nodes_.isEmpty())
    {
      XmlNode n=(XmlNode)nodes_.pop();
      System.err.println(t+n.tag_); t+="  ";
    }
    System.exit(-3);
  }*/

  @Override
  public void attribute(String _name,String _value)
  {
    attributes_.put(_name,_value);
  }

  @Override
  public void text(String _data)
  {
    if(current_!=null)
      current_.children_.addElement(_data);
  }

  @Override
  public void error(String _message)
  {
    errors_++;
    if(errors_<20)
    {
      String o=origin_;
      if(o==null) o="-";
      o=o.substring(o.lastIndexOf('/')+1);
      System.err.println
        ("error reading xml: "+_message+" ["+lineno_+":"+charno_+":"+o+"]");
    }
    else
    if(errors_==20)
    {
      System.err.println("too many errors (next ones to be ignored)");
    }
    //System.exit(1);
  }

  public XmlNode getTree()
  {
    return root_;//current_;
  }

  public static void main(String[] _args)
  {
    for(int i=0;i<_args.length;i++)
    {
      try
      {
        String f=_args[i];
	int    m=XmlParser.XML;
	if(f.endsWith(".html")||f.endsWith(".htm")) m=XmlParser.HTML;
	if(f.endsWith(".txt"))                      m=XmlParser.TXT;
	System.err.println("FILE: "+f+" MODE: "+m);
	XmlParser p=new XmlParser(f,m);
	XmlReader r=new XmlReader(m);
	p.setXmlListener(r);
	p.parse();
	System.out.println(r.getTree());
      }
      catch(Exception ex)
      {
	ex.printStackTrace();
	//System.err.println(ex);
      }
    }
  }
}
