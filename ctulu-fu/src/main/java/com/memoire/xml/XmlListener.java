/**
 * @modification 2001-10-25
 * @statut       unstable
 * @file         XmlListener.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.xml;

public interface XmlListener
{
  void location    (String _origin, int _lineno, int _charno);
  void startElement(String _tag);
  void endElement  (String _tag);
  void attribute   (String _name,String _value);
  void text        (String _data);
  void error       (String _message);
}
