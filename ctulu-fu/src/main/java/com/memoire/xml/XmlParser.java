/**
 * @modification 2001-10-25
 * @statut       unstable
 * @file         XmlParser.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.xml;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Stack;

public class XmlParser implements XmlListener {
  public static final int XML = 0;
  public static final int HTML = 1;
  public static final int TXT = 2;

  private Reader in_;
  private char[] buf_;
  private int nbc_;
  private int lineno_, charno_;
  private String origin_;
  private int mode_;
  private XmlListener listener_;

  public XmlParser(String _origin) throws IOException {
    this(_origin, XML);
  }

  public XmlParser(String _origin, int _mode) throws IOException {
    this(convert(_origin), _origin, _mode);
  }

  private static final Reader convert(String _o) throws IOException {
    Reader r = null;
    try {
      r = new InputStreamReader(new URL(_o).openStream());
    } catch (MalformedURLException ex) {
      r = new FileReader(_o);
    }
    return r;
  }

  public XmlParser(File _origin) throws IOException {
    this(new FileReader(_origin), _origin.getName(), XML);
  }

  public XmlParser(File _origin, int _mode) throws IOException {
    this(new FileReader(_origin), _origin.getName(), _mode);
  }

  public XmlParser(URL _origin) throws IOException {
    this(new InputStreamReader(_origin.openStream()), _origin.toString(), XML);
  }

  public XmlParser(URL _origin, int _mode) throws IOException {
    this(new InputStreamReader(_origin.openStream()), _origin.toString(), _mode);
  }

  public XmlParser(Reader _in, String _origin) {
    this(_in, _origin, XML);
  }

  public XmlParser(Reader _in, String _origin, int _mode) {
    Reader in = _in;
    if (!(in instanceof BufferedReader)) in = new BufferedReader(in);

    in_ = in;
    buf_ = new char[8];
    nbc_ = 0;
    origin_ = _origin;
    lineno_ = 1;
    charno_ = 1;
    mode_ = _mode;
    listener_ = this;
  }

  /*
   * private boolean ready() throws IOException { while(in_.ready()&&isSpace(peekC())) readC(); return in_.ready(); }
   */

  public int getMode() {
    return mode_;
  }

  public void setMode(int _mode) {
    mode_ = _mode;
  }

  public String origin() {
    return origin_;
  }

  public int lineno() {
    return lineno_;
  }

  public int charno() {
    return charno_;
  }

  private char readC() throws IOException {
    char r;

    if (nbc_ > 0) {
      nbc_--;
      r = buf_[nbc_];
    } else {
      int c = in_.read();
      if (c == -1) throw new EOFException();
      r = (char) c;
      if (r == '\n') {
        charno_ = 1;
        lineno_++;
      } else
        charno_++;
    }

    return r;
  }

  private char peekC() {
    char r;

    try {
      r = readC();
      putC(r);
    } catch (IOException ex) {
      r = (char) -1;
    }

    return r;
  }

  private void putC(char _c) {
    buf_[nbc_] = _c;
    nbc_++;
  }

  private static final boolean isSpace(char c) {
    return Character.isWhitespace(c);
  }

  private static final boolean isLetterOrDigit(char c) {
    return Character.isLetterOrDigit(c);
  }

  private int tag ;

  private String readToken0() throws IOException {
    char c;
    // boolean espaces=false;

    if ((getMode() != HTML) || (tag > 0)) {
      while (isSpace(c = readC()))/* espaces=true */;
    } else {
      c = readC();
    }

    StringBuffer sb = new StringBuffer(10);

    if (getMode() == TXT) {
      sb.append(c);
      if (isLetterOrDigit(c)) {
        while (isLetterOrDigit(c = peekC()) || (c == '-') || (c == '_'))
          sb.append(readC());
        if (c == '@') {
          sb.append(readC());
          while (isLetterOrDigit(c = peekC()) || (c == '.') || (c == '-') || (c == '_'))
            sb.append(readC());
        } else if ((c == ':')
            && ("http".equals(sb.toString()) || "mailto".equals(sb.toString()) || "ftp".equals(sb.toString()))) while (!isSpace(c = peekC()))
          sb.append(readC());
      }
    } else if ((tag == 0) && (c == '<')) {
      sb.append(c);
      if (peekC() == '!') {
        sb.append(readC());
        char t = readC();
        if ((t == '-') && (peekC() == '-')) {
          putC(t);
          do {
            sb.append(readC());
          } while (!sb.toString().endsWith("-->"));
          tag--;
        } else if ((t == '[') && peekC() == 'C') {
          putC(t);
          do {
            sb.append(readC());
          } while (!sb.toString().endsWith("]]>"));
          tag--;
        } else
          putC(t);
      } else if (peekC() == '?') sb.append(readC());
      else if (peekC() == '/') sb.append(readC());
      tag++;
    } else if ((tag != 0) && (c == '>')) {
      sb.append(c);
      tag--;
    } else if ((tag != 0) && ((c == '?') || (c == '/')) && (peekC() == '>')) {
      sb.append(c);
      sb.append(readC());
      tag--;
    } else if (c == '&') {
      sb.append(c);
      do {
        c = readC();
        sb.append(c);
      } while (c != ';');
    } else if ((tag != 0) && (c == '"')) {
      sb.append(c);
      do {
        c = readC();
        sb.append(c);
      } while (c != '"');
    } else if ((tag != 0) && (c == '\'')) {
      sb.append(c);
      do {
        c = readC();
        sb.append(c);
      } while (c != '\'');
    } else if ((tag != 0) && (c == '[')) {
      sb.append(c);
      do {
        c = readC();
        sb.append(c);
      } while (c != ']');
    } else if ((tag != 0) && isLetterOrDigit(c)) {
      sb.append(c);
      while (isLetterOrDigit(c = peekC()) || (c == '.') || (c == '-') || (c == '_') || (c == ':')) {
        sb.append(readC());
      }

      if ((getMode() == HTML) && (peekC() == '%')) {
        sb.append(readC());
      }
    } else if (tag != 0) {
      sb.append(c);

      if ((getMode() == HTML) && ((c == '#') || (c == '+') || (c == '-'))) {
        while (isLetterOrDigit(c = peekC()) || (c == '.') || (c == '-') || (c == '_') || (c == ':')) {
          sb.append(readC());
        }
      }
    } else {
      // if((getMode()==HTML)&&espaces) sb.append(' ');
      sb.append(c);
      while (((c = peekC()) != '<') && (c != -1))
        sb.append(readC());
    }

    // System.err.println("::"+tag+" '"+sb+"'");
    return sb.toString();
  }

  private Stack pile_ = new Stack();

  private String readToken() throws IOException {
    String r;

    do {
      if (!pile_.empty()) r = (String) pile_.pop();
      else
        r = readToken0();
    } while (r.startsWith("<!--"));

    return r;
  }

  private void pushToken(String _token) {
    pile_.push(_token);
  }

  public void parse() throws IOException {
    try {
      pile_.removeAllElements();

      while (true) {
        String e = readToken();
        listener_.location(origin_, lineno_, charno_);

        if (getMode() == TXT) {
          listener_.text(e);
        } else if ("<".equals(e)) {
          e = readToken();
          if (getMode() == HTML) e = e.toUpperCase();

          String t = e;

          while (true) {
            e = readToken();
            if (">".equals(e) || "/>".equals(e)) break;

            String a = e;
            e = readToken(); // must be '='

            if (getMode() == HTML) {
              if (!e.equals("=")) {
                pushToken(e);
                e = "TRUE";
              } else
                e = readToken();
              // System.err.println(a+"="+e);
            } else {
              if (!e.equals("=")) listener_.error("Missing value for Attribute '" + a + "'");
              else
                e = readToken();
            }

            if ((getMode() == HTML) && ("#".equals(e))) e += readToken();

            if (e.length() >= 2) {
              char c = e.charAt(0);
              if ((c == '"' || c == '\'') && (c == e.charAt(e.length() - 1))) e = e.substring(1, e.length() - 1);
              else if (getMode() != HTML) listener_.error("Invalid Attribute Value '" + a + "': " + e);
            } else if (getMode() != HTML) listener_.error("Invalid Attribute Value '" + a + "': " + e);

            if (getMode() == HTML) a = a.toUpperCase();
            // System.err.println("ATT: "+a+" ~ "+e);
            listener_.attribute(a, e);
          }

          // if(getMode()==HTML) t=t.toUpperCase();
          // System.err.println("OPEN TAG: "+t);
          listener_.startElement(t);

          if (">".equals(e)) {
            if ((getMode() == HTML)
                && ("BR".equals(t) || "FRAME".equals(t) || "HR".equals(t) || "IMG".equals(t) || "LINK".equals(t)
                    || "INPUT".equals(t) || "META".equals(t) || "PARAM".equals(t))) listener_.endElement(t);
          } else if ("/>".equals(e)) {
            // System.err.println("CLOSE TAG: "+t);
            listener_.endElement(t);
          }
        } else if ("</".equals(e)) {
          e = readToken();
          if (getMode() == HTML) e = e.toUpperCase();
          // System.err.println("CLOSE TAG: "+e);
          if ((getMode() == HTML)
              && ("BR".equals(e) || "FRAME".equals(e) || "HR".equals(e) || "IMG".equals(e) || "LINK".equals(e)
                  || "INPUT".equals(e) || "META".equals(e) || "PARAM".equals(e))) ;
          else
            listener_.endElement(e);
          e = readToken();
        } else if ("<?".equals(e)) {
          // System.err.println("PROCESSING");
          do {
            e = readToken();
          } while (!"?>".equals(e));
        } else if (e.startsWith("<![CDATA[")) {
          // System.err.println("BINARY DATA");
          listener_.text(e.substring(9, e.length() - 3));
        } else if ("<!".equals(e)) {
          // System.err.println("DECLARATION");
          do {
            e = readToken();
          } while (!">".equals(e));
        } else {
          // System.err.println("TEXT: "+e);
          listener_.text(e);
        }
      }
    } catch (EOFException ex) {}
  }

  public void setXmlListener(XmlListener _l) {
    if (_l == null) listener_ = this;
    else
      listener_ = _l;
  }

  @Override
  public void location(String _origin, int _lineno, int _charno) {}

  @Override
  public void startElement(String _tag) {
    System.out.println("OPEN TAG  : " + _tag);
  }

  @Override
  public void endElement(String _tag) {
    System.out.println("CLOSE TAG : " + _tag);
  }

  @Override
  public void attribute(String _name, String _value) {
    System.out.println("ATTRIBUTE : " + _name + " WITH VALUE : " + _value);
  }

  @Override
  public void text(String _data) {
    System.out.println("TEXT DATA : " + _data);
  }

  @Override
  public void error(String _message) {
    System.out.println("ERROR     : " + _message + " IN " + origin());
  }

  public static void main(String[] _args) {
    for (int i = 0; i < _args.length; i++) {
      try {
        String f = _args[i];
        int m = XML;
        if (f.endsWith(".html") || f.endsWith(".htm")) m = HTML;
        if (f.endsWith(".txt")) m = TXT;
        System.err.println("FILE: " + f + " MODE: " + m);
        XmlParser p = new XmlParser(f, m);
        p.parse();
      } catch (Exception ex) {
        ex.printStackTrace();
        // System.err.println(ex);
      }
    }
  }
}
