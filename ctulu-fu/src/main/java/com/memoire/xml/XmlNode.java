/**
 * @modification 2001-10-25
 * @statut       unstable
 * @file         XmlNode.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package com.memoire.xml;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

public final class XmlNode
{
  public String    tag_;
  public Hashtable attributes_=new Hashtable(11);
  public Vector    children_  =new Vector(11);

  public String getText()
  {
    String r="";
    Enumeration e=children_.elements();
    while(e.hasMoreElements())
    {
      Object o=e.nextElement();
      if(!(o instanceof XmlNode)) r+=o.toString();//.trim();
    }
    return r;
  }

  public String findText()
  {
    String r="";
    Enumeration e=children_.elements();
    while(e.hasMoreElements())
    {
      Object o=e.nextElement();
      if(o instanceof XmlNode) r+=((XmlNode)o).findText();
      else                     r+=o.toString();
    }
    return r;
  }

  public XmlNode[] getNodes()
  {
    Vector v=new Vector();
    Enumeration e=children_.elements();
    while(e.hasMoreElements())
    {
      Object o=e.nextElement();
      if(o instanceof XmlNode) v.addElement(o);
    }
    int       l=v.size();
    XmlNode[] r=new XmlNode[l];
    for(int i=0;i<l;i++) r[i]=(XmlNode)v.elementAt(i);
    return r;
  }

  public XmlNode[] getNodes(String _name)
  {
    Vector v=new Vector();
    Enumeration e=children_.elements();
    while(e.hasMoreElements())
    {
      Object o=e.nextElement();
      if((o instanceof XmlNode)&&(_name.equals(((XmlNode)o).tag_)))
	v.addElement(o);
    }
    int       l=v.size();
    XmlNode[] r=new XmlNode[l];
    for(int i=0;i<l;i++) r[i]=(XmlNode)v.elementAt(i);
    return r;
  }

  public int findSize()
  {
    int         r=0;
    Enumeration e=children_.elements();
    while(e.hasMoreElements())
    {
      r++;
      Object o=e.nextElement();
      if(o instanceof XmlNode) r+=((XmlNode)o).findSize();
    }
    return r;
  }

  public XmlNode getNode(String _name)
  {
    XmlNode   r=null;
    XmlNode[] n=getNodes(_name);
    if(n.length>0) r=n[0];
    return r;
  }

  public XmlNode findNode(String _name)
  {
    XmlNode   r=null;
    XmlNode[] n=getNodes();
    for(int i=0;i<n.length;i++)
    {
      if(n[i].tag_.equals(_name))
	{ r=n[i]; break; }
      r=n[i].findNode(_name);
      if(r!=null) break;
    }
    return r;
  }

  public XmlNode[] findNodes(String _name)
  {
    Vector v=new Vector();
    findNodes(_name,v);

    int       l=v.size();
    XmlNode[] r=new XmlNode[l];
    for(int i=0;i<r.length;i++)
      r[i]=(XmlNode)v.elementAt(i);
    return r;
  }

  protected void findNodes(String _name, Vector _v)
  {
    XmlNode[] n=getNodes();
    for(int i=0;i<n.length;i++)
    {
      if(n[i].tag_.equals(_name))
	_v.addElement(n[i]);
      else
        n[i].findNodes(_name,_v);
    }
  }

  public String getAttribute(String _name)
  {
    return (String)attributes_.get(_name);
  }

  public String getAttribute(String _name, String _default)
  {
    String r=(String)attributes_.get(_name);
    if(r==null) r=_default;
    return r;
  }

  private static final String indent(final int _n)
  {
    String r="";
    for(int i=0;i<_n;i++) r+="  ";
    return r;
  }

  public String str(int _n)
  {
    int n=_n;
    String r="";
    //System.err.println("STR "+tag_+" "+hashCode());
    r+=indent(n)+"<"+tag_;

    Enumeration e;

    e=attributes_.keys();
    while(e.hasMoreElements())
    {
      String k=(String)e.nextElement();
      String v=(String)attributes_.get(k);
      r+=" "+k+"=\""+v+"\"";
    }

    if(children_.size()>0)
    {
      r+=">\n";

      n++;
      e=children_.elements();
      while(e.hasMoreElements())
      {
	Object o=e.nextElement();
	if(o instanceof XmlNode)
        {
	  r+=((XmlNode)o).str(n);
        }
        else
        {
	  StringTokenizer st=new StringTokenizer(o.toString().trim(),"\n");
	  while(st.hasMoreTokens())
	  {
	    r+=indent(n);
	    r+=st.nextToken().trim();
	    r+="\n";
	  }
        }
      }
      n--;

      for(int i=0;i<n;i++) r+="  ";
      r+="</"+tag_+">\n";
    }
    else r+=" />\n";

    return r;
  }

  @Override
  public String toString()
  {
    return str(0);
  }
}
