/**
 * @modification $Date: 2006-09-19 14:35:19 $
 * @statut       unstable
 * @file         VfsOperations.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2004 Guillaume Desnoix
 */

package com.memoire.vfs;

import java.io.File;

public interface VfsOperations
{
  void renameFile (File   _o);
  void copyFile   (File   _o,File _dst);
  void moveFiles  (File[] _o,File _dir);
  void copyFiles  (File[] _o,File _dir);
  void linkFiles  (File[] _o,File _dir);
  void deleteFiles(File[] _o);
}
