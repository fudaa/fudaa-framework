/**
 * @modification $Date: 2006-09-19 14:35:18 $
 * @statut       unstable
 * @file         VfsFileRam.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.vfs;

import com.memoire.fu.FuEmptyArrays;
import com.memoire.fu.FuLib;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownServiceException;
import java.util.Vector;

/**
 * File in memory (ram disk).
 */
public class VfsFileRam
  extends VfsFile
  implements FuEmptyArrays
{
  private static boolean init_=false;

  static { init(); }

  public static void init()
  {
    if(init_) return;
    init_=true;

    try
    {
      Method m=File.class.getMethod("listRoots",CLASS0);
      File[] l=(File[])m.invoke(null,OBJECT0);
      for(int i=0;i<l.length;i++)
      {
	String p=l[i].getPath(); //AbsolutePath();
	createRootPath(p);
      }
    }
    catch(NoSuchMethodException ex1) { }
    catch(Exception ex2) { ex2.printStackTrace(); }

    if(VfsFile.isRamMode())
    {
      createPropertyPath("user.dir");
      createPropertyPath("user.home");
      createPropertyPath("java.io.tmpdir");
    }

    VfsUrlRam.init();
  }

  private static void createRootPath(String _path)
  {
    if(_path!=null)
      new VfsFileRam(_path).mkdir();
  }

  private static void createPropertyPath(String _name)
  {
    String p=null;
    try { p=System.getProperty(_name); }
    catch(SecurityException ex) { }

    if(p!=null)
    {
      //System.err.println("Create "+_path);
      new VfsFileRam(p).mkdirs();
    }
  }

  private static String cleanPrefix(String _path)
  {
    String path=_path;
    if((path!=null)&&path.startsWith("ram:"))
      path=path.substring(4);
    return path;
  }

  private static String addPrefix(String _path)
  {
    String path=_path;
    if(path!=null)
    {
      path=cleanPrefix(path);
      if(!isRamMode()&&path.startsWith(separator))
        path="ram:"+path;
    }
    return path;
  }

  public VfsFileRam(File _file, String _name)
  {
    this(_file==null ? (String)null : _file.getAbsolutePath(),_name);
  }

  public VfsFileRam(String _path)
  {
    super(cleanPrefix(_path));
  }

  public VfsFileRam(String _file, String _name)
  {
    super(cleanPrefix(_file),cleanPrefix(_name));
  }

  @Override
  public final VfsFile createChild(String _name)
  {
    return new VfsFileRam(this,_name);
  }

  @Override
  public InputStream getInputStream() throws IOException
  {
    if(!canRead()) return null;
    byte[] bytes;
    if(isDirectory())
    {
      String localseparator;
      try { localseparator=System.getProperty("line.separator"); }
      catch(SecurityException ex) { localseparator="\n"; }

      String[]     f=list();
      int          l=f.length;
      StringBuffer b=new StringBuffer(l*80);
      for(int i=0;i<l;i++)
      {
	b.append(f[i]);
	b.append(localseparator);
      }
      bytes=b.toString().getBytes();
    }
    else
    {
      String p=cleanPrefix(getAbsolutePath());
      bytes=VfsRamDisk.getInstance()
        .getOutputStream(p).toByteArray();
    }
    return new ByteArrayInputStream(bytes);
  }

  @Override
  public OutputStream getOutputStream() throws IOException
  {
    if(!canWrite()) return null;
    if(isDirectory())
      throw new UnknownServiceException
	("protocol doesn't support output");
    String p=cleanPrefix(getAbsolutePath());
    VfsRamDisk.getInstance().createFile(p);
    return VfsRamDisk.getInstance().getOutputStream(p);
  }

  @Override
  public String getSeparator()
  {
    return separator;
  }

  @Override
  public String getViewText()
  {
    String p=cleanPrefix(getAbsolutePath());
    return addPrefix(isRamMode()
                     ? FuLib.reducedPath(p)
                     : p);
  }

  // Standard API

  @Override
  public boolean canRead()
  {
    return VfsRamDisk.getInstance().canRead
      (cleanPrefix(getAbsolutePath()));
  }

  @Override
  public boolean canWrite()
  {
    return VfsRamDisk.getInstance().canWrite
      (cleanPrefix(getAbsolutePath()));
  }

  /*
  public int compareTo(File _path)
  {
    return super.compareTo(_path);
  }
  */

  /*
  public int compareTo(Object _o)
  {
    return super.compareTo(_o);
  }
  */

  /*
  public boolean createNewFile();
  {
    return super.createNewFile();
  }
  */

  @Override
  public boolean delete()
  {
    return VfsRamDisk.getInstance().delete
      (cleanPrefix(getAbsolutePath()));
  }

  /*
  public void deleteOnExit()
  {
    super.deleteOnExit();
  }
  */

  @Override
  public boolean equals(Object _o)
  {
    if((_o==null)||!(_o instanceof File)) return false;

    String a=getAbsolutePath();
    String b=((File)_o).getAbsolutePath();
    if(a.endsWith("/")) a=a.substring(0,a.length()-1);
    if(b.endsWith("/")) b=b.substring(0,b.length()-1);
    return a.equals(b);
  }

  @Override
  public int hashCode()
  {
    String a=getAbsolutePath();
    if(a.endsWith("/")) a=a.substring(0,a.length()-1);
    return a.hashCode();
  }

  @Override
  public boolean exists()
  {
    return VfsRamDisk.getInstance().exists
      (cleanPrefix(getAbsolutePath()));
  }
    
  @Override
  public String getAbsolutePath()
  {
    return addPrefix(super.getAbsolutePath());
  }

  @Override
  public String getCanonicalPath()
    throws IOException
  {
    return getAbsolutePath();
  }

  @Override
  public String getParent()
  {
    return addPrefix(super.getParent());
  }

  @Override
  public String getPath()
  {
    return addPrefix(super.getPath());
  }

  @Override
  public boolean isAbsolute()
  {
    return super.isAbsolute();
  }

  @Override
  public boolean isDirectory()
  {
    return VfsRamDisk.getInstance().isDirectory
      (cleanPrefix(getAbsolutePath()));
  }

  @Override
  public boolean isFile()
  {
    return VfsRamDisk.getInstance().isFile
      (cleanPrefix(getAbsolutePath()));
  }

  /*
  public boolean isHidden()
  { 
    return super.isHidden();
  }
  */

  @Override
  public long lastModified()
  {
    return VfsRamDisk.getInstance().lastModified
      (cleanPrefix(getAbsolutePath()));
  }

  @Override
  public long length()
  {
    return VfsRamDisk.getInstance().length
      (cleanPrefix(getAbsolutePath()));
  }

  //Not efficient at all.
  @Override
  public String[] list()
  {
    String p=cleanPrefix(getAbsolutePath());
    //System.err.println("((((( RAM list("+p+")");
    if(!p.endsWith(File.separator)) p+=File.separator;

    String[] paths=VfsRamDisk.getInstance().getPaths();
    int      lp=paths.length;
    int      ls=lp;

    boolean hasParent=(getParent()!=null);
    if(hasParent) ls++;
    ls++; // for current dir

    String[] s=new String[ls];
    int      i=0;

    s[i]="."; i++;
    if(hasParent) { s[i]=".."; i++; }

    int n=p.length();
    for(int j=0;j<lp;j++)
    {
      String q=paths[j];
      if(!q.equals(p)&&q.startsWith(p)&&(q.indexOf(File.separator,n)<0))
      {
	s[i]=q.substring(n);
	i++;
	//System.err.println("  Q="+q+" "+s[i]);
      }
    }
    //FuSort.sort(s,0,i);
    //for(i=0;i<l;i++)
    //System.out.println(s[i]);

    String[] r=new String[i];
    System.arraycopy(s,0,r,0,i);
    //System.err.println("((((( RAM found "+i+" files");
    return r;
  }

  //String[] list(FilenameFilter filter)

  @Override
  public File[] listFiles()
  {
    String[] s=list();
    if(s==null) return null;

    int      n=s.length;
    File[]   r=new File[n];
    for(int i=0; i<n; i++)
      r[i]=createChild(s[i]);

    return r;
  }

  //File[] listFiles(FileFilter filter)
  //File[] listFiles(FilenameFilter filter)

  @Override
  public boolean mkdir()
  {
    if(exists()) return false;
    VfsRamDisk.getInstance().createDirectory
      (cleanPrefix(getAbsolutePath()));
    return true;
  }

  @Override
  public boolean mkdirs()
  {
    return exists() ? false : mkdirs0();    
  }

  protected boolean mkdirs0()
  {
    //System.err.println("mkdirs0: "+this);
    Vector    v=new Vector();
    VfsFileRam f=this;
    while(/*(f!=null)&&*/!f.exists())
    {
      v.addElement(f);
      f=new VfsFileRam(f.getParent());
    }

    boolean r=true;
    for(int i=v.size()-1;i>=0;i--)
    {
      f=(VfsFileRam)v.elementAt(i);
      r&=f.mkdir();
      //System.err.println("create-dir "+f+" "+f.exists());
    }

    return r;
  }

  @Override
  public synchronized boolean renameTo(File _dest)
  {
    return VfsRamDisk.getInstance().rename
      (cleanPrefix(getAbsolutePath()),
       cleanPrefix(_dest.getAbsolutePath()));
  }

  /*
  public boolean setLastModified(long _time)
  {
    return super.setLastModified(_time);
  }
  */

  /*
  public boolean setReadOnly()
  {
    return super.setReadOnly();
  }
  */

  @Override
  public String toString()
  {
    return super.toString();
  }
    
  @Override
  public URL toURL()
    throws MalformedURLException
  {
    String r=cleanPrefix(getAbsolutePath());
    if(separatorChar!='/') r=r.replace(separatorChar,'/');
    if(!r.startsWith("/")) r="/"+r;
    if(!r.endsWith("/")&&isDirectory()) r=r+"/";
    return new URL("ram","",r);
  }

  /*
  public URI toURI()
  {
    return super.setReadOnly();
  }
  */

  /*
  public VfsFile getParentVfsFile()
  {
    String path=getParent();
    System.err.println("PARENT-PATH: "+path);
    return (path==null) ? null : new VfsFileRam(path);
  }

  public VfsFile getAbsoluteVfsFile()
  {
    return new VfsFileRam(getAbsolutePath());
  }

  public VfsFile getCanonicalVfsFile()
    throws IOException
  {
    return new VfsFileRam(getCanonicalPath());
  }
  */

  /*
  public static void tree()
  {
    int i=0,l=DIR.size();
    Enumeration e=DIR.keys();
    String[]    s=new String[l];
    while(e.hasMoreElements())
      { s[i]=(String)e.nextElement(); i++; }
    FuSort.sort(s);
    for(i=0;i<l;i++)
      System.out.println(s[i]);
  }
  */
  /*
  private static void test(File f)
  {
    try
    {
      System.err.println("getName   : "+f.getName());
      System.err.println("getAbsPath: "+f.getAbsolutePath());
      System.err.println("getCanPath: "+f.getCanonicalPath());
      System.err.println("getPath   : "+f.getPath());
      System.err.println("getParent : "+f.getParent());
      System.err.println("getParFile: "+f.getParentFile());
      try { System.err.println("toURL     : "+f.toURL()); }
      catch(MalformedURLException ex) { }
      System.err.println("isAbsolute: "+f.isDirectory());
      System.err.println("isDir     : "+f.isDirectory());
      System.err.println("isFile    : "+f.isFile());
      System.err.println("canRead   : "+f.canRead());
      System.err.println("canWrite  : "+f.canWrite());
      //File[] c=f.listFiles();
      //if(c!=null) for(int i=0;i<c.length;i++) System.err.println("  "+c[i]);
      //if(c!=null) System.err.println("listFiles: "+c.length+" files");
    }
    catch(Exception ex) { ex.printStackTrace(); }
  }

  private static void compare(String p)
  {
    test(new VfsFileRam(p));
    System.err.println("-------");
    test(new File(p));
    System.err.println("-----------------------------------------------");
  }

  public static void main(String[] _args)
  {
    if(_args.length==0) _args=new String[]
      { "/tmp" };//"/home/x","/home/desnoix" };

    System.err.println("-----------------------------------------------");
    for(int i=0;i<_args.length;i++)
      compare(_args[i]);
  }*/
}
