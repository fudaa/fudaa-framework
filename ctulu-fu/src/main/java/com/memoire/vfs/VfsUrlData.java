/**
 * @modification $Date: 2007-05-04 13:41:59 $
 * @statut       unstable
 * @file         VfsUrlData.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.vfs;

import com.memoire.fu.FuLib;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

/**
 * Data protocol.
 */
public class VfsUrlData {
  private static class Connection extends URLConnection {
    private boolean connected_;
    private InputStream in_;
    // private OutputStream out_ =null;
    private long modified_;
    private int length_ = -1;
    private String type_;
    private byte[] data_;

    public Connection(URL _url) {
      super(_url);
    }

    @Override
    public synchronized void connect() throws IOException {
      if (connected_) return;

      String s = getURL().toString();

      int p = s.indexOf(',');
      if (p < 0) throw new MalformedURLException(s);

      String data = s.substring(p + 1);

      if (s.substring(0, p).endsWith("base64")) data_ = FuLib.decodeBase64(data);
      else
        data_ = FuLib.decodeWwwFormUrl(data).getBytes();

      length_ = data_.length;

      s = s.substring(5, p);
      String[] t = FuLib.split(s, ';');
      // if(s.endsWith(";base64"))
      // s=s.substring(0,s.length()-7);
      // p=s.indexOf(';');
      // if(p>0) s=s.substring(0,p);
      if (t.length == 0) type_ = "text/plain";
      else
        type_ = t[0];
      // System.err.println("VUD: TYPE="+type_);

      if (getDoInput()) in_ = new ByteArrayInputStream(data_);
      // if(getDoOutput()) out_=f.getOutputStream();
      connected_ = true;
    }

    /*
     * public Permission getPermission() throws IOException { return null; }
     */

    @Override
    public InputStream getInputStream() throws IOException {
      connect();
      return in_;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
      connect();
      return null;// out_;
    }

    @Override
    public long getLastModified() {
      return modified_;
    }

    @Override
    public int getContentLength() {
      return length_;
    }

    @Override
    public String getContentType() {
      return type_;
    }
  }

  private static boolean init_ = false;

  static {
    init();
  }

  public static void init() {
    if (init_) return;
    init_ = true;

    URLStreamHandler h = new URLStreamHandler() {
      @Override
      protected URLConnection openConnection(URL _url) throws IOException {
        // FuLog.debug("VUD: openConnection("+_url+")");
        return new Connection(_url);
      }
    };

    FuLib.setUrlHandler("data", h);
  }

  /*
   * public static void main(String[] _args) throws IOException { init(); URL u=new URL("data:,A%20brief%20note");
   * InputStream in=u.openStream(); int c; System.err.println("URL: "+u); System.err.print("DAT: ");
   * while((c=in.read())!=-1) System.err.print((char)c); System.err.flush(); u=new
   * URL("data:image/gif;base64,R0lGODdhMAAwAPAAAAAAAP///ywAAAAAMAAwAAAC8IyPqcvt3wCcDkiLc7C0qwyGHhSWpjQu5yqmCYsapyuvUUlvONmOZtfzgFzByTB10QgxOR0TqBQejhRNzOfkVJ+5YiUqrXF5Y5lKh/DeuNcP5yLWGsEbtLiOSpa/TPg7JpJHxyendzWTBfX0cxOnKPjgBzi4diinWGdkF8kjdfnycQZXZeYGejmJlZeGl9i2icVqaNVailT6F5iJ90m6mvuTS4OK05M0vDk0Q4XUtwvKOzrcd3iq9uisF81M1OIcR7lEewwcLp7tuNNkM3uNna3F2JQFo97Vriy/Xl4/f1cf5VWzXyym7PHhhx4dbgYKAAA7");
   * final Image i=Toolkit.getDefaultToolkit().createImage(u); Frame f=new Frame("Test") { public void paint(Graphics
   * _g) { super.paint(_g); System.err.println("WH="+i.getWidth(this)+"x"+i.getHeight(this));
   * _g.drawImage(i,20,20,null); } }; f.setSize(100,100); f.show(); }
   */
}
