/**
 * @modification $Date: 2007-05-04 13:41:59 $
 * @statut       unstable
 * @file         VfsUrlBzip2.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.vfs;

import com.memoire.bzip.BZip2InputStream;
import com.memoire.fu.FuLib;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

/**
 * Bzip2 protocol.
 */
public class VfsUrlBzip2 {
  private static class Connection extends URLConnection {
    private boolean connected_;
    private InputStream in_;
    // private OutputStream out_ =null;
    private long modified_;
    private int length_ = -1;
    private String type_;

    public Connection(URL _url) {
      super(_url);
    }

    @Override
    public synchronized void connect() throws IOException {
      if (connected_) return;

      String f = getURL().getFile();
      URL u = new URL(f);
      URLConnection c = u.openConnection();
      modified_ = c.getLastModified();
      length_ = -1;// c.getContentLength();
      type_ = c.getContentType();// guessContentTypeFromName(f);

      if (getDoInput()) {
        in_ = new BufferedInputStream(c.getInputStream());
        if (FuLib.isBzip2ed(in_)) in_ = new BZip2InputStream(in_);
      }

      connected_ = true;
    }

    /*
     * public Permission getPermission() throws IOException { return null; }
     */

    @Override
    public InputStream getInputStream() throws IOException {
      connect();
      return in_;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
      connect();
      return null;// out_;
    }

    @Override
    public long getLastModified() {
      return modified_;
    }

    @Override
    public int getContentLength() {
      return length_;
    }

    @Override
    public String getContentType() {
      return type_;
    }
  }

  private static boolean init_ = false;

  static {
    init();
  }

  public static void init() {
    if (init_) return;
    init_ = true;

    URLStreamHandler h = new URLStreamHandler() {
      @Override
      protected URLConnection openConnection(URL _url) throws IOException {
        // FuLog.debug("VUG: openConnection("+_url+")");
        return new Connection(_url);
      }
    };

    FuLib.setUrlHandler("bzip2", h);
  }

  /*
   * public static void test(URL u) throws IOException { InputStream in=u.openStream(); //System.err.println("Type
   * ="+u.getContentType()); //System.err.println("Length="+u.getContentLength()); int c; //System.err.println("URL:
   * "+u); //System.err.print("DAT: "); while((c=in.read())!=-1) System.err.print((char)c); System.err.flush();
   * in.close(); } public static void main(String[] _args) throws IOException { init(); URL u; u=new
   * URL("bzip2:file:/home/desnoix/hello.txt.bz2"); test(u); }
   */
}
