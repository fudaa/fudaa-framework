/**
 * @modification $Date: 2007-05-04 13:41:59 $
 * @statut       unstable
 * @file         VfsUrlRam.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.vfs;

import com.memoire.fu.FuLib;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

/**
 * Ram protocol.
 */
public class VfsUrlRam
  //extends VfsFile
  //implements FuEmptyArrays
{
  private static class Connection extends URLConnection
  {
    private boolean      connected_;
    private InputStream  in_       ;
    private OutputStream out_      ;
    private long         modified_ ;
    private int          length_   =-1;
    private String       type_     ;

    public Connection(URL _url)
    {
      super(_url);
    }

    @Override
    public synchronized void connect() throws IOException
    {
      if(connected_) return;
      VfsFileRam f=new VfsFileRam(getURL().getFile());
      if(getDoInput())  in_ =f.getInputStream();
      if(getDoOutput()) out_=f.getOutputStream();
      modified_=f.lastModified();
      type_=guessContentTypeFromName(f.getPath());
      if(f.length()<Integer.MAX_VALUE)
        length_=(int)f.length();
      connected_=true;
    }

    /*
    public Permission getPermission() throws IOException
    {
      return null;
    }
    */

    @Override
    public InputStream getInputStream() throws IOException
    {
      connect();
      return in_;
    }

    @Override
    public OutputStream getOutputStream() throws IOException
    {
      connect();
      return out_;
    }

    @Override
    public long getLastModified()
    {
      return modified_;
    }

    @Override
    public int getContentLength()
    {
      return length_;
    }

    @Override
    public String getContentType()
    {
      return type_;
    }
  }

  private static boolean init_=false;

  static { init(); }

  public static void init()
  {
    if(init_) return;
    init_=true;

    URLStreamHandler h=new URLStreamHandler()
    {
      @Override
      protected URLConnection openConnection(URL _url) throws IOException
      {
	//FuLog.debug("VUR: openConnection("+_url+")");
	return new Connection(_url);
      }							   
    };

    FuLib.setUrlHandler("ram",h);
    VfsFileRam.init();
  }
}
    

