/**
 * @modification $Date: 2006-09-19 14:35:20 $
 * @statut       unstable
 * @file         VfsFileTar.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.vfs;

import com.memoire.fu.FuVectorString;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Tar file.
 */
public class VfsFileTar
  extends VfsFileUrl
{
  public VfsFileTar(URL _a, String _n) throws MalformedURLException
  {
    this(new URL(_a,_n));
  }

  public VfsFileTar(URL _url)
  {
    super(_url);
    if(!"tar".equals(_url.getProtocol()))
      throw new IllegalArgumentException("Not a TAR url");
    canWrite_=false;
    build();
  }

  @Override
  protected FuVectorString readList(BufferedReader _nr)
    throws IOException
  {
    FuVectorString v=new FuVectorString(100);

    while(true)
    {
      String l=_nr.readLine();
      if(l==null) break;
      v.addElement(l);
    }

    return v;
  }

  /*
  public static void main(String[] _args)
    throws IOException
  {
    VfsUrlTar.init();

    URL u=new URL("tar:file:/home/desnoix/myMileage-0.30.tar!/");
    VfsFile f=new VfsFileTar(u);
    System.err.println("isDirectory="+f.isDirectory());
    System.err.println("isAbsolute ="+f.isAbsolute());
    System.err.println("canRead    ="+f.canRead());
    System.err.println("canWrite   ="+f.canWrite());
    System.err.println("path       ="+f.getPath());
    System.err.println("absolute   ="+f.getAbsolutePath());
    System.err.println("canonical  ="+f.getCanonicalPath());
    System.err.println("parent     ="+f.getParent());
    System.err.println("parent file="+f.getParentFile());

    String[] s=f.list();
    if(s==null) s=new String[0];
    for(int i=0;i<s.length;i++)
      System.err.println(s[i]);

    //System.err.println(f.createChild("hello"));
    //System.err.println(VfsFile.ensureVfsFile(f.createChild("hello")));
  }
  */
}
