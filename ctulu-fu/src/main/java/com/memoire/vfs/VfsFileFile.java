/**
 * @modification $Date: 2006-09-19 14:35:18 $
 * @statut       unstable
 * @file         VfsFileFile.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.vfs;

import com.memoire.fu.FuLib;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

/**
 * Local file.
 */
public class VfsFileFile
  extends    VfsFile
{
  public VfsFileFile(File _file, String _name)
  {
    super(_file,_name);
  }

  public VfsFileFile(String _path)
  {
    super(_path);
  }

  public VfsFileFile(String _file, String _name)
  {
    super(_file,_name);
  }

  @Override
  public final VfsFile createChild(String _name)
  {
    return new VfsFileFile(this,_name);
  }

  @Override
  public InputStream getInputStream() throws IOException
  {
    return canRead() ? new FileInputStream(this) : null;
  }

  @Override
  public OutputStream getOutputStream() throws IOException
  {
    boolean writable=canWrite();
    if(!writable)
    {
      VfsFile p=getParentVfsFile();
      writable=(p!=null)&&p.exists()&&p.canWrite();
    }
    return writable ? new FileOutputStream(this) : null;
  }

  @Override
  public String getSeparator()
  {
    return separator;
  }

  // Standard API

  @Override
  public boolean canRead()
  {
    return super.canRead();
  }

  @Override
  public boolean canWrite()
  {
    return super.canWrite();
  }

  /*
  public int compareTo(File _path)
  {
    return super.compareTo(_path);
  }
  */

  /*
  public int compareTo(Object _o)
  {
    return super.compareTo(_o);
  }
  */

  /*
  public boolean createNewFile();
  {
    return super.createNewFile();
  }
  */

  @Override
  public boolean delete()
  {
    return super.delete();
  }

  /*
  public void deleteOnExit()
  {
    super.deleteOnExit();
  }
  */

  @Override
  public boolean equals(Object _o)
  {
    return super.equals(_o);
  }

  @Override
  public boolean exists()
  {
    return super.exists();
  }
    
  @Override
  public String getAbsolutePath()
  {
    return super.getAbsolutePath();
  }

  @Override
  public String getCanonicalPath()
    throws IOException
  {
    return super.getCanonicalPath();
  }

  @Override
  public String getName()
  {
    return super.getName();
  }

  @Override
  public String getParent()
  {
    return super.getParent();
  }

  @Override
  public String getPath()
  {
    return super.getPath();
  }

  @Override
  public int hashCode()
  {
    return super.hashCode();
  }

  @Override
  public boolean isAbsolute()
  {
    return super.isAbsolute();
  }

  @Override
  public boolean isDirectory()
  {
    return super.isDirectory();
  }

  @Override
  public boolean isFile()
  {
    return super.isFile();
  }

  /*
  public boolean isHidden()
  { 
    return super.isHidden();
  }
  */

  @Override
  public boolean isLink()
  {
    boolean r=false;
    if(!FuLib.isWindows())
    {
      try
      {
        String[] pe=FuLib.split(getAbsolutePath(),separatorChar,false,false);
        for(int i=0;i<pe.length;i++)
        {
          if(".".equals(pe[i]))
            pe[i]=null;
          else
          if("..".equals(pe[i]))
            pe[i]=pe[i-1]=null;
        }
        String ap=FuLib.join(pe,separatorChar);
        String cp=getCanonicalPath();
        
        if(!cp.equals(ap))
        {
          VfsFile p=getParentVfsFile();
          if((p!=null)&&p.isLink()) 
	  {
            try
	    {
              VfsFile p1=p.getCanonicalVfsFile().createChild(getName());
              VfsFile p2=p.createChild(getName());
              //System.err.println(p1.getPath());
              //System.err.println(p2.getCanonicalPath());
              r=!p1.getPath().equals(p2.getCanonicalPath());
            }
            catch(IOException ex) { r=false; }
          }
        }
      }
      catch(IOException ex) { r=false; }
    }

    return r;
  }

  @Override
  public long lastModified()
  {
    return super.lastModified();
  }

  @Override
  public long length()
  {
    return super.length();
  }

  @Override
  public String[] list()
  {
    return super.list();
  }

  //String[] list(FilenameFilter filter)

  @Override
  public File[] listFiles()
  {
    String[] s=list();
    if(s==null) return null;

    int      n=s.length;
    File[]   r=new File[n];
    for(int i=0; i<n; i++)
      r[i]=createChild(s[i]);

    return r;
  }

  //File[] listFiles(FileFilter filter)
  //File[] listFiles(FilenameFilter filter)

  @Override
  public boolean mkdir()
  {
    return super.mkdir();
  }

  @Override
  public boolean mkdirs()
  {
    return super.mkdirs();
  }

  @Override
  public boolean renameTo(File _dest)
  {
    return super.renameTo(_dest);
  }

  /*
  public boolean setLastModified(long _time)
  {
    return super.setLastModified(_time);
  }
  */

  /*
  public boolean setReadOnly()
  {
    return super.setReadOnly();
  }
  */

  @Override
  public String toString()
  {
    return super.toString();
  }
    
  @Override
  public URL toURL()
    throws MalformedURLException
  {
    /*
    String r=getPath();
    if(separatorChar!='/') r=r.replace(separatorChar,'/');
    if(!r.startsWith("/")) r="/"+r;
    if(!r.endsWith("/")&&isDirectory()) r=r+"/";
    return new URL("file","",r);
    */
    return FuLib.toURL(this);
  }

  @Override
  public URI toURI()
  {
    return super.toURI();
  }
}
