/**
 * @modification $Date: 2007-05-04 13:41:59 $
 * @statut       unstable
 * @file         VfsUrlTar.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.vfs;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuVectorString;
import com.memoire.tar.TarEntry;
import com.memoire.tar.TarFile;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.Enumeration;

/**
 * Tar protocol.
 */
public class VfsUrlTar
{
  private static class Connection extends URLConnection
  {
    private boolean      connected_;
    private InputStream  in_       ;
    //private OutputStream out_      =null;
    private long         modified_ ;
    private int          length_   =-1;
    private String       type_     ;

    public Connection(URL _url)
    {
      super(_url);
    }

    @Override
    public synchronized void connect() throws IOException
    {
      if(connected_) return;

      String s=getURL().getFile();
      if(s.startsWith("file:")) s=s.substring(5);
      while(s.startsWith("//")) s=s.substring(1);

      int    p=s.indexOf("!/"); //last...
      String t=s.substring(p+2);
      s=s.substring(0,p);

      boolean isDirectory=t.equals("")||t.endsWith("/");

      //System.err.println("S="+s);
      //System.err.println("T="+t);

      File f=new File(s);
      if(!f.exists())
        throw new IOException(s+" does not exist");
      if(!f.canRead())
        throw new IOException(s+" is not readable");

      if(getDoInput())
      {
        TarFile zf=new TarFile(f,TarFile.OPEN_READ);

        if(isDirectory)
        {
          Enumeration e=zf.entries();
          FuVectorString v=new FuVectorString();
          while(e.hasMoreElements())
          {
            TarEntry ze=(TarEntry)e.nextElement();
            String ne=ze.getName();
            //System.err.println("Z="+ne);
            if(ne.equals(t))
            {
              modified_=ze.getTime();
            }
            else
            if(ne.startsWith(t))
            {
              ne=ne.substring(t.length());
              int q=ne.indexOf("/");
              if(q>=0) ne=ne.substring(0,q+1);
              v.addElement(ne);
              //System.err.println("add "+ne);
            }
          }
        
          v.sort();
          v.uniq();
          s=FuLib.join(v.toArray(),'\n')+'\n';
          //System.err.println("S="+s);

          byte[] b=s.getBytes();
          type_  ="text/plain";
          length_=b.length;
          in_=new ByteArrayInputStream(b);
          //if(getDoOutput()) out_=f.getOutputStream();
        }
        else
        {
          TarEntry ze=zf.getEntry(t);
          if(ze==null)
            throw new FileNotFoundException("entry not found: "+t);

          /*
          FuLog.debug("*** "+ze+" "+ze.getPosition()+" "+ze.getSize());
          int c;
          in_=zf.getInputStream(ze);
          while((c=in_.read())!=-1)
            System.err.print((char)c);
          System.err.flush();
          in_.close();
          */

          in_=zf.getInputStream(ze);
          modified_=ze.getTime();
          type_    =guessContentTypeFromName(t);

          long sz=ze.getSize();
          if(sz>Integer.MAX_VALUE) sz=-1;
          length_=(int)sz;
        }
      }

      connected_=true;
    }

    /*
    public Permission getPermission() throws IOException
    {
      return null;
    }
    */

    @Override
    public InputStream getInputStream() throws IOException
    {
      connect();
      return in_;
    }

    @Override
    public OutputStream getOutputStream() throws IOException
    {
      connect();
      return null;//out_;
    }

    @Override
    public long getLastModified()
    {
      return modified_;
    }

    @Override
    public int getContentLength()
    {
      return length_;
    }

    @Override
    public String getContentType()
    {
      return type_;
    }
  }

  private static boolean init_=false;

  static { init(); }

  public static void init()
  {
    if(init_) return;
    init_=true;

    URLStreamHandler h=new URLStreamHandler()
    {
      @Override
      protected URLConnection openConnection(URL _url) throws IOException
      {
        URL url=_url;
	//FuLog.debug("VUT: openConnection("+_url+")");

        String n=url.getFile();
        int    p=n.indexOf("!/");

        if(p<0)
          return new URL(n).openConnection();

        //if(!n.endsWith("/"))
        //return new URL("jar:"+n).openConnection();

        String u=n.substring(0,p);

        if(!n.startsWith("file:"))
        {
          try
          {
            VfsCache cache=VfsLib.getCache();
            if(cache!=null)
            {
              File f=cache.getFile(new URL(u),VfsCache.CHECK_AND_DOWNLOAD);
              if(f!=null)
              {
                n=f.toURI().toURL()+n.substring(p);
                //FuLog.debug("TAR: converted url is tar:"+n);
                url=new URL("tar:"+n);
              }
            }
          }
          catch(MalformedURLException ex)
          {
            throw new IOException("not an URL: "+u);
          }
        }

        if(!n.startsWith("file:"))
          throw new IOException("not a local tar: "+u);

	return new Connection(url);
      }							   
    };

    FuLib.setUrlHandler("tar",h);
  }

  /*
  public static void test(URL u)
    throws IOException
  {
    InputStream in=u.openStream();
    int c;
    //System.err.println("URL: "+u);
    //System.err.print("DAT: ");
    while((c=in.read())!=-1)
      System.err.print((char)c);
    System.err.flush();
    in.close();
  }

  // Test
  public static void main(String[] _args)
    throws IOException
  {
    init();
    URL u;

    u=new URL("tar:file:/home/desnoix/myMileage-0.30.tar!/");
    test(u);
    u=new URL("tar:file:/home/desnoix/myMileage-0.30.tar!/myMileage/");
    test(u);
    u=new URL("tar:file:/home/desnoix/myMileage-0.30.tar!/myMileage/src/");
    test(u);
    u=new URL("tar:file:/home/desnoix/myMileage-0.30.tar!/myMileage/src/TableEntry.java");
    test(u);
    System.err.println("--------------------------------");
    u=new URL("tar:file:/home/desnoix/myMileage-0.30.tar!/myMileage/src/");
    VfsFile f=VfsFile.createFile(u);
    System.err.println("isDirectory="+f.isDirectory());
    System.err.println("isAbsolute ="+f.isAbsolute());
    System.err.println("canRead    ="+f.canRead());
    System.err.println("canWrite   ="+f.canWrite());
    System.err.println("path       ="+f.getPath());
    System.err.println("absolute   ="+f.getAbsolutePath());
    System.err.println("canonical  ="+f.getCanonicalPath());
    System.err.println("parent     ="+f.getParent());
    System.err.println("parent     ="+f.getParentFile());
    System.err.println("p parent   ="+f.getParentFile().getParent());
    System.err.println("p p parent ="+f.getParentFile().getParentFile().getParent());
  }
  */
}
