/**
 * @modification $Date: 2006-09-19 14:35:19 $
 * @statut       unstable
 * @file         VfsFile.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.vfs;

import com.memoire.fu.FuLib;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

public abstract class VfsFile extends File {
  /*
   * public static final char separatorChar =File.separatorChar; public static final String separator =File.separator;
   * public static final char pathSeparatorChar=File.pathSeparatorChar; public static final String pathSeparator
   * =File.pathSeparator;
   */

  private static boolean ram_ = false;

  public static boolean isRamMode() {
    return ram_;
  }

  public static void setRamMode(boolean _ram) {
    ram_ = _ram;
    if (ram_) VfsFileRam.init();
  }

  public static final VfsFile createFile(URL _url) {
    VfsFile r = null;

    if (_url != null) {
      String p = _url.getProtocol();

      if ("file".equals(p)) r = createFile(FuLib.decodeWwwFormUrl(_url.getFile()));
      else if ("ram".equals(p)) r = new VfsFileRam(FuLib.decodeWwwFormUrl(_url.getFile()));
      else if ("ftp".equals(p)) r = new VfsFileFtp(_url);
      else if ("http".equals(p)) r = new VfsFileHttp(_url);
      else if ("tar".equals(p)) r = new VfsFileTar(_url);
      else if ("zip".equals(p)) r = new VfsFileZip(_url);
      else
        r = new VfsFileUrl(_url);
    }

    return r;
  }

  public static final VfsFile createFile(String _path) {
    VfsFile r = null;
    // URL u=null;

    try {
      r = createFile(new URL(_path));
    } catch (MalformedURLException ex) {
      if (isRamMode()) r = new VfsFileRam(_path);
      else
        r = new VfsFileFile(_path);
    }

    return r;
  }

  public static final VfsFile ensureVfsFile(Object _file) {

    if (_file == null) return null;
    VfsFile r = null;
    if (_file instanceof VfsFile) r = (VfsFile) _file;
    else if (_file instanceof File) r = createFile(((File) _file).getPath());
    else
      throw new ClassCastException(_file.toString());
    return r;
  }

  public static final VfsFile convertToVfsFile(Object _file) {

    if (_file == null) return null;
    VfsFile r = null;
    if (_file instanceof VfsFile) r = (VfsFile) _file;
    else if (_file instanceof File) r = createFile(((File) _file).getPath());
    else if (_file instanceof URL) r = createFile((URL) _file);
    else if (_file instanceof String) r = createFile(FuLib.expandedPath((String) _file));
    // else throw new ClassCastException(_file.toString());
    return r;
  }

  // Dynamic part

  protected VfsFile(File _file, String _name) {
    super(_file, _name);
  }

  protected VfsFile(String _path) {
    super(_path);
  }

  protected VfsFile(String _file, String _name) {
    super(_file, _name);
  }

  // Extensions

  public abstract VfsFile createChild(String _name);

  public abstract InputStream getInputStream() throws IOException;

  public abstract OutputStream getOutputStream() throws IOException;

  public abstract String getSeparator();

  private String text_ = null;
  private String name_ = null;

  public void build() {}

  public boolean isBuilt() {
    return true;
  }

  public String getViewText() {
    return (text_ != null) ? text_ : FuLib.reducedPath(getAbsolutePath());
  }

  public void setViewText(String _text) {
    text_ = _text;
  }

  public String getViewName() {
    return (name_ != null) ? name_ : getName();
  }

  public void setViewName(String _name) {
    name_ = _name;
  }

  @Override
  public final File getAbsoluteFile() {
    return getAbsoluteVfsFile();
  }

  @Override
  public final File getCanonicalFile() throws IOException {
    return getCanonicalVfsFile();
  }

  @Override
  public final File getParentFile() {
    return getParentVfsFile();
  }

  public VfsFile getAbsoluteVfsFile() {
    return createFile(getAbsolutePath());
  }

  public VfsFile getCanonicalVfsFile() throws IOException {
    return createFile(getCanonicalPath());
  }

  public VfsFile getParentVfsFile() {
    String path = getParent();
    return (path == null) ? null : createFile(path);
  }

  public final VfsFile[] listVfsFiles() {
    File[] o = listFiles();
    if (o == null) return new VfsFile[0];

    int l = o.length;
    VfsFile[] r = new VfsFile[l];
    System.arraycopy(o, 0, r, 0, l);
    return r;
  }

  public boolean isLink() {
    return false;
  }

  @Override
  public abstract URL toURL() throws MalformedURLException;

  public final URL toNullOrURL() {
    URL r = null;
    try {
      r = toURL();
    } catch (MalformedURLException ex) {}
    return r;
  }

  // Standard API 1.1

  // boolean canRead();
  // boolean canWrite();
  // boolean delete();
  // boolean equals(Object _o);
  // boolean exists();
  // String getAbsolutePath();
  // String getCanonicalPath() throws IOException;
  // String getName();
  // String getParent();
  // String getPath();
  // int hashCode();
  // boolean isAbsolute();
  // boolean isDirectory();
  // boolean isFile();
  // long lastModified();
  // long length();
  // String[] list();
  // boolean mkdir();
  // boolean mkdirs();
  // boolean renameTo(File _dest);
  // String toString();

  // Standard API 1.2

  // int compareTo(File pathname)
  // int compareTo(Object o)
  // boolean createNewFile();
  // void deleteOnExit();
  // boolean isHidden();
  // String[] list(FilenameFilter filter)
  // File[] listFiles();

  // File[] listFiles(FileFilter filter)
  // File[] listFiles(FilenameFilter filter)
  // boolean setLastModified(long _time);
  // boolean setReadOnly();
  // URI toURI()
}
