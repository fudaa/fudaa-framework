/**
 * @modification $Date: 2006-09-19 14:35:20 $
 * @statut       unstable
 * @file         VfsCache.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2004 Guillaume Desnoix
 */

package com.memoire.vfs;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public interface VfsCache
{
  int CHECK_AND_DOWNLOAD=0; // check and download if updated
  int CHECK_ONLY        =1; // only check the cache
  int DOWNLOAD_ONLY     =2; // download if nothing in cache

  File getFile(URL _url, int _mode) throws IOException;

  boolean isConnected();
}
