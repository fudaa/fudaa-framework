/**
 * @modification $Date: 2006-09-19 14:35:20 $
 * @statut       unstable
 * @file         VfsLib.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.vfs;

import com.memoire.fu.FuLog;

public class VfsLib
{
  private static VfsCache      cache_=null;
  private static VfsOperations opers_=null;

  public static final VfsCache getCache()
  {
    return cache_;
  }

  public static final synchronized void setCache(VfsCache _cache)
  {
    if(cache_!=null)
    {
      FuLog.error("VFS: cache already set");
      return;
    }
    cache_=_cache;
  }

  public static final VfsOperations getOperations()
  {
    return opers_;
  }

  public static final synchronized void setOperations(VfsOperations _opers)
  {
    if(opers_!=null)
    {
      FuLog.error("VFS: operations already set");
      return;
    }
    opers_=_opers;
  }
}
