/**
 * @modification $Date: 2006-09-19 14:35:18 $
 * @statut       unstable
 * @file         VfsFileFtp.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.vfs;

import com.memoire.fu.FuVectorString;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Remote ftp file.
 */
public class VfsFileFtp
  extends VfsFileUrl
{
  public VfsFileFtp(URL _a, String _n) throws MalformedURLException
  {
    this(new URL(_a,_n));
  }

  public VfsFileFtp(URL _url)
  {
    super(_url);
    if(!"ftp".equals(_url.getProtocol()))
      throw new IllegalArgumentException("Not a FTP url");
  }

  @Override
  protected FuVectorString readList(BufferedReader _nr)
    throws IOException
  {
    //FuLog.debug("VFS: VfsFileFtp.readList: "+url_);
    FuVectorString v=new FuVectorString(100);

    while(true)
    {
      String l=_nr.readLine();
      if(l==null) break;
      if(l.length()<53) continue;
      // Date: FuLog.debug(l.substring(43,56));
      char d=l.charAt(0);
      l=l.substring(53);
      int i=0;
      while((i<l.length())&&
	    !Character.isWhitespace(l.charAt(i))) i++;
      l=l.substring(i).trim();
      i=l.indexOf(" -> ");
      if(i>=0) l=l.substring(0,i).trim();
      if(d=='d') l+="/";
      //FuLog.debug("VFS: file '"+l+"'");
      v.addElement(l);
    }

    return v;
  }

  public static void main(String[] _args)
  {
    try
    {
      VfsFile f=new VfsFileFtp(new URL(_args[0]));
      System.out.println(f+"  exists="+f.exists());
      if(!f.isDirectory()) System.out.println("IN="+f.getInputStream());
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
