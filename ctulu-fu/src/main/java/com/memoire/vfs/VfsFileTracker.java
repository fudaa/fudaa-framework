/**
 * @modification $Date: 2006-09-19 14:35:18 $
 * @statut       unstable
 * @file         VfsFileTracker.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.vfs;

import com.memoire.fu.FuEmptyArrays;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuSoftCache;
import com.memoire.fu.FuVectorString;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.EventObject;
import java.util.Vector;

public final class VfsFileTracker
  implements Runnable
{
  static final class Entry
  {
    boolean  exists;
    long     checked;
    long     modified;
    //long     length;
    boolean  directory;
    String[] list;
  }

  public static final class Event
    extends EventObject
  {
    private String path_;

    public Event(Object _source, String _path)
    {
      super(_source);
      path_=_path;
    }

    public String getPath()
    {
      return path_;
    }
  }

  public interface Listener
    extends EventListener
  {
    void fileChanged(Event _evt);
  }

  protected Vector  listeners_;
  protected Thread  thread_;

  protected String  path_;
  protected long    since_;
  protected boolean recursive_;
  protected boolean follow_;
  protected long    freshness_;

  public VfsFileTracker(String _path,long _since,
                        boolean _recursive,boolean _follow,
                        long _freshness)
  {
    path_     =_path;
    since_    =_since;
    recursive_=_recursive;
    follow_   =_follow;
    freshness_=_freshness;
    listeners_=new Vector(1,1);
  }

  public synchronized void addFileChangeListener(Listener _l)
  {
    if(!listeners_.contains(_l))
    {
      listeners_.addElement(_l);

      if(listeners_.size()==1)
      {
        thread_=new Thread(this,"VfsFileTracker "+path_);
        thread_.setPriority(Thread.MIN_PRIORITY);
        thread_.start();
      }
    }
  }

  public synchronized void removeFileChangeListener(Listener _l)
  {
    listeners_.removeElement(_l);
    if(listeners_.size()==0) thread_=null;
  }

  protected void fireEvent(Event _evt)
  {
    Enumeration e=listeners_.elements();
    while(e.hasMoreElements())
      ((Listener)e.nextElement()).fileChanged(_evt);
  }

  @Override
  public void run()
  {
    while(thread_==Thread.currentThread())
    {
      long before=System.currentTimeMillis();
      String[] r=check(path_,since_,recursive_,follow_,freshness_,false);
      long after=System.currentTimeMillis();

      for(int i=0;i<r.length;i++)
        fireEvent(new Event(this,r[i]));

      if(freshness_>after-before+10L)
      {
	try { Thread.sleep(freshness_-(after-before)); }
	catch(InterruptedException ex) { }
      }

      since_=after;
    }
  }

  // static methods

  //private static final Hashtable CACHE=new Hashtable();
  //private static final Hashtable CANON=new Hashtable();
  private static final FuSoftCache CACHE=new FuSoftCache();
  private static final FuSoftCache CANON=new FuSoftCache();

  /*
  private static boolean checkFile(File _file, long _since)
  {
    return (_file!=null)&&_file.exists()&&(_file.lastModified()>_since);
  }
  */

  private static final void check(FuVectorString _r, String _path,
				  long _since, boolean _recursive,
                                  boolean _follow,
                                  long _freshness,boolean _verbose)
  {
    if(_path==null) return;
    String path=_path;

    // we keep canonical paths.
    // this can be a problem.
    Object canonical=CANON.get(path);
    if(Boolean.TRUE!=canonical)
    {
      /*GCJ*/
      try
      {
        //VfsFile file=VfsFile.createFile(_path);
        File file=new File(path);
        String cp=file.getCanonicalPath();
        if(!path.equals(cp))
        {
          CANON.put(path,Boolean.FALSE);
          if(!_follow) return;
          path=cp;
        }
        CANON.put(path,Boolean.TRUE);
        //check(_r,_path,_since,_recursive,_freshness,_verbose);
      }
      catch(IOException ex) { return; }
      /*GCJ*/
      //return;
    }

    Entry   entry  =(Entry)CACHE.get(path);
    boolean created=false;

    if((entry!=null)&&
       (entry.checked>=_freshness)&&
       !entry.exists)
      return;

    /*
    File file=new File(_path);
    if(!_file.exists())
    {
      if(entry==null)
      {
        entry=new Entry();
        entry.checked=System.currentTimeMillis();
        entry.exists =false;
        CACHE.put(_path,entry);
      }
      return;
    }
    */

    if(entry==null)
    {
      entry=new Entry();
      created=true;
    }

    if(created||(entry.checked<_freshness))
    {
      //VfsFile file=VfsFile.createFile(_path);
      File file=new File(path);
      //System.err.println("accessing file "+file);
      entry.exists=file.exists();
      if(entry.exists)
      {
        entry.modified =file.lastModified();
       // entry.length   =file.length();
        entry.directory=file.isDirectory();
        entry.list=(entry.directory ? file.list() : null);
      }
      entry.checked=System.currentTimeMillis();
    }

    if(created) CACHE.put(path,entry);

    if(_r.contains(path)) return;

    //if(checkFile(_file,_since)) _r.addElement(_file);
    if(entry.modified>=_since) _r.addElement(path);

    if(entry.directory&&_recursive)
    {
      if(_verbose)
      {
        String p=path;
        while(p.length()>60)
        {
          int q=p.indexOf('/');
          if(q<0) break;
          p="..."+p.substring(q+1);
        }
        p=FuLib.rightpad(p,60,' ');
        System.err.print
          ("\rChecking "+p+
           FuLib.leftpad(" ("+_r.size()+")",18,' ')+"\033[K\r");
        System.err.flush();
      }

      /*
      File[] files=_file.listFiles();
      if(files!=null)
	for(int i=0;i<files.length;i++)
	  check(_r,files[i],_since,_recursive);
      */
      //String[] files=_file.list();
      if(entry.list!=null)
	for(int i=0;i<entry.list.length;i++)
        {
          //new File(_file,files[i])
          // local file only
	  check(_r,path+
                (path.endsWith(File.separator) ? "" : File.separator)+
                entry.list[i],
                _since,_recursive,_follow,_freshness,_verbose);
        }
    }
  }

  public static String[] check(String _path,long _since,
                               boolean _recursive,boolean _follow,
                               long _freshness,boolean _verbose)
  {
    FuVectorString v=new FuVectorString(220);
    check(v,_path,_since,_recursive,_follow,_freshness,_verbose);

    int l=v.size();
    if(l==0) return FuEmptyArrays.STRING0;

    String[] r=new String[l];
    for(int i=0;i<l;i++) r[i]=v.elementAt(i);
    return r;
  }

 /* public static void main(String[] _args)
  {
    if(_args.length==0) _args=new String[] { "/tmp" };

    Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
    long daemon=0L;

    do
    {
      long    before   =System.currentTimeMillis();
      boolean recursive=false;
      boolean follow   =false;
      long    delay    =86400000L;
      boolean verbose  =false;

      for(int i=0;i<_args.length;i++)
      {
        if("-f".equals(_args[i]))
        {
	  follow=true;
        }
        else
        if("-r".equals(_args[i]))
        {
	  recursive=true;
        }
        else
        if("-v".equals(_args[i]))
        {
	  verbose=true;
        }
        else
	if("-d".equals(_args[i]))
        {
	  i++;
	  daemon=Long.parseLong(_args[i]);
	}
	else
	if("-t".equals(_args[i]))
	{
	  i++;
	  delay=Long.parseLong(_args[i]);
	}
        else
        {
          String[] f=check(_args[i],before-delay,recursive,
                           follow,before-daemon,verbose);
	  if(daemon>0L)
	    System.out.println
	      ("\033[0;0H\033[2J["+
               f.length+
	       " new files found in "+_args[i]+"]");
	  for(int j=0;j<f.length;j++)
	    System.out.println(f[j]);
	}
      }

      long after=System.currentTimeMillis();
      //System.err.println("step in "+(after-before)+"ms");
      if(daemon>after-before+10L)
      {
	try { Thread.sleep(daemon-(after-before)); }
	catch(InterruptedException ex) { }
      }
    } while(daemon>0L);
  }*/
}
