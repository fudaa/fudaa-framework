/**
 * @modification $Date: 2006-09-19 14:35:19 $
 * @statut       unstable
 * @file         VfsFileHttp.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package com.memoire.vfs;

import com.memoire.fu.FuVectorString;
import com.memoire.xml.XmlNode;
import com.memoire.xml.XmlParser;
import com.memoire.xml.XmlReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Remote http file.
 */
public class VfsFileHttp
  extends    VfsFileUrl
{
  public VfsFileHttp(URL _a, String _n) throws MalformedURLException
  {
    this(new URL(_a,_n));
  }

  public VfsFileHttp(URL _url)
  {
    super(_url);
    if(!"http".equals(_url.getProtocol()))
      throw new IllegalArgumentException("Not a HTTP url");
  }

  @Override
  protected FuVectorString readList(BufferedReader _nr)
    throws IOException
  {
    FuVectorString v=new FuVectorString(50);

    v.addElement("[content].html");

    XmlParser p=new XmlParser(_nr,url_.toString(),XmlParser.HTML);
    XmlReader r=new XmlReader(XmlParser.HTML);
    p.setXmlListener(r);
    p.parse();
    //System.out.println(r.getTree());
    String    base =""+url_;
    XmlNode[] nodes;

    nodes=r.getTree().findNodes("A");
    for(int i=0;i<nodes.length;i++)
    {
      String href=nodes[i].getAttribute("HREF");
      if(href!=null)
      {
	try
	{
	  String l=""+new URL(url_,href);
	  if(l.startsWith(base))
	  {
	    l=l.substring(base.length());
	    int k;
	    k=l.indexOf("?");
	    if(k>=0) l=l.substring(0,k);
	    k=l.indexOf("#");
	    if(k>=0) l=l.substring(0,k);
	    k=l.indexOf("/");
	    if((k==-1)||(k==l.length()-1))
		if(!"".equals(l)&&!v.contains(l))
		  v.addElement(l);
	  }
	}
	catch(Exception ex) { }
      }
    }

    nodes=r.getTree().findNodes("LINK");
    for(int i=0;i<nodes.length;i++)
    {
      String href=nodes[i].getAttribute("HREF");
      if(href!=null)
      {
	try
	{
	  String l=""+new URL(url_,href);
	  if(l.startsWith(base))
	  {
	    l=l.substring(base.length());
	    int k;
	    k=l.indexOf("?");
	    if(k>=0) l=l.substring(0,k);
	    k=l.indexOf("#");
	    if(k>=0) l=l.substring(0,k);
	    k=l.indexOf("/");
	    if((k==-1)||(k==l.length()-1))
		if(!"".equals(l)&&!v.contains(l))
		  v.addElement(l);
	  }
	}
	catch(Exception ex) { }
      }
    }

    nodes=r.getTree().findNodes("IMG");
    for(int i=0;i<nodes.length;i++)
    {
      String href=nodes[i].getAttribute("SRC");
      if(href!=null)
      {
	try
	{
	  String l=""+new URL(url_,href);
	  if(l.startsWith(base))
	  {
	    l=l.substring(base.length());
	    int k;
	    k=l.indexOf("?");
	    if(k>=0) l=l.substring(0,k);
	    k=l.indexOf("#");
	    if(k>=0) l=l.substring(0,k);
	    k=l.indexOf("/");
	    if((k==-1)||(k==l.length()-1))
		if(!"".equals(l)&&!v.contains(l))
		  v.addElement(l);
	  }
	}
	catch(Exception ex) { }
      }
    }

    nodes=r.getTree().findNodes("FRAME");
    for(int i=0;i<nodes.length;i++)
    {
      String href=nodes[i].getAttribute("SRC");
      if(href!=null)
      {
	try
	{
	  String l=""+new URL(url_,href);
	  if(l.startsWith(base))
	  {
	    l=l.substring(base.length());
	    int k;
	    k=l.indexOf("?");
	    if(k>=0) l=l.substring(0,k);
	    k=l.indexOf("#");
	    if(k>=0) l=l.substring(0,k);
	    k=l.indexOf("/");
	    if((k==-1)||(k==l.length()-1))
		if(!"".equals(l)&&!v.contains(l))
		  v.addElement(l);
	  }
	}
	catch(Exception ex) { }
      }
    }

    return v;
  }

  @Override
  public URL getContentURL()
  {
    if(getPath().endsWith("[content].html"))
    {
      try { return new URL(getParent()); }
      catch(MalformedURLException ex) { }
    }
    
    return url_;
  }

  // GET POST HEAD OPTIONS PUT DELETE TRACE

  @Override
  public final boolean mkdir()
  {
    return true;

    /*
    boolean r=false;
    try
    {
      String p=url_.toString();
      if(!p.endsWith("/")) p+="/";
      URL u=new URL(p);

      HttpURLConnection cnx=new HttpURLConnection(u);
      cnx.setRequestMethode("PUT");
      cnx.connect();
      int    c=cnx.getResponseCode();
      String m=cnx.getResponseMessage();
      System.err.println("HTTP: "+c+" "+m);
      cnx.disconect();
    }
    catch(Exception ex) { }
    return r;
    */
  }
}
