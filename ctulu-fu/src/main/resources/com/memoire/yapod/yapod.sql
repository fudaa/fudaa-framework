-- Yapod SQL DB mapping

-- root  --------------------------------------------------------------

USE mysql;

CREATE DATABASE yapod;

INSERT INTO db VALUES (
  '%', 'yapod', 'boony','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y');

INSERT INTO user VALUES (
  '%', 'boony', PASSWORD('doebfu'), 'N', 'N' ,'N','N','N','N','N','N','N','Y','N','N','N','N');

FLUSH PRIVILEGES;

-- boony --------------------------------------------------------------

USE yapod;

CREATE TABLE object (
  oid VARCHAR(24) NOT NULL,   # oid
  type TEXT NOT NULL,         # type de objet ou du tableau
  depth INT,                  # >0  si tableau
  length INT,                 # >=0 si tableau
  PRIMARY KEY(oid));

CREATE TABLE field (
  name TEXT NOT NULL,         # nom du champ ou indice du tableau
  owner VARCHAR(24) NOT NULL, # objet ou tableau propriétaire
  reference VARCHAR(24),      #    =objet
  type TEXT,                  # OU =type simple:  type
  value TEXT,                 #                   valeur
  KEY(reference), KEY(owner));


-- Exemple d'application

-- Table object
INSERT INTO object VALUES (
  'RO-1','com.memoire.yapod.YapodTestC3','\N','\N');
INSERT INTO object VALUES (
  'RO-3','com.memoire.yapod.YapodTestC1','\N','\N');
INSERT INTO object VALUES (
  'RO-2','com.memoire.yapod.YapodTestC2','\N','\N');
INSERT INTO object VALUES (
  'RT-4','double','1','3');

-- Table field
INSERT INTO field VALUES (
  'b','RO-1','\N','java.lang.Integer','70');
INSERT INTO field VALUES (
  'a','RO-1','\N','java.lang.Integer','19');
INSERT INTO field VALUES (
  'a','RO-3','\N','java.lang.String','b&#9;&lt;1&gt;&#39;_&#39;&#32;&quot;&#32;o&#39321;');
INSERT INTO field VALUES (
  'b','RO-3','\N','java.lang.Integer','12');
INSERT INTO field VALUES (
  'c','RO-3','\N','java.lang.Boolean','t');
INSERT INTO field VALUES (
  'd','RO-3','\N','java.lang.Character','10');
INSERT INTO field VALUES (
  'e','RO-3','\N','\N','\N');
INSERT INTO field VALUES (
  'f','RO-3','\N','java.lang.Character','4660');
INSERT INTO field VALUES (
  'h','RO-3','RT-4','\N','\N');
INSERT INTO field VALUES (
  'h','RO-2','RO-3','\N','\N');
INSERT INTO field VALUES (
  '0','RT-4','\N','\N','101.0');
INSERT INTO field VALUES (
  '1','RT-4','\N','\N','102.0');
INSERT INTO field VALUES (
  '2','RT-4','\N','\N','103.0');

/*
# Dump de la base
# (boony doit avoir le File_priv dans mysql.user)

date=`date`
rm -rf /tmp/yapod-$LOGNAME
mkdir /tmp/yapod-$LOGNAME
chmod 777 /tmp/yapod-$LOGNAME
mysqldump -u boony -pdoebfu -t yapod -T /tmp/yapod-$LOGNAME
echo "# Yapod database dump : $date" > /tmp/yapod.txt
for i in /tmp/yapod-$LOGNAME/*
do
  echo >> /tmp/yapod.txt
  echo $i >> /tmp/yapod.txt
  cat $i >> /tmp/yapod.txt
done
rm -rf /tmp/yapod-$LOGNAME
*/
