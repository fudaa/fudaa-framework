/*
 * @creation 13 avr. 2006
 * @modification $Date: 2006-10-26 07:31:04 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.common;

import com.memoire.yapod.YapodXmlDeserializer;
import com.memoire.yapod.YapodXmlSerializer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.boony.BoonyXmlDeserializer;
import org.fudaa.dodico.boony.BoonyXmlSerializer;

/**
 * @author fred deniger
 * @version $Id: TestBoony.java,v 1.4 2006-10-26 07:31:04 deniger Exp $
 */
public final class TestBoony {

  private TestBoony() {}

  /**
   * compare yapod et boony.
   * 
   * @param _args rien
   */
  public static void main(String[] _args) {
    try {
      final int nb = 300;
      double[] tabDouble = new double[nb];
      byte[] tabBytes = new byte[nb];

      final java.util.List l = new Vector(3);
      for (int i = 0; i < nb; i++) {
        tabDouble[i] = i;
        tabBytes[i] = (byte) i;
      }
      l.add(tabDouble);
      l.add(tabBytes);
      l.add("������");
      final File f = File.createTempFile("toto", ".txt");
      int iteration = 5;
      long[] yapodr = new long[2];
      long[] boonyr = new long[2];
      for (int i = iteration; i > 0; i--) {
        long[] r = doYapod(f, l);
        yapodr[0] += r[0];
        yapodr[1] += r[1];
        r = doBoony(f, l);
        boonyr[0] += r[0];
        boonyr[1] += r[1];
      }
      System.out.println("YAPOD");
      String ms = " ms";
      System.out.println("  ecriture " + (yapodr[0] / iteration) + ms);
      System.out.println("  lecture  " + (yapodr[1] / iteration) + ms);
      System.out.println("BOONY");
      System.out.println("  ecriture " + (boonyr[0] / iteration) + ms);
      System.out.println("  lecture  " + (boonyr[1] / iteration) + ms);
      f.delete();

    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  /**
   * @param _f le fichier de dest
   * @param _l le vecteur a serialiser
   * @return les temps
   * @throws Exception io,....
   */
  public static long[] doBoony(File _f, List _l) throws Exception {
    long[] r = new long[2];
    long l1;
    BoonyXmlSerializer sb = new BoonyXmlSerializer(false);
    sb.open(new FileOutputStream(_f));
    l1 = System.currentTimeMillis();
    sb.write(_l);
    r[0] = (System.currentTimeMillis() - l1);
    sb.close();
    BoonyXmlDeserializer dsb = new BoonyXmlDeserializer(false);
    dsb.open(new FileInputStream(_f));
    l1 = System.currentTimeMillis();
    List o2 = (List) dsb.read();
    r[1] = (System.currentTimeMillis() - l1);
    dsb.close();
    if (!compare(_l, o2)) new Throwable().printStackTrace();
    return r;
  }

  /**
   * @param _f le fichier de dest
   * @param _l le vecteur a serialiser
   * @return les temps
   * @throws Exception io,....
   */
  public static long[] doYapod(File _f, List _l) throws Exception {
    long[] r = new long[2];
    YapodXmlSerializer s = new YapodXmlSerializer();
    s.open(new FileOutputStream(_f));
    long l1 = System.currentTimeMillis();
    s.write(_l);
    r[0] = (System.currentTimeMillis() - l1);
    s.close();
    YapodXmlDeserializer ds = new YapodXmlDeserializer();
    ds.open(new FileInputStream(_f));
    l1 = System.currentTimeMillis();
    /*List o = (List)*/ ds.read();
    r[1] = (System.currentTimeMillis() - l1);
    ds.close();
    //if (!compare(_l, o)) new Throwable().printStackTrace();
    return r;
  }

  /**
   * @param _l1 la premiere liste a comparer
   * @param _l2 la deuxieme
   * @return true si les 2 vecteur sont equivalent
   */
  public static boolean compare(List _l1, List _l2) {
    if (_l1.size() != _l2.size()) return false;
    for (int i = _l1.size() - 1; i >= 0; i--) {
      if (!_l1.get(i).equals(_l2.get(i))) {
        Object li = _l1.get(i);
        Object li2 = _l2.get(i);
        if (li instanceof Double[] && li2 instanceof Double[]) {
          return Arrays.equals((Double[]) li, (Double[]) li2);
        } else if (li instanceof Byte[] && li2 instanceof Byte[]) {
          return Arrays.equals((Byte[]) li, (Byte[]) li2);
        }
        if (li.getClass().isArray() && li2.getClass().isArray()) {
          return CtuluLibArray.isEquals(li, li2);
        }
        return false;
      }
    }
    return true;
  }

}
