/**
 * @creation 2002-06-28
 * @modification $Date: 2006-04-07 09:23:16 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.common;

import java.io.File;
import java.io.FileReader;
import org.fudaa.dodico.fortran.FortranReader;
/**
 * @version $Id: TestFortranReader.java,v 1.2 2006-04-07 09:23:16 deniger Exp $
 * @author Fred Deniger
 */
public final class TestFortranReader {

  private TestFortranReader() {

  }

  /**
   * @param args non utilise.
   */
  public static void main(String[] args){
    try {
      String s = TestFortranReader.class.getResource("fichierTest.txt").getFile();
      System.out.println("fichier " + s);
      FortranReader f = new FortranReader(new FileReader(new File(s)));
      System.out.println("fichier trouve");
      int NB = 7;
      for (int i = 0; i < NB; i++) {
        f.readFieldsOld();
        for (int j = 0; j < f.getNumberOfFields(); j++)
          System.out.print("\"" + f.stringField(j) + "\"" + " ");
        System.out.println("");
      }
      System.out.println("nouvelle Version");
      f = new FortranReader(new FileReader(new File(s)));
      for (int i = 0; i < NB; i++) {
        f.readFields();
        for (int j = 0; j < f.getNumberOfFields(); j++)
          System.out.print("\"" + f.stringField(j) + "\"" + " ");
        System.out.println("");
      }
    }
    catch (Exception e) {
      System.out.println(e);
    }
  }
}