/*
 * @file FortranBinaryTest.java @creation 29 ao�t 2003 @modification $Date: 2005-09-09 11:32:19 $
 * @license GNU General Public License 2 @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231
 * Compiegne @mail devel@fudaa.org
 */
package org.fudaa.dodico.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import junit.framework.TestCase;
import org.fudaa.dodico.fortran.FortranBinaryInputStream;
import org.fudaa.dodico.fortran.FortranBinaryOutputStream;
/**
 * @author deniger
 * @version $Id: TestJFortranBinary.java,v 1.1 2005-09-09 11:32:19 deniger Exp $
 */
public class TestJFortranBinary extends TestCase {

  /**
   * Constructeur par defaut
   */
  public TestJFortranBinary() {
    super();
  }

  /**
   * Test ecriture puis lecture dans un fichier temporaire
   */
  public void testEcritureLecture(){
    int nbValeurs = 10;
    //ces 11 valeurs seront repet�s nbTemps fois.
    int nbTemps = 20;
    //un acces direct avec un enregistrement assez long
    //on laisse 10 de rabe:
    int lgRecord = nbValeurs *4;
    FortranBinaryOutputStream out = null;
    File tempFile = null;
    try {
      tempFile = File.createTempFile("fudaa", "bin");
      out = new FortranBinaryOutputStream(new FileOutputStream(tempFile), false);
      //on suppose qu'il y 11 valeurs mis sur un enregistrement double.
      //on prend des lignes grandes pour tester.
      //*2 aurait suffit.
      out.setRecordLength(lgRecord);
      //premiere ligne le nb de temps.
      out.writeInteger(nbTemps + 200);
      out.writeRecord();
      for (int i = 0; i < nbTemps; i++) {
        for (int j = 0; j < nbValeurs; j++) {
          out.writeDoublePrecision(i + j + 1d);
        }
        out.writeRecord();
      }
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    finally {
      try {
        if (out != null)
          out.close();
      }
      catch (IOException e1) {
        e1.printStackTrace();
      }
    }
    assertNotNull(tempFile);
    FortranBinaryInputStream in = null;
    try {
      in = new FortranBinaryInputStream(new FileInputStream(tempFile), false);
      in.setRecordLength(lgRecord);
      in.readRecord();
      assertEquals(nbTemps + 200, in.readInteger());
      for (int i = 0; i < nbTemps; i++) {
        in.readRecord();
        for (int j = 0; j < nbValeurs; j++) {
          assertEquals(i + j + 1d, in.readDoublePrecision(), 1E-15d);
        }
      }
    }
    catch (IOException e1) {
      e1.printStackTrace();
    }
    finally {
      try {
        if (in != null)
          in.close();
      }
      catch (IOException e2) {
        e2.printStackTrace();
      }
    }
  }
}