/**
 * @creation 31 mars 2003
 * @modification $Date: 2007-01-19 13:07:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.common;

import com.memoire.fu.FuLib;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluLibFile;

/**
 * @author deniger
 * @version $Id: TestIO.java,v 1.6 2007-01-19 13:07:19 deniger Exp $
 */
public abstract class TestIO extends TestCase {

  protected double eps_;
  protected File fic_;
  protected File tempDir;

  @Override
  protected void setUp() throws Exception {
    tempDir = CtuluLibFile.createTempDir();
  }

  @Override
  protected void tearDown() throws Exception {
    CtuluLibFile.deleteDir(tempDir);
  }

  /**
   * @param _fic le nom du fichier a charger
   */
  public TestIO(String _fic) {
    eps_ = 1E-15;
    if (_fic != null) {
      fic_ = getFile(_fic);
      assertNotNull(fic_);
      assertTrue(fic_.exists());
    }
  }

  /**
   * @return un fichier temporaire
   */
  public File createTempFile() {
    File f = null;
    try {
      f = File.createTempFile("fudaa", ".test", tempDir);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return f;
  }

  public File createTempFile(String _ext) {
    File f = null;
    try {
      f = File.createTempFile("fudaa", _ext, tempDir);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return f;
  }

  /**
   * Compare 2 doubles avec une precision eps_ ( variable protegee 1e-15).
   *
   * @param _v1 le premier double a comparer
   * @param _v2 le deuxieme double a comparer
   */
  public void assertDoubleEquals(double _v1, double _v2) {
    assertEquals(_v1, _v2, eps_);
  }
  public void assertDoubleEquals(String message,double _v1, double _v2) {
    assertEquals(message,_v1, _v2, eps_);
  }

  /**
   * Permet de trouver une ressource presente dans le rep de la classe donnee.
   *
   * @param _f le nom du fichier a trouver
   * @return le fichier
   */
  protected final File getFile(String _f) {
    return getFile(getClass(), _f);
  }

  /**
   * Permet de trouver une ressource presente dans le rep de la classe donnee.
   *
   * @param _c la classe a considerer
   * @param _f le nom du fichier a trouver
   * @return le fichier
   */
  public static final File getFile(Class _c, String _f) {
    URL url = _c.getResource(_f);
    if (url == null) {
      ClassLoader.getSystemClassLoader().getResource(_f);
    }
    if (url == null) {
      return null;
    }
    return new File(FuLib.decodeWwwFormUrl(url.getPath()));
  }
}
