/*
 GPL 2
 */
package org.fudaa.dodico.fortran;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import org.fudaa.dodico.common.TestIO;

/**
 *
 * @author Frederic Deniger
 */
public class DodicoIntegerArrayBinaryFileSaverTest extends TestIO {

  public DodicoIntegerArrayBinaryFileSaverTest() {
    super(null);
  }

  public void testReadWriteIntArrayArray() {
    int[][] values = new int[10][];
    int k = 5;
    for (int i = 0; i < values.length; i++) {
      values[i] = new int[k++];
      for (int j = 0; j < values[i].length; j++) {
        values[i][j] = i * j;
      }
    }
    File tempFile = createTempFile();
    DodicoArraysBinaryFileSaver saver = new DodicoArraysBinaryFileSaver();
    try {
      OutputStream out = new FileOutputStream(tempFile);
      saver.save(out, values);
      InputStream in = new FileInputStream(tempFile);
      int[][] load = saver.load(in);
      assertEquals(values.length, load.length);
      for (int i = 0; i < values.length; i++) {
        assertEquals(values[i].length, load[i].length);
        for (int j = 0; j < values[i].length; j++) {
          assertEquals(values[i][j], load[i][j]);
        }
      }
    } catch (Exception fileNotFoundException) {
      fail(fileNotFoundException.getMessage());
    }
  }

  public void testReadWriteIntArray() {
    int[] values = new int[10];
    for (int i = 0; i < values.length; i++) {
      values[i] = i * 2;
    }
    File tempFile = createTempFile();
    DodicoArraysBinaryFileSaver saver = new DodicoArraysBinaryFileSaver();
    saver.saveSimpleArray(tempFile, values);
    int[] load = saver.loadSimpleArray(tempFile);
    assertEquals(values.length, load.length);
    for (int i = 0; i < values.length; i++) {
      assertEquals(values[i], load[i]);
    }
  }

  public void testReadWriteBooleanArray() {
    boolean[] values = new boolean[11];
    for (int i = 0; i < values.length; i++) {
      values[i] = i % 2 == 0;
    }
    File tempFile = createTempFile();
    DodicoArraysBinaryFileSaver saver = new DodicoArraysBinaryFileSaver();
    saver.saveBoolean(tempFile, values);
    boolean[] load = saver.loadBoolean(tempFile);
    assertEquals(values.length, load.length);
    for (int i = 0; i < values.length; i++) {
      assertEquals(values[i], load[i]);
    }
  }

  public void testReadWriteDoubleArray() {
    double[] values = new double[11];
    for (int i = 0; i < values.length; i++) {
      values[i] = Math.random() * 100;
    }
    File tempFile = createTempFile();
    DodicoArraysBinaryFileSaver saver = new DodicoArraysBinaryFileSaver();
    saver.saveDouble(tempFile, values);
    double[] load = saver.loadDouble(tempFile);
    assertEquals(values.length, load.length);
    for (int i = 0; i < values.length; i++) {
      assertEquals(values[i], load[i], 1e-5);
    }
  }
}
