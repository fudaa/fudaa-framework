package org.fudaa.dodico.fortran;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluLibArray;

public class DodicoDoubleArrayBinaryFileSaverTest extends TestCase {


  public void testWriteRead() {
    File des = null;
    try {
      des = File.createTempFile("test", ".bin");
      DodicoDoubleArrayBinaryFileSaver saver = new DodicoDoubleArrayBinaryFileSaver(des);
      Random random = new Random();
      Map<String, double[]> mapOfValues = generateMaps(random);
      boolean saved = saver.save(mapOfValues);
      assertTrue(saved);
      Map<String, double[]> loaded = saver.load();
      assertEquals(loaded.size(), mapOfValues.size());
      for (Entry<String, double[]> entry : loaded.entrySet()) {
        double[] ds = mapOfValues.get(entry.getKey());
        assertNotNull(ds);
        assertTrue(CtuluLibArray.equals(ds, entry.getValue(), 1e-15));
      }

    } catch (Exception e) {
      fail(e.getMessage());
    } finally {
      if (des != null) {
        des.delete();
      }
    }
  }

  private static Map<String, double[]> generateMaps(Random random) {
    Map<String, double[]> mapOfValues = new HashMap<String, double[]>();
    for (int i = 10; i < 20; i++) {
      String key = Integer.toString(i);
      double[] values = new double[i];
      for (int j = 0; j < values.length; j++) {
        values[j] = random.nextDouble() * 100;
      }
      values[values.length - 1] = 0;
      mapOfValues.put(key, values);
    }
    return mapOfValues;
  }

  public static void main(String[] args) {
    File f = new File(
            "/home/genesis/Devel/test-Fudaa-Prepro/edf/perf/casEdfRElectureVariable/export_partiel_cas180210_p10.res.1.2RC1.POST/Vue_2D_N_4.LAY/GRAPHES/Graphe_0_profil_spatial3136/COURBES/3.bin");
    DodicoDoubleArrayBinaryFileSaver saver = new DodicoDoubleArrayBinaryFileSaver(f);
    Map<String, double[]> load = saver.load();
    System.err.println("load " + load.size());
  }
}