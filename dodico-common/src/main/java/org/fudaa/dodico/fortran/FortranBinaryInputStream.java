/*
 * @file         FortranBinaryInputStream.java
 * @creation     1998-08-19
 * @modification $Date: 2006-09-19 14:42:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.fortran;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteOrder;
import org.fudaa.ctulu.fileformat.FortranInterface;
import org.fudaa.dodico.fichiers.NativeBinaryInputStream;
import org.fudaa.dodico.fichiers.NativeBinarySystem;

/**
 * Une classe facilitant la lecture de fichiers binaires �crits par Fortran Utilise la classe NativeBinaryInputStream
 * qui est deja "bufferisee". L'�quivalence d'intructions entre Java et Fortran se fera de la mani�re suivante :<BR>
 * (en consid�rant i=integer/int, f=real/float, d=double precision/double et s=character*()/String)<BR>
 * <br>
 * <b>1) Pour un fichier � acces s�quentiel :</b><BR>
 * <BR>
 * <b>Fortran</b><BR>
 * <i> open (unit=10,file='fichier.bin',access='sequentiel',form='unformatted')<BR>
 * read (unit=10)<BR>
 * ...<BR>
 * close (unit=10)<BR>
 * </i><BR>
 * <BR>
 * <b>Java</b><BR>
 * <i> FortranBinaryInputStream in=<BR>
 * new FortranBinaryInputStream(new FileInputStream("fichier.bin"),true);<BR>
 * in.readRecord();<BR>
 * i=in.readInteger();<BR>
 * f=in.readReal();<BR>
 * d=in.readDoublePrecision();<BR>
 * s=in.readCharacter(s.length());<BR>
 * ...<BR>
 * in.close();<BR>
 * </i> <b>2) Pour un fichier � acces direct :</b><br/> <b>Fortran</b><br/> <i> open
 * (unit=10,file='fichier.bin',access='direct',recl=30,form='unformatted')<br/> read (unit=10,rec=1) <br/> ...<br/>
 * close (unit=10)<br/> </i> <br>
 * <b>Java</b><br>
 * FortranBinaryInputStream in=<br>
 * new FortranBinaryInputStream(new FileInputStream("fichier.bin"),false);<br>
 * in.setRecordLength(30);<br>
 * in.readRecord();<br>
 * i=in.readInteger();<br>
 * f=in.readReal();<br>
 * d=in.readDoublePrecision();<br>
 * s=in.readCharacter(s.length());<br>
 * ...<br>
 * in.close();<br>
 * Dans le cas acces sequentiel, un compteur permet de se positionner dans le flux.
 *
 * @version $Id: FortranBinaryInputStream.java,v 1.18 2006-09-19 14:42:29 deniger Exp $
 * @author Bertrand Marchand
 */
public class FortranBinaryInputStream extends NativeBinaryInputStream implements FortranInterface {
  // private NativeBinaryInputStream bufStream_;
  // private ByteArrayInputStream arrayStream_;
  private boolean sequential_;
  private int recordLength_;
  private long currentPos_;
  private long nextPos_;

  @Override
  public long skip(final long _l) throws IOException {
    currentPos_ += _l;
    return super.skip(_l);
  }

  @Override
  public void skipBytes(final int _l) throws IOException {
    currentPos_ += _l;
    super.skipBytes(_l);
  }

  /**
   * Cr�ation en pr�cisant si le fichier binaire est � access s�quentiel ou non. Si sequentiel, alors certaines donnees (
   * generees par la meme instruction write de fortran) sont entourees par 4 octets qui correspondent a la longueur de
   * l'enregistrement. La variable systeme ("os.arch") permet de d�terminer l'architecture a utiliser.
   *
   * @param _in InputStream (inutile d'utiliser un buffer)
   * @param _sequential <B>true</B> le fichier est binaire � acc�s <I>Sequential</I>. <B>false</B> le fichier est
   *          binaire � acc�s <I>Direct</I>
   * @throws IOException
   */
  public FortranBinaryInputStream(final InputStream _in, final boolean _sequential) throws IOException {
    this(_in, _sequential, NativeBinarySystem.getNativeByteOrder());
  }

  /**
   * Il est possible de definir l'architecture intel ( little indian) ou sparc (big indian).
   *
   * @param _in InputStream.(inutile d'utiliser un buffer)
   * @param _sequential <B>true</B> le fichier est binaire � acc�s <I>Sequential</I>. <B>false</B> le fichier est
   *          binaire � acc�s <I>Direct</I>
   * @param _architectureID "sparc" pour le big endian et "X86" pour le little endian. Utiliser les identifiant de
   *          NativeBinaryInputStream
   * @see org.fudaa.dodico.fichiers.NativeBinaryInputStream
   * @throws IOException
   */
  public FortranBinaryInputStream(final InputStream _in, final boolean _sequential, final ByteOrder _byteOrder) throws IOException {
    super(_in, _byteOrder);
    sequential_ = _sequential;
    recordLength_ = 0;
    nextPos_ = 0;
    currentPos_ = nextPos_;
  }

  /**
   * ATTENTION Different du cas acces direct. see #getRecordLength()
   *
   * @return la taille de l'enregistrement lu par le flux.
   */
  public int getSequentialRecordLength() {
    return recordLength_;
  }

  /**
   * Affectation de la longueur des enregistrements (pour les fichiers � acc�s <I>Direct</I>.
   *
   * @param _length Longueur d'enregistrement en longworld (1 longworld=4 octets)
   */
  public void setRecordLength(final int _length) {
    recordLength_ = _length * 4;
  }

  /**
   * Retourne la longueur des enregistrements (pour les fichiers � acc�s <I>Direct</I>.
   *
   * @return Longueur d'enregistrement en longworld (1 longworld=4 octets)
   */
  public int getRecordLength() {
    return recordLength_ / 4;
  }

  /**
   * Lecture d'un champ chaine de caract�res "<I>character</I>" Fortran.
   *
   * @param _lgString Longueur de la chaine � lire
   * @return String correspondant � la chaine de caract�res
   */
  @Override
  public String readCharacter(final int _lgString) throws IOException {
    currentPos_ += _lgString;
    return super.readCharacter(_lgString);
  }

  /**
   * Lecture d'un champ entier "<I>integer</I>" Fortran.
   *
   * @return int correspondant au integer
   * @throws IOException
   */
  public int readInteger() throws IOException {
    currentPos_ += 4;
    return super.readInt32();
  }

  /**
   * Lecture d'un champ r�el "<I>real</I>" Fortran.
   *
   * @return float correspondant au real
   * @throws IOException
   */
  public float readReal() throws IOException {
    currentPos_ += 4;
    return super.readFloat32();
  }

  /**
   * Lecture d'un champ r�el en double pr�cision "<I>double precision</I>" soit 8 octets.
   *
   * @return double correspondant au double precision
   * @throws IOException
   */
  public double readDoublePrecision() throws IOException {
    currentPos_ += 8;
    return super.readFloat64();
  }

  /**
   * Redefinition de la methode (changement: la position courante est geree).
   */
  @Override
  public byte readInt8() throws IOException {
    currentPos_ += 1;
    return super.readInt8();
  }

  /**
   * Redefinition de la methode (changement: la position courante est geree).
   */
  @Override
  public short readUInt8() throws IOException {
    currentPos_ += 1;
    return super.readUInt8();
  }

  /**
   * Redefinition de la methode (changement: la position courante est geree).
   */
  @Override
  public short readInt16() throws IOException {
    currentPos_ += 2;
    return super.readInt16();
  }

  /**
   * Redefinition de la methode (changement: la position courante est geree).
   */
  @Override
  public int readUInt16() throws IOException {
    currentPos_ += 2;
    return super.readUInt16();
  }

  /**
   * Redefinition de la methode (changement: la position courante est geree).
   */
  @Override
  public int readInt32() throws IOException {
    currentPos_ += 4;
    return super.readInt32();
  }

  /**
   * Redefinition de la methode (changement: la position courante est geree).
   */
  @Override
  public long readUInt32() throws IOException {
    currentPos_ += 4;
    return super.readUInt32();
  }

  @Override
  public long readInt64() throws IOException {
    currentPos_ += 8;
    return super.readInt64();
  }

  @Override
  public float readFloat32() throws IOException {
    currentPos_ += 4;
    return super.readFloat32();
  }

  @Override
  public double readFloat64() throws IOException {
    currentPos_ += 8;
    return super.readFloat64();
  }

  /**
   * @return en nb d'octets la position courante.
   */
  public long positionCourante() {
    return currentPos_;
  }

  /**
   * Lecture des champs de l'enregistrement. L'enregistrement doit etre lu avant la lecture des champs. Un
   * enregistrement correspond � une instruction READ du Fortran
   *
   * @throws IOException
   */
  public void readRecord() throws IOException {
   /* final long l = */skip(nextPos_ - currentPos_);
    //if (l > 0 && l != (nextPos_ - currentPos_)) FuLog.warning("Problem whith binary read");
    currentPos_ = nextPos_;
    if (sequential_) {
      recordLength_ = readInt32();
      nextPos_ += (long) recordLength_ + 8;
    } else {
      nextPos_ += recordLength_;
    }
  }

  /**
   * @return Renvoie la position courante dans le flux.
   */
  public int getCurrentPosition() {
    return (int) currentPos_;
  }

  /**
   * @return la position de la prochaine sequence
   */
  public long getNextPos() {
    return nextPos_;
  }

  /**
   * @return l'indice du dernier champ de l'enregistrement en cours de lecture
   */
  public long getEndOfRecord() {
    return sequential_ ? nextPos_ - 8 : nextPos_;
  }
}
