/**
 * @creation 27 sept. 2004
 * @modification $Date: 2007-05-04 13:45:58 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.fortran;

import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.ProgressionUpdater;

import java.io.IOException;

/**
 * @author Fred Deniger
 * @version $Id: FortranDoubleWriter.java,v 1.9 2007-05-04 13:45:58 deniger Exp $
 */
public class FortranDoubleWriter extends FileOpWriterCharSimpleAbstract {
  int[] fmt_;
  String entete_;

  public String getEntete() {
    return entete_;
  }

  public void setFmt(int[] newFormat) {
    this.fmt_ = newFormat;
  }

  public void setEntete(final String _entete) {
    entete_ = _entete;
  }

  /**
   * @param _fmt les formats
   */
  public FortranDoubleWriter(final int[] _fmt) {
    fmt_ = _fmt;
  }

  @Override
  public void internalWrite(final Object _o) {
    if (!(_o instanceof FortranDoubleReaderResultInterface)) {
      donneesInvalides(_o);
      return;
    }
    final FortranDoubleReaderResultInterface r = (FortranDoubleReaderResultInterface) _o;
    final FortranWriter w = new FortranWriter(out_);

    w.setStringQuoted(false);
    w.setSpaceBefore(true);
    final int nbCol = r.getNbColonne();
    final int nbLigne = r.getNbLigne();
    final ProgressionUpdater up = new ProgressionUpdater(progress_);
    up.setValue(4, nbLigne);
    try {
      if (entete_ != null) {
        w.writeln(entete_);
      }
      for (int i = 0; i < nbLigne; i++) {
        for (int j = nbCol - 1; j >= 0; j--) {
          final CtuluNumberFormatI format = r.getFormat(j);
          if (format != null) {
            w.stringField(j, format.format(r.getValue(i, j)));
          } else {
            w.stringField(j, Double.toString(r.getValue(i, j)));
          }
        }
        w.writeFields(fmt_);
        up.majAvancement();
      }
    } catch (final IOException e) {
      analyze_.manageException(e);
    }
  }
}
