/**
 * @creation 1998-08-19
 * @modification $Date: 2006-09-19 14:42:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.fortran;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import org.fudaa.ctulu.fileformat.FortranInterface;
import org.fudaa.dodico.fichiers.NativeBinarySystem;

/**
 * Une classe facilitant l'�criture de fichiers binaires lus par Fortran. ATTENTION les methodes heritees et non red�finies dans cette donneront des
 * r�sultats faux. L'�quivalence d'intructions entre Java et Fortran se fera de la mani�re suivante :<BR>
 *
 * <PRE>
 * (en consid�rant i=integer/int, f=real/float, d=double precision/double et
 * s=character*()/String)
 * 1) Pour un fichier � acces s�quentiel :
 *   Fortran
 *     open  (unit=10,file='fichier.bin',access='sequentiel',form='unformatted')
 *     write (unit=10)
 *     ...
 *     close (unit=10)
 *   Java
 *     FortranBinaryOutputStream out=
 *      new FortranBinaryOutputStream(new FileOutputStream("fichier.bin"),true);
 *     out.writeInteger(i);
 *     out.writeReal(f);
 *     out.writeDoublePrecision(d);
 *     out.writeCharacter(s);
 *     out.writeRecord();
 *     ...
 *     out.close();
 * 2) Pour un fichier � acces direct :
 *   Fortran
 *     open(unit=10,file='fichier.bin',access='direct',recl=30,form='unformatted')
 *     write (unit=10,rec=1)
 *     ...
 *     close (unit=10)
 *   Java
 *     FortranBinaryOutputStream out=
 *      new FortranBinaryOutputStream(new FileOutputStream("fichier.bin"),false);
 *     out.setRecordLength(30);
 *     out.writeInteger(i);
 *     out.writeReal(f);
 *     out.writeDoublePrecision(d);
 *     out.writeCharacter(s);
 *     out.writeRecord();
 *     ...
 *     out.close();
 * </PRE>
 *
 * @version $Id: FortranBinaryOutputStream.java,v 1.14 2006-09-19 14:42:29 deniger Exp $
 * @author Bertrand Marchand
 */
public class FortranBinaryOutputStream implements FortranInterface {
  //  private NativeBinaryOutputStream bufStream_;

  private ByteArrayOutputStream arrayStream_;
  private boolean sequential_;
  private int recordLength_;
  Charset charset = Charset.forName("ISO-8859-1");
  private ByteOrder byteOrder;
  final OutputStream out;

  /**
   * Cr�ation en pr�cisant si le fichier binaire est � access s�quentiel ou non.
   *
   * @param _out OutputStream
   * @param _sequential <B>true</B> le fichier est binaire � acc�s <I>Sequential</I>. <B>false</B> le fichier est binaire � acc�s <I>Direct</I>
   */
  public FortranBinaryOutputStream(final OutputStream _out, final boolean _sequential) {
    this(_out, _sequential, NativeBinarySystem.getNativeByteOrder());
  }

  /**
   * Cr�ation en pr�cisant si le fichier binaire est � access s�quentiel ou non.
   *
   * @param _out OutputStream
   * @param _sequential <B>true</B> le fichier est binaire � acc�s <I>Sequential</I>. <B>false</B> le fichier est binaire � acc�s <I>Direct</I>
   * @param _architectureID l'architecture demandee
   * @deprecated L'identifiant machine ne permet pas d'identifier le byte order natif.
   */
  public FortranBinaryOutputStream(final OutputStream _out, final boolean _sequential, final String _architectureID) {
    this.out = _out;
    arrayStream_ = new ByteArrayOutputStream();
    byteOrder = NativeBinarySystem.getByteOrder(_architectureID);
    sequential_ = _sequential;
    recordLength_ = 0;
  }

  /**
   * Cr�ation en pr�cisant si le fichier binaire est � access s�quentiel ou non.
   *
   * @param _out OutputStream
   * @param _sequential <B>true</B> le fichier est binaire � acc�s <I>Sequential</I>. <B>false</B> le fichier est binaire � acc�s <I>Direct</I>
   * @param _byteOrder l'endianess demandee
   */
  public FortranBinaryOutputStream(final OutputStream _out, final boolean _sequential, final ByteOrder _byteOrder) {
    this.out = _out;
    arrayStream_ = new ByteArrayOutputStream();
    byteOrder = _byteOrder;
    sequential_ = _sequential;
    recordLength_ = 0;
  }

  /**
   * Affectation de la longueur des enregistrements (pour les fichiers � acc�s <I>Direct</I>).
   *
   * @param _length Longueur d'enregistrement en longworld (1 longworld=4 octets)
   */
  public void setRecordLength(final int _length) {
    recordLength_ = _length * 4;
  }

  /**
   * Retourne la longueur des enregistrements (pour les fichiers � acc�s <I>Direct</I>.
   *
   * @return Longueur d'enregistrement en longworld (1 longworld=4 octets)
   */
  public int getRecordLength() {
    return recordLength_ / 4;
  }

  /**
   * Ecriture d'un champ chaine de caract�res "<I>character</I>" Fortran.
   *
   * @param _s string correspondant � la chaine de caract�res
   * @throws IOException
   */
  public void writeCharacter(final String _s) throws IOException {
    //we encode we a defaut encoding:
    ByteBuffer encode = charset.encode(_s).order(byteOrder);
    arrayStream_.write(encode.array());
  }

  /**
   * Ecriture d'un champ entier "<I>integer</I>" Fortran.
   *
   * @param _i int correspondant au integer
   * @throws IOException
   */
  public void writeInteger(final int _i) throws IOException {
    arrayStream_.write(integerToByte(_i));
  }

  private byte[] integerToByte(final int _i) {
    ByteBuffer buffer = getByteBuffer4().putInt(_i);
    byte[] array = buffer.array();
    return array;
  }

  /**
   * Ecriture d'un champ r�el "<I>real</I>" Fortran.
   *
   * @param _f float correspondant au real
   * @throws IOException
   */
  public void writeReal(final float _f) throws IOException {
    float f = _f;
    if (_f == -0.0) {
      f = 0f;
    }
    arrayStream_.write(getByteBuffer4().putFloat(f).array());
  }

  /**
   * Ecriture d'un champ r�el en double pr�cision "<I>double precision</I>". Fortran
   *
   * @param _d double correspondant au double precision
   * @throws IOException
   */
  public void writeDoublePrecision(final double _d) throws IOException {
    double d = _d;
    if (_d == -0.00) {
      d = 0;
    }
    arrayStream_.write(getByteBuffer8().putDouble(d).array());
  }

  /**
   * Ecriture des champs de l'enregistrement. L'enregistrement doit etre �crit pour que les champs soient �galement �crits sur le fichiers. Un
   * enregistrement correspond � une instruction WRITE du Fortran
   *
   * @throws IOException
   */
  public void writeRecord() throws IOException {
    if (sequential_) {
      final int t = arrayStream_.size();
      byte[] integerToByte = integerToByte(t);
      out.write(integerToByte);
      arrayStream_.writeTo(out);
      out.write(integerToByte);
    } else {
      final byte[] buffer = arrayStream_.toByteArray();
      out.write(buffer, 0, Math.min(buffer.length, recordLength_));
      for (int i = buffer.length; i < recordLength_; i++) {
        out.write(0);
      }
    }
    arrayStream_.reset();
  }

  /**
   * Fermeture du fichier.
   */
  @Override
  public void close() throws IOException {
    out.close();
  }
  ByteBuffer byteBuffer4;
  ByteBuffer byteBuffer8;

  public ByteBuffer getByteBuffer4() {
    if (byteBuffer4 == null) {
      byteBuffer4 = ByteBuffer.allocate(4).order(byteOrder);
    } else {
      byteBuffer4.clear();
    }
    return byteBuffer4;
//    return ByteBuffer.allocate(4).order(byteOrder);
  }

  public ByteBuffer getByteBuffer8() {
    if (byteBuffer8 == null) {
      byteBuffer8 = ByteBuffer.allocate(8).order(byteOrder);
    } else {
      byteBuffer8.clear();
    }
    return byteBuffer8;
  }
}
