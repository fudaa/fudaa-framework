package org.fudaa.dodico.fortran;

import com.memoire.fu.FuLog;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.fudaa.ctulu.fileformat.FortranLib;

public class DodicoDoubleArrayBinaryFileSaver {

  private final File destFile;

  public DodicoDoubleArrayBinaryFileSaver(File destFile) {
    super();
    this.destFile = destFile;
  }

  public Map<String, double[]> load() {
    Map<String, double[]> mapOfValues = new HashMap<String, double[]>();
    if (destFile.length() == 0) {
      return mapOfValues;
    }
    FortranBinaryInputStream input = null;
    try {
      try {
          // FIXME B.M. Force a little endian pour correspondre anciennement a X86
        input = new FortranBinaryInputStream(new FileInputStream(destFile), true, ByteOrder.LITTLE_ENDIAN);
        while (true) {
          input.readRecord();
          int readInteger = input.readInteger();
          if (readInteger <= 0) {
            break;
          }
          String name = input.readCharacter(readInteger);
          readInteger = input.readInteger();
          double[] values = new double[readInteger];
          for (int i = 0; i < values.length; i++) {
            values[i] = input.readDoublePrecision();
          }
          mapOfValues.put(name, values);
        }
      } catch (EOFException e) {
        FuLog.error(e);
      }

    } catch (Exception e) {
      FuLog.error(e);
    }
    return mapOfValues;
  }

  public boolean save(Map<String, double[]> mapOfValues) {
    FortranBinaryOutputStream output = null;
    boolean ok = true;
    try {
        // FIXME B.M. Force a little endian pour correspondre anciennement a X86
      output = new FortranBinaryOutputStream(new FileOutputStream(destFile), true, ByteOrder.LITTLE_ENDIAN);
      for (Entry<String, double[]> entry : mapOfValues.entrySet()) {
        String key = entry.getKey();
        if (key.isEmpty()) {
          FuLog.error(new Throwable());
          continue;
        }
        double[] values = entry.getValue();
        output.writeInteger(key.length());
        output.writeCharacter(key);
        output.writeInteger(values.length);
        for (int i = 0; i < values.length; i++) {
          output.writeDoublePrecision(values[i]);
        }
        output.writeRecord();
      }
    } catch (Exception e) {
      ok = false;
      FuLog.error(e);
    } finally {
      FortranLib.close(output);
    }
    return ok;

  }
}
