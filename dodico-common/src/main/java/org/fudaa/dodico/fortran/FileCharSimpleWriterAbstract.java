/**
 * @creation 21 mars 2003
 * @modification $Date: 2006-09-19 14:42:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.fortran;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluLog;
import org.fudaa.ctulu.fileformat.FileOperationWriterAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;

/**
 * @author deniger
 * @version $Id: FileOpWriterCharSimpleAbstract.java,v 1.4 2006-09-19 14:42:29 deniger Exp $
 */
public abstract class FileCharSimpleWriterAbstract<T> extends FileOperationWriterAbstract<T> {

  protected String lineSep_;
  protected Writer out_;

  /**
   * Initialise le separateur de ligne.
   */
  public FileCharSimpleWriterAbstract() {
    lineSep_ = CtuluLibString.LINE_SEP;
  }

  @Override
  protected FortranInterface getFortranInterface() {
    return new FortranInterface() {

      @Override
      public void close() throws IOException {
        if (out_ != null) {
          out_.close();
        }
      }
    };
  }

  /**
   * Renvoie le separateur de ligne.
   *
   * @return separateur de ligne
   */
  public final String getLineSeparator() {
    return lineSep_;
  }

  @Override
  public void setFile(final File _f) {
    analyze_ = new CtuluLog();
    analyze_.setResource(_f.getAbsolutePath());
    FileWriter out = null;
    try {
      out = new FileWriter(_f);
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
    if (out != null) {
      out_ = out;
    }
  }

  /**
   * @param _f le fichier a ecrire (_f[0]).
   */
  public final void setFile(final File[] _f) {
    setFile(_f[0]);
  }

  /**
   * Modifie le separateur de ligne.
   *
   * @param _lineSep The lineSep to set
   */
  public final void setLineSeparator(final String _lineSep) {
    lineSep_ = _lineSep;
  }

  /**
   * @param _b la chaine a ajouter au flux
   * @throws IOException
   */
  public void writeToOut(final String _b) throws IOException {
    out_.write(_b);
  }

  /**
   * Ecrit un retour chariot.
   *
   * @throws IOException
   */
  public void writelnToOut() throws IOException {
    out_.write(lineSep_);
  }

  /**
   * Ecrit la chaine plus un retour chariot.
   *
   * @param _b la chaine a ajouter au flux
   * @throws IOException
   */
  public void writelnToOut(final String _b) throws IOException {
    writeToOut(_b + lineSep_);
  }

}
