/*
 *  @creation     24 sept. 2004
 *  @modification $Date: 2007-06-13 12:57:16 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.dodico.fortran;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import gnu.trove.TDoubleArrayList;
import java.io.EOFException;
import java.io.IOException;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.fileformat.FortranInterface;
import org.fudaa.dodico.commun.DodicoLib;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class FortranDoubleReader extends FileOpReadCharSimpleAbstract implements FortranInterface {
  final int[] fmt_;
  String charComments_=null;

  @Override
  public void close() throws IOException {
    in_.close();
  }

  boolean isFmtError_;
  final int nbField_;
  boolean skipFirstLine_;

  public boolean isSkipFirstLine() {
    return skipFirstLine_;
  }

  public void setSkipFirstLine(final boolean _skipFirstLine) {
    skipFirstLine_ = _skipFirstLine;
  }
  
  /**
   * La lecture accepte les lignes de commentaires. Elle sont introduites par l'un des caract�re contenus
   * dans _charComments.<p>
   * <b>Attention</b><br>
   * la casse du caract�re qui introduit un commentaire est importante. Par exemple, si on
   * souhaite des lignes commentaires commencant par 'C' ou 'c', il faudra donner _charComments="Cc"
   * @param _charComments Les diff�rents caract�res autoris�s pour introduire une ligne commentaire.
   */
  public void acceptCommentLines(String _charComments) {
    charComments_=_charComments;
  }

  public FortranDoubleReader(final int _nbField) {
    nbField_ = _nbField;
    fmt_ = null;
  }

  public FortranDoubleReader(final int[] _fmt) {
    fmt_ = _fmt;
    nbField_ = -1;
  }

  private int getNbSupposedField() {
    return fmt_ == null ? nbField_ : fmt_.length;
  }

  @Override
  public Object internalRead() {
    if (in_ == null) {
      analyze_.addFatalError(DodicoLib.getS("Flux d'entr�e non trouv�"));
      return null;
    }
    // si demand�,on ne lit pas la premiere ligne

    in_.setBlankZero(true);
    final FortranDoubleReaderResult result = new FortranDoubleReaderResult();
    result.nbField_ = -1;
    final int nb = getNbSupposedField();
    final TDoubleArrayList[] r = new TDoubleArrayList[nb];

    for (int i = nb - 1; i >= 0; i--) {
      r[i] = new TDoubleArrayList(50);
    }
    try {
      if (skipFirstLine_) {
        in_.readLine();
      }
      boolean first = true;
      while (true) {
        if (fmt_ == null) {
          in_.readFields();
        } else {
          if (first && in_.getLine() != null) {
            first = false;
            result.isLineComplete_ = in_.getLine().length() == CtuluLibArray.getSum(fmt_);
          }
          in_.readFields(fmt_);
        }
        if (in_.getNumberOfFields() == 0 || in_.getLine() == null || in_.getLine().trim().length() == 0 ||
            // Ligne commentaires ?
            (charComments_!=null && charComments_.indexOf(in_.getLine().charAt(0))!=-1)) {
          continue;
        }
        if (result.nbField_ < 0) {
          result.nbField_ = in_.getNumberOfFields();
        }
        for (int i = nb - 1; i >= 0; i--) {
          r[i].add(in_.doubleField(i));
        }
      }
    } catch (final EOFException _e) {
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("DFR: fin de fichier");
      }
    } catch (final NumberFormatException _e) {
      isFmtError_ = true;
      analyze_.manageException(_e, in_.getLineNumber());
    } catch (final IOException _e) {
      analyze_.manageException(_e);
    }
    final double[][] rf = new double[r.length][];
    for (int i = r.length - 1; i >= 0; i--) {
      rf[i] = r[i].toNativeArray();
    }
    result.r_ = rf;
    return result;
  }

  public final boolean isFmtError() {
    return isFmtError_;
  }
}
