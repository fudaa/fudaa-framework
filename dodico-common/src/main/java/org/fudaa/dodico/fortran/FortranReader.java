/**
 * @creation 1997-03-12
 * @modification $Date: 2007-06-20 12:21:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.fortran;

import com.memoire.fu.FuLog;
import java.io.EOFException;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FortranInterface;
import org.fudaa.dodico.fichiers.DodicoLineNumberReader;

/**
 * FortranReader d�rive de LineNumberReader (et non plus de FilterReader), ce qui permet de lire les fichiers 10 fois +
 * vite. Une classe permettant de facilement lire les fichiers Fortran. Etendant LineNumberReader, fichiers formates
 * (cad avec une largeur de colonnes) ou libres. constantes).
 * 
 * @version $Id: FortranReader.java,v 1.26 2007-06-20 12:21:48 deniger Exp $
 * @author Guillaume Desnoix , Axel von Arnim, Bertrand Marchand
 */
public class FortranReader implements FortranInterface {

  protected static String clearSpaces(final String _s) {
    return _s.trim();
  }

  private boolean jumpCommentLine_;
  // protected int number;
  protected boolean blankIsZero_;

  protected List fields_;
  protected String line_;
  String commentChar_;
  LineNumberReader in_;
  DodicoLineNumberReader inGnu_;
  boolean jumpBlankLine_;

  /**
   * @param _reader le support
   */
  public FortranReader(final LineNumberReader _reader) {
    if (_reader == null) {
      throw new RuntimeException("Argument is nul");
    }
    in_ = _reader;
  }

  /**
   * @param _reader le support
   */
  public FortranReader(final DodicoLineNumberReader _reader) {
    if (_reader == null) {
      throw new RuntimeException("Argument is nul");
    }
    inGnu_ = _reader;
  }

  /**
   * @param _reader le support
   */
  public FortranReader(final Reader _reader) {
    this(new LineNumberReader(_reader));
    // inutile car initialise par defaut.
    // fields=null;
    // line =null;
    // number=-1;
    // blankIsZero=false;
  }

  /**
   * @param _reader le support
   * @param _sz la taille du buffer
   */
  public FortranReader(final Reader _reader, final int _sz) {
    this(new LineNumberReader(_reader, _sz));
  }

  // B.M. (20/05/99) Ne pas supprimer les lignes suivantes en commentaire
  /*
   * public int getLineNumber() { return number; }
   */
  /**
   * Analyse la ligne courante selon le format passe en parametre. Cette methode permet d'utiliser la methode readLine()
   * pour connaitre le debut de la ligne puis de l'analyser avec un format precis.
   * 
   * @param _fmt les formats a utiliser (taille des champs)
   * @throws IOException
   */
  public void analyzeCurrentLine(final int[] _fmt) throws IOException {

    if (_fmt != null) {
      int i, j, l;
      l = _fmt.length;
      if (fields_ == null) {
        fields_ = new ArrayList(l);
      } else {
        fields_.clear();
      }
      j = 0;
      final int lineLength = line_.length();
      int begin;
      int end;
      int temp;
      int fmti;
      char[] cars;
      StringBuilder tempS;
      for (i = 0; i < l; i++) {
        fmti = _fmt[i];
        // begin=Math.min(j, lineLength);
        begin = j < lineLength ? j : lineLength;
        // end=Math.min(j + _fmt[i], lineLength);
        temp = j + fmti;
        end = temp < lineLength ? temp : lineLength;
        cars = new char[end - begin];
        line_.getChars(begin, end, cars, 0);
        tempS = new StringBuilder(fmti);
        tempS.append(cars);
        // MODIF AXEL
        // fields[i] =
        // line.substring(
        // Math.min(j, lineLength),
        // Math.min(j + _fmt[i], lineLength));
        // MODIF BERTRAND
        // NE se produit que si les formats annonces sont superieurs
        // a la longueur reelle de la ligne. Ne doit normalement jamais arrive.
        while (tempS.length() < fmti) {
          tempS.append(' ');
        }
        // fields[i]= tempS.toString();
        fields_.add(tempS.toString());
        j += fmti;
      }
    }
  }

  /**
   * Appel la methode LineNumberReader.close().
   * 
   * @see java.io.LineNumberReader#close()
   */
  @Override
  public void close() throws IOException {
    if (inGnu_ == null) {
      in_.close();
    } else {
      inGnu_.close();
    }
  }

  public long skip(final long _n) throws IOException {
    if (inGnu_ != null) {
      return inGnu_.skip(_n);
    }
    return in_.skip(_n);
  }

  /**
   * Renvoie le champ numero _i sous la forme d'un reel.
   * 
   * @param _i le numero du champ
   * @return la valeur interpretee comme un double
   */
  public double doubleField(final int _i) {
    double r = 0.;
    if ((fields_ != null) && (_i < fields_.size())) {
      // r=(new Double(clearSpaces(fields[_i]))).doubleValue();
      // String s=clearSpaces(fields[_i]);
      final String res = ((String) fields_.get(_i));
      if (blankIsZero_ && CtuluLibString.isEmpty(res)) r = 0;
      else
        r = Double.parseDouble(res.trim());
    }
    return r;
  }

  /**
   * @return la ligne lue
   */
  public String getLine() {
    return line_;
  }

  /**
   * Appel la methode LineNumberReader.getLinenumber().
   * 
   * @see java.io.LineNumberReader#getLineNumber()
   * @return le numero de ligne lu
   */
  public int getLineNumber() {
    if (inGnu_ != null) {
      return inGnu_.getLineNumber();
    }
    return in_.getLineNumber();
  }

  /**
   * Renvoie le nombre de champs lus lors du dernier appel � readFields().
   * 
   * @return Le nombre de champs.
   */
  public int getNumberOfFields() {
    return fields_ == null ? 0 : fields_.size();
  }

  /**
   * @return le nombre de champs non vides
   */
  public int getNumberOfNotEmptyField() {
    final int nb = getNumberOfFields();
    int r = 0;
    for (int i = nb - 1; i >= 0; i--) {
      if (!CtuluLibString.isEmpty((String) fields_.get(i))) r++;
    }
    return r;
  }

  /**
   * Renvoie le champ numero _i sous la forme d'un entier.
   * 
   * @param _i le numero du champ
   * @return la valeur interprete comme un int
   */
  public int intField(final int _i) {
    int r = 0;
    if ((fields_ != null) && (_i < fields_.size())) {
      // r=(new Integer(clearSpaces(fields[_i]))).intValue();
      String res = (String) fields_.get(_i);
      if (blankIsZero_ && CtuluLibString.isEmpty(res)) r = 0;
      else
        r = Integer.parseInt(clearSpaces(res));
    }
    return r;
  }

  /**
   * Retourne l'�tat de l'�quivalence champ blanc/Zero.
   * 
   * @return true si un champ blanc est vu comme egale a zero
   */
  public boolean isBlankZero() {
    return blankIsZero_;
  }

  /**
   * Cette methode ne peut etre appelee que si le caractere de commentaire a ete initialise avec la methode
   * setCommentInOneField(). Dans ce cas, renvoie true si la ligne lue commence par le caractere de commentaire. non
   * testee.
   * 
   * @see #setCommentInOneField(String)
   * @return true si comment line
   */
  public boolean isCommentLine() {
    if (commentChar_ == null) {
      throw new IllegalArgumentException("Comment char must be set with setCommandInOneField()");
    }
    if ((fields_.size() == 1) && (((String) fields_.get(0)).startsWith(commentChar_))) {
      return true;
    }
    return false;
  }

  /**
   * Renvoie la propriete jumpBlankLine.
   * 
   * @return boolean
   */
  public boolean isJumpBlankLine() {
    return jumpBlankLine_;
  }

  /**
   * Appel la methode LineNumberReader.mark().
   * 
   * @param _l la marque
   * @throws IOException
   * @see java.io.LineNumberReader#mark(int)
   */
  public void mark(final int _l) throws IOException {
    if (inGnu_ == null) {
      in_.mark(_l);
    } else {
      inGnu_.mark(_l);
    }
  }

  /**
   * Lit une ligne et l'analyse selon le format libre (espaces).
   * 
   * @throws IOException
   */
  public void readFields() throws IOException {
    // String s=readLine();
    // line=s;
    // if(line==null) throw new EOFException();
    readLine();
    if (jumpBlankLine_) {
      if (line_.trim().length() == 0) {
        this.readFields();
        return;
      }
    }
    // si le caractere de commentaire a ete defini
    if (commentChar_ != null) {
      // si la ligne commence par le caractere
      if (line_.startsWith(commentChar_)) {
        // on veut ignorer les lignes de commentaire
        if (jumpCommentLine_) {
          this.readFields();
        }
        // on stocke la ligne ...
        else {
          if (fields_ == null) {
            fields_ = new ArrayList(20);
          } else {
            fields_.clear();
          }
          fields_.add(line_);
        }
        return;
      }
    }
    readRealField();
  }

  private void readRealField() {
    int i;
    fields_ = null;
    int fieldBegin = -1;
    int fieldEnd = -1;
    boolean fieldQuote = false;
    final char[] c = line_.toCharArray();
    final int staille = c.length;
    final List v = new ArrayList(21);
    for (i = 0; i < staille; i++) {
      // Hors d'un champ
      if (fieldBegin == -1) {
        if (c[i] == '\'') {
          fieldQuote = true;
          fieldBegin = i;
        } else if (c[i] != ' ' && c[i] != '\t') {
          fieldBegin = i;
        }
      }
      // Dans un champ
      else {
        if (fieldQuote) {
          if (c[i] == '\'') {
            if ((i + 1 == staille) || (c[i + 1] != '\'')) {
              fieldEnd = i + 1;
              fieldQuote = false;
            }
          }
        } else {
          if ((c[i] == ' ') || (c[i] == '\t')) {
            fieldEnd = i;
          }
        }
        // Ajout du champ dans le vecteur des champs
        if (fieldEnd != -1) {
          v.add(line_.substring(fieldBegin, fieldEnd));
          fieldBegin = -1;
          fieldEnd = -1;
        }
      }
    }
    if (fieldBegin != -1) {
      if (fieldQuote) {
        FuLog.warning("error with field " + line_.substring(fieldBegin, staille));
      } else {
        v.add(line_.substring(fieldBegin, staille));
      }
    }
    fields_ = v;
  }

  /**
   * Lit une ligne et l'analyse selon le format fixe fourni.
   * 
   * @param _fmt le format sous forme d'un tableau de largeurs
   * @throws IOException
   */
  public void readFields(final int[] _fmt) throws IOException {
    // String s=readLine();
    // line=s;
    // if(s==null) throw new EOFException();
    readLine();
    if (jumpBlankLine_) {
      if ("".equals(line_.trim())) {
        this.readFields(_fmt);
        return;
      }
    }
    if (jumpCommentLine_ && commentChar_ != null && line_.startsWith(commentChar_)) {
      this.readFields(_fmt);
    } else
      analyzeCurrentLine(_fmt);
  }

  /**
   * Ancienne methode de lecture.
   * 
   * @throws IOException
   */
  public void readFieldsOld() throws IOException {
    // String s=readLine();
    // line=s;
    readLine();
    if (line_ == null) {
      throw new EOFException();
    }
    int i;
    fields_ = null;
    final String s = clearSpaces(line_) + ' ';
    int fieldBegin = -1;
    int fieldEnd = -1;
    boolean fieldQuote = false;
    final char[] c = s.toCharArray();
    final int staille = c.length;
    final List v = new ArrayList(21);
    // for(i=0; i<s.length(); i++)
    for (i = 0; i < staille; i++) {
      // Hors d'un champ
      if (fieldBegin == -1) {
        if (c[i] == '\'') {
          fieldQuote = true;
          fieldBegin = i;
        } else if (c[i] != ' ' && c[i] != '\t') {
          fieldBegin = i;
        }
      }
      // Dans un champ
      else {
        if (fieldQuote) {
          if ((i + 1 < staille) && (c[i] == '\'') && (c[i + 1] != '\'')) {
            fieldEnd = i + 1;
            fieldQuote = false;
          }
        } else {
          if (c[i] == ' ' || c[i] == '\t') {
            fieldEnd = i;
          }
        }
        if (fieldEnd != -1) {
          final String stemp = s.substring(fieldBegin, fieldEnd);
          v.add(stemp);
          fieldBegin = -1;
          fieldEnd = -1;
        }
      }
    }
    // l= v.size();
    fields_ = v;
  }

  /**
   * Appel la methode LineNumberReader.readLine().
   * 
   * @see java.io.LineNumberReader#readLine()
   * @return la ligne lue
   * @throws IOException
   */
  public String readLine() throws IOException {
    if (inGnu_ == null) {
      line_ = in_.readLine();
    } else {
      line_ = inGnu_.readLine();
    }
    if (line_ == null) {
      throw new EOFException();
    }
    if ((jumpBlankLine_ && line_.trim().length() == 0) || (jumpCommentLine_ && line_.startsWith(commentChar_))) {
      this.readLine();
      return line_;
    }
    return line_;
  }

  /**
   * Appel la methode LineNumberReader.ready().
   * 
   * @return true si le lecteur est pret.
   * @throws IOException
   * @see java.io.LineNumberReader#ready()
   */
  public boolean ready() throws IOException {
    if (inGnu_ != null) {
      return inGnu_.ready();
    }
    return in_.ready();
  }

  /**
   * Appel la methode LineNumberReader.reset().
   * 
   * @throws IOException
   * @see java.io.LineNumberReader#reset()
   */
  public void reset() throws IOException {
    if (inGnu_ == null) {
      in_.reset();
    } else {
      inGnu_.reset();
    }
  }

  /**
   * D�finit si un champ blanc est consid�r� comme ayant valeur 0 pour les int, float ou double.
   * 
   * @param _state <code>true</code> Un champ blanc vaut 0.<code>false</code> Un champ blanc d�clenche une
   *          exception defaut : Une exception est d�clench�e.
   */
  public void setBlankZero(final boolean _state) {
    blankIsZero_ = _state;
  }

  /**
   * Definit si les lignes commencant par le caractere '_c' sont traites comme un seul champ ou non. Par defaut, toutes
   * les lignes sont traites champs par champs (_c=null). Cette optimisation n'est utilisee que par la methode
   * readFields();
   * 
   * @param _c le caractere indiquant un commentaire
   */
  public void setCommentInOneField(final String _c) {
    commentChar_ = _c;
  }

  /**
   * Affecte la propriete jumbblankLine. Si true, les lignes 'blanches' sont ignorees. Cette proprietes n'est utilisee
   * que pour la methode readFields().
   * 
   * @param _jumpBlankLine The jumpBlankLine to set
   */
  public void setJumpBlankLine(final boolean _jumpBlankLine) {
    jumpBlankLine_ = _jumpBlankLine;
  }

  /**
   * Si true, les lignes commencant par un commentaire seront ignoree lors d'un readField. ATTENTION : ne marche qui se
   * setCommentInOneField a ete appele avec une valeur non nulle.
   * 
   * @param _b la nouvelle valeurs pour sauter les commentaires
   */
  public void setJumpCommentLine(final boolean _b) {
    jumpCommentLine_ = _b;

  }

  /**
   * Renvoie le champ numero _i sous la forme d'une chaine. Les espaces de debut et de fin ont ete retires.
   * 
   * @param _i le numero du champ
   * @return la valeur interprete comme un String
   */
  public String stringField(final int _i) {
    String r = "";
    if ((fields_ != null) && (_i < fields_.size())) {
      r = clearSpaces((String) fields_.get(_i));
    }
    if (r.length() >= 2) {
      final char c = r.charAt(0);
      if ((c == '\'' || c == '\"') && (c == r.charAt(r.length() - 1))) {
        r = r.substring(1, r.length() - 1);
      }
    }
    return r;
  }
}
