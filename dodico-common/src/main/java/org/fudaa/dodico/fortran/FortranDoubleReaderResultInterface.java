/*
 *  @creation     24 sept. 2004
 *  @modification $Date: 2007-04-30 14:21:34 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.fortran;

import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.collection.CollectionPointDataDoubleInterface;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;

/**
 * @author Fred Deniger
 * @version $Id: FortranDoubleReaderResultInterface.java,v 1.9 2007-04-30 14:21:34 deniger Exp $
 */
public interface FortranDoubleReaderResultInterface {

  int getNbColonne();

  int getNbLigne();

  double getValue(int _ligne, int _col);
  
  CtuluCollectionDouble getCollectionFor(int _col);

  CtuluNumberFormatI getFormat(int _col);

  /**
   * NON UTILISE PAR le FortranDoubleWriter.
   *
   * @return l'interface pour des donn�es geo
   */
  CollectionPointDataDoubleInterface createXYValuesInterface();

}
