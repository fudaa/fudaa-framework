/**
 * @creation 21 mars 2003
 * @modification $Date: 2006-09-19 14:42:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.fortran;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;
import org.fudaa.dodico.commun.DodicoLib;
/**
 * @deprecated use {@link FileCharSimpleReaderAbstract}
 * @author deniger
 * @version $Id: FileOpReadCharSimpleAbstract.java,v 1.4 2006-09-19 14:42:29 deniger Exp $
 */
public abstract class FileOpReadCharSimpleAbstract extends FileReadOperationAbstract {

  protected FortranReader in_;

  protected void setFile(final Reader _r){
    in_ = new FortranReader(_r);
  }

  protected void addAnalyzeLigneLue(){
    analyze_.addInfo(DodicoLib.getS("Ligne lue") + ": " + in_.getLine(), in_.getLineNumber());
  }

  @Override
  protected FortranInterface getFortranInterface(){
    return in_;
  }


  protected void processFile(final File _f){

  }

  @Override
  public void setFile(final File _f){
    analyze_ = new CtuluAnalyze();
    analyze_.setResource(_f.getAbsolutePath());
    FileReader r = null;
    try {
      r = new FileReader(_f);
      processFile(_f);
    }
    catch (final FileNotFoundException _e) {
      analyze_.addFatalError(DodicoLib.getS("Fichier inconnu"));
    }
    if (r != null) {
      setFile(r);
    }
  }

  /**
   * @param _f le fichier a lire (_f[0])
   */
  public final void setFile(final File[] _f){
    setFile(_f[0]);
  }
}
