/*
 * @creation 1997-03-12
 * @modification $Date: 2007-05-04 13:45:58 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.fortran;

import com.memoire.fu.FuLog;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Collections;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FortranInterface;
import org.fudaa.dodico.commun.DodicoArrayList;

/**
 * Une classe permettant de facilement ecrire des fichiers Fortran. Etendant PrintWriter, elle permet l'ecriture en
 * colonnes ou libre.
 * 
 * @version $Id: FortranWriter.java,v 1.24 2007-05-04 13:45:58 deniger Exp $
 * @author Guillaume Desnoix , Bertrand Marchand
 */
public class FortranWriter implements FortranInterface // extends PrintWriter
{

  protected DodicoArrayList fields_;
  int nbMaxFields_;
  // protected Vector fields;
  protected int number_;
  private final Writer out_;
  private String lineSep_ = CtuluLibString.LINE_SEP;
  private StringBuffer buffer_;
  private boolean isSpaceBefore_;

  public final boolean isSpaceBefore() {
    return isSpaceBefore_;
  }

  /**
   * @param _isSpaceBefore true si les espaces doivent etre ajoutes avant la chaine
   */
  public final void setSpaceBefore(final boolean _isSpaceBefore) {
    isSpaceBefore_ = _isSpaceBefore;
  }

  /**
   * @param _w le writer support
   */
  public FortranWriter(final Writer _w) {
    out_ = _w;
    fields_ = new DodicoArrayList();
    // fields= new Vector();
    number_ = -1;
    nbMaxFields_ = -1;
  }

  private void write(final String _b) throws IOException {
    out_.write(_b.toCharArray());
  }

  private StringBuffer write(final StringBuffer _b) throws IOException {
    final char[] c = new char[_b.length()];
    _b.getChars(0, c.length, c, 0);
    out_.write(c);
    // le setLength est lent pour l'instant
    // il vaut mieux recr�er un StringBuffer
    // _b.setLength(0);
    return new StringBuffer(100);
  }

  /**
   * @param _w le writer support
   */
  public FortranWriter(final PrintWriter _w) {
    out_ = _w;
    fields_ = new DodicoArrayList();
    // fields= new Vector();
    number_ = -1;
  }

  @Override
  public void close() throws IOException {
    out_.close();
  }

  /**
   * @throws IOException
   */
  public void flush() throws IOException {
    out_.flush();
  }

  /**
   * Ecrit la chaine <code>_l</code> et reviens a la ligne. Incremente le num de ligne.
   * 
   * @param _l la chaine a ecrire
   * @throws IOException
   */
  public void writeln(final String _l) throws IOException {
    write(_l + lineSep_);
    // fields.clear();
    number_++;
  }

  /**
   * Ecrit separateur de ligne.
   * 
   * @throws IOException
   */
  public void println() throws IOException {
    write(lineSep_);
    // fields.clear();
    number_++;
  }

  /**
   * @return le numero de ligne en cours
   */
  public int getLineNumber() {
    return number_;
  }

  private boolean stringQuoted_ = true;

  /**
   * @return true si les champs doivent etre quotes
   */
  public boolean isStringQuoted() {
    return stringQuoted_;
  }

  /**
   * @param _stringQuoted true si les champs doivent etre quotes
   */
  public void setStringQuoted(final boolean _stringQuoted) {
    stringQuoted_ = _stringQuoted;
  }

  /**
   * @return le separateur de ligne
   */
  public String getLineSeparator() {
    return lineSep_;
  }

  /**
   * @param _ls le nouveau sep de ligne
   */
  public void setLineSeparator(final String _ls) {
    lineSep_ = _ls;
  }

  /**
   * Ecrit une ligne en format libre contenant les champs definis.
   * 
   * @throws IOException
   */
  public void writeFields() throws IOException {
    FormatField o;
    if (buffer_ == null) {
      buffer_ = new StringBuffer(200);
    }
    // for (Iterator e= fields.iterator(); e.hasNext();) {
    // o= e.next();
    for (int i = 0; i <= nbMaxFields_; i++) {
      o = (FormatField) fields_.get(i);
      if (o != null) {
        buffer_.append(o.getStringNonFormate()).append(CtuluLibString.ESPACE);
      }
    }
    // out_.write(getLineSeparator());
    buffer_.append(getLineSeparator());
    buffer_ = write(buffer_);
    number_++;
    // fields.clear();
    nbMaxFields_ = -1;
    Collections.fill(fields_, null);
    // fields.
  }

  /**
   * Ecrit une ligne en format fixe contenant les champs definis.
   * 
   * @param _fmt le format des colonnes
   * @throws IOException
   */
  public void writeFields(final int[] _fmt) throws IOException {
    // int l= fields.size();
    if (buffer_ == null) {
      buffer_ = new StringBuffer(200);
    }
    String s;
    FormatField o;
    // for (int i= 0; i < l; i++) {
    for (int i = 0; i <= nbMaxFields_; i++) {
      o = (FormatField) fields_.get(i);
      s = CtuluLibString.EMPTY_STRING;
      if (o == null) {
        s = addSpacesAfter(_fmt[i], "");
      } else {
        s = o.getStringFormate(_fmt[i]);
      }
      // out_.write(s);
      buffer_.append(s);
    }
    // out_.write(getLineSeparator());
    buffer_.append(getLineSeparator());
    // Ecrit le buffer et le met a 0.
    buffer_ = write(buffer_);
    number_++;
    // fields.clear();
    nbMaxFields_ = -1;
    Collections.fill(fields_, null);
  }

  /**
   * Formate un reel (double ou float) pour qu'il tienne sur la longueur du champ.
   * 
   * @param _l le nombre de caractere voulu
   * @param _d l'objet reel ou float
   * @return la chaine correspondante formatee en _l caractere
   */
  public final static String formateReel(final int _l, final Object _d) {
    String r = _d.toString();
    if (r.length() >= _l) {
      // int i=0;
      // while (i<r.length() && r.charAt(i)!='E') i++;
      // if (i==r.length()) r=r.substring(0,lg);
      // else r=r.substring(0,lg-r.length()+i)+r.substring(i);
      final int i = r.indexOf('.');
      final int j = r.indexOf('E');
      if ((i < _l) && (j < 0)) {
        r = r.substring(0, _l);
      } else if (j > 0) {
        r = r.substring(0, _l - r.length() + j) + r.substring(j);
      }
    }
    return r;
  }

  /**
   * Ajoute des espace avant pour tenir en t caractere.Erreur si la chaine s est plus grande que t.
   * 
   * @param _t le nombre de caractere
   * @param _s la chaine a modifier
   * @return la chaine en t caractere
   */
  public final static String addSpacesBefore(final int _t, final String _s) {
    final int l = _s.length();
    final int lMotif = 5;
    final StringBuffer r = new StringBuffer(_t);
    if (l > _t) {
      FuLog.error("Impossible to write the value " + _s + " on a field which length is " + _t);
      final String a = "*****";
      final int nbMotif = _t / lMotif;
      final int sup = _t % lMotif;
      for (int i = nbMotif; i > 0; i--) {
        r.append(a);
      }
      addEtoiles(r, sup);
      return r.toString();
    }
    final String b = "     ";
    final int nbMotif = (_t - l) / lMotif;
    final int sup = (_t - l) % lMotif;
    for (int i = nbMotif; i > 0; i--) {
      r.append(b);
    }
    addSpaces(r, sup);
    r.append(_s);
    if (r.length() != _t) {
      FuLog.error("Length error :" + r.length() + " instead of " + _t);
    }
    return r.toString();
  }

  private static void addEtoiles(final StringBuffer _r, final int _sup) {
    switch (_sup) {
    case 0:
      break;
    case 1:
      _r.append('*');
      break;
    case 2:
      _r.append("**");
      break;
    case 3:
      _r.append("***");
      break;
    case 4:
      _r.append("****");
      break;
    default:
      break;
    }
  }

  /**
   * Construit une chaine de t caractere a partir de s en ajoutant des caractere apres. Erreur si t est trop petit.
   * 
   * @param _nbChar le nombre de caractere a obtenir
   * @param _chaineAModifier la chaine a transformer
   * @return la chaine en t caractere
   */
  public final static String addSpacesAfter(final int _nbChar, final String _chaineAModifier) {
    final int l = _chaineAModifier.length();
    final int lMotif = 5;
    final StringBuffer r = new StringBuffer(_nbChar);
    if (l > _nbChar) {
      FuLog.error("Impossible to write the value " + _chaineAModifier + " on a field which length is " + _nbChar);
      final String a = "*****";
      final int nbMotif = _nbChar / lMotif;
      final int sup = _nbChar % lMotif;
      for (int i = nbMotif; i > 0; i--) {
        r.append(a);
      }
      addEtoiles(r, sup);
      return r.toString();
    }
    r.append(_chaineAModifier);
    final String b = "     ";
    final int nbMotif = (_nbChar - l) / lMotif;
    final int sup = (_nbChar - l) % lMotif;
    for (int i = nbMotif; i > 0; i--) {
      r.append(b);
    }
    addSpaces(r, sup);
    if (r.length() != _nbChar) {
      FuLog.error("Length error " + r.length() + " instead of " + _nbChar);
    }
    return r.toString();
  }

  private static void addSpaces(final StringBuffer _r, final int _sup) {
    switch (_sup) {
    case 0:
      break;
    case 1:
      _r.append(' ');
      break;
    case 2:
      _r.append("  ");
      break;
    case 3:
      _r.append("   ");
      break;
    case 4:
      _r.append("    ");
      break;
    default:
      break;
    }
  }

  /**
   * Fixe la valeur du champ numero i avec un reel.
   * 
   * @param _indexDuChamp le numero du champ
   * @param _valeurDuChamp la valeur du champ de type double
   */
  public void doubleField(final int _indexDuChamp, final double _valeurDuChamp) {
    objectField(_indexDuChamp, getDoubleFormatField(_valeurDuChamp));
  }

  /**
   * Fixe la valeur du champ numero i avec un reel.
   * 
   * @param _indexDuChamp le numero du champ
   * @param _valeurDuChamp la valeur du champ de type float
   */
  public void floatField(final int _indexDuChamp, final double _valeurDuChamp) {
    objectField(_indexDuChamp, getFloatFormatField((float) _valeurDuChamp));
  }

  /**
   * Fixe la valeur du champ numero i avec un entier.
   * 
   * @param _indexDuChamp le numero du champ
   * @param _valeurDuChamp la valeur du champ de type int
   */
  public void intField(final int _indexDuChamp, final int _valeurDuChamp) {
    objectField(_indexDuChamp, getIntegerFormatField(_valeurDuChamp));
  }

  /**
   * Fixe la valeur du champ numero i avec une chaine de caracteres.
   * 
   * @param _indexDuChamp le numero du champ
   * @param _valeurDuChamp la valeur du champ de type string
   */
  public void stringField(final int _indexDuChamp, final String _valeurDuChamp) {
    objectField(_indexDuChamp, getStringFormatField(_valeurDuChamp));
  }

  private void objectField(final int _indexDuChamp, final Object _valeurDuChamp) {
    if (fields_.size() <= _indexDuChamp) {
      fields_.setSize(_indexDuChamp + 1);
    }
    // fields.ensureCapacity(_i+1);
    fields_.set(_indexDuChamp, _valeurDuChamp);
    if (_indexDuChamp > nbMaxFields_) {
      nbMaxFields_ = _indexDuChamp;
    }
  }

  private static interface FormatField {

    /**
     * @return la chaine non formatee
     */
    String getStringNonFormate();

    /**
     * @param _f le nombnre de caractere voulu
     * @return la chaine formate en _f colonne
     */
    String getStringFormate(int _f);
  }

  private final StringFormatField ffEmptyString_ = new StringFormatField(CtuluLibString.EMPTY_STRING);
  private final StringFormatField ffEspaceString_ = new StringFormatField(CtuluLibString.ESPACE);

  private StringFormatField getStringFormatField(final String _s) {
    if (_s == CtuluLibString.EMPTY_STRING) {
      return ffEmptyString_;
    }
    if (_s == CtuluLibString.ESPACE) {
      return ffEspaceString_;
    }
    if (isSpaceBefore_) {
      return new StringFormatFieldAvant(_s);
    }
    return new StringFormatField(_s);
  }

  private class StringFormatField implements FormatField {

    protected String s_;

    /**
     * @param _s la chaine initiale
     */
    public StringFormatField(final String _s) {
      s_ = _s;
    }

    @Override
    public String getStringFormate(final int _f) {
      return addSpacesAfter(_f, s_);
    }

    @Override
    public String getStringNonFormate() {
      if (isStringQuoted()) {
        String r = s_;
        if (r.indexOf('\'') >= 0) {
          for (int j = r.length() - 1; j >= 0; j--) {
            if (r.charAt(j) == '\'') {
              r = r.substring(0, j) + "''" + r.substring(j + 1);
            }
          }
        }
        r = "'" + r + "'";
        return r;
      }
      return s_;
    }
  }

  private final class StringFormatFieldAvant extends StringFormatField {

    /**
     * @param _s la chaine initiale
     */
    public StringFormatFieldAvant(final String _s) {
      super(_s);
    }

    @Override
    public String getStringFormate(final int _f) {
      return addSpacesBefore(_f, s_);
    }

  }

  private final DoubleFormatField doubleZero_ = new DoubleFormatField(0d);

  private DoubleFormatField getDoubleFormatField(final double _d) {
    if (_d == 0d) {
      return doubleZero_;
    }
    return new DoubleFormatField(_d);
  }

  private final FloatFormatField floatZero_ = new FloatFormatField(0f);

  private FloatFormatField getFloatFormatField(final float _d) {
    if (_d == 0f) {
      return floatZero_;
    }
    return new FloatFormatField(_d);
  }

  private static final class DoubleFormatField implements FormatField {

    private final double d_;

    /**
     * @param _d le double a formater
     */
    public DoubleFormatField(final double _d) {
      d_ = _d;
    }

    @Override
    public String getStringFormate(final int _f) {
      return addSpacesBefore(_f, formateReel(_f, Double.toString(d_)));
    }

    @Override
    public String getStringNonFormate() {
      return Double.toString(d_);
    }
  }

  private static final class FloatFormatField implements FormatField {

    private final float d_;

    /**
     * @param _d le float a formater
     */
    public FloatFormatField(final float _d) {
      d_ = _d;
    }

    @Override
    public String getStringFormate(final int _f) {
      return addSpacesBefore(_f, formateReel(_f, Float.toString(d_)));
    }

    @Override
    public String getStringNonFormate() {
      return Float.toString(d_);
    }
  }

  IntegerFormatField[] ffIntegers_;

  private void initFFInteger() {
    ffIntegers_ = new IntegerFormatField[7];
    for (int i = ffIntegers_.length - 1; i >= 0; i--) {
      ffIntegers_[i] = new IntegerFormatField(i);
    }
  }

  private IntegerFormatField getIntegerFormatField(final int _i) {
    if (ffIntegers_ == null) {
      initFFInteger();
    }
    return ((_i >= 0) && (_i < ffIntegers_.length)) ? ffIntegers_[_i] : new IntegerFormatField(_i);
  }

  private static final class IntegerFormatField implements FormatField {

    private final int d_;

    /**
     * @param _d l'entier a formater
     */
    public IntegerFormatField(final int _d) {
      d_ = _d;
    }

    @Override
    public String getStringFormate(final int _f) {
      return addSpacesBefore(_f, CtuluLibString.getString(d_));
    }

    @Override
    public String getStringNonFormate() {
      return CtuluLibString.getString(d_);
    }
  }
}
