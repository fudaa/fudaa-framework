/*
 * @creation 24 sept. 2004
 * @modification $Date: 2007-06-13 12:57:16 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.fortran;

import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.collection.CollectionPointDataDoubleInterface;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleAbstract;
import org.fudaa.ctulu.collection.CtuluListDouble;

/**
 * @author Fred Deniger
 * @version $Id: FortranDoubleReaderResult.java,v 1.22 2007-06-13 12:57:16 deniger Exp $
 */
public class FortranDoubleReaderResult implements FortranDoubleReaderResultInterface {

  final class Adapter implements CollectionPointDataDoubleInterface {

    Adapter() {

    }

    @Override
    public void fill(final int _idxValue, final CtuluListDouble _m) {
      _m.initWith(r_[_idxValue + 2]);

    }

    @Override
    public double getDoubleValue(final int _idxValue, final int _idxPt) {
      return r_[_idxValue + 2][_idxPt];
    }

    @Override
    public int getNbPoint() {
      return getNbLigne();
    }

    @Override
    public int getNbValues() {
      return getNbColonne() - 2;
    }

    @Override
    public double getX(final int _i) {
      return r_[0][_i];
    }

    @Override
    public double getY(final int _i) {
      return r_[1][_i];
    }

    @Override
    public double getZ(final int _i) {
      return 0;
    }
  }

  public static CtuluCollectionDouble getCollectionFor(final int _col, final FortranDoubleReaderResultInterface _res) {
    return new CtuluCollectionDoubleAbstract() {

      @Override
      public int getSize() {
        return _res.getNbLigne();
      }

      @Override
      public double getValue(int _i) {
        return _res.getValue(_i, _col);
      }

    };
  }

  /**
   * Les formats.
   */
  public CtuluNumberFormatI[] fmt_;

  public boolean isLineComplete_;

  /**
   * Le nombre de champ reellement lu.
   */
  public int nbField_;

  /**
   * Les valeurs lues en accord avec le format donne.
   */
  public double[][] r_;

  @Override
  public CollectionPointDataDoubleInterface createXYValuesInterface() {
    if (getNbColonne() < 2) {
      return null;
    }
    return new Adapter();
  }

  @Override
  public CtuluCollectionDouble getCollectionFor(final int _col) {
    return getCollectionFor(_col, this);
  }
  /**
   * @return null si fmt_ est null sinon renvoie fmt_[_col]
   */
  @Override
  public CtuluNumberFormatI getFormat(final int _col) {
    return fmt_ == null ? null : fmt_[_col];
  }

  @Override
  public int getNbColonne() {
    return r_.length;
  }

  @Override
  public int getNbLigne() {
    return getNbColonne() == 0 ? 0 : r_[0].length;
  }

  @Override
  public double getValue(final int _ligne, final int _col) {
    return r_[_col][_ligne];
  }
}
