package org.fudaa.dodico.fortran;

import com.memoire.fu.FuEmptyArrays;
import com.memoire.fu.FuLog;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteOrder;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.fileformat.FortranLib;

/**
 * Permet d'�crire facilement des tableaux d'entier, bool�ens dans un fichier binaire.
 *
 * @author Frederic Deniger
 */
public class DodicoArraysBinaryFileSaver {

  public DodicoArraysBinaryFileSaver() {
  }

  public boolean saveSimpleArray(File file, int[] in) {
    int[][] toSave = new int[1][];
    toSave[0] = in;
    return save(file, toSave);
  }

  public boolean saveSimpleArray(OutputStream file, int[] in) {
     int[][] toSave = new int[1][];
      toSave[0] = in;
      return save(file, toSave);
  }

  public boolean saveDouble(File file, double[] in) {
    boolean ok = false;
    FileOutputStream out = null;
    try {
      out = new FileOutputStream(file);
      ok = saveDouble(out, in);
    } catch (Exception exception) {
      ok = false;
      FuLog.error(exception);
    } finally {
      CtuluLibFile.close(out);
    }
    return ok;

  }

  public boolean saveDouble(OutputStream file, double[] in) {
    FortranBinaryOutputStream output = null;
    boolean ok = true;
    try {
      // FIXME B.M. Force a little endian pour correspondre anciennement a X86
      output = new FortranBinaryOutputStream(file, true, ByteOrder.LITTLE_ENDIAN);
      int nbValues = in.length;
      output.writeInteger(nbValues);
      output.writeRecord();
      for (int i = 0; i < nbValues; i++) {
        output.writeDoublePrecision(in[i]);
      }
      output.writeRecord();
    } catch (Exception e) {
      ok = false;
      FuLog.error(e);
    } finally {
    }
    return ok;
  }

  public int[] loadSimpleArray(File file) {
    int[][] res = load(file);
    return CtuluLibArray.isEmpty(res) ? FuEmptyArrays.INT0 : res[0];
  }

  public int[] loadSimpleArray(InputStream file) {
    int[][] res = load(file);
    return CtuluLibArray.isEmpty(res) ? FuEmptyArrays.INT0 : res[0];
  }

  private static int[] toInt(boolean[] in) {
    if (in == null) {
      return FuEmptyArrays.INT0;
    }
    int[] out = new int[in.length];
    for (int i = 0; i < out.length; i++) {
      out[i] = in[i] ? 1 : 0;
    }
    return out;
  }

  private static boolean[] toBoolean(int[][] in) {
    if (in == null || in.length == 0 || in[0].length == 0) {
      return FuEmptyArrays.BOOLEAN0;
    }
    final int[] values = in[0];
    boolean[] out = new boolean[values.length];
    for (int i = 0; i < out.length; i++) {
      out[i] = values[i] == 1 ? true : false;
    }
    return out;
  }

  public boolean saveBoolean(File file, boolean[] in) {
    int[][] toSave = new int[1][];
    toSave[0] = toInt(in);
    return save(file, toSave);
  }

  public boolean saveBoolean(OutputStream file, boolean[] in) {
    int[][] toSave = new int[1][];
    toSave[0] = toInt(in);
    return save(file, toSave);
  }

  public boolean[] loadBoolean(File file) {
    int[][] res = load(file);
    return toBoolean(res);
  }

  public boolean[] loadBoolean(InputStream file) {
    int[][] res = load(file);
    return toBoolean(res);
  }

  public int[][] load(File file) {
    FileInputStream input = null;
    int[][] res = null;
    try {
      input = new FileInputStream(file);
      res = load(input);
    } catch (Exception e) {
      FuLog.error(e);
    } finally {
      CtuluLibFile.close(input);
    }
    return res == null ? new int[0][0] : res;
  }

  public double[] loadDouble(File file) {
    FileInputStream input = null;
    double[] res = null;
    try {
      input = new FileInputStream(file);
      res = loadDouble(input);
    } catch (Exception e) {
      FuLog.error(e);
    } finally {
      CtuluLibFile.close(input);
    }
    return res == null ? FuEmptyArrays.DOUBLE0 : res;
  }

  public int[][] load(InputStream in) {
    FortranBinaryInputStream input = null;
    if (in == null) {
      return new int[0][0];
    }
    boolean ok = true;
    int[][] mapOfValues = null;
    try {
      try {
          // FIXME B.M. Force a little endian pour correspondre anciennement a X86
        input = new FortranBinaryInputStream(in, true, ByteOrder.LITTLE_ENDIAN);
        input.readRecord();
        int nbValues = input.readInteger();
        if (nbValues >= 0) {
          mapOfValues = new int[nbValues][];
          for (int i = 0; i < nbValues; i++) {
            input.readRecord();
            int nbItem = input.readInteger();
            if (nbItem < 0) {
              ok = false;
              break;
            }
            mapOfValues[i] = new int[nbItem];
            for (int j = 0; j < nbItem; j++) {
              mapOfValues[i][j] = input.readInteger();
            }
          }
        } else {
          ok = false;
        }
      } catch (Exception e) {
        FuLog.error(e);
      }

    } catch (Exception e) {
      FuLog.error(e);
    }
    if (ok) {
      return mapOfValues;
    }
    return new int[0][0];
  }

  public double[] loadDouble(InputStream in) {
    if (in == null) {
      return FuEmptyArrays.DOUBLE0;
    }
    boolean ok = true;
    double[] mapOfValues = null;
    FortranBinaryInputStream input = null;
    try {
      try {
          // FIXME B.M. Force a little endian pour correspondre anciennement a X86
        input = new FortranBinaryInputStream(in, true, ByteOrder.LITTLE_ENDIAN);
        input.readRecord();
        int nbValues = input.readInteger();
        input.readRecord();
        if (nbValues >= 0) {
          mapOfValues = new double[nbValues];
          for (int i = 0; i < nbValues; i++) {
            mapOfValues[i] = input.readDoublePrecision();
          }
        } else {
          ok = false;
        }
      } catch (Exception e) {
        FuLog.error(e);
      }

    } catch (Exception e) {
      FuLog.error(e);
    }
    if (ok) {
      return mapOfValues;
    }
    return FuEmptyArrays.DOUBLE0;
  }

  public boolean save(File file, int[][] mapOfValues) {
    boolean ok = false;
    FileOutputStream out = null;
    try {
      out = new FileOutputStream(file);
      ok = save(out, mapOfValues);
    } catch (Exception exception) {
      ok = false;
      FuLog.error(exception);
    } finally {
      CtuluLibFile.close(out);
    }
    return ok;
  }

  public boolean save(OutputStream out, int[][] mapOfValues) {
    return save(out, mapOfValues, false);

  }

  public boolean save(OutputStream out, int[][] mapOfValues, boolean close) {
    FortranBinaryOutputStream output = null;
    boolean ok = true;
    try {
        // FIXME B.M. Force a little endian pour correspondre anciennement a X86
      output = new FortranBinaryOutputStream(out, true, ByteOrder.LITTLE_ENDIAN);
      int nbValues = mapOfValues.length;
      output.writeInteger(nbValues);
      output.writeRecord();
      for (int i = 0; i < nbValues; i++) {
        int[] values = mapOfValues[i];
        output.writeInteger(values.length);
        for (int j = 0; j < values.length; j++) {
          output.writeInteger(values[j]);
        }
        output.writeRecord();
      }
    } catch (Exception e) {
      ok = false;
      FuLog.error(e);
    } finally {
      if (close) {
        FortranLib.close(output);
      }
    }
    return ok;

  }
}
