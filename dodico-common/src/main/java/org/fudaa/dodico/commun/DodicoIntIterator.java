/*
 *  @creation     7 oct. 2003
 *  @modification $Date: 2006-04-14 14:51:48 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.dodico.commun;

/**
 * Une interface definissant un iterateur sur une liste d'entier.
 *
 * @author deniger
 * @version $Id: DodicoIntIterator.java,v 1.6 2006-04-14 14:51:48 deniger Exp $
 */
public interface DodicoIntIterator {
  /**
   * @return true si l'iterateur possede une autre entree
   */
  boolean hasNext();

  /**
   * @return renvoie le prochain entier et incremente l'iterateur.
   */
  int next();
}
