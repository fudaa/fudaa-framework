/*
 * @creation     2002-11-20
 * @modification $Date: 2006-09-19 14:42:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.dodico.commun;
import com.memoire.bu.BuPreferences;
import org.fudaa.ctulu.CtuluLibString;
/**
 * @version      $Id$
 * @author       Fred Deniger
 */
public class DodicoPreferences extends BuPreferences {
  /**
   * singleton pour les prefs.
   */
  public final static BuPreferences DODICO;
  
  static {
    if (root_==null) {
      DODICO= new DodicoPreferences();
    }
    else
      DODICO=BU;
  }

  /**
   * le separateur pour les champs des cles.
   */
  protected DodicoPreferences() {}


  /**
   * @param _deb le premier champ de la cle
   * @param _fin le dernier champ de la cle
   * @return _deb._fin
   */
  public static String buildPrefKey(final String _deb, final String _fin) {
    return buildPrefKey(_deb, _fin, false);
  }
  /**
   * @param _deb le premier champ
   * @param _fin le deuxieme champ
   * @param _sepFin true s'il faut ajouter un separateur a la fin
   * @return _deb._fin
   */
  public static String buildPrefKey(
    final String _deb,
    final String _fin,
    final boolean _sepFin) {
    final   StringBuffer b= new StringBuffer(_deb.length() + _fin.length() + 2);
    b.append(_deb).append(CtuluLibString.DOT).append(_fin);
    if (_sepFin) {
      b.append(CtuluLibString.DOT);
    }
    return b.toString();
  }

  /**
   * @param _a le premiere partie
   * @param _b la deuxieme
   * @param _c la troisieme
   * @return _a._b._c
   */
  public static String buildPrefKey(final String _a, final String _b, final String _c) {
    return buildPrefKey(_a, _b, _c, false);
  }
  /**
  * @param _a le premiere partie
   * @param _b la deuxieme
   * @param _c la troisieme
   * @param _sepFin true si on doit ajouter un sep a la fin
   * @return _a._b._c ou _a._b._c.
   */
  public static String buildPrefKey(
    final String _a,
    final String _b,
    final String _c,
    final boolean _sepFin) {
    final StringBuffer b=
      new StringBuffer(_a.length() + _b.length() + _c.length() + 1);
    b.append(_a).append(CtuluLibString.DOT).append(_b).append(CtuluLibString.DOT).append(
      _c);
    if (_sepFin) {
      b.append(CtuluLibString.DOT);
    }
    return b.toString();
  }
}
