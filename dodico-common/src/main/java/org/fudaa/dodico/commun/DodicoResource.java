/*
 * @creation     2002-11-20
 * @modification $Date: 2006-09-19 14:42:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.dodico.commun;

import com.memoire.bu.BuResource;
import org.fudaa.ctulu.CtuluResource;

/**
 * @version $Id: DodicoResource.java,v 1.14 2006-09-19 14:42:29 deniger Exp $
 * @author Fred Deniger
 */
public class DodicoResource extends CtuluResource {
  public final static DodicoResource DODICO = new DodicoResource(CtuluResource.CTULU);

  protected DodicoResource(final BuResource _parent) {
    setParent(_parent);
  }
}
