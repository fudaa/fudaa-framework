/*
 *  @creation     2002-12-02
 *  @modification $Date: 2006-09-19 14:42:29 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.dodico.commun;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuTextField;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * Un panel d'edition pour les preferences des serveurs Dodico. Permet de sp�cifier l'adresse du serveur, les numeros de
 * ports utilises, le masque de sous-reseau... Il sera possible de specifier la connexion automatique au serveur local.
 *
 * @version $Id: DodicoPreferencesPanel.java,v 1.14 2006-09-19 14:42:29 deniger Exp $
 * @author Fred Deniger
 */
public class DodicoPreferencesPanel extends BuAbstractPreferencesPanel implements DocumentListener {

  /** Le champ texte contenant l'adresse du serveur. */
  BuTextField nsAddr_;
  /** Le numero de port TCP. */
  BuTextField tcpPort_;
  /** Le champ texte pour l'adresse du sous-reseau. */
  BuTextField udpAddr_;
  /** Le numero de port UDP. */
  BuTextField udpPort_;

  // BuCheckBox connexionLocale_;
  /** Construit le panel et initialise les preferences. */
  public DodicoPreferencesPanel() {
    super();
    initPanel();
  }

  /** Relit le fichier des prefs et met a jour l'affichage. */
  @Override
  public void cancelPreferences() {
    DodicoPreferences.DODICO.readIniFile();
    updateComponents();
  }

  private BuTextField createIntegerField(final String _s) {
    final BuTextField r = BuTextField.createIntegerField();
    r.setToolTipText(DodicoLib.getS(_s));
    return r;
  }

  private BuLabel createLabel(final String _s) {
    final BuLabel r = new BuLabel(DodicoLib.getS(_s));
    r.setVerticalTextPosition(SwingConstants.CENTER);
    return r;
  }

  private BuTextField createTextField(final String _s) {
    final BuTextField r = new BuTextField();
    r.getDocument().addDocumentListener(this);
    r.setToolTipText(DodicoLib.getS(_s));
    return r;
  }

  // Methodes privees
  /** Enregistrement dans la hastables des preferences editees par ce panel. */
  private void fillTable() {
    putNsAddress(nsAddr_.getText());
    // putLOCAL_ADDR(localAddr_.getText());
    putUdpAddress(udpAddr_.getText());
    putTcpPort(Integer.parseInt(tcpPort_.getText()));
    putUdpPort(Integer.parseInt(udpPort_.getText()));
    // putCONNEXION_LOCAL(connexionLocale_.isSelected());
  }

  /**
   * @return Title
   */
  @Override
  public String getTitle() {
    return DodicoLib.getS("Serveur de calcul");
  }

  /** Construction du panel. */
  private void initPanel() {
    setLayout(new BuGridLayout(2, 5, 5));
    /*
     * BuLabel cl = createTextField("Connexion locale par d�faut")); //Pour l'instant cl.setEnabled(false); add(cl);
     * connexionLocale_ = new BuCheckBox(); connexionLocale_.setEnabled(false); connexionLocale_.setToolTipText("Si
     * activ�, le serveur local sera automatiquement choisi"));
     */
    /*
     * add(createLabel("Adresse locale")); localAddr_ = createTextField("L'adresse locale de la machine");
     */
    add(createLabel("Adresse du serveur principal"));
    nsAddr_ = createTextField("L'adresse IP du serveur principal");
    add(nsAddr_);
    add(createLabel("Port TCP"));
    tcpPort_ = createIntegerField("Le port TCP (r�ception et envoi)");
    add(tcpPort_);
    add(createLabel("Adresse UDP"));
    udpAddr_ = createTextField("L'adresse sur laquelle les datagrammes d'informations seront envoy�s par le protocole UDP");
    add(udpAddr_);
    add(createLabel("Port UDP"));
    udpPort_ = createIntegerField("Le port UDP (r�ception et envoi)");
    add(udpPort_);
    updateComponents();
    setDirty(false);
  }

  /**
   * les preferences sont-elles applicables.
   *
   * @return PreferencesApplyable
   */
  @Override
  public boolean isPreferencesApplyable() {
    return false;
  }

  /**
   * Renvoie true si les preferences peuvent etre annulees.
   *
   * @return PreferencesCancelable
   */
  @Override
  public boolean isPreferencesCancelable() {
    return true;
  }

  /**
   * @return PreferencesValidable
   */
  @Override
  public boolean isPreferencesValidable() {
    return true;
  }

  /** Maj des champs de ce panel a partir des preferences. */
  private void updateComponents() {
    nsAddr_.setText(getNsAddress());
    udpAddr_.setText(getUdpAddress());
    // localAddr_.setText(getLOCAL_ADDR());
    tcpPort_.setText(String.valueOf(getTcpPort()));
    udpPort_.setText(String.valueOf(getUdpPort()));
    // connexionLocale_.setSelected(getCONNEXION_LOCAL());
  }

  /** Maj a jour la hastable des preferences et le fichier. */
  @Override
  public void validatePreferences() {
    fillTable();
    DodicoPreferences.DODICO.writeIniFile();
    setDirty(false);
  }

  /**
   * Renvoie, a partir des preferences, du type de connexion par defaut. Si true la connexion sera par defaut local.
   *
   * @return le type de connexion ( valeur par defaut true).
   */
  public final static boolean getConnexionLocal() {
    return DodicoPreferences.DODICO.getBooleanProperty("dodico.local.connexion", true);
  }

  /**
   * Renvoie, a partir des preferences, l'adresse locale de la machine de l'utilisateur.
   *
   * @return l'adresse local ( "127.0.0.1" par defaut).
   */
  public final static String getLocalAddress() {
    return DodicoPreferences.DODICO.getStringProperty("dodico.local.addr", "127.0.0.1");
  }

  /**
   * Renvoie, a partir des preferences, l'adresse du serveur de calcul.
   *
   * @return l'adresse du serveur ( "171.17.250.82" par defaut).
   */
  public final static String getNsAddress() {
    return DodicoPreferences.DODICO.getStringProperty("dodico.ns.addr", "172.17.250.82");
  }

  /**
   * Renvoie, a partir des preferences, le port TCP du diffuseur.
   *
   * @return le port TCP lu dans les preferences ( defaut =14001).
   */
  public final static int getTcpPort() {
    return DodicoPreferences.DODICO.getIntegerProperty("dodico.tcp.port", 14001);
  }

  /**
   * Renvoie, a partir des preferences, l'adresse IP pour la diffusion UDP. Elle correspond au masque de sous-reseau.
   *
   * @return l'adresse du sous-reseau ( par defaut "172.17.0.0").
   */
  public final static String getUdpAddress() {
    return DodicoPreferences.DODICO.getStringProperty("dodico.udp.addr", "172.17.0.0");
  }

  /**
   * Renvoie, a partir des preferences, le port UDP du diffuseur.
   *
   * @return le port UDP lu dans les preferences ( defaut =14001).
   */
  public final static int getUdpPort() {
    return DodicoPreferences.DODICO.getIntegerProperty("dodico.udp.port", 14001);
  }

  /**
   * Mise a jour du type de connexion. Si true, le serveur local sera automatiquement choisi.
   *
   * @param _b
   */
  public final static void putConnexionLocal(final boolean _b) {
    DodicoPreferences.DODICO.putBooleanProperty("dodico.local.connexion", _b);
  }

  /**
   * Mise a jour de l'adresse locale dans les preferences ( il faut effectuer la commande preferences.writeIniFile
   * ensuite pour mettre a jour le fichier ).
   *
   * @param _addr
   */
  public final static void putLocalAddress(final String _addr) {
    if (_addr != null) {
      DodicoPreferences.DODICO.putStringProperty("dodico.local.addr", _addr);
    }
  }

  /**
   * Mise a jour de l'adresse du serveur dans les preferences ( il faut effectuer la commande writeIniFile ensuite pour
   * mettre a jour le fichier ).
   *
   * @param _addr
   */
  public final static void putNsAddress(final String _addr) {
    if (_addr != null) {
      DodicoPreferences.DODICO.putStringProperty("dodico.ns.addr", _addr);
    }
  }

  /**
   * Mise a jour du port tcp dans les preferences ( il faut effectuer la commande writeIniFile ensuite pour mettre a
   * jour le fichier ).
   *
   * @param _n
   */
  public final static void putTcpPort(final int _n) {
    DodicoPreferences.DODICO.putIntegerProperty("dodico.tcp.port", _n);
  }

  /**
   * Mise a jour de l'adresse UDP dans les preferences ( il faut effectuer la commande writeIniFile ensuite pour mettre
   * a jour le fichier ).
   *
   * @param _addr
   */
  public final static void putUdpAddress(final String _addr) {
    if (_addr != null) {
      DodicoPreferences.DODICO.putStringProperty("dodico.udp.addr", _addr);
    }
  }

  /**
   * Mise a jour du port udp dans les preferences ( il faut effectuer la commande writeIniFile ensuite pour mettre a
   * jour le fichier ).
   *
   * @param _n
   */
  public final static void putUdpPort(final int _n) {
    DodicoPreferences.DODICO.putIntegerProperty("dodico.udp.port", _n);
  }

  /**
   * @see javax.swing.event.DocumentListener#changedUpdate(javax.swing.event.DocumentEvent)
   */
  @Override
  public void changedUpdate(final DocumentEvent _e) {
    setDirty(true);
  }

  /**
   * @see javax.swing.event.DocumentListener#insertUpdate(javax.swing.event.DocumentEvent)
   */
  @Override
  public void insertUpdate(final DocumentEvent _e) {
    setDirty(true);
  }

  /**
   * @see javax.swing.event.DocumentListener#removeUpdate(javax.swing.event.DocumentEvent)
   */
  @Override
  public void removeUpdate(final DocumentEvent _e) {
    setDirty(true);
  }

}
