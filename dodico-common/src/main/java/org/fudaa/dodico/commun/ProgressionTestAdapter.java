/*
 * @creation 8 ao�t 2003
 * @modification $Date: 2006-09-19 14:42:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.commun;

import org.fudaa.ctulu.ProgressionInterface;

/**
 * Une classe implementant tres simplement ProgressionInterface: les modifs sont ecrites sur la sortie standard.
 *
 * @author deniger
 * @version $Id: ProgressionTestAdapter.java,v 1.11 2006-09-19 14:42:29 deniger Exp $
 */
public class ProgressionTestAdapter implements ProgressionInterface {
  final boolean sysout_;

  public ProgressionTestAdapter(final boolean _sysout) {
    super();
    sysout_ = _sysout;
  }

  public ProgressionTestAdapter() {
    this(true);
  }

  @Override
  public void setProgression(final int _v) {
    if(sysout_) {
      System.out.println("progress : " + _v);
    }
  }

  @Override
  public void setDesc(final String _s) {
    if(sysout_) {
      System.out.println("progress desc : " + _s);
    }
  }

  @Override
  public void reset() {}

}
