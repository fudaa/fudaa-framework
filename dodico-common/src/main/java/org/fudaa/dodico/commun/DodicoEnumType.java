/**
 * @creation 26 juin 2003
 * @modification $Date: 2006-09-29 09:10:15 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.commun;

import java.util.Iterator;
import java.util.List;

/**
 * @author deniger
 * @version $Id: DodicoEnumType.java,v 1.13 2006-09-29 09:10:15 deniger Exp $
 */
public abstract class DodicoEnumType implements Comparable {

  String nom_;

  /**
   * @param _nom le nom decrivant l'enum
   */
  public DodicoEnumType(final String _nom) {
    nom_ = _nom;
  }

  DodicoEnumType() {}

  /**
   * @return le nom decrivant l'enum
   */
  public String getName() {
    return nom_;
  }

  /**
   * @see java.lang.Object#toString()
   * @return getNom
   */
  public String toString() {
    return nom_;
  }

  /**
   * Teste si la classe est identique puis teste l'identifiant.
   * 
   * @see java.lang.Object#equals(java.lang.Object)
   */
  public boolean equals(final Object _o) {
    if (_o == this) {
      return true;
    }
    if (_o == null) {
      return false;
    }
    if (_o.getClass().equals(getClass())) {
      return nom_.equals(((DodicoEnumType) _o).nom_);
    }
    return false;
  }

  public int hashCode() {
    return getClass().hashCode() * 7 + nom_.hashCode() * 101;
  }

  /**
   * Compare selon le nom de le nom de l'enumeration. Si les objets sont differents mais possedent le meme nom, le
   * hashcode est compare.
   */
  @Override
  public int compareTo(final Object _obj) {
    if (_obj == this) {
      return 0;
    }
    if (_obj instanceof DodicoEnumType) {
      final int r = nom_.compareTo(((DodicoEnumType) _obj).nom_);
      if (r == 0) {
        return hashCode() - _obj.hashCode();
      }
      return r;
    }
    throw new IllegalArgumentException("compareTo " + getClass().getName());
  }

  /**
   * @param _l la liste contenant des enum
   * @param _id l'id a retrouver
   * @return l'enum contenu dans la liste et dont l'id est _id. null si aucun.
   */
  public static DodicoEnumType getIdInList(final List _l, final String _id) {
    for (final Iterator it = _l.iterator(); it.hasNext();) {
      final DodicoEnumType e = (DodicoEnumType) it.next();
      if (e.nom_.equals(_id)) {
        return e;
      }
    }
    return null;
  }

}
