/*
 *  @creation     22 sept. 2005
 *  @modification $Date: 2006-09-19 14:42:29 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.commun;


public final class DodicoLib {

  private DodicoLib(){}

  /**
   * A "shortcut" to get i18n String.
   * @param _s la chaine a traduire
   * @return DODICO.getString(_s);
   */
  public static String getS(final String _s) {
    return DodicoResource.DODICO.getString(_s);
  }

  /**
   * A "shortcut" to get i18n String.
   * @param _s la chaine a traduire
   * @param _v0 le remplacant pour {0}
   * @return DODICO.getString(_s);
   */
  public static String getS(final String _s, final String _v0) {
    return DodicoResource.DODICO.getString(_s, _v0);
  }

  /**
   * A "shortcut" to get i18n String.
   * @param _s la chaine a traduire.
   * @param _v0 le remplacant pour {0}
   * @param _v1 le remplacant pour {1}
   * @return DODICO.getString(_s);
   */
  public static String getS(final String _s, final String _v0, final String _v1) {
    return DodicoResource.DODICO.getString(_s, _v0, _v1);
  }

}
