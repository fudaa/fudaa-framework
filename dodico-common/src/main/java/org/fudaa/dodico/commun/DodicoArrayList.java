/*
 *  @creation     27 ao�t 2003
 *  @modification $Date: 2006-09-19 14:42:29 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.dodico.commun;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Une arraylist permettant de specifier la taille de la liste.
 *
 * @author deniger
 * @version $Id: DodicoArrayList.java,v 1.10 2006-09-19 14:42:29 deniger Exp $
 */
public final class DodicoArrayList extends ArrayList {
  /**
   * @param _initialCapacity la capacite initiale
   */
  public DodicoArrayList(final int _initialCapacity) {
    super(_initialCapacity);
  }

  /**
   * constructeur par defaut.
   */
  public DodicoArrayList() {
    super();
  }

  /**
   * @param _c la collection a ajouter
   */
  public DodicoArrayList(final Collection _c) {
    super(_c);
  }

  /**
   * Permet de specifier la nouvelle taille de la liste. Si elle est inferieure a la taille actuelle les indices en trop
   * sont enleve. Sinon, null est ajoute pour obtenir la taille demandee.
   *
   * @param _i la nouvelle taille de la liste.
   */
  public void setSize(final int _i) {
    final int oldS = size();
    if ((_i == oldS) || (_i < 0)) {
      return;
    } else if (_i == 0) {
      clear();
    } else if (_i < oldS) {
      removeRange(_i, oldS);
    } else {
      addAll(Collections.nCopies(_i - oldS, null));
    }
  }
}
