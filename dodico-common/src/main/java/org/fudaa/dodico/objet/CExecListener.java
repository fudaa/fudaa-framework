/**
 *  @creation     13 juin 2003
 *  @modification $Date: 2006-09-19 14:44:21 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.objet;

/**
 * Une interface qui permet a une classe de recupere le process d'une execution immediatement apres son lancement et
 * eventuellement avant la commande waitFor.
 * 
 * @author fred deniger
 * @version $Id: CExecListener.java,v 1.6 2006-09-19 14:44:21 deniger Exp $
 */
public interface CExecListener {
  /**
   * @param _p le process a recuperer.
   */
  void setProcess(Process _p);
}
