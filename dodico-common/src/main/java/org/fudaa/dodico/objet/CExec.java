/**
 * @creation 1999-07-21
 * @modification $Date: 2007-03-27 16:10:00 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.objet;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.commun.DodicoLib;

/**
 * Thread qui permet de lire les donnees d'un BufferReader et de les ecrire dans un PrintStream. Il se peut que ce
 * thread soit bloqu� si le flux d'entree du BufferedReader ne soit jamais initialise
 */

class MyReaderThread extends Thread {

  private boolean blocked_;
  private BufferedReader br_;
  private boolean finish_;

  private PrintStream str_;

  /**
   * @param _str le flux dans lequel les donnees du buffer seront ecrites
   * @param _br le buffer lu ( en general un buffer de sortie de process).
   */
  public MyReaderThread(final PrintStream _str, final BufferedReader _br) {
    super();
    blocked_ = true;
    str_ = _str;
    br_ = _br;
    setPriority(Thread.MAX_PRIORITY - 2);
  }

  /**
   * Essai de stopper le thread.
   */
  public void stopTheThread() {
    finish_ = true;
  }

  /**
   * Si le thread est lance et si cette commande renvoie true, cela signifie que le thread est bloque sur la premiere
   * commande readLine. Dans ce cas, il est conseille d'interrompre le thread.
   * 
   * @return true si bloque
   */
  public boolean isBlocked() {
    return blocked_;
  }

  /**
   * Lancement du thread de lecture: tant que le buffer contient des lignes elles sont renvoyees dans le PrintStream.
   */
  @Override
  public void run() {
    int charLu = -1;
    try {
      blocked_ = true;
      // sous windows le thread de lecture peut bloquer.
      // On attend que le buffer puisse etre lu.
      while ((!br_.ready()) && (!finish_)) {
        try {
          Thread.sleep(500);
        } catch (final InterruptedException e) {}
      }
      blocked_ = false;
      if ((finish_) && (!br_.ready())) {
        return;
      }
      charLu = br_.read();
      if (str_ == null) {
        while ((charLu != -1)) {
          charLu = br_.read();
        }
      } else {
        while ((charLu != -1)) {
          str_.write(charLu);
          charLu = br_.read();
        }
      }
    } catch (final IOException _io) {
      FuLog.warning(_io);
    } finally {
      if (str_ != null) {
        str_.flush();
      }
    }
  }
}

/**
 * Un lanceur de processus externes. Lance la commande contenue dans le tableau de String[] dans un nouveau process. 2
 * Threads autres sont lances pour lire les flux de sortie standard et d'erreur. Ils sont arretes a la fin du process.
 * Ces threads sont obligatoire pour vider les buffers de sorties du processus ( surtout sous windows). Il se peut qu'un
 * thread soit bloque : si un flux n'est pas ouvert la methode readline() reste bloqu�. Dans ce cas, le thread concern�
 * sera interrompu.
 * 
 * @version $Id: CExec.java,v 1.25 2007-03-27 16:10:00 deniger Exp $
 * @author Axel von Arnim
 */

public class CExec {

  private Process p_;
  boolean catchE_;

  String[] cmd_;
  File execDirectory_;
  CExecListener listener_;
  PrintStream out_, err_;
  MyReaderThread thOut_, thErr_;

  public final String[] getEnvs() {
    return envs_;
  }

  public final void setEnvs(final String[] _envs) {
    envs_ = _envs;
  }

  /**
   * Commandes a null et les exceptions seront catch�s et ecrites sur la sortie d'erreur.
   */
  public CExec() {
    catchE_ = true;
    out_ = System.out;
    err_ = System.err;
  }

  /**
   * @param _cmd les parametres du processus a lancer.
   * @see java.lang.Runtime#exec(String[])
   */
  public CExec(final String[] _cmd) {
    this();
    setCommand(_cmd);
  }

  /**
   * Permet de fermer les threads de lecture apres l'arret du process. Si le thread est bloqu�, il est interrompu.
   * Sinon, s'il n'est pas ferm�, on attend 50 ms avant de l'interrompre. Normalement les thread de lecture doivent
   * s'arreter par eux-meme avant la fin du process.
   */
  private void closeReaderThread(final MyReaderThread _th) {
    try {
      if (_th == null) {
        return;
      }
      if (!_th.isAlive()) {
        return;
      }
      try {
        _th.join(500);
      } catch (final InterruptedException _e) {}
      if (!_th.isAlive()) {
        return;
      }
      // il est encore vivant, on attend
      _th.setPriority(Thread.MAX_PRIORITY - 2);
      // sous windows, il arrivre que la lecture du flux soit bloquee.
      if (_th.isBlocked()) {
        if (CtuluLibMessage.DEBUG && (err_ != null)) {
          err_.println(_th.getName() + " " + DodicoLib.getS("bloqu� "));
        }
      }
      try {
        _th.join(500);
      } catch (final InterruptedException _e) {}
      // on a attendu 50ms, on interrompt le thread
      if (_th.isAlive()) {
        _th.stopTheThread();
        try {
          _th.join(500);
        } catch (final InterruptedException _e) {}
      }
    } catch (final SecurityException _e) {
      CtuluLibMessage.error(_e.getMessage());
    }
  }

  String[] envs_;

  /**
   * Lance la command dans une process. 2 Threads sont lances pour lire les flux de sortie standard et d'erreur. Ils
   * sont arretes a la fin du process.
   */
  public void exec() {
    if ((cmd_ == null) || (cmd_.length <= 0)) {
      if (err_ != null) {
        err_.println(DodicoLib.getS("Commande nulle"));
      }
      return;
    }

    final Thread currentThread = Thread.currentThread();
    final int oldpriority = currentThread.getPriority();
    try {
      if (err_ != null) {
        err_.println("*****  " + cmd_[0] + " " + DodicoLib.getS("lance") + " *****");
      }
      currentThread.setPriority(Thread.MIN_PRIORITY);
      if (Fu.DEBUG && FuLog.isDebug()) FuLog.debug("DCE: launch in " + (execDirectory_ == null ? "null" : execDirectory_
          .getAbsolutePath()));
      if (execDirectory_ == null) {
        p_ = Runtime.getRuntime().exec(cmd_, envs_);
      } else {
        // DodicoLib.printStringArray(cmd_);
        System.out.println("lancement de "+CtuluLibString.arrayToString(cmd_)+" dans "+execDirectory_.getAbsolutePath());

        p_ = Runtime.getRuntime().exec(cmd_, envs_, execDirectory_);
      }
      if (listener_ != null) {
        listener_.setProcess(p_);
      }
      if (err_ != null) {
        if (CtuluLibMessage.DEBUG) {
          CtuluLibMessage.debug("CEXEC lecture sortie erreur");
        }
        BufferedReader psErr = null;
        psErr = new BufferedReader(new InputStreamReader(p_.getErrorStream()));
        thErr_ = new MyReaderThread(err_, psErr);
        thErr_.setName(DodicoLib.getS("Sortie d'erreur"));
        thErr_.start();
      }
      if (out_ != null) {
        if (CtuluLibMessage.DEBUG) {
          CtuluLibMessage.debug("CEXEC lecture sortie sortie standart");
        }
        BufferedReader psIn = null;
        psIn = new BufferedReader(new InputStreamReader(p_.getInputStream()));
        thOut_ = new MyReaderThread(out_, psIn);
        thOut_.setName(DodicoLib.getS("Sortie standard"));
        thOut_.start();
      }
      boolean interr = true;
      while (interr) {
        try {
          p_.waitFor();
          interr = false;
        } catch (final InterruptedException e) {
          interr = true;
        }
      }
      closeReaderThread(thOut_);
      closeReaderThread(thErr_);
      if (p_.exitValue() != 0) {
        throw new RuntimeException(cmd_[0] + ": abnormal exit");
      }
      if (err_ != null) {
        err_.println("*****  " + cmd_[0] + " " + DodicoLib.getS("termine") + "  *****");
      }
    } catch (final IOException io) {
      if (catchE_) {
        FuLog.warning(io);
      } else {
        throw new RuntimeException(io.getMessage());
      }
    } catch (final RuntimeException r) {
      System.err.println("runtime exception");
      if (catchE_) {
        System.err.println(r);
      } else {
        throw r;
      }
    } finally {
      currentThread.setPriority(oldpriority);
    }
    if (CtuluLibMessage.DEBUG) {
      CtuluLibMessage.debug("CExec.exec() terminee");
    }
    if (nextProcess_ != null) nextProcess_.run();
  }

  Runnable nextProcess_;

  /**
   * @return le rep dans lequel l'exe sera lance
   */
  public File getExecDirectory() {
    return execDirectory_;
  }

  /**
   * @return le listener
   */
  public CExecListener getListener() {
    return listener_;
  }

  /**
   * @return le processus d'execution. Sera null si l'exec n'est pas lance
   */
  public Process getProcess() {
    return p_;
  }

  /**
   * Specifie si les exceptions doivent etre "catches" ou non. Si elles sont catchees une trace est laiss�e sur la
   * sortie d'erreur.
   * 
   * @param _catchE true si exception catchees. Si false, les exceptions sont renvoyees.
   */
  public void setCatchExceptions(final boolean _catchE) {
    catchE_ = _catchE;
  }

  /**
   * Initialisation des commandes.
   * 
   * @param _cmd les commandes de l'exec
   */
  public void setCommand(final String[] _cmd) {
    cmd_ = _cmd;
  }

  /**
   * Definition du flux recuperant la sortie d'erreur.
   * 
   * @param _err le flux d'erreur
   */
  public void setErrStream(final PrintStream _err) {
    err_ = _err;
  }

  /**
   * @param _file le repertoire dans lequel l'executable sera lance.
   */
  public void setExecDirectory(final File _file) {
    execDirectory_ = _file;
  }

  /**
   * @param _listener
   */
  public void setListener(final CExecListener _listener) {
    listener_ = _listener;
  }

  /**
   * @param _out le flux sortie standard
   */
  public void setOutStream(final PrintStream _out) {
    out_ = _out;
  }

  public Runnable getNextProcess() {
    return nextProcess_;
  }

  public void setNextProcess(Runnable _nextProcess) {
    nextProcess_ = _nextProcess;
  }

}
