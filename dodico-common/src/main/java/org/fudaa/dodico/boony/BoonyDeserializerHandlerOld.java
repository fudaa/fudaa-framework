/*
 * @creation 21 avr. 2004
 * @modification $Date: 2007-11-20 11:43:12 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.boony;

import com.memoire.mst.MstHandlerBase;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Stack;
import java.util.TreeMap;
import org.fudaa.dodico.boony.BoonyXmlDeserializer.CorbaEnum;

class BoonyDeserializerHandlerOld extends MstHandlerBase {

  Stack arrays_;
  private final Map classNameTypeData_ = new TreeMap();

  private StringBuffer data_ = new StringBuffer(128);
  private final Map dobjetIObjet_;
  private final Stack fields_;
  private boolean isCorba_ = true;
  private final Object nullValue_ = new Object();
  private final Map obj_; // (id,obj)
  private List objectNotSet_ = new ArrayList();

  private final Stack objects_;

  private final Map options_ = new HashMap();
  private final Map ref_; // (obj,id)

  private String singleClass_;
  private List singles_;

  private final Map typeClass_;

  /**
   * @param _containsCorba true, si les objets corba doivent etre geree.
   */
  public BoonyDeserializerHandlerOld(final boolean _containsCorba) {
    isCorba_ = _containsCorba;
    ref_ = new HashMap();
    obj_ = new HashMap();
    objects_ = new Stack();
    fields_ = new Stack();
    arrays_ = new Stack();
    singles_ = new ArrayList();
    dobjetIObjet_ = new HashMap();
    typeClass_ = new TreeMap();
    typeClass_.put("boolean", Boolean.TYPE);
    typeClass_.put("char", Character.TYPE);
    typeClass_.put("byte", Byte.TYPE);
    typeClass_.put("short", Short.TYPE);
    typeClass_.put("int", Integer.TYPE);
    typeClass_.put("long", Long.TYPE);
    typeClass_.put("float", Float.TYPE);
    typeClass_.put("double", Double.TYPE);
    typeClass_.put("void", Void.TYPE);

  }

  Object buildTieFromTieName(final String _tieClass) {
    Object  r;
    TypeData d = (TypeData) classNameTypeData_.get(_tieClass);
    if (d == null) {
      d = new TypeData();
      classNameTypeData_.put(_tieClass, d);
    }
    try {
      if (d.tieConstructor_ == null) {
        Class tieClass = d.c_;
        if (tieClass == null) {
          tieClass = Class.forName(_tieClass);
          d.c_ = tieClass;
        }
        final Constructor[] cs = tieClass.getConstructors();
        for (int i = cs.length - 1; i >= 0; i--) {
          if (cs[i].getParameterTypes().length == 2) {
            d.tieConstructor_ = cs[i];
            break;
          }
        }
      }
      r =  d.tieConstructor_.newInstance(new Object[] { null,
          (BoonyLib.isAllLocal() ? null : BoonyLib.getPOA()) });
    } catch (final Exception e) {
      e.printStackTrace();
      return null;
    }
    return r;
  }

  protected Field getField(final Class _cOld, final String _name) {
    final String s = _cOld.getName();
    TypeData d = (TypeData) classNameTypeData_.get(s);
    if (d == null) {
      d = new TypeData();
      d.c_ = _cOld;
      classNameTypeData_.put(s, d);
    }
    Field f = (Field) d.nomField_.get(_name);
    if (f == null) {
      f = searchField(_cOld, _name);
      d.nomField_.put(_name, f);
    }
    return f;
  }

  protected Object getFinalObjectToSet(final Object _r) {
    if ((dobjetIObjet_ == null) || (_r == null)) {
      return _r;
    } else if (dobjetIObjet_.containsKey(_r)) {
      Object o = dobjetIObjet_.get(_r);
      if (o == null) {
        o = getDelegate( _r);
        if (o == null) {
          return null;
        }
        dobjetIObjet_.put(_r, o);
      }
      return o;
    } else {
      return _r;
    }
  }

  /**
   * @param _s la classe deleguee (le tie)
   * @return le DObjet utilise
   */
  public Object getDelegate(final Object _s) {
    Object r = null;
    try {
      final Field f = getField(_s.getClass(), "_impl");
      f.setAccessible(true);
      r = f.get(_s);
    } catch (final SecurityException e) {
      e.printStackTrace();
    } catch (final IllegalAccessException e) {
      e.printStackTrace();
    }
    return r;
  }

  protected final Object getObj(final String _id) {
    if ("null".equals(_id)) {
      return null;
    }
    final Object r = obj_.get(_id);
    if (r == null) {
      System.out.println("probleme");
    }

    return r;
  }

  protected Class normalize(final String _type) {
    Class r = (Class) typeClass_.get(_type);
    if (r == null) {
      try {
        r = getClass(_type);
      } catch (final ClassNotFoundException ex) {
        BoonyLib.warning("class not found for " + _type);
      }
    }
    if (r == null) {
      r = Object.class;
    }
    return r;
  }

  protected void saveCorbaObject(final Object _dobjet) {
    dobjetIObjet_.put(_dobjet, null);
  }

  protected Field searchField(final Class _c, final String _name) {
    if (_c == null) {
      return null;
    }
    final Field[] fs = _c.getDeclaredFields();
    if (fs != null) {
      for (int i = fs.length - 1; i >= 0; i--) {
        if (fs[i].getName().equals(_name)) {
          return fs[i];
        }
      }
    }
    return searchField(_c.getSuperclass(), _name);
  }

  /**
   * @see com.memoire.mst.MstXmlHandler#attribute(java.lang.String, java.lang.String, boolean)
   */
  @Override
  public void attribute(final String _attribut, final String _valeur, final boolean _specifie) throws Exception {
    if (_specifie) {
      options_.put(_attribut, _valeur);
    }
  }

  @Override
  public void charData(final char[] _ch, final int _start, final int _length) throws Exception {
    data_.append(_ch, _start, _length);
  }

  @Override
  public void endDocument() throws Exception {
    super.endDocument();
    if (objectNotSet_ != null) {
      for (int i = objectNotSet_.size() - 1; i >= 0; i--) {
        ((SpecialRemember) objectNotSet_.get(i)).go();
      }
    }
  }

  @Override
  public void endElement(final String _oldElement) throws Exception {
    if ("array-byte".equals(_oldElement)) {
      endElementTypeArray();
      return;
    }
    final String data = data_.toString().trim();
    /*if (!"".equals(data)) {
      //trace(data);
    }*/

    if ("yapod".equals(_oldElement)) {
      // rien
    } else if ("object".equals(_oldElement)) {
      endElementObject();
    } else if ("field".equals(_oldElement)) {
      endElementField();
    } else if ("null".equals(_oldElement)) {
      singles_.add(nullValue_);
    } else if ("single".equals(_oldElement)) {
      endElementSingle(data);
      // data="";
    } else if ("array".equals(_oldElement)) {
      endElementArray();
    } else if ("reference".equals(_oldElement)) {} else {
      BoonyLib.warning("unreconized tag: " + _oldElement);
    }
  }

  private void endElementArray() {
    final Object a = arrays_.pop();
    final int l = Array.getLength(a);
    if (singles_.size() != l) {
      BoonyLib.warning("singles.size!=array.length (" + singles_.size() + ")");
    }
    for (int i = 0; i < l; i++) {
      Object r = singles_.get(i);
      if (r == nullValue_) {
        r = null;
      }
      final Object fr = getFinalObjectToSet(r);
      if (fr != null) {
        Array.set(a, i, fr);
      } else if (r != null) {
        final ArrayRemember ar = new ArrayRemember();
        ar.array_ = a;
        ar.idx_ = i;
        ar.valueToSet_ = r;
        if (objectNotSet_ == null) {
          objectNotSet_ = new ArrayList(20);
        }
        objectNotSet_.add(ar);
      }
    }
    singles_ = (List) arrays_.pop();
    singles_.add(a);
  }

  private void endElementSingle(final String _data) {
    Object o = nullValue_;
    final Class c = normalize(singleClass_);

    if (c == Boolean.class) {
      o = _data.equalsIgnoreCase("true") ? Boolean.TRUE : Boolean.FALSE;
    } else if (c == Character.class) {
      try {
        o = new Character((char) new Integer(_data).intValue());
      } catch (final NumberFormatException ex) {
        BoonyLib.warning("invalid char value " + _data);
      }
    } else if (c == String.class) {
      o = BoonyLib.fromXmlCharset(_data);
    } else { // Number
      try {
        final Constructor x = c.getDeclaredConstructor(new Class[] { String.class });
        x.setAccessible(true);
        o = x.newInstance(new Object[] { _data });
      } catch (final NoSuchMethodException ex) {
        BoonyLib.warning("constructor(string) not found for class " + singleClass_);
      } catch (final IllegalAccessException ex) {
        BoonyLib.warning("constructor(string) not accessible " + singleClass_);
      } catch (final InstantiationException ex) {
        BoonyLib.warning("constructor(string) not instantiate " + singleClass_);
      } catch (final InvocationTargetException ex) {
        BoonyLib.warning("constructor(string) not target !" + singleClass_);
      }
    }

    //trace(o);
    singles_.add(o);
  }

  private void endElementField() {
    final Field f = (Field) fields_.pop();

    if (singles_.size() != 1) {
      BoonyLib.warning("singles.size!=1 (" + singles_.size() + ")");

    }
    Object r = singles_.get(0);
    if (r == nullValue_) {
      r = null;

    }
    if (f != null) {
      try {
        final Object o = getFinalObjectToSet(r);
        if (o != null) {
          f.set(objects_.peek(), o);
        } else if (r != null) {
          final FieldRemember fr = new FieldRemember();
          fr.f_ = f;
          fr.oDest_ = objects_.peek();
          fr.valueToSet_ = r;
          if (objectNotSet_ == null) {
            objectNotSet_ = new ArrayList(20);
          }
          objectNotSet_.add(fr);
        }

      } catch (final IllegalAccessException ex) {
        System.err.println("### no access to field " + f.getName() + " in " + objects_.peek().getClass());
      }
    }

    singles_ = (List) fields_.pop();
  }

  private void endElementObject() {
    Object o = objects_.pop();
    if (o instanceof CorbaEnum) {
      final Object id = ref_.get(o);

      Object n = ((CorbaEnum) o).build();
      if (n == null) {
        System.err.println("### failed to build enum ");
        n = nullValue_;
      }
      o = n;
      ref_.put(o, id);
      obj_.put(id, o);
    }
  }

  private void endElementTypeArray() {
    final int l = data_.length();
    final byte[] o = new byte[l];
    for (int k = 0; k < l; k++) {
      o[k] = (byte) data_.charAt(k);
    }
    singles_.add(o);
    data_ = new StringBuffer(32);
    System.gc();
  }

  @Override
  public void error(final String _message, final String _systemId, final int _line, final int _column) throws Exception {
    System.err.println("Yapod XML error  : " + _message);
    super.error(_message, _systemId, _line, _column);
  }

  /**
   * Utilise une map pour se ne pas refaire le calcul.
   * 
   * @param _className le nom de la classe
   * @return la classe trouvee
   * @throws ClassNotFoundException
   */
  public Class getClass(final String _className) throws ClassNotFoundException {
    TypeData d = (TypeData) classNameTypeData_.get(_className);
    if (d == null) {
      d = new TypeData();
      classNameTypeData_.put(_className, d);
    }
    if (d.c_ == null) {
      d.c_ = Class.forName(_className);
    }
    return d.c_;
  }

  /**
   * @see org.fudaa.dodico.boony.BoonyDeserializerHandlerInterface#getFirstObject()
   */
  public Object getFirstObject() {
    if (singles_.size() == 0) {
      throw new NoSuchElementException();
    }
    Object r = singles_.get(0);
    if (r == nullValue_) {
      r = null;
    }
    singles_.remove(0);
    return getFinalObjectToSet(r);
  }

  /**
   * @param _className le nom de la classe
   * @return true si c'est un tie
   */
  public boolean isTieType(final String _className) {
    if (!isCorba_) {
      return false;
    }
    TypeData d = (TypeData) classNameTypeData_.get(_className);
    if (d == null) {
      d = new TypeData();
      classNameTypeData_.put(_className, d);
    }
    if (d.isTie_ == null) {
      d.isTie_ = _className.endsWith("_Tie") ? Boolean.TRUE : Boolean.FALSE;
    }
    return d.isTie_.booleanValue();
  }

  /**
   * @see com.memoire.mst.MstXmlHandler#startElement(java.lang.String)
   */
  @Override
  public void startElement(final String _element) throws Exception {
    //trace(_element);
    if ("yapod".equals(_element)) {} else if ("object".equals(_element)) {
      startElementObject();
    }

    else if ("field".equals(_element)) {
      startElementField();
    } else if ("array-byte".equals(_element)) {} else if ("null".equals(_element)) {} else if ("single"
        .equals(_element)) {
      singleClass_ = (String) options_.get("type");
    } else if ("array".equals(_element)) {
      startElementArray();
    } else if ("reference".equals(_element)) {
      final String id = (String) options_.get("idref");

      Object o = getObj(id);
      if (o == null) {
        o = nullValue_;
      }
      singles_.add(o);
    } else {
      BoonyLib.warning("unreconized tag: " + _element);

      // TMP @GDX
      // if(!"".equals(data_.toString().trim()))
      // BoonyLib.warning("data should be empty: "+data_);

      // Slow on 1.4
      // data_.setLength(0);
      // Change to:
    }
    data_ = new StringBuffer(32);
    options_.clear();
  }

  private void startElementArray() {
    final String type = (String) options_.get("type");
    final String id = (String) options_.get("id");
    final int length = Integer.parseInt((String) options_.get("length"));
    final int depth = Integer.parseInt((String) options_.get("depth"));

    final Class c = normalize(type);

    final int[] dims = new int[depth];
    dims[0] = length;
    final Object a = Array.newInstance(c, dims);
    //trace(a);

    if (id != null) {
      ref_.put(a, id);
      obj_.put(id, a);
    }

    arrays_.push(singles_);
    arrays_.push(a);
    singles_ = new ArrayList(10);
  }

  private void startElementField() {
    final String name = (String) options_.get("name");

    final Object o = objects_.peek();
    final Class c = o.getClass();
    final Field f = getField(c, name);
    if (f != null) {
      f.setAccessible(true);
    }
    //  trace(f);

    fields_.push(singles_);
    fields_.push(f);
    singles_ = new ArrayList();

    if (f == null) {
      /*
       * System.err.println("### field " + name + " removed in " + objects.peek().getClass());
       */
    }
  }

  private void startElementObject() {
    final String type = (String) options_.get("type");
    final String id = (String) options_.get("id");
    Object o = nullValue_;
    Class c = null;
    final boolean isTie = isTieType(type);
    try {
      c = getClass(type);
    } catch (final ClassNotFoundException ex) {
      BoonyLib.warning("class not found " + type);
    }

    if (c != null) {
      try {

        if (isTie) {
          o = buildTieFromTieName(type);
          saveCorbaObject(o);
        } else {
          final Constructor x = c.getDeclaredConstructor(new Class[] {});
          x.setAccessible(true);
          o = x.newInstance(new Object[] {});
        }
        //trace(o);
      } catch (final Exception ex) {}

      // CORBA Enum ?
      if (o == nullValue_) {
        if (isCorbaEnum(c)) {
          o = new CorbaEnum(c);
          //trace(o);
        }
      }
      if (o == nullValue_) {
        BoonyLib.warning("default constructor not found for class " + type);
      }
    }

    if (id != null) {
      ref_.put(o, id);
      obj_.put(id, o);
    }

    objects_.push(o);
  }

/*  *//**
   * @param _o l'objet a tracer
   *//*
  public void trace(final Object _o) {
  // try { System.err.println("Yapod XML trace : "+_o); }
  // catch(Throwable th) { }
  }*/

  /**
   * utilise une map interne pour enregistrer l'etat d'une classe.
   * 
   * @param _c la classe a tester
   * @return true si corba enum
   */
  public boolean isCorbaEnum(final Class _c) {
    final String className = _c.getName();
    TypeData d = (TypeData) classNameTypeData_.get(className);
    if (d == null) {
      d = new TypeData();
      classNameTypeData_.put(className, d);
    }
    if (d.isCorbaEnum_ == null) {
      d.isCorbaEnum_ = ((BoonyXmlDeserializer.isCorbaEnum(_c)) ? Boolean.TRUE : Boolean.FALSE);
    }
    return d.isCorbaEnum_.booleanValue();
  }

  /**
   * Reaffecte un element d'une matrice.
   * 
   * @author Fred Deniger
   * @version $Id: BoonyDeserializerHandlerOld.java,v 1.16 2007-11-20 11:43:12 bmarchan Exp $
   */
  public class ArrayRemember implements SpecialRemember {

    Object array_;
    int idx_;
    Object valueToSet_;

    /**
     * @overrride
     */
    @Override
    public void go() throws Exception {
      final Object r = getFinalObjectToSet(valueToSet_);
      if (r != null) {
        Array.set(array_, idx_, r);

      } else {
        new Throwable().printStackTrace();
      }
    }
  }

  /**
   * Reaffecte un champ.
   * 
   * @author Fred Deniger
   * @version $Id: BoonyDeserializerHandlerOld.java,v 1.16 2007-11-20 11:43:12 bmarchan Exp $
   */
  public class FieldRemember implements SpecialRemember {

    Field f_;
    Object oDest_;
    Object valueToSet_;

    @Override
    public void go() throws Exception {
      final Object r = getFinalObjectToSet(valueToSet_);
      if (r != null) {
        f_.set(oDest_, r);

      } else {
        new Throwable().printStackTrace();
      }
    }
  }

  /**
   * Dans le cas des references cycliques, certains d�l�gu�s ne sont pas encore initialise au moment d'une affectation.
   * Les sauvegardes de ces cas se font sous cette interface
   * 
   * @author Fred Deniger
   * @version $Id: BoonyDeserializerHandlerOld.java,v 1.16 2007-11-20 11:43:12 bmarchan Exp $
   */
  public interface SpecialRemember {

    /**
     * Retente l'affectation.
     * 
     * @throws Exception
     */
    void go() throws Exception;
  }

  public static class TypeData {

    Class c_;
    Boolean isTie_;
    Boolean isCorbaEnum_;
    final Map nomField_ = new HashMap();
    Constructor tieConstructor_;
  }
}
