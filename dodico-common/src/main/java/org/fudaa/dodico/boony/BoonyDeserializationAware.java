/*
 GPL 2
 */
package org.fudaa.dodico.boony;

/**
 *
 * @author Frederic Deniger
 */
public interface BoonyDeserializationAware {

  void endDeserialization();

}
