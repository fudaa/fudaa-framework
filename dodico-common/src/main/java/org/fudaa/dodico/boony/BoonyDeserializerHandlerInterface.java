/**
 *  @creation     23 avr. 2004
 *  @modification $Date: 2007-11-20 11:43:12 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.boony;

import org.xml.sax.ContentHandler;


/**
 * @author Fred Deniger
 * @version $Id: BoonyDeserializerHandlerInterface.java,v 1.7 2007-11-20 11:43:12 bmarchan Exp $
 */
public interface BoonyDeserializerHandlerInterface extends ContentHandler {

  /**
   * @return l'objet du bas de la pile
   */
  Object getFirstObject();

}
