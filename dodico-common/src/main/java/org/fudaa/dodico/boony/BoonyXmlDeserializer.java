/**
 *  @file         DodicoXmlDeserializer.java
 *  @creation     19 avr. 2004
 *  @modification $Date: 2007-12-10 13:13:18 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.boony;

import com.memoire.fu.FuLog;
import com.memoire.mst.MstXmlHandler;
import com.memoire.mst.MstXmlParser;
import com.memoire.yapod.YapodDeserializer;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Hashtable;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * @author Fred Deniger
 * @version $Id: BoonyXmlDeserializer.java,v 1.16 2007-12-10 13:13:18 jm_lacombe Exp $
 */
public class BoonyXmlDeserializer implements YapodDeserializer {

  protected final Object nullValue_ = new Object();
  /**
   * l'encoding par defaut.
   */
  public static final String ENCODING = "UTF-8"; // "ISO-8859-1"
  private BoonyDeserializerHandlerInterface handler_;
  private boolean isCorba_ = true;

  private Reader inReader_;

  /**
   * this(true).
   */
  public BoonyXmlDeserializer() {
    this(true);
  }

  /**
   * @param _isCorba true si contient des objets corba
   */
  public BoonyXmlDeserializer(final boolean _isCorba) {
    isCorba_ = _isCorba;
  }

  @Override
  public synchronized void open(final InputStream _in) throws IOException {
    InputStream inStr = _in;
    if (!(inStr instanceof BufferedInputStream)) {
      inStr = new BufferedInputStream(_in, BUFFER_SIZE);

    }
    inReader_ = new InputStreamReader(inStr, ENCODING);
    handler_ = null;
  }

  @Override
  public synchronized void close() throws IOException {
    inReader_.close();
  }

  protected String dir_;
  protected String root_;


  /**
   * D�finit le handler.
   */
  public void setHandler(BoonyDeserializerHandlerInterface _handler) {
    handler_=_handler;
  }

  @Override
  public synchronized Object read() throws IOException, ClassNotFoundException {
    if (handler_ == null)
     handler_ = new BoonyDeserializerHandler(isCorba_);

    readAll(handler_);
    return handler_.getFirstObject();
  }

  /**
   * Lecture speciale pour les anciens projets mascaret.
   *
   * @return l'iobjet
   * @throws IOException si io exception
   * @throws ClassNotFoundException si classe non trouvee
   */
  public synchronized Object readOld() throws IOException, ClassNotFoundException {

//    if (handler_ == null) {
    BoonyDeserializerHandlerOld handler = new BoonyDeserializerHandlerOld(isCorba_);
      readAllOld(handler);
      // readAllOld(new CopyOfBoonyDeserializerHandlerOld(false));
//    }
    return handler.getFirstObject();
  }

  private synchronized void readAll(final ContentHandler _handler) {

    // MstXmlParser parser = new MstXmlParser();

    // avec jdk1.4
    try {
      final SAXParserFactory parser = SAXParserFactory.newInstance();
      parser.setNamespaceAware(false);
      parser.setValidating(false);
      parser.setFeature("http://xml.org/sax/features/namespaces", false);
      parser.setFeature("http://xml.org/sax/features/namespace-prefixes", false);
      parser.setFeature("http://xml.org/sax/features/lexical-handler/parameter-entities", false);
      final SAXParser saxparser = parser.newSAXParser();
      saxparser.getXMLReader().setContentHandler(_handler);
      final InputSource s = new InputSource(inReader_);
      s.setEncoding(ENCODING);
      saxparser.getXMLReader().parse(s);
      // avec Piccolo : il y a des problemes avec Piccolo
      /*
       * Piccolo parser = new Piccolo(); parser.setFeature("http://xml.org/sax/features/namespaces", false);
       * parser.setFeature("http://xml.org/sax/features/namespace-prefixes", false);
       * parser.setFeature("http://xml.org/sax/features/lexical-handler/parameter-entities", false); //features pour
       * picollo parser.setFeature("http://xml.org/sax/features/external-general-entities", false);
       * parser.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
       * parser.setFeature("http://xml.org/sax/features/resolve-dtd-uris", false); InputSource s = new InputSource(in);
       * s.setEncoding(ENCODING); parser.parse(s);
       */
    } catch (final SAXException _evt) {
      FuLog.error(_evt);
    } catch (final ParserConfigurationException _evt) {
      FuLog.error(_evt);

    } catch (final IOException _evt) {
      FuLog.error(_evt);

    }

  }

  private synchronized void readAllOld(final MstXmlHandler _handler) {

    final MstXmlParser parser = new MstXmlParser();

    parser.setHandler(_handler);

    try {
      parser.parse("", "", inReader_);
    } catch (final Exception ex) {
      ex.printStackTrace();
    }

  }

  /**
   * Class speciale pour les enum corba.
   *
   * @author Fred Deniger
   * @version $Id: BoonyXmlDeserializer.java,v 1.16 2007-12-10 13:13:18 jm_lacombe Exp $
   */
  public static final class CorbaEnum {

    // NE PAS MODIFIER CE NOM: IL EST ACCEDE PAR REFLEXION
    // CHECKSTYLE:OFF
    private int __value;
    // CHECKSTYLE:ON
    private Class clazz_;

    /**
     * @param _clazz la classe de l'enum
     */
    public CorbaEnum(final Class _clazz) {
      clazz_ = _clazz;
      __value = -1;
    }

    /**
     * @return l'objet construit
     */
    public Object build() {
      Object o = null;
      try {
        final Method m = clazz_.getDeclaredMethod("from_int", new Class[] { Integer.TYPE });
        m.setAccessible(true);
        o = m.invoke(null, new Object[] { new Integer(__value) });
      } catch (final NoSuchMethodException ex) {
        FuLog.error(ex);
      } catch (final IllegalAccessException ex) {} catch (final IllegalArgumentException ex) {} catch (final InvocationTargetException ex) {}
      return o;
    }

    public String toString() {
      return "enum " + clazz_.getName() + (__value == -1 ? "" : "(" + __value + ")");
    }
  }

  /**
   * @param _c la class a tester
   * @return true si contient une methode statique from_int
   */
  public static final boolean isCorbaEnum(final Class _c) {
    boolean r = false;
    try {
      final Method m = _c.getDeclaredMethod("from_int", new Class[] { Integer.TYPE });

      r = ((m.getModifiers() & Modifier.STATIC) != 0) && (m.getReturnType() == _c);
    } catch (final NoSuchMethodException ex) {}
    return r;
  }

  @Override
  public String extension() {
    return "xml";
  }

  @Override
  public Object retrieve(final String _id, final Object _dbcx, final Hashtable _ref, final Hashtable _obj,
                         final Hashtable _nmo) throws Exception {
    new Throwable().printStackTrace();
    return null;
  }
}
