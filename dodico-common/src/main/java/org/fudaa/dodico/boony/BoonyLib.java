/*
 *  @creation     28 avr. 2004
 *  @modification $Date: 2007-11-20 11:43:12 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.dodico.boony;

import com.memoire.fu.FuLog;

/**
 * @author Fred Deniger
 * @version $Id: BoonyLib.java,v 1.13 2007-11-20 11:43:12 bmarchan Exp $
 */
public final class BoonyLib {
  private static BoonyInterface bonnyImpl;

  private BoonyLib() {
    super();
  }

  /**
   * Initialise l'implementation si elle n'est pas d�j� initialis�e.
   *
   * @param _impl la nouvelle impl
   */
  public static void setBoonyImpl(final BoonyInterface _impl) {
    if (bonnyImpl != null) {
      throw new IllegalAccessError("impl already set");
    }
    bonnyImpl = _impl;
  }

  /**
   * @return l'impl utilisee
   */
  public static BoonyInterface getBoonyImpl() {
    return bonnyImpl;
  }

  /**
   * @return le POA qui permet de retrouver tous les objets corba
   */
  public static Object getPOA() {
    return bonnyImpl.getPOA();
  }
  public static Object getReferentToServant(Object o) {
    return bonnyImpl.getReferentToServant(o);
  }

  /**
   * BoonyLib peut etre en mode "tout local" ce qui permet d'optimiser les manipulations des objets corba.
   *
   * @return true si fonctionnement purement local.
   */
  public static boolean isAllLocal() {
    return bonnyImpl.isAllLocal();
  }

  static Object buildStubFromDObject(final Object _dobjet) {
    return bonnyImpl.buildStubFromDelegate(_dobjet);
  }

  public static void warning(final String _s) {
    FuLog.warning("Yapod XML warning: " + _s);
  }

  public static String fromXmlCharset(final String _s) {
    final StringBuffer r = new StringBuffer(_s.length());
    int i = 0;
    int j = 0;
    while ((j = _s.indexOf("&", i)) >= 0) {
      if (i != j) {
        r.append(_s.substring(i, j));
      }
      i = j;
      final int k = _s.indexOf(";", i);
      if (k <= 0) {
        warning("suspicious single &");
        i++;
        r.append('&');
        continue;
      }
      final String t = _s.substring(i + 1, k);
      if ("#32".equals(t)) {
        r.append(' ');
      } else if ("lt".equals(t)) {
        r.append('<');
      } else if ("gt".equals(t)) {
        r.append('>');
      } else if ("quot".equals(t)) {
        r.append('"');
      } else if ("amp".equals(t)) {
        r.append('&');
      } else if (t.charAt(0) == '#') {
        try {
          final char c = (char) Integer.parseInt(t.substring(1));
          r.append(c);
        } catch (final NumberFormatException ex) {
          warning("invalid char value &" + t + ";");
          // System.err.println("invalid char value &"+t+";");
          r.append('&');
          r.append(t);
          r.append(';');
        }
      } else {
        warning("invalid entity &" + t + ";");
        // System.err.println("invalid entity &"+t+";");
        r.append('&');
        r.append(t);
        r.append(';');
      }
      i = k + 1;
    }
    r.append(_s.substring(i));
    return r.toString();
  }

  public static boolean isCorbaObject(Object o) {
return  bonnyImpl.isCorbaObject(o);
//    return (_o instanceof org.omg.CORBA.Object);

  }
}
