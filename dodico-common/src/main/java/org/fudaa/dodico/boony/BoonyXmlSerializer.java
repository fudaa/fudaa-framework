/*
 * @modification $Date: 2008-02-11 10:11:50 $
 * @statut unstable
 * @version 0.17
 * @author Guillaume Desnoix
 * @email guillaume@desnoix.com
 * @license GNU General Public License 2 (GPL2)
 */

package org.fudaa.dodico.boony;

import com.memoire.fu.FuLog;
import com.memoire.yapod.YapodLib;
import com.memoire.yapod.YapodSerializer;
import org.fudaa.ctulu.CtuluLibString;

import java.io.*;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/**
 * @author Fred Deniger
 * @version $Id: BoonyXmlSerializer.java,v 1.22 2008-02-11 10:11:50 jm_lacombe Exp $
 */
public class BoonyXmlSerializer implements YapodSerializer {
  /**
   * L'encoding par defaut.
   */
  public static final String ENCODING = "UTF-8"; // "ISO-8859-1"

  protected static final String toXmlCharset(final char _c) {
    String r;

    if (_c == '<') {
      r = "&lt;";
    } else if (_c == '>') {
      r = "&gt;";
    } else if (_c == ' ') {
      r = "&#32;";
    } else if (_c == '&') {
      r = "&amp;";
    } else if (_c == '"') {
      r = "&quot;";
    } else if (Character.isISOControl(_c) || (_c == '\'') || (_c == '\"') || (_c > 254)) {
      r = "&#" + (int) _c + ";";
    } else {
      r = "" + _c;
    }
    return r;
  }

  protected static final String toXmlCharset(final String _s) {
    final StringBuffer r = new StringBuffer(2 * _s.length());
    final int l = _s.length();

    for (int i = 0; i < l; i++) {
      r.append(toXmlCharset(_s.charAt(i)));
    }

    return r.toString();
  }

  /**
   * @param _s le message d'avertissement
   */
  public static void warning(final String _s) {
    FuLog.error("Yapod XML warning: " + _s);
  }

  private final Map classAttr_ = new HashMap();
  private final Map classFields_ = new HashMap();
  private final Object fakeValue_ = new Object();
  private int idbase_;
  private final boolean isCorba_;
  private final boolean istabDoubleCDATA_;
  private Map obj_; // (id,obj)
  private Writer out_;
  private OutputStream outInit_;
  private Map ref_; // (obj,id)
  private Object root_;
  protected int indentation_;
  /**
   * Pas d'indentation.
   */
  public boolean isIdent_;

  /**
   * this(true).
   */
  public BoonyXmlSerializer() {
    this(true, false);
  }

  /**
   * @param _containsCorba true si l'objet contient des objets corba
   */
  public BoonyXmlSerializer(final boolean _containsCorba) {
    this(_containsCorba, false);
  }

  /**
   * @param _containsCorba true si l'objet contient des objets corba
   * @param _istabDoubleCDATA true si l'on s�rialise les tableaux de double de facon compacte (CDATA)
   */
  public BoonyXmlSerializer(final boolean _containsCorba, final boolean _istabDoubleCDATA) {
    isCorba_ = _containsCorba;
    istabDoubleCDATA_ = _istabDoubleCDATA;
  }

  private String getDepthAttr(final Object _value) {
    if (_value == null) {
      return CtuluLibString.EMPTY_STRING;
    }

    Class c = _value.getClass();
    while (!c.isArray()) {
      return CtuluLibString.EMPTY_STRING;
    }
    int n = 0;
    while (c.isArray()) {
      c = c.getComponentType();
      n++;
    }
    return " depth=\"" + n + '"';
  }

  private String getEnd() {
    return ">";
  }

  private String getFin() {
    return "\">\n";
  }

  private String getIdStr() {
    return " id=\"";
  }

  private Object getObjectToDo(final Object _ref) {
    if (BoonyLib.isAllLocal()) {
      return _ref;
    }
    Object r = null;
    try {
      r = BoonyLib.getReferentToServant(_ref);
    } catch (final Exception _e) {
      _e.printStackTrace();
    }
    return getDelegate(r);
  }

  private String getSingleEnd() {
    return "</single>\n";
  }

  private String getSingleStr() {
    return "<single";
  }

  private String getTypeAttr(final Object _value) {
    if (_value == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    final Class c = _value.getClass();
    if (classAttr_.containsKey(c)) {
      return (String) classAttr_.get(c);
    }
    final String r = getTypeAttrFromClass(c);
    classAttr_.put(c, r);
    return r;
  }

  private String getTypeAttrFromClass(final Class _c) {
    Class cEnCours = _c;
    while (cEnCours.isArray()) {
      cEnCours = cEnCours.getComponentType();
    }
    return " type=\"" + cEnCours.getName() + '"';
  }

  Object getDelegate(final Object _s) {
    Object r = null;
    try {
      final Field[] fs = _s.getClass().getDeclaredFields();
      Field f = null;
      for (int i = 0; i < fs.length; i++) {
        if ("_impl".equals(fs[i].getName())) {
          f = fs[i];
          break;
        }
      }
      if (f == null) {
        return null;
      }
      f.setAccessible(true);
      r = f.get(_s);
    } catch (final SecurityException _e) {
      _e.printStackTrace();
    } catch (final IllegalAccessException _e) {
      _e.printStackTrace();
    }
    return r;
  }

  protected String getId(final Object _o) {
    if (_o == null) {
      return "null";
    }

    String r = (String) ref_.get(_o);

    if (r == null) {
      do {
        r = "RO-" + idbase_;
        idbase_++;
      } while (obj_.get(r) != null);
      ref_.put(_o, r);
      obj_.put(r, _o);
    }

    return r;
  }

  /**
   * @return rien
   */
  protected String indente() {
    return CtuluLibString.EMPTY_STRING;
  }

  protected final void output(final Writer _w, final String _o) throws IOException {
    /*
     * ByteBuffer b=encoder.encode(CharBuffer.wrap(_o)); byte[] bs=new byte[b.limit()]; b.get(bs); output0(_w, bs);
     */
    _w.write(_o);
  }

  protected synchronized void writeArray(final Object _o, final Writer _w) throws IOException {
    final int l = Array.getLength(_o);

    if (isIdent_) {
      output(_w, indente());
    }
    output(_w, "<array");
    output(_w, getTypeAttr(_o));
    output(_w, getDepthAttr(_o));
    output(_w, getIdStr());
    output(_w, getId(_o));
    output(_w, "\" length=\"");
    output(_w, "" + l);
    output(_w, getFin());
    indentation_++;

    for (int i = 0; i < l; i++) {
      write(Array.get(_o, i), _w);
    }

    indentation_--;
    if (isIdent_) {
      output(_w, indente());
    }
    output(_w, "</array>\n");
  }

  protected synchronized void writeBoolean(final Boolean _o, final Writer _w) throws IOException {
    if (isIdent_) {
      output(_w, indente());
    }
    output(_w, "<single type=\"Bool\">");
/*    output(_w, getSingleStr());
    output(_w, getTypeAttr(_o));
    output(_w, getEnd());*/
    output(_w, _o.toString());
    output(_w, getSingleEnd());
  }

  protected synchronized void writeBytes(final byte[] _o, final Writer _w) throws IOException {
    if (isIdent_) {
      output(_w, indente());
    }
    output(_w, "<array-byte><![CDATA[");
    out_.flush();
    // Base64Encoder.encode(outInit,_o,0,_o.length);
    outInit_.write(BoonyBase64.encode(_o));
    outInit_.flush();
    output(_w, "]]></array-byte>\n");
  }

  protected synchronized void writeCharacter(final Character _o, final Writer _w) throws IOException {
    if (isIdent_) {
      output(_w, indente());
    }
    output(_w, "<single type=\"Char\">");
/*    output(_w, getSingleStr());
    output(_w, getTypeAttr(_o));
    output(_w, getEnd());*/
    output(_w, "" + (int) _o.charValue());
    output(_w, getSingleEnd());
  }

  protected synchronized void writeDoubles(final double[] _o, final Writer _w) throws IOException {

    final int l = _o.length;

    output(_w, "<array type=\"double\"");
    output(_w, getDepthAttr(_o));
    output(_w, getIdStr());
    output(_w, getId(_o));
    output(_w, "\" length=\"");
    output(_w, Integer.toString(l));
    output(_w, getFin());

    for (int i = 0; i < l; i++) {
      output(_w, "<single type=\"Double\">");
      _w.write(Double.toString(_o[i]));
      output(_w, getSingleEnd());
    }

    output(_w, "</array>\n");
  }

  protected synchronized void writeDoublesCDATA(final double[] _o, final Writer _w) throws IOException {
    if (isIdent_) {
      output(_w, indente());
    }
    output(_w, "<array-double><![CDATA");

    //out_.flush();
//    tabDoubleToString(out_,_o);
    tabDoubleToString(_w, _o);
    //outInit_.flush();
    output(_w, "]></array-double>\n");
  }

  private static final void tabDoubleToString(Writer _w, double[] a) throws IOException {
    if (a == null) {
      _w.write("null");
      return;
    }
    int iMax = a.length - 1;
    if (iMax == -1) {
      _w.write("[]");
      return;
    }
    _w.write('[');
    for (int i = 0; ; i++) {
      _w.write(Double.toString(a[i]));
      if (i == iMax) {
        _w.write(']');
        return;
      }
      _w.write(" ");
    }
  }

  protected void writeField(final Object _o, final Field _f, final Writer _w) throws IOException {
    // System.err.println(""+_f);
    if (!YapodLib.isValid(_f)) {
      return;
    }
    _f.setAccessible(true);

    Object value = fakeValue_;

    try {
      value = _f.get(_o);
    } catch (final IllegalAccessException ex1) {
      String fn = _f.getName();
      // System.err.println("Access error: "+fn+" in "+_o.getClass());

      Method m = null;

      try {
        m = _o.getClass().getMethod(fn, new Class[0]);
      } catch (final NoSuchMethodException ex2) {
      }

      if ((m == null) && (fn.endsWith("_"))) {
        fn = fn.substring(0, fn.length() - 1);
        try {
          m = _o.getClass().getMethod(fn, new Class[0]);
        } catch (final NoSuchMethodException ex3) {
        }
      }

      if (m == null) {
        fn = "get" + fn.substring(0, 1).toUpperCase() + fn.substring(1);
        try {
          m = _o.getClass().getMethod(fn, new Class[0]);
        } catch (final NoSuchMethodException ex4) {
        }
      }

      if (m != null) {
        try {
          value = m.invoke(_o, new Object[0]);
          // System.err.println("Using method "+fn+"()");
        } catch (final IllegalAccessException ex5) {
        } catch (final InvocationTargetException ex6) {
        }
      }
    }

    if (value == fakeValue_) {
      warning("No access to field " + _f.getName() + " in " + _o.getClass());
      value = null;
    }

    writeField(_f.getName(), value, _w);
  }

  protected synchronized void writeField(final String _name, final Object _value, final Writer _w) throws IOException {
    if (isIdent_) {
      output(_w, indente());
    }
    output(_w, "<field name=\"");
    output(_w, _name);
    output(_w, getFin());
    indentation_++;

    write(_value, _w);

    indentation_--;
    if (isIdent_) {
      output(_w, indente());
    }
    output(_w, "</field>\n");
  }

  protected synchronized void writeFooter(final Writer _w) throws IOException {
    output(_w, "</yapod>\n");
  }

  protected synchronized void writeHeader(final Writer _w) throws IOException {
    output(_w, "<?xml version=\"1.0\" encoding=\"" + ENCODING + "\"?>\n");
    output(_w, "<?xml-stylesheet href=\"yapod.css\" type=\"text/css\"?>\n");
    output(_w, "<yapod>\n");
  }

  protected synchronized void writeInts(final int[] _o, final Writer _w) throws IOException {
    final int l = _o.length;

    output(_w, "<array type=\"int\"");
    output(_w, getDepthAttr(_o));
    output(_w, getIdStr());
    output(_w, getId(_o));
    output(_w, "\" length=\"");
    output(_w, Integer.toString(l));
    output(_w, getFin());

    for (int i = 0; i < l; i++) {
      output(_w, "<single type=\"Int\">");
      _w.write(Integer.toString(_o[i]));
      output(_w, getSingleEnd());
    }

    output(_w, "</array>\n");
  }

  protected synchronized void writeNull(final Writer _w) throws IOException {
    if (isIdent_) {
      output(_w, indente());
    }
    output(_w, "<null/>\n");
  }

  protected synchronized void writeDouble(final Double _o, final Writer _w) throws IOException {
    if (isIdent_) {
      output(_w, indente());
    }
    output(_w, "<single type=\"Double\">");
/*    output(_w, getSingleStr());
    output(_w, getTypeAttr(_o));
    output(_w, getEnd());*/
    output(_w, _o.toString());
    output(_w, getSingleEnd());
  }

  protected synchronized void writeFloat(final Float _o, final Writer _w) throws IOException {
    if (isIdent_) {
      output(_w, indente());
    }
    output(_w, "<single type=\"Float\">");
/*    output(_w, getSingleStr());
    output(_w, getTypeAttr(_o));
    output(_w, getEnd());*/
    output(_w, _o.toString());
    output(_w, getSingleEnd());
  }

  protected synchronized void writeInteger(final Integer _o, final Writer _w) throws IOException {
    if (isIdent_) {
      output(_w, indente());
    }
    output(_w, "<single type=\"Int\">");
/*    output(_w, getSingleStr());
    output(_w, getTypeAttr(_o));
    output(_w, getEnd());*/
    output(_w, _o.toString());
    output(_w, getSingleEnd());
  }

  protected synchronized void writeObject(final Object _o, final Writer _w) throws IOException {
    Object objectToDo = _o;
    final boolean isCorbaObj = isCorba_ ? isCorbaObject(objectToDo) : false;
    if (isCorbaObj) {
      objectToDo = getObjectToDo(_o);
    }
    synchronized (objectToDo) {
      if (isIdent_) {
        output(_w, indente());
      }
      output(_w, (isCorbaObj ? "<iobject" : "<object"));
      output(_w, getTypeAttr(objectToDo));
      output(_w, getIdStr());
      output(_w, getId(_o));
      output(_w, getFin());
      indentation_++;

      final Class className = objectToDo.getClass();
      Field[] fields = (Field[]) classFields_.get(className);
      if (fields == null) {
        fields = YapodLib.getAllFields(objectToDo.getClass());
        classFields_.put(className, fields);
      }
      for (int i = 0; i < fields.length; i++) {
        writeField(objectToDo, fields[i], _w);
      }

      indentation_--;
      if (isIdent_) {
        output(_w, indente());
      }
      output(_w, (isCorbaObj ? "</iobject>\n" : "</object>\n"));
    }
  }

  protected synchronized void writeReference(final Object _o, final Writer _w) throws IOException {
    if (isIdent_) {
      output(_w, indente());
    }
    output(_w, "<reference idref=\"");
    output(_w, getId(_o));
    output(_w, "\"/>\n");
  }

  protected synchronized void writeString(final String _o, final Writer _w) throws IOException {
    if (isIdent_) {
      output(_w, indente());
    }

    output(_w, "<single type=\"String\">");
/*    output(_w, getSingleStr());
    output(_w, getTypeAttr(_o));
    output(_w, getEnd());*/
    output(_w, toXmlCharset(_o));
    output(_w, getSingleEnd());
  }

  /**
   * @see com.memoire.yapod.YapodSerializer#close()
   * @deprecated Cette methode ferme le flux, alors que open() ne l'ouvre pas.
   *     utiliser endWriting() a la place, et fermer le flux manuellement.
   */
  @Override
  public synchronized void close() throws IOException {
    writeFooter(out_);
    out_.close();
  }

  /**
   * Fin de la serialisation.
   */
  public synchronized void endWriting() throws IOException {
    writeFooter(out_);
    out_.flush();
  }

  /**
   * @see com.memoire.yapod.YapodSerializer#extension()
   */
  @Override
  public String extension() {
    return "xml";
  }

  /**
   * @param _o
   */
  public boolean isCorbaObject(final Object _o) {
    return BoonyLib.isCorbaObject(_o);
  }

  /**
   * @see com.memoire.yapod.YapodSerializer#open(java.io.OutputStream)
   */
  public synchronized void startWriting(final OutputStream _out) throws IOException {
    outInit_ = _out;
    boolean buffer = false;
    if (_out instanceof BufferedOutputStream) {
      buffer = true;
    }

    // OW+BO ou BW+OW
    // out=new OutputStreamWriter(new BufferedOutputStream(_out,8192));

    idbase_ = 1;
    ref_ = new HashMap();
    obj_ = new HashMap();
    if (!buffer) {
      out_ = new BufferedWriter(new OutputStreamWriter(_out, Charset.forName(ENCODING).newEncoder()), BUFFER_SIZE);
    }

    indentation_ = 0;
    writeHeader(out_);
  }

  /**
   * @see com.memoire.yapod.YapodSerializer#open(java.io.OutputStream)
   * @deprecated Utiliser startWriting()/endWriting() a la place.
   */
  @Override
  public synchronized void open(final OutputStream _out) throws IOException {
    startWriting(_out);
  }

  /**
   * @see com.memoire.yapod.YapodSerializer#store(java.lang.Object, java.lang.Object, java.util.Hashtable,
   *     java.util.Hashtable)
   */
  @Override
  public synchronized void store(final Object _o, final Object _dbcx, final Hashtable _ref, final Hashtable _obj)
      throws IOException {
    /*
     * String _dir = (String) dbcx_; String k = (String) _ref.get(_o); if (k == null) throw new
     * IllegalArgumentException("object not referenced: " + _o); Map old_ref = ref; Map old_obj = obj; ref = _ref; obj =
     * _obj; //ref.remove(_o); //obj.remove(k); root = _o; indentation_ = 0; out = new BufferedOutputStream(new
     * FileOutputStream(_dir + File.separator + k + ".xml"), 8192); output(out, " <?xml version=\"1.0\" encoding=\"" +
     * ENCODING + "\"?>\n"); output(out, " <?xml-stylesheet href=\"yapod.css\" type=\"text/css\"?>\n"); output(out, "
     * <yapod>\n"); write(_o); output(out, " </yapod>\n"); out.close(); root = null; //ref.put(_o,k); //obj.put(k,_o); ref =
     * old_ref; obj = old_obj;
     */
  }

  /**
   * @see com.memoire.yapod.YapodSerializer#write(java.lang.Object)
   */
  @Override
  public final synchronized void write(final Object _o) throws IOException {
    indentation_++;
    write(_o, out_);
    indentation_--;
  }

  /**
   * @param _o l'objet a ecrire
   * @param _w le writer
   * @throws IOException io exception
   */
  public synchronized void write(final Object _o, final Writer _w) throws IOException {
    if (_o == null) {
      writeNull(_w);
    } else if (_o instanceof Boolean) {
      writeBoolean((Boolean) _o, _w);
    } else if (_o instanceof Double) {
      writeDouble((Double) _o, _w);
    } else if (_o instanceof Float) {
      writeFloat((Float) _o, _w);
    } else if (_o instanceof Integer) {
      writeInteger((Integer) _o, _w);
    } else if (_o instanceof Character) {
      writeCharacter((Character) _o, _w);
    } else if (_o instanceof String) {
      writeString((String) _o, _w);
    } else if (_o instanceof byte[]) {
      writeBytes((byte[]) _o, _w);
    } else if (_o instanceof double[]) {
      if (istabDoubleCDATA_) {
        writeDoublesCDATA((double[]) _o, _w);
      } else {
        writeDoubles((double[]) _o, _w);
      }
    } else if (_o instanceof int[]) {
      writeInts((int[]) _o, _w);
    } else if (_o.getClass().isArray()) {
      writeArray(_o, _w);
    } else {
      if ((root_ == _o) || (ref_.get(_o) == null)) {
        writeObject(_o, _w);
      }

      writeReference(_o, _w);
    }
  }

  public static void main(String[] args) {
    double[][] tab = new double[10][100000];
    for (int i = 0; i < tab.length; i++) {
      for (int j = 0; j < tab[i].length; j++) {
        tab[i][j] = 100 * Math.random();
      }
    }
    try {
      BoonyXmlSerializer fluxEcrit = new BoonyXmlSerializer(false, false);
      try (FileOutputStream _out = new FileOutputStream("tabDoubleSansCData.xml")) {
        long t0 = System.currentTimeMillis();
        fluxEcrit.startWriting(_out);
        fluxEcrit.write(tab);
        fluxEcrit.endWriting();
        long dt = System.currentTimeMillis() - t0;
        FuLog.debug("Dur�e de s�rialisation sans CData : " + ((double) dt) / 1000 + " sec");
      }

      fluxEcrit = new BoonyXmlSerializer(false, true);
      try (FileOutputStream _out = new FileOutputStream("tabDoubleAvecCData.xml")) {
        long t0 = System.currentTimeMillis();
        fluxEcrit.startWriting(_out);
        fluxEcrit.write(tab);
        fluxEcrit.endWriting();
        long dt = System.currentTimeMillis() - t0;
        FuLog.debug("Dur�e de s�rialisation avec CData : " + ((double) dt) / 1000 + " sec");
      }

      BoonyXmlDeserializer fluxLu = new BoonyXmlDeserializer();
      fluxLu.open(new FileInputStream("tabDoubleSansCData.xml"));
      fluxLu.setHandler(new BoonyDeserializerHandler(false));
      long t0 = System.currentTimeMillis();
      double[][] tabDeserialise = (double[][]) fluxLu.read();
      long dt = System.currentTimeMillis() - t0;
      FuLog.debug("Dur�e de D�s�rialisation sans CData : " + ((double) dt) / 1000 + " sec");
      for (int i = 0; i < tab.length; i++) {
        System.out.println("Egalit� des tableaux " + i + " : " + Arrays.equals(tabDeserialise[i], tab[i]));
      }
      fluxLu.close();

      fluxLu = new BoonyXmlDeserializer();
      fluxLu.open(new FileInputStream("tabDoubleAvecCData.xml"));
      fluxLu.setHandler(new BoonyDeserializerHandler(false));
      t0 = System.currentTimeMillis();
      tabDeserialise = (double[][]) fluxLu.read();
      dt = System.currentTimeMillis() - t0;
      FuLog.debug("Dur�e de D�s�rialisation avec CData : " + ((double) dt) / 1000 + " sec");
      for (int i = 0; i < tab.length; i++) {
        FuLog.debug("Egalit� des tableaux " + i + " : " + Arrays.equals(tabDeserialise[i], tab[i]));
      }
      fluxLu.close();
    } catch (Exception ex) {
      FuLog.error(ex);
    }
  }
}
