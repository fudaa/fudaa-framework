/*
 * @creation     22 avr. 2004
 * @modification $Date: 2006-09-19 14:42:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.boony;

import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;
import org.fudaa.ctulu.CtuluLibString;

/**
 * @author Fred Deniger
 * @version $Id: BoonyXgzSerializer.java,v 1.8 2006-09-19 14:42:26 deniger Exp $
 */
public class BoonyXgzSerializer extends BoonyXmlSerializer {

  /**
   * Constructeur vide.
   */
  public BoonyXgzSerializer() {
    super();
  }

  @Override
  public String extension() {
    return "xgz";
  }

  @Override
  public String indente() {
    return CtuluLibString.EMPTY_STRING;
  }

  @Override
  public synchronized void open(final OutputStream _out) throws IOException {
    super.open(new GZIPOutputStream(_out));
  }

}
