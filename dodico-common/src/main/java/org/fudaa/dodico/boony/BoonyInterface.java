/*
 *  @creation     28 avr. 2004
 *  @modification $Date: 2006-04-13 12:35:36 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.boony;

/**
 * @author Fred Deniger
 * @version $Id: BoonyInterface.java,v 1.5 2006-04-13 12:35:36 deniger Exp $
 */
public interface BoonyInterface {

  /**
   * @return le poa qui stocke tous les objets corba ( et les delegue)
   */
  Object getPOA();

  Object getReferentToServant( Object _poa);

  /**
   * @return true si le fonctionnement est purement local
   */
  boolean isAllLocal();

  /**
   * @param _delegate le delegue
   * @return l'objet corba construit a partir du delegue.
   */
  Object buildStubFromDelegate(Object _delegate);

  boolean isCorbaObject(Object o);
}
