/*
 * @modification $Date: 2006-09-19 14:42:23 $
 * @statut       unstable
 * @version      0.17
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 */

package org.fudaa.dodico.boony;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import org.fudaa.ctulu.CtuluLibString;

/**
 * @author Fred Deniger
 * @version $Id: BoonyXgzDeserializer.java,v 1.6 2006-09-19 14:42:23 deniger Exp $
 */
public class BoonyXgzDeserializer extends BoonyXmlDeserializer {
  /**
   * constructeur vide.
   */
  public BoonyXgzDeserializer() {}

  @Override
  public String extension() {
    return "xgz";
  }

  /**
   * @return rien
   */
  public String indente() {
    return CtuluLibString.EMPTY_STRING;
  }

  @Override
  public synchronized void open(final InputStream _in) throws IOException {
    InputStream inStream = _in;
    if (!(inStream instanceof BufferedInputStream)) {
      inStream = new BufferedInputStream(_in, BUFFER_SIZE);
    }
    super.open(new GZIPInputStream(inStream));
  }
}
