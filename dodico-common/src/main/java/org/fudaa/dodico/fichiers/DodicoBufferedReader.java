/*
 * @creation 8 janv. 07
 * @modification $Date: 2007-05-04 13:45:58 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.fichiers;

import com.memoire.fu.FuLog;
import java.io.IOException;
import java.io.Reader;

/**
 * Ce lecteur est sp�cial: il lit tout le temps jusqu'au caract�re \n (pas comme l'impl�mentation de sun) et permet de
 * connaitre sa position dans le fichier (en byte depuis le d�but).
 * 
 * @author fred deniger
 * @version $Id: DodicoBufferedReader.java,v 1.4 2007-05-04 13:45:58 deniger Exp $
 */
public class DodicoBufferedReader extends Reader {

  private static final int defaultCharBufferSize = 8192;

  private static final int defaultExpectedLineLength = 80;
  private char[] cb_;

  private DodicoReader in_;

  private int nChars_, nextChar_;
  /** If the next character is a line feed, skip it. */
  private boolean skipLF_;

  /**
   * Create a buffering character-input stream that uses a default-sized input buffer.
   * 
   * @param _in A Reader
   */
  public DodicoBufferedReader(final Reader _in) {
    this(new DodicoReader(_in), defaultCharBufferSize);
  }

  @Override
  public int read(final char[] _cbuf) throws IOException {
    return super.read(_cbuf);
  }

  /**
   * Create a buffering character-input stream that uses an input buffer of the specified size.
   * 
   * @param _in A Reader
   * @param _sz Input-buffer size
   * @exception IllegalArgumentException If sz is <= 0
   */
  public DodicoBufferedReader(final DodicoReader _in, final int _sz) {
    super(_in);
    if (_sz <= 0) {
      throw new IllegalArgumentException("Buffer size <= 0");
    }
    this.in_ = _in;
    cb_ = new char[_sz];
    nextChar_ = 0;
    nChars_ = 0;
  }

  /** Check to make sure that the stream has not been closed. */
  private void ensureOpen() throws IOException {
    if (in_ == null) {
      throw new IOException("Stream closed");
    }
  }

  /**
   * Fill the input buffer, taking the mark into account if it is valid.
   */
  private void fill() throws IOException {
    int n;
    do {
      n = in_.read(cb_, 0, cb_.length);
    } while (n == 0);
    if (n > 0) {
      nChars_ = n;
      nextChar_ = 0;
    }
  }

  /**
   * Read characters into a portion of an array, reading from the underlying stream if necessary.
   */
  private int read1(final char[] _cbuf, final int _off, final int _len) throws IOException {

    if (nextChar_ >= nChars_) {
      /*
       * If the requested length is at least as large as the buffer, and if there is no mark/reset activity, and if line
       * feeds are not being skipped, do not bother to copy the characters into the local buffer. In this way buffered
       * streams will cascade harmlessly.
       */
      if (_len >= cb_.length && !skipLF_) {
        return in_.read(_cbuf, _off, _len);

      }
      fill();
    }
    if (nextChar_ >= nChars_) {
      return -1;
    }
    if (skipLF_) {
      skipLF_ = false;
      if (cb_[nextChar_] == '\n') {
        nextChar_++;
        if (nextChar_ >= nChars_) {
          fill();
        }
        if (nextChar_ >= nChars_) {
          return -1;
        }
      }
    }
    final int n = Math.min(_len, nChars_ - nextChar_);
    System.arraycopy(cb_, nextChar_, _cbuf, _off, n);
    nextChar_ += n;
    return n;
  }

  /**
   * Read a line of text. A line is considered to be terminated by any one of a line feed ('\n'), a carriage return
   * ('\r'), or a carriage return followed immediately by a linefeed.
   * 
   * @param _ignoreLF If true, the next '\n' will be skipped
   * @return A String containing the contents of the line, not including any line-termination characters, or null if the
   *         end of the stream has been reached
   * @see java.io.LineNumberReader#readLine()
   * @exception IOException If an I/O error occurs
   */
  String readLine(final boolean _ignoreLF) throws IOException {
    StringBuffer s = null;
    int startChar;
    boolean omitLF = _ignoreLF || skipLF_;

    synchronized (lock) {
      ensureOpen();

      /* bufferLoop: */for (;;) {

        if (nextChar_ >= nChars_) {
          fill();
        }
        if (nextChar_ >= nChars_) { /* EOF */
          if (s != null && s.length() > 0) {
            return s.toString();
          }
          return null;
        }
        boolean eol = false;
        char c = 0;
        int i;

        /* Skip a leftover '\n', if necessary */
        if (omitLF && (cb_[nextChar_] == '\n')) {
          nextChar_++;
        }
        skipLF_ = false;
        omitLF = false;

        charLoop: for (i = nextChar_; i < nChars_; i++) {
          c = cb_[i];
          if ((c == '\n') /* || (c == '\r') */) {
            eol = true;
            break charLoop;
          }
        }

        startChar = nextChar_;
        nextChar_ = i;

        if (eol) {
          String str;
          if (s == null) {
            str = new String(cb_, startChar, i - startChar);
          } else {
            s.append(cb_, startChar, i - startChar);
            str = s.toString();
          }
          nextChar_++;
          if (c == '\r') {
            skipLF_ = true;
          }
          return str;
        }

        if (s == null) {
          s = new StringBuffer(defaultExpectedLineLength);
        }
        s.append(cb_, startChar, i - startChar);
      }
    }
  }

  /**
   * Close the stream.
   * 
   * @exception IOException If an I/O error occurs
   */
  @Override
  public void close() throws IOException {
    synchronized (lock) {
      if (in_ == null) {
        return;
      }
      in_.close();
      in_ = null;
      cb_ = null;
    }
  }

  public long getPositionInStream() {
    if (in_ == null) {
      FuLog.error("DFI: reader used to count byte is null");
      return 0;
    }
    final long count = in_.getCount();
    final long res = count - nChars_ + nextChar_;
    if (res <= 0) {
      return 0;
    } else if (res >= count) {
      return count;
    }
    return res;

  }

  /**
   * Read a single character.
   * 
   * @return The character read, as an integer in the range 0 to 65535 (<tt>0x00-0xffff</tt>), or -1 if the end of
   *         the stream has been reached
   * @exception IOException If an I/O error occurs
   */
  @Override
  public int read() throws IOException {
    synchronized (lock) {
      ensureOpen();
      for (;;) {
        if (nextChar_ >= nChars_) {
          fill();
          if (nextChar_ >= nChars_) {
            return -1;
          }
        }
        if (skipLF_) {
          skipLF_ = false;
          if (cb_[nextChar_] == '\n') {
            nextChar_++;
            continue;
          }
        }
        return cb_[nextChar_++];
      }
    }
  }

  /**
   * Read characters into a portion of an array.
   * <p>
   * This method implements the general contract of the corresponding
   * <code>{@link Reader#read(char[], int, int) read}</code> method of the <code>{@link Reader}</code> class. As an
   * additional convenience, it attempts to read as many characters as possible by repeatedly invoking the
   * <code>read</code> method of the underlying stream. This iterated <code>read</code> continues until one of the
   * following conditions becomes true:
   * <ul>
   * <li> The specified number of characters have been read,
   * <li> The <code>read</code> method of the underlying stream returns <code>-1</code>, indicating end-of-file, or
   * <li> The <code>ready</code> method of the underlying stream returns <code>false</code>, indicating that
   * further input requests would block.
   * </ul>
   * If the first <code>read</code> on the underlying stream returns <code>-1</code> to indicate end-of-file then
   * this method returns <code>-1</code>. Otherwise this method returns the number of characters actually read.
   * <p>
   * Subclasses of this class are encouraged, but not required, to attempt to read as many characters as possible in the
   * same fashion.
   * <p>
   * Ordinarily this method takes characters from this stream's character buffer, filling it from the underlying stream
   * as necessary. If, however, the buffer is empty, the mark is not valid, and the requested length is at least as
   * large as the buffer, then this method will read characters directly from the underlying stream into the given
   * array. Thus redundant <code>BufferedReader</code>s will not copy data unnecessarily.
   * 
   * @param _cbuf Destination buffer
   * @param _off Offset at which to start storing characters
   * @param _len Maximum number of characters to read
   * @return The number of characters read, or -1 if the end of the stream has been reached
   * @exception IOException If an I/O error occurs
   */
  @Override
  public int read(final char[] _cbuf, final int _off, final int _len) throws IOException {
    synchronized (lock) {
      ensureOpen();
      if ((_off < 0) || (_off > _cbuf.length) || (_len < 0) || ((_off + _len) > _cbuf.length) || ((_off + _len) < 0)) {
        throw new IndexOutOfBoundsException();
      } else if (_len == 0) {
        return 0;
      }

      int n = read1(_cbuf, _off, _len);

      if (n <= 0) {
        return n;
      }
      while ((n < _len) && in_.ready()) {
        final int n1 = read1(_cbuf, _off + n, _len - n);
        if (n1 <= 0) {
          break;
        }
        n += n1;
      }
      return n;
    }
  }

  /**
   * Read a line of text. A line is considered to be terminated by any one of a line feed ('\n'), a carriage return
   * ('\r'), or a carriage return followed immediately by a linefeed.
   * 
   * @return A String containing the contents of the line, not including any line-termination characters, or null if the
   *         end of the stream has been reached
   * @exception IOException If an I/O error occurs
   */
  public String readLine() throws IOException {
    return readLine(false);
  }

  /**
   * Tell whether this stream is ready to be read. A buffered character stream is ready if the buffer is not empty, or
   * if the underlying character stream is ready.
   * 
   * @exception IOException If an I/O error occurs
   */
  @Override
  public boolean ready() throws IOException {
    synchronized (lock) {
      ensureOpen();

      /*
       * If newline needs to be skipped and the next char to be read is a newline character, then just skip it right
       * away.
       */
      if (skipLF_) {
        /*
         * Note that in.ready() will return true if and only if the next read on the stream will not block.
         */
        if (nextChar_ >= nChars_ && in_.ready()) {
          fill();
        }
        if (nextChar_ < nChars_) {
          if (cb_[nextChar_] == '\n') {
            nextChar_++;
          }
          skipLF_ = false;
        }
      }
      return (nextChar_ < nChars_) || in_.ready();
    }
  }

  /**
   * Skip characters.
   * 
   * @param _n The number of characters to skip
   * @return The number of characters actually skipped
   * @exception IllegalArgumentException If <code>n</code> is negative.
   * @exception IOException If an I/O error occurs
   */
  @Override
  public long skip(final long _n) throws IOException {
    if (_n < 0L) {
      throw new IllegalArgumentException("skip value is negative");
    }
    synchronized (lock) {
      ensureOpen();
      long r = _n;
      while (r > 0) {
        if (nextChar_ >= nChars_) {
          fill();
        }
        if (nextChar_ >= nChars_) {
          break;
        }
        if (skipLF_) {
          skipLF_ = false;
          if (cb_[nextChar_] == '\n') {
            nextChar_++;
          }
        }
        final long d = nChars_ - nextChar_;
        if (r <= d) {
          nextChar_ += r;
          r = 0;
          break;
        }
        r -= d;
        nextChar_ = nChars_;
      }
      return _n - r;
    }
  }

}
