/*
 *  @creation     11 f�vr. 2004
 *  @modification $Date: 2006-09-19 14:42:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.fichiers;

import gnu.trove.TDoubleArrayList;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.StringTokenizer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FortranInterface;

/**
 * Cette classe permet de lire un fichier csv contenant des nombres. Il est possible d'ignorer les lignes de
 * commentaires (commencant par # ou /) et de deviner le separateur de champ.
 * 
 * @author deniger
 * @version $Id: CsvDoubleReader.java,v 1.4 2006-09-19 14:42:28 deniger Exp $
 */
public class CsvDoubleReader extends LineNumberReader implements FortranInterface {

  String sep_;
  private boolean jumpComment_;

  /**
   * @param _r le reader support
   */
  public CsvDoubleReader(final Reader _r) {
    super(_r);
    sep_ = CtuluLibString.EMPTY_STRING;
    jumpComment_ = true;
  }

  /**
   * Les valeurs lues sont stock�es dans _l. Attention _l est remis a 0( clear).
   * 
   * @param _l la cible
   * @return true si lecture correcte
   * @throws IOException
   */
  public boolean readLine(final TDoubleArrayList _l) throws IOException {
    return readLine(_l, -1);
  }

  /**
   * Les valeurs lues ( au maximum _maxTokenToRead) sont stock��s dans _l.Attention _l est remis a 0( clear).
   * 
   * @param _l la cible
   * @param _maxTokenToRead le nombre max de colonne a lire
   * @return true si lecture correcte
   * @throws IOException
   */
  public boolean readLine(final TDoubleArrayList _l, final int _maxTokenToRead) throws IOException {
    if (_l == null) {
      throw new IllegalArgumentException("_l is null");
    }
    String l = super.readLine();
    if (l == null) {
      return false;
    }
    l = l.trim();
    if (l.length() == 0) {
      return readLine(_l);
    }
    if (jumpComment_ && (l.charAt(0) == '/' || l.charAt(0) == '#')) {
      return readLine(_l);
    }

    final StringTokenizer tk = new StringTokenizer(l, sep_);
    _l.clear();
    if (_maxTokenToRead > 0) {
      int idx = 0;
      while (tk.hasMoreTokens() && (idx < _maxTokenToRead)) {
        _l.add(Double.parseDouble(tk.nextToken()));
        idx++;
      }
    } else {
      while (tk.hasMoreTokens()) {
        _l.add(Double.parseDouble(tk.nextToken()));
      }
    }
    return true;
  }

  /**
   * @return true si les commentaires doivent etre ignore (#,/)
   */
  public boolean isJumpComment() {
    return jumpComment_;
  }

  /**
   * @param _b la nouvelle valeur pour ignorer ou non les commentaires
   */
  public void setJumpComment(final boolean _b) {
    jumpComment_ = _b;
  }

  /**
   * @param _f le fichier pour lequel les separateurs doivent etre devines
   */
  public void guessSepChar(final File _f) {
    sep_ = findGuessSepChar(_f);
  }

  /**
   * @param _f le fichier a verifier
   * @return le separateur suppose
   */
  public static String findGuessSepChar(final File _f) {
    return findGuessSepChar(_f, new String[] { ";", " ", "\t", "|", CtuluLibString.VIR });
  }

  /**
   * @param _f le fichier a verifier
   * @param _sep les separateurs a tester
   * @return le separateur suppose
   */
  public static String findGuessSepChar(final File _f, final String[] _sep) {
    final String r = null;
    final int n = _sep.length;
    LineNumberReader reader = null;
    try {
      reader = new LineNumberReader(new FileReader(_f));
      String l;
      while ((l = reader.readLine()) != null) {
        l = l.trim();
        if (l.length() > 0 && l.charAt(0) != '#' && l.charAt(0) != '/') {
          for (int i = 0; i < n; i++) {
            if (l.indexOf(_sep[i]) > 0) {
              return _sep[i];
            }
          }
        }
      }

    } catch (final Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (reader != null) {
          reader.close();
        }
      } catch (final IOException e1) {
        e1.printStackTrace();
      }
    }
    return r;

  }

  /**
   * @return le separateur
   */
  public String getSep() {
    return sep_;
  }

  /**
   * @param _string le nouveau separateur
   */
  public void setSep(final String _string) {
    sep_ = _string;
  }

}
