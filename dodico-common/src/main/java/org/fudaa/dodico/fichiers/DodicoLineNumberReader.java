/*
 * @creation 8 janv. 07
 * @modification $Date: 2007-05-04 13:45:58 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.fichiers;

import java.io.IOException;
import java.io.Reader;

/**
 * Ce lecteur est sp�cial: il lit tout le temps jusqu'au caract�re \n (pas comme l'impl�mentation de sun) et permet de
 * connaitre sa position dans le fichier (en byte depuis le d�but).
 * 
 * @author fred deniger
 * @version $Id: DodicoLineNumberReader.java,v 1.3 2007-05-04 13:45:58 deniger Exp $
 */
public class DodicoLineNumberReader extends DodicoBufferedReader {

  /** The current line number. */
  private int lineNumber_;

  /** If the next character is a line feed, skip it. */
  private boolean skipLF_;

  /**
   * Create a new line-numbering reader, using the default input-buffer size.
   * 
   * @param _in a Reader object to provide the underlying stream.
   */
  public DodicoLineNumberReader(final Reader _in) {
    super(_in);
  }

  /**
   * Create a new line-numbering reader, reading characters into a buffer of the given size.
   * 
   * @param _in a Reader object to provide the underlying stream.
   * @param _sz an int specifying the size of the buffer.
   */
  public DodicoLineNumberReader(final Reader _in, final int _sz) {
    super(new DodicoReader(_in), _sz);
  }

  /**
   * Set the current line number.
   * 
   * @param _lineNumber an int specifying the line number.
   * @see #getLineNumber
   */
  public void setLineNumber(final int _lineNumber) {
    this.lineNumber_ = _lineNumber;
  }

  /**
   * Get the current line number.
   * 
   * @return The current line number.
   * @see #setLineNumber
   */
  public int getLineNumber() {
    return lineNumber_;
  }

  /**
   * Read a single character. Line terminators are compressed into single newline ('\n') characters.
   * 
   * @return The character read, or -1 if the end of the stream has been reached
   * @exception IOException If an I/O error occurs
   */
  @Override
  public int read() throws IOException {
    synchronized (lock) {
      int c = super.read();
      if (skipLF_) {
        if (c == '\n') {
          c = super.read();
        }
        skipLF_ = false;
      }
      switch (c) {
      case '\r':
        skipLF_ = true;
      case '\n': /* Fall through */
        lineNumber_++;
        return '\n';
      default:
        break;
      }
      return c;
    }
  }

  /**
   * Read characters into a portion of an array.
   * 
   * @param _cbuf Destination buffer
   * @param _off Offset at which to start storing characters
   * @param _len Maximum number of characters to read
   * @return The number of bytes read, or -1 if the end of the stream has already been reached
   * @exception IOException If an I/O error occurs
   */
  @Override
  public int read(final char[] _cbuf, final int _off, final int _len) throws IOException {
    synchronized (lock) {
      final int n = super.read(_cbuf, _off, _len);

      for (int i = _off; i < _off + n; i++) {
        final int c = _cbuf[i];
        if (skipLF_) {
          skipLF_ = false;
          if (c == '\n') {
            continue;
          }
        }
        switch (c) {
        case '\r':
          skipLF_ = true;
        case '\n': /* Fall through */
          lineNumber_++;
          break;
        }
      }

      return n;
    }
  }

  /**
   * Read a line of text. A line is considered to be terminated by any one of a line feed ('\n'), a carriage return
   * ('\r'), or a carriage return followed immediately by a linefeed.
   * 
   * @return A String containing the contents of the line, not including any line-termination characters, or null if the
   *         end of the stream has been reached
   * @exception IOException If an I/O error occurs
   */
  @Override
  public String readLine() throws IOException {
    synchronized (lock) {
      final String l = super.readLine(skipLF_);
      skipLF_ = false;
      if (l != null) {
        lineNumber_++;
      }
      return l;
    }
  }

  /** Maximum skip-buffer size. */

  /** Skip buffer, null until allocated. */
  private char[] skipBuffer_;

  /**
   * Skip characters.
   * 
   * @param _n The number of characters to skip
   * @return The number of characters actually skipped
   * @exception IOException If an I/O error occurs
   */
  @Override
  public long skip(final long _n) throws IOException {
    if (_n < 0) {
      throw new IllegalArgumentException("skip() value is negative");
    }
    final int nn = (int) Math.min(_n, 8192);
    synchronized (lock) {
      if ((skipBuffer_ == null) || (skipBuffer_.length < nn)) {
        skipBuffer_ = new char[nn];
      }
      long r = _n;
      while (r > 0) {
        final int nc = read(skipBuffer_, 0, (int) Math.min(r, nn));
        if (nc == -1) {
          break;
        }
        r -= nc;
      }
      return _n - r;
    }
  }

}
