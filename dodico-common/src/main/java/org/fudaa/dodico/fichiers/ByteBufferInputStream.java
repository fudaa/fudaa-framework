/*
 *  @creation     17 d�c. 2004
 *  @modification $Date: 2006-09-19 14:42:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.dodico.fichiers;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * Permet de lire un byteBuffer.
 *
 * @author Fred Deniger
 * @version $Id: ByteBufferInputStream.java,v 1.5 2006-09-19 14:42:28 deniger Exp $
 */
public class ByteBufferInputStream extends InputStream {

  final ByteBuffer buf_;

  /**
   * @param _buf le buffer a lire
   */
  public ByteBufferInputStream(final ByteBuffer _buf) {
    buf_ = _buf;
  }

  @Override
  public final synchronized int read() throws IOException {
    return buf_.hasRemaining() ? buf_.get() : -1;
  }

  @Override
  public synchronized int read(final byte[] _bytes, final int _off, final int _len) throws IOException {
    final int length = Math.min(_len, buf_.remaining());
    buf_.get(_bytes, _off, length);
    return length;
  }
}
