/*
 *  @modification $Date: 2008-01-10 17:09:19 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.fichiers;

import com.memoire.bu.BuInformationsSoftware;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Properties;
import org.fudaa.ctulu.CtuluLibString;

/**
 * Precise le systeme, le logiciel et la version pour un fichier projet.
 * 
 * @author deniger
 * @version $Id: FileFormatSoftware.java,v 1.15 2008-01-10 17:09:19 bmarchan Exp $
 */
public final class FileFormatSoftware {

  /**
   * Les informations pour le logiciel telemac.
   */
  public final static BuInformationsSoftware TELEMAC_IS = new BuInformationsSoftware();
  /**
   * Les informations pour le logiciel reflux.
   */
  public final static BuInformationsSoftware REFLUX_IS = new BuInformationsSoftware();
  /**
   * Les informations pour le logiciel rubar.
   */
  public final static BuInformationsSoftware RUBAR_IS = new BuInformationsSoftware();
  static {
    TELEMAC_IS.name = "telemac";
    TELEMAC_IS.http = "http://telemacsystem.com";
    TELEMAC_IS.rights = "LNHE";
    TELEMAC_IS.contact = "info@telemacsystem.com";
    REFLUX_IS.name = "reflux";
    REFLUX_IS.http = "http://www.cetmef.equipement.gouv.fr/projets/hydraulique/reflux/bandeau.html";
    REFLUX_IS.rights = "CETMEF";
    REFLUX_IS.contact = "philippe.sergent@equipement.gouv.fr";
    RUBAR_IS.name = "rubar";
    RUBAR_IS.http = "http://www.lyon.cemagref.fr/";
    RUBAR_IS.rights = "CEMAGREF";

  }

  public static BuInformationsSoftware getSoftware(final FileFormatSoftware _ftSoft) {
    return getSoftware(_ftSoft.system_);
  }

  public static BuInformationsSoftware getSoftware(final String _name) {
    if (REFLUX_IS.name.equals(_name)) {
      return REFLUX_IS;
    }
    if (RUBAR_IS.name.equals(_name)) {
      return RUBAR_IS;
    }
    if (TELEMAC_IS.name.equals(_name)) {
      return TELEMAC_IS;
    }
    return null;
  }

  public String system_;
  public String soft_;
  public String version_;
  public String language_;

  public FileFormatSoftware() {}

  public FileFormatSoftware(final BuInformationsSoftware _soft) {
    if (_soft != null) {
      system_ = _soft.name;
    }
  }

  public Properties toProperties() {
    final Properties res = new Properties();
    if (system_ != null) {
      res.put("system", system_);
    }
    if (soft_ != null) {
      res.put("soft", soft_);
    }
    if (version_ != null) {
      res.put("version", version_);
    }
    if (language_ != null) {
      res.put("language", language_);
    }
    return res;
  }

  public static FileFormatSoftware createFromProperties(final InputStream _p) throws IOException {
    if(_p==null) {
      return null;
    }
    final Properties prop = new Properties();
    prop.load(_p);
    return creatreFromProperties(prop);
  }

  public static FileFormatSoftware creatreFromProperties(final Properties _p) {
    final FileFormatSoftware res = new FileFormatSoftware();
    if (res.fromProperties(_p)) {
      return res;
    }
    return null;
  }

  public boolean fromProperties(final Properties _p) {
    system_ = _p.getProperty("system");
    if (system_ == null) {
      return false;
    }
    soft_ = _p.getProperty("soft");
    version_ = _p.getProperty("version");
    language_ = _p.getProperty("language");
    return true;
  }

  public void toProperties(final OutputStream _out) throws IOException {
    final Writer out = new BufferedWriter(new OutputStreamWriter(_out));
    out.write("#NE PAS EDITER");
    out.write(CtuluLibString.LINE_SEP);
    out.write("#DO NOT EDIT");
    out.write(CtuluLibString.LINE_SEP);
    out.write("#Les propri�t�s du syst�mes de mod�lisation utilis�");
    out.write(CtuluLibString.LINE_SEP);
    out.write("#Properties describing the modelling system");
    out.write(CtuluLibString.LINE_SEP);
    if (system_ != null) {
      out.write("#le nom du syst�me de mod�lisation");
      out.write(CtuluLibString.LINE_SEP);
      out.write("#The name of the modelling system");
      out.write(CtuluLibString.LINE_SEP);
      out.write("system= ");
      out.write(system_);
      out.write(CtuluLibString.LINE_SEP);
    }
    if (soft_ != null) {
      out.write("#le nom du code de calcul utilis�");
      out.write(CtuluLibString.LINE_SEP);
      out.write("#The name of the soft");
      out.write(CtuluLibString.LINE_SEP);
      out.write("soft= ");
      out.write(soft_);
      out.write(CtuluLibString.LINE_SEP);
    }
    if (version_ != null) {
      out.write("#la version du code");
      out.write(CtuluLibString.LINE_SEP);
      out.write("#The version of the used soft");
      out.write(CtuluLibString.LINE_SEP);
      out.write("version= ");
      out.write(version_);
      out.write(CtuluLibString.LINE_SEP);
    }
    if (language_ != null) {
      out.write("#la langue utilis� par le code");
      out.write(CtuluLibString.LINE_SEP);
      out.write("#The language used by the code");
      out.write(CtuluLibString.LINE_SEP);
      out.write("language= ");
      out.write(language_);
      out.write(CtuluLibString.LINE_SEP);
    }

  }

  public String toString() {
    return getClass().getName() + " system=" + system_ + " soft=" + soft_ + " version=" + version_ + " language="
        + language_;
  }
}