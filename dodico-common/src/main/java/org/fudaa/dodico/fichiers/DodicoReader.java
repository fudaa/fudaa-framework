/*
 * @creation 12 avr. 07
 * @modification $Date: 2007-05-04 13:45:58 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.fichiers;

import java.io.IOException;
import java.io.Reader;

/**
 * @author fred deniger
 * @version $Id: DodicoReader.java,v 1.2 2007-05-04 13:45:58 deniger Exp $
 */
public class DodicoReader extends Reader {

  private final Reader in_;
  private long count_;

  public DodicoReader(final Reader _in) {
    super();
    in_ = _in;
  }

  @Override
  public void close() throws IOException {
    if (in_ != null) {
      in_.close();
    }

  }

  @Override
  public int read(final char[] _cbuf, final int _off, final int _len) throws IOException {
    final int res = in_.read(_cbuf, _off, _len);
    if (res >= 0) {
      count_ = count_ + res;
    }
    return res;
  }

  public long getCount() {
    return count_;
  }

}
