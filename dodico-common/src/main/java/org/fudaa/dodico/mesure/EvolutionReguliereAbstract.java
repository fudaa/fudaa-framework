/**
 * @creation 26 juin 2003
 * @modification $Date: 2007-05-22 13:11:24 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.mesure;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluValueValidator;

/**
 * Une evolution reguliere qui n'utilise pas les objets corba.
 * 
 * @see org.fudaa.dodico.all.TestJEvolution
 * @author deniger
 * @version $Id: EvolutionReguliereAbstract.java,v 1.13 2007-05-22 13:11:24 deniger Exp $
 */
public abstract class EvolutionReguliereAbstract implements Comparable, EvolutionReguliereInterface {

  private static CtuluValueValidator xTimeValidator_;

  public static CtuluValueValidator getXTimeValidator() {
    if (xTimeValidator_ == null) {
      xTimeValidator_ = new CtuluValueValidator.DoubleMin(0);
    }
    return xTimeValidator_;
  }

  protected EvolutionListener listener_;

  protected int used_;

  String nom_;
  
  boolean xAxeIsTime;

  public boolean isxAxeIsTime() {
    return xAxeIsTime;
  }

  public void setxAxeIsTime(boolean xAxeIsTime) {
    this.xAxeIsTime = xAxeIsTime;
  }

  protected CtuluValueValidator xVal_;

  protected CtuluValueValidator yVal_;

  String unite_;
  TDoubleArrayList val_;

  /**
   * Initialise les tableaux.
   */
  public EvolutionReguliereAbstract() {
    val_ = new TDoubleArrayList();
  }

  protected void initWith(final EvolutionReguliereAbstract _evol) {
    if (_evol == null) {
      return;
    }
    used_ = _evol.used_;
    nom_ = _evol.nom_;
    unite_ = _evol.unite_;
    xVal_ = _evol.xVal_;
    yVal_ = _evol.yVal_;
    listener_ = _evol.listener_;
    val_.clear();
    val_.add(_evol.val_.toNativeArray());

  }

  @Override
  public boolean isEquivalentTo(final EvolutionReguliereInterface _e) {
    if (_e != null && _e.getNbValues() == getNbValues()) {
      for (int i = getNbValues() - 1; i >= 0; i--) {
        if ((getX(i) != _e.getX(i)) || (getY(i) != _e.getY(i))) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  @Override
  public abstract boolean isNuagePoints();
  
  /**
   * Initialise les tableaux et le nom.
   * 
   * @param _s le nom.
   */
  public EvolutionReguliereAbstract(final String _s) {
    this();
    nom_ = _s;
  }

  protected final boolean setYForIdx(final int _idx, final double _y) {
    if ((getY(_idx) != _y) && (isValuesValid(getX(_idx), _y))) {
      val_.set(_idx, _y);
      return true;
    }
    return false;
  }

  protected final void fireEvolutionUsedChanged(final int _old, final boolean _isAdjusting) {
    if (listener_ != null) {
      listener_.evolutionUsedChanged(this, _old, used_, _isAdjusting);
    }
  }

  /**
   * Compare les noms.
   */
  @Override
  public final int compareTo(final Object _o) {
    if (_o instanceof EvolutionReguliereInterface) {
      return nom_.compareTo(((EvolutionReguliereInterface) _o).getNom());
    }
    throw new IllegalAccessError();
  }

  /**
   * Pour savoir si l'abscisse _x est utilisee.
   * 
   * @param _x le x a chercher
   * @return true si contient la valeur _x
   */
  @Override
  public final boolean containsX(final double _x) {
    return getIndexOfX(_x) >= 0;
  }

  /**
   * @param _c la capacite des tableaux dynamiques.
   */
  public void ensureCapacity(final int _c) {
    val_.ensureCapacity(_c);
  }

  /**
   * Peut etre long...
   * 
   * @return une chaines donnant tous les x,y
   */
  @Override
  public final String getDescriptionTotale() {
    final StringBuffer b = new StringBuffer();
    b.append(toString()).append(CtuluLibString.LINE_SEP);
    final StringBuffer t = new StringBuffer();
    final StringBuffer v = new StringBuffer();
    for (int i = 0; i < getNbValues(); i++) {
      if (i > 0) {
        t.append(CtuluLibString.VIR);
        v.append(CtuluLibString.VIR);
      }
      t.append(getX(i));
      v.append(getY(i));
    }
    b.append("time steps=[").append(t.toString()).append(']').append(CtuluLibString.LINE_SEP);
    b.append("values=[").append(v.toString()).append(']').append(CtuluLibString.LINE_SEP);
    return b.toString();
  }

  /**
   * Recupere la valeur en y correspondant a t (en interpolant si nécessaire). Si _x est en dehors des bornes, renvoie
   * la valeur de la borne la plus proche.
   * 
   * @param _x le x demande
   * @return la valeur y correspondante (interpolee si necessaire)
   */
  @Override
  public final double getInterpolateYValueFor(final double _x) {
    if (_x >= getMaxX()) {
      return val_.isEmpty() ? 0 : val_.getQuick(getNbValues() - 1);
    } else if (_x <= getMinX()) {
      return val_.isEmpty() ? 0 : val_.getQuick(0);
    } else {
      int index = getIndexOfX(_x);
      // chance: la valeur existe
      if (index >= 0) {
        return val_.getQuick(index);
      }
      // interpolation
      index = -index - 2;
      // l'index d'insertion. _t se trouve entre index-2 et index-1
      final double y0 = val_.getQuick(index);
      final double x0 = getX(index);
      return (val_.getQuick(index + 1) - y0) * (_x - x0) / (getX(index + 1) - x0) + y0;
    }
  }

  /**
   * @return le renvoyeur d'evt
   */
  @Override
  public final EvolutionListener getListener() {
    return this.listener_;
  }

  /**
   * Le max est calcul a chaque fois.
   * 
   * @return le max de y
   */
  @Override
  public final double getMaxY() {
    if (getNbValues() == 0) {
      return 0;
    }
    double r = val_.getQuick(0);
    double temp;
    for (int i = getNbValues() - 1; i > 0; i--) {
      temp = val_.getQuick(i);
      if (temp > r) {
        r = temp;
      }
    }
    return r;
  }

  /**
   * Le min est calcul a chaque fois.
   * 
   * @return le min de y
   */
  @Override
  public final double getMinY() {
    if (getNbValues() == 0) {
      return 0;
    }
    double r = val_.getQuick(0);
    double temp;
    for (int i = getNbValues() - 1; i > 0; i--) {
      temp = val_.getQuick(i);
      if (temp < r) {
        r = temp;
      }
    }
    return r;
  }

  /**
   * @return le nom
   */
  @Override
  public final String getNom() {
    return nom_;
  }

  /**
   * @return true si nom nom vide
   */
  @Override
  public final boolean isNomSet() {
    return (nom_ != null) && (nom_.trim().length() > 0);
  }

  /**
   * @return l'unite pour y.
   */
  @Override
  public final String getUnite() {
    return unite_;
  }

  /**
   * Le compteur est a mettre a jour par le developpeur.
   * 
   * @return le nombre de fois que cette evolution est utilisee
   */
  @Override
  public final int getUsed() {
    return used_;
  }

  /**
   * @param _idx l'indice demande
   * @return la valeur y a l'indice _idx
   */
  @Override
  public final double getY(final int _idx) {
    return val_.getQuick(_idx);
  }

  /**
   * @param _x le x a tester
   * @return true si l'instant t est compris dans les bornes de cette evolution.
   */
  @Override
  public final boolean isInclude(final double _x) {
    return (getMinX() <= _x) && (getMaxX() >= _x);
  }

  /**
   * @return true si utilise au moins 1 fois.
   */
  @Override
  public final boolean isUsed() {
    return used_ > 0;
  }

  /**
   * @param _x la valeur de x a tester
   * @param _y la valeur de y a tester
   * @return true si donnees valides
   */
  @Override
  public boolean isValuesValid(final double _x, final double _y) {
    return true;
  }

  /**
   * @param _listener le nouveau listener
   */
  @Override
  public final void setListener(final EvolutionListener _listener) {
    listener_ = _listener;
  }

  /**
   * @param _string le nom
   */
  @Override
  public void setNom(final String _string) {
    nom_ = _string;
  }

  /**
   * @param _unit l'unite pour y
   */
  @Override
  public final void setUnite(final Object _unit) {
    unite_ = _unit == null ? CtuluLibString.EMPTY_STRING : _unit.toString();
  }

  /**
   * @param _isAdjusting true si ajustement du compteur
   */
  @Override
  public final void setUnUsed(final boolean _isAdjusting) {
    final int old = used_;
    used_--;
    fireEvolutionUsedChanged(old, _isAdjusting);
  }
  
  public final void setUsed() {
    setUsed(false);
  }
  public final void setUnUsed() {
    setUnUsed(false);
  }

  /**
   * @param _isAdjusting true si ajustement du compteur
   */
  @Override
  public final void setUsed(final boolean _isAdjusting) {
    final int old = used_;
    used_++;
    fireEvolutionUsedChanged(old, _isAdjusting);
  }

  @Override
  public double[] getArrayY() {
    return val_.toNativeArray();
  }

  /**
   * @param _idx les indices a modifier
   * @param _y les nouvelles valeurs pour y
   * @param _cmd
   * @return true si changement
   */
  @Override
  public boolean setYValues(final int[] _idx, final double[] _y, final CtuluCommandContainer _cmd) {
    if ((_y == null) || (_idx == null) || (_idx.length != _y.length)) {
      return false;
    }
    boolean modified = false;
    TIntArrayList idx = null;
    TDoubleArrayList oldValues = null;
    TDoubleArrayList newValues = null;
    if (_cmd != null) {
      idx = new TIntArrayList(_idx.length);
      oldValues = new TDoubleArrayList(_idx.length);
      newValues = new TDoubleArrayList(_idx.length);
    }
    int iEnCours;
    for (int i = _idx.length - 1; i >= 0; i--) {
      iEnCours = _idx[i];
      final double old = getY(iEnCours);
      // on modifie. Si commande on ajoute les valeurs dans les tableaux qui vont bien
      if (setYForIdx(iEnCours, _y[i])) {
        modified = true;
        if (_cmd != null) {
          idx.add(iEnCours);
          oldValues.add(old);
          newValues.add(_y[i]);
        }
      }
    }
    if (modified && _cmd != null) {
      final int[] idxs = idx.toNativeArray();
      final double[] oldValueTab = oldValues.toNativeArray();
      final double[] newValueTab = newValues.toNativeArray();
      _cmd.addCmd(new CtuluCommand() {

        @Override
        public void redo() {
          setYValues(idxs, newValueTab, null);
          if (listener_ != null) {
            listener_.evolutionChanged(EvolutionReguliereAbstract.this);
          }
        }

        @Override
        public void undo() {
          setYValues(idxs, oldValueTab, null);
          if (listener_ != null) {
            listener_.evolutionChanged(EvolutionReguliereAbstract.this);
          }
        }
      });
    }
    if (modified && listener_ != null) {
      listener_.evolutionChanged(this);
    }
    return modified;
  }

  public final String toString() {
    return nom_;
  }

  @Override
  public CtuluValueValidator getXValidator() {
    return xVal_;
  }

  @Override
  public CtuluValueValidator getYValidator() {
    return yVal_;
  }

}
