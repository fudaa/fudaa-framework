/**
 *  @file         H2dEvolutionListenerDispatcher.java
 *  @creation     16 d�c. 2003
 *  @modification $Date: 2004-09-06 12:58:30 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.dodico.mesure;



/**
 * Un renvoyeur d'evt pour les evolutions
 * @author deniger
 * @version $Id: EvolutionListenerDispatcher.java,v 1.4 2004-09-06 12:58:30 deniger Exp $
 */
public interface EvolutionListenerDispatcher
  extends EvolutionListener {
  /**
   * @param _l le listener a ajouter
   */
  void addEvolutionListener(EvolutionListener _l);
  /**
   * @param _l le listener a enlever
   */
  void removeEvolutionListener(EvolutionListener _l);
  /**
   * @param _l le listener a tester
   * @return true si _l est deja enregistre en tant que listener
   */
  boolean containsEvolutionListener(EvolutionListener _l);
  
}
