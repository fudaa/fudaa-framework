/**
 * @creation 26 juin 2003
 * @modification $Date: 2007-05-22 13:11:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.mesure;

import com.memoire.fu.FuLog;
import gnu.trove.TDoubleArrayList;
import gnu.trove.TDoubleHashSet;
import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.dodico.commun.DodicoLib;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Une evolution reguliere qui n'utilise pas les objets corba.
 *
 * @author deniger
 * @version $Id: EvolutionReguliere.java,v 1.27 2007-05-22 13:11:25 deniger Exp $
 */
public class EvolutionReguliere extends EvolutionReguliereAbstract {
  public static final String INFO_TYPE = "type";
  private boolean nuagePoints_ = false;
  public String titreAxeX_;
  public String titreAxeY_;
  public String uniteAxeX_;
  public String uniteAxeY_;
  /**
   * Indique si il faut transformer l'�volution r�guli�re au format scope.
   */
  public boolean isScope_ = false;
  /**
   * Infos supplementaires ajoutees aux courbes afin de transferer des infos specifiques.
   */
  public Map infos_ = new HashMap();

  @Override
  public boolean isNuagePoints() {
    return nuagePoints_;
  }

  public Map getInfos() {
    return infos_;
  }

  public void setNuagePoints_(boolean nuagePoints_) {
    this.nuagePoints_ = nuagePoints_;
  }

  private class CommandAdd implements CtuluCommand {
    private final double newX_;
    private final double newY_;

    /**
     * @param _newX la nouvelle valeur ajoutee
     * @param _newY
     */
    public CommandAdd(final double _newX, final double _newY) {
      newX_ = _newX;
      newY_ = _newY;
    }

    @Override
    public void redo() {
      // utilise les methodes publiques pour envoyer les evts.
      add(newX_, newY_);
    }

    @Override
    public void undo() {
      removeXValue(newX_);
    }
  }

  private class CommandRemove implements CtuluCommand {
    private final double oldT_;
    private final double oldV_;

    /**
     * @param _oldT le x enleve
     * @param _oldV le y enleve
     */
    public CommandRemove(final double _oldT, final double _oldV) {
      oldV_ = _oldV;
      oldT_ = _oldT;
    }

    @Override
    public void redo() {
      removeXValue(oldT_);
    }

    @Override
    public void undo() {
      add(oldT_, oldV_);
    }
  }

  /**
   * @author Fred Deniger
   * @version $Id: EvolutionReguliere.java,v 1.27 2007-05-22 13:11:25 deniger Exp $
   */
  private class CommandReplace implements CtuluCommand {
    private final double newX_;
    private final double newY_;
    private final double oldX_;
    private final double oldY_;

    /**
     * @param _oldX
     * @param _oldY
     * @param _newX
     * @param _newY
     */
    public CommandReplace(final double _oldX, final double _oldY, final double _newX, final double _newY) {
      oldX_ = _oldX;
      oldY_ = _oldY;
      newX_ = _newX;
      newY_ = _newY;
    }

    @Override
    public void redo() {
      final int index = getIndexOfX(oldX_);
      if (index >= 0) {
        t_.remove(index);
        val_.remove(index);
      }
      add(newX_, newY_);
    }

    @Override
    public void undo() {
      final int index = getIndexOfX(newX_);
      if (index >= 0) {
        t_.remove(index);
        val_.remove(index);
      }
      add(oldX_, oldY_);
    }
  }

  /**
   * utilise pour preciser qu'une selection d'evolution possede des evolutions differentes.
   */
  public static final EvolutionReguliereAbstract MIXTE = new EvolutionReguliere(DodicoLib.getS("Mixte"));

  /**
   * Modifie r en ajoutant _nb points sur la droite (_xmin,_ymin) (xmax,ymax).
   *
   * @param _r la courbe a modifie
   * @param _xmin le x min de la droite
   * @param _xmax le x max de la droite
   * @param _ymin le y min de la droite
   * @param _ymax le y max de la droite
   * @param _nb nombre de point a ajouter
   */
  public static void createEvolution(final EvolutionReguliere _r, final double _xmin, final double _xmax,
                                     final double _ymin, final double _ymax, final int _nb) {
    if ((_xmax == _xmin) || (_nb < 2)) {
      _r.put(_xmin, _ymin);
    } else {
      final DecimalFormat fmt = CtuluLib.getDecimalFormat();
      fmt.setMaximumFractionDigits(5);
      double x2 = _xmax;
      double x1 = _xmin;
      if (x1 > x2) {
        x2 = _xmin;
        x1 = _xmax;
      }
      _r.put(x1, _ymin);
      if (_nb > 2) {
        final int nb = _nb - 1;
        final double coef = (_ymax - _ymin) / nb;
        final double incX = (x2 - x1) / nb;
        for (int i = 1; i < nb; i++) {
          final double x = Double.parseDouble(fmt.format(x1 + i * incX));
          final double y = Double.parseDouble(fmt.format(_ymin + i * coef));
          _r.put(x, y);
          // r.put(x1 + i * incX, _ymin + i * coef);
        }
      }
      _r.put(x2, _ymax);
    }
  }

  /**
   * Permet de reperer les evolutions identiques presentent dans un tableau.
   *
   * @param _l objet de type EvolutionReguliere
   * @return le table courbe->courbesequivalent
   */
  public static Map getDoublons(final Object[] _l) {
    if (_l == null) {
      return null;
    }
    final int nb = _l.length;
    final Map r = new HashMap(_l.length);
    for (int i = 0; i < nb; i++) {
      final EvolutionReguliereInterface eTest = (EvolutionReguliereInterface) _l[i];
      // si l'evolution est deja un doublon ce n'est pas la peine
      if (!r.containsKey(eTest)) {
        for (int j = i + 1; j < nb; j++) {
          final EvolutionReguliereAbstract eTestJ = (EvolutionReguliereAbstract) _l[j];
          // si rj est deja un doublon c'est n'est pas la peine
          if ((eTestJ != null) && !r.containsKey(eTestJ) && eTest.isEquivalentTo(eTestJ)) {
            // doublon trouve
            r.put(eTestJ, eTest);
          }
        }
      }
    }
    return r;
  }

  TDoubleArrayList t_;

  /**
   * Initialise les tableaux.
   */
  public EvolutionReguliere() {
    t_ = new TDoubleArrayList();
    val_ = new TDoubleArrayList();
  }

  /**
   * @param _l la source pour l'initialisation
   */
  public EvolutionReguliere(final EvolutionReguliere _l) {
    initFromEvolutionReguliere(_l);
  }

  /**
   * @param _n le nombre de points attendus
   */
  public EvolutionReguliere(final int _n) {
    t_ = new TDoubleArrayList(_n);
    val_ = new TDoubleArrayList(_n);
  }

  /**
   * Initialise les tableaux et le nom.
   *
   * @param _s le nom.
   */
  public EvolutionReguliere(final String _s) {
    this();
    nom_ = _s;
  }

  private boolean setXYAt(final int _idx, final double _x, final double _y) {
    if (isValuesValid(_x, _y) && ((getX(_idx) != _x) || (getY(_idx) != _y))) {
      t_.remove(_idx);
      val_.remove(_idx);
      put(_x, _y);
      return true;
    }
    return false;
  }

  /**
   * Attention: l'utilisation n'est pas copi�e?
   *
   * @param _evol l'evol copie.
   */
  protected final void initFromEvolutionReguliere(final EvolutionReguliere _evol) {
    nom_ = _evol.nom_;
    unite_ = _evol.unite_;
    t_ = new TDoubleArrayList(_evol.t_.toNativeArray());
    val_ = new TDoubleArrayList(_evol.val_.toNativeArray());
    listener_ = _evol.listener_;
    xVal_ = _evol.xVal_;
    yVal_ = _evol.yVal_;
    // used_ = _evol.used_;
    infos_ = new HashMap(_evol.infos_);
  }

  /**
   * @param _evol l'evolution de base
   * @param _l le listener ( en fait un renvoyeur d'evt)
   * @return l'evolution h2d correctement initialisee.
   */
  public static EvolutionReguliere createEvolution(final EvolutionReguliereInterface _evol, final EvolutionListener _l) {
    if (_evol == null) {
      return null;
    }
    final EvolutionReguliere r = new EvolutionReguliere(_evol);
    r.setListener(_l);
    return r;
  }

  public static EvolutionReguliere createTimeEvolution(final EvolutionReguliereInterface _evol,
                                                       final EvolutionListener _l) {
    final EvolutionReguliere res = createEvolution(_evol, _l);
    res.xVal_ = getXTimeValidator();
    return res;
  }

  protected final void initFromEvolution(final EvolutionReguliereInterface _evol) {
    nom_ = _evol.getNom();
    unite_ = _evol.getUnite();
    t_ = new TDoubleArrayList(_evol.getArrayX());
    val_ = new TDoubleArrayList(_evol.getArrayY());
    listener_ = _evol.getListener();
    xVal_ = _evol.getXValidator();
    yVal_ = _evol.getYValidator();
    // used_ = _evol.used_;
  }

  /**
   * Le x est ajoute et l'ordre des x est conserve.
   *
   * @param _x le x ajouter
   * @param _y le y correspondant
   */
  void put(final double _x, final double _y) {
    if (t_.size() == 0) {
      t_.add(_x);
      val_.add(_y);
    } else {
      int k = t_.binarySearch(_x);
      if (k < 0) {
        k = -k;
        if (k > t_.size()) {
          t_.add(_x);
          val_.add(_y);
        } else {
          t_.insert(k - 1, _x);
          val_.insert(k - 1, _y);
        }
      } else {
        val_.set(k, _y);
      }
    }
  }

  public EvolutionReguliere(final double[] _t, final double[] _v, final boolean _time) {
    this(_t.length);
    add(_t, _v, null);
    if (_time) {
      xVal_ = getXTimeValidator();
    }
  }

  public EvolutionReguliere(final EvolutionReguliereInterface _evolReg) {
    initFromEvolution(_evolReg);
  }

  public EvolutionReguliere duplicate() {
    return new EvolutionReguliere(this);
  }

  /**
   * Ajoute que si le _temps n'est pas deja present.
   *
   * @param _temps le pas de temps a ajouter
   * @param _value la valeur a ce pas de temps
   * @return true si ajoute.
   */
  public final boolean add(final double _temps, final double _value) {
    if (!containsX(_temps) && (isValuesValid(_temps, _value))) {
      put(_temps, _value);
      if (listener_ != null) {
        listener_.evolutionChanged(this);
      }
      return true;
    }
    return false;
  }

  /**
   * Ajoute que si le _temps n'est pas deja present.
   *
   * @param _temps le pas de temps a ajouter
   * @param _value la valeur a ce pas de temps
   * @return true si ajoute.
   */
  public final boolean add(final double _temps, final double _value, final CtuluCommandContainer _c) {
    if (!containsX(_temps) && isValuesValid(_temps, _value)) {
      put(_temps, _value);
      if (listener_ != null) {
        listener_.evolutionChanged(this);
      }
      if (_c != null) {
        _c.addCmd(new CommandAdd(_temps, _value));
      }
      return true;
    }
    return false;
  }

  public boolean add(final double[] _x, final double[] _y, final CtuluCommandContainer _cmd) {
    if ((_x == null) || (_y == null) || (_y.length != _x.length)) {
      return false;
    }
    final TDoubleArrayList xAdd = new TDoubleArrayList(_x.length);
    final TDoubleArrayList yAdd = new TDoubleArrayList(_x.length);
    for (int i = _x.length - 1; i >= 0; i--) {
      if (isValuesValid(_x[i], _y[i]) && !containsX(_x[i])) {
        put(_x[i], _y[i]);
        xAdd.add(_x[i]);
        yAdd.add(_y[i]);
      }
    }
    final boolean r = xAdd.size() > 0;
    if (_cmd != null && r) {
      final double[] xNew = xAdd.toNativeArray();
      final double[] yNew = yAdd.toNativeArray();
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          for (int i = xNew.length - 1; i >= 0; i--) {
            put(xNew[i], yNew[i]);
          }
          if (listener_ != null) {
            listener_.evolutionChanged(EvolutionReguliere.this);
          }
        }

        @Override
        public void undo() {
          for (int i = xNew.length - 1; i >= 0; i--) {
            final int k = getIndexOfX(xNew[i]);
            t_.remove(k);
            val_.remove(k);
          }
          if (listener_ != null) {
            listener_.evolutionChanged(EvolutionReguliere.this);
          }
        }
      });
    }
    if (r && listener_ != null) {
      listener_.evolutionChanged(this);
    }
    return xAdd.size() > 0;
  }

  /**
   * Ajoute tous les instants du tableau <code>_newT</code> en interpolant les valeurs. Add all the time step from the
   * array <code>_newT</code> and the values are interpolated.
   *
   * @param _newT les instants a ajouter
   */
  public void addNewInterpolationValues(final double[] _newT) {
    if (_newT == null) {
      return;
    }
    final int n = _newT.length;
    for (int i = 0; i < n; i++) {
      put(_newT[i], getInterpolateYValueFor(_newT[i]));
    }
  }

  /**
   * Cree une nouvelle evolution avec comme base de temps le tableau <code>_newT</code> et comme valeurs les
   * interpolations issues de cette evolution. Create a new evolution containing the time step <code>_newT</code> and
   * the value are interpolated from this evolution
   *
   * @param _newX les nouvelles valeurs de x
   * @return une evolutions ayant comme abscisses _newT et des valeurs interpolees a partir de cette evolution
   */
  public EvolutionReguliere createEvolutionFromInterpolation(final double[] _newX) {
    if (_newX == null) {
      return null;
    }
    final EvolutionReguliere r = new EvolutionReguliere(_newX.length);
    r.xVal_ = xVal_;
    r.yVal_ = yVal_;
    final int n = _newX.length;
    for (int i = 0; i < n; i++) {
      r.put(_newX[i], getInterpolateYValueFor(_newX[i]));
    }
    r.nom_ = nom_;
    r.unite_ = unite_;
    return r;
  }

  public EvolutionReguliere createTimeEvolutionFromInterpolation(final double[] _newX) {
    final EvolutionReguliere res = createEvolutionFromInterpolation(_newX);
    res.xVal_ = getXTimeValidator();
    return res;
  }

  /**
   * @param _c la capacite des tableaux dynamiques.
   */
  @Override
  public void ensureCapacity(final int _c) {
    t_.ensureCapacity(_c);
    val_.ensureCapacity(_c);
  }

  /**
   * @return tableau des x
   */
  @Override
  public double[] getArrayX() {
    return t_.toNativeArray();
  }

  /**
   * Suppose que les x sont donnes dans l'ordre croissant. Recherche binaire pour le x.
   *
   * @param _x la valeur x a cherche
   * @return -1 si non trouvee
   * @see TDoubleArrayList#binarySearch(double)
   */
  @Override
  public int getIndexOfX(final double _x) {
    return t_.binarySearch(_x);
  }

  /**
   * Suppose que les x sont donnees dans l'ordre croissant et renvoie donc le x au dernier indice.
   *
   * @return la valeur max.
   */
  @Override
  public double getMaxX() {
    final int n = t_.size() - 1;
    return n < 0 ? 0 : t_.getQuick(n);
  }

  protected EvolutionReguliere(final int _end, final double _v) {
    add(0, _v);
    add(_end, _v);
  }

  @Override
  public EvolutionReguliereInterface getCopy(final EvolutionListener _newListener) {
    final EvolutionReguliere res = new EvolutionReguliere(this);
    res.setListener(_newListener);
    return res;
  }

  /**
   * Suppose que les x sont donnees dans l'ordre croissant et renvoie donc le premier x .
   *
   * @return la valeur min de x.
   */
  @Override
  public double getMinX() {
    return (t_.size() == 0) ? 0 : t_.getQuick(0);
  }

  /**
   * @return le nombre de valeur.
   */
  @Override
  public int getNbValues() {
    return (val_ == null || t_ == null) ? 0 : Math.min(t_.size(), val_.size());
  }

  /**
   * @param _idx l'indice demande
   * @return la valeur x a l'indice _idx
   */
  @Override
  public double getX(final int _idx) {
    return t_.getQuick(_idx);
  }

  /**
   * @param _e l'evolution a comparer
   * @return true si egales.
   */
  @Override
  public boolean isEquivalentTo(final EvolutionReguliereInterface _e) {
    if (_e != null && (_e instanceof EvolutionReguliere) && _e.getNbValues() == getNbValues()) {
      final EvolutionReguliere r = (EvolutionReguliere) _e;
      return (r.t_.equals(t_)) && (r.val_.equals(val_));
    }
    return super.isEquivalentTo(_e);
  }

  /**
   * Compare les X de cette evolution avec <code>_toCompare</code>. Si les x sont diff�rents renvoie false et stocke
   * dans <code>_commonX</code> l'union des valeurs des x.
   *
   * @param _toCompare l'evolution a comparer
   * @param _commonX la liste a remplir avec l'union des x des 2 evolutions si differentes
   * @return true si identique
   */
  public boolean isEvolutionWithSameX(final EvolutionReguliere _toCompare, final TDoubleHashSet _commonX) {
    if (_toCompare == null) {
      return false;
    }
    if (t_.equals(_toCompare.t_)) {
      return true;
    }
    if (_commonX != null) {
      _commonX.ensureCapacity(t_.size() + _toCompare.t_.size());
      _commonX.addAll(t_.toNativeArray());
      _commonX.addAll(_toCompare.t_.toNativeArray());
    }
    return false;
  }

  /**
   * @param _x la valeur de x a tester
   * @param _y la valeur de y a tester
   * @return true si donnees valides
   */
  @Override
  public boolean isValuesValid(final double _x, final double _y) {
    boolean r = true;
    if (xVal_ != null) {
      r = xVal_.isValueValid(_x);
    }
    // le & est important
    if (yVal_ != null) {
      r &= yVal_.isValueValid(_y);
    }
    return r;
  }

  /**
   * @param _idx l'indice a enlever
   * @return true si indice reellement enleve.
   */
  public boolean remove(final int _idx) {
    return remove(_idx, null);
  }

  /**
   * @param _idx l'indice a enlever
   * @param _c si non la commande generee sera ajoutee a ce container.
   * @return true si indice enlever : indice appartenant a [0,nbVal[
   */
  public boolean remove(final int _idx, final CtuluCommandContainer _c) {
    if ((_idx >= 0) && (_idx < getNbValues())) {
      final double oldT = t_.remove(_idx);
      final double oldV = val_.remove(_idx);
      if (listener_ != null) {
        listener_.evolutionChanged(this);
      }
      if (_c != null) {
        _c.addCmd(new CommandRemove(oldT, oldV));
      }
      return true;
    }
    return false;
  }

  private int[] checkIdx(final int[] _idx) {
    if (CtuluLibArray.isEmpty(_idx)) {
      return _idx;
    }
    final TIntArrayList r = new TIntArrayList(_idx.length);
    final int max = t_ == null ? 0 : t_.size();
    for (int i = 0; i < _idx.length; i++) {
      final int test = _idx[i];
      if (test >= 0 && test < max) {
        r.add(test);
      }
    }
    return r.toNativeArray();
  }

  public boolean remove(final int[] _idx, final CtuluCommandContainer _c) {
    if ((_idx != null) && (_idx.length > 0) && t_ != null && t_.size() > 0) {
      final int[] realidx = checkIdx(_idx);
      final double[] oldX = new double[realidx.length];
      final double[] oldY = new double[realidx.length];
      Arrays.sort(realidx);
      for (int i = realidx.length - 1; i >= 0; i--) {
        final int ie = realidx[i];
        oldX[i] = t_.remove(ie);
        oldY[i] = val_.remove(ie);
      }
      if (_c != null) {
        _c.addCmd(new CtuluCommand() {
          @Override
          public void redo() {
            for (int i = realidx.length - 1; i >= 0; i--) {
              final int ie = realidx[i];
              t_.remove(ie);
              val_.remove(ie);
            }
            if (listener_ != null) {
              listener_.evolutionChanged(EvolutionReguliere.this);
            }
          }

          @Override
          public void undo() {
            for (int i = oldX.length - 1; i >= 0; i--) {
              put(oldX[i], oldY[i]);
            }
            if (listener_ != null) {
              listener_.evolutionChanged(EvolutionReguliere.this);
            }
          }
        });
      }
    }
    if (listener_ != null) {
      listener_.evolutionChanged(this);
    }
    return true;
  }

  /**
   * @param _value la valeur a enlever.
   * @return true si changement
   */
  public boolean removeXValue(final double _value) {
    return removeXValue(_value, null);
  }

  /**
   * @param _value la valeur a enlever
   * @param _c si non nul ajoute la commande adaptee
   * @return true si modif
   */
  public boolean removeXValue(final double _value, final CtuluCommandContainer _c) {
    final int index = getIndexOfX(_value);
    if (index >= 0) {
      final double oldT = t_.remove(index);
      final double oldV = val_.remove(index);
      if (_c != null) {
        _c.addCmd(new CommandRemove(oldT, oldV));
      }
      if (listener_ != null) {
        listener_.evolutionChanged(this);
      }
      return true;
    }
    return false;
  }

  /**
   * @param _idx l'indice a modifier
   * @param _t la nouvelle valeur pour le temps
   * @param _v la nouvelle valeur
   * @return true si modification
   */
  public boolean setValue(final int _idx, final double _t, final double _v) {
    return setValue(_idx, _t, _v, null);
  }

  /**
   * @param _idx l'indice a modifier
   * @param _t la nouvelle valeur pour le temps
   * @param _v la nouvelle valeur
   * @param _c Si non nul, la commande generee est ajoutee
   * @return true si modification
   */
  public boolean setValue(final int _idx, final double _t, final double _v, final CtuluCommandContainer _c) {
    if ((_idx >= 0) && (_idx < getNbValues()) && isValuesValid(_t, _v)
        && ((getX(_idx) != _t) || (getY(_idx) != _v))) {
      final double oldX = t_.remove(_idx);
      final double oldY = val_.remove(_idx);
      put(_t, _v);
      if (listener_ != null) {
        listener_.evolutionChanged(this);
      }
      if (_c != null) {
        _c.addCmd(new CommandReplace(oldX, oldY, _t, _v));
      }
      return true;
    }
    return false;
  }

  /**
   * @param _idx
   * @param _x
   * @param _y
   * @param _cmd
   * @return true si modif
   */
  public boolean setValues(final int[] _idx, final double[] _x, final double[] _y, final CtuluCommandContainer _cmd) {
    if (_x == null) {
      return setYValues(_idx, _y, _cmd);
    }
    if ((_y == null) || (_idx == null) || (_idx.length != _y.length) || (_idx.length != _x.length)) {
      return false;
    }
    boolean modified = false;
    TIntArrayList idx = null;
    TDoubleArrayList oldXValues = null;
    TDoubleArrayList oldYValues = null;
    TDoubleArrayList newXValues = null;
    TDoubleArrayList newYValues = null;
    if (_cmd != null) {
      idx = new TIntArrayList(_idx.length);
      oldXValues = new TDoubleArrayList(_idx.length);
      newXValues = new TDoubleArrayList(_idx.length);
      oldYValues = new TDoubleArrayList(_idx.length);
      newYValues = new TDoubleArrayList(_idx.length);
    }
    int iEnCours;
    for (int i = _idx.length - 1; i >= 0; i--) {
      iEnCours = _idx[i];
      final double oldY = getY(iEnCours);
      final double oldX = getX(iEnCours);
      // on modifie. Si commande on ajoute les valeurs dans les tableaux qui vont bien
      if (setXYAt(iEnCours, _x[i], _y[i])) {
        modified = true;
        if (_cmd != null) {
          // c'est non null
          idx.add(iEnCours);
          oldXValues.add(oldX);
          oldYValues.add(oldY);
          newYValues.add(_y[i]);
          newXValues.add(_x[i]);
        }
      }
    }
    if (modified && _cmd != null) {
      createUndo(_cmd, idx, oldXValues, oldYValues, newXValues, newYValues);
    }
    if (modified && listener_ != null) {
      listener_.evolutionChanged(this);
    }
    return modified;
  }

  private void createUndo(final CtuluCommandContainer _cmd, final TIntArrayList _idx,
                          final TDoubleArrayList _oldXValues, final TDoubleArrayList _oldYValues, final TDoubleArrayList _newXValues,
                          final TDoubleArrayList _newYValues) {
    final int[] idxs = _idx.toNativeArray();
    final double[] oldXValueTab = _oldXValues.toNativeArray();
    final double[] oldYValueTab = _oldYValues.toNativeArray();
    final double[] newXValueTab = _newXValues.toNativeArray();
    final double[] newYValueTab = _newYValues.toNativeArray();
    _cmd.addCmd(new CtuluCommand() {
      @Override
      public void redo() {
        setValues(idxs, newXValueTab, newYValueTab, null);
        if (listener_ != null) {
          listener_.evolutionChanged(EvolutionReguliere.this);
        }
      }

      @Override
      public void undo() {
        setValues(idxs, oldXValueTab, oldYValueTab, null);
        if (listener_ != null) {
          listener_.evolutionChanged(EvolutionReguliere.this);
        }
      }
    });
  }

  /**
   * @param _nbPoint le nombre de points que doit contenir l'evolution
   * @param _l le listener ( en fait un renvoyeur d'evt)
   * @return l'evolution h2d correctement initialisee.
   */
  public static EvolutionReguliereAbstract createTimeEvolution(final int _nbPoint, final EvolutionListenerDispatcher _l) {
    final EvolutionReguliereAbstract r = new EvolutionReguliere(_nbPoint);
    r.xVal_ = getXTimeValidator();
    r.setListener(_l);
    if (_l == null) {
      FuLog.error("Attention: evolution creee sans listener");
    }
    return r;
  }
}
