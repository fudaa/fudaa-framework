/*
 * @creation 16 d�c. 2003
 * @modification $Date: 2007-01-19 13:07:21 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.mesure;

import java.io.File;
import java.util.Map;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;

/**
 * @author deniger
 * @version $Id: EvolutionFileFormatVersion.java,v 1.2 2007-01-19 13:07:21 deniger Exp $
 */
public interface EvolutionFileFormatVersion {

  /**
   * @return le reader pour lire des evolution
   */
  FileReadOperationAbstract createReader();

  /**
   * @return le format parent
   */
  FileFormat getFileFormat();

  /**
   * @return le nom de cette version
   */
  String getName();

  /**
   * @param _f le fichier a lire
   * @param _prog la barre de progression
   * @param _options les options ( separateur par exemple)
   * @return une synthese avec un tableau EvolutionReguliereInterface[] comme source
   */
  CtuluIOOperationSynthese readEvolutions(File _f, ProgressionInterface _prog, Map _options);

}