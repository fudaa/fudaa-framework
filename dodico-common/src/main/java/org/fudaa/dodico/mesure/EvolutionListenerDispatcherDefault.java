/**
 * @creation 16 d�c. 2003
 * @modification $Date: 2006-09-19 14:44:21 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.mesure;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * @author deniger
 * @version $Id: EvolutionListenerDispatcherDefault.java,v 1.12 2006-09-19 14:44:21 deniger Exp $
 */
public class EvolutionListenerDispatcherDefault implements EvolutionListenerDispatcher {

  public EvolutionListenerDispatcherDefault() {
    super();
  }

  protected Set listener_;

  @Override
  public  void addEvolutionListener(final EvolutionListener _l){
    if (listener_ == null) {
      listener_ = new HashSet(20);
    }
    if (_l != this) {
      listener_.add(_l);
    }
  }

  @Override
  public synchronized void removeEvolutionListener(final EvolutionListener _l){
    if (listener_ != null) {
      listener_.remove(_l);
    }
  }

  @Override
  public boolean containsEvolutionListener(final EvolutionListener _l){
    return (listener_ != null) && (listener_.size() > 0) && (listener_.contains(_l));
  }

  protected void internEvolutionChanged(final EvolutionReguliereInterface _e){}

  protected void internEvolutionUsedChanged(final EvolutionReguliereInterface _e,final int _old,final int _new,
    final boolean _isAdjusting){}

  @Override
  public final void evolutionChanged(final EvolutionReguliereInterface _e){
    if (listener_ != null) {
      synchronized (listener_) {
        for (final Iterator it = listener_.iterator(); it.hasNext();) {
          ((EvolutionListener) it.next()).evolutionChanged(_e);
        }
      }
    }
    internEvolutionChanged(_e);
  }


  @Override
  public final void evolutionUsedChanged(final EvolutionReguliereInterface _e, final int _old, final int _new,
                                         final boolean _isAdjusting){
    if (listener_ != null) {
      synchronized (listener_) {
        for (final Iterator it = listener_.iterator(); it.hasNext();) {
          ((EvolutionListener) it.next()).evolutionUsedChanged(_e, _old, _new, _isAdjusting);
        }
      }
    }
    internEvolutionUsedChanged(_e, _old, _new, _isAdjusting);
  }

}
