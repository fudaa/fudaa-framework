/**
 * @creation 15 d�c. 2003
 * @modification $Date: 2007-05-04 13:47:32 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.mesure;

import com.memoire.fu.FuVectorString;
import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibMessage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FortranInterface;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fortran.FortranReader;

/**
 * @author deniger
 * @version $Id: DodicoCsvReader.java,v 1.10 2007-05-04 13:47:32 deniger Exp $
 */
public class DodicoCsvReader extends FileReadOperationAbstract {
  /** La colonne contient des valeurs de type chaine */
  public static final int COL_TYPE_STRING=0;
  /** La colonne contient des valeurs de type num�rique */
  public static final int COL_TYPE_NUMERIC=1;

  File file_;
  protected ReaderDelegate reader_;
  /** Les types des colonnes */
  protected int[] colTypes_;
  /** Accepte les valeurs vides en fin de ligne => Valeur="" */
  protected boolean isBlankValid_=false;

  public final boolean isNumeric() {
    return isNumeric_;
  }

  public void initFromOption(final Map _options) {
    if (_options == null) {
      return;
    }
    if (_options.containsKey("SEP")) {
      setSepChar((String) _options.get("SEP"));
    } else if (_options.containsKey("COLS")) {
      setFortranFormat((int[]) _options.get("COLS"));
    }
    if (_options.containsKey("COL_TYPES")) {
      colTypes_=(int[])_options.get("COL_TYPES");
    }
  }
  
  /** 
   * @return Le type des colonnes, initialis� depuis les options. Ce type
   * est indicatif, les valeurs lues sont toujours sous forme de String.
   */
  public int[] getColTypes() {
    return colTypes_;
  }

  /**
   * Definit que les champs lus d'une ligne doivent �tre num�riques. Si true et
   * qu'au moins un champ n'est pas num�rique, la ligne n'est pas conserv�e.
   * 
   * @param _isNumeric True : Tous les champs d'une ligne doivent �tre num�riques.
   */
  public final void setNumeric(final boolean _isNumeric) {
    isNumeric_ = _isNumeric;
  }
  
  /**
   * Autorise ou non les champs blancs en fin de ligne. Si ce n'est pas le cas,
   * la ligne complete est pass�e.
   * @param _isBlankValid True : Les champs vides en fin de ligne sont autoris�s.
   */
  public void setBlankValid(final boolean _isBlankValid) {
    isBlankValid_=_isBlankValid;
  }

  /**
   * format null.
   */
  public DodicoCsvReader() {
    reader_ = new SepCharReaderDelegate();
  }

  boolean isNumeric_;

  /**
   * @return les noms des colonnes si trouv�s en comment 1ere ligne, null sinon.
   */
  public String[] getName() {
    return reader_ == null ? null : reader_.name_;
  }

  /**
   * @return les premieres lignes decoupees en colonnes
   */
  public String[][] getPreview() {
    if (!CtuluLibFile.exists(file_)) {
      return new String[0][0];
    }
    final List r = new ArrayList();
    int nbEvol = -1;
    String[] temp = null;
    try {
      reader_.initReader();
      if (reader_.readLine()) {
        nbEvol = reader_.vals_.length;
        temp = new String[nbEvol];
        final int nb = Math.min(temp.length, nbEvol);
        for (int i = nb - 1; i >= 0; i--) {
          temp[i] = reader_.vals_[i];
        }
        r.add(temp);
      }
      for (int j = 3; j > 0; j--) {
        if (reader_.readLine()) {
          nbEvol = reader_.vals_.length;
          temp = new String[nbEvol];
          for (int i = nbEvol - 1; i >= 0; i--) {
            temp[i] = reader_.vals_[i];
          }
          r.add(temp);
        }

      }
    } catch (final IOException _io) {} finally {
      try {
        reader_.close();
      } catch (final IOException e) {
        CtuluLibMessage.error("Can't close " + file_.getAbsolutePath());
      }
    }
    final String[][] rf = new String[r.size()][];
    for (int i = r.size() - 1; i >= 0; i--) {
      CtuluLibString.getObjectInString(r.get(i), true);
      temp = (String[]) r.get(i);
      rf[i] = temp;
    }
    return rf;
  }

  private String[][] readDatas() {

    FuVectorString[] r = null;
    try {
      reader_.initReader();
      while (reader_.hasNext()) {
        if (reader_.readLine()) {
          if (r == null) {
            r = new FuVectorString[reader_.vals_.length];
            for (int i = reader_.vals_.length - 1; i >= 0; i--) {
              r[i] = new FuVectorString();
            }
          }
          for (int i = reader_.vals_.length - 1; i >= 0; i--) {
            r[i].addElement(reader_.vals_[i]);
          }
        }
      }
    } catch (final IOException _io) {
      analyze_.manageException(_io);
    }
    if (r == null) {
      return null;
    }
    final String[][] res = new String[r.length][];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = r[i].toArray();
    }
    return res;

  }

  /**
   * @param _sep le separateur de colonne
   */
  public void setSepChar(final String _sep) {
    if (!(reader_ instanceof SepCharReaderDelegate)) {
      reader_ = new SepCharReaderDelegate();
    }
    ((SepCharReaderDelegate) reader_).sepChar_ = _sep;
  }

  /**
   * @param _fmt le format fixe a utiliser
   */
  public void setFortranFormat(final int[] _fmt) {
    if (reader_ instanceof FortranReaderDelegate) {
      ((FortranReaderDelegate) reader_).fmt_ = _fmt;
    } else {
      reader_ = new FortranReaderDelegate(_fmt);
    }

  }

  protected abstract class ReaderDelegate {

    String[] vals_;
    String[] name_;

    public final String[] getName() {
      return name_;
    }

    public final void setName(final String[] _name) {
      name_ = _name;
    }

    public int getNbValues() {
      return vals_ == null ? 0 : vals_.length;
    }

    /**
     * Initialisation du lecteur.
     * 
     * @throws IOException
     */
    public void initReader() throws IOException {
      vals_ = null;
      name_ = null;
      init();
    }

    protected abstract void init() throws IOException;

    /**
     * @return true si ligne lue et parsee correctement
     */
    public abstract boolean readLine();

    /**
     * @return true si une ligne suit
     */
    public abstract boolean hasNext();

    /**
     * Fermeture du flux.
     * 
     * @throws IOException
     */
    public abstract void close() throws IOException;
  }

  class SepCharReaderDelegate extends ReaderDelegate {

    /**
     * Le lecteur.
     */
    LineNumberReader lineReader_;
    String sepChar_ = CtuluLibString.ESPACE;
    String line_;
    boolean oneLineRead_;
    int nbMaxCols_;

    @Override
    public void init() throws IOException {
      countMaxCols();
      vals_=new String[nbMaxCols_];
      oneLineRead_ = false;
      lineReader_ = new LineNumberReader(new FileReader(file_));
      line_ = lineReader_.readLine();
    }

    /**
     * Compte le nombre max de colonnes trouv�es dans le fichier.
     * @throws IOException 
     */
    private void countMaxCols() throws IOException {
      lineReader_ = new LineNumberReader(new FileReader(file_));
      String line;
      nbMaxCols_=0;

      try {
        while ((line=lineReader_.readLine()) != null) {
          if (isCommentLine(line)) continue;
          
          final StringTokenizer t = new StringTokenizer(line, sepChar_);
          nbMaxCols_=Math.max(t.countTokens(), nbMaxCols_);
        }
      }
      catch (IOException ex) {
      }
      lineReader_.close();
    }

    @Override
    public void close() throws IOException {
      lineReader_.close();
    }
    
    @Override
    public boolean readLine() {
      try {
        if (line_ == null) {
          return false;
        }
        line_ = line_.trim();
        boolean r = true;
        while (r) {
          boolean valid = true;
          if (line_ != null && line_.length() > 0) {

            // Ligne de comment : Si c'est le premier lu, on r�cup�re le nom des champs
            if (isCommentLine(line_)) {
              if (!oneLineRead_) {
                final StringTokenizer t = new StringTokenizer(line_.substring(1), sepChar_);
                name_ = new String[t.countTokens()];
                int idx = 0;
                while (t.hasMoreTokens()) {
                  name_[idx++] = t.nextToken();
                }
                oneLineRead_ = true;
              } else if (addCommentInResult_ && !CtuluLibArray.isEmpty(vals_)) {
                vals_[0] = line_;
                r = false;

              }
            // Ligne de r�sultats
            } else {
              final StringTokenizer t = new StringTokenizer(line_, sepChar_);
              // Le nombre de valeurs n'est pas suffisant
              if (minValueByLine_ > 0 && t.countTokens() < minValueByLine_) {
                if (addCommentInResult_ && !CtuluLibArray.isEmpty(vals_)) {
                  vals_[0] = "SEP " + line_;
                  r = false;
                }
              } else {
                r = false;
                // Le nombre de valeurs n'est pas le nombre attendu
                if (!isBlankValid_ && vals_.length != t.countTokens()) {
                  valid = false;
                }
                int idx = 0;
                while (idx < vals_.length) {
                  if (t.hasMoreTokens())
                    vals_[idx] = t.nextToken();
                  else if (isNumeric_)
                    vals_[idx] = "0";
                  else
                    vals_[idx] = "";
                  
                  if (isNumeric_ && !CtuluLibString.isNumeric(vals_[idx])) {
                    valid = false;
                    break;
                  }
                  idx++;
                }

              }
            }
          }

          line_ = lineReader_.readLine();
          if (line_ != null) {
            line_ = line_.trim();
          }
          if (!r) {
            return valid;
          } else if (line_ == null) {
            r = false;
          }
        }
      } catch (final IOException e) {
        return false;
      }
      return false;
    }

    @Override
    public boolean hasNext() {
      return line_ != null;
    }

  }

  public final static boolean isCommentLine(final String _line) {
    return _line.length() > 0 && (_line.charAt(0) == '#' || _line.startsWith("/"));
  }

  private class FortranReaderDelegate extends ReaderDelegate {

    int[] fmt_;
    FortranReader fortranReader_;
    boolean hasNext_;

    FortranReaderDelegate(final int[] _fmt) {
      fmt_ = _fmt;
    }

    @Override
    public void close() throws IOException {
      fortranReader_.close();
    }

    @Override
    public void init() throws IOException {
      fortranReader_ = new FortranReader(new FileReader(file_));
      fortranReader_.setBlankZero(true);
      fortranReader_.setJumpBlankLine(true);
      hasNext_ = true;
      vals_ = new String[fmt_.length];
    }

    @Override
    public boolean readLine() {
      try {
        while (true) {
          fortranReader_.readFields(fmt_);
          if (isCommentLine(fortranReader_.getLine())) {
            if (fortranReader_.getLineNumber() == 0 || !addCommentInResult_ || CtuluLibArray.isEmpty(vals_)) {
              continue;
            }
            vals_[0] = fortranReader_.getLine();
            return true;

          }
          for (int i = vals_.length - 1; i >= 0; i--) {
            vals_[i] = fortranReader_.stringField(i);
            if (isNumeric_ && !CtuluLibString.isNumeric(vals_[i])) {
              return false;
            }
          }
          return true;
        }
      } catch (final EOFException e) {
        hasNext_ = false;
        return false;
      } catch (final IOException e) {
        return false;
      }
    }

    @Override
    public boolean hasNext() {
      return hasNext_;
    }

  }

  /**
   * @return le fichier lu.
   */
  public File getFile() {
    return file_;
  }

  protected int minValueByLine_ = -1;

  @Override
  public void setFile(final File _file) {
    analyze_ = new CtuluAnalyze();
    file_ = _file;
    if (file_ == null || !file_.exists()) {
      analyze_.addFatalError(DodicoLib.getS("Fichier inconnu"));
    }
  }

  /**
   * @return Un tableau contenant pour chaque index toutes les valeurs de chaque
   * colonne. String[col][row]
   */
  @Override
  protected String[][] internalRead() {
    return readDatas();
  }

  boolean addCommentInResult_;

  @Override
  protected FortranInterface getFortranInterface() {
    return new FortranInterface() {

      @Override
      public void close() throws IOException {
        if (reader_ != null) {
          reader_.close();
        }
      }
    };
  }

  /**
   * @param _separateur le separateur a ajouter aux options
   * @return la table des options
   */
  public static Map buildOptionSep(final String _separateur) {
    final Map r = new HashMap();
    r.put("SEP", _separateur);
    return r;
  }

  public static Map buildFixedSize(final int[] _separateur) {
    final Map r = new HashMap();
    r.put("COLS", _separateur);
    return r;
  }

  public boolean isAddCommentInResult() {
    return addCommentInResult_;
  }

  /**
   * Si true,les commentaires rencontr�es dans le fichier (sauf le premier) sont ajoute dans les resultats.
   * 
   * @param _addCommentInResult
   */
  public void setAddCommentInResult(final boolean _addCommentInResult) {
    addCommentInResult_ = _addCommentInResult;
  }

}
