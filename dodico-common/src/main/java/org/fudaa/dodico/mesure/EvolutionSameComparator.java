/*
 * @creation 17 janv. 07
 * @modification $Date: 2007-05-04 13:47:32 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.mesure;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author fred deniger
 * @version $Id: EvolutionSameComparator.java,v 1.2 2007-05-04 13:47:32 deniger Exp $
 */
public class EvolutionSameComparator implements Comparator {

  @Override
  public int compare(final Object _o1, final Object _o2) {
    if (_o1 == _o2) {
      return 0;
    }
    if (_o1 == null) {
      return -1;
    }
    if (_o2 == null) {
      return 1;
    }
    if (((EvolutionReguliereInterface) _o1).isEquivalentTo((EvolutionReguliereInterface) _o2)) {
      return 0;
    }
    return _o1.hashCode() - _o2.hashCode();
  }

  public static EvolutionReguliereInterface[] getUniqueEvols(final Object[] _o) {
    EvolutionReguliereInterface[] res = null;
    if (_o != null) {
      final Set set = new TreeSet(new EvolutionSameComparator());
      set.addAll(Arrays.asList(_o));
      res = new EvolutionReguliereInterface[set.size()];
      set.toArray(res);
    }
    return res;
  }
}
