/*
 * @creation 16 d�c. 2003
 * @modification $Date: 2007-05-04 13:47:32 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.mesure;

import java.io.File;
import java.util.Map;
import org.fudaa.ctulu.CtuluDoubleParser;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.fileformat.FileFormatUnique;
import org.fudaa.ctulu.fileformat.FileReadOperationAbstract;
import org.fudaa.ctulu.fileformat.FileWriteOperationAbstract;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.fichiers.FileFormatSoftware;

/**
 * @author deniger
 * @version $Id: EvolutionFileFormat.java,v 1.5 2007-05-04 13:47:32 deniger Exp $
 */
public class EvolutionFileFormat extends FileFormatUnique implements EvolutionFileFormatVersion {

  @Override
  public FileReadOperationAbstract createReader() {
    return new DodicoCsvReader();
  }

  @Override
  public FileWriteOperationAbstract createWriter() {
    return null;
  }

  public static EvolutionReguliereInterface[] getEvols(final DodicoCsvReader _csvReader, final String[][] _read,
      final boolean _time) {
    final String[] names = _csvReader.getName();
    final CtuluDoubleParser parser = new CtuluDoubleParser();
    parser.setEmptyIsNull(true);
    if (_read != null && _read.length > 1) {
      final EvolutionReguliereAbstract[] h2dEvol = new EvolutionReguliereAbstract[_read.length - 1];
      final double[] times = new double[_read[0].length];
      for (int k = times.length - 1; k >= 0; k--) {
        times[k] = parser.parse(_read[0][k]);
      }
      for (int i = h2dEvol.length - 1; i >= 0; i--) {
        final double[] values = new double[_read[i + 1].length];
        for (int k = values.length - 1; k >= 0; k--) {
          values[k] = parser.parse(_read[i + 1][k]);
        }
        h2dEvol[i] = new EvolutionReguliere(times, values, _time);
        if (CtuluLibArray.isEmpty(names) || names.length <= i) {
          h2dEvol[i].setNom("C " + (i + 1));
        } else {
          h2dEvol[i].setNom(names[i]);
        }
      }
      return h2dEvol;
    }

    return null;
  }

  /**
   * A modifier par la suite pour lire plusieurs evolutions.
   */
  @Override
  public CtuluIOOperationSynthese readEvolutions(final File _f, final ProgressionInterface _prog, final Map _options) {
    final DodicoCsvReader r = new DodicoCsvReader();
    r.setNumeric(true);
    r.initFromOption(_options);
    final CtuluIOOperationSynthese s = r.read(_f, _prog);
    s.setSource(getEvols(r, (String[][]) s.getSource(), false));
    return s;
  }

  private static final EvolutionFileFormat INSTANCE = new EvolutionFileFormat();

  /**
   * @return le singleton
   */
  public static EvolutionFileFormat getInstance() {
    return INSTANCE;
  }

  /**
   * Initialise les donnees principales.
   */
  public EvolutionFileFormat() {
    super(1);
    nom_ = DodicoLib.getS("Fichier csv (texte)");
    extensions_ = new String[] { "csv", "txt" };
    id_ = "CSV";
    description_ = DodicoLib.getS("Fichier csv");
    software_ = FileFormatSoftware.REFLUX_IS;
    type_ = nom_;
  }

}
