/**
 * @creation 4 ao�t 2004
 * @modification $Date: 2006-04-07 09:23:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.mesure;


/**
 * @author Fred Deniger
 * @version $Id: EvolutionListener.java,v 1.5 2006-04-07 09:23:19 deniger Exp $
 */
public interface EvolutionListener {

  /**
   * @param _e l'evolution modifiee
   */
  void evolutionChanged(EvolutionReguliereInterface _e);
  
  /**
   * @param _e l'evolution dont l'utilisation a ete modifiee
   * @param _old l'ancienne valeur d'utilisation
   * @param _new la nouvelle valeur d'utilisation
   * @param _isAdjusting true si la modification est juste un ajustement
   */
  void evolutionUsedChanged(EvolutionReguliereInterface _e,int _old,int _new,boolean _isAdjusting);

}