/**
 *  @creation     20 janv. 2005
 *  @modification $Date: 2006-09-19 14:44:21 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
* @mail devel@fudaa.fr
 */
package org.fudaa.dodico.mesure;

import gnu.trove.TDoubleArrayList;
import org.fudaa.ctulu.CtuluCommandContainer;


/**
 * @author Fred Deniger
 * @version $Id: EvolutionReguliereFixe.java,v 1.5 2006-09-19 14:44:21 deniger Exp $
 */
public class EvolutionReguliereFixe extends EvolutionReguliereTFixe {
  
  

  /**
   * @param _t les temps
   * @param _var les variables
   */
  public EvolutionReguliereFixe(final double[] _t,final double[] _var) {
    super(_t);
    val_=new TDoubleArrayList(_var);
  }
  
  
  
  
  @Override
  public boolean setYValues(final int[] _idx, final double[] _y, final CtuluCommandContainer _cmd){
    return false;
  }
}
