/**
 * @creation 21 d�c. 2004
 * @modification $Date: 2007-05-22 13:11:24 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.mesure;

import java.util.Arrays;
import org.fudaa.ctulu.CtuluLibArray;

/**
 * @author Fred Deniger
 * @version $Id: EvolutionReguliereTFixe.java,v 1.6 2007-05-22 13:11:24 deniger Exp $
 */
public class EvolutionReguliereTFixe extends EvolutionReguliereAbstract {

  double[] t_;

  public EvolutionReguliereTFixe(final double[] _t) {
    super();
    t_ = _t;
    val_.ensureCapacity(_t.length);
    val_.add(new double[t_.length]);
  }

  @Override
  public EvolutionReguliereInterface getCopy(final EvolutionListener _newListener) {
    final EvolutionReguliereTFixe res = new EvolutionReguliereTFixe(CtuluLibArray.copy(t_));
    res.initWith(this);
    res.setListener(_newListener);
    return res;
  }

  @Override
  public double[] getArrayX() {
    return CtuluLibArray.copy(t_);
  }

  @Override
  public int getIndexOfX(final double _x) {
    return Arrays.binarySearch(t_, _x);
  }

  @Override
  public double getMaxX() {
    return t_[t_.length - 1];
  }

  @Override
  public double getMinX() {
    return t_[0];
  }

  @Override
  public int getNbValues() {
    return (val_ == null || t_ == null) ? 0 : Math.min(t_.length, val_.size());
  }

  @Override
  public double getX(final int _idx) {
    return t_[_idx];
  }

  @Override
  public boolean isEquivalentTo(final EvolutionReguliereInterface _e) {
    if ((_e instanceof EvolutionReguliereTFixe) && _e.getNbValues() == getNbValues()) {
      final EvolutionReguliereTFixe t = (EvolutionReguliereTFixe) _e;
      return (Arrays.equals(t_, t.t_)) && (t.val_.equals(val_));
    }
    return super.isEquivalentTo(_e);
  }

@Override
public boolean isNuagePoints() {
	// TODO Auto-generated method stub
	return false;
}

}
