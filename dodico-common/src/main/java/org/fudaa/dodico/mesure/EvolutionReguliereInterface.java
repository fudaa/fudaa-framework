/**
 *  @creation     21 d�c. 2004
 *  @modification $Date: 2007-05-22 13:11:24 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.mesure;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.CtuluValueValidator;
import org.fudaa.ctulu.collection.CourbeInterface;

/**
 * @author Fred Deniger
 * @version $Id: EvolutionReguliereInterface.java,v 1.11 2007-05-22 13:11:24 deniger Exp $
 */
public interface EvolutionReguliereInterface extends CourbeInterface {

  /**
   * Pour savoir si l'abscisse _x est utilisee.
   * 
   * @param _x le x a chercher
   * @return true si contient la valeur _x
   */
  boolean containsX(double _x);

  /**
   * @return tableau des x
   */
  double[] getArrayX();

  double[] getArrayY();

  EvolutionReguliereInterface getCopy(EvolutionListener _newListener);

  CtuluValueValidator getXValidator();

  CtuluValueValidator getYValidator();

  /**
   * Peut etre long...
   * 
   * @return une chaines donnant tous les x,y
   */
  String getDescriptionTotale();

  /**
   * Suppose que les x sont donnes dans l'ordre croissant. Recherche binaire pour le x.
   * 
   * @param _x la valeur x a cherche
   * @return -1 si non trouvee
   */
  int getIndexOfX(double _x);

  /**
   * Recupere la valeur en y correspondant a t (en interpolant si n�cessaire). Si _x est en dehors des bornes, renvoie
   * la valeur de la borne la plus proche.
   * 
   * @param _x le x demande
   * @return la valeur y correspondante (interpolee si necessaire)
   */
  double getInterpolateYValueFor(double _x);

  /**
   * @return le renvoyeur d'evt
   */
  EvolutionListener getListener();

  /**
   * Suppose que les x sont donnees dans l'ordre croissant et renvoie donc le x au dernier indice.
   * 
   * @return la valeur max.
   */
  double getMaxX();

  /**
   * Le max est calcul a chaque fois.
   * 
   * @return le max de y
   */
  double getMaxY();

  /**
   * Suppose que les x sont donnees dans l'ordre croissant et renvoie donc le premier x .
   * 
   * @return la valeur min de x.
   */
  double getMinX();

  /**
   * Le min est calcul a chaque fois.
   * 
   * @return le min de y
   */
  double getMinY();
  

  /**
   * @return le nom
   */
  String getNom();

  /**
   * @return true si nom nom vide
   */
  boolean isNomSet();

  /**
   * @return l'unite pour y.
   */
  String getUnite();

  /**
   * Le compteur est a mettre a jour par le developpeur.
   * 
   * @return le nombre de fois que cette evolution est utilisee
   */
  int getUsed();

  public  boolean isNuagePoints();

  /**
   * @param _e l'evolution a comparer
   * @return true si egales.
   */
  boolean isEquivalentTo(EvolutionReguliereInterface _e);

  /**
   * @param _x le x a tester
   * @return true si l'instant t est compris dans les bornes de cette evolution.
   */
  boolean isInclude(double _x);

  /**
   * @return true si utilise au moins 1 fois.
   */
  boolean isUsed();

  /**
   * @param _x la valeur de x a tester
   * @param _y la valeur de y a tester
   * @return true si donnees valides
   */
  boolean isValuesValid(double _x, double _y);

  /**
   * @param _listener le nouveau listener
   */
  void setListener(EvolutionListener _listener);

  /**
   * @param _string le nom
   */
  void setNom(String _string);

  /**
   * @param _string l'unite pour y
   */
  void setUnite(Object _string);

  /**
   * @param _isAdjusting true si ajustement du compteur
   */
  void setUnUsed(boolean _isAdjusting);

  /**
   * @param _isAdjusting true si ajustement du compteur
   */
  void setUsed(boolean _isAdjusting);

  /**
   * @param _idx les indices a modifier
   * @param _y les nouvelles valeurs pour y
   * @param _cmd
   * @return true si changement
   */
  boolean setYValues(int[] _idx, double[] _y, CtuluCommandContainer _cmd);
}