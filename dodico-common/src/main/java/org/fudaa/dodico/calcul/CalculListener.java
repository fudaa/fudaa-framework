/*
 * @creation 13 juin 2003
 * @modification $Date: 2006-04-14 14:51:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.calcul;

/**
 * @author deniger
 * @version $Id: CalculListener.java,v 1.8 2006-04-14 14:51:48 deniger Exp $
 */
public interface CalculListener {

  /**
   * Appele lorsque que le process est termine.
   *
   * @param _source la source de l'evt
   */
  void calculFinished(CalculLauncher _source);
}