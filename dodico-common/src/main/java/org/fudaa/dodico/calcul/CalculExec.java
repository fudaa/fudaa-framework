/**
 * @creation 12 juin 2003
 * @modification $Date: 2007-03-02 13:00:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.calcul;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import java.io.File;
import java.io.PrintStream;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.CtuluUIDefault;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.commun.DodicoPreferences;
import org.fudaa.dodico.objet.CExec;
import org.fudaa.dodico.objet.CExecListener;

/**
 * Classe permettant d'utiliser facilement la classe Cexec et de creer des fichiers de lancement.
 * 
 * @author deniger
 * @version $Id: CalculExec.java,v 1.29 2007-03-02 13:00:53 deniger Exp $
 */
public abstract class CalculExec {

  /**
   * Permet de creer (si necessaire) un launcher special pour le code <code>_serveurName</code>. A partir d'un template
   * (le fichier serveur/ <code>_launchertplFile</code>) cree un launcher <code>_launcherFile</code> qui pourra lancer
   * l'executable <code>_exeFile</code> depuis n'importe quel dossier. Le calculManager ne sert qu'a recuperer les
   * erreurs. Voir le serveur reflux pour exemple.
   * 
   * @param _serveurName le nom du dossier du serveur (exemple telemac)
   * @param _exeFile le nom de l'exe (exemple telemac2d.bat
   * @param _launchtplFile le nom du fichier template
   * @param _launchFile le fichier a ecrire
   * @param _ui le receveur d'info
   * @return le fichier cree
   */
  public static File manageLauncher(final String _serveurName, final String _exeFile, final String _launchtplFile,
      final String _launchFile, final CtuluUI _ui) {
    final File serveurDir = new File("serveurs/" + _serveurName + "/");
    final File launchFile = new File(serveurDir, _launchFile);
    final File in = new File(serveurDir, _launchtplFile);
    if (!launchFile.exists() || (in.lastModified() > launchFile.lastModified())) {
      final File serveur = new File(serveurDir, _exeFile);
      final String newToken = serveur.getAbsolutePath();
      if (!CtuluLibFile.replaceAndCopyFile(in, launchFile, "@exe@", newToken)) {
        _ui.error("Copie de fichier", "Erreur lors de la copie de " + _launchtplFile, false);
      }
    }
    return launchFile;
  }

  protected PrintStream outError_;
  protected PrintStream outStandard_;
  boolean changeWorkingDirectory_;
  String[] env_;
  boolean isStartCmdUse_;
  boolean launchInNewTerm_;

  ProgressionInterface progress_;

  CtuluUI ui_;

  /**
   * Initialise les sorties standart/erreur avec Sytem.out/err.
   */
  public CalculExec() {
    outError_ = System.err;
    outStandard_ = System.out;
  }

  public CExec buildExec(final File _f) {
    return buildExec(_f, null);
  }

  public CExec buildExec(final File _f, final Runnable _nextProcess) {
    if (ui_ == null) {
      ui_ = new CtuluUIDefault();
    }
    String[] cmd = getLaunchCmd(_f, ui_);
    if (launchInNewTerm_) {
      cmd = getCmdWithNewTermCmd(cmd);
    }
    if (cmd == null) {
      new Throwable().printStackTrace();
      return null;
    }
    // CtuluLibString.arrayToString(cmd);
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("DCA: " + CtuluLibString.arrayToString(cmd));
    }
    final CExec ex = new CExec(cmd);
    ex.setEnvs(env_);
    ex.setCatchExceptions(false);
    ex.setNextProcess(_nextProcess);
    ex.setErrStream(outError_);
    ex.setOutStream(outStandard_);
    if (changeWorkingDirectory_ && _f != null) {

      ex.setExecDirectory(_f.isDirectory() ? _f : _f.getParentFile());
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("DCA: CEXEC change directory " + ex.getExecDirectory());
      }
    }
    return ex;
  }

  // String exec_;

  /**
   * @param _cmd les commandes intiales
   * @return les commandes de l'interpreteur system+_cmd
   */
  public String[] getCmdWithNewTermCmd(final String[] _cmd) {
    StringBuffer s = null;
    // si la commande n'est pas nulle, on la parse
    if (_cmd != null) {
      s = new StringBuffer(_cmd.length * 10);
      final int n = _cmd.length;
      /*
       * if (FuLib.isLinux() || FuLib.isUnix()) { s.append(CtuluLibString.ESPACE); }
       */
      if (isStartCmdUse_ && FuLib.isWindows()) {
        final String t = getWindowsStartCmd();
        if (!CtuluLibString.isEmpty(t)) {
          s.append(t.trim()).append(' ');
        }

      }
      if (_cmd.length > 0) {
        s.append(_cmd[0]);
        for (int i = 1; i < n; i++) {
          s.append(' ').append(_cmd[i]);
        }
      }
    }
    final String termCmd = getTermCmd();
    String[] deb = null;
    if (termCmd != null) {
      final String[] init = CtuluLibString.parseString(termCmd, CtuluLibString.ESPACE);
      final int nInit = init.length;
      for (int i = nInit - 1; i >= 0; i--) {
        if ("@title@".equals(init[i])) {
          init[i] = "'" + getExecTitle();
          if (_cmd != null && _cmd.length > 0) {
            init[i] += CtuluLibString.ESPACE + _cmd[_cmd.length - 1];
          }
          init[i] += "'";
          break;
        }
      }
      if (s == null) { return init; }
      deb = new String[nInit + 1];
      System.arraycopy(init, 0, deb, 0, nInit);
      final String delim = FuLib.isWindows() ? "\"" : CtuluLibString.EMPTY_STRING;
      deb[deb.length - 1] = delim + s.toString() + delim;
      return deb;
    }
    return _cmd;
  }

  /**
   * @return les commandes permettant de lancer l'interpreteur adapte au systeme d'exploitation (cmd.exe pour windows et
   *         xterm pour le reste)
   */
  public String getDefaultTermCmd() {
    if (FuLib.isWindows()) {
      if (FuLib.getSystemProperty("os.name").indexOf("9") > 0) { return "command.com /C"; }
      return "cmd.exe /C";
    }
    if (FuLib.isLinux()) { return "xterm -ls -hold -title @title@ -e"; }
    return "xterm -ls -title @title@ -e";
  }

  /**
   * @return la commande utilisee par defaut pour le start de windows
   */
  public String getDefaultWindowsStartCmd() {
    // return "start /wait /max /separate";
    return "start /wait /max";
  }

  public final String[] getEnv() {
    return CtuluLibArray.copy(env_);
  }

  public abstract String getExecTitle();

  /**
   * Ne pas mettre des commandes specifiques au system. Elles seront ajout�es par la suite
   * 
   * @param _paramsFile le fichier parametres source de l'exe
   * @param _ui l interface utilisateur receveur des infos/warns/errors
   * @return les commandes de l'exe seulement
   */
  public abstract String[] getLaunchCmd(File _paramsFile, CtuluUI _ui);

  /**
   * @return le canal d'erreur *
   */
  public PrintStream getOutError() {
    return outError_;
  }

  /**
   * @return le canal standard
   */
  public PrintStream getOutStandard() {
    return outStandard_;
  }

  /**
   * @return la barre de progression
   */
  public ProgressionInterface getProgression() {
    return progress_;
  }

  /**
   * @return la commande utilisee pour le lancement du terminal
   */
  public String getTermCmd() {
    return DodicoPreferences.DODICO.getStringProperty("cexec.term.launch", getDefaultTermCmd());
  }

  /**
   * @return l'ui utilisee.
   */
  public CtuluUI getUI() {
    return ui_;
  }

  /**
   * @return la commande utilisee pour le start de windows
   */
  public String getWindowsStartCmd() {
    return DodicoPreferences.DODICO.getStringProperty("cexec.windows.start", getDefaultWindowsStartCmd());
  }

  /**
   * @return true si l'execution doit se faire dans le repertoire parent du fichier parametre
   */
  public boolean isChangeWorkingDirectory() {
    return changeWorkingDirectory_;
  }

  /**
   * Si true, des commandes supplementaires seront ajoutees pour lancer un terminal.
   * 
   * @return true si l'execution doit se faire dans un terminal.
   */
  public boolean isLauchInNewTerm() {
    return launchInNewTerm_;
  }

  /**
   * Uniquement windows.
   * 
   * @return true si la ligne "start /wait /max /separate" doit etre ajoutee
   */
  public final boolean isStartCmdUse() {
    return isStartCmdUse_;
  }

  /**
   * Lance l'executable pour le fichier_f. La commande est creee a partir getLaunchCmd puis avec getCmdWithNewTermCmd.
   * 
   * @param _f le fichier parametre pour l'executable
   * @param _l le listener qui permet de savoir quand le calcul est termin�
   * @return true if the execution succeed ( no exception).
   */
  public boolean launch(final File _f, final CExecListener _l) {
    return launch(_f, _l, null);
  }

  boolean showErrorInUI = true;

  public boolean launch(final File _f, final CExecListener _l, final Runnable _nextProcess) {
    final CExec ex = buildExec(_f, _nextProcess);
    if (ex == null) { return false; }

    ex.setListener(_l);
    boolean ok = false;
    if (progress_ != null) {
      progress_.setDesc(DodicoLib.getS("Lancement de {0}", getExecTitle()));
      progress_.setProgression(0);
    }
    try {
      ex.exec();
      ok = true;
    } catch (final RuntimeException _e) {
      if (FuLog.isTrace()) {
        _e.printStackTrace();
      }
      if (showErrorInUI) {
        ui_.error(getExecTitle(), DodicoLib.getS("L'ex�cution s'est termin�e avec des erreurs")
            + CtuluLibString.LINE_SEP + _e.getMessage(), false);
      }
    }
    if (progress_ != null) {
      progress_.setDesc(getExecTitle() + CtuluLibString.ESPACE + DodicoLib.getS("Calcul termin�"));
      progress_.setProgression(100);
    }
    return ok;
  }

  /**
   * @param _b true si l'execution doit se faire dans le repertoire parent du fichier parmetres
   */
  public void setChangeWorkingDirectory(final boolean _b) {
    changeWorkingDirectory_ = _b;
  }

  public final void setEnv(final String[] _env) {
    env_ = CtuluLibArray.copy(_env);
  }

  /**
   * @param _b true si l'execution doit se faire dans un terminal
   */
  public void setLauchInNewTerm(final boolean _b) {
    launchInNewTerm_ = _b;
  }

  /**
   * @param _writer le nouveau canal d'erreur
   */
  public void setOutError(final PrintStream _writer) {
    outError_ = _writer;
  }

  /**
   * @param _writer le nouveau canal standard
   */
  public void setOutStandard(final PrintStream _writer) {
    outStandard_ = _writer;
  }

  /**
   * @param _interface
   */
  public void setProgression(final ProgressionInterface _interface) {
    progress_ = _interface;
  }

  /**
   * Uniquement windows.
   * 
   * @param _isStartCmdUse true si la ligne "start /wait /max /separate" doit etre ajoutee
   */
  public final void setStartCmdUse(final boolean _isStartCmdUse) {
    isStartCmdUse_ = _isStartCmdUse;
  }

  /**
   * @param _dodicoUI
   */
  public void setUI(final CtuluUI _dodicoUI) {
    if (_dodicoUI != null) {
      ui_ = _dodicoUI;
    }
  }

  /**
   * @return the showErrorInUI
   */
  public boolean isShowErrorInUI() {
    return showErrorInUI;
  }

  /**
   * @param showErrorInUI the showErrorInUI to set
   */
  public void setShowErrorInUI(final boolean showErrorInUI) {
    this.showErrorInUI = showErrorInUI;
  }

}