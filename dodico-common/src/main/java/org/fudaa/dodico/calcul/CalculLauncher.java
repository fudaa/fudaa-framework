/*
 *  @creation     12 juin 2003
 *  @modification $Date: 2006-04-14 14:51:48 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.dodico.calcul;
import java.io.PrintStream;
/**
 * Interface pour un controleur d'un executable.
 * @author deniger
 * @version $Id: CalculLauncher.java,v 1.7 2006-04-14 14:51:48 deniger Exp $
 */
public interface CalculLauncher {

  /**
   * Lance l'execution.
   */
  void execute();

  /**
   * Arrete l'execution.
   */
  void stop();
  /**
   * @return true si l'execution est terminee
   */
  boolean isFinished();
  /**
   * @param _l le listener a ajoute
   */
  void addListener(CalculListener _l);
  /**
   * @param _l le listener a tester
   * @return true si contient le listener _l
   */
  boolean containsListener(CalculListener _l);
  /**
   * @param _l le listener a enlever
   */
  void removeListener(CalculListener _l);
  /**
   * @param _err le flux qui recevra les messages d'erreur de l'execution
   */
  void setErr(PrintStream _err);
  /**
   * @param _std le flux qui recevra les messages standard de l'exe
   */
  void setStd(PrintStream _std);
}
