/*
 *  @creation     22 oct. 2004
 *  @modification $Date: 2006-12-22 14:38:09 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.dodico.calcul;

import java.io.File;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluUI;

/**
 * @author Fred Deniger
 * @version $Id: CalculExecDefault.java,v 1.4 2006-12-22 14:38:09 deniger Exp $
 */
public class CalculExecDefault extends CalculExec {

  String[] coms_;

  public CalculExecDefault(final String[] _com) {
    coms_ = _com;
  }

  @Override
  public String getExecTitle() {
    return coms_[0];
  }

  @Override
  public String[] getLaunchCmd(final File _paramsFile, final CtuluUI _ui) {
    if (_paramsFile != null && !_paramsFile.isDirectory()) {
      String[] res = new String[coms_.length + 1];
      System.arraycopy(coms_, 0, res, 0, coms_.length);

      if (changeWorkingDirectory_) {
        res[res.length - 1] = _paramsFile.getName();
      } else
        res[res.length - 1] = _paramsFile.getAbsolutePath();
      return res;
    }
    return CtuluLibArray.copy(coms_);

  }
}
