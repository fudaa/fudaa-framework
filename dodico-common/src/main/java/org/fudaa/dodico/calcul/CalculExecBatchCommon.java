/*
 * @creation 22 d�c. 06
 * @modification $Date: 2007-05-04 13:45:07 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.calcul;

import java.io.File;
import org.fudaa.ctulu.CtuluLibString;

/**
 * @author fred deniger
 * @version $Id: CalculExecBatchCommon.java,v 1.2 2007-05-04 13:45:07 deniger Exp $
 */
public class CalculExecBatchCommon extends CalculExecBatch {

  String exePath_;
  String args_;

  public CalculExecBatchCommon(final String _exeName, final String _exePath) {
    super(_exeName);
    exePath_ = _exePath;
    setLauchInNewTerm(true);
  }

  @Override
  protected String getCmdForGeneratedBatFile(final String _exe, final File _destFile) {
    if (CtuluLibString.isEmpty(args_)) {
      return super.getCmdForGeneratedBatFile(_exe, _destFile.isDirectory() ? null
          : _destFile);
    }
    return "\"" + _exe + "\" " + args_ + CtuluLibString.ESPACE
        + (_destFile == null ? CtuluLibString.EMPTY_STRING : (CtuluLibString.ESPACE + _destFile.getName()));

  }

  @Override
  public String getExecFile() {
    return exePath_;
  }

  @Override
  public void setExecFile(final String _execPath) {
    super.setExecFile(_execPath);
  }

  public String getArgs() {
    return args_;
  }

  public void setArgs(final String _args) {
    args_ = _args;
  }

}
