/*
 *  @creation     13 juin 2003
 *  @modification $Date: 2006-09-19 14:42:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.dodico.calcul;

import java.util.ArrayList;
import java.util.List;

/**
 * @author deniger
 * @version $Id: CalculLauncherAbstract.java,v 1.10 2006-09-19 14:42:28 deniger Exp $
 */
public abstract class CalculLauncherAbstract implements CalculLauncher {

  protected List listeners_;
  
  public CalculLauncherAbstract(){
  }

  @Override
  public void addListener(final CalculListener _l) {
    if (listeners_ == null) {
      listeners_ = new ArrayList(10);
    }
    listeners_.add(_l);
  }

  /**
   * @see org.fudaa.dodico.calcul.CalculLauncher#containsListener(org.fudaa.dodico.calcul.CalculListener)
   */
  @Override
  public boolean containsListener(final CalculListener _l) {
    if (listeners_ == null) {
      return false;
    }
    return listeners_.contains(_l);
  }

  @Override
  public void removeListener(final CalculListener _l) {
    if (listeners_ == null) {
      return;
    }
    listeners_.remove(_l);
  }
  /**
   * Permet de lancer l'evenement de fin de calcul.
   */
  public void fireFinishedEvent() {
    if (listeners_ == null) {
      return;
    }
    final  int n = listeners_.size() - 1;
    for (int i = n; i >= 0; i--) {
      ((CalculListener) listeners_.get(i)).calculFinished(this);
    }
  }
}
