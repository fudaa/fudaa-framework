/**
 * @creation 12 juin 2003
 * @modification $Date: 2007-03-02 13:00:52 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.calcul;

import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.dodico.commun.DodicoLib;
import org.fudaa.dodico.commun.DodicoPreferences;

/**
 * @author deniger
 * @version $Id: CalculExecBatch.java,v 1.14 2007-03-02 13:00:52 deniger Exp $
 */
public class CalculExecBatch extends CalculExec {

  /**
   * @param _exec le prefix de l'executable qui sert d'id
   * @param _exePath le path pour l'exec
   */
  public static void setExecFile(final String _exec, final String _exePath) {
    DodicoPreferences.DODICO.putStringProperty(_exec + getPrefId(), _exePath);
  }

  private static String getPrefId() {
    return ".exec.path";
  }

  /**
   * @param _exec l'executable
   * @return true si le chemin est d�fini
   */
  public static boolean isExecPathSet(final String _exec) {
    return DodicoPreferences.DODICO.getStringProperty(_exec + getPrefId(), null) != null;
  }

  /**
   * Supprime le path par defaut. serveurs/reflux sera desormais utilise.
   */
  public static void unsetExecFile(final String _exec) {
    DodicoPreferences.DODICO.removeProperty(_exec + getPrefId());
  }

  @Override
  public String getExecTitle() {
    return exec_;
  }

  protected String exec_;

  /**
   * Lancement dans un nouveau repertoire et ajout des commandes pour le mode terminal.
   */
  public CalculExecBatch(final String _exec) {
    setChangeWorkingDirectory(true);
    setLauchInNewTerm(true);
    setStartCmdUse(true);
    exec_ = _exec;
  }

  public String getExecFile() {
    return DodicoPreferences.DODICO.getStringProperty(exec_ + getPrefId(), new File(getTemplateDir() + exec_ + ".exe")
        .getAbsolutePath());
  }

  public final String getExecName() {
    return exec_;
  }

  public boolean isOSSupported() {
    return FuLib.isWindows() || FuLib.isLinux() || FuLib.isUnix();
  }

  public boolean isExecFileExists() {
    return new File(getExecFile()).exists();
  }

  public File createBatFile(final File _paramsFile) {
    String ext=FuLib.isWindows()?".bat":".sh";
    return new File(_paramsFile.isDirectory() ? _paramsFile : _paramsFile.getParentFile(), exec_ + "-"
        + _paramsFile.getName() + ext);
  }

  public String[] getLaunchCmdWithTemplate(final File _paramsFile, final CtuluUI _ui) {
    final File exec = new File(getExecFile());
    if (!exec.exists()) {
      _ui.error(null, DodicoLib.getS("Ex�cutable non trouv�", exec.getAbsolutePath()), false);
      return null;
    }
    Reader template = null;
    template = new InputStreamReader(getClass().getResourceAsStream(getTemplateFile()));
    final File dest = createBatFile(_paramsFile);
    if ((!dest.exists() && !dest.getParentFile().canWrite()) || (dest.exists() && !dest.canWrite())) {
      _ui.error(DodicoLib.getS("S�curit�"), DodicoLib.getS(
          "Le fichier {0} ne peut pas �tre cr�e. V�rifier les droits d'�criture.", dest.getAbsolutePath()), false);
      return null;
    }
    if (!CtuluLibFile.replaceAndCopyFile(template, dest, "@exe@", exec.getAbsolutePath(), "@project@",
        getProjectPath(_paramsFile), null)) {
      _ui.error(DodicoLib.getS("Copie de fichier"), DodicoLib.getS("Erreur lors de la cr�ation du fichier {0} ", dest
          .getAbsolutePath()), false);
      return null;
    }
    try {
      template.close();
    } catch (final IOException e) {
      ui_.error(e.getMessage());
      e.printStackTrace();
    }
    // System.err.println(dest.getAbsolutePath());
    return new String[] { dest.getName() };

  }

  protected String getCmdForGeneratedBatFile(final String _exe, final File _destFile) {
    return "\"" + _exe + "\""
        + (_destFile == null ? CtuluLibString.EMPTY_STRING : (CtuluLibString.ESPACE + _destFile.getName()));
  }

  /**
   * Par d�faut, on appelle "exe" 'file'. Si "exe" contient un espace on construit un fichier bat.
   */
  @Override
  public String[] getLaunchCmd(final File _paramsFile, final CtuluUI _ui) {
    final String exe = getExecFile();
    if (exe == null) {
      return null;
    }
    if (mustCreateBatFile(exe)) {
      final File dest = createBatFile(_paramsFile);
      final String err = CtuluLibFile.canWrite(dest);
      if (err != null && _ui != null) {
        _ui.error(exe, err, false);
        return null;
      }
      FileWriter writer = null;
      try {
        writer = new FileWriter(dest);
        writer.write(getCmdForGeneratedBatFile(exe, _paramsFile));
      } catch (final IOException e) {
        e.printStackTrace();
        if (_ui != null) {
          _ui.error(exe, e.getMessage(), false);
        }
        FuLog.error(e);
        return null;
      } finally {
        if (writer != null) {
          try {
            writer.close();
          } catch (final IOException e) {
            e.printStackTrace();
          }
        }
      }
      return new String[] { dest.getName() };

    }

    return new String[] { exe, _paramsFile.getName() };

  }

  protected boolean mustCreateBatFile(final String _exe) {
    return _exe.indexOf(' ') > 0;
  }

  public String getProjectPath(final File _paramsFile) {
    return CtuluLibFile.getSansExtension(_paramsFile.getName());
  }

  public String getTemplateDir() {
    return "serveurs/" + exec_ + "/";
  }

  public String getTemplateFile() {
    if (FuLib.isWindows()) {
      return exec_ + "-launch.tpl.bat";
    }
    return exec_ + "-launch.tpl.sh";
  }

  public void setExecFile(final String _execPath) {
    setExecFile(exec_, _execPath);
  }

  /**
   * @see java.lang.Object#toString()
   */
  public String toString() {
    return DodicoLib.getS("Ex�cutable" + CtuluLibString.ESPACE + exec_);
  }

}
