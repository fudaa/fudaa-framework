/*
 *  @creation     16 janv. 2006
 *  @modification $Date: 2006-10-19 14:15:26 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.fudaa.commun.aide.FudaaHelpPanel;

/**
 * @author Fred Deniger
 * @version $Id: TestHelp.java,v 1.5 2006-10-19 14:15:26 deniger Exp $
 */
public final class HelpExample {
  private HelpExample() {}

  public static void main(final String[] _args) {
    URL url = null;
    try {
      File jar = null;
      try {
        jar = new File("../aide/src/examples/build/example-help.jar").getCanonicalFile();
      } catch (final IOException e) {
        e.printStackTrace();
        System.exit(1);
      }
      if ((jar == null || !jar.exists())) {
        System.out.println("aucune ressource disponible " + jar + " non trouv�");
        System.exit(1);
        return;
      }
      url = new URL("jar:" + jar.toURI().toURL().toString() + "!/aide/example/");
      System.out.println(url);
      if (!CtuluLibArray.isEmpty(_args)) {
        final String arg = _args[0];
        if ("-inpath".equals(arg)) {
          System.out.println("In path: le jar example-help.jar doit etre dans votre path");
          url = ClassLoader.getSystemClassLoader().getResource("aide/example/");
          System.out.println("url trouve " + url);

        } else {
          File f = new File(arg).getAbsoluteFile();
          System.out.println("ouverture de " + f.getAbsolutePath());
          if (!f.isDirectory()) {
            f = f.getParentFile();
          }
          if (!f.exists() || !f.isDirectory()) {
            System.out.println("Le dossier n'existe pas");
            System.exit(1);
          }
          url = f.toURI().toURL();
        }
      }
    } catch (final MalformedURLException e1) {
      e1.printStackTrace();
      System.exit(1);
    }

    final FudaaHelpPanel pn = new FudaaHelpPanel(null, url, null);
    pn.setStandalone(true);
    pn.showInFrame();

  }

}
