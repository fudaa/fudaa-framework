/*
 * @creation 9 mars 07
 * @modification $Date: 2007-03-15 17:01:22 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun;

import com.memoire.bu.BuApplication;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuResource;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import org.fudaa.fudaa.commun.report.FudaaReportFille;
import org.fudaa.fudaa.commun.report.FudaaReportFrameController;

/**
 * @author fred deniger
 * @version $Id: TestFudaaDjaFrame.java,v 1.2 2007-03-15 17:01:22 deniger Exp $
 */
public final class FudaaDjaFrameExample {

  private FudaaDjaFrameExample() {}

  public static void main(String[] _args) {
    BuApplication app = new BuApplication();
    BuCommonImplementation impl = new BuCommonImplementation();
    app.setImplementation(impl);
    app.init();
    app.start();
    FudaaReportFrameController control = new FudaaReportFrameController(impl);
    FudaaReportFille fille = control.getFille();
    JComponent cmp = fille.createPanelComponent();
    fille.majComponent(cmp);
    impl.getMainPanel().getRightColumn().addToggledComponent(fille.getComponentTitle(), "TOGGLE_SPEC",
        BuResource.BU.getToolIcon("arbre"), new JScrollPane(cmp), true, impl);

  }
}
