/*
 * @file         TestBu.java
 * @creation     2002-10-30
 * @modification $Date: 2006-09-22 15:47:11 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun;

import com.memoire.bu.BuGridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 * @version $Id: TestBu.java,v 1.4 2006-09-22 15:47:11 deniger Exp $
 * @author Fred Deniger
 */
public class BuExample extends JFrame implements ActionListener {
  public BuExample() {
    init();
  }

  private void init() {
    setSize(300, 300);
    final JPanel p = new JPanel(new BuGridLayout(3));
    final JButton b1 = new JButton("JDialog");
    b1.addActionListener(this);
    b1.setActionCommand("JDIALOG".intern());
    final JButton b11 = new JButton("DialogF");
    b11.addActionListener(this);
    b11.setActionCommand("DIALOGF".intern());
    final JButton b2 = new JButton("DialogC");
    b2.addActionListener(this);
    b2.setActionCommand("DIALOGC".intern());
    p.add(b1);
    p.add(b11);
    p.add(b2);
    setContentPane(p);
    doLayout();
    pack();
    setVisible(true);
  }
  private static class DialogFred extends JDialog {
    public DialogFred(final JFrame _f, final boolean _b) {
      super(_f, _b);
    }

    @Override
    public String toString() {
      return "JDialogFred";
    }
  }
  private static class DialogChris extends JDialog {
    public DialogChris(final JFrame _f, final boolean _b) {
      super(_f, _b);
    }

    @Override
    public String toString() {
      return "JDialogChris";
    }
  }

  @Override
  public void actionPerformed(final ActionEvent _ae) {
    final String com = _ae.getActionCommand();
    System.out.println(com);
    if ("DIALOGF" == com) {
      System.out.println("F)");
      DialogFred b = new DialogFred(this, true);
      final JPanel p = new JPanel();
      p.add(new JLabel("coucou"));
      b.setContentPane(p);
      p.revalidate();
      b.setSize(200, 200);
      b.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
      b.pack();
      b.setVisible(true);
      System.out.println("fermeture");
      b.dispose();
      b = null;
    } else if ("DIALOGC" == com) {
      System.out.println("C");
      DialogChris b = new DialogChris(this, true);
      final JPanel p = new JPanel();
      p.add(new JLabel("coucou"));
      b.setContentPane(p);
      p.revalidate();
      b.setSize(200, 200);
      b.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
      b.pack();
      b.setVisible(true);
      System.out.println("fermeture");
      b.dispose();
      b = null;
    } else if ("JDIALOG" == com) {
      System.out.println("J");
      JDialog b = new JDialog(this, true);
      final JPanel p = new JPanel();
      p.add(new JLabel("coucou"));
      b.setContentPane(p);
      p.revalidate();
      b.setSize(200, 200);
      b.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
      b.pack();
      b.setVisible(true);
      System.out.println("fermeture");
      b.dispose();
      b = null;
    }
  }

  public static void main(final String[] _args) {
    new BuExample();
  }
}
