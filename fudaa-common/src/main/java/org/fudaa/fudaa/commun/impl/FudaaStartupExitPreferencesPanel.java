/**
 *  @creation     31 janv. 2005
 *  @modification $Date: 2008-01-15 11:30:24 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuVerticalLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * Panneau de preferences lancement/arret. Gere la mise a jour, l'apparition du
 * SplashScreen, la confirmation de sortie. Cat�gorie Systeme des pr�f�rences. 
 * 
 * @author Fred Deniger
 * @version $Id: FudaaStartupExitPreferencesPanel.java,v 1.2 2008-01-15 11:30:24 bmarchan Exp $
 */
public class FudaaStartupExitPreferencesPanel extends BuAbstractPreferencesPanel implements ActionListener {

  public static final String PREF_CHECK_NEW_VERSION = "check.update";
  
  private final String splash_ = "splashscreen.visible";
  private final String exit_ = "confirm.exit";
  
  final BuCheckBox cbExit_;
  final BuCheckBox cbSplash_;
  final BuCheckBox cbUpdate_;

  public FudaaStartupExitPreferencesPanel(final boolean _update) {
    setLayout(new BuVerticalLayout());
    if (_update) {
      cbUpdate_ = new BuCheckBox();
      cbUpdate_.setText(FudaaLib.getS("Tester les mises � jour au d�marrage"));
      cbUpdate_.addActionListener(this);
      add(cbUpdate_);
    } else {
      cbUpdate_ = null;
    }
    cbSplash_ = new BuCheckBox();
    cbSplash_.setText(FudaaLib.getS("Banni�re � l'ouverture (splash screen)"));
    cbSplash_.setToolTipText(FudaaLib.getS("Si activ�, une banni�re est affich�e lors du lancement de l'application"));
    cbSplash_.addActionListener(this);
    add(cbSplash_);
    cbExit_ = new BuCheckBox();
    cbExit_.setText(FudaaLib.getS("Confirmer la fermeture"));
    cbExit_.setToolTipText(FudaaLib
        .getS("Si activ�, une bo�te de dialogue demande � l'utilisateur de confirmer la fermeture de l'application"));
    cbExit_.addActionListener(this);
    add(cbSplash_);
    add(cbExit_);
    cancelPreferences();
  }

  public static boolean isSplashActivated() {
    return BuPreferences.BU.getBooleanProperty("splashscreen.visible", true);
  }

  @Override
  public String getCategory() {
    return BuResource.BU.getString("Syst�me");
  }

  public static boolean isExitConfirmed() {
    return BuPreferences.BU.getBooleanProperty("confirm.exit", true);
  }

  @Override
  public void validatePreferences() {
    BuPreferences.BU.putBooleanProperty(exit_, cbExit_.isSelected());
    BuPreferences.BU.putBooleanProperty(splash_, cbSplash_.isSelected());
    if (cbUpdate_ != null) {
      BuPreferences.BU.putBooleanProperty(PREF_CHECK_NEW_VERSION, cbUpdate_.isSelected());
    }
    setDirty(false);
  }

  @Override
  public final void cancelPreferences() {
    cbExit_.setSelected(BuPreferences.BU.getBooleanProperty(exit_, true));
    cbSplash_.setSelected(BuPreferences.BU.getBooleanProperty(splash_, true));
    if (cbUpdate_ != null) {
      cbUpdate_.setSelected(BuPreferences.BU.getBooleanProperty(PREF_CHECK_NEW_VERSION, true));
    }

    setDirty(false);
  }

  @Override
  public boolean isPreferencesValidable() {
    return true;
  }

  @Override
  public boolean isPreferencesCancelable() {
    return true;
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    setDirty(true);
  }

  @Override
  public String getTitle() {
    return FudaaLib.getS("D�marrage / Sortie");
  }
}