/*
 *  @file         FudaaExecCellRenderer.java
 *  @creation     4 juin 2003
 *  @modification $Date: 2006-09-19 15:01:54 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.exec;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;

/**
 * @author deniger
 * @version $Id: FudaaExecCellRenderer.java,v 1.12 2006-09-19 15:01:54 deniger Exp $
 */
public class FudaaExecCellRenderer extends CtuluCellTextRenderer {
  @Override
  public void setValue(final Object _o) {
    final FudaaExec e= (FudaaExec)_o;
    if (e == null) {
      setText(CtuluLibString.EMPTY_STRING);
      setToolTipText(CtuluLibString.EMPTY_STRING);
      setIcon(FudaaExec.EMPTY_TOOL_ICON);
    } else {
      setText(e.getViewedName());
      setIcon(e.getIcon());
      setToolTipText(e.getExecCommand());
    }
  }
}
