/*
 * @creation 8 f�vr. 07
 * @modification $Date: 2007-03-30 15:37:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.dja.DjaGridEvent;
import com.memoire.dja.DjaGridListener;
import com.memoire.dja.DjaObject;
import com.memoire.dja.DjaOwner;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.AbstractListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;

/**
 * @author fred deniger
 * @version $Id: FudaaDjaFormListener.java,v 1.3 2007-03-30 15:37:08 deniger Exp $
 */
public class FudaaDjaFormListener extends AbstractListModel implements DjaGridListener, ListSelectionListener {

  public interface SelectionListener {

    void selectionChanged(DjaGridEvent _evt);
  }

  final FudaaReportFrameController controller_;

  protected class TableCellModel extends AbstractTableModel {

    @Override
    public int getColumnCount() {
      return 2;
    }

    @Override
    public String getColumnName(int _column) {
      if (_column == 1) return CtuluLib.getS("Titre");
      return CtuluLibString.EMPTY_STRING;
    }

    @Override
    public int getRowCount() {
      return getSize();
    }

    @Override
    public Object getValueAt(int _rowIndex, int _columnIndex) {
      if (_columnIndex == 0) return getElementAt(_rowIndex);
      return getFudaaDjaObject(_rowIndex).getTitle();
    }

    @Override
    public boolean isCellEditable(int _rowIndex, int _columnIndex) {
      return _columnIndex == 1;
    }

    @Override
    public void setValueAt(Object _value, int _rowIndex, int _columnIndex) {
      if (_columnIndex == 1) {
        FudaaDjaFormInterface dja = getFudaaDjaObject(_rowIndex);
        dja.setTitle((String) _value);
        fireTableCellUpdated(_rowIndex, _columnIndex);
      }
    }

    @Override
    public Class getColumnClass(int _columnIndex) {
      return _columnIndex == 0 ? FudaaDjaFormInterface.class : String.class;
    }

  }

  final Set selectionListener_;

  final DefaultListSelectionModel model_;

  boolean isInternalUpdate_;

  TableCellModel tableModel_;

  public TableModel getTableModel() {
    if (tableModel_ == null) tableModel_ = new TableCellModel();
    return tableModel_;
  }

  public void setTableModel(TableCellModel _tableModel) {
    tableModel_ = _tableModel;
  }

  public FudaaDjaFormListener(final FudaaReportFrameController _controller) {
    super();
    controller_ = _controller;
    controller_.grid_.addGridListener(this);
    selectionListener_ = new HashSet();
    model_ = new DefaultListSelectionModel();
    updateFromDja();
    model_.addListSelectionListener(this);
  }

  protected void updateFromDja() {
    if (isInternalUpdate_) {
      return;
    }
    isInternalUpdate_ = true;
    for (int i = getSize() - 1; i >= 0; i--) {
      boolean selected = isSelectedInDjaVector(i);
      if (selected && !model_.isSelectedIndex(i)) {
        model_.addSelectionInterval(i, i);
      } else if (!selected && model_.isSelectedIndex(i)) {
        model_.removeSelectionInterval(i, i);
      }
    }
    isInternalUpdate_ = false;

  }

  private boolean isSelectedInDjaVector(final int _i) {
    return getDjaObject(_i) != null && getDjaObject(_i).isSelected();
  }

  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    if (isInternalUpdate_ || _e.getValueIsAdjusting()) {
      return;
    }
    isInternalUpdate_ = true;
    for (int i = _e.getFirstIndex(); i <= _e.getLastIndex(); i++) {
      if (i < 0) {
        continue;
      }
      boolean selected = model_.isSelectedIndex(i);
      boolean djaSelected = isSelectedInDjaVector(i);
      if (selected && !djaSelected) {
        controller_.grid_.addSelection(getDjaObject(i));
      }
      if (!selected && djaSelected) {
        controller_.grid_.removeSelection(getDjaObject(i));
      }

    }
    isInternalUpdate_ = false;

  }

  DjaObject getDjaObject(final int _i) {
    if(_i<0|| _i>=getSize()) return null;
    return (DjaObject) getElementAt(_i);
  }

  FudaaDjaFormInterface getFudaaDjaObject(final int _i) {
    return (FudaaDjaFormInterface) getElementAt(_i);
  }

  protected void fireSelectionChanged(final DjaGridEvent _evt) {

    for (final Iterator it = selectionListener_.iterator(); it.hasNext();) {
      ((SelectionListener) it.next()).selectionChanged(_evt);

    }
  }

  public void addSelectionListener(final SelectionListener _l) {
    selectionListener_.add(_l);
  }

  @Override
  public Object getElementAt(final int _index) {
    return controller_.grid_.getObjects().elementAt(_index);
  }

  @Override
  public int getSize() {
    return controller_.grid_.getObjects().size();
  }

  @Override
  public void objectAdded(final DjaGridEvent _evt) {
    if (_evt.getObject() instanceof FudaaDjaFormInterface) {
      final FudaaDjaFormInterface object = (FudaaDjaFormInterface) _evt.getObject();
      object.active(controller_);
      controller_.objectAdded(object);
    }
    final int idxAdded = 0;
    fireIntervalAdded(this, idxAdded, idxAdded);
    if (tableModel_ != null) tableModel_.fireTableRowsInserted(idxAdded, idxAdded);

  }

  @Override
  public void objectConnected(final DjaGridEvent _evt) {}

  @Override
  public void objectDisconnected(final DjaGridEvent _evt) {}

  @Override
  public void objectModified(final DjaGridEvent _evt) {
    DjaOwner owner = _evt.getObject();
    if (owner == null) return;
    int idx = controller_.grid_.getObjects().indexOf(owner);
    if (idx >= 0) {
      fireContentsChanged(this, idx, idx);
      if (tableModel_ != null) tableModel_.fireTableRowsUpdated(idx, idx);
    }

  }

  @Override
  public void objectRemoved(final DjaGridEvent _evt) {
    if (_evt.getObject() instanceof FudaaDjaFormInterface) {
      final FudaaDjaFormInterface object = (FudaaDjaFormInterface) _evt.getObject();
      object.unactive(controller_);
      controller_.objectRemoved(object);
    }
    // quel indice ?
    fireIntervalRemoved(this, 0, getSize() == 0 ? 0 : getSize() - 1);
    if (tableModel_ != null) tableModel_.fireTableRowsDeleted(0, getSize() == 0 ? 0 : getSize() - 1);

  }

  @Override
  public void objectSelected(final DjaGridEvent _evt) {
    fireSelectionChanged(_evt);
    updateFromDja();
  }

  @Override
  public void objectUnselected(final DjaGridEvent _evt) {
    fireSelectionChanged(_evt);
    updateFromDja();
  }

  public void removeSelectionListener(final SelectionListener _l) {
    selectionListener_.remove(_l);
  }

  public DefaultListSelectionModel getSelectionModel() {
    return model_;
  }

}
