/*
 * @creation 8 f�vr. 07
 * @modification $Date: 2007-05-04 13:58:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.dja.DjaOwner;
import java.beans.PropertyChangeListener;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;


/**
 * @author fred deniger
 * @version $Id: FudaaDjaConfigureAbstract.java,v 1.2 2007-05-04 13:58:05 deniger Exp $
 */
public abstract class FudaaDjaConfigureAbstract implements /*BConfigurableSectionInterface, */BSelecteurTargetInterface,
    BConfigurableInterface {

  DjaOwner target_;
  final String title_;

  public FudaaDjaConfigureAbstract(final DjaOwner _target, final String _title) {
    super();
    target_ = _target;
    title_ = _title;
  }

  @Override
  public void removePropertyChangeListener(String _key, PropertyChangeListener _l) {}

  @Override
  public void addPropertyChangeListener(String _key, PropertyChangeListener _l) {}

  @Override
  public Object getMin(String _key) {
    return getProperty(_key);
  }

  @Override
  public Object getMoy(String _key) {
    return getProperty(_key);
  }

  @Override
  public BConfigurableInterface[] getSections() {
    return null;

  }

  @Override
  public BSelecteurTargetInterface getTarget() {
    return this;
  }

  @Override
  public String getTitle() {
    return title_;
  }

  public void repaintGrid() {
    target_.getGrid().repaint();
  }

  @Override
  public void stopConfiguration() {}

 

}
