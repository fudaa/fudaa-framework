package org.fudaa.fudaa.commun.trace2d;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
/**
 * Trac� d'un symbole dans le contexte graphique.
 *
 * @version      $Revision: 1.2 $ $Date: 2006-09-19 15:10:23 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class TraceSymbole {
  /** Contexte graphique. */
  private Graphics2D g2d_;
  /** Couleur de trac� */
  private Color fgc_= Color.black;
  /** Taille de trac� des symboles */
  private int taille_= 15;
  /**
   * Cr�ation d'un traceur de symboles
   */
  public TraceSymbole(Graphics2D _g) {
    g2d_= _g;
  }
  /**
   * D�finition de la couleur de trac�.
   */
  public void setCouleur(Color _fgc) {
    fgc_= _fgc;
  }
  /**
   * Retourne la couleur de trac�.
   */
  public Color getCouleur() {
    return fgc_;
  }
  /**
   * D�finition de la taille de trac� du symbole.
   */
  public void setTaille(int _taille) {
    taille_= _taille;
  }
  /**
   * Retourne la taille de trac� du symbole.
   */
  public int getTaille() {
    return taille_;
  }
  /**
   * Trac� d'un symbole ancr� dans le contexte graphique � la position _x,_y
   */
  public void dessineSymboleAncre(Symbole _s, double _x, double _y) {
    AffineTransform at= new AffineTransform();
    at.setToTranslation(_x, _y);
    at.rotate(_s.rotationZ * Math.PI / 180.);
    at.scale(taille_, taille_);
    at.translate(-_s.xAncre, -_s.yAncre);
    //    at.translate(-.5, -.5);
    Shape sh= at.createTransformedShape(_s.trace);
    g2d_.setColor(fgc_);
    if (!_s.plein)
      g2d_.draw(sh);
    else
      g2d_.fill(sh);
  }
  /**
   * Trac� d'un symbole centr� � la position _x, _y.
   */
  public void dessineSymbole(Symbole _s, double _x, double _y) {
    AffineTransform at= new AffineTransform();
    at.setToTranslation(_x, _y);
    at.rotate(_s.rotationZ * Math.PI / 180.);
    at.scale(taille_, taille_);
    //    at.translate(-_s.xAncre, -_s.yAncre);
    at.translate(-.5, -.5);
    Shape sh= at.createTransformedShape(_s.trace);
    g2d_.setColor(fgc_);
    if (!_s.plein)
      g2d_.draw(sh);
    else
      g2d_.fill(sh);
  }
  public static void main(String[] _args) {
    JFrame f= new JFrame("Trac� de symboles");
    JComponent c= new JComponent() {
      @Override
      public void paintComponent(Graphics _g) {
        TraceSymbole ts= new TraceSymbole((Graphics2D)_g);
        double y= 20;
        int taille= 10;
        for (int i= 0; i < 4; i++) {
          double x= 0;
          Symbole sa= new Symbole(Symbole.CARRE, 0);
          Symbole s1= new Symbole(Symbole.CARRE_PLEIN, i * 45);
          Symbole s2= new Symbole(Symbole.CERCLE_PLEIN, i * 45);
          Symbole s3= new Symbole(Symbole.FLECHE, i * 45);
          Symbole s4= new Symbole(Symbole.FLECHE_DIFFRACTEE, i * 45);
          Symbole s5= new Symbole(Symbole.FLECHE_LIEE, i * 45);
          Symbole s6= new Symbole(Symbole.LOSANGE, i * 45);
          Symbole s7= new Symbole(Symbole.TRIANGLE_PLEIN, i * 45);
          x += taille * 1.5;
          ts.setCouleur(Color.blue);
          ts.setTaille(taille);
          ts.dessineSymboleAncre(s1, x, y);
          ts.setCouleur(Color.black);
          ts.setTaille(1);
          ts.dessineSymboleAncre(sa, x, y);
          x += taille * 1.5;
          ts.setCouleur(Color.green);
          ts.setTaille(taille);
          ts.dessineSymboleAncre(s2, x, y);
          ts.setCouleur(Color.black);
          ts.setTaille(1);
          ts.dessineSymboleAncre(sa, x, y);
          x += taille * 1.5;
          ts.setCouleur(Color.red);
          ts.setTaille(taille);
          ts.dessineSymboleAncre(s3, x, y);
          ts.setCouleur(Color.black);
          ts.setTaille(1);
          ts.dessineSymboleAncre(sa, x, y);
          x += taille * 1.5;
          ts.setCouleur(Color.cyan);
          ts.setTaille(taille);
          ts.dessineSymboleAncre(s4, x, y);
          ts.setCouleur(Color.black);
          ts.setTaille(1);
          ts.dessineSymboleAncre(sa, x, y);
          x += taille * 1.5;
          ts.setCouleur(Color.magenta);
          ts.setTaille(taille);
          ts.dessineSymboleAncre(s5, x, y);
          ts.setCouleur(Color.black);
          ts.setTaille(1);
          ts.dessineSymboleAncre(sa, x, y);
          x += taille * 1.5;
          ts.setCouleur(Color.orange);
          ts.setTaille(taille);
          ts.dessineSymboleAncre(s6, x, y);
          ts.setCouleur(Color.black);
          ts.setTaille(1);
          ts.dessineSymboleAncre(sa, x, y);
          x += taille * 1.5;
          ts.setCouleur(Color.pink);
          ts.setTaille(taille);
          ts.dessineSymboleAncre(s7, x, y);
          ts.setCouleur(Color.black);
          ts.setTaille(1);
          ts.dessineSymboleAncre(sa, x, y);
          taille= taille + 10;
          y += taille;
        }
      }
    };
    c.setPreferredSize(new Dimension(450, 150));
    JPanel pnPns= new JPanel(new FlowLayout());
    class JPanelSymb extends JPanel {
      private Symbole s;
      private int taille;
      public JPanelSymb(Symbole _s, int _taille) {
        s= _s;
        taille= _taille;
        setBackground(Color.yellow.darker());
      }
      @Override
      public void paintComponent(Graphics _g) {
        super.paintComponent(_g);
        TraceSymbole ts= new TraceSymbole((Graphics2D)_g);
        ts.dessineSymbole(s, taille / 2, taille / 2);
      }
      @Override
      public Dimension getPreferredSize() {
        return new Dimension(taille + 1, taille + 1);
      }
    };
    pnPns.add(new JPanelSymb(new Symbole(Symbole.CARRE, 45), 15));
    pnPns.add(new JPanelSymb(new Symbole(Symbole.CERCLE, 0), 15));
    pnPns.add(new JPanelSymb(new Symbole(Symbole.FLECHE, 90), 15));
    pnPns.add(new JPanelSymb(new Symbole(Symbole.FLECHE_DIFFRACTEE, 27), 20));
    pnPns.add(new JPanelSymb(new Symbole(Symbole.FLECHE_LIEE, 180), 20));
    pnPns.add(new JPanelSymb(new Symbole(Symbole.LOSANGE_PLEIN, 0), 20));
    pnPns.add(new JPanelSymb(new Symbole(Symbole.TRIANGLE, 180), 20));
    JPanel pnButs= new JPanel(new FlowLayout());
    pnButs.add(
      new JButton(
        new SymboleIcon(new Symbole(Symbole.CARRE_PLEIN, 45), Color.red, 45)));
    pnButs.add(
      new JButton(new SymboleIcon(new Symbole(Symbole.CERCLE, 0), 64)));
    pnButs.add(
      new JButton(new SymboleIcon(new Symbole(Symbole.FLECHE, 90), 16)));
    pnButs.add(
      new JButton(
        new SymboleIcon(new Symbole(Symbole.FLECHE_DIFFRACTEE, 27), 40)));
    pnButs.add(
      new JButton(new SymboleIcon(new Symbole(Symbole.FLECHE_LIEE, 180), 16)));
    pnButs.add(
      new JButton(new SymboleIcon(new Symbole(Symbole.LOSANGE, 0), 20)));
    pnButs.add(
      new JButton(new SymboleIcon(new Symbole(Symbole.TRIANGLE, 180), 16)));
    f.getContentPane().add(pnButs, BorderLayout.NORTH);
    f.getContentPane().add(c, BorderLayout.CENTER);
    f.getContentPane().add(pnPns, BorderLayout.SOUTH);
    f.pack();
    f.setVisible(true);
  }
}
