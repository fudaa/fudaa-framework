/**
 * @modification $Date: 2007-05-04 13:58:05 $
 * @statut       unstable
 * @file         BuBrowserFrame.java
 * @version      0.43
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2005 Guillaume Desnoix
 */

package org.fudaa.fudaa.commun.aide;

// import com.memoire.dnd.*;
import com.memoire.bu.*;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyVetoException;
import java.net.URL;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluPopupMenu;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.commun.FudaaBrowserControl;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * A tiny HTML browser. Is an internal frame on the desktop and an alternative to an external browser as lynx ;-)
 */
public class FudaaHelpPanel extends BuPanel implements ActionListener, BuCutCopyPasteInterface, FudaaHelpParentI {

  private static final int GAP = BuPreferences.BU.getIntegerProperty("layout.gap", 5);

  protected BuCommonImplementation app_;
  protected JFrame frame_;
  protected BuInternalFrame iframe_;
  protected BuTextField tfURL_;
  protected BuStatusBar sbStatus_;
  protected FudaaHelpHtmlPanel pane_;
  protected BuPanel container_;
  protected BuPanel urlBar_;

  protected BuButton btAvancer_;
  protected BuButton btFull_;
  protected BuButton btReculer_;
  protected BuButton btRafraichir_;
  protected BuButton btMaison_;
  boolean standalone_;

  public final FudaaHelpHtmlPanel getHtmlPane() {
    return pane_;
  }

  public final boolean isStandalone() {
    return standalone_;
  }

  public final void setStandalone(final boolean _standalone) {
    standalone_ = _standalone;
  }

  public FudaaHelpPanel() {
    this(null);
  }

  protected final FudaaHelpHtmlPanel createHtmlPanel(final URL _base) {
    return new FudaaHelpHtmlPanel(_base, this);
  }

  public FudaaHelpPanel(final BuCommonImplementation _app) {
    this(_app, null, null);
  }

  URL pdf_;

  public FudaaHelpPanel(final BuCommonImplementation _app, final URL _source, final URL _pdf) {
    app_ = _app;

    setName("ifNAVIGATEUR");
    tfURL_ = new BuTextField(CtuluLibString.EMPTY_STRING);
    tfURL_.setEditable(false);
    urlBar_ = new BuPanel(new BuBorderLayout(GAP, GAP, true, false));
    // urlBar_.setBorder(new EmptyBorder(2,5,2,5));
    // urlBar_.setBorder(BuLib.getEmptyBorder(GAP));//BuBorders.EMPTY5555);
    urlBar_.add(tfURL_, BuBorderLayout.CENTER);

    sbStatus_ = new BuStatusBar(false, false);
    sbStatus_.setFont(BuLib.deriveFont("Label", Font.PLAIN, -2));
    // sbStatus_.setBorder(BuBorders.EMPTY2222);
    sbStatus_.setMessage(" ");

    btAvancer_ = new BuButton();
    btAvancer_.setActionCommand("AVANCER");
    btAvancer_.setToolTipText(getS("Avancer � la page suivante"));
    btAvancer_.setIcon(BuResource.BU.loadToolCommandIcon("AVANCER"));
    btAvancer_.setName("btAVANCER");
    btAvancer_.setMargin(BuInsets.INSETS1111);
    btAvancer_.setRequestFocusEnabled(false);
    btAvancer_.addActionListener(this);

    btReculer_ = new BuButton();
    btReculer_.setActionCommand("RECULER");
    btReculer_.setToolTipText(getS("Revenir � la page pr�c�dente"));
    btReculer_.setIcon(BuResource.BU.loadToolCommandIcon("RECULER"));
    btReculer_.setName("btRECULER");
    btReculer_.setMargin(BuInsets.INSETS1111);
    btReculer_.setRequestFocusEnabled(false);
    btReculer_.addActionListener(this);

    btRafraichir_ = new BuButton();
    btRafraichir_.setActionCommand("RAFRAICHIR");
    btRafraichir_.setToolTipText(getS("Rafra�chir la page"));
    btRafraichir_.setIcon(BuResource.BU.loadToolCommandIcon("RAFRAICHIR"));
    btRafraichir_.setName("btRAFRAICHIR");
    btRafraichir_.setMargin(BuInsets.INSETS1111);
    btRafraichir_.setRequestFocusEnabled(false);
    btRafraichir_.addActionListener(this);

    btMaison_ = new BuButton();
    btMaison_.setActionCommand("MAISON");
    btMaison_.setToolTipText(getS("Aller � la page principale"));
    btMaison_.setIcon(BuResource.BU.getToolIcon("www"));
    btMaison_.setName("btMAISON");
    btMaison_.setMargin(BuInsets.INSETS1111);
    btMaison_.setRequestFocusEnabled(false);
    btMaison_.addActionListener(this);

    btAvancer_.setEnabled(false);
    btReculer_.setEnabled(false);
    pane_ = createHtmlPanel(_source);

    container_ = new BuPanel(new BuBorderLayout(GAP, GAP));// 0,0));
    container_.setBorder(BuLib.getEmptyBorder(GAP));// BuBorders.EMPTY0000);
    container_.add(urlBar_, BuBorderLayout.NORTH);
    container_.add(pane_, BuBorderLayout.CENTER);

    setLayout(new BuBorderLayout(0, 0));
    final BuToolBar bar = new BuToolBar();
    bar.add(btReculer_);
    bar.add(btAvancer_);
    bar.add(btRafraichir_);
    bar.add(btMaison_);
    final BuButton btConf = new BuButton();
    btConf.setActionCommand("CONFIGURE");
    btConf.setToolTipText(getS("Configurer les param�tres de connexion"));
    btConf.setIcon(BuResource.BU.getToolIcon("configurer"));
    btConf.setName("btCONFIGURE");
    btConf.setMargin(BuInsets.INSETS1111);
    btConf.setRequestFocusEnabled(false);
    btConf.addActionListener(this);
    bar.add(btConf);
    if (_pdf != null) {
      pdf_ = _pdf;
      final BuButton btPdf = new BuButton();
      btPdf.setActionCommand("PDF");
      btPdf.setToolTipText("PDF");
      btPdf.setName("btPDF");
      btPdf.setIcon(FudaaResource.FUDAA.getToolIcon("pdfdoc"));
      btPdf.addActionListener(this);
      btPdf.setMargin(BuInsets.INSETS1111);
      bar.add(btPdf);

    }
    if (isAppOk()) {
      btFull_ = new BuButton();
      btFull_.setActionCommand("FULL");
      btFull_.setToolTipText(FudaaLib.getS("D�tacher la fen�tre"));
      btFull_.setIcon(BuResource.BU.getToolIcon("pleinecran"));
      btFull_.setName("btFull");
      btFull_.setMargin(BuInsets.INSETS1111);
      btFull_.setRequestFocusEnabled(false);
      btFull_.addActionListener(this);
      bar.add(btFull_);

    }
    add(bar, BuBorderLayout.NORTH);
    add(container_, BuBorderLayout.CENTER);
    add(sbStatus_, BuBorderLayout.SOUTH);
    // pour le popup
    pane_.getEditorPane().addMouseListener(new MouseListener() {

      @Override
      public void mouseReleased(final MouseEvent _e) {
        if (EbliLib.isPopupMouseEvent(_e)) {
          popup(_e);
        }
      }

      @Override
      public void mousePressed(final MouseEvent _e) {
        if (EbliLib.isPopupMouseEvent(_e)) {
          popup(_e);
        }
      }

      @Override
      public void mouseExited(final MouseEvent _e) {}

      @Override
      public void mouseEntered(final MouseEvent _e) {}

      @Override
      public void mouseClicked(final MouseEvent _e) {
        if (EbliLib.isPopupMouseEvent(_e)) {
          popup(_e);
        }

      }

    });

    final Dimension ps = new Dimension(400, 500);
    setSize(ps);
    setPreferredSize(ps);
  }

  protected String getTitleBase() {
    return getS("Aide");
  }

  // State

  @Override
  public void setMessage(final String _s) {
    sbStatus_.setMessage(_s);
  }

  @Override
  protected final String getS(final String _s) {
    return FudaaLib.getS(_s);
  }

  @Override
  public void setTitle(final String _s) {
    String debTitle = _s;
    if (debTitle == null) {
      debTitle = "";
    }
    debTitle = debTitle.trim();
    if (debTitle.length() > 0) {
      debTitle += " - ";
    }
    final String title = debTitle + getTitleBase();
    if (frame_ != null) {
      frame_.setTitle(title);
    }
    if (iframe_ != null) {
      iframe_.setTitle(title);
    }
    super.putClientProperty("frame.title", title);
  }

  @Override
  public void setBackEnabled(final boolean _e) {
    btReculer_.setEnabled(_e);
  }

  @Override
  public void setForwardEnabled(final boolean _e) {
    btAvancer_.setEnabled(_e);
  }

  // Action

  public void beep() {
    getToolkit().beep();
  }

  public void setParentSelected() {
    pane_.requestFocus();
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    final String action = _evt.getActionCommand();
    // FuLog.debug("BuBrowserFrame : "+action);

    if ("RECULER".equals(action)) {
      pane_.back();
    } else if ("AVANCER".equals(action)) {
      pane_.forward();
    } else if ("RECULER_CHAPITRE".equals(action)) {
      pane_.isBackChapterAvailable(true);
    } else if ("AVANCER_CHAPITRE".equals(action)) {
      pane_.isNextChapterAvailable(true);
    } else if ("RAFRAICHIR".equals(action)) {
      pane_.reload();
    } else if ("MAISON".equals(action)) {
      pane_.goHome();
    } else if ("COPIER".equals(action)) {
      copy();
    } else if ("CONFIGURE".equals(action)) {
      getHtmlPane().showProxy();
    } else if ("PDF".equals(action) && pdf_ != null) {
      if (pdf_.getProtocol().equals("http")) {
        FudaaBrowserControl.displayURL(pdf_);
      } else {
        setDocumentUrl(pdf_, false);
      }
    } else if ("FULL".equals(action)) {
      if (frame_ == null) {
        btFull_.setIcon(FudaaResource.FUDAA.getToolIcon("fermerpleinecran"));
        showInFrame();
      } else {
        btFull_.setIcon(FudaaResource.FUDAA.getToolIcon("pleinecran"));
        showInApp();
      }

    }
  }

  public void showInFrame() {
    shownInApp_ = true;
    if (frame_ == null) {
      if (iframe_ != null) {
        app_.removeInternalFrame(iframe_);
        iframe_.dispose();
        iframe_ = null;
      }
      frame_ = new JFrame() {

        @Override
        protected void processFocusEvent(final java.awt.event.FocusEvent _e) {
          if (_e.getID() == FocusEvent.FOCUS_GAINED) {
            pane_.requestFocus();
          }
        }

        @Override
        public void dispose() {
          super.dispose();
          frame_ = null;
        }
      };
      frame_.setDefaultCloseOperation(standalone_ ? WindowConstants.EXIT_ON_CLOSE : WindowConstants.DISPOSE_ON_CLOSE);
      frame_.setTitle(getFrameTitle());
      frame_.setIconImage(getFrameImage());
      frame_.setContentPane(this);
      frame_.pack();
      frame_.setVisible(true);
      frame_.setExtendedState(Frame.MAXIMIZED_BOTH);
    } else {
      frame_.setVisible(true);
    }
  }

  String getFrameTitle() {
    String title = (String) getClientProperty("frame.title");
    if (title == null) {
      title = getTitleBase();
    }
    return title;
  }

  protected void addMenuItem(final CtuluPopupMenu _menu, final BuButton _bt) {
    _menu.addMenuItem(_bt.getToolTipText(), _bt.getActionCommand(), _bt.getIcon(), _bt.isEnabled()).addActionListener(
        this);
  }

  protected void popup(final MouseEvent _evt) {
    final CtuluPopupMenu menu = new CtuluPopupMenu();
    addMenuItem(menu, btMaison_);
    addMenuItem(menu, btRafraichir_);
    addMenuItem(menu, btAvancer_);
    addMenuItem(menu, btReculer_);
    if (btFull_ != null) {
      menu.addSeparator();
      addMenuItem(menu, btFull_);
    }
    menu.addSeparator();
    menu.addMenuItem(BuResource.BU.getString("Copier"), "COPIER", BuResource.BU.getToolIcon("copier"),
        pane_.isCopyEnable()).addActionListener(this);
    menu.show(_evt.getComponent(), _evt.getX(), _evt.getY());

  }

  private class HelpIframe extends BuInternalFrame implements BuCutCopyPasteInterface {

    HelpIframe() {
      super(getFrameTitle(), true, true, true, true);
    }

    @Override
    public String[] getEnabledActions() {
      return FudaaHelpPanel.this.getEnabledActions();
    }

    @Override
    public void copy() {
      FudaaHelpPanel.this.copy();

    }

    @Override
    public void cut() {
      FudaaHelpPanel.this.cut();

    }

    @Override
    public void duplicate() {
      FudaaHelpPanel.this.duplicate();

    }

    @Override
    public void paste() {
      FudaaHelpPanel.this.paste();

    }

    @Override
    public void setSelected(final boolean _selected) throws PropertyVetoException {
      super.setSelected(_selected);
      if (_selected) {
        pane_.requestFocus();
      }
    }

  }

  protected boolean isAppOk() {
    return app_ != null && app_.getMainPanel().getDesktop() != null;
  }

  public void showHelp() {
    if (shownInApp_ && isAppOk()) {
      showInApp();
    } else {
      showInFrame();
    }
  }

  private boolean shownInApp_ = true;

  public void showInApp() {
    shownInApp_ = true;
    if (iframe_ != null) {
      app_.addInternalFrame(iframe_);
      iframe_.moveToFront();

    } else if (app_ != null) {
      if (frame_ != null) {
        frame_.setVisible(false);
        frame_.dispose();
        frame_ = null;
      }
      iframe_ = new HelpIframe() {
        @Override
        public void dispose() {
          super.dispose();
          iframe_ = null;

        }
      };
      iframe_.setFrameIcon(getFrameIcon());
      iframe_.setContentPane(this);
      iframe_.pack();
      app_.addInternalFrame(iframe_);
      iframe_.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
      try {
        iframe_.setMaximum(true);
      } catch (final PropertyVetoException e) {}
    } else {
      showInFrame();
    }
  }

  protected Image getFrameImage() {
    Image img = null;
    if (app_ != null) {
      img = app_.getFrame().getIconImage();
    }
    if (img == null) {
      img = BuResource.BU.getToolIcon("aide").getImage();
    }
    return img;

  }

  protected Icon getFrameIcon() {
    Icon img = null;
    if (app_ != null) {
      img = app_.getInformationsSoftware().logo;
    }
    if (img == null) {
      img = BuResource.BU.getToolIcon("aide");
    }
    return img;

  }

  public final void setDocumentUrl(final URL _url, final String _title, final boolean _forceReload) {
    setUrlText(_title);
    pane_.setDocumentUrl(_url, _title, _forceReload);

  }

  @Override
  public void setDocumentUrl(final URL _url, final boolean _forceReload) {
    setDocumentUrl(_url, null, _forceReload);
  }

  public final void setHtmlSource(final String _source, final String _title) {
    setUrlText(_title);
    pane_.setSource(_source, _title);

  }

  @Override
  public void setUrlText(final String _title, final boolean _b) {
    setUrlText(_title);
  }

  public void setUrlText(final String _title) {
    tfURL_.setText(_title);
  }

  public String[] getEnabledActions() {
    return new String[] { "COPIER" };
  }

  // BuCutCopyPasteInterface

  @Override
  public void cut() {}

  @Override
  public void copy() {
    pane_.copy();
  }

  @Override
  public void paste() {}

  @Override
  public void duplicate() {}

}
