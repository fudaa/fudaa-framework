/*
 * @creation 8 f�vr. 07
 * @modification $Date: 2007-03-23 17:25:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.bu.BuResource;
import com.memoire.dja.DjaGridEvent;
import com.memoire.dja.DjaGridListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.controle.BSelecteurSpinner;

/**
 * @author fred deniger
 * @version $Id: FudaaDjaConfigurePosition.java,v 1.2 2007-03-23 17:25:30 deniger Exp $
 */
public class FudaaDjaConfigurePosition extends FudaaDjaConfigureAbstract implements DjaGridListener {

  public static final String PROPERTY_POS_X = "posX";
  public static final String PROPERTY_POS_Y = "posY";
  public static final String PROPERTY_WIDTH = "djaW";
  public static final String PROPERTY_HEIGHT = "djaH";
  
  final FudaaDjaGrid grid_;

  public FudaaDjaConfigurePosition(final FudaaDjaFormAbstract _target,FudaaDjaGrid _grid) {
    super(_target, EbliLib.getS("Position"));
    grid_=_grid;
    grid_.addGridListener(this);
  }

  @Override
  public void objectAdded(DjaGridEvent _evt) {}

  @Override
  public void objectConnected(DjaGridEvent _evt) {}

  @Override
  public void objectDisconnected(DjaGridEvent _evt) {}

  @Override
  public void objectModified(DjaGridEvent _evt) {
    if (_evt.getObject() == target_ || _evt.getObject() == null) {
      listener_.firePropertyChange(PROPERTY_POS_Y, null, new Integer(getIfForm().getX()));
      listener_.firePropertyChange(PROPERTY_POS_X, null, new Integer(getIfForm().getY()));
      listener_.firePropertyChange(PROPERTY_WIDTH, null, new Integer(getIfForm().getWidth()));
      listener_.firePropertyChange(PROPERTY_HEIGHT, null, new Integer(getIfForm().getHeight()));
    }

  }

  @Override
  public void objectRemoved(DjaGridEvent _evt) {}

  @Override
  public void objectSelected(DjaGridEvent _evt) {}

  @Override
  public void objectUnselected(DjaGridEvent _evt) {}

  @Override
  public void stopConfiguration() {
    grid_.removeGridListener(this);
    super.stopConfiguration();
  }

  FudaaDjaFormAbstract getIfForm() {
    return (FudaaDjaFormAbstract) target_;
  }

  PropertyChangeSupport listener_ = new PropertyChangeSupport(this);

  @Override
  public void addPropertyChangeListener(String _key, PropertyChangeListener _l) {
    listener_.addPropertyChangeListener(_key, _l);
  }

  @Override
  public void removePropertyChangeListener(String _key, PropertyChangeListener _l) {
    listener_.removePropertyChangeListener(_key, _l);
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    BSelecteurSpinner x = new BSelecteurSpinner(PROPERTY_POS_X, createSlider("X"));
    x.setTitle("X");
    BSelecteurSpinner y = new BSelecteurSpinner(PROPERTY_POS_Y, createSlider("Y"));
    y.setTitle("Y");
    String string = BuResource.BU.getString("Largeur");
    BSelecteurSpinner w = new BSelecteurSpinner(PROPERTY_WIDTH, createSlider(string));
    w.setTitle(string);
    string = BuResource.BU.getString("Hauteur");
    BSelecteurSpinner h = new BSelecteurSpinner(PROPERTY_HEIGHT, createSlider(string));
    h.setTitle(string);
    x.setUnableIfNull(true);
    y.setUnableIfNull(true);
    w.setUnableIfNull(true);
    h.setUnableIfNull(true);
    return new BSelecteurInterface[] { x, y, w, h };

  }

  private JSpinner createSlider(String _title) {
    SpinnerNumberModel model = new SpinnerNumberModel();
    final JSpinner slX = new JSpinner(model);
    slX.setName(_title);
    model.setStepSize(new Integer(5));
    return slX;
  }

  @Override
  public Object getProperty(String _key) {
    if (PROPERTY_POS_X.equals(_key)) {
      return new Integer(getIfForm().getX());
    } else if (PROPERTY_POS_Y.equals(_key)) {
      return new Integer(getIfForm().getY());
    } else if (PROPERTY_WIDTH.equals(_key)) {
      return new Integer(getIfForm().getWidth());
    } else if (PROPERTY_HEIGHT.equals(_key)) {
      return new Integer(getIfForm().getHeight());
    }
    return null;
  }

  @Override
  public boolean setProperty(String _key, Object _newProp) {
    boolean r = false;
    if (PROPERTY_POS_X.equals(_key)) {
      getIfForm().setX(((Number) _newProp).intValue());
      r = true;
    } else if (PROPERTY_POS_Y.equals(_key)) {
      getIfForm().setY(((Number) _newProp).intValue());
      r = true;
    } else if (PROPERTY_WIDTH.equals(_key)) {
      getIfForm().setWidth(((Number) _newProp).intValue());
      r = true;
    } else if (PROPERTY_HEIGHT.equals(_key)) {
      getIfForm().setHeight(((Number) _newProp).intValue());
      r = true;
    }
    if (r) {
      target_.getGrid().repaint();
    }
    return r;
  }
}
