/*
 * @file         FudaaSplashScreen.java
 * @creation     2000-11-07
 * @modification $Date: 2008-02-29 16:47:10 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuPicture;
import com.memoire.bu.BuSplashScreen;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * @version $Revision: 1.2 $ $Date: 2008-02-29 16:47:10 $ by $Author: opasteur $
 * @author Guillaume Desnoix
 */
@SuppressWarnings("serial")
public class FudaaSplashScreen extends BuSplashScreen {

  public FudaaSplashScreen(final BuInformationsSoftware _info, final long _delai, final String[][] _classes) {
    super(_info, _delai, _classes);
  }

  @Override
  public final void build() {
    super.build();
    name_.setVerticalAlignment(SwingConstants.BOTTOM);
    // setForeground(Color.WHITE);
    String text = name_.getText();
    name_.setText(text.startsWith("Fudaa-")?text:("Fudaa-" + text));
    left_.add(new BuPicture(FudaaResource.FUDAA.getIcon("fudaa-logo")));
  }

  /**
   * 
   * @param b attention ne sert � rien !
   */
  public void setTextUnvisible(boolean b) {
    setLabelVisibleFalse(panel_);
  }
  
  public void setTextUnvisible() {
    setLabelVisibleFalse(panel_);
  }

  private void setLabelVisibleFalse(JPanel panel) {
    Component[] comps = panel.getComponents();
    for (int i = 0; i < comps.length; i++) {
      if (comps[i] instanceof JLabel) ((JLabel) comps[i]).setText("      ");
      else if (comps[i] instanceof JPanel) {
        setLabelVisibleFalse((JPanel) comps[i]);
      }
    }
  }

}
