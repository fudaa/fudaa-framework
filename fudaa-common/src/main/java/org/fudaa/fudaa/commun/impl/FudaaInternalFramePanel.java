/*
 * @creation 5 d�c. 06
 * @modification $Date: 2007-03-30 15:36:58 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuBorders;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuScrollPane;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.JList;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author fred deniger
 * @version $Id: FudaaInternalFramePanel.java,v 1.3 2007-03-30 15:36:58 deniger Exp $
 */
public class FudaaInternalFramePanel extends CtuluDialogPanel implements ListSelectionListener {

  private class ActionDecorator implements Action {
    final Action act_;

    public ActionDecorator(final Action _act) {
      act_ = _act;
    }

    protected boolean doBefore() {
      return true;
    }

    protected void doAfter() {}

    @Override
    public void actionPerformed(final ActionEvent _e) {
      if (doBefore()) {
        act_.actionPerformed(_e);
        doAfter();
      }

    }

    @Override
    public void addPropertyChangeListener(final PropertyChangeListener _listener) {
      act_.addPropertyChangeListener(_listener);
    }

    @Override
    public Object getValue(final String _key) {
      return act_.getValue(_key);
    }

    @Override
    public boolean isEnabled() {
      return act_.isEnabled();
    }

    @Override
    public void putValue(final String _key, final Object _value) {
      act_.putValue(_key, _value);
    }

    @Override
    public void removePropertyChangeListener(final PropertyChangeListener _listener) {
      act_.removePropertyChangeListener(_listener);
    }

    @Override
    public void setEnabled(final boolean _b) {
      act_.setEnabled(_b);
    }

  }

  private static class KeyCancel implements KeyListener {
    final JDialog target_;
    final boolean autoSwitch_;

    public KeyCancel(final JDialog _target, final boolean _autoSwitch) {
      super();
      target_ = _target;
      autoSwitch_ = _autoSwitch;
    }

    @Override
    public void keyPressed(final KeyEvent _e) {}

    @Override
    public void keyReleased(final KeyEvent _e) {
      if (_e.getKeyCode() == KeyEvent.VK_ESCAPE || _e.getKeyCode() == KeyEvent.VK_ENTER
          || (autoSwitch_ && !_e.isControlDown())) {
        target_.setVisible(false);
        target_.dispose();
      }

    }

    @Override
    public void keyTyped(final KeyEvent _e) {}

  }

  protected static void addKeyListener(final Component _c, final KeyListener _key) {
    if (_c != null) {
      _c.addKeyListener(_key);
      if (_c instanceof JComponent) {
        final JComponent jc = (JComponent) _c;
        for (int i = jc.getComponentCount() - 1; i >= 0; i--) {
          addKeyListener(jc.getComponent(i), _key);
        }
      }
    }
  }

  public static void active(final BuDesktop _d, final boolean _autoSwitch) {
    final FudaaInternalFramePanel list = new FudaaInternalFramePanel(_d, _autoSwitch);
    final CtuluDialog s = list.createDialog(_d);
    s.setOption(CtuluDialog.ZERO_OPTION);
    s.setTitle(CtuluLib.getS("fen�tres"));
    addKeyListener(list.l_, new KeyCancel(s, _autoSwitch));
    s.addWindowListener(new WindowAdapter() {

      @Override
      public void windowClosed(final WindowEvent _e) {
        if (list.lastSelected_ != null && list.l_.getModel().getSize() > 1) {
          final int idx = list.l_.getSelectedIndex();
          if (list.lastSelected_ != list.frames_[idx]) {
            _d.setPosition(list.lastSelected_, 1);
          }
        }
      }
    });
    s.afficheDialogModal(new Runnable() {

      @Override
      public void run() {
        list.l_.requestFocusInWindow();

      }

    });
  }
  final BuDesktop desktop_;

  JInternalFrame[] frames_;

  JList l_;
  JInternalFrame lastSelected_;

  public FudaaInternalFramePanel(final BuDesktop _desk, final boolean _autoswith) {
    super(false);
    desktop_ = _desk;
    JInternalFrame[] frames = desktop_.getAllFrames();
    if (frames == null) {
      frames = new JInternalFrame[0];
    }
    final List listFrames = new ArrayList();
    // on enleve les palettes
    for (int i = 0; i < frames.length; i++) {
      JInternalFrame internalFrame = frames[i];
      if (!BuLib.isPalette(internalFrame)) {
        listFrames.add(internalFrame);
      }
    }
    frames_ = (JInternalFrame[]) listFrames.toArray(new JInternalFrame[listFrames.size()]);
    setLayout(new BuBorderLayout(0, 4));
    setBorder(BuBorders.EMPTY3333);
    l_ = new JList(frames_);
    l_.setCellRenderer(FudaaLib.createInternalFrameCellRenderer());
    final int idx = CtuluLibArray.findObjectEgalEgal(frames_, desktop_.getSelectedFrame());
    if (idx >= 0) {
      l_.setSelectedIndex(idx);
      lastSelected_ = frames[idx];
    }

    add(new BuLabel(FudaaLib.getS("S�lectionner la fen�tre � activer")), BuBorderLayout.NORTH);
    if (!_autoswith) {
      setHelpText(FudaaLib.getS("Cliquer 2 fois sur la liste pour modifier le titre d'une fen�tre correspondante."));
    }
    add(new BuScrollPane(l_));
    l_.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    l_.getSelectionModel().addListSelectionListener(this);
    l_.requestFocus();
    final ActionMap map = l_.getActionMap();
    String key = "selectNextRow";
    Action a = map.get(key);

    if (a != null) {
      final Action up = new ActionDecorator(a) {
        @Override
        protected boolean doBefore() {
          int size = l_.getModel().getSize();
          if (size > 0 && l_.getSelectedIndex() == size - 1) {
            l_.setSelectedIndex(0);
            return false;
          }
          return true;
        }
      };
      map.put(key, up);
    }
    l_.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_MASK), key);
    key = "selectPreviousRow";
    a = map.get(key);
    if (a != null) {
      final Action down = new ActionDecorator(a) {
        @Override
        protected boolean doBefore() {
          int size = l_.getModel().getSize();
          if (size > 0 && l_.getSelectedIndex() == 0) {
            l_.setSelectedIndex(size - 1);
            return false;
          }
          return true;
        }
      };
      map.put(key, down);
      l_.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK), key);
    }
    if (_autoswith) {
      l_.setSelectedIndex((l_.getSelectedIndex() + 1) % frames.length);
    } else {
      l_.addMouseListener(new MouseAdapter() {

        @Override
        public void mouseClicked(MouseEvent _e) {
          if (!l_.isSelectionEmpty() && _e.getClickCount() == 2) {
            FudaaInternalFrameList.editSelected(l_);
          }
        }
      });
    }

  }

  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    if (_e.getValueIsAdjusting()) {
      return;
    }
    final int idx = l_.getSelectedIndex();
    if (idx >= 0 && idx < frames_.length) {
      desktop_.activateInternalFrame(frames_[idx]);
    }

  }
}
