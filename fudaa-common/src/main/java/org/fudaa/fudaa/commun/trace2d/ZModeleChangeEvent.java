package org.fudaa.fudaa.commun.trace2d;
import java.util.EventObject;
public class ZModeleChangeEvent extends EventObject {
  /** Values changed */
  public static final int VALUES_CHANGED= 0;
  public int type;
  public ZModeleChangeEvent(Object _source, int _type) {
    super(_source);
    type= _type;
  }
}
