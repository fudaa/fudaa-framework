/*
 * @creation 12 mars 07
 * @modification $Date: 2007-03-15 17:01:12 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

/**
 * @author fred deniger
 * @version $Id: FudaaDjaSquare.java,v 1.1 2007-03-15 17:01:12 deniger Exp $
 */
public class FudaaDjaSquare extends FudaaDjaBox {

  @Override
  public FudaaDjaFormAbstract createNew() {
    return new FudaaDjaSquare();
  }

  @Override
  public String getDjaName() {
    return "Square";
  }

  @Override
  public void setWidth(int _w) {
    super.setHeight(_w);
    super.setWidth(_w);
  }

  @Override
  public void setHeight(int _h) {
    super.setWidth(_h);
    super.setHeight(_h);
  }

}
