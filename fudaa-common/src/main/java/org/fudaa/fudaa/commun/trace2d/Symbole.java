package org.fudaa.fudaa.commun.trace2d;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Rectangle2D;
/**
 * Un symbole.
 * <p>
 * Un symbole poss�de une ancre (point du symbole en correspondance avec la
 * position de trac� du symbole) et une rotation en degr�s suivant Z.
 * <p>
 * <ul>
 *   <li>Le symbole est d�fini dans un rep�re xOy, X �tant orient�
 *       horizontalement vers la gauche, Y verticalement vers le bas, et limit�
 *       � l'intervalle [0,1] suivant X et Y.</li>
 *   <li>Les coordonn�es de l'ancre sont d�finies dans ce m�me rep�re limit�.</li>
 *   <li>La rotation Z est orient�e dans le sens trigonom�trique, une rotation
 *       d'angle 0 correspondant � un vecteur (1,0) dans le rep�re de d�finition
 *       du symbole.</li>
 * </ul>
 *
 * @version      $Revision: 1.4 $ $Date: 2006-09-19 15:10:23 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public final class Symbole {
  /** Symbole carr� */
  public static final int CARRE= 0;
  /** Symbole cercle */
  public static final int CERCLE= 1;
  /** Symbole losange */
  public static final int LOSANGE= 2;
  /** Symbole fl�che */
  public static final int FLECHE= 3;
  /** Symbole fl�che li�e */
  public static final int FLECHE_LIEE= 4;
  /** Symbole fl�che diffract�e */
  public static final int FLECHE_DIFFRACTEE= 5;
  /** Symbole triangle */
  public static final int TRIANGLE= 6;
  /** Symbole carr� plein */
  public static final int CARRE_PLEIN= 7;
  /** Symbole cercle plein */
  public static final int CERCLE_PLEIN= 8;
  /** Symbole losange plein */
  public static final int LOSANGE_PLEIN= 9;
  /** Symbole traingle plein */
  public static final int TRIANGLE_PLEIN= 10;
  /** Rotation suivant Z. 0 par d�faut. */
  public double rotationZ= 0;
  /** Trace du symbole. Carr� par d�faut */
  public Shape trace= null;
  /** Coordonn�e suivant X de l'ancre du symbole. */
  public double xAncre= 0;
  /** Coordonn�e suivant Y de l'ancre du symbole. */
  public double yAncre= 0;
  /** Symbole plein ou non */
  public boolean plein= false;
  /**
   * Cr�ation d'un symbole par d�faut
   */
  public Symbole() {
    this(CARRE, 0.);
  }
  /**
   * Cr�ation d'un symbole suivant un trace utilisateur
   */
  public Symbole(Shape _trace, double _xAncre, double _yAncre, double _rotZ) {
    init(_trace, _xAncre, _yAncre, _rotZ);
  }
  /**
   * Cr�ation d'un symbole suivant un type pr�d�fini.
   * @param _type     Type de symbole (CARRE, CERCLE, LOSANGE, etc.).
   * @param _rotZ     Rotation en degr�s du symbole par rapport � l'axe Z.
   */
  public Symbole(int _type, double _rotZ) {
    Shape traceLoc;
    double xAncreTmp;
    double yAncreTmp;
    if (_type == CARRE || _type == CARRE_PLEIN) {
      Rectangle2D r= new Rectangle2D.Double(0.146, 0.146, 0.707, 0.707);
      xAncreTmp= 0.5;
      yAncreTmp= 0.5;
      traceLoc= r;
      plein= _type == CARRE_PLEIN;
    } else if (_type == CERCLE || _type == CERCLE_PLEIN) {
      Ellipse2D e= new Ellipse2D.Double(0., 0., 1., 1.);
      xAncreTmp= 0.5;
      yAncreTmp= 0.5;
      traceLoc= e;
      plein= _type == CERCLE_PLEIN;
    } else if (_type == LOSANGE || _type == LOSANGE_PLEIN) {
      GeneralPath gp= new GeneralPath();
      gp.moveTo(0.5f, 0.0f);
      gp.lineTo(1.0f, 0.5f);
      gp.lineTo(0.5f, 1.0f);
      gp.lineTo(0.0f, 0.5f);
      gp.closePath();
      xAncreTmp= 0.5;
      yAncreTmp= 0.5;
      traceLoc= gp;
      plein= _type == LOSANGE_PLEIN;
    } else if (_type == FLECHE) {
      GeneralPath gp= new GeneralPath();
      gp.moveTo((float) (7.5 / 15.), (float) (2.0 / 15.));
      gp.lineTo((float) (7.5 / 15.), (float) (15.0 / 15.));
      gp.moveTo((float) (3.5 / 15.), (float) (11.0 / 15.));
      gp.lineTo((float) (7.5 / 15.), (float) (15.0 / 15.));
      gp.lineTo((float) (11.5 / 15.), (float) (11.0 / 15.));
      xAncreTmp= 7.5 / 15.;
      yAncreTmp= 2. / 15.;
      traceLoc= gp;
    } else if (_type == FLECHE_LIEE) {
      GeneralPath gp= new GeneralPath();
      gp.moveTo((float) (5.5 / 15.), (float) (0.0 / 15.));
      gp.lineTo((float) (9.5 / 15.), (float) (0.0 / 15.));
      gp.lineTo((float) (9.5 / 15.), (float) (4.0 / 15.));
      gp.lineTo((float) (5.5 / 15.), (float) (4.0 / 15.));
      gp.closePath();
      gp.moveTo((float) (7.5 / 15.), (float) (4.0 / 15.));
      gp.lineTo((float) (7.5 / 15.), (float) (15.0 / 15.));
      gp.moveTo((float) (3.5 / 15.), (float) (11.0 / 15.));
      gp.lineTo((float) (7.5 / 15.), (float) (15.0 / 15.));
      gp.lineTo((float) (11.5 / 15.), (float) (11.0 / 15.));
      xAncreTmp= 7.5 / 15.;
      yAncreTmp= 2. / 15.;
      traceLoc= gp;
    } else if (_type == FLECHE_DIFFRACTEE) {
      GeneralPath gp= new GeneralPath();
      gp.moveTo((float) (7.5 / 15.), (float) (2.0 / 15.));
      gp.lineTo((float) (7.5 / 15.), (float) (15.0 / 15.));
      gp.moveTo((float) (3.5 / 15.), (float) (11.0 / 15.));
      gp.lineTo((float) (7.5 / 15.), (float) (15.0 / 15.));
      gp.lineTo((float) (11.5 / 15.), (float) (11.0 / 15.));
      gp.moveTo((float) (3.5 / 15.), (float) (5.0 / 15.));
      gp.lineTo((float) (5.5 / 15.), (float) (7.0 / 15.));
      gp.lineTo((float) (9.5 / 15.), (float) (7.0 / 15.));
      gp.lineTo((float) (11.5 / 15.), (float) (5.0 / 15.));
      gp.moveTo((float) (5.5 / 15.), (float) (4.0 / 15.));
      gp.lineTo((float) (6.5 / 15.), (float) (5.0 / 15.));
      gp.lineTo((float) (8.5 / 15.), (float) (5.0 / 15.));
      gp.lineTo((float) (9.5 / 15.), (float) (4.0 / 15.));
      xAncreTmp= 7.5 / 15.;
      yAncreTmp= 2. / 15.;
      traceLoc= gp;
    } else {
      GeneralPath gp= new GeneralPath();
      gp.moveTo(0.0667f, 0.25f);
      gp.lineTo(0.933f, 0.25f);
      gp.lineTo(0.5f, 1.f);
      gp.closePath();
      xAncreTmp= 0.5;
      yAncreTmp= 0.5;
      traceLoc= gp;
      plein= _type == TRIANGLE_PLEIN;
    }
    init(traceLoc, xAncreTmp, yAncreTmp, _rotZ);
  }
  private void init(
    Shape _trace,
    double _xAncre,
    double _yAncre,
    double _rotZ) {
    trace= _trace;
    xAncre= _xAncre;
    yAncre= _yAncre;
    rotationZ= _rotZ;
  }
}
