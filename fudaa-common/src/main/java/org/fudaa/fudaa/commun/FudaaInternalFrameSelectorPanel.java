/*
 * @creation 31 janv. 07
 * @modification $Date: 2007-05-04 13:58:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;
import javax.swing.ButtonGroup;
import javax.swing.JInternalFrame;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;

/**
 * @author fred deniger
 * @version $Id: FudaaInternalFrameSelectorPanel.java,v 1.3 2007-05-04 13:58:08 deniger Exp $
 */
public class FudaaInternalFrameSelectorPanel extends CtuluDialogPanel {
  final BuRadioButton cbNew_;
  final BuComboBox l_;
  final BuTextField tfTitle_;

  public FudaaInternalFrameSelectorPanel(final List _qInternal, final String _newFrame, final String _newFrameTitle) {
    super(false);
    final CtuluCellTextRenderer renderer = new CtuluCellTextRenderer() {
      @Override
      protected void setValue(final Object _value) {

        setText(_value == null ? CtuluLibString.EMPTY_STRING : ((JInternalFrame) _value).getTitle());
      }
    };
    setLayout(new BuVerticalLayout(4, true, false));
    l_ = new BuComboBox(_qInternal.toArray());
    l_.setRenderer(renderer);
    cbNew_ = new BuRadioButton(_newFrame);
    final BuRadioButton cbOld = new BuRadioButton(FudaaLib.getS("Afficher les courbes dans la fen�tre:"));

    final ButtonGroup bg = new ButtonGroup();
    bg.add(cbNew_);
    bg.add(cbOld);
    final boolean b = _qInternal.size() > 0;
    cbOld.setEnabled(b);
    if (b) {
      l_.setSelectedIndex(0);
    }

    final BuPanel pnOld = new BuPanel(new BuHorizontalLayout(2, true, false));

    pnOld.add(cbOld);
    pnOld.add(l_);

    final BuPanel pnNew = new BuPanel(new BuHorizontalLayout(2, true, false));
    pnNew.add(cbNew_);
    tfTitle_ = new BuTextField(10);
    tfTitle_.setText(_newFrame == null ? FudaaLib.getS("Fen�tre") : _newFrameTitle);

    pnNew.add(cbNew_);
    pnNew.add(tfTitle_);

    add(pnOld);
    add(pnNew);
    cbNew_.getModel().addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(final ItemEvent _e) {
        updateCbState();

      }
    });
    if (b) {
      cbOld.setSelected(true);
    } else {
      cbNew_.setSelected(true);
    }
    updateCbState();
  }

  public JInternalFrame getSelectedFrame() {
    if (cbNew_.isSelected()) {
      return null;
    }
    return (JInternalFrame) l_.getModel().getSelectedItem();
  }

  public String getNewFrameTitle() {
    return tfTitle_.getText();
  }

  public BuRadioButton getCbNew() {
    return cbNew_;
  }

  final void updateCbState() {
    l_.setEnabled(!cbNew_.isSelected());
    tfTitle_.setEnabled(cbNew_.isSelected());
  }
}
