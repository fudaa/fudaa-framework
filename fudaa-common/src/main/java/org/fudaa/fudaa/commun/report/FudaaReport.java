/*
 * @creation 5 f�vr. 07
 * @modification $Date: 2007-02-07 09:56:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.bu.BuCommonImplementation;

/**
 * @author fred deniger
 * @version $Id: FudaaReport.java,v 1.1 2007-02-07 09:56:19 deniger Exp $
 */
public class FudaaReport {

  final BuCommonImplementation impl_;

  public FudaaReport(final BuCommonImplementation _impl) {
    super();
    impl_ = _impl;
    new FudaaReportFrameController(_impl);

  }
}
