/*
 * @file         FudaaPanneauNavigation.java
 * @creation     1999-04-27
 * @modification $Date: 2007-01-19 13:14:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.impl;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import java.awt.BorderLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import javax.swing.border.EmptyBorder;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @version      $Id: FudaaPanelNavigation.java,v 1.1 2007-01-19 13:14:06 deniger Exp $
 * @author       Axel von Arnim
 */
public class FudaaPanelNavigation extends BuPanel {
  public final static int INSETS_SIZE= 5;
  public final static int AUCUN= 0x00;
  public final static int VALIDER= 0x01;
  public final static int FERMER= 0x02;
  public final static int PRECEDENT= 0x04;
  public final static int SUIVANT= 0x08;
  public final static int TERMINER= 0x10;
  public final static int ANNULER= 0x20;
  public final static int ALIGN_LEFT= 0x40;
  public final static int ALIGN_RIGHT= 0x80;
  public final static int ALIGN_CENTER= 0x100;
  BuButton btNavValider_;
  BuButton btNavFermer_;
  BuButton btNavPrecedent_;
  BuButton btNavSuivant_;
  BuButton btNavTerminer_;
  BuPanel pnNav_;
  public FudaaPanelNavigation() {
    this(0);
  }
  public FudaaPanelNavigation(final int _mode) {
    btNavValider_= null;
    btNavFermer_= null;
    btNavPrecedent_= null;
    btNavSuivant_= null;
    btNavTerminer_= null;
    pnNav_= new BuPanel();
    setLayout(new BorderLayout());
    setBorder(new EmptyBorder(new Insets(0, 0, INSETS_SIZE, 0)));
    if ((_mode & VALIDER) != 0) {
      btNavValider_= new BuButton(BuResource.BU.getString("Valider"));
      btNavValider_.setName("btNAVVALIDER");
      btNavValider_.setActionCommand("VALIDER");
      pnNav_.add(btNavValider_);
    }
    if ((_mode & FERMER) != 0) {
      btNavFermer_= new BuButton(BuResource.BU.getString("Fermer"));
      btNavFermer_.setName("btNAVFERMER");
      btNavFermer_.setActionCommand("FERMER");
      pnNav_.add(btNavFermer_);
    }
    if ((_mode & PRECEDENT) != 0) {
      btNavPrecedent_= new BuButton(FudaaLib.getS("Précédent"));
      btNavPrecedent_.setName("btNAVPRECEDENT");
      btNavPrecedent_.setActionCommand("PRECEDENT");
      pnNav_.add(btNavPrecedent_);
    }
    if ((_mode & SUIVANT) != 0) {
      btNavSuivant_= new BuButton(FudaaLib.getS("Suivant"));
      btNavSuivant_.setName("btNAVSUIVANT");
      btNavSuivant_.setActionCommand("SUIVANT");
      pnNav_.add(btNavSuivant_);
    }
    if ((_mode & TERMINER) != 0) {
      btNavTerminer_= new BuButton(FudaaLib.getS("Terminer"));
      btNavTerminer_.setName("btNAVTERMINER");
      btNavTerminer_.setActionCommand("TERMINER");
      pnNav_.add(btNavTerminer_);
    }
    // Layout
    if ((_mode & ALIGN_LEFT) != 0) {
      add(BorderLayout.WEST, pnNav_);
    } else if ((_mode & ALIGN_RIGHT) != 0) {
      add(BorderLayout.EAST, pnNav_);
    } else {
      add(BorderLayout.CENTER, pnNav_);
    }
  }
  @Override
  public void setName(final String _n) {
    if (btNavValider_ != null) {
      btNavValider_.setName(_n + ":VALIDER");
    }
    if (btNavFermer_ != null) {
      btNavFermer_.setName(_n + ":FERMER");
    }
    if (btNavPrecedent_ != null) {
      btNavPrecedent_.setName(_n + ":PRECEDENT");
    }
    if (btNavSuivant_ != null) {
      btNavSuivant_.setName(_n + ":SUIVANT");
    }
    if (btNavTerminer_ != null) {
      btNavTerminer_.setName(_n + ":TERMINER");
    }
    super.setName(_n);
  }
  public void addActionListener(final ActionListener _l) {
    if (btNavValider_ != null) {
      btNavValider_.addActionListener(_l);
    }
    if (btNavFermer_ != null) {
      btNavFermer_.addActionListener(_l);
    }
    if (btNavPrecedent_ != null) {
      btNavPrecedent_.addActionListener(_l);
    }
    if (btNavSuivant_ != null) {
      btNavSuivant_.addActionListener(_l);
    }
    if (btNavTerminer_ != null) {
      btNavTerminer_.addActionListener(_l);
    }
  }
  public void removeActionListener(final ActionListener _l) {
    if (btNavValider_ != null) {
      btNavValider_.removeActionListener(_l);
    }
    if (btNavFermer_ != null) {
      btNavFermer_.removeActionListener(_l);
    }
    if (btNavPrecedent_ != null) {
      btNavPrecedent_.removeActionListener(_l);
    }
    if (btNavSuivant_ != null) {
      btNavSuivant_.removeActionListener(_l);
    }
    if (btNavTerminer_ != null) {
      btNavTerminer_.removeActionListener(_l);
    }
  }
}
