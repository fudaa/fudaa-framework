/*
 *  @creation     10 janv. 2006
 *  @modification $Date: 2006-01-23 13:31:39 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.aide;

import java.net.URL;


/**
 * @author Fred Deniger
 * @version $Id: FudaaHelpParentI.java,v 1.1 2006-01-23 13:31:39 deniger Exp $
 */
public interface FudaaHelpParentI {
  void setMessage(String _message);
  void setDocumentUrl(URL _url,boolean _forceReload);
  void setTitle(String _title);
  void setUrlText(String _title,boolean _b);
  void setBackEnabled(boolean _b);
  void setForwardEnabled(boolean _b);
}
