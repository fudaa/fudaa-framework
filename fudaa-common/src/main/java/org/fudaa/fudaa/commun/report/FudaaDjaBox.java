/*
 * @creation 8 mars 07
 * @modification $Date: 2007-03-23 17:25:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.dja.DjaGridEvent;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BSelecteurColorChooserBt;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.controle.BSelecteurLineModel;
import org.fudaa.ebli.controle.BSelecteurSpinner;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author fred deniger
 * @version $Id: FudaaDjaBox.java,v 1.4 2007-03-23 17:25:30 deniger Exp $
 */
public class FudaaDjaBox extends FudaaDjaFormAbstract {
  static int idx_;
  int roundBox_;
  public static final String ROUND_PROPERTY = "dja.round";

  public FudaaDjaBox() {
    title_ = EbliLib.getS("Texte {0}", CtuluLibString.getString(++idx_));
  }

  @Override
  public FudaaDjaFormAbstract createNew() {
    return new FudaaDjaBox();
  }

  @Override
  public String getDjaName() {
    return "Box";
  }

  @Override
  public BSelecteurTargetInterface getVisibleTitleTarget() {
    return new FudaaReportSelecteurTitleTarget(this);
  }

  public boolean setRound(int _r) {
    if (_r != roundBox_) {
      roundBox_ = _r;
      fireGridEvent(this, DjaGridEvent.MODIFIED);
      return true;
    }
    return false;
  }

  @Override
  protected BConfigurableComposite createAffichageConfigure() {
    return new BConfigurableComposite(new FudaaDjaConfigurePosition(this, (FudaaDjaGrid) getGrid()),
        new RoundConfigure(this), new FudaaDjaFormTransform(this), FudaaLib.getS("Affichage"));
  }
  protected static class RoundConfigure extends FudaaDjaFormConfigure {

    public RoundConfigure(FudaaDjaBox _target) {
      super(_target);
    }

    @Override
    public BSelecteurInterface[] createSelecteurs() {
      BSelecteurColorChooserBt foreground = new BSelecteurColorChooserBt(true);
      BSelecteurColorChooserBt background = new BSelecteurColorChooserBt(true, PROPERTY_BACKGROUND);
      BSelecteurLineModel line = new BSelecteurLineModel();
      line.setTitle(EbliLib.getS("Trac� du bord"));
      background.setTitle(EbliLib.getS("Couleur de fond"));
      BSelecteurSpinner selecteurSpinner = new BSelecteurSpinner(ROUND_PROPERTY);
      selecteurSpinner.setTitle(FudaaLib.getS("Rayon"));
      return new BSelecteurInterface[] { foreground, background, line, selecteurSpinner };
    }

    @Override
    public Object getProperty(String _key) {
      if (_key == ROUND_PROPERTY) {
        return new Integer(((FudaaDjaBox) target_).getRound());
      }
      return super.getProperty(_key);
    }

    @Override
    public boolean setProperty(String _key, Object _newProp) {
      if (_key == ROUND_PROPERTY) {
        FudaaDjaBox box = (FudaaDjaBox) target_;
        boolean r = box.setRound(((Integer) _newProp).intValue());
        if (r) getDja().getGrid().repaint();
        return r;
      }
      return super.setProperty(_key, _newProp);
    }
  }

  @Override
  public String getDescription() {
    return FudaaLib.getS("Rectangle");
  }

  @Override
  public void paintObject(Graphics _g) {
    int x = getX();
    int y = getY();
    int w = getWidth();
    int h = getHeight();
    CtuluLibImage.setBestQuality((Graphics2D) _g);

    Color bg = getBackground();

    if (bg != null) {
      _g.setColor(bg);
      if (roundBox_ <= 0) _g.fillRect(x, y, w, h);
      else {
        _g.fillRoundRect(x, y, w, h, roundBox_, roundBox_);
      }
    }
    if (ligne_.getCouleur() != null) {
      if (roundBox_ <= 0) ligne_.dessineRectangle((Graphics2D) _g, x, y, w - 1, h - 1);
      else {
        ligne_.dessineRoundRectangle((Graphics2D) _g, x, y, w - 1, h - 1, roundBox_);
      }
    }

    super.paintObject(_g);
  }

  public int getRound() {
    return roundBox_;
  }
}
