/*
 * @creation     18 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.exetools;

import com.memoire.bu.BuVerticalLayout;
import java.util.Arrays;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import org.fudaa.ctulu.gui.CtuluCellDialogEditor;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaPreferences;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;

/**
 * Un panneau pour g�rer les external tools.
 * 
 * @author marchand@deltacad.fr
 * @version $Id$
 */
public class FudaaManageExeToolsPanel extends CtuluDialogPanel {

  private FudaaCommonImplementation impl_;
  private FudaaExeTool[] tools_;
  private CtuluListEditorModel mdTools_;
  private FudaaManageExeTools mng_;

  public FudaaManageExeToolsPanel(FudaaCommonImplementation _impl, FudaaManageExeTools _mng) {
    impl_=_impl;
    mng_=_mng;
    tools_=mng_.getFromPrefs(FudaaPreferences.FUDAA);

    setLayout(new BuVerticalLayout(5, true, true));
    JTable tbTools=new JTable();
    JScrollPane spTools=new JScrollPane();
    spTools.getViewport().add(tbTools);
    
    add(new JLabel(FudaaLib.getS("Liste des utilitaires")));
    mdTools_=new CtuluListEditorModel(Arrays.asList(tools_), true) {
      @Override
      public void edit(final int _r) {
        FudaaEditExeToolPanel pn=new FudaaEditExeToolPanel(impl_,mng_);
        pn.setTool((FudaaExeTool)mdTools_.getValueAt(_r));
        if (pn.afficheModaleOk(impl_.getParentComponent(),FudaaLib.getS("Edition de l'utilitaire"))) {
          mdTools_.setValueAt(pn.getTool(), _r, 1);
        }
      }

      @Override
      public boolean isCellEditable(int _rowIndex, int _columnIndex) {
        return false;
      }
      
      @Override
      public Object createNewObject() {
        return new FudaaExeTool();
      }
      
      @Override
      public Object copyObject(int _r) {
        FudaaExeTool ret=((FudaaExeTool)mdTools_.getValueAt(_r)).clone();
        ret.name+=" ("+FudaaLib.getS("Copie")+")";
        return ret;
      }
    };
    CtuluListEditorPanel pnTools=new CtuluListEditorPanel(mdTools_, true, true, false, true, false, true) {

      @Override
      public void actionAdd() {
        FudaaEditExeToolPanel pn=new FudaaEditExeToolPanel(impl_,mng_);
        pn.setTool(new FudaaExeTool());
        if (pn.afficheModaleOk(impl_.getParentComponent(),FudaaLib.getS("Nouvel utilitaire"))) {
          final int n = table_.getRowCount();
          mdTools_.addElement(pn.getTool());
          table_.getSelectionModel().setSelectionInterval(n, n);
        }
      }
    };
    CtuluDialog di=new CtuluDialog(new FudaaEditExeToolPanel(impl_,mng_));
    pnTools.setValueListCellEditor(new CtuluCellDialogEditor(di));
    pnTools.setValueListCellRenderer(new CtuluCellTextRenderer() {
      @Override
      public void setValue(Object _exeTool) {
        FudaaExeTool tool=(FudaaExeTool)_exeTool;
        setText(tool==null ? "":tool.name);
      }
    });
    add(pnTools);
  }

  @Override
  public boolean isDataValid() {
    return true;
  }

  /**
   * Retourne la nouvelle liste d' utilitaires
   * @return Les utilitaires
   */
  public FudaaExeTool[] getTools() {
    FudaaExeTool[] tools=new FudaaExeTool[mdTools_.getRowCount()];
    mdTools_.getValues(tools);
    return tools;
  }
}
