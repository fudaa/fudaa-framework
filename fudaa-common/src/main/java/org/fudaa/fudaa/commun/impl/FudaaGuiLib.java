/*
 * @creation 19 janv. 07
 * 
 * @modification $Date: 2007-02-02 11:22:25 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuFileFilterAll;
import com.memoire.bu.BuResource;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractButton;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import org.fudaa.ctulu.CtuluExportDataInterface;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.ctulu.gui.CtuluFileChooserFileTester;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaPreferences;

/**
 * @author fred deniger
 * @version $Id: FudaaGuiLib.java,v 1.2 2007-02-02 11:22:25 deniger Exp $
 */
public final class FudaaGuiLib {
  
  private FudaaGuiLib() {
  }

  /**
   * @param _parent le composant parent
   * @param _saveDialog si true, filechooser ouvert dans le mode saveDialog
   * @return le fichier choisi ou nul si aucun, _filtres[0] est le filtre s�lectionn�.
   */
  public static File chooseFile(final Component _parent, final boolean _saveDialog, final CtuluFileChooser _fch) {
    final int returnVal = _saveDialog ? _fch.showSaveDialog(_parent) : _fch.showOpenDialog(_parent);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      return _fch.getSelectedFile();
    }
    return null;
    
  }

  /**
   * @param _titre le titre du file chooser
   * @param _filtres le tableau de filtres voulu
   * @param _tester le testeur de fichier
   * @return le fichier choisi ou nul si aucun, _filtres[0] est le filtre s�lectionn�.
   */
  public static CtuluFileChooser getFileChooser(final String _titre, final FileFilter[] _filtres, final CtuluFileChooserFileTester _tester) {
    final CtuluFileChooser fileChooser = new CtuluFileChooser(true);
    fileChooser.setTester(_tester);
    fileChooser.setFileHidingEnabled(true);
    fileChooser.setMultiSelectionEnabled(false);
    fileChooser.setDialogTitle(_titre);
    if (_filtres != null) {
      for (int i = 0; i < _filtres.length; i++) {
        fileChooser.addChoosableFileFilter(_filtres[i]);
      }
      if (_filtres.length == 1) {
        fileChooser.setFileFilter(_filtres[0]);
      }
    }
    return fileChooser;
  }

  /**
   * @param _titre le titre du file chooser
   * @param _filtre le filtre voulu
   * @param _parent le composant parent
   * @param _saveDialog si true, filechooser ouvert dans le mode saveDialog
   * @return le fichier choisi ou nul si aucun
   */
  public static File ouvrirFileChooser(final String _titre, final FileFilter _filtre, final Component _parent, final boolean _saveDialog) {
    return ouvrirFileChooser(_titre, _filtre, _parent, _saveDialog, null);
  }

  /**
   * @param _titre le titre du file chooser
   * @param _filtre le filtre voulu
   * @param _parent le composant parent
   * @param _saveDialog si true, filechooser ouvert dans le mode saveDialog
   * @param _tester le testeur de fichier selectionne
   * @return le fichier choisi ou nul si aucun
   */
  public static File ouvrirFileChooser(final String _titre, final FileFilter _filtre, final Component _parent, final boolean _saveDialog,
          final CtuluFileChooserFileTester _tester) {
    FileFilter[] filtres = null;
    if (_filtre != null) {
      filtres = new FileFilter[1];
      filtres[0] = _filtre;
    }
    return ouvrirFileChooser(_titre, filtres, _parent, _saveDialog, _tester);
  }

  /**
   * @param _titre le titre
   * @param _filtres les filtres
   * @param _parent la parent
   * @param _saveDialog true si save dialog
   * @return le fichier selectionne
   */
  public static File ouvrirFileChooser(final String _titre, final FileFilter[] _filtres, final Component _parent, final boolean _saveDialog,
          String preferenceName) {
    return ouvrirFileChooser(_titre, _filtres, _parent, _saveDialog, null, preferenceName);
  }
  
  public static File ouvrirFileChooser(final String _titre, final FileFilter[] _filtres, final Component _parent, final boolean _saveDialog) {
    return ouvrirFileChooser(_titre, _filtres, _parent, _saveDialog, null, null);
  }

  /**
   * @param _titre le titre du file chooser
   * @param _filtres le tableau de filtres voulu
   * @param _parent le composant parent
   * @param _saveDialog si true, filechooser ouvert dans le mode saveDialog
   * @param _tester le testeur de fichier
   * @return le fichier choisi ou nul si aucun, _filtres[0] est le filtre s�lectionn�.
   */
  public static File ouvrirFileChooser(final String _titre, final FileFilter[] _filtres, final Component _parent, final boolean _saveDialog,
          final CtuluFileChooserFileTester _tester) {
    return ouvrirFileChooser(_titre, _filtres, _parent, _saveDialog, _tester, null);
  }
  
  public static File ouvrirFileChooser(final String _titre, final FileFilter[] _filtres, final Component _parent, final boolean _saveDialog,
          final CtuluFileChooserFileTester _tester, String preferenceName) {
    final CtuluFileChooser fileChooser = getFileChooser(_titre, _filtres, _tester);
    String preferenceKey = preferenceName + ".lastFilterChosen";
    if (preferenceName != null) {
      String stringProperty = FudaaPreferences.FUDAA.getStringProperty(preferenceKey, null);
      if (stringProperty != null) {
        FileFilter selected = findFilter(_filtres, stringProperty);
        if (selected != null) {
          fileChooser.setFileFilter(selected);
        }
      }
    }
    final File f = chooseFile(_parent, _saveDialog, fileChooser);
    if (_filtres != null) {
      if (_filtres.length > 0) {
        _filtres[0] = fileChooser.getFileFilter();
        if (preferenceName != null) {
          FudaaPreferences.FUDAA.putStringProperty(preferenceKey, fileChooser.getFileFilter().getDescription());
          FudaaPreferences.FUDAA.writeIniFile();
        }
        
      }
    }
    return f;
  }
  
  private static FileFilter findFilter(FileFilter[] _filtres, String stringProperty) {
    if (_filtres == null || stringProperty == null) {
      return null;
    }
    for (int i = 0; i < _filtres.length; i++) {
      FileFilter fileFilter = _filtres[i];
      if (fileFilter.getDescription().equals(stringProperty)) {
        return fileFilter;
      }
    }
    return null;
  }

  /**
   * Affiche un FileChooser.
   *
   * @param _desc la description du type de fichier
   * @param _extension les extensions voulues
   * @param _parent le composant parent
   * @return le fichier choisi ou nul si aucun
   */
  public static File ouvrirFileChooser(final String _desc, final String[] _extension, final Component _parent) {
    return ouvrirFileChooser(_desc, _extension, _parent, false, null);
  }

  /**
   * Affiche un FileChooser.
   *
   * @param _desc la description du type de fichier
   * @param _extension les extensions voulues
   * @param _parent le composant parent
   * @param _saveDialog si true, le filechooser est ouvert dans le mode "sauvegarde"
   * @param _tester le testeur de fichiers
   * @return le fichier choisi ou nul si aucun
   */
  public static File ouvrirFileChooser(final String _desc, final String[] _extension, final Component _parent, final boolean _saveDialog,
          final CtuluFileChooserFileTester _tester) {
    BuFileFilter flt = null;
    final String titre = _desc;
    if (_extension != null) {
      flt = new BuFileFilter(_extension, _desc);
    }
    return ouvrirFileChooser(titre, flt, _parent, _saveDialog, _tester);
  }
  
  public static void initExportDataButton(final AbstractButton _bt, final ActionListener _l) {
    _bt.setIcon(BuResource.BU.getIcon("exporter"));
    _bt.setText(FudaaLib.getS("Exporter donn�es"));
    _bt.setActionCommand(CtuluExportDataInterface.EXPORT_CMD);
    _bt.setToolTipText(FudaaLib.getS("Exporter les donn�es de la fen�tre active"));
    _bt.setEnabled(false);
    _bt.addActionListener(_l);
  }

  /**
   * @return EXPORTER
   */
  public static String getExporterName() {
    return "EXPORTER";
  }

  /**
   * @return ENREGISTRERCOPIE
   */
  public static String getSaveCopy() {
    return "ENREGISTRERCOPIE";
  }
  
  public static String getArchiver() {
    return "ARCHIVER";
  }
  
  public static FudaaLib.OpenResult openFile(final BuCommonImplementation _parent, final FileFormat[] _gridFmt, final String _title,
          final FileFormat _default) {
    BuFileFilter selected = null;
    final BuFileFilter[] filters = new BuFileFilter[_gridFmt.length + 1];
    final Map m = new HashMap(_gridFmt.length);
    for (int i = 0; i < _gridFmt.length; i++) {
      filters[i] = _gridFmt[i].createFileFilter();
      m.put(filters[i], _gridFmt[i]);
      if (_default == _gridFmt[i]) {
        selected = filters[i];
      }
    }
    filters[filters.length - 1] = new BuFileFilterAll();
    final CtuluFileChooser fileChooser = getFileChooser(_title, filters, null);
    if (selected != null) {
      fileChooser.setFileFilter(selected);
    }
    final File f = chooseFile(_parent.getFrame(), false, fileChooser);
    if (f == null) {
      return null;
    }
    final FudaaLib.OpenResult res = new FudaaLib.OpenResult();
    res.file_ = f;
    res.ft_ = (FileFormat) m.get(fileChooser.getFileFilter());
    if (res.ft_ == null) {
      res.ft_ = FudaaLib.chooseFileFormat(_parent.getFrame(), _gridFmt, _title);
    }
    if (res.ft_ == null) {
      return null;
    }
    return res;
    
  }
}
