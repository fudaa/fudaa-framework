/*
 * @creation 12 nov. 2003
 * @modification $Date: 2006-09-22 15:46:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.commun.undo;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuUndoRedoInterface;
import java.awt.Component;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluUndoRedoInterface;
/**
 * @author deniger
 * @version $Id: FudaaUndoPaneFille.java,v 1.17 2006-09-22 15:46:05 deniger Exp $
 */
public class FudaaUndoPaneFille extends BuInternalFrame implements ChangeListener,
    BuUndoRedoInterface, CtuluUndoRedoInterface {

  final private JTabbedPane pane_;
  FudaaUndoCmdMngListener l_;

  protected JComponent getPaneComponent(final String _name){
    return (JComponent) BuLib.findNamedComponent(pane_, _name);

  }

  /**
   *
   */
  public FudaaUndoPaneFille(final FudaaUndoCmdMngListener _l) {
    pane_ = new BuTabbedPane();
    final JPanel p = new JPanel();
    p.setOpaque(true);
    p.setLayout(new BuBorderLayout());
    p.add(pane_, BuBorderLayout.CENTER);
    setContentPane(p);
    l_ = _l;
    pane_.getModel().addChangeListener(this);
  }

  /**
   * @param _title
   * @param _resizable
   * @param _closable
   * @param _maximizable
   * @param _iconifiable
   */
  public FudaaUndoPaneFille(final String _title, final boolean _resizable, final boolean _closable,
      final boolean _maximizable, final boolean _iconifiable, final FudaaUndoCmdMngListener _l) {
    super(_title, _resizable, _closable, _maximizable, _iconifiable);
    pane_ = new BuTabbedPane();
    setContentPane(pane_);
    l_ = _l;
    pane_.getModel().addChangeListener(this);
  }

  /**
   * Si c est une instance de CtuluUndoCmdMngContainer, le listener est mis � jour.
   * @param _title le titre
   * @param icon l'icone
   * @param c le composant
   * @param tooltip le tooltip
   */
  public final void addTab(final String _title,final Icon _icon,final CtuluUndoRedoInterface _c,final String _tooltip){
    pane_.addTab(_title, _icon, (Component) _c, _tooltip);
    if (_c.getCmdMng() != null) {
      _c.getCmdMng().setListener(l_);
    }
  }

  public Component getSelectedComponent(){
    return pane_ == null ? null : pane_.getSelectedComponent();
  }

  public CtuluCommandManager getActiveCmdMng(){
    final CtuluUndoRedoInterface cpt = (CtuluUndoRedoInterface) pane_.getSelectedComponent();
    return cpt.getCmdMng();
  }

  @Override
  public void stateChanged(final ChangeEvent _e){
    final CtuluUndoRedoInterface selected = (CtuluUndoRedoInterface) pane_.getSelectedComponent();
    for (int j = pane_.getTabCount() - 1; j >= 0; j--) {
      if (pane_.getComponentAt(j) != selected) {
        ((CtuluUndoRedoInterface) pane_.getComponentAt(j)).setActive(false);
      }
    }
    l_.setActive(selected, true);
  }

  @Override
  public void redo(){
    final CtuluCommandManager mng = getActiveCmdMng();
    if (mng != null) {
      mng.redo();
    }
  }

  /**
   *
   */
  @Override
  public void undo(){
    final CtuluCommandManager mng = getActiveCmdMng();
    if (mng != null) {
      mng.undo();
    }
  }

  /**
   * @see org.fudaa.ctulu.CtuluUndoRedoInterface#getCmdMng()
   */
  @Override
  public CtuluCommandManager getCmdMng(){
    return getActiveCmdMng();
  }

  /**
   * @see org.fudaa.ctulu.CtuluUndoRedoInterface#setActive(boolean)
   */
  @Override
  public void setActive(final boolean _b){
    ((CtuluUndoRedoInterface) pane_.getSelectedComponent()).setActive(_b);
  }

  @Override
  public void clearCmd(final CtuluCommandManager _source){
    for (int j = pane_.getTabCount()- 1; j >= 0; j--) {
      if (pane_.getComponentAt(j) != this) {
        ((CtuluUndoRedoInterface) pane_.getComponentAt(j)).clearCmd(_source);
      }
    }

  }

}
