/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.commun.exetools;

import com.memoire.fu.FuPreferences;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.fudaa.fudaa.commun.exetools.FudaaExeTool.ParamConst;
import org.fudaa.fudaa.commun.exetools.FudaaExeTool.ParamI;
import org.fudaa.fudaa.commun.exetools.FudaaExeTool.ParamVar;

/**
 * Un manager pour tous les exe tools
 * @author marchand@deltacad.fr
 */
public class FudaaManageExeTools {
  /** Les différents types de parametres */
  protected List<ParamI> paramTypes=new ArrayList<ParamI>();
  
  public FudaaManageExeTools() {
    initParamTypes();
  }
  
  public void initParamTypes() {
    paramTypes.add(new ParamConst());
    paramTypes.add(new ParamVar());
  }
  
  /**
   * Renvoi les utilitaires depuis les préférences
   * @param _prefs Les préférences
   * @return Les utilitaires
   */
  public FudaaExeTool[] getFromPrefs(FuPreferences _prefs) {
    List<FudaaExeTool> tools=new ArrayList<FudaaExeTool>();
    
    int i=1;
    while (true) {
      FudaaExeTool tool=new FudaaExeTool();
      tool.name=_prefs.getStringProperty("externaltool." + i + ".name",null);
      if (tool.name==null) break;
      
      tool.exePath=new File(_prefs.getStringProperty("externaltool." + i + ".exe",null));
      tool.exeDirPath=new File(_prefs.getStringProperty("externaltool." + i + ".dir",null));
      
      List<ParamI> params=new ArrayList<ParamI>();
      int j=1;
      while (true) {
        ParamI param=null;
        String type=_prefs.getStringProperty("externaltool."+i+".param."+j+".type",null);
        if (type!=null) {
          param=createFromType(type);
          if (param==null) break;
        }
        else break;
        
        param.setValue(_prefs.getStringProperty("externaltool."+i+".param."+j+".value",null));
        param.name=_prefs.getStringProperty("externaltool."+i+".param."+j+".name","");
        params.add(param);
        j++;
      }
      tool.setParams(params.toArray(new ParamI[0]));
      tools.add(tool);
      i++;
    }
    return tools.toArray(new FudaaExeTool[0]);
  }
  
  /**
   * Remplit les préférences depuis les utilitaires, après avoir supprimé les
   * précédentes préférences.
   * 
   * @param _tools Les utilitaires
   * @param _prefs Les préférences.
   */
  public void putInPrefs(FudaaExeTool[] _tools, FuPreferences _prefs) {
    // Suppression des anciennes préférences.
    int i=1;
    while (_prefs.getStringProperty("externaltool." + i + ".name",null)!=null) {
      _prefs.removeProperty("externaltool." + i + ".name");
      _prefs.removeProperty("externaltool." + i + ".exe");
      _prefs.removeProperty("externaltool." + i + ".dir");
      
      int j=1;
      while (_prefs.getStringProperty("externaltool."+i+".param."+j+".type",null)!=null) {
        _prefs.removeProperty("externaltool."+i+".param."+j+".name");
        _prefs.removeProperty("externaltool."+i+".param."+j+".type");
        _prefs.removeProperty("externaltool."+i+".param."+j+".value");
        j++;
      }
      i++;
    }
    
    // Ajout des nouvelles prefs
    
    i=1;
    for (FudaaExeTool tool : _tools) {
      _prefs.putStringProperty("externaltool." + i + ".name",tool.name);
      _prefs.putStringProperty("externaltool." + i + ".exe",tool.exePath.getPath());
      _prefs.putStringProperty("externaltool." + i + ".dir",tool.exeDirPath.getPath());
      
      int j=1;
      for (ParamI param : tool.getParams()) {
        _prefs.putStringProperty("externaltool."+i+".param."+j+".name",param.name);
        _prefs.putStringProperty("externaltool."+i+".param."+j+".type",param.getType());
        _prefs.putStringProperty("externaltool."+i+".param."+j+".value",param.getValue());
        j++;
      }
      i++;
    }
  }
  
  public ParamI createFromType(String _type) {
    try {
      for (ParamI ptype : paramTypes) {
        if (_type.equals(ptype.getType()))
          return ptype.getClass().newInstance();
      }
    }
    catch (InstantiationException ex) {
      Logger.getLogger(FudaaManageExeTools.class.getName()).log(Level.SEVERE, null, ex);
    }
    catch (IllegalAccessException ex) {
      Logger.getLogger(FudaaManageExeTools.class.getName()).log(Level.SEVERE, null, ex);
    }
    return null;
  }
}
