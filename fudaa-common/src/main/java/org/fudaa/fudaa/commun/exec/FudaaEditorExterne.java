/*
 *  @file         TrEditorExterne.java
 *  @creation     30 avr. 2003
 *  @modification $Date: 2006-09-19 15:01:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.exec;
import java.io.File;
/**
 * @author deniger
 * @version $Id: FudaaEditorExterne.java,v 1.9 2006-09-19 15:01:53 deniger Exp $
 */
public class FudaaEditorExterne
  extends FudaaExec
  implements FudaaEditorInterface {
  public FudaaEditorExterne(final String _name) {
    super(_name);
  }
  @Override
  public synchronized void edit(final File _f) {
    if (_f != null) {
      execOnFile(_f,null);
    }
  }
  public FudaaExec getExec() {
    return this;
  }
  @Override
  public String getName() {
    return getViewedName();
  }
  /**
   *
   */
  @Override
  public final boolean isExterne() {
    return true;
  }
  /**
   *
   */
  @Override
  public String getCmd() {
    return getExecCommand();
  }
}
