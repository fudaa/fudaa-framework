/*
 * @creation 8 f�vr. 07
 * @modification $Date: 2007-03-15 17:01:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import java.beans.PropertyChangeListener;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;
import org.fudaa.ebli.controle.BSelecteurTextField;

/**
 * @author fred deniger
 * @version $Id: FudaaReportSelecteurTitleTarget.java,v 1.3 2007-03-15 17:01:09 deniger Exp $
 */
public class FudaaReportSelecteurTitleTarget implements BSelecteurTargetInterface {

  final FudaaDjaFormInterface target_;

  public FudaaReportSelecteurTitleTarget(final FudaaDjaFormInterface _target) {
    super();
    target_ = _target;
  }

  @Override
  public void addPropertyChangeListener(String _key, PropertyChangeListener _l) {}

  @Override
  public Object getMin(String _key) {
    return getProperty(_key);
  }

  @Override
  public Object getMoy(String _key) {
    return getProperty(_key);
  }

  @Override
  public Object getProperty(String _key) {
    if (_key == BSelecteurTextField.TITLE_PROPERTY) {
      return target_.getTitle();
    }
    return null;
  }

  @Override
  public void removePropertyChangeListener(String _key, PropertyChangeListener _l) {}

  @Override
  public boolean setProperty(String _key, Object _newProp) {
    if (_key == BSelecteurTextField.TITLE_PROPERTY) {
      target_.setTitle((String) _newProp);
      return true;
    }
    return false;
  }
}
