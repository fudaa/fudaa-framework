/*
 * @creation     18 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.exetools;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.io.IOException;
import java.io.OutputStream;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import org.fudaa.ctulu.CtuluTaskOperationAbstract;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.ProgressionDetailedInterface;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * Un panneau pour afficher la trace d'�x�cution d'un utilitaire externe.
 * 
 * @author marchand@deltacad.fr
 * @version $Id$
 */
public class FudaaOutputPanel extends CtuluDialogPanel implements ProgressionDetailedInterface {

  private CtuluUI ui_;
  private OutputStream outs_;
//  private JButton btValider;
  private CtuluTaskOperationAbstract task_;
  
  private JTextArea taTrace_;

  public FudaaOutputPanel(CtuluUI _ui, CtuluTaskOperationAbstract _task) {
    ui_=_ui;
    task_=_task;
    
    setLayout(new BorderLayout(5, 5));
    taTrace_=new JTextArea();
    taTrace_.setFont(new Font("Courier",Font.PLAIN,taTrace_.getFont().getSize()));
    JScrollPane pnTrace=new JScrollPane(taTrace_);
    pnTrace.setPreferredSize(new Dimension(800,400));
    add(pnTrace,BorderLayout.CENTER);
    setHelpText(FudaaLib.getS("Utilisez 'Annuler' pour arreter la progression"));
    
    outs_=new OutputStream() {
      @Override
      public void write(int b) throws IOException {
        taTrace_.append(String.valueOf((char)b));
      }
    };
  }
  
  public void setTask(CtuluTaskOperationAbstract _task) {
    task_=_task;
  }

  @Override
  public boolean cancel() {
    if (task_!=null && task_.isAlive()) 
      task_.requestStop();
    return false;
  }
  
  @Override
  public boolean isDataValid() {
    return true;
  }

  @Override
  public void appendDetail(String _s) {
    taTrace_.append(_s);
  }

  @Override
  public void appendDetailln(String _s) {
    taTrace_.append(_s+"\n");
  }

  @Override
  public void setProgression(int _v) {
  }

  @Override
  public void setDesc(String _s) {
  }

  @Override
  public void reset() {
    try {
      taTrace_.getDocument().remove(0, taTrace_.getDocument().getLength());
    }
    catch (BadLocationException ex) {
    }
  }
  
  /**
   * @return L'output stream li� � la fenetre pour affichage des messages.
   */
  public OutputStream getOutputStream() {
    return outs_;
  }
  
  public static void main(String[] _args) {
    FudaaOutputPanel pn=new FudaaOutputPanel(null,null);
    pn.afficheModale(pn, "Essai", CtuluDialog.CANCEL_OPTION);
  }
}
