package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.*;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.fudaa.commun.FudaaLib;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author fred deniger
 * @version $Id: FudaaInternalFrameList.java,v 1.7 2007-03-30 15:36:58 deniger Exp $
 */
public final class FudaaInternalFrameList extends JList {
  /**
   * @author fred deniger
   * @version $Id: FudaaInternalFrameList.java,v 1.7 2007-03-30 15:36:58 deniger Exp $
   */
  final class CellFrameRenderer extends CtuluCellTextRenderer {
    private final Border border_ = BuBorders.EMPTY1111;

    public CellFrameRenderer() {
      setBorder(border_);
      setHorizontalAlignment(SwingConstants.CENTER);
      setVerticalAlignment(SwingConstants.CENTER);
      setVerticalTextPosition(SwingConstants.CENTER);
      setHorizontalTextPosition(SwingConstants.CENTER);
    }

    @Override
    protected void setValue(Object _value) {
      JInternalFrame internalFrame = (JInternalFrame) _value;
      setIcon((internalFrame).getFrameIcon());
      setToolTipText((internalFrame).getTitle());
      // setBackground(Color.ORANGE);
      setOpaque(((FudaaInternalFrameModel) getModel()).desk_.getSelectedFrame() == internalFrame);
      setBorder(border_);
    }
  }

  /**
   *
   */
  private final FudaaInternalFrameToolTip t_;

  /**
   *
   */
  public FudaaInternalFrameList(FudaaInternalFrameModel _model) {
    setModel(_model);
    getModel().addListDataListener(new ListDataListener() {
      @Override
      public void intervalAdded(ListDataEvent e) {
        repaint(0);
      }

      @Override
      public void intervalRemoved(ListDataEvent e) {
        repaint(0);
      }

      @Override
      public void contentsChanged(ListDataEvent e) {
        repaint(0);
      }
    });
    setSelectionModel(_model.selection_);
    setBorder(BuBorders.EMPTY1111);
    setDoubleBuffered(false);
    setOpaque(false);
    setCellRenderer(getRenderer());
    t_ = new FudaaInternalFrameToolTip();
    t_.setComponent(this);
    addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent _e) {
        if (!isSelectionEmpty() && _e.getClickCount() == 2) {
          editSelected(FudaaInternalFrameList.this);
        }
      }
    });
  }

  public static void editSelected(JList _l) {
    int i = _l.getSelectedIndex();
    final JInternalFrame fr = (JInternalFrame) _l.getModel().getElementAt(i);
    CtuluDialogPanel pn = createTitleEditor(fr);
    CtuluDialog dial = new CtuluDialog(CtuluLibSwing.getFrameAncestor(_l), pn);
    setRenameTitle(dial);
    Point pt = _l.getLocationOnScreen();
    dial.setLocation(pt);
    dial.setModal(true);
    dial.pack();
    dial.afficheDialog(false);
  }

  private static void setRenameTitle(CtuluDialog dial) {
    dial.setTitle(FudaaLib.getS("Modifier le titre de la fen�tre"));
  }

  public static CtuluDialogPanel createTitleEditor(final JInternalFrame _fr) {
    final BuLabel lb = new BuLabel(FudaaLib.getS("Titre"));
    final BuTextField tf = new BuTextField(_fr.getTitle());
    CtuluDialogPanel pn = new CtuluDialogPanel(false) {
      @Override
      public boolean isDataValid() {
        if (CtuluLibString.isEmpty(tf.getText())) {
          lb.setForeground(Color.RED);
          lb.setToolTipText(CtuluLib.getS("Le nom ne doit pas �tre vide"));
          return false;
        }
        return true;
      }

      @Override
      public boolean apply() {
        _fr.setTitle(tf.getText());
        return true;
      }
    };
    pn.setLayout(new BuGridLayout(2, 5, 5));
    pn.add(lb);
    pn.add(tf);
    return pn;
  }

  public static void editFrameTitle(JInternalFrame _f, int _x, int _y) {
    CtuluDialogPanel pn = createTitleEditor(_f);
    CtuluDialog dial = new CtuluDialog(CtuluLibSwing.getFrameAncestor(_f), pn);
    setRenameTitle(dial);
    dial.setLocation(new Point(_x, _y));
    dial.setModal(true);
    dial.pack();
    dial.afficheDialog(false);
  }

  public BuInternalFrame getToolTipTextFrame(MouseEvent _event) {
    if (_event != null) {
      Point p = _event.getPoint();
      int index = locationToIndex(p);
      return index >= 0 ? (BuInternalFrame) getModel().getElementAt(index) : null;
    }
    return null;
  }

  @Override
  public JToolTip createToolTip() {
    t_.repaint();
    return t_;
  }

  @Override
  public String getToolTipText(MouseEvent _event) {
    t_.setIf(getToolTipTextFrame(_event));
    return super.getToolTipText(_event);
  }

  public ListCellRenderer getRenderer() {
    return new CellFrameRenderer();
  }
}
