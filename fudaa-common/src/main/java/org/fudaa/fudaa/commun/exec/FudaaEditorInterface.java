/*
 *  @file         TrEditorInterface.java
 *  @creation     30 avr. 2003
 *  @modification $Date: 2006-09-19 15:01:54 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.exec;
import java.io.File;
/**
 * @author deniger
 * @version $Id: FudaaEditorInterface.java,v 1.7 2006-09-19 15:01:54 deniger Exp $
 */
public interface FudaaEditorInterface {
   void edit(File _f);
  boolean isExterne();
  String getName();
  /**
   * @return ID: Ne doit jamais etre nulle: represente l'editeur dans les pref.
   */
  String getIDName();
  String getCmd();
}
