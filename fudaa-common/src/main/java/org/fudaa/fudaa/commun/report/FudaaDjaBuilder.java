/*
 * @creation 12 mars 07
 * @modification $Date: 2007-03-30 15:37:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuTextField;
import com.memoire.dja.DjaObject;
import com.memoire.dja.DjaVector;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultCellEditor;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumnModel;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluPopupMenu;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.commun.EbliPopupListener;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author fred deniger
 * @version $Id: FudaaDjaBuilder.java,v 1.4 2007-03-30 15:37:08 deniger Exp $
 */
public final class FudaaDjaBuilder {

  private static class DjaPopupMenu extends CtuluPopupMenu implements ActionListener {

    BuMenuItem remove_;
    BuMenuItem up_;
    BuMenuItem down_;
    BuMenuItem first_;
    BuMenuItem last_;
    final FudaaDjaGrid grid_;

    public DjaPopupMenu(FudaaDjaGrid _grid) {
      grid_ = _grid;
      first_ = addMenuItem(EbliLib.getS("En premier"), "FIRST", EbliResource.EBLI.getIcon("enpremier"), false, this);
      up_ = addMenuItem(CtuluLib.getS("Monter"), "UP", CtuluResource.CTULU.getIcon("monter"), false, this);
      down_ = addMenuItem(CtuluLib.getS("Descendre"), "DOWN", CtuluResource.CTULU.getIcon("descendre"), false, this);
      last_ = addMenuItem(EbliResource.EBLI.getString("En dernier"), "DERNIER", EbliResource.EBLI.getIcon("endernier"),
          false, this);
      addSeparator();
      remove_ = addMenuItem(EbliResource.EBLI.getString("D�truire"), "DETRUIRE", EbliResource.EBLI.getIcon("detruire"),
          false, this);

    }

    @Override
    public void setVisible(boolean _b) {
      if (_b) {
        DjaVector object = grid_.getSelection();
        boolean ok = object.size() > 0;
        remove_.setEnabled(ok);
        boolean canUp = ok && grid_.getObjects().indexOf(object.elementAt(0)) > 0;
        boolean canDown = ok
            && grid_.getObjects().indexOf(object.elementAt(object.size() - 1)) < grid_.getObjects().size() - 1;
        up_.setEnabled(canUp);
        first_.setEnabled(canUp);
        down_.setEnabled(canDown);
        last_.setEnabled(canDown);
      }
      super.setVisible(_b);
    }

    @Override
    public void actionPerformed(ActionEvent _e) {
      Object src = _e.getSource();

      if (remove_ == src) {
        grid_.removeSelected();
        return;
      }
      DjaVector object = grid_.getSelection();
      grid_.clearSelection();
      if (first_ == src) {
        for (int i = object.size() - 1; i >= 0; i--) {
          DjaObject djaObject = ((DjaObject) object.elementAt(i));
          // je ne sais pas pourqu
          int x = djaObject.getX();
          int y = djaObject.getY();
          grid_.remove(djaObject);

          grid_.add(djaObject);
          // le add reteste les coordonn�es de l'objet. Or dans notre cas, on veut garder les anciennes valeurs,donc
          djaObject.setX(x);
          djaObject.setY(y);
        }

      } else if (last_ == src) {
        for (int i = 0; i < object.size(); i++) {
          DjaObject djaObject = ((DjaObject) object.elementAt(i));
          grid_.remove(djaObject);
          grid_.insert(grid_.getObjects().size(), djaObject);
        }
      } else if (up_ == src) {
        for (int i = 0; i < object.size(); i++) {
          DjaObject djaObject = ((DjaObject) object.elementAt(i));
          int idx = grid_.getObjects().indexOf(djaObject);
          grid_.remove(djaObject);
          grid_.insert(idx - 1, djaObject);
        }
      } else if (down_ == src) {
        for (int i = object.size() - 1; i >= 0; i--) {
          DjaObject djaObject = ((DjaObject) object.elementAt(i));
          int idx = grid_.getObjects().indexOf(djaObject);
          grid_.remove(djaObject);
          grid_.insert(idx + 1, djaObject);
        }
      }
      // on remet la s�lection � jour
      for (int i = object.size() - 1; i >= 0; i--) {
        DjaObject djaObject = ((DjaObject) object.elementAt(i));
        djaObject.setSelected(true);
      }

      grid_.repaint();

    }
  }

  public static CtuluTable createDjaTable(FudaaDjaGrid _grid) {
    return new FudaaDjaTable(_grid);
  }

  public static void buildComposantTable(CtuluTable _table, FudaaDjaFormListener _listener) {
    CtuluTable l = _table;
    l.setColumnSelectionAllowed(false);
    l.setRowSelectionAllowed(true);
    l.setModel(_listener.getTableModel());
    l.setSelectionModel(_listener.getSelectionModel());
    TableColumnModel columnModel = l.getColumnModel();
    columnModel.getColumn(0).setCellRenderer(FudaaDjaBuilder.getIconCellRenderer());
    columnModel.getColumn(0).setMaxWidth(20);
    columnModel.getColumn(1).setCellRenderer(FudaaDjaBuilder.getTitleCellRenderer());
    columnModel.getColumn(1).setCellEditor(FudaaDjaBuilder.getTitleCellEditor());
    l.setPreferredSize(new Dimension(150, 300));
    l.setSize(new Dimension(150, 300));
  }

  public static class FudaaDjaTable extends CtuluTable {
    DjaPopupMenu popup_;

    public FudaaDjaTable(FudaaDjaGrid _grid) {
      popup_ = new DjaPopupMenu(_grid);
      addMouseListener(new EbliPopupListener() {
        @Override
        public void popup(java.awt.Component _src, int _x, int _y) {
          popup_.show(_src, _x, _y);
        }
      });
    }
  }

  private FudaaDjaBuilder() {}

  public static CtuluCellTextRenderer getTitleCellRenderer() {
    return new CtuluCellTextRenderer() {

      @Override
      protected void setValue(Object _value) {
        super.setText((String) _value);

      }
    };
  }

  public static CtuluCellTextRenderer getIconCellRenderer() {
    return new CtuluCellTextRenderer() {

      @Override
      protected void setValue(Object _value) {
        FudaaDjaFormInterface fudaaDjaFormInterface = (FudaaDjaFormInterface) _value;
        // setBackground(fudaaDjaFormInterface.isAnimated() ? Color.ORANGE : Color.WHITE);
        setToolTipText(fudaaDjaFormInterface.isAnimated() ? fudaaDjaFormInterface.getTitle() + CtuluLibString.ESPACE
            + FudaaLib.getS("Anim�") : fudaaDjaFormInterface.getTitle());
        super.setIcon(fudaaDjaFormInterface.getIcon());

      }
    };
  }

  public static TableCellEditor getTitleCellEditor() {
    DefaultCellEditor res = new DefaultCellEditor(new BuTextField());
    res.setClickCountToStart(2);
    return res;
  }

  CtuluPopupMenu getPopupMenu(FudaaDjaGrid _grid) {
    return new DjaPopupMenu(_grid);

  }
}
