/*
 * @modification $Date: 2007-05-04 13:58:05 $
 * @statut       unstable
 * @file         DjaKeyListener.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package org.fudaa.fudaa.commun.report;

import com.memoire.dja.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Enumeration;

public class FudaaDjaKeyListener extends DjaKeyListener
// not serializable
{
  public FudaaDjaKeyListener(DjaGridInteractive _grid) {
    super(_grid);
  }

  @Override
  public void keyPressed(KeyEvent _evt) {
    int kc = _evt.getKeyCode();
    boolean ctrl = _evt.isControlDown();
    boolean meta = _evt.isMetaDown();
    boolean shift = _evt.isShiftDown();


    if (previous_ == -1) {
      if (kc == KeyEvent.VK_F) {
        previous_ = kc; // Forms
      }
      if (kc == KeyEvent.VK_U) {
        previous_ = kc; // Uml
      }
      if (kc == KeyEvent.VK_O) {
        previous_ = kc; // Options
      }
      if (previous_ != -1) {
        grid_.repaint(0, 0, 32, 32);
        return;
      }
    } else {

      if (previous_ == KeyEvent.VK_O) {
        boolean done = false;

        if (kc == KeyEvent.VK_A) {
          done = true;
          grid_.setAnchorsVisible(!grid_.isAnchorsVisible());
        }
        if (kc == KeyEvent.VK_D) {
          done = true;
          grid_.setDotsVisible(!grid_.isDotsVisible());
        }
        if (kc == KeyEvent.VK_T) {
          done = true;
          grid_.setAttachsVisible(!grid_.isAttachsVisible());
        }

        if (done) {
          grid_.repaint();
        }
      }

      previous_ = -1;
      grid_.repaint(0, 0, 32, 32);
      return;
    }

    if (!ctrl && !shift && !meta) {
      boolean done = false;
      DjaObject f = null;

      if (kc == KeyEvent.VK_J) {
        Dimension ps = grid_.getPreferredSize();
        Dimension cs = grid_.getBounds().getSize();

        if (!cs.equals(ps)) {
          final int FACTOR = 12800;
          int jx = (FACTOR * (cs.width - 2 * deltaX + 1)) / (ps.width - 2 * deltaX + 1);
          int jy = (FACTOR * (cs.height - 2 * deltaY + 1)) / (ps.height - 2 * deltaY + 1);

          for (Enumeration e = grid_.getSelection().elements(); e.hasMoreElements(); ) {
            f = (DjaObject) e.nextElement();
            f.setX(((f.getX() - deltaX) * jx) / FACTOR + deltaX);
            f.setY(((f.getY() - deltaY) * jy) / FACTOR + deltaY);
            f.setWidth((f.getWidth() * jx) / FACTOR);
            f.setHeight((f.getHeight() * jy) / FACTOR);
            DjaLib.snap(f);
          }

          done = true;
        }
      }

      if (kc == KeyEvent.VK_G) {
        DjaVector s = grid_.getSelection();
        DjaGroup g = new DjaGroup(); // "groupe");

        for (Enumeration e = s.elements(); e.hasMoreElements(); ) {
          f = (DjaObject) e.nextElement();
          // quiet
          grid_.remove(f, true);
          g.add(f, true);
          f.setSelected(false);
        }

        grid_.add(g);
        grid_.setSelection(g);
      }

      if (kc == KeyEvent.VK_D) {
        DjaVector s = grid_.getSelection();
        for (Enumeration e = s.elements(); e.hasMoreElements(); ) {
          f = (DjaObject) e.nextElement();
          if (f instanceof DjaGroup) {
            DjaGroup g = (DjaGroup) f;
            grid_.remove(g);
            DjaVector v = (DjaVector) g.getObjects().clone();
            for (Enumeration ex = v.elements(); ex.hasMoreElements(); ) {
              DjaObject fx = (DjaObject) ex.nextElement();
              // quiet
              g.remove(fx, true);
              grid_.add(fx, true);
              fx.setSelected(true);
            }
          }
        }
      }

      if (kc == KeyEvent.VK_X) {
        for (Enumeration e = grid_.getSelection().elements(); e.hasMoreElements(); ) {
          f = (DjaObject) e.nextElement();
          if (f instanceof DjaLink) {
            DjaLink a = (DjaLink) f;
            a.setBeginObject(null);
            a.setEndObject(null);
          }
        }
      }

      if (kc == KeyEvent.VK_Y) {
        for (Enumeration e = grid_.getSelection().elements(); e.hasMoreElements(); ) {
          f = (DjaObject) e.nextElement();
          if (f instanceof DjaLink) {
            DjaLink a = (DjaLink) f;
            if (a.getBeginObject() == null) {
              DjaAnchor anchor = a.getAnchors()[0];
              int xa = anchor.getX();
              int ya = anchor.getY();

              for (Enumeration ex = grid_.getObjects().elements(); ex.hasMoreElements(); ) {
                DjaObject fx = (DjaObject) ex.nextElement();
                if ((fx != a) && !(fx instanceof DjaLink)) {
                  DjaAnchor[] vx = fx.getAnchors();
                  for (int i = 0; i < vx.length; i++)
                    if (DjaLib.close(xa, ya, vx[i].getX(), vx[i].getY())) {
                      a.setBegin(fx, i, vx[i].getO());
                    }
                }
              }
            }
            if (a.getEndObject() == null) {
              DjaAnchor anchor = a.getAnchors()[1];
              int xa = anchor.getX();
              int ya = anchor.getY();

              for (Enumeration ex = grid_.getObjects().elements(); ex.hasMoreElements(); ) {
                DjaObject fx = (DjaObject) ex.nextElement();
                if (fx != a) {
                  DjaAnchor[] vx = fx.getAnchors();
                  for (int i = 0; i < vx.length; i++)
                    if (DjaLib.close(xa, ya, vx[i].getX(), vx[i].getY())) {
                      a.setEnd(fx, i, vx[i].getO());
                    }
                }
              }
            }
          }
        }
      }

      if (kc == KeyEvent.VK_F3) {
        DjaLib.setForeground(grid_);
        done = true;
      }

      if (kc == KeyEvent.VK_F4) {
        DjaLib.setBackground(grid_);
        done = true;
      }

      if (kc == KeyEvent.VK_F5) {
        DjaLib.setColor(grid_);
        done = true;
      }

      /*
       * if(kc==KeyEvent.VK_F6) { DjaVector s=grid_.getSelection(); if(s.size()>0) { Font ft=new
       * Font("SansSerif",Font.PLAIN,12); ft=JFontChooser.showDialog(grid_,"Text Font",ft); if(ft!=null) for(Enumeration
       * e=s.elements(); e.hasMoreElements(); ) { f=(DjaObject)e.nextElement(); f.setFont(ft); } done=true; } }
       */

      if (done) {
        grid_.repaint();
        return;
      }
    }

    if (!ctrl && !shift && !meta) {
      if (kc == KeyEvent.VK_PAGE_UP) {
        grid_.putSelectionToFront();
      } else if (kc == KeyEvent.VK_PAGE_DOWN) {
        grid_.putSelectionToBack();
      } else {
        DjaVector s = grid_.getSelection();

        for (Enumeration e = s.elements(); e.hasMoreElements(); ) {
          DjaObject o = (DjaObject) e.nextElement();

          if (kc == KeyEvent.VK_RIGHT) {
            o.setX(o.getX() + deltaX);
            DjaLib.snap(o);
          }
          if (kc == KeyEvent.VK_LEFT) {
            o.setX(Math.max(0, o.getX() - deltaX));
            DjaLib.snap(o);
          }
          if (kc == KeyEvent.VK_DOWN) {
            o.setY(o.getY() + deltaY);
            DjaLib.snap(o);
          }
          if (kc == KeyEvent.VK_UP) {
            o.setY(Math.max(0, o.getY() - deltaY));
            DjaLib.snap(o);
          }
        }

        grid_.repaint();
      }

      return;
    }

    if (!ctrl && shift && !meta) {
      for (Enumeration e = grid_.getSelection().elements(); e.hasMoreElements(); ) {
        DjaObject o = (DjaObject) e.nextElement();
        // Dimension d=f.getSize();

        if (o instanceof DjaForm) {
          DjaForm f = (DjaForm) o;

          if (kc == KeyEvent.VK_RIGHT) {
            f.setWidth(f.getWidth() + deltaX);
          }
          if (kc == KeyEvent.VK_LEFT) {
            f.setWidth(Math.max(deltaX, f.getWidth() - deltaX));
          }
          if (kc == KeyEvent.VK_DOWN) {
            f.setHeight(f.getHeight() + deltaY);
          }
          if (kc == KeyEvent.VK_UP) {
            f.setHeight(Math.max(deltaY, f.getHeight() - deltaY));
          }
          if (kc == KeyEvent.VK_DELETE) {
            grid_.remove(f);
          }
          DjaLib.snap(f);
        }
      }
      grid_.repaint();
      return;
    }

    if (ctrl && !shift && !meta) {
      DjaVector s = grid_.getSelection();
      DjaVector f = grid_.getObjects();

      if (kc == KeyEvent.VK_A) {
        grid_.setSelection(f);
        grid_.repaint();
        return;
      }

      int p = Integer.MAX_VALUE;

      if (kc == KeyEvent.VK_LEFT) {
        p = 1;
        if (s.size() > 0) {
          p = f.indexOf(s.elementAt(0));
        }
        p--;
      }
      if (kc == KeyEvent.VK_RIGHT) {
        p = -1;
        if (s.size() > 0) {
          p = f.indexOf(s.elementAt(0));
        }
        p++;
      }
      if (kc == KeyEvent.VK_UP) {
        p = 0;
      }
      if (kc == KeyEvent.VK_DOWN) {
        p = f.size() - 1;
      }

      if (p != Integer.MAX_VALUE) {
        if (p < 0) {
          p = f.size() - 1;
        }
        if (p >= f.size()) {
          p = 0;
        }
        grid_.setSelection((DjaObject) f.elementAt(p));
        grid_.repaint();
        return;
      }
    }

    if ((!ctrl && !shift && meta) || (ctrl && shift && !meta)) {
      if (kc == KeyEvent.VK_SPACE) {
        anchor_ = !anchor_;
        return;
      }

      for (Enumeration e = grid_.getSelection().elements(); e.hasMoreElements(); ) {
        DjaObject f = (DjaObject) e.nextElement();
        if (f instanceof DjaLink) {
          DjaLink a = (DjaLink) f;

          if (anchor_) {
            int n = a.getEndPosition();
            int t = a.getEndType();
            if (kc == KeyEvent.VK_RIGHT) {
              a.setEndPosition(n + 1);
            }
            if (kc == KeyEvent.VK_LEFT) {
              a.setEndPosition(n - 1);
            }
            if (kc == KeyEvent.VK_UP) {
              a.setEndType(t + 1);
            }
            if (kc == KeyEvent.VK_DOWN) {
              a.setEndType(t - 1);
            }
          } else {
            int n = a.getBeginPosition();
            int t = a.getBeginType();
            if (kc == KeyEvent.VK_RIGHT) {
              a.setBeginPosition(n + 1);
            }
            if (kc == KeyEvent.VK_LEFT) {
              a.setBeginPosition(n - 1);
            }
            if (kc == KeyEvent.VK_UP) {
              a.setBeginType(t + 1);
            }
            if (kc == KeyEvent.VK_DOWN) {
              a.setBeginType(t - 1);
            }
          }
        }
      }

      grid_.repaint();
      return;
    }
  }
}
