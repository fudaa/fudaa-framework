/*
 *  @file         FudaaPreferencesAbstract.java
 *  @creation     11 juin 2003
 *  @modification $Date: 2006-09-19 15:01:55 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun;

import com.memoire.bu.BuPreferences;
import java.util.Properties;
import org.fudaa.ctulu.CtuluLibString;
/**
 * @author deniger
 * @version $Id$
 */
public abstract class FudaaPreferencesAbstract extends BuPreferences {

  protected Properties defaultValues_;

  /**
   * Permet de sp�ficier des preferences par d�faut.
   */
  public FudaaPreferencesAbstract() {
    super();
    defaultValues_ = new Properties();
    values_ = new Properties(defaultValues_);
    readIniFile();
  }
  
  public String[] getStringArrayProperty(final String _key,final String[] _defaut){
    final String s = getStringProperty(_key,null);
    if (s == null) {
      return _defaut;
    }
    return CtuluLibString.parseString(s, ";");
  }

  public void putStringArrayProperty(final String _key,final String[] _defaut){
    putStringProperty(_key, CtuluLibString.arrayToString(_defaut, ";"));
  }
}
