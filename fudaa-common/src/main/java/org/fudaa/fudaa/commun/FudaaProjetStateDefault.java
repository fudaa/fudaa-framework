/*
 * @creation 11 juin 07
 * @modification $Date: 2008-01-10 09:58:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Une implementation par defaut de l'�tat courant d'un projet 
 * (UI modifi�e, donn�es modifi�es, etc.).
 * 
 * @author fred deniger
 * @version $Id: FudaaProjetStateDefault.java,v 1.2 2008-01-10 09:58:19 bmarchan Exp $
 */
public class FudaaProjetStateDefault implements FudaaProjetStateInterface {

  boolean uiModified_;
  boolean paramModified_;
  final Set listener_ = new HashSet();

  @Override
  public void addListener(FudaaProjectStateListener _l) {
    listener_.add(_l);
  }

  public void removeAllListeners() {
    listener_.clear();
  }

  @Override
  public boolean isParamsModified() {
    return paramModified_;
  }

  public void clearStates() {
    uiModified_ = false;
    paramModified_ = false;
    fireChanged();
  }

  protected void fireChanged() {
    for (Iterator it = listener_.iterator(); it.hasNext();) {
      ((FudaaProjectStateListener) it.next()).projectStateChanged(this);
    }
  }

  public void setParamModified(boolean _paramModified) {
    if (paramModified_ != _paramModified) {
      paramModified_ = _paramModified;
      fireChanged();
    }
  }

  public void setUiModified(boolean _uiModified) {
    if (uiModified_ != _uiModified) {
      uiModified_ = _uiModified;
      fireChanged();
    }
  }

  @Override
  public boolean isUIModified() {
    return uiModified_;
  }

}
