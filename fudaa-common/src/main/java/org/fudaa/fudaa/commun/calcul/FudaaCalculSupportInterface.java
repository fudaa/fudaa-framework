/*
 *  @file         TrCalculSupportInterface.java
 *  @creation     10 juin 2003
 *  @modification $Date: 2006-09-19 15:02:27 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.calcul;

import org.fudaa.dodico.calcul.CalculLauncher;

/**
 * @author deniger
 * @version $Id: FudaaCalculSupportInterface.java,v 1.4 2006-09-19 15:02:27 deniger Exp $
 */
public interface FudaaCalculSupportInterface {
  CalculLauncher actionCalcul();
}
