/**
 * @modification $Date: 2007/05/04 14:01:46 $
 * @statut       unstable
 * @file         BuLookPreferencesPanel.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.*;
import com.memoire.fu.FuLib;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * A panel where the user can choose his favorite look. (background, look and feel, icons, ...) desktop.decor
 * desktop.texture desktop.background lookandfeel.name lookandfeel.class lookandfeel.theme icons.size.
 * In visual category. Replace BuLookPreferencesPanel, with options not applicable in Fudaa applications.
 */
public class FudaaLookPreferencesPanel extends BuAbstractPreferencesPanel implements ActionListener {

  BuCommonInterface appli_;
  AbstractBorder boGenDesk_;
  BuOptionRenderer cbRenderer_;
  BuLabel lbGenAspAspect_, lbGenAspSlaf_, lbGenAspMetal_;

  BuLabel lbGenAspSkinlf_, lbGenAspOyoaha_;
  BuLabel lbGenDeskDecor_, lbGenDeskTexture_, lbGenDeskCouleur_;
  BuComboBox liGenAspAspect_, liGenAspSlaf_, liGenAspMetal_;
  BuComboBox liGenDeskDecor_, liGenDeskTexture_, liGenDeskCouleur_;
  BuGridLayout loGen_;
  BuGridLayout loGenDesk_;
  BuPreferences options_;
  Map<Object,Object> optionsStr_;
  BuPanel pnGenDesk_;
  BuTextField tfGenAspSkinlf_, tfGenAspOyoaha_;

  @Override
  protected final String getS(final String _s) {
    return FudaaResource.FUDAA.getString(_s);
  }

  public FudaaLookPreferencesPanel(final BuCommonInterface _appli) {
    super();
    appli_ = _appli;
    options_ = BuPreferences.BU;
    cbRenderer_ = new BuOptionRenderer();
    optionsStr_ = new HashMap<Object, Object>();
    final String aspectStr = "aspect";
    optionsStr_.put("DESKTOP_UNIFORME", new BuOptionItem(getS("Uniforme"), BuResource.BU.getMenuIcon("uniforme")));
    optionsStr_.put("DESKTOP_DEGRADE", new BuOptionItem(getS("D�grad�"), BuResource.BU.getMenuIcon("degrade")));
    optionsStr_.put(getTexture(), new BuOptionItem(getS("Textur�"), BuResource.BU.getMenuIcon("texture")));

    for (int i = 1; i <= 9; i++) {
      optionsStr_.put(getTexture() + i, new BuOptionItem(getS("Texture") + ' ' + i, new BuTextureIcon(BuPreferences.BU
          .getTexture(i))));
    }
    optionsStr_.put(getDesktopUniforme(), new BuOptionItem(getS("Uniforme"), BuResource.BU.getMenuIcon("uniforme")));
    optionsStr_.put("DESKTOP_DEGRADE", new BuOptionItem(getS("D�grad�"), BuResource.BU.getMenuIcon("degrade")));
    optionsStr_.put("DESKTOP_ROUGE", new BuOptionItem(getS("Rouge"), new BuColorIcon(new Color(128, 64, 64))));
    optionsStr_.put("DESKTOP_VERT", new BuOptionItem(getS("Vert"), new BuColorIcon(new Color(64, 129, 64))));
    optionsStr_.put("DESKTOP_BLEU", new BuOptionItem(getS("Bleu"), new BuColorIcon(new Color(64, 64, 128))));
    optionsStr_.put("DESKTOP_ORANGE", new BuOptionItem(getS("Orange"), new BuColorIcon(new Color(192, 128, 96))));
    optionsStr_.put("DESKTOP_SIMILAIRE", new BuOptionItem(getS("Similaire"), new BuColorIcon(UIManager
        .getColor("Panel.background"))));
    optionsStr_.put(getDesktopDefault(), new BuOptionItem(getS("D�faut"), new BuColorIcon(UIManager
        .getColor("Desktop.background"))));

    optionsStr_.put(getAspectDefaut(), new BuOptionItem(getS("D�faut"), BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled(getAspectDefaut())));
    optionsStr_.put("ASPECT_AMIGA", new BuOptionItem("Amiga", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_AMIGA")));
    optionsStr_.put("ASPECT_FHLAF", new BuOptionItem("FHLaf", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_FHLAF")));
    optionsStr_.put("ASPECT_GTK", new BuOptionItem(getS("GTK"), BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_GTK")));
    optionsStr_.put("ASPECT_KUNSTSTOFF", new BuOptionItem("Kunststoff", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_KUNSTSTOFF")));
    optionsStr_.put("ASPECT_LIQUID", new BuOptionItem("Liquid", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_LIQUID")));
    optionsStr_.put("ASPECT_SUBSTANCE", new BuOptionItem("Substance", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_SUBSTANCE")));
    optionsStr_.put("ASPECT_MAC", new BuOptionItem("Mac", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_MAC")));
    optionsStr_.put("ASPECT_METAL", new BuOptionItem("Metal", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_METAL")));
    optionsStr_.put("ASPECT_MOTIF", new BuOptionItem("Motif", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_MOTIF")));
    optionsStr_.put("ASPECT_NIMBUS", new BuOptionItem("Nimbus", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_NIMBUS")));
    optionsStr_.put("ASPECT_NEXT", new BuOptionItem("Next", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_NEXT")));
    optionsStr_.put("ASPECT_ORGANIC", new BuOptionItem("Organic", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_ORGANIC")));
    optionsStr_.put("ASPECT_OYOAHA", new BuOptionItem("Oyoaha", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_OYOAHA")));
    optionsStr_.put("ASPECT_PLASTIC", new BuOptionItem("Plastic", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_PLASTIC")));
    optionsStr_.put("ASPECT_PLASTIC3D", new BuOptionItem("Plastic 3D", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_PLASTIC3D")));
    optionsStr_.put("ASPECT_PLASTICXP", new BuOptionItem("Plastic XP", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_PLASTICXP")));
    optionsStr_.put("ASPECT_SKINLF", new BuOptionItem("SkinLF", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_SKINLF")));
    optionsStr_.put("ASPECT_SLAF", new BuOptionItem("Slaf", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_SLAF")));
    optionsStr_.put("ASPECT_WINDOWS", new BuOptionItem("Windows", BuResource.BU.getMenuIcon(aspectStr), options_
        .isEnabled("ASPECT_WINDOWS")));
    final Object[][] o = FuLib.convertHashtableToArray(optionsStr_);
    for (int i = o.length - 1; i >= 0; i--) {
      optionsStr_.put(o[i][1], o[i][0]);
    }

    createPanel();
  }

  private String getTexture() {
    return "DESKTOP_TEXTURE";
  }

  private String getDesktopUniforme() {
    return "DESKTOP_UNIFORME";
  }

  private void createPanel() {
    Object[] items;
    int nGenDesk;
    // setLayout(new BuBorderLayout());
    setLayout(new BuVerticalLayout(5));
    setBorder(EMPTY5555);

    // Desktop
    pnGenDesk_ = new BuPanel();
    boGenDesk_ = new CompoundBorder(new TitledBorder(getS("Aspect")), EMPTY5555);
    pnGenDesk_.setBorder(boGenDesk_);
    loGenDesk_ = new BuGridLayout();
    loGenDesk_.setColumns(2);
    loGenDesk_.setVgap(5);
    loGenDesk_.setHgap(5);
    pnGenDesk_.setLayout(loGenDesk_);
    nGenDesk = 0;

    // Aspect
    lbGenAspAspect_ = new BuLabel(getS("Aspect:"), SwingConstants.RIGHT);
    items = new BuOptionItem[12];
    int i = 0;
    items[i++] = optionsStr_.get(getAspectDefaut());
    // items[ i++]=optionsStr_.get("ASPECT_AMIGA");
    // items[ i++]=optionsStr_.get("ASPECT_FHLAF");
    items[i++] = optionsStr_.get("ASPECT_GTK");
    // items[ i++]=optionsStr_.get("ASPECT_KUNSTSTOFF");
    items[i++] = optionsStr_.get("ASPECT_LIQUID");
    items[i++] = optionsStr_.get("ASPECT_MAC");
    items[i++] = optionsStr_.get("ASPECT_METAL");
    items[i++] = optionsStr_.get("ASPECT_MOTIF");
    /*
     * items[ i++]=optionsStr_.get("ASPECT_NEXT"); items[i++]=optionsStr_.get("ASPECT_ORGANIC");
     * items[i++]=optionsStr_.get("ASPECT_OYOAHA");
     */
    items[i++] = optionsStr_.get("ASPECT_NIMBUS");
    items[i++] = optionsStr_.get("ASPECT_PLASTIC");
    items[i++] = optionsStr_.get("ASPECT_PLASTIC3D");
    items[i++] = optionsStr_.get("ASPECT_PLASTICXP");
    items[i++] = optionsStr_.get("ASPECT_SUBSTANCE");
    /*
     * items[i++]=optionsStr_.get("ASPECT_SKINLF"); items[i++]=optionsStr_.get("ASPECT_SLAF");
     */
    items[i++] = optionsStr_.get("ASPECT_WINDOWS");
    liGenAspAspect_ = new BuComboBox(items);
    liGenAspAspect_.setRenderer(cbRenderer_);
    liGenAspAspect_.setEditable(false);
    liGenAspAspect_.addActionListener(this);
    pnGenDesk_.add(lbGenAspAspect_, nGenDesk++);
    pnGenDesk_.add(liGenAspAspect_, nGenDesk++);

    // Decor
    lbGenDeskDecor_ = new BuLabel(getS("D�cor:"), SwingConstants.RIGHT);
    items = new BuOptionItem[3];
    items[0] = optionsStr_.get(getDesktopUniforme());
    items[1] = optionsStr_.get("DESKTOP_DEGRADE");
    items[2] = optionsStr_.get(getTexture());
    liGenDeskDecor_ = new BuComboBox(items);
    liGenDeskDecor_.setRenderer(cbRenderer_);
    liGenDeskDecor_.setEditable(false);
    liGenDeskDecor_.addActionListener(this);
    pnGenDesk_.add(lbGenDeskDecor_, nGenDesk++);
    pnGenDesk_.add(liGenDeskDecor_, nGenDesk++);

    // Texture
    lbGenDeskTexture_ = new BuLabel(getS("Texture:"), SwingConstants.RIGHT);
    items = new BuOptionItem[4];
    items[0] = optionsStr_.get("DESKTOP_TEXTURE1");
    items[1] = optionsStr_.get("DESKTOP_TEXTURE2");
    items[2] = optionsStr_.get("DESKTOP_TEXTURE3");
    items[3] = optionsStr_.get("DESKTOP_TEXTURE4");
    liGenDeskTexture_ = new BuComboBox(items);
    liGenDeskTexture_.setRenderer(cbRenderer_);
    liGenDeskTexture_.setEditable(false);
    liGenDeskTexture_.addActionListener(this);
    pnGenDesk_.add(lbGenDeskTexture_, nGenDesk++);
    pnGenDesk_.add(liGenDeskTexture_, nGenDesk++);

    // Couleur
    lbGenDeskCouleur_ = new BuLabel(getS("Couleur:"), SwingConstants.RIGHT);
    items = new BuOptionItem[6];
    items[0] = optionsStr_.get("DESKTOP_ROUGE");
    items[1] = optionsStr_.get("DESKTOP_VERT");
    items[2] = optionsStr_.get("DESKTOP_BLEU");
    items[3] = optionsStr_.get("DESKTOP_ORANGE");
    items[4] = optionsStr_.get("DESKTOP_SIMILAIRE");
    items[5] = optionsStr_.get(getDesktopDefault());
    liGenDeskCouleur_ = new BuComboBox(items);
    liGenDeskCouleur_.setRenderer(cbRenderer_);
    liGenDeskCouleur_.setEditable(false);
    liGenDeskCouleur_.addActionListener(this);
    pnGenDesk_.add(lbGenDeskCouleur_, nGenDesk++);
    pnGenDesk_.add(liGenDeskCouleur_, nGenDesk++);

    add(pnGenDesk_);// ,BuBorderLayout.CENTER);

    updateComponents();
  }

  private String getDesktopDefault() {
    return "DESKTOP_DEFAUT";
  }

  private String getAspectDefaut() {
    return "ASPECT_DEFAUT";
  }

  private void fillTable() {
    Object o;

    o = liGenDeskDecor_.getSelectedItem();
    o = (o == null) ? getDesktopUniforme() : optionsStr_.get(o);
    if (o != null) {
      options_.putStringProperty("desktop.decor", o.toString());
    }

    o = liGenDeskTexture_.getSelectedItem();
    o = (o == null) ? "DESKTOP_TEXTURE1" : optionsStr_.get(o);
    options_.putStringProperty("desktop.texture", o.toString());

    o = liGenDeskCouleur_.getSelectedItem();
    o = (o == null) ? getDesktopDefault() : optionsStr_.get(o);
    options_.putStringProperty("desktop.background", o.toString());

    o = liGenAspAspect_.getSelectedItem();
    o = (o == null) ? getAspectDefaut() : optionsStr_.get(o);
    options_.putStringProperty("lookandfeel.name", o.toString());

    /*
     * o=liGenAspMetal_.getSelectedItem(); o=(o==null) ? "THEME_METAL0" : optionsStr_.get(o);
     * options_.putStringProperty("metal.theme",o.toString()); o=liGenAspSlaf_.getSelectedItem(); o=(o==null) ?
     * "default" : optionsStr_.get(o); options_.putStringProperty("slaf.theme",o.toString());
     * options_.putStringProperty("skinlf.theme",tfGenAspSkinlf_.getText());
     * options_.putStringProperty("oyoaha.theme",tfGenAspOyoaha_.getText());
     */
    setDirty(false);
    options_.putBooleanProperty("tr.lnf", true);
  }

  private void updateComponents() {
    liGenDeskDecor_.setSelectedItem(optionsStr_.get(options_.getStringProperty("desktop.decor", getDesktopUniforme())));
    liGenDeskTexture_.setSelectedItem(optionsStr_
        .get(options_.getStringProperty("desktop.texture", "DESKTOP_TEXTURE1")));
    liGenDeskCouleur_.setSelectedItem(optionsStr_.get(options_.getStringProperty("desktop.background",
        getDesktopDefault())));
    liGenAspAspect_.setSelectedItem(optionsStr_.get(options_.getStringProperty("lookandfeel.name", "ASPECT_DEFAUT")));

    updateAbility();
    setDirty(false);
  }

  // Evenements

  @Override
  protected void updateAbility() {
    boolean b;

    b = (liGenDeskDecor_.getSelectedItem() == optionsStr_.get(getTexture()));
    liGenDeskTexture_.setEnabled(b);
    liGenDeskCouleur_.setEnabled(!b);
  }

  // Methodes publiques

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    setDirty(true);
    updateAbility();
  }

  @Override
  public void cancelPreferences() {
    options_.readIniFile();
    updateComponents();
  }

  @Override
  public String getCategory() {
    return BuResource.BU.getString("Visuel");
  }

  @Override
  public String getTitle() {
    return getS("Aspect");
  }

  @Override
  public boolean isPreferencesApplyable() {
    return false;
  }

  // Methodes privees

  @Override
  public boolean isPreferencesCancelable() {
    return true;
  }

  @Override
  public boolean isPreferencesValidable() {
    return true;
  }

  @Override
  public void validatePreferences() {
    fillTable();
    options_.writeIniFile();
  }
}
