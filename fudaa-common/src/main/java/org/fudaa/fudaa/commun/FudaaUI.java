/**
 * @file FudaaUI.java
 * @creation 6 juin 2003
 * @modification $Date: 2003/11/25 10:13:28
 * $ @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun;

import java.io.File;
import org.fudaa.ctulu.CtuluUI;
/**
 * @author deniger
 * @version $Id: FudaaUI.java,v 1.16 2006-09-19 15:01:55 deniger Exp $
 */
public interface FudaaUI extends CtuluUI {



  /**
   * Permet a l'utilisateur de choisir un fichier.
   * @param _desc la description du fichier
   * @param _extension les extensions voulues
   * @param _newFile si ce doit etre un nouveau fichier
   *
   * @return le fichier choisi ou nul si aucun
   */
  File ouvrirFileChooser(String _desc, String[] _extension, boolean _newFile);

  /**
   * Affiche un message principal.
   *
   * @param _s le message principal
   */
  void setMainMessage(String _s);

  /**
   * @param _i la progression [0,100]
   */
  void setMainProgression(int _i);

  /**
   * Efface eventuellement les messages.
   */
  void unsetMainMessage();

  /**
   * Initialise la progression.
   */
  void unsetMainProgression();
}
