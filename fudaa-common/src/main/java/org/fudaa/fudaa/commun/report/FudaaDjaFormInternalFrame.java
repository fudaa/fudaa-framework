/*
 * @creation 6 f�vr. 07
 * @modification $Date: 2007-03-30 15:37:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.bu.BuResource;
import com.memoire.dja.DjaGraphics;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JInternalFrame;
import org.fudaa.ctulu.CtuluLibImage;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.animation.EbliAnimatedInterface;
import org.fudaa.ebli.animation.EbliAnimationSourceInterface;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author fred deniger
 * @version $Id: FudaaDjaFormInternalFrame.java,v 1.2 2007-03-30 15:37:08 deniger Exp $
 */
class FudaaDjaFormInternalFrame extends FudaaDjaFormAbstract {
  BufferedImage im_;

  final boolean isAnimated_;

  final CtuluImageProducer prd_;
  final Map options_;

  public FudaaDjaFormInternalFrame(final CtuluImageProducer _prd, FudaaDjaGrid _grid) {
    // pas de fond ...
    super.setBackground(null);
    super.setForeground(null);
    prd_ = _prd;
    Rectangle viewRect = _grid.getPane().getViewport().getViewRect();
    int h = Math.min(_prd.getDefaultImageDimension().height, (int) (viewRect.height * 0.8));
    int w = Math.min(_prd.getDefaultImageDimension().width, (int) (viewRect.width * 0.8));
    super.setWidth(w);
    super.setHeight(h);
    Point p = _grid.getPane().getViewport().getViewPosition();
    setX(p.x + (viewRect.width - w) / 2);
    setY(p.y + (viewRect.height - h) / 2);
    // ne pas dessiner le fond
    im_ = prd_.produceImage(w, h, Collections.EMPTY_MAP);
    isAnimated_ = prd_ instanceof EbliAnimatedInterface;
    addText(CtuluLibString.EMPTY_STRING);
    getTextAt(0).setVisible(false);
    ligne_.setCouleur(null);
    icon_ = BuResource.BU.getIcon("image");
    options_ = new HashMap();
    options_.put(CtuluLibImage.PARAMS_CREATE_SCREEN_COMPATIBLE_IMAGE, Boolean.FALSE);
    options_.put(CtuluLibImage.PARAMS_FILL_BACKGROUND_BOOLEAN, Boolean.FALSE);
  }

  @Override
  public FudaaDjaFormAbstract createNew() {
    return null;
  }

  @Override
  public String getDescription() {
    return FudaaLib.getS("Vue fen�tre");
  }

  @Override
  public String getDjaName() {
    return null;
  }

  JInternalFrame getFrame() {
    return (JInternalFrame) prd_;
  }

  @Override
  public boolean isTitleModifiable() {
    return false;
  }

  @Override
  public void active(FudaaReportFrameController _controller) {
    getFrame().setClosable(false);
    _controller.ifListener_.internalFrameAdded(getFrame(), this);

  }

  @Override
  public EbliAnimationSourceInterface getAnim() {
    return ((EbliAnimatedInterface) prd_).getAnimationSrc();
  }

  @Override
  public String getTitle() {
    return getFrame().getTitle();
  }

  @Override
  public boolean isAnimated() {
    return isAnimated_;
  }

  @Override
  public void paintObject(Graphics _g) {
    int x = getX();
    int y = getY();
    int w = getWidth();
    int h = getHeight();
    CtuluLibImage.setBestQuality((Graphics2D) _g);

    Color bg = getBackground();

    if (bg != null) {
      DjaGraphics.setColor(_g, bg);
      DjaGraphics.fillRect(_g, x, y, w, h);
    }
    if (ligne_.getCouleur() != null) {
      ligne_.dessineRectangle((Graphics2D) _g, x, y, w - 1, h - 1);
    }

    _g.drawImage(im_, x, y, w, h, null);
    super.paintObject(_g);
  }

  @Override
  public void refresh() {
    options_.put(CtuluLibImage.PARAMS_CREATE_SCREEN_COMPATIBLE_IMAGE, Boolean.TRUE);
    im_ = prd_.produceImage(getWidth(), getHeight(), options_);
  }

  @Override
  public void unactive(FudaaReportFrameController _controller) {
    getFrame().setClosable(true);
    _controller.ifListener_.internalFrameRemoved(getFrame(), this);
  }

}