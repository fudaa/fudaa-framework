/*
 * @creation 8 f�vr. 07
 * @modification $Date: 2007-03-23 17:25:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.dja.DjaGridEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractListModel;
import javax.swing.JInternalFrame;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import org.fudaa.ctulu.image.CtuluImageProducer;

/**
 * @author fred deniger
 * @version $Id: FudaaReportIfManager.java,v 1.5 2007-03-23 17:25:30 deniger Exp $
 */
public class FudaaReportIfManager extends InternalFrameAdapter {

  protected class ListFrame extends AbstractListModel {

    public ListFrame() {
      super();
    }

    @Override
    protected void fireContentsChanged(Object _source, int _index0, int _index1) {
      super.fireContentsChanged(_source, _index0, _index1);
    }

    @Override
    protected void fireIntervalAdded(Object _source, int _index0, int _index1) {
      super.fireIntervalAdded(_source, _index0, _index1);
    }

    @Override
    protected void fireIntervalRemoved(Object _source, int _index0, int _index1) {
      super.fireIntervalRemoved(_source, _index0, _index1);
    }

    @Override
    public Object getElementAt(int _index) {
      return availableFrame_.get(_index);
    }

    @Override
    public int getSize() {
      return availableFrame_.size();
    }

  }
  final List availableFrame_;
  final FudaaReportFrameController controller_;

  final ListFrame list_;
  final Map usedFrame_;

  FudaaReportIfManager(final FudaaReportFrameController _controller, List _available) {
    super();
    availableFrame_ = new ArrayList(_available);
    controller_ = _controller;
    usedFrame_ = new HashMap();
    controller_.owner_.addInternalFrameListener(new InternalFrameAdapter() {
      @Override
      public void internalFrameClosed(InternalFrameEvent _e) {
        controller_.owner_.getApp().removeIfListener(FudaaReportIfManager.this);
        controller_.clearAll();
      }

    });
    list_ = new ListFrame();
  }

  private void addAvailableFrame(JInternalFrame _f) {
    if (_f != controller_.owner_ && isAvailable(_f) == null && isUsed(_f) == null && _f instanceof CtuluImageProducer) {
      availableFrame_.add(_f);
      list_.fireIntervalAdded(list_, list_.getSize() - 1, list_.getSize() - 1);

    }
  }

  private void removeAvailableFrame(JInternalFrame _f) {
    final int i = availableFrame_.indexOf(_f);
    if (i >= 0) {
      availableFrame_.remove(i);
      list_.fireIntervalRemoved(this, i, i);
    }
  }

  protected JInternalFrame isAvailable(JInternalFrame _f) {
    return (availableFrame_.contains(_f)) ? _f : null;
  }

  protected JInternalFrame isUsed(JInternalFrame _f) {
    return (usedFrame_.containsKey(_f)) ? _f : null;
  }

  protected void refresh(JInternalFrame _f) {
    FudaaDjaFormInterface djaForm = (FudaaDjaFormInterface) usedFrame_.get(_f);
    if (djaForm != null) djaForm.refresh();
    controller_.grid_.fireGridEvent(djaForm, DjaGridEvent.MODIFIED);
    controller_.grid_.repaint();
  }

  @Override
  public void internalFrameActivated(InternalFrameEvent _e) {

    if (_e.getInternalFrame() instanceof CtuluImageProducer) {
      addAvailableFrame(_e.getInternalFrame());

    }

  }

  public void internalFrameAdded(JInternalFrame _f, FudaaDjaFormInternalFrame _form) {
    usedFrame_.put(_f, _form);
    removeAvailableFrame(_f);
  }

  @Override
  public void internalFrameClosing(InternalFrameEvent _e) {
    removeAvailableFrame(_e.getInternalFrame());

  }

  @Override
  public void internalFrameDeactivated(InternalFrameEvent _e) {
    if (isUsed(_e.getInternalFrame()) != null) refresh(_e.getInternalFrame());

  }

  public void internalFrameRemoved(JInternalFrame _f, FudaaDjaFormInternalFrame _form) {
    usedFrame_.remove(_f);
    addAvailableFrame(_f);
  }

  public void refreshAll() {
    for (Iterator it = usedFrame_.values().iterator(); it.hasNext();) {
      ((FudaaDjaFormInterface) it.next()).refresh();
    }
    controller_.grid_.repaint();
  }

}
