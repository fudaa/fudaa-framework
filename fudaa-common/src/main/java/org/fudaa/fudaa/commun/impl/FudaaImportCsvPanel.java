/**
 *  @creation     19 d�c. 2003
 *  @modification $Date: 2007-05-04 13:58:05 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuTable;
import com.memoire.bu.BuTextArea;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuVerticalLayout;
import com.memoire.fu.FuLog;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Map;
import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.text.Document;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.dodico.mesure.DodicoCsvReader;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * Permet d'importer une courbe.
 * 
 * @author deniger
 * @version $Id: FudaaImportCsvPanel.java,v 1.3 2007-05-04 13:58:05 deniger Exp $
 */
public class FudaaImportCsvPanel extends CtuluDialogPanel implements DocumentListener {

  final JTextField txtFile_;
  final JTextField txtFmtFixe_;
  final JTextField txtSep_;
  String[][] preview_;
  /** Le type des colonnes, calcul� par d�faut */
  int[] colTypeDef_;
  /** Le type des colonnes, choisies par l'utilisateur (0 : String, 1 : Numerique) */
  int[] colType_;
  final PreviewTableModel model_;
  final BuTextArea txtArea_;
  DodicoCsvReader reader_;
  String sep_;
  final BuComboBox cbSepChoice_;
  final BuRadioButton cbSep_;
  final BuRadioButton cbFixe_;
  final JRadioButton rbNumeric_;
  final JRadioButton rbString_;
    
  public JTextField getFileText() {
    return txtFile_;
  }

  public Map buildOption() {
    Map ret;
    if (cbSep_.isSelected()) {
      ret=DodicoCsvReader.buildOptionSep(getSepString());
    }
    else {
      ret=DodicoCsvReader.buildFixedSize(getFmtFixe());
    }
    ret.put("COL_TYPES", colType_);
    return ret;
  }

  /**
   * @author Fred Deniger
   * @version $Id: FudaaImportCsvPanel.java,v 1.3 2007-05-04 13:58:05 deniger Exp $
   */
  private static class SepModel extends AbstractListModel implements ComboBoxModel {

    final String[] name_;
    final String[] sepLst_;
    Object selected_;

    SepModel() {
      name_ = new String[5];
      sepLst_ = new String[5];
      int idx = 0;
      name_[idx] = FudaaLib.getS("Espace");
      sepLst_[idx++] = CtuluLibString.ESPACE;
      name_[idx] = FudaaLib.getS("Point-virgule");
      sepLst_[idx++] = getPointVirg();
      name_[idx] = FudaaLib.getS("Virgule");
      sepLst_[idx++] = CtuluLibString.VIR;
      name_[idx] = FudaaLib.getS("Tabulation");
      sepLst_[idx++] = "\t";
      name_[idx] = FudaaLib.getS("Autre");
    }

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if (_anItem != selected_) {
        selected_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }
    }

    @Override
    public Object getElementAt(final int _index) {
      return name_[_index];
    }

    @Override
    public int getSize() {
      return name_.length;
    }
  }

  public FudaaImportCsvPanel() {
    this(null, true);
  }

  static String getPointVirg() {
    return ";";
  }

  public FudaaImportCsvPanel(final File _f, final boolean _fileEditable) {
    setLayout(new BuVerticalLayout(2, true, false));
    final String s = FudaaLib.getS("Fichier csv");
    if (_fileEditable) {
      final CtuluFileChooserPanel pn = new CtuluFileChooserPanel(s);
      txtFile_ = pn.getTf();
      add(CtuluFileChooserPanel.buildPanel(pn, s));
    } else {
      final BuPanel pn = new BuPanel(new BuGridLayout(2, 4, 4));
      txtFile_ = new BuTextField(20);
      txtFile_.setEditable(false);
      addLabel(pn, s);
      pn.add(txtFile_);
      add(pn);
    }
    if (_f != null) {
      txtFile_.setText(_f.getAbsolutePath());
      txtFile_.setToolTipText(_f.getAbsolutePath());
    }
    txtFile_.getDocument().addDocumentListener(this);
    final BuPanel sep = new BuPanel(new BuGridLayout(2, 5, 5, true, true));
    sep.setBorder(BorderFactory.createTitledBorder(FudaaLib.getS("Options de s�paration")));
    cbSep_ = new BuRadioButton();
    cbFixe_ = new BuRadioButton();
    cbSep_.setText(FudaaLib.getS("S�par�"));
    cbFixe_.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(final ItemEvent _e) {
        txtFmtFixe_.setEnabled(cbFixe_.isSelected());
      }
    });
    cbFixe_.setText(FudaaLib.getS("Largeur fixe"));
    cbSep_.addItemListener(new ItemListener() {

      @Override
      public void itemStateChanged(final ItemEvent _e) {
        cbSepChoice_.setEnabled(cbSep_.isSelected());
        if (cbSep_.isSelected()) {
          updateSep();
          updatePreview();
        } else {
          updateFmtFixe();
        }

      }
    });
    final ButtonGroup group = new ButtonGroup();
    group.add(cbSep_);
    group.add(cbFixe_);
    sep.add(cbSep_);
    BuPanel pnSepChoix = new BuPanel(new BuGridLayout(2, 5, 5, true, true));
    cbSepChoice_ = new BuComboBox();
    cbSepChoice_.setModel(new SepModel());
    pnSepChoix.add(cbSepChoice_);
    cbSepChoice_.getModel().addListDataListener(new ListDataListener() {

      @Override
      public void contentsChanged(final ListDataEvent _e) {
        updateSep();
      }

      @Override
      public void intervalAdded(final ListDataEvent _e) {}

      @Override
      public void intervalRemoved(final ListDataEvent _e) {}
    });
    txtSep_ = addStringText(pnSepChoix, getPointVirg());
    txtSep_.setEnabled(false);
    txtSep_.getDocument().addDocumentListener(this);
    sep.add(pnSepChoix);
    sep.add(cbFixe_);
    txtFmtFixe_ = addStringText(sep, "10;10");
    txtFmtFixe_.setToolTipText(FudaaLib.getS("Entrer les largeurs des colonnes s�par�es par des ';'"));
    txtFmtFixe_.getDocument().addDocumentListener(this);
    txtFmtFixe_.setEnabled(false);
    add(sep);
    preview_ = new String[0][0];
    model_ = new PreviewTableModel();
    txtArea_ = new BuTextArea(10, 40);
    JScrollPane sp = new JScrollPane(txtArea_, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
        ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    pnSepChoix = new BuPanel();
    pnSepChoix.setBorder(BorderFactory.createTitledBorder(FudaaLib.getS("Contenu du fichier")));
    pnSepChoix.add(sp);
    add(pnSepChoix);
    rbString_=new JRadioButton(FudaaLib.getS("Chaine"));
    rbString_.setEnabled(false);
    rbNumeric_=new JRadioButton(FudaaLib.getS("Num�rique"));
    rbNumeric_.setEnabled(false);
    ButtonGroup bg=new ButtonGroup();
    bg.add(rbString_);
    bg.add(rbNumeric_);
    final BuTable tbPreview=new BuTable(model_);
    tbPreview.setColumnSelectionAllowed(true);
    tbPreview.setRowSelectionAllowed(false);
    tbPreview.getColumnModel().getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        int colSel=tbPreview.getSelectedColumn();
        rbString_.setEnabled(colSel!=-1);
        rbNumeric_.setEnabled(colSel!=-1 && colTypeDef_[colSel]==DodicoCsvReader.COL_TYPE_NUMERIC);
        if (colSel==-1) return;
        
        rbString_.setSelected(colType_[colSel]==DodicoCsvReader.COL_TYPE_STRING);
        rbNumeric_.setSelected(colType_[colSel]==DodicoCsvReader.COL_TYPE_NUMERIC);
      }
    });
    rbString_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        colType_[tbPreview.getSelectedColumn()]=DodicoCsvReader.COL_TYPE_STRING;
      }
    });
    rbNumeric_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        colType_[tbPreview.getSelectedColumn()]=DodicoCsvReader.COL_TYPE_NUMERIC;
      }
    });
    sp = new JScrollPane(tbPreview, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
        ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    sp.setPreferredSize(txtArea_.getPreferredSize());
    pnSepChoix = new BuPanel();
    pnSepChoix.setLayout(new BuVerticalLayout(2));
    pnSepChoix.setBorder(BorderFactory.createTitledBorder(FudaaLib.getS("Pr�visualisation")));
    JPanel pnType=new JPanel();
    pnType.setLayout(new FlowLayout(FlowLayout.LEFT));
    pnType.add(new BuLabel(FudaaLib.getS("Type de la colonne")));
    pnType.add(rbString_);
    pnType.add(rbNumeric_);
    pnSepChoix.add(sp);
    pnSepChoix.add(pnType);
    add(pnSepChoix);
    cbSepChoice_.setSelectedIndex(1);
    cbSep_.setSelected(true);
    if (_f != null && _f.exists()) {
      updateFile();
    }
  }

  public String getSepString() {
    return sep_;
  }

  void updateSep() {
    final SepModel model = (SepModel) cbSepChoice_.getModel();
    String separateur = model.sepLst_[cbSepChoice_.getSelectedIndex()];
    txtSep_.setEnabled(separateur == null);
    if (separateur == null) {
      separateur = txtSep_.getText();
      if (separateur == null) {
        separateur = getPointVirg();
        txtSep_.setText(separateur);
      }
    }
    setSep(separateur);
  }

  class PreviewTableModel extends AbstractTableModel {

    @Override
    public int getColumnCount() {
      return (preview_ != null && preview_.length > 0) ? preview_[0].length : 0;
    }

    @Override
    public int getRowCount() {
      return (preview_ != null && preview_.length > 0) ? preview_.length : 0;
    }

    @Override
    public Object getValueAt(final int _row, final int _col) {
      return preview_[_row][_col];
    }

    @Override
    public String getColumnName(final int _column) {
      final String[] name = reader_.getName();
      return (name == null || _column >= name.length) ? CtuluLibString.EMPTY_STRING : name[_column];
    }

  }

  public File getFile() {
    final String txt = txtFile_.getText();
    if (txt.length() != 0) {
      final File f = new File(txt);
      return f.exists() ? f : null;
    }
    return null;
  }

  private void updateFile() {
    final File f = getFile();
    if (f != null) {
      if (reader_ == null) {
        reader_ = new DodicoCsvReader();
        guessSep();
        if (cbSep_.isSelected()) {
          reader_.setSepChar(sep_);
        } else {
          reader_.setFortranFormat(getFmtFixe());
        }
      } else if (f.equals(reader_.getFile())) {
        return;
      }
      reader_.setFile(f);
      reader_.setNumeric(false);
      reader_.setBlankValid(true);
      preview_ = reader_.getPreview();
      guessColType();
      model_.fireTableStructureChanged();
      updateFileContent();
    }
  }

  /**
   * Tente de deviner le s�parateur de champ.
   * Si aucun ne correspond, c'est que c'est plutot un format fixe.
   */
  private void guessSep() {
    if (reader_ != null && cbSep_.isSelected() && getFile() != null) {
      final SepModel model = (SepModel) cbSepChoice_.getModel();
      final DodicoCsvReader reader = new DodicoCsvReader();
      reader.setFile(getFile());
      reader.setNumeric(false);
      reader.setBlankValid(true);
      for (int i = model.getSize() - 1; i >= 0; i--) {
        if (model.sepLst_[i] != null) {
          reader.setSepChar(model.sepLst_[i]);
          final String[][] preview = reader.getPreview();
          if (preview != null && preview.length > 0 && preview[0].length > 0) {
            cbSepChoice_.setSelectedIndex(i);
            return;
          }
        }
      }
    }

  }
  
  /**
   * Tente de deviner le type des colonnes a partir du preview. On essai Double,
   * si erreur, on conserve String.
   */
  private void guessColType() {
    if (preview_==null) {
      colTypeDef_=null;
      colType_=null;
      return;
    }
    if (preview_.length==0) {
      colTypeDef_=new int[0];
      colType_=new int[0];
      return;
    }
    
    colTypeDef_=new int[preview_[0].length];
    colType_=new int[preview_[0].length];
    for (int i=0; i<preview_[0].length; i++) {
      colTypeDef_[i]=DodicoCsvReader.COL_TYPE_NUMERIC;
      colType_[i]=DodicoCsvReader.COL_TYPE_NUMERIC;
      for (int j=0; j<preview_.length; j++) {
        try {
          Double.parseDouble(preview_[j][i]);
        }
        catch (NumberFormatException _exc) {
          colTypeDef_[i]=DodicoCsvReader.COL_TYPE_STRING;
          colType_[i]=DodicoCsvReader.COL_TYPE_STRING;
          break;
        }
      }
    }
  }

  private void updateFileContent() {
    final File f = getFile();
    if (f == null) {
      txtArea_.setText(CtuluLibString.EMPTY_STRING);
    } else {
      LineNumberReader in = null;
      try {
        in = new LineNumberReader(new FileReader(f));
        String line = in.readLine();
        if (line != null) {
          final StringBuffer r = new StringBuffer(line.length() * 4);
          for (int i = 4; (i > 0) && (line != null); i--) {
            r.append(line).append(CtuluLibString.LINE_SEP);
            line = in.readLine();
          }
          txtArea_.setText(r.toString());
        }

      } catch (final IOException _e) {
        txtArea_.setText(CtuluLibString.EMPTY_STRING);
      } finally {
        if (in != null) {
          try {
            in.close();
          } catch (final IOException _e1) {
            FuLog.error(_e1);
          }
        }
      }
    }
  }

  protected void setSep(final String _s) {
    setErrorText(null);
    if (_s.length() == 0) {
      setErrorText(FudaaLib.getS("pr�ciser un s�parateur"));
      return;
    }
    sep_ = _s;
    if (reader_ != null) {
      reader_.setSepChar(_s);
      updatePreview();

    }

  }

  void updatePreview() {
    if (reader_ == null) {
      return;
    }
    preview_ = reader_.getPreview();
    guessColType();
    model_.fireTableStructureChanged();
  }

  private void updateFromDocument(final Document _d) {
    if (_d == txtFile_.getDocument()) {
      updateFile();
      guessSep();
    } else if (_d == txtSep_.getDocument()) {
      setSep(txtSep_.getText());
    } else if (_d == txtFmtFixe_.getDocument()) {
      updateFmtFixe();
    }
  }

  void updateFmtFixe() {
    final int[] fmt = getFmtFixe();
    if (fmt != null) {
      reader_.setFortranFormat(fmt);
      updatePreview();
    }
  }

  @Override
  public boolean isDataValid() {
    return CtuluLibString.isEmpty(getErrorText() );
  }

  public int[] getFmtFixe() {
    setErrorText(null);
    if (txtFmtFixe_.getText() == null || txtFmtFixe_.getText().trim().length() == 0) {
      setErrorText(FudaaLib.getS("Pr�ciser les tailles des colonnes"));
      return null;
    }
    final String[] fmt = CtuluLibString.parseString(txtFmtFixe_.getText(), getPointVirg());
    final int[] fmtLenght = new int[fmt.length];
    try {
      for (int i = fmt.length - 1; i >= 0; i--) {
        fmtLenght[i] = Integer.parseInt(fmt[i]);
        if (fmtLenght[i] < 0) {
          fmtLenght[i] = 0;
        }
      }
    } catch (final NumberFormatException e) {
      setErrorText(FudaaLib.getS("La saisie pour les tailles des colonnes est invalide"));
      return null;
    }
    return fmtLenght;

  }

  @Override
  public void changedUpdate(final DocumentEvent _e) {
    updateFromDocument(_e.getDocument());
  }

  @Override
  public void insertUpdate(final DocumentEvent _e) {
    updateFromDocument(_e.getDocument());
  }

  @Override
  public void removeUpdate(final DocumentEvent _e) {
    updateFromDocument(_e.getDocument());
  }

  @Override
  public JFileChooser createFileChooser() {
    final JFileChooser f = super.createFileChooser();
    f.setFileFilter(new BuFileFilter(new String[] { "txt", "csv" }, FudaaLib.getS("Fichier csv")));
    return f;

  }

  public DodicoCsvReader getReader() {
    return reader_;
  }

  public void setReader(final DodicoCsvReader _reader) {
    reader_ = _reader;
    if (reader_ != null) {
      if (cbSep_.isSelected()) {
        reader_.setSepChar(sep_);
      } else {
        reader_.setFortranFormat(getFmtFixe());
      }
      reader_.setFile(getFile());
      if (cbSep_.isSelected()) {
        updateSep();
      } else {
        updateFmtFixe();
      }
    }
  }

}