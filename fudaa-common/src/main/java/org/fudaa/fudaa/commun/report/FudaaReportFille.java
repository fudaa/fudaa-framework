/*
 * @creation 6 f�vr. 07
 * @modification $Date: 2007-03-30 15:37:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuList;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.dja.DjaFrame;
import com.memoire.dja.DjaResource;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.gui.CtuluFilleWithComponent;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionPaletteAbstract;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.impression.EbliPageFormat;
import org.fudaa.ebli.impression.EbliPageable;
import org.fudaa.ebli.impression.EbliPageableDelegate;
import org.fudaa.fudaa.commun.FudaaLib;
import org.jdesktop.swingx.ScrollPaneSelector;

/**
 * @author fred deniger
 * @version $Id: FudaaReportFille.java,v 1.7 2007-03-30 15:37:08 deniger Exp $
 */
public class FudaaReportFille extends DjaFrame implements CtuluImageProducer, CtuluFilleWithComponent, EbliPageable{

  FudaaReportFrameController controller_;

  JComponent[] spec_;

  public FudaaReportFille(final BuCommonImplementation _app, FudaaReportFrameController _controller) {
    super(_app, FudaaLib.getS("Rapport"), _controller.grid_);
    controller_ = _controller;
    ScrollPaneSelector.installScrollPaneSelector(super.scrollPane_);
    super.scrollPane_.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    super.scrollPane_.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
    super.scrollPane_.setAutoscrolls(true);
    controller_.grid_.setPane(scrollPane_);

  }

  @Override
  public JComponent createPanelComponent() {
    return FudaaDjaBuilder.createDjaTable(getFudaaGrid());
  }

  @Override
  public Class getComponentClass() {
    return BuList.class;
  }

  @Override
  public String getComponentTitle() {
    return FudaaLib.getS("Composants");
  }

  @Override
  public void majComponent(Object _o) {
    FudaaDjaBuilder.buildComposantTable((CtuluTable) _o, controller_.getDjaFormListener());
  }

  FudaaDjaGrid getFudaaGrid() {
    return (FudaaDjaGrid) grid_;
  }

  public BuCommonImplementation getApp() {
    return app_;
  }

  @Override
  public Dimension getDefaultImageDimension() {
    return getFudaaGrid().getDefaultImageDimension();
  }

  @Override
  public final String[] getEnabledActions() {
    return new String[] { "IMPRIMER", "MISEENPAGE", "PREVISUALISER", "COUPER", "COPIER", "COLLER", "DUPLIQUER",
        "TOUTSELECTIONNER", CtuluLibImage.SNAPSHOT_COMMAND, };
  }

  protected ActionListener createActionForList(final BuList _l) {
    return new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent _e) {
        controller_.addIf(_l);

      }

    };
  }

  List actions_;

  public final void buildActions() {
    if (actions_ != null) return;
    actions_ = new ArrayList(10);
    final EbliActionPaletteAbstract actAddFrame = new EbliActionPaletteAbstract(FudaaLib.getS("Fenetre"), BuResource.BU
        .getToolIcon("ajouter"), "ADDFRAME") {

      @Override
      public JComponent buildContentPane() {
        BuPanel pn = new BuPanel(new BuBorderLayout());
        BuList l = new BuList(controller_.getFrameListModel());
        l.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        l.setCellRenderer(FudaaLib.createInternalFrameCellRenderer());
        pn.add(new BuScrollPane(l));
        final ActionListener action = createActionForList(l);
        l.addMouseListener(new MouseAdapter() {

          @Override
          public void mouseClicked(MouseEvent _e) {
            if (_e.getClickCount() == 2) {
              action.actionPerformed(null);
            }

          }

        });
        BuButton bt = new BuButton(BuResource.BU.getString("Ajouter"));
        bt.addActionListener(action);
        pn.add(bt, BuBorderLayout.SOUTH);

        return pn;
      }

    };

    actions_.add(new EbliActionSimple(BuResource.BU.getString("rafraichir"), BuResource.BU.getToolIcon("rafraichir"),
        "REFRESH") {
      @Override
      public void actionPerformed(ActionEvent _e) {
        controller_.refreshAll();
      }
    });

    actions_.add(null);

    EbliActionSimple actRemove = new EbliActionSimple(CtuluLib.getS("Supprimer les objets s�lectionner"),
        CtuluResource.CTULU.getIcon("detruire"), "DELETE") {
      @Override
      public void actionPerformed(ActionEvent _e) {
        controller_.grid_.removeSelected();
      }
    };
    actRemove.setKey(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));
    actions_.add(actRemove);

    actions_.add(null);

    actions_.add(controller_.getAnimAction());
    BuLib.invokeLater(new Runnable() {
      @Override
      public void run() {
        actAddFrame.changeAll();
      }
    });
    actions_.add(new FudaaReportConfigureAction(controller_));
    actions_.add(null);
    FudaaDjaAlignAction.addActionToList((FudaaDjaGrid) grid_, actions_);
    actions_.add(null);
    actions_.add(actAddFrame);
    EbliActionPaletteAbstract palette = new EbliActionPaletteAbstract("FORME", DjaResource.DJA.getIcon("dja-box", 16),
        "FORMS") {
      @Override
      public JComponent buildContentPane() {
        return new FudaaDjaPaletteForm(getFudaaGrid());
      }

    };
    actions_.add(palette);
    EbliLib.updateMapKeyStroke(this, actions_);
  }

  @Override
  public JComponent[] getSpecificTools() {
    if (spec_ == null) {
      List res = new ArrayList(/* Arrays.asList(super.getSpecificTools()) */);
      buildActions();
      EbliLib.updateToolButtons(actions_, super.app_.getMainPanel().getDesktop());

      for (int i = 0; i < actions_.size(); i++) {

        EbliActionInterface action = (EbliActionInterface) actions_.get(i);
        if (action == null) res.add(null);
        else
          res.add((action).buildToolButton(EbliComponentFactory.INSTANCE));
      }
      spec_ = (JComponent[]) res.toArray(new JComponent[res.size()]);
    }
    return spec_;
  }

  BuMenu menu_;

  @Override
  public JMenu[] getSpecificMenus() {
    if (menu_ == null) {
      menu_ = new BuMenu();
      buildActions();
      for (int i = 0; i < actions_.size(); i++) {
        EbliActionInterface aci = (EbliActionInterface) actions_.get(i);
        if (aci == null) menu_.addSeparator();
        else
          menu_.add(aci.buildMenuItem(EbliComponentFactory.INSTANCE));
      }
    }
    menu_.setText(getTitle());
    return new JMenu[] { menu_ };
  }

  @Override
  public BufferedImage produceImage(final int _w, final int _h, Map _params) {
    final boolean isInteractive = getFudaaGrid().isInteractive();
    if (isInteractive) {
      getFudaaGrid().setInteractive(false);
    }
    final BufferedImage im = getFudaaGrid().produceImage(_w, _h, _params);
    if (isInteractive) {
      getFudaaGrid().setInteractive(true);
    }
    return im;
  }

  @Override
  public BufferedImage produceImage(Map _params) {
    final boolean isInteractive = getFudaaGrid().isInteractive();
    if (isInteractive) {
      getFudaaGrid().setInteractive(false);
    }
    final BufferedImage im = getFudaaGrid().produceImage(_params);
    if (isInteractive) {
      getFudaaGrid().setInteractive(true);
    }
    return im;
  }
 private EbliPageableDelegate delegueImpression_=new EbliPageableDelegate(this);
 private BuInformationsSoftware is_;
 private BuInformationsDocument id_;
    @Override
    public int print(Graphics _g, PageFormat _format, int _page) {
        if(controller_ != null) {
            BufferedImage image = controller_.grid_.produceImage((int)_format.getWidth(),(int) _format.getHeight(), null);
            _g.drawImage(image, 0,0,null);
        }
       return Printable.PAGE_EXISTS;
    }

    @Override
    public BuInformationsSoftware getInformationsSoftware() {
    if (is_ == null) {
      is_= new BuInformationsSoftware();
    }
    return is_;
  }
  @Override
  public BuInformationsDocument getInformationsDocument() {
    if (id_ == null) {
      id_= new BuInformationsDocument();
    }
    return id_;
  }

    @Override
  public EbliPageFormat getDefaultEbliPageFormat() {
    if (delegueImpression_ == null) {
      delegueImpression_= new EbliPageableDelegate(this);
    }
    return delegueImpression_.getDefaultEbliPageFormat();
  }

    @Override
    public int getNumberOfPages() {
        return 1;
    }

        @Override
  public PageFormat getPageFormat(final int _i) {
    if (delegueImpression_ == null) {
      delegueImpression_= new EbliPageableDelegate(this);
    }
    return delegueImpression_.getPageFormat(_i);
  }
@Override
  public Printable getPrintable(final int _i) {
    if (delegueImpression_ == null) {
      delegueImpression_= new EbliPageableDelegate(this);
    }
    return delegueImpression_.getPrintable(_i);
  }

}
