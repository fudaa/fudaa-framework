/*
 * @creation 13 f�vr. 07
 * @modification $Date: 2007-03-19 13:27:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;
import com.memoire.dja.DjaGrid;
import com.memoire.dja.DjaObject;
import com.memoire.dja.DjaText;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluHtmlEditorPanel;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurButton;
import org.fudaa.ebli.controle.BSelecteurCheckBox;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.controle.BSelecteurTextField;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author fred deniger
 * @version $Id: FudaaReportTextConfigure.java,v 1.3 2007-03-19 13:27:20 deniger Exp $
 */
public class FudaaReportTextConfigure extends FudaaDjaConfigureAbstract implements ActionListener {

  public FudaaReportTextConfigure(DjaObject _target) {
    super(_target.getTextAt(0), EbliLib.getS("Texte"));
    if (target_ == null) {
      _target.addText("<html><body><p></p></body></html>");
      _target.getTextAt(0).setVisible(true);
      target_ = _target.getTextAt(0);
    }
  }

  @Override
  public void addPropertyChangeListener(String _key, PropertyChangeListener _l) {
    getTxt().addPropertyChangeListener(_key, _l);
  }

  @Override
  public void removePropertyChangeListener(String _key, PropertyChangeListener _l) {
    getTxt().removePropertyChangeListener(_key, _l);
  }

  public FudaaReportTextConfigure(FudaaDjaText _target) {
    super(_target, EbliLib.getS("Texte"));
  }

  public static void afficheHtmlDialog(DjaGrid _parent, DjaText _txt) {
    String txt = _txt.getText();
    String newTxt = CtuluHtmlEditorPanel.showMinimalHtmlDialog(txt, _parent, true);
    if (newTxt != null && !newTxt.equals(txt)) {
      _txt.setText(newTxt);
      _parent.repaint();
    }

  }

  protected void afficheHtmlDialog() {
    afficheHtmlDialog(target_.getGrid(), getTxt());
  }

  protected FudaaDjaText getTxt() {
    return (FudaaDjaText) target_;
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    afficheHtmlDialog();
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    final BSelecteurButton bt = new BSelecteurButton("HTML", EbliLib.getS("Editer"));
    bt.setAddListenerToTarget(false);
    bt.getBt().addActionListener(this);
    bt.setTitle(EbliLib.getS("Texte"));
    BuValueValidator value = BuValueValidator.MULTI(BuValueValidator.INTEGER, BuValueValidator.MIN(1));
    BuTextField tfWidth = BuTextField.createIntegerField();
    tfWidth.setValueValidator(value);
    BuTextField tfHeight = BuTextField.createIntegerField();
    tfHeight.setValueValidator(value);
    BSelecteurCheckBox cb=new BSelecteurCheckBox(FudaaDjaText.TEXT_AJUST);
    cb.setTitle(FudaaLib.getS("Ajuster automatiquement la taille"));
    return new BSelecteurInterface[] { new BSelecteurCheckBox(BSelecteurCheckBox.PROP_VISIBLE),cb,
        new BSelecteurTextField(FudaaDjaText.TEXT_WIDTH, EbliLib.getS("Largeur"), tfWidth),
        new BSelecteurTextField(FudaaDjaText.TEXT_HEIGHT, EbliLib.getS("Hauteur"), tfHeight), bt };
  }

  @Override
  public Object getProperty(String _key) {
    if (_key == BSelecteurCheckBox.PROP_VISIBLE) {
      DjaText txt = getTxt();
      return Boolean.valueOf(txt != null && txt.isVisible());
    } else if (_key.equals(FudaaDjaText.TEXT_AJUST)) {
      FudaaDjaText txt = getTxt();
      return txt == null ? Boolean.TRUE : Boolean.valueOf(txt.isTextAjust());
    } else if (_key.equals(FudaaDjaText.TEXT_PROPERTY)) {
      DjaText txt = getTxt();
      return txt == null ? CtuluLibString.EMPTY_STRING : txt.getText();
    } else if (_key.equals(FudaaDjaText.TEXT_WIDTH)) {
      return new Integer(getTxt().getW());
    } else if (_key.equals(FudaaDjaText.TEXT_HEIGHT)) {
      return new Integer(getTxt().getH());
    }

    return null;
  }

  @Override
  public boolean setProperty(String _key, Object _newProp) {
    if (_key == BSelecteurCheckBox.PROP_VISIBLE) {
      boolean visible = ((Boolean) _newProp).booleanValue();
      DjaText txt = getTxt();
      if (txt != null) txt.setVisible(visible);
      repaintGrid();
      return true;

    } else if (_key.equals(FudaaDjaText.TEXT_AJUST)) {
      getTxt().setTextAjust(((Boolean) _newProp).booleanValue());
      repaintGrid();
    } else if (_key.equals(FudaaDjaText.TEXT_WIDTH)) {
      getTxt().setW(((Integer) _newProp).intValue());
      repaintGrid();
    } else if (_key.equals(FudaaDjaText.TEXT_HEIGHT)) {
      getTxt().setH(((Integer) _newProp).intValue());
      repaintGrid();
    }

    return false;
  }
}
