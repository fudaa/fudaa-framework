/*
 * @creation 6 f�vr. 07
 * @modification $Date: 2007-06-28 09:28:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuList;
import com.memoire.bu.BuResource;
import com.memoire.dja.DjaVector;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.awt.Dimension;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JInternalFrame;
import javax.swing.ListModel;
import javax.swing.WindowConstants;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.animation.EbliAnimationAction;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author fred deniger
 * @version $Id: FudaaReportFrameController.java,v 1.6 2007-06-28 09:28:20 deniger Exp $
 */
public class FudaaReportFrameController {

  EbliAnimationAction act_;
  private EbliAnimationAction animAction_;

  final FudaaDjaGrid grid_;

  final FudaaReportFille owner_;

  final List usedAnimatedFrame_;

  final FudaaReportIfManager ifListener_;
  final FudaaDjaFormListener djaFormListener_;

  public FudaaReportFrameController(BuCommonImplementation _impl) {
    super();

    JInternalFrame[] frs = _impl.getAllInternalFrames();
    JInternalFrame activated = _impl.getCurrentInternalFrame();
    final List availableFrame = new ArrayList(frs.length);
    for (int i = 0; i < frs.length; i++) {
      if (frs[i] instanceof CtuluImageProducer) {
        availableFrame.add(frs[i]);
      } else if (frs[i] == activated) {
        activated = null;
      }
    }
    grid_ = new FudaaDjaGrid(true, null);

    owner_ = new FudaaReportFille(_impl, this);
    owner_.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    // pour stopper l'animation si l'utilisateur ferme la fenetre
    owner_.addInternalFrameListener(new InternalFrameAdapter() {
      @Override
      public void internalFrameClosing(InternalFrameEvent _e) {
        closeFrame();
      }
    });
    usedAnimatedFrame_ = new ArrayList();

    // le listener sur la grille
    djaFormListener_ = new FudaaDjaFormListener(this);

    // le listener qui permet de lister les Internal Frames de l'applications qui sont visible dans le rapport ou non.
    ifListener_ = new FudaaReportIfManager(this, availableFrame);
    _impl.addIfListener(ifListener_);

    Dimension frameDim = new Dimension(600, 500);
    if (activated != null) {
      frameDim = ((CtuluImageProducer) activated).getDefaultImageDimension();
      int initMarge = 100;
      frameDim.width = frameDim.width + initMarge;
      frameDim.height = frameDim.height + initMarge;

    }
    owner_.setPreferredSize(frameDim);
    owner_.setSize(frameDim);
    _impl.addInternalFrame(owner_);

  }

  protected void closeFrame() {
    if (owner_.getDefaultCloseOperation() == WindowConstants.DO_NOTHING_ON_CLOSE) {
      if (animAction_ != null && animAction_.getAnim().isPlaying()) {
        String yesText = BuResource.BU.getString("Fermer");
        String noText = FudaaLib.getS("Ne pas fermer cette fen�tre");
        boolean ok = CtuluLibDialog.showConfirmation(getFille(), FudaaLib.getS("Une animation est en cours"), FudaaLib
            .getS("Voulez-vous annuler cett animation et fermer cette fen�tre ?"), yesText, noText);
        if (!ok) return;
        stopAnimation();

      }
      owner_.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
      try {
        owner_.setClosed(true);
      } catch (PropertyVetoException _evt) {
        FuLog.error(_evt);
      }
    }

  }

  protected void stopAnimation() {
    if (Fu.DEBUG && FuLog.isDebug()) FuLog.debug("FRE: animation stopped");
    if (animAction_ != null) {
      animAction_.getAnim().animStop();
    }

  }

  protected EbliAnimationAction getAnimAction() {
    if (animAction_ == null) animAction_ = new EbliAnimationAction(new FudaaReportAnimationAdapter(this));
    return animAction_;
  }

  protected void clearAll() {
    DjaVector vect = grid_.getObjects();
    for (int i = 0; i < vect.size(); i++) {
      if (vect.elementAt(i) instanceof FudaaDjaFormInterface) {
        ((FudaaDjaFormInterface) vect.elementAt(i)).unactive(this);
      }
    }
  }

  public void objectRemoved(FudaaDjaFormInterface _form) {
    if (_form.isAnimated()) removeAnimatedDjaForm(_form);
  }

  public void objectAdded(FudaaDjaFormInterface _form) {
    if (_form.isAnimated()) addAnimatedDjaForm(_form);
  }

  protected void addIf(BuList _l) {
    int[] idx = _l.getSelectedIndices();
    if (!CtuluLibArray.isEmpty(idx)) {
      for (int i = 0; i < idx.length; i++) {
        addIf((JInternalFrame) ifListener_.availableFrame_.get(idx[i]));
      }
    }
  }

  protected void addIf(JInternalFrame _f) {
    FudaaDjaFormInternalFrame djaForm = new FudaaDjaFormInternalFrame((CtuluImageProducer) _f, grid_);

    grid_.add(djaForm);
    grid_.setSelection(djaForm);
  }

  protected void addAnimatedDjaForm(FudaaDjaFormInterface _djaForm) {
    usedAnimatedFrame_.add(_djaForm);
    updateAnim();
  }

  ListModel getFrameListModel() {
    return ifListener_.list_;
  }

  protected void updateAnim() {
    if (animAction_ != null) {
      animAction_.setEnabled(usedAnimatedFrame_.size() > 0);
    }
  }

  protected void removeAnimatedDjaForm(FudaaDjaFormInterface _object) {
    usedAnimatedFrame_.remove(_object);
    updateAnim();
  }

  public void refreshAll() {
    ifListener_.refreshAll();
  }

  public FudaaDjaFormListener getDjaFormListener() {
    return djaFormListener_;
  }

  public FudaaReportFille getFille() {
    return owner_;
  }

}
