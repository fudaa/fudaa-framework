/*
 *  @file         FudaaEditorPanel.java
 *  @creation     2 juin 2003
 *  @modification $Date: 2006-09-19 15:01:54 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.exec;

import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuRadioButton;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.ButtonGroup;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author deniger
 * @version $Id: FudaaEditorPanel.java,v 1.15 2006-09-19 15:01:54 deniger Exp $
 */
public class FudaaEditorPanel extends CtuluDialogPanel {
  BuRadioButton cbInterne_;
  BuRadioButton cbExterne_;
  BuComboBox choixExterne_;

  public FudaaEditorPanel() {
    setLayout(new BuGridLayout(2, 5, 5));
    cbExterne_ = new BuRadioButton(FudaaLib.getS("Editeur externe"));
    cbExterne_.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(final ItemEvent _ie) {
        choixExterne_.setEnabled(_ie.getStateChange() == ItemEvent.SELECTED);
      }
    });
    add(cbExterne_);
    choixExterne_ = new BuComboBox(FudaaEditor.getInstance().getEditorExternes());
    choixExterne_.setEnabled(false);
    choixExterne_.setRenderer(new CtuluCellTextRenderer() {
      @Override
      public void setValue(final Object _o) {
        if (_o != null) {
          setText(((FudaaEditorExterne) _o).getName());
        }
      }
    });
    add(choixExterne_);
    cbInterne_ = new BuRadioButton(FudaaLib.getS("Editeur interne"));
    add(cbInterne_);
    final ButtonGroup gr = new ButtonGroup();
    gr.add(cbExterne_);
    gr.add(cbInterne_);
    loadFromPref();
  }

  private void loadFromPref() {
    if (FudaaEditor.getInstance().isInterneEditorDefault()) {
      cbInterne_.setSelected(true);
    } else {
      cbExterne_.setSelected(true);
      choixExterne_.setSelectedItem(FudaaEditor.getInstance().getDefaultEditor());
    }
  }

  @Override
  public boolean apply() {
    if (cbInterne_.isSelected()) {
      FudaaEditor.getInstance().setInterneEditorAsDefault();
    } else {
      FudaaEditor.getInstance().setDefaultEditor((FudaaEditorExterne) choixExterne_.getSelectedItem());
    }
    return true;
  }

}
