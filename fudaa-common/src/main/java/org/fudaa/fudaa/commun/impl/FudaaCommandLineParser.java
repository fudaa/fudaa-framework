/**
 * @file         FudaaCommandLineParser.java
 * @creation     1999-07-16
 * @modification $Date: 2007-01-19 13:14:07 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.impl;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import org.fudaa.ctulu.CtuluLibString;


/**
 * Le parseur d'arguments en ligne de commande pour Fudaa.
 * 
 * @version $Revision: 1.1 $ $Date: 2007-01-19 13:14:07 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public class FudaaCommandLineParser {

  /**
   * Si true, l'utilisateur ne veut pas de fichier de log.
   */
  public boolean noLog_;
  /**
   * Si true, pas de splash screen.
   */
  public boolean noSplash_;
  /**
   * si true, l'utilisateur veut la version du logiciel.
   */
  public boolean version_;

  /**
   * Test la version de java.
   */
  public FudaaCommandLineParser() {
    // Verif version JVM
    if (System.getProperty("java.version").compareTo("1.2") < 0) {
      System.err.println("JVM >= 1.2 needed");
      final Dialog dlg = new Dialog(new Frame(), "Error", true);
      dlg.addWindowListener(new WindowAdapter() {
        @Override
        public void windowClosing(final WindowEvent _e) {
          System.exit(-1);
          // B.M. 27/08/99 Supprim� pour cause d'exception disgracieuse � l'�cran
          // ((Dialog)e.getSource()).dispose();
        }
      });
      dlg.setLayout(new BorderLayout());
      dlg.add(new Label("JVM >= 1.2 needed"), BorderLayout.CENTER);
      final Button okBut = new Button("OK");
      okBut.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(final ActionEvent _e) {
          System.exit(-1);
          // B.M. 27/08/99 Supprim� pour cause d'exception disgracieuse � l'�cran
          // ((Dialog)((Button)e.getSource()).getParent()).dispose();
        }
      });
      dlg.add(okBut, BorderLayout.SOUTH);
      dlg.pack();
      dlg.setVisible(true);
    }

  }

  /**
   * @return les flag autoris�s
   */
  public String[] flagTexts() {
    return new String[] { "--no_terminal", "--no_log", "--no_corba", "--corba", "--no_splash", "--version" };
  }

  /**
   * @return les flag sur une seule ligne
   */
  public String flagTotalText() {
    
    final String[] t = flagTexts();
    if (t == null) {
      return CtuluLibString.EMPTY_STRING;
    }
    final StringBuilder r = new StringBuilder(t.length * 4);
    boolean p = true;
    for (int i = 0; i < t.length; i++) {
      if (!p) {
        r.append(", ");
      } else {
        p = false;
      }
      r.append(t[i]);
    }
    return r.toString();
  }

  /**
   * Gere --no_terminal,--no_corba,--no_splash,--corba et --version. Si --no_corba ou --corba, les champs statiques de
   * la classe FudaaDodicoSelectionServeur sont modifi�s.
   * 
   * @param _args les arguments a parser
   * @return les arguments non reconnus
   */
  public String[] parse(final String[] _args) {
    boolean noCorba = false;
    boolean corba = false;
    final String[] tmpArgs = new String[(_args == null ? 0 : _args.length)];
    int passed = 0;
    if(_args!=null) {
      for (int i = 0; i < tmpArgs.length; i++) {
        if ("--no_terminal".equals(_args[i]) || ("--no_log".equals(_args[i]))) {
          noLog_ = true;
        } else if ("--no_corba".equals(_args[i])) {
          noCorba = true;
        } else if ("--corba".equals(_args[i])) {
          corba = true;
        } else if ("--no_splash".equals(_args[i])) {
          noSplash_ = true;
        } else if ("--version".equals(_args[i])) {
          version_ = true;
        } else if ((_args[i] == null) || (_args[i].trim().equals(""))) {
          ; // ignorer
        } else {
          tmpArgs[passed++] = _args[i];
        }
      }
    }
//    if (corba) {
//      FudaaDodicoSelectionServeur.setForceCorba();
//    } else if (noCorba) {
//      FudaaDodicoSelectionServeur.setForceNoCorba();
//    }
    if (passed == 0) {
      return new String[0];
    }
    final String[] res = new String[passed];
    System.arraycopy(tmpArgs, 0, res, 0, passed);
    /*
     * int r=0; for(int i=0;i<args.length;i++) { if( tmpArgs[i]!=null ) res[r++]=tmpArgs[i]; }
     */
    return res;
  }
}
