/*
 * @creation     2001-09-01
 * @modification $Date: 2007-01-19 13:14:24 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun;
import com.memoire.bu.BuPreferences;

/**
 * Classe de gestion des astuces.
 *
 * @version      $Revision: 1.11 $ $Date: 2007-01-19 13:14:24 $ by $Author: deniger $
 * @author       Fred Deniger
 */
public class FudaaAstuces extends FudaaAstucesAbstract{
  /**
   * Singleton de FudaaAstuces.
   */
  public static final FudaaAstuces FUDAA= new FudaaAstuces();
  /**
   * @see org.fudaa.fudaa.commun.FudaaAstucesAbstract#getParent()
   */
  @Override
  protected FudaaAstucesAbstract getParent() {
    return null;
  }
  @Override
  protected BuPreferences getPrefs() {
    return FudaaPreferences.FUDAA;
  }
}
