/**
 * @modification $Date: 2007-05-04 13:58:05 $
 * @statut       unstable
 * @file         DjaPaletteForm.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package org.fudaa.fudaa.commun.report;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuIcon;
import com.memoire.bu.BuPanel;
import com.memoire.dja.DjaResource;
import gnu.trove.TObjectIntHashMap;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.border.EmptyBorder;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.fudaa.commun.FudaaLib;

public class FudaaDjaPaletteForm extends BuPanel implements ActionListener {
  BuButton[] buttons_;
  BuGridLayout layout_;
  TObjectIntHashMap idx_ = new TObjectIntHashMap();

  FudaaDjaFormAbstract[] names_ = { new FudaaDjaBox(), new FudaaDjaDiamond(), new FudaaDjaSquare(),
      new FudaaDjaCercle(), new FudaaDjaEllipse(), new FudaaDjaHLine(), new FudaaDjaVLine() };
  final FudaaDjaGrid grid_;

  public FudaaDjaPaletteForm(FudaaDjaGrid _grid) {
    super();
    grid_ = _grid;

    layout_ = new BuGridLayout(4, 2, 2);
    layout_.setCfilled(false);
    setLayout(layout_);
    setBorder(new EmptyBorder(2, 2, 2, 2));

    buttons_ = new BuButton[names_.length];

    for (int i = 0; i < buttons_.length; i++) {
      BuIcon icon = DjaResource.DJA.getIcon("dja-" + names_[i].getDjaName().toLowerCase(), 16);

      buttons_[i] = new BuButton();
      buttons_[i].setIcon(icon);
      buttons_[i].setMargin(new Insets(1, 1, 1, 1));
      buttons_[i].setRequestFocusEnabled(false);
      buttons_[i].setToolTipText("<html><body>" + FudaaLib.getS("Ajouter:") + "<br>" + names_[i].getDescription());
      buttons_[i].setActionCommand("DJA_CREATE_FORM(" + names_[i] + ')');
      buttons_[i].putClientProperty("DJA_TARGET", names_[i]);
      add(buttons_[i]);
      buttons_[i].addActionListener(this);
    }
  }

  @Override
  public void actionPerformed(ActionEvent _e) {
    BuButton bt = (BuButton) _e.getSource();
    FudaaDjaFormAbstract dja = (FudaaDjaFormAbstract) bt.getClientProperty("DJA_TARGET");
    FudaaDjaFormAbstract createNew = dja.createNew();
    String djaName = createNew.getDjaName();
    int idx = idx_.get(djaName) + 1;
    createNew.setTitle(createNew.getDescription() + CtuluLibString.getEspaceString(idx));

    idx_.put(djaName, idx);
    grid_.add(createNew);
    grid_.clearSelection();
    createNew.setSelected(true);
    grid_.repaint();

  }

}
