/*
 * @creation     2000-05-15
 * @modification $Date: 2006-09-19 15:10:23 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.trace2d;
import com.memoire.bu.BuGridLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.text.NumberFormat;
import java.util.Locale;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
/**
 * Une palette de couleurs associ�es � des plages de valeurs. Les valeurs hors
 * plages sont consid�r�es comme "autres".
 *
 * @version $Id: BPaletteCouleurPlage.java,v 1.4 2006-09-19 15:10:23 deniger Exp $
 * @author Bertrand Marchand
 */
public class BPaletteCouleurPlage extends JPanel {
  /**
   * Un classe repr�sentant une plage.
   */
  class Plage {
    Color couleur= null; // Couleur de la plage
    double min; // Minimum des valeurs de la plage
    double max; // Maximum des valeurs de la plage
    Symbole symbole= null; // Symbole pour la plage
    int taille= 15; // Taille du symbole pour la plage.
    String legende= null; // L�gende pour la plage
  }
  Plage[] plages_; // Plages
  Plage autres_; // D�finition pour les valeurs hors plages
  private String titre_= null; // Titre de la l�gende
  private String ssTitre_= null; // sous titre de l�gende
  private boolean autresVisible_= true;
  // Les valeurs hors plages sont visibles
  private Symbole defaultSymbol_= new Symbole(Symbole.CARRE_PLEIN, 0);
  // Le symbole par d�faut pour les plages
  private JLabel lbTitre= new JLabel();
  private JPanel pnPlages= new JPanel();
  private JLabel[] lbPlages_;
  private JLabel lbAutres_= new JLabel();
  private Box bxTitres;
  private JLabel lbSSTitre= new JLabel();
  /**
   * Cr�ation d'une palette vide.
   */
  public BPaletteCouleurPlage() {
    try {
      jbInit();
    } catch (Exception e) {
      e.printStackTrace();
    }
    autres_= new Plage();
    lbAutres_= new JLabel();
    lbAutres_.setFont(new java.awt.Font("Dialog", 0, 10));
    setSymboleAutres(defaultSymbol_);
    setCouleurAutres(Color.lightGray);
    setLegendeAutres("Autres");
    setAutresVisible(false);
    Plage pg= new Plage();
    pg.couleur= Color.blue;
    pg.min= 0;
    pg.max= 1;
    plages_= new Plage[] { pg };
    setNbPlages(1);
    setTitre("Titre");
    setSousTitre("Sous titre");
  }
  private void jbInit() throws Exception {
    // Graphiques
    BorderLayout lyThis= new BorderLayout();
    BuGridLayout lyPlages= new BuGridLayout();
    Border bdPlages;
    bdPlages= BorderFactory.createEmptyBorder(5, 5, 5, 5);
    bxTitres= Box.createVerticalBox();
    setLayout(lyThis);
    lbTitre.setHorizontalAlignment(SwingConstants.CENTER);
    lbTitre.setFont(new java.awt.Font("Dialog", 0, 10));
    lbTitre.setAlignmentX((float)0.5);
    lbTitre.setText(titre_);
    pnPlages.setLayout(lyPlages);
    lyPlages.setColumns(1);
    lyPlages.setHgap(5);
    pnPlages.setBorder(bdPlages);
    pnPlages.setOpaque(false);
    lbSSTitre.setHorizontalAlignment(SwingConstants.CENTER);
    lbSSTitre.setFont(new java.awt.Font("Dialog", 0, 10));
    lbSSTitre.setAlignmentX((float)0.5);
    lbSSTitre.setText(ssTitre_);
    this.setOpaque(false);
    add(pnPlages, BorderLayout.CENTER);
    this.add(bxTitres, BorderLayout.NORTH);
    bxTitres.add(lbTitre, null);
    bxTitres.add(lbSSTitre, null);
  }
  /**
   * Clone de la palette.
   */
  @Override
  public Object clone() {
    BPaletteCouleurPlage r= new BPaletteCouleurPlage();
    // Plage "Autres"
    r.setCouleurAutres(autres_.couleur);
    r.setLegendeAutres(autres_.legende);
    r.setSymboleAutres(autres_.symbole);
    r.setTailleAutres(autres_.taille);
    r.setAutresVisible(isAutresVisible());
    r.setSymboleDefaut(defaultSymbol_);
    // Plages
    r.setNbPlages(getNbPlages());
    for (int i= 0; i < getNbPlages(); i++) {
      r.setCouleurPlage(i, plages_[i].couleur);
      r.setLegendePlage(i, plages_[i].legende);
      r.setSymbolePlage(i, plages_[i].symbole);
      r.setTaillePlage(i, plages_[i].taille);
      r.setMinPlage(i, plages_[i].min);
      r.setMaxPlage(i, plages_[i].max);
    }
    // Titres
    r.setTitre(getTitre());
    r.setSousTitre(getSousTitre());
    return r;
  }
  /**
   * D�finition du nombre de plages.
   * <p>
   * Les plages sont red�finies automatiquement entre les 2 bornes min. et max.,
   * de mani�re r�guli�re. Les couleurs sont red�finies, les symboles et les
   * tailles, les l�gendes �galement.
   *
   * @param _nb Le nombre de plages, sup�rieur ou �gal � 1. Si inf�rieur,
   *            palette par d�faut.
   */
  public void setNbPlages(int _nb) {
    _nb= Math.max(_nb, 1);
    // Cr�ation des composants correspondants aux plages
    pnPlages.removeAll();
    lbPlages_= new JLabel[_nb];
    for (int i= 0; i < _nb; i++) {
      lbPlages_[i]= new JLabel();
      lbPlages_[i].setFont(new java.awt.Font("Dialog", 0, 10));
      setSymbolePlage(i, defaultSymbol_);
      pnPlages.add(lbPlages_[i]);
    }
    // Ajout des composant autres
    pnPlages.add(lbAutres_);
    double min= getMinPalette();
    double max= getMaxPalette();
    // Plages
    plages_= new Plage[_nb];
    for (int i= 0; i < _nb; i++) {
      plages_[i]= new Plage();
      setSymbolePlage(i, defaultSymbol_);
    }
    setMinPalette(min);
    setMaxPalette(max);
    //    // Hors plages
    //    setCouleurAutres(autres_.couleur);
    //    setSymboleAutres(autres_.symbole);
    //    setLegendeAutres(autres_.legende);
    //    setAutresVisible(autresVisible_);
    ajustePlages();
    ajusteLegendes();
    propagerCouleurs(Color.blue, Color.red);
  }
  /**
   * Propager les couleurs.
   * Les couleurs sont interpol�es d'une couleur vers une autre pour toutes les
   * plages de la palette.
   * <p>
   * La couleur <i>_cmin</i> correspond � la plage 0, la couleur <i>_cmax</i>
   * � la plage getNbPlages()-1.
   *
   * @param _cmin Couleur pour la plage 0
   * @param _cmax Couleur pour la plage getNbPlage()-1
   */
  public void propagerCouleurs(Color _cmin, Color _cmax) {
    int nb= getNbPlages();
    for (int i= 0; i < nb; i++) {
      if (nb == 1)
        setCouleurPlage(i, getCouleur(_cmin, _cmax, 0));
      else
        setCouleurPlage(
          i,
          getCouleur(_cmin, _cmax, (double)i / (double) (nb - 1)));
    }
  }
  /**
   * Propager les tailles de symboles.
   * Les tailles sont interpol�es d'une taille vers une autre pour toutes les
   * plages de la palette.
   * <p>
   * La taille <i>_tmin</i> correspond � la plage 0, la taille <i>_tmax</i>
   * � la plage getNbPlages()-1.
   *
   * @param _tmin Taille pour la plage 0
   * @param _tmax Taille pour la plage getNbPlage()-1
   */
  public void propagerTailles(int _tmin, int _tmax) {
    int nb= getNbPlages();
    for (int i= 0; i < nb; i++) {
      setTaillePlage(
        i,
        _tmin + (int) (((double)i / (double) (nb - 1)) * (_tmax - _tmin)));
    }
  }
  /**
   * D�finit si les valeurs "autres" sont rendues visibles.
   *
   * @param _visible <i>true</i> Les valeurs "autres" sont rendues visibles.
   */
  public void setAutresVisible(boolean _visible) {
    if (autresVisible_ != _visible) {
      autresVisible_= _visible;
      lbAutres_.setVisible(_visible);
      firePropertyChange("palette", null, this);
    }
  }
  /**
   * Retourne si les valeurs "autres" sont visibles.
   *
   * @return <i>true</i> Les valeurs hors plages sont visibles.
   */
  public boolean isAutresVisible() {
    return autresVisible_;
  }
  /**
   * Retourne le nombre de plages de la palette.
   *
   * @return Le nombre de plages
   */
  public int getNbPlages() {
    return plages_.length;
  }
  /**
   * D�finition de la borne min. de la palette.
   * <p>
   * Les bornes des plages ne sont pas red�finies automatiquement.
   * Le r�ajustement peut �tre effectu� par ajustePlages().
   *
   * @see #ajustePlages()
   */
  public void setMinPalette(double _min) {
    if (plages_[0].min != _min) {
      plages_[0].min= _min;
      firePropertyChange("palette", null, this);
    }
  }
  /**
   * Retourne la borne min. de la palette.
   *
   * @return La borne min.
   */
  public double getMinPalette() {
    return plages_[0].min;
  }
  /**
   * D�finition de la borne max. de la palette.
   * <p>
   * Les bornes des plages ne sont pas red�finies automatiquement.
   * Le r�ajustement peut �tre effectu� par ajustePlages().
   *
   * @see #ajustePlages()
   */
  public void setMaxPalette(double _max) {
    if (plages_[plages_.length - 1].max != _max) {
      plages_[plages_.length - 1].max= _max;
      firePropertyChange("palette", null, this);
    }
  }
  /**
   * Retourne la borne max. de la palette.
   *
   * @return La borne max.
   */
  public double getMaxPalette() {
    return plages_[plages_.length - 1].max;
  }
  /**
   * R�ajustement des bornes des plages de mani�re r�guli�re.
   * Pour r�ajuster les l�gendes aux plages ainsi red�finies, appeler
   * ajusteLegendes().
   *
   * @see #ajusteLegendes()
   */
  public void ajustePlages() {
    double min= getMinPalette();
    double max= getMaxPalette();
    int nb= getNbPlages();
    if (max <= min)
      max= min;
    for (int i= 0; i < nb; i++) {
      plages_[i].min= min + i * (max - min) / nb;
      plages_[i].max= min + (i + 1) * (max - min) / nb;
    }
    firePropertyChange("palette", null, this);
  }
  /**
   * R�ajustement des l�gendes des plages. La l�gende d'une plage est d�finie
   * de la mani�re suivante :
   * <pre>
   *   legende=plage.min+" � "+plage.max;
   * </pre>
   */
  public void ajusteLegendes() {
    NumberFormat nf= NumberFormat.getInstance(Locale.US);
    nf.setMaximumFractionDigits(3);
    nf.setMinimumFractionDigits(3);
    String lg;
    for (int i= 0; i < plages_.length; i++) {
      if (plages_[i].min == Double.POSITIVE_INFINITY)
        lg= "" + "+Infini";
      else if (plages_[i].min == Double.NEGATIVE_INFINITY)
        lg= "" + "-Infini";
      else
        lg= "" + nf.format(plages_[i].min);
      // Rajout du deuxieme intervalle si la valeur du min<>valeur du max
      if (plages_[i].min != plages_[i].max) {
        lg += " � ";
        if (plages_[i].max == Double.POSITIVE_INFINITY)
          lg += "" + "+Infini";
        else if (plages_[i].max == Double.NEGATIVE_INFINITY)
          lg += "" + "-Infini";
        else
          lg += "" + nf.format(plages_[i].max);
      }
      setLegendePlage(i, lg);
    }
  }
  /**
   * D�finition du titre de la palette.
   *
   * @param _titre Le titre.
   */
  public void setTitre(String _titre) {
    if (_titre == null)
      return;
    if (!_titre.equals(titre_)) {
      titre_= _titre;
      lbTitre.setText(titre_);
      firePropertyChange("palette", null, this);
    }
  }
  /**
   * Retourne le titre de la palette.
   *
   * @return Le titre.
   */
  public String getTitre() {
    return titre_;
  }
  /**
   * D�finition du sous titre de la palette.
   *
   * @param _ssTitre Le sous titre.
   */
  public void setSousTitre(String _ssTitre) {
    if (_ssTitre == null)
      return;
    if (!_ssTitre.equals(ssTitre_)) {
      ssTitre_= _ssTitre;
      lbSSTitre.setText(ssTitre_);
      firePropertyChange("palette", null, this);
    }
  }
  /**
   * Retourne le sous titre de la palette.
   *
   * @return Le sous titre.
   */
  public String getSousTitre() {
    return ssTitre_;
  }
  /**
   * D�finition d'une couleur pour une plage.
   * Si l'indice de plage est hors de la palette, aucune couleur n'est d�finie.
   *
   * @param _coul Couleur de la plage.
   * @param _ind  Index de la plage dans la palette.
   */
  public void setCouleurPlage(int _ind, Color _coul) {
    if (_ind < 0 || _ind >= getNbPlages() || _coul == null)
      return;
    if (!_coul.equals(plages_[_ind].couleur)) {
      plages_[_ind].couleur= _coul;
      ((SymboleIcon)lbPlages_[_ind].getIcon()).setCouleur(_coul);
      firePropertyChange("palette", null, this);
    }
  }
  /**
   * Retourne la couleur pour une plage.
   *
   * @param _ind Index de la plage dans la palette.
   * @return La couleur. Si l'indice de plage est hors de la palette,
   *         la couleur est <i>null</i>.
   */
  public Color getCouleurPlage(int _ind) {
    if (_ind < 0 || _ind >= getNbPlages())
      return null;
    return plages_[_ind].couleur;
  }
  /**
   * D�finition de la couleur pour les valeurs "autres".
   *
   * @param _coul Couleur pour les valeurs "autres".
   */
  public void setCouleurAutres(Color _coul) {
    if (_coul == null)
      return;
    if (!_coul.equals(autres_.couleur)) {
      autres_.couleur= _coul;
      ((SymboleIcon)lbAutres_.getIcon()).setCouleur(_coul);
      firePropertyChange("palette", null, this);
    }
  }
  /**
   * Retourne la couleur pour les valeurs "autres".
   *
   * @return La couleur.
   */
  public Color getCouleurAutres() {
    return autres_.couleur;
  }
  /**
   * D�finition d'un symbole pour une plage.
   * Si l'indice de plage est hors de la palette, aucun symbole n'est d�fini.
   *
   * @param _symb Symbole de la plage.
   * @param _ind  Index de la plage dans la palette.
   */
  public void setSymbolePlage(int _ind, Symbole _symb) {
    if (_ind < 0 || _ind >= getNbPlages() || _symb == null)
      return;
    if (!_symb.equals(plages_[_ind].symbole)) {
      plages_[_ind].symbole= _symb;
      lbPlages_[_ind].setIcon(
        new SymboleIcon(_symb, plages_[_ind].couleur, 15));
      firePropertyChange("palette", null, this);
    }
  }
  /**
   * Retourne le symbole pour une plage.
   *
   * @param _ind Index de la plage dans la palette.
   * @return Le symbole. Si l'indice de plage est hors de la palette,
   *         le symbole est <i>null</i>.
   */
  public Symbole getSymbolePlage(int _ind) {
    if (_ind < 0 || _ind >= getNbPlages())
      return null;
    return plages_[_ind].symbole;
  }
  /**
   * D�finition du symbole pour les valeurs "autres".
   *
   * @param _symb Symbole pour les valeurs "autres".
   */
  public void setSymboleAutres(Symbole _symb) {
    if (_symb == null)
      return;
    if (!_symb.equals(autres_.symbole)) {
      autres_.symbole= _symb;
      lbAutres_.setIcon(new SymboleIcon(_symb, autres_.couleur, 15));
      firePropertyChange("palette", null, this);
    }
  }
  /**
   * Retourne le symbole pour les valeurs "autres".
   *
   * @return Le symbole.
   */
  public Symbole getSymboleAutres() {
    return autres_.symbole;
  }
  /**
   * D�finition du symbole par d�faut pour les plages.
   * @param _s Symbole par d�faut.
   */
  public void setSymboleDefaut(Symbole _s) {
    defaultSymbol_= _s;
  }
  /**
   * Retourne le symbole par d�faut.
   *
   * @return Le symbole.
   */
  public Symbole getSymboleDefaut() {
    return defaultSymbol_;
  }
  /**
   * D�finition de la taille du symbole pour une plage.
   * Si l'indice de plage est hors de la palette, aucune taille n'est d�finie.
   *
   * @param _ind    Index de la plage dans la palette.
   * @param _taille Taille du symbole.
   */
  public void setTaillePlage(int _ind, int _taille) {
    if (_ind < 0 || _ind >= getNbPlages())
      return;
    if (_taille != plages_[_ind].taille) {
      plages_[_ind].taille= _taille;
      firePropertyChange("palette", null, this);
    }
  }
  /**
   * Retourne la taille du symbole pour une plage.
   * @return La taille. Si l'indice de plage est hors de la palette,
   *         la taille est <i>0</i>.
   *
   */
  public int getTaillePlage(int _ind) {
    if (_ind < 0 || _ind >= getNbPlages())
      return 0;
    return plages_[_ind].taille;
  }
  /**
   * D�finition de la taille du symbole pour les valeurs "autres".
   *
   * @param _taille Taille du symbole.
   */
  public void setTailleAutres(int _taille) {
    if (_taille != autres_.taille) {
      autres_.taille= _taille;
      firePropertyChange("palette", null, this);
    }
  }
  /**
   * Retourne la taille du symbole pour les valeurs "autres".
   *
   * @return La taille.
   */
  public int getTailleAutres() {
    return autres_.taille;
  }
  /**
   * D�finition d'une l�gende pour une plage.
   * Si l'indice de plage est hors de la palette, aucune l�gende n'est d�finie.
   *
   * @param _text L�gende de la plage.
   * @param _ind  Index de la plage dans la palette.
   */
  public void setLegendePlage(int _ind, String _text) {
    if (_ind < 0 || _ind >= getNbPlages() || _text == null)
      return;
    if (!_text.equals(plages_[_ind].legende)) {
      plages_[_ind].legende= _text;
      lbPlages_[_ind].setText(_text);
      firePropertyChange("palette", null, this);
    }
  }
  /**
   * Retourne la l�gende pour une plage.
   *
   * @param _ind Index de la plage dans la palette.
   * @return La l�gende. Si l'indice de plage est hors de la palette,
   *         la l�gende est <i>null</i>.
   */
  public String getLegendePlage(int _ind) {
    if (_ind < 0 || _ind >= getNbPlages())
      return null;
    return plages_[_ind].legende;
  }
  /**
   * D�finition de la l�gende pour les valeurs "autres".
   *
   * @param _text L�gende pour les valeurs "autres".
   */
  public void setLegendeAutres(String _text) {
    if (_text == null)
      return;
    if (!_text.equals(autres_.legende)) {
      autres_.legende= _text;
      lbAutres_.setText(_text);
      firePropertyChange("palette", null, this);
    }
  }
  /**
   * Retourne la l�gende pour les valeurs "autres".
   *
   * @return La l�gende.
   */
  public String getLegendeAutres() {
    return autres_.legende;
  }
  /**
   * D�finition du min. pour une plage.
   * Si l'indice de plage est hors de la palette, aucun min. n'est d�fini.
   *
   * @param _min  Borne min. pour la plage.
   * @param _ind  Index de la plage dans la palette.
   */
  public void setMinPlage(int _ind, double _min) {
    if (_ind < 0 || _ind >= getNbPlages())
      return;
    if (plages_[_ind].min != _min) {
      plages_[_ind].min= _min;
      firePropertyChange("palette", null, this);
    }
  }
  /**
   * Retourne le min. pour une plage.
   *
   * @param _ind Index de la plage dans la palette.
   * @return La borne min. pour la plage.
   */
  public double getMinPlage(int _ind) {
    //    if (_ind<0 || _ind>=getNbPlages()) return null;
    return plages_[_ind].min;
  }
  /**
   * D�finition du max. pour une plage.
   * Si l'indice de plage est hors de la palette, aucun max. n'est d�fini.
   *
   * @param _max  Borne max. pour la plage.
   * @param _ind  Index de la plage dans la palette.
   */
  public void setMaxPlage(int _ind, double _max) {
    if (_ind < 0 || _ind >= getNbPlages())
      return;
    if (plages_[_ind].max != _max) {
      plages_[_ind].max= _max;
      firePropertyChange("palette", null, this);
    }
  }
  /**
   * Retourne le max. pour une plage.
   *
   * @param _ind Index de la plage dans la palette.
   * @return La borne max. pour la plage.
   */
  public double getMaxPlage(int _ind) {
    //    if (_ind<0 || _ind>=getNbPlages()) return null;
    return plages_[_ind].max;
  }
  /**
   * Retourne la couleur associ�e � une valeur.
   * <p>
   * Si la valeur est en dehors des plages, la couleur retourn�e est ou bien
   * la couleur "autres" si les valeurs "autres" doivent �tre repr�sent�es,
   * ou bien <i>null</i> si les valeurs "autres" ne doivent pas �tre
   * repr�sent�es.
   *
   * @return La couleur associ�e � la valeur.
   */
  public Color couleur(double _val) {
    for (int i= 0; i < getNbPlages(); i++)
      if (_val >= plages_[i].min && _val <= plages_[i].max)
        return plages_[i].couleur;
    return isAutresVisible() ? autres_.couleur : null;
  }
  /**
   * Retourne le symbole associ� � une valeur.
   * <p>
   * Si la valeur est en dehors des plages, le symbole retourn� est ou bien
   * le symbole "autres" si les valeurs "autres" doivent �tre repr�sent�es,
   * ou bien <i>null</i> si les valeurs "autres" ne doivent pas �tre
   * repr�sent�es.
   *
   * @return Le symbole associ� � la valeur.
   */
  public Symbole symbole(double _val) {
    for (int i= 0; i < getNbPlages(); i++)
      if (_val >= plages_[i].min && _val <= plages_[i].max)
        return plages_[i].symbole;
    return autres_.symbole;
  }
  /**
   * Retourne la taille de symbole associ�e � une valeur.
   * <p>
   * Si la valeur est en dehors des plages, la taille de symbole retourn�e est
   * ou bien la taille de symbole "autres" si les valeurs "autres" doivent �tre
   * repr�sent�es, ou bien <i>0</i> si les valeurs "autres" ne doivent pas �tre
   * repr�sent�es.
   *
   * @return La taille associ�e � la valeur.
   */
  public int taille(double _val) {
    for (int i= 0; i < getNbPlages(); i++)
      if (_val >= plages_[i].min && _val <= plages_[i].max)
        return plages_[i].taille;
    return autres_.taille;
  }
  private static float[] tdl(Color _c) {
    return Color.RGBtoHSB(_c.getRed(), _c.getGreen(), _c.getBlue(), null);
  }
  /**
   * Retourne une couleur interpol�e dans une palette d�finie par 2 couleurs
   * extr�mes.
   */
  private static Color getCouleur(
    Color _couleurMin,
    Color _couleurMax,
    double z) {
    Color c= Color.black;
    if (z < 0.)
      z= 0.;
    if (z >= 1.)
      z= 0.99999999;
    z -= Math.floor(z);
    float t, d, l;
    int n, a, amin, amax;
    float[] tdlmin, tdlmax;
    tdlmin= tdl(_couleurMin);
    tdlmax= tdl(_couleurMax);
    amin= _couleurMin.getAlpha();
    amax= _couleurMax.getAlpha();
    n= (int) (z * 255.);
    a= amin + (amax - amin) * n / 255;
    t= tdlmin[0] + (tdlmax[0] - tdlmin[0]) * (float)z;
    d= tdlmin[1] + (tdlmax[1] - tdlmin[1]) * (float)z;
    l= tdlmin[2] + (tdlmax[2] - tdlmin[2]) * (float)z;
    int v= Color.getHSBColor(t, d, l).getRGB();
    c= new Color((v & 0x00FFFFFF) | (a << 24), true);
    return c;
  }
  /**
   * Pour test.
   */
  public static void main(String[] args) {
    //    try {
    //      UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
    //    } catch (Exception _exc) {}
    JFrame f= new JFrame("Palette");
    BPaletteCouleurPlage plt= new BPaletteCouleurPlage();
    //    plt.setMinPalette(0);
    //    plt.setMaxPalette(1.e12);
    plt.setNbPlages(7);
    //    for (int i=0; i<plt.getNbPlages(); i++) {
    //      nf.setMaximumFractionDigits(2);
    //      System.out.println(nf.format(plt.getMinPlage(i)));
    //    }
    plt.setTitre("");
    plt.setSousTitre("");
    plt.setCouleurAutres(Color.black);
    //    plt.setAutresVisible(true);
    //    plt.setLegendeAutres("Autres plages");
    plt.setBackground(Color.white);
    plt.setOpaque(true);
    f.getContentPane().add(plt, BorderLayout.CENTER);
    f.pack();
    f.setVisible(true);
  }
}
