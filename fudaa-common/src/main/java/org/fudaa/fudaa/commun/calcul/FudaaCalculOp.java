/**
 *  @creation     13 juin 2003
 *  @modification $Date: 2007-02-15 17:10:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.calcul;

import com.memoire.fu.FuLog;
import java.io.File;
import java.io.PrintStream;
import java.util.Timer;
import java.util.TimerTask;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.dodico.calcul.CalculExec;
import org.fudaa.dodico.calcul.CalculLauncherAbstract;
import org.fudaa.dodico.objet.CExecListener;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaUI;

/**
 * @author deniger
 * @version $Id: FudaaCalculOp.java,v 1.19 2007-02-15 17:10:51 deniger Exp $
 */
public abstract class FudaaCalculOp extends CalculLauncherAbstract implements CExecListener {
  protected FudaaUI ui_;
  protected CalculExec exe_;
  boolean isFinished_;
  boolean stop_;
  Process p_;

  /**
   * @param _exe l'exe a lancer
   */
  public FudaaCalculOp(final CalculExec _exe) {
    exe_ = _exe;
  }

  /**
   * @return le nom du process
   */
  protected String getTaskName() {
    return FudaaLib.getS("Calcul") + CtuluLibString.ESPACE + exe_.getExecTitle();
  }

  /**
   * @param _impl l'impl parent
   */
  public void setUi(final FudaaUI _impl) {
    ui_ = _impl;
  }

  @Override
  public void execute() {
    final CtuluTaskDelegate task = ui_.createTask(getTaskName());
    task.start(new Runnable() {

      @Override
      public void run() {
        isFinished_ = false;
        internCalcul(task.getStateReceiver());
        isFinished_ = true;
        fireFinishedEvent();

      }

    });
  }

  /**
   * Methode appele pour sauvegarder le fichier du calcul.
   * 
   * @param _inter la barre de progression
   * @return le fichier parametres
   */
  public abstract File proceedParamFile(ProgressionInterface _inter);

  protected void setEtat(final String _msg, final int _p) {
    if (ui_ != null) {
      ui_.setMainMessage(_msg);
      ui_.setMainProgression(_p);
    }
  }

  protected void initEtat() {
    setEtat(CtuluLibString.EMPTY_STRING, 0);
  }

  protected void internCalcul(final ProgressionInterface _progress) {
    setEtat(FudaaLib.getS("Enregistrement des param�tres"), 0);
    if (stop_) {
      initEtat();
      return;
    }
    final File f = proceedParamFile(_progress);
    setEtat(FudaaLib.getS("Calcul") + "...", 10);
    if (stop_) {
      initEtat();
      return;
    }
    exe_.setUI(ui_);
    exe_.setProgression(_progress);
    try {
      exe_.launch(f, this);
    } catch (final RuntimeException _e) {
      FuLog.warning(_e);
    }

    actionToDoJustAfterLaunching();
    setEtat(FudaaLib.getS("Le calcul est termin�:")
        + (f == null ? CtuluLibString.EMPTY_STRING : (CtuluLibString.LINE_SEP + f.getAbsolutePath())), 100);
    new Timer().schedule(new TimerTask() {
      @Override
      public void run() {
        initEtat();
      }
    }, 2 * 1000);
    isFinished_ = true;
  }

  public void actionToDoJustAfterLaunching() {}

  @Override
  public void stop() {
    stop_ = true;
    if (p_ != null) {
      p_.destroy();
    }
  }

  /**
   * Methode appeler par CExec afin de stocker le processus avec le blocage du thread (p.waitfor()). Cela permet de
   * stopper le process si necessaire.
   * 
   * @param _p
   */
  @Override
  public void setProcess(final Process _p) {
    p_ = _p;
  }

  @Override
  public boolean isFinished() {
    return isFinished_;
  }

  @Override
  public void setErr(final PrintStream _err) {
    exe_.setOutError(_err);
  }

  @Override
  public void setStd(final PrintStream _std) {
    exe_.setOutStandard(_std);
  }
}
