/*
 * @creation 6 f�vr. 07
 * @modification $Date: 2007-03-27 16:11:06 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.dja.DjaGridInteractive;
import com.memoire.dja.DjaKeyListener;
import com.memoire.dja.DjaMouseListener;
import com.memoire.dja.DjaObject;
import com.memoire.dja.DjaVector;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Enumeration;
import java.util.Map;
import javax.swing.JScrollPane;
import org.fudaa.ctulu.CtuluLibImage;
import org.fudaa.ctulu.image.CtuluImageProducer;

class FudaaDjaGrid extends DjaGridInteractive implements CtuluImageProducer {

  JScrollPane pane_;

  public FudaaDjaGrid() {
    super();
  }

  @Override
  public DjaVector getSelection() {
    final DjaVector r =  new DjaVector();
    for (Enumeration e = getObjects().elements(); e.hasMoreElements();) {
      DjaObject o = (DjaObject) e.nextElement();
      if (o.isSelected()) {
        r.addElement(o);
      }
    }
    return r;
  }

  @Override
  protected DjaKeyListener createInteractiveKeyListener() {
    return new FudaaDjaKeyListener(this);
  }

  public void removeSelected() {
    DjaVector s = getSelection();
    clearSelection();
    for (Enumeration e = s.elements(); e.hasMoreElements();) {
      DjaObject o = (DjaObject) e.nextElement();
      removeDependencies(o);
      remove(o);
    }
    repaint();
  }

  @Override
  public void clearSelection() {
    if (!isInteractive()) return; // new DjaVector(0);
    for (Enumeration e = getObjects().elements(); e.hasMoreElements();) {
      DjaObject o = (DjaObject) e.nextElement();
      if (o.isSelected()) o.setSelected(false);
    }
  }

  @Override
  public void add(DjaObject _o, boolean _quiet) {
    if (pane_ != null) {
      Rectangle rec = pane_.getViewport().getVisibleRect();
      Point p = pane_.getViewport().getViewPosition();
      int x = p.x + (rec.width - _o.getWidth()) / 2;
      int y = p.y + (rec.height - _o.getHeight()) / 2;
      _o.setX(Math.max(x, p.x));
      _o.setY(Math.max(y, p.y));
    }
    super.add(_o, _quiet);
  }

  public FudaaDjaGrid(boolean _interactive, DjaVector _objects) {
    super(_interactive, _objects);
  }

  public FudaaDjaGrid(boolean _interactive) {
    super(_interactive);
  }

  @Override
  protected DjaMouseListener createInteractiveMouseListener() {
    return new FudaaDjaMouseListener(this);
  }

  @Override
  public Dimension getDefaultImageDimension() {
    return getSize();
  }

  @Override
  public BufferedImage produceImage(Map _params) {
    return CtuluLibImage.produceImageForComponent(this, _params);
  }

  @Override
  public BufferedImage produceImage(int _w, int _h, Map _params) {
    return CtuluLibImage.produceImageForComponent(this, _w, _h, _params);
  }

  public JScrollPane getPane() {
    return pane_;
  }

  public void setPane(JScrollPane _pane) {
    pane_ = _pane;
  }
}