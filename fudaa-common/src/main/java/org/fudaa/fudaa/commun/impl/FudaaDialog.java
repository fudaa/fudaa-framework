/*
 * @file         FudaaDialog.java
 * @creation     1999-07-01
 * @modification $Date: 2007-01-19 13:14:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.impl;
import com.memoire.bu.BuHorizontalLayout;
import com.memoire.bu.BuResource;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Iterator;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
/**
 * Une fen�tre de dialogue g�n�rique avec panel de boutons action (OK, CANCEL,
 * APPLY en fonction de l'option). Les actions sont g�r�es en dehors de la boite
 * dans le listener. La source g�n�rant l'�v�nement est un des boutons
 * OK_BUTTON, CANCEL_BUTTON, APPLY_BUTTON.
 *
 * @version      $Revision: 1.1 $ $Date: 2007-01-19 13:14:06 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class FudaaDialog extends JDialog {
  public static final int OK_CANCEL_OPTION= 0;
  public static final int OK_APPLY_OPTION= 1;
  public static final int OK_CANCEL_APPLY_OPTION= 2;
  public JButton btOk_= new JButton();
  public JButton btCancel_= new JButton();
  public JButton btApply_= new JButton();
  protected JPanel pnAffichage_= new JPanel();
  protected int option_;
  private final JPanel pnAction_= new JPanel();
  private final BuHorizontalLayout lyAction_= new BuHorizontalLayout();
  private final JPanel pnAction1_= new JPanel();
  private final BorderLayout lyThis_= new BorderLayout();
  private final BorderLayout lyAffichage_= new BorderLayout();
//  private Frame parent_= null;
  private final HashSet listeners_= new HashSet();
  /**
   * Cr�ation d'une fen�tre dialogue avec centrage sur l'�cran.
   */
  public FudaaDialog() {
    this(null);
  }
  /**
   * Cr�ation d'une fen�tre dialogue avec centrage sur le Frame _parent.
   */
  public FudaaDialog(final Frame _parent) {
    this(_parent, OK_CANCEL_OPTION);
  }
  /**
   * Cr�ation d'une fen�tre dialogue avec centrage sur le Frame _parent et
   * affectation des boutons d'action (_option).
   */
  public FudaaDialog(final Frame _parent, final int _option) {
    super(_parent, true);
    // Pour recentrage de la fen�tre � l'�cran
  //  parent_= _parent;
    btOk_.setText(BuResource.BU.getString("Continuer"));
    btOk_.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        btOkActionPerformed(_e);
      }
    });
    btCancel_.setText(BuResource.BU.getString("Annuler"));
    btCancel_.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        btCancelActionPerformed(_e);
      }
    });
    btApply_.setText(BuResource.BU.getString("Appliquer"));
    btApply_.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _e) {
        btApplyActionPerformed(_e);
      }
    });
    pnAffichage_.setLayout(lyAffichage_);
    pnAffichage_.setBorder(new EmptyBorder(5, 5, 5, 5));
    lyAction_.setHgap(5);
    lyAction_.setVfilled(false);
    pnAction_.setLayout(lyAction_);
    pnAction1_.add(pnAction_, null);
    this.getContentPane().setLayout(lyThis_);
    this.getContentPane().add(pnAction1_, BorderLayout.SOUTH);
    this.getContentPane().add(pnAffichage_, BorderLayout.CENTER);
    setOption(_option);
  }
  /**
   * D�finition des buttons actions � afficher (OK, CANCEL, APPLIQUER).
   */
  public final void setOption(final int _option) {
    option_= _option;
    pnAction_.removeAll();
    switch (_option) {
      case OK_CANCEL_OPTION :
        pnAction_.add(btOk_, null);
        pnAction_.add(btCancel_, null);
        break;
      case OK_APPLY_OPTION :
        pnAction_.add(btOk_, null);
        pnAction_.add(btApply_, null);
        break;
      case OK_CANCEL_APPLY_OPTION :
        pnAction_.add(btOk_, null);
        pnAction_.add(btCancel_, null);
        pnAction_.add(btApply_, null);
        break;
      default:
        // TODO ajouter des instructions au cas "default"
    }
  }
  public int getOption() {
    return option_;
  }
  /**
   * Affichage de la boite de dialogue avec un recentrage.
   */
  @Override
  public void show() {
    // Calcul de la position de la fenetre
    Dimension dParent;
    Point pParent;
    final Dimension dThis= this.getPreferredSize();
    final Point pThis= new Point();
    /* BM 19/02/2003 : la fenetre se d�cale vers la gauche a chaque affichage ???
       La taille du parent change a chaque affichage.
       => On ne tient plus compte du parent, on centre sur l'�cran.

        if (parent_ == null) {
          dParent = Toolkit.getDefaultToolkit().getScreenSize();
          pParent = new Point(0,0);
        }
        else {
         dParent = parent_.getPreferredSize();
         pParent = parent_.getLocation();
        }
    */
    dParent= Toolkit.getDefaultToolkit().getScreenSize();
    pParent= new Point(0, 0);
    pThis.x= pParent.x + (dParent.width - dThis.width) / 2;
    pThis.y= pParent.y + (dParent.height - dThis.height) / 2;
    this.setLocation(pThis);
    super.show();
  }
  /**
   * Ajout d'un listener aux boutons action du dialog (OK, ANNULER, APPLIQUER). Le
   * bouton press� est obtenu par getSource().
   */
  public synchronized void addActionListener(final ActionListener _listener) {
    listeners_.add(_listener);
  }
  /**
   * Suppression d'un listener aux boutons action du dialog (OK, ANNULER, APPLIQUER).
   */
  public synchronized void removeActionListener(final ActionListener _listener) {
    listeners_.remove(_listener);
  }
  /**
   * Notification aux auditeurs qu'un �venement <I>ActionEvent</I> s'est produit.
   */
  public synchronized void fireActionPerformed(final ActionEvent _evt) {
    for (final Iterator i= listeners_.iterator(); i.hasNext();) {
      ((ActionListener)i.next()).actionPerformed(_evt);
    }
  }
  /**
   * Bouton "Ok" press�, effacage du dialog, traitement dans le listener du dialog.
   */
  protected void btOkActionPerformed(final ActionEvent _evt) {
    fireActionPerformed(_evt);
    dispose();
  }
  /**
   * Bouton "Appliquer" press�, traitement dans le listener du dialog.
   */
  protected void btApplyActionPerformed(final ActionEvent _evt) {
    fireActionPerformed(_evt);
  }
  /**
   * Bouton "annuler" press�, effacage du dialog, traitement dans le listener du dialog.
   */
  protected void btCancelActionPerformed(final ActionEvent _evt) {
    fireActionPerformed(_evt);
    dispose();
  }
  /*  public static void main(String[] argv) {
      RefluxDialogNormale d = new RefluxDialogNormale();
      d.affiche(new Double(10.));
      System.exit(0);
    } */
}
