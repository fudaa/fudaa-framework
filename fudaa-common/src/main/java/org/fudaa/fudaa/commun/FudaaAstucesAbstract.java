/**
 * @file         FudaaAstucesAbsrtact.java
 * @creation     2004-05-04
 * @modification $Date: 2007-05-04 13:58:09 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun;

import com.memoire.bu.BuPreferences;
import com.memoire.fu.FuLib;
import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import org.fudaa.ctulu.CtuluLibString;

/**
 * Classe de gestion des astuces.
 * 
 * @version $Revision: 1.3 $ $Date: 2007-05-04 13:58:09 $ by $Author: deniger $
 * @author Fred Deniger
 */
public abstract class FudaaAstucesAbstract {

  /**
   * Le nom du fichier d'astuces.
   */
  protected final static String NOM_FICHIER = "astuces.txt";
  /**
   * La variable utilisee dans les fichiers de ressources des applications.
   */
  protected final static String NOM_VARIABLE = "astuce.nombre";

  /**
   * Choisit au hasard un entier strictement positif inferieur a _nombreMax. Si nombreMax_ est inferieur ou egal a 1, 1
   * est renvoye.
   * 
   * @param _nombreMax
   * @return entier strictement positif inferieur a _nombreMax
   */
  public static int hasard(final int _nombreMax) {
    // choisi dans l'intervalle [1,_nombreMax]
    if (_nombreMax < 2) {
      return 1;
    }
    return (int) (Math.round(Math.random() * (_nombreMax - 1))) + 1;
  }

  /**
   * Retourne l'astuce positionne a la place i.
   * 
   * @param _i
   * @return astuce
   */
  public String getAstuce(final int _i) {
    return lecture(_i);
  }

  /**
   * Retourne l'astuce suivante.
   * 
   * @param _i
   * @return La valeur AstuceRecursif
   */
  public String getAstuceRecursif(final int _i) {
    if ((_i < 1) || (_i > getNombreLigneTotal())) {
      return CtuluLibString.LINE_SEP + FudaaLib.getS("Pas d'astuces trouv�es");
    }
    int nombreInf = 0;
    if (getParent() != null) {
      nombreInf = getParent().getNombreLigneTotal();
    }
    if (_i > nombreInf) {
      return getAstuce(_i - nombreInf);
    }
    if (getParent() != null) {
      return getParent().getAstuceRecursif(_i);
    }
    return CtuluLibString.LINE_SEP + FudaaLib.getS("Pas d'astuces trouv�es");
  }

  /**
   * Calcule le nombre d'astuces total, choisi un nombre au hasard dans [0,nombreTotalAstuces] et retourne l'astuce
   * correspondante.
   * 
   * @return astuce au hasard.
   */
  public String getAstuceHasard() {
    return getAstuceRecursif(hasard(getNombreLigneTotal()));
  }

  /**
   * Retourne le nombre d'astuces.
   * 
   * @return La valeur NombreLigne
   */
  public int getNombreLigne() {
    String temp = (getPrefs().getStringProperty(NOM_VARIABLE)).trim();
    if ((temp == null) || (temp.length() == 0) || ("0".equals(temp))) {
      lecture(0);
      temp = getPrefs().getStringProperty(NOM_VARIABLE);
    }
    return Integer.parseInt(temp);
  }

  /**
   * Retourne la somme de tous les nombres de lignes des astuces parents.
   * 
   * @return La valeur NombreLigneTotal
   */
  public int getNombreLigneTotal() {
    int nbLigne = getNombreLigne();
    if (getParent() != null) {
      nbLigne += getParent().getNombreLigneTotal();
    }
    return nbLigne;
  }

  protected abstract FudaaAstucesAbstract getParent();

  /**
   * Les preferences utilisees a ce niveau.
   * 
   * @return FudaaPreferences.FUDAA
   */
  protected abstract BuPreferences getPrefs();

  /**
   * Lit la ligne i dans le fichier d'astuces. En plus, Mise a jour de la valeur de NOM_VARIABLE dans les Preferences
   * (PREF()).
   * 
   * @param _i
   * @return Astuce a la place i.
   */
  protected String lecture(final int _i) {
    String retour = CtuluLibString.LINE_SEP + FudaaLib.getS("Astuce non trouv�e");
    int nombreLigneLu = 0;
    LineNumberReader lu = null;
    try {
      lu = new LineNumberReader(new InputStreamReader(new BufferedInputStream(getClass().getResourceAsStream(
          NOM_FICHIER))));
      while (lu.ready()) {
        final String ligneLu = lu.readLine();
        if ((ligneLu == null) || (ligneLu.length() == 0)) {
          break;
        }
        if (++nombreLigneLu == _i) {
          retour = ligneLu;
        }
      }
      getPrefs().putIntegerProperty(NOM_VARIABLE, nombreLigneLu);
      getPrefs().writeIniFile();
      
    } catch (final Exception _exception) {
    } finally {
      FuLib.safeClose(lu);
    }
    return retour;
  }
  /**
   * Main
   * 
   * @param argv
   */
  /*
   * public static void main(String argv[]) { for(int i = 0; i < 20; i++) { System.out.println("num "+i);
   * System.out.println(hasard(i)); } }
   */
}
