/*
 *  @file         FudaaProjetInformationPanel.java
 *  @creation     23 mars 2004
 *  @modification $Date: 2006-10-19 13:55:16 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTextArea;
import com.memoire.bu.BuTextField;
import java.awt.Frame;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author Fred Deniger
 * @version $Id: FudaaProjetInformationPanel.java,v 1.7 2006-10-19 13:55:16 deniger Exp $
 */
public class FudaaProjetInformationPanel extends CtuluDialogPanel {

  BuInformationsDocument d_;
  BuTextField author_;
  BuTextField date_;
  BuTextArea com_;

  /**
   * @param _d les infos non nulles
   */
  public FudaaProjetInformationPanel(final BuInformationsDocument _d) {
    super();
    d_ = _d;
    addEmptyBorder(10);
    final BuPanel pn = new BuPanel();
    pn.setLayout(new BuGridLayout(2, 5, 5));
    author_ = addLabelStringText(pn, FudaaLib.getS("Auteur"));
    author_.setText(d_.author);
    date_ = addLabelStringText(pn, FudaaLib.getS("Date"));
    date_.setText(d_.date);
    setLayout(new BuBorderLayout(10, 10));
    add(pn, BuBorderLayout.CENTER);
    com_ = new BuTextArea();
    com_.setText(d_.comment);
    add(new BuScrollPane(com_), BuBorderLayout.SOUTH);
  }

  /**
   * @return les nouvelles info sur le doc si modifie. null sinon
   */
  public BuInformationsDocument getModifyDoc(){
    boolean change = false;
    if (!d_.author.equals(author_.getText().trim())) {
      change = true;
    }
    else if (!d_.date.equals(date_.getText().trim())) {
      change = true;
    }
    else if (!d_.comment.equals(com_.getText().trim())) {
      change = true;
    }
    if (change) {
      final BuInformationsDocument r = new BuInformationsDocument();
      r.author = author_.getText().trim();
      r.date = date_.getText().trim();
      r.comment = com_.getText().trim();
      return r;
    }
    return null;
  }

  /**
   * @param _f la fenetre
   * @param _init le document init
   * @return le doc modifie ou nul si pas de modif
   */
  public static BuInformationsDocument editInfoDoc(final Frame _f,final BuInformationsDocument _init){
    BuInformationsDocument init = _init;
    if (init == null) {
      init = new BuInformationsDocument();
    }
    final FudaaProjetInformationPanel pn = new FudaaProjetInformationPanel(init);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_f))) {
      final BuInformationsDocument d = pn.getModifyDoc();
      if (d != null) {
        return d;
      } else if (_init == null) {
        return init;
      }
    }
    return null;
  }
}
