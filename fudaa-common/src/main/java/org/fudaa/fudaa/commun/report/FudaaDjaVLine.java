/**
 * @modification $Date: 2007-03-19 13:27:20 $
 * @statut       unstable
 * @file         DjaVLine.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package org.fudaa.fudaa.commun.report;

import com.memoire.dja.DjaAnchor;
import com.memoire.dja.DjaHandle;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import org.fudaa.fudaa.commun.FudaaLib;

public class FudaaDjaVLine extends FudaaDjaFormAbstract {
  public FudaaDjaVLine() {
    
    
  }

  @Override
  public int getWidth() {
    return ligne_==null?1:(int) ligne_.getEpaisseur();
  }

  /*public BConfigurableInterface[] getConfigureInterfaces() {
    return new BConfigurableInterface[] { new FudaaReportDjaFormConfigure(this) };
  }*/

  @Override
  public boolean isBackgroundModifiable() {
    return false;
  }

  @Override
  public FudaaDjaFormAbstract createNew() {
    return new FudaaDjaVLine();
  }

  @Override
  public String getDjaName() {
    return "VLine";
  }

  @Override
  public void setWidth(int _w) {
    super.setWidth(1);
  }
  
  @Override
  public String getDescription() {
    return FudaaLib.getS("Ligne verticale");
  }

  @Override
  public boolean contains(int _x, int _y) {
    return new Rectangle(getX() - 3, getY(), 7, getHeight()).contains(_x, _y);
  }

  @Override
  public DjaAnchor[] getAnchors() {
    int x = getX();
    int y = getY();
    int h = getHeight();

    DjaAnchor[] r = new DjaAnchor[2];
    r[0] = new DjaAnchor(this, 0, NORTH, x, y);
    r[1] = new DjaAnchor(this, 1, SOUTH, x, y + h - 1);

    return r;
  }

  @Override
  public DjaHandle[] getHandles() {
    int x = getX();
    int y = getY();
    int h = getHeight();

    DjaHandle[] r = new DjaHandle[2];
    r[0] = new DjaHandle(this, NORTH, x, y - deltaY);
    r[1] = new DjaHandle(this, SOUTH, x, y + h - 1 + deltaY);

    return r;
  }

  @Override
  public void paintObject(Graphics _g) {
    if (ligne_.getCouleur() != null) {
      ligne_.dessineTrait((Graphics2D) _g, getX(), getY(), getX(), getY() + getHeight());
    }

    super.paintObject(_g);
  }
}
