/*
 * @creation 5 mars 07
 * @modification $Date: 2007-03-07 17:25:07 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuInternalFrame;

import javax.swing.*;
import javax.swing.event.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fred deniger
 * @version $Id: FudaaInternalFrameModel.java,v 1.1 2007-03-07 17:25:07 deniger Exp $
 */
public class FudaaInternalFrameModel extends AbstractListModel implements InternalFrameListener {
  final BuDesktop desk_;
  final List if_ = new ArrayList(20);
  ListSelectionModel selection_ = new DefaultListSelectionModel();

  public FudaaInternalFrameModel(BuCommonImplementation _impl) {
    _impl.addIfListener(this);
    desk_ = _impl.getMainPanel().getDesktop();
    selection_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    selection_.addListSelectionListener(_e -> {
      if (!_e.getValueIsAdjusting()) {
        int idx = selection_.getMaxSelectionIndex();
        if (idx >= 0) {
          desk_.activateInternalFrame(((BuInternalFrame) if_.get(idx)));
        } else {
          int idxSelected = if_.indexOf(desk_.getSelectedFrame());
          if (idxSelected >= 0) {
            selection_.setSelectionInterval(idxSelected, idxSelected);
          }
        }
      }
    });
  }

  @Override
  public Object getElementAt(int _index) {
    return if_.get(_index);
  }

  @Override
  public int getSize() {
    return if_.size();
  }

  @Override
  public void internalFrameActivated(InternalFrameEvent _e) {
    int idx = if_.indexOf(_e.getInternalFrame());
    if (idx < 0 && !desk_.isPalette(_e.getInternalFrame())) {
      if_.add(_e.getInternalFrame());
      fireIntervalAdded(this, if_.size() - 1, if_.size() - 1);
      selection_.setSelectionInterval(if_.size() - 1, if_.size() - 1);
      return;
    }
    int enCours = selection_.getMaxSelectionIndex();
    if (idx != enCours) {
      selection_.setSelectionInterval(idx, idx);
    }
  }

  @Override
  public void internalFrameClosed(InternalFrameEvent _e) {
    int idx = if_.indexOf(_e.getInternalFrame());
    if (idx >= 0) {
      try {
        if_.remove(idx);
        fireIntervalRemoved(this, idx, idx);
      } catch (Exception e) {
      }
    }
  }

  @Override
  public void internalFrameClosing(InternalFrameEvent _e) {
  }

  @Override
  public void internalFrameDeactivated(InternalFrameEvent _e) {
  }

  @Override
  public void internalFrameDeiconified(InternalFrameEvent _e) {
  }

  @Override
  public void internalFrameIconified(InternalFrameEvent _e) {
  }

  @Override
  public void internalFrameOpened(InternalFrameEvent _e) {

  }

  @Override
  public void removeListDataListener(ListDataListener _l) {
  }
}
