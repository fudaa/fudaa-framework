/*
 *  @file         FudaaExecComboBoxEditor.java
 *  @creation     4 juin 2003
 *  @modification $Date: 2006-09-19 15:01:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.exec;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxEditor;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author deniger
 * @version $Id: FudaaExecComboBoxEditor.java,v 1.14 2006-09-19 15:01:53 deniger Exp $
 */
public class FudaaExecComboBoxEditor extends JLabel implements ComboBoxEditor, MouseListener {
  FudaaExec exec_;
  FudaaGlobalExecManager mng_;

  public FudaaExecComboBoxEditor(final FudaaGlobalExecManager _mng) {
    mng_ = _mng;
    setBackground(UIManager.getColor("ComboBox.background"));
    setForeground(UIManager.getColor("ComboBox.foreground"));
    setFont(UIManager.getFont("ComboBox.font"));
    setOpaque(false);
    super.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
    addMouseListener(this);
  }

  @Override
  public Component getEditorComponent() {
    return this;
  }

  @Override
  public void setItem(final Object _anObject) {
    exec_ = (FudaaExec) _anObject;
    if (exec_ == null) {
      setIcon(FudaaExec.EMPTY_TOOL_ICON);
      setText(CtuluLibString.EMPTY_STRING);
      setToolTipText(CtuluLibString.EMPTY_STRING);
    } else {
      setIcon(exec_.getIcon());
      setText(exec_.getViewedName());
      setToolTipText(exec_.getExecCommand());
    }
  }

  @Override
  public Object getItem() {
    return exec_;
  }

  @Override
  public void selectAll() {}

  @Override
  public void addActionListener(final ActionListener _l) {}

  @Override
  public void removeActionListener(final ActionListener _l) {}

  @Override
  public void mouseClicked(final MouseEvent _e) {
    if (mng_.getFudaaExecNb() == 0) {
      return;
    }
    if (_e.getClickCount() >= 2) {
      final int r = new FudaaExecPanel(exec_).afficheModale(this, FudaaLib.getS("Modifier l'exécutable"));
      if (CtuluDialogPanel.isOkResponse(r)) {
        mng_.savePref(exec_);
        mng_.writePref();
        this.setItem(exec_);
      }
    }
  }

  @Override
  public void mouseEntered(final MouseEvent _e) {}

  @Override
  public void mouseExited(final MouseEvent _e) {}

  @Override
  public void mousePressed(final MouseEvent _e) {}

  @Override
  public void mouseReleased(final MouseEvent _e) {}

  @Override
  public void setBorder(final Border _border) {}
}
