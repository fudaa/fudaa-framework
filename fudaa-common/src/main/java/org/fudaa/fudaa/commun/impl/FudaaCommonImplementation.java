/**
 * @creation 12 juin 2003
 * @modification $Date$
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuBrowserPreferencesPanel;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuColumn;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDesktopPreferencesPanel;
import com.memoire.bu.BuDialogWarning;
import com.memoire.bu.BuGlassPaneStop;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuInsets;
import com.memoire.bu.BuLanguagePreferencesPanel;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuLookPreferencesPanel;
import com.memoire.bu.BuMainPanel;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuMenuBar;
import com.memoire.bu.BuMenuInterface;
import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuPreferencesDialog;
import com.memoire.bu.BuPreferencesMainPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTask;
import com.memoire.bu.BuToolBar;
import com.memoire.bu.BuToolButton;
import com.memoire.bu.BuUserPreferencesPanel;
import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import ghm.followgui.FollowApp;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.InternalFrameEvent;
import javax.swing.plaf.InternalFrameUI;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.gui.CtuluAnalyzeGUI;
import org.fudaa.ctulu.gui.CtuluFileChooserFileTester;
import org.fudaa.ctulu.gui.CtuluFilleWithComponent;
import org.fudaa.ctulu.gui.CtuluLibDialog;
import org.fudaa.ctulu.gui.CtuluPopupMenu;
import org.fudaa.ctulu.gui.CtuluTaskOperationGUI;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ctulu.image.CtuluImageProducer;
import org.fudaa.ebli.commun.EbliActionAbstract;
import org.fudaa.ebli.commun.EbliActionMap;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.impression.EbliFillePrevisualisation;
import org.fudaa.ebli.impression.EbliMiseEnPageDialog;
import org.fudaa.ebli.impression.EbliMiseEnPagePreferencesPanel;
import org.fudaa.ebli.impression.EbliPageable;
import org.fudaa.fudaa.commun.FudaaAstuces;
import org.fudaa.fudaa.commun.FudaaAstucesAbstract;
import org.fudaa.fudaa.commun.FudaaBrowserControl;
import org.fudaa.fudaa.commun.FudaaHelpPDFViewer;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaPreferences;
import org.fudaa.fudaa.commun.FudaaTee;
import org.fudaa.fudaa.commun.FudaaUI;
import org.fudaa.fudaa.commun.aide.FudaaAidePreferencesPanel;
import org.fudaa.fudaa.commun.aide.FudaaHelpPanel;
import org.fudaa.fudaa.commun.exetools.FudaaExeToolsMenu;
import org.fudaa.fudaa.commun.exetools.FudaaManageExeTools;
import org.fudaa.fudaa.commun.report.FudaaReport;
import org.fudaa.fudaa.commun.undo.FudaaUndoCmdMngListener;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * @author deniger
 * @version $Id$
 */
public abstract class FudaaCommonImplementation extends BuCommonImplementation implements FudaaUI {

  JComponent oldRightComponent_;
  JScrollPane rightC_;
  protected FudaaHelpPanel aide_;
  protected FudaaUndoCmdMngListener cmdListener_;
  protected BuGlassPaneStop glassPaneStop_;
  protected EbliFillePrevisualisation previsuFille_;
  protected FollowApp tailFrame_;
  private boolean onlyOneApplicationFrame = true;
  /**
   * Description du champ.
   */
  protected JFrame ts_;
  /**
   * Mettre a true pour utiliser le nouveau composant d'aide.
   */
  protected boolean useNewHelp_;
  /**
   *
   */
  public static final String RIGHT_COMPONENT_IN_SCROLL = "useScrollPane";

  /**
   * Ajoute le composant de la fenetre rendue active � la colonne de droite. Fait apparaitre la colonne si celle ci �tait invisible.
   *
   * @param _comp La fenetre rendue active.
   */
  private void addRightComponent(final CtuluFilleWithComponent _comp) {
    if (oldRightComponent_ == null || _comp == null) {
      return;
    }
    _comp.majComponent(oldRightComponent_);
    oldRightComponent_.setToolTipText(_comp.getComponentTitle());
    JComponent toUse = null;
    if (oldRightComponent_.getClientProperty(RIGHT_COMPONENT_IN_SCROLL) == Boolean.FALSE) {
      toUse = oldRightComponent_;
    } else {
      if (rightC_ == null) {
        rightC_ = new JScrollPane(oldRightComponent_);
        rightC_.setPreferredSize(new Dimension(175, 350));
        rightC_.setVisible(true);
      } else {
        rightC_.setViewportView(oldRightComponent_);
      }
      toUse = rightC_;
    }
    // B.M. : Pour moi, le dimensionnement de la colonne et la visibilit� n'ont
    // rien a faire ici.
    // Logiquement, les BuColumn devraient avoir une taille par defaut dans
    // BuMainPanel, taille surcharg�e dans BuPreferences.applyOnMainPanel().
    // Cependant, vu le nombre d'appli qui gere les BuColumn de mani�re bizarre,
    // on laisse comme ca.
    getMainPanel().getRightColumn().setVisible(true);
    Dimension preferredSize = getMainPanel().getRightColumn().getPreferredSize();
    if (preferredSize.width <= 0) {
      preferredSize.width = 175;
      getMainPanel().getRightColumn().setPreferredSize(preferredSize);
      getMainPanel().updateSplits();
    }
    getMainPanel().getRightColumn().addToggledComponent(_comp.getComponentTitle(), "TOGGLE_SPEC",
            BuResource.BU.getToolIcon("arbre"), toUse, toUse.isVisible(), this).setToolTipText(
            FudaaLib.getS("Afficher/Cacher") + CtuluLibString.ESPACE + _comp.getComponentTitle());
  }

  private int findRangerPalette(final BuToolBar _tb) {
    final String cmd = "RANGERPALETTES";
    int idx = -1;
    for (int i = _tb.getComponentCount() - 1; i >= 0; i--) {
      if (_tb.getComponent(i) instanceof BuToolButton
              && cmd.equals(((BuToolButton) _tb.getComponent(i)).getActionCommand())) {
        idx = i;
        break;
      }
    }
    return idx;
  }

  private void gestionnaireImpression(final String _commande) {
    final JInternalFrame frame = getCurrentInternalFrame();
    final EbliPageable target;
    if (frame instanceof EbliPageable) {
      target = (EbliPageable) frame;
    } else if (frame instanceof EbliFillePrevisualisation) {
      target = ((EbliFillePrevisualisation) frame).getEbliPageable();
    } else {
      new BuDialogWarning(this, getInformationsSoftware(), FudaaLib.getS("Cette fen�tre n'est pas imprimable"))
              .activate();
      return;
    }
    if ("IMPRIMER".equals(_commande)) {
      cmdImprimer(target);
    } else if ("MISEENPAGE".equals(_commande)) {
      cmdMiseEnPage(target);
    } else if ("PREVISUALISER".equals(_commande)) {
      cmdPrevisualisation(target);
    }
  }

  private void rangerInternalFrames() {
    final JInternalFrame[] ifs = getAllInternalFrames();
    boolean done = false;
    final JComponent pn = getMainPanel().getMiddleComponent();
    final Dimension pnDim = (pn instanceof JScrollPane) ? ((JScrollPane) pn).getViewport().getExtentSize() : pn
            .getSize();
    if (ifs != null) {
      for (int i = ifs.length - 1; i >= 0; i--) {
        if (!BuLib.isPalette(ifs[i])) {
          final Dimension d = ifs[i].getSize();
          final Point loc = ifs[i].getLocation();
          if (((loc.x + d.width) > pnDim.width) || ((loc.y + d.height) > pnDim.height)) {
            d.width = Math.min(d.width, pnDim.width);
            d.height = Math.min(d.height, pnDim.height);
            loc.x = 0;
            loc.y = 0;
            ifs[i].setSize(d);
            ifs[i].setLocation(loc);
            done = true;
          }

        }
      }

    }

    if (done) {
      getMainPanel().arrangePalettes();
    }

  }

  /**
   * Retire de la colonne droite le composant li� � la fenetre interne rendue inactive. Fait disparaitre la colonne si plus aucun composant dans cette
   * colonne.
   */
  private void removeRightComponent() {
    getMainPanel().getRightColumn().removeToggledComponent("TOGGLE_SPEC");
  }

  void renameInternalFrame(final int _x, final int _y) {
    final JInternalFrame frame = getCurrentInternalFrame();
    if (frame != null) {
      FudaaInternalFrameList.editFrameTitle(frame, _x, _y);
    }
  }

  /**
   * Ajoute un composant d'acc�s a la liste des InternalFrame a gauche du frame principal.
   */
  protected void addFrameListLeft() {
    if (getMainPanel() != null) {
      final BuColumn left = getMainPanel().getLeftColumn();
      if (left != null) {
        final BuToolButton bt = new BuToolButton(BuResource.BU.getFrameIcon("liste"));
        bt.setToolTipText(CtuluLib.getS("Liste des fen�tres"));
        bt.setMargin(BuInsets.INSETS0000);
        bt.addActionListener(this);
        bt.setActionCommand("IFRAMESPANEL");
        left.add(bt);
        left.add(new FudaaInternalFrameList(new FudaaInternalFrameModel(this)));
        left.setVisible(true);
        Dimension preferredSize = left.getPreferredSize();
        if (preferredSize.width <= 0) {
          preferredSize.width = 22;
          left.setPreferredSize(preferredSize);
        }
      }
    }
  }

  /**
   * Si true, un icone "export donnees" sera ajoute a la tool bar.
   *
   * @return false par defaut
   */
  protected boolean buildExportDataToolIcon() {
    return false;
  }

  protected boolean buildFudaaReportTool() {
    return false;
  }

  /**
   * Si true, un icone "capture d'�cran" sera ajoute a la tool bar.
   *
   * @return false par defaut
   */
  protected boolean buildImageToolIcon() {
    return false;
  }

  /**
   * Remplace un item menu en provenance de Bu par une action, plus g�n�raliste.
   *
   * @param _command La commande.
   * @return L'action de remplacement, ou null si la commande n'existait pas.
   */
  protected EbliActionAbstract replaceItemByAction(String _command) {
    JMenuItem it = ((JMenuItem) getMainMenuBar().getMenuItem(_command));
    if (it == null) {
      return null;
    }
    BuMenu mn = (BuMenu) getMainMenuBar().getMenuForAction(_command);
    if (mn == null) {
      return null;
    }
    int idx = mn.indexOf(_command);
    if (idx == -1) {
      return null;
    }

    EbliActionAbstract act = new EbliActionSimple(it.getText(), it.getIcon(), it.getActionCommand()) {
      @Override
      public void actionPerformed(ActionEvent _evt) {
        FudaaCommonImplementation.this.actionPerformed(_evt);
      }
    };
    act.setDefaultToolTip(it.getToolTipText());
    act.setKey(it.getAccelerator());
    mn.remove(idx);
    mn.insert(act, idx)
            // Pour rester compatible avec Bu.
            .setName("mi" + _command);

    return act;
  }

  /**
   * Cr�ation des actions sp�cifiques a Fudaa.
   */
  private void buildActions() {
    EbliActionAbstract act;

    act = replaceItemByAction("TOUTSELECTIONNER");
    addActionToUniqueRegistry(act);
    act = replaceItemByAction("RECHERCHER");
    addActionToUniqueRegistry(act);

    act = new EbliActionSimple(CtuluLib.getS("Inverser la s�lection"), BuResource.BU.getIcon("aucun"),
            "INVERSESELECTION") {
      @Override
      public void actionPerformed(ActionEvent _evt) {
        FudaaCommonImplementation.this.actionPerformed(_evt);
      }
    };
    act.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_MASK));
    addActionToUniqueRegistry(act);

    act = new EbliActionSimple(CtuluLib.getS("Effacer la s�lection"), BuResource.BU.getIcon("effacer"),
            "CLEARSELECTION") {
      @Override
      public void actionPerformed(ActionEvent _evt) {
        FudaaCommonImplementation.this.actionPerformed(_evt);
      }
    };
    act.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK
            | InputEvent.SHIFT_MASK));
    addActionToUniqueRegistry(act);

    act = replaceItemByAction("DUPLIQUER");
    act.setEnabled(false);
    addActionToUniqueRegistry(act);

    act = replaceItemByAction("COUPER");
    act.setEnabled(false);
    addActionToUniqueRegistry(act);
    act = replaceItemByAction("COLLER");
    act.setEnabled(false);
    addActionToUniqueRegistry(act);
    act = replaceItemByAction("COPIER");
    act.setEnabled(false);
    addActionToUniqueRegistry(act);
  }

  private void addActionToUniqueRegistry(EbliActionAbstract act) {
    if (act != null && onlyOneApplicationFrame) {
      EbliActionMap.getInstance().addAction(act);
    }
  }

  /**
   * Methode a surcharger pour ajouter ces propres panneaux de pref.
   */
  protected void buildPreferences(final List<BuAbstractPreferencesPanel> _frAddTab) {
    _frAddTab.add(new BuUserPreferencesPanel(this));
    _frAddTab.add(new BuLanguagePreferencesPanel(this));
    _frAddTab.add(new BuDesktopPreferencesPanel(this));
    _frAddTab.add(new BuLookPreferencesPanel(this));
    _frAddTab.add(new BuBrowserPreferencesPanel(this));
    _frAddTab.add(new EbliMiseEnPagePreferencesPanel());
    _frAddTab.add(new FudaaAidePreferencesPanel(this, getApplicationPreferences()));
  }

  protected void cmdOpenFollowOnTsLog() {
    if (FudaaTee.isCreated()) {
      openFileInLogFrame(FudaaTee.getFudaaTee().getFile());
    }
  }

  protected JButton createFudaaReportToolButton() {
    final BuToolButton bt = new BuToolButton(BuResource.BU.getToolIcon("generer"));
    bt.setToolTipText(FudaaLib.getS("G�n�rer un rapport"));
    bt.setActionCommand("FUDAA_REPORT");
    bt.addActionListener(this);
    return bt;
  }

  /**
   * Methode appel�e par le menu export/images.
   */
  protected void exportImage() {
    final JInternalFrame frame = getCurrentInternalFrame();
    if (frame == null) {
      return;
    }
    if (frame instanceof CtuluImageProducer) {
      CtuluImageExport.exportImageFor(this, (CtuluImageProducer) frame);
    }
  }

  protected void exportImageInClipboard() {
    final JInternalFrame frame = getCurrentInternalFrame();
    if (frame == null) {
      return;
    }
    if (frame instanceof CtuluImageProducer) {
      CtuluImageExport.exportImageInClipboard((CtuluImageProducer) frame, this);

    }
  }

  protected FudaaHelpPanel getHelpPanel() {
    if (aide_ == null) {
      try {
        aide_ = new FudaaHelpPanel(this, new URL(getHelpDir()), null);
      } catch (final MalformedURLException _e) {
        FuLog.error(_e);
      }
    }
    return aide_;
  }

  protected void initExportImageToClipborad(final AbstractButton _bt) {
    _bt.setIcon(CtuluResource.CTULU.getIcon("copie-image"));
    _bt.setText(CtuluLib.getS("Copier l'image dans le presse-papier"));
    _bt.setActionCommand(CtuluLibImage.SNAPSHOT_CLIPBOARD_COMMAND);
    _bt.setToolTipText(CtuluLib.getS("Placer l'image de la fen�tre active dans le presse-papier"));
    _bt.setEnabled(false);
    _bt.addActionListener(this);
  }

  /**
   * Description de la methode.
   */
  protected void preferences() {

    final BuPreferencesMainPanel pn = new BuPreferencesMainPanel();
    final List r = new ArrayList();
    buildPreferences(r);
    final int nb = r.size();
    for (int i = 0; i < nb; i++) {
      pn.addTab((BuAbstractPreferencesPanel) r.get(i));
    }
    final BuPreferencesDialog d = new BuPreferencesDialog(getFrame(), pn);
    d.setLocationRelativeTo(getFrame());
    d.setVisible(true);
    d.dispose();
  }

  @Override
  public void actionPerformed(final ActionEvent event) {
    String action = event.getActionCommand();
    final int i = action.indexOf('(');
    if (i >= 0) {
      action = action.substring(i + 1, action.length() - 1);
    }
    if (("PREVISUALISER".equals(action)) || ("MISEENPAGE".equals(action)) || ("IMPRIMER".equals(action))) {
      gestionnaireImpression(action);
    } else if (action.equals("PREFERENCE")) {
      preferences();
    } else if (action.equals("CONSOLE")) {
      if (ts_ != null) {
        ts_.pack();
        ts_.setVisible(true);
      } else {
        cmdOpenFollowOnTsLog();
      }
    } else if (action.equals(CtuluLibImage.SNAPSHOT_COMMAND)) {
      exportImage();
    } else if (action.equals(CtuluLibImage.SNAPSHOT_CLIPBOARD_COMMAND)) {
      exportImageInClipboard();
    } else if (action.equals(CtuluExportDataInterface.EXPORT_CMD)) {
      final JInternalFrame f = getCurrentInternalFrame();
      if (f instanceof CtuluExportDataInterface) {
        ((CtuluExportDataInterface) f).startExport(this);
      }
    } else if ("CLEARSELECTION".equals(action)) {
      final JInternalFrame frame = getCurrentInternalFrame();

      if (frame instanceof CtuluSelectionInterface) {
        FuLog.trace("Clear selection");
        ((CtuluSelectionInterface) frame).clearSelection();
      }
    } else if ("INVERSESELECTION".equals(action)) {
      final JInternalFrame frame = getCurrentInternalFrame();

      if (frame instanceof CtuluSelectionInterface) {
        FuLog.trace("Inverse selection");
        ((CtuluSelectionInterface) frame).inverseSelection();
      }
    } else if (action.equals("AIDE")) {
      // displayHelp(null);
      displayHelpPDF(getInformationsSoftware().baseManUrl());
    } else if (action.equals("AIDE_INDEX")) {
      displayHelp(getInformationsSoftware().baseManUrl());
    } else if (action.equals("INDEX_ALPHA")) {
      displayHelp(getInformationsSoftware().baseManUrl() + "alphabetique.html");
    } else if (action.equals("INDEX_THEMA")) {
      displayHelp(getInformationsSoftware().baseManUrl() + "thematique.html");
    } else if (action.equals("IFRAMESPANEL")) {
      FudaaInternalFramePanel.active(getMainPanel().getDesktop(), false);
    } else if (action.equals("IFRAMESPANELSWITCH")) {
      FudaaInternalFramePanel.active(getMainPanel().getDesktop(), true);
    } else if (action.equals("FUDAA_REPORT")) {
      new FudaaReport(this);
    } else if ("RANGERFRAMES".equals(action)) {
      rangerInternalFrames();
    } else {
      super.actionPerformed(event);
    }
  }

  public BuMenuItem addConsoleMenu(final BuMenuInterface _dest) {
    return _dest.addMenuItem(FudaaLib.getS("Console"), "CONSOLE", FudaaResource.FUDAA.getIcon("tail"), FudaaTee
            .isCreated());
  }

  protected FudaaManageExeTools getExeToolManager() {
    return new FudaaManageExeTools();
  }

  /**
   * Ajoute le menu des exes lancables depuis l'application.
   *
   * @param _dest Le menu destinataire
   * @return Le nouveau menu.
   */
  public BuMenu addExeToolsMenu(final BuMenuInterface _dest) {
    BuMenu mnExeTools = new FudaaExeToolsMenu(this, FudaaPreferences.FUDAA, FudaaResource.FUDAA, getExeToolManager());
    _dest.addSubMenu(mnExeTools, true);

    return mnExeTools;
  }
  FudaaHelpPDFViewer frameHelpPDF_;
  protected String documentationJar = "fudaaDocumentation.jar";

  /**
   * Affiche la fenetre de viewer pdf
   */
  public void displayHelpPDF(String url) {
    if (frameHelpPDF_ == null) {
      frameHelpPDF_ = new FudaaHelpPDFViewer(documentationJar, url, this, false);
    }
    // -- ajout de la fenetre --//:
    if (getMainPanel().getDesktop() != null) {
      this.addInternalFrame(frameHelpPDF_);
    } else {
      JFrame frame = new JFrame();
      frame.setContentPane(frameHelpPDF_.getContentPane());
      frame.setPreferredSize(new Dimension(800, 800));
      frame.setSize(new Dimension(800, 800));
      frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
      frame.setVisible(true);
    }
  }

  protected void rebuildEditionMenu(final BuMenu _dest) {
    int indSelect = _dest.indexOf("TOUTSELECTIONNER");
    _dest.remove(indSelect + 2);
    _dest.insert(EbliActionMap.getInstance().getAction("INVERSESELECTION"), indSelect + 1);
    _dest.insert(EbliActionMap.getInstance().getAction("CLEARSELECTION"), indSelect + 2);
    addConsoleMenu(_dest);
  }

  /**
   * Permet d'ajouter un menu a la barre des menu.
   *
   * @param _m le menu a ajouter
   * @param _idx l'indice voulu
   */
  public void addMenu(final JMenu _m, final int _idx) {
    final BuMenuBar b = getMainMenuBar();
    b.add(_m, _idx);
    b.computeMnemonics();
    b.revalidate();
  }

  /**
   * @param _filename le nom du fichier
   * @return dir/_txt.fr.html ou .en.html
   */
  public String buildHelpPathFor(final String _filename) {
    String lang = "en";
    String l = Locale.getDefault().getLanguage();
    if (l.length() > 2) {
      l = l.substring(0, 2);
    }
    if (getInformationsSoftware().languages.indexOf(l) >= 0) {
      lang = l;
    }
    return getHelpDir() + _filename + CtuluLibString.DOT + lang + ".html";
  }

  /**
   * @param _txt le texte
   * @param _filename le nom du fichier
   * @return un hyperlien qui va bien
   */
  public String buildLink(final String _txt, final String _filename) {
    return "<a href=\"" + buildHelpPathFor(_filename) + "\">" + _txt + "</a>";
  }

  /**
   * @param _sec le nombre de secondes a attendre avant d'effacer le message principal
   */
  public void clearMainMessage(final int _sec) {
    new javax.swing.Timer(_sec * 1000, null) {
      {
        setRepeats(false);
      }

      @Override
      protected void fireActionPerformed(final ActionEvent _e) {
        unsetMainMessage();
        super.fireActionPerformed(_e);
      }
    }.start();
  }

  /**
   * Efface le message principal apres 2 sec.
   */
  public void clearMainMessageInTwoSec() {
    clearMainMessage(2);
  }

  @Override
  public void clearMainProgression() {
    unsetMainMessage();
    unsetMainProgression();

  }

  /**
   * @param _bu la tache a enleve de la taskview
   */
  public void clearTaskView(final BuTask _bu) {
    getMainPanel().getTaskView().removeTask(_bu);
  }

  /**
   * Impression de la page
   * <code>_target</code> dans un nouveau thread.
   *
   * @param _target la page a imprimer
   */
  public void cmdImprimer(final EbliPageable _target) {
    final PrinterJob printJob = PrinterJob.getPrinterJob();
    final BuMainPanel mp = getMainPanel();
    printJob.setPageable(_target);
    if (printJob.printDialog()) {
      mp.setProgression(5);
      new CtuluTaskOperationGUI(this, FudaaResource.FUDAA.getString("Impression")) {
        @Override
        public void act() {
          try {
            mp.setProgression(10);
            printJob.print();
            mp.setProgression(100);
          } catch (final Exception _printEx) {
            mp.setProgression(0);
            FuLog.error(_printEx);
          }
        }
      }.start();
    }
  }

  /**
   * Commande pour la mise en page.
   *
   * @param _target la page a mettre en page
   */
  public void cmdMiseEnPage(final EbliPageable _target) {
    new EbliMiseEnPageDialog(_target, getApp(), getInformationsSoftware()).activate();
  }

  /**
   * Cette m�thode doit �tre implant�e, pour q'un projet puisse �tre ouvert au d�marrage de l'application.
   *
   * @see Fudaa#launch(String[], BuInformationsSoftware, boolean)
   * @param _f le fichier a ouvrir
   */
  public void cmdOuvrirFile(final File _f) {
    new Throwable("vous devez implanter cette m�thode");
  }

  /**
   * @param _target la page a pr�visualiser
   */
  public void cmdPrevisualisation(final EbliPageable _target) {
    if (previsuFille_ == null) {
      previsuFille_ = new EbliFillePrevisualisation(getApp(), _target);
    } else {
      previsuFille_.setEbliPageable(_target);
    }
    addInternalFrame(previsuFille_);
  }

  /**
   * @see com.memoire.bu.BuCommonImplementation#contextHelp(java.lang.String)
   */
  @Override
  public void contextHelp(final String _url) {
    String url = _url;
    if ((url == null) || (url.length() == 0) || (url.startsWith("#"))) {
      url = "index.html";
    } else {
      url = url.trim();
    }
    super.contextHelp(url);
  }

  public ProgressionInterface createProgressionInterface(final BuTask _bu) {
    return new ProgressionBuAdapter(_bu);
  }

  @Override
  public CtuluTaskDelegate createTask(final String _name) {
    return new CtuluTaskOperationGUI(this, _name);
  }

  public void displayHelp(final String _url) {
    if (!useNewHelp_) {
      displayURL(_url);
      return;
    }
    final FudaaHelpPanel panel = getHelpPanel();
    try {
      panel.setDocumentUrl(new URL(_url), false);
      panel.showHelp();
    } catch (final MalformedURLException _e) {
      FuLog.warning(_e);
      error(_e.getMessage());
    }

  }

  /**
   * Permet d'afficher le contenu du fichier f dans un browser.
   *
   * @param _f le fichier a afficher
   * @see #displayURL(String)
   */
  public void displayURL(final File _f) {
    displayURL(_f.getAbsolutePath());
  }

  /**
   * Permet d'afficher une url
   * <code>_url</code> dans un browser. Le browser est choisi en fonction des preferences (
   * <code>browser.type</code>). Si l'url ne commence pas par http, elle est modifiee en fonction de la plate-forme.
   *
   * @see CtuluLibString#pathToUrl(String)
   * @param _url l'url (http) ou le chemin du fichier a afficher
   */
  @Override
  public void displayURL(final String _url) {
    String url = _url;
    if ((url == null) || (url.length() == 0)) {
      url = FudaaLib.LOCAL_MAN;
    }
    if (!url.startsWith("http")) {
      if (url.endsWith("/")) {
        url += "index.html";
      }
      url = CtuluLibString.pathToUrl(url);
    }
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("FCM: displayURL " + url);
    }
    FudaaBrowserControl.displayURL(url);
  }

  @Override
  public void error(final String _msg) {
    error(BuResource.BU.getString("Erreur"), _msg, false);
  }

  /**
   * Affiche un message d'erreur.
   */
  public void error(final String _titre, final String _text) {
    error(_titre, _text, false);
  }

  /**
   * @param _titre le titre de l'erreur
   * @param _text le contenu
   */
  @Override
  public void error(final String _titre, final String _text, final boolean _tempo) {
    if (!EventQueue.isDispatchThread()) {
      BuLib.invokeNow(new Runnable() {
        @Override
        public void run() {
          error(_titre, _text, _tempo);
        }
      });
      return;

    }
    if (_tempo) {
      setMainErrorAndClear(_text);
    } else {
      CtuluLibDialog.showError(this, _titre, _text);
    }
  }

  /**
   * @return Renvoie les preferences de l'application
   */
  public abstract BuPreferences getApplicationPreferences();

  /**
   * @return valeur Astuces
   */
  public FudaaAstucesAbstract getAstuces() {
    return FudaaAstuces.FUDAA;
  }

  /**
   * @return le repertoire de l'aide
   */
  public String getHelpDir() {
    return FudaaLib.LOCAL_MAN;
  }

  @Override
  public abstract BuInformationsSoftware getInformationsSoftware();

  @Override
  public ProgressionInterface getMainProgression() {
    return new CtuluTaskOperationGUI.MainProgression(getMainPanel());
  }

  /**
   * @return le nombre de menu de la barre des menus
   */
  public int getNbMenuInMenuBar() {
    return getMainMenuBar().getMenuCount();
  }

  @Override
  public Component getParentComponent() {
    return getFrame();
  }

  /**
   * @return le gestion de commande
   */
  public FudaaUndoCmdMngListener getUndoCmdListener() {
    if (cmdListener_ == null) {
      cmdListener_ = new FudaaUndoCmdMngListener(this);
    }
    return cmdListener_;
  }

  public Runnable getUnsetGlassPaneRunnable() {
    return new Runnable() {
      @Override
      public void run() {
        unsetGlassPaneStop();
      }
    };
  }

  /**
   * @see com.memoire.bu.BuCommonInterface#init()
   */
  @Override
  public void init() {
    super.init();

    buildActions();
    rebuildEditionMenu((BuMenu) getMainMenuBar().getMenu("MENU_EDITION"));

    final BuToolBar tb = getMainToolBar();
    // on place la nouvelle action apres le ranger Palettes
    final int idx = findRangerPalette(tb);
    final BuToolButton btRanger = tb.addToolButton(FudaaLib.getS("Ranger les fen�tres"), "RANGERFRAMES", true);
    btRanger.setIcon(FudaaResource.FUDAA.getToolIcon("arrange-frames_16.png"));
    if (idx > 0) {
      tb.add(btRanger, idx);
    }

    if (buildExportDataToolIcon()) {
      final BuToolButton bt = new BuToolButton();
      initExportDataButton(bt);
      tb.add(bt);
    }

    if (buildImageToolIcon()) {
      final BuToolButton btCopie = new BuToolButton();
      initExportImageToClipborad(btCopie);
      final BuToolButton bt = new BuToolButton() {
        @Override
        public void setEnabled(final boolean _b) {
          btCopie.setEnabled(_b);
          super.setEnabled(_b);
        }
      };

      initExportImageButton(bt);
      tb.add(bt);
      tb.add(btCopie);
    }

    if (buildFudaaReportTool()) {
      tb.add(createFudaaReportToolButton());
    }

    BuMenu mn = (BuMenu) getMainMenuBar().getMenu("MENU_FENETRES");

    if (mn != null) {
      mn.remove(0);

      BuMenuItem it = new BuMenuItem(FudaaResource.FUDAA.getMenuIcon("next-frame"), FudaaLib.getS("Fen�tre suivante"));
      it.setActionCommand("IFRAMESPANELSWITCH");
      it.addActionListener(this);
      it.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, InputEvent.CTRL_MASK));
      mn.add(it, 0);
      mn.computeMnemonics();
      it = new BuMenuItem(BuResource.BU.getMenuIcon("liste"), CtuluLib.getS("fen�tres"));
      it.setActionCommand("IFRAMESPANEL");
      it.addActionListener(this);
      it.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.CTRL_MASK));
      mn.add(it, 0);
    }
    
    mn = (BuMenu) getMainMenuBar().getMenu("MENU_AIDE");
    if (mn!=null) {
      JMenuItem www=mn.getItem(mn.indexOf("WWW_ACCUEIL"));
      if (www!=null) {
        www.setToolTipText(getInformationsSoftware().http);
      }
    }
  }

  public void initExportDataButton(final AbstractButton _bt) {
    FudaaGuiLib.initExportDataButton(_bt, this);
  }

  public void initExportImageButton(final AbstractButton _bt) {
    _bt.setIcon(BuResource.BU.getIcon("photographie"));
    _bt.setText(CtuluLib.getS("Exporter image"));
    _bt.setActionCommand(CtuluLibImage.SNAPSHOT_COMMAND);
    _bt.setToolTipText(CtuluLib.getS("Cr�er une image � partir de la fen�tre active"));
    _bt.setEnabled(false);
    _bt.addActionListener(this);
  }

  /**
   * @param _bu la tache a ajouter au taskview
   */
  public void initTaskView(final BuTask _bu) {
    _bu.setTaskView(getMainPanel().getTaskView());
    getMainPanel().getTaskView().addTask(_bu);
  }

  @Override
  public void internalFrameActivated(final InternalFrameEvent _evt) {
    super.internalFrameActivated(_evt);

    if (cmdListener_ != null) {
      cmdListener_.internalFrameActivated(_evt);
    }
    if (_evt.getInternalFrame() instanceof CtuluFilleWithComponent) {
      final CtuluFilleWithComponent c = (CtuluFilleWithComponent) _evt.getInternalFrame();
      oldRightComponent_ = c.createPanelComponent();
      addRightComponent(c);
    } else {
      removeRightComponent();
    }
  }

  @Override
  public void internalFrameClosed(final InternalFrameEvent _evt) {
    super.internalFrameClosed(_evt);
    if (cmdListener_ != null) {
      cmdListener_.internalFrameClosed(_evt);
    }
  }

  @Override
  public void internalFrameClosing(final InternalFrameEvent _evt) {
    super.internalFrameClosing(_evt);
    if (cmdListener_ != null) {
      cmdListener_.internalFrameClosing(_evt);
    }
  }

  @Override
  public void internalFrameDeactivated(final InternalFrameEvent _evt) {
    super.internalFrameDeactivated(_evt);
    if (cmdListener_ != null) {
      cmdListener_.internalFrameDeactivated(_evt);
    }
    removeRightComponent();
  }

  @Override
  public void internalFrameDeiconified(final InternalFrameEvent _evt) {
    super.internalFrameDeiconified(_evt);
    if (cmdListener_ != null) {
      cmdListener_.internalFrameDeiconified(_evt);
    }
  }

  @Override
  public void internalFrameIconified(final InternalFrameEvent _evt) {
    super.internalFrameIconified(_evt);
    if (cmdListener_ != null) {
      cmdListener_.internalFrameIconified(_evt);
    }
  }

  @Override
  public void internalFrameOpened(final InternalFrameEvent internalFrameEvent) {
    super.internalFrameOpened(internalFrameEvent);
    if (cmdListener_ != null) {
      cmdListener_.internalFrameOpened(internalFrameEvent);
    }
    final JInternalFrame frame = internalFrameEvent.getInternalFrame();

    if (!BuLib.isPalette(frame)) {
      final InternalFrameUI ui = frame.getUI();
      if (ui instanceof BasicInternalFrameUI && ((BasicInternalFrameUI) ui).getNorthPane() != null) {
        ((BasicInternalFrameUI) ui).getNorthPane().add(new BuButton(BuResource.BU.getToolIcon("aide")));
        ((BasicInternalFrameUI) ui).getNorthPane().addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(final MouseEvent _e) {
            if (SwingUtilities.isRightMouseButton(_e)) {
              final CtuluPopupMenu pop = new CtuluPopupMenu();

              pop.addMenuItem(BuResource.BU.getString("Arranger les fen�tres"), "RANGERFRAMES", FudaaResource.FUDAA
                      .getToolIcon("arrange-frames_16.png"), true, FudaaCommonImplementation.this);

              pop.addMenuItem(BuResource.BU.getString("renommer"), "RENAMEFRAMES", BuResource.BU
                      .getToolIcon("renommer"), true, new ActionListener() {
                @Override
                public void actionPerformed(final ActionEvent _evtAct) {
                  final Point p = _e.getComponent().getLocationOnScreen();
                  FudaaCommonImplementation.this.renameInternalFrame(p.x + _e.getX(), p.y + _e.getY());

                }
              });
              pop.show(FudaaCommonImplementation.this.getCurrentInternalFrame(), _e.getX(), _e.getY());
            }
          }
        });
      }
    }

  }

  @Override
  public boolean manageAnalyzeAndIsFatal(final CtuluAnalyze _analyze) {
    if (_analyze == null) {
      return false;
    }
    if (!_analyze.isEmpty()) {
      CtuluAnalyzeGUI.showDialogErrorFiltre(_analyze, this, _analyze.getDesc(), false);
    }
    return _analyze.containsFatalError();
  }

  @Override
  public boolean manageAnalyzeAndIsFatal(final CtuluLog _analyze) {
    if (_analyze == null) {
      return false;
    }
    if (!_analyze.isEmpty()) {
      CtuluAnalyzeGUI.showDialogErrorFiltre(_analyze, this, _analyze.getDesc(), false);
    }
    return _analyze.containsSevereError();
  }

  @Override
  public boolean manageErrorOperationAndIsFatal(final CtuluIOOperationSynthese _opResult) {
    if (_opResult == null) {
      error(FudaaLib.getS("L'op�ration a �chou�"));
      return true;
    }
    if (_opResult.getClosingException() != null) {
      CtuluLibMessage.error("FATAL :can't close " + _opResult.getAnalyze().getResource());
    }
    if (_opResult.getAnalyze() != null) {
      return manageAnalyzeAndIsFatal(_opResult.getAnalyze());
    }
    return false;
  }

  @Override
  public boolean manageErrorOperationAndIsFatal(final CtuluIOResult _opResult) {
    if (_opResult == null) {
      error(FudaaLib.getS("L'op�ration a �chou�"));
      return true;
    }
    if (_opResult.getClosingException() != null) {
      CtuluLibMessage.error("FATAL :can't close " + _opResult.getAnalyze().getResource());
    }
    if (_opResult.getAnalyze() != null) {
      return manageAnalyzeAndIsFatal(_opResult.getAnalyze());
    }
    return false;
  }

  public void message(final String _msg) {
    message(BuResource.BU.getString("Message"), _msg, false);
  }

  @Override
  public void message(final String _titre, final String _text, final boolean _tempo) {
    if (!EventQueue.isDispatchThread()) {
      BuLib.invokeNow(new Runnable() {
        @Override
        public void run() {
          message(_titre, _text, _tempo);
        }
      });
      return;

    }
    if (_tempo) {
      setMainMessageAndClear(_text);
    } else {
      CtuluLibDialog.showMessage(this, _titre, _text);
    }
  }

  /**
   * Ouvre le "tail java" et suit le fichier pass� en param�tres.
   *
   * @param _f le fichier a suivre
   */
  public void openFileInLogFrame(final File _f) {
    if (tailFrame_ == null) {
      new CtuluTaskOperationGUI(this, FudaaLib.getS("Console")) {
        @Override
        public void act() {
          try {
            if (_f == null) {
              tailFrame_ = FollowApp.openFollowApp(new String[0], true);
            } else {
              tailFrame_ = FollowApp.openFollowApp(new String[]{_f.getAbsolutePath()}, true);
            }
            tailFrame_.setCloseFrameExitJvm(false);
          } catch (final IOException _ioe) {
            FuLog.error(_ioe);
          } catch (final InterruptedException _ie) {
            FuLog.error(_ie);
          } catch (final InvocationTargetException _e) {
            FuLog.error(_e);
          }
        }
      }.start();
    } else {
      tailFrame_.openFile(_f, false, true);
      tailFrame_.show();
    }
  }

  /**
   * Ouvre le tail en java.
   */
  public void openLogFrame() {
    openFileInLogFrame(null);
  }

  /**
   * Affiche un FileChooser.
   *
   * @param _desc la description du type de fichier
   * @param _extension les extensions voulues
   * @return le fichier choisi ou nul si aucun
   */
  public File ouvrirFileChooser(final String _desc, final String[] _extension) {
    return FudaaGuiLib.ouvrirFileChooser(_desc, _extension, getFrame(), false, null);
  }

  /**
   * Affiche un FileChooser.
   *
   * @param _desc la description du type de fichier
   * @param _extension les extensions voulues
   * @param _newFile si le file chooser doit etre ouvert pour choisir un nouveau fichier
   * @return le fichier choisi ou nul si aucun
   */
  @Override
  public File ouvrirFileChooser(final String _desc, final String[] _extension, final boolean _newFile) {
    return FudaaGuiLib.ouvrirFileChooser(_desc, _extension, getFrame(), _newFile, null);
  }

  /**
   * @param _desc la description du type de fichier
   * @param _extension les extensions voulues
   * @param _newFile si le file chooser doit etre ouvert pour choisir un nouveau fichier
   * @param _tester le testeur de fichier
   * @return le fichier selectionne
   */
  public File ouvrirFileChooser(final String _desc, final String[] _extension, final boolean _newFile,
          final CtuluFileChooserFileTester _tester) {
    return FudaaGuiLib.ouvrirFileChooser(_desc, _extension, getFrame(), _newFile, _tester);
  }

  /**
   * Description de la methode.
   */
  @Override
  public final void print() {
    // Systeme d'impression du JDK1.1
    // Ne doit plus etre utiliser dans Fudaa
    FuLog.warning(new Exception("The method FudaaImplementation.print() is deprecated."));
    super.print();
  }

  /**
   * @see org.fudaa.fudaa.commun.FudaaUI#question(java.lang.String, java.lang.String)
   */
  @Override
  public boolean question(final String _titre, final String _text) {
    return CtuluLibDialog.showConfirmation(this.getFrame(), _titre, _text);
  }

  /**
   * Modifie la valeur de l attribut Console pour FudaaImplementation object.
   *
   * @deprecated utiliser setConsoleState
   * @param _ts La nouvelle valeur de Console
   */
  public void setConsole(final JFrame _ts) {
    ts_ = _ts;
    if (ts_ != null) {
      setEnabledForAction("CONSOLE", true);
    }
  }

  /**
   * Ajoute un glasspane �l'application et l'active.
   */
  public void setGlassPaneStop() {
    if (glassPaneStop_ == null) {
      glassPaneStop_ = new BuGlassPaneStop();
      ((JFrame) getFrame()).setGlassPane(glassPaneStop_);
      glassPaneStop_.setVisible(true);
      return;
    }
    final Component glasspane = ((JFrame) getFrame()).getGlassPane();
    if ((glasspane == null) || (glasspane != glassPaneStop_)) {
      ((JFrame) getFrame()).setGlassPane(glassPaneStop_);
    }
    glassPaneStop_.setVisible(true);
  }

  /**
   * Affiche un message d'erreur.
   *
   * @param _msg le message a afficher en rouge
   */
  public void setMainErrorAndClear(final String _msg) {
    setMainMessageAndClear("<html><body style=\"color:#FF0000\">" + _msg + "</body></html>");
  }

  /**
   * Affiche un message dans le main panel. final
   */
  @Override
  public void setMainMessage(final String _s) {
    getMainPanel().setMessage(_s);
  }

  /**
   * @param _msg le message a afficher pendant 2 secondes
   */
  public void setMainMessageAndClear(final String _msg) {
    setMainMessageAndClear(_msg, 3);
  }

  /**
   * Ecrit temporairement un message.
   *
   * @param _msg le message a ecrire
   * @param _nbSec le nombre de seconde avant de l'effacer
   */
  public void setMainMessageAndClear(final String _msg, final int _nbSec) {
    setMainMessage(_msg);
    clearMainMessage(_nbSec);
  }

  @Override
  public void setMainProgression(final int _i) {
    getMainPanel().setProgression(_i);
  }

  /**
   * Desactive le glasspane.
   */
  public void unsetGlassPaneStop() {
    if (SwingUtilities.isEventDispatchThread()) {
      if (glassPaneStop_ != null) {
        glassPaneStop_.setVisible(false);
      }
    } else {
      BuLib.invokeLater(getUnsetGlassPaneRunnable());
    }
  }

  @Override
  public void unsetMainMessage() {
    getMainPanel().setMessage(CtuluLibString.EMPTY_STRING);
    getMainPanel().setProgression(0);
  }

  @Override
  public void unsetMainProgression() {
    getMainPanel().setProgression(0);
  }

  /**
   * @param _titre le titre de l'avertissement
   * @param _text le texte
   */
  public void warn(final String _titre, final String _text) {
    warn(_titre, _text, false);
  }

  /**
   * @param _titre le titre de l'avertissement
   * @param _text le texte
   */
  @Override
  public void warn(final String _titre, final String _text, final boolean _tempo) {
    if (!EventQueue.isDispatchThread()) {
      BuLib.invokeNow(new Runnable() {
        @Override
        public void run() {
          warn(_titre, _text, _tempo);
        }
      });
      return;
    }
    if (_tempo) {
      setMainMessageAndClear(BuResource.BU.getString("Avertissement") + ": " + _text);
    } else {
      CtuluLibDialog.showWarn(this, _titre, _text);
    }
  }

  /**
   * @return the onlyOneApplicationFrame
   */
  public boolean isOnlyOneApplicationFrame() {
    return onlyOneApplicationFrame;
  }

  /**
   * To be used to specify that only one frame will be used by the user.
   *
   * @param onlyOneApplicationFrame the onlyOneApplicationFrame to set
   */
  public void setOnlyOneApplicationFrame(boolean onlyOneApplicationFrame) {
    this.onlyOneApplicationFrame = onlyOneApplicationFrame;
  }
}
