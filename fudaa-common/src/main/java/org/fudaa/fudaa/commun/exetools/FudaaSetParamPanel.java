/*
 * @creation     18 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.exetools;

import com.memoire.bu.BuVerticalLayout;
import java.awt.Dimension;
import java.util.Map;
import java.util.Map.Entry;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluCellRendererRowHeader;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.table.CtuluTableCellEditorProxy;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * Panneau pour renseigner les param�tres d'un utilitaire au lancement.
 * 
 * @author marchand@deltacad.fr
 * @version $Id$
 */
public class FudaaSetParamPanel extends CtuluDialogPanel {

  private CtuluUI ui_;
  private DefaultTableModel mdParams_;
  private JTable tbParams_;
  private Map<Integer,FudaaExeTool.ParamI> params_;

  public FudaaSetParamPanel(CtuluUI _ui) {
    ui_=_ui;
    
    setLayout(new BuVerticalLayout(5, true, true));
    
    mdParams_=new DefaultTableModel(new String[]{FudaaLib.getS("Name"),FudaaLib.getS("Valeur")},0) {
      @Override
      public Class getColumnClass(int _col) {
        if (_col==1) {
          return FudaaExeTool.ParamI.class;
        }
        return super.getColumnClass(_col);
      }

      @Override
      public boolean isCellEditable(int _row, int _col) {
        if (_col==0) return false;
        return true;
      }
    };
    
    tbParams_=new JTable(mdParams_);
    CtuluCellRendererRowHeader rend=new CtuluCellRendererRowHeader();
    rend.setHorizontalAlignment(SwingConstants.LEFT);

    tbParams_.getColumnModel().getColumn(0).setCellRenderer(rend);
    tbParams_.getColumnModel().getColumn(0).setPreferredWidth(50);

    tbParams_.getColumnModel().getColumn(1).setCellRenderer(new CtuluCellTextRenderer() {
      @Override
      public void setValue(Object _param) {
        FudaaExeTool.ParamI param=(FudaaExeTool.ParamI)_param;
        setText(param.getSetCellText());
      }
    });
    tbParams_.getColumnModel().getColumn(1).setPreferredWidth(250);
    
    JScrollPane spParams=new JScrollPane();
    spParams.setPreferredSize(new Dimension(350,180));
    spParams.setViewportView(tbParams_);
    
    add(new JLabel(FudaaLib.getS("Donnez les valeurs des param�tres")));
    add(spParams);
  }
  
  /**
   * Les param�tres � d�finir.
   * @param _params Les param�tres.
   */
  public void setParams(Map<Integer,FudaaExeTool.ParamI> _params) {
    params_=_params;
    TableCellEditor[] editors=new TableCellEditor[_params.size()];
    Object[][] values=new Object[_params.size()][2];
    
    int i=0;
    for (Entry<Integer, FudaaExeTool.ParamI> entry : _params.entrySet()) {
      editors[i]=entry.getValue().getSetCellEditor();
      String name=entry.getValue().name;
      if (name.equals("")) name="P" + entry.getKey();
      values[i][0]=name;
      values[i][1]=entry.getValue();
      mdParams_.addRow(values[i]);
      i++;
    }
    
    CtuluTableCellEditorProxy editor=new CtuluTableCellEditorProxy(editors);
    tbParams_.getColumnModel().getColumn(1).setCellEditor(editor);
  }
  
  @Override
  public boolean isDataValid() {
    if (tbParams_.getCellEditor()!=null)
      tbParams_.getCellEditor().stopCellEditing();
    
    for (FudaaExeTool.ParamI param : params_.values()) {
      if (param.getValSet()==null) {
        setErrorText(FudaaLib.getS("Au moins un des param�tres n'est pas d�fini"));
        return false;
      }
    }
    return true;
  }
}
