/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.commun.exetools;

import java.awt.event.ActionEvent;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.FudaaPreferences;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;

/**
 * Une action pour g�rer les utilitaires externes
 * @author marchand@deltacad.fr
 */
public class FudaaManageExeToolsAction extends EbliActionSimple {
  FudaaCommonImplementation impl_;
  FudaaManageExeTools mng_;
  
  public FudaaManageExeToolsAction(FudaaCommonImplementation _impl, FudaaManageExeTools _mng) {
    super(FudaaLib.getS("G�rer les utilitaires..."), null, "MANAGE_EXE_TOOLS");
    impl_=_impl;
    mng_=_mng;
  }
  
  @Override
  public void actionPerformed(ActionEvent _evt) {
    FudaaManageExeToolsPanel pn=new FudaaManageExeToolsPanel(impl_,mng_);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(impl_.getParentComponent(), FudaaLib.getS("G�rer les utilitaires")))) {
      FudaaExeTool[] tools=pn.getTools();
      mng_.putInPrefs(tools, FudaaPreferences.FUDAA);
      FudaaPreferences.FUDAA.writeIniFile();
    }
  }
}
