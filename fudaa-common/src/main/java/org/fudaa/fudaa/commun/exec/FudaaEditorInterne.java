/**
 *  @creation     30 avr. 2003
 *  @modification $Date: 2006-09-19 15:01:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.exec;

import com.memoire.bu.BuEditorPane;
import com.memoire.bu.BuToolBar;
import com.memoire.fu.FuLog;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.*;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.ressource.FudaaResource;
/**
 * @author deniger
 * @version $Id: FudaaEditorInterne.java,v 1.13 2006-09-19 15:01:53 deniger Exp $
 */
public class FudaaEditorInterne implements FudaaEditorInterface {

  public FudaaEditorInterne() {}

  @Override
  public final boolean isExterne(){
    return false;
  }

  @Override
  public void edit(final File _f){
    String r = null;
    FileInputStream in = null;
    try {
      in = new FileInputStream(_f);
      final int l = in.available();
      final byte[] bs = new byte[l];
      in.read(bs, 0, l);
      //TODO gerer l'encoding
      r = new String(bs);
    }
    catch (final IOException _io) {
      FuLog.warning(_io);
      return;
    }
    finally {
      try {
        if (in != null) {
          in.close();
        }
      }
      catch (final IOException _io) {
        FuLog.warning("can close io for " + _f.getAbsolutePath());
      }
    }
    final BuEditorPane edit = new BuEditorPane();
    edit.setText(r);
    final JFrame f = new JFrame(_f.getAbsolutePath());
    f.getContentPane().add("Center", new JScrollPane(edit));
    
    BuToolBar toolbar=new BuToolBar();
    
    EbliActionSimple actionSave=new EbliActionSimple(FudaaResource.FUDAA.getString("Enregistrer"),EbliResource.EBLI.getIcon("enregistrer"),"SAUVERTEXT"){
      @Override
		public void actionPerformed(ActionEvent _e) {
			String text=edit.getText();
			
			FileOutputStream out = null;
			    try {
			      out = new FileOutputStream(_f);
			      out.write(text.getBytes());
			    }
			    catch (final IOException _io) {
			      FuLog.warning(_io);
			      return;
			    }
			    finally {
			      try {
			        if (out != null) {
			          out.close();
			          
			          JOptionPane.showMessageDialog(null, FudaaResource.FUDAA.getString("Modifications correctements enregistrées"));
			        }
			      }
			      catch (final IOException _io) {
			        FuLog.warning("can close io for " + _f.getAbsolutePath());
			      }
			    }
		}
    };
    //actionSave.setKey(KeyStroke.getKeyStroke(KeyEvent.VK_S,InputEvent.CTRL_DOWN_MASK));
    AbstractButton bouton=actionSave.buildToolButton(EbliComponentFactory.INSTANCE);
    bouton.setMnemonic(KeyEvent.VK_S);
    
    JMenu menu=new JMenu(FudaaResource.FUDAA.getString("Fichier"));
    JMenuItem item=(JMenuItem) actionSave.buildMenuItem(EbliComponentFactory.INSTANCE);
    item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,InputEvent.CTRL_DOWN_MASK));
    menu.add(item);
    JMenuBar barre=new JMenuBar();
    barre.add(menu);
    f.setJMenuBar(barre);
    toolbar.add(bouton);
    toolbar.setFloatable(false);
   // f.getContentPane().add("North",toolbar);
    f.setSize(400, 300);
    f.setVisible(true);
    
  }

  @Override
  public String getCmd(){
    return null;
  }

  @Override
  public String getIDName(){
    return FudaaEditor.INTERNE_EDITOR_VALUE;
  }

  @Override
  public String getName(){
    return FudaaLib.getS("Interne");
  }
}
