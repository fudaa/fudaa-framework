/*
 * @creation 8 f�vr. 07
 * @modification $Date: 2007-03-30 15:37:08 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuResource;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurButton;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.controle.BSelecteurSlider;
import org.fudaa.ebli.controle.BSelecteurSpinner;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author fred deniger
 * @version $Id: FudaaDjaFormTransform.java,v 1.2 2007-03-30 15:37:08 deniger Exp $
 */
public class FudaaDjaFormTransform extends FudaaDjaConfigureAbstract {

  public static final String PROPERTY_ROTATION = "rotation";
  public static final String PROPERTY_SHEAR_X = "shearX";
  public static final String PROPERTY_SHEAR_Y = "shearY";
  public static final String PROPERTY_INIT = "InitAll";

  public FudaaDjaFormTransform(final FudaaDjaFormAbstract _target) {
    super(_target, EbliLib.getS("Transformation"));
  }

  FudaaDjaFormAbstract getIfForm() {
    return (FudaaDjaFormAbstract) target_;
  }

  PropertyChangeSupport listener_ = new PropertyChangeSupport(this);

  @Override
  public void addPropertyChangeListener(String _key, PropertyChangeListener _l) {
    listener_.addPropertyChangeListener(_key, _l);
  }

  @Override
  public void removePropertyChangeListener(String _key, PropertyChangeListener _l) {
    listener_.removePropertyChangeListener(_key, _l);
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    JSlider sl = new JSlider(-180, 180);
    sl.setSnapToTicks(true);
    sl.setPaintTrack(true);
    sl.setPaintLabels(true);
    sl.setMajorTickSpacing(90);
    sl.setMinorTickSpacing(5);
    BSelecteurSlider selecteurSlider = new BSelecteurSlider(PROPERTY_ROTATION, sl);
    selecteurSlider.setTitle(EbliLib.getS("Rotation"));
    BSelecteurSpinner selecteurSliderX = new BSelecteurSpinner(PROPERTY_SHEAR_X, createShearSlider());
    selecteurSliderX.setTitle(EbliLib.getS("cisaillement X"));
    BSelecteurSpinner selecteurSliderY = new BSelecteurSpinner(PROPERTY_SHEAR_Y, createShearSlider());
    selecteurSliderY.setTitle(EbliLib.getS("cisaillement Y"));
    BuButton bt = new BuButton(BuResource.BU.getIcon("reinitialiser"));
    BSelecteurButton bst = new BSelecteurButton(PROPERTY_INIT, bt);
    bst.setTitle(FudaaLib.getS("Annuler les transformations"));
    bt.setToolTipText(bst.getTitle());
    return new BSelecteurInterface[] { selecteurSlider, selecteurSliderX, selecteurSliderY, bst };

  }

  private JSpinner createShearSlider() {
    JSpinner slX = new JSpinner();
    return slX;
  }

  @Override
  public Object getProperty(String _key) {
    if (PROPERTY_ROTATION.equals(_key)) {
      return new Integer((int) Math.toDegrees(getIfForm().getRotation()));
    }
    if (PROPERTY_SHEAR_X.equals(_key)) {
      return new Integer((int) (-getIfForm().getShearX() * 10D));
    }
    // a ameliore
    if (PROPERTY_SHEAR_Y.equals(_key)) {
      return new Integer((int) (getIfForm().getShearY() * 10D));
    }
    return null;
  }

  @Override
  public boolean setProperty(String _key, Object _newProp) {
    boolean r = false;
    if (PROPERTY_ROTATION.equals(_key)) {
      getIfForm().setRotation(Math.toRadians(((Number) _newProp).doubleValue()));
      r = true;
    } else if (PROPERTY_SHEAR_X.equals(_key)) {
      getIfForm().setShearX(-((Number) _newProp).doubleValue() / 10);
      r = true;
    } else if (PROPERTY_SHEAR_Y.equals(_key)) {
      getIfForm().setShearY(((Number) _newProp).doubleValue() / 10);
      r = true;
    } else if (PROPERTY_INIT.equals(_key)) {
      getIfForm().clearTransform();
      listener_.firePropertyChange(PROPERTY_SHEAR_X, true, false);
      listener_.firePropertyChange(PROPERTY_SHEAR_Y, true, false);
      listener_.firePropertyChange(PROPERTY_ROTATION, true, false);
      r = true;
    }
    if (r) {
      target_.getGrid().repaint();
    }
    return r;
  }
}
