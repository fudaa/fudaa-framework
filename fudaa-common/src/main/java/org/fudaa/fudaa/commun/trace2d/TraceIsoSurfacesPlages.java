/*
 * @file         TraceIsoSurfacesPlages.java
 * @creation     1999-01-11
 * @modification $Date: 2006-09-19 15:10:23 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.trace2d;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.util.Vector;
/**
 * Trace d'iso-surfaces avec une palette d�finie par des plages de valeurs.
 * <p>
 * Les plages peuvent �tre r�parties de mani�re non homog�ne. Le trac� accepte
 * �galement une palette � plages non contig�es (avec des trous), ou dont les
 * bornes sont incluses dans l'intervalle min/max des valeurs � tracer.
 *
 * @version      $Revision: 1.4 $ $Date: 2006-09-19 15:10:23 $ by $Author: deniger $
 * @author       Bertrand Marchand
 */
public class TraceIsoSurfacesPlages {
  /**
   * Niveaux de limite des plages. Si 2 plages cons�cutives ne sont pas
   * contig�es, un niveau est ajout� avec comme couleur correspondante
   * la couleur "Autres" de la palette, ou la couleur <i>null</i> si la palette
   * n'affiche pas les couleurs "autres".
   */
  private double[] vniv_;
  /**
   * Couleur associ�e � chaque niveau. Les valeurs inf�rieures au niveau
   * <i>n</i> ont pour couleur la couleur <i>n</i>. La couleur peut �tre
   * <i>null</i> si les plages de la palette ne sont pas contig�es.
   */
  private Color[] cniv_;
  /**
   * Cr�ation du trac� d'isosurface.
   * <p>
   * <b>Remarque :</b> Lors de la cr�ation, le trac� d�fini les niveaux. Il
   * doit �tre recr�� chaque fois que le contenu de la palette est modifi�.
   */
  public TraceIsoSurfacesPlages(BPaletteCouleurPlage _pal) {

    // D�finition des niveaux.
    Vector vvniv= new Vector();
    Vector vcniv= new Vector();
    int nbPlages= _pal.getNbPlages();
    Color colAutres= _pal.isAutresVisible() ? _pal.getCouleurAutres() : null;
    double maxOld= Double.NEGATIVE_INFINITY;

    // Cas d'une palette dont le min et le max sont constants
    if (Math.abs(_pal.getMaxPalette()-_pal.getMinPalette())<1.e-8) {
      double min=_pal.getMinPalette();

      // Ajout eventuel d'un niveau hors plages
      if (min > maxOld) {
        vvniv.add(new Double(min));
        vcniv.add(colAutres);
      }
      maxOld= _pal.getMinPalette()*1.00000001; // On triche sur l'intervalle.

      // Ajout de la plage
      vvniv.add(new Double(maxOld));
      vcniv.add(_pal.getCouleurPlage(0));

      // Ajout d'un niveau +Infini
      vvniv.add(new Double(Double.POSITIVE_INFINITY));
      vcniv.add(colAutres);
    }

    else {
      for (int i= 0; i < nbPlages; i++) {
        double min= _pal.getMinPlage(i);

        // Ajout eventuel d'un niveau hors plages
        if (min > maxOld) {
          vvniv.add(new Double(min));
          vcniv.add(colAutres);
        }
        maxOld= _pal.getMaxPlage(i);

        // Ajout de la plage
        vvniv.add(new Double(maxOld));
        vcniv.add(_pal.getCouleurPlage(i));
      }

      // Ajout d'un niveau +Infini
      vvniv.add(new Double(Double.POSITIVE_INFINITY));
      vcniv.add(colAutres);
    }

    vniv_= new double[vvniv.size()];
    for (int i= 0; i < vvniv.size(); i++)
      vniv_[i]= ((Double)vvniv.get(i)).doubleValue();
    cniv_= new Color[vcniv.size()];
    vcniv.toArray(cniv_);
  }

  public void draw(Graphics _g, Polygon _p, double[] _v) {
    drawX(_g, _p, _v);
  }

  private void drawX(Graphics _g, Polygon _p, double[] _v) {
    if (_p.npoints == 3)
      draw3(_g, _p, _v);
    else if (_p.npoints == 4)
      draw4(_g, _p, _v);
    else if (_p.npoints >= 5)
      draw5(_g, _p, _v);
  }
  private void draw5(Graphics _g, Polygon _p, double[] _v) {
    Polygon p1= new Polygon();
    Polygon p2= new Polygon();
    double[] v1= new double[3];
    double[] v2= new double[_p.npoints - 1];
    p1.addPoint(_p.xpoints[0], _p.ypoints[0]);
    p1.addPoint(_p.xpoints[1], _p.ypoints[1]);
    p1.addPoint(_p.xpoints[_p.npoints - 1], _p.ypoints[_p.npoints - 1]);
    v1[0]= _v[0];
    v1[1]= _v[1];
    v1[2]= _v[_p.npoints - 1];
    for (int i= 1; i < _p.npoints; i++) {
      p2.addPoint(_p.xpoints[i], _p.ypoints[i]);
      v2[i - 1]= _v[i];
    }
    draw3(_g, p1, v1);
    drawX(_g, p2, v2);
  }
  private void draw4(Graphics _g, Polygon _p, double[] _v) {
    Polygon p1= new Polygon();
    Polygon p2= new Polygon();
    p1.addPoint(_p.xpoints[0], _p.ypoints[0]);
    p1.addPoint(_p.xpoints[1], _p.ypoints[1]);
    p1.addPoint(_p.xpoints[3], _p.ypoints[3]);
    p2.addPoint(_p.xpoints[1], _p.ypoints[1]);
    p2.addPoint(_p.xpoints[2], _p.ypoints[2]);
    p2.addPoint(_p.xpoints[3], _p.ypoints[3]);
    double[] v1= new double[3];
    double[] v2= new double[3];
    v1[0]= _v[0];
    v1[1]= _v[1];
    v1[2]= _v[3];
    v2[0]= _v[1];
    v2[1]= _v[2];
    v2[2]= _v[3];
    draw3(_g, p1, v1);
    draw3(_g, p2, v2);
  }
  /**
   * Trac� d'un triangle
   */
  private void draw3(Graphics _g, Polygon _p, double[] _v) {
    int[] xp= new int[5];
    int[] yp= new int[5];
    int np;
    //int      pn=_p.npoints;
    int[] px= _p.xpoints;
    int[] py= _p.ypoints;
    double d;
    // Ordonnancement des valeurs aux 3 points.
    int nmin;
    int nmoy;
    int nmax;
    if (_v[0] > _v[1]) {
      if (_v[0] > _v[2]) {
        nmax= 0;
        if (_v[1] > _v[2]) {
          nmoy= 1;
          nmin= 2;
        } else {
          nmoy= 2;
          nmin= 1;
        }
      } else {
        nmin= 1;
        nmoy= 0;
        nmax= 2;
      }
    } else {
      if (_v[0] < _v[2]) {
        nmin= 0;
        if (_v[1] < _v[2]) {
          nmoy= 1;
          nmax= 2;
        } else {
          nmoy= 2;
          nmax= 1;
        }
      } else {
        nmin= 2;
        nmoy= 0;
        nmax= 1;
      }
    }
    boolean nminInt= false;
    boolean nmoyInt= false;
    boolean nmaxInt= false;
    int n11= -1; // Pour acceptation compil.
    int n12= -1; // Pour acceptation compil.
    int n21= -1; // Pour acceptation compil.
    int n22= -1; // Pour acceptation compil.
    np= -1; // Pour acceptation compil.
    for (int i= 0; i < vniv_.length; i++) {
      if (nmaxInt)
        break; // Le point de valeur max est trac� => Fin de trac�.
      // transfert des points pr�c�dents.
      if (nminInt) {
        int xp1= xp[np - 1];
        int yp1= yp[np - 1];
        int xp2= xp[np - 2];
        int yp2= yp[np - 2];
        xp[0]= xp1;
        yp[0]= yp1;
        xp[1]= xp2;
        yp[1]= yp2;
        np= 2;
      } else
        np= 0;
      if (_v[nmin] < vniv_[i]) {
        if (!nminInt) {
          xp[np]= px[nmin];
          yp[np]= py[nmin];
          np++;
          nminInt= true;
          n11= nmin;
          n12= nmoy;
          n21= nmin;
          n22= nmax;
        }
      }
      if (_v[nmoy] < vniv_[i]) {
        if (!nmoyInt) {
          xp[np]= px[nmoy];
          yp[np]= py[nmoy];
          np++;
          nmoyInt= true;
          n11= nmoy;
          n12= nmax;
        }
      }
      if (_v[nmax] < vniv_[i]) {
        if (!nmaxInt) {
          xp[np]= px[nmax];
          yp[np]= py[nmax];
          np++;
          nmaxInt= true;
        }
      }
      if (nminInt && !nmaxInt) {
        // Points d'intersection
        d= (vniv_[i] - _v[n11]) / (_v[n12] - _v[n11]);
        xp[np]= (int) ((px[n12] - px[n11]) * d + px[n11]);
        yp[np]= (int) ((py[n12] - py[n11]) * d + py[n11]);
        np++;
        d= (vniv_[i] - _v[n21]) / (_v[n22] - _v[n21]);
        xp[np]= (int) ((px[n22] - px[n21]) * d + px[n21]);
        yp[np]= (int) ((py[n22] - py[n21]) * d + py[n21]);
        np++;
      }
      if (nminInt && cniv_[i] != null) {
        _g.setColor(cniv_[i]);
        _g.fillPolygon(xp, yp, np);
        _g.drawPolygon(xp, yp, np);
      }
    }
  }
}
