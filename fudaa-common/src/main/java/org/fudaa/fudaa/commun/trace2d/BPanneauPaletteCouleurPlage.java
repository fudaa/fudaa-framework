/*

 * @file         BPanneauPaletteCouleurPlage.java

 * @creation     1999-07-09

 * @modification $Date: 2006-11-14 09:08:06 $

 * @license      GNU General Public License 2

 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne

 * @mail         devel@fudaa.org

 */
package org.fudaa.fudaa.commun.trace2d;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuResource;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ebli.controle.BSelecteurCouleur;
/**

 * Un panneau permettant la d�finition d'une palette de couleurs.

 *

 * @version      $Id: BPanneauPaletteCouleurPlage.java,v 1.5 2006-11-14 09:08:06 deniger Exp $

 * @author       Bertrand Marchand

 */
public class BPanneauPaletteCouleurPlage extends JPanel {
  private BPaletteCouleurPlage plt_;
  /**

   * Palette utilis�e pour les modifications tant que le bouton Appliquer n'a

   * pas �t� activ�.

   */
  private BPaletteCouleurPlage pltTmp_;
  private boolean selEventSent_;
  private double borneMax_= 1.;
  private double borneMin_= 0.;
  static final DecimalFormat fmt_=
    (DecimalFormat)NumberFormat.getInstance(Locale.US);
  Border bdThis;
  JTabbedPane pnDefinitions= new JTabbedPane();
  JPanel pnCouleurs= new JPanel();
  JPanel pnPlages= new JPanel();
  BuGridLayout lyPlages= new BuGridLayout();
  JPanel pnNbPlages= new JPanel();
  JLabel lbNbPlages= new JLabel();
  JTextField tfNbPlages= new JTextField();
  BuGridLayout lyNbPlages= new BuGridLayout();
  JPanel pnBornes= new JPanel();
  BuGridLayout lyBornes= new BuGridLayout();
  JLabel lbBorneMin= new JLabel();
  JTextField tfBorneMin= new JTextField();
  JLabel lbBorneMax= new JLabel();
  JTextField tfBorneMax= new JTextField();
  TitledBorder bdBornes;
  BuGridLayout lyCouleurs= new BuGridLayout();
  JPanel pnInterpolation= new JPanel();
  JRadioButton rbMinMax= new JRadioButton();
  JRadioButton rbAucune= new JRadioButton();
  TitledBorder bdInterpolation;
  JPanel pnCouleur= new JPanel();
  BSelecteurCouleur slCol= new BSelecteurCouleur();
  TitledBorder bdCouleur;
  BuGridLayout lyInterpolation= new BuGridLayout();
  BuGridLayout lyCouleur= new BuGridLayout();
  Box bxPlages= Box.createVerticalBox();
  JScrollPane spPlages= new JScrollPane();
  JList lsPlages= new JList();
  JLabel lbPlages= new JLabel();
  ButtonGroup bgInterpolation= new ButtonGroup();
  ButtonGroup bgAjust= new ButtonGroup();
  JPanel pnPrincipal= new JPanel();
  BorderLayout lyThis= new BorderLayout();
  JPanel pnButtons= new JPanel();
  JButton btAppliquer= new JButton();
  JPanel pnAjust= new JPanel();
  TitledBorder bdAjust;
  BuGridLayout lyAjust= new BuGridLayout();
  JRadioButton rbAjustAucun= new JRadioButton();
  JRadioButton rbAjustBornes= new JRadioButton();
  JPanel pnTaille= new JPanel();
  GridLayout lyTaille= new GridLayout();
  JComboBox coTailleMin= new JComboBox();
  JComboBox coTailleMax= new JComboBox();
  JLabel lbTailleMinIcon= new JLabel();
  JLabel lbTailleMaxIcon= new JLabel();
  JPanel pnTailleMax= new JPanel();
  JPanel pnTailleMin= new JPanel();
  TitledBorder bdTailleMin;
  TitledBorder bdTailleMax;
  BorderLayout lyTailleMin= new BorderLayout();
  JPanel pnCoTailleMin= new JPanel();
  JPanel pnCoTailleMax= new JPanel();
  BorderLayout lyTailleMax= new BorderLayout();
  BorderLayout lyPrincipal= new BorderLayout();
  /**

   * Constructeur

   */
  public BPanneauPaletteCouleurPlage() {
    super();
    fmt_.applyPattern("#0.0##");
    setPalette(new BPaletteCouleurPlage());
    jbInit();
  }
  /**

   * D�finition de l'IU

   */
  private void jbInit() {
    bdBornes=
      new TitledBorder(
        BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
        "Valeurs");
    bdInterpolation=
      new TitledBorder(
        BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
        "Interpolation des couleurs");
    bdCouleur=
      new TitledBorder(
        BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
        "Couleur de la plage courante");
    bdAjust=
      new TitledBorder(
        BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
        "Ajustement");
    bdTailleMin=
      new TitledBorder(
        BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
        "Taille mini");
    bdTailleMax=
      new TitledBorder(
        BorderFactory.createEtchedBorder(Color.white, new Color(148, 145, 140)),
        "Taille maxi");
    pnPlages.setLayout(lyPlages);
    lyPlages.setColumns(1);
    lyPlages.setHgap(5);
    lyPlages.setVgap(5);
    lbNbPlages.setText("Nb plages");
    tfNbPlages.setMinimumSize(new Dimension(50, 21));
    tfNbPlages.setText("" + pltTmp_.getNbPlages());
    tfNbPlages.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent _evt) {
        tfNbPlages_actionPerformed(_evt);
      }
    });
    pnNbPlages.setLayout(lyNbPlages);
    lyNbPlages.setColumns(2);
    lyNbPlages.setHgap(5);
    pnNbPlages.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    pnNbPlages.setToolTipText("");
    pnBornes.setLayout(lyBornes);
    lyBornes.setColumns(2);
    lyBornes.setHgap(5);
    lyBornes.setVgap(5);
    lbBorneMin.setText("Min");
    lbBorneMax.setText("Max");
    pnBornes.setBorder(bdBornes);
    tfBorneMin.setPreferredSize(new Dimension(50, 21));
    tfBorneMin.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        tfBorneMin_actionPerformed(e);
      }
    });
    tfBorneMax.setPreferredSize(new Dimension(50, 21));
    tfBorneMax.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        tfBorneMax_actionPerformed(e);
      }
    });
    pnCouleurs.setLayout(lyCouleurs);
    lyCouleurs.setColumns(1);
    lyCouleurs.setHgap(5);
    lyCouleurs.setVgap(5);
    rbMinMax.setText("Du min au max");
    rbMinMax.addItemListener(new java.awt.event.ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        rbMinMax_itemStateChanged(e);
      }
    });
    rbAucune.setText("Aucune");
    slCol.setEnabled(false);
    slCol.addPropertyChangeListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(PropertyChangeEvent _evt) {
        slCol_propertyChange(_evt);
      }
    });
    spPlages.setPreferredSize(new Dimension(100, 150));
    this.setLayout(lyThis);
    btAppliquer.setText("Appliquer");
    btAppliquer.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btAppliquer_actionPerformed(e);
      }
    });
    btAppliquer.setEnabled(false);
    btAppliquer.setIcon(BuResource.BU.getIcon("appliquer"));
    bdCouleur.setTitle("Plage courante");
    bdInterpolation.setTitle("Interpolation");
    pnAjust.setBorder(bdAjust);
    pnAjust.setLayout(lyAjust);
    lyAjust.setColumns(1);
    rbAjustAucun.setToolTipText("Aucun ajustement des bornes de la palette");
    rbAjustAucun.setSelected(true);
    rbAjustAucun.setText("Aucun");
    rbAjustBornes.setToolTipText(
      "Ajustement des bornes sur les valeurs min/max du temps courant");
    rbAjustBornes.setText("Sur les min/max");
    rbAjustBornes.addItemListener(new java.awt.event.ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        rbAjustBornes_itemStateChanged(e);
      }
    });
    pnTaille.setLayout(lyTaille);
    coTailleMin.setPreferredSize(new Dimension(45, 21));
    coTailleMin.setEditable(true);
    coTailleMin.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        coTailleMin_actionPerformed(e);
      }
    });
    coTailleMax.setPreferredSize(new Dimension(45, 21));
    coTailleMax.setEditable(true);
    coTailleMax.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        coTailleMax_actionPerformed(e);
      }
    });
    lbTailleMinIcon.setBackground(Color.white);
    lbTailleMinIcon.setBorder(BorderFactory.createLoweredBevelBorder());
    lbTailleMinIcon.setOpaque(true);
    lbTailleMinIcon.setPreferredSize(new Dimension(70, 45));
    lbTailleMinIcon.setHorizontalAlignment(SwingConstants.CENTER);
    lbTailleMaxIcon.setBackground(Color.white);
    lbTailleMaxIcon.setBorder(BorderFactory.createLoweredBevelBorder());
    lbTailleMaxIcon.setOpaque(true);
    lbTailleMaxIcon.setPreferredSize(new Dimension(70, 45));
    lbTailleMaxIcon.setHorizontalAlignment(SwingConstants.CENTER);
    pnTailleMin.setBorder(bdTailleMin);
    pnTailleMin.setLayout(lyTailleMin);
    pnTailleMax.setBorder(bdTailleMax);
    pnTailleMax.setLayout(lyTailleMax);
    lyTaille.setRows(2);
    lyTaille.setColumns(1);
    pnTaille.setToolTipText("Taille des symboles");
    pnPlages.setToolTipText("Plages");
    pnCouleurs.setToolTipText("Couleurs");
    lyPrincipal.setHgap(5);
    pnTailleMax.add(pnCoTailleMax, BorderLayout.WEST);
    pnCoTailleMax.add(coTailleMax, null);
    bgInterpolation.add(rbMinMax);
    bgInterpolation.add(rbAucune);
    rbAucune.setSelected(true);
    pnInterpolation.setBorder(bdInterpolation);
    pnInterpolation.setLayout(lyInterpolation);
    pnCouleur.setBorder(bdCouleur);
    pnCouleur.setLayout(lyCouleur);
    lyInterpolation.setColumns(1);
    pnDefinitions.add(pnPlages, "Plages");
    pnDefinitions.add(pnCouleurs, "Coul.");
    pnDefinitions.add(pnTaille, "Taille");
    pnDefinitions.setToolTipTextAt(0, "Plages");
    pnDefinitions.setToolTipTextAt(1, "Couleurs plages/symboles");
    pnDefinitions.setToolTipTextAt(2, "Taille des symboles");
    pnCouleurs.add(pnInterpolation, null);
    pnInterpolation.add(rbAucune, null);
    pnInterpolation.add(rbMinMax, null);
    pnPlages.add(pnNbPlages, null);
    pnNbPlages.add(lbNbPlages, null);
    pnNbPlages.add(tfNbPlages, null);
    pnPlages.add(pnBornes, null);
    pnBornes.add(lbBorneMin, null);
    pnBornes.add(tfBorneMin, null);
    pnBornes.add(lbBorneMax, null);
    pnBornes.add(tfBorneMax, null);
    pnPlages.add(pnAjust, null);
    pnAjust.add(rbAjustAucun, null);
    pnAjust.add(rbAjustBornes, null);
    pnCouleurs.add(pnCouleur, null);
    pnCouleur.add(slCol, null);
    pnTaille.add(pnTailleMin, null);
    pnTailleMin.add(lbTailleMinIcon, BorderLayout.CENTER);
    pnTailleMin.add(pnCoTailleMin, BorderLayout.WEST);
    pnCoTailleMin.add(coTailleMin, null);
    pnTaille.add(pnTailleMax, null);
    pnTailleMax.add(lbTailleMaxIcon, BorderLayout.CENTER);
    // Liste des plages
    lbPlages.setText("Plages");
    lsPlages.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    lsPlages.setModel(new ListePlagesModel(pltTmp_));
    lsPlages.setCellRenderer(new ListePlagesCellRenderer());
    lsPlages.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent _evt) {
        lsPlages_valueChanged(_evt);
      }
    });
    spPlages.setAlignmentX((float)0.0);
    spPlages.getViewport().add(lsPlages);
    bxPlages.add(lbPlages);
    bxPlages.add(spPlages);
    // Liste des taille
    coTailleMin.addItem("0");
    coTailleMin.addItem("5");
    coTailleMin.addItem("10");
    coTailleMin.addItem("15");
    coTailleMin.addItem("25");
    coTailleMin.addItem("40");
    coTailleMin.addItem("60");
    coTailleMax.addItem("0");
    coTailleMax.addItem("5");
    coTailleMax.addItem("10");
    coTailleMax.addItem("15");
    coTailleMax.addItem("25");
    coTailleMax.addItem("40");
    coTailleMax.addItem("60");
    bdThis=
      BorderFactory.createCompoundBorder(
        new EtchedBorder(
          EtchedBorder.LOWERED,
          Color.white,
          new Color(134, 134, 134)),
        BorderFactory.createEmptyBorder(5, 5, 5, 5));
    pnPrincipal.setLayout(lyPrincipal);
    pnPrincipal.setBorder(bdThis);
    pnPrincipal.add(bxPlages, BorderLayout.CENTER);
    pnPrincipal.add(pnDefinitions, BorderLayout.EAST);
    this.add(pnPrincipal, BorderLayout.CENTER);
    this.add(pnButtons, BorderLayout.SOUTH);
    pnButtons.add(btAppliquer, null);
    bgAjust.add(rbAjustAucun);
    bgAjust.add(rbAjustBornes);
  }
  /**

   * Affectation d'une palette de couleurs au panneau. Une copie locale de la

   * palette est cr��e de facon � ne modifier la palette que lorsque le bouton

   * Appliquer est activ�.

   */
  public void setPalette(BPaletteCouleurPlage _pal) {
    plt_= _pal;
    pltTmp_= (BPaletteCouleurPlage)plt_.clone();
    lsPlages.setModel(new ListePlagesModel(pltTmp_));
    rbAucune.setSelected(true);
    rbAjustAucun.setSelected(true);
    tfNbPlages.setText("" + pltTmp_.getNbPlages());
    int nb= _pal.getNbPlages();
    int tmin= _pal.getTaillePlage(0);
    int tmax= _pal.getTaillePlage(nb - 1);
    lbTailleMinIcon.setIcon(
      new SymboleIcon(_pal.getSymbolePlage(0), _pal.getCouleurPlage(0)));
    lbTailleMaxIcon.setIcon(
      new SymboleIcon(
        _pal.getSymbolePlage(nb - 1),
        _pal.getCouleurPlage(nb - 1)));
    coTailleMin.setSelectedItem("" + tmin);
    coTailleMax.setSelectedItem("" + tmax);
    btAppliquer.setEnabled(false);
  }
  /**

   * Retourne la copie locale de la palette associ�e au panneau.

   * La palette n'est pas modifi�e tant que le bouton Appliquer n'a pas �t�

   * activ�.

   */
  public BPaletteCouleurPlage getPalette() {
    return plt_;
  }
  /**

   * Definit la borne min des valeurs pour l'ajustement.

   */
  public void setBorneMin(double _borne) {
    borneMin_= _borne;
  }
  /**

   * Retourne la borne min des valeurs pour l'ajustement.

   */
  public double getBorneMin() {
    return borneMin_;
  }
  /**

   * Definit la borne max des valeurs pour l'ajustement.

   */
  public void setBorneMax(double _borne) {
    borneMax_= _borne;
  }
  /**

   * Retourne la borne max des valeurs pour l'ajustement.

   */
  public double getBorneMax() {
    return borneMax_;
  }
  /**

   * D�finit l'activit� ou non du panneau taille.

   */
  public void setPanneauTailleEnabled(boolean _b) {
    pnDefinitions.setEnabledAt(pnDefinitions.indexOfTab("Taille"), _b);
    if (!_b
      && pnDefinitions.getSelectedIndex() == pnDefinitions.indexOfTab("Taille"))
      pnDefinitions.setSelectedIndex(0);
  }
  /**

   * Retourne l'activit� ou non du panneau taille.

   */
  public boolean isPanneauTailleEnabled() {
    return pnDefinitions.isEnabledAt(pnDefinitions.indexOfTab("Taille"));
  }
  /**

   * Changement de la palette associ�e

   */
  //  public void propertyChange(PropertyChangeEvent _evt) {
  //    btAppliquer.setEnabled(true);
  //  }
  //----------------------------------------------------------------------------
  // Actions
  //----------------------------------------------------------------------------
  /**

   * Le nombre de plages a �t� modifi�.

   */
  void tfNbPlages_actionPerformed(ActionEvent _evt) {
    try {
      int i= Integer.parseInt(tfNbPlages.getText());
      i= Math.min(i, 25);
      int tmin= pltTmp_.getTaillePlage(0);
      int tmax= pltTmp_.getTaillePlage(pltTmp_.getNbPlages() - 1);
      pltTmp_.setNbPlages(i);
      pltTmp_.propagerTailles(tmin, tmax);
      ((SymboleIcon)lbTailleMinIcon.getIcon()).setCouleur(
        pltTmp_.getCouleurPlage(0));
      ((SymboleIcon)lbTailleMaxIcon.getIcon()).setCouleur(
        pltTmp_.getCouleurPlage(pltTmp_.getNbPlages() - 1));
      lsPlages.setModel(new ListePlagesModel(pltTmp_));
      btAppliquer.setEnabled(true);
      lsPlages.repaint();
    } catch (NumberFormatException _exc) {}
  }
  /**

   * Une plage a �t� s�lectionn�e, ou aucune.

   */
  void lsPlages_valueChanged(ListSelectionEvent _evt) {
    if (_evt.getValueIsAdjusting())
      return;
    BPaletteCouleurPlage.Plage plg;
    plg= (BPaletteCouleurPlage.Plage)lsPlages.getSelectedValue();
    // Ev�nement de s�lection envoy�
    selEventSent_= true;
    slCol.setEnabled(plg != null);
    tfBorneMin.setEnabled(plg != null);
    tfBorneMax.setEnabled(plg != null);
    if (plg != null) {
      tfBorneMin.setText("" + fmt_.format(plg.min));
      tfBorneMax.setText("" + fmt_.format(plg.max));
      slCol.setCouleur(plg.couleur);
    }
  }
  /**

   * La borne min de la plage a �t� modifi�e.

   */
  void tfBorneMin_actionPerformed(ActionEvent _evt) {
    try {
      double val= Double.parseDouble(tfBorneMin.getText());
      pltTmp_.setMinPlage(lsPlages.getSelectedIndex(), val);
      pltTmp_.ajusteLegendes();
      rbAjustAucun.setSelected(true);
      btAppliquer.setEnabled(true);
      lsPlages.repaint();
    } catch (NumberFormatException _exc) {}
  }
  /**

   * La borne max de la plage a �t� modifi�e.

   */
  void tfBorneMax_actionPerformed(ActionEvent _evt) {
    try {
      double val= Double.parseDouble(tfBorneMax.getText());
      pltTmp_.setMaxPlage(lsPlages.getSelectedIndex(), val);
      pltTmp_.ajusteLegendes();
      rbAjustAucun.setSelected(true);
      btAppliquer.setEnabled(true);
      lsPlages.repaint();
    } catch (NumberFormatException _exc) {}
  }
  /**

   * La couleur a �t� modifi�e pour la plage donn�e.

   */
  void slCol_propertyChange(PropertyChangeEvent _evt) {
    if (_evt.getPropertyName().equals("couleur")) {
      // La couleur a �t� modifi�e suite � un evenement de s�lection
      // => Pas d'action.
      if (selEventSent_) {
        selEventSent_= false;
        return;
      }
      rbAucune.setSelected(true);
      int ind= lsPlages.getSelectedIndex();
      Color c= (Color)_evt.getNewValue();
      pltTmp_.setCouleurPlage(ind, c);
      if (ind == 0)
         ((SymboleIcon)lbTailleMinIcon.getIcon()).setCouleur(c);
      if (ind == pltTmp_.getNbPlages() - 1)
         ((SymboleIcon)lbTailleMaxIcon.getIcon()).setCouleur(c);
      btAppliquer.setEnabled(true);
      lsPlages.repaint();
    }
  }
  /**

   * Radio bouton s�lectionn�.

   * => Interpolation entre les 2 couleurs extr�mes.

   */
  void rbMinMax_itemStateChanged(ItemEvent _evt) {
    if (_evt.getStateChange() == ItemEvent.DESELECTED)
      return;
    pltTmp_.propagerCouleurs(
      pltTmp_.getCouleurPlage(0),
      pltTmp_.getCouleurPlage(pltTmp_.getNbPlages() - 1));
    btAppliquer.setEnabled(true);
    lsPlages.repaint();
  }
  /**

   * Bouton ajustement des bornes palettes s�lectionn�.

   * => Envoi d'un �venement pour r�ajuster la palette depuis le calque

   */
  void rbAjustBornes_itemStateChanged(ItemEvent _evt) {
    if (_evt.getStateChange() == ItemEvent.DESELECTED)
      return;
    pltTmp_.setMinPalette(borneMin_);
    pltTmp_.setMaxPalette(borneMax_);
    pltTmp_.ajustePlages();
    pltTmp_.ajusteLegendes();
    lsPlages.getSelectionModel().clearSelection();
    btAppliquer.setEnabled(true);
    lsPlages.repaint();
  }
  /**

   * Bouton appliquer activ�.

   */
  void btAppliquer_actionPerformed(ActionEvent _evt) {
    plt_= pltTmp_;
    pltTmp_= (BPaletteCouleurPlage)plt_.clone();
    lsPlages.setModel(new ListePlagesModel(pltTmp_));
    btAppliquer.setEnabled(false);
    firePropertyChange("palette", null, this);
  }
  /**

   * Changement de taille min des symboles.

   */
  void coTailleMin_actionPerformed(ActionEvent e) {
    try {
      int tmin= Integer.parseInt((String)coTailleMin.getSelectedItem());
      ((SymboleIcon)lbTailleMinIcon.getIcon()).setTaille(tmin);
      pltTmp_.setTaillePlage(0, tmin);
      int tmax= Integer.parseInt((String)coTailleMax.getSelectedItem());
      if (tmin > tmax)
        coTailleMax.setSelectedItem("" + tmin);
    } catch (NumberFormatException _exc) {}
    btAppliquer.setEnabled(true);
    lbTailleMinIcon.repaint();
  }
  /**

   * Changement de taille max des symboles.

   */
  void coTailleMax_actionPerformed(ActionEvent e) {
    try {
      int tmax= Integer.parseInt((String)coTailleMax.getSelectedItem());
      ((SymboleIcon)lbTailleMaxIcon.getIcon()).setTaille(tmax);
      pltTmp_.setTaillePlage(plt_.getNbPlages() - 1, tmax);
      int tmin= Integer.parseInt((String)coTailleMin.getSelectedItem());
      if (tmin > tmax)
        coTailleMin.setSelectedItem("" + tmax);
    } catch (NumberFormatException _exc) {}
    btAppliquer.setEnabled(true);
    lbTailleMaxIcon.repaint();
  }
  //----------------------------------------------------------------------------
  // Classes internes
  //----------------------------------------------------------------------------
  /**

   * Modele pour la liste de plages

   */
  class ListePlagesModel extends AbstractListModel {
    private BPaletteCouleurPlage pal_= null;
    public ListePlagesModel(BPaletteCouleurPlage _pal) {
      pal_= _pal;
    }
    @Override
    public int getSize() {
      return pal_.getNbPlages();
    }
    @Override
    public Object getElementAt(int i) {
      return pal_.plages_[i];
    }
  }
  /**

   * Affichage des plages dans la liste des plages.

   */
  class ListePlagesCellRenderer extends JLabel implements ListCellRenderer {
    //    JPanel pn_;
    //    JLabel lb_;
    //    public ListePlagesCellRenderer() {
    //      BuGridLayout lyThis=new BuGridLayout(2,5,0,false,false);
    //      lb_=new JLabel();
    //      pn_=new JPanel();
    //      this.setBorder(BorderFactory.createEmptyBorder(0,5,0,5));
    //      this.setLayout(lyPrincipal);
    //      this.add(pn_);
    //      this.add(lb_);
    //    }
    @Override
    public Component getListCellRendererComponent(
      JList list,
      Object value,
      int index,
      boolean isSelected,
      boolean cellHasFocus) {
      BPaletteCouleurPlage.Plage plg= (BPaletteCouleurPlage.Plage)value;
      setText(fmt_.format(plg.min) + " � " + fmt_.format(plg.max));
      setIcon(new SymboleIcon(plg.symbole, plg.couleur, 15));
      //      pn_.setBackground(plg.couleur);
      if (isSelected) {
        this.setOpaque(true);
        this.setBackground(list.getSelectionBackground());
        this.setForeground(list.getSelectionForeground());
        //	lb_.setBackground(list.getSelectionBackground());
        //	lb_.setForeground(list.getSelectionForeground());
      } else {
        this.setOpaque(false);
        this.setBackground(list.getBackground());
        this.setForeground(list.getForeground());
        //	lb_.setBackground(list.getBackground());
        //	lb_.setForeground(list.getForeground());
      }
      return this;
    }
  }
  /**

   * Pour test.

   */
  public static void main(String[] args) {
    try {
      //      UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
      UIManager.setLookAndFeel(
        "com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
    } catch (Exception _exc) {}
    JFrame f= new JFrame("Palette");
    BPaletteCouleurPlage plt= new BPaletteCouleurPlage();
    plt.setSymboleDefaut(new Symbole(Symbole.FLECHE, -90));
    plt.setNbPlages(5);
    BPanneauPaletteCouleurPlage pn= new BPanneauPaletteCouleurPlage();
    pn.setPalette(plt);
    //    pn.setPanneauTailleEnabled(false);
    f.getContentPane().add(pn, BorderLayout.CENTER);
    f.pack();
    f.setVisible(true);
  }
}
