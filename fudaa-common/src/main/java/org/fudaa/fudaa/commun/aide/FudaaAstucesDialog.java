/*
 * @file         FudaaAstucesDialog.java
 * @creation     2001-09-01
 * @modification $Date: 2007-05-04 13:58:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.aide;
import com.memoire.bu.*;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import org.fudaa.fudaa.commun.FudaaAstucesAbstract;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.ressource.FudaaResource;
/**
 * Fenetre dialohue pour afficher les astuces.
 *
 * @version      $Revision: 1.11 $ $Date: 2007-05-04 13:58:05 $ by $Author: deniger $
 * @author       Fred Deniger
 */
public class FudaaAstucesDialog extends BuDialog {
  /**
   * CheckBox pour valider ou non l'apparition des astuces.
   */
  private final JCheckBox cbShowNextTime_;
  /**
   * Le panel d'affichage.
   */
  private final FudaaAstucesPanel textePanel_;
  /**
   * La chaine correspondant a fermer.
   */
  private final static String FERMER= FudaaLib.getS("Fermer");
  /**
   * L'icone fermer.
   */
  private final static BuIcon BOUTON_FERMER= FudaaResource.FUDAA.getIcon("fermer");
  /**
   * L'icone suivant:nouvelle astuce.
   */
  private final static BuIcon BOUTON_SUIVANT=FudaaResource.FUDAA.getIcon("avancer");
  /**
   * La chaine correspondant a afficher une autre astuce.
   */
  private final static String SUIVANT= FudaaLib.getS("Astuce suivante");
  /**
   * Chaine pour ne plus voir les astuces.
   */
  private final static String VOIR_ASTUCE=
    FudaaLib.getS("Toujours afficher les astuces");
  /**
   * Les preferences utilisees.
   */
  private final BuPreferences options_;
  /**
   * initialisation de textPanel_ et affichage.
   *
   * @param _parent
   * @param _isoft
   * @param _message
   */
  public FudaaAstucesDialog(
    final BuPreferences _options,
    final BuCommonInterface _parent,
    final BuInformationsSoftware _isoft,
    final FudaaAstucesPanel _message) {
    super(_parent, _isoft, FudaaLib.getS("Astuces"), _message);
    options_= _options;
    textePanel_= _message;
    cbShowNextTime_= new BuCheckBox(VOIR_ASTUCE);
    cbShowNextTime_.setSelected(
      FudaaAidePreferencesPanel.isAstucesVisibles(options_));
    final BuButton btFermer= new BuButton(BOUTON_FERMER, FERMER);
    btFermer.setActionCommand("FERMER");
    getRootPane().setDefaultButton(btFermer);
    final BuButton btSuivant= new BuButton(BOUTON_SUIVANT, SUIVANT);
    btSuivant.setActionCommand("SUIVANT");
    final BuPanel sousBoutons= new BuPanel();
    sousBoutons.setLayout(new BuHorizontalLayout(5, true, true));
    sousBoutons.add(btSuivant);
    sousBoutons.add(btFermer);
    final BuPanel boutons= new BuPanel();
    boutons.setLayout(new BuBorderLayout(5, 0));
    boutons.add(sousBoutons, BuBorderLayout.EAST);
    boutons.add(cbShowNextTime_, BuBorderLayout.WEST);
    btSuivant.addActionListener(this);
    btFermer.addActionListener(this);
    content_.add(boutons, BorderLayout.SOUTH);
    pack();
  }
  /**
   * Fonction appel�e lors de la fermeture du dialogue.
   */
  public void fermer() {
    reponse_= JOptionPane.CLOSED_OPTION;
    FudaaAidePreferencesPanel.setAstucesVisibles(
      options_,
      cbShowNextTime_.isSelected());
    dispose();
  }
  /**
   * Gestion des actions sur la croix.
   *
   * @param _e
   */
  @Override
  protected void processWindowEvent(final WindowEvent _e) {
    if (_e.getID() == WindowEvent.WINDOW_CLOSING) {
      fermer();
    }
  }
  /**
   * retourne la valeur Component de FudaaAstucesDialog object.
   *
   * @return   La valeur Component
   */
  @Override
  public JComponent getComponent() {
    return textePanel_;
  }
  /**
   * Gestion des evenements sur les boutons fermer et suivant.
   *
   * @param _evt
   */
  @Override
  public void actionPerformed(final ActionEvent _evt) {
    final String source= _evt.getActionCommand();
    if ("SUIVANT".equals(source)) {
      reponse_= JOptionPane.OK_OPTION;
      textePanel_.changeTexte();
    }
    if ("FERMER".equals(source)) {
      fermer();
    }
  }
  /**
   * Affiche la boite de dialogue de FudaaAstuces.
   */
  public static int showDialog(
    final BuPreferences _pref,
    final BuCommonImplementation _app,
    final FudaaAstucesAbstract _astuces) {
    return new FudaaAstucesDialog(
      _pref,
      _app,
      _app.getInformationsSoftware(),
      new FudaaAstucesPanel(_astuces))
      .activate();
  }
}
