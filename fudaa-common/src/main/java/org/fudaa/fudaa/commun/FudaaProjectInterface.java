/*
 * @creation 30 avr. 07
 * @modification $Date: 2008-01-21 08:50:47 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun;

import java.io.File;

/**
 * Une interface pour un projet d'application.
 * 
 * @author fred deniger
 * @version $Id: FudaaProjectInterface.java,v 1.1.6.1 2008-01-21 08:50:47 bmarchan Exp $
 */
public interface FudaaProjectInterface {

  /**
   * Retourne la date de derni�re sauvegarde du projet sur fichier.
   * @return La date sous forme de chaine. <code>null</code> si le projet n'a pas �t� sauvegard�.
   */
  String getLastSaveDate();

  /**
   * Retourne le titre � afficher dans le bandeau de la fenetre application. Le plus souvent le nom
   * de l'appli + le nom du fichier projet.
   * @return Le titre.
   */
  String getFrameTitle();

  /**
   * Retourne le titre du projet d'�tude renseign� par l'utilisateur.
   * @return Le titre de l'�tude.
   */
  String getTitle();

  /**
   * Le fichier projet associ� au projet.
   * 
   * @return Le fichier projet, ou <code>null</code> si le projet n'a pas encore �t� sauvegard�, ou lu
   * a partir d'un fichier.
   */
  File getParamsFile();

}
