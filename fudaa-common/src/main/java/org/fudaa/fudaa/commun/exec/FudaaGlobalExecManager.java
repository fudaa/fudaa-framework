/*
 *  @file         FudaaExecManager.java
 *  @creation     28 mai 2003
 *  @modification $Date: 2006-09-19 15:01:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.exec;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
/**
 * Permet de g�rer tous les executables des applications Fudaa.
 * 
 * @author deniger
 * @version $Id: FudaaGlobalExecManager.java,v 1.7 2006-09-19 15:01:53 deniger Exp $
 */
public class FudaaGlobalExecManager {

  protected ArrayList                        execs_;

  public FudaaGlobalExecManager() {
    loadList();
  }

  public int getFudaaExecIndex(final FudaaExec _name) {
    return execs_.indexOf(_name);
  }

  /**
   * @param _n l'identifiant de l'executable.
   */
  private int getFudaaExecIndex(final String _name) {
    return FudaaExec.getIdNameIndex(execs_, _name);
  }

  public FudaaExec getFudaaExec(final String _name) {
    final int i = getFudaaExecIndex(_name);
    return i >= 0 ? (FudaaExec) execs_.get(i) : null;
  }

  public List getAllExe() {
    return new ArrayList(execs_);
  }

  public int getFudaaExecNb() {
    return execs_.size();
  }

  protected FudaaExec getFudaaExec(final int _i) {
    return (FudaaExec) execs_.get(_i);
  }

  private void loadList() {
    execs_ = new ArrayList(15);
    final int prefLength = FudaaExec.KEY_PREFIXE.length();
    final int exeLength = FudaaExec.EXE.length();
    for (final Enumeration e = FudaaExec.PREF.keys(); 
    e.hasMoreElements();
    ) {
      final String k = (String) e.nextElement();
      if ((k.startsWith(FudaaExec.KEY_PREFIXE)) && (k.endsWith(FudaaExec.EXE))) {
        final String name = k.substring(prefLength + 1, k.length() - exeLength - 1);
        final FudaaExec exe = 
        FudaaExec.loadFromPref(name);
        execs_.add(exe);
      }
    }
  }

  public void reloadFromPref() {
    for (int i = getFudaaExecNb() - 1; i >= 0; i--) {
      getFudaaExec(i).reloadFromPref();
    }
  }

  public void savePref() {
    final int n = execs_.size() - 1;
    for (int i = n; i >= 0; i--) {
      savePref(i);
    }
  }

  public boolean contains(final String _id) {
    return FudaaExec.getIdNameIndex(execs_, _id) >= 0;
  }

  public boolean contains(final FudaaExec _ex) {
    return execs_.contains(_ex);
  }

  public void writePref() {
    FudaaExec.PREF.writeIniFile();
  }

  public void savePref(final int _i) {
    getFudaaExec(_i).savePref();
  }

  public void savePref(final FudaaExec _ex) {
    final int i = getFudaaExecIndex(_ex);
    if (i >= 0) {
      savePref(i);
    }
  }

  @Override
  public String toString() {
    final int n = execs_.size();
    final StringBuffer buf = new StringBuffer(n * 10);
    for (int i = 0; i < n; i++) {
      buf.append(getFudaaExec(i).getIDName());
      if (i != n - 1) {
        buf.append(',');
      }
    }
    return buf.toString();
  }

 

  public void setNewValues(final List _newVal) {
    final int n = execs_.size() - 1;
    for (int i = n; i >= 0; i--) {
      final FudaaExec ex = getFudaaExec(i);
      final int index = _newVal.indexOf(ex);
      if (index < 0) {
        ex.razPref();
      }
    }
    execs_ = new ArrayList(_newVal);
    savePref();
  }

  public ComboBoxModel createComboBoxModel() {
    return new ExecComboBoxModel();
  }

  class ExecComboBoxModel
  extends AbstractListModel
  implements ComboBoxModel {

    private transient Object selected_;

    @Override
    public Object getElementAt(final int _i) {
      return execs_.get(_i);
    }

    @Override
    public int getSize() {
      return execs_.size();
    }

    @Override
    public Object getSelectedItem() {
      return selected_;
    }

    @Override
    public void setSelectedItem(final Object _anItem) {
      if ((selected_ == null && _anItem != null)
      || (selected_ != null && !selected_.equals(_anItem))) {
        selected_ = _anItem;
        fireContentsChanged(this, -1, -1);
      }
    }
  }

}
