/**
 *  @creation     4 mai 2004
 *  @modification $Date: 2004-07-22 16:27:34 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.BuPreferences;
import java.awt.Frame;


/**
 * @author Fred Deniger
 * @version $Id: FudaaDodicoImplementationInterface.java,v 1.2 2004-07-22 16:27:34 deniger Exp $
 */
public interface FudaaDodicoImplementationInterface {
  
  /**
   * @return la fenetre de l'imp
   */
  Frame getFrame();
  /**
   * @param _s le message a afficher
   */
  void setMainMessage(String _s);
  
  /**
   * Initialiser les messages principaux
   */
  void unsetMainMessage();
  /**
   * @param _s le messafe a afficher et a effacer apres 2sec
   */
  void setMainMessageAndClear(String _s);
  /**
   * @param _p la progression
   */
  void setMainProgression(int _p);
  /**
   * Initialiser la barre de progression principale
   */
  void unsetMainProgression();
  /**
   * @return les pref de l'appli
   */
  BuPreferences getApplicationPreferences();

}
