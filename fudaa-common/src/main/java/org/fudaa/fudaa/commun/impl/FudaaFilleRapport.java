/*
 * @file         FudaaFilleRapport.java
 * @creation     1998-10-15
 * @modification $Date: 2007-06-05 09:01:16 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuCutCopyPasteInterface;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuResource;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import org.fudaa.ebli.impression.EbliFilleImprimable;
import org.fudaa.ebli.impression.EbliPrinter;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * Fenetre fille pour l'edition du rapport.
 * 
 * @version $Revision: 1.2 $ $Date: 2007-06-05 09:01:16 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class FudaaFilleRapport extends EbliFilleImprimable implements BuCutCopyPasteInterface {
  private FudaaEditeurRapport editeur_;
  private JComponent content_;
  private JMenu mnFormat_;
  private JMenu mnAlignement_;
  private JMenu mnPolice_;
  private JMenu mnStyle_;
  private JMenu mnTaille_;
  private BuButton btLeft_;
  private BuButton btCenter_;
  private BuButton btRight_;
  private BuButton btSansSerif_;
  private BuButton btSerif_;
  private BuButton btMonospaced_;
  private BuButton btBold_;
  private BuButton btItalic_;
  private BuButton btUnderlined_;
  private BuInformationsDocument id_;

  public FudaaFilleRapport(final BuCommonInterface _app) {
    super("", true, false, true, true, _app, null);
    id_ = new BuInformationsDocument();
    setName("ifRAPPORT");
    editeur_ = new FudaaEditeurRapport();
    editeur_.setEditable(true);
    editeur_.setDefaultBorder();
    editeur_.setDefaultStyle();
    /*
     * JMenuBar menuBar = new JMenuBar(); JMenu fileMenu = new JMenu("Fichier"); JMenuItem ouvrirItem = new
     * JMenuItem("Ouvrir"); JMenuItem sauvegarderItem = new JMenuItem("Sauvegarder"); fileMenu.add(ouvrirItem);
     * fileMenu.add(sauvegarderItem); menuBar.add(fileMenu);
     */
    mnStyle_ = new JMenu(FudaaLib.getS("Style"));
    final JMenuItem boldItem = new JMenuItem(FudaaLib.getS("Gras"));
    final JMenuItem italicItem = new JMenuItem(FudaaLib.getS("Italique"));
    final JMenuItem underlineItem = new JMenuItem(FudaaLib.getS("Soulign�"));
    final String sans = "SansSerif";
    boldItem.setFont(new Font(sans, Font.BOLD, 12));
    italicItem.setFont(new Font(sans, Font.ITALIC, 12));
    underlineItem.setFont(new Font(sans, Font.PLAIN, 12));
    mnStyle_.add(boldItem);
    mnStyle_.add(italicItem);
    mnStyle_.add(underlineItem);
    boldItem.addActionListener(editeur_.getCommand("font-bold"));
    italicItem.addActionListener(editeur_.getCommand("font-italic"));
    underlineItem.addActionListener(editeur_.getCommand("font-underline"));
    mnTaille_ = new JMenu(FudaaLib.getS("Taille"));
    final JMenuItem size08Item = new JMenuItem("8 points");
    final JMenuItem size10Item = new JMenuItem("10 points");
    final JMenuItem size12Item = new JMenuItem("12 points");
    final JMenuItem size14Item = new JMenuItem("14 points");
    final JMenuItem size16Item = new JMenuItem("16 points");
    final JMenuItem size18Item = new JMenuItem("18 points");
    final JMenuItem size24Item = new JMenuItem("24 points");
    size08Item.setFont(new Font(sans, Font.PLAIN, 8));
    size10Item.setFont(new Font(sans, Font.PLAIN, 10));
    size12Item.setFont(new Font(sans, Font.PLAIN, 12));
    size14Item.setFont(new Font(sans, Font.PLAIN, 14));
    size16Item.setFont(new Font(sans, Font.PLAIN, 16));
    size18Item.setFont(new Font(sans, Font.PLAIN, 18));
    size24Item.setFont(new Font(sans, Font.PLAIN, 24));
    mnTaille_.add(size08Item);
    mnTaille_.add(size10Item);
    mnTaille_.add(size12Item);
    mnTaille_.add(size14Item);
    mnTaille_.add(size16Item);
    mnTaille_.add(size18Item);
    mnTaille_.add(size24Item);
    size08Item.addActionListener(editeur_.getCommand("font-size-8"));
    size10Item.addActionListener(editeur_.getCommand("font-size-10"));
    size12Item.addActionListener(editeur_.getCommand("font-size-12"));
    size14Item.addActionListener(editeur_.getCommand("font-size-14"));
    size16Item.addActionListener(editeur_.getCommand("font-size-16"));
    size18Item.addActionListener(editeur_.getCommand("font-size-18"));
    size24Item.addActionListener(editeur_.getCommand("font-size-24"));
    mnPolice_ = new JMenu(FudaaLib.getS("Police"));
    final JMenuItem sansSerifItem = new JMenuItem(FudaaLib.getS("Sans s�rif"));
    final JMenuItem serifItem = new JMenuItem(FudaaLib.getS("S�rif"));
    final JMenuItem monospacedItem = new JMenuItem(FudaaLib.getS("Fixe"));
    sansSerifItem.setFont(new Font(sans, Font.PLAIN, 12));
    serifItem.setFont(new Font("Serif", Font.PLAIN, 12));
    monospacedItem.setFont(new Font("Monospaced", Font.PLAIN, 12));
    mnPolice_.add(sansSerifItem);
    mnPolice_.add(serifItem);
    mnPolice_.add(monospacedItem);
    sansSerifItem.addActionListener(editeur_.getCommand("font-family-SansSerif"));
    sansSerifItem.addActionListener(editeur_.getCommand("font-family-Serif"));
    monospacedItem.addActionListener(editeur_.getCommand("font-family-Monospaced"));
    mnAlignement_ = new BuMenu(FudaaLib.getS("Alignement"), "ALIGNEMENT");
    final JMenuItem leftItem = new JMenuItem(FudaaLib.getS("Gauche"));
    final JMenuItem centerItem = new JMenuItem(FudaaLib.getS("Centre"));
    final JMenuItem rightItem = new JMenuItem(FudaaLib.getS("Droite"));
    leftItem.setHorizontalTextPosition(SwingConstants.RIGHT);
    centerItem.setHorizontalTextPosition(SwingConstants.RIGHT);
    rightItem.setHorizontalTextPosition(SwingConstants.RIGHT);
    leftItem.setIcon(BuResource.BU.getIcon("alignementgauche"));
    centerItem.setIcon(BuResource.BU.getIcon("alignementcentre"));
    rightItem.setIcon(BuResource.BU.getIcon("alignementdroite"));
    mnAlignement_.add(leftItem);
    mnAlignement_.add(centerItem);
    mnAlignement_.add(rightItem);
    leftItem.addActionListener(editeur_.getCommand("left-justify"));
    centerItem.addActionListener(editeur_.getCommand("center-justify"));
    rightItem.addActionListener(editeur_.getCommand("right-justify"));
    btLeft_ = new BuButton();
    btCenter_ = new BuButton();
    btRight_ = new BuButton();
    btLeft_.setToolTipText(FudaaLib.getS("Aligner � gauche"));
    btCenter_.setToolTipText(FudaaLib.getS("Centrer"));
    btRight_.setToolTipText(FudaaLib.getS("Aligner � droite"));
    btLeft_.setIcon(BuResource.BU.getIcon("alignementgauche"));
    btCenter_.setIcon(BuResource.BU.getIcon("alignementcentre"));
    btRight_.setIcon(BuResource.BU.getIcon("alignementdroite"));
    btLeft_.addActionListener(editeur_.getCommand("left-justify"));
    btCenter_.addActionListener(editeur_.getCommand("center-justify"));
    btRight_.addActionListener(editeur_.getCommand("right-justify"));
    btSansSerif_ = new BuButton();
    btSerif_ = new BuButton();
    btMonospaced_ = new BuButton();
    btSansSerif_.setToolTipText(FudaaLib.getS("Sans s�rif"));
    btSerif_.setToolTipText(FudaaLib.getS("S�rif"));
    btMonospaced_.setToolTipText(FudaaLib.getS("Fixe"));
    btSansSerif_.setIcon(BuResource.BU.getIcon("fontesansserif"));
    btSerif_.setIcon(BuResource.BU.getIcon("fonteserif"));
    btMonospaced_.setIcon(BuResource.BU.getIcon("fontefixed"));
    btSansSerif_.addActionListener(editeur_.getCommand("font-family-SansSerif"));
    btSansSerif_.addActionListener(editeur_.getCommand("font-family-Serif"));
    btMonospaced_.addActionListener(editeur_.getCommand("font-family-Monospaced"));
    btBold_ = new BuButton();
    btItalic_ = new BuButton();
    btUnderlined_ = new BuButton();
    btBold_.setToolTipText(FudaaLib.getS("Gras"));
    btItalic_.setToolTipText(FudaaLib.getS("Italique"));
    btUnderlined_.setToolTipText(FudaaLib.getS("Soulign�"));
    btBold_.setIcon(BuResource.BU.getIcon("stylegras"));
    btItalic_.setIcon(BuResource.BU.getIcon("styleitalique"));
    btUnderlined_.setIcon(BuResource.BU.getIcon("stylesouligne"));
    btBold_.addActionListener(editeur_.getCommand("font-bold"));
    btItalic_.addActionListener(editeur_.getCommand("font-italic"));
    btUnderlined_.addActionListener(editeur_.getCommand("font-underline"));
    mnFormat_ = new JMenu(FudaaLib.getS("Format"));
    mnFormat_.add(mnAlignement_);
    mnFormat_.add(mnPolice_);
    mnFormat_.add(mnStyle_);
    mnFormat_.add(mnTaille_);
    // les deux actions ajoutees :
    // les noms ("ouvrir" par ex) sont donnees dans les constructeurs des
    // classes associees (voir class OuvrirAction)
    // ouvrirItem.addActionListener( (Action) commandes.get("ouvrir") );
    // sauvegarderItem.addActionListener( (Action)commandes.get("sauvegarder") );
    final JScrollPane sp = new JScrollPane(editeur_);
    sp.setBorder(null);
    content_ = (JComponent) getContentPane();
    content_.setLayout(new BuBorderLayout());
    content_.add(sp, BuBorderLayout.CENTER);
    // JScrollPane scrollPane = new JScrollPane();
    // JViewport vp = scrollPane.getViewport();
    // vp.add(editeur);
    // vp.setBackingStoreEnabled(true);
    // panel.add(scrollPane, BorderLayout.CENTER);
    // frame.setContentPane(panel);
    setTitle(FudaaLib.getS("Rapport"));
    setFrameIcon(BuResource.BU.getIcon("texte"));
    final Dimension d = new Dimension(340, 230);
    setPreferredSize(d);
    setSize(d);
  }

  public void insereIcone(final Icon _icon) {
    editeur_.insereIcone(_icon);
  }

  public void insereIcone(final Icon _icon, final String _text) {
    editeur_.insereIcone(_icon, _text);
  }

  /*
   * public void insereIcone(String _name, Icon _icon, String _text, int _align) {
   * editeur_.insereIcone(_name,_icon,_text,_align); }
   */
  public void insereEnteteEtude(final BuInformationsDocument _id) {
    if (_id != null) {
      id_ = _id;
    }
    editeur_.insereEnteteEtude(_id);
  }

  // BuInternalFrame
  @Override
  public String[] getEnabledActions() {
    return new String[] { "IMPRIMER", "MISEENPAGE", "PREVISUALISER", "COUPER", "COPIER", "COLLER", "DUPLIQUER" };
  }

  @Override
  public JComponent[] getSpecificTools() {
    final JComponent[] r = new JComponent[11];
    r[0] = btLeft_;
    r[1] = btCenter_;
    r[2] = btRight_;
    r[3] = null;
    r[4] = btSansSerif_;
    r[5] = btSerif_;
    r[6] = btMonospaced_;
    r[7] = null;
    r[8] = btBold_;
    r[9] = btItalic_;
    r[10] = btUnderlined_;
    return r;
  }

  @Override
  public JMenu[] getSpecificMenus() {
    final JMenu[] r = new JMenu[1];
    r[0] = mnFormat_;
    return r;
  }

  // BuCutCopyPasteInterface
  @Override
  public void cut() {
    final Action c = editeur_.getCommand("cut-to-clipboard");
    c.actionPerformed(null);
    editeur_.repaint(); // probleme swing...
  }

  @Override
  public void copy() {
    final Action c = editeur_.getCommand("copy-to-clipboard");
    c.actionPerformed(null);
  }

  @Override
  public void duplicate() {
    cut();
    copy();
    copy();
  }

  @Override
  public void paste() {
    final Action c = editeur_.getCommand("paste-from-clipboard");
    c.actionPerformed(null);
  }

  // BuPrintable
  /*
   * public void print(PrintJob _job,Graphics _g) { BuPrinter.INFO_DOC =new BuInformationsDocument();
   * BuPrinter.INFO_DOC.name=getTitle(); BuPrinter.INFO_DOC.logo=BuResource.BU.getIcon("texte");
   * BuPrinter.printString(_job,_g,editeur_.getText()); }
   */
  @Override
  public int getNumberOfPages() {
    return 1;
  }

  @Override
  public int print(final Graphics _g, final PageFormat _format, final int _numPage) {
    if (_numPage != 0) {
      return Printable.NO_SUCH_PAGE;
    }
    if (editeur_.getSource() == null) {
      System.err.println("probl�me avec FudaaFilleRapport" + "editeur_.getSource()==null");
      return Printable.NO_SUCH_PAGE;
    }
    EbliPrinter.printString(_g, _format, editeur_.getSource());
    return Printable.PAGE_EXISTS;
  }

  @Override
  public BuInformationsDocument getInformationsDocument() {
    return id_;
  }

  public FudaaEditeurRapport getEditeur() {
    return editeur_;
  }
}
