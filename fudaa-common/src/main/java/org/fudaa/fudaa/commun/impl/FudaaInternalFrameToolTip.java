/*
 * @creation 5 mars 07
 * @modification $Date: 2007-03-07 17:25:07 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.BuInternalFrame;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JToolTip;
import org.fudaa.ctulu.CtuluLibImage;

/**
 * @author fred deniger
 * @version $Id: FudaaInternalFrameToolTip.java,v 1.1 2007-03-07 17:25:07 deniger Exp $
 */
public class FudaaInternalFrameToolTip extends JToolTip {

  int w_ = 200;

  BuInternalFrame if_;

  public FudaaInternalFrameToolTip() {
    setDoubleBuffered(false);
    setOpaque(true);
  }

  @Override
  public Dimension getPreferredSize() {
    return new Dimension(w_, w_);
  }

  @Override
  protected void paintComponent(Graphics _g) {
    super.paintComponent(_g);
    _g.setColor(getBackground());
    _g.fillRect(1, 1, w_ - 1, w_ - 1);
    FontMetrics m = _g.getFontMetrics();
    int hFont = m.getHeight();
    int marge = 3;
    int h = (w_ - hFont - marge-1);
    int w = h;
    CtuluLibImage.setBestQuality((Graphics2D) _g);
    String title = if_.getTitle();
    int margeGauche = (w_ - w) / 2;
    //on dessine les rectangles
    _g.setColor(Color.WHITE);
    _g.fillRect(margeGauche, 1, w-2, hFont+1);
    _g.fillRect(margeGauche, hFont + marge+1, w - 2, h - 2);
    _g.setColor(getForeground());
    _g.drawRect(margeGauche, 1, w-2, hFont+1);
   
    
    _g.drawString(title, (w_ - m.stringWidth(title)) / 2, m.getAscent()+1);
    if_.setDoubleBuffered(false);
    int top = hFont + marge+1;
    _g.setClip(margeGauche, top, w , h );
    BufferedImage produceImageForComponent = CtuluLibImage.produceImageForComponent(if_.getContentPane(), w - 1, h - 1,
        null);
    
    _g.drawImage(produceImageForComponent, margeGauche, top, w - 1, h - 1, null);
    _g.drawRect(margeGauche, top, w - 2, h - 2); 
    

  }

  protected void setIf(BuInternalFrame _if) {
    if_ = _if;
  }

}
