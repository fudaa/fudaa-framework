/*
 * @creation 30 avr. 07
 * @modification $Date: 2007-04-30 14:22:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun;

import java.awt.EventQueue;
import java.io.File;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

/**
 * @author fred deniger
 * @version $Id: FudaaProjectStateSupportLabelListener.java,v 1.1 2007-04-30 14:22:43 deniger Exp $
 */
public class FudaaProjectStateSupportLabelListener extends FudaaProjectStateLabelListener {

  final JLabel lbDate_;
  final JInternalFrame lbTitle_;
  final JLabel lbFile_;

  public FudaaProjectStateSupportLabelListener(final JLabel _lbModified, final JLabel _lbDate) {
    this(_lbModified, _lbDate, null, null);

  }

  public FudaaProjectStateSupportLabelListener(final JLabel _lbModified, final JLabel _lbDate,
      final JInternalFrame _lbTitle, final JLabel _lbFile) {
    super(_lbModified);
    lbDate_ = _lbDate;
    lbTitle_ = _lbTitle;
    lbFile_ = _lbFile;

  }

  @Override
  public void finishUpdate(final FudaaProjetStateInterface _proj) {

  }

  @Override
  public void initWith(final FudaaProjetStateInterface _l) {
    _l.addListener(this);
    projectStateChanged(_l);

  }

  @Override
  public void projectStateChanged(final FudaaProjetStateInterface _proj) {
    if (EventQueue.isDispatchThread()) {
      doChange(_proj);
    } else {
      SwingUtilities.invokeLater(new Runnable() {

        @Override
        public void run() {
          doChange(_proj);
        }

      });
    }

  }

  FudaaProjectInterface support_;

  @Override
  void doChange(final FudaaProjetStateInterface _proj) {
    super.doChange(_proj);

    if (lbDate_ != null) {
      String date = support_ == null ? null : support_.getLastSaveDate();
      if (date == null) {
        date = FudaaLib.getS("Non enregistré");
      }
      lbDate_.setText(date);
    }
    if (lbTitle_ != null) {
      final String title = support_ == null ? null : support_.getTitle();
      if ((lbTitle_.getTitle() == null) || (!lbTitle_.getTitle().equals(title))) {
        lbTitle_.setTitle(title);
      }
    }
    if (lbFile_ != null) {
      final File f = support_ == null ? null : support_.getParamsFile();
      String fTitle = "?";
      if (f != null) {
        fTitle = f.getAbsolutePath();
      }
      lbFile_.setText(fTitle);
    }
    finishUpdate(_proj);
  }

  public FudaaProjectInterface getSupport() {
    return support_;
  }

  public void setSupport(final FudaaProjectInterface _support) {
    support_ = _support;
  }

}
