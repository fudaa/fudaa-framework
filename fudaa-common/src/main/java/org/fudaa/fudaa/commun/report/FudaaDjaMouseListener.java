/*
 * @creation 14 f�vr. 07
 * @modification $Date: 2007-03-23 17:25:30 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.dja.DjaForm;
import com.memoire.dja.DjaGridEvent;
import com.memoire.dja.DjaLib;
import com.memoire.dja.DjaLibDetect;
import com.memoire.dja.DjaLink;
import com.memoire.dja.DjaMouseListener;
import com.memoire.dja.DjaObject;
import com.memoire.dja.DjaOwner;
import com.memoire.dja.DjaText;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import javax.swing.JViewport;

/**
 * @author fred deniger
 * @version $Id: FudaaDjaMouseListener.java,v 1.4 2007-03-23 17:25:30 deniger Exp $
 */
public class FudaaDjaMouseListener extends DjaMouseListener {

  Dimension oldGridPreferredSize_;

  private void updateGridPreferredSize() {
    oldGridPreferredSize_ = grid_.getSize();
  }

  protected void scrollpaneChanged(DjaOwner _owner) {
    if (_owner instanceof DjaObject) {
      JViewport view = ((FudaaDjaGrid) grid_).getPane().getViewport();
      Rectangle rec = view.getViewRect();
      int w = rec.width;
      int h = rec.height;
      DjaObject obj = (DjaObject) _owner;
      boolean mustRevalidate = false;
      if (obj.getX() < rec.x) {
        rec.x = obj.getX();
        mustRevalidate = true;
      } else if (obj.getX() + obj.getWidth() > rec.x + w) {
        rec.x = obj.getX() + obj.getWidth() - w;
        mustRevalidate = true;
      }
      if (obj.getY() < rec.y) {
        rec.y = obj.getY();
        mustRevalidate = true;
      } else if (obj.getY() + obj.getHeight() > rec.y + h) {
        rec.y = obj.getY() + obj.getHeight() - h;
        mustRevalidate = true;
      }
     // boolean canMove = oldGridPreferredSize_ != null 
      ////     && oldGridPreferredSize_.width >= rec.x + w && oldGridPreferredSize_.height >= rec.y + h;
      if (mustRevalidate) {
        grid_.revalidate();
        // updateGridPreferredSize();
      }
     // if (canMove) view.scrollRectToVisible(rec);
      /*
       * rec.x = Math.max(0, rec.x); rec.y = Math.max(0, rec.y); view.scrollRectToVisible(rec);
       */

    }

  }

  public FudaaDjaMouseListener(FudaaDjaGrid _grid) {
    super(_grid);
  }

  @Override
  public void mouseClicked(MouseEvent _evt) {
    Object o = DjaLibDetect.detect(grid_, _evt.getX(), _evt.getY());
    if (linkbegin_ || linkend_) return;
    if (o instanceof DjaText) {
      if (_evt.getClickCount() != 2) return;
      FudaaReportTextConfigure.afficheHtmlDialog(grid_, (DjaText) o);

    } else
      super.mouseClicked(_evt);
  }

  @Override
  public void mousePressed(MouseEvent _evt) {
    int x = _evt.getX();
    int y = _evt.getY();
    Object o = DjaLibDetect.detect(grid_, x, y);

    grid_.requestFocus();
    if (o instanceof DjaText && !_evt.isControlDown() && (((DjaText) o).getParent() != null)) {
      o = ((DjaText) o).getParent();
    }
    mousePressed(_evt, x, y, o);

  }

  @Override
  public void mouseReleased(MouseEvent _evt) {
    super.mouseReleased(_evt);
    updateGridPreferredSize();
    grid_.revalidate();
    
  }

  @Override
  public void mouseDragged(MouseEvent _evt) {
    boolean erase = true;

    int x = _evt.getX();
    int y = _evt.getY();

    Rectangle r = null;

    if (handle_ != null) {
      r = mouseDraggedHandle(x, y);
      // grid_.repaint(f.getExtendedBounds());
    } else if (control_ != null) {
      r = DjaLib.getDirtyArea(control_.getParent());
      // grid_.repaint(control_.getParent().getExtendedBounds());
      control_.draggedTo(x, y);
      r = DjaLib.getDirtyArea(control_.getParent()).union(r);
      // grid_.repaint(control_.getParent().getExtendedBounds());
    } else if (text_ != null && _evt.isControlDown() && text_.isMoveable()) {
      r = text_.getBounds();
      // grid_.repaint(text_.getBounds());
      Point p = new Point(lastx_ + x - lastdx_, lasty_ + y - lastdy_);
      // DjaLib.snap(p);
      text_.setLocation(p);
      text_.setSelected(true);
      r = text_.getBounds().union(r);
      // grid_.repaint(text_.getBounds());
    } else if (last_ != null) {
      if (last_ instanceof DjaForm) {
        if (!last_.isSelected()) {
         /* DjaForm f = (DjaForm) last_;
          r = DjaLib.getDirtyArea(f);
          // Rectangle r=f.getExtendedBounds();
          f.setX(x + lastdx_);
          f.setY(y + lastdy_);
          r = DjaLib.getDirtyArea(f).union(r);*/
          // grid_.repaint(f.getExtendedBounds().union(r));
        } else {
          grid_.moveSelection(x - lastx_ + lastdx_, y - lasty_ + lastdy_);
          lastx_ = last_.getX();
          lasty_ = last_.getY();
        }
      } else if (last_ instanceof DjaLink) {
        DjaLink a = (DjaLink) last_;
        r = DjaLib.getDirtyArea(a);
        // grid_.repaint(a.getExtendedBounds());
        if (linkend_) {
          a.setEndObject(null);
          a.setEndX(x + lastdx_);
          a.setEndY(y + lastdy_);
        } else if (linkbegin_) {
          a.setBeginObject(null);
          a.setBeginX(x + lastdx_);
          a.setBeginY(y + lastdy_);
        } else {
          a.setX(x + lastdx_);
          a.setY(y + lastdy_);
        }
        r = DjaLib.getDirtyArea(a).union(r);
        // grid_.repaint(a.getExtendedBounds());
      }
    } else
      erase = true;

    if (r != null) grid_.repaint(r);
    if (last_ != null) {
      scrollpaneChanged(last_);
    }

    Graphics g = grid_.getGraphics();
    g.setXORMode(selectionZone);

    if (last_ == null) {
      if (erase) g.drawRect(lastx_, lasty_, lastw_, lasth_);
      lastw_ = x - lastx_;
      lasth_ = y - lasty_;
      g.drawRect(lastx_, lasty_, lastw_, lasth_);
      g.setPaintMode();
    }
    else{
      grid_.fireGridEvent(last_, DjaGridEvent.MODIFIED);
    }
  }

  private Rectangle mouseDraggedHandle(int _x, int _y) {
    Rectangle r;
    DjaForm f = (DjaForm) handle_.getParent();
    r = DjaLib.getDirtyArea(f);
    switch (handle_.getO()) {
    case NORTH:
      f.setY(lasty_ + Math.min(lasth_, _y - lastdy_));
      f.setHeight(Math.max(0, lasth_ - _y + lastdy_));
      break;
    case WEST:
      f.setX(lastx_ + Math.min(lastw_, _x - lastdx_));
      f.setWidth(Math.max(0, lastw_ - _x + lastdx_));
      break;
    case EAST:
      f.setWidth(Math.max(0, lastw_ + _x - lastdx_));
      break;
    case SOUTH:
      f.setHeight(Math.max(0, lasth_ + _y - lastdy_));
      break;
    case NORTH_WEST:
      f.setX(lastx_ + Math.min(lastw_, _x - lastdx_));
      f.setY(lasty_ + Math.min(lasth_, _y - lastdy_));
      f.setWidth(Math.max(0, lastw_ - _x + lastdx_));
      f.setHeight(Math.max(0, lasth_ - _y + lastdy_));
      break;
    case NORTH_EAST:
      f.setY(lasty_ + Math.min(lasth_, _y - lastdy_));
      f.setWidth(Math.max(0, lastw_ + _x - lastdx_));
      f.setHeight(Math.max(0, lasth_ - _y + lastdy_));
      break;
    case SOUTH_EAST:
      f.setWidth(Math.max(0, lastw_ + _x - lastdx_));
      f.setHeight(Math.max(0, lasth_ + _y - lastdy_));
      break;
    case SOUTH_WEST:
      f.setX(lastx_ + Math.min(lastw_, _x - lastdx_));
      f.setWidth(Math.max(0, lastw_ - _x + lastdx_));
      f.setHeight(Math.max(0, lasth_ + _y - lastdy_));
      break;
    default:
      break;
    }

    r = DjaLib.getDirtyArea(f).union(r);
    return r;
  }

  @Override
  protected void adjustCursor(int _x, int _y, boolean _shift, boolean _ctrl) {

    super.adjustCursor(_x, _y, _shift, _ctrl);

    Object o = DjaLibDetect.detect(grid_, _x, _y);

    if (o instanceof DjaText && !_ctrl && !_shift) {
      Cursor c = Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR);
      if (grid_.getCursor() != c) grid_.setCursor(c);

    }
  }

}
