/*
 * @file         FudaaPreferences.java
 * @creation     1998-12-23
 * @modification $Date: 2005-08-11 09:48:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun;

import com.memoire.bu.BuPreferences;

/**
 * La classe de preferences pour Fudaa.
 *
 * @version      $Revision: 1.10 $ $Date: 2005-08-11 09:48:25 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class FudaaPreferences extends FudaaPreferencesAbstract {
  /**
   * Les prefs du projet Fudaa
   */
  public final static BuPreferences FUDAA;
  
  static {
    if (root_==null) {
      FUDAA= new FudaaPreferences();
    }
    else
      FUDAA=BU;
  }
  protected FudaaPreferences() {
    
  }
}
