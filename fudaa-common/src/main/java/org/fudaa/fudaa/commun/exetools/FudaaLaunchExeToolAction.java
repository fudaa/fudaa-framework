/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.commun.exetools;

import java.awt.event.ActionEvent;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import org.fudaa.ctulu.CtuluTaskOperationAbstract;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.dodico.objet.CExec;
import org.fudaa.dodico.objet.CExecListener;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;

/**
 * Lance un utilitaire externe
 * @author marchand@deltacad.fr
 */
public class FudaaLaunchExeToolAction extends EbliActionSimple {
  FudaaCommonImplementation impl_;
  FudaaExeTool tool_;
  FudaaManageExeTools mng_;
  
  public FudaaLaunchExeToolAction(FudaaCommonImplementation _impl, FudaaManageExeTools _mng, FudaaExeTool _tool) {
    super(_tool.name, null, "LAUNCH_EXE_TOOL");
    tool_=_tool;
    impl_=_impl;
    mng_=_mng;
  }
  
  @Override
  public void actionPerformed(ActionEvent _evt) {
    // Controle de l'ex�cutable et du r�pertoire d'ex�cution
    if (!tool_.exePath.exists()) {
      impl_.error(FudaaLib.getS("Erreur"),FudaaLib.getS("L'ex�cutable '{0}' n'existe pas",tool_.exePath));
      return;
    }
    if (!tool_.exeDirPath.exists()) {
      impl_.error(FudaaLib.getS("Erreur"),FudaaLib.getS("Le r�pertoire d'ex�cution '{0}' n'existe pas",tool_.exeDirPath));
      return;
    }
    
    // Les seuls param�tres variables � afficher au moment du lancement
    int i=0;
    final Map<Integer, FudaaExeTool.ParamI> params=new HashMap<Integer, FudaaExeTool.ParamI>();
    for (FudaaExeTool.ParamI param : tool_.getParams()) {
      if (param.mustBeSet()) {
        params.put(i+1,param);
      }
      i++;
    }
    
    if (params.size()!=0) {
      FudaaSetParamPanel pn=new FudaaSetParamPanel(impl_);
      pn.setParams(params);
      if (!pn.afficheModaleOk(impl_.getParentComponent(),FudaaLib.getS("Param�tres d'ex�cution de {0}",tool_.name))) return;
    }
    

    final FudaaOutputPanel pnOut=new FudaaOutputPanel(impl_,null);
    final CtuluDialog di=pnOut.createDialog(impl_.getParentComponent());
    di.setOption(CtuluDialog.CANCEL_OPTION);
    di.setDefaultCloseOperation(CtuluDialog.DO_NOTHING_ON_CLOSE);
    di.setTitle(FudaaLib.getS("Progression de {0}...",tool_.name));

    CtuluTaskOperationAbstract th=new CtuluTaskOperationAbstract("Execution") {
      Process proc;
      
      @Override
      public void act() {
        try {
          // Traitement �ventuel des param�tres avant lancement
          for (FudaaExeTool.ParamI param : params.values()) {
            if (!param.prepareLaunch(pnOut)) {
              return;
            }
          }

          String[] cmd=new String[tool_.getParams().length + 1];
          cmd[0]=tool_.exePath.getPath();
          StringBuilder sb=new StringBuilder();
          sb.append(cmd[0]);
          int i=1;
          for (FudaaExeTool.ParamI param : tool_.getParams()) {
            cmd[i]=param.getValParam();
            sb.append(" ").append(cmd[i++]);
          }
          pnOut.appendDetailln(FudaaLib.getS("Ex�cution de la commande '{0}'", sb.toString()));
          CExec process=new CExec(cmd);
          process.setListener(new CExecListener() {

            @Override
            public void setProcess(Process _p) {
              proc=_p;
            }
          });
          process.setExecDirectory(tool_.exeDirPath);
          process.setOutStream(new PrintStream(pnOut.getOutputStream()));
          process.setErrStream(new PrintStream(pnOut.getOutputStream()));
          process.exec();
        }
        finally {
          di.setOption(CtuluDialog.OK_OPTION);
        }
      }

      @Override
      public ProgressionInterface getStateReceiver() {
        return pnOut;
      }

      @Override
      public ProgressionInterface getMainStateReceiver() {
        return pnOut;
      }

      @Override
      public void requestStop() {
        super.requestStop();
        proc.destroy();
        pnOut.appendDetailln("La tache a �t� interrompue");
      }
      
      
    };
    
    pnOut.setTask(th);
    th.start();
    di.afficheDialogModal();

    // Dans le cas ou il y a eu interruption de la part de l'utilisateur, on s'arrete.
    if (th.isStopRequested()) {
      return;
    }

    // Traitement �ventuel des param�tres apr�s lancement.
    for (FudaaExeTool.ParamI param : tool_.getParams()) {
      param.postLaunch(pnOut);
    }
  }
}
