/*
 * @creation 9 mars 07
 * @modification $Date: 2007-03-15 17:01:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.dja.DjaAnchor;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author fred deniger
 * @version $Id: FudaaDjaCercle.java,v 1.1 2007-03-15 17:01:09 deniger Exp $
 */
public class FudaaDjaCercle extends FudaaDjaEllipse {

  @Override
  public void setWidth(int _w) {
    super.setHeight(_w);
    super.setWidth(_w);
  }

  @Override
  public FudaaDjaFormAbstract createNew() {
    return new FudaaDjaCercle();
  }
  
  @Override
  public String getDescription() {
    return FudaaLib.getS("Cercle");
  }

  @Override
  public String getDjaName() {
    return "Circle";
  }

  @Override
  public void setHeight(int _h) {
    super.setWidth(_h);
    super.setHeight(_h);
  }

  @Override
  public DjaAnchor[] getAnchors() {
    int x = getX();
    int y = getY();
    int w = getWidth();
    int h = getHeight();

    DjaAnchor[] r = new DjaAnchor[8];

    r[0] = new DjaAnchor(this, 0, NORTH, x + w / 2, y);
    r[2] = new DjaAnchor(this, 2, EAST, x + w - 1, y + h / 2);
    r[4] = new DjaAnchor(this, 4, SOUTH, x + w / 2, y + h - 1);
    r[6] = new DjaAnchor(this, 6, WEST, x, y + h / 2);

    r[1] = new DjaAnchor(this, 1, NORTH, x + w - 1 - 295 * w / 2000, y + 295 * h / 2000);
    r[3] = new DjaAnchor(this, 3, SOUTH, x + w - 1 - 295 * w / 2000, y + h - 1 - 295 * h / 2000);
    r[5] = new DjaAnchor(this, 5, SOUTH, x + 295 * w / 2000, y + h - 1 - 295 * h / 2000);
    r[7] = new DjaAnchor(this, 7, NORTH, x + 295 * w / 2000, y + 295 * h / 2000);

    return r;
  }

}
