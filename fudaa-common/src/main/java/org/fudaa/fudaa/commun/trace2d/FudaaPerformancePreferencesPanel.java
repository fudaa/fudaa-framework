/*
 * @file         FudaaPreferencesPanel.java
 * @creation     2001-09-13
 * @modification $Date: 2008-01-15 11:29:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.trace2d;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuVerticalLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * Panneau de preferences pour l'aide dans Fudaa. Gere l'apparition des astuces.
 *
 * @version $Revision: 1.12 $ $Date: 2008-01-15 11:29:55 $ by $Author: bmarchan $
 * @author Fred Deniger
 */
public class FudaaPerformancePreferencesPanel extends BuAbstractPreferencesPanel implements ActionListener {

  /**
   * La checkbox controlant l'aparition des astuces au demarrage.
   */
  private final BuCheckBox cbUseOpenGl;
  /**
   * L'application concernee.
   */
  BuCommonImplementation app_;
  /**
   * Les options.
   */
  BuPreferences options_ = BuPreferences.BU;
  /**
   * Les noms des preferences utilisees.
   */
  private static final String USE_OPENGL = "openGl.active";

  /**
   * Renvoie
   * <code>true</code> si les astuces sont declares comme visibles au demarrage dans les preferences
   * <code>_options</code>.
   */
  public static boolean isOpenGLActive() {
    return BuPreferences.BU.getBooleanProperty(USE_OPENGL, true);
  }

  public static void applyOptimization() {
    boolean active = isOpenGLActive();
    System.setProperty("sun.java2d.opengl", active ? "true" : "false");

  }

  /**
   * Initialise la bordure utilisee par les panels principaux: panel du format de page et le panel des entetes et pieds de page.
   *
   * @param _titre le titre ajoute a la bordure.
   */
  private Border initTitreBorder(final String _titre) {
    return BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(FudaaLib.getS(_titre)), BorderFactory
            .createEmptyBorder(5, 5, 5, 5));
  }

  /**
   * Initialisation de l'application et des composant swing.
   *
   * @param _app
   */
  public FudaaPerformancePreferencesPanel(final BuCommonImplementation _app) {
    super();
    setLayout(new BuVerticalLayout(5, true, true));
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    app_ = _app;
    // astuces
    final BuPanel optimizationPanel = new BuPanel();
    optimizationPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    optimizationPanel.setBorder(initTitreBorder("Optimisation graphique"));
    cbUseOpenGl = new BuCheckBox(FudaaLib.getS("Activer l'accélération"));
    optimizationPanel.add(cbUseOpenGl, BuBorderLayout.CENTER);
    cbUseOpenGl.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        setDirty(cbUseOpenGl.isSelected() != isOpenGLActive());
      }
    });
    add(optimizationPanel);
    updateComponents();
  }

  private void fillTable() {

    options_.putBooleanProperty(USE_OPENGL, cbUseOpenGl.isSelected());
  }

  private void updateComponents() {
    cbUseOpenGl.setSelected(options_.getBooleanProperty(USE_OPENGL, true));
  }

  /**
   * Le titre du panel.
   *
   * @return "Fudaa"
   */
  @Override
  public String getTitle() {
    return FudaaLib.getS("Optimisation graphique");
  }

  /**
   * @return true
   */
  @Override
  public boolean isPreferencesValidable() {
    return true;
  }

  /**
   * @return false
   */
  @Override
  public boolean isPreferencesCancelable() {
    return true;
  }

  /**
   * Gestion des evenements.
   *
   * @param _evt
   */
  @Override
  public void actionPerformed(final ActionEvent _evt) {
  }

  /**
   * Enregistrement des modification apportees dans ce panel.
   */
  @Override
  public void validatePreferences() {
    fillTable();
    System.setProperty("sun.java2d.opengl", cbUseOpenGl.isSelected() ? "true" : "false");
    options_.writeIniFile();
    setDirty(false);
    updateComponents();
  }

  /**
   * Annuler les modifications.
   */
  @Override
  public void cancelPreferences() {
    options_.readIniFile();
    setDirty(false);
    updateComponents();
  }
}
