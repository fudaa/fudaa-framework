/*
 * @creation 30 avr. 07
 * @modification $Date: 2007-04-30 14:22:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun;

import java.awt.EventQueue;
import java.awt.Frame;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.CtuluLibString;

/**
 * @author fred deniger
 * @version $Id: FudaaProjectStateFrameListener.java,v 1.1 2007-04-30 14:22:43 deniger Exp $
 */
public class FudaaProjectStateFrameListener implements FudaaProjectStateListener {

  final Frame f_;
  final FudaaProjectInterface support_;

  public FudaaProjectStateFrameListener(final Frame _f, final FudaaProjectInterface _support) {
    super();
    f_ = _f;
    support_ = _support;
  }

  public void initWith(final FudaaProjetStateInterface _l) {
    _l.addListener(this);
    projectStateChanged(_l);

  }

  @Override
  public void projectStateChanged(final FudaaProjetStateInterface _proj) {
    if (EventQueue.isDispatchThread()) {
      doChange(_proj);
    } else {
      SwingUtilities.invokeLater(new Runnable() {

        @Override
        public void run() {
          doChange(_proj);
        }

      });
    }

  }

  void doChange(final FudaaProjetStateInterface _proj) {
    String end = CtuluLibString.EMPTY_STRING;
    if (_proj.isParamsModified()) {
      end = " *";
    } else if (_proj.isUIModified()) {
      end = " +";
    }
    f_.setTitle(support_.getFrameTitle() + end);
  }

}
