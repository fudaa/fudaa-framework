/*
 * @creation 8 f�vr. 07
 * @modification $Date: 2007-03-15 17:01:12 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.Map;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.animation.EbliAnimationSourceInterface;

class FudaaReportAnimationAdapter implements EbliAnimationSourceInterface {

  final FudaaReportFrameController controller_;

  public FudaaReportAnimationAdapter(final FudaaReportFrameController _controller) {
    super();
    controller_ = _controller;
  }

  protected EbliAnimationSourceInterface getAnimFor(int _i) {
    return ((FudaaDjaFormInterface) controller_.usedAnimatedFrame_.get(_i)).getAnim();
  }

  protected EbliAnimationSourceInterface getFirstAnim() {
    for (int i = 0; i < controller_.usedAnimatedFrame_.size(); i++) {
      if (getAnimFor(i) != null) return getAnimFor(i);
    }
    return null;
  }

  @Override
  public Component getComponent() {
    return controller_.grid_;
  }

  @Override
  public Dimension getDefaultImageDimension() {
    return controller_.grid_.getDefaultImageDimension();
  }

  @Override
  public int getNbTimeStep() {
    EbliAnimationSourceInterface first = getFirstAnim();
    return first == null ? 0 : first.getNbTimeStep();
  }

  @Override
  public String getTimeStep(int _idx) {
    EbliAnimationSourceInterface first = getFirstAnim();
    return first == null ? CtuluLibString.ZERO : first.getTimeStep(_idx);
  }

  @Override
  public double getTimeStepValueSec(int _idx) {
    EbliAnimationSourceInterface first = getFirstAnim();
    return first == null ? 0 : first.getTimeStepValueSec(_idx);
  }

  @Override
  public String getTitle() {
    return controller_.owner_.getTitle();
  }

  @Override
  public BufferedImage produceImage(Map _params) {
    return controller_.grid_.produceImage(null);
  }

  @Override
  public BufferedImage produceImage(int _w, int _h, Map _params) {
    return controller_.grid_.produceImage(_w, _h, null);
  }

  @Override
  public void setTimeStep(int _idx) {
    for (int i = 0; i < controller_.usedAnimatedFrame_.size(); i++) {
      if (getAnimFor(i) != null) getAnimFor(i).setTimeStep(_idx);
    }
    controller_.refreshAll();
  }

  @Override
  public void setVideoMode(boolean _b) {
    for (int i = 0; i < controller_.usedAnimatedFrame_.size(); i++) {
      if (getAnimFor(i) != null) getAnimFor(i).setVideoMode(_b);
    }
    controller_.grid_.setInteractive(!_b);
  }

}