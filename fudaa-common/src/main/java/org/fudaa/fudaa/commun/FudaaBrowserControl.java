/*
 * @file Fudaa.java @creation 2001-09-13 @modification $Date: 2007-01-19 13:14:25 $ @license GNU
 * General Public License 2 @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne @mail
 * devel@fudaa.org
 */
package org.fudaa.fudaa.commun;

import com.memoire.bu.BuBrowserControl;
import java.net.URL;
import org.fudaa.ctulu.CtuluLibString;


/**
 * Controle le lancement du browser externe et gere les problemes sous windows. Si l'URL contient une ancre, la dll
 * windows <code>url.dll</code> ne la prend pas en compte. Dans ce cas, <code>FudaaBrowserControl</code> cree un
 * fichier temporaire qui redirige automatiquement sur l'URL demandee.
 * 
 * @version $Revision: 1.18 $ $Date: 2007-01-19 13:14:25 $ by $Author: deniger $
 * @author Fred Deniger
 */
public class FudaaBrowserControl extends BuBrowserControl {

  /**
   * Affiche l'url. Si l'URL est nulle <code>FudaaImplementation.LOCAL_MAN</code> est utilisee.
   * 
   * @param _url l'url a afficher
   * @see com.memoire.bu.BuBrowserControl#displayURL(String)
   */
  public static void displayURL(final String _url) {
    String url = _url;
    if ((url == null) || (url.length() == 0)) {
      url = CtuluLibString.pathToUrl(FudaaLib.LOCAL_MAN);
    }
    BuBrowserControl.displayURL(url);
  }

  public static void displayURL(final URL _url) {
    if (_url == null) {
      displayURL(CtuluLibString.pathToUrl(FudaaLib.LOCAL_MAN));
    } else {
      BuBrowserControl.displayURL(_url.toString());
    }
  }

}