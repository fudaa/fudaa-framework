/*
 * @creation 9 mars 07
 * @modification $Date: 2007-03-15 17:01:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.dja.DjaObject;
import com.memoire.dja.DjaText;
import java.awt.Dimension;
import java.awt.Graphics;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.View;
import javax.swing.text.html.HTMLDocument;
import org.fudaa.ctulu.CtuluLibString;

/**
 * @author fred deniger
 * @version $Id: FudaaDjaText.java,v 1.2 2007-03-15 17:01:09 deniger Exp $
 */
public class FudaaDjaText extends DjaText {

  public final static String TEXT_PROPERTY = "textProperty";
  public final static String TEXT_AJUST = "textAjust";
  public final static String TEXT_WIDTH = "textWidth";
  public final static String TEXT_HEIGHT = "textHeight";

  PropertyChangeSupport support_ = new PropertyChangeSupport(this);

  boolean textAjust_ = true;

  public FudaaDjaText(DjaObject _parent, int _num, String _text) {
    super(_parent, _num, _text);
    super.marge_ = 2;
    initView();
  }

  private void initView() {
    String text = getText();
    if (getGrid() == null) return;
    if (CtuluLibString.isEmpty(text)) {
      text = "<html><body><p></p></body></html>";
      setW(10);
      setH(10);
    }
    view_ = BasicHTML.createHTMLView(getGrid(), text);
    HTMLDocument doc = (HTMLDocument) view_.getDocument();
    doc.getStyleSheet().addRule("body { color: #000000;padding: 4px}");
  }

  @Override
  public void setText(String _text) {
    if (_text == null || _text.equals(getText())) return;
    String old = getText();
    super.setText(_text);
    initView();
    if (view_ != null) {
      Dimension d = new Dimension((int) view_.getPreferredSpan(View.X_AXIS), (int) view_.getPreferredSpan(View.Y_AXIS));
      int oldW = getW();
      if (d.width > oldW) {
        setW(d.width);
        if (d.width > getParent().getWidth()) getParent().setWidth(d.width);
      }
      setH(Math.min(d.height, getParent().getHeight()));
      if (textAjust_) {
        int w = getParent().getWidth();
        if (w < d.width + 1) {
          getParent().setWidth(d.width + 1);
        }
        int h = getParent().getHeight();

        if (h < d.height + 1) {
          getParent().setHeight(d.height + 1);

        }

      }
    }
    firePropertyChange(TEXT_PROPERTY, old, _text);
  }

  @Override
  public void setW(int _w) {
    if (w_ != _w) {
      int old = w_;
      super.setW(_w);
      firePropertyChange(TEXT_WIDTH, old, w_);
    }
  }

  @Override
  public void setH(int _h) {
    if (h_ != _h) {
      int old = h_;
      super.setH(_h);
      firePropertyChange(TEXT_HEIGHT, old, h_);
    }
  }

  @Override
  public int getH() {
    return getSize().height;
  }

  @Override
  public int getW() {
    return getSize().width;
  }

  @Override
  public Dimension getSize() {
    Dimension r = optimalSize();
    r.width = Math.min(w_ + 2 * marge_, getParent().getWidth() - 1);
    r.height = Math.max(h_ + 2 * marge_, r.height);
    return r;
  }

  @Override
  public void paint(Graphics _g) {
    if (view_ == null) initView();
    if (view_ == null) return;
    super.paint(_g);
  }

  @Override
  public Dimension optimalSize() {
    if (view_ == null) return super.optimalSize();
    return getViewOptimalSize();
  }

  protected Dimension getViewOptimalSize() {
    return new Dimension((int) view_.getMinimumSpan(View.X_AXIS), (int) view_.getMinimumSpan(View.Y_AXIS));
  }

  public void addPropertyChangeListener(PropertyChangeListener _listener) {
    support_.addPropertyChangeListener(_listener);
  }

  public void addPropertyChangeListener(String _propertyName, PropertyChangeListener _listener) {
    support_.addPropertyChangeListener(_propertyName, _listener);
  }

  public void firePropertyChange(String _propertyName, int _oldValue, int _newValue) {
    support_.firePropertyChange(_propertyName, _oldValue, _newValue);
  }

  public void firePropertyChange(String _propertyName, Object _oldValue, Object _newValue) {
    support_.firePropertyChange(_propertyName, _oldValue, _newValue);
  }

  public void removePropertyChangeListener(PropertyChangeListener _listener) {
    support_.removePropertyChangeListener(_listener);
  }

  public void removePropertyChangeListener(String _propertyName, PropertyChangeListener _listener) {
    support_.removePropertyChangeListener(_propertyName, _listener);
  }

  public boolean isTextAjust() {
    return textAjust_;
  }

  public void setTextAjust(boolean _textAjust) {
    if (textAjust_ != _textAjust) {
      textAjust_ = _textAjust;
      firePropertyChange(TEXT_PROPERTY, Boolean.valueOf(!_textAjust), Boolean.valueOf(_textAjust));
      if (textAjust_) {
        Dimension d = getViewOptimalSize();
        DjaObject parent = getParent();
        parent.setWidth(Math.min(d.width, parent.getWidth()));
        parent.setHeight(Math.min(d.width, parent.getHeight()));
      }
    }
  }
}
