/*
 *  @file         FudaaExecCellEditor.java
 *  @creation     4 juin 2003
 *  @modification $Date: 2006-09-19 15:01:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.exec;

import org.fudaa.ctulu.gui.CtuluCellDialogEditor;
import org.fudaa.ctulu.gui.CtuluDialog;

/**
 * @author deniger
 * @version $Id: FudaaExecCellEditor.java,v 1.10 2006-09-19 15:01:53 deniger Exp $
 */
public class FudaaExecCellEditor extends CtuluCellDialogEditor {
  public FudaaExecCellEditor() {
    super(new CtuluDialog(new FudaaExecPanel()));
  }

  @Override
  public void setValue(final Object _o) {
    final FudaaExec e = (FudaaExec) _o;
    setText(e.getViewedName());
    setIcon(e.getIcon());
  }
}
