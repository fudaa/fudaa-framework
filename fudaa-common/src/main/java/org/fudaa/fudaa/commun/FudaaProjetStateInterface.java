/*
 * @creation 27 avr. 07
 * @modification $Date: 2008-01-10 09:58:19 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun;

/**
 * L'�tat courant d'un projet (UI modifi�e, donn�es modifi�es, etc.).
 * @author fred deniger
 * @version $Id: FudaaProjetStateInterface.java,v 1.2 2008-01-10 09:58:19 bmarchan Exp $
 */
public interface FudaaProjetStateInterface {

  /**
   * Les donn�es sont-elles modifi�es ?
   * @return true Elles sont modifi�es (a priori depuis la derni�re sauvegarde).
   */
  boolean isParamsModified();

  /**
   * L'UI est-il modifi� ?
   * @return true Il a �t� modifi�.
   */
  boolean isUIModified();

  /**
   * Ajout d'un listener notifi� en cas de modification des param�tres ou de l'UI.
   * @param _l Le listener.
   */
  void addListener(FudaaProjectStateListener _l);
}
