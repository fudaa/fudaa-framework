/*
 * @creation 9 mars 07
 * @modification $Date: 2007-03-15 17:01:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.dja.DjaAnchor;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author fred deniger
 * @version $Id: FudaaDjaEllipse.java,v 1.1 2007-03-15 17:01:09 deniger Exp $
 */
public class FudaaDjaEllipse extends FudaaDjaFormAbstract {

  static int idx_;

  public FudaaDjaEllipse() {
  }
  
  
  
  

  @Override
  public String getDescription() {
    return FudaaLib.getS("Ellipse");
  }





  @Override
  public DjaAnchor[] getAnchors() {
    int x = getX();
    int y = getY();
    int w = getWidth();
    int h = getHeight();

    DjaAnchor[] r = new DjaAnchor[8];
    r[0] = new DjaAnchor(this, 0, NORTH, x + w / 2, y);
    r[2] = new DjaAnchor(this, 2, EAST, x + w - 1, y + h / 2);
    r[4] = new DjaAnchor(this, 4, SOUTH, x + w / 2, y + h - 1);
    r[6] = new DjaAnchor(this, 6, WEST, x, y + h / 2);

    if (w >= h) {
      r[1] = new DjaAnchor(this, 1, NORTH, x + 3 * w / 4, y + 134 * h / 2000);
      r[3] = new DjaAnchor(this, 3, SOUTH, x + 3 * w / 4, y + h - 1 - 134 * h / 2000);
      r[5] = new DjaAnchor(this, 5, SOUTH, x + w / 4, y + h - 1 - 134 * h / 2000);
      r[7] = new DjaAnchor(this, 7, NORTH, x + w / 4, y + 134 * h / 2000);
    } else {
      r[1] = new DjaAnchor(this, 1, EAST, x + w - 1 - 134 * w / 2000, y + h / 4);
      r[3] = new DjaAnchor(this, 3, EAST, x + w - 1 - 134 * w / 2000, y + 3 * h / 4);
      r[5] = new DjaAnchor(this, 5, WEST, x + 134 * w / 2000, y + 3 * h / 4);
      r[7] = new DjaAnchor(this, 7, WEST, x + 134 * w / 2000, y + h / 4);
    }

    return r;
  }
  
  @Override
  public FudaaDjaFormAbstract createNew() {
    return new FudaaDjaEllipse();
  }

  @Override
  public String getDjaName() {
    return "Ellipse";
  }

  @Override
  public void paintObject(Graphics _g) {
    int x = getX();
    int y = getY();
    int w = getWidth();
    int h = getHeight();

    Color bg = getBackground();
    Color fg = getForeground();

    if (bg != null) {
      _g.setColor(bg);
      _g.fillOval(x, y, w - 1, h - 1);
    }

    if (fg != null) {
      ligne_.dessineEllipse2D((Graphics2D) _g, x, y, w, h);
    }
    super.paintObject(_g);
  }

}
