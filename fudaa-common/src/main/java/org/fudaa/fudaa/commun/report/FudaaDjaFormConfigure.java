/*
 * @creation 8 f�vr. 07
 * @modification $Date: 2007-03-19 13:27:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.dja.DjaObject;
import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.ebli.controle.BSelecteurColorChooser;
import org.fudaa.ebli.controle.BSelecteurColorChooserBt;
import org.fudaa.ebli.controle.BSelecteurInterface;
import org.fudaa.ebli.controle.BSelecteurLineModel;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * @author fred deniger
 * @version $Id: FudaaDjaFormConfigure.java,v 1.1 2007-03-19 13:27:20 deniger Exp $
 */
public class FudaaDjaFormConfigure extends FudaaDjaConfigureAbstract {

  public static final String PROPERTY_BACKGROUND = "background";



  public FudaaDjaFormConfigure(final FudaaDjaFormInterface _target) {
    super(_target, EbliLib.getS("Affichage"));
  }

  FudaaDjaFormInterface getIfForm() {
    return (FudaaDjaFormInterface) target_;
  }

  protected DjaObject getDja() {
    return (DjaObject) target_;
  }
  PropertyChangeSupport listener_ = new PropertyChangeSupport(this);
  @Override
  public void addPropertyChangeListener(String _key, PropertyChangeListener _l) {
    listener_.addPropertyChangeListener(_key, _l);
  }
  @Override
  public void removePropertyChangeListener(String _key, PropertyChangeListener _l) {
    listener_.removePropertyChangeListener(_key, _l);
  }

  @Override
  public BSelecteurInterface[] createSelecteurs() {
    // BSelecteurColorChooserBt foreground = new BSelecteurColorChooserBt(true);
    BSelecteurColorChooserBt background = new BSelecteurColorChooserBt(true, PROPERTY_BACKGROUND);
    BSelecteurLineModel line = new BSelecteurLineModel();
    line.setTitle(EbliLib.getS("Trac� du bord"));
    background.setTitle(EbliLib.getS("Couleur de fond"));
    return getIfForm().isBackgroundModifiable() ? new BSelecteurInterface[] { background, line }
        : new BSelecteurInterface[] { line };
  }

  @Override
  public Object getProperty(String _key) {
    if (BSelecteurLineModel.PROPERTY.equals(_key)) return new TraceLigneModel(getIfForm().getContourLigne());
    else if (BSelecteurColorChooser.DEFAULT_PROPERTY.equals(_key)) {
      return getDja().getForeground();
    } else if (PROPERTY_BACKGROUND.equals(_key)) {
      return getDja().getBackground();
    }
    return null;
  }

 

  @Override
  public boolean setProperty(String _key, Object _newProp) {
    boolean r = false;
    if (BSelecteurLineModel.PROPERTY.equals(_key)) {
      getIfForm().getContourLigne().updateData((TraceLigneModel) _newProp);
      r = true;
      listener_.firePropertyChange(BSelecteurColorChooser.DEFAULT_PROPERTY, null, getDja().getForeground());
    } else if (BSelecteurColorChooser.DEFAULT_PROPERTY.equals(_key)) {
      getDja().setForeground((Color) _newProp);
      listener_.firePropertyChange(BSelecteurLineModel.PROPERTY, null, getIfForm().getContourLigne());
      r = true;
    } else if (PROPERTY_BACKGROUND.equals(_key)) {
      getDja().setBackground((Color) _newProp);
      r = true;
    }
    if (r) {
      target_.getGrid().repaint();
    }
    return r;
  }

}
