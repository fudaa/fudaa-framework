/**
 * @modification $Date: 2007-03-19 13:27:20 $
 * @statut       unstable
 * @file         DjaHLine.java
 * @version      0.36
 * @author       Guillaume Desnoix
 * @email        guillaume@desnoix.com
 * @license      GNU General Public License 2 (GPL2)
 * @copyright    1998-2001 Guillaume Desnoix
 */

package org.fudaa.fudaa.commun.report;

import com.memoire.dja.DjaAnchor;
import com.memoire.dja.DjaHandle;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import org.fudaa.fudaa.commun.FudaaLib;

public class FudaaDjaHLine extends FudaaDjaFormAbstract {

  public FudaaDjaHLine() {}

  @Override
  public int getHeight() {
    return ligne_ == null ? 1 : (int) ligne_.getEpaisseur();
  }

  @Override
  public boolean isBackgroundModifiable() {
    return false;
  }

  @Override
  public FudaaDjaFormAbstract createNew() {
    return new FudaaDjaHLine();
  }

/*  public BConfigurableInterface[] getConfigureInterfaces() {
    return new BConfigurableInterface[] { new FudaaReportDjaFormConfigure(this) };
  }*/

  @Override
  public String getDjaName() {
    return "HLine";
  }
  
  @Override
  public String getDescription() {
    return FudaaLib.getS("Ligne horizontale");
  }

  @Override
  public void setHeight(int _h) {
    super.setHeight(1);
  }

  @Override
  public boolean contains(int _x, int _y) {
    return new Rectangle(getX(), getY() - 3, getWidth(), 7).contains(_x, _y);
  }

  @Override
  public DjaAnchor[] getAnchors() {
    int x = getX();
    int y = getY();
    int w = getWidth();

    DjaAnchor[] r = new DjaAnchor[2];
    r[0] = new DjaAnchor(this, 0, WEST, x, y);
    r[1] = new DjaAnchor(this, 1, EAST, x + w - 1, y);

    return r;
  }

  @Override
  public DjaHandle[] getHandles() {
    int x = getX();
    int y = getY();
    int w = getWidth();

    DjaHandle[] r = new DjaHandle[2];
    r[0] = new DjaHandle(this, WEST, x - deltaX, y);
    r[1] = new DjaHandle(this, EAST, x + w - 1 + deltaX, y);

    return r;
  }

  @Override
  public void paintObject(Graphics _g) {
    if (ligne_.getCouleur() != null) {
      ligne_.dessineTrait((Graphics2D) _g, getX(), getY(), getX() + getWidth(), getY());
    }
    super.paintObject(_g);
  }
}
