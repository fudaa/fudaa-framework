/*
 *  @file         FudaaExecManagerPanel.java
 *  @creation     28 mai 2003
 *  @modification $Date: 2007-06-05 09:01:16 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.exec;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuList;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuSeparator;
import com.memoire.bu.BuVerticalLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluPopupMenu;
import org.fudaa.ebli.commun.EbliLib;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * Un panneau pour specifier les applications a utiliser.
 * 
 * @author deniger
 * @version $Id: FudaaExecManagerPanel.java,v 1.22 2007-06-05 09:01:16 deniger Exp $
 */
public class FudaaExecManagerPanel extends CtuluDialogPanel implements ListSelectionListener, ActionListener {

  FudaaAppliManagerImpl mng_;
  JList listAppli_;
  JList listBiblio_;
  BuButton dansBib_;
  BuButton dansAppli_;
  BuButton suppExec_;

  /**
   * @param _mng le manager d'appli
   */
  public FudaaExecManagerPanel(final FudaaAppliManagerImpl _mng) {
    mng_ = _mng;
    final List bibliotheque = _mng.getExecMng().getAllExe();
    final List myAppli = new ArrayList(_mng.getFudaaExecList());
    for (int i = myAppli.size() - 1; i >= 0; i--) {
      bibliotheque.remove(myAppli.get(i));
    }
    final FudaaExecListModel modelAppli = new FudaaExecListModel(myAppli);
    final FudaaExecListModel modelBiblio = new FudaaExecListModel(bibliotheque);
    addEmptyBorder(10);
    setLayout(new BuGridLayout(3, 10, 10));
    addLabel(FudaaLib.getS("Applications sélectionnées"));
    addLabel(CtuluLibString.EMPTY_STRING);
    addLabel(FudaaLib.getS("Applications disponibles"));
    listAppli_ = new BuList();
    listAppli_.setModel(modelAppli);
    final FudaaExecCellRenderer r = new FudaaExecCellRenderer();
    listAppli_.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(final MouseEvent _e) {
        if ((!EbliLib.isPopupMouseEvent(_e)) && (_e.getClickCount() == 2)) {
          edit(listAppli_);
        }
      }

      @Override
      public void mousePressed(final MouseEvent _e) {
        if (EbliLib.isPopupMouseEvent(_e)) {
          popupAppli(_e.getX(), _e.getY());
        }
      }
    });
    listAppli_.setCellRenderer(r);
    listBiblio_ = new BuList();
    listBiblio_.setModel(modelBiblio);
    final FudaaExecCellRenderer r2 = new FudaaExecCellRenderer();
    listBiblio_.addMouseListener(new MouseAdapter() {

      @Override
      public void mouseClicked(final MouseEvent _e) {
        if (_e.getClickCount() == 2) {
          edit(listBiblio_);
        }
      }
    });
    listBiblio_.setCellRenderer(r2);
    add(new BuScrollPane(listAppli_));
    final BuPanel pn = new BuPanel();
    pn.setLayout(new BuVerticalLayout(5, true, false));
    final BuButton bt = new BuButton(BuResource.BU.getIcon("ajouter"));
    bt.setActionCommand("AJOUTER");
    bt.addActionListener(this);
    bt.setToolTipText(FudaaLib.getS("Ajouter une application"));
    pn.add(bt);
    dansAppli_ = new BuButton(BuResource.BU.getIcon("reculer"));
    dansAppli_.setActionCommand("DANS_APPLI");
    dansAppli_.addActionListener(this);
    dansAppli_.setToolTipText(FudaaLib.getS("Activer les applications sélectionnées"));
    pn.add(dansAppli_);
    dansAppli_.setEnabled(false);
    dansBib_ = new BuButton(BuResource.BU.getIcon("avancer"));
    dansBib_.setActionCommand("DANS_BIB");
    dansBib_.addActionListener(this);
    dansBib_.setToolTipText(FudaaLib.getS("Désactiver les applications sélectionnées"));
    dansBib_.setEnabled(false);
    pn.add(dansBib_);
    suppExec_ = new BuButton(BuResource.BU.getIcon("enlever"));
    suppExec_.setActionCommand("DELETE");
    suppExec_.addActionListener(this);
    suppExec_.setToolTipText(FudaaLib.getS("Supprimer les applications sélectionnées"));
    suppExec_.setEnabled(false);
    pn.add(new BuSeparator());
    pn.add(suppExec_);
    add(pn);
    add(new BuScrollPane(listBiblio_));
    listAppli_.setPreferredSize(new Dimension(150, 200));
    listBiblio_.setPreferredSize(new Dimension(150, 200));
    listBiblio_.getSelectionModel().addListSelectionListener(this);
    listAppli_.getSelectionModel().addListSelectionListener(this);
  }

  protected void edit(final JList _m) {
    final int i = new FudaaExecPanel((FudaaExec) _m.getSelectedValue()).afficheModale(this);
    if (CtuluDialogPanel.isOkResponse(i)) {
      _m.repaint();
    }
  }

  protected void popupAppli(final int _x, final int _y) {
    final CtuluPopupMenu m = new CtuluPopupMenu();
    final boolean b = (listAppli_.getModel().getSize() > 1) && (!listAppli_.isSelectionEmpty())
        && (listAppli_.getSelectedIndices().length == 1);
    boolean up = b && (listAppli_.getSelectedIndex() > 0);
    m.addMenuItem(BuResource.BU.getString("Monter"), "UP", up, this).setIcon(BuResource.BU.getIcon("bu_menu_up"));
    up = b && (listAppli_.getSelectedIndex() < listAppli_.getModel().getSize() - 1);
    m.addMenuItem(BuResource.BU.getString("Descendre"), "DOWN", up, this)
        .setIcon(BuResource.BU.getIcon("bu_menu_down"));
    m.show(this, _x + 10, _y);
  }

  protected void addExec() {
    final FudaaExecPanel ex = new FudaaExecPanel();
    final FudaaExecListModel m = (FudaaExecListModel) listAppli_.getModel();
    final FudaaExec[] t = new FudaaExec[m.getSize()];
    m.toArray(t);
    ex.setControle(t);
    final int i = ex.afficheModale(FudaaExecManagerPanel.this, FudaaLib.getS("Nouvel exécutable"));
    if (CtuluDialogPanel.isOkResponse(i)) {
      m.add(ex.getExec());
      listAppli_.repaint();
    }
  }

  /**
   * @return 
   * @see org.fudaa.ctulu.gui.CtuluDialogPanel#apply()
   */
  @Override
  public boolean apply() {
    final FudaaExecListModel mApp = (FudaaExecListModel) listAppli_.getModel();
    final FudaaExec[] ex = new FudaaExec[mApp.size()];
    mApp.toArray(ex);
    final FudaaExecListModel m = (FudaaExecListModel) listBiblio_.getModel();
    final ArrayList l = new ArrayList(m.size() + ex.length);
    m.fillList(l);
    mApp.fillList(l);
    mng_.getExecMng().setNewValues(l);
    mng_.setNewValues(ex);
    return true;
  }

  /**
   * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
   */
  @Override
  public void actionPerformed(final ActionEvent _evt) {
    final String c = _evt.getActionCommand();
    if ("AJOUTER".equals(c)) {
      addExec();
    } else if ("DANS_BIB".equals(c)) {
      move((FudaaExecListModel) listAppli_.getModel(), (FudaaExecListModel) listBiblio_.getModel(), listAppli_
          .getSelectedValues());
      ((FudaaExecListModel) listBiblio_.getModel()).sort();
      listAppli_.clearSelection();
      listBiblio_.clearSelection();
    } else if ("DANS_APPLI".equals(c)) {
      final Object[] o = listBiblio_.getSelectedValues();
      move((FudaaExecListModel) listBiblio_.getModel(), (FudaaExecListModel) listAppli_.getModel(), o);
      listBiblio_.clearSelection();
      if (o != null) {
        for (int i = o.length - 1; i >= 0; i--) {
          listAppli_.setSelectedValue(o[i], false);
        }
      }
    } else if ("DELETE".equals(c)) {
      ((FudaaExecListModel) listBiblio_.getModel()).removeAll(listBiblio_.getSelectedValues());
    } else if ("UP".equals(c)) {
      final FudaaExecListModel m = (FudaaExecListModel) listAppli_.getModel();
      final int i = listAppli_.getSelectedIndex();
      m.add(i - 1, m.remove(i));
    } else if ("DOWN".equals(c)) {
      final FudaaExecListModel m = (FudaaExecListModel) listAppli_.getModel();
      final int i = listAppli_.getSelectedIndex();
      m.add(i + 1, m.remove(i));
    }
  }

  private void move(final FudaaExecListModel _from, final FudaaExecListModel _to, final Object[] _objToMove) {
    if (_objToMove != null) {
      final List c = Arrays.asList(_objToMove);
      _from.removeAll(c);
      _to.addAll(c);
    }
  }

  /**
   * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
   */
  @Override
  public void valueChanged(final ListSelectionEvent _e) {
    if (_e.getSource() == listBiblio_.getSelectionModel()) {
      dansAppli_.setEnabled(!listBiblio_.isSelectionEmpty());
      this.suppExec_.setEnabled(!listBiblio_.isSelectionEmpty());
    } else {
      dansBib_.setEnabled(!listAppli_.isSelectionEmpty());
    }
  }

  /**
   * @see org.fudaa.ctulu.gui.CtuluDialogPanel#cancel()
   */
  @Override
  public boolean cancel() {
    mng_.getExecMng().reloadFromPref();
    return true;
  }
}
