/*
 *  @file         TrEditor.java
 *  @creation     30 avr. 2003
 *  @modification $Date: 2007-06-05 09:01:16 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.exec;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import org.fudaa.fudaa.commun.FudaaPreferences;

/**
 * @author deniger
 * @version $Id: FudaaEditor.java,v 1.14 2007-06-05 09:01:16 deniger Exp $
 */
public final class FudaaEditor {
  FudaaEditorExterne[] editors_;
  FudaaEditorInterface defautTextEditor_;
  FudaaEditorInterne interne_;
  public static final String EXTERNE_EDITOR_PREFIXE = "editor";
  public static final String INTERNE_EDITOR_VALUE = "interne";
  public static final String DEFAULT_EDITOR_KEY = "editor.default";
  private final static FudaaEditor INSTANCE = new FudaaEditor();

  public static FudaaEditor getInstance() {
    return INSTANCE;
  }

  private FudaaEditor() {
    initFromPref();
  }

  public void initFromPref() {
    final ArrayList editor = new ArrayList(10);
    for (final Enumeration e = FudaaPreferences.FUDAA.allKeysWithDefaults(); e.hasMoreElements();) {
      final String k = (String) e.nextElement();
      if ((k.startsWith(EXTERNE_EDITOR_PREFIXE)) && (k.endsWith(FudaaExec.EXE))) {
        final String name = k.substring(EXTERNE_EDITOR_PREFIXE.length() + 1, k.length() - FudaaExec.EXE.length() - 1);
        if (FudaaExec.containsIdName(editor, name)) {
          FuLog.warning("several executables with same name");
        } else {
          final FudaaEditorExterne exe = new FudaaEditorExterne(name);
          editor.add(exe);
        }
      }
    }
    editors_ = new FudaaEditorExterne[editor.size()];
    editor.toArray(editors_);
    final String def = FudaaPreferences.FUDAA.getStringProperty(DEFAULT_EDITOR_KEY, INTERNE_EDITOR_VALUE);
    if (Fu.DEBUG && FuLog.isDebug()) FuLog.debug("FCO:  Editor: definition =" + DEFAULT_EDITOR_KEY + " =" + def);
    Arrays.sort(editors_);
    setDefaultEditor(def, false);
  }

  public void setDefaultEditor(final String _v) {
    setDefaultEditor(_v, true);
  }

  public void setDefaultEditor(final String _v, final boolean _maj) {
    defautTextEditor_ = findEditor(_v);
    if (defautTextEditor_ == null) {
      setInterneEditorAsDefault(_maj);
    } else {
      if (_maj) {
        FudaaPreferences.FUDAA.putStringProperty(DEFAULT_EDITOR_KEY, defautTextEditor_.getIDName());
      }
    }
  }

  public void setInterneEditorAsDefault() {
    setInterneEditorAsDefault(true);
  }

  private void setInterneEditorAsDefault(final boolean _maj) {
    if (defautTextEditor_.equals(interne_)) {
      return;
    }
    if (interne_ == null) {
      interne_ = new FudaaEditorInterne();
      defautTextEditor_ = interne_;
    }
    if (_maj) {
      FudaaPreferences.FUDAA.putStringProperty(DEFAULT_EDITOR_KEY, INTERNE_EDITOR_VALUE);
    }
  }

  public boolean isInterneEditorDefault() {
    return ((interne_ != null) && (defautTextEditor_ == interne_));
  }

  public FudaaEditorExterne[] getEditorExternes() {
    final FudaaEditorExterne[] r = new FudaaEditorExterne[editors_.length];
    System.arraycopy(editors_, 0, r, 0, editors_.length);
    return r;
  }

  public FudaaEditorInterface getDefaultEditor() {
    return defautTextEditor_;
  }

  public void setDefaultEditor(final FudaaEditorExterne _ex) {
    if (_ex.equals(defautTextEditor_)) {
      return;
    }
    final int i = Arrays.binarySearch(editors_, _ex);
    if (i < 0) {
      setInterneEditorAsDefault();
    } else {
      defautTextEditor_ = _ex;
    }
    FudaaPreferences.FUDAA.putStringProperty(DEFAULT_EDITOR_KEY, defautTextEditor_.getIDName());
  }

  private FudaaEditorInterface findEditor(final String _name) {
    if (INTERNE_EDITOR_VALUE.equals(_name)) {
      if (interne_ == null) {
        interne_ = new FudaaEditorInterne();
      }
      return interne_;
    }
    final int n = editors_.length - 1;
    FudaaEditorInterface edit;
    for (int i = n; i >= 0; i--) {
      edit = editors_[i];
      if (edit.getIDName().equals(_name)) {
        return edit;
      }
    }
    return null;
  }

  public void edit(final File _file) {
    defautTextEditor_.edit(_file);
  }
}
