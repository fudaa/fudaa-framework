/*
 *  @creation     8 sept. 2005
 *  @modification $Date: 2007-01-19 13:14:06 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.impl;

import javax.swing.JButton;
import javax.swing.JComponent;
import org.fudaa.ctulu.CtuluAnalyzeGroup;
import org.fudaa.ctulu.ProgressionInterface;

public interface FudaaPanelTaskModel {

  /**
   * @return null si valide, l'explication sinon
   */
  String isTakDataValid();

  /**
   * @return le titre de la tache
   */
  String getTitre();

  void decoreBtApply(JButton _bt);

  int getNbMessageMax();

  /**
   * @return le panneau comportant les options
   */
  JComponent getPanel();

  /**
   * Cette m�thode longue est lanc�e dans un thread CtuluTaskOperation
   * 
   * @param _prog la progression
   * @param _analyze recupe les erreurs
   * @param un tableau de taille 2*getNbMessageMax _messages[2*i]: libell� _messages[2*i+1]=valeur
   */
  void actTask(ProgressionInterface _prog, CtuluAnalyzeGroup _analyze, String[] _messages);

  void stopTask();

  /**
   * Appele quand le dialog est ferm�
   */
  void dialogClosed();

}
