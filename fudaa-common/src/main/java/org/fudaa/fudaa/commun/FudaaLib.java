/*
 *  @creation     22 sept. 2005
 *  @modification $Date: 2007-06-28 09:28:19 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuList;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.fu.FuLib;
import java.awt.Frame;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import javax.swing.Icon;
import javax.swing.JInternalFrame;
import javax.swing.ListSelectionModel;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.fileformat.FileFormat;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * @author Fred Deniger
 * @version $Id: FudaaLib.java,v 1.45 2007-06-28 09:28:19 deniger Exp $
 */
public final class FudaaLib {

  /**
   * @author Fred Deniger
   * @version $Id: FudaaLib.java,v 1.45 2007-06-28 09:28:19 deniger Exp $
   */
  public static class OpenResult {

    public File file_;
    public FileFormat ft_;

  }

  public static FileFormat chooseFileFormat(final Frame _parent, final FileFormat[] _ft, final String _title) {
    final BuList list = CtuluLibSwing.createBuList(_ft);
    list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    final CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BuBorderLayout());
    pn.add(new BuLabel(FudaaResource.FUDAA.getString("Choisir le format du fichier")), BuBorderLayout.NORTH);
    pn.add(new BuScrollPane(list));
    if (pn.afficheModaleOk(_parent, _title)) {
      return (FileFormat) list.getSelectedValue();
    }
    return null;
  }

  /**
   * Traduit et retourne la chaine traduite, avec ou sans valeurs � ins�rer.
   *
   * @param _s La chaine � traduire.
   * @param _vals Les valeurs, de n'importe quelle type.
   * @return La chaine traduite.
   */
  public static String getS(final String _s, final Object ... _vals) {
    String r = FudaaResource.FUDAA.getString(_s);
    if (r == null) {
      return r;
    }

    for (int i=0; i<_vals.length; i++) {
      r = FuLib.replace(r, "{"+i+"}", _vals[i].toString());
    }
    return r;
  }

  /**
   * le chemin de l'aide locale.
   */
  public final static String LOCAL_MAN = getUserDirUrl() + "/aide/";
  /**
   * Le chemin de l'aide distante.
   */
  public final static String REMOTE_MAN = "http://www.utc.fr/fudaa/aide/aide_html/";

  private FudaaLib() {}

  public static SimpleDateFormat getDefaultDateFormat() {
    return new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss z",Locale.getDefault());
  }

  /**
   * Return le chemin de la propriete user.dir.
   * 
   * @return le chemin de l'utilisateur sous forme d'url valide
   */
  public static String getUserDirUrl() {
    String r = System.getProperty("INSTALLDIR", null);
    if (r == null) {
      r = System.getProperty("user.dir");
    }
    return r;
  }

//  public static void initGrPoint(final GISPoint _pt, final GrPoint _ptToinit) {
//    if (_ptToinit == null) {
//      FuLog.error("Point to init is null");
//    } else if (_pt == null) {
//      FuLog.error("Point to init From is null");
//    } else {
//      _ptToinit.x_ = _pt.getX();
//      _ptToinit.y_ = _pt.getY();
//      _ptToinit.z_ = _pt.getZ();
//    }
//  }

  public static List getFrameWithClientProperty(final BuCommonImplementation _impl, final String _s) {
    final JInternalFrame[] frs = _impl.getAllInternalFrames();
    final int nb = CtuluLibArray.getNbItem(frs);
    final List qInternal = new ArrayList(nb);
    for (int i = 0; i < nb; i++) {
      if (frs[i].getClientProperty(_s) == Boolean.TRUE) {
        qInternal.add(frs[i]);
      }
    }
    return qInternal;
  }

  public static CtuluCellTextRenderer createInternalFrameCellRenderer() {
    return new CtuluCellTextRenderer() {
      @Override
      protected void setValue(final Object _value) {
        final JInternalFrame f = (JInternalFrame) _value;
        setText(f.getTitle());
        final Icon frameIcon = f.getFrameIcon();
        setIcon(frameIcon == null ? BuResource.BU.getToolIcon("bu_internalframe_corner_ocean") : frameIcon);
      }
    };
  }

  public static String getModifiedSaveState(final FudaaProjetStateInterface _proj) {
    if (_proj.isParamsModified() || _proj.isUIModified()) {
      String txt = FudaaResource.FUDAA.getString("Projet modifi�:") + CtuluLibString.ESPACE;
      if (_proj.isParamsModified()) {
        txt += FudaaResource.FUDAA.getString("param�tres du mod�le");
        if (_proj.isUIModified()) {
          txt += CtuluLibString.ESPACE
              + FudaaResource.FUDAA.getString("et propri�t�s graphiques");
        }
      } else {
        txt += FudaaResource.FUDAA.getString("propri�t�s graphiques uniquement");
      }

      return txt;
    }
    return FudaaResource.FUDAA.getString("Projet enregistr�");

  }

  public static String getCurrentDateForFile() {
    return new SimpleDateFormat("yyyy-MM-dd-HH'h'mm", Locale.getDefault()).format(Calendar.getInstance().getTime());
  }

}
