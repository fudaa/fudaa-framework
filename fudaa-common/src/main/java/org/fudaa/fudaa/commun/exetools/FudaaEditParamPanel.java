/*
 * @creation     18 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.exetools;

import com.memoire.bu.BuVerticalLayout;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * Un panneau pour editer un param�tre d'un utilitaire.
 * 
 * @author marchand@deltacad.fr
 * @version $Id$
 */
public class FudaaEditParamPanel extends CtuluDialogPanel {

  private CtuluUI ui_;
  
  private JComboBox coType_;
  private JTextField tfName_;
  private CardLayout lyAllValues_;
  private JPanel pnAllValues_;
  private FudaaManageExeTools mng_;
  private List<CtuluDialogPanel> pnParam=new ArrayList<CtuluDialogPanel>();

  public FudaaEditParamPanel(CtuluUI _ui, FudaaManageExeTools _mng) {
    setHelpText(FudaaLib.getS("Le nom peut �tre donn�, il apparait lors du lancement de l'utilitaire"));
    
    ui_=_ui;
    mng_=_mng;
    
    setLayout(new BuVerticalLayout(5, true, true));
    
    JPanel pnName=new JPanel();
    pnName.setLayout(new BorderLayout());
    pnName.add(new JLabel(FudaaLib.getS("Nom ")),BorderLayout.WEST);
    pnName.add(tfName_=new JTextField(),BorderLayout.CENTER);
    
    JPanel pnType=new JPanel();
    pnType.setLayout(new BorderLayout());
    pnType.add(new JLabel(FudaaLib.getS("Type ")),BorderLayout.WEST);
    coType_=new JComboBox(new String[]{""});
    coType_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        coTypeActionPerformed(e);
      }
    });
    pnType.add(coType_,BorderLayout.CENTER);
    
    lyAllValues_=new CardLayout();
    pnAllValues_=new JPanel();
    pnAllValues_.setLayout(lyAllValues_);
    
    pnAllValues_.add(new JPanel(),"0");
    
    for (FudaaExeTool.ParamI param : mng_.paramTypes) {
      coType_.addItem(param.getLabel());
      CtuluDialogPanel pn=param.getDefinitionPanel();
      pnAllValues_.add(pn,param.getType());
      pnParam.add(pn);
    }
    add(pnName);
    add(pnType);
    add(pnAllValues_);
  }
  
  private void coTypeActionPerformed(ActionEvent e) {
    int ind=coType_.getSelectedIndex();
    String name=ind==0?"0":mng_.paramTypes.get(ind-1).getType();
    lyAllValues_.show(pnAllValues_,name);
  }
  
  /**
   * D�finit le param�tre � modifier.
   * @param _param Le param�tre
   */
  public void setParam(FudaaExeTool.ParamI _param) {
    if (_param==null) return;
    
    for (int i=0; i<mng_.paramTypes.size(); i++) {
      if (mng_.paramTypes.get(i).getType().equals(_param.getType())) {
        pnParam.get(i).setValue(_param);
        coType_.setSelectedIndex(i+1);
        tfName_.setText(_param.name);
      }
    }
  }
  
  /**
   * @return Le nouveau param�tre d�fini
   */
  public FudaaExeTool.ParamI getParam() {
    FudaaExeTool.ParamI param=(FudaaExeTool.ParamI)pnParam.get(coType_.getSelectedIndex()-1).getValue(); 
    param.name=tfName_.getText().trim();
    return param;
  }
  
  @Override
  public boolean isDataValid() {
    if (coType_.getSelectedIndex()==0) {
      setErrorText(FudaaLib.getS("Le type doit �tre choisi"));
      return false;
    }
    else {
      return pnParam.get(coType_.getSelectedIndex()-1).isDataValid();
    }
  }
}
