/*
 * @creation     18 d�c. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.exetools;

import com.memoire.bu.BuVerticalLayout;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFileChooserPanel;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * Un panneau pour editer les propri�t�s d'un utilitaire.
 * 
 * @author marchand@deltacad.fr
 * @version $Id$
 */
public class FudaaEditExeToolPanel extends CtuluDialogPanel {

  private CtuluUI ui_;
  
  private JTextField tfName_;
  private CtuluFileChooserPanel fcExePath_;
  private CtuluFileChooserPanel fcRepExePath_;
  private CtuluListEditorModel mdParams_;
  private FudaaManageExeTools mng_;

  public FudaaEditExeToolPanel(CtuluUI _ui, FudaaManageExeTools _mng) {
    ui_=_ui;
    mng_=_mng;
    
    setLayout(new BuVerticalLayout(5, true, true));
    
    // Name
    tfName_=new JTextField();
    JButton btDummy=new JButton("...");
    btDummy.setVisible(false);
    JPanel pnName=new JPanel();
    pnName.setLayout(new BorderLayout(5,0));
    pnName.add(new JLabel(FudaaLib.getS("Nom")),BorderLayout.WEST);
    pnName.add(tfName_,BorderLayout.CENTER);
    pnName.add(btDummy,BorderLayout.EAST);
    add(pnName);
    setHelpText(FudaaLib.getS("Les param�tres sont transmis � l'utilitaire dans l'ordre ou ils sont d�finis\nUtilisez Monter/Descendre pour modifier cet ordre"));
    
    // Executable
    fcExePath_=new CtuluFileChooserPanel();
    add(CtuluFileChooserPanel.buildPanel(fcExePath_, FudaaLib.getS("Ex�cutable")));
    
    // Repertoire d'ex�cution
    fcRepExePath_=new CtuluFileChooserPanel();
    fcRepExePath_.setFileSelectMode(JFileChooser.DIRECTORIES_ONLY);
    add(CtuluFileChooserPanel.buildPanel(fcRepExePath_,FudaaLib.getS("R�pertoire d'ex�cution")));
    
    // Label parametres
    add(new JLabel(FudaaLib.getS("Liste des param�tres"),JLabel.LEFT));
    
    // Liste des param�tres
    mdParams_=new CtuluListEditorModel(true) {
      @Override
      public void edit(final int _r) {
        FudaaEditParamPanel pn=new FudaaEditParamPanel(ui_,mng_);
        pn.setParam((FudaaExeTool.ParamI)mdParams_.getValueAt(_r));
        if (pn.afficheModaleOk(ui_.getParentComponent(),FudaaLib.getS("D�finition du param�tre"))) {
          mdParams_.setValueAt(pn.getParam(), _r, 1);
        }
      }
      @Override
      public boolean isCellEditable(int _rowIndex, int _columnIndex) {
        return false;
      }
      @Override
      public Object createNewObject() {
        return null;//new FudaaExeTool.ParamI();
      }
    };
    
    CtuluListEditorPanel pnParams=new CtuluListEditorPanel(mdParams_, true, true, true, true, true) {
      @Override
      public void actionAdd() {
        FudaaEditParamPanel pn=new FudaaEditParamPanel(ui_,mng_);
        if (pn.afficheModaleOk(ui_.getParentComponent(),FudaaLib.getS("D�finition du param�tre"))) {
          final int n = table_.getRowCount();
          mdParams_.addElement(pn.getParam());
          table_.getSelectionModel().setSelectionInterval(n, n);
        }
      }
      @Override
      public void actionInserer() {
        FudaaEditParamPanel pn=new FudaaEditParamPanel(ui_,mng_);
        if (pn.afficheModaleOk(ui_.getParentComponent(), FudaaLib.getS("D�finition du param�tre"))) {
          final int n=table_.getSelectionModel().getMinSelectionIndex();
          mdParams_.add(n, pn.getParam());
          table_.getSelectionModel().setSelectionInterval(n, n);
        }
      }
    };
    
    pnParams.setValueListCellRenderer(new CtuluCellTextRenderer() {
      @Override
      public void setValue(Object _param) {
        FudaaExeTool.ParamI param=(FudaaExeTool.ParamI)_param;
        setText(param.getDefinitionCellText());
      }
    });

    add(pnParams);
  }
  
  @Override
  public void setValue(Object _tool) {
    if (!(_tool instanceof FudaaExeTool))
      throw new IllegalArgumentException("bad type parameter");
    setTool((FudaaExeTool)_tool);
  }
  
  @Override
  public FudaaExeTool getValue() {
    return getTool();
  }
  
  /**
   * D�finit l'utilitaire � modifier.
   * @param _tool L'utilitaire
   */
  public void setTool(FudaaExeTool _tool) {
    if (_tool==null) return;
    
    tfName_.setText(_tool.name);
    fcExePath_.setFile(_tool.exePath);
    fcRepExePath_.setFile(_tool.exeDirPath);
    
    mdParams_.setData(_tool.getParams());
  }
  
  public FudaaExeTool getTool() {
    FudaaExeTool tool=new FudaaExeTool();
    tool.name=tfName_.getText();
    tool.exePath=fcExePath_.getFile();
    tool.exeDirPath=fcRepExePath_.getFile();
    
    FudaaExeTool.ParamI[] params=new FudaaExeTool.ParamI[mdParams_.getRowCount()];
    mdParams_.getValues(params);
    tool.setParams(params);
    
    return tool;
  }
  
  @Override
  public boolean isDataValid() {
    if (tfName_.getText().trim().isEmpty()) {
      setErrorText(FudaaLib.getS("Le nom doit �tre donn�"));
      return false;
    }
    if (fcExePath_.getFile()==null || !fcExePath_.getFile().exists()) {
      setErrorText(FudaaLib.getS("L'ex�cutable n'a pas �t� donn� ou est invalide"));
      return false;
    }
    if (fcRepExePath_.getFile()==null || !fcRepExePath_.getFile().exists()) {
      setErrorText(FudaaLib.getS("Le r�pertoire d'ex�cution n'a pas �t� donn� ou est invalide"));
      return false;
    }
    return true;
  }
}
