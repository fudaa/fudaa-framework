/*
 * @creation 30 avr. 07
 * @modification $Date: 2007-04-30 14:22:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun;

import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import org.fudaa.ctulu.gui.CtuluLibSwing;

/**
 * @author fred deniger
 * @version $Id: FudaaProjectStateLabelListener.java,v 1.1 2007-04-30 14:22:43 deniger Exp $
 */
public class FudaaProjectStateLabelListener implements FudaaProjectStateListener {

  final JLabel lb_;

  public FudaaProjectStateLabelListener(final JLabel _lbModified) {
    super();
    lb_ = _lbModified;

  }

  public void finishUpdate(final FudaaProjetStateInterface _proj) {

  }

  public void initWith(final FudaaProjetStateInterface _l) {
    _l.addListener(this);
    projectStateChanged(_l);
  }

  @Override
  public void projectStateChanged(final FudaaProjetStateInterface _proj) {
    if (EventQueue.isDispatchThread()) {
      doChange(_proj);
    } else {
      SwingUtilities.invokeLater(new Runnable() {

        @Override
        public void run() {
          doChange(_proj);
        }

      });
    }

  }

  void doChange(final FudaaProjetStateInterface _proj) {
    if (lb_ != null) {
      lb_.setText(FudaaLib.getModifiedSaveState(_proj));
      lb_.setForeground(_proj.isParamsModified() || _proj.isUIModified() ? Color.red : CtuluLibSwing
          .getDefaultLabelForegroundColor());
    }
    finishUpdate(_proj);
  }

}
