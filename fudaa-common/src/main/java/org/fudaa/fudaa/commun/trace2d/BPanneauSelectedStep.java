/*
 * @file         BPanneauSelectedStep.java
 * @creation     1999-07-09
 * @modification $Date: 2006-09-19 15:10:23 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.trace2d;
import com.memoire.bu.BuResource;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
/**
 * Un panneau permettant la s�lection d'un pas de temps.
 *
 * @version      $Id: BPanneauSelectedStep.java,v 1.4 2006-09-19 15:10:23 deniger Exp $
 * @author       Bertrand Marchand
 */
public class BPanneauSelectedStep
  extends JPanel
  implements PropertyChangeListener {
  Border bdThis;
  boolean selEventSent_;
  JPanel pnPrincipal= new JPanel();
  BorderLayout lyThis= new BorderLayout();
  JPanel pnButtons= new JPanel();
  JButton btAppliquer= new JButton();
  JLabel lbPas= new JLabel();
  JComboBox coPas= new JComboBox();
  /** Step value */
  Double t_;
  /**
   * Constructeur
   */
  public BPanneauSelectedStep() {
    super();
    jbInit();
  }
  /**
   * D�finition de l'IU
   */
  public void jbInit() {
    this.setLayout(lyThis);
    btAppliquer.setText("Appliquer");
    btAppliquer.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btAppliquer_actionPerformed(e);
      }
    });
    btAppliquer.setEnabled(false);
    btAppliquer.setIcon(BuResource.BU.getIcon("appliquer"));
    coPas.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        coPas_actionPerformed(e);
      }
    });
    bdThis=
      BorderFactory.createCompoundBorder(
        new EtchedBorder(
          EtchedBorder.LOWERED,
          Color.white,
          new Color(134, 134, 134)),
        BorderFactory.createEmptyBorder(5, 5, 5, 5));
    pnPrincipal.setBorder(bdThis);
    lbPas.setText("Pas : ");
    coPas.setPreferredSize(new Dimension(100, 21));
    this.add(pnPrincipal, BorderLayout.CENTER);
    pnPrincipal.add(lbPas, null);
    pnPrincipal.add(coPas, null);
    this.add(pnButtons, BorderLayout.SOUTH);
    pnButtons.add(btAppliquer, null);
  }
  /**
   * Changement de la palette associ�e
   */
  @Override
  public void propertyChange(PropertyChangeEvent _evt) {
    btAppliquer.setEnabled(true);
  }
  public void clearSteps() {
    coPas.removeAllItems();
  }
  public void addStep(Double _t) {
    coPas.addItem(_t);
    if (coPas.getItemCount() == 1)
      setSelectedStep(_t); // First step add => Selected
  }
  /**
   * Get the selected step. Selected step keeps value until "Appliquer" is
   * actioned.
   * @return Selected step.
   */
  public Double getSelectedStep() {
    return t_;
  }
  /**
   * Define selected step
   */
  public void setSelectedStep(Double _t) {
    t_= _t;
    coPas.setSelectedItem(_t);
    btAppliquer.setEnabled(false);
  }
  //----------------------------------------------------------------------------
  // Actions
  //----------------------------------------------------------------------------
  /**
   * Nouveau pas s�lectionn�.
   */
  public void coPas_actionPerformed(ActionEvent _evt) {
    btAppliquer.setEnabled(true);
  }
  /**
   * Bouton appliquer activ�.
   */
  public void btAppliquer_actionPerformed(ActionEvent _evt) {
    t_= (Double)coPas.getSelectedItem();
    btAppliquer.setEnabled(false);
    firePropertyChange("selectedStep", null, this);
  }
  /**
   * Pour test.
   */
  public static void main(String[] args) {
    try {
      UIManager.setLookAndFeel(
        "com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
    } catch (Exception _exc) {}
    JFrame f= new JFrame("Selected step");
    BPanneauSelectedStep pn= new BPanneauSelectedStep();
    pn.addStep(new Double(1.0));
    pn.addStep(new Double(100.000));
    pn.addStep(new Double(2000.000));
    f.getContentPane().add(pn, BorderLayout.CENTER);
    f.pack();
    f.setVisible(true);
  }
}
