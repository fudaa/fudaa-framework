/*
 *  @file         FudaaExecPanel.java
 *  @creation     28 mai 2003
 *  @modification $Date: 2007-03-30 15:36:49 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.exec;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuFileChooser;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuResource;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluFavoriteFiles;
import org.fudaa.ctulu.gui.CtuluFileChooser;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author deniger
 * @version $Id: FudaaExecPanel.java,v 1.28 2007-03-30 15:36:49 deniger Exp $
 */
public class FudaaExecPanel extends CtuluDialogPanel {
  FudaaExec fudaaExec_;
  FudaaExec[] othersFudaaExec_;
  private JTextField txtCmd_;
  private JTextField txtArgs_;
  private JTextField txtName_;
  private JCheckBox cbBatch_;
  private JButton iconButton_;
  String iconURL_;

  public FudaaExecPanel() {
    this(null);
  }

  public FudaaExecPanel(final FudaaExec _f) {
    addEmptyBorder(5);
    fudaaExec_ = _f;
    final BuGridLayout l = new BuGridLayout(2, 5, 5, true, false, false, false, false);
    l.setXAlign(0);
    setLayout(l);
    // addLabel("Nom : ");
    final String s = ": ";
    txtName_ = addLabelStringText(FudaaLib.getS("Nom") + s);
    txtName_.setColumns(20);
    txtCmd_ = addLabelFileChooserPanel(FudaaLib.getS("Ex�cutable") + s, null,false, false);
    txtArgs_ = addLabelStringText(FudaaLib.getS("Arguments") + s);
    txtArgs_.setColumns(10);
    final String tltip = FudaaLib.getS("Si cocher, l'ex�cutable sera lanc� dans un terminal ou une fen�tre DOS");
    addLabel(FudaaLib.getS("Lancer dans un terminal")).setToolTipText(tltip);
    cbBatch_ = new BuCheckBox();
    add(cbBatch_);

    cbBatch_.setToolTipText(tltip);
    addLabel(FudaaLib.getS("Ic�ne") + s);
    iconButton_ = new BuButton();
    iconButton_.setPreferredSize(new Dimension(32, 32));
    iconButton_.setMaximumSize(new Dimension(32, 32));
    iconButton_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(final ActionEvent _ae) {
        final BuFileChooser bf = new CtuluFileChooser(false);
        if (iconURL_ == null) {
          bf.setCurrentDirectory(CtuluFavoriteFiles.getLastDir());
        } else {
          bf.setCurrentDirectory(FuLib.getParentFile(new File(iconURL_)));
        }
        bf.setDialogTitle((fudaaExec_ == null ? CtuluLibString.EMPTY_STRING : fudaaExec_.getViewedName()) + s
            + BuResource.BU.getString("Ic�ne"));
        bf.setMultiSelectionEnabled(false);
        final int r = bf.showOpenDialog(FudaaExecPanel.this);
        if (r == JFileChooser.APPROVE_OPTION) {
          setIconUrl(bf.getSelectedFile().getAbsolutePath());
        }
      }
    });
    add(iconButton_);
    initFudaaExec();
  }

  private void initFudaaExec() {
    if (fudaaExec_ == null) {
      txtName_.setText(CtuluLibString.EMPTY_STRING);
      txtCmd_.setText(CtuluLibString.EMPTY_STRING);
      iconButton_.setIcon(FudaaExec.EMPTY_TOOL_ICON);
      iconURL_ = CtuluLibString.EMPTY_STRING;
      cbBatch_.setSelected(false);
      txtArgs_.setText(CtuluLibString.EMPTY_STRING);
    } else {
      txtName_.setText(fudaaExec_.getViewedName());
      txtCmd_.setText(fudaaExec_.getExecCommand());
      cbBatch_.setSelected(fudaaExec_.isBatchMode());
      txtArgs_.setText(fudaaExec_.getArgs());
      final Icon ic = fudaaExec_.getIcon();
      iconButton_.setIcon(ic);

      if (!fudaaExec_.isIconInterne()) {
        iconButton_.setToolTipText(fudaaExec_.getIconURL());
      }
      iconURL_ = fudaaExec_.getIconURL();
    }
  }

  void setIconUrl(final String _iconURL) {
    iconURL_ = _iconURL;
    final Icon ic = FudaaExec.getIcon(iconURL_);
    if (ic != null) {
      iconButton_.setToolTipText(iconURL_);
      iconButton_.setIcon(ic);
    }
  }

  public void setControle(final FudaaExec[] _old) {
    othersFudaaExec_ = _old;
  }

  @Override
  public boolean apply() {
    String s = FuLib.expandedPath(txtCmd_.getText());
    if (fudaaExec_ == null) {
      fudaaExec_ = new FudaaExec(FuLib.clean(txtName_.getText()).toLowerCase(), txtCmd_.getText(), iconURL_);
      fudaaExec_.setShownName(txtName_.getText());
    } else {
      if (!s.equals(fudaaExec_.getExecCommand())) {
        fudaaExec_.setExecCommand(s);
      }

      s = txtName_.getText();
      if (!s.equals(fudaaExec_.getShownName())) {
        fudaaExec_.setShownName(s);
      }
      if ((iconURL_ == null)) {
        if ((fudaaExec_.getIconURL() != null)) {
          fudaaExec_.setIconURL(null);
        }
      } else if (!iconURL_.equals(fudaaExec_.getIconURL())) {
        fudaaExec_.setIconURL(iconURL_);
      }
    }
    fudaaExec_.setBatchMode(cbBatch_.isSelected());
    fudaaExec_.savePref();
    return true;
  }

  public FudaaExec getExec() {
    return fudaaExec_;
  }

  /**
   *
   */
  @Override
  public Object getValue() {
    return fudaaExec_;
  }

  @Override
  public void setValue(final Object _v) {
    if ((_v == fudaaExec_) || ((_v != null) && (_v.equals(fudaaExec_)))) {
      return;
    }
    if (_v == null) {
      fudaaExec_ = null;
      initFudaaExec();
    }
    if (_v instanceof FudaaExec) {
      fudaaExec_ = (FudaaExec) _v;
      initFudaaExec();
    }
  }

  @Override
  public boolean isDataValid() {
    if (fudaaExec_ == null) {
      if (othersFudaaExec_ == null) {
        FuLog.warning("FudaaExecPanel :NO CONTROL");
        return false;
      }
      if (txtName_.getText().trim().length() == 0) {
        setErrorText(FudaaLib.getS("Le nom de l'ex�cutable est vide"));
        return false;
      }
      final String name = FuLib.clean(txtName_.getText()).toLowerCase();
      if (FudaaExec.getIdNameIndex(othersFudaaExec_, name) >= 0) {
        setErrorText(FudaaLib.getS("L'identifiant {0} est d�j� utilis�.", name) + CtuluLibString.LINE_SEP
            + FudaaLib.getS("Modifiez le nom de votre application") + CtuluLibString.DOT);
        return false;
      }
    }
    return true;
  }
}
