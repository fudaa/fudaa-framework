/*
 * @creation 8 f�vr. 07
 * @modification $Date: 2007-03-15 17:01:09 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.dja.DjaOptions;
import com.memoire.dja.DjaOwner;
import javax.swing.Icon;
import org.fudaa.ebli.animation.EbliAnimationSourceInterface;
import org.fudaa.ebli.controle.BConfigurePaletteTargetInterface;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * @author fred deniger
 * @version $Id: FudaaDjaFormInterface.java,v 1.1 2007-03-15 17:01:09 deniger Exp $
 */
public interface FudaaDjaFormInterface extends BConfigurePaletteTargetInterface, DjaOwner, DjaOptions {

  EbliAnimationSourceInterface getAnim();

  boolean isAnimated();

  void refresh();

  String getTitle();
  Icon getIcon();

  void setTitle(String _t);

  String getDescription();

  String getDjaName();

  TraceLigneModel getContourLigne();

  boolean isBackgroundModifiable();

  void active(FudaaReportFrameController _controller);

  void unactive(FudaaReportFrameController _controller);

}
