package org.fudaa.fudaa.commun;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.pdf.CtuluPanelPdfViewer;
import org.fudaa.dodico.objet.CExec;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.ebli.commun.EbliComponentFactory;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.jdesktop.swingx.JXTreeTable;
import org.jdesktop.swingx.treetable.DefaultMutableTreeTableNode;
import org.jdesktop.swingx.treetable.DefaultTreeTableModel;
import org.jdesktop.swingx.treetable.MutableTreeTableNode;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Internal Frame qui gere les aide fudaa au format pdf. Il faut lui fournir le repertoire qui contient tous les fichiers d'aidr.
 * La structure de l'aide est la suivante: - REP1 (ex repertoire POST) - voir tout si REP1.pdf existe (ex pdf complet POST.pdf) - affiche tous les
 * fichiers pdf du r�pertoire avec poru nom de chapitre le nom du fichier sans pdf: - nomChapitre1.pdf (ex si layout.pdf existe, il sera affich� comme
 * r�pertoire) - ... - nomchapitreN.pdf - REP2 (ex rurepertoire RUBAR) - voir tout si REP2.pdf existe (ex pdf complet RUBAR.pdf) -... -... - REPN
 * Cette classe g�n�rera une internalframe qui contient un arbre des fichiers aide et un viewer pdf int�gr�. Les execution des viewer pdf des os et
 * aussi impl�ment� (pour optimiser la memoire java)
 * EN COURS DE DEV
 *
 * @author Adrien Hadoux
 */
public class FudaaHelpPDFViewer extends BuInternalFrame implements ListSelectionListener {
  /**
   * La liste des projet afffichage graphique.
   */
  public JXTreeTable treePost_;
  /**
   * Jar qui contient l'arborescence de fichiers aide.
   */
  String pathJarDocAide_;
  /**
   * Map qui enregistre l'ensemble des fichiers pdf disponibles dans le fichier jar.
   */
  public Map<String, JarEntry> mapEntryPdfJar_;
  /**
   * File qui contient l'arborescence de fichiers aide.
   */
  File fileDocAide_;
  /**
   * Boolean qui indique si on recupere la doc depuis un repertoire classique ou un jar.
   */
  boolean useJarAide = true;
  /**
   * modele structure arborescente
   */
  DefaultTreeTableModel modelList_;
  ArrayList<String> listeColonnesModel_ = new ArrayList<String>();
  BuRadioButton useOsPdfViewer_ = new BuRadioButton(FudaaLib.getS("Utiliser le lecteur pdf du syst�me d'explloitation"));
  BuRadioButton useCtuluPdfViewer_ = new BuRadioButton(FudaaLib.getS("Utiliser le lecteur pdf int�gr� � Fudaa"));
  ;
  /**
   * IHM lecteur pdf fudaa
   */
  CtuluPanelPdfViewer pdfViewer;
  /**
   * Implementation mere
   */
  FudaaCommonImplementation ui_;
  BuPanel outlinePanel;
  BuPanel thumbscrollContainer;
  BuPanel conteneurPdf_;
  public static String FUDAA_HOME = "FUDAA_HOME";
  public static String nomRep = "doc";

  /**
   * path par defaut de la documentation
   */
  /**
   * Constructeur pour la creation de l'interface Fudaa.
   *
   * @param jarName nom du jar � utiliser (celui qui contient la doc). Peut etre le jar logiciel lui meme.
   * @param pathFichierJar le path dans le fichier jar: Non UTILISE !
   * @param _ui implementaion fudaa.
   * @param useThumbs Utilise ou non les apercu images des pages (pour un gain de place, mettre � false).
   */
  public FudaaHelpPDFViewer(String jarName, String pathFichierJar, FudaaCommonImplementation _ui, boolean useThumbs) {
    ui_ = _ui;
    //-- tentative de verification que le jar existe bien a cet emplacement --//

    String jarPath = null;
    String pathFudaa = System.getenv(FUDAA_HOME);
    final String propertyForHelp = "fudaa.doc." + jarName;
    if (pathFudaa == null) {
      jarPath = FudaaPreferences.FUDAA.getStringProperty(propertyForHelp, null);
    } else {
      jarPath = new File(new File(pathFudaa, nomRep), jarName).getAbsolutePath();
    }
    pathJarDocAide_ = retrieveCorrectPathForJarDoc(jarPath, propertyForHelp);
    if (pathJarDocAide_ == null) {
      return;
    }
    File jarFile = new File(pathJarDocAide_);
    if (!jarFile.isFile()) {
      return;
    }
    // --creation du modele depuis un fichier jar ou un r�pertoire classique selon le constructeur appel�.--//
    listeColonnesModel_.add(FudaaResource.FUDAA.getString("Aide disponible"));
    modelList_ = new DefaultTreeTableModel(constructStructureModelWithJAREntree(), listeColonnesModel_);

    constructFudaaHelpPDFViewer(useThumbs);
  }

  /**
   * Verifie que le chemin saisi contient bien un jar sinon demande correction a l'utilisateur.
   *
   */
  public String retrieveCorrectPathForJarDoc(String initPathForHelp, String propertyForHelp) {
    File test = initPathForHelp == null ? null : new File(initPathForHelp);
    if (initPathForHelp == null || !test.exists() || test.isDirectory()) {
      ui_.warn(FudaaResource.FUDAA.getString("Aide non trouv�e"), FudaaResource.FUDAA.getString("Choisir le chemin vers l'archive de l'aide"));

      BuFileChooser chooser = new BuFileChooser();
      int rep = chooser.showOpenDialog(ui_.getParentComponent());
      if (rep == BuFileChooser.APPROVE_OPTION) {
        initPathForHelp = chooser.getSelectedFile().getAbsolutePath();
        FudaaPreferences.FUDAA.putStringProperty(propertyForHelp, initPathForHelp);
      }
    }
    return initPathForHelp;
  }

  public FudaaHelpPDFViewer(File fichier, FudaaCommonImplementation _ui, boolean useThumbs) {
    ui_ = _ui;
    fileDocAide_ = fichier;
    useJarAide = false;
    // --creation du modele depuis un fichier jar ou un r�pertoire classique selon le constructeur appel�.--//
    listeColonnesModel_.add(FudaaResource.FUDAA.getString("Aide disponible"));
    modelList_ = new DefaultTreeTableModel(constructStructureModelWithFileEntree(), listeColonnesModel_);

    constructFudaaHelpPDFViewer(useThumbs);
  }

  boolean isFrame = false;
  JFrame frame_ = new JFrame();

  private void constructFudaaHelpPDFViewer(boolean useThumbs) {

    treePost_ = new JXTreeTable(modelList_);
    //-- listener du tree --//
    treePost_.addMouseListener(new MouseAdapter() {
      @Override
      public void mousePressed(final MouseEvent e) {
        if (e.getClickCount() == 2) {
          affichePdfNode(e.getX(), e.getY());
        }
      }
//      public void mouseReleased(final MouseEvent e) {
//        if ( e.getClickCount() == 2) {
//          affichePdfNode(e.getX(), e.getY());
//        }
//      }
    });
    treePost_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    treePost_.setBorder(BorderFactory.createEtchedBorder());
    treePost_.setEditable(false);
    setTitle(FudaaLib.getS("Aide Fudaa"));
    final JScrollPane listScroller = new JScrollPane(treePost_);
    listScroller.setBorder(BorderFactory.createTitledBorder(FudaaResource.FUDAA.getString("Menu des fichiers")));

    final BuPanel operationPanel = new BuPanel(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER));
    operationPanel.add(useOsPdfViewer_);
    operationPanel.add(useCtuluPdfViewer_);

    ButtonGroup group = new ButtonGroup();
    group.add(useOsPdfViewer_);
    group.add(useCtuluPdfViewer_);

    //-- construction de l'interface pdf--//
    outlinePanel = new BuPanel();
    outlinePanel.setBorder(BorderFactory.createTitledBorder("Chapitres"));
    thumbscrollContainer = new BuPanel();
    thumbscrollContainer.setBorder(BorderFactory.createTitledBorder(CtuluResource.CTULU.getString("Pages")));

    pdfViewer = new CtuluPanelPdfViewer(useThumbs, ui_, outlinePanel, thumbscrollContainer);

    conteneurPdf_ = new BuPanel(new BorderLayout());

    conteneurPdf_.add(new JScrollPane(pdfViewer), BorderLayout.CENTER);
    conteneurPdf_.add(new JScrollPane(outlinePanel), BorderLayout.EAST);
    conteneurPdf_.add(operationPanel, BorderLayout.SOUTH);

    //-- ajotu des thumbnails si activ�--//
    if (useThumbs) {
      conteneurPdf_.add(new JScrollPane(thumbscrollContainer), BorderLayout.WEST);
    }

    final JSplitPane pane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    pane.setTopComponent(listScroller);
    pane.setBottomComponent(conteneurPdf_);
    pane.setDividerLocation(250);

    pdfViewer.getPage().setPreferredSize(Toolkit.getDefaultToolkit().getScreenSize());

    final BuPanel contentPane = new BuPanel(new BorderLayout());

    contentPane.add(pane, BorderLayout.CENTER);
    contentPane.add(pdfViewer.getToolBar(), BorderLayout.NORTH);
    this.setContentPane(contentPane);
    useCtuluPdfViewer_.setSelected(true);

    useOsPdfViewer_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        pane.remove(conteneurPdf_);

        rafraichir();
      }
    });
    useCtuluPdfViewer_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        pane.setBottomComponent(conteneurPdf_);
        pane.setDividerLocation(250);
        rafraichir();
      }
    });

    //--MenuBarre --//
    JMenuBar bm = pdfViewer.getMenuBar();
    pdfViewer.getMenuBar().add(new JMenu(FudaaResource.FUDAA.getString("Edition")));
    this.setJMenuBar(bm);

    //-- construction de la toolbar --//

    EbliActionSimple action = new EbliActionSimple(FudaaResource.FUDAA.getString("Sauvegarder"), FudaaResource.FUDAA.getToolIcon("pdfdoc"), "SAUVERPDF") {
      @Override
      public void actionPerformed(ActionEvent _e) {
        sauvegarderFichierPdf();
      }
    };
    pdfViewer.getToolBar().add(action.buildToolButton(EbliComponentFactory.INSTANCE), 0);
    pdfViewer.getMenuBar().getMenu(0).add(action.buildMenuItem(EbliComponentFactory.INSTANCE), 0);
    action = new EbliActionSimple(FudaaResource.FUDAA.getString("Rafraichir"), FudaaResource.FUDAA.getToolIcon("RAFRAICHIR"), "RAFRAICHIRPDF") {
      @Override
      public void actionPerformed(ActionEvent _e) {
        rafraichir();
      }
    };
    pdfViewer.getToolBar().add(action.buildToolButton(EbliComponentFactory.INSTANCE), 0);
    pdfViewer.getMenuBar().getMenu(2).add(action.buildMenuItem(EbliComponentFactory.INSTANCE), 0);

    action = new EbliActionSimple(FudaaLib.getS("D�tacher la fen�tre"), BuResource.BU.getToolIcon("pleinecran"), "PLEINEECRAN") {
      @Override
      public void actionPerformed(ActionEvent _e) {

        if (!isFrame) {

          frame_.setTitle(FudaaLib.getS("Aide Fudaa"));
          frame_.setSize(800, 600);
          frame_.setContentPane(contentPane);
          frame_.setJMenuBar(pdfViewer.getMenuBar());
          ui_.removeInternalFrame(FudaaHelpPDFViewer.this);
          frame_.setVisible(true);
        } else {
          FudaaHelpPDFViewer.this.setJMenuBar(pdfViewer.getMenuBar());
          ui_.addInternalFrame(FudaaHelpPDFViewer.this);
          FudaaHelpPDFViewer.this.setContentPane(contentPane);
          frame_.setVisible(false);
        }
        isFrame = !isFrame;
      }
    };
    pdfViewer.getToolBar().add(action.buildToolButton(EbliComponentFactory.INSTANCE), 0);
    pdfViewer.getMenuBar().getMenu(2).add(action.buildMenuItem(EbliComponentFactory.INSTANCE), 0);

    pdfViewer.getToolBar().add(useCtuluPdfViewer_);
    pdfViewer.getToolBar().add(useOsPdfViewer_);

    //-- initialiser le pdfviewer interne avec le premier pdf feuille de la liste --//
    if (useJarAide) {
      //useCtuluPdfViewer_.setSelected(true);
      if (mapEntryPdfJar_ != null && !mapEntryPdfJar_.isEmpty()) {
        openPdfJar(mapEntryPdfJar_.get(mapEntryPdfJar_.keySet().iterator().next()));
      }
    }
  }

  /**
   * Construit le model du tableau si l'aides est propos�es depuis un repertoire classique.
   */
  public DefaultMutableTreeTableNode constructStructureModelWithFileEntree() {

    if (fileDocAide_ == null || !fileDocAide_.isDirectory()) {
      return new DefaultMutableTreeTableNode(FudaaLib.getS("Pas de documentation "));
    }

    //-- root de l'arborescence --//
    DefaultMutableTreeTableNode root = new DefaultMutableTreeTableNode(FudaaLib.getS("Aide") + " " + fileDocAide_.getName());

    //-- ajout recursivement le contenu du reperttoire a l'arbre --//
    addRecursiveFilesToTree(root, fileDocAide_);

    return root;
  }

  /**
   * Ajout recursif des fichiers du repertoire d'aide.
   *
   * @param parent
   * @param repertoire
   */
  public void addRecursiveFilesToTree(DefaultMutableTreeTableNode parent, File repertoire) {

    //-- Repertoire ou se situent les fichiers d'aide --//
    for (int i = 0; i < repertoire.listFiles().length; i++) {
      File fichier = repertoire.listFiles()[i];
      if (fichier != null) {
        if (fichier.isDirectory()) {
          //-- si un repertoire l'user object est un simple nom de fichier et non pas un objet File --//
          DefaultMutableTreeTableNode nodeRepertoire = new DefaultMutableTreeTableNode(fichier.getName());
          parent.add(nodeRepertoire);
          //-- on ajoute recursivement le contenu du repertoire fils --//
          addRecursiveFilesToTree(nodeRepertoire, fichier);
        } else {
          DefaultMutableTreeTableNode node = new DefaultMutableTreeTableNode(fichier);
          //-- on ajoute le node qui contient le File --//
          parent.add(node);
        }
      }
    }
  }

  /**
   * Construit le model du tableau si l'aides est propos�es d'un fichier jar d'aide.
   */
  public DefaultMutableTreeTableNode constructStructureModelWithJAREntree() {

    mapEntryPdfJar_ = new HashMap<String, JarEntry>();

    if (pathJarDocAide_ == null) {
      return new DefaultMutableTreeTableNode(FudaaLib.getS("Pas de documentation "));
    }

    JarFile jarDocAide = null;
    try {
      jarDocAide = new JarFile(pathJarDocAide_);
    } catch (IOException e1) {
      ui_.error(FudaaResource.FUDAA.getString("Erreur, Fichier documentation jar inexact \n Il n'existe pas de fichier jar � l'emplacement: " + pathJarDocAide_));

      return new DefaultMutableTreeTableNode(FudaaLib.getS("Pas de documentation "));
    }
    if (jarDocAide == null) {
      return new DefaultMutableTreeTableNode(FudaaLib.getS("Pas de documentation "));
    }

    //-- root de l'arborescence --//
    DefaultMutableTreeTableNode root = new DefaultMutableTreeTableNode(FudaaLib.getS("Aide")/*+" "+jarDocAide.getName()*/);

    //-- ajout recursivement le contenu du reperttoire a l'arbre --//
    addFilesFromJarToTree(root, jarDocAide.entries());

    //-- fermeture du jar doc --//
    try {
      jarDocAide.close();
    } catch (IOException e) {
      ui_.error(FudaaResource.FUDAA.getString("Erreur, Impossible de fermer le flux de lecture du jar: " + pathJarDocAide_));
    }

    return root;
  }

  /**
   * Ajout recursif des fichiers d'un fichier jar d'aide. N'accepte aue des jar de repertoire de profondeur 1.
   *
   * @param root
   */
  public void addFilesFromJarToTree(DefaultMutableTreeTableNode root, Enumeration<JarEntry> listeEntries) {
    //-- map qui stocke l'ensemble des nodes de niveau 1, ce sont ces nodes qui vont attacher les noeuds feuilles pdf --//
    HashMap<String, DefaultMutableTreeTableNode> mapSousRepertoireNiveau1 = new HashMap<String, DefaultMutableTreeTableNode>();

    //-- Repertoire ou se situent les fichiers d'aide --//
    while (listeEntries.hasMoreElements()) {

      //-- recuperation de l'entree jar --//
      JarEntry entry = listeEntries.nextElement();
      if (entry != null) {
        if (entry.isDirectory()) {
          //-- si un repertoire l'user object est un simple nom de fichier et non pas un objet File --//
//          DefaultMutableTreeTableNode nodeRepertoire=new DefaultMutableTreeTableNode(entry.getName());
//          parent.add(nodeRepertoire);
//          //-- on ajoute recursivement le contenu du repertoire fils --//
          //addRecursiveFilesFromJarToTree(nodeRepertoire, entry.);
        } else {
          //-- recuperation du nom du repertoire --//
          String nomRepertoire = null;

          //-- tentative de recuperation du nom de rep --//
          int deb = 0;
          int fin = entry.getName().lastIndexOf("/");
          if (fin != -1) {
            nomRepertoire = (entry.getName()).substring(deb, fin);
          }

          //-- si nom repertoire est null, ceci est un pdf niveau 0 --//
          if (nomRepertoire == null) {

            //-- on verifie qu'il s'agit bien d'un fichier pdf --//
            if (entry.getName().contains(".pdf")) {
              mapEntryPdfJar_.put(entry.getName().replace(".pdf", ""), entry);
              DefaultMutableTreeTableNode node = new DefaultMutableTreeTableNode(entry.getName().replace(".pdf", ""));
              //-- on ajoute le node qui contient le File --//
              root.add(node);
            }
          } else {
            if (entry.getName().contains(".pdf")) {
              //-- on tente de recuperer le node associ� a ce repertoire --//
              DefaultMutableTreeTableNode nodeRepertoire = mapSousRepertoireNiveau1.get(nomRepertoire);
              //-- si on ne trouve pas de node associ�, on en cree 1 et on l'ajoute dans la map --//
              if (nodeRepertoire == null) {
                nodeRepertoire = new DefaultMutableTreeTableNode(nomRepertoire);
                mapSousRepertoireNiveau1.put(nomRepertoire, nodeRepertoire);
              }
              //-- creation du node entry pdf --//

              //-- recuperation du no du fichier --//
              String nomFichierPdf = entry.getName().replace(nomRepertoire + "/", "").replace("+", " ");
              mapEntryPdfJar_.put(nomFichierPdf.replace(".pdf", ""), entry);
              DefaultMutableTreeTableNode node = new DefaultMutableTreeTableNode(nomFichierPdf.replace(".pdf", ""));
              nodeRepertoire.add(node);
            }
          }
        }
      }
    }

    //-- on ajoute au  parent les node de tous les repertoire de la map --//
    for (String key : mapSousRepertoireNiveau1.keySet()) {
      if (!key.equals("META-INF")) {
        DefaultMutableTreeTableNode nodeRep = mapSousRepertoireNiveau1.get(key);
        if (nodeRep != null) {
          root.add(nodeRep);
        }
      }
    }
  }

  /**
   * Affiche le fichier pdf correspondant au feuille cliqu�
   *
   * @param x
   * @param y
   */
  public void affichePdfNode(final int x, final int y) {
    // Get the tree element under the mouse
    final TreePath clickedElement = treePost_.getPathForLocation(x, y);
    MutableTreeTableNode treeNode = null;
    if (clickedElement != null) {
      treeNode = (MutableTreeTableNode) clickedElement.getLastPathComponent();
    }
    if (treeNode != null) {
      // -- recuperation du node associe --//
      String nomNoeud = null;
      File fichierChoisi = null;
      JarEntry jarEntryChoisie = null;
      if (treeNode.getUserObject() instanceof String) {
        nomNoeud = (String) treeNode.getUserObject();
      } else if (treeNode.getUserObject() instanceof File) {
        fichierChoisi = (File) treeNode.getUserObject();
      }
//        else if(treeNode.getUserObject() instanceof JarEntry){
//          jarEntryChoisie=(JarEntry)treeNode.getUserObject();
//        }

      if (fichierChoisi == null && nomNoeud == null) {
        return;
      }
      //-- ouverture dans le viewer du fichier pdf slectionn� selon le mode pdf ou file--//

      if (fichierChoisi != null) {
        openPdfFile(fichierChoisi);
      } else if (mapEntryPdfJar_ != null) {
        //-- on tente de recuperer la jar entry pour le nom de fichier specifie --//
        jarEntryChoisie = mapEntryPdfJar_.get(nomNoeud);
        if (jarEntryChoisie != null) {
          openPdfJar(jarEntryChoisie);
        }
      }
    }
  }

  /**
   * retourne le fichier courant.
   */
  JarEntry getCurrentJarEntry() {
    return currentJarEntry_;
  }

  void rafraichir() {
    pdfViewer.revalidate();
    this.revalidate();
    frame_.validate();
  }

  /**
   * le jar entry en train d'etre test�.
   */
  JarEntry currentJarEntry_ = null;

  /**
   * Methode qui ouvre le fichier pdf sp�cifi� par un fichier dur.
   *
   * @param fichier
   */
  public void openPdfFile(File fichier) {
    try {
      if (this.useOsPdfViewer_.isSelected()) {
        //-- ouverture du fichier pdf avec le viewer os --//
        final String os = System.getProperty("os.name");

        if (os.startsWith("Windows")) {
          //-- sous crosoft --//
          Process p = Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + fichier.getAbsolutePath() + "");
          //p.waitFor();
        } else {
          //-- linux power --//
          String[] cmd = new String[2];
          cmd[0] = "evince";
          cmd[1] = fichier.getAbsolutePath();
          try {
            final CExec ex = new CExec();
            ex.setCommand(cmd);
            ex.setOutStream(System.out);
            ex.setErrStream(System.err);
            ex.exec();
          } catch (final Throwable _e1) {
            ui_.error(FudaaResource.FUDAA.getString("Impossible d'ouvrir le fichier pdf sous Linux, le visionneur int�gr� � Fudaa sera utilis�."));
            this.useOsPdfViewer_.setSelected(false);
            openPdfFile(fichier);
          }
        }
      } else {
        //-- ouverture du fichier pdf avec le viewer fudaa --//
        this.pdfViewer.openFile(fichier);

//      this.pdfViewer.getPage().setSize(2000,2000);
//      rafraichir();

      }
    } catch (Exception e) {
      e.printStackTrace();
      ui_.error(FudaaResource.FUDAA.getString("Impossible d'ouvrir le fichier pdf " + fichier.getName()));
    }

    //-- mise a jour de tous les composants --//
    this.rafraichir();
  }

  /**
   * Methode qui ouvre le fichier pdf sp�cifi� par un fichier dur.
   *
   * @param jarEntry
   */
  public void openPdfJar(JarEntry jarEntry) {
    //-- creation d'un fichier temporaire depuis la jar entry specifiee ---//
    File fileTemporaire = null;
    try {
      fileTemporaire = File.createTempFile("tmpPDF", ".pdf");
    } catch (IOException e) {
      ui_.error(FudaaResource.FUDAA.getString("Impossible de g�n�rer un fichier temporaire..."));
      return;
    }
    generateFileFromJar(jarEntry, fileTemporaire);
    currentJarEntry_ = jarEntry;

    //-- ouverture du fichier pdf --//
    openPdfFile(fileTemporaire);
  }

  /**
   * Algo de generation du fichier depuis un format jar
   *
   * @param entry
   */
  private void generateFileFromJar(JarEntry entry, File fTemp) {
    InputStream entryStream = null;

    try (JarFile jarDocAide = new JarFile(pathJarDocAide_)) {
      entryStream = jarDocAide.getInputStream(entry);
      //-- creation d' un fichier output --//
      try (FileOutputStream file = new FileOutputStream(fTemp)) {
        //-- buffer de lecture --//
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = entryStream.read(buffer)) != -1) {
          file.write(buffer, 0, bytesRead);
        }
        FuLog.debug(entry.getName() + " extrait.");
      }
    } catch (FileNotFoundException e) {
      ui_.error(FudaaResource.FUDAA.getString("Erreur, Fichier documentation jar inexact \n Il n'existe pas de fichier jar � l'emplacement: " + pathJarDocAide_));
    } catch (IOException e) {
      ui_.error(FudaaResource.FUDAA.getString("Impossible d'extraire le fichier depuis le fichier jar " + entry.getName()));
    }
  }

  /**
   * Methode qui sauvegarde le fichier en cours de lecture sur l'emplacement voulu.
   */
  public void sauvegarderFichierPdf() {
    JarEntry fichierJar = getCurrentJarEntry();
    if (fichierJar == null) {
      return;
    }
    File fichierDest = null;
    BuFileChooser chooser = new BuFileChooser();
    int rep = chooser.showOpenDialog(ui_.getParentComponent());
    if (rep == BuFileChooser.APPROVE_OPTION) {
      fichierDest = chooser.getSelectedFile();
    }

    if (fichierDest != null) {
      generateFileFromJar(fichierJar, fichierDest);
    }
  }

  @Override
  public void valueChanged(ListSelectionEvent e) {
  }
}
