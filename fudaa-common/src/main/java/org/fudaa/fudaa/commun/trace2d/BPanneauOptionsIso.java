/*
 * @file         BPanneauOptionsIso.java
 * @creation     1999-07-09
 * @modification $Date: 2006-09-19 15:10:23 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.trace2d;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuResource;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
/**
 * Un panneau permettant la saisie des options de trac� d'isolignes/
 * isosurfaces.
 *
 * @version      $Id: BPanneauOptionsIso.java,v 1.4 2006-09-19 15:10:23 deniger Exp $
 * @author       Bertrand Marchand
 */
public class BPanneauOptionsIso
  extends JPanel
  implements PropertyChangeListener {
  BuGridLayout lyPrincipal= new BuGridLayout();
  Border bdThis;
  boolean selEventSent_;
  JPanel pnPrincipal= new JPanel();
  BorderLayout lyThis= new BorderLayout();
  JPanel pnButtons= new JPanel();
  JButton btAppliquer= new JButton();
  JCheckBox cbIsosurfaces= new JCheckBox();
  JCheckBox cbIsolignes= new JCheckBox();
  JCheckBox cbMaillage= new JCheckBox();
  /**
   * Constructeur
   */
  public BPanneauOptionsIso() {
    super();
    jbInit();
  }
  /**
   * D�finition de l'IU
   */
  public void jbInit() {
    cbMaillage.setText("Maillage");
    cbMaillage.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        cbMaillage_actionPerformed(e);
      }
    });
    cbIsolignes.setText("Isolignes");
    cbIsolignes.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        cbIsolignes_actionPerformed(e);
      }
    });
    cbIsosurfaces.setText("Isosurfaces");
    cbIsosurfaces.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        cbIsosurfaces_actionPerformed(e);
      }
    });
    this.setLayout(lyThis);
    btAppliquer.setText("Appliquer");
    btAppliquer.addActionListener(new java.awt.event.ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btAppliquer_actionPerformed(e);
      }
    });
    btAppliquer.setEnabled(false);
    btAppliquer.setIcon(BuResource.BU.getIcon("appliquer"));
    bdThis=
      BorderFactory.createCompoundBorder(
        new EtchedBorder(
          EtchedBorder.LOWERED,
          Color.white,
          new Color(134, 134, 134)),
        BorderFactory.createEmptyBorder(5, 5, 5, 5));
    lyPrincipal.setColumns(1);
    lyPrincipal.setHgap(5);
    lyPrincipal.setCfilled(false);
    pnPrincipal.setLayout(lyPrincipal);
    pnPrincipal.setBorder(bdThis);
    this.add(pnPrincipal, BorderLayout.CENTER);
    pnPrincipal.add(cbIsosurfaces, null);
    pnPrincipal.add(cbIsolignes, null);
    pnPrincipal.add(cbMaillage, null);
    this.add(pnButtons, BorderLayout.SOUTH);
    pnButtons.add(btAppliquer, null);
  }
  /**
   * Changement de la palette associ�e
   */
  @Override
  public void propertyChange(PropertyChangeEvent _evt) {
    btAppliquer.setEnabled(true);
  }
  /**
   * Maillage s�lectionn� ?
   */
  public boolean isMaillageSelected() {
    return cbMaillage.isSelected();
  }
  /**
   * Isosurfaces s�lectionn� ?
   */
  public boolean isIsosurfacesSelected() {
    return cbIsosurfaces.isSelected();
  }
  /**
   * Isolignes s�lectionn� ?
   */
  public boolean isIsolignesSelected() {
    return cbIsolignes.isSelected();
  }
  //----------------------------------------------------------------------------
  // Actions
  //----------------------------------------------------------------------------
  /**
   * Isosurfaces s�lectionn�.
   */
  public void cbIsosurfaces_actionPerformed(ActionEvent _evt) {
    btAppliquer.setEnabled(true);
  }
  /**
   * Isolignes s�lectionn�.
   */
  public void cbIsolignes_actionPerformed(ActionEvent _evt) {
    btAppliquer.setEnabled(true);
  }
  /**
   * Maillage s�lectionn�.
   */
  public void cbMaillage_actionPerformed(ActionEvent _evt) {
    btAppliquer.setEnabled(true);
  }
  /**
   * Bouton appliquer activ�.
   */
  public void btAppliquer_actionPerformed(ActionEvent _evt) {
    btAppliquer.setEnabled(false);
    firePropertyChange("optionsIso", null, this);
  }
  /**
   * Pour test.
   */
  public static void main(String[] args) {
    //    try {
    //      UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
    //    } catch (Exception _exc) {}
    JFrame f= new JFrame("Options iso");
    BPanneauOptionsIso pn= new BPanneauOptionsIso();
    f.getContentPane().add(pn, BorderLayout.CENTER);
    f.pack();
    f.setVisible(true);
  }
}
