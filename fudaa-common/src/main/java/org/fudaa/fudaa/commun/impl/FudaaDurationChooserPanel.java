/**
 * @creation 2 mars 2005
 * @modification $Date: 2007-04-02 08:56:22 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.SwingConstants;
import org.fudaa.ctulu.CtuluDurationDateFormatter;
import org.fudaa.ctulu.CtuluDurationFormatter;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluNumberFormat;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.gui.CtuluDefaultComboBoxModel;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluDurationDateFormatterComponent;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author Fred Deniger
 * @version $Id: FudaaDurationChooserPanel.java,v 1.2 2007-04-02 08:56:22 deniger Exp $
 */
public class FudaaDurationChooserPanel extends CtuluDialogPanel implements ItemListener {

  /**
   * @return une liste de formatteur par defaut.
   */
  public static CtuluNumberFormatI[] buildDefaultFormatter() {
    return new CtuluNumberFormatI[] { CtuluDurationFormatter.DEFAULT_TIME, new CtuluDurationFormatter(true, true),
        new CtuluDurationFormatter(false, true), new CtuluDurationFormatter(true, false), new CtuluDurationFormatter(false, false) };
  }

  /**
   * @param _f le fenetre parente
   * @param _init le format initiale
   * @return le format choisi ou le format initiale si l'utilisateur n'a rien choisi.
   */
  public static CtuluNumberFormatI chooseFormatter(final Frame _f, final CtuluNumberFormatI _init) {
    final FudaaDurationChooserPanel pn = new FudaaDurationChooserPanel(_init);
    if (CtuluDialogPanel.isOkResponse(pn.afficheModale(_f, FudaaLib.getS("Formatage du temps")))) {
      return pn.getFormatter();
    }
    return _init;
  }

  private static boolean containDurationDate(CtuluNumberFormatI[] in) {
    for (CtuluNumberFormatI ctuluNumberFormatI : in) {
      if (ctuluNumberFormatI instanceof CtuluDurationDateFormatter) {
        return true;
      }
    }
    return false;
  }

  CtuluNumberFormatI fmt_;
  BuCheckBox cbZeroUsed_;
  BuLabel lbExample_;

  /**
   * @param _fmt le format choisi par defaut
   */
  public FudaaDurationChooserPanel(final CtuluNumberFormatI _fmt) {
    this(buildDefaultFormatter(), _fmt);
  }

  /**
   * @param _defautFormatter les formatteur a proposer
   * @param _selectedFormatter le format utilise
   */
  public FudaaDurationChooserPanel(final CtuluNumberFormatI[] _defautFormatter, final CtuluNumberFormatI _selectedFormatter) {
    setLayout(new BuGridLayout(2, 5, 5));
    addLabel(FudaaLib.getS("Format:"));
    final int i = CtuluLibArray.findObject(_defautFormatter, _selectedFormatter);
    if (i >= 0) {
      fmt_ = _defautFormatter[i];
    }
    final BuComboBox cb = new BuComboBox();
    cb.setRenderer(new DefaultListCellRenderer() {

      @Override
      public Component getListCellRendererComponent(final JList _list, final Object _value, final int _index, final boolean _isSelected,
          final boolean _cellHasFocus) {
        final Component r = super.getListCellRendererComponent(_list, _value, _index, _isSelected, _cellHasFocus);
        if (_value == null) {
          setText(FudaaLib.getS("Aucun"));
        } else if (_value instanceof CtuluDurationDateFormatter) {
          setText(FudaaLib.getS("A partir d'une date"));
        }
        return r;
      }
    });
    cb.setModel(new CtuluDefaultComboBoxModel(_defautFormatter));
    if (fmt_ != null) {
      cb.setSelectedItem(fmt_);
    } else {
      cb.setSelectedIndex(0);
    }
    cb.addItemListener(this);
    add(cb);
    if (containDurationDate(_defautFormatter)) {
      add(new JLabel());
      final JButton editDuration = new JButton(FudaaLib.getS("Editer"));
      cb.addItemListener(new ItemListener() {

        @Override
        public void itemStateChanged(ItemEvent e) {
          editDuration.setEnabled(cb.getSelectedItem() instanceof CtuluDurationDateFormatter);
        }
      });
      editDuration.setEnabled(cb.getSelectedItem() instanceof CtuluDurationDateFormatter);
      add(editDuration);
      editDuration.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
          editCurrentItem();

        }

      });
    }
    if (_selectedFormatter instanceof CtuluDurationFormatter || _defautFormatter[_defautFormatter.length - 1] instanceof CtuluDurationFormatter) {
      addLabel(CtuluLib.getS("Afficher les valeurs nulles:"));
      cbZeroUsed_ = new BuCheckBox();
      cbZeroUsed_.setSelected(_selectedFormatter != null && (_selectedFormatter instanceof CtuluDurationFormatter)
          && ((CtuluDurationFormatter) _selectedFormatter).isAlwaysDisplayZero());
      cbZeroUsed_.addItemListener(this);
      add(cbZeroUsed_);
    }
    addLabel(FudaaLib.getS("Exemple:"));
    lbExample_ = new BuLabel();
    lbExample_.setHorizontalAlignment(SwingConstants.CENTER);
    lbExample_.setHorizontalTextPosition(SwingConstants.CENTER);
    lbExample_.setBackground(Color.WHITE);
    lbExample_.setOpaque(true);
    add(lbExample_);
    updateFmt();
  }

  private void updateFmt() {
    final StringBuffer lbText = new StringBuffer(200);
    lbText.append("<html><table >");
    lbText.append("<tr><td><u>").append(FudaaLib.getS("Temps")).append(" (s)").append("</u></td><td>&gt;&gt;<u>").append(FudaaLib.getS("Résultats"))
        .append("</u></td></tr><tr><td>");
    int t1 = 15 + 60 * 23 + 3600 * 14;
    String res = fmt_ == null ? Integer.toString(t1) : fmt_.format(t1);
    lbText.append(t1).append("</td><td>&gt;&gt;").append(res).append("</td></tr><td>");
    t1 = 0 + 60 * 12 + 3600 * 4 + (3600 * 24) * 5 + (3600 * 24 * 30) * 18;
    res = fmt_ == null ? Integer.toString(t1) : fmt_.format(t1);
    lbText.append(t1).append("</td><td>&gt;&gt;").append(res).append("</td></tr></table></html>");
    lbExample_.setText(lbText.toString());
  }

  /**
   * @return le format choisi
   */
  public final CtuluNumberFormatI getFormatter() {
    return fmt_;
  }

  /**
   * Used by date formatter.
   */
  private long defaultReferenceDate;

  public long getDefaultReferenceDate() {
    return defaultReferenceDate;
  }

  public void setDefaultReferenceDate(long defaultReferenceDate) {
    this.defaultReferenceDate = defaultReferenceDate;
  }

  @Override
  public void itemStateChanged(final ItemEvent _e) {
    if (_e.getSource() == cbZeroUsed_) {
      if (fmt_ != null) {
        ((CtuluDurationFormatter) fmt_).setAlwaysDisplayZero(cbZeroUsed_.isSelected());
      }
      updateFmt();
    } else if (_e.getStateChange() == ItemEvent.SELECTED) {
      final JComboBox cb = ((JComboBox) _e.getSource());
      if (cb.getSelectedIndex() == 0) {
        fmt_ = null;
      } else {
        fmt_ = (CtuluNumberFormat) cb.getSelectedItem();
        if (cbZeroUsed_ != null) {
          ((CtuluDurationFormatter) fmt_).setAlwaysDisplayZero(cbZeroUsed_.isSelected());
        }
      }
      updateFmt();
    }
  }

  private void editCurrentItem() {
    CtuluDurationDateFormatterComponent cmp = new CtuluDurationDateFormatterComponent();
    cmp.setDefaultReferenceTime(defaultReferenceDate);
    cmp.setInitFormatter((CtuluDurationDateFormatter) fmt_);
    CtuluDialogPanel pn = new CtuluDialogPanel();
    pn.setLayout(new BorderLayout());
    pn.add(cmp.buildPanel());
    if (pn.afficheModaleOk(this)) {
      ((CtuluDurationDateFormatter) fmt_).initFrom(cmp.buildFormatter());
      updateFmt();
    }

  }
}