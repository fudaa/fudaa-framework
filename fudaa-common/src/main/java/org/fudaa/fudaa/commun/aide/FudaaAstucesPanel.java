/*
 * @file         FudaaAstucesPanel.java
 * @creation     2000-09-01
 * @modification $Date: 2007-05-04 13:58:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.aide;

import com.memoire.bu.BuLabelMultiLine;
import com.memoire.bu.BuOverlayLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPicture;
import java.awt.Font;
import java.util.Calendar;
import javax.swing.border.EmptyBorder;
import org.fudaa.fudaa.commun.FudaaAstucesAbstract;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * Classe de cr�ation du Panel d'affichage des astuces.
 * 
 * @version $Revision: 1.9 $ $Date: 2007-05-04 13:58:05 $ by $Author: deniger $
 * @author Fred Deniger
 */
public class FudaaAstucesPanel extends BuPanel {
  // variables privees
  /**
   * Astuces utilisees.
   */
  private final FudaaAstucesAbstract astuce_;
  /**
   * Le label contenant les astuces.
   */
  private BuLabelMultiLine texte_;
  /**
   * L'image de fond.
   */
  private BuPicture imageFond_;
  /**
   * Chaine correspondant a la fin du fichier image.
   */
  private final static String FIN_NOM_IMAGE = "_astuce.jpg";

  /**
   * Initialisation: texte,astuce et image.
   * 
   * @param _astuce
   */
  public FudaaAstucesPanel(final FudaaAstucesAbstract _astuce) {
    super();
    setBorder(new EmptyBorder(15, 0, 0, 0));
    initialiseEcran();
    astuce_ = _astuce;
    changeTexte();
  }

  /**
   * Initialisation de l'image de fond puis du texte.
   */
  private void initialiseEcran() {
    setLayout(new BuOverlayLayout());
    buildImageFond();
    texte_ = new BuLabelMultiLine("?");
    texte_.setOpaque(false);
    texte_.setBorder(new EmptyBorder(5, 5, 5, 5));
    // texte_.setFont(BuLib.deriveFont("Label",Font.PLAIN,0));
    texte_.setFont(new Font("SansSerif", Font.PLAIN, 12));
    // texte_.setSize(imageFond_.getPreferredSize());
    add(texte_);
    add(imageFond_);
  }

  /**
   * Choisit une image en fonction du mois. Les images sont dans le fichier astuces.
   */
  private void buildImageFond() {
    final String mois = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1);
    imageFond_ = new BuPicture(FudaaResource.FUDAA.getIcon("astuces/" + mois + FIN_NOM_IMAGE));
    imageFond_.setMode(BuPicture.SCALE);
  }

  /**
   * @return imageFond_
   */
  public BuPicture getImageFond() {
    return imageFond_;
  }

  /**
   * Choisit une astuce au hasard et.
   */
  public final void changeTexte() {
    String t = astuce_.getAstuceHasard();
    for (int i = 35; i < t.length(); i += 35) {
      final int j = t.indexOf(' ', i);
      if (j >= 0) {
        t = t.substring(0, j) + '\n' + t.substring(j + 1);
      }
      if (j > i) {
        i = j;
      }
    }
    texte_.setText(t);
  }
  /*
   * public static void main(String[] argv) { FudaaAstucesPanel panel=new FudaaAstucesPanel(FudaaAstuces.FUDAA); JFrame
   * fen=new JFrame("essai"); fen.getContentPane().add(panel); fen.pack(); fen.show(); }
   */
}
