/**
 * @file FudaaTee.java
 * @creation 1999-08-27
 * @modification $Date: 2006-09-19 15:01:56 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.fudaa.commun;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibMessage;

/**
 * @version $Revision: 1.20 $ $Date: 2006-09-19 15:01:56 $ by $Author: deniger $
 * @author Axel von Arnim
 */
public final class FudaaTee {

  /**
   * Le fichier contenant les messages de l'appli.
   */
  private File tsLogFile_ = new File(System.getProperty("user.home"), "ts.log");

  private static FudaaTee singleton;

  /**
   * La sortie standard et la sortie d'erreur seront redirig�e dans un fichier ts.log.
   */
  private FudaaTee() {
    try {
      if (null != CtuluLibFile.canWrite(tsLogFile_)) {
        tsLogFile_ = File.createTempFile("fudaaTs", ".log");
      }
    } catch (final IOException e) {
      System.err.println(FudaaLib.getS("Impossible de cr�er le fichier de log"));
    }
    if (tsLogFile_ != null && null == CtuluLibFile.canWrite(tsLogFile_)) {
      // pour fulog
      // pour ne pas couper les messages apres 80 caract�res :utile ?
      System.setProperty("fulog.cut", "no");
      // BM : Pas necessaire de limiter le niveau de log.
      try {
        OutputStream log = new BufferedOutputStream(new FileOutputStream(tsLogFile_));
        final PrintStream ps = new PrintStream(log, true);
        // BM : Ca double les messages dans la console ??
//        FuLog.LOGSTREAM = ps;
        System.setErr(ps);
        System.setOut(ps);
      } catch (final FileNotFoundException e1) {
        e1.printStackTrace();
      }
    } else {
      System.err.println(FudaaLib.getS("Impossible de cr�er le fichier de log"));
    }
  }

  public File getFile() {
    return tsLogFile_;
  }

  public static synchronized void createFudaaTee() {
    if (singleton == null) {
      if (CtuluLibMessage.DEBUG) {
        CtuluLibMessage.debug("ts.log create");
      }
      singleton = new FudaaTee();
    }
  }

  public static synchronized FudaaTee getFudaaTee() {
    return singleton;
  }

  public synchronized static boolean isCreated() {
    return singleton != null;

  }

}
