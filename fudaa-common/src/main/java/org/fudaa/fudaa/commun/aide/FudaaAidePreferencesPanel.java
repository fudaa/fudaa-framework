/*
 * @file         FudaaPreferencesPanel.java
 * @creation     2001-09-13
 * @modification $Date: 2008-01-15 11:29:55 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.aide;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuVerticalLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * Panneau de preferences pour l'aide dans Fudaa. Gere l'apparition des astuces.
 *
 * @version $Revision: 1.12 $ $Date: 2008-01-15 11:29:55 $ by $Author: bmarchan $
 * @author Fred Deniger
 */
public class FudaaAidePreferencesPanel extends BuAbstractPreferencesPanel implements ActionListener {
  /**
   * La checkbox controlant l'aparition des astuces au demarrage.
   */
  private final BuCheckBox cbAstuceVisible_;
  /**
   * L'application concernee.
   */
  BuCommonImplementation app_;
  /**
   * Les options.
   */
  BuPreferences options_;
  /**
   * Les noms des preferences utilisees.
   */
  private static final String ASTUCES_VISIBLES_PREF = "astuce.visible";

  /**
   * Renvoie <code>true</code> si les astuces sont declares comme visibles au demarrage dans les preferences
   * <code>_options</code>.
   */
  public static boolean isAstucesVisibles(final BuPreferences _options) {
    return _options.getBooleanProperty(ASTUCES_VISIBLES_PREF, true);
  }

  /**
   * Modifie la valeur concernant l'apparition automatique des astuces au demarrage de l'application.
   */
  public static void setAstucesVisibles(final BuPreferences _options, final boolean _b) {
    _options.putBooleanProperty(ASTUCES_VISIBLES_PREF, _b);
    _options.writeIniFile();
  }

  /**
   * Initialise la bordure utilisee par les panels principaux: panel du format de page et le panel des entetes et pieds
   * de page.
   *
   * @param _titre le titre ajoute a la bordure.
   */
  private  Border initTitreBorder(final String _titre) {
    return BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder(FudaaLib.getS(_titre)), BorderFactory
        .createEmptyBorder(5, 5, 5, 5));
  }

  /**
   * Initialisation de l'application et des composant swing.
   *
   * @param _app
   */
  public FudaaAidePreferencesPanel(final BuCommonImplementation _app, final BuPreferences _options) {
    super();
    setLayout(new BuVerticalLayout(5, true, true));
    setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    app_ = _app;
    options_ = _options;
    // astuces
    final BuPanel panelAstuce = new BuPanel();
    panelAstuce.setLayout(new BuBorderLayout());
    panelAstuce.setBorder(initTitreBorder("Astuces"));
    cbAstuceVisible_ = new BuCheckBox(FudaaLib.getS("Astuces au démarrage"));
    panelAstuce.add(cbAstuceVisible_, BuBorderLayout.CENTER);
    add(panelAstuce);
    updateComponents();
  }

  private void fillTable() {

    options_.putBooleanProperty(ASTUCES_VISIBLES_PREF, cbAstuceVisible_.isSelected());
  }

  private void updateComponents() {
    cbAstuceVisible_.setSelected(options_.getBooleanProperty(ASTUCES_VISIBLES_PREF, true));
  }

  /**
   * Le titre du panel.
   *
   * @return "Fudaa"
   */
  @Override
  public String getTitle() {
    return FudaaLib.getS("aide");
  }

  /**
   * @return true
   */
  @Override
  public boolean isPreferencesValidable() {
    return true;
  }

  /**
   * @return false
   */
  @Override
  public boolean isPreferencesCancelable() {
    return true;
  }

  /**
   * Gestion des evenements.
   *
   * @param _evt
   */
  @Override
  public void actionPerformed(final ActionEvent _evt) {}

  /**
   * Enregistrement des modification apportees dans ce panel.
   */
  @Override
  public void validatePreferences() {
    fillTable();
    options_.writeIniFile();
    updateComponents();
  }

  /**
   * Annuler les modifications.
   */
  @Override
  public void cancelPreferences() {
    options_.readIniFile();
    updateComponents();
  }
}
