/*
 * @creation 9 mars 07
 * @modification $Date: 2007-05-04 13:58:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.dja.DjaForm;
import com.memoire.dja.DjaGridEvent;
import com.memoire.dja.DjaHandle;
import com.memoire.dja.DjaResource;
import com.memoire.dja.DjaText;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import javax.swing.Icon;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ebli.animation.EbliAnimationSourceInterface;
import org.fudaa.ebli.controle.BConfigurableComposite;
import org.fudaa.ebli.controle.BConfigurableInterface;
import org.fudaa.ebli.controle.BSelecteurTargetInterface;
import org.fudaa.ebli.trace.TraceLigne;
import org.fudaa.ebli.trace.TraceLigneModel;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author fred deniger
 * @version $Id: FudaaDjaFormAbstract.java,v 1.4 2007-05-04 13:58:05 deniger Exp $
 */
public abstract class FudaaDjaFormAbstract extends DjaForm implements FudaaDjaFormInterface {

  TraceLigne ligne_ = new TraceLigne(TraceLigne.LISSE, 1f, Color.BLACK);
  Icon icon_;

  double rotation_;

  String title_ = "Form";

  public FudaaDjaFormAbstract() {
    super(CtuluLibString.EMPTY_STRING);
    title_ = getDescription();
    if (getDjaName() != null) icon_ = DjaResource.DJA.getIcon("dja-" + getDjaName().toLowerCase(), 16);
  }

  @Override
  public Icon getIcon() {
    return icon_;
  }

  public void clearTransform() {
    rotation_ = 0;
    shearX_ = 0;
    shearY_ = 0;
  }

  @Override
  public void paint(Graphics _g) {
    Graphics2D g2d = (Graphics2D) _g;
    AffineTransform old = prepare(g2d);
    super.paint(g2d);
    if (old != null) g2d.setTransform(old);
  }

  private AffineTransform prepare(Graphics2D _g2d) {
    AffineTransform old = null;
    if (isTransformChanged()) {
      Rectangle l = getBounds();

      old = _g2d.getTransform();
      AffineTransform transform = _g2d.getTransform();
      initTransform(l, transform);
      _g2d.setTransform(transform);
    }
    return old;
  }

  private boolean isTransformChanged() {
    return Math.abs(rotation_) > 1E-3 || Math.abs(shearY_) > 1E-3 || Math.abs(shearX_) > 1E-3;
  }

  double shearX_;
  double shearY_;

  private void initTransform(Rectangle _bounds, AffineTransform _transform) {
    double centerY = _bounds.getCenterY();
    double centerX = _bounds.getCenterX();
    _transform.translate(centerX, centerY);
    _transform.shear(shearX_, shearY_);
    _transform.rotate(rotation_);
    _transform.translate(-centerX, -centerY);

  }

  @Override
  public void paintInteractive(Graphics _g) {
    Graphics2D g2d = (Graphics2D) _g;
    AffineTransform old = prepare(g2d);
    super.paintInteractive(_g);
    if (old != null) g2d.setTransform(old);
  }


  @Override
  public DjaHandle[] getHandles() {
    // Rectangle rec = getBounds();
    int x = getX();
    int y = getY();
    int w = getWidth();
    int h = getHeight();
    DjaHandle[] r = new DjaHandle[8];
    r[0] = new DjaHandle(this, NORTH, x + w / 2, y - deltaY);
    r[2] = new DjaHandle(this, EAST, x + w - 1 + deltaX, y + h / 2);
    r[4] = new DjaHandle(this, SOUTH, x + w / 2, y + h - 1 + deltaY);
    r[6] = new DjaHandle(this, WEST, x - deltaX, y + h / 2);
    r[1] = new DjaHandle(this, NORTH_EAST, x + w - 1 + deltaX, y - deltaY);
    r[3] = new DjaHandle(this, SOUTH_EAST, x + w - 1 + deltaX, y + h - 1 + deltaY);
    r[5] = new DjaHandle(this, SOUTH_WEST, x - deltaX, y + h - 1 + deltaY);
    r[7] = new DjaHandle(this, NORTH_WEST, x - deltaX, y - deltaY);
    if (isTransformChanged()) {
      Rectangle rect = getInitBounds();
      AffineTransform transfo = new AffineTransform();
      initTransform(rect, transfo);
      Point2D.Double tmpSrc = new Point2D.Double();
      Point2D.Double tmpRes = new Point2D.Double();
      for (int i = 0; i < r.length; i++) {
        tmpSrc.x = r[i].getX();
        tmpSrc.y = r[i].getY();
        transfo.transform(tmpSrc, tmpRes);
        r[i].setX((int) tmpRes.x);
        r[i].setY((int) tmpRes.y);
      }
    }

    return r;
  }

  

  public abstract FudaaDjaFormAbstract createNew();


  @Override
  protected final DjaText createText(String _text, int _num) {
    return new FudaaDjaText(this, _num, _text);
  }

  @Override
  public void setX(int _x) {
    int x = Math.max(5 - getWidth(), _x);
    if (x != getX()) {
      super.setX(x);
      // fireGridEvent(this, DjaGridEvent.MODIFIED);
    }
  }

  @Override
  public void setY(int _y) {
    int y = Math.max(5 - getHeight(), _y);
    if (y != getY()) {
      super.setY(y);
      // fireGridEvent(this, DjaGridEvent.MODIFIED);
    }
  }

  @Override
  public void setWidth(int _width) {
    if (_width != super.getWidth()) {
      // fireGridEvent(this, DjaGridEvent.MODIFIED);
      super.setWidth(_width);
    }
  }

  @Override
  public void setHeight(int _height) {
    if (_height != super.getHeight()) {
      // fireGridEvent(this, DjaGridEvent.MODIFIED);
      super.setHeight(_height);
    }
  }

  public Rectangle getInitBounds() {
    return super.getBounds();
  }

  @Override
  public Rectangle getBounds() {
    Rectangle rec = getInitBounds();
    if (!isTransformChanged()) return rec;

    AffineTransform tr = new AffineTransform();
    initTransform(rec, tr);
    return tr.createTransformedShape(rec).getBounds();
  }

  @Override
  protected Rectangle getExtendedBounds(Rectangle _r) {
    Rectangle res = super.getExtendedBounds(_r);
    if (isTransformChanged()) {
      AffineTransform tr = new AffineTransform();
      initTransform(res, tr);
      return tr.createTransformedShape(res).getBounds();
    }
    /*
     * if (Math.abs(rotation_) < 1E-3) return res; AffineTransform tr = AffineTransform.getRotateInstance(rotation_,
     * res.getCenterX(), res.getCenterY()); _r.setBounds(tr.createTransformedShape(res).getBounds());
     */
    return res;
  }

  @Override
  public void active(FudaaReportFrameController _controller) {}

  @Override
  public EbliAnimationSourceInterface getAnim() {
    return null;
  }

  @Override
  public boolean isBackgroundModifiable() {
    return true;
  }

  @Override
  public final Color getBackground() {
    return background_ == null ? null : background_.getColor();
  }

  protected BConfigurableComposite createAffichageConfigure() {
    return new BConfigurableComposite(new FudaaDjaConfigurePosition(this, (FudaaDjaGrid) getGrid()),
        new FudaaDjaFormConfigure(this), new FudaaDjaFormTransform(this), FudaaLib.getS("Affichage"));
  }

  @Override
  public BConfigurableInterface[] getConfigureInterfaces() {
    return new BConfigurableInterface[] { new BConfigurableComposite(createAffichageConfigure(),
        new FudaaReportTextConfigure(this), null) };
  }

  @Override
  public final TraceLigneModel getContourLigne() {
    return ligne_.getModel();
  }

  @Override
  public final Color getForeground() {
    return ligne_.getCouleur();
  }

  @Override
  public String getTitle() {
    return title_;
  }

  @Override
  public BSelecteurTargetInterface getVisibleTitleTarget() {
    return new FudaaReportSelecteurTitleTarget(this);
  }

  @Override
  public boolean isAnimated() {
    return false;
  }

  @Override
  public boolean isTitleModifiable() {
    return true;
  }

  @Override
  public void refresh() {}

  @Override
  public final void setForeground(Color _foreground) {
    if (ligne_ != null) ligne_.setCouleur(_foreground);
  }

  /**
   * @param _title the title to set
   */
  @Override
  public void setTitle(String _title) {
    if (title_ != _title) {
      title_ = _title;
      fireGridEvent(this, DjaGridEvent.MODIFIED);
    }
  }

  @Override
  public void unactive(FudaaReportFrameController _controller) {}

  public double getRotation() {
    return rotation_;
  }

  public void setRotation(double _rotation) {
    rotation_ = _rotation;
  }

  public double getShearX() {
    return shearX_;
  }

  public void setShearX(double _shearX) {
    shearX_ = _shearX;
    if (Math.abs(shearX_) < 1E-3) shearX_ = 0;
  }

  public double getShearY() {
    return shearY_;
  }

  public void setShearY(double _shearY) {
    shearY_ = _shearY;
    if (Math.abs(shearY_) < 1E-3) shearY_ = 0;
  }

}
