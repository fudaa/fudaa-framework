/**
 * @creation 12 mai 2004
 * @modification $Date: 2007-01-19 13:14:06 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogInput;
import com.memoire.bu.BuDynamicMenu;
import com.memoire.bu.BuExplorer;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuResource;
import com.memoire.dnd.DndSource;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;
import com.memoire.vfs.VfsFile;
import com.memoire.vfs.VfsFileRam;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.Observable;
import java.util.Observer;
import java.util.StringTokenizer;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.ctulu.gui.CtuluFavoriteFiles;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.ressource.FudaaResource;

public class FudaaExplorer extends BuExplorer implements Observer {

  private class FavoriteMenu extends BuDynamicMenu {

    public FavoriteMenu() {
      super(FudaaResource.FUDAA.getString("Favoris"), "FAVORITES", BuResource.BU.getIcon("aucun"));
      setEnabled(true);
    }

    @Override
    protected void build() {
      if (getMenuComponentCount() == 0 && CtuluFavoriteFiles.INSTANCE.getNbFavoritesFile() > 0) {
        CtuluFavoriteFiles.INSTANCE.fillMenu(this, FudaaExplorer.this);
        addSeparator();
        addMenuItem(FudaaLib.getS("Editer"), "EDIT_FAVORITES", FudaaExplorer.this);
      }

    }

    @Override
    protected boolean isActive() {
      return true;
    }
  }

  private FavoriteMenu favMenu_;

  public FudaaExplorer() {
    this(null, true, null);
  }

  public FudaaExplorer(final BuCommonInterface _app) {
    this(_app, false, null);
  }

  public FudaaExplorer(final BuCommonInterface _app, final boolean _viewer) {
    this(_app, _viewer, null);
  }

  public FudaaExplorer(final BuCommonInterface _app, final boolean _viewer, final VfsFile _path) {
    super(_app, _viewer, _path, true, true);
    buildFavorites();
    CtuluFavoriteFiles.INSTANCE.setObserver(this);
    removeDndSource(dirs_);
    removeDndSource(files_);
  }

  private static void removeDndSource(final JComponent _c) {
    _c.putClientProperty("DND_SOURCE", null);
    final MouseListener[] ls = _c.getMouseListeners();
    if (ls != null) {
      for (int i = ls.length - 1; i >= 0; i--) {
        if (ls[i] instanceof DndSource) {
          _c.removeMouseListener(ls[i]);
          _c.removeMouseMotionListener((DndSource) ls[i]);
        }
      }
    }

  }

  public FudaaExplorer(final VfsFile _path) {
    this(null, true, _path);
  }

  @Override
  public void actionPerformed(final ActionEvent _evt) {
    final String action = _evt.getActionCommand();
    if ("EDIT_FAVORITES".equals(action)) {
      CtuluFavoriteFiles.INSTANCE.editFavorite(this);
    } else if (CtuluFavoriteFiles.isFavAction(action)) {
      final VfsFile r = VfsFile.createFile(CtuluFavoriteFiles.INSTANCE.getPathFromMenuCommand(_evt.getActionCommand()));
      if (r != null) {
        timestamp_ = 1;
        updateLists(r, null);
      }
    } else {
      super.actionPerformed(_evt);
    }
  }

  protected void addFavorites() {
    CtuluFavoriteFiles.INSTANCE.addFile(getSelectedDirectory());
  }

  @Override
  protected void addMtab() {
    if (VfsFile.isRamMode()) {
      roots_.addItem(new VfsFileRam("/"));
      return;
    }

    final String od = clean(BuPreferences.BU.getStringProperty("directory.open", null));
    if (od != null) {
      roots_.addItem(new Root(od, od, "directory"));
    }

    final String sd = clean(BuPreferences.BU.getStringProperty("directory.save", null));
    if ((sd != null) && !sd.equals(od)) {
      roots_.addItem(new Root(sd, sd, "directory"));
    }

    final String ud = clean(FuLib.getSystemProperty("user.dir"));
    if ((ud != null) && !ud.equals(sd) && !ud.equals(od)) {
      roots_.addItem(new Root(ud, ud, "directory"));
    }

    final String hd = clean(FuLib.getSystemProperty("user.home"));
    if ((hd != null) && !hd.equals(ud) && !hd.equals(sd) && !hd.equals(od)) {
      roots_.addItem(new Root(hd, getS("Maison"),
          "home"));
    }

    if (FuLib.isUnix()) {
      LineNumberReader rin = null;
      try {
        rin = new LineNumberReader(new InputStreamReader(new FileInputStream("/etc/mtab")));
        // FileReader (TMP)

        String l;
        while ((l = rin.readLine()) != null) {
          l = l.trim();
          if (l.startsWith("#")) {
            continue;
          }

          try {
            final StringTokenizer st = new StringTokenizer(l);
            final String device = st.nextToken();
            final String mount = st.nextToken();
            String type = st.nextToken();
            if (!"none".equals(device) && !"swap".equals(type) && !"devpts".equals(type) && !"devfs".equals(type)
                && !"tmpfs".equals(type) && !"usbdevfs".equals(type) && !"/boot".equals(mount)) {
              if (!device.startsWith("/")) {
                type = "computer";
              }
              if (mount.endsWith("floppy")) {
                type = "floppy";
              }

              String display = mount.substring(mount.lastIndexOf(File.separator) + 1);

              if ("".equals(display)) {
                display = "/";
                /*
                 * else display=display.substring(0,1) .toUpperCase()+display.substring(1);
                 */
              }

              roots_.addItem(new Root(mount, display, type));
            }
          } catch (final Exception ex) {}
        }
      } catch (final Throwable th) {} // IOException ex
      finally {
        try {
          if (rin != null) {
            rin.close();
          }
        } catch (final IOException e) {
          // ne fait rien
        }
      }
    }

    try {
      // Method m=File.class.getMethod("listRoots",CLASS0);
      // File[] l=(File[])m.invoke(null,OBJECT0);
      // TMP
      File[] l = null;
      // if(FuLib.isWindows()) l=new File[] { new File("C:\\") };
      if (FuLib.isWindows()) {
        l = File.listRoots();
      } else {
        l = new File[0];
      }

      for (int i = 0; i < l.length; i++) {
        final String p = l[i].getPath(); // AbsolutePath();
        // if(!p.equals("/")&&!p.equals("A:\\")&&!p.equals("B:\\"))
        roots_.addItem(new Root(p, p, "disk"));
      }
    }
    // catch(NoSuchMethodException ex1) { }
    catch (final Exception ex2) {
      FuLog.error(ex2);
    }

    /*
     * for(int i=1;i<=9;i++) { String path=BuPreferences.BU.getStringProperty ("explorer.favorite."+i+".path");
     * if(!"".equals(path)) { URL u=null; try { u=new URL(path); } catch(MalformedURLException ex) { } if(u!=null) {
     * roots_.addItem(VfsFile.createFile(u)); } else { String type=BuPreferences.BU.getStringProperty
     * ("explorer.favorite."+i+".type","directory"); Root f=new Root(path,path,type); if(f.exists()) roots_.addItem(f); } } }
     */
  }

  protected final void buildFavorites() {
    if (favMenu_ != null) {
      favMenu_.removeAll();
    }
  }

  @Override
  public void createDirectory() {
    final BuDialogInput d = new BuDialogInput(null, null, CtuluResource.CTULU.getString("Cr�er"), CtuluResource.CTULU
        .getString("Nom du sous-r�pertoire:"), "");
    if (d.activate() == JOptionPane.OK_OPTION) {
      final String name = d.getValue();

      final VfsFile current = getCurrentDirectory();

      if (current.createChild(name).mkdir()) {
        refresh();
      } else {
        new BuDialogError(null, null, "Impossible de cr�er le sous-r�pertoire " + name + "\n" + "dans le r�pertoire "
            + current.getAbsolutePath()).activate();
      }
    }
    d.dispose();
  }

  @Override
  public void deleteDirectory() {
    final VfsFile selected = getSelectedDirectory();
    if (selected == null) {
      return;
    }
    String mes = CtuluResource.CTULU
        .getString("Voulez-vous vraiment d�truire\nle r�pertoire {0} ?", selected.getName());
    final BuDialogConfirmation d = new BuDialogConfirmation(null, null, mes);

    if (d.activate() == JOptionPane.OK_OPTION) {

      if (!CtuluLibFile.deleteDir(selected)) {
        mes = CtuluResource.CTULU.getString("Le r�pertoire {0}\n n'a pas pu �tre d�truit", selected.getName());
        new BuDialogError(null, null, mes).activate();
      }

    }
    d.dispose();
  }
  
  
  @Override
  protected String getS(final String _s) {
    return FudaaLib.getS(_s);
  }

 /* public void deleteFiles() {
    VfsFile selected = getSelectedFile();
    if (selected == null) return;
    String mes = CtuluResource.CTULU.getString("Voulez-vous vraiment d�truire\n le fichier {0}?", selected.getName());
    BuDialogConfirmation d = new BuDialogConfirmation(null, null, mes);

    if (d.activate() == JOptionPane.OK_OPTION) {
      if (selected.delete()) refresh();
      else {
        mes = CtuluResource.CTULU.getString("Le fichier {0}\n n'a pas pu �tre d�truit.", selected.getName());
        new BuDialogError(null, null, mes).activate();
      }
    }
    d.dispose();
  }*/

  public BuMenu getFavoriteMenu() {
    if (favMenu_ == null) {
      favMenu_ = new FavoriteMenu();
      favMenu_.setIcon(null);
    }
    return favMenu_;
  }

  @Override
  public void renameDirectory() {
    final VfsFile selected = getSelectedDirectory();

    final BuDialogInput d = new BuDialogInput(null, null, CtuluResource.CTULU.getString("Renommer"), CtuluResource.CTULU
        .getString("Nouveau nom du r�pertoire:"), selected.getName());
    if (d.activate() == JOptionPane.OK_OPTION) {
      final String name = d.getValue();
      final VfsFile target = selected.getParentVfsFile().createChild(name);

      if (selected.renameTo(target)) {
        refresh();
      } else {
        new BuDialogError(null, null, CtuluResource.CTULU.getString("Impossible de renommer le r�pertoire") + CtuluLibString.ESPACE
            + selected.getName() + "\n" + CtuluResource.CTULU.getString("en") + CtuluLibString.ESPACE + name).activate();
      }
    }
  }

  @Override
  public void renameFile() {
    final VfsFile selected = getSelectedFile();

    final BuDialogInput d = new BuDialogInput(null, null, CtuluResource.CTULU.getString("Renommer"), CtuluResource.CTULU
        .getString("Nouveau nom du fichier:"), selected.getName());
    if (d.activate() == JOptionPane.OK_OPTION) {
      final String name = d.getValue();
      final VfsFile target = selected.getParentVfsFile().createChild(name);

      if (selected.renameTo(target)) {
        refresh();
      } else {
        new BuDialogError(null, null, CtuluResource.CTULU.getString("Impossible de renommer le fichier") + CtuluLibString.ESPACE
            + selected.getName() + "\n" + CtuluResource.CTULU.getString("en") + CtuluLibString.ESPACE + name).activate();
      }
    }
  }

  /**
   * Utiliser quand les favoris sont mis a jour.
   * 
   * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
   */
  @Override
  public void update(final Observable _o, final Object _arg) {
    buildFavorites();
  }

}
