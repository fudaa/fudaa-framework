/*
 * @file         FudaaDialogImport.java
 * @creation     1998-10-06
 * @modification $Date: 2007-01-19 13:14:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.impl;
import com.memoire.bu.BuResource;
import java.awt.Frame;
import java.util.Hashtable;
import javax.swing.JDialog;
/**
 * Une boite g�n�rique d'importation de fichiers.
 *
 * @version      $Revision: 1.1 $ $Date: 2007-01-19 13:14:06 $ by $Author: deniger $
 * @author       Axel von Arnim 
 */
public class FudaaDialogImport extends JDialog {
  // Donnees membres privees
  protected Hashtable imports_;
  // Constructeur
  public FudaaDialogImport(final Frame _parent) {
    super(_parent, BuResource.BU.getString("Importer"), true);
    imports_= new Hashtable();
  }
  public String getImport(final String _key) {
    return (String)imports_.get(_key);
  }
}
