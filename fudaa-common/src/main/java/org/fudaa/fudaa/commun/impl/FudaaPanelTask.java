/*
 * @creation 8 sept. 2005
 * 
 * @modification $Date: 2007-01-19 13:14:07 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuLib;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuProgressBar;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuVerticalLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import org.fudaa.ctulu.CtuluAnalyzeGroup;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluProgressionBarAdapter;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.CtuluTaskOperationAbstract;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluAnalyzeGUI;
import org.fudaa.ctulu.gui.CtuluDialog;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * Un panneau qui permet d'effectuer une tache sans fermer le dialogue et d'afficher des messages issus de
 * l'exportation.
 * 
 * @author Fred Deniger
 * @version $Id: FudaaPanelTask.java,v 1.1 2007-01-19 13:14:07 deniger Exp $
 */
public class FudaaPanelTask implements ActionListener {

  final JButton btStopThread_;
  final FudaaPanelTaskModel model_;
  final CtuluUI impl_;
  final String[] messages_;
  CtuluDialogPanel panel_;

  BuProgressBar bar_;

  /**
   * @param _ui l'implementation parent
   * @param _model le model
   */
  public FudaaPanelTask(final CtuluUI _ui, final FudaaPanelTaskModel _model) {
    super();
    impl_ = _ui;
    model_ = _model;
    final int nbLb = 2 * _model.getNbMessageMax();
    messages_ = new String[nbLb];
    bar_ = new BuProgressBar();
    lbInfo_ = new BuLabel[messages_.length];
    btStopThread_ = new JButton();
    btStopThread_.setActionCommand("STOP_THREAD");
    btStopThread_.setIcon(BuResource.BU.getToolIcon("arreter"));
    btStopThread_.setText(FudaaLib.getS("Annuler"));
    btStopThread_.setHorizontalAlignment(SwingConstants.CENTER);
    btStopThread_.setToolTipText(FudaaLib.getS("Annuler la t�che en cours"));
    btStopThread_.setEnabled(false);
    btStopThread_.addActionListener(this);
    panel_ = new CtuluDialogPanel() {

      @Override
      public boolean apply() {
        applyPanel();
        return true;
      }

      @Override
      public boolean isDataValid() {
        return validePanel();
      }

      @Override
      public boolean cancel() {
        cancelPanel();
        return true;
      }
    };
    panel_.setLayout(new BuVerticalLayout(2, true, true));
    panel_.add(_model.getPanel());
    panel_.add(bar_);
    panel_.add(btStopThread_);
    final BuPanel pnInfo = new BuPanel();
    pnInfo.setBorder(BorderFactory.createTitledBorder(FudaaLib.getS("R�sultats de la derni�re op�ration")));
    pnInfo.setLayout(new BuGridLayout(2, 3, 2));
    for (int i = 0; i < nbLb; i++) {
      lbInfo_[i] = new BuLabel();
      lbInfo_[i].setText(CtuluLibString.ESPACE);
      pnInfo.add(lbInfo_[i]);
    }
    panel_.add(pnInfo);
  }

  @Override
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == btStopThread_) {
      model_.stopTask();
    }
  }

  public void applyPanel() {
    btStopThread_.setEnabled(true);
    final CtuluTaskDelegate task = impl_.createTask(model_.getTitre());
    task.start(new Runnable() {

      @Override
      public void run() {

        final CtuluAnalyzeGroup an = new CtuluAnalyzeGroup(null);
        Arrays.fill(messages_, CtuluLibString.EMPTY_STRING);
        model_.actTask(CtuluTaskOperationAbstract.createComposite(task.getStateReceiver(),
            new CtuluProgressionBarAdapter(bar_)), an, messages_);
        final Runnable r = new Runnable() {

          @Override
          public void run() {
            CtuluAnalyzeGUI.showDialog(an, impl_, model_.getTitre());
//            impl_.manageAnalyzeAndIsFatal(an);
            btStopThread_.setEnabled(false);
            for (int i = 0; i < messages_.length; i++) {
              lbInfo_[i].setText(messages_[i]);
            }
          }
        };
        BuLib.invokeLater(r);

      }

    });

  }

  final BuLabel[] lbInfo_;

  public boolean validePanel() {
    final String err = model_.isTakDataValid();
    panel_.setErrorText(err);
    return err == null;
  }

  public void cancelPanel() {

  }

  CtuluDialog dial_;

  /**
   * Affiche le dialogue.
   */
  public void afficheDialog() {
    if (dial_ == null) {
      dial_ = new CtuluDialog(CtuluLibSwing.getFrameAncestorHelper(impl_.getParentComponent()), panel_) {

        @Override
        protected JButton construireApply() {
          final JButton b = super.construireApply();
          model_.decoreBtApply(b);
          return b;
        }

        @Override
        protected JButton construireCancel() {
          final JButton b = super.construireCancel();
          b.setText(BuResource.BU.getString("Fermer"));
          b.setIcon(BuResource.BU.getIcon("Fermer"));
          return b;
        }
      };
      dial_.setOption(CtuluDialog.APPLY_CANCEL_OPTION);
      dial_.setTitle(model_.getTitre());
    }
    dial_.afficheDialogModal();
    model_.dialogClosed();
  }

}
