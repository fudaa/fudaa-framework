/*
 *  @file         FudaaExecListModel.java
 *  @creation     10 mars 2004
 *  @modification $Date: 2006-09-19 15:01:54 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.exec;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.swing.AbstractListModel;


/**
 * Modele utilisant une arrayList et envoyent des evts a chaque modif.
 * @author Fred Deniger
 * @version $id$
 * 
 */
public class FudaaExecListModel extends AbstractListModel {
  ArrayList l_;

  /**
   * Constructeur par defaut.
   * @param _l la liste initiales
   */
  public FudaaExecListModel(final List _l) {
    l_=new ArrayList(_l);
  }
  
  /**
   * @param _l la liste qui contiendra toutes les donnees
   */
  public void fillList(final List _l){
    _l.addAll(l_);
  }
  
  /**
   * Tri les elements du model.
   */
  public void sort(){
    Collections.sort(l_);
    fireContentsChanged(this,0,getSize());
  }

  /**
   * @see javax.swing.ListModel#getSize()
   */
  @Override
  public int getSize() {
    return l_.size();
  }

  /**
   * @see javax.swing.ListModel#getElementAt(int)
   */
  @Override
  public Object getElementAt(final int _index) {
    return l_.get(_index);
  }
  /**
   * @param _index index d'insertion
   * @param _element index a ajouter
   */
  public void add(final int _index, final Object _element) {
    l_.add(_index, _element);
    fireIntervalAdded(this,_index,_index);
  }

  /**
   * @param _o objet a enlever
   * @return true si reussi
   */
  public boolean add(final Object _o) {
    final boolean b=l_.add(_o);
    if(b) {
      fireIntervalAdded(this,getSize(),getSize());
    }
    return b;
  }

  /**
   * @param _index index d'insertion
   * @param _c la collection a ajouter
   * @return true si reussie
   */
  public boolean addAll(final int _index, final Collection _c) {
    final boolean b=l_.addAll(_index, _c);
    if(b) {
      fireIntervalAdded(this,_index,_index+_c.size());
    }
    return b;
  }

  /**
   * @param _c la collection a ajouter
   * @return true si reussie
   */
  public boolean addAll(final Collection _c) {
    final int i=getSize();
    final boolean b=l_.addAll(_c);
    if(b) {
      fireIntervalAdded(this,i,i+_c.size());
    }
    return b;
  }

  public void clear() {
    final int n=getSize();
    l_.clear();
    fireIntervalRemoved(this,0,n);
  }

  /**
   * @param _o objt a tester
   * @return true si contenu
   */
  public boolean contains(final Object _o) {
    return l_.contains(_o);
  }

  /**
   * @param _c la collection a tester
   * @return true si action reussie
   */
  public boolean containsAll(final Collection _c) {
    return l_.containsAll(_c);
  }

  /**
   * @param _minCapacity la capacite min
   */
  public void ensureCapacity(final int _minCapacity) {
    l_.ensureCapacity(_minCapacity);
  }

  /**
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(final Object _obj) {
    return l_.equals(_obj);
  }
  
  

  @Override
  public int hashCode(){
    return l_.hashCode();
  }
  /**
   * @param _index l'index a rechercher
   * @return l'objet a l'index donne
   */
  public Object get(final int _index) {
    return l_.get(_index);
  }

  /**
   * @param _o l'objet recherche
   * @return l'index
   */
  public int indexOf(final Object _o) {
    return l_.indexOf(_o);
  }

  /**
   * @return true si vide
   */
  public boolean isEmpty() {
    return l_.isEmpty();
  }


  /**
   * @param _index a enlever
   * @return true si action reussie
   */
  public Object remove(final int _index) {
    final Object r=l_.remove(_index);
    if(r!=null) {
      fireIntervalRemoved(this,_index,_index);
    }
    return r;
  }

  /**
   * @param _o l'objet a enlever
   * @return true si enlev�
   */
  public boolean remove(final Object _o) {
    return remove(indexOf(_o))!=null;
  }
  
  /**
   * @param _o les objet a enlever
   * @return true si enlev�s
   */
  public boolean removeAll(final Object[] _o) {
    return removeAll(Arrays.asList(_o));
  }

  /**
   * @param _c les objets a enlever
   * @return true si enlev�s
   */
  public boolean removeAll(final Collection _c) {
    final int nb=getSize();
    final boolean r=l_.removeAll(_c);
    fireContentsChanged(this,0,nb);
    return r;
  }

  /**
   * @param _c la collection a retenir
   * @return true si action reussie
   */
  public boolean retainAll(final Collection _c) {
    final int nb=getSize();
    final boolean r=l_.retainAll(_c);
    fireContentsChanged(this,0,nb);
    return r;
  }

  /**
   * @param _index index du set
   * @param _element l'objet a mettre
   * @return l'ancien objet
   */
  public Object set(final int _index, final Object _element) {
    final Object r=l_.set(_index, _element);
    if(r!=null) {
      fireContentsChanged(this,_index,_index);
    }
    return r;
  }

  /**
   * @return taille
   */
  public int size() {
    return l_.size();
  }

  /**
   * @param _fromIndex l'index de deb
   * @param _toIndex l'index de fin
   * @return la sous liste
   */
  public List subList(final int _fromIndex, final int _toIndex) {
    return l_.subList(_fromIndex, _toIndex);
  }

  /**
   * @param _a le tableau de dest
   * @return le tableau final
   */
  public Object[] toArray(final Object[] _a) {
    return l_.toArray(_a);
  }

}
