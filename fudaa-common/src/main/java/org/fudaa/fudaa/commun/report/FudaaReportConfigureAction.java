/*
 * @creation 8 f�vr. 07
 * @modification $Date: 2007-03-19 13:27:20 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.fudaa.commun.report;

import com.memoire.dja.DjaGridEvent;
import com.memoire.dja.DjaVector;
import org.fudaa.ebli.commun.BPalettePanelInterface;
import org.fudaa.ebli.controle.BConfigurePalette;
import org.fudaa.ebli.controle.BConfigurePaletteAction;

/**
 * @author fred deniger
 * @version $Id: FudaaReportConfigureAction.java,v 1.3 2007-03-19 13:27:20 deniger Exp $
 */
public class FudaaReportConfigureAction extends BConfigurePaletteAction implements
    FudaaDjaFormListener.SelectionListener {

  public FudaaReportConfigureAction(FudaaReportFrameController _controller) {
    super(null);
    _controller.djaFormListener_.addSelectionListener(this);
  }

  @Override
  public void selectionChanged(DjaGridEvent _evt) {
    FudaaDjaGrid grid = (FudaaDjaGrid) _evt.getGrid();
    DjaVector vect = grid.getSelection();
    Object[] o = null;
    if (vect != null && vect.size() > 0) {
      o = new Object[vect.size()];
      vect.copyInto(o);
      setPaletteTarget(o);
    }
    setTarget(o);

  }

  @Override
  public BPalettePanelInterface buildPaletteContent() {
    return new BConfigurePalette(false,null,null);
  }

}
