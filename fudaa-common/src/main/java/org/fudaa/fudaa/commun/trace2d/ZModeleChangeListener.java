package org.fudaa.fudaa.commun.trace2d;
import java.util.EventListener;
public interface ZModeleChangeListener extends EventListener {
  public void modelChanged(ZModeleChangeEvent _evt);
}
