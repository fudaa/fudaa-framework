package org.fudaa.fudaa.commun.trace2d;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import javax.swing.Icon;
/**
 * Un symbole repr�sentable sous forme d'ic�ne au sens Swing du terme (Affichable
 * dans un bouton, etc.), avec une couleur de trac� et une taille.
 *
 * @version      $Revision: 1.3 $ $Date: 2006-09-19 15:10:23 $ by $Author: deniger $
 * @author       Bertrand Marchand
 * @see Symbole
 */
public final class SymboleIcon implements Icon {
  /** Le symbole */
  private Symbole s_;
  /** La couleur de trac�. <code>null</code> signifie sans trac�. */
  private Color fgc_= Color.black;
  /** Taille de l'icone en pixels. */
  private int taille_= 15;
  /**
   * Constructeur par d�faut.
   */
  public SymboleIcon(Symbole _s) {
    s_= _s;
  }
  /**
   * Constructeur avec couleur.
   *
   * @param _fgc Couleur de trac�.
   */
  public SymboleIcon(Symbole _s, Color _fgc) {
    s_= _s;
    fgc_= _fgc;
  }
  /**
   * Constructeur avec taille seulement.
   */
  public SymboleIcon(Symbole _s, int _taille) {
    s_= _s;
    taille_= _taille;
  }
  /**
   * Constructeur avec couleur et taille.
   *
   * @param _fgc Couleur de trac�.
   * @param _taille Hauteur de l'icone.
   */
  public SymboleIcon(Symbole _s, Color _fgc, int _taille) {
    this(_s, _fgc);
    taille_= _taille;
  }
  /**
   * Retourne le symbole associ�.
   */
  public Symbole getSymbole() {
    return s_;
  }
  /**
   * D�finition de la couleur de trac�.
   */
  public void setCouleur(Color _fgc) {
    fgc_= _fgc;
  }
  /**
   * Retourne la couleur de trac�.
   */
  public Color getCouleur() {
    return fgc_;
  }
  /**
   * D�finition de la taille de l'icone.
   */
  public void setTaille(int _taille) {
    taille_= _taille;
  }
  /**
   * Retourne la taille de l'icone.
   */
  public int getTaille() {
    return taille_;
  }
  // >>> Icon  -----------------------------------------------------------------
  @Override
  public int getIconHeight() {
    return taille_ + 1; // + 1 pour les probl�mes d'arrondis.
  }
  @Override
  public int getIconWidth() {
    return taille_ + 1; // + 1 pour les probl�mes d'arrondis.
  }
  @Override
  public void paintIcon(Component _c, Graphics _g, int _x, int _y) {
    AffineTransform at= new AffineTransform();
    at.setToTranslation(_x, _y);
    at.translate(taille_ / 2, taille_ / 2);
    at.rotate(s_.rotationZ * Math.PI / 180.);
    at.scale(taille_, taille_);
    at.translate(-.5, -.5);
    Shape sh= at.createTransformedShape(s_.trace);
    _g.setColor(fgc_);
    if (s_.plein)
       ((Graphics2D)_g).fill(sh);
    else
       ((Graphics2D)_g).draw(sh);
  }
  // <<< Icon  -----------------------------------------------------------------
}
