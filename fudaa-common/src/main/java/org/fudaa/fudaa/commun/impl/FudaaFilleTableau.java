/*
 * @creation     2006-01-30
 * @modification $Date: 2007-06-14 12:01:10 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.commun.impl;

import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuToolButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import org.fudaa.ctulu.CtuluExportDataInterface;
import org.fudaa.ctulu.CtuluUI;
import org.fudaa.ctulu.gui.CtuluTableExportPanel;
import org.fudaa.ebli.tableau.EbliFilleTableau;


/**
 * @author fred deniger
 * @version $Id: FudaaFilleTableau.java,v 1.2 2007-06-14 12:01:10 deniger Exp $
 */
public class FudaaFilleTableau extends EbliFilleTableau implements CtuluExportDataInterface {

  @Override
  public void startExport(final CtuluUI _impl) {
    CtuluTableExportPanel.doExport(getTable(), (FudaaCommonImplementation) _impl);
  }

  BuToolButton btExport_;

  @Override
  public JComponent[] getSpecificTools() {
    final JComponent[] r = new JComponent[utiliseFormules_ ? 3 : 1];
    int idx = 0;
    if (utiliseFormules_) {
      r[idx++] = btAjouter_;
      r[idx++] = btEnlever_;
    }
    if (btExport_ == null) {
      btExport_ = new BuToolButton();
      FudaaGuiLib.initExportDataButton(btExport_, new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent _e) {
          startExport(getApp());
        }

      });
      btExport_.setEnabled(true);
    }

    r[idx++] = btExport_;
    return r;
  }

  protected FudaaCommonImplementation getApp() {
    return (FudaaCommonImplementation) app_;
  }

  public FudaaFilleTableau(final FudaaCommonImplementation _app, final BuInformationsDocument _id) {
    super(_app, _id);
  }

  @Override
  public String[] getEnabledActions() {
    return new String[] { "IMPRIMER", "MISEENPAGE", "PREVISUALISER", "COPIER", "EXPORTER" };
  }

}
