/**
 *  @creation     10 juin 2003
 *  @modification $Date: 2007-06-05 09:01:16 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.commun.calcul;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuButtonLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import org.fudaa.dodico.calcul.CalculLauncher;
import org.fudaa.dodico.calcul.CalculListener;
import org.fudaa.ebli.commun.EbliActionInterface;
import org.fudaa.ebli.commun.EbliActionSimple;
import org.fudaa.fudaa.commun.FudaaLib;

/**
 * @author deniger
 * @version $Id: FudaaCalculAction.java,v 1.16 2007-06-05 09:01:16 deniger Exp $
 */
public class FudaaCalculAction implements CalculListener {
  transient EbliActionSimple actCalcul_;
  transient EbliActionSimple actStopCalcul_;
  transient FudaaCalculSupportInterface support_;
  transient CalculLauncher calculEnCours_;
  boolean onlyOneProcess_ = true;

  public FudaaCalculAction(final FudaaCalculSupportInterface _support, final boolean _stopCalcul) {
    support_ = _support;
    initCalcul();
    if (_stopCalcul) {
      initStopCalcul();
    }
  }

  private void initCalcul() {
    actCalcul_ = new EbliActionSimple(FudaaLib.getS("Calculer"), BuResource.BU.getIcon("calculer"), "COMPUTE") {
      @Override
      public void actionPerformed(final ActionEvent _com) {
        calculEnCours_ = support_.actionCalcul();
        if (calculEnCours_ == null) {
          return;
        }
        calculEnCours_.addListener(FudaaCalculAction.this);
        calculEnCours_.execute();
        if (onlyOneProcess_) {
          setEnableStopCalcul(true);
          setEnableCalcul(false);
        }
      }
    };
    actCalcul_.putValue(Action.SHORT_DESCRIPTION, FudaaLib.getS("Lancer le calcul"));
    actCalcul_.setEnabled(false);
  }

  public boolean calculIsRunning() {
    return (calculEnCours_ == null) || (calculEnCours_.isFinished());
  }

  private void initStopCalcul() {
    actStopCalcul_ = new EbliActionSimple(FudaaLib.getS("Arr�ter"), BuResource.BU.getIcon("arreter"), "STOP_COMPUTE") {
      @Override
      public void actionPerformed(final ActionEvent _com) {
        if (calculIsRunning()) {
          calculEnCours_.stop();
        }
      }
    };
    actStopCalcul_.setEnabled(false);
  }

  public void setEnableCalcul(final boolean _b) {
    if (actCalcul_ != null) {
      actCalcul_.setEnabled(_b);
    }
  }

  public void setEnableStopCalcul(final boolean _b) {
    if (actStopCalcul_ != null) {
      actStopCalcul_.setEnabled(_b);
    }
  }

  public EbliActionInterface getCalcul() {
    return actCalcul_;
  }

  public EbliActionInterface getStopCalcul() {
    return actStopCalcul_;
  }

  public boolean isStopCalculVisible() {
    return actStopCalcul_ != null;
  }

  public JPanel createPanel() {
    final BuPanel p = new BuPanel();
    p.setLayout(new BuButtonLayout(5, SwingConstants.RIGHT));
    if (actCalcul_ != null) {
      final BuButton bt = new BuButton();
      bt.setAction(actCalcul_);
      p.add(bt);
    }
    if (actStopCalcul_ != null) {
      final BuButton bt = new BuButton();
      bt.setAction(actStopCalcul_);
      p.add(bt);
    }
    return p;
  }

  /**
   * @param _source
   */
  @Override
  public void calculFinished(final CalculLauncher _source) {
    if (_source == calculEnCours_) {
      calculEnCours_ = null;
      if (onlyOneProcess_) {
        setEnableStopCalcul(false);
        setEnableCalcul(true);
      }
    }
  }

  public boolean isOnlyOneProcess() {
    return onlyOneProcess_;
  }

  public void setOnlyOneProcess(final boolean _onlyOneProcess) {
    onlyOneProcess_ = _onlyOneProcess;
  }
}
