/**
 * @creation     1999-01-20
 * @modification $Date: 2005-09-28 08:46:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.ressource;

import com.memoire.bu.BuResource;
import org.fudaa.dodico.commun.DodicoResource;
import org.fudaa.ebli.ressource.EbliResource;
/**
 * @version      $Revision: 1.7 $ $Date: 2005-09-28 08:46:06 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class FudaaResource extends DodicoResource {

  public final static FudaaResource FUDAA = new FudaaResource(EbliResource.EBLI);

  protected FudaaResource(BuResource _parent) {
    super(_parent);
  }
}
