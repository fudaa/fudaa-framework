package org.fudaa.ebli.graphe3D.renderer;

import java.awt.Color;

import org.fudaa.ebli.graphe3D.data.EG3dSurfaceModel.PlotColor;
import org.fudaa.ebli.graphe3D.data.EG3dSurfaceModel.PlotType;



/** 
 * implementation du surfaceColor pour 2 couleurs
 * 
 *
 */
public class EG3dSurfaceColorModelSet
{
	
	public static float RED_H=0.941896f;
	public static float RED_S=0.7517241f;
	public static float RED_B=0.5686275f;
	
	public static float GOLD_H=0.1f;
	public static float GOLD_S=0.9497207f;
	public static float GOLD_B=0.7019608f;
	
	
	protected EG3dSurfaceColorModel dualshade;
	protected EG3dSurfaceColorModel grayscale;
	protected EG3dSurfaceColorModel spectrum;
	protected EG3dSurfaceColorModel fog;
	protected EG3dSurfaceColorModel opaque;

	protected EG3dSurfaceColorModel alt_dualshade;
	protected EG3dSurfaceColorModel alt_grayscale;
	protected EG3dSurfaceColorModel alt_spectrum;
	protected EG3dSurfaceColorModel alt_fog;
	protected EG3dSurfaceColorModel alt_opaque;
	
	public EG3dSurfaceColorModelSet()
	{
				
		dualshade= new EG3dSurfaceColorModel(		EG3dSurfaceColorModel.DUALSHADE,	RED_H	,	RED_S	,	RED_B	,	0.4f	,	1f		);
		grayscale= new EG3dSurfaceColorModel(		EG3dSurfaceColorModel.DUALSHADE,	0f		,	0f		,	0f		,	0f		,	1f		);
		spectrum= new EG3dSurfaceColorModel(		EG3dSurfaceColorModel.SPECTRUM	,	0f		,	1f		,	1f		,	0f		,	.6666f	);
		fog= new EG3dSurfaceColorModel(			EG3dSurfaceColorModel.FOG		,	RED_H	,	RED_S	,	RED_B	,	0f		,	1f		);
		opaque= new EG3dSurfaceColorModel(			EG3dSurfaceColorModel.OPAQUE	,	RED_H	,	0.1f	,	1f		,	0f		,	0f		);

		
		
		alt_dualshade= new EG3dSurfaceColorModel(	EG3dSurfaceColorModel.DUALSHADE,	GOLD_H	,	GOLD_S	,	GOLD_B	,	0.4f	,	1f		);
		alt_grayscale= new EG3dSurfaceColorModel(	EG3dSurfaceColorModel.DUALSHADE,	0f		,	0f		,	0f		,	0f		,	1f		);
		alt_spectrum= new EG3dSurfaceColorModel(	EG3dSurfaceColorModel.SPECTRUM	,	0f		,	1f		,	0.8f	,	0f		,	.6666f	);
		alt_fog= new EG3dSurfaceColorModel(		EG3dSurfaceColorModel.FOG		,	GOLD_H	,	0f		,	GOLD_B	,	0f		,	1f		);
		alt_opaque= new EG3dSurfaceColorModel(		EG3dSurfaceColorModel.OPAQUE	,	GOLD_H	,	0.1f	,	1f		,	0f		,	0f		);
		
	}
	
	protected PlotColor color_mode= PlotColor.FOG;
	public void setPlotColor(PlotColor v)	
	{
		this.color_mode=v;
	}
	public PlotColor getPlotColor() {
	  return color_mode;
	}
	
	protected PlotType plot_mode= PlotType.SURFACE;
	public void setPlotType(PlotType type)
	{
		this.plot_mode=type;
	}
  public PlotType getPlotType() {
    return plot_mode;
  }
	
	
	public Color getLineColor(int curve, float z)
	{
		return Color.BLACK;
		
	}
	
	
	public Color getPolygonColor(int curve, float z)
	{
		if (curve==1) return getFirstPolygonColor(z);
		if (curve==2) return getSecondPolygonColor(z);
		return Color.blue;
	}
	
	
	public Color getFirstPolygonColor(float z)
	{
		
		if(plot_mode==PlotType.CONTOUR ||plot_mode==PlotType.DENSITY)
		{
			if (color_mode==PlotColor.OPAQUE)
				return dualshade.getPolygonColor(z);
		}
		
		switch ( color_mode)
		{
		case OPAQUE	:return opaque.getPolygonColor(z);
		case GRAYSCALE	:return grayscale.getPolygonColor(z);
		case SPECTRUM	:return spectrum.getPolygonColor(z);
		case DUALSHADE	:return dualshade.getPolygonColor(z);
		case FOG		:return fog.getPolygonColor(z);
		default: return Color.blue;
		}
	}
	
	
	public Color getSecondPolygonColor(float z)
	{
		
		if(plot_mode==PlotType.CONTOUR ||plot_mode==PlotType.DENSITY)
		{
			if (color_mode==PlotColor.OPAQUE)
				return alt_dualshade.getPolygonColor(z);
		}
		switch ( color_mode)
		{
		case OPAQUE	:return alt_opaque.getPolygonColor(z);
		case GRAYSCALE	:return alt_grayscale.getPolygonColor(z);
		case SPECTRUM	:return alt_spectrum.getPolygonColor(z);
		case DUALSHADE	:return alt_dualshade.getPolygonColor(z);
		case FOG		:return alt_fog.getPolygonColor(z);
		default: return Color.blue;
		}
	}
	
	
	
}
