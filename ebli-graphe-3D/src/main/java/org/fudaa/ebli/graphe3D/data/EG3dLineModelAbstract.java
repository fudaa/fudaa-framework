package org.fudaa.ebli.graphe3D.data;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 * Une classe abstraite qui implemente les fonctions principales, mais ne definit pas la structure
 * des donn�es.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 *
 */
public abstract class EG3dLineModelAbstract implements EG3dLineModel {
  PropertyChangeSupport support=new PropertyChangeSupport(this.getClass());
  boolean mustReset_=true;
  
  protected float xmin;
  protected float xmax;
  protected float ymin;
  protected float ymax;
  protected float zmin;
  protected float zmax;

  @Override
  public void addPropertyChangeListener(PropertyChangeListener listener) {
    support.addPropertyChangeListener(listener);
  }

  @Override
  public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
    support.addPropertyChangeListener(propertyName,listener);

  }

  @Override
  public void removePropertyChangeListener(PropertyChangeListener listener) {
    support.removePropertyChangeListener(listener);
  }

  @Override
  public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
    support.removePropertyChangeListener(propertyName,listener);
  }
  
  protected void firePropertyChange(String _propname, Object _oldval, Object _newval) {
    support.firePropertyChange(_propname, _oldval, _newval);
  }
  
  protected void resetLimits() {
    xmin=Float.POSITIVE_INFINITY;
    xmax=Float.NEGATIVE_INFINITY;
    ymin=Float.POSITIVE_INFINITY;
    ymax=Float.NEGATIVE_INFINITY;
    zmin=Float.POSITIVE_INFINITY;
    zmax=Float.NEGATIVE_INFINITY;
    
    for (int i=0; i<getNbPoints(); i++) {
      xmin=Math.min(xmin, getX(i));
      xmax=Math.max(xmax, getX(i));
      ymin=Math.min(ymin, getY(i));
      ymax=Math.max(ymax, getY(i));
      zmin=Math.min(zmin, getZ(i));
      zmax=Math.max(zmax, getZ(i));
    }
    mustReset_=false;
  }

  public void clearCache() {
    mustReset_=true;
  }
  
  @Override
  public float getXMin() {
    if (mustReset_)
      resetLimits();
    return xmin;
  }

  @Override
  public float getYMin() {
    if (mustReset_)
      resetLimits();
    return ymin;
  }

  @Override
  public float getZMin() {
    if (mustReset_)
      resetLimits();
    return zmin;
  }

  @Override
  public float getXMax() {
    if (mustReset_)
      resetLimits();
    return xmax;
  }

  @Override
  public float getYMax() {
    if (mustReset_)
      resetLimits();
    return ymax;
  }

  @Override
  public float getZMax() {
    if (mustReset_)
      resetLimits();
    return zmax;
  }

  @Override
  public boolean isDataAvailable() {
    return true;
  }
}
