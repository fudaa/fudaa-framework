
package org.fudaa.ebli.graphe3D.data;




 


/**
 * Un data model de surface.
 * @author Adrien Hadoux
 */
public interface EG3dSurfaceModel extends EG3dDataModel
{
  
  public enum PlotType{
    SURFACE("surfaceType"), 
    WIREFRAME("wireframeType"), 
    DENSITY("densityType"), 
    CONTOUR("contourType"),
    WIREFRAME_PT("wireframePTType");
    
    final String att;
    PlotType(String att){this.att = att;}
    public String getPropertyName() {return att;}
  
    };
	//TODO replace with enum
	//plot type constant 
	
	public enum PlotColor{
		OPAQUE("hiddenMode"), 
		SPECTRUM("spectrumMode"), 
		DUALSHADE("dualShadeMode"), 
		GRAYSCALE("grayScaleMode"), 
		FOG("fogMode");
		
		final String att;
		PlotColor(String att){this.att = att;}
		public String getPropertyName() {return att;}
	
		
			
	};
	//TODO replace with enums
	// plot color constant

  void addChangeListener(javax.swing.event.ChangeListener listener);

  void removeChangeListener(javax.swing.event.ChangeListener listener);

//  PlotType getPlotType();
	
	public EG3dVertex[][] getSurfaceVertex(); //maybe provide a less brutal parameter passing, but 
	//I have to ber careful, there is performance at stake
	
//	public PlotColor getPlotColor();
	public int getCalcDivisions();
	public int getContourLines();
	public int getDispDivisions(); 

//	/**
//	 * Determines whether to show x-y mesh.
//	 *
//	 * @return <code>true</code> if to show x-y mesh
//	 */
//	public boolean isMesh();
	/**
	 * Determines whether the first function is selected.
	 *
	 * @return <code>true</code> if the first function is checked, 
	 *         <code>false</code> otherwise
	 */
	public boolean isPlotFunction1();
	
	/**
	 * Determines whether the first function is selected.
	 *
	 * @return <code>true</code> if the first function is checked, 
	 *         <code>false</code> otherwise
	 */
	
	public boolean isPlotFunction2();
		
	
}

