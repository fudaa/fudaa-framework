package org.fudaa.ebli.graphe3D.data;

/**
 * 
 * @author Adrien Hadoux
 *
 */
public class EG3dSurfaceProfilsModel2 extends EG3dSurfaceModelAbstract {

	public static class EbliGraphDataStructure {
		
		public float xmin;
		public float xmax;
		public float ymin;
		public 	float ymax;
		public 	float zmin;
		public 	float zmax;
		
		public int size;
		public float[] xData;
		public float[][] yData;
		public float[] zData;
		
		public EbliGraphDataStructure(float[] xData, float[][] profilValue, float[] profilAbscisse, int nbProfilsValue) {
			this.xData = xData;
			this.yData = profilValue;
			this.zData =profilAbscisse;
			size = nbProfilsValue;
			computeExtremum();
		}

		private void computeExtremum() {
			ymin=yData[0][0];
			ymax=yData[0][0];
			for(float[] yd : yData) {
			
			for(int i=0;i<yd.length;i++) {
				if(yd[i]<ymin)
					ymin = yd[i];
				if(yd[i]>ymax)
					ymax = yd[i];
			}
			}
			xmin = xData[0];
			xmax = xData[0];
			for(int i=0;i<xData.length;i++) {
				if(xData[i]<xmin)
					xmin = xData[i];
				if(xData[i]>xmax)
					xmax = xData[i];				
			}
			
			zmin=zData[0];
			zmax=zData[0];
			for(int i=0;i<zData.length;i++) {
				if(zData[i]<zmin)
					zmin = zData[i];
				if(zData[i]>zmax)
					zmax = zData[i];
				
			}
			
		}
		
		
	}
	
	
	EG3dVertex[][] surfaceVertex;
	public EbliGraphDataStructure s;
	
	public void setValues(EbliGraphDataStructure structure) {
		s = structure;
		float xmin = structure.xmin;
		float xmax = structure.xmax;
		float ymin = structure.zmin;
		float ymax = structure.zmax;
		float zmin = structure.ymin;
		float zmax = structure.ymax;
		
		float[] prodondeur = structure.zData;
		
		int max = structure.xData.length;
		
				
		float[][] z1 =structure.yData;
		float[][] z2 = null;
		
		
		setValues(xmin, xmax, ymin, ymax, max, z1, z2, prodondeur, s.xData, zmin, zmax);
		
	}
	
	
	/** Creates two surfaces using data from the array.
	 * 
	 * @param xmin lower bound of x values
	 * @param xmax upper bound of x values
	 * @param ymin lower bound of y values
	 * @param ymax upper bound of y values
	 * @param size number of items in each dimensions (ie z1 = float[size][size] )
	 * @param z1 value matrix (null supported)
	 * @param z2 secondary function value matrix (null supported)
	 */
	public void setValues(float xmin, float xmax, float ymin, float ymax, int size, float[][] z1, float[][] z2, float[] profondeur, float[] xdata,float zmin, float zmax) {
		setDataAvailable(false); // clean space
		setXMin(xmin);
		setXMax(xmax);
		setYMin(ymin);
		setYMax(ymax);
		setCalcDivisions(size-1);
		
		setZMin(zmin);
		setZMax(zmax);
		z1Max = zmax;
		z1Min = zmin;
		z2Max = zmax;
		z2Min = zmin;
		
		
		final float stepx = (xMax - xMin) / calcDivisions;
		final float stepy = (yMax - yMin) / calcDivisions;
		final float xfactor = 20 / (xMax - xMin); // 20 aint magic: surface vertex requires a value in [-10 ; 10]
		final float yfactor = 20 / (yMax - yMin);
		
		final int total = (calcDivisions + 1) * (calcDivisions + 1); // compute total size
		surfaceVertex = new EG3dVertex[2][total];
		
		
		for (int i = 0; i <= calcDivisions; i++)
			for (int j = 0; j <= calcDivisions; j++) {
			//for (int j = 0; j < profondeur.length; j++) {
				int k = i * (calcDivisions + 1) + j;

				float xv = xMin + i * stepx;
				float yv = yMin + j * stepy;
				
				//-- adrien pour avoir du 2d, toujours mettre y =0; --//
				yv= profondeur[j];//yMin + profondeur[j] * stepy;
				xv = xdata[i];	
				
				    //-- Profil 1 --//
				    float v1 = z1!=null?z1[i][j]:Float.NaN;
					/*
				    if (Float.isInfinite(v1))
						v1 = Float.NaN;
					if (!Float.isNaN(v1)) {
						if (Float.isNaN(z1Max) || (v1 > z1Max))
							z1Max = v1;
						else if (Float.isNaN(z1Min) || (v1 < z1Min))
							z1Min = v1;
					}
				    */

					surfaceVertex[0][k] = new EG3dVertex((xv - xMin) * xfactor - 10, (yv - yMin) * yfactor - 10, v1);
					/*
					
				    //-- Profil 2 --//					
					float v2 = z2!=null?z2[i][j]:Float.NaN;
					if (Float.isInfinite(v2))
						v2 = Float.NaN;
					if (!Float.isNaN(v2)) {
						if (Float.isNaN(z2Max) || (v2 > z2Max))
							z2Max = v2;
						else if (Float.isNaN(z2Min) || (v2 < z2Min))
							z2Min = v2;
					}
					float yv2 = yv;
					//-- adrien pour avoir du 2d, toujours mettre y =0; --//
					//yv2= 5;
					surfaceVertex[1][k] = new EbliGraph3dVertex((xv - xMin) * xfactor - 10, (yv2 - yMin) * yfactor - 10, v2);
					
					*/
					
					
			}
		
		
		autoScale();
		setDataAvailable(true); 
		fireStateChanged();
	}


	@Override
  public EG3dVertex[][] getSurfaceVertex() {
		return surfaceVertex;
	}

}
