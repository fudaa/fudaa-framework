package org.fudaa.ebli.graphe3D.data;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.event.SwingPropertyChangeSupport;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;

public abstract class EG3dSurfaceModelAbstract implements EG3dSurfaceModel {
  public interface Plotter {
    public int getHeight();

    public int getWidth();

    public float getX(int i);

    public float getY(int j);

    public void setValue(int i, int j, float v1, float v2);
  }

  public static synchronized double ceil(double d, int digits) {
    if (d == 0) {
      return d;
    }
    long og = (long) Math.ceil((Math.log(Math.abs(d)) / Math.log(10)));
    double factor = Math.pow(10, digits - og);
    double res = Math.ceil((d * factor)) / factor;
    return res;
  }

  public static synchronized double floor(double d, int digits) {
    if (d == 0) {
      return d;
    }
    // computes order of magnitude
    long og = (long) Math.ceil((Math.log(Math.abs(d)) / Math.log(10)));

    double factor = Math.pow(10, digits - og);
    // the matissa
    double res = Math.floor((d * factor)) / factor;
    // res contains the closed power of ten
    return res;
  }

  private static final int INIT_CALC_DIV = 20;
  private static final int INIT_DISP_DIV = 20;
  protected boolean autoScaleZ = true;
  //	protected boolean boxed;
  protected int calcDivisions = INIT_CALC_DIV;
  //	protected ColorModelSet colorModel;
  protected int contourLines;
  protected boolean dataAvailable;
  protected int dispDivisions = INIT_DISP_DIV;

//	protected boolean displayGrids;

//	protected boolean displayXY;

//	protected boolean displayZ;

//	protected boolean expectDelay = false;
  protected boolean hasFunction1 = true;
  protected boolean hasFunction2 = true;
  protected EventListenerList listenerList = new EventListenerList();

//	protected boolean mesh;
//
//	protected PlotColor plotColor;
  protected boolean plotFunction1 = hasFunction1;
  protected boolean plotFunction2 = hasFunction2;

//	protected PlotType plotType = PlotType.SURFACE;

//	private EbliGraphProjection3dto2d projector;
  protected PropertyChangeSupport property;

//	protected boolean scaleBox;
  protected float xMax = 1f;
  protected float xMin;
  protected float yMax = 1f;
  protected float yMin;
  protected float z1Max;// the max computed
  protected float z1Min;// the min computed
  protected float z2Max;// the max computed
  protected float z2Min;// the min computed
  protected float zMax;
  protected float zMin;

  public EG3dSurfaceModelAbstract() {
    super();
    property = new SwingPropertyChangeSupport(this);
//		setColorModel(new ColorModelSet());

    setCalcDivisions(50);
    setDispDivisions(50);
    setContourLines(10);
/*
		setXMin(-3);
		setXMax(3);
		setYMin(-3);
		setYMax(3);
*/
//		setBoxed(true);
//		setDisplayXY(true);
//		setExpectDelay(false);
//		setAutoScaleZ(true);
//		setDisplayZ(true);
//		setMesh(true);
//		setPlotType(PlotType.SURFACE);
    setFirstFunctionOnly(true);
//		setPlotColor(PlotColor.FOG);
  }

  @Override
  public void addChangeListener(ChangeListener ol) {
    listenerList.add(ChangeListener.class, ol);
  }

  @Override
  public void addPropertyChangeListener(java.beans.PropertyChangeListener listener) {
    property.addPropertyChangeListener(listener);
  }

  @Override
  public void addPropertyChangeListener(String propertyName, java.beans.PropertyChangeListener listener) {
    property.addPropertyChangeListener(propertyName, listener);
  }

  public void autoScale() {
    // compute auto scale and repaint
    if (!autoScaleZ) {
      return;
    }
    if (plotFunction1 && plotFunction2) {
      setZMin(Math.min(z1Min, z2Min));
      setZMax(Math.max(z1Max, z2Max));
    } else {
      if (plotFunction1) {
        setZMin(z1Min);
        setZMax(z1Max);
      }
      if (plotFunction2) {
        setZMin(z2Min);
        setZMax(z2Max);
      }
    }
  }

  public void exportCSV(File file) throws IOException {
    EG3dVertex[][] surfaceVertex = getSurfaceVertex();
    if (file == null) {
      return;
    }
    try (java.io.FileWriter w = new java.io.FileWriter(file)) {
      float stepx, stepy, x, y;
      float xi, xx, yi, yx;
      int i, j, k;

      try {
        xi = getXMin();
        yi = getYMin();
        xx = getXMax();
        yx = getYMax();
        if ((xi >= xx) || (yi >= yx)) {
          throw new NumberFormatException();
        }
      } catch (NumberFormatException e) {
        setMessage("Error in ranges");
        return;
      }

      calcDivisions = getCalcDivisions();
      // func1calc = f1; func2calc = f2;

      stepx = (xx - xi) / calcDivisions;
      stepy = (yx - yi) / calcDivisions;

      if (surfaceVertex == null) {
        return;
      }
      i = 0;
      j = 0;
      k = 0;
      x = xi;
      y = yi;

      w.write("X\\Y->Z;");
      while (j <= calcDivisions) {

        w.write(Float.toString(y));
        if (j != calcDivisions) {
          w.write(';');
        }
        j++;
        y += stepy;
        k++;
      }
      w.write("\n");
      // first line written
      i = 0;
      j = 0;
      k = 0;
      x = xi;
      y = yi;

      while (i <= calcDivisions) {
        w.write(Float.toString(x));
        w.write(';');
        while (j <= calcDivisions) {
          w.write(Float.toString(surfaceVertex[0][k].z));
          if (j != calcDivisions) {
            w.write(';');
          }
          j++;
          y += stepy;
          k++;
        }
        w.write('\n');
        j = 0;
        y = yi;
        i++;
        x += stepx;
      }
      w.flush();
    }
  }

  private void fireAllFunction(boolean oldHas1, boolean oldHas2) {
    property.firePropertyChange("firstFunctionOnly", (!oldHas2) && oldHas1, (!plotFunction2) && plotFunction1);
    property.firePropertyChange("secondFunctionOnly", (!oldHas1) && oldHas2, (!plotFunction1) && plotFunction2);
    property.firePropertyChange("bothFunction", oldHas1 && oldHas2, plotFunction1 && plotFunction2);
    autoScale();
  }

  private void fireAllMode(PlotColor oldValue, PlotColor newValue) {
    for (PlotColor c : PlotColor.values())
      property.firePropertyChange(c.getPropertyName(), oldValue == c, newValue == c);
  }

  private void fireAllType(PlotType oldValue, PlotType newValue) {
    for (PlotType c : PlotType.values())
      property.firePropertyChange(c.getPropertyName(), oldValue == c, newValue == c);
  }

  protected void fireStateChanged() {

    Object[] listeners = listenerList.getListenerList();

    ChangeEvent e = null;
    for (int i = listeners.length - 2; i >= 0; i -= 2) {
      if (listeners[i] == ChangeListener.class) {

        if (e == null) {
          e = new ChangeEvent(this);
        }
        ((ChangeListener) listeners[i + 1]).stateChanged(e);
      }
    }
  }

  @Override
  public int getCalcDivisions() {
    return calcDivisions;
  }
//
//	public ColorModelSet getColorModel() {
//		return colorModel;
//	}

  @Override
  public int getContourLines() {
    return contourLines;
  }

  @Override
  public int getDispDivisions() {
    if (dispDivisions > calcDivisions) {
      dispDivisions = calcDivisions;
    }
    while ((calcDivisions % dispDivisions) != 0)
      dispDivisions++;
    return dispDivisions;
  }
//
//	public PlotColor getPlotColor() {
//		return plotColor;
//	}
//
//	public PlotType getPlotType() {
//		return plotType;
//	}
//
//	public EbliGraphProjection3dto2d getProjector() {
//		if (projector == null) {
//			projector = new EbliGraphProjection3dto2d();
//			projector.setDistance(70);
//			projector.set2DScaling(15);
//			projector.setRotationAngle(125);
//			projector.setElevationAngle(10);
//		}
//		return projector;
//	}

  public PropertyChangeSupport getPropertyChangeSupport() {
    if (property == null) {
      property = new SwingPropertyChangeSupport(this);
    }
    return property;
  }

  @Override
  public float getXMax() {
    return xMax;
  }

  @Override
  public float getXMin() {
    return xMin;
  }

  @Override
  public float getYMax() {
    return yMax;
  }

  @Override
  public float getYMin() {
    return yMin;
  }

  @Override
  public float getZMax() {
    return zMax;
  }

  @Override
  public float getZMin() {
    return zMin;
  }
//
//	public boolean isAutoScaleZ() {
//		return autoScaleZ;
//	}

  public boolean isBothFunction() {
    return plotFunction1 && plotFunction2;
  }
//
//	public boolean isBoxed() {
//		return boxed;
//	}
//
//	public boolean isContourType() {
//		return plotType == PlotType.CONTOUR;
//	}

  @Override
  public boolean isDataAvailable() {
    return dataAvailable;
  }
//
//	public boolean isDensityType() {
//		return plotType == PlotType.DENSITY;
//	}
//
//	public boolean isDisplayGrids() {
//		return displayGrids;
//	}
//
//	public boolean isDisplayXY() {
//		return displayXY;
//	}
//
//	public boolean isDisplayZ() {
//		return displayZ;
//	}

//	public boolean isDualShadeMode() {
//		return plotColor == PlotColor.DUALSHADE;
//	}
//
//	public boolean isExpectDelay() {
//		return expectDelay;
//	}

  public boolean isFirstFunctionOnly() {
    return plotFunction1 && !plotFunction2;
  }
//
//	public boolean isFogMode() {
//		return plotColor == PlotColor.FOG;
//	}
//
//	public boolean isGrayScaleMode() {
//		return plotColor == PlotColor.GRAYSCALE;
//	}
//
//	public boolean isHiddenMode() {
//		return plotColor == PlotColor.OPAQUE;
//	}

//	public boolean isMesh() {
//		return mesh;
//	}

  @Override
  public boolean isPlotFunction1() {
    return plotFunction1;
  }

  @Override
  public boolean isPlotFunction2() {
    return plotFunction2;
  }
//
//	public boolean isScaleBox() {
//		return scaleBox;
//	}

  public boolean isSecondFunctionOnly() {
    return (!plotFunction1) && plotFunction2;
  }

  //
//	public boolean isSpectrumMode() {
//		return plotColor == PlotColor.SPECTRUM;
//	}
//
//	public boolean isSurfaceType() {
//		return plotType == PlotType.SURFACE;
//	}
//
//	public boolean isWireframeType() {
//		return plotType == PlotType.WIREFRAME;
//	}
//	public boolean isWireframePTType() {
//		return plotType == PlotType.WIREFRAME_PT;
//	}
  @Override
  public void removeChangeListener(ChangeListener ol) {
    listenerList.remove(ChangeListener.class, ol);
  }

  @Override
  public void removePropertyChangeListener(java.beans.PropertyChangeListener listener) {
    property.removePropertyChangeListener(listener);
  }

  @Override
  public void removePropertyChangeListener(String propertyName, java.beans.PropertyChangeListener listener) {
    property.removePropertyChangeListener(propertyName, listener);
  }

  public void rotationStops() {

    // setting_panel.rotationStops();
  }
//
//	public void setAutoScaleZ(boolean autoScaleZ) {
//		getPropertyChangeSupport().firePropertyChange("this.autoScaleZ", this.autoScaleZ, this.autoScaleZ = autoScaleZ);
//		autoScale();
//	}

  public void setBothFunction(boolean val) {
    setPlotFunction12(val, val);
  }
//
//	public void setBoxed(boolean boxed) {
//		getPropertyChangeSupport().firePropertyChange("boxed", this.boxed, this.boxed = boxed);
//	}
//
//	protected void setColorModel(ColorModelSet colorModel) {
//		getPropertyChangeSupport().firePropertyChange("colorModel", this.colorModel, this.colorModel = colorModel);
//		if (colorModel != null) {
//			colorModel.setPlotColor(plotColor); // this shouls be handled by the model itself, without any
//			colorModel.setPlotType(plotType);
//		}
//	}

  public void setContourLines(int contourLines) {
    getPropertyChangeSupport().firePropertyChange("contourLines", this.contourLines, this.contourLines = contourLines);
  }
//
//	public void setContourType(boolean val) {
//		setPlotType(val ? PlotType.CONTOUR : PlotType.SURFACE);
//	}

  public void setDataAvailable(boolean dataAvailable) {
    getPropertyChangeSupport().firePropertyChange("dataAvailable", this.dataAvailable, this.dataAvailable = dataAvailable);
  }
//
//	public void setDensityType(boolean val) {
//		setPlotType(val ? PlotType.DENSITY : PlotType.SURFACE);
//	}

  public void setDispDivisions(int dispDivisions) {
    getPropertyChangeSupport().firePropertyChange("dispDivisions", this.dispDivisions, this.dispDivisions = dispDivisions);
  }
//
//	public void setDualShadeMode(boolean val) {
//		setPlotColor(val ? PlotColor.DUALSHADE : PlotColor.SPECTRUM);
//	}

  public void setFirstFunctionOnly(boolean val) {
    setPlotFunction12(val, !val);
  }
//
//	public void setFogMode(boolean val) {
//		setPlotColor(val ? PlotColor.FOG : PlotColor.SPECTRUM);
//	}
//
//	public void setGrayScaleMode(boolean val) {
//		setPlotColor(val ? PlotColor.GRAYSCALE : PlotColor.SPECTRUM);
//	}
//
//	public void setHiddenMode(boolean val) {
//		setPlotColor(val ? PlotColor.OPAQUE : PlotColor.SPECTRUM);
//	}

  public void setMessage(String text) {
    // @todo
    // System.out.println("Message"+text);
  }

  public void setPlotFunction1(boolean plotFunction1) {
    setPlotFunction12(plotFunction1, plotFunction2);
  }
//
//	public void setPlotColor(PlotColor plotColor) {
//		PlotColor old = this.plotColor;
//		getPropertyChangeSupport().firePropertyChange("plotColor", this.plotColor, this.plotColor = plotColor);
//		fireAllMode(old, this.plotColor);
//		if (colorModel != null)
//			colorModel.setPlotColor(plotColor); // this should be handled by the model itself, without any
//	}

  public void setPlotFunction12(boolean p1, boolean p2) {
    boolean o1 = this.plotFunction1;
    boolean o2 = this.plotFunction2;

    this.plotFunction1 = hasFunction1 && p1;
    property.firePropertyChange("plotFunction1", o1, p1);

    this.plotFunction2 = hasFunction2 && p2;
    property.firePropertyChange("plotFunction1", o2, p2);
    fireAllFunction(o1, o2);
  }

  public void setPlotFunction2(boolean v) {
    setPlotFunction12(plotFunction1, plotFunction2);
  }
//
//	public void setPlotType(PlotType plotType) {
//		PlotType o = this.plotType;
//		this.plotType = plotType;
////		if (colorModel != null)
////			colorModel.setPlotType(plotType); // this should be handled by the model itself, without any
//		property.firePropertyChange("plotType", o, this.plotType);
//		fireAllType(o, this.plotType);
//	}

  public void setSecondFunctionOnly(boolean val) {
    setPlotFunction12(!val, val);
  }

  //
//	public void setSpectrumMode(boolean val) {
//		setPlotColor(val ? PlotColor.SPECTRUM : PlotColor.GRAYSCALE);
//	}
//
//	public void setSurfaceType(boolean val) {
//		setPlotType(val ? PlotType.SURFACE : PlotType.WIREFRAME);
//	}
//
//	public void setWireframeType(boolean val) {
//		if (val)
//			setPlotType(PlotType.WIREFRAME);
//		else
//			setPlotType(PlotType.SURFACE);
//	}
//	
//	public void setWireframePTType(boolean val) {
//		if (val)
//			setPlotType(PlotType.WIREFRAME_PT);
//		else
//			setPlotType(PlotType.SURFACE);
//	}
  public void setXMax(float xMax) {
    getPropertyChangeSupport().firePropertyChange("xMax", this.xMax, this.xMax = xMax);
  }

  public void setXMin(float xMin) {
    getPropertyChangeSupport().firePropertyChange("xMin", this.xMin, this.xMin = xMin);
  }

  public void setYMax(float yMax) {
    getPropertyChangeSupport().firePropertyChange("yMax", this.yMax, this.yMax = yMax);
  }

  public void setYMin(float yMin) {
    getPropertyChangeSupport().firePropertyChange("yMin", this.yMin, this.yMin = yMin);
  }

  public void setZMax(float zMax) {
    if (zMax <= zMin) {
      return;
    }
    getPropertyChangeSupport().firePropertyChange("zMax", this.zMax, this.zMax = zMax);
  }

  public void setZMin(float zMin) {
    if (zMin >= zMax) {
      return;
    }
    getPropertyChangeSupport().firePropertyChange("zMin", this.zMin, this.zMin = zMin);
  }
//
//	public void setDisplayGrids(boolean displayGrids) {
//		getPropertyChangeSupport().firePropertyChange("displayGrids", this.displayGrids, this.displayGrids = displayGrids);
//	}
//
//	public void setDisplayXY(boolean displayXY) {
//		getPropertyChangeSupport().firePropertyChange("displayXY", this.displayXY, this.displayXY = displayXY);
//	}
//
//	public void setDisplayZ(boolean displayZ) {
//		getPropertyChangeSupport().firePropertyChange("displayZ", this.displayZ, this.displayZ = displayZ);
//	}
//
//	public void setExpectDelay(boolean expectDelay) {
//		getPropertyChangeSupport().firePropertyChange("expectDelay", this.expectDelay, this.expectDelay = expectDelay);
//	}
//
//	public void setMesh(boolean mesh) {
//		getPropertyChangeSupport().firePropertyChange("mesh", this.mesh, this.mesh = mesh);
//	}
//
//	public void setScaleBox(boolean scaleBox) {
//		getPropertyChangeSupport().firePropertyChange("scaleBox", this.scaleBox, this.scaleBox = scaleBox);
//	}
//
//	public void toggleAutoScaleZ() {
//		setAutoScaleZ(!isAutoScaleZ());
//	}
//
//	public void toggleBoxed() {
//		setBoxed(!isBoxed());
//	}

  public void setCalcDivisions(int calcDivisions) {
    getPropertyChangeSupport().firePropertyChange("calcDivisions", this.calcDivisions, this.calcDivisions = calcDivisions);
  }

//
//	public void toggleDisplayGrids() {
//		setDisplayGrids(!isDisplayGrids());
//	}
//
//	
//	public void toggleDisplayXY() {
//		setDisplayXY(!isDisplayXY());
//	}
//
//	public void toggleDisplayZ() {
//		setDisplayZ(!isDisplayZ());
//	}
//
//	public void toggleExpectDelay() {
//		setExpectDelay(!isExpectDelay());
//	}
//
//	public void toggleMesh() {
//		setMesh(!isMesh());
//	}

  public void togglePlotFunction1() {
    setPlotFunction1(!isPlotFunction1());
  }

  public void togglePlotFunction2() {
    setPlotFunction2(!isPlotFunction2());
  }
//
//	public void toggleScaleBox() {
//		setScaleBox(!isScaleBox());
//	}
}
