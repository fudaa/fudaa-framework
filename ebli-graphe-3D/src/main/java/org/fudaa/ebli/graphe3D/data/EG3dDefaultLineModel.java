package org.fudaa.ebli.graphe3D.data;

import java.util.List;

/**
 * Une classe model de ligne par defaut. Cette classe s'appuie sur une liste de points.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 *
 */
public class EG3dDefaultLineModel extends EG3dLineModelAbstract {
  List<EG3dVertex> pts;
  
  public EG3dDefaultLineModel(List<EG3dVertex> _pts) {
    setPoints(_pts);
  }
  
  public void setPoints(List<EG3dVertex> _pts) {
    firePropertyChange("points", pts, pts=_pts);
    clearCache();
  }

  @Override
  public int getNbPoints() {
    return pts.size();
  }

  @Override
  public float getX(int _idx) {
    return pts.get(_idx).x;
  }

  @Override
  public float getY(int _idx) {
    return pts.get(_idx).y;
  }

  @Override
  public float getZ(int _idx) {
    return pts.get(_idx).z;
  }
}
