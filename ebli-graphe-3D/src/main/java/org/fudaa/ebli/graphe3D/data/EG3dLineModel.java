package org.fudaa.ebli.graphe3D.data;


/**
 * Une interface a implementer pour un modele de courbe.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public interface EG3dLineModel extends EG3dDataModel {
  
  int getNbPoints();
  float getX(int _idx);
  float getY(int _idx);
  float getZ(int _idx);
}
