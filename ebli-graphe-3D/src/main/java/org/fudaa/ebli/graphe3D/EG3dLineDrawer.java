
package org.fudaa.ebli.graphe3D;

import java.awt.Graphics;
import java.util.LinkedList;
import java.util.List;

 
public class EG3dLineDrawer {
  private List<Record> drawAccumulator; 
  

   
  EG3dLineDrawer() {
    drawAccumulator = new LinkedList<Record>();
  } 
  
  
   
  public void addLine(int x1, int y1, int x2, int y2) {
		if (x1<=0 || y1<=0 || x2<=0 || y2<=0 ) return; 
		
    drawAccumulator.add(new Record(x1,y1,x2,y2));
  }
  
		

   
  public void clearAccumulator() {
    drawAccumulator.clear();
  } 
  
 
   
  public void drawAll(Graphics g) {
	    for(Record line: drawAccumulator)
	    	g.drawLine(line.x1,line.y1,line.x2,line.y2);
  }
}


 
class Record {
  
  public final int x1;

  
  public final int y1;

 
  public final int x2;

  public final int y2;
    
   
  Record(int x1, int y1, int x2, int y2) {
    super();
    this.x1 = x1; this.y1 = y1;
    this.x2 = x2; this.y2 = y2;
  }
}

