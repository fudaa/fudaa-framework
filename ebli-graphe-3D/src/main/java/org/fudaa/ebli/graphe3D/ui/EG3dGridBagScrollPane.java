package org.fudaa.ebli.graphe3D.ui;

import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JScrollPane;


/** 
 * 
 * @author Adrien Hadoux
 *
 */
public class EG3dGridBagScrollPane extends JScrollPane{

	private boolean widthFixed =false;
	private boolean heightFixed = false;

	
	
	
	public EG3dGridBagScrollPane() {
		super();
	}




	public EG3dGridBagScrollPane(Component view, int vsbPolicy, int hsbPolicy) {
		super(view, vsbPolicy, hsbPolicy);
	}




	public EG3dGridBagScrollPane(Component view) {
		super(view);
	}




	public EG3dGridBagScrollPane(int vsbPolicy, int hsbPolicy) {
		super(vsbPolicy, hsbPolicy);
	}




	@Override
	public Dimension getMinimumSize() {
			Dimension min = super.getMinimumSize();
			int w = min.width;
			int h = min.height;
			if (widthFixed){
				w = getPreferredSize().width; // the content size
				w+= getVerticalScrollBar().getWidth();
			}
			if (heightFixed){
				h = getPreferredSize().height; // the content size
				h+= getHorizontalScrollBar().getHeight();
			}
			
			return new Dimension(w, h);
	}


	@Override
	public void layout() {
		boolean vertical = getVerticalScrollBar().isVisible();
		super.layout();
		if (vertical != getVerticalScrollBar().isVisible() ){
			getParent().invalidate() ;
			getParent().validate();
		}
	}




	public boolean isWidthFixed() {
		return widthFixed;
	}



	/** 
	 * 
	 * @param widthFixed true to force the minimum width to preferred's width.
	 */
	public void setWidthFixed(boolean widthFixed) {
		firePropertyChange("widthFixed", this.widthFixed , this.widthFixed = widthFixed);
		invalidate();
		if (getParent()!=null) 
			getParent().validate();
	}




	public boolean isHeightFixed() {
		return heightFixed;
	}



	/**
	 * 
	 * @param heightFixed true to force the minimum height to preferred's height.
	 */
	public void setHeightFixed(boolean heightFixed) {
		firePropertyChange("heightFixed", this.heightFixed , this.heightFixed = heightFixed);
		if (getParent()!=null) 
			getParent().validate();
	}

	
	
	
}
