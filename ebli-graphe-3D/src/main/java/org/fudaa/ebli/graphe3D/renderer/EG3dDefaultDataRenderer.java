package org.fudaa.ebli.graphe3D.renderer;

import java.awt.Color;
import java.beans.PropertyChangeSupport;

import javax.swing.event.SwingPropertyChangeSupport;

import org.fudaa.ebli.graphe3D.data.EG3dSurfaceModel.PlotColor;
import org.fudaa.ebli.graphe3D.data.EG3dSurfaceModel.PlotType;
import org.fudaa.ebli.trace.TraceIconModel;
import org.fudaa.ebli.trace.TraceLigneModel;

/**
 * Un renderer avec un rendu homogene sur la totalit� de la ligne.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public class EG3dDefaultDataRenderer implements EG3dDataRenderer {
  protected TraceLigneModel lm;
  protected TraceIconModel im;
  protected EG3dSurfaceColorModelSet surfColorModel_;
  protected PropertyChangeSupport property;
  
  public EG3dDefaultDataRenderer() {
    this(new TraceLigneModel(),new TraceIconModel());
  }
  
  public EG3dDefaultDataRenderer(TraceLigneModel _lineModel, TraceIconModel _markModel) {
    lm=_lineModel;
    im=_markModel;
    surfColorModel_=new EG3dSurfaceColorModelSet();
  }
  
  @Override
  public int getLineStyle(int _idx) {
    return lm.getTypeTrait();
  }
  
  public void setLineStyle(int _idx, int _style) {
    Object oldValue=lm.getTypeTrait();
    lm.setTypeTrait(_style);
    getPropertyChangeSupport().firePropertyChange("lineStyle", oldValue, _style);
  }

  @Override
  public Color getLineColor(int _idx) {
    return lm.getCouleur();
  }
  
  public void setLineColor(int _idx, Color _color) {
    Object oldValue=lm.getCouleur();
    lm.setCouleur(_color);
    getPropertyChangeSupport().firePropertyChange("lineColor", oldValue, _color);
  }

  @Override
  public float getLineThickness(int _idx) {
    return lm.getEpaisseur();
  }
  
  public void setLineThickness(int _idx, float _thickness) {
    Object oldValue=lm.getEpaisseur();
    lm.setEpaisseur(_thickness);
    getPropertyChangeSupport().firePropertyChange("lineThickness", oldValue, _thickness);
  }

  @Override
  public int getMarkType(int _idx) {
    return im.getType();
  }

  public void setMarkType(int _idx, int _type) {
    Object oldValue=im.getType();
    im.setType(_type);
    getPropertyChangeSupport().firePropertyChange("markType", oldValue, _type);
  }

  @Override
  public Color getMarkColor(int _idx) {
    return im.getCouleur();
  }
  
  public void setMarkColor(int _idx, Color _color) {
    Object oldValue=im.getCouleur();
    im.setCouleur(_color);
    getPropertyChangeSupport().firePropertyChange("markType", oldValue, _color);
  }

  @Override
  public int getMarkSize(int _idx) {
    return im.getTaille();
  }

  public void setMarkSize(int _idx, int _size) {
    Object oldValue=im.getTaille();
    im.setTaille(_size);
    getPropertyChangeSupport().firePropertyChange("markSize", oldValue, _size);
  }
  
  public void setPlotColor(PlotColor _color) {
    Object oldValue=surfColorModel_.getPlotColor();
    surfColorModel_.setPlotColor(_color);
    getPropertyChangeSupport().firePropertyChange("plotColor", oldValue, _color);
  }

  public void setPlotType(PlotType _type) {
    Object oldValue=surfColorModel_.getPlotType();
    surfColorModel_.setPlotType(_type);
    getPropertyChangeSupport().firePropertyChange("plotType", oldValue, _type);
  }
  
  @Override
  public Color getLineColor(int curve, float z) {
    return surfColorModel_.getLineColor(curve, z);
  }

  @Override
  public Color getPolygonColor(int curve, float z) {
    return surfColorModel_.getPolygonColor(curve, z);
  }

  @Override
  public Color getFirstPolygonColor(float z) {
    return surfColorModel_.getFirstPolygonColor(z);
  }

  @Override
  public Color getSecondPolygonColor(float z) {
    return surfColorModel_.getSecondPolygonColor(z);
  }

  @Override
  public PlotType getPlotType() {
    return surfColorModel_.getPlotType();
  }

  @Override
  public PlotColor getPlotColor() {
    return surfColorModel_.getPlotColor();
  }
  
  @Override
  public PropertyChangeSupport getPropertyChangeSupport() {
    if (property == null)
      property = new SwingPropertyChangeSupport(this);
    return property;
  }

}
