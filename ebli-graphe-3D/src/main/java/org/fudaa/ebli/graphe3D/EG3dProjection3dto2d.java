/*----------------------------------------------------------------------------------------*
 * Projector.java version 1.7                                                  Nov  8 1996 *
 * Projector.java version 1.71                                                May 14 1997 *
 *                                                                                        *
 * Copyright (c) Yanto Suryono <yanto@fedu.uec.ac.jp>                                     *
 *                                                                                        *
 * This program is free software; you can redistribute it and/or modify it                *
 * under the terms of the GNU Lesser General Public License as published by the                  *
 * Free Software Foundation; either version 2 of the License, or (at your option)         *
 * any later version.                                                                     *
 *                                                                                        *
 * This program is distributed in the hope that it will be useful, but                    *
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or          *
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for               *
 * more details.                                                                          *
 *                                                                                        *
 * You should have received a copy of the GNU Lesser General Public License along                *
 * with this program; if not, write to the Free Software Foundation, Inc.,                *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA                                  *
 *                                                                                        *
 *----------------------------------------------------------------------------------------*/
package org.fudaa.ebli.graphe3D;

import java.awt.Point;
import java.awt.Rectangle;



public final class EG3dProjection3dto2d {
	private float scale_x, scale_y, scale_z;       
	private float distance;                        
	private float _2D_scale;                       
	private float rotation, elevation;             
	private float sin_rotation, cos_rotation;      
	private float sin_elevation, cos_elevation;    
	private int   _2D_trans_x, _2D_trans_y;       
	private int   x1, x2, y1, y2;                  
	private int   center_x, center_y;                      

	private int   trans_x, trans_y;
	private float factor;
	private float sx_cos, sy_cos, sz_cos;
	private float sx_sin, sy_sin, sz_sin;

	private final float DEGTORAD  = (float)Math.PI / 180;

	public float zmin, zmax;
	public float zfactor;


	/**
	 * The constructor of <code>Projector</code>.
	 */

	public EG3dProjection3dto2d() {
		setScaling(1); 
		setRotationAngle(0); 
		setElevationAngle(0); 
		setDistance(10);
		set2DScaling(1);
		set2DTranslation(0,0);
	}


    //--aha projection --//
	public void setProjectionArea(Rectangle r) {
		x1 = r.x; x2 = x1 + r.width;
		y1 = r.y; y2 = y1 + r.height;
		center_x = (x1 + x2) / 2;
		center_y = (y1 + y2) / 2;        

		trans_x = center_x + _2D_trans_x;
		trans_y = center_y + _2D_trans_y;
	}

  /**
   * Rotation suivant Z.
   * @param angle En degr�s
   */
	public void setRotationAngle(float angle) {
		rotation = angle;
		sin_rotation = (float)Math.sin(angle * DEGTORAD);
		cos_rotation = (float)Math.cos(angle * DEGTORAD);

		sx_cos = -scale_x * cos_rotation;
		sx_sin = -scale_x * sin_rotation;
		sy_cos = -scale_y * cos_rotation;
		sy_sin =  scale_y * sin_rotation;
	}


	public float getRotationAngle() {
		return rotation;
	}


	public float getSinRotationAngle() {
		return sin_rotation;
	}


	public float getCosRotationAngle() {
		return cos_rotation;
	}


  /**
   * Rotation suivant Y.
   * @param angle En degr�s
   */
	public void setElevationAngle(float angle) {
		elevation = angle;
		sin_elevation = (float)Math.sin(angle * DEGTORAD);
		cos_elevation = (float)Math.cos(angle * DEGTORAD);
		sz_cos =  scale_z * cos_elevation;
		sz_sin =  scale_z * sin_elevation;
	}

	public float getElevationAngle() {
		return elevation;
	}


	public float getSinElevationAngle() {
		return sin_elevation;
	}



	public float getCosElevationAngle() {
		return cos_elevation;
	}

	/**
	 * Distance de l'oeil � l'objet.
	 * @param new_distance Pour une perspective cavali�re : Donner une grande distance.
	 */
	public void setDistance(float new_distance) {
		distance = new_distance;
		factor = distance * _2D_scale;
	}



	public float getDistance() {
		return distance;
	}



	public void setXScaling(float scaling) {
		scale_x = scaling;
		sx_cos = -scale_x * cos_rotation;
		sx_sin = -scale_x * sin_rotation;
	}



	public float getXScaling() {
		return scale_x;
	}



	public void setYScaling(float scaling) {
		scale_y = scaling;
		sy_cos = -scale_y * cos_rotation;
		sy_sin =  scale_y * sin_rotation;
	}



	public float getYScaling() {
		return scale_y;
	}


	public void setZScaling(float scaling) {
		scale_z = scaling;
		sz_cos =  scale_z * cos_elevation;
		sz_sin =  scale_z * sin_elevation;
	}



	public float getZScaling() {
		return scale_z;
	}



	public void setScaling(float x, float y, float z) {
		scale_x = x; scale_y = y; scale_z = z;

		sx_cos = -scale_x * cos_rotation;
		sx_sin = -scale_x * sin_rotation;
		sy_cos = -scale_y * cos_rotation;
		sy_sin =  scale_y * sin_rotation;
		sz_cos =  scale_z * cos_elevation;
		sz_sin =  scale_z * sin_elevation;
	}



	public void setScaling(float scaling) {
		scale_x = scale_y = scale_z = scaling;

		sx_cos = -scale_x * cos_rotation;
		sx_sin = -scale_x * sin_rotation;
		sy_cos = -scale_y * cos_rotation;
		sy_sin =  scale_y * sin_rotation;
		sz_cos =  scale_z * cos_elevation;
		sz_sin =  scale_z * sin_elevation;
	}



	public void set2DScaling(float scaling) {
		_2D_scale = scaling;
		factor = distance * _2D_scale;
	}



	public float get2DScaling() {
		return _2D_scale;
	}



	public void set2DTranslation(int x, int y) {
		_2D_trans_x = x; _2D_trans_y = y;

		trans_x = center_x + _2D_trans_x;
		trans_y = center_y + _2D_trans_y;
	}



	public void set2D_xTranslation(int x) {
		_2D_trans_x = x;
		trans_x = center_x + _2D_trans_x;
	}


	public int get2D_xTranslation() {
		return _2D_trans_x;
	}



	public void set2D_yTranslation(int y) {
		_2D_trans_y = y;
		trans_y = center_y + _2D_trans_y;
	}



	public int get2D_yTranslation() {
		return _2D_trans_y;
	}



	public final Point project(float x, float y, float z) {
		float temp;

		temp = x; 
		x = x * sx_cos + y * sy_sin;
		y = temp * sx_sin + y * sy_cos;

		temp = factor / (y * cos_elevation - z * sz_sin + distance);
		return new Point((int)(Math.round(x * temp) + trans_x),
				(int)(Math.round((y * sin_elevation + z * sz_cos) * -temp) + trans_y));
	}



	public  void setZRange(float zmin, float zmax) {
		this.zmin = zmin; this.zmax = zmax;
		this.zfactor = 20/(zmax-zmin);
	}

}
