package org.fudaa.ebli.graphe3D.data;


/**
 * Un data modele 3d dont herite tous les autres modeles
 * @author Bertrand Marchand (marchand@deltacad.fr)
 *
 */
public interface EG3dDataModel {

  //events
  void addPropertyChangeListener(java.beans.PropertyChangeListener listener);

  void addPropertyChangeListener(String propertyName, java.beans.PropertyChangeListener listener);

  void removePropertyChangeListener(java.beans.PropertyChangeListener listener);

  void removePropertyChangeListener(String propertyName, java.beans.PropertyChangeListener listener);

  float getXMin();

  float getYMin();

  float getZMin();

  float getXMax();

  float getYMax();

  float getZMax();

  /**
   * Sets data availability flag
   */
  boolean isDataAvailable();

}