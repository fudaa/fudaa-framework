package org.fudaa.ebli.graphe3D.renderer;

import org.fudaa.ebli.graphe3D.EG3dProjection3dto2d;

/**
 * Une interface rendu pour la vue du graphe 3d.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 */
public interface EG3dViewRenderer {

  EG3dProjection3dto2d getProjector(); //project is kind of "point of view"

  boolean isAutoScaleZ();

  EG3dViewColorSet getColorModel(); // not the right place, but JSurface does not work with any colorset, should be removed lately

  /**
   * Determines whether the delay regeneration checkbox is checked.
   *
   * @return <code>true</code> if the checkbox is checked, 
   *         <code>false</code> otherwise
   */
  boolean isExpectDelay();

  /**
   * Determines whether to show bounding box.
   *
   * @return <code>true</code> if to show bounding box
   */
  boolean isBoxed();

  /**
   * Determines whether to scale axes and bounding box.
   *
   * @return <code>true</code> if to scale bounding box
   */

  boolean isScaleBox();

  /**
   * Determines whether to show x-y ticks.
   *
   * @return <code>true</code> if to show x-y ticks
   */
  boolean isDisplayXY();

  /**
   * Determines whether to show z ticks.
   *
   * @return <code>true</code> if to show z ticks
   */
  boolean isDisplayZ();

  /**
   * Determines whether to show face grids.
   *
   * @return <code>true</code> if to show face grids
   */
  boolean isDisplayGrids();

}