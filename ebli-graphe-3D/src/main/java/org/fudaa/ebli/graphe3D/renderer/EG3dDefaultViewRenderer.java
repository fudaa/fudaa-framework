package org.fudaa.ebli.graphe3D.renderer;

import java.beans.PropertyChangeSupport;

import javax.swing.event.SwingPropertyChangeSupport;

import org.fudaa.ebli.graphe3D.EG3dProjection3dto2d;

/**
 * Une implementation par defaut du renderer de graph 3D.
 * @author Bertrand Marchand (marchand@deltacad.fr)
 *
 */
public class EG3dDefaultViewRenderer implements EG3dViewRenderer {
  protected EG3dProjection3dto2d projector;
  protected boolean autoScaleZ=true;
  protected EG3dViewColorSet colorModel;
  protected PropertyChangeSupport property;
  protected boolean expectDelay;
  protected boolean boxed=true;
  protected boolean scaleBox;
  protected boolean displayXY=true;
  protected boolean displayZ=true;
  protected boolean displayGrids=false;
  
  @Override
  public EG3dProjection3dto2d getProjector() {
    if (projector == null) {
      projector = new EG3dProjection3dto2d();
      projector.setDistance(70);
      projector.set2DScaling(15);
      projector.setRotationAngle(125);
      projector.setElevationAngle(10);
    }
    return projector;
  }

  @Override
  public boolean isAutoScaleZ() {
    return autoScaleZ;
  }
  

  public void setAutoScaleZ(boolean autoScaleZ) {
    getPropertyChangeSupport().firePropertyChange("this.autoScaleZ", this.autoScaleZ, this.autoScaleZ = autoScaleZ);
//    autoScale();
  }

  @Override
  public EG3dViewColorSet getColorModel() {
    if (colorModel==null) {
      colorModel=new EG3dDefaultViewColorSet();
    }
    return colorModel;
  }

  protected void setColorModel(EG3dViewColorSet colorModel) {
    getPropertyChangeSupport().firePropertyChange("colorModel", this.colorModel, this.colorModel = colorModel);
//    if (colorModel != null) {
//      colorModel.setPlotColor(plotColor); // this shouls be handled by the model itself, without any
//      colorModel.setPlotType(plotType);
//    }
  }

  @Override
  public boolean isExpectDelay() {
    return expectDelay;
  }

  public void setExpectDelay(boolean expectDelay) {
    getPropertyChangeSupport().firePropertyChange("expectDelay", this.expectDelay, this.expectDelay = expectDelay);
  }

  @Override
  public boolean isBoxed() {
    return boxed;
  }

  public void setBoxed(boolean boxed) {
    getPropertyChangeSupport().firePropertyChange("boxed", this.boxed, this.boxed = boxed);
  }

  @Override
  public boolean isScaleBox() {
    return scaleBox;
  }

  public void setScaleBox(boolean scaleBox) {
    getPropertyChangeSupport().firePropertyChange("scaleBox", this.scaleBox, this.scaleBox = scaleBox);
  }

  @Override
  public boolean isDisplayXY() {
    return displayXY;
  }

  public void setDisplayXY(boolean displayXY) {
    getPropertyChangeSupport().firePropertyChange("displayXY", this.displayXY, this.displayXY = displayXY);
  }

  @Override
  public boolean isDisplayZ() {
    return displayZ;
  }

  public void setDisplayZ(boolean displayZ) {
    getPropertyChangeSupport().firePropertyChange("displayZ", this.displayZ, this.displayZ = displayZ);
  }

  @Override
  public boolean isDisplayGrids() {
    return displayGrids;
  }

  public void setDisplayGrids(boolean _displayGrids) {
    getPropertyChangeSupport().firePropertyChange("displayGrids", this.displayGrids, this.displayGrids = _displayGrids);
  }
  
  public PropertyChangeSupport getPropertyChangeSupport() {
    if (property == null)
      property = new SwingPropertyChangeSupport(this);
    return property;
  }

}
