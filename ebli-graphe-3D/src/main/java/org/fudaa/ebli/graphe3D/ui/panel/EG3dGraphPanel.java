
package org.fudaa.ebli.graphe3D.ui.panel;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;

import org.fudaa.ebli.graphe3D.EG3dGraph;
import org.fudaa.ebli.graphe3D.data.EG3dSurfacePlotModel;
import org.fudaa.ebli.graphe3D.data.EG3dGraphM;
import org.fudaa.ebli.graphe3D.ui.EG3dGridBagScrollPane;


public class EG3dGraphPanel extends JPanel {

	
	private static final long serialVersionUID = 1L;
	private EG3dGraph graphsurface;
	private EG3dGridBagScrollPane scrollpane;
	private EG3dBtnPanel btnPanel;

	private void initGraphPanel() {	

		graphsurface = new EG3dGraph();
		scrollpane = new EG3dGridBagScrollPane();
		btnPanel = new EG3dBtnPanel();
		

		setName("this");
		setLayout(new GridBagLayout());
		((GridBagLayout)getLayout()).columnWidths = new int[] {0, 0, 0};
		((GridBagLayout)getLayout()).rowHeights = new int[] {0, 0, 0};
		((GridBagLayout)getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
		((GridBagLayout)getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0E-4};





		//Graphsurface.setInheritsPopupMenu(true);
		graphsurface.setName("surface");
		graphsurface.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				surfaceMouseClicked(e);
			}
			@Override
			public void mousePressed(MouseEvent e) {
				EG3dGraphPanel.this.mousePressed();
			}
		});
		add(graphsurface, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));


		{
			scrollpane.setWidthFixed(true);
			scrollpane.setName("scrollpane");

			btnPanel.setNextFocusableComponent(this);
			btnPanel.setName("configurationPanel");
			scrollpane.setViewportView(btnPanel);
		}
		add(scrollpane, new GridBagConstraints(1, 0, 1, 2, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));



	}

//	private static EG3dDefaultViewRenderer createDefaultRenderer() {
//	  EG3dDefaultViewRenderer renderer=new EG3dDefaultViewRenderer();
//	  renderer.setBoxed(true);
//	  renderer.setDisplayXY(true);
//	  renderer.setExpectDelay(false);
//	  renderer.setAutoScaleZ(true);
//	  renderer.setDisplayZ(true);
//	  renderer.setDisplayGrids(true);
//	  
//	  return renderer;
//	}
	
	/**
	 * @return
	 */
	public static EG3dSurfacePlotModel createDefaultSurfaceModel() {
		final EG3dSurfacePlotModel graphModel = new EG3dSurfacePlotModel();

		graphModel.setPlotFunction2(false);

		graphModel.setCalcDivisions(50);
		graphModel.setDispDivisions(50);
		graphModel.setContourLines(10);

		graphModel.setXMin(-3);
		graphModel.setXMax(3);
		graphModel.setYMin(-3);
		graphModel.setYMax(3);

//		graphModel.setMesh(false);
//		graphModel.setPlotType(PlotType.SURFACE);
		graphModel.setPlotFunction1(true);//setBothFunction(true);
		
//		graphModel.setPlotColor(PlotColor.SPECTRUM);

		//graphModel.setPlotColor(PlotColor.DUALSHADE);
	
		graphModel.setMapper(new EG3dGraphM() {
			@Override
      public  float f1(float x, float y)
			{
				float r = x*x+y*y;

				if (r == 0 ) return 1f;
				return (float)( Math.sin(r)/(r));
			}

			@Override
      public  float f2(float x, float y)
			{
				return (float)(Math.sin(x*y));
			}
		});
		graphModel.plot().execute();
		return graphModel;

	}

	public EG3dGraphPanel() {
		super(new BorderLayout());
		initGraphPanel();
//		graphsurface.setViewRenderer(_renderer);
		btnPanel.setGraph(graphsurface);
//		setModel(model);
	}



//	public void setModel(EG3dDataModel model) {
//		if (model instanceof EbliGraphModel)
//			btnPanel.setModel((EbliGraphModel) model);
//		else {
//			scrollpane.setVisible(false);
//			btnPanel.setModel(null);
//		}
//		graphsurface.setModel(model);
//	}


	/**
	 * @return
	 * @see java.awt.Component#isVisible()
	 */
	public boolean isConfigurationVisible() {
		return scrollpane.isVisible();
	}

	/**
	 * @param aFlag
	 * @see javax.swing.JComponent#setVisible(boolean)
	 */
	public void setBtnPanelVisible(boolean aFlag) {
		scrollpane.setVisible(aFlag);
		invalidate();
		revalidate();
	}


	private void toggleConfiguration() {
		setBtnPanelVisible(!isConfigurationVisible());
		if ( !isConfigurationVisible())
			graphsurface.requestFocusInWindow();
	}

	public EG3dGraph getView() {
		return graphsurface;
	}

	private void mousePressed() {
		graphsurface.requestFocusInWindow();
	}

	private void surfaceMouseClicked(MouseEvent e) {
		if (e.getClickCount()>=2)
			toggleConfiguration();
	}

	public EG3dBtnPanel getBtnPanel() {
		return btnPanel;
	}

	public void setBtnPanel(EG3dBtnPanel btnPanel) {
		this.btnPanel = btnPanel;
	}

}
