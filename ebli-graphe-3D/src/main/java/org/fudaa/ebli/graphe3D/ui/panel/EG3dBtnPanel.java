
package org.fudaa.ebli.graphe3D.ui.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.fudaa.ebli.graphe3D.EG3dGraph;
import org.fudaa.ebli.graphe3D.data.EG3dSurfaceModel.PlotColor;
import org.fudaa.ebli.graphe3D.data.EG3dSurfaceModel.PlotType;
import org.fudaa.ebli.graphe3D.renderer.EG3dDefaultDataRenderer;
import org.fudaa.ebli.graphe3D.ui.EG3dScrollablePanel;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TraceLigne;

import com.memoire.bu.BuResource;



/** 
 * panel de configuration des actions li�es au graphe.
 *
 */
public class EG3dBtnPanel extends EG3dScrollablePanel {
	
	private JCheckBox btnShowBox;	
	private JCheckBox btnDisplayXY;
	private JCheckBox btnDisplayZ;
	private JCheckBox btnDisplayGrids;	
//	private EventModel graphEventModel;
//	private EbliGraphPlotModel graphPlotModel;
	private JRadioButton contourType;
	private JRadioButton surfaceType;
	private JRadioButton densityType;
	private JCheckBox scaleBox;
	private JCheckBox mesh;
	private JRadioButton hiddenMode;
	private JRadioButton spectrumMode;
	private JRadioButton dualShadeMode;
	private JRadioButton grayScaleMode;
	private JRadioButton fogMode;
	private JRadioButton wireframeType;
	private JRadioButton wireframePTType;
	
	private int lineStyle_;
	
	private static final long serialVersionUID = 1L;
	/** Vue 3D */
	private EG3dGraph view_;
	private EG3dDefaultDataRenderer defaultDataRenderer_;
	
	
	public EG3dBtnPanel() {
		buildBtnPanel();
	}
	
	public void setGraph(EG3dGraph _view) {
//		graphEventModel.setSurfaceModel(model);
	  defaultDataRenderer_=new EG3dDefaultDataRenderer();
	  lineStyle_=defaultDataRenderer_.getLineStyle(0);
	  
    view_=_view;
	  view_.setDefaultDataRenderer(defaultDataRenderer_);
	  
	  btnShowBox.setSelected(_view.getViewRenderer().isBoxed());
	  btnDisplayXY.setSelected(_view.getViewRenderer().isDisplayXY());
    btnDisplayZ.setSelected(_view.getViewRenderer().isDisplayZ());
    btnDisplayGrids.setSelected(_view.getViewRenderer().isDisplayGrids());
    scaleBox.setSelected(_view.getViewRenderer().isScaleBox());
    mesh.setSelected(lineStyle_!=TraceLigne.INVISIBLE);
    
    hiddenMode.setSelected(defaultDataRenderer_.getPlotColor()==PlotColor.OPAQUE);
    fogMode.setSelected(defaultDataRenderer_.getPlotColor()==PlotColor.FOG);
    dualShadeMode.setSelected(defaultDataRenderer_.getPlotColor()==PlotColor.DUALSHADE);
    grayScaleMode.setSelected(defaultDataRenderer_.getPlotColor()==PlotColor.GRAYSCALE);
    spectrumMode.setSelected(defaultDataRenderer_.getPlotColor()==PlotColor.SPECTRUM);

	  wireframeType.setSelected(defaultDataRenderer_.getPlotType()==PlotType.WIREFRAME);
    wireframePTType.setSelected(defaultDataRenderer_.getPlotType()==PlotType.WIREFRAME_PT);
    densityType.setSelected(defaultDataRenderer_.getPlotType()==PlotType.DENSITY);
    contourType.setSelected(defaultDataRenderer_.getPlotType()==PlotType.CONTOUR);
    surfaceType.setSelected(defaultDataRenderer_.getPlotType()==PlotType.SURFACE);
	}

	
	
	private void buildBtnPanel() {
		btnShowBox = new JCheckBox();
		btnDisplayXY = new JCheckBox();
		btnDisplayZ = new JCheckBox();
		btnDisplayGrids = new JCheckBox();
//		graphEventModel = new EventModel();
//		graphPlotModel = new EbliGraphPlotModel();
		scaleBox = new JCheckBox();
		mesh = new JCheckBox();
		hiddenMode = new JRadioButton();
		spectrumMode = new JRadioButton();
		grayScaleMode = new JRadioButton();
		dualShadeMode = new JRadioButton();
		fogMode = new JRadioButton();
		wireframeType = new JRadioButton();
		wireframePTType = new JRadioButton();
		surfaceType = new JRadioButton();
		contourType = new JRadioButton();
		densityType = new JRadioButton();
		
		
		setBorder(new EmptyBorder(6, 6, 6, 6));
		setBackground(Color.white);
		
		setLayout(new BorderLayout());
		
		Box box =Box.createVerticalBox();
		add(box, BorderLayout.CENTER);
		
		
		btnShowBox.setBackground(Color.white);
		btnShowBox.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (view_!=null)
          view_.getViewRenderer().setBoxed(btnShowBox.isSelected());
      }
    });
		box.add(btnShowBox);
		
		btnDisplayXY.setBackground(Color.white);
    btnDisplayXY.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (view_!=null)
          view_.getViewRenderer().setDisplayXY(btnDisplayXY.isSelected());
      }
    });
		box.add(btnDisplayXY);

		btnDisplayZ.setBackground(Color.white);
		btnDisplayZ.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (view_!=null)
          view_.getViewRenderer().setDisplayZ(btnDisplayZ.isSelected());
      }
    });
		box.add(btnDisplayZ);
		
		btnDisplayGrids.setBackground(Color.white);
		btnDisplayGrids.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (view_!=null)
          view_.getViewRenderer().setDisplayGrids(btnDisplayGrids.isSelected());
      }
    });
		box.add(btnDisplayGrids);
		
		scaleBox.setBackground(Color.white);
		scaleBox.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (view_!=null)
          view_.getViewRenderer().setScaleBox(scaleBox.isSelected());
      }
    });
		box.add(scaleBox);

		mesh.setBackground(Color.white);
		mesh.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (view_!=null) {
          if (defaultDataRenderer_.getLineStyle(0)==TraceLigne.INVISIBLE)
            defaultDataRenderer_.setLineStyle(0,lineStyle_);
          else
            defaultDataRenderer_.setLineStyle(0,TraceLigne.INVISIBLE);
        }
      }
    });
		box.add(mesh);
		box.add(new JSeparator(SwingConstants.HORIZONTAL));
		
		hiddenMode.setBackground(Color.white);
		hiddenMode.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (view_!=null) {
          defaultDataRenderer_.setPlotColor(PlotColor.OPAQUE);
        }
      }
    });
//		hiddenMode.setSourceBean(graphEventModel);
//		hiddenMode.setPropertyName("hiddenMode");
		box.add(hiddenMode);
		
		spectrumMode.setBackground(Color.white);
		spectrumMode.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (view_!=null) {
          defaultDataRenderer_.setPlotColor(PlotColor.SPECTRUM);
        }
      }
    });
//		spectrumMode.setSourceBean(graphEventModel);
//		spectrumMode.setPropertyName("spectrumMode");
		box.add(spectrumMode);
		
		grayScaleMode.setBackground(Color.white);
		grayScaleMode.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (view_!=null) {
          defaultDataRenderer_.setPlotColor(PlotColor.GRAYSCALE);
        }
      }
    });
//		grayScaleMode.setSourceBean(graphEventModel);
//		grayScaleMode.setPropertyName("grayScaleMode");
		box.add(grayScaleMode);
		
		dualShadeMode.setBackground(Color.white);
		dualShadeMode.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (view_!=null) {
          defaultDataRenderer_.setPlotColor(PlotColor.DUALSHADE);
        }
      }
    });
//		dualShadeMode.setSourceBean(graphEventModel);
//		dualShadeMode.setPropertyName("dualShadeMode");
		box.add(dualShadeMode);
		
		
		fogMode.setBackground(Color.white);
		fogMode.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (view_!=null) {
          defaultDataRenderer_.setPlotColor(PlotColor.FOG);
        }
      }
    });
//		fogMode.setSourceBean(graphEventModel);
//		fogMode.setPropertyName("fogMode");
		box.add(fogMode);
		
		box.add(new JSeparator(SwingConstants.HORIZONTAL));
		
		
		wireframeType.setBackground(Color.white);
		wireframeType.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (view_!=null) {
          defaultDataRenderer_.setPlotType(PlotType.WIREFRAME);
        }
      }
    });
//		wireframeType.setSourceBean(graphEventModel);
//		wireframeType.setPropertyName("wireframeType");
		box.add(wireframeType);
		
		wireframePTType.setBackground(Color.white);
		wireframePTType.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (view_!=null) {
          defaultDataRenderer_.setPlotType(PlotType.WIREFRAME_PT);
        }
      }
    });
//		wireframePTType.setSourceBean(graphEventModel);
//		wireframePTType.setPropertyName("wireframePTType");
		box.add(wireframePTType);
		
		
		surfaceType.setBackground(Color.white);
		surfaceType.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (view_!=null) {
          defaultDataRenderer_.setPlotType(PlotType.SURFACE);
        }
      }
    });
//		surfaceType.setSourceBean(graphEventModel);
//		surfaceType.setPropertyName("surfaceType");
		box.add(surfaceType);
		box.add(new JSeparator(SwingConstants.HORIZONTAL));
		
		
		contourType.setBackground(Color.white);
		contourType.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (view_!=null) {
          defaultDataRenderer_.setPlotType(PlotType.CONTOUR);
        }
      }
    });
//		contourType.setSourceBean(graphEventModel);
//		contourType.setPropertyName("contourType");
		box.add(contourType);
		
		
		densityType.setBackground(Color.white);
		densityType.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        if (view_!=null) {
          defaultDataRenderer_.setPlotType(PlotType.DENSITY);
        }
      }
    });
//		densityType.setSourceBean(graphEventModel);
//		densityType.setPropertyName("densityType");
		box.add(densityType);
		
//		graphEventModel.setSurfaceModel(graphPlotModel);
//		graphPlotModel.setSecondFunctionOnly(true);
		
		ButtonGroup buttonGroup1 = new ButtonGroup();
		buttonGroup1.add(hiddenMode);
		buttonGroup1.add(spectrumMode);
		buttonGroup1.add(grayScaleMode);
		buttonGroup1.add(dualShadeMode);
		buttonGroup1.add(fogMode);

		//---- buttonGroup2 ----
		ButtonGroup buttonGroup2 = new ButtonGroup();
		buttonGroup2.add(wireframeType);
		buttonGroup2.add(wireframePTType);
		
		buttonGroup2.add(surfaceType);
		buttonGroup2.add(contourType);
		buttonGroup2.add(densityType);
		// Par defaut, la traduction est dans le fichier de traduction Ebli.
		translate(EbliResource.EBLI);
	}

	
  public void translate(BuResource resource) {
    densityType.setText(resource.getString("Mode densit�"));
    wireframeType.setText(resource.getString("Type Wireframe"));
    contourType.setText(resource.getString("Mode contour"));
    surfaceType.setText(resource.getString("Type Surface"));
    wireframePTType.setText(resource.getString("Type Nuage de points"));
    fogMode.setText(resource.getString("Mode Fog"));
    dualShadeMode.setText(resource.getString("Nuance"));
    grayScaleMode.setText(resource.getString("Nuance de gris"));
    spectrumMode.setText(resource.getString("Mode spectre"));
    hiddenMode.setText(resource.getString("Mode cach�"));
    mesh.setText(resource.getString("Voir le maillage"));
    scaleBox.setText(resource.getString("Redimensionner"));
    btnDisplayGrids.setText(resource.getString("Afficher la grille"));
    btnDisplayZ.setText(resource.getString("Afficher l'axe Z"));
    btnDisplayXY.setText(resource.getString("Afficher les axes X et Y"));
    btnShowBox.setText(resource.getString("Afficher les extr�mit�s"));
  }
	
	/**
	 * Affiche ou non les boutons sp�cifiques aux datas surface.
	 * @param _b True : Les boutons sont affich�s.
	 */
  public void showSurfaceButton(boolean _b) {
    mesh.setVisible(_b);
    hiddenMode.setVisible(_b);
    spectrumMode.setVisible(_b);
    grayScaleMode.setVisible(_b);
    dualShadeMode.setVisible(_b);
    fogMode.setVisible(_b);
    wireframeType.setVisible(_b);
    wireframePTType.setVisible(_b);
    surfaceType.setVisible(_b);
    contourType.setVisible(_b);
    densityType.setVisible(_b);
  }

	public JCheckBox getBtnShowBox() {
		return btnShowBox;
	}

	public void setBtnShowBox(JCheckBox btnShowBox) {
		this.btnShowBox = btnShowBox;
	}

	public JCheckBox getBtnDisplayXY() {
		return btnDisplayXY;
	}

	public void setBtnDisplayXY(JCheckBox btnDisplayXY) {
		this.btnDisplayXY = btnDisplayXY;
	}

	public JCheckBox getBtnDisplayZ() {
		return btnDisplayZ;
	}

	public void setBtnDisplayZ(JCheckBox btnDisplayZ) {
		this.btnDisplayZ = btnDisplayZ;
	}

	public JCheckBox getBtnDisplayGrids() {
		return btnDisplayGrids;
	}

	public void setBtnDisplayGrids(JCheckBox btnDisplayGrids) {
		this.btnDisplayGrids = btnDisplayGrids;
	}

	public JRadioButton getContourType() {
		return contourType;
	}

	public void setContourType(JRadioButton contourType) {
		this.contourType = contourType;
	}

	public JRadioButton getSurfaceType() {
		return surfaceType;
	}

	public void setSurfaceType(JRadioButton surfaceType) {
		this.surfaceType = surfaceType;
	}

	public JRadioButton getDensityType() {
		return densityType;
	}

	public void setDensityType(JRadioButton densityType) {
		this.densityType = densityType;
	}

	public JCheckBox getScaleBox() {
		return scaleBox;
	}

	public void setScaleBox(JCheckBox scaleBox) {
		this.scaleBox = scaleBox;
	}

	public JCheckBox getMesh() {
		return mesh;
	}

	public void setMesh(JCheckBox mesh) {
		this.mesh = mesh;
	}

	public JRadioButton getHiddenMode() {
		return hiddenMode;
	}

	public void setHiddenMode(JRadioButton hiddenMode) {
		this.hiddenMode = hiddenMode;
	}

	public JRadioButton getSpectrumMode() {
		return spectrumMode;
	}

	public void setSpectrumMode(JRadioButton spectrumMode) {
		this.spectrumMode = spectrumMode;
	}

	public JRadioButton getDualShadeMode() {
		return dualShadeMode;
	}

	public void setDualShadeMode(JRadioButton dualShadeMode) {
		this.dualShadeMode = dualShadeMode;
	}

	public JRadioButton getGrayScaleMode() {
		return grayScaleMode;
	}

	public void setGrayScaleMode(JRadioButton grayScaleMode) {
		this.grayScaleMode = grayScaleMode;
	}

	public JRadioButton getFogMode() {
		return fogMode;
	}

	public void setFogMode(JRadioButton fogMode) {
		this.fogMode = fogMode;
	}

	public JRadioButton getWireframeType() {
		return wireframeType;
	}

	public void setWireframeType(JRadioButton wireframeType) {
		this.wireframeType = wireframeType;
	}

	public JRadioButton getWireframePTType() {
		return wireframePTType;
	}

	public void setWireframePTType(JRadioButton wireframePTType) {
		this.wireframePTType = wireframePTType;
	}
	

}
