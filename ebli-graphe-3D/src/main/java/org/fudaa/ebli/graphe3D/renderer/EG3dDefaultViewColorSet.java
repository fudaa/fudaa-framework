package org.fudaa.ebli.graphe3D.renderer;

import java.awt.Color;

public class EG3dDefaultViewColorSet implements EG3dViewColorSet {

  protected Color lineColor=Color.DARK_GRAY;
  protected Color lineboxColor=Color.getHSBColor(0f, 0f, 0.5f);
  protected Color lightColor=Color.WHITE;
  protected Color boxColor=Color.getHSBColor(0f, 0f, 0.95f);

  @Override
  public Color getBackgroundColor() {
    return lightColor;
  }

  @Override
  public Color getLineBoxColor() {
    return lineboxColor;
  }

  @Override
  public Color getBoxColor() {
    return boxColor;
  }

  @Override
  public Color getLineColor() {
    return lineColor;
  }

  @Override
  public Color getTextColor() {
    return lineColor;
  }

}
