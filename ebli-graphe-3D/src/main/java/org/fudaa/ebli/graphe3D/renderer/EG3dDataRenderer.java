package org.fudaa.ebli.graphe3D.renderer;

import java.awt.Color;
import java.beans.PropertyChangeSupport;

import org.fudaa.ebli.graphe3D.data.EG3dSurfaceModel.PlotColor;
import org.fudaa.ebli.graphe3D.data.EG3dSurfaceModel.PlotType;

public interface EG3dDataRenderer {
  
  /**
   * @param _idx L'indice du point
   * @return Le type de trait entre ce point et le suivant. Peut �tre invisible.
   */
  int getLineStyle(int _idx);
  
  /**
   * @param _idx
   * @return La couleur du trait entre ce point et le suivant.
   */
  Color getLineColor(int _idx);
  
  /**
   * @param _idx
   * @return L'epaisseur du trait entre ce point et le suivant.
   */
  float getLineThickness(int _idx);
  
  /**
   * @param _idx
   * @return Le type du marqueur pour ce point.
   */
  int getMarkType(int _idx);
  
  /**
   * @param _idx
   * @return La couleur du marqueur pour ce point.
   */
  Color getMarkColor(int _idx);
  
  /**
   * @param _idx
   * @return La taille du marqueur pour ce point.
   */
  int getMarkSize(int _idx);

  /**
   * Surface 3D
   * @param curve
   * @param z
   * @return
   */
  public Color getLineColor(int curve, float z);

  /**
   * Surface 3D
   */
  public Color getPolygonColor(int curve, float z);

  /**
   * Surface 3D
   */
  public Color getFirstPolygonColor(float z);

  /**
   * Surface 3D
   */
  public Color getSecondPolygonColor(float z);

  /**
   * Surface 3D
   */
  public PlotType getPlotType();

  /**
   * Surface 3D
   */
  public PlotColor getPlotColor();
  
  public PropertyChangeSupport getPropertyChangeSupport();
}
