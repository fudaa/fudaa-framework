package org.fudaa.ebli.graphe3D.renderer;

import java.awt.Color;

public interface EG3dViewColorSet {

	public Color getBackgroundColor();

	public Color getLineBoxColor();

	public Color getBoxColor();

	public Color getLineColor();

	public Color getTextColor();
}