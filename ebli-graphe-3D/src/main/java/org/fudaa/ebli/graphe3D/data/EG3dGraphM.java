package org.fudaa.ebli.graphe3D.data;


public interface EG3dGraphM {

	public float f1(float x, float y);

	public float f2(float x, float y);

}