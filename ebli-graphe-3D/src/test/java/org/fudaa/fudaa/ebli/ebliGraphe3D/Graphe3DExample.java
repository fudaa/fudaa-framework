package org.fudaa.fudaa.ebli.ebliGraphe3D;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.fudaa.ebli.graphe3D.data.EG3dSurfaceArrayModel;
import org.fudaa.ebli.graphe3D.data.EG3dSurfaceProfilesModel;
import org.fudaa.ebli.graphe3D.data.EG3dSurfaceProfilesModel.EbliGraphDataStructure;
import org.fudaa.ebli.graphe3D.ui.panel.EG3dGraphPanel;


public class Graphe3DExample {

	public void testSomething() {
		EG3dGraphPanel jsp = new EG3dGraphPanel();
		jsp.setPreferredSize(new Dimension(600,600));
		JFrame jf = new JFrame("test");
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf.getContentPane().add(jsp, BorderLayout.CENTER);
		jf.pack();
		jf.setVisible(true);

		Random rand = new Random();
		int max = 10;
		System.out.println("valeurs de listeProfils:");
		float[][] z1 = new float[max][max];
		float[][] z2 = new float[max][max];
		for (int i = 0; i < max; i++) {
			for (int j = 0; j < max; j++) {
				z1[i][j] = rand.nextFloat() * 20 - 10f;
				z2[i][j] = rand.nextFloat() * 20 - 10f;
				System.out.println("z1["+i+"]["+j+"]="+z1[i][j] );
			}
		}
		EG3dSurfaceArrayModel sm = new EG3dSurfaceArrayModel();
		/*
		 * Note adrien: il faut changer le ArraySurfaceModel.setValues()
		 * actuellement il prend pour une matrice t[i][j]
		 * x = i
		 * y = j
		 * et z= t[i][j]
		 * Attention a bien recalculer les xmin max et idem pour y.
		 * 
		 */		
		
		//-- exemple courbe x caré --//
		z1 = new float[max][max];
		
		for (int i = 0; i < max; i++) {
			for (int j = 0; j < max; j++) {
				z1[i][j]= i*i;
			}
		}
		
		int ymax = 30;
		sm.setValues(0f,10f,0f,ymax,max, z1, z2);
		
		EG3dSurfaceProfilesModel dataModel = new EG3dSurfaceProfilesModel();
		
		
		int nbPasTemps = 10;
		int nbProfils = 20;
		int aChangerPourAvoirtotuProfils = nbProfils;
		float[][] xData = new float[aChangerPourAvoirtotuProfils][nbProfils]; //les x des courbes il faut ramener les x entre -10 et 10
		
		
		float[] abscisseData =  new float[nbProfils]; // les absicces des biefs, il faut ramener les abscisses entre -10 et 10.
		
		
		for(int i=1;i<nbProfils;i++)
			abscisseData[i] = abscisseData[i-1]+i;
		abscisseData[0] = 4;
		abscisseData[1] = 13;
		abscisseData[2] = 40;
		
		
		
		/*xData[1] = 10;
		xData[2] = 25;
		*/
		
		
		float[][] listeProfils = new float[aChangerPourAvoirtotuProfils][nbProfils];
		
		for (int i = 0; i < nbProfils; i++){ 
			listeProfils[0][i] = 8;
			listeProfils[1][i] = 7;
			listeProfils[2][i] = 6;
			listeProfils[3][i] = 5;
			listeProfils[4][i] = 2;
			listeProfils[5][i] = 2;
			listeProfils[6][i] = 2;
			listeProfils[7][i] = 7;
			listeProfils[8][i] = 9;
			listeProfils[9][i] = 8;
			for(int j=10;j<aChangerPourAvoirtotuProfils;j++)
				listeProfils[j][i] = listeProfils[9][i];
			
			
			for(int k=0;k<aChangerPourAvoirtotuProfils;k++) {
				if(k<nbPasTemps)
					xData[k][i] = k;
				else
					xData[k][i] = xData[k-1][i];
			}
			
		}
		int i=2;
		/*
		listeProfils[0][i] = 5;
		listeProfils[1][i] = 4;
		listeProfils[2][i] = 4;
		listeProfils[3][i] = 3;
		listeProfils[4][i] = 2;
		listeProfils[5][i] = 1;
		listeProfils[6][i] = 3;
		listeProfils[7][i] = 8;
		listeProfils[8][i] = 9;
		listeProfils[9][i] = 8;
		for(int j=10;j<aChangerPourAvoirtotuProfils;j++)
			listeProfils[j][i] = listeProfils[9][i];
		*/
		i=19;
		listeProfils[0][i] = 7;
		listeProfils[1][i] = 6;
		listeProfils[2][i] = 5;
		listeProfils[3][i] = 4;
		listeProfils[4][i] = 3;
		listeProfils[5][i] = 3;
		listeProfils[6][i] = 5;
		listeProfils[7][i] = 7;
		listeProfils[8][i] = 9;
		listeProfils[9][i] = 8;
		for(int j=10;j<aChangerPourAvoirtotuProfils;j++)
			listeProfils[j][i] = listeProfils[9][i];
		
		EbliGraphDataStructure s = new EbliGraphDataStructure(xData, listeProfils,abscisseData,nbProfils);
		
		dataModel.setValues(s);
		
		
		
		//jsp.setModel(sm); //model par defaut
		jsp.getView().addData(dataModel); //model custom 
		
		//dataModel.getProjector().setProjectionArea(new java.awt.Rectangle(20,20, 200,300));
		
		
		// sm.doRotate();

		// canvas.doPrint();
		// sm.doCompute();
	}

	public static float f1(float x, float y) {
		// System.out.print('.');
		return (float) (Math.sin(x * x + y * y) / (x * x + y * y));
		// return (float)(10*x*x+5*y*y+8*x*y -5*x+3*y);
	}

	public static float f2(float x, float y) {
		return (float) (Math.sin(x * x - y * y) / (x * x + y * y));
		// return (float)(10*x*x+5*y*y+15*x*y-2*x-y);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
      public void run() {
				new Graphe3DExample().testSomething();
			}
		});

	}

}
