/*
 *  @creation     11 avr. 2005
 *  @modification $Date: 2007-05-21 10:28:31 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gis;

import gnu.trove.TDoubleArrayList;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluLibString;
import org.locationtech.jts.algorithm.RobustLineIntersector;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Polygon;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class TestJGIS extends TestCase {

  public TestJGIS() {

  }
  
  public void testProjectOnPlane(){
    // Cas g�n�ral
    GISPoint[] plan = {new GISPoint(1, 0, 0), new GISPoint(0, 1, 0), new GISPoint(0, 0, 1)};
    GISPoint result = GISLib.projectOnPlane(plan, new GISPoint(0., 0., 0.));
    assertEquals(result.getX(), 1/3., 0.0000001);
    assertEquals(result.getY(), 1/3., 0.0000001);
    assertEquals(result.getZ(), 1/3., 0.0000001);
    // Point sur plan : Le point retour est le m�me que le point en entr�e.
    GISPoint res2=GISLib.projectOnPlane(plan, result);
    assertEquals(result.getX(),res2.getX(), 0.0000001);
    assertEquals(result.getY(),res2.getY(), 0.0000001);
    assertEquals(result.getZ(),res2.getZ(), 0.0000001);
    // Test de robustesse : valeur null
    assertNull(GISLib.projectOnPlane(null, new GISPoint(0, 0, 0)));
    assertNull(GISLib.projectOnPlane(plan, null));
    plan[1] = null;
    assertNull(GISLib.projectOnPlane(plan, new GISPoint(0, 0, 0)));
    // Test de robustesse : plan mal d�fini
    plan[1] = plan[0];
    assertNull(GISLib.projectOnPlane(plan, new GISPoint(0, 0, 0)));
    GISPoint[] plan2 = {new GISPoint(0, 1, 0), new GISPoint(0, 0, 1)};
    assertNull(GISLib.projectOnPlane(plan2, new GISPoint(0, 0, 0)));
  }
  

  public void testPoint() {
    final GISZoneCollectionPoint ptzone = GISZoneAttributeFactory.createPoint(null, new GISAttributeInterface[] {
        GISAttributeConstants.TITRE, GISAttributeConstants.BATHY });
    assertEquals(2, ptzone.getNbAttributes());
    assertEquals(GISAttributeConstants.TITRE, ptzone.getAttribute(0));
    assertEquals(GISAttributeConstants.BATHY, ptzone.getAttribute(1));
    final GISPoint[] pt = new GISPoint[3];
    final String[] name = new String[pt.length];
    final double[] val = new double[pt.length];
    for (int i = pt.length - 1; i >= 0; i--) {
      pt[i] = new GISPoint(i * 10 + 1, i * 10 + 2, i * 10 + 3);
      val[i] = (i + 1) * 100;
      name[i] = CtuluLibString.getString(i);
    }
    final List r = new ArrayList();
    r.add(name);
    r.add(val);
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    ptzone.addAll(pt, r, cmp);
    assertEquals(pt.length, ptzone.getNumGeometries());
    for (int i = pt.length - 1; i >= 0; i--) {
      final GISPoint p = ptzone.get(i);
      assertTrue(p.isSame(pt[i]));
      assertEquals(name[i], ptzone.getModel(GISAttributeConstants.TITRE).getObjectValueAt(i));
      assertEquals(val[i], ((GISAttributeModelDoubleInterface) ptzone.getModel(GISAttributeConstants.BATHY))
          .getValue(i), 1E-5);
    }
    cmp.undo();
    assertEquals(0, ptzone.getNumGeometries());
    assertEquals(0, ptzone.getModel(GISAttributeConstants.TITRE).getSize());
    assertEquals(0, ptzone.getModel(GISAttributeConstants.BATHY).getSize());
    cmp.redo();
    assertEquals(pt.length, ptzone.getNumGeometries());
    for (int i = pt.length - 1; i >= 0; i--) {
      final GISPoint p = ptzone.get(i);
      assertTrue(p.isSame(pt[i]));
      assertEquals(name[i], ptzone.getModel(GISAttributeConstants.TITRE).getObjectValueAt(i));
      assertEquals(val[i], ((GISAttributeModelDoubleInterface) ptzone.getModel(GISAttributeConstants.BATHY))
          .getValue(i), 1E-5);
    }
  }

  /**
   * On tester les zones de lignes brisee et un adapter de polyligne en source de donn�es.
   */
  public void testAdapter() {
    final TDoubleArrayList liste = new TDoubleArrayList();
    liste.add(0);
    liste.add(5);
    liste.add(5.5);
    liste.add(10);
    liste.add(15);
    liste.add(15.5);
    liste.add(20);
    liste.add(25);
    liste.add(25.5);
    final GISCoordinateSequence seq = new GISCoordinateSequence(liste);
    final GISPolyligne line = (GISPolyligne) GISGeometryFactory.INSTANCE.createLineString(seq);

    final GISAttributeInterface att = new GISAttributeDouble("toto", false);
    final GISAttributeInterface attAtomic = new GISAttributeDouble("tata", true);
    final GISZoneCollectionLigneBrisee zone = GISZoneAttributeFactory.createPolyligne(null,
        new GISAttributeInterface[] { att, attAtomic });
    final CtuluCommandComposite cmp = new CtuluCommandComposite();
    final Object[] o = new Object[2];
    o[0] = new Double(23);
    final Double[] v = new Double[3];
    v[0] = new Double(100);
    v[1] = new Double(200);
    v[2] = new Double(300);
    o[1] = v;
    zone.addPolyligne(line, o, cmp);
    assertEquals(1, zone.getNumGeometries());
    cmp.undo();
    assertEquals(0, zone.getNumGeometries());
    cmp.redo();
    assertEquals(1, zone.getNumGeometries());
    GISAttributeModelDoubleInterface model = (GISAttributeModelDoubleInterface) zone.getModel(att);
    assertNotNull(model);
    assertEquals(23, model.getValue(0), 1E-5);
    model = (GISAttributeModelDoubleInterface) ((GISAttributeModelObjectInterface) zone.getModel(attAtomic))
        .getObjectValueAt(0);
    assertNotNull(model);
    assertEquals(100, model.getValue(0), 1E-5);
    assertEquals(200, model.getValue(1), 1E-5);
    assertEquals(300, model.getValue(2), 1E-5);
    final GISDataModelZoneLignePointAdapter adapter = new GISDataModelZoneLignePointAdapter(zone, 0, new int[] { 0, 1 });
    assertEquals(2, adapter.getNbAttributes());
    assertEquals(seq.size(), adapter.getNumGeometries());
    for (int i = adapter.getNumGeometries() - 1; i >= 0; i--) {
      assertEquals(23, adapter.getDoubleValue(0, i), 1E-5);
      assertEquals((i + 1) * 100, adapter.getDoubleValue(1, i), 1E-5);
    }
    final GISDataModelFilterAdapter adapterInutil = new GISDataModelFilterAdapter(adapter, null, null);
    assertEquals(adapter.getNbAttributes(), adapterInutil.getNbAttributes());
    assertEquals(adapter.getNumGeometries(), adapterInutil.getNumGeometries());
    for (int i = adapter.getNumGeometries() - 1; i >= 0; i--) {
      assertEquals(23, adapterInutil.getDoubleValue(0, i), 1E-5);
      assertEquals((i + 1) * 100, adapterInutil.getDoubleValue(1, i), 1E-5);
    }
    GISDataModelFilterAdapter adapterTest = GISDataModelFilterAdapter.buildLigneAdapter(adapter, null);
    assertEquals(adapterInutil.getNumGeometries(), adapterTest.getNumGeometries());
    adapterTest = GISDataModelFilterAdapter.buildLigneOuverteAdapter(adapter, null);
    assertEquals(adapterInutil.getNumGeometries(), adapterTest.getNumGeometries());
    adapterTest = GISDataModelFilterAdapter.buildPointAdapter(adapter, null);
    assertEquals(0, adapterTest.getNumGeometries());
    adapterTest = GISDataModelFilterAdapter.buildLigneFermeeAdapter(adapter, null);
    assertEquals(0, adapterTest.getNumGeometries());
    zone.addPolygone(GISGeometryFactory.INSTANCE.createLinearRing(0, 10, 0, 123), null, null);
    adapterTest = GISDataModelFilterAdapter.buildLigneOuverteAdapter(new GISDataModelZoneAdapter(zone, null), null);
    assertEquals(1, adapterTest.getNumGeometries());
    final Double d = (Double) adapterTest.getValue(0, 0);
    assertNotNull(d);
    assertEquals(23, d.doubleValue(), 1E-5);
    assertNull(adapterTest.getValue(2, 0));
    model = (GISAttributeModelDoubleInterface) adapterTest.getValue(1, 0);
    assertNotNull(model);
    assertEquals(100, model.getValue(0), 1E-5);
    assertEquals(200, model.getValue(1), 1E-5);
    assertEquals(300, model.getValue(2), 1E-5);
  }

  public void testPolygoneIntersect() {
    final GISEnvelopeSplitter spli = new GISEnvelopeSplitter(0, 10, 0, 5);
    Coordinate[] cs = new Coordinate[4];
    cs[0] = new Coordinate(10, 2);
    cs[1] = new Coordinate(5, 2);
    cs[2] = new Coordinate(5, 1);
    cs[3] = new Coordinate(5, 0);
    Polygon p = spli.split(GISGeometryFactory.INSTANCE.createLineString(cs));
    assertNotNull(p);
    LinearRing r = (LinearRing) p.getExteriorRing();
    assertEquals(6, r.getNumPoints());
    Coordinate[] res = new Coordinate[6];
    res[0] = new Coordinate(5, 0);
    res[1] = new Coordinate(10, 0);
    res[2] = new Coordinate(10, 2);
    res[3] = new Coordinate(5, 2);
    res[4] = new Coordinate(5, 1);
    res[5] = res[0];
    for (int i = 0; i < 6; i++) {
      assertTrue(r.getCoordinateN(i).equals2D(res[i]));
    }
    cs = new Coordinate[3];
    cs[0] = new Coordinate(11, 2);
    cs[1] = new Coordinate(5, 2);
    cs[2] = new Coordinate(5, 0);
    p = spli.split(GISGeometryFactory.INSTANCE.createLineString(cs));
    assertNull(p);
    cs = new Coordinate[4];
    cs[0] = new Coordinate(5, 0);
    cs[1] = new Coordinate(5, 1);
    cs[2] = new Coordinate(5, 2);
    cs[3] = new Coordinate(0, 2);
    p = spli.split(GISGeometryFactory.INSTANCE.createLineString(cs));
    assertNotNull(p);
    r = (LinearRing) p.getExteriorRing();
    assertEquals(8, r.getNumPoints());
    res = new Coordinate[8];
    res[0] = new Coordinate(5, 0);
    res[1] = new Coordinate(10, 0);
    res[2] = new Coordinate(10, 5);
    res[3] = new Coordinate(0, 5);
    res[4] = new Coordinate(0, 2);
    res[5] = new Coordinate(5, 2);
    res[6] = new Coordinate(5, 1);
    res[7] = res[0];
    for (int i = 0; i < res.length; i++) {
      assertTrue(r.getCoordinateN(i).equals2D(res[i]));
    }

  }

  public void testIntersect() {
    final Coordinate c1 = new Coordinate(1, 3);
    final Coordinate c2 = new Coordinate(3, 3);
    final Coordinate c3 = new Coordinate(0, 0);
    final Coordinate c4 = new Coordinate(0, 5);
    final RobustLineIntersector intersector = new RobustLineIntersector();
    intersector.computeIntersection(c1, c2, c3, c4);
    assertFalse(intersector.hasIntersection());
    assertFalse(intersector.isProper());
    intersector.computeIntersection(c3, c4, c1, c2);
    assertFalse(intersector.hasIntersection());
    assertFalse(intersector.isProper());
    final GISInfiniteLineIntersector lineInters = new GISInfiniteLineIntersector();
    lineInters.computeIntersectionLineSegment(c1, c2, c3, c4);
    assertTrue(lineInters.hasIntersection());
    assertTrue(lineInters.isProper());
    Coordinate ptInter = lineInters.getIntersectedPoint();
    assertNotNull(ptInter);
    assertEquals(0, ptInter.x, 1E-5);
    assertEquals(3, ptInter.y, 1E-5);
    lineInters.computeIntersectionLineSegment(c3, c4, c1, c2);
    assertFalse(lineInters.hasIntersection());
    assertFalse(lineInters.isProper());
    lineInters.computeIntersectionLineLine(c3, c4, c1, c2);
    assertTrue(lineInters.hasIntersection());
    assertTrue(lineInters.isProper());
    ptInter = lineInters.getIntersectedPoint();
    assertNotNull(ptInter);
    assertEquals(0, ptInter.x, 1E-5);
    assertEquals(3, ptInter.y, 1E-5);

  }

  /**
   * @param _arg0
   */
  public TestJGIS(final String _arg0) {
    super(_arg0);
  }

}
