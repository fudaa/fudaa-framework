/*
 * @creation     8 oct. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Point;
import junit.framework.TestCase;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;

/**
 * Test de la classe GISDataModelAttributAdapter.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class TestJGISDataModelAttributAdapter extends TestCase {

  private GISZoneCollection zoneEmpty_;
  private GISZoneCollection zoneNoEmpty_;
  private GISAttributeInterface[] attr_;
  private Geometry[] geometries_;
  private GISDataModelAttributeAdapter adapterInitialised_;
  
  /**
   * @param name
   */
  public TestJGISDataModelAttributAdapter(String name) {
    super(name);
  }

  public void testAddAttribut_1(){
    // TEST SUR LA ZONE NON VIDE \\
    // Test de la valeur de retour Sur un attribut global
    assertEquals(1, adapterInitialised_.addAttribut(GISAttributeConstants.ETAT_GEOM, GISAttributeConstants.ATT_VAL_ETAT_MODI));
    assertEquals(0, adapterInitialised_.addAttribut(GISAttributeConstants.ETAT_GEOM, GISAttributeConstants.ATT_VAL_ETAT_MODI));
    // Test de de la valeur de retour sur un attribut atomique
    GISAttribute attr = new GISAttribute(new CtuluValueEditorDouble(), "MyAttributAtomique", true);
    assertEquals(1, adapterInitialised_.addAttribut(attr, new Double(2)));
    assertEquals(0, adapterInitialised_.addAttribut(attr, new Double(12)));
    // Test de coh�rence des attributs
    assertEquals(GISAttributeConstants.ETAT_GEOM, adapterInitialised_.getAttribute(attr_.length));
    assertEquals(attr, adapterInitialised_.getAttribute(attr_.length+1));
    // Test de coh�rence des valeurs d'attribut
    assertEquals(GISAttributeConstants.ATT_VAL_ETAT_MODI, adapterInitialised_.getValue(adapterInitialised_.getNbAttributes()-2, 0));
    assertEquals(GISAttributeConstants.ATT_VAL_ETAT_MODI, adapterInitialised_.getValue(adapterInitialised_.getNbAttributes()-2, adapterInitialised_.getNumGeometries()-1));
    Object result = adapterInitialised_.getValue(adapterInitialised_.getNbAttributes()-1, 0);
    assertTrue(result instanceof GISAttributeModelObjectArray);
    assertNotNull(result);
    GISAttributeModelObjectArray expected = (GISAttributeModelObjectArray)attr.createDataForGeom(new Double(2), zoneNoEmpty_.getGeometry(0).getNumPoints());
    assertEquals(expected.getSize(), ((GISAttributeModelObjectArray)result).getSize());
    for(int i=0;i<expected.getSize();i++)
      assertEquals(expected.getValue(i), ((GISAttributeModelObjectArray)result).getValue(i));
    
    // TEST SUR LA ZONE VIDE \\
    GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(zoneEmpty_);
    // Test de la valeur de retour Sur un attribut global
    assertEquals(1, adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, GISAttributeConstants.ATT_VAL_ETAT_MODI));
    assertEquals(0, adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, GISAttributeConstants.ATT_VAL_ETAT_MODI));
    // Test de de la valeur de retour sur un attribut atomique
    assertEquals(1, adapter.addAttribut(attr, new Double(2)));
    assertEquals(0, adapter.addAttribut(attr, new Double(12)));
    // Test de coh�rence des attributs
    assertEquals(GISAttributeConstants.ETAT_GEOM, adapter.getAttribute(0));
    assertEquals(attr, adapter.getAttribute(1));
    
    // TEST AVEC VALEURS NULL \\
    assertEquals(-2, adapterInitialised_.addAttribut(null, new Double(2)));
    assertEquals(-2, adapterInitialised_.addAttribut(GISAttributeConstants.VISIBILITE, null));
    assertEquals(-2, adapterInitialised_.addAttribut(null, null));
  }
  
  public void testAddAttribut_2(){
    // TEST SUR LA ZONE NON VIDE \\
    // Test de la valeur de retour Sur un attribut global
    Object[] valuesEtatGeom = new Object[geometries_.length];
    for(int i=0;i<geometries_.length;i++)
      if(i>0 && valuesEtatGeom[i-1]==GISAttributeConstants.ATT_VAL_ETAT_ORIG)
        valuesEtatGeom[i]=GISAttributeConstants.ATT_VAL_ETAT_MODI;
      else
        valuesEtatGeom[i]=GISAttributeConstants.ATT_VAL_ETAT_ORIG;
    assertEquals(-1, adapterInitialised_.addAttribut(GISAttributeConstants.ETAT_GEOM, new Object[0]));
    assertEquals(1, adapterInitialised_.addAttribut(GISAttributeConstants.ETAT_GEOM, valuesEtatGeom));
    assertEquals(0, adapterInitialised_.addAttribut(GISAttributeConstants.ETAT_GEOM, valuesEtatGeom));
    // Test de de la valeur de retour sur un attribut atomique
    Object[] valuesMyAttr = new Object[geometries_.length];
    for(int i=0;i<geometries_.length;i++)
      valuesMyAttr[i]=new Double(i);
    GISAttribute attr = new GISAttribute(new CtuluValueEditorDouble(), "MyAttributAtomique", true);
    assertEquals(1, adapterInitialised_.addAttribut(attr, valuesMyAttr));
    assertEquals(0, adapterInitialised_.addAttribut(attr, valuesMyAttr));
    // Test de coh�rence des attributs
    assertEquals(GISAttributeConstants.ETAT_GEOM, adapterInitialised_.getAttribute(attr_.length));
    assertEquals(attr, adapterInitialised_.getAttribute(attr_.length+1));
    // Test de coh�rence des valeurs d'attribut
    assertEquals(valuesEtatGeom[0], adapterInitialised_.getValue(adapterInitialised_.getNbAttributes()-2, 0));
    assertEquals(valuesEtatGeom[adapterInitialised_.getNumGeometries()-1], adapterInitialised_.getValue(adapterInitialised_.getNbAttributes()-2, adapterInitialised_.getNumGeometries()-1));
    Object result = adapterInitialised_.getValue(adapterInitialised_.getNbAttributes()-1, 0);
    assertTrue(result instanceof GISAttributeModelObjectArray);
    assertNotNull(result);
    GISAttributeModelObjectArray expected = (GISAttributeModelObjectArray)attr.createDataForGeom(valuesMyAttr[0], zoneNoEmpty_.getGeometry(0).getNumPoints());
    assertEquals(expected.getSize(), ((GISAttributeModelObjectArray)result).getSize());
    for(int i=0;i<expected.getSize();i++)
      assertEquals(expected.getValue(i), ((GISAttributeModelObjectArray)result).getValue(i));
    
    // TEST SUR LA ZONE VIDE \\
    GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(zoneEmpty_);
    // Test de la valeur de retour Sur un attribut global
    assertEquals(-1, adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, valuesEtatGeom));
    assertEquals(1, adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, new Object[0]));
    assertEquals(0, adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, new Object[0]));
    // Test de de la valeur de retour sur un attribut atomique
    assertEquals(-1, adapter.addAttribut(attr, valuesMyAttr));
    assertEquals(1, adapter.addAttribut(attr, new Object[0]));
    assertEquals(0, adapter.addAttribut(attr, new Object[0]));
    // Test de coh�rence des attributs
    assertEquals(GISAttributeConstants.ETAT_GEOM, adapter.getAttribute(0));
    assertEquals(attr, adapter.getAttribute(1));
    
    // TEST AVEC VALEURS NULL \\
    assertEquals(-2, adapterInitialised_.addAttribut(null, new Double[geometries_.length]));
  }
  
  public void testAddAttribut_3(){
    // TEST SUR LA ZONE NON VIDE \\
    // Test de la valeur de retour Sur un attribut global
    assertEquals(-3, adapterInitialised_.addAttribut(GISAttributeConstants.ETAT_GEOM, new Object[0][0]));
    // Test de de la valeur de retour sur un attribut atomique
    Object[][] valuesMyAttr = new Object[geometries_.length][];
    for(int i=0;i<geometries_.length;i++){
      valuesMyAttr[i] = new Object[geometries_[i].getNumPoints()];
      for(int j=0;j<valuesMyAttr[i].length;j++)
        valuesMyAttr[i][j] = new Double(i+j);
    }
    GISAttribute attr = new GISAttribute(new CtuluValueEditorDouble(), "MyAttributAtomique", true);
    assertEquals(-1, adapterInitialised_.addAttribut(attr, new Object[0][0]));
    assertEquals(-1, adapterInitialised_.addAttribut(attr, new Object[geometries_.length][0]));
    assertEquals(1, adapterInitialised_.addAttribut(attr, valuesMyAttr));
    assertEquals(0, adapterInitialised_.addAttribut(attr, valuesMyAttr));
    // Test de coh�rence des attributs
    assertEquals(attr, adapterInitialised_.getAttribute(attr_.length));
    // Test de coh�rence des valeurs d'attribut
    Object result = adapterInitialised_.getValue(adapterInitialised_.getNbAttributes()-1, 0);
    assertTrue(result instanceof GISAttributeModelObjectArray);
    assertNotNull(result);
    GISAttributeModelObjectArray expected = (GISAttributeModelObjectArray)attr.createDataForGeom(valuesMyAttr[0], zoneNoEmpty_.getGeometry(0).getNumPoints());
    assertEquals(expected.getSize(), ((GISAttributeModelObjectArray)result).getSize());
    for(int i=0;i<expected.getSize();i++)
      assertEquals(expected.getValue(i), ((GISAttributeModelObjectArray)result).getValue(i));
    
    
    // TEST SUR LA ZONE VIDE \\
    GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(zoneEmpty_);
    // Test de la valeur de retour Sur un attribut global
    assertEquals(-3, adapterInitialised_.addAttribut(GISAttributeConstants.ETAT_GEOM, new Object[0][0]));
    // Test de de la valeur de retour sur un attribut atomique
    assertEquals(-1, adapter.addAttribut(attr, valuesMyAttr));
    assertEquals(1, adapter.addAttribut(attr, new Object[0][]));
    assertEquals(0, adapter.addAttribut(attr, new Object[0][]));
    // Test de coh�rence des attributs
    assertEquals(attr, adapter.getAttribute(0));
    
    // TEST AVEC VALEURS NULL \\
    assertEquals(-2, adapterInitialised_.addAttribut(null, new Double[geometries_.length][]));
    assertEquals(-2, adapterInitialised_.addAttribut(null, new Double[geometries_.length][0]));
  }
  
  public void testGetAttribut(){
    adapterInitialised_.addAttribut(GISAttributeConstants.ETAT_GEOM, GISAttributeConstants.ATT_VAL_ETAT_MODI);
    assertEquals(attr_[0], adapterInitialised_.getAttribute(0));
    assertEquals(GISAttributeConstants.ETAT_GEOM, adapterInitialised_.getAttribute(attr_.length));
    assertNull(adapterInitialised_.getAttribute(-1));
    assertNull(adapterInitialised_.getAttribute(attr_.length+1));
  }

  public void testGetDoubleValue(){
    adapterInitialised_.addAttribut(GISAttributeConstants.ETAT_GEOM, GISAttributeConstants.ATT_VAL_ETAT_MODI);
    assertEquals((Double)GISAttributeConstants.BATHY.getDefaultValue(), adapterInitialised_.getDoubleValue(0, 0), 0.00001);
    assertEquals(0.0, adapterInitialised_.getDoubleValue(-1, 0), 0.00001);
    assertEquals(0.0, adapterInitialised_.getDoubleValue(0, -1), 0.00001);
    assertEquals(0.0, adapterInitialised_.getDoubleValue(-1, -1), 0.00001);
  }
  
  public void testGetEnvelopeInternal(){
    adapterInitialised_.addAttribut(GISAttributeConstants.ETAT_GEOM, GISAttributeConstants.ATT_VAL_ETAT_MODI);
    assertEquals(zoneNoEmpty_.getEnvelopeInternal(), adapterInitialised_.getEnvelopeInternal());
  }
  
  public void testGetGeometry(){
    adapterInitialised_.addAttribut(GISAttributeConstants.ETAT_GEOM, GISAttributeConstants.ATT_VAL_ETAT_MODI);
    assertEquals(geometries_[0], adapterInitialised_.getGeometry(0));
    assertEquals(geometries_[attr_.length-1], adapterInitialised_.getGeometry(attr_.length-1));
    assertNull(adapterInitialised_.getGeometry(-1));
    assertNull(adapterInitialised_.getGeometry(attr_.length));
  }
  
  public void testGetIndiceOf(){
    adapterInitialised_.addAttribut(GISAttributeConstants.ETAT_GEOM, GISAttributeConstants.ATT_VAL_ETAT_MODI);
    assertEquals(0, adapterInitialised_.getIndiceOf(attr_[0]));
    assertEquals(attr_.length-1, adapterInitialised_.getIndiceOf(attr_[attr_.length-1]));
    assertEquals(attr_.length, adapterInitialised_.getIndiceOf(GISAttributeConstants.ETAT_GEOM));
    assertEquals(-1, adapterInitialised_.getIndiceOf(GISAttributeConstants.VISIBILITE));
    assertEquals(-1, adapterInitialised_.getIndiceOf(null));
  }
  
 public void testGetNbAttributes(){
   assertEquals(attr_.length, adapterInitialised_.getNbAttributes());
   GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(zoneEmpty_);
   assertEquals(0, adapter.getNbAttributes());
   adapterInitialised_.addAttribut(GISAttributeConstants.ETAT_GEOM, GISAttributeConstants.ATT_VAL_ETAT_MODI);
   assertEquals(attr_.length+1, adapterInitialised_.getNbAttributes());
   adapter.addAttribut(GISAttributeConstants.ETAT_GEOM, GISAttributeConstants.ATT_VAL_ETAT_MODI);
   assertEquals(1, adapter.getNbAttributes()); 
 }
  
 public void testGetNumGeometries(){
   assertEquals(geometries_.length, adapterInitialised_.getNumGeometries());
   GISDataModelAttributeAdapter adapter = new GISDataModelAttributeAdapter(zoneEmpty_);
   assertEquals(0, adapter.getNumGeometries());
 }
 
 public void testGetValue(){
   GISAttributeModelDoubleArray reel = (GISAttributeModelDoubleArray) GISAttributeConstants.BATHY.createDataForGeom(GISAttributeConstants.BATHY.getDefaultValue(), geometries_[0].getNumGeometries());
   Object actual = adapterInitialised_.getValue(0, 0);
   assertTrue(actual instanceof GISAttributeModelDoubleArray);
   assertNotNull(actual);
   assertTrue(reel.getSize()==((GISAttributeModelDoubleArray)actual).getSize());
   for(int i=0;i<reel.getSize();i++)
     assertEquals((Double)reel.getValue(i),(Double) ((GISAttributeModelDoubleArray)actual).getValue(i), 0.00001);
   
   Object result = GISAttributeConstants.NATURE.createDataForGeom(GISAttributeConstants.NATURE.getDefaultValue(), geometries_[0].getNumGeometries());
   assertEquals(result, (String)adapterInitialised_.getValue(1, 0));
   
   GISAttributeModelObjectArray reel2 = (GISAttributeModelObjectArray) attr_[2].createDataForGeom(attr_[2].getDefaultValue(), geometries_[0].getNumGeometries());
   actual = adapterInitialised_.getValue(2, 0);
   assertTrue(actual instanceof GISAttributeModelObjectArray);
   assertNotNull(actual);
   assertTrue(reel2.getSize()==((GISAttributeModelObjectArray)actual).getSize());
   for(int i=0;i<reel2.getSize();i++)
     assertEquals(reel2.getValue(i),((GISAttributeModelObjectArray)actual).getValue(i));
   
   reel2 = (GISAttributeModelObjectArray) attr_[2].createDataForGeom(attr_[2].getDefaultValue(), geometries_[1].getNumGeometries());
   actual = adapterInitialised_.getValue(2, 1);
   assertTrue(actual instanceof GISAttributeModelObjectArray);
   assertNotNull(actual);
   assertTrue(reel2.getSize()==((GISAttributeModelObjectArray)actual).getSize());
   for(int i=0;i<reel2.getSize();i++)
     assertEquals(reel2.getValue(i),((GISAttributeModelObjectArray)actual).getValue(i));
   
   assertNull(adapterInitialised_.getValue(-1, 0));
   assertNull(adapterInitialised_.getValue(0, -1));
   assertNull(adapterInitialised_.getValue(-1, -1));
   assertNull(adapterInitialised_.getValue(attr_.length, 0));
   assertNull(adapterInitialised_.getValue(0, geometries_.length));
   assertNull(adapterInitialised_.getValue(attr_.length, geometries_.length));
 }

  /* (non-Javadoc)
   * @see junit.framework.TestCase#setUp()
   */
  @Override
  protected void setUp() throws Exception {
    super.setUp();
    zoneEmpty_ = new GISZoneCollectionGeometry();
    zoneNoEmpty_ = new GISZoneCollectionGeometry();
    // Ajout d'attributs \\
    attr_ = new GISAttributeInterface[]{
        GISAttributeConstants.BATHY,
        GISAttributeConstants.NATURE,
        new GISAttribute(new CtuluValueEditorDouble(), "MyAttribut", true)
        };
    zoneNoEmpty_.setAttributes(attr_, null);
    // Ajout de g�om�tries \\
    geometries_ = new Geometry[]{
        GISGeometryFactory.INSTANCE.createPoint(0, 0, 0),
        GISGeometryFactory.INSTANCE.createMultiPoint(new Point[]{GISGeometryFactory.INSTANCE.createPoint(1, 2, 3), GISGeometryFactory.INSTANCE.createPoint(4, 5, 6)}),
        GISGeometryFactory.INSTANCE.createLinearRing(new Coordinate[]{new Coordinate(7, 8), new Coordinate(4, 10), new Coordinate(9, 10), new Coordinate(7, 8)})
        };
    for (int j=0; j<geometries_.length; j++) {
      Object[] attrModel=new Object[attr_.length];
      for (int i=0; i<attr_.length; i++)
        attrModel[i] =attr_[i].createDataForGeom(attr_[i].getDefaultValue(), geometries_[j].getNumPoints());
      zoneNoEmpty_.addGeometry(geometries_[j], attrModel, null);
    }
    adapterInitialised_ = new GISDataModelAttributeAdapter(zoneNoEmpty_);
  }

  /* (non-Javadoc)
   * @see junit.framework.TestCase#tearDown()
   */
  @Override
  protected void tearDown() throws Exception {
    super.tearDown();
    zoneEmpty_=null;
    zoneNoEmpty_=null;
    attr_=null;
  }

}
