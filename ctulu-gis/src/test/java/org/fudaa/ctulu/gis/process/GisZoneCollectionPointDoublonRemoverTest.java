/*
 GPL 2
 */
package org.fudaa.ctulu.gis.process;

import org.locationtech.jts.geom.Coordinate;
import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;
import org.fudaa.ctulu.interpolation.SupportCoordinate;

/**
 *
 * @author Frederic Deniger
 */
public class GisZoneCollectionPointDoublonRemoverTest extends TestCase {
  
  public void testFilter() {
    List<Coordinate> coords = new ArrayList<Coordinate>();
    coords.add(new Coordinate(1, 2));
    coords.add(new Coordinate(1, 2));//doublon
    coords.add(new Coordinate(1, 3));
    coords.add(new Coordinate(1, 5));
    coords.add(new Coordinate(-100, 5));
    int[] expected = new int[]{4, 0, 2, 3};
    
    SupportCoordinate support = new SupportCoordinate((Coordinate[]) coords.toArray(new Coordinate[coords.size()]));
    GisZoneCollectionPointDoublonRemover remover = new GisZoneCollectionPointDoublonRemover(support, 1e-1);
    PointsMappingDefault createFiltered = remover.createFiltered(null);
    assertNotNull(createFiltered);
    assertEquals(coords.size() - 1, createFiltered.getPtsNb());
    for (int i = 0; i < expected.length; i++) {
      assertEquals(expected[i], createFiltered.getInitialIdx(i));
    }
    
  }
}
