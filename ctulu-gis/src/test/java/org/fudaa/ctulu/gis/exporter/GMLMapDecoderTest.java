/*
 GPL 2
 */
package org.fudaa.ctulu.gis.exporter;

import junit.framework.TestCase;
import org.fudaa.ctulu.gis.*;
import org.locationtech.jts.geom.Coordinate;

import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Frederic Deniger
 */
public class GMLMapDecoderTest extends TestCase {
  protected GISZoneCollectionGeometry load(String file) throws Exception {
    return load(file, false);
  }

  protected GISZoneCollectionGeometry load(String file, boolean applyIndex) throws Exception {
    assertNotNull(GMLMapDecoderTest.class.getResource(file));
    try (final InputStream resourceAsStream = GMLMapDecoderTest.class.getResourceAsStream(file)) {
      GMLReader gml = new GMLReader();
      return new GMLMapDecoder(gml.decode(resourceAsStream), applyIndex).decode();
    }
  }

  public void testRead() {
    try {

      GISDataModel load = load("semis.gml");
      int numGeometries = load.getNumGeometries();
      assertEquals(42, numGeometries);
      GISMultiPoint multipoint = (GISMultiPoint) load.getGeometry(0);
      assertEquals(44, multipoint.getNumPoints());
      Coordinate[] coordinates = multipoint.getCoordinates();
      assertEquals(44, coordinates.length);
      assertEquals(752.2912, coordinates[0].x, 1e-4);
      assertEquals(427.0246, coordinates[0].y, 1e-4);
      assertEquals(10.2625, coordinates[0].getZ(), 1e-4);

      assertEquals(322.2913, coordinates[43].x, 1e-4);
      assertEquals(427.0042, coordinates[43].y, 1e-4);
      assertEquals(10.2637, coordinates[43].getZ(), 1e-4);

      multipoint = (GISMultiPoint) load.getGeometry(41);
      assertEquals(5, multipoint.getNumPoints());
      coordinates = multipoint.getCoordinates();
      assertEquals(5, coordinates.length);
      assertEquals(508.9566, coordinates[0].x, 1e-4);
      assertEquals(317.0947, coordinates[0].y, 1e-4);
      assertEquals(0, coordinates[0].getZ(), 1e-4);

      assertEquals(483.7864, coordinates[4].x, 1e-4);
      assertEquals(307.1576, coordinates[4].y, 1e-4);
      assertEquals(13.4223, coordinates[4].getZ(), 1e-4);
    } catch (Exception ex) {
      Logger.getLogger(GMLMapDecoderTest.class.getName()).log(Level.SEVERE, null, ex);
      fail(ex.getMessage());
    }
  }

  public void testDecodePoint() {
    try {

      GISZoneCollectionPoint load = (GISZoneCollectionPoint) load("pointsZ.gml");
      int numGeometries = load.getNumGeometries();
      assertEquals(4, numGeometries);

      assertEquals(1, load.getNbAttributes());

      final GISAttributeInterface index = load.getAttribute(0);
      assertEquals("ATTRIBUTE_INDEX_GEOM", index.getName());
      assertEquals(Integer.class, index.getDataClass());

      int idxGeom = 0;
      GISPoint multipoint = (GISPoint) load.getGeometry(idxGeom);
      assertEquals(3.074573517465475, multipoint.getX(), 1e-4);
      assertEquals(4.8430950446791226, multipoint.getY(), 1e-4);
      assertEquals(123.0, multipoint.getZ(), 1e-4);
      assertEquals(0, ((Integer) load.getValue(0, idxGeom)).intValue());

      idxGeom = 1;
      multipoint = (GISPoint) load.getGeometry(idxGeom);
      assertEquals(4.655077173030056, multipoint.getX(), 1e-4);
      assertEquals(4.5590982940698614, multipoint.getY(), 1e-4);
      assertEquals(124.0, multipoint.getZ(), 1e-4);
      assertEquals(1, ((Integer) load.getValue(0, idxGeom)).intValue());

      idxGeom = 3;
      multipoint = (GISPoint) load.getGeometry(idxGeom);
      assertEquals(8.779203899268886, multipoint.getX(), 1e-4);
      assertEquals(4.102233956133224, multipoint.getY(), 1e-4);
      assertEquals(126.0, multipoint.getZ(), 1e-4);
      assertEquals(3, ((Integer) load.getValue(0, idxGeom)).intValue());
    } catch (Exception ex) {
      Logger.getLogger(GMLMapDecoderTest.class.getName()).log(Level.SEVERE, null, ex);
      fail(ex.getMessage());
    }
  }

  public void testDecodePointWithFriction() {
    try {

      GISZoneCollectionPoint load = (GISZoneCollectionPoint) load("pointsFrottements.gml");
      int numGeometries = load.getNumGeometries();
      assertEquals(3, numGeometries);

      assertEquals(2, load.getNbAttributes());
      final GISAttributeInterface frictions = load.getAttribute(0);
      assertEquals("frottements", frictions.getName());
      assertEquals(Double.class, frictions.getDataClass());

      final GISAttributeInterface index = load.getAttribute(1);
      assertEquals("ATTRIBUTE_INDEX_GEOM", index.getName());
      assertEquals(Integer.class, index.getDataClass());

      int idxGeom = 0;
      GISPoint gisPoint = (GISPoint) load.getGeometry(idxGeom);
      assertEquals(6.050365556458163, gisPoint.getX(), 1e-4);
      assertEquals(5.127091795288383, gisPoint.getY(), 1e-4);
      assertEquals(1.0, ((Double) load.getValue(0, idxGeom)).doubleValue(), 1e-4);
      assertEquals(0, ((Integer) load.getValue(1, idxGeom)).intValue());

      idxGeom = 1;
      gisPoint = (GISPoint) load.getGeometry(idxGeom);
      assertEquals(7.828432168968317, gisPoint.getX(), 1e-4);
      assertEquals(5.0653533712428915, gisPoint.getY(), 1e-4);
      assertEquals(10.0, ((Double) load.getValue(0, idxGeom)).doubleValue(), 1e-4);
      assertEquals(1, ((Integer) load.getValue(1, idxGeom)).intValue());

      idxGeom = 2;
      gisPoint = (GISPoint) load.getGeometry(idxGeom);
      assertEquals(8.519902518277823, gisPoint.getX(), 1e-4);
      assertEquals(4.509707554833469, gisPoint.getY(), 1e-4);
      assertEquals(21.0, ((Double) load.getValue(0, idxGeom)).doubleValue(), 1e-4);
      assertEquals(2, ((Integer) load.getValue(1, idxGeom)).intValue());
    } catch (Exception ex) {
      Logger.getLogger(GMLMapDecoderTest.class.getName()).log(Level.SEVERE, null, ex);
      fail(ex.getMessage());
    }
  }

  public void testDecodePointWithFrictionIndexApplied() {
    try {

      GISZoneCollectionPoint load = (GISZoneCollectionPoint) load("pointsFrottementsIndexApplied.gml",true);
      int numGeometries = load.getNumGeometries();
      assertEquals(3, numGeometries);

      assertEquals(1, load.getNbAttributes());
      final GISAttributeInterface frictions = load.getAttribute(0);
      assertEquals("frottements", frictions.getName());
      assertEquals(Double.class, frictions.getDataClass());


      int idxGeom = 1;
      GISPoint gisPoint = (GISPoint) load.getGeometry(idxGeom);
      assertEquals(6.050365556458163, gisPoint.getX(), 1e-4);
      assertEquals(5.127091795288383, gisPoint.getY(), 1e-4);
      assertEquals(1.0, ((Double) load.getValue(0, idxGeom)).doubleValue(), 1e-4);

      idxGeom = 2;
      gisPoint = (GISPoint) load.getGeometry(idxGeom);
      assertEquals(7.828432168968317, gisPoint.getX(), 1e-4);
      assertEquals(5.0653533712428915, gisPoint.getY(), 1e-4);
      assertEquals(10.0, ((Double) load.getValue(0, idxGeom)).doubleValue(), 1e-4);

      idxGeom = 0;
      gisPoint = (GISPoint) load.getGeometry(idxGeom);
      assertEquals(8.519902518277823, gisPoint.getX(), 1e-4);
      assertEquals(4.509707554833469, gisPoint.getY(), 1e-4);
      assertEquals(21.0, ((Double) load.getValue(0, idxGeom)).doubleValue(), 1e-4);
    } catch (Exception ex) {
      Logger.getLogger(GMLMapDecoderTest.class.getName()).log(Level.SEVERE, null, ex);
      fail(ex.getMessage());
    }
  }

  public void testAtomicValues() {
    try {

      GISZoneCollectionLigneBrisee load = (GISZoneCollectionLigneBrisee) load("testAtomique.gml");
      int numGeometries = load.getNumGeometries();
      assertEquals(1, numGeometries);

      assertEquals(2, load.getNbAttributes());

      final GISAttributeInterface labelAttribute = load.getAttribute(0);
      assertEquals("Label", labelAttribute.getName());
      assertEquals(String.class, labelAttribute.getDataClass());
      assertTrue(labelAttribute.isAtomicValue());

      final GISAttributeInterface index = load.getAttribute(1);
      assertEquals("ATTRIBUTE_INDEX_GEOM", index.getName());
      assertEquals(Integer.class, index.getDataClass());

      int idxGeom = 0;
      GISLigneBrisee line = (GISLigneBrisee) load.getGeometry(idxGeom);
      assertEquals(5, line.getCoordinateSequence().size());
      assertTrue(line.isClosed());
      assertEquals(3.7169260045509422, line.getCoordinateSequence().getX(0), 1e-4);
      assertEquals(4.365131466322834, line.getCoordinateSequence().getY(0), 1e-4);

      assertEquals(5.338429431221977, line.getCoordinateSequence().getX(2), 1e-4);
      assertEquals(5.377047138606262, line.getCoordinateSequence().getY(2), 1e-4);

      final GISAttributeModelObjectArray value = (GISAttributeModelObjectArray) load.getValue(0, 0);
      assertEquals("hello1", value.getValue(0));
      assertEquals("hello2", value.getValue(1));
      assertEquals("hello3", value.getValue(2));
      assertEquals("hello4", value.getValue(3));
    } catch (Exception ex) {
      Logger.getLogger(GMLMapDecoderTest.class.getName()).log(Level.SEVERE, null, ex);
      fail(ex.getMessage());
    }
  }
}
