/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import junit.framework.TestCase;
import org.fudaa.ctulu.gis.GisAbscCurviligneToCoordinate.Result;

/**
 *
 * @author deniger ( genesis)
 */
public class GisAbscCurviligneToCoordinateTest extends TestCase {

  public void testFindCoordinate() {
    Coordinate c1 = new Coordinate(100, 100);
    Coordinate c2 = new Coordinate(200, 200);
    Coordinate c3 = new Coordinate(300, 300);
    Coordinate c4 = new Coordinate(400, 400);
    CoordinateSequence create = GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(new Coordinate[]{c1, c2, c3, c4});
    Result find = GisAbscCurviligneToCoordinate.find(create, 0);
    assertEquals(0, find.getSegmentidx());
    assertTrue(c1.equals2D(find.getCoordinate()));
    GisAbscCurviligneToCoordinate.find(create, 1, find);
    assertEquals(2, find.getSegmentidx());
    assertTrue(c4.equals2D(find.getCoordinate()));
    GisAbscCurviligneToCoordinate.find(create, 0.5, find);
    assertEquals(1, find.getSegmentidx());
    assertTrue(new Coordinate(250, 250).equals2D(find.getCoordinate()));
    GisAbscCurviligneToCoordinate.find(create, 0.1, find);
    assertEquals(0, find.getSegmentidx());
    assertTrue(new Coordinate(130, 130).equals2D(find.getCoordinate()));


  }
}
