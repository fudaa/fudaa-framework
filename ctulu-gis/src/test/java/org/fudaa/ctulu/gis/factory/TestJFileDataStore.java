/*
 *  @creation     28 sept. 2005
 *  @modification $Date: 2007-01-17 10:45:27 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gis.factory;

import junit.framework.TestCase;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.geotools.data.*;
import org.geotools.data.geojson.store.GeoJSONDataStoreFactory;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.feature.type.BasicFeatureTypes;
import org.locationtech.jts.geom.*;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

/**
 * @author fred deniger
 * @version $Id: TestJFileDataStore.java,v 1.1 2007-01-17 10:45:27 deniger Exp $
 */
public class TestJFileDataStore extends TestCase {
  private static void createFrElementType(SimpleFeatureTypeBuilder builder) {
    builder.add(BasicFeatureTypes.GEOMETRY_ATTRIBUTE_NAME, MultiLineString.class);
  }

  private static void createFrictionType(SimpleFeatureTypeBuilder builder) {
    builder.add("Friction", Double.class);
  }

  public void testShapeFile() {
    goTest(new ShapefileDataStoreFactory(), ".shp");
  }

  public void testGeoJson() {
    goTest(new GeoJSONDataStoreFactory(), ".geojson");
  }

  private void goTest(final DataStoreFactorySpi store, final String fileExtension) {
    File dest;
    try {
      dest = testLineWrite(store, fileExtension, false);
      // lecture
      readFile(dest, store, false);
      if (dest != null) {
        Files.delete(dest.toPath());
      }
    } catch (final Exception _evt) {
      _evt.printStackTrace();
      fail();
    }
    try {

      dest = testLineWrite(store, fileExtension, true);
      // lecture
      readFile(dest, store, true);
    } catch (final Exception _evt) {
      _evt.printStackTrace();
      fail();
    }
  }

  private static File testLineWrite(final DataStoreFactorySpi factorySpi, final String fileExtension, final boolean useDoubleValue) throws IOException {
    final File dest = File.createTempFile("fudaa", fileExtension);
    dest.delete();
    final SimpleFeatureType featureType = createFeatureType(useDoubleValue);
    assertNotNull(dest);
    final FeatureCollection fc = createLineString(featureType, useDoubleValue);
    final DataStore store = createDataStore(factorySpi, dest);

    store.createSchema(featureType);
    final FeatureSource source = store.getFeatureSource(store.getTypeNames()[0]);
    if (!(source instanceof FeatureStore)) {
      throw new IOException("Le fichier n'est pas accessible en �criture");
    }
    final FeatureStore featStore = (FeatureStore) source;
    final Transaction t = featStore.getTransaction();

    featStore.addFeatures(fc);
    t.commit();
    t.close();
    // lecture
    return dest;
  }

  private static DataStore createDataStore(DataStoreFactorySpi factorySpi, File dest) throws IOException {
    Map<String, Serializable> params = new HashMap<>();
    params.put(ShapefileDataStoreFactory.URLP.key, dest.toURI().toURL());
    return factorySpi.createDataStore(params);
  }

  private static SimpleFeatureType createFeatureType(final boolean _double) {
    SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
    createFrElementType(builder);
    if (_double) {
      createFrictionType(builder);
    }
    builder.setName("mesh");
    return builder.buildFeatureType();
  }

  private static FeatureCollection createLineString(final SimpleFeatureType featureType, final boolean addDoubleValue) {
    // ecriture
    final LineString ls = createLinearString();
    DefaultFeatureCollection collection = new DefaultFeatureCollection();

    if (addDoubleValue) {
      collection.add(SimpleFeatureBuilder.build(featureType, new Object[]{ls, (double) getDoubleValue()}, null));
    } else {
      collection.add(SimpleFeatureBuilder.build(featureType, new LineString[]{ls}, null));
    }
    return collection;
  }

  private static int getDoubleValue() {
    return 23;
  }

  private static LineString createLinearString() {
    final Coordinate[] c = new Coordinate[5];
    c[0] = new Coordinate(0, 0, 100);
    c[1] = new Coordinate(10, 0, 100);
    c[2] = new Coordinate(10, 10, 100);
    c[3] = new Coordinate(0, 10, 100);
    c[4] = c[0];

    final GeometryFactory geometryFactory = GISGeometryFactory.INSTANCE;

    return geometryFactory.createLinearRing(c);
  }

  private static void readFile(final File file, final DataStoreFactorySpi factorySpi, final boolean useDoubleValue) throws Exception {
    final DataStore dataStore = createDataStore(factorySpi, file);
    final String[] name = dataStore.getTypeNames();
    final SimpleFeatureSource src = dataStore.getFeatureSource(name[0]);
    assertNotNull(src);
    final SimpleFeatureType features = src.getSchema();
    assertNotNull(features);
    assertTrue(features.getAttributeCount() >= 1);
    if (useDoubleValue) {
      assertEquals(2, features.getAttributeCount());
    }
    // final AttributeType ti = features.getAttributeType(0);
    try (final SimpleFeatureIterator it = src.getFeatures().features()) {
      assertTrue(it.hasNext());
      final SimpleFeature readFeature = it.next();
      assertFalse(it.hasNext());
      assertNotNull(readFeature);
      final Geometry g = (Geometry) readFeature.getDefaultGeometry();
      assertTrue("Must be LineString but read " + g.getClass(), g instanceof MultiLineString);
      LineString lineString = (LineString) g.getGeometryN(0);

      if (useDoubleValue) {
        final AttributeDescriptor descriptor = readFeature.getFeatureType().getDescriptor("Friction");
        assertEquals(Double.class, descriptor.getType().getBinding());
        assertEquals("Friction", descriptor.getName().getLocalPart());
//        final Object o = readFeature.getAttribute(t.getName());
        final Object o = readFeature.getAttribute(descriptor.getName());
        assertNotNull(o);
        assertTrue(o instanceof Double);
        assertEquals(getDoubleValue(), (Double) o, 1E-10);
      }

      assertEquals(5, lineString.getNumPoints());
      assertTrue(lineString.isClosed());
    }
  }
}
