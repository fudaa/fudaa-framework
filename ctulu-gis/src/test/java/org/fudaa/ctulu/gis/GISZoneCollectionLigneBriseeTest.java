/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import junit.framework.TestCase;

/**
 *
 * @author deniger ( genesis)
 */
public class GISZoneCollectionLigneBriseeTest extends TestCase {
  
  public void testClone() {
    GISZoneCollectionLigneBrisee ligne = new GISZoneCollectionLigneBrisee();
    //NATURE is non atomic
    //BATH is atomic
    ligne.setAttributes(new GISAttributeInterface[]{GISAttributeConstants.NATURE, GISAttributeConstants.BATHY}, null);
    ligne.setAttributeIsZ(GISAttributeConstants.BATHY);
    CoordinateSequence line1 = GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(new Coordinate[]{new Coordinate(
              10, 20), new Coordinate(20,
                                      30)});
    String line1Nature = "Nat1";
    Double[] line1Z = new Double[]{0d, 1d};
    
    CoordinateSequence line2 = GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(new Coordinate[]{new Coordinate(
              100, 200), new Coordinate(200,
                                        300)});
    String line2Nature = "Nat2";
    Double[] line2Z = new Double[]{10d, 20d};
    
    ligne.addCoordinateSequence(line1, new Object[]{line1Nature, line1Z}, null);
    ligne.addCoordinateSequence(line2, new Object[]{line2Nature, line2Z}, null);
    assertEquals(2, ligne.getNumGeometries());
    assertEquals("Nat1", ligne.getValue(0, 0));
    GISAttributeModelDoubleArray value = (GISAttributeModelDoubleArray) ligne.getValue(1, 1);
    assertEquals(line2Z[0], value.getValue(0));
    assertEquals(line2Z[1], value.getValue(1));
    
    GISZoneCollectionLigneBrisee clonedLigne = (GISZoneCollectionLigneBrisee) ligne.clone();
    assertTrue(clonedLigne.getAttributeIsZ() == GISAttributeConstants.BATHY);
    assertEquals(clonedLigne.getNbGeometries(), ligne.getNbGeometries());
    assertFalse(clonedLigne.getGeometry(0) == ligne.getGeometry(0));
    assertTrue(clonedLigne.getGeometry(0).equals(ligne.getGeometry(0)));
    assertTrue(clonedLigne.getGeometry(1).equals(ligne.getGeometry(1)));
    assertEquals(clonedLigne.getAttributes().length, ligne.getAttributes().length);
    for (int i = 0; i < clonedLigne.getAttributes().length; i++) {
      assertEquals(clonedLigne.getAttributes()[i], ligne.getAttributes()[i]);
    }
    assertEquals("Nat1", clonedLigne.getValue(0, 0));
    value = (GISAttributeModelDoubleArray) clonedLigne.getValue(1, 1);
    assertEquals(line2Z[0], value.getValue(0));
    assertEquals(line2Z[1], value.getValue(1));

    //nouvelles références:
    assertFalse(clonedLigne.getModel(0) == ligne.getModel(0));
    assertFalse(clonedLigne.getModel(1) == ligne.getModel(1));
    final GISAttributeModel model = clonedLigne.getModel(GISAttributeConstants.BATHY);
    Object clonedModel = model.getObjectValueAt(0);
    assertTrue(clonedModel instanceof GISAttributeModelDoubleArray);
    clonedModel = model.getObjectValueAt(1);
    assertTrue(clonedModel instanceof GISAttributeModelDoubleArray);
    assertEquals(20d, ((GISAttributeModelDoubleArray) clonedModel).getValue(1),1e-15);
    assertFalse(clonedModel == ligne.getModel(GISAttributeConstants.BATHY).getObjectValueAt(0));
  }
}
