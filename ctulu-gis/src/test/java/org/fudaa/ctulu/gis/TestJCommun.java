/**
 * @creation 27 ao�t 2003
 * @modification $Date: 2007-01-17 10:45:25 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gis;

import gnu.trove.TDoubleArrayList;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandCompositeInverse;
import org.fudaa.ctulu.CtuluCommandManager;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluRange;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluArrayInteger;
import org.fudaa.ctulu.collection.CtuluListDouble;
import org.fudaa.ctulu.collection.CtuluListInteger;

/**
 * @author deniger
 * @version $Id: TestJCommun.java,v 1.9 2007-01-17 10:45:25 deniger Exp $
 */
public class TestJCommun extends TestCase {

  public TestJCommun() {
    super();
  }
  
  public void testAireTriangle() {
    double aireTriangle = CtuluLibGeometrie.aireTriangle(0, 0, 2, 0, 1, 1);
    assertEquals(1, aireTriangle, 1e-5);
  }

  public void testAttributeWrite() {
    final String s = GISAttributeConstants.toString(GISAttributeConstants.BATHY);
    assertNotNull(s);
    GISAttributeInterface res = GISAttributeConstants.restoreFrom(s);
    assertNotNull(res);
    assertTrue(res instanceof GISAttributeDouble);
    assertTrue(res.isAtomicValue());
    assertEquals("z", res.getName());
    assertEquals(null, res.getUserData());
    assertEquals(res, GISAttributeConstants.BATHY);
    assertTrue(res == GISAttributeConstants.BATHY);
    res = GISAttributeConstants.restoreFrom(GISAttributeConstants.toString(GISAttributeConstants.TITRE));
    assertTrue(res instanceof GISAttributeString);
    assertEquals(res.getDataClass(), GISAttributeConstants.TITRE.getDataClass());
    assertEquals(res.isAtomicValue(), GISAttributeConstants.TITRE.isAtomicValue());
    assertEquals(GISAttributeConstants.TITRE.getName(), res.getName());
    assertEquals(GISAttributeConstants.TITRE.getUserData(), res.getUserData());
    assertTrue(res == GISAttributeConstants.TITRE);
    testTranslate(new GISAttributeInteger("toto", false));
    testTranslate(new GISAttributeInteger("toto", true));
    testTranslate(new GISAttributeString("toto string", false));
    testTranslate(new GISAttributeString("toto string", true));
    testTranslate(new GISAttributeDouble("toto double", false));
    testTranslate(new GISAttributeDouble("toto double", true));
  }

  protected void testTranslate(final GISAttributeInterface _att) {
    final String toString = GISAttributeConstants.toString(_att);
    assertEquals(toString, _att.toString());
    assertEquals(GISAttributeConstants.restoreFrom(toString), _att);
  }

  /**
   * Compare avec une precision de 1E-15.
   * 
   * @param _d le premier double
   * @param _d1 le second
   */
  public void assertDoubleEquals(final double _d, final double _d1) {
    assertEquals(_d, _d1, 1E-15);
  }

  public void testSetAttributes() {
    final GISZoneCollectionPolygone polygones = new GISZoneCollectionPolygone();
    final CtuluCommandManager cmd = new CtuluCommandManager();
    polygones.addPolygone(GISGeometryFactory.INSTANCE.createLinearRing(0, 10, 1, 10), cmd);
    assertEquals(1, polygones.getNumGeometries());
    cmd.undo();
    assertEquals(0, polygones.getNumGeometries());
    cmd.redo();
    assertEquals(1, polygones.getNumGeometries());
    final GISPolygone pol = (GISPolygone) polygones.getGeometry(0);
    assertNotNull(pol);
    assertEquals(pol.getNumPoints(), 5);
    final double eps = 1E-15;
    assertEquals(0, pol.getCoordinateSequence().getX(0), eps);
    assertEquals(1, pol.getCoordinateSequence().getY(0), eps);
    assertEquals(0, pol.getCoordinateSequence().getX(4), eps);
    assertEquals(1, pol.getCoordinateSequence().getY(4), eps);
    assertEquals(10, pol.getCoordinateSequence().getX(1), eps);
    assertEquals(1, pol.getCoordinateSequence().getY(1), eps);
    assertEquals(10, pol.getCoordinateSequence().getX(2), eps);
    assertEquals(10, pol.getCoordinateSequence().getY(2), eps);
    assertEquals(0, pol.getCoordinateSequence().getX(3), eps);
    assertEquals(10, pol.getCoordinateSequence().getY(3), eps);
    assertEquals(0, polygones.getNbAttributes());
    polygones.setAttributes(new GISAttribute[] { GISAttributeConstants.BATHY }, cmd);
    assertEquals(1, polygones.getNbAttributes());
    assertNotNull(polygones.getAttribute(0));
    GISAttributeModel model = polygones.getDataModel(0);
    assertEquals(1, model.getSize());
    assertTrue(model instanceof GISAttributeModelObjectList);
    final GISAttributeModelObjectList obj = (GISAttributeModelObjectList) model;
    final Object array = obj.getObjectValueAt(0);
    assertNotNull(array);
    assertTrue(array instanceof GISAttributeModelDoubleArray);
    final GISAttributeModelDoubleArray doubleArray = (GISAttributeModelDoubleArray) array;
    assertEquals(5, doubleArray.getSize());
    doubleArray.set(0, 15);
    cmd.undo();
    assertEquals(0, polygones.getNbAttributes());
    assertNull(polygones.getAttribute(0));
    cmd.redo();
    assertEquals(1, polygones.getNbAttributes());
    assertNotNull(polygones.getAttribute(0));
    model = polygones.getDataModel(0);
    testBathyDoubleArray(model);
    polygones.setAttributes(new GISAttribute[] { GISAttributeConstants.TITRE, GISAttributeConstants.BATHY }, cmd);
    assertEquals(2, polygones.getNbAttributes());
    assertNotNull(polygones.getAttribute(0));
    assertNotNull(polygones.getAttribute(1));
    model = polygones.getDataModel(1);
    testBathyDoubleArray(model);
    cmd.undo();
    assertEquals(1, polygones.getNbAttributes());
    assertNotNull(polygones.getAttribute(0));
    model = polygones.getDataModel(0);
    testBathyDoubleArray(model);
  }

  private void testBathyDoubleArray(final GISAttributeModel _model) {
    assertEquals(1, _model.getSize());
    assertTrue(_model instanceof GISAttributeModelObjectList);
    final GISAttributeModelObjectList obj = (GISAttributeModelObjectList) _model;
    final Object array = obj.getObjectValueAt(0);
    assertNotNull(array);
    assertTrue(array instanceof GISAttributeModelDoubleArray);
    final GISAttributeModelDoubleArray doubleArray = (GISAttributeModelDoubleArray) array;
    assertEquals(5, doubleArray.getSize());
    assertEquals(15, doubleArray.getValue(0), 1E-15);

  }

  public void testSet() {
    final double[] initValues = new double[] { 5, 5, 6, 6 };
    final CtuluListDouble listDouble = new CtuluListDouble(initValues);
    final CtuluArrayDouble arrayDouble = new CtuluArrayDouble(initValues);
    assertEquals(listDouble.getSize(), 4);
    assertEquals(arrayDouble.getSize(), 4);
    CtuluCommandComposite cmp = new CtuluCommandComposite();
    listDouble.set(1, 2, cmp);
    assertDoubleEquals(listDouble.getValue(1), 2);
    cmp.undo();
    assertDoubleEquals(listDouble.getValue(1), 5);
    cmp.redo();
    assertDoubleEquals(listDouble.getValue(1), 2);
    cmp.undo();
    assertDoubleEquals(listDouble.getValue(1), 5);
    cmp = new CtuluCommandComposite();
    arrayDouble.set(1, 2, cmp);
    assertDoubleEquals(arrayDouble.getValue(1), 2);
    cmp.undo();
    assertDoubleEquals(arrayDouble.getValue(1), 5);
    cmp.redo();
    assertDoubleEquals(arrayDouble.getValue(1), 2);
    cmp.undo();
    assertDoubleEquals(arrayDouble.getValue(1), 5);
    cmp = new CtuluCommandComposite();
    listDouble.set(new int[] { 1, 2 }, 6, cmp);
    assertDoubleEquals(listDouble.getValue(1), 6);
    assertDoubleEquals(listDouble.getValue(2), 6);
    cmp.undo();
    assertDoubleEquals(listDouble.getValue(1), 5);
    assertDoubleEquals(listDouble.getValue(2), 6);
    cmp.redo();
    assertDoubleEquals(listDouble.getValue(1), 6);
    assertDoubleEquals(listDouble.getValue(2), 6);
    cmp.undo();
    cmp = new CtuluCommandComposite();
    arrayDouble.set(new int[] { 1, 2 }, 6, cmp);
    assertDoubleEquals(arrayDouble.getValue(1), 6);
    assertDoubleEquals(arrayDouble.getValue(2), 6);
    cmp.undo();
    assertDoubleEquals(arrayDouble.getValue(1), 5);
    assertDoubleEquals(arrayDouble.getValue(2), 6);
    cmp.redo();
    assertDoubleEquals(arrayDouble.getValue(1), 6);
    assertDoubleEquals(arrayDouble.getValue(2), 6);
    cmp.undo();
    // set int double
    cmp = new CtuluCommandComposite();
    listDouble.set(new int[] { 1, 2 }, new double[] { 6, 8 }, cmp);
    assertDoubleEquals(listDouble.getValue(1), 6);
    assertDoubleEquals(listDouble.getValue(2), 8);
    cmp.undo();
    assertDoubleEquals(listDouble.getValue(1), 5);
    assertDoubleEquals(listDouble.getValue(2), 6);
    cmp.redo();
    assertDoubleEquals(listDouble.getValue(1), 6);
    assertDoubleEquals(listDouble.getValue(2), 8);
    cmp = new CtuluCommandComposite();
    arrayDouble.set(new int[] { 1, 2 }, new double[] { 6, 8 }, cmp);
    assertDoubleEquals(arrayDouble.getValue(1), 6);
    assertDoubleEquals(arrayDouble.getValue(2), 8);
    cmp.undo();
    assertDoubleEquals(arrayDouble.getValue(1), 5);
    assertDoubleEquals(arrayDouble.getValue(2), 6);
    cmp.redo();
    assertDoubleEquals(arrayDouble.getValue(1), 6);
    assertDoubleEquals(arrayDouble.getValue(2), 8);

  }

  public void testSetInt() {
    final int[] initValues = new int[] { 5, 5, 6, 6 };
    final CtuluListInteger list = new CtuluListInteger(initValues);
    final CtuluArrayInteger array = new CtuluArrayInteger(initValues);
    assertEquals(list.getSize(), 4);
    assertEquals(array.getSize(), 4);
    CtuluCommandComposite cmp = new CtuluCommandComposite();
    list.set(1, 2, cmp);
    assertDoubleEquals(list.getValue(1), 2);
    cmp.undo();
    assertDoubleEquals(list.getValue(1), 5);
    cmp.redo();
    assertDoubleEquals(list.getValue(1), 2);
    cmp.undo();
    assertDoubleEquals(list.getValue(1), 5);
    cmp = new CtuluCommandComposite();
    array.set(1, 2, cmp);
    assertDoubleEquals(array.getValue(1), 2);
    cmp.undo();
    assertDoubleEquals(array.getValue(1), 5);
    cmp.redo();
    assertDoubleEquals(array.getValue(1), 2);
    cmp.undo();
    assertDoubleEquals(array.getValue(1), 5);
    cmp = new CtuluCommandComposite();
    list.set(new int[] { 1, 2 }, 6, cmp);
    assertDoubleEquals(list.getValue(1), 6);
    assertDoubleEquals(list.getValue(2), 6);
    cmp.undo();
    assertDoubleEquals(list.getValue(1), 5);
    assertDoubleEquals(list.getValue(2), 6);
    cmp.redo();
    assertDoubleEquals(list.getValue(1), 6);
    assertDoubleEquals(list.getValue(2), 6);
    cmp.undo();
    cmp = new CtuluCommandComposite();
    array.set(new int[] { 1, 2 }, 6, cmp);
    assertDoubleEquals(array.getValue(1), 6);
    assertDoubleEquals(array.getValue(2), 6);
    cmp.undo();
    assertDoubleEquals(array.getValue(1), 5);
    assertDoubleEquals(array.getValue(2), 6);
    cmp.redo();
    assertDoubleEquals(array.getValue(1), 6);
    assertDoubleEquals(array.getValue(2), 6);
    cmp.undo();
    // set int double
    cmp = new CtuluCommandComposite();
    list.set(new int[] { 1, 2 }, new int[] { 6, 8 }, cmp);
    assertDoubleEquals(list.getValue(1), 6);
    assertDoubleEquals(list.getValue(2), 8);
    cmp.undo();
    assertDoubleEquals(list.getValue(1), 5);
    assertDoubleEquals(list.getValue(2), 6);
    cmp.redo();
    assertDoubleEquals(list.getValue(1), 6);
    assertDoubleEquals(list.getValue(2), 8);
    cmp = new CtuluCommandComposite();
    array.set(new int[] { 1, 2 }, new int[] { 6, 8 }, cmp);
    assertDoubleEquals(array.getValue(1), 6);
    assertDoubleEquals(array.getValue(2), 8);
    cmp.undo();
    assertDoubleEquals(array.getValue(1), 5);
    assertDoubleEquals(array.getValue(2), 6);
    cmp.redo();
    assertDoubleEquals(array.getValue(1), 6);
    assertDoubleEquals(array.getValue(2), 8);

  }

  public void testCtuluDoubleArrayModel() {
    final double[] di = new double[10];
    for (int i = 9; i >= 0; i--) {
      di[i] = i;
    }
    final CtuluArrayDouble mDouble = new CtuluArrayDouble(di);
    assertEquals(10, mDouble.getSize());
    for (int i = 9; i >= 0; i--) {
      assertDoubleEquals(di[i], mDouble.getValue(i));
    }
    CtuluRange r = new CtuluRange();
    mDouble.getRange(r);
    assertDoubleEquals(9, r.max_);
    assertDoubleEquals(0, r.min_);
    // test set
    CtuluCommandComposite c;
    final CtuluRange reelRange = new CtuluRange();
    // on test le set normal
    internTest(di, mDouble);

    // set sur 2 entier
    int[] idx = new int[2];
    idx[0] = 0;
    internTestSet(di, mDouble, idx, 9);
    // sur 4 entiers
    idx = new int[4];
    idx[0] = 0;
    idx[2] = 8;
    idx[3] = 9;
    internTestSet(di, mDouble, idx, 7);

    // test setAll
    final double[] newVals = new double[di.length];
    for (int j = di.length - 1; j >= 0; j--) {
      newVals[j] = (Math.random() - 1) * 100;
    }
    CtuluRange.getRangeFor(newVals, reelRange);
    c = new CtuluCommandComposite();
    assertTrue(mDouble.setAll(newVals, c));
    for (int j = di.length - 1; j >= 0; j--) {
      assertDoubleEquals(newVals[j], mDouble.getValue(j));
    }
    r = new CtuluRange();
    mDouble.getRange(r);
    assertDoubleEquals(reelRange.max_, r.max_);
    assertDoubleEquals(reelRange.min_, r.min_);
    c.undo();
    for (int j = di.length - 1; j >= 0; j--) {
      assertDoubleEquals(di[j], mDouble.getValue(j));
    }
    mDouble.getRange(r);
    assertDoubleEquals(9, r.max_);
    assertDoubleEquals(0, r.min_);
    c.redo();
    for (int j = di.length - 1; j >= 0; j--) {
      assertDoubleEquals(newVals[j], mDouble.getValue(j));
    }
    mDouble.getRange(r);
    assertDoubleEquals(reelRange.max_, r.max_);
    assertDoubleEquals(reelRange.min_, r.min_);
  }

  private void internTestSet(final double[] _di, final CtuluArrayDouble _mDouble, final int[] _idx,
      final int _nbIteration) {
    CtuluCommandComposite c;

    double[] valSet;
    for (int i = _nbIteration; i >= 1; i--) {
      _idx[1] = i;

      valSet = new double[_idx.length];
      for (int j = _idx.length - 1; j >= 0; j--) {
        valSet[j] = (Math.random() - 1) * 100;
      }
      final double[] vals = CtuluLibArray.copy(_di);
      for (int j = _idx.length - 1; j >= 0; j--) {
        vals[_idx[j]] = valSet[j];
      }
      c = new CtuluCommandComposite();
      assertTrue(_mDouble.set(_idx, valSet, c));
      interntestRange(_di, _mDouble, _nbIteration, c, vals);
    }
  }

  private void interntestRange(final double[] _di, final CtuluArrayDouble _mDouble, final int _nbIteration,
      final CtuluCommandComposite _c, final double[] _vals) {
    for (int j = _nbIteration; j >= 0; j--) {
      assertDoubleEquals(_vals[j], _mDouble.getValue(j));
    }
    final CtuluRange r = new CtuluRange();
    final CtuluRange reelRange = new CtuluRange();
    _mDouble.getRange(r);
    CtuluRange.getRangeFor(_vals, reelRange);
    assertDoubleEquals(reelRange.max_, r.max_);
    assertDoubleEquals(reelRange.min_, r.min_);
    _c.undo();
    for (int j = 9; j >= 0; j--) {
      assertDoubleEquals(_di[j], _mDouble.getValue(j));
    }
    _mDouble.getRange(r);
    assertDoubleEquals(9, r.max_);
    assertDoubleEquals(0, r.min_);
    _c.redo();
    for (int j = 9; j >= 0; j--) {
      assertDoubleEquals(_vals[j], _mDouble.getValue(j));
    }
    _mDouble.getRange(r);
    assertDoubleEquals(reelRange.max_, r.max_);
    assertDoubleEquals(reelRange.min_, r.min_);
    _c.undo();
  }

  private void internTest(final double[] _di, final CtuluArrayDouble _mDouble) {
    double newVal;
    CtuluCommandComposite c;
    for (int i = 9; i >= 0; i--) {
      c = new CtuluCommandComposite();
      newVal = (Math.random() - 1) * 100;
      assertTrue(_mDouble.set(i, newVal, c));
      final double[] vals = CtuluLibArray.copy(_di);
      vals[i] = newVal;
      interntestRange(_di, _mDouble, 9, c, vals);
    }
  }

  /**
   * Test CtuluDoubleModel.
   * 
   * @see CtuluListDouble
   */
  public void testCtuluListDoubleModelRange() {
    final CtuluListDouble m = new CtuluListDouble();
    assertEquals(0, m.getSize());
    CtuluCommandCompositeInverse c = new CtuluCommandCompositeInverse();
    final CtuluRange r = new CtuluRange();
    m.getRange(r);
    assertDoubleEquals(0, r.min_);
    assertDoubleEquals(0, r.max_);
    m.add(5, c);
    m.getRange(r);
    assertDoubleEquals(5, r.max_);
    assertDoubleEquals(5, r.min_);
    assertEquals(1, m.getSize());
    assertDoubleEquals(5, m.getValue(0));
    c.undo();
    assertEquals(0, m.getSize());
    m.getRange(r);
    assertDoubleEquals(0, r.min_);
    assertDoubleEquals(0, r.max_);
    c.redo();
    m.getRange(r);
    assertDoubleEquals(5, r.max_);
    assertDoubleEquals(5, r.min_);
    assertEquals(1, m.getSize());
    assertDoubleEquals(5, m.getValue(0));
    c = new CtuluCommandCompositeInverse();
    m.add(6, c);
    assertEquals(2, m.getSize());
    assertDoubleEquals(6, m.getValue(1));
    m.getRange(r);
    assertDoubleEquals(6, r.max_);
    assertDoubleEquals(5, r.min_);
    c.undo();
    m.getRange(r);
    assertDoubleEquals(5, r.max_);
    assertDoubleEquals(5, r.min_);
    c.redo();
    m.getRange(r);
    assertDoubleEquals(6, r.max_);
    assertDoubleEquals(5, r.min_);
    c = new CtuluCommandCompositeInverse();
    m.insert(1, 5.5, c);
    assertEquals(3, m.getSize());
    assertDoubleEquals(5, m.getValue(0));
    assertDoubleEquals(5.5, m.getValue(1));
    assertDoubleEquals(6, m.getValue(2));
    c.undo();
    assertEquals(2, m.getSize());
    assertDoubleEquals(5, m.getValue(0));
    assertDoubleEquals(6, m.getValue(1));
    c.redo();
    assertEquals(3, m.getSize());
    assertDoubleEquals(5, m.getValue(0));
    assertDoubleEquals(5.5, m.getValue(1));
    assertDoubleEquals(6, m.getValue(2));
    m.insert(4, 5.5);
    assertEquals(3, m.getSize());
  }

  public void testCtuluListDoubleModel() {
    final CtuluListDouble m = new CtuluListDouble();
    final CtuluRange r = new CtuluRange();
    m.add(5);
    m.add(5.5);
    m.add(6);
    CtuluCommandCompositeInverse c = new CtuluCommandCompositeInverse();
    m.remove(1, c);
    assertEquals(2, m.getSize());
    assertDoubleEquals(5, m.getValue(0));
    assertDoubleEquals(6, m.getValue(1));
    c.undo();
    assertEquals(3, m.getSize());
    assertDoubleEquals(5, m.getValue(0));
    assertDoubleEquals(5.5, m.getValue(1));
    assertDoubleEquals(6, m.getValue(2));
    c.redo();
    assertEquals(2, m.getSize());
    assertDoubleEquals(5, m.getValue(0));
    assertDoubleEquals(6, m.getValue(1));
    c.undo();
    c = new CtuluCommandCompositeInverse();
    m.remove(0, c);
    assertEquals(2, m.getSize());
    assertDoubleEquals(5.5, m.getValue(0));
    assertDoubleEquals(6, m.getValue(1));
    m.getRange(r);
    assertDoubleEquals(6, r.max_);
    assertDoubleEquals(5.5, r.min_);
    c.undo();
    assertEquals(3, m.getSize());
    assertDoubleEquals(5, m.getValue(0));
    assertDoubleEquals(5.5, m.getValue(1));
    assertDoubleEquals(6, m.getValue(2));
    m.getRange(r);
    assertDoubleEquals(6, r.max_);
    assertDoubleEquals(5, r.min_);
    c = new CtuluCommandCompositeInverse();
    m.remove(2, c);
    assertEquals(2, m.getSize());
    assertDoubleEquals(5, m.getValue(0));
    assertDoubleEquals(5.5, m.getValue(1));
    c.undo();
    assertEquals(3, m.getSize());
    assertDoubleEquals(5, m.getValue(0));
    assertDoubleEquals(5.5, m.getValue(1));
    assertDoubleEquals(6, m.getValue(2));
    m.getRange(r);
    assertDoubleEquals(6, r.max_);
    assertDoubleEquals(5, r.min_);
    m.add(2);
    m.getRange(r);
    assertDoubleEquals(6, r.max_);
    assertDoubleEquals(2, r.min_);
    // 5 , 5.5 , 6 , 2
    c = new CtuluCommandCompositeInverse();
    m.set(3, -1, c);
    assertEquals(4, m.getSize());
    assertDoubleEquals(5, m.getValue(0));
    assertDoubleEquals(5.5, m.getValue(1));
    assertDoubleEquals(6, m.getValue(2));
    assertDoubleEquals(-1, m.getValue(3));
    m.getRange(r);
    assertDoubleEquals(6, r.max_);
    assertDoubleEquals(-1, r.min_);
    c.undo();
    assertEquals(4, m.getSize());
    assertDoubleEquals(5, m.getValue(0));
    assertDoubleEquals(5.5, m.getValue(1));
    assertDoubleEquals(6, m.getValue(2));
    assertDoubleEquals(2, m.getValue(3));
    m.getRange(r);
    assertDoubleEquals(6, r.max_);
    assertDoubleEquals(2, r.min_);
    c.redo();
    assertEquals(4, m.getSize());
    assertDoubleEquals(5, m.getValue(0));
    assertDoubleEquals(5.5, m.getValue(1));
    assertDoubleEquals(6, m.getValue(2));
    assertDoubleEquals(-1, m.getValue(3));
    m.getRange(r);
    assertDoubleEquals(6, r.max_);
    assertDoubleEquals(-1, r.min_);
    // 5 , 5.5 , 6 , -1
    c = new CtuluCommandCompositeInverse();
    m.set(2, 15, c);
    // 5 , 5.5 , 15 , -1
    assertEquals(4, m.getSize());
    assertDoubleEquals(5, m.getValue(0));
    assertDoubleEquals(5.5, m.getValue(1));
    assertDoubleEquals(15, m.getValue(2));
    assertDoubleEquals(-1, m.getValue(3));
    m.getRange(r);
    assertDoubleEquals(15, r.max_);
    assertDoubleEquals(-1, r.min_);
    c.undo();
    m.getRange(r);
    assertDoubleEquals(6, r.max_);
    assertDoubleEquals(-1, r.min_);
    // 5 , 5.5 , 6 , -1
    c = new CtuluCommandCompositeInverse();
    m.set(new int[] { 0, 1 }, new double[] { 1000, -35 }, c);
    // 1000, -35 , 6 , -1
    assertEquals(4, m.getSize());
    assertDoubleEquals(1000, m.getValue(0));
    assertDoubleEquals(-35, m.getValue(1));
    assertDoubleEquals(6, m.getValue(2));
    assertDoubleEquals(-1, m.getValue(3));
    m.getRange(r);
    assertDoubleEquals(1000, r.max_);
    assertDoubleEquals(-35, r.min_);
    c.undo();
    assertEquals(4, m.getSize());
    assertDoubleEquals(5, m.getValue(0));
    assertDoubleEquals(5.5, m.getValue(1));
    assertDoubleEquals(6, m.getValue(2));
    assertDoubleEquals(-1, m.getValue(3));
    m.getRange(r);
    assertDoubleEquals(6, r.max_);
    assertDoubleEquals(-1, r.min_);

    internTestDoubleModelRemove(false);
    internTestDoubleModelRemove(true);
  }

  /**
   * @param _memento true si le memento doit etre utilisee
   */
  protected void internTestDoubleModelRemove(final boolean _memento) {
    final CtuluListDouble m = new CtuluListDouble();
    m.add(1);
    m.add(5);
    m.add(-2);
    m.add(6);
    final CtuluRange r = new CtuluRange();
    m.getRange(r);
    assertDoubleEquals(-2, r.min_);
    assertDoubleEquals(6, r.max_);
    // 1,5,-2,6
    final int[] deux = new int[2];
    deux[0] = 0;
    deux[1] = 1;
    CtuluCommandComposite c = new CtuluCommandComposite();
    m.remove(deux, c, _memento);
    internTestDoubleDefaultModel(m, -2, 6, -2, 6);
    c.undo();
    internTestDoubleDefaultModel(m);
    c.redo();
    internTestDoubleDefaultModel(m, -2, 6, -2, 6);
    c.undo();
    deux[0] = 1;
    deux[1] = 2;
    // 1,6
    c = new CtuluCommandComposite();
    m.remove(deux, c, _memento);
    internTestDoubleDefaultModel(m, 1, 6, 1, 6);
    c.undo();
    internTestDoubleDefaultModel(m);
    c.redo();
    internTestDoubleDefaultModel(m, 1, 6, 1, 6);
    c.undo();
    deux[0] = 0;
    deux[1] = 3;
    // 5,-2
    c = new CtuluCommandComposite();
    m.remove(deux, c, _memento);
    internTestDoubleDefaultModel(m, 5, -2, -2, 5);
    c.undo();
    internTestDoubleDefaultModel(m);
    c.redo();
    internTestDoubleDefaultModel(m, 5, -2, -2, 5);
    c.undo();
    deux[0] = 2;
    deux[1] = 3;
    // 1,5
    c = new CtuluCommandComposite();
    m.remove(deux, c, _memento);
    internTestDoubleDefaultModel(m, 1, 5, 1, 5);
    c.undo();
    internTestDoubleDefaultModel(m);
    c.redo();
    internTestDoubleDefaultModel(m, 1, 5, 1, 5);
    c.undo();
  }

  /**
   * Un test par defaut.
   * 
   * @param _m
   * @param _v1
   * @param _v2
   * @param _min
   * @param _max
   */
  protected void internTestDoubleDefaultModel(final CtuluListDouble _m, final double _v1, final double _v2,
      final double _min, final double _max) {
    assertEquals(2, _m.getSize());
    assertDoubleEquals(_v1, _m.getValue(0));
    assertDoubleEquals(_v2, _m.getValue(1));
    final CtuluRange r = new CtuluRange();
    _m.getRange(r);
    assertDoubleEquals(_min, r.min_);
    assertDoubleEquals(_max, r.max_);
  }

  protected void internTestDoubleDefaultModel(final CtuluListDouble _m) {
    assertEquals(4, _m.getSize());
    assertDoubleEquals(1, _m.getValue(0));
    assertDoubleEquals(5, _m.getValue(1));
    assertDoubleEquals(-2, _m.getValue(2));
    assertDoubleEquals(6, _m.getValue(3));
    final CtuluRange r = new CtuluRange();
    _m.getRange(r);
    assertDoubleEquals(-2, r.min_);
    assertDoubleEquals(6, r.max_);
  }

  /**
   * test TDoubleArrayList.
   */
  public void testDoubleArrayList() {
    final TDoubleArrayList l = new TDoubleArrayList();
    l.add(3);
    l.add(4);
    l.add(5);
    l.add(6);
    assertEquals(6, l.max(), 1E-15);
    assertEquals(3, l.min(), 1E-15);
  }

  /**
   * Test la suppression d'indice d'une liste de double.
   */
  public void testRemoveIdx() {
    final TDoubleArrayList init = new TDoubleArrayList();
    init.add(0);
    init.add(1);
    init.add(2);
    init.add(3);
    init.add(4);
    init.add(5);
    init.add(6);
    init.add(7);
    final int[] idxToRemove = new int[] { 0, 2, 7 };
    final double[] old = new double[3];
    final boolean r = CtuluLib.removeIdx(init, idxToRemove, old);
    assertTrue(r);
    assertEquals(5, init.size());
    assertEquals(old[0], 0, 1e-15);
    assertEquals(init.getQuick(0), 1, 1e-15);
    assertEquals(old[1], 2, 1e-15);
    assertEquals(init.getQuick(1), 3, 1e-15);
    assertEquals(init.getQuick(2), 4, 1e-15);
    assertEquals(init.getQuick(3), 5, 1e-15);
    assertEquals(init.getQuick(4), 6, 1e-15);
    assertEquals(init.getQuick(4), 6, 1e-15);
    assertEquals(old[2], 7, 1e-15);
  }

}