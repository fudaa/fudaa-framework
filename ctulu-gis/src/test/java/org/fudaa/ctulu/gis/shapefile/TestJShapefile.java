/*
 *  @creation     28 sept. 2005
 *  @modification $Date: 2007-01-17 10:45:27 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gis.shapefile;

import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.geotools.data.DataStore;
import org.geotools.data.FeatureSource;
import org.geotools.data.FeatureStore;
import org.geotools.data.Transaction;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.feature.type.BasicFeatureTypes;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.MultiLineString;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeType;

import java.io.File;
import java.io.IOException;

/**
 * @author fred deniger
 * @version $Id: TestJShapefile.java,v 1.1 2007-01-17 10:45:27 deniger Exp $
 */
public class TestJShapefile extends TestCase {
  private static SimpleFeatureType createFrElementType() {
    SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
    builder.add(BasicFeatureTypes.GEOMETRY_ATTRIBUTE_NAME, MultiLineString.class);
    builder.setName("geom");
    builder.setNamespaceURI((String)null);
    return builder.buildFeatureType();
  }

  public void testLineString() {
    try {
      final File dest = testLineWrite();
      // lecture
      readFile(dest);
    } catch (final Exception _evt) {
      _evt.printStackTrace();
      fail();
    }
  }

  public static File testLineWrite() throws IOException {
    final File dest = File.createTempFile("fudaa", ".shp");
    assertNotNull(dest);
    // ecriture
    final LineString ls = GISGeometryFactory.INSTANCE.createLinearRing(0, 10, -20, 20);
    final ShapefileDataStoreFactory fac = new ShapefileDataStoreFactory();
    final DataStore store = fac.createDataStore(dest.toURI().toURL());
    final SimpleFeatureType featureType = createFrElementType();
    store.createSchema(featureType);
    String typeName = store.getTypeNames()[0];
    final FeatureSource source = store.getFeatureSource(typeName);

    if (!(source instanceof FeatureStore)) {
      throw new IOException(CtuluLib.getS("Le fichier n'est pas accessible en �criture"));
    }
    final FeatureStore featStore = (FeatureStore) source;
    final Transaction t = featStore.getTransaction();

    final DefaultFeatureCollection fc = new DefaultFeatureCollection();
    fc.add(SimpleFeatureBuilder.build(featureType, new Object[]{ls}, null));
    featStore.addFeatures(fc);
    t.commit();
    t.close();
    // lecture
    return dest;
  }

  private static void readFile(final File _f) throws Exception {
    final ShapefileDataStoreFactory fac = new ShapefileDataStoreFactory();
    final DataStore dataStore = fac.createDataStore(_f.toURI().toURL());
    final String[] name = dataStore.getTypeNames();
    final SimpleFeatureSource src = dataStore.getFeatureSource(name[0]);
    assertNotNull(src);
    final SimpleFeatureType features = src.getSchema();
    assertNotNull(features);
    assertEquals(1, features.getAttributeCount());
    final AttributeType attributeType = features.getType(0);
    try (final SimpleFeatureIterator it = src.getFeatures().features()) {
      assertTrue(it.hasNext());
      final SimpleFeature readFeature = it.next();
      assertFalse(it.hasNext());
      assertNotNull(readFeature);
      final Geometry g = (Geometry) readFeature.getDefaultGeometry();
      assertTrue(g instanceof MultiLineString);
      assertEquals(1, g.getNumGeometries());
      assertTrue(g.getGeometryN(0) instanceof LineString);
      final LineString r = (LineString) g.getGeometryN(0);
      assertEquals(5, r.getNumPoints());
      assertTrue(r.isClosed());
      _f.delete();
    }
  }
}
