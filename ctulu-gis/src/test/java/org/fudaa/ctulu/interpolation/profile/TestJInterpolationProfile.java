/*
 * @creation     23 sept. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.interpolation.profile;

import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISPoint;

/**
 * Test de l'interpolateur de profil. A partir d'un nuage de points et d'une trace 2D.
 * @author Bertrand Marchand
 * @version $Id$
 */
public class TestJInterpolationProfile extends TestCase {
  
  // Coordonn�es trace
  static double[][] ctrace={
    {427238.0, 245099.0, 0},
    {427229.6, 245019.8, 0},
    {427223.0, 244852.0, 0}
  };
  
  // Coordonn�es nuage de points
  static double[][] ccloud={
    {427209.348, 245104.277, 25.0},
    {427269.064, 245095.43, 25.0},
    {427212.297, 245076.262, 25.0},
    {427183.545, 245077.736, 25.0},
    {427292.655, 245077.736, 25.0},
    {427191.654, 245031.291, 25.0},
    {427288.969, 245020.232, 25.0},
    {427174.698, 244956.093, 23.0},
    {427276.436, 244963.465, 23.0},
    {427283.071, 244925.129, 23.0},
    {427173.961, 244913.333, 24.0},
    {427221.144, 244889.005, 24.0},
    {427209.348, 244868.362, 24.0},
    {427257.268, 244880.895, 24.0},
    {427265.378, 244851.406, 24.0},
    {427289.706, 244858.041, 24.0},
    {427157.741, 244851.406, 24.0},
    {427198.289, 244833.712, 24.0}
  };
  
  // Coordonn�es window
  static double[][] cwin={
    {427134.887, 244823.391, 0},
    {427324.356, 244823.391, 0},
    {427324.356, 245116.81, 0},
    {427134.887, 245116.81, 0}
  };

  /**
   * Lancement du test de calcul de profil.
   * @param args
   */
  public void testInterp() {
    // Un nuage de point.
    GISPoint[] ptscloud=new GISPoint[ccloud.length];
    for (int i=0; i<ptscloud.length; i++) {
      ptscloud[i]=(GISPoint)GISGeometryFactory.INSTANCE.createPoint(ccloud[i][0],ccloud[i][1],ccloud[i][2]);
    }
    PointCloudPointsAdapter cloud=new PointCloudPointsAdapter(ptscloud);
    
    // Une fenetre, rectangle, contenant le nuage de points.
    GISPoint[] ptswin=new GISPoint[cwin.length];
    for (int i=0; i<ptswin.length; i++) {
      ptswin[i]=(GISPoint)GISGeometryFactory.INSTANCE.createPoint(cwin[i][0],cwin[i][1],cwin[i][2]);
    }
    ProfileCalculatorWindow win=new ProfileCalculatorWindow(ptswin);
    
    // Le calculateur de profils
    ProfileCalculator pc=new ProfileCalculator();
    pc.setWindow(win);
    pc.setCloud(cloud);
    
    GISPoint[] trace;
    GISPoint[] prof;
    
    // Extraction pour une trace fix�e, a l'interieur de la fenetre de selection. 
    // A priori, le Z de la trace n'a pas d'importance.
    
    trace=new GISPoint[ctrace.length];
    for (int i=0; i<trace.length; i++) {
      trace[i]=(GISPoint)GISGeometryFactory.INSTANCE.createPoint(ctrace[i][0],ctrace[i][1],ctrace[i][2]);
    }
    pc.setTrace(trace);
    prof=pc.extractProfile(0,new CtuluAnalyze());
    
    for (int i = 0; i < prof.length; i++) {
      assertTrue(prof[i].getZ() >= 23 - 1.E6 && prof[i].getZ() <= 25 + 1.0E6);
    }
    
    // Extraction pour une trace calcul�e.
    
    pc.computeDefaultTrace();
    trace=pc.getTrace();
    prof = pc.extractProfile(1, new CtuluAnalyze());
    for (int i = 0; i < prof.length; i++) {
      assertTrue(prof[i].getZ() >= 23 - 1.E6 && prof[i].getZ() <= 25 + 1.0E6);
    }
  }
}
