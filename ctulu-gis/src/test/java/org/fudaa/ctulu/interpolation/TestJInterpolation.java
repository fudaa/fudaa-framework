/*
 * @creation 29 mai 07
 * @modification $Date: 2007-06-11 13:03:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

import gnu.trove.TDoubleArrayList;
import java.util.Arrays;
import java.util.List;
import junit.framework.TestCase;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.CtuluVariableDefault;
import org.fudaa.ctulu.collection.CtuluListDouble;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.interpolation.bilinear.InterpolatorBilinear;

/**
 * @author fred deniger
 * @version $Id: TestJInterpolation.java,v 1.2 2007-06-11 13:03:48 deniger Exp $
 */
public class TestJInterpolation extends TestCase {

  public void testToPolaire() {
    final double eps = 1E-15;
    TDoubleArrayList vxs = new TDoubleArrayList();
    TDoubleArrayList vys = new TDoubleArrayList();
    TDoubleArrayList rs = new TDoubleArrayList();
    TDoubleArrayList thetas = new TDoubleArrayList();

    double vx = 0;
    double vy = 0;
    double r = 0;
    double theta = 0;
    vxs.add(vx);
    vys.add(vy);
    rs.add(r);
    thetas.add(theta);
    assertEquals(r, InterpolationVectorContainer.getNorme(vx, vy), eps);
    assertEquals(theta, InterpolationVectorContainer.getTheta(vx, vy), eps);
    vx = 0;
    vy = 1;
    r = 1;
    theta = Math.PI / 2;
    vxs.add(vx);
    vys.add(vy);
    rs.add(r);
    thetas.add(theta);
    assertEquals(r, InterpolationVectorContainer.getNorme(vx, vy), eps);
    assertEquals(theta, InterpolationVectorContainer.getTheta(vx, vy), eps);
    vx = 0;
    vy = -1;
    r = 1;
    theta = -Math.PI / 2;
    vxs.add(vx);
    vys.add(vy);
    rs.add(r);
    thetas.add(theta);
    assertEquals(r, InterpolationVectorContainer.getNorme(vx, vy), eps);
    assertEquals(theta, InterpolationVectorContainer.getTheta(vx, vy), eps);
    vx = 0.5;
    vy = 0;
    r = 0.5;
    theta = 0;
    vxs.add(vx);
    vys.add(vy);
    rs.add(r);
    thetas.add(theta);
    assertEquals(r, InterpolationVectorContainer.getNorme(vx, vy), eps);
    assertEquals(theta, InterpolationVectorContainer.getTheta(vx, vy), eps);
    vx = -0.5;
    vy = 0;
    r = 0.5;
    theta = Math.PI;
    vxs.add(vx);
    vys.add(vy);
    rs.add(r);
    thetas.add(theta);
    assertEquals(r, InterpolationVectorContainer.getNorme(vx, vy), eps);
    assertEquals(theta, InterpolationVectorContainer.getTheta(vx, vy), eps);
    vx = 1;
    vy = 1;
    r = Math.sqrt(2);
    theta = Math.PI / 4;
    vxs.add(vx);
    vys.add(vy);
    rs.add(r);
    thetas.add(theta);
    assertEquals(r, InterpolationVectorContainer.getNorme(vx, vy), eps);
    assertEquals(theta, InterpolationVectorContainer.getTheta(vx, vy), eps);
    vx = -1;
    vy = 1;
    r = Math.sqrt(2);
    theta = 3 * Math.PI / 4;
    vxs.add(vx);
    vys.add(vy);
    rs.add(r);
    thetas.add(theta);
    assertEquals(r, InterpolationVectorContainer.getNorme(vx, vy), eps);
    assertEquals(theta, InterpolationVectorContainer.getTheta(vx, vy), eps);
    vx = 1;
    vy = -1;
    r = Math.sqrt(2);
    theta = -Math.PI / 4;
    vxs.add(vx);
    vys.add(vy);
    rs.add(r);
    thetas.add(theta);
    assertEquals(r, InterpolationVectorContainer.getNorme(vx, vy), eps);
    assertEquals(theta, InterpolationVectorContainer.getTheta(vx, vy), eps);
    vx = -1;
    vy = -1;
    r = Math.sqrt(2);
    theta = -3 * Math.PI / 4;
    vxs.add(vx);
    vys.add(vy);
    rs.add(r);
    thetas.add(theta);
    assertEquals(r, InterpolationVectorContainer.getNorme(vx, vy), eps);
    assertEquals(theta, InterpolationVectorContainer.getTheta(vx, vy), eps);
    vx = Math.sqrt(3) / 2;
    vy = 0.5;
    r = 1;
    theta = Math.PI / 6;
    vxs.add(vx);
    vys.add(vy);
    rs.add(r);
    thetas.add(theta);
    assertEquals(r, InterpolationVectorContainer.getNorme(vx, vy), eps);
    assertEquals(theta, InterpolationVectorContainer.getTheta(vx, vy), eps);
    CtuluListDouble listVx = new CtuluListDouble(vxs.toNativeArray());
    CtuluListDouble listVy = new CtuluListDouble(vys.toNativeArray());
    assertTrue(CtuluLibArray.isDoubleEquals(InterpolationVectorContainer.getNorme(listVx, listVy), rs.toNativeArray(),
        eps));
    assertTrue(CtuluLibArray.isDoubleEquals(InterpolationVectorContainer.getTheta(listVx, listVy), thetas
        .toNativeArray(), eps));
    CtuluListDouble listR = new CtuluListDouble(rs.toNativeArray());
    CtuluListDouble listTheta = new CtuluListDouble(thetas.toNativeArray());
    assertTrue(CtuluLibArray.isDoubleEquals(InterpolationVectorContainer.getVx(listR, listTheta), vxs.toNativeArray(),
        eps));
    assertTrue(CtuluLibArray.isDoubleEquals(InterpolationVectorContainer.getVy(listR, listTheta), vys.toNativeArray(),
        eps));
  }

  public void testToVxVy() {
    final double eps = 1E-15;
    double r = 0;
    double theta = 0;
    assertEquals(0, InterpolationVectorContainer.getVx(r, theta), eps);
    assertEquals(0, InterpolationVectorContainer.getVy(r, theta), eps);
    r = 1;
    theta = 0;
    assertEquals(1, InterpolationVectorContainer.getVx(r, theta), eps);
    assertEquals(0, InterpolationVectorContainer.getVy(r, theta), eps);
    theta = Math.PI;
    assertEquals(-1, InterpolationVectorContainer.getVx(r, theta), eps);
    assertEquals(0, InterpolationVectorContainer.getVy(r, theta), eps);
    theta = Math.PI / 2;
    assertEquals(0, InterpolationVectorContainer.getVx(r, theta), eps);
    assertEquals(1, InterpolationVectorContainer.getVy(r, theta), eps);
    theta = -Math.PI / 2;
    assertEquals(0, InterpolationVectorContainer.getVx(r, theta), eps);
    assertEquals(-1, InterpolationVectorContainer.getVy(r, theta), eps);
    double res = Math.sqrt(2) / 2;
    theta = Math.PI / 4;
    assertEquals(res, InterpolationVectorContainer.getVx(r, theta), eps);
    assertEquals(res, InterpolationVectorContainer.getVy(r, theta), eps);
    theta = -Math.PI / 4;
    assertEquals(res, InterpolationVectorContainer.getVx(r, theta), eps);
    assertEquals(-res, InterpolationVectorContainer.getVy(r, theta), eps);
    theta = -3 * Math.PI / 4;
    assertEquals(-res, InterpolationVectorContainer.getVx(r, theta), eps);
    assertEquals(-res, InterpolationVectorContainer.getVy(r, theta), eps);
    theta = 3 * Math.PI / 4;
    assertEquals(-res, InterpolationVectorContainer.getVx(r, theta), eps);
    assertEquals(res, InterpolationVectorContainer.getVy(r, theta), eps);
  }

  public void testVectorParameters() {
    CtuluVariableDefault vx = new CtuluVariableDefault("vx");
    CtuluVariableDefault vy = new CtuluVariableDefault("vy");
    CtuluVariableDefault bathy = new CtuluVariableDefault("bathy");
    InterpolationVectorContainer vect = new InterpolationVectorContainer();
    vect.addVxVy(vx, vy);
    assertTrue(vect.isVect(vx));
    assertTrue(vect.isVx(vx));
    assertTrue(vect.isVect(vy));
    assertTrue(vect.isVy(vy));
    assertFalse(vect.isVect(bathy));
    List var = Arrays.asList(new Object[] { vx, bathy });
    InterpolationSrcAdapter src = new InterpolationSrcAdapter(bathy, vx, vy);
    double dist = CtuluLibGeometrie.getDistance(0, 0, 0.5, 0.5);
    double normeAttendue = getInterpolValue(dist, src.getNorme(0), dist, src.getNorme(1), dist, src.getNorme(2), dist,
        src.getNorme(3));
    double angleAttendue = getInterpolValue(dist, src.getAngle(0), dist, src.getAngle(1), dist, src.getAngle(2), dist,
        src.getAngle(3));
    double vxAttendu = InterpolationVectorContainer.getVx(normeAttendue, angleAttendue);
    double vyAttendu = InterpolationVectorContainer.getVy(normeAttendue, angleAttendue);
    InterpolationParameters params = new InterpolationParameters(var, new InterpolationTargetTmp(), src, vect);
    // on teste les parametres tempos cr�es pour contenir les vecteurs
    InterpolationParameters forVect = params.createParametersForVect();
    assertEquals(3, forVect.getVariable().size());
    assertEquals(vy, forVect.getVariable().get(2));
    new InterpolatorBilinear(src).interpolate(params);
    InterpolationResultsHolderI res = params.getResults();
    assertEquals(params.getTarget().getPtsNb(), res.getNbPtResultSet());
    double[] valuesForPt = res.getValuesForPt(0);
    assertNotNull(valuesForPt);
    assertEquals(2, valuesForPt.length);
    assertEquals(getInterpolValue(dist, src.getV(bathy, 0), dist, src.getV(bathy, 1), dist, src.getV(bathy, 2), dist,
        src.getV(bathy, 3)), valuesForPt[1], 1E-5);
    assertEquals(vxAttendu, valuesForPt[0], 1E-5);

    // 2eme essai
    var = Arrays.asList(new Object[] { vy, vx, bathy });
    params = new InterpolationParameters(var, new InterpolationTargetTmp(), src, vect);
    new InterpolatorBilinear(src).interpolate(params);
    assertEquals(params.getTarget().getPtsNb(), res.getNbPtResultSet());
    valuesForPt = params.getResults().getValuesForPt(0);
    assertEquals(3, valuesForPt.length);
    assertEquals(getInterpolValue(dist, src.getV(bathy, 0), dist, src.getV(bathy, 1), dist, src.getV(bathy, 2), dist,
        src.getV(bathy, 3)), valuesForPt[2], 1E-5);

    assertEquals(vxAttendu, valuesForPt[1], 1E-5);

    assertEquals(vyAttendu, valuesForPt[0], 1E-5);

  }

  public static double getInterpolValue(final double _d1, final double _z1, final double _d2, final double _z2,
      final double _d3, final double _z3, final double _d4, final double _z4) {
    return (_z1 / (_d1 * _d1) + _z2 / (_d2 * _d2) + _z3 / (_d3 * _d3) + _z4 / (_d4 * _d4))
        / (1 / (_d1 * _d1) + 1 / (_d2 * _d2) + 1 / (_d3 * _d3) + 1 / (_d4 * _d4));
  }

  static class InterpolationTargetTmp implements InterpolationTarget {
    final double[] x_ = new double[] { 0.5 };
    final double[] y_ = new double[] { 0.5 };

    @Override
    public int getPtsNb() {
      return x_.length;
    }

    @Override
    public double getPtX(int _i) {
      return x_[_i];
    }

    @Override
    public double getPtY(int _i) {
      return y_[_i];
    }

    @Override
    public boolean isInBuffer(double _xToTest, double _yToTest, double _maxDist) {
      return true;
    }

  }

  static class InterpolationSrcAdapter implements InterpolationSupportValuesI, SupportLocationI {

    final double[] x_ = new double[] { 0, 1, 0, 1 };
    final double[] y_ = new double[] { 0, 0, 1, 1 };
    final double[] bathy_ = new double[] { 1, -1, 2, -2 };
    final double[] vx_ = new double[] { 10, -10, -30, 30 };
    final double[] vy_ = new double[] { -100, 100, -200, 200 };
    final CtuluVariable vBathy_;
    final CtuluVariable vVx_;
    final CtuluVariable vVy_;

    public InterpolationSrcAdapter(final CtuluVariable _bathy, final CtuluVariable _vx, final CtuluVariable _vy) {
      super();
      vBathy_ = _bathy;
      vVx_ = _vx;
      vVy_ = _vy;
    }

    @Override
    public int getPtsNb() {
      return x_.length;
    }

    public double getNorme(int _i) {
      return InterpolationVectorContainer.getNorme(vx_[_i], vy_[_i]);
    }

    public double getAngle(int _i) {
      return InterpolationVectorContainer.getTheta(vx_[_i], vy_[_i]);
    }

    @Override
    public double getPtX(int _i) {
      return x_[_i];
    }

    @Override
    public double getPtY(int _i) {
      return y_[_i];
    }

    @Override
    public double getV(CtuluVariable _var, int _ptIdx) {
      if (_var == vBathy_) return bathy_[_ptIdx];
      if (_var == vVx_) return vx_[_ptIdx];
      if (_var == vVy_) return vy_[_ptIdx];
      return -1000000;
    }
  }

}
