<?xml version='1.0' encoding='UTF-8'?>
<JCSDataFile xmlns:gml="http://www.opengis.net/gml" xmlns:xsi="http://www.w3.org/2000/10/XMLSchema-instance" >
<JCSGMLInputTemplate>
<CollectionElement>featureCollection</CollectionElement>
<FeatureElement>feature</FeatureElement>
<GeometryElement>geometry</GeometryElement>
<ColumnDefinitions>
     <column>
          <name>frottements</name>
          <type>DOUBLE</type>
          <valueElement elementName="property" attributeName="name" attributeValue="frottements"/>
          <valueLocation position="body"/>
     </column>
     <column>
          <name>ATTRIBUTE_INDEX_GEOM</name>
          <type>INTEGER</type>
          <valueElement elementName="property" attributeName="name" attributeValue="ATTRIBUTE_INDEX_GEOM"/>
          <valueLocation position="body"/>
     </column>
</ColumnDefinitions>
</JCSGMLInputTemplate>

<featureCollection>
     <feature>
          <geometry>
                <gml:Point>
                  <gml:coordinates>6.050365556458163,5.127091795288383,0.0</gml:coordinates>
                </gml:Point>
          </geometry>
          <property name="frottements">1.0</property>
          <property name="ATTRIBUTE_INDEX_GEOM">0</property>
     </feature>

     <feature>
          <geometry>
                <gml:Point>
                  <gml:coordinates>7.828432168968317,5.0653533712428915,0.0</gml:coordinates>
                </gml:Point>
          </geometry>
          <property name="frottements">10.0</property>
          <property name="ATTRIBUTE_INDEX_GEOM">1</property>
     </feature>

     <feature>
          <geometry>
                <gml:Point>
                  <gml:coordinates>8.519902518277823,4.509707554833469,0.0</gml:coordinates>
                </gml:Point>
          </geometry>
          <property name="frottements">21.0</property>
          <property name="ATTRIBUTE_INDEX_GEOM">2</property>
     </feature>

     </featureCollection>
</JCSDataFile>
