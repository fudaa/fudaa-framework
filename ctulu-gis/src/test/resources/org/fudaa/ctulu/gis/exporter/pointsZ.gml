<?xml version='1.0' encoding='UTF-8'?>
<JCSDataFile xmlns:gml="http://www.opengis.net/gml" xmlns:xsi="http://www.w3.org/2000/10/XMLSchema-instance" >
<JCSGMLInputTemplate>
<CollectionElement>featureCollection</CollectionElement>
<FeatureElement>feature</FeatureElement>
<GeometryElement>geometry</GeometryElement>
<ColumnDefinitions>
     <column>
          <name>ATTRIBUTE_INDEX_GEOM</name>
          <type>INTEGER</type>
          <valueElement elementName="property" attributeName="name" attributeValue="ATTRIBUTE_INDEX_GEOM"/>
          <valueLocation position="body"/>
     </column>
</ColumnDefinitions>
</JCSGMLInputTemplate>

<featureCollection>
     <feature>
          <geometry>
                <gml:Point>
                  <gml:coordinates>3.074573517465475,4.8430950446791226,123.0</gml:coordinates>
                </gml:Point>
          </geometry>
          <property name="ATTRIBUTE_INDEX_GEOM">0</property>
     </feature>

     <feature>
          <geometry>
                <gml:Point>
                  <gml:coordinates>4.655077173030056,4.5590982940698614,124.0</gml:coordinates>
                </gml:Point>
          </geometry>
          <property name="ATTRIBUTE_INDEX_GEOM">1</property>
     </feature>

     <feature>
          <geometry>
                <gml:Point>
                  <gml:coordinates>6.964094232331437,4.608489033306254,125.0</gml:coordinates>
                </gml:Point>
          </geometry>
          <property name="ATTRIBUTE_INDEX_GEOM">2</property>
     </feature>

     <feature>
          <geometry>
                <gml:Point>
                  <gml:coordinates>8.779203899268886,4.102233956133224,126.0</gml:coordinates>
                </gml:Point>
          </geometry>
          <property name="ATTRIBUTE_INDEX_GEOM">3</property>
     </feature>

     </featureCollection>
</JCSDataFile>

