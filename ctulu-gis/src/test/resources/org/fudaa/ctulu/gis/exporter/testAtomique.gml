<?xml version='1.0' encoding='UTF-8'?>
<JCSDataFile xmlns:gml="http://www.opengis.net/gml" xmlns:xsi="http://www.w3.org/2000/10/XMLSchema-instance" >
<JCSGMLInputTemplate>
<CollectionElement>featureCollection</CollectionElement> 
<FeatureElement>feature</FeatureElement>
<GeometryElement>geometry</GeometryElement>
<ColumnDefinitions>
     <column>
          <name>[S]Label</name>
          <type>STRING</type>
          <valueElement elementName="property" attributeName="name" attributeValue="[S]Label"/>
          <valueLocation position="body"/>
     </column>
     <column>
          <name>ATTRIBUTE_INDEX_GEOM</name>
          <type>INTEGER</type>
          <valueElement elementName="property" attributeName="name" attributeValue="ATTRIBUTE_INDEX_GEOM"/>
          <valueLocation position="body"/>
     </column>
</ColumnDefinitions>
</JCSGMLInputTemplate>

<featureCollection>
     <feature> 
          <geometry>
                <gml:LinearRing>
                  <gml:coordinates>3.7169260045509422,4.365131466322834,2.0 
                    3.7169260045509422,5.377047138606262,2.0 
                    5.338429431221977,5.377047138606262,2.0 
                    5.338429431221977,4.365131466322834,2.0 
                    3.7169260045509422,4.365131466322834,2.0 </gml:coordinates>
                </gml:LinearRing>
          </geometry>
          <property name="[S]Label">{hello1,hello2,hello3,hello4,}</property>
          <property name="ATTRIBUTE_INDEX_GEOM">0</property>
     </feature>

     </featureCollection>
</JCSDataFile>
