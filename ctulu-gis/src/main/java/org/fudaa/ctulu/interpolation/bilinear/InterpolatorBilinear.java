/*
 * @creation 11 f�vr. 2004
 * @modification $Date: 2007-06-11 13:03:48 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.org
 */
package org.fudaa.ctulu.interpolation.bilinear;

import gnu.trove.TIntArrayList;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.gis.CtuluLibGeometrie;
import org.fudaa.ctulu.interpolation.InterpolationParameters;
import org.fudaa.ctulu.interpolation.InterpolationResultsHolderI;
import org.fudaa.ctulu.interpolation.InterpolationSupportValuesI;
import org.fudaa.ctulu.interpolation.InterpolationTarget;
import org.fudaa.ctulu.interpolation.Interpolator;
import org.fudaa.ctulu.interpolation.SupportLocationI;

/**
 * @author deniger
 * @version $Id: InterpolatorBilinear.java,v 1.2 2007-06-11 13:03:48 deniger Exp $
 */
public final class InterpolatorBilinear extends Interpolator {

  double distance_;
  InterpolationBilinearSupportSorted ref_;
  final SupportLocationI init_;
  boolean stop_;

  /**
   * @param _target
   * @param _ref
   * @param _results
   */
  public InterpolatorBilinear(final SupportLocationI _ref) {
    super();
    init_ = _ref;
    distance_ = -1;
  }

  /**
   * @return la distance minimal par rapport � la frontiere
   */
  public double getDistance() {
    return distance_;
  }

  @Override
  protected boolean doInterpolate(InterpolationParameters _params) {
    // cas simple
    if (_params == null || _params.getTarget() == null || (init_ == null)) {
      return false;
    }
    _params.createDefaultResults();
    // on optimise le suppport
    if (ref_ == null) {
      ref_ = InterpolationBilinearSupportSorted.buildSortedSrc(init_, _params.getProg());
    }
    final InterpolationTarget target = _params.getTarget();
    final InterpolationResultsHolderI results = _params.getResults();
    final InterpolationSupportValuesI srcValues = _params.getValues();
    // nb represente le nombre de point
    final int nb = target.getPtsNb();
    // la progression en 3 etapes de 30
    final ProgressionUpdater prog = new ProgressionUpdater(_params.getProg());
    prog.setValue(3, nb, 0, 90);
    // les indices des 4 points entourants le point en question
    final int[] quad = new int[4];
    // valeur d'indice templ

    // final double[] r = new double[nb];
    // indique si un point a au moins un point correct pour l'interpolation
    boolean made;
    // indice du point identique. -1 sinon
    int sameIdx;
    // les points qui n'ont aucun point d'interpolation correct
    TIntArrayList pointNotSet = null;
    double x, y,  xref, yref;
    CtuluPermanentList variable = _params.getVariable();
    final int nbValues = variable.size();
    final double[] numerateur = new double[nbValues];
    final double[] denominateur = new double[nbValues];
    final double[] values = new double[nbValues];
    // on parcourt tous les points du maillage
    for (int i = nb - 1; i >= 0; i--) {
      if (stop_) {
        return false;
      }
      // x et y du point
      x = target.getPtX(i);
      y = target.getPtY(i);
      // on recherche les quatres points les plus proches
      // dans les quadrants
      ref_.getQuadrantIdx(x, y, quad);
      // initialisation des variables
      made = false;
      sameIdx = -1;
      Arrays.fill(numerateur, 0);
      Arrays.fill(denominateur, 0);
      // pour chaque point trouve, interpolation
      for (int j = 3; j >= 0; j--) {
        // temp est l'indice du point de reference
        final int temp = quad[j];

        if (temp >= 0) {
          xref = init_.getPtX(temp);
          yref = init_.getPtY(temp);
          // dans le cas ou une distance max est donne, la distance du point
          // trouve et de la frontiere doit etre inferieur a distanceMax...
          if ((distance_ < 0) || (target.isInBuffer(xref, yref, distance_))) {
            made = true;
            double dist = CtuluLibGeometrie.getD2(xref, yref, x, y);
            // le point est identique: pas besoin d'aller + loin
            //TODO Verifier si erreur vient de l�.
//            if (dist == 0) {
            if (CtuluLib.isZero(dist, _params.getEps())) {
              sameIdx = temp;
              break;
              // sinon on incremente les num et denominateur
            }
            for (int k = nbValues - 1; k >= 0; k--) {
              numerateur[k] += srcValues.getV((CtuluVariable) variable.get(k), temp) / dist;
              denominateur[k] += 1d / dist;
            }

          }
        }
      }
      // si un point identique a ete trouve
      if (sameIdx >= 0) {
        for (int k = nbValues - 1; k >= 0; k--) {
          values[k] = srcValues.getV((CtuluVariable) variable.get(k), sameIdx);
        }
        results.setResult(i, values);
        // si au moins un point trouve
      } else if (made) {
        for (int k = nbValues - 1; k >= 0; k--) {
          values[k] = numerateur[k] / denominateur[k];
        }
        results.setResult(i, values);
        // sinon, on met a jour le vecteur des indices incorrects
      } else {
        if (pointNotSet == null) {
          pointNotSet = new TIntArrayList();
        }
        pointNotSet.add(i);
      }
      prog.majAvancement();
    }
    // traitement des cas penibles
    if (pointNotSet != null) {
      manageBadPoints(target, _params.getAnalyze(), nb, pointNotSet);
      if (_params.getAnalyze().containsFatalError()) {
        return false;
      }
      // On recupere les points les plus proches des points non interpoles
      // correctement et on leur affecte une valeur
      extrapolateValues(target, results, pointNotSet);
    }
    return true;
  }

  private void extrapolateValues(final InterpolationTarget _target, InterpolationResultsHolderI _res,
      final TIntArrayList _pointNotSet) {
    int nbPt = _target.getPtsNb();
    for (int i = _pointNotSet.size() - 1; i >= 0; i--) {
      double dmin = Double.MAX_VALUE;
      final int temp = _pointNotSet.getQuick(i);
      final double x = _target.getPtX(temp);
      final double y = _target.getPtY(temp);
      int nearest = -1;
      for (int j = nbPt - 1; j >= 0; j--) {
        // le point considere ne doit pas etre un point non interpole
        if (_res.isDone(j)) {
          final double d = CtuluLibGeometrie.getD2(x, y, _target.getPtX(j), _target.getPtY(j));
          if (d < dmin) {
            dmin = d;
            nearest = j;
          }
        }
      }
      if (nearest >= 0) {
        _res.setResult(temp, nearest);
      }

    }
  }

  private void manageBadPoints(final InterpolationTarget _target, final CtuluAnalyze _analyze,
      final int _nbToInterpolate, final TIntArrayList _pointNotSet) {
    _pointNotSet.sort();
    final int nbBad = _pointNotSet.size();

    // Affichage avertissement
    if (nbBad == _nbToInterpolate) {
      _analyze.addFatalError(CtuluLib.getS("Aucun point n'a pu �tre interpol�"), 0);
    } else if (nbBad > 15) {
      _analyze.addWarn(CtuluLib.getS("{0} points n'ont pas �t� interpol�s correctement", CtuluLibString
          .getString(nbBad)), 0);
    } else {
      final StringBuffer b = new StringBuffer();
      for (int i = 0; i < nbBad; i++) {
        final int temp = _pointNotSet.getQuick(i);
        b.append(CtuluLibString.getString(temp)).append(CtuluLibString.ESPACE).append("(").append(_target.getPtX(temp))
            .append(";").append(_target.getPtY(temp)).append(")");
      }
      _analyze.addWarn(CtuluLib.getS("Points non interpol�s correctement: {0}", b.toString()), 0);
    }
  }

  public void setDistance(final double _distance) {
    distance_ = _distance;
  }

  @Override
  public void stop() {
    stop_ = true;
  }
}