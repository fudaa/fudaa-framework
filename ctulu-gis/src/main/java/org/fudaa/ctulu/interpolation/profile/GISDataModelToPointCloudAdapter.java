/*
 * @creation     27 nov. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.interpolation.profile;

import java.util.HashMap;
import java.util.Map;
import org.fudaa.ctulu.collection.CtuluCollection;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISMultiPoint;

/**
 * Prend une GISDataModel, ne tient compte que des GISMultiPoints et montre le
 * tout sous forme d'un nuage de points.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class GISDataModelToPointCloudAdapter implements PointCloudI{

  /**
   * Table de correspondance entre les indices virtuels et r�els. La clef est
   * l'indice virtuel, le tableau contient en premi�re valeur l'indice du
   * multipoint et en seconde valeur l'indice du point r�el dans le mutipoint.
   */
  private Map<Integer, Integer[]> correspondance_=new HashMap<Integer, Integer[]>();
  /** Le model de r�f�rence. */
  private GISDataModel model_;
  /** L'attribut contenant le z. */
  private GISAttributeInterface attributeZ_;
  
  /**
   * @param _model le mod�le a adapter
   * @param l'attribut contenant le z. Si null le z des points sera prit.
   */
  public GISDataModelToPointCloudAdapter(GISDataModel _model, GISAttributeInterface _attrZ){
    int idxCorrespondance=0;
    model_=_model;
    attributeZ_=_attrZ;
    for(int i=0;i<model_.getNumGeometries();i++)
      if(model_.getGeometry(i) instanceof GISMultiPoint)
        for(int j=0, jend=model_.getGeometry(i).getNumPoints(); j<jend; j++)
          correspondance_.put(idxCorrespondance++, new Integer[]{i, j});
  }
  
  /* (non-Javadoc)
   * @see org.fudaa.ctulu.interpolation.profile.PointCloudI#getNbPoints()
   */
  @Override
  public int getNbPoints() {
    return correspondance_.size();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.interpolation.profile.PointCloudI#getX(int)
   */
  @Override
  public double getX(int _idx) {
    return ((GISMultiPoint)model_.getGeometry(correspondance_.get(_idx)[0])).getCoordinateSequence().getX(correspondance_.get(_idx)[1]);
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.interpolation.profile.PointCloudI#getY(int)
   */
  @Override
  public double getY(int _idx) {
    return ((GISMultiPoint)model_.getGeometry(correspondance_.get(_idx)[0])).getCoordinateSequence().getY(correspondance_.get(_idx)[1]);
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.interpolation.profile.PointCloudI#getZ(int)
   */
  @Override
  public double getZ(int _idx) {
    if (attributeZ_==null)
      return ((GISMultiPoint)model_.getGeometry(correspondance_.get(_idx)[0])).getCoordinateSequence().getOrdinate(correspondance_.get(_idx)[1], 2);
    else {
      Object value=model_.getValue(model_.getIndiceOf(attributeZ_), correspondance_.get(_idx)[0]);
      if (!attributeZ_.isAtomicValue())
        return ((Double)value).doubleValue();
      else
        return ((Double)((CtuluCollection)value).getObjectValueAt(correspondance_.get(_idx)[1])).doubleValue();
    }
  }
}
