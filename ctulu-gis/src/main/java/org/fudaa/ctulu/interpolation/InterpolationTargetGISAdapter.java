/*
 *  @creation     5 sept. 2005
 *  @modification $Date: 2007-06-05 08:57:43 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

import org.fudaa.ctulu.gis.GISCoordinateSequenceContainerInterface;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.geom.*;

import java.util.ArrayList;

/**
 * Un adapteur pour une cible d'interpolation a partir d'un modele contenant des
 * géométries.
 *
 * @author Fred Deniger
 * @version $Id: InterpolationSupportGISAdapter.java,v 1.2 2007-06-11 13:03:29 deniger Exp $
 */
public class InterpolationTargetGISAdapter implements InterpolationTarget {
  Envelope env_;
  LinearRing extern_;
  final GISZoneCollection geometries_;
  IndexedPointInAreaLocator tester_;
  /**
   * Si le modele ne contient que des points, optimisation de la cible
   */
  final boolean onlyPoints_;
  /**
   * Si le modele continet autre chose que des points
   */
  int[][] idx2geom_;
  final Coordinate tmp_ = new Coordinate();
  final Coordinate tmpX1_ = new Coordinate();
  final Coordinate tmpX2_ = new Coordinate();

  /**
   * @param _geoms
   */
  public InterpolationTargetGISAdapter(final GISZoneCollection _geoms) {
    super();
    geometries_ = _geoms;

    onlyPoints_ = containsOnlyPoints();
    if (!onlyPoints_) {
      initIndexes();
    }
  }

  /**
   * Retourne vrai si le modele ne contient que des points.
   *
   * @return True : Si vide ou uniquement des points.
   */
  public boolean containsOnlyPoints() {
    int nb = geometries_.getNumGeometries();
    for (int i = 0; i < nb; i++) {
      if (!(geometries_.getGeometry(i) instanceof Point)) {
        return false;
      }
    }
    return true;
  }

  private void initIndexes() {
    ArrayList<int[]> vidx2geom = new ArrayList<int[]>();

    int nb = geometries_.getNumGeometries();
    for (int i = 0; i < nb; i++) {
      Geometry g = geometries_.getGeometry(i);
      int nbpts = g.getNumPoints();
      for (int j = 0; j < nbpts; j++) {
        vidx2geom.add(new int[]{i, j});
      }
    }
    idx2geom_ = vidx2geom.toArray(new int[0][0]);
  }

  private void buildEnvs() {
    env_ = geometries_.getEnvelopeInternal();
    final Geometry g = geometries_.convexHull();
    if (g instanceof LinearRing) {
      extern_ = (LinearRing) g;
    } else if (g instanceof Polygon) {
      extern_ = (LinearRing) ((Polygon) g).getExteriorRing();
    }
    tester_ = new IndexedPointInAreaLocator(extern_);
  }

  private double getDistanceFromFrontier(final double _xref, final double _yref) {
    tmp_.x = _xref;
    tmp_.y = _yref;
    return GISLib.getDistanceFromFrontier(extern_.getCoordinateSequence(), tmp_, tmpX1_, tmpX2_);
  }

  @Override
  public int getPtsNb() {
    int nb;

    if (onlyPoints_) {
      nb = geometries_.getNumPoints();
    } else {
      nb = idx2geom_.length;
    }

    return nb;
  }

  @Override
  public double getPtX(final int _i) {
    if (onlyPoints_) {
      return ((Point) geometries_.getGeometry(_i)).getX();
    } else {
      CoordinateSequence seq =
          ((GISCoordinateSequenceContainerInterface) geometries_.getGeometry(idx2geom_[_i][0])).getCoordinateSequence();
      return seq.getX(idx2geom_[_i][1]);
    }
//    return geometries_.getX(_i);
  }

  @Override
  public double getPtY(final int _i) {
    if (onlyPoints_) {
      return ((Point) geometries_.getGeometry(_i)).getY();
    } else {
      CoordinateSequence seq =
          ((GISCoordinateSequenceContainerInterface) geometries_.getGeometry(idx2geom_[_i][0])).getCoordinateSequence();
      return seq.getY(idx2geom_[_i][1]);
    }
//    return geometries_.getY(_i);
  }

  @Override
  public boolean isInBuffer(final double _xToTest, final double _yToTest, final double _maxDist) {
    if (env_ == null) {
      buildEnvs();
    }
    // le point n'appartient pas a l'enveloppe du maillage
    if (!env_.contains(_xToTest, _yToTest)) {
      if (GISLib.getDistance(_xToTest, _yToTest, env_) > _maxDist) {
        return false;
      }

      return getDistanceFromFrontier(_xToTest, _yToTest) <= _maxDist;
    }
    tmp_.x = _xToTest;
    tmp_.y = _yToTest;
    // le point est contenu dans la forme: ok
    if (GISLib.isInside(tester_, tmp_)) {
      return true;
    }
    // test distance
    return getDistanceFromFrontier(_xToTest, _yToTest) <= _maxDist;
  }
}
