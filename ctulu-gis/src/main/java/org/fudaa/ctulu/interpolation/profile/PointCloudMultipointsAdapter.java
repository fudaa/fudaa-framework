/*
 * @creation     23 sept. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.interpolation.profile;

import org.fudaa.ctulu.gis.GISMultiPoint;

/**
 * Un nuage de point bas� sur un GISMultiPoint.
 * L'adapter permet d'acceder aux informations du nuage sans toucher a la structure des
 * donn�es manipul�es.
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public class PointCloudMultipointsAdapter implements PointCloudI {
  GISMultiPoint mp_;
  
  
  /**
   * Un nuage de point construit a partir d'un multipoint.
   * @param _mp Le multipoint.
   */
  public PointCloudMultipointsAdapter(GISMultiPoint _mp) {
    mp_=_mp;
  }
  
  @Override
  public int getNbPoints() {
    return mp_.getNumPoints();
  }

  @Override
  public double getX(int _idx) {
    return mp_.getCoordinateSequence().getOrdinate(_idx,0);
  }
  @Override
  public double getY(int _idx) {
    return mp_.getCoordinateSequence().getOrdinate(_idx,1);
  }
  @Override
  public double getZ(int _idx) {
    return mp_.getCoordinateSequence().getOrdinate(_idx,2);
  }
}
