/*
 * @creation 29 mai 07
 * @modification $Date: 2007-06-11 13:03:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

import java.util.HashMap;
import java.util.Map;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;

/**
 * @author fred deniger
 * @version $Id: InterpolationVectorContainer.java,v 1.2 2007-06-11 13:03:29 deniger Exp $
 */
public class InterpolationVectorContainer {

  final Map vxVy_ = new HashMap();
  final Map vyVx_ = new HashMap();

  public void addVxVy(final CtuluVariable _vx, final CtuluVariable _vy) {
    vxVy_.put(_vx, _vy);
    vyVx_.put(_vy, _vx);
  }

  public boolean isVect(final CtuluVariable _var) {
    return vxVy_.containsKey(_var) || vyVx_.containsKey(_var);
  }

  public CtuluCollectionDouble getVxValue(CtuluVariable _vxOrVy, InterpolationSupportValuesMultiI _src) {
    if (isVx(_vxOrVy)) {
      return _src.getValues(_vxOrVy);
    } else if (isVy(_vxOrVy)) return _src.getValues(getVx(_vxOrVy));
    return null;
  }

  public CtuluCollectionDouble getVyValue(CtuluVariable _vxOrVy, InterpolationSupportValuesMultiI _src) {
    if (isVy(_vxOrVy)) {
      return _src.getValues(_vxOrVy);
    } else if (isVx(_vxOrVy)) return _src.getValues(getVy(_vxOrVy));
    return null;
  }

  public boolean isVx(final CtuluVariable _var) {
    return vxVy_.containsKey(_var);
  }

  /**
   * @param _vx la variable vx
   * @return le vy correspond ou null si _vx n'est pas connue
   */
  public CtuluVariable getVy(final CtuluVariable _vx) {
    return (CtuluVariable) vxVy_.get(_vx);
  }

  /**
   * @param _vxOrVy la variable vx ou vy
   * @return le vy correspondant ou le parametre _vxOrVy (qui est donc suppos� �tre un vy)
   */
  public CtuluVariable getVySafe(final CtuluVariable _vxOrVy) {
    CtuluVariable res = (CtuluVariable) vxVy_.get(_vxOrVy);
    return res == null ? _vxOrVy : res;
  }

  /**
   * @param _vy la variable vy
   * @return le vx correspond ou null si _vy n'est pas connue
   */
  public CtuluVariable getVx(final CtuluVariable _vy) {
    return (CtuluVariable) vyVx_.get(_vy);
  }

  /**
   * @param _vxOrVy la variable vx ou vy
   * @return le vx correspondant ou le parametre _vxOrVy (qui est donc suppos� �tre un vx)
   */
  public CtuluVariable getVxSafe(final CtuluVariable _vxOrVy) {
    CtuluVariable res = (CtuluVariable) vyVx_.get(_vxOrVy);
    return res == null ? _vxOrVy : res;
  }

  public void clear() {
    vxVy_.clear();
    vyVx_.clear();
  }

  public boolean isVy(final CtuluVariable _var) {
    return vyVx_.containsKey(_var);
  }

  public static double getNorme(double _vx, double _vy) {
    return Math.hypot(_vx, _vy);
  }

  public static double getTheta(double _vx, double _vy) {
    double n = getNorme(_vx, _vy);
    if (CtuluLib.isZero(n)) return 0;
    if (CtuluLib.isZero(_vx)) { return CtuluLib.getSign(_vy) * Math.PI / 2; }
    if (CtuluLib.isZero(_vy)) { return _vx >= 0 ? 0 : Math.PI; }
    double theta = Math.atan(_vy / _vx);
    return _vx > 0 ? theta : (CtuluLib.getSign(_vy) * Math.PI + theta);
  }

  public static double getVx(double _r, double _theta) {
    return _r * Math.cos(_theta);
  }

  public static double getVy(double _r, double _theta) {
    return _r * Math.sin(_theta);
  }

  public static double[] getNorme(CtuluCollectionDouble _vx, CtuluCollectionDouble _vy) {
    double[] res = new double[_vx.getSize()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = getNorme(_vx.getValue(i), _vy.getValue(i));
    }
    return res;
  }

  public static double[] getTheta(CtuluCollectionDouble _vx, CtuluCollectionDouble _vy) {
    double[] res = new double[_vx.getSize()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = getTheta(_vx.getValue(i), _vy.getValue(i));
    }
    return res;
  }

  public static double[] getVx(CtuluCollectionDouble _r, CtuluCollectionDouble _theta) {
    double[] res = new double[_r.getSize()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = getVx(_r.getValue(i), _theta.getValue(i));
    }
    return res;
  }

  public static double[] getVy(CtuluCollectionDouble _r, CtuluCollectionDouble _theta) {
    double[] res = new double[_r.getSize()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = getVy(_r.getValue(i), _theta.getValue(i));
    }
    return res;
  }
}