/*
 * @creation     28 nov. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.interpolation.profile;

import org.locationtech.jts.geom.Coordinate;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.gis.GISPoint;

/**
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class InterpolationProfilTest {

  private GISPoint[] ptsTrace_;
  private PointCloudI cloud_;

  public InterpolationProfilTest(GISPoint[] _ptsTrace, PointCloudI _cloud) {
    ptsTrace_=_ptsTrace;
    cloud_=_cloud;
  }

  public GISPoint[] process() {
    GISPoint ptTrace1=ptsTrace_[0];
    GISPoint ptTrace2=ptsTrace_[ptsTrace_.length-1];
    List<GISPoint> profil=new ArrayList<GISPoint>();
    // 1 : Cr�ation de la liste des points projet�s et clacul de la distance
    // entre le point d'origine et le point projet�. \\
    /*
     * Le point projet�, son indice dans le nuage et la distance entre les deux
     * points, distance entre le point projet� et l'origine de la trace, et si
     * il se trouve en haut de la trace.
     */
    List<Object[]> pointProj=new ArrayList<Object[]>(cloud_.getNbPoints());
    for (int i=0; i<cloud_.getNbPoints(); i++) {
      double x=cloud_.getX(i);
      double y=cloud_.getY(i);
      double z=cloud_.getZ(i);
      Coordinate proj=projection(ptTrace1, ptTrace2, x, y, z);
      // Si le point projet� est situ� sur la trace, on garde sinon on l'oublie.
      if(proj!=null)
        pointProj.add(new Object[]{proj, i, dist(proj.x, proj.y, proj.z, x, y, z), dist(ptTrace1.getCoordinate(), proj),
            enHaut(ptTrace1, ptTrace2, x, y, z)});
    }

    // Tri des points projet�s par rapport � la distance entre le point proj et
    // l'origine de la trace
    for (int i=0; i<pointProj.size(); i++)
      for (int j=0; j<pointProj.size()-1; j++)
        if ((Double)pointProj.get(j)[3]>(Double)pointProj.get(j+1)[3]) {
          Object[] tmp=pointProj.get(j);
          pointProj.set(j, pointProj.get(j+1));
          pointProj.set(j+1, tmp);
        }

    // 2 : Suppression des points trop �loign�s
    // Boucle sur les points en haut de la droite; cr�ation d'une table de
    // correspondance ne gardant que les points au dessus
    List<Integer> pHaut=new ArrayList<Integer>();
    for (int i=0; i<pointProj.size(); i++)
      if ((Boolean)pointProj.get(i)[4])
        pHaut.add(i);

    List<Integer> ptSuppr=new ArrayList<Integer>();
    int i=1;
    while (i<pHaut.size()-1) {
      int iProjPre=pHaut.get(i-1);
      int iProj=pHaut.get(i);
      int iProjPost=pHaut.get(i+1);
      int iNuagePre=(Integer)pointProj.get(iProjPre)[1];
      int iNuagePost=(Integer)pointProj.get(iProjPost)[1];

      if ((Double)pointProj.get(iProj)[2]>dist(cloud_.getX(iNuagePre), cloud_.getY(iNuagePre), cloud_.getZ(iNuagePre), cloud_
          .getX(iNuagePost), cloud_.getY(iNuagePost), cloud_.getZ(iNuagePost))
          &&(Double)pointProj.get(iProj)[2]>(Double)pointProj.get(iProjPre)[2]
          &&(Double)pointProj.get(iProj)[2]>(Double)pointProj.get(iProjPost)[2]) {
        pHaut.remove(i);
        ptSuppr.add(iProj);
        if (i>1)
          i--; // reteste le point pr�c�dent, sont suivant ayant chang�.
      }
      else
        i++;
    }
    // supression des points inutils en haut.
    for (i=0; i<ptSuppr.size(); i++) {
      for (int j=0; j<ptSuppr.size()-1; j++)
        if (ptSuppr.get(j)>ptSuppr.get(j+1)) {
          Integer tmp=ptSuppr.get(j);
          ptSuppr.set(j, ptSuppr.get(j+1));
          ptSuppr.set(j+1, tmp);
        }
    }
    for (int j=ptSuppr.size()-1; j>=0; j--)
      pointProj.remove(((Integer)ptSuppr.get(j)).intValue());

    // Boucle sur les points en bas de la droite
    ptSuppr.clear();
    List<Integer> pBas=new ArrayList<Integer>();
    for (i=0; i<pointProj.size(); i++)
      if (!(Boolean)pointProj.get(i)[4])
        pBas.add(i);

    i=1;
    while (i<pBas.size()-1) {
      int iProjPre=pBas.get(i-1);
      int iProj=pBas.get(i);
      int iProjPost=pBas.get(i+1);
      int iNuagePre=(Integer)pointProj.get(iProjPre)[1];
      int iNuagePost=(Integer)pointProj.get(iProjPost)[1];

      if ((Double)pointProj.get(iProj)[2]>dist(cloud_.getX(iNuagePre), cloud_.getY(iNuagePre), cloud_.getZ(iNuagePre), cloud_
          .getX(iNuagePost), cloud_.getY(iNuagePost), cloud_.getZ(iNuagePost))
          &&(Double)pointProj.get(iProj)[2]>(Double)pointProj.get(iProjPre)[2]
          &&(Double)pointProj.get(iProj)[2]>(Double)pointProj.get(iProjPost)[2]) {
        pBas.remove(i);
        ptSuppr.add(iProj);
        if (i>1)
          i--; // reteste le point pr�c�dent, sont suivant ayant chang�.
      }
      else
        i++;
    }
    // Supression des points inutils en bas.
    for (i=0; i<ptSuppr.size(); i++) {
      for (int j=0; j<ptSuppr.size()-1; j++)
        if (ptSuppr.get(j)>ptSuppr.get(j+1)) {
          Integer tmp=ptSuppr.get(j);
          ptSuppr.set(j, ptSuppr.get(j+1));
          ptSuppr.set(j+1, tmp);
        }
    }
    for (int j=ptSuppr.size()-1; j>=0; j--)
      pointProj.remove(((Integer)ptSuppr.get(j)).intValue());

    // 3 : Cr�ation des points du profil
    // premier point mis d'office
    profil.add(new GISPoint(((Coordinate)pointProj.get(0)[0]).x, ((Coordinate)pointProj.get(0)[0]).y, cloud_
        .getZ(((Integer)pointProj.get(0)[1]).intValue())));
    for (i=1; i<pointProj.size()-1; i++) {
      int iNuage=((Integer)pointProj.get(i)[1]).intValue();
      double distTpIpre=(Double)pointProj.get(i-1)[2];
      double distTpI=(Double)pointProj.get(i)[2];
      double distTpIpost=(Double)pointProj.get(i+1)[2];
      double sommeDist=distTpIpre+distTpI+distTpIpost;
      profil.add(new GISPoint(
          (((Coordinate)pointProj.get(i-1)[0]).x*distTpIpre+((Coordinate)pointProj.get(i)[0]).x*distTpI+((Coordinate)pointProj
              .get(i+1)[0]).x
              *distTpIpost)
              /sommeDist,
          (((Coordinate)pointProj.get(i-1)[0]).y*distTpIpre+((Coordinate)pointProj.get(i)[0]).y*distTpI+((Coordinate)pointProj
              .get(i+1)[0]).y
              *distTpIpost)
              /sommeDist, (cloud_.getZ(iNuage)*distTpIpre+cloud_.getZ(iNuage)*distTpI+cloud_.getZ(iNuage)*distTpIpost)/sommeDist));
    }
    // Dernier point mis d'office
    profil.add(new GISPoint(((Coordinate)pointProj.get(pointProj.size()-1)[0]).x,
        ((Coordinate)pointProj.get(pointProj.size()-1)[0]).y, cloud_.getZ(((Integer)pointProj.get(pointProj.size()-1)[1])
            .intValue())));

    // 4 : Suppression des points trop proches entre eux
    /*
     * Cr�ation du seuil pour la suppression des points lorsqu'ils sont trop
     * proches.
     */
    double SEUIL;
    double minZ=profil.get(0).getZ();
    double maxZ=profil.get(profil.size()-1).getZ();
    for (i=0; i<profil.size(); i++) {
      if (profil.get(i).getZ()<minZ)
        minZ=profil.get(i).getZ();
      else if (profil.get(i).getZ()>maxZ)
        minZ=profil.get(i).getZ();
    }
    /* Cast en float car en double �a conne des fois des r�sultat incoh�rent... */
    SEUIL=((float)(maxZ-minZ))/((float)100);
    // Suppression des points trop proches
    i=1;
    while (i<profil.size())
      if (Math.abs(profil.get(i-1).getZ()-profil.get(i).getZ())<=SEUIL&&profil.size()>2)
        profil.remove(i);
      else
        i++;
    // 5 : Fin de l'algo
    return profil.toArray(new GISPoint[0]);
  }

  /** Projection de a(x, y, z) sur la droite passant par o et e. */
  private Coordinate projection(GISPoint o, GISPoint e, double x, double y, double z) {
    Coordinate vOE=new Coordinate(e.getX()-o.getX(), e.getY()-o.getY(), e.getZ()-o.getZ());
    Coordinate vOA=new Coordinate(x-o.getX(), y-o.getY(), z-o.getZ());
    double normeOE=Math.sqrt(vOE.x*vOE.x+vOE.y*vOE.y+vOE.z*vOE.z);
    double produitScalaireOEOA=vOE.x*vOA.x+vOE.y*vOA.y+vOE.z*vOA.z;
    
    double rapportSurAB=produitScalaireOEOA/(normeOE*normeOE);
    
    if(rapportSurAB<0||rapportSurAB>1)
      return null;
    else
      return new Coordinate(vOE.x*rapportSurAB+o.getX(), vOE.y*rapportSurAB+o.getY(), vOE.z*rapportSurAB+o.getZ());
  }

  /**
   * Retourne vrai si le point a(x, y, z) est au dessus de la droite passant par
   * o et e. Faux sinon. On part du principe que a et e on des valeurs de z=0
   */
  private boolean enHaut(GISPoint o, GISPoint e, double x, double y, double z) {
    // Calcul de vecteur u=vec oe et normalisation
    Coordinate vecU=new Coordinate(e.getX()-o.getX(), e.getY()-o.getY(), e.getZ()-o.getZ());
    double normeU=Math.sqrt(vecU.x*vecU.x+vecU.y*vecU.y+vecU.z*vecU.z);
    vecU.x=vecU.x/normeU;
    vecU.y=vecU.y/normeU;
    vecU.z=vecU.z/normeU;
    // Calcul du produit scalaire u et oa =k
    double k=(x-o.getX())*vecU.x+(y-o.getY())*vecU.y+(z-o.getZ())*vecU.z;
    // OP=ku
    Coordinate p=new Coordinate(k*vecU.x+o.getX(), k*vecU.y+o.getY(), k*vecU.z+o.getZ());
    // Calcul du vecteur normal
    Coordinate vecNormal=new Coordinate(vecU.y, -vecU.x);
    // Calcul de l tq vecN*l=AP avec a projet� sur le plan z=0
    double l=(p.x-x)/vecNormal.x;
    return l>0;
  }

  private double dist(Coordinate a, Coordinate b) {
    return dist(a.x, a.y, a.z, b.x, b.y, b.z);
  }

  private double dist(double x2, double y2, double z2, double x, double y, double z) {
    return Math.sqrt((x-x2)*(x-x2)+(y-y2)*(y-y2)+(z-z2)*(z-z2));
  }
}
