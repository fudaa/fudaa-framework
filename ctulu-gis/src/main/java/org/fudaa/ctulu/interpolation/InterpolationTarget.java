/*
 * @creation 4 f�vr. 2004
 * @modification $Date: 2007-06-05 08:57:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

/**
 * Une interface pour les donnees cibles. Les methodes de bases sont getPtX getPty et getPtNb. Les autres methodes sont
 * optionnelles et utilis�es (pour l'instant) que dans le cas d'une interpolation sur un maillage avec la methode des
 * plus proche voisins.
 * 
 * @author deniger
 * @version $Id: InterpolationTarget.java,v 1.1 2007-06-05 08:57:43 deniger Exp $
 */
public interface InterpolationTarget {

  /**
   * @return le x du point _i
   * @param _i l'indice
   */
  double getPtX(int _i);

  /**
   * @param _i indice demande
   * @return le y du point _i
   */
  double getPtY(int _i);

  /**
   * @return Le nombre de point a interpoler.
   */
  int getPtsNb();

  /**
   * Cette methode n'est utilise que si les donnees de destination contiennent une frontiere et que l'on veut prendre en
   * compte uniquement les points de reference qui sont contenus dans cette frontiere : typiquement un maillage. Pour ne
   * pas prendre en compte cette fonctionnalit�, il suffit que cette methode renvoie true tout le temps.
   * 
   * @param _xToTest x a tester
   * @param _yToTest y a tester
   * @param _maxDist la distance max autorisee
   * @return true si contient dans le semis de point.
   */
  boolean isInBuffer(double _xToTest, double _yToTest, double _maxDist);

}