/*
 *  @creation     31 mai 2005
 *  @modification $Date: 2007-06-11 13:03:29 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

import gnu.trove.TIntObjectHashMap;
import gnu.trove.TIntObjectIterator;
import gnu.trove.TObjectIntHashMap;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.CtuluVariable;

/**
 * @author Fred Deniger
 * @version $Id: InterpolationResultsHolder.java,v 1.2 2007-06-11 13:03:29 deniger Exp $
 */
public class InterpolationResultsHolder implements InterpolationResultsHolderI {

  TIntObjectHashMap result_;
  final CtuluPermanentList variable_;
  final TObjectIntHashMap varPos_;
  final int nbPt_;

  public InterpolationResultsHolder(CtuluPermanentList _variable, InterpolationTarget _target) {
    this(_variable, _target.getPtsNb());
  }

  /**
   * @param _variable les variables dans l'ordre
   */
  public InterpolationResultsHolder(CtuluPermanentList _variable, int _targetPt) {
    super();
    result_ = new TIntObjectHashMap(_targetPt);
    variable_ = _variable;
    varPos_ = new TObjectIntHashMap(variable_.size());
    for (int i = variable_.size() - 1; i >= 0; i--) {
      varPos_.put(_variable.get(i), i);
    }
    nbPt_ = _targetPt;
  }

  @Override
  public int getNbPtResultSet() {
    return result_.size();
  }

  @Override
  public boolean isDone(int _idxPt) {
    return result_.contains(_idxPt);
  }

  public InterpolationResultsHolder getCopy() {
    final TIntObjectHashMap retMap = new TIntObjectHashMap(result_);
    final TIntObjectIterator it = result_.iterator();
    for (int i = result_.size(); i-- > 0;) {
      it.advance();
      retMap.put(it.key(), CtuluLibArray.copy((double[]) it.value()));
    }
    final InterpolationResultsHolder ret = new InterpolationResultsHolder(variable_, nbPt_);
    ret.result_ = retMap;
    return ret;

  }

  @Override
  public double[] getValueForVariable(final CtuluVariable _idxVariable) {
    if (!varPos_.contains(_idxVariable)) return null;
    return getValueForVariable(varPos_.get(_idxVariable));
  }

  public double[] getValueForVariable(final int _idxVariable) {
    final double[] r = new double[nbPt_];
    final TIntObjectIterator it = result_.iterator();
    for (int i = result_.size(); i-- > 0;) {
      it.advance();
      r[it.key()] = ((double[]) it.value())[_idxVariable];
    }
    return r;

  }

  @Override
  public void setResult(final int _idx, final double[] _values) {
    result_.put(_idx, CtuluLibArray.copy(_values));
  }

  @Override
  public double[] getValuesForPt(int _ptIdx) {
    return (double[]) result_.get(_ptIdx);
  }

  @Override
  public void setResult(int _idx, int _alreadSetIdx) {
    result_.put(_idx, CtuluLibArray.copy(getValuesForPt(_alreadSetIdx)));

  }

  public boolean isEquiv(final InterpolationResultsHolder _other) {
    if (_other == null) {
      return false;
    }
    if (result_.size() != _other.result_.size()) {
      return false;
    }
    if (result_.size() == 0) {
      return true;
    }
    final int[] keys = result_.keys();
    Arrays.sort(keys);
    final int[] okeys = _other.result_.keys();
    Arrays.sort(okeys);
    if (!Arrays.equals(keys, okeys)) {
      return false;
    }
    for (int i = keys.length - 1; i >= 0; i--) {
      final double[] d = (double[]) result_.get(keys[i]);
      final double[] dOther = (double[]) _other.result_.get(keys[i]);
      if (!Arrays.equals(d, dOther)) {
        return false;
      }
    }
    return true;
  }
}
