/*
 * @creation 4 f�vr. 2004
 * @modification $Date: 2007-06-11 13:03:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

import org.fudaa.ctulu.CtuluVariable;

/**
 * @author deniger
 * @version $Id: InterpolationSupportValuesI.java,v 1.2 2007-06-11 13:03:29 deniger Exp $
 */
public interface InterpolationSupportValuesI {

  /**
   * @param _ptIdx indice du point
   * @param _valueIdx indice de la valeur
   * @return les valeurs de reference a interpoler.
   */
  double getV(CtuluVariable _var, int _ptIdx);

}