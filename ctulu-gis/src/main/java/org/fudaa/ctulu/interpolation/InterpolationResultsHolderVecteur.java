/*
 * @creation 29 mai 07
 * @modification $Date: 2007-06-11 13:03:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

import gnu.trove.TIntArrayList;
import gnu.trove.TIntIntHashMap;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.CtuluVariable;

/**
 * Une classe permmettant de g�rer les param�tres utilis�s lors d'une interpolation.
 * 
 * @author fred deniger
 * @version $Id: InterpolationResultsHolderVecteur.java,v 1.2 2007-06-11 13:03:29 deniger Exp $
 */
public final class InterpolationResultsHolderVecteur implements InterpolationResultsHolderI {
  protected static InterpolationParameters createResultHolder(final InterpolationParameters _params) {
    final CtuluPermanentList variable = _params.getVariable();
    final InterpolationVectorContainer vects = _params.vects_;
    final int nbInitVar = variable.size();

    final TIntArrayList vectIdx = new TIntArrayList(nbInitVar);
    for (int i = 0; i < nbInitVar; i++) {
      if (vects.isVect((CtuluVariable) variable.get(i))) {
        vectIdx.add(i);
      }
    }
    // pas de variables vecteurs
    if (vectIdx.isEmpty()) {
      return _params;
    }
    _params.createDefaultResults();
    final InterpolationResultsHolderVecteur resHolder = new InterpolationResultsHolderVecteur(_params.getResults(),
        vects, nbInitVar, vectIdx.toNativeArray());
    final int size = vectIdx.size();
    // on ajoute les variable n�cessaires
    final List newVar = new ArrayList(variable.size() + size);
    newVar.addAll(variable);
    final TIntIntHashMap vxVy = resHolder.vxVy_;
    final TIntIntHashMap vyVx = resHolder.vyVx_;
    vxVy.ensureCapacity(size);
    vyVx.ensureCapacity(size);

    for (int i = 0; i < size; i++) {
      final int idxInit = vectIdx.getQuick(i);
      final CtuluVariable object = (CtuluVariable) variable.get(idxInit);
      if (vects.isVx(object)) {
        final CtuluVariable vy = vects.getVy(object);
        int idxY = newVar.indexOf(vy);
        if (idxY < 0) {
          idxY = newVar.size();
          newVar.add(vy);
        }
        vxVy.put(idxInit, idxY);
        vyVx.put(idxY, idxInit);
      }
      if (vects.isVy(object)) {
        final CtuluVariable vx = vects.getVx(object);
        int idxX = newVar.indexOf(vx);
        if (idxX < 0) {
          idxX = newVar.size();
          newVar.add(vx);
        }
        vxVy.put(idxX, idxInit);
        vyVx.put(idxInit, idxX);
      }
    }
    // les nouveaux parametres: le dernier parametre � true indique que les nouveaux parametres g�rent les donn�es
    // vectorielles.
    final InterpolationParameters res = new InterpolationParameters(newVar, _params.getTarget(),
        new InterpolationSupportVectorDecorator(_params.getValues(), vects), null, true);
    res.setResults(resHolder);
    return res;
  }

  final int nbInitValues_;
  final InterpolationResultsHolderI res_;
  final int[] vectIdxInInit_;
  final InterpolationVectorContainer vectors_;
  final TIntIntHashMap vxVy_ = new TIntIntHashMap();
  final TIntIntHashMap vyVx_ = new TIntIntHashMap();

  private InterpolationResultsHolderVecteur(final InterpolationResultsHolderI _res,
      final InterpolationVectorContainer _vectors, final int _nbInitValues, final int[] _vectIdxInInit) {
    super();
    res_ = _res;
    vectors_ = _vectors;
    nbInitValues_ = _nbInitValues;
    vectIdxInInit_ = _vectIdxInInit;
  }

  @Override
  public int getNbPtResultSet() {
    return res_.getNbPtResultSet();
  }

  @Override
  public double[] getValueForVariable(final CtuluVariable _idxVariable) {
    return res_.getValueForVariable(_idxVariable);
  }

  @Override
  public double[] getValuesForPt(final int _ptIdx) {
    return res_.getValuesForPt(_ptIdx);
  }

  @Override
  public boolean isDone(final int _idxPt) {
    return res_.isDone(_idxPt);
  }

  @Override
  public void setResult(final int _idx, final double[] _values) {
    final double[] values = new double[nbInitValues_];
    System.arraycopy(_values, 0, values, 0, values.length);
    for (int i = 0; i < vectIdxInInit_.length; i++) {
      final int idxVect = vectIdxInInit_[i];
      final boolean isVx = vxVy_.contains(idxVect);
      double newValue = 0;
      if (isVx) {
        newValue = InterpolationVectorContainer.getVx(_values[idxVect], _values[vxVy_.get(idxVect)]);
      } else {
        newValue = InterpolationVectorContainer.getVy(_values[vyVx_.get(idxVect)], _values[idxVect]);
      }
      values[idxVect] = newValue;
    }
    res_.setResult(_idx, values);

  }

  @Override
  public void setResult(final int _idx, final int _alreadSetIdx) {
    res_.setResult(_idx, _alreadSetIdx);
  }
}
