/*
 *  @creation     31 mai 2005
 *  @modification $Date: 2007-06-11 13:03:29 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

import org.fudaa.ctulu.CtuluVariable;

/**
 * @author Fred Deniger
 * @version $Id: InterpolationResultsHolderI.java,v 1.2 2007-06-11 13:03:29 deniger Exp $
 */
public interface InterpolationResultsHolderI {

  /**
   * @param _idx l'indice du point pour la cible
   * @param _values les valeurs en ce point pour chaque valeur. Ce tableau est r�utilis� par l'interpolateur.
   */
  void setResult(int _idx, double[] _values);

  void setResult(int _idx, int _alreadSetIdx);

  double[] getValueForVariable(final CtuluVariable _idxVariable);

  boolean isDone(int _idxPt);

  int getNbPtResultSet();

  /**
   * Non protege en ecriture.
   * 
   * @return la table des r�sultats indice du point-> valeur pour chaque variable
   */
  double[] getValuesForPt(int _ptIdx);

}
