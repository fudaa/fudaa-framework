/*
 * @creation 4 f�vr. 2004
 * @modification $Date: 2007-06-05 08:57:42 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

/**
 * @author deniger
 * @version $Id: InterpolationSupportLocationI.java,v 1.1 2007-06-05 08:57:42 deniger Exp $
 */
public interface SupportLocationI {

  /**
   * @return le nombre de points
   */
  int getPtsNb();

  /**
   * @param _i indice
   * @return x en _i
   */
  double getPtX(int _i);

  /**
   * @param _i indice
   * @return y en _i
   */
  double getPtY(int _i);


}