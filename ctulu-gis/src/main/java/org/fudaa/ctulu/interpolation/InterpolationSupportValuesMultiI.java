/*
 * @creation 4 juin 07
 * @modification $Date: 2007-06-11 13:03:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;

/**
 * @author fred deniger
 * @version $Id: InterpolationSupportValuesMultiI.java,v 1.2 2007-06-11 13:03:29 deniger Exp $
 */
public interface InterpolationSupportValuesMultiI {

  CtuluCollectionDouble getValues(CtuluVariable _var);

}
