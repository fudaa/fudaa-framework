/*
 * @creation 30 mai 07
 * @modification $Date: 2007-06-05 08:57:43 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

import org.fudaa.ctulu.CtuluActivity;

/**
 * Un interpolateur.
 * @author fred deniger
 * @version $Id: Interpolator.java,v 1.1 2007-06-05 08:57:43 deniger Exp $
 */
public abstract class Interpolator implements CtuluActivity {

  /**
   * @see #interpolate(InterpolationParameters)
   */
  protected abstract boolean doInterpolate(InterpolationParameters _params);

  /**
   * Interpole les valeurs pour les points de la cible.
   * @param _params Les parametres d'interpolation, contenant la cible, le support, les resultats.
   * @return True : Si l'interpolation s'est correctement d�roul�e. False en cas d'�chec sur 1 ou
   * plusieurs points.
   */
  public final boolean interpolate(InterpolationParameters _params) {
    if (_params == null) return false;
    return doInterpolate(_params.createParametersForVect());
  }
}
