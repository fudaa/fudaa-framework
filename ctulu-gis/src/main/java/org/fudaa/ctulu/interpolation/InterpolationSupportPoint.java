/*
 * @creation     22 avr. 2005
 * @modification $Date: 2007-06-11 13:03:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.gis.GISPoint;

/**
 * @author Fred Deniger
 * @version $Id: InterpolationSupportPoint.java,v 1.2 2007-06-11 13:03:29 deniger Exp $
 */
public class InterpolationSupportPoint implements SupportLocationI, InterpolationSupportValuesI {

  final GISPoint[] list_;

  public InterpolationSupportPoint(final GISPoint[] _list) {
    list_ = _list;
  }

  @Override
  public int getPtsNb() {
    return list_.length;
  }

  @Override
  public double getPtX(final int _i) {
    return list_[_i].getX();
  }

  @Override
  public double getPtY(final int _i) {
    return list_[_i].getY();
  }

  @Override
  public double getV(CtuluVariable _var, int _ptIdx) {
    return list_[_ptIdx].getZ();
  }

}
