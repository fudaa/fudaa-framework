/*
 * @creation 29 mai 07
 * @modification $Date: 2007-06-11 13:03:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

import org.fudaa.ctulu.CtuluVariable;

/**
 * Une classe qui transforme les variable vx,vy en r, theta.
 * 
 * @author fred deniger
 * @version $Id: InterpolationSupportVectorDecorator.java,v 1.2 2007-06-11 13:03:29 deniger Exp $
 */
public class InterpolationSupportVectorDecorator implements InterpolationSupportValuesI {

  final InterpolationSupportValuesI support_;
  final InterpolationVectorContainer vect_;

  public InterpolationSupportVectorDecorator(final InterpolationSupportValuesI _support,
      final InterpolationVectorContainer _vect) {
    super();
    support_ = _support;
    vect_ = _vect;
  }

  @Override
  public double getV(CtuluVariable _var, int _ptIdx) {
    if (vect_.isVx(_var)) {
      CtuluVariable vy = vect_.getVy(_var);
      return InterpolationVectorContainer.getNorme(support_.getV(_var, _ptIdx), support_.getV(vy, _ptIdx));
    }
    if (vect_.isVy(_var)) {
      CtuluVariable vx = vect_.getVx(_var);
      return InterpolationVectorContainer.getTheta(support_.getV(vx, _ptIdx), support_.getV(_var, _ptIdx));
    }

    return support_.getV(_var, _ptIdx);
  }
}
