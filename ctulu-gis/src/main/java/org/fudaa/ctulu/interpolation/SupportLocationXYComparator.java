/*
 * @creation     16 juil. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.interpolation;

import java.util.Comparator;

/**
 * Un comparateur de coordonn�es XY d'un support. Utilis� pour trier.
 * @author Bertrand Marchand
 * @version $Id:$
 */
public class SupportLocationXYComparator implements Comparator<Integer> {

  final SupportLocationI srcData_;

  /**
   * @param _data
   */
  public SupportLocationXYComparator(final SupportLocationI _data) {
    super();
    srcData_ = _data;
  }

  /**
   * Compare les coordonn�es d'un point _idx avec les coordonn�es _x2, y2.
   * @param _idx Le point a comparer.
   * @param _x2 Le x a comparer.
   * @param _y2 Le y a comparer.
   * @return -1 : Si point _idx a coordonn�e X inf�rieur. 1 : Si point a coordonn�e X sup�rieur.
   *         0 : Si m�me coordonn�es.
   */
  public int compare(final int _idx, final double _x2, final double _y2) {
    final double x1 = srcData_.getPtX(_idx);
    if (x1 < _x2) {
      return -1;
    }
    if (x1 > _x2) {
      return 1;
    }
    final double y1 = srcData_.getPtY(_idx);
    if (y1 < _y2) {
      return -1;
    }
    if (y1 > _y2) {
      return 1;
    }
    return 0;
  }

  public int compare(final int _o1, final int _o2) {
    if (_o1 == _o2) {
      return 0;
    }
    return compare(_o1, srcData_.getPtX(_o2), srcData_.getPtY(_o2));
  }

  @Override
  public int compare(final Integer _o1, final Integer _o2) {
    return compare(_o1.intValue(),_o2.intValue());
  }

}