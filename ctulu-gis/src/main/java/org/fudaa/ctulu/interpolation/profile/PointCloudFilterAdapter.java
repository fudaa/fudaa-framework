/*
 * @creation     23 sept. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.interpolation.profile;

/**
 * Un filtre de nuage de point. Ne r�cup�rer du nuage que les indices pass�s. Les indices
 * des points peuvent provenir d'un filtrage par fenetre de selection.
 * 
 * @author Bertrand Marchand
 * @version $Id$
 */
public class PointCloudFilterAdapter implements PointCloudI {
  /** Les indexes des seuls points � utiliser */
  int[] indexes_;
  /** Le nuage de points initial */
  PointCloudI initialCloud_;

  /**
   * Construteur.
   * @param _initialCloud Le nuage initial, non modifi�, non dupliqu�.
   * @param _idx Les indices des points sur ce nuage.
   */
  public PointCloudFilterAdapter(PointCloudI _initialCloud, int[] _idx) {
    initialCloud_=_initialCloud;
    indexes_=_idx;
  }
  
  @Override
  public int getNbPoints() {
    return indexes_.length;
  }

  @Override
  public double getX(int _idx) {
    return initialCloud_.getX(indexes_[_idx]);
  }

  @Override
  public double getY(int _idx) {
    return initialCloud_.getY(indexes_[_idx]);
  }

  @Override
  public double getZ(int _idx) {
    return initialCloud_.getZ(indexes_[_idx]);
  }
}
