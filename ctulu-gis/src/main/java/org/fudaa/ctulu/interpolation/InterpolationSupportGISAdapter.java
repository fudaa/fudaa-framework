/*
 * @creation     31 mai 2005
 * @modification $Date: 2007-06-11 13:03:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Point;
import gnu.trove.TObjectIntHashMap;
import java.util.ArrayList;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISCoordinateSequenceContainerInterface;
import org.fudaa.ctulu.gis.GISDataModel;

/**
 * Un adapteur pour un support d'interpolation a partir d'un modele contenant des
 * g�om�tries.
 * 
 * @author Fred Deniger
 * @version $Id: InterpolationSupportGISAdapter.java,v 1.2 2007-06-11 13:03:29 deniger Exp $
 */
public class InterpolationSupportGISAdapter implements SupportLocationI, InterpolationSupportValuesI {

  final GISDataModel model_;
  /** Une table qui associe un attribut a un indice sur le modele */
  final TObjectIntHashMap attInt_;
  /** Si le modele ne contient que des points, optimisation du support */
  final boolean onlyPoints_;
  /** Si le modele continet autre chose que des points */
  int[][] idx2geom_;
 
  /**
   * Construction de l'adapteur a partir d'un modele g�om�trique. Si le modele n'est constitu� que
   * de Point, le support est optimis�.
   * @param _model un model contenant des g�om�tries.
   */
  public InterpolationSupportGISAdapter(final GISDataModel _model) {
    model_ = _model;
    int nbAttributes = model_.getNbAttributes();
    attInt_ = new TObjectIntHashMap(nbAttributes);
    for (int i = 0; i < nbAttributes; i++) {
      attInt_.put(model_.getAttribute(i), i);
    }

    onlyPoints_=containsOnlyPoints();
    if (!onlyPoints_) initIndexes();
  }
  
  /**
   * Retourne vrai si le modele ne contient que des points.
   * @return True : Si vide ou uniquement des points.
   */
  public boolean containsOnlyPoints() {
    int nb=model_.getNumGeometries();
    for (int i=0; i<nb; i++) {
      if (!(model_.getGeometry(i) instanceof Point)) return false;
    }
    return true;
  }
  
  private void initIndexes() {
    ArrayList<int[]> vidx2geom=new ArrayList<int[]>();

    int nb=model_.getNumGeometries();
    for (int i=0; i<nb; i++) {
      Geometry g=model_.getGeometry(i);
      int nbpts=g.getNumPoints();
      for (int j=0; j<nbpts; j++) {
        vidx2geom.add(new int[]{i,j});
      }
    }
    idx2geom_=vidx2geom.toArray(new int[0][0]);
  }

  @Override
  public int getPtsNb() {
    int nb;

    if (onlyPoints_)
      nb=model_.getNumGeometries();
    else
      nb=idx2geom_.length;
    
    return nb;
  }

  @Override
  public double getPtX(final int _i) {
    if (onlyPoints_) {
      return ((Point)model_.getGeometry(_i)).getX();
    }
    else {
      CoordinateSequence seq=
        ((GISCoordinateSequenceContainerInterface)model_.getGeometry(idx2geom_[_i][0])).getCoordinateSequence();
      return seq.getX(idx2geom_[_i][1]);
    }
  }

  @Override
  public double getPtY(final int _i) {
    if (onlyPoints_) {
      return ((Point)model_.getGeometry(_i)).getY();
    }
    else {
      CoordinateSequence seq=
        ((GISCoordinateSequenceContainerInterface)model_.getGeometry(idx2geom_[_i][0])).getCoordinateSequence();
      return seq.getY(idx2geom_[_i][1]);
    }
  }

  /*
   * _var Un attribut pour lequel trouver la valeur. L'attribut peut �tre atomique ou
   * global, mais doit �tre de type GISAttributDouble.
   */
  @Override
  public double getV(CtuluVariable _var, int _i) {
    if (!(_var instanceof GISAttributeDouble))
      throw new IllegalArgumentException("Bad argument _var");
    
    double val;

    if (onlyPoints_) {
      val=model_.getDoubleValue(attInt_.get(_var), _i);
    }
    else {
      GISAttributeDouble att=(GISAttributeDouble)_var;
      if (att.isAtomicValue()) {
        Object vals=model_.getValue(attInt_.get(_var),idx2geom_[_i][0]);
        if (vals instanceof CtuluCollectionDouble) {
          val=((CtuluCollectionDouble)vals).getValue(idx2geom_[_i][1]);
        }
        else {
          val=0;
        }
      }
      else {
        val=model_.getDoubleValue(attInt_.get(_var),idx2geom_[_i][0]);
      }
    }
    
    return val;
  }
}
