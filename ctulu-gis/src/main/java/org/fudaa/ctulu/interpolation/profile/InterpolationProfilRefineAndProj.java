/*
 * @creation     27 Mars 2009
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.interpolation.profile;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.gis.GISAttributeConstants;
import org.fudaa.ctulu.gis.GISAttributeInterface;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISPoint;
import org.fudaa.ctulu.gis.GISZoneCollectionPoint;
import org.fudaa.ctulu.interpolation.InterpolationParameters;
import org.fudaa.ctulu.interpolation.InterpolationResultsHolderI;
import org.fudaa.ctulu.interpolation.InterpolationSupportValuesI;
import org.fudaa.ctulu.interpolation.InterpolationTarget;
import org.fudaa.ctulu.interpolation.InterpolationTargetGISAdapter;
import org.fudaa.ctulu.interpolation.SupportLocationI;
import org.fudaa.ctulu.interpolation.bilinear.InterpolatorBilinear;

/**
 * Un algorithme de cr�ation d'un profil par rafinement puis projection sur un nuage de points. L'algorithme prend en entr�e
 * une trace de profil et un nuage de points.
 * L'algo proc�de comme suit:<p>
 * <ol>
 * <li>Ajout de points sur le profil par rafinement</li>
 * <li>Projection sur le nuage de points de ces nouveaux points cr��s</li>
 * <li>Cr�ation des points 3D en retour.</li>
 * </ol>
 * 
 * @author Bertrand Marchand
 * @version $Id$
 */
public class InterpolationProfilRefineAndProj {
  
  /**
   *  Une classe pour adapter le nuage de points � un support d'interpolation.
   * @author Bertrand Marchand
   * @version $Id$
   */
  class SupportCloudAdapter implements SupportLocationI, InterpolationSupportValuesI {
    PointCloudI cloud_;
    public SupportCloudAdapter(PointCloudI _cloud) {
      cloud_=_cloud;
    }
    @Override
    public double getPtX(int _i) {
      return cloud_.getX(_i);
    }
    @Override
    public double getPtY(int _i) {
      return cloud_.getY(_i);
    }
    @Override
    public int getPtsNb() {
      return cloud_.getNbPoints();
    }
    @Override
    public double getV(CtuluVariable _var, int _i) {
      return cloud_.getZ(_i);
    }
  }

  /** Les points de la trace. */
  private GISPoint[] ptsTrace_;
  /** Le nuage de points */
  private PointCloudI cloud_;

  /**
   * Instance de l'algorithme.
   * @param _ptsTrace
   * @param _cloud
   */
  public InterpolationProfilRefineAndProj(GISPoint[] _ptsTrace, PointCloudI _cloud) {
    ptsTrace_=_ptsTrace;
    cloud_=_cloud;
  }

  /**
   * Lancement de l'algorithme.
   * @param _ana L'analyseur du processus.
   * @return Les points du profil. Peut retourner null si des erreurs ont �t� constat�es.
   */
  public GISPoint[] process(CtuluAnalyze _ana) {
    
    // Raffinement \\
    
    // Cr�ation des points interm�diaires entre 2 points. Le nombre de points total est d�fini � 30.
    Coordinate[] coords=new Coordinate[ptsTrace_.length];
    for (int i=0; i<ptsTrace_.length; i++) 
      coords[i]=ptsTrace_[i].getCoordinate();
    CoordinateSequence seq=GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(coords);
    seq=GISLib.refine(seq, 0, ptsTrace_.length-1, 2, 28, 0);
    
    // Interpolation \\
    
    // Cr�ation de la cible d'interpolation (les points du profil)
    GISZoneCollectionPoint zpts=new GISZoneCollectionPoint(seq.size(),null);
    for (int i=0; i<seq.size(); i++) 
      zpts.addGeometry(GISGeometryFactory.INSTANCE.createPoint(seq.getCoordinate(i)),null,null);
    InterpolationTarget target=new InterpolationTargetGISAdapter(zpts);

    // Cr�ation du support (nuage de points)
    SupportCloudAdapter support=new SupportCloudAdapter(cloud_);
    
    // Cr�ation des variables a interpoler (BATHY et c'est tout)
    List<GISAttributeInterface> vars=new ArrayList<GISAttributeInterface>();
    vars.add(GISAttributeConstants.BATHY);
    
    // Interpolation
    InterpolationParameters params=new InterpolationParameters(vars,target,support);
    InterpolatorBilinear interp=new InterpolatorBilinear(support);
    interp.interpolate(params);
    
    // Erreurs => On ne va pas plus loin
    _ana.merge(params.getAnalyze());
    if (_ana.containsErrors()) 
      return null;

    // R�cup�ration des r�sultats, et transfert aux Z des points profils.
    GISPoint[] pts=new GISPoint[seq.size()];
    InterpolationResultsHolderI res=params.getResults();
    for (int i=0; i<seq.size(); i++) {
      pts[i]=((GISPoint)zpts.getGeometry(i));
      pts[i].setZ(res.getValuesForPt(i)[0]);
    }

    return pts;
  }
}
