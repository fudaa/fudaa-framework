/*
 * @creation 30 mai 07
 * @modification $Date: 2007-06-11 13:03:29 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleAbstract;

/**
 * @author fred deniger
 * @version $Id: InterpolationCollectionAdapter.java,v 1.2 2007-06-11 13:03:29 deniger Exp $
 */
public abstract class InterpolationCollectionAdapter extends CtuluCollectionDoubleAbstract {
  final CtuluCollectionDouble vx_;
  final CtuluCollectionDouble vy_;

  public InterpolationCollectionAdapter(final CtuluCollectionDouble _vx, final CtuluCollectionDouble _vy) {
    vx_ = _vx;
    vy_ = _vy;
    if (vx_ == null) throw new IllegalArgumentException("vx_ is null");
    if (vy_ == null) throw new IllegalArgumentException("vy_ is null");
  }

  @Override
  public int getSize() {
    return vx_.getSize();
  }

  public static class Norm extends InterpolationCollectionAdapter {

    public Norm(CtuluCollectionDouble _vx, CtuluCollectionDouble _vy) {
      super(_vx, _vy);
    }

    @Override
    public double getValue(int _i) {
      return InterpolationVectorContainer.getNorme(vx_.getValue(_i), vy_.getValue(_i));
    }
  }
  public static class Theta extends InterpolationCollectionAdapter {

    public Theta(CtuluCollectionDouble _vx, CtuluCollectionDouble _vy) {
      super(_vx, _vy);
    }

    @Override
    public double getValue(int _i) {
      return InterpolationVectorContainer.getTheta(vx_.getValue(_i), vy_.getValue(_i));
    }
  }
}
