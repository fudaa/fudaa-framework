/*
 * @creation     22 avr. 2005
 * @modification $Date: 2007-06-11 13:03:29 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

import org.locationtech.jts.geom.Coordinate;
import org.fudaa.ctulu.CtuluVariable;

/**
 * Un support de location � partir de coordonn�es {@link Coordinate}
 * @author Bertrand Marchand
 * @version $Id: InterpolationSupportPoint.java,v 1.2 2007-06-11 13:03:29 deniger Exp $
 */
public class SupportCoordinate implements SupportLocationI, InterpolationSupportValuesI {

  final Coordinate[] list_;

  public SupportCoordinate(final Coordinate[] _list) {
    list_ = _list;
  }

  @Override
  public int getPtsNb() {
    return list_.length;
  }

  @Override
  public double getPtX(final int _i) {
    return list_[_i].x;
  }

  @Override
  public double getPtY(final int _i) {
    return list_[_i].y;
  }

  @Override
  public double getV(CtuluVariable _var, int _ptIdx) {
    /* _var n'est pas utilis�e. */
    return list_[_ptIdx].z;
  }

}
