/*
 * creation  29/08/2008
 * license   GNU General Public License 2
 * copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * mail      devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation.profile;

import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.fudaa.ctulu.gis.GISLib;
import org.fudaa.ctulu.gis.GISPoint;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LinearRing;

/**
 * D�finition de la fen�tre rectangulaire dans laquelle on s�lectionne les points � utiliser
 * pour le calcul du profil.
 *
 * @author J.B. Faure
 * @author Bertrand Marchand
 * @version $Id$
 */
public class ProfileCalculatorWindow {
  /**
   * Les points repr�sentant la fenetre
   */
  GISPoint[] points_;
  /**
   * Le tester associ� au rectangle de selection
   */
  PointOnGeometryLocator tester_;
  /**
   * Pour optimiser la vitesse de traitement
   */
  Coordinate ctmp_;

  /**
   * Constructeur, a partir des 4 points formant un rectangle.
   *
   * @param _points Les points du rectangle.
   */
  public ProfileCalculatorWindow(GISPoint[] _points) {

    // TODO : Verifier que les 4 points forment bien un rectangle.
    Coordinate[] coords = new Coordinate[5];
    for (int i = 0; i < _points.length; i++) {
      coords[i] = (Coordinate) _points[i].getCoordinate().clone();
    }
    coords[4] = coords[0]; // Ferme le polygone.

    // Pour pouvoir tester qu'un point est interne au rectangle.
    LinearRing pl = GISGeometryFactory.INSTANCE.createLinearRing(coords);
    tester_ = GISLib.getTester(new LinearRing[]{pl})[0];
    ctmp_ = new Coordinate();

    points_ = _points;
  }

  /**
   * @param _idx L'index du sommet, de 0 � 3.
   * @return Retourne le coin d'index _idx
   */
  public GISPoint getCorner(int _idx) {
    return points_[_idx];
  }

  /**
   * V�rifie si le point pass� en argument se trouve � l'int�rieur de la fen�tre
   *
   * @param _x La coordonn�e X
   * @param _y La coordonn�e Y
   * @return True si point � l'interieur.
   */
  public boolean isInside(double _x, double _y) {
    ctmp_.x = _x;
    ctmp_.y = _y;
    return GISLib.isInside(tester_, ctmp_);
  }

  /**
   * G�n�re un nouveau nuage de point en s�lectionnant ceux qui sont dans la fen�tre
   *
   * @param _cloud Le nuage de points initial
   * @return Le nouveau nuage de points.
   */
  public PointCloudI select(PointCloudI _cloud) {
    TIntArrayList idx = new TIntArrayList();
    int nb = _cloud.getNbPoints();
    for (int i = 0; i < nb; i++) {
      if (isInside(_cloud.getX(i), _cloud.getY(i))) {
        idx.add(i);
      }
    }
    return new PointCloudFilterAdapter(_cloud, idx.toNativeArray());
  }
}
