/*
 * @creation 25 mai 07
 * @modification $Date: 2007-06-05 08:57:44 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation;

import java.util.List;
import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluPermanentList;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * @author fred deniger
 * @version $Id: InterpolationParameters.java,v 1.1 2007-06-05 08:57:44 deniger Exp $
 */
public class InterpolationParameters {
  private double eps=1E-2;
  public double getEps()
  {
    return eps;
  }

  public void setEps(double eps)
  {
    this.eps = eps;
  }

  private final InterpolationTarget target_;
  final InterpolationVectorContainer vects_;
  private InterpolationResultsHolderI results_;
  private CtuluPermanentList variable_;
  private final CtuluAnalyze analyze_ = new CtuluAnalyze();
  private ProgressionInterface prog_;
  private final InterpolationSupportValuesI values_;
  private final boolean isVectSafe_;

  public InterpolationParameters(List _var, InterpolationTarget _target, InterpolationSupportValuesI _values) {
    this(_var, _target, _values, null, false);
  }

  public InterpolationParameters(List _var, InterpolationTarget _target, InterpolationSupportValuesI _values,
      InterpolationVectorContainer _vects) {
    this(_var, _target, _values, _vects, false);
  }

  InterpolationParameters(List _var, InterpolationTarget _target, InterpolationSupportValuesI _values,
      InterpolationVectorContainer _vects, boolean _vectSafe) {
    variable_ = new CtuluPermanentList(_var);
    values_ = _values;
    target_ = _target;
    vects_ = _vects;
    isVectSafe_ = _vectSafe;
  }

  public InterpolationResultsHolderI getResults() {
    return results_;
  }

  public InterpolationTarget getTarget() {
    return target_;
  }

  public void createDefaultResults() {
    if (results_ == null) results_ = new InterpolationResultsHolder(variable_, target_.getPtsNb());
  }

  public CtuluPermanentList getVariable() {
    return variable_;
  }

  public CtuluAnalyze getAnalyze() {
    return analyze_;
  }

  public ProgressionInterface getProg() {
    return prog_;
  }

  public void setProg(ProgressionInterface _prog) {
    prog_ = _prog;
  }

  public InterpolationSupportValuesI getValues() {
    return values_;
  }

  public void setResults(InterpolationResultsHolderI _results) {
    results_ = _results;
  }

  public InterpolationParameters createParametersForVect() {
    if (vects_ == null || isVectSafe_) return this;
    return InterpolationResultsHolderVecteur.createResultHolder(this);
  }
}
