/*
 * @creation     29 janv. 2004
 * @modification $Date: 2007-06-05 08:57:42 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.interpolation.bilinear;

import java.util.Arrays;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.interpolation.SupportLocationI;
import org.fudaa.ctulu.interpolation.SupportLocationXYComparator;

/**
 * Un classe representant une source pour une interpolation des plus proches voisins.
 * 
 * @author deniger
 * @version $Id: InterpolationBilinearSupportSorted.java,v 1.1 2007-06-05 08:57:42 deniger Exp $
 */
public final class InterpolationBilinearSupportSorted {

  public static InterpolationBilinearSupportSorted buildSortedSrc(final SupportLocationI _src,
      final ProgressionInterface _prog) {
    final Integer[] idxs = new Integer[_src.getPtsNb()];
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    up.setValue(4, idxs.length, 0, 80);
    for (int i = idxs.length - 1; i >= 0; i--) {
      idxs[i] = new Integer(i);
      up.majAvancement();
    }
    Arrays.sort(idxs, new SupportLocationXYComparator(_src));
    final int[] sortedIdx = new int[idxs.length];
    for (int i = idxs.length - 1; i >= 0; i--) {
      sortedIdx[i] = idxs[i].intValue();
    }
    return new InterpolationBilinearSupportSorted(_src, sortedIdx);

  }

  final private SupportLocationXYComparator comp_;
  SupportLocationI ref_;

  final int[] sortedIdx_;

  private InterpolationBilinearSupportSorted(final SupportLocationI _ref, final int[] _sorted) {
    ref_ = _ref;
    sortedIdx_ = _sorted;
    comp_ = new SupportLocationXYComparator(ref_);
  }

  private int searchIdx(final double _x, final double _y) {
    int low = 0;
    int high = sortedIdx_.length - 1;

    while (low <= high) {
      final int mid = (low + high) >> 1;
      final int midVal = sortedIdx_[mid];
      final int cmp = comp_.compare(midVal, _x, _y);

      if (cmp < 0) {
        low = mid + 1;
      } else if (cmp > 0) {
        high = mid - 1;
      } else {
        return mid; // key found
      }
    }
    return -(low + 1); // key not found.
  }

  /**
   * Remplit le tableau <code>_fourIndexToSet</code> ( qui doit avoir une taille d'au moins 4) avec les indices des
   * points les plus proches de (_x,_y) dans les 4 quadrants. Si aucun trouve, l'indice sera mis a -1 L'ordre est le
   * suivant:<br>
   * 
   * <pre>
   *    3  | 2
   *   ----|----
   *    0  | 1
   * </pre>
   * 
   * @param _x le x demande
   * @param _y le y
   * @param _fourIndexToSet tableau de 4 a remplir.
   * @return false si le tableau a une taille <4
   */
  public boolean getQuadrantIdx(final double _x, final double _y, final int[] _fourIndexToSet) {
    if ((_fourIndexToSet == null) || (_fourIndexToSet.length < 4)) {
      return false;
    }
    Arrays.fill(_fourIndexToSet, -1);
    double d0 = Double.MAX_VALUE;
    double d1 = Double.MAX_VALUE;
    double d2 = Double.MAX_VALUE;
    double d3 = Double.MAX_VALUE;
    int idx = searchIdx(_x, _y);
    if (idx < 0) {
      idx = -idx - 1;
    }
    // recherche des points 1 et 2
    final int max = ref_.getPtsNb();
    boolean p1found = false;
    boolean p1foundOne = false;
    boolean p2found = false;
    boolean p2foundOne = false;
    for (int i = idx; i < max; i++) {
      final int realIdx = sortedIdx_[i];
      final double xref = ref_.getPtX(realIdx);
      final double yref = ref_.getPtY(realIdx);
      final double dx = xref - _x;
      final double dy = yref - _y;
      final double dxdx = dx * dx;
      // p1
      if (dy <= 0) {
        if (p1foundOne && dxdx > d1) {
          p1found = true;
        } else {
          final double dist = dxdx + dy * dy;
          if (dist < d1) {
            p1foundOne = true;
            d1 = dist;
            _fourIndexToSet[1] = realIdx;
          }
        }

      }
      // p2
      else {
        if (p2foundOne && dxdx > d2) {
          p2found = true;
        } else {
          final double dist = dxdx + dy * dy;
          if (dist < d2) {
            p2foundOne = true;
            d2 = dist;
            _fourIndexToSet[2] = realIdx;
          }
        }

      }
      if (p1found && p2found) {
        break;
      }
    }
    boolean p0found = false;
    boolean p0foundOne = false;
    boolean p3found = false;
    boolean p3foundOne = false;
    for (int i = idx - 1; i >= 0; i--) {
      final int realIdx = sortedIdx_[i];
      final double xref = ref_.getPtX(realIdx);
      final double yref = ref_.getPtY(realIdx);
      final double dx = xref - _x;
      final double dy = yref - _y;
      final double dxdx = dx * dx;
      // p0
      if (dy <= 0) {
        if (p0foundOne && dxdx > d0) {
          p0found = true;
        } else {
          final double dist = dxdx + dy * dy;
          if (dist < d0) {
            p0foundOne = true;
            d0 = dist;
            _fourIndexToSet[0] = realIdx;
          }
        }

      }
      // p3
      else {
        if (p3foundOne && dxdx > d3) {
          p3found = true;
        } else {
          final double dist = dxdx + dy * dy;
          if (dist < d3) {
            p3foundOne = true;
            d3 = dist;
            _fourIndexToSet[3] = realIdx;
          }
        }

      }
      if (p0found && p3found) {
        break;
      }
    }

    return true;
  }
}
