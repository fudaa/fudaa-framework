/*
 * @creation     23 sept. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.interpolation.profile;

import org.fudaa.ctulu.gis.GISPoint;

/**
 * Un nuage de point bas� sur un tableau de GISPoint.
 * L'adapter permet d'acceder aux informations du nuage sans toucher a la structure des
 * donn�es manipul�es.
 *
 * @author Bertrand Marchand
 * @version $Id$
 */
public class PointCloudPointsAdapter implements PointCloudI {
  GISPoint[] pts_;
  
  /**
   * Un nuage de point construit a partir d'un tableau de points.
   * @param _pts Les points.
   */
  public PointCloudPointsAdapter(GISPoint[] _pts) {
    pts_=_pts;
  }

  @Override
  public int getNbPoints() {
    return pts_.length;
  }

  @Override
  public double getX(int _idx) {
    return pts_[_idx].getX();
  }
  @Override
  public double getY(int _idx) {
    return pts_[_idx].getY();
  }
  @Override
  public double getZ(int _idx) {
    return pts_[_idx].getZ();
  }
}
