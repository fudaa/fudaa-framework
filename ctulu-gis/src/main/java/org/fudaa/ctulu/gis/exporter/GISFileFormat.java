package org.fudaa.ctulu.gis.exporter;

import com.memoire.bu.BuFileFilter;
import org.geotools.wfs.GML;
import org.geotools.data.FileDataStoreFactorySpi;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;

public enum GISFileFormat {
  GML_V2("gml", null, GML.Version.GML2),
  GML_V3("gml", null, GML.Version.GML3),
  SHP("shp", new ShapefileDataStoreFactory(), null);
  private final String extension;
  private final FileDataStoreFactorySpi dataStoreFactorySpi;
  private final GML.Version gmlVersion;

  GISFileFormat(String extension, FileDataStoreFactorySpi dataStoreFactorySpi, GML.Version gmlVersion) {
    this.extension = extension;
    this.dataStoreFactorySpi = dataStoreFactorySpi;
    this.gmlVersion = gmlVersion;
  }

  public boolean isGML() {
    return gmlVersion != null;
  }

  public GML.Version getGmlVersion() {
    return gmlVersion;
  }

  public FileDataStoreFactorySpi getDataStoreFactorySpi() {
    return dataStoreFactorySpi;
  }

  public String getExtension() {
    return extension;
  }

  public BuFileFilter createFileFilter() {
    final BuFileFilter buFileFilter = new BuFileFilter(extension);
    if (isGML()) {
      buFileFilter.setDescription(gmlVersion.toString());
    }else{
      buFileFilter.setDescription(dataStoreFactorySpi.getDescription());
    }
    return buFileFilter;
  }
}
