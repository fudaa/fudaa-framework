/*
 * @creation 25 juin 2003
 *
 * @modification $Date: 2006-07-13 13:34:35 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Point;

/**
 * @author deniger
 * @version $Id: GISPoint.java,v 1.5 2006-07-13 13:34:35 deniger Exp $
 */
public class GISPoint extends Point implements GISCoordinateSequenceContainerInterface {
  protected long id_ = GISGeometryFactory.INSTANCE.getNewId();

  @Override
  public long getId() {
    return id_;
  }

  /**
   * @param _p1 le point 1
   * @param _p2 le point 2
   * @return le milieu de _p1,_p2
   */
  public static GISPoint createMilieu(final GISPoint _p1, final GISPoint _p2) {
    final GISPoint r = new GISPoint();
    createMilieu(_p1, _p2, r);
    return r;
  }

  /**
   * @param _n1 le point 1
   * @param _n2 le point 2
   * @param _target le point a modifier
   */
  public static void createMilieu(final GISPoint _n1, final GISPoint _n2, final GISPoint _target) {
    _target.setXYZ((_n1.getX() + _n2.getX()) / 2, (_n1.getY() + _n2.getY()) / 2, (_n1.getZ() + _n2.getZ()) / 2);
  }

  @Override
  public boolean accept(final GISVisitor _visitor) {
    return _visitor.visitPoint(this);
  }

  /**
   * (0,0,0).
   */
  public GISPoint() {
    this(0, 0, 0);
  }

  @Override
  protected Point copyInternal() {
    return new GISPoint(getX(), getY(), getZ());
  }

  /**
   * @param _x le x du point
   * @param _y y
   * @param _z z
   */
  public GISPoint(final double _x, final double _y, final double _z) {
    super(new GISCoordinateSequenceUnique(_x, _y, _z), GISGeometryFactory.INSTANCE);
  }

  public GISPoint(final Coordinate _c) {
    super(new GISCoordinateSequenceUnique(_c.x, _c.y, _c.z), GISGeometryFactory.INSTANCE);
  }

  /**
   * @param _p le point source pour l'initialisation.
   */
  public GISPoint(final GISPoint _p) {
    super(new GISCoordinateSequenceUnique(_p.getX(), _p.getY(), _p.getZ()), GISGeometryFactory.INSTANCE);
  }

  @Override
  public GISPoint createTranslateGeometry(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    return new GISPoint(getX() + xyToAdd.getX(), getY() + xyToAdd.getY(), getZ());
  }

  protected void setX(final double _x) {
    super.getCoordinateSequence().setOrdinate(0, CoordinateSequence.X, _x);
  }

  /**
   * @param _x le nouveau x
   * @param _y le nouveau y
   * @param _z le nouveau z
   */
  protected void setXYZ(final double _x, final double _y, final double _z) {
    setX(_x);
    setY(_y);
    setZ(_z);
  }

  protected void setY(final double _y) {
    super.getCoordinateSequence().setOrdinate(0, CoordinateSequence.Y, _y);
  }

  public void setZ(final double _z) {
    super.getCoordinateSequence().setOrdinate(0, CoordinateSequence.Z, _z);
  }

  /**
   * @param _seg1 point 1 du segment
   * @param _seg2 point 2 du segment
   * @return distance XY entre ce point et le segment.
   */
  public final double distanceXYFromSeg(final GISPoint _seg1, final GISPoint _seg2) {
    return CtuluLibGeometrie.distanceFromSegment(_seg1.getX(), _seg1.getY(), _seg2.getX(), _seg2.getY(), getX(), getY());
  }

  /**
   * @param _p le point cible
   * @return distance xy entre ce point et le point _p
   */
  public double getDistanceXY(final GISPoint _p) {
    return CtuluLibGeometrie.getDistance(getX(), getY(), _p.getX(), _p.getY());
  }

  /**
   * @return x
   */
  @Override
  public final double getX() {
    return ((GISCoordinateSequenceUnique) getCoordinateSequence()).x_;
  }

  /**
   * @return y
   */
  @Override
  public final double getY() {
    return ((GISCoordinateSequenceUnique) getCoordinateSequence()).y_;
  }

  /**
   * @return �
   */
  public final double getZ() {
    return ((GISCoordinateSequenceUnique) getCoordinateSequence()).z_;
  }

  /**
   * Comparaison stricte en x,y,z : a eviter normalement.
   *
   * @param _p le point com
   * @return true si identique ( sans marge d'erreur)
   */
  public boolean isSame(final GISPoint _p) {
    return isSame(_p, 0);
  }

  /**
   * Compare les coordonnees X,Y,Z des points avec une marge de <code>_eps</code>.
   *
   * @param _p le point a comparer
   * @param _eps la marge en x,y et z.
   * @return true si identique en x,y modulo la marge.
   */
  public boolean isSame(final GISPoint _p, final double _eps) {
    return (Math.abs(getX() - _p.getX()) <= _eps) && (Math.abs(getY() - _p.getY()) <= _eps) && (Math.abs(getZ() - _p.getZ()) <= _eps);
  }

  /**
   * Comparaison stricte en x,y,z : a eviter normalement.
   *
   * @param _p le point com
   * @return true si identique ( sans marge d'erreur)
   */
  public boolean isSame(final Coordinate _p) {
    return isSame(_p, 0);
  }

  /**
   * Compare les coordonnees X,Y,Z des points avec une marge de <code>_eps</code>.
   *
   * @param _p le point a comparer
   * @param _eps la marge en x,y et z.
   * @return true si identique en x,y modulo la marge.
   */
  public boolean isSame(final Coordinate _p, final double _eps) {
    return (Math.abs(getX() - _p.x) <= _eps) && (Math.abs(getY() - _p.y) <= _eps) && (Math.abs(getZ() - _p.z) <= _eps);
  }

  /**
   * Compare les coordonnees X,Y des points avec une marge de <code>0</code>.
   *
   * @param _p le point a comparer
   * @return true si (x,y) est le meme.
   */
  public boolean isSameXY(final GISPoint _p) {
    return isSameXY(_p, 0d);
  }

  /**
   * Compare les coordonnees X,Y des points avec une marge de <code>_eps</code>.
   *
   * @param _p le point a comparer
   * @param _eps la marge en x et y
   * @return true si identique en x,y modulo la marge.
   */
  public boolean isSameXY(final GISPoint _p, final double _eps) {
    return (Math.abs(getX() - _p.getX()) <= _eps) && (Math.abs(getY() - _p.getY()) <= _eps);
  }

  /**
   * Compare les coordonnees X,Y des points avec une marge de <code>0</code>.
   *
   * @param _p le point a comparer
   * @return true si (x,y) est le meme.
   */
  public boolean isSameXY(final Coordinate _p) {
    return isSameXY(_p, 0d);
  }

  /**
   * Compare les coordonnees X,Y des points avec une marge de <code>_eps</code>.
   *
   * @param _p le point a comparer
   * @param _eps la marge en x et y
   * @return true si identique en x,y modulo la marge.
   */
  public boolean isSameXY(final Coordinate _p, final double _eps) {
    return (Math.abs(getX() - _p.x) <= _eps) && (Math.abs(getY() - _p.y) <= _eps);
  }
}
