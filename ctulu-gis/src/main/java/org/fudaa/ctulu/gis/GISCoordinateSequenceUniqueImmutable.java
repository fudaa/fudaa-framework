/*
 *  @creation     22 avr. 2005
 *  @modification $Date: 2006-09-19 14:36:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;

/**
 * @author Fred Deniger
 * @version $Id: GisCoordinateSequenceUniqueImmutable.java,v 1.6 2006-09-19 14:36:53 deniger Exp $
 */
public final class GISCoordinateSequenceUniqueImmutable extends GISCoordinateSequenceUnique {

  /**
   * @param _x
   * @param _y
   * @param _z
   */
  public GISCoordinateSequenceUniqueImmutable(final double _x, final double _y, final double _z) {
    super(_x, _y, _z);
  }

  public GISCoordinateSequenceUniqueImmutable() {
    super(0, 0, 0);
  }
  
  /**
   * @param _c
   */
  public GISCoordinateSequenceUniqueImmutable(final Coordinate _c) {
    super(_c);
  }
  
  @Override
  public  Object clone() {
    return new GISCoordinateSequenceUniqueImmutable(x_, y_, z_);
  }

  @Override
  public void setOrdinate(final int _index,final int _ordinateIndex,final double _value){
    FuLog.error("not supported");
  }
}
