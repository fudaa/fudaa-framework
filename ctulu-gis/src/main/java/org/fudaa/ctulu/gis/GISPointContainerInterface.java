/*
 *  @creation     11 f�vr. 2004
 *  @modification $Date: 2006-02-09 08:59:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

/**
 * Interface definissant des classes contenant des points.
 *
 * @author deniger
 * @version $Id: GISPointContainerInterface.java,v 1.3 2006-02-09 08:59:28 deniger Exp $
 */
public interface GISPointContainerInterface {

  /**
   * @param _listToFill la liste des points a remplir
   * @return true si points ajoutes a la liste
   */
  boolean fillListWithPoint(GISCollectionPointInteface _listToFill);
}
