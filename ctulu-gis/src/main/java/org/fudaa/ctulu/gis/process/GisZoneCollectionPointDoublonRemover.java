/*
 GPL 2
 */
package org.fudaa.ctulu.gis.process;

import org.locationtech.jts.geom.Coordinate;
import java.util.Map.Entry;
import java.util.TreeMap;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.gis.comparator.CoordinateComparator;
import org.fudaa.ctulu.interpolation.SupportLocationI;

/**
 *
 * Permet de construire une liste de points en s'assurant qu'il n'y ait pas de doublons dans les points.
 *
 * @author Frederic Deniger
 */
public class GisZoneCollectionPointDoublonRemover {

  private double eps = 1e-3;
  private final SupportLocationI in;

  public GisZoneCollectionPointDoublonRemover(SupportLocationI in) {
    this.in = in;
  }

  public GisZoneCollectionPointDoublonRemover(SupportLocationI in, double eps) {
    this.in = in;
    this.eps = eps;
  }

  public PointsMappingDefault createFiltered(ProgressionInterface prog) {
    ProgressionUpdater updater = new ProgressionUpdater(prog);
    updater.majProgessionStateOnly(CtuluLib.getS("Indexation des points"));
    TreeMap<Coordinate, Integer> indexs = new TreeMap<Coordinate, Integer>(new CoordinateComparator(eps));
    updater.setValue(10, in.getPtsNb());
    for (int i = 0; i < in.getPtsNb(); i++) {
      Coordinate c = new Coordinate(in.getPtX(i), in.getPtY(i));
      Integer index = indexs.get(c);
      //le point n'existe pas
      if (index == null) {
        indexs.put(c, i);
      }
      updater.majAvancement();
    }
    Coordinate[] coordinate = new Coordinate[indexs.size()];
    int[] correspondance = new int[coordinate.length];
    int idx = 0;
    updater.majProgessionStateOnly(CtuluLib.getS("Construction filtre"));
    updater.setValue(10, coordinate.length);
    for (Entry<Coordinate, Integer> entry : indexs.entrySet()) {
      coordinate[idx] = entry.getKey();
      correspondance[idx++] = entry.getValue();
      updater.majAvancement();
    }
    return new PointsMappingDefault(correspondance, in);
  }
}
