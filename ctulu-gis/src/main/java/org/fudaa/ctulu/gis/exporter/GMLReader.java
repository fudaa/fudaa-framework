package org.fudaa.ctulu.gis.exporter;

import org.fudaa.ctulu.gis.GISGeometryFactory;
import org.geotools.feature.AttributeTypeBuilder;
import org.geotools.feature.NameImpl;
import org.geotools.feature.type.SchemaImpl;
import org.geotools.gml2.GMLConfiguration;
import org.geotools.xsd.Configuration;
import org.geotools.xsd.Parser;
import org.geotools.xs.XS;
import org.geotools.xs.XSSchema;
import org.locationtech.jts.geom.CoordinateSequenceFactory;
import org.locationtech.jts.geom.GeometryFactory;
import org.opengis.feature.type.Schema;
import org.picocontainer.MutablePicoContainer;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * see http://docs.geotools.org/latest/userguide/library/xml/faq.html#q-for-wfs-why-does-parser-return-a-map
 */
public class GMLReader {
  private final GMLConfiguration gmlConfiguration;
  private final String gmlLocation;
  private final String gmlNamespace;
  private List<Schema> schemas = new ArrayList<>();

  public static class CustomGMLConfiguration extends org.geotools.gml2.GMLConfiguration {
    @Override
    public void configureContext(MutablePicoContainer container) {
      super.configureContext(container);
      container.unregisterComponent(CoordinateSequenceFactory.class);
      container.unregisterComponent(GeometryFactory.class);
      container.registerComponentInstance(CoordinateSequenceFactory.class, GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory());
      container.registerComponentInstance(GeometryFactory.class, GISGeometryFactory.INSTANCE);
    }
  }

  public GMLReader() {
    gmlNamespace = org.geotools.gml2.GML.NAMESPACE;
    gmlLocation = "gml/2.1.2/feature.xsd";
    gmlConfiguration = new CustomGMLConfiguration();
    schemas.add(new XSSchema().profile()); // encoding of common java types
    Schema hack = new SchemaImpl(XS.NAMESPACE);

    AttributeTypeBuilder builder = new AttributeTypeBuilder();
    builder.setName("date");
    builder.setBinding(Date.class);
    hack.put(new NameImpl(XS.DATETIME), builder.buildType());

    schemas.add(hack);
    schemas.add(new org.geotools.gml2.GMLSchema().profile());
  }

  public Map decode(InputStream in)
      throws IOException, SAXException, ParserConfigurationException {
    Configuration cfg = gmlConfiguration;
    Parser parser = new Parser(cfg);
    return (Map) parser.parse(in);
  }
}
