/*
 * @creation 11 f�vr. 2004
 * @modification $Date: 2006-09-19 14:36:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import gnu.trove.TDoubleArrayList;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.impl.PackedCoordinateSequence;

import java.util.Arrays;

/**
 * Demonstrates how to implement a CoordinateSequence for a new kind of coordinate (an
 * {@link Coordinate}in this example). In this implementation, Coordinates returned by #toArray and
 * #get are live -- parties that change them are actually changing the CoordinateSequence's
 * underlying data.
 *
 * @version 1.4
 */
public class GISCoordinateSequence extends PackedCoordinateSequence.Double {

  private static Coordinate[] transformTo3d(Coordinate[] in) {
    if (in == null) {
      return null;
    }
    Coordinate[] res = new Coordinate[in.length];
    for (int i = 0; i < res.length; i++) {
      if (in[i].getClass() == Coordinate.class) {
        res[i] = in[i];
      } else {
        res[i] = new Coordinate(in[i]);
      }
    }
    return res;
  }

  public GISCoordinateSequence() {
    super(new Coordinate[0]);
  }

  /**
   * Copy constructor -- simply aliases the input array, for better performance.
   */
  public GISCoordinateSequence(final Coordinate[] _coordinates) {
    super(transformTo3d(_coordinates));
  }

  public GISCoordinateSequence(final TDoubleArrayList _l) {
    super(_l.toNativeArray(), 3, 0);
    if (_l.size() % 3 != 0) {
      throw new IllegalArgumentException("size must be a multiple of 3");
    }
  }

  public GISCoordinateSequence(final double[] coords) {
    super(coords, 3, 0);
    if (coords.length % 3 != 0) {
      throw new IllegalArgumentException("size must be a multiple of 3");
    }
  }

  @Override
  public Double copy() {
    double[] coords = super.getRawCoordinates();
    double[] clone = Arrays.copyOf(coords, coords.length);
    return new GISCoordinateSequence(clone);
  }

  @Override
  public final Object clone() {
    return new GISCoordinateSequence(this);
  }

  /**
   * Constructor that makes a copy of a CoordinateSequence.
   */
  public GISCoordinateSequence(final CoordinateSequence _coordSeq) {
    super(transformTo3d(_coordSeq.toCoordinateArray()));
  }

  public void setZ(final int _i, final double _value) {
    coordRef = null;
    setOrdinate(_i, CoordinateSequence.Z, _value);
  }

  @Override
  public void setOrdinate(int index, int ordinate, double value) {
    if(ordinate==CoordinateSequence.Z && getDimension()<=2){
      throw new IllegalAccessError("Can set z for a 2 dimensions coordinate sequence");
    }
    super.setOrdinate(index, ordinate, value);
  }

  @Override
  public double getOrdinate(int index, int ordinate) {
    if(ordinate==CoordinateSequence.Z && getDimension()<=2){
      throw new IllegalAccessError("Can get z for a 2 dimensions coordinate sequence");
    }
    return super.getOrdinate(index, ordinate);
  }

  /**
   * Constructs a sequence of a given size, populated with new {@link Coordinate}s.
   *
   * @param _size the size of the sequence to create
   */
  public GISCoordinateSequence(final int _size) {
    super(_size, 3, 0);
  }
  public GISCoordinateSequence(final int _size,int dimension) {
    super(_size, dimension, 0);
  }
}
