/*
 * @creation 19 mai 2005
 *
 * @modification $Date: 2008-03-28 14:59:28 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis.exporter;

import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelToFeatureCollection;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.geotools.wfs.GML;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Fred Deniger
 * @version $Id: GISGMLZoneExporter.java,v 1.1.6.1 2008-03-28 14:59:28 bmarchan Exp $
 */
public class GISGMLZoneExporter implements CtuluActivity {
  boolean stop_;
  GISDataModelToFeatureCollection converter = new GISDataModelToFeatureCollection();
  private final GML gml;

  public GISGMLZoneExporter() {
    this(GML.Version.GML2);
  }

  public GISGMLZoneExporter(GML.Version version) {
    gml = new GML(version);
    if (GML.Version.GML2.equals(version)) {
      gml.setLegacy(true);
    }
  }

  @Override
  public void stop() {
    converter.stop();
    stop_ = true;
  }

  /**
   * Attention: l'outputstream n'est pas ferme.
   *
   * @param _prog
   * @param _zone
   * @param _dest
   */
  public void processGML(final ProgressionInterface _prog, final GISZoneCollection _zone, final OutputStream _dest) throws IOException {
    _zone.prepareExport();
    gml.encode(_dest, converter.processGML(_prog, _zone));
  }

  public void processGML(final ProgressionInterface _prog, final GISDataModel _zone, final OutputStream _dest, Class dataStoreClass) throws IOException {
    gml.encode(_dest, converter.processGML(_prog, _zone,dataStoreClass));
  }

  /**
   * A utiliser pour des donn�es sensibles lorsque l'on veut que certains attributs soient retrouv� dans toutes les langues.
   *
   * @return true si on utilise l'ID de la variable pour construire l'attribut � inclure dans le fichier de sortie.
   */
  protected boolean isUseIdAsName() {
    return converter.isUseIdAsName();
  }

  public void setUseIdAsName(final boolean _useIdAsName) {
    converter.setUseIdAsName(_useIdAsName);
  }
}
