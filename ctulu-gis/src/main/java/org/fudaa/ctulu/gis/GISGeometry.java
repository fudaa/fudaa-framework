/*
 *  @creation     31 mai 2005
 *  @modification $Date: 2006-02-09 08:59:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;


/**
 * @author Fred Deniger
 * @version $Id: GISGeometry.java,v 1.2 2006-02-09 08:59:28 deniger Exp $
 */
public interface GISGeometry {
  
  /**
   * L'identifiant, unique pour une g�om�trie. Cet identifiant peut �tre utilis� pour donner
   * un nom par d�faut � une g�om�trie.
   * @see GISGeometryFactory
   * @return L'identifiant de la g�om�trie.
   */
  long getId();

  /**
   * Implementation du pattern visitor. Le visitor parcours l'arbre des g�om�tries jusqu'aux g�om�tries simples, et r�alise 
   * un traitement sur celles-ci (modification, stockage d'information, etc). Le traitement est d�finit dans le visitor.
   * @param _v Le visitor.
   * @return false : Si la visite doit �tre interrompue.
   */
  boolean accept(GISVisitor _v);
  
  GISGeometry createTranslateGeometry(GISPoint xyToAdd);

}
