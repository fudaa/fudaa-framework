/*
 * @creation 21 mars 2005
 *
 * @modification $Date: 2008-02-01 14:42:44 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;

import java.util.ArrayList;
import java.util.List;

/**
 * Une collection GIS contenant des collections GIS de type points, lignes, polygones.
 *
 * @author Fred Deniger
 * @version $Id: GISZone.java,v 1.6.8.1 2008-02-01 14:42:44 bmarchan Exp $
 */
public class GISZone extends GISCollection {
  @Override
  public long getId() {
    return -1;
  }

  String nom_;

  public GISZone() {
    super(10);
  }

  public final String getNom() {
    return nom_;
  }

  public final void setNom(final String _nom) {
    nom_ = _nom;
  }


  @Override
  protected int getTypeCode() {
    return Geometry.TYPECODE_GEOMETRYCOLLECTION;
  }

  @Override
  public GISZone createTranslateGeometry(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    GISZone res = new GISZone();
    res.geometry_ = new GISObjectContainer(super.geometry_);
    List list = res.geometry_.getList();
    int nb = list.size();
    for (int i = 0; i < nb; i++) {
      GISGeometry object = (GISGeometry) list.get(i);
      list.set(i, object.createTranslateGeometry(xyToAdd));
    }
    return res;
  }

  @Override
  public Geometry reverse() {
    throw new UnsupportedOperationException("not supported");
  }

  protected int getSortIndex() {
//    see Geometry.SORTINDEX_GEOMETRYCOLLECTION
    return 7;
  }

  @Override
  public CoordinateSequence getCoordinateSequence(final int _i) {
    return null;
  }

  public GISZoneCollectionPoint[] getPointCollections() {
    final List r = getListCollections(GISZoneCollectionPoint.class);
    final GISZoneCollectionPoint[] rf = new GISZoneCollectionPoint[r.size()];
    r.toArray(rf);
    return rf;
  }

  public GISZoneCollectionLigneBrisee[] getLigneBriseeCollections() {
    final List r = getListCollections(GISZoneCollectionLigneBrisee.class);
    final GISZoneCollectionLigneBrisee[] rf = new GISZoneCollectionLigneBrisee[r.size()];
    r.toArray(rf);
    return rf;
  }

  public int getNumPolylignes() {
    return getNumGeometry(getListCollections(GISZoneCollectionPolyligne.class));
  }

  public int getNumPolyligones() {
    return getNumGeometry(getListCollections(GISZoneCollectionPolygone.class));
  }

  public GISZoneCollectionPolygone[] getPolygoneCollections() {
    final List r = getListCollections(GISZoneCollectionPolygone.class);
    final GISZoneCollectionPolygone[] rf = new GISZoneCollectionPolygone[r.size()];
    r.toArray(rf);
    return rf;
  }

  public GISZoneCollectionPolyligne[] getPolyligneCollections() {
    final List r = getListCollections(GISZoneCollectionPolyligne.class);
    final GISZoneCollectionPolyligne[] rf = new GISZoneCollectionPolyligne[r.size()];
    r.toArray(rf);
    return rf;
  }

  public int getNumGeometry(final List _geometry) {
    int r = 0;
    if (_geometry != null) {
      for (int i = _geometry.size() - 1; i >= 0; i--) {
        r += ((Geometry) _geometry.get(i)).getNumGeometries();
      }
    }
    return r;
  }

  public int getNumLigneBrisee() {
    return getNumGeometry(getListCollections(GISZoneCollectionLigneBrisee.class));
  }

  public List getListCollections(final Class _c) {
    final List r = new ArrayList();
    final int n = getNumGeometries();
    for (int i = 0; i < n; i++) {
      final Geometry g = getGeometry(i);
      if (_c.isInstance(g)) {
        r.add(g);
      }
    }
    return r;
  }

  public static final int getNbPoints(final GISZone _z) {
    return _z.getNumPoints();
  }

  public GISZoneCollectionPoint createPointContainer() {
    final GISZoneCollectionPoint semis = new GISZoneCollectionPoint(null);
    super.geometry_.addObject(semis, null);
    return semis;
  }

  public GISZoneCollectionPoint createPointContainer(final int _nb) {
    final GISZoneCollectionPoint r = new GISZoneCollectionPoint(_nb, null);
    super.geometry_.addObject(r, null);
    return r;
  }

  public GISZoneCollectionLigneBrisee createLigneBriseeContainer() {
    final GISZoneCollectionLigneBrisee r = new GISZoneCollectionLigneBrisee(null);
    super.geometry_.addObject(r, null);
    return r;
  }

  public void add(final GISZoneCollection _pt) {
    super.geometry_.addObject(_pt, null);
  }

  public GISZoneCollectionLigneBrisee createLigneBriseeContainer(final int _nb) {
    final GISZoneCollectionLigneBrisee r = new GISZoneCollectionLigneBrisee(_nb, null);
    super.geometry_.addObject(r, null);
    return r;
  }

  public GISZoneCollectionPolygone createPolygoneContainer() {
    final GISZoneCollectionPolygone r = new GISZoneCollectionPolygone(null);
    super.geometry_.addObject(r, null);
    return r;
  }

  public GISZoneCollectionPolygone createPolygoneContainer(final int _nb) {
    final GISZoneCollectionPolygone r = new GISZoneCollectionPolygone(_nb, null);
    super.geometry_.addObject(r, null);
    return r;
  }

  public GISZoneCollectionPolyligne createPolyligneContainer() {
    final GISZoneCollectionPolyligne r = new GISZoneCollectionPolyligne(null);
    super.geometry_.addObject(r, null);
    return r;
  }

  public GISZoneCollectionPolyligne createPolyligneContainer(final int _nb) {
    final GISZoneCollectionPolyligne r = new GISZoneCollectionPolyligne(_nb, null);
    super.geometry_.addObject(r, null);
    return r;
  }

  public void initPointContainer(final int _expectedPoints) {
    createPointContainer(_expectedPoints);
  }
}
