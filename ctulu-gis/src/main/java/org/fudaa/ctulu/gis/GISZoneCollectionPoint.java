/*
 * @creation 21 mars 2005
 *
 * @modification $Date: 2008-02-01 14:42:44 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEditAbstract;
import org.locationtech.jts.geom.*;

import java.util.Arrays;
import java.util.List;

/**
 * Une collection contenant des points GIS. <p> Ces points peuvent comporter des attributs, dont les valeurs sont stock�es dans
 * des {@link GISAttributeModel}.
 *
 * @author Fred Deniger
 */
public class GISZoneCollectionPoint extends GISZoneCollectionGeometry {
  /**
   * @param _l le listener
   */
  public GISZoneCollectionPoint(final GISZoneListener _l) {
    this(10, _l);
  }

  @Override
  public GISGeometry createTranslateGeometry(GISPoint xyToAdd) {
    return createTranslate(xyToAdd);
  }

  @Override
  public GISZoneCollectionPoint createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    GISZoneCollectionPoint res = new GISZoneCollectionPoint();
    res.attr_ = attr_;
    res.attListHasChanged_ = attListHasChanged_;
    res.attributeIsZ_ = attributeIsZ_;
    res.geometry_ = new GISObjectContainer(super.geometry_);
    List list = res.geometry_.getList();
    int nb = list.size();
    for (int i = 0; i < nb; i++) {
      GISPoint object = (GISPoint) list.get(i);
      list.set(i, object.createTranslateGeometry(xyToAdd));
    }
    return res;
  }

  private GISZoneCollectionPointAsSequence asSequence;

  public CoordinateSequence getAttachedSequence() {
    if (asSequence == null) {
      asSequence = new GISZoneCollectionPointAsSequence(this);
    }
    return asSequence;
  }

  public GISZoneCollectionPoint() {
    this(null);
  }

  @Override
  public double getDoubleValue(final int _idxAtt, final int _idxGeom) {
    return ((CtuluCollectionDouble) getModel(_idxAtt)).getValue(_idxGeom);
  }

  public CtuluCollectionDoubleEdit createEditForX() {
    return new CtuluCollectionDoubleEditAbstract() {
      @Override
      public double getValue(final int _i) {
        return getX(_i);
      }

      @Override
      public int getSize() {
        return getNumPoints();
      }

      @Override
      protected void internalSetValue(final int _i, final double _newV) {
        GISZoneCollectionPoint.this.set(_i, _newV, getY(_i), null);
      }
    };
  }

  public CtuluCollectionDoubleEdit createEditForY() {
    return new CtuluCollectionDoubleEditAbstract() {
      @Override
      public double getValue(final int _i) {
        return getY(_i);
      }

      @Override
      public int getSize() {
        return getNumPoints();
      }

      @Override
      protected void internalSetValue(final int _i, final double _newV) {
        GISZoneCollectionPoint.this.set(_i, getX(_i), _newV, null);
      }
    };
  }

  public Envelope expandEnvelope(final Envelope _env) {
    if (_env != null && getNumGeometries() > 0) {
      _env.expandToInclude(getEnvelopeInternal());
    }
    return _env;
  }

  public void getCoordinate(final int _index, final Coordinate _coord) {
    _coord.x = getX(_index);
    _coord.y = getY(_index);
    _coord.z = getZ(_index);
  }

  public int getMeasures() {
    return 0;
  }

  public boolean hasZ() {
    return true;
  }

  public boolean hasM() {
    return false;
  }

  public Coordinate getCoordinate(final int _i) {
    final Coordinate res = new Coordinate();
    getCoordinate(_i, res);
    return res;
  }

  public Coordinate getCoordinateCopy(final int _i) {
    return getCoordinate(_i);
  }

  public double getOrdinate(final int _index, final int _ordinateIndex) {
    switch (_ordinateIndex) {
      case 0:
        return getX(_index);
      case 1:
        return getY(_index);
      case 2:
        return getZ(_index);
      default:
        return 0;
    }
  }

  public int size() {
    return getNumGeometries();
  }

  public Coordinate[] toCoordinateArray() {
    final Coordinate[] res = new Coordinate[getNumGeometries()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = getCoordinate(i);
    }
    return res;
  }

  @Override
  public boolean accept(final GISVisitor _v) {
    final int nb = getNumGeometries();
    for (int i = 0; i < nb; i++) {
      if (!((GISPoint) getGeometry(i)).accept(_v)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public void setCoordinateSequence(final int _idx, final CoordinateSequence _newSeq, final CtuluCommandContainer _cmd) {
    if (_newSeq != null && _newSeq.size() == 1) {
      set(_idx, _newSeq.getX(0), _newSeq.getY(0), _cmd);
    }
  }

  @Override
  public void updateListeners() {
    for (int i = getNbAttributes() - 1; i >= 0; i--) {
      final GISAttributeModelObservable m = getModelListener(i);
      if (m != null) {
        m.setListener(this);
      }
    }
  }

  @Override
  public CoordinateSequence getCoordinateSequence(final int _i) {
    return ((Point) getGeometry(_i)).getCoordinateSequence();
  }

  @Override
  public Class getDataStoreClass() {
    return Point.class;
  }

  /**
   * @param _nbObject le nombre attendu de points
   */
  public GISZoneCollectionPoint(final int _nbObject, final GISZoneListener _l) {
    super(_nbObject, _l);
  }

  public final double getX(final int _i) {
    return get(_i).getX();
  }

  public final double getY(final int _i) {
    return get(_i).getY();
  }

  public final double getZ(final int _i) {
    return get(_i).getZ();
  }

  public double getM(int index) {
    return 0;
  }

  /**
   * Ajoute un point avec des valeurs par defaut pour chaque attribut, et sans container de commandes.
   *
   * @param _x Coordonn�e X.
   * @param _y Coordonn�e Y.
   * @param _z Coordonn�e Z.
   * @return false si la collection est non modifiable.
   */
  public final boolean add(final double _x, final double _y, final double _z) {
    if (!isGeomModifiable_) {
      return false;
    }
    return add(_x, _y, _z, null, null);
  }

  public final boolean addAll(final GISPoint[] _data, final List _list, final CtuluCommandContainer _c) {
    if (!isGeomModifiable_) {
      return false;
    }
    return geometry_.addAll(_data, _list, _c);
  }

  /**
   * Ajoute un point avec des valeurs par defaut pour chaque attribut avec container de commandes.
   *
   * @param _x Coordonn�e X.
   * @param _y Coordonn�e Y.
   * @param _z Coordonn�e Z.
   * @param _c Le container.
   * @return false si la collection est non modifiable.
   */
  public final boolean add(final double _x, final double _y, final double _z, final CtuluCommandContainer _c) {
    if (!isGeomModifiable_) {
      return false;
    }
    return add(_x, _y, _z, null, _c);
  }

  /**
   * Ajoute un point.
   *
   * @param _x Coordonn�e X.
   * @param _y Coordonn�e Y.
   * @param _z Coordonn�e Z.
   * @param _c Le container.
   * @param _o La liste des valeurs d'attributs, dans l'ordre et suivant les types d�finis par GISZoneCollection.setAttributes()
   * @return false si la collection est non modifiable.
   */
  public final boolean add(final double _x, final double _y, final double _z, final List _o, final CtuluCommandContainer _c) {
    if (!isGeomModifiable_) {
      return false;
    }
    return geometry_.add(new GISPoint(_x, _y, _z), _o, _c);
  }

  @Override
  public int addGeometry(final Geometry _seq, final Object[] _datas, final CtuluCommandContainer _cmd) {
    final Point p = (Point) _seq;
    add(p.getX(), p.getY(), p.getCoordinateSequence().getOrdinate(0, 2), _datas == null ? null : Arrays.asList(_datas), _cmd);
    return size() - 1;
  }

  @Override
  public void addCoordinateSequence(final CoordinateSequence _seq, final Object[] _datas, final CtuluCommandContainer _cmd) {
    if (_seq == null || _seq.size() != 1) {
      return;
    }
    add(_seq.getX(0), _seq.getY(0), _seq.getOrdinate(0, 2), _datas == null ? null : Arrays.asList(_datas), _cmd);
  }

  public final GISPoint get(final int _i) {
    return (GISPoint) geometry_.getValueAt(_i);
  }

  /**
   * @param _p
   * @param _c
   * @return true si ok
   * @see #add(double, double, double, CtuluCommandContainer)
   */
  public final boolean add(final GISPoint _p, final CtuluCommandContainer _c) {
    if (!isGeomModifiable_) {
      return false;
    }
    return add(_p.getX(), _p.getY(), _p.getZ(), null, _c);
  }

  public boolean remove(final int[] _indiceDuPointAEnlever, final CtuluCommandContainer _cmd) {
    if (!isGeomModifiable_) {
      return false;
    }
    return geometry_.remove(_indiceDuPointAEnlever, _cmd);
  }

  public final boolean get(final int _i, final GISPointMutable _p) {
    _p.initWith(get(_i));
    return true;
  }

  @Override
  protected Envelope computeEnvelopeInternal() {
    final Envelope e = new Envelope();
    final int n = getNumGeometries();
    for (int i = 0; i < n; i++) {
      e.expandToInclude(getX(i), getY(i));
    }
    return e;
  }

  public final boolean set(final int _idxPtToReplace, final double _newX, final double _newY, final CtuluCommandContainer _c) {
    if (!isGeomModifiable_) {
      return false;
    }
    return geometry_.setObject(_idxPtToReplace, new GISPoint(_newX, _newY, getZ(_idxPtToReplace)), _c);
  }
}
