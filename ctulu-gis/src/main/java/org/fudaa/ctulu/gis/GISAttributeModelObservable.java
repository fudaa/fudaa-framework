/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2006-02-09 08:59:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;


/**
 * @author Fred Deniger
 * @version $Id: GISAttributeModelListener.java,v 1.3 2006-02-09 08:59:28 deniger Exp $
 */
public interface GISAttributeModelObservable extends GISAttributeModel {

  /**
   * @param _listener le listener
   */
  void setListener(GISAttributeListener _listener);

}
