/*
 * @creation 21 mars 2005
 *
 * @modification $Date: 2008-04-01 07:22:48 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import gnu.trove.TIntArrayList;
import org.fudaa.ctulu.*;
import org.fudaa.ctulu.collection.CtuluCollectionDouble;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;

import java.util.Arrays;
import java.util.List;

/**
 * Une collection pour les g�om�tries quelconques.
 *
 * @author Bertrand Marchand
 * @version $Id: GISZoneCollectionMultiPoint.java,v 1.1.2.2 2008-04-01 07:22:48 bmarchan Exp $
 */
public class GISZoneCollectionGeometry extends GISZoneCollection {
  public GISZoneCollectionGeometry() {
    this(null);
  }

  /**
   * @param _listener le listener
   */
  public GISZoneCollectionGeometry(final GISZoneListener _listener) {
    this(10, _listener);
  }

  /**
   * @param _listener le listener
   */
  public GISZoneCollectionGeometry(final GISZoneListener _listener, final GISZoneCollectionGeometry _col) {
    this(10, _listener);
    if (_col != null) {
      for (int i = 0; i < _col.getNumGeometries(); i++) {
        super.geometry_.add(_col.getGeometry(i), null, null);
      }
    }
  }

  @Override
  protected int getTypeCode() {
    return Geometry.TYPECODE_GEOMETRYCOLLECTION;
  }

  @Override
  public Geometry reverse() {
    throw new UnsupportedOperationException("not supported");
  }

  protected int getSortIndex() {
//    see Geometry.SORTINDEX_GEOMETRYCOLLECTION
    return 7;
  }

  public GISZoneCollectionGeometry(final int _nbObject, final GISZoneListener _listener) {
    super(_nbObject);
    setListener(_listener);
  }

  @Override
  public GISGeometry createTranslateGeometry(GISPoint xyToAdd) {
    return createTranslate(xyToAdd);
  }

  @Override
  public GISZoneCollectionGeometry createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    GISZoneCollectionGeometry res = new GISZoneCollectionGeometry();
    res.attr_ = attr_;
    res.attListHasChanged_ = attListHasChanged_;
    res.attributeIsZ_ = attributeIsZ_;
    res.geometry_ = new GISObjectContainer(super.geometry_);
    List list = res.geometry_.getList();
    int nb = list.size();
    for (int i = 0; i < nb; i++) {
      GISGeometry object = (GISGeometry) list.get(i);
      list.set(i, object.createTranslateGeometry(xyToAdd));
    }
    return res;
  }

  /**
   * Cr�e un tableau de valeurs d'attributs.
   * <p>
   * Les valeurs peuvent �tre globales ou atomiques.
   *
   * @param _init Les valeurs d'initialisation, dans l'ordre et au nombre des mod�les d'attributs. Si null, les valeurs
   *     par d�faut sont affect�es (voir {@link GISAttributeInterface#getDefaultValue()})
   * @param _dest La g�ometrie.
   * @param _isClosed Param�tre inutilis�.
   */
  protected Object[] createAttributeList(final Object[] _init, final Geometry _dest, final boolean _isClosed) {
    final Object[] rf = new Object[getNbAttributes()];
    if (_init != null && _init.length != getNbAttributes()) {
      throw new IllegalArgumentException("bad size");
    }
    for (int i = getNbAttributes() - 1; i >= 0; i--) {
      rf[i] = createOrAssignAttribut(i, _init == null ? null : _init[i], _dest);
    }
    return rf;
  }

  /**
   * Ajoute une g�om�trie, d�finie a partir d'une sequence de coordonn�es.
   * <p>
   * Attention : Ne peut �tre utilis� que si la liste contient des objets GIS homogenes.
   *
   * @param _seq La sequence de coordonnees
   * @param _data Les valeurs d'attribut associ�es � la g�om�trie, dans l'ordre des attributs.
   * @param _cmd Le manager de commandes.
   */
  @Override
  public void addCoordinateSequence(final CoordinateSequence _seq, final Object[] _data, final CtuluCommandContainer _cmd) {
    throw new IllegalAccessError("Can't call this method");
  }

  @Override
  public int addGeometry(final Geometry _geom, final Object[] _data, final CtuluCommandContainer _cmd) {
    if (!(_geom instanceof GISCoordinateSequenceContainerInterface)) {
      throw new IllegalArgumentException("Bad type geometry");
    }

    if (isGeomModifiable_) {
      geometry_.add(_geom, Arrays.asList(createAttributeList(_data, _geom, true)), _cmd);
      return geometry_.getSize() - 1;
    }
    return -1;
  }

  @Override
  public boolean accept(final GISVisitor _v) {
    final int nb = getNumGeometries();
    for (int i = 0; i < nb; i++) {
      if (!((GISCoordinateSequenceContainerInterface) getGeometry(i)).accept(_v)) {
        return false;
      }
    }
    return true;
  }

  /**
   * Ajoute un sommet a une g�om�trie. Les attributs atomiques sont ajout�s au sommet ins�r�, par interpolation.
   *
   * @param _idxGeom L'index de la g�om�trie
   * @param _idxBefore L'indice du point juste avant. Si _idxBefore est �gal � -1, le point est ajout� en d�but de
   *     g�om�trie
   * @param _x La coordonn�e X du sommet ajout�.
   * @param _y La coordonn�e Y du sommet ajout�.
   * @param _cmd Le manager de commandes.
   * @return L'index du point ins�r�, ou -1 si non ins�r�.
   */
  public int addAtomic(final int _idxGeom, final int _idxBefore, final double _x, final double _y, final CtuluCommandContainer _cmd) {

    if (!isGeomModifiable_) {
      return -1;
    }
    Geometry geom = (Geometry) super.geometry_.getValueAt(_idxGeom);
    final Coordinate[] oldcs = geom.getCoordinates();
    final int initSize = oldcs.length;
    if (_idxBefore < -1 || _idxBefore >= initSize) {
      throw new IllegalArgumentException("L'index du point � ajouter doit appartenir � la g�om�trie.");
    }
    final Coordinate[] cs = new Coordinate[initSize + 1];
    int idx = 0;
    for (int i = -1; i < initSize; i++) {
      if (i >= 0) {
        cs[idx++] = (Coordinate) oldcs[i].clone();
      }
      if (i == _idxBefore) {
        cs[idx++] = new Coordinate(_x, _y, 0.);
      }
    }

    // Polygone => La derni�re coordonn�e doit �tre egale a la premiere
    if (geom instanceof GISPolygone) {
      cs[initSize] = cs[0];
    }

    geom = GISGeometryFactory.INSTANCE.createGeometry(geom.getClass(), cs);
    setGeometry(_idxGeom, geom, _cmd);

    return _idxBefore + 1;
  }

  /**
   * Remplace une geometrie et modifie la valeur de ses attributs atomiques par interpolation si le nombre de sommets
   * est diff�rent de la geometrie initiale.
   *
   * @param _idx l'indice de la ligne a changer
   * @param _geom la nouvelle ligne
   * @param _cmd le receveur de commande
   */
  public void setGeometry(final int _idx, final Geometry _geom, final CtuluCommandContainer _cmd) {
    if (!isGeomModifiable_) {
      return;
    }
    final Geometry old = (Geometry) super.geometry_.getValueAt(_idx);
    if (old.getNumPoints() == _geom.getNumPoints() || !containsAtomicAttribute()) {
      super.geometry_.setObject(_idx, _geom, _cmd);
    } else {
      final CtuluCommandComposite cmp = new CtuluCommandComposite();
      for (int i = getNbAttributes() - 1; i >= 0; i--) {
        final GISAttributeInterface att = getAttribute(i);
        if (att.isAtomicValue()) {
          final GISAttributeModel m = (GISAttributeModel) (getModelListener(i).getObjectValueAt(_idx));
          final GISReprojectInterpolateurI interpolateur = GISZoneAttributeFactory.create1DInterpolateur(att,
              (GISCoordinateSequenceContainerInterface) old, (GISCoordinateSequenceContainerInterface) _geom, m);
          getModelListener(i).setObject(_idx, m.deriveNewModel(_geom.getNumPoints(), interpolateur), cmp);
        }
      }
      super.geometry_.setObject(_idx, _geom, cmp);
      if (_cmd != null) {
        _cmd.addCmd(cmp.getSimplify());
      }
    }
  }

  @Override
  public CoordinateSequence getCoordinateSequence(final int _i) {
    if (!(getGeometry(_i) instanceof GISCoordinateSequenceContainerInterface)) {
      return null;
    }
    return ((GISCoordinateSequenceContainerInterface) getGeometry(_i)).getCoordinateSequence();
  }

  @Override
  public Class getDataStoreClass() {
    return GISCoordinateSequenceContainerInterface.class;
  }

  @Override
  public double getDoubleValue(final int _idxAtt, final int _idxGeom) {
    if (getAttribute(_idxAtt).isAtomicValue()) {
      return CtuluLibArray.getMoyenne(((CtuluCollectionDouble) getModel(_idxAtt).getObjectValueAt(_idxGeom)).getValues());
    }
    return ((CtuluCollectionDouble) getModel(_idxAtt)).getValue(_idxGeom);
  }

  @Override
  public void setCoordinateSequence(final int _idx, final CoordinateSequence _newSeq, final CtuluCommandContainer _cmd) {
    final Geometry old = (Geometry) super.geometry_.getValueAt(_idx);
    if (_newSeq != null && _newSeq.size() == old.getNumPoints()) {
      final CoordinateSequence seq = GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(_newSeq);
      geometry_.setObject(_idx, GISGeometryFactory.INSTANCE.createGeometry(old.getClass(), seq), _cmd);
    }
  }

  @Override
  public void updateListeners() {
    for (int i = getNbAttributes() - 1; i >= 0; i--) {
      final GISAttributeModelObservable m = getModelListener(i);
      if (m != null) {
        if (m.getAttribute().isAtomicValue()) {
          for (int j = m.getSize() - 1; j >= 0; j--) {
            ((GISAttributeModelObservable) m.getObjectValueAt(j)).setListener(this);
          }
        } else {
          m.setListener(this);
        }
      }
    }
  }

  public boolean containsAtomicAttribute() {
    for (int i = getNbAttributes() - 1; i >= 0; i--) {
      if (getAttribute(i).isAtomicValue()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Supprime des sommets sur une g�om�trie.
   *
   * @param _idxGeom L'indice de la g�om�trie.
   * @param _sel La liste des sommets s�lectionn�s.
   * @param _ui L'interface utilisateur.
   * @param _cmd Le manager de commandes.
   * @return true si tout s'est bien pass�.
   */
  public boolean removeAtomics(final int _idxGeom, final CtuluListSelectionInterface _sel, final CtuluUI _ui, final CtuluCommandContainer _cmd) {
    if (_sel == null || _sel.isEmpty()) {
      return false;
    }
    boolean r = true;
    final Geometry old = (Geometry) getGeometry(_idxGeom);
    int nbPt = old.getNumPoints();

    // Raccourci si tous les sommets sont s�lectionn�s.
    int nbSel = _sel.getNbSelectedIndex();
    if (nbSel == nbPt || (nbSel + 1 == nbPt && old instanceof GISPolygone)) {
      removeGeometries(new int[]{_idxGeom}, _cmd);
      return true;
    }

    // les nouvelles coordonnees.
    final Coordinate[] newCs = new Coordinate[nbPt - nbSel];
    // cette liste contient les indices a enlever : creee que si necessaire
    // si des attributes atomics existent
    final TIntArrayList idxToRemove = containsAtomicAttribute() ? new TIntArrayList(nbPt - nbSel) : null;

    // dans le cas polygone le dernier point n'est pas a considerer=premier point
    if (old instanceof GISPolygone) {
      nbPt--;
    }

    // compteur tempo
    int count = 0;
    for (int idxPt = 0; idxPt < nbPt; idxPt++) {
      if (_sel.isSelected(idxPt)) {
        if (idxToRemove != null) {
          idxToRemove.add(idxPt);
        }
      } else {
        newCs[count++] = ((GISCoordinateSequenceContainerInterface) old).getCoordinateSequence().getCoordinate(idxPt);
        //        newCs[count++] = (Coordinate)old.getCoordinates()[idxPt].clone();
      }
    }

    if (old instanceof GISPolygone) {
      newCs[count] = newCs[0];
    }

    Geometry newGeom = null;

    // Un polygone
    if (old instanceof GISPolygone && count < 3) {
      r = false;
      if (_ui != null) {
        _ui.error(null, CtuluLib.getS("La ligne brisee doit contenir {0} points au moins", CtuluLibString.TROIS), false);
      }
    }

    // Une polyligne
    else if (old instanceof GISPolyligne && count < 2) {
      r = false;
      if (_ui != null) {
        _ui.error(null, CtuluLib.getS("La ligne brisee doit contenir {0} points au moins", CtuluLibString.DEUX), false);
      }
    }

    // Un multipoint
    else if (old instanceof GISMultiPoint && count < 1) {
      r = false;
      if (_ui != null) {
        _ui.error(null, CtuluLib.getS("Le multipoint doit contenir {0} points au moins", CtuluLibString.UN), false);
      }
    } else {
      //      final Coordinate[] coord=new Coordinate[count];
      //      System.arraycopy(newCs, 0, coord, 0, coord.length);
      newGeom = GISGeometryFactory.INSTANCE.createGeometry(old.getClass(), newCs);
      newGeom.setUserData(old.getUserData());
    }

    if (r) {
      final CtuluCommandComposite cmp = new CtuluCommandComposite();
      super.geometry_.setObject(_idxGeom, newGeom, cmp);
      // si des attributs atomics sont concerne
      if (idxToRemove != null && idxToRemove.size() > 0) {
        final int[] idx = idxToRemove.toNativeArray();
        Arrays.sort(idx);
        for (int i = getNbAttributes() - 1; i >= 0; i--) {
          // attribut atomic
          if (getAttribute(i).isAtomicValue()) {
            // dans ce cas on recupere le model contenant cet attribut
            final GISAttributeModel m = getModelListener(i);
            // on recupere le sous-model concerne par la modif de geometrie
            final GISAttributeModel atomicModel = (GISAttributeModel) m.getObjectValueAt(_idxGeom);
            // on le change
            m.setObject(_idxGeom, atomicModel.createSubModel(idx), cmp);
          }
        }
      }
      if (_cmd != null) {
        _cmd.addCmd(cmp.getSimplify());
      }
    }
    return r;
  }

  @Override
  public boolean addAll(final GISDataModel _model, final CtuluCommandContainer _cmd, final boolean _doPostImport) {
    if (!isGeomModifiable_ || _model == null) {
      return false;
    }
    // Controle que les geometries pass�e sont du bon type 
    for (int i = 0; i < _model.getNumGeometries(); i++) {
      if (!(_model.getGeometry(i) instanceof GISCoordinateSequenceContainerInterface)) {
        throw new IllegalArgumentException("Bad geometry");
      }
    }
    return super.addAll(_model, _cmd, _doPostImport);
  }
}
