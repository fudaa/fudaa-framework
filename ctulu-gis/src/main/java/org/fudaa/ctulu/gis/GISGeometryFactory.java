/*
 *  @creation     18 mars 2005
 *  @modification $Date: 2007-04-20 16:20:17 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.*;

/**
 * @author Fred Deniger
 * @version $Id: GISGeometryFactory.java,v 1.10 2007-04-20 16:20:17 deniger Exp $
 */
public final class GISGeometryFactory extends GeometryFactory {
  /**
   * Numero d'identifiant, incr�ment� � chaque nouvelle cr�ation de g�om�trie.
   * En cas de suppression de g�om�trie, ce num�ro n'est pas d�cr�ment�.
   */
  protected long numid_ = 0;
  /**
   * Singletion
   */
  public static final GISGeometryFactory INSTANCE = new GISGeometryFactory();

  private GISGeometryFactory() {

    super(new PrecisionModel(10E6), 0, new GISCoordinateSequenceFactory());
  }

  /**
   * Retourne un nouvel identifiant a affecter a la g�om�trie
   */
  protected long getNewId() {
    return ++numid_;
  }

  public CoordinateSequence createTranslated(CoordinateSequence init, Point xyToAdd) {
    return ((GISCoordinateSequenceFactory) getCoordinateSequenceFactory()).createTranslated(init, xyToAdd);
  }

  @Override
  public GISPoint createPoint(final Coordinate _coordinate) {
    return new GISPoint(_coordinate);
  }

  public GISPoint createPoint(final double _x, final double _y, final double _z) {
    return new GISPoint(_x, _y, _z);
  }

  public CoordinateSequence createLinearRingDefaultSequence() {
    final CoordinateSequence seq = getCoordinateSequenceFactory().create(4, 3);
    seq.setOrdinate(0, 0, 0);
    seq.setOrdinate(0, 1, 0);
    seq.setOrdinate(1, 0, 2);
    seq.setOrdinate(1, 1, 0);
    seq.setOrdinate(2, 0, 1);
    seq.setOrdinate(2, 1, 1);
    seq.setOrdinate(3, 0, 0);
    seq.setOrdinate(3, 1, 0);
    return seq;
  }

  public GISPolygone createLinearRing(final double _xMin, final double _xMax, final double _yMin, final double _yMax) {
    final CoordinateSequence seq = getCoordinateSequenceFactory().create(5, 3);
    seq.setOrdinate(0, 0, _xMin);
    seq.setOrdinate(0, 1, _yMin);
    seq.setOrdinate(1, 0, _xMax);
    seq.setOrdinate(1, 1, _yMin);
    seq.setOrdinate(2, 0, _xMax);
    seq.setOrdinate(2, 1, _yMax);
    seq.setOrdinate(3, 0, _xMin);
    seq.setOrdinate(3, 1, _yMax);
    seq.setOrdinate(4, 0, _xMin);
    seq.setOrdinate(4, 1, _yMin);
    return (GISPolygone) createLinearRing(seq);
  }

  @Override
  public GISPoint createPoint(final CoordinateSequence _coordinates) {
    return createPoint(_coordinates.getCoordinate(0));
  }

  @Override
  public GISPolygone createLinearRing(final Coordinate[] _coordinates) {
    return new GISPolygone(getCoordinateSequenceFactory().create(_coordinates));
  }

  public GISPolyligne createSegment(final double _x1, final double _y1, final double _x2, final double _y2) {
    final CoordinateSequence seq = getCoordinateSequenceFactory().create(2, 3);
    seq.setOrdinate(0, 0, _x1);
    seq.setOrdinate(0, 1, _y1);
    seq.setOrdinate(1, 0, _x2);
    seq.setOrdinate(1, 1, _y2);
    return (GISPolyligne) createLineString(seq);
  }

  public GISPolygone createLinearRingImmutable(final Coordinate[] _coordinates) {
    return new GISPolygone(((GISCoordinateSequenceFactory) getCoordinateSequenceFactory())
        .createImmutable(_coordinates));
  }

  @Override
  public GISCoordinateSequenceFactory getCoordinateSequenceFactory() {
    return (GISCoordinateSequenceFactory) super.getCoordinateSequenceFactory();
  }

  @Override
  public GISPolygone createLinearRing(final CoordinateSequence _coordinates) {
    return new GISPolygone(getCoordinateSequenceFactory().transformToCustom(_coordinates));
  }

  public LinearRing transformToLinearRing(final LineString _s) {
    if (_s == null) {
      return null;
    }
    if (_s instanceof LinearRing) {
      return (LinearRing) _s;
    }
    if (_s.isClosed()) {
      return createLinearRing(_s.getCoordinateSequence());
    }
    final CoordinateSequence seq = _s.getCoordinateSequence();
    final CoordinateSequence newSeq = GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(seq.size() + 1,
        3);
    final int oldSize = seq.size();
    for (int i = oldSize - 1; i >= 0; i--) {
      newSeq.setOrdinate(i, 0, seq.getX(i));
      newSeq.setOrdinate(i, 1, seq.getY(i));
    }
    newSeq.setOrdinate(oldSize, 0, seq.getX(0));
    newSeq.setOrdinate(oldSize, 1, seq.getY(0));
    return createLinearRing(newSeq);
  }

  @Override
  public GISPolyligne createLineString(final Coordinate[] _coordinates) {
    return new GISPolyligne(getCoordinateSequenceFactory().create(_coordinates));
  }

  @Override
  public LineString createLineString() {
    return new GISPolyligne();
  }

  @Override
  public GISPolyligne createLineString(final CoordinateSequence _coordinates) {
    return new GISPolyligne(getCoordinateSequenceFactory().transformToCustom(_coordinates));
  }

  public GISPolyligneNiveau createLineStringNiveau(final CoordinateSequence _coordinates) {
    return new GISPolyligneNiveau(_coordinates);
  }

  public GISPolygoneNiveau createLinearRingNiveau(final CoordinateSequence _coordinates) {
    return new GISPolygoneNiveau(_coordinates);
  }

  @Override
  public GISMultiPoint createMultiPoint(final Coordinate[] _coordinates) {
    if (_coordinates == null) {
      return null;
    }
    final int n = _coordinates.length;
    final GISPoint[] pt = new GISPoint[_coordinates.length];
    for (int i = 0; i < n; i++) {
      pt[i] = createPoint(_coordinates[i]);
    }
    return new GISMultiPoint(pt);
  }

  @Override
  public MultiPoint createMultiPoint() {
    return new GISMultiPoint(null);
  }

  @Override
  public GISMultiPoint createMultiPoint(final Point[] _point) {
    return new GISMultiPoint(_point);
  }

  @Override
  public GeometryCollection createGeometryCollection(final Geometry[] _geometries) {
    return new GISGeometryCollection(_geometries, this);
  }

  @Override
  public GISMultiPolyligne createMultiLineString(final LineString[] _lineStrings) {
    return new GISMultiPolyligne(_lineStrings);
  }

  @Override
  public GISMultiPoint createMultiPoint(final CoordinateSequence _coordinates) {
    if (_coordinates == null) {
      return null;
    }
    final int n = _coordinates.size();
    final GISPoint[] pt = new GISPoint[n];
    for (int i = 0; i < n; i++) {
      // Si la s�quence en entr�e n'a que 2 dimensions, on rajoute la 3ieme avec valeur 0.
      pt[i] = (GISPoint) createPoint(_coordinates.getX(i), _coordinates.getY(i), _coordinates.getDimension() == 2 ? 0 : _coordinates.getOrdinate(i, CoordinateSequence.Z));
    }
    return new GISMultiPoint(pt);
  }

  /**
   * Renvoie un GISPolygone si non vide.
   */
  @Override
  public Geometry toGeometry(final Envelope _envelope) {
    if (_envelope.isNull()) {
      return createPoint((CoordinateSequence) null);
    }
    if (_envelope.getMinX() == _envelope.getMaxX() && _envelope.getMinY() == _envelope.getMaxY()) {
      return createPoint(new Coordinate(
          _envelope.getMinX(), _envelope.getMinY()));
    }
    return createLinearRing(new Coordinate[]{
        new Coordinate(_envelope.getMinX(), _envelope.getMinY()),
        new Coordinate(_envelope.getMaxX(), _envelope.getMinY()),
        new Coordinate(_envelope.getMaxX(), _envelope.getMaxY()),
        new Coordinate(_envelope.getMinX(), _envelope.getMaxY()),
        new Coordinate(_envelope.getMinX(), _envelope.getMinY())});
  }

  @Override
  public Polygon createPolygon(final LinearRing _shell, final LinearRing[] _holes) {
    return new GISPolygoneWithHole(_shell, _holes);
  }

  @Override
  public MultiPolygon createMultiPolygon(final Polygon[] _polygons) {
    return super.createMultiPolygon(_polygons);
  }

  @Override
  public Geometry createGeometry(final Geometry _g) {
    return super.createGeometry(_g);
  }

  /**
   * Cr�e une g�om�trie de classe donn�e.
   *
   * @param _init La classe a cr�er.
   * @param _seq La sequance de coordonn�es.
   * @return La g�om�trie.
   */
  public Geometry createGeometry(final Class _clazz, final Coordinate[] _coordinates) {
    return createGeometry(_clazz, getCoordinateSequenceFactory().create(_coordinates));
  }

  /**
   * Cr�e une g�om�trie de classe donn�e.
   *
   * @param _seq La sequance de coordonn�es.
   * @return La g�om�trie.
   */
  public Geometry createGeometry(final Class _clazz, final CoordinateSequence _seq) {
    if (_clazz == GISPolygoneNiveau.class) {
      return createLinearRingNiveau(_seq);
    } else if (_clazz == GISPolygone.class) {
      return createLinearRing(_seq);
    } else if (_clazz == GISPolyligneNiveau.class) {
      return createLineStringNiveau(_seq);
    } else if (_clazz == GISPolyligne.class) {
      return createLineString(_seq);
    } else if (_clazz == GISMultiPoint.class) {
      return createMultiPoint(_seq);
    } else if (_clazz == GISPoint.class) {
      return createPoint(_seq);
    } else {
      return null;
    }
  }

  public Geometry transformToCustom(Geometry in) {
    if (in == null || in instanceof GISCoordinateSequenceContainerInterface) {
      return in;
    }
    Class initClass = in.getClass();
    if (initClass == LinearRing.class) {
      return createLinearRing(((LinearRing) in).getCoordinateSequence());
    } else if (initClass == LineString.class) {
      LineString inString = (LineString) in;
      if (inString.isClosed()) {
        return createLinearRing(((LineString) in).getCoordinateSequence());
      }
      return createLineString(((LineString) in).getCoordinateSequence());
    } else if (initClass == MultiPoint.class) {
      return createMultiPoint(in.getCoordinates());
    } else if (initClass == Point.class) {
      return createPoint(((Point) in).getCoordinateSequence());
    } else if (initClass == Polygon.class) {
      final Polygon polygon = (Polygon) in;
      LinearRing ext = (LinearRing) transformToCustom(polygon.getExteriorRing());
      LinearRing[] interiors = new LinearRing[polygon.getNumInteriorRing()];
      for (int i = 0; i < interiors.length; i++) {
        interiors[i] = (LinearRing) transformToCustom(polygon.getInteriorRingN(i));
      }
      return createPolygon(ext, interiors);
    } else {
      return in;
    }
  }
}
