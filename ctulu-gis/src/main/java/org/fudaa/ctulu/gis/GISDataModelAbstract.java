/*
 * @creation 8 janv. 07
 * @modification $Date: 2007-01-10 08:58:47 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Envelope;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * Un adapteur permettant d'exposer certains attributs seulement d'un mod�le de donn�es. Seuls des attributs existants dans 
 * le modele original peuvent �tre filtr�s. Les indices des attributs expos�s sont donn�s sous forme d'un tableau,
 * pas n�cessairement dans un ordre croissant.
 * 
 * @author fred deniger
 * @version $Id$
 */
public abstract class GISDataModelAbstract implements GISDataModel {

  final int[] attShown_;
  protected final GISDataModel model_;

  /**
   * @param _attShown Les indices des seuls attributs a exposer. Peut etre <code>null</code>. Auquel cas tous les attributs sont
   * expos�s.
   * @param _model Le mod�le d'origine.
   */
  public GISDataModelAbstract(final int[] _attShown, final GISDataModel _model) {
    super();
    attShown_ = _attShown;
    model_ = _model;
  }

  @Override
  public final void preload(final GISAttributeInterface[] _att, final ProgressionInterface _prog) {
    model_.preload(_att, _prog);
  }

  @Override
  public final int getIndiceOf(final GISAttributeInterface _att) {
    if (_att==null) return -1;
    for (int i = getNbAttributes() - 1; i >= 0; i--) {
      if (_att.equals(getAttribute(i))) {
        return i;
      }
    }
    return -1;
  }

  @Override
  public final int getNbAttributes() {
    return attShown_ == null ? model_.getNbAttributes() : attShown_.length;
  }

  @Override
  public final GISAttributeInterface getAttribute(final int _idxAtt) {
    int idx=getRealAttributIndex(_idxAtt);
    if (idx<0 || idx>model_.getNbAttributes()) return null;
    return model_.getAttribute(idx);
  }

  @Override
  public Envelope getEnvelopeInternal() {
    return model_.getEnvelopeInternal();
  }
  
  protected int getRealAttributIndex(final int _idxAtt) {
    return attShown_ == null ? _idxAtt : attShown_[_idxAtt];
  }
}
