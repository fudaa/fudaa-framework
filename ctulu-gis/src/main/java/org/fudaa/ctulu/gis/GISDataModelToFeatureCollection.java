/*
 * @creation 19 mai 2005
 *
 * @modification $Date: 2008-03-28 14:59:28 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import com.memoire.fu.FuLog;
import gnu.trove.TIntArrayList;
import gnu.trove.TObjectIntHashMap;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.geotools.data.memory.MemoryFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.feature.FeatureTypes;
import org.geotools.feature.SchemaException;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.feature.type.BasicFeatureTypes;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: GISGMLZoneExporter.java,v 1.1.6.1 2008-03-28 14:59:28 bmarchan Exp $
 */
public class GISDataModelToFeatureCollection implements CtuluActivity {
  boolean stop_;

  public static String getValueAsString(GISDataModel _zone, TObjectIntHashMap<AttributeDescriptor> attIdx, List<AttributeDescriptor> atts, int i, int j) {
    Object value = _zone.getValue(attIdx.get(atts.get(j)), i);
    StringBuilder sb = new StringBuilder();
    if (value instanceof GISAttributeModel) {
      GISAttributeModel md = (GISAttributeModel) value;
      sb.append('{').append(StringUtils
          .replace(ObjectUtils.toString(md.getObjectValueAt(0)), GISDataModelFeatureAdapter.VALUE_SEPARATOR, GISDataModelFeatureAdapter.VALUE_SEPARATOR_AS_STRING));
      for (int k = 1; k < md.getSize(); k++) {
        sb.append(',').append(StringUtils
            .replace(ObjectUtils.toString(md.getObjectValueAt(k)), GISDataModelFeatureAdapter.VALUE_SEPARATOR, GISDataModelFeatureAdapter.VALUE_SEPARATOR_AS_STRING));
      }
      sb.append('}');
    } else if (value != null) {
      sb.append('{').append(StringUtils.replace(value.toString(), GISDataModelFeatureAdapter.VALUE_SEPARATOR, GISDataModelFeatureAdapter.VALUE_SEPARATOR_AS_STRING));
      sb.append('}');
    }

    return sb.toString();
  }

  @Override
  public void stop() {
    stop_ = true;
  }

  public GISDataModelToFeatureCollection() {
    super();
  }

  private boolean useIdAsName_;

  /**
   * @param att
   * @return true si les donn�es font parties des donn�es support�es par les exports SIG. (cf ShapefileDataStore)
   */
  public static boolean isAccepted(GISAttributeInterface att) {
    Class dataClass = att.getDataClass();
    return Date.class.isAssignableFrom(dataClass) || Number.class.isAssignableFrom(dataClass) || CharSequence.class.isAssignableFrom(dataClass)
        || Boolean.class.equals(dataClass);
  }

  /**
   * Cr�ation de tous les attributes types a partir des attributs de la zone. L'attribut pris pour Z n'est pas cr��, car transf�r� dans le Z.
   *
   * @param _zone La zone.
   * @param _attIsZ L'attribut pris pour Z
   * @param _attrIdx Argument de sortie. Une table associant l'indice des attributs de la zone a un attribut cr��.
   * @return Les attributs cr�es.
   */
  public SimpleFeatureType createAttributes(final GISDataModel _zone, final GISAttributeDouble _attIsZ,
                                            final TObjectIntHashMap<AttributeDescriptor> _attrIdx, Class geometryClass) {
    SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
    builder.setName("zone");
    final int attributeNb = _zone.getNbAttributes();
    final TIntArrayList attribute = new TIntArrayList(attributeNb);

    for (int i = 0; i < attributeNb; i++) {
      final GISAttributeInterface processedAttribute = _zone.getAttribute(i);
      // Seul l'attribut pris pour Z n'est pas enregistr�.
      if (!processedAttribute.equals(_attIsZ) && isAccepted(processedAttribute)) {
        attribute.add(i);
        if (FuLog.isDebug()) {
          FuLog.debug("add attribute " + _zone.getAttribute(i).getID() + " classe " + _zone.getAttribute(i).getDataClass());
        }
      }
    }
    final int size = attribute.size();
    // on construit les attributs que vont bien
    // le premier correspond a la geometrie
    builder.setDefaultGeometry(BasicFeatureTypes.GEOMETRY_ATTRIBUTE_NAME);
    builder.add(BasicFeatureTypes.GEOMETRY_ATTRIBUTE_NAME, geometryClass);

    for (int i = 0; i < size; i++) {
      final int posInZone = attribute.get(i);
      final AttributeDescriptor descriptor = GISLib.createAttributeFrom(_zone.getAttribute(posInZone), useIdAsName_);
      builder.add(descriptor);
      _attrIdx.put(descriptor, posInZone);
    }

    // Le dernier correspond � l'index de la g�om�trie dans la GISZone
    GISAttributeInterface attInd = GISAttributeConstants.INDEX_GEOM;
    builder.add(attInd.getID(), attInd.getDataClass());
    return builder.buildFeatureType();
  }

  /**
   * Attention: l'outputstream n'est pas ferme.
   *
   * @param _prog
   * @param _zone
   */
  public SimpleFeatureCollection processGML(final ProgressionInterface _prog, final GISZoneCollection _zone) {
    _zone.prepareExport();
    return process(_prog, _zone, _zone.getAttributeIsZ(), _zone.getDataStoreClass());
  }

  public SimpleFeatureCollection processGML(final ProgressionInterface _prog, final GISDataModel _zone, Class dataStoreClass) {
    return process(_prog, _zone, null, dataStoreClass);
  }

  /**
   * Enregistre la zone sous un format GML. <p> Les attributs globaux sont enregistr�s comme attributs globaux de chaque g�om�trie, l'attribut pris
   * pour Z est enregistr� dans les coordonn�es Z des g�om�tries.<br> Les attributs atomiques sont enregistr�s sous forme de tableau de String,
   * s�par�s par une virgule.
   *
   * @param _prog Progression de la tache.
   * @param _zone La zone a enregistrer
   * @param _attIsZ L'attrribut pris pour Z (atomique ou non).
   */
  private SimpleFeatureCollection process(final ProgressionInterface _prog, final GISDataModel _zone, GISAttributeDouble _attIsZ, Class dataStoreClass) {
    /*
     * Pour que les z atomiques soient export�s correctement, il faut que prepareExport() ait �t� appel� en amont de cette
     * m�thode.
     */
    stop_ = false;

    final TObjectIntHashMap<AttributeDescriptor> attIdx = new TObjectIntHashMap(_zone.getNbAttributes());
    final SimpleFeatureType featureType = createAttributes(_zone, _attIsZ, attIdx, dataStoreClass);
    MemoryFeatureCollection featureCollection = new MemoryFeatureCollection(featureType);
    if (stop_) {
      return featureCollection;
    }
    try {
      final int nb = _zone.getNumGeometries();
      final ProgressionUpdater up = new ProgressionUpdater(_prog);
      up.setValue(9, nb, 10, 90);
      final List<AttributeDescriptor> attributeDescriptors = featureType.getAttributeDescriptors();
      final int nbAttribute = attributeDescriptors.size();
      for (int i = 0; i < nb; i++) {
        if (stop_) {
          return featureCollection;
        }
        List<Object> values = new ArrayList<>();
        values.add(_zone.getGeometry(i));
        for (int j = 1; j < nbAttribute - 1; j++) {

          // Pour les attributs atomiques, les valeurs sont stock�es sous forme d'un tableau {a,b,c,...}
          if (_zone.getAttribute(attIdx.get(attributeDescriptors.get(j))).isAtomicValue()) {
            values.add(getValueAsString(_zone, attIdx, attributeDescriptors, i, j));
          } /// Atributs standards.
          else {
            values.add(_zone.getValue(attIdx.get(attributeDescriptors.get(j)), i));
          }
        }

        // Enregistrement de l'index de la g�om�trie dans le fichier
        values.add(Integer.valueOf(i));

        up.majAvancement();
        featureCollection.add(SimpleFeatureBuilder.build(featureType, values, Integer.toString(i)));
      }
    } catch (Exception ex) {
      FuLog.error(ex);
    }
    return featureCollection;
  }

  public SimpleFeatureType createFeatureType(final GISDataModel _zone, final AttributeDescriptor[] _atts) throws SchemaException {
    return FeatureTypes.newFeatureType(_atts, "zone");
  }

  /**
   * A utiliser pour des donn�es sensibles lorsque l'on veut que certains attributs soient retrouv� dans toutes les langues.
   *
   * @return true si on utilise l'ID de la variable pour construire l'attribut � inclure dans le fichier de sortie.
   */
  public boolean isUseIdAsName() {
    return useIdAsName_;
  }

  public void setUseIdAsName(final boolean _useIdAsName) {
    useIdAsName_ = _useIdAsName;
  }
}
