/*
 *  @creation     8 avr. 2005
 *  @modification $Date: 2006-09-19 14:36:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;

/**
 * @author Fred Deniger
 * @version $Id: GISReprojectInterpolateur1DDouble.java,v 1.5 2006-09-19 14:36:53 deniger Exp $
 */
public class GISReprojectInterpolateur1DDouble extends GISReprojectInterpolateur1D implements
    GISReprojectInterpolateurI.DoubleTarget {

  final GISAttributeModelDoubleInterface modelSrc_;

  /**
   * @param _src les coordonnées sources
   * @param _target les coordonnées cibles
   * @param _modelSrc le model source
   */
  public GISReprojectInterpolateur1DDouble(final CoordinateSequence _src, final CoordinateSequence _target,
      final GISAttributeModelDoubleInterface _modelSrc) {
    super(_src, _target);
    modelSrc_ = _modelSrc;
  }

  int[] tmp_ = new int[2];

  @Override
  public double interpol(final int _newIdx){
    final  boolean res = super.getSrcNearest(_newIdx, tmp_);

    if (tmp_[0] == tmp_[1]) {
      return modelSrc_.getValue(tmp_[0]);
    }

    //tmpSrc est le point A
    //tmpSrc2 est le point B
    src_.getCoordinate(tmp_[0], tmpSrc_);
    if (tmpSrc2_ == null) {
      tmpSrc2_ = new Coordinate();
    }
    src_.getCoordinate(tmp_[1], tmpSrc2_);
    target_.getCoordinate(_newIdx, tmpTarget_);
    //r pour savoir ou se situe le point par rapport au segment
    double r = 0;
    if (!res) {
      r = ((tmpTarget_.x - tmpSrc_.x) * (tmpSrc2_.x - tmpSrc_.x) + (tmpTarget_.y - tmpSrc_.y)
        * (tmpSrc2_.y - tmpSrc_.y))
        / ((tmpSrc2_.x - tmpSrc_.x) * (tmpSrc2_.x - tmpSrc_.x) + (tmpSrc2_.y - tmpSrc_.y)
          * (tmpSrc2_.y - tmpSrc_.y));
    }
    final  double v1 = modelSrc_.getValue(tmp_[0]);
    final  double v2 = modelSrc_.getValue(tmp_[1]);
    //le point appartient au segment ou est "compris" dans le segment.
    if (res || (r >= 0 && r <= 1)) {
      //on interpole en fonction des distance
      final double d1 = CtuluLibGeometrie.getDistance(tmpTarget_.x, tmpTarget_.y, tmpSrc_.x, tmpSrc_.y);
      final  double d2 = CtuluLibGeometrie.getDistance(tmpSrc2_.x, tmpSrc2_.y, tmpSrc_.x, tmpSrc_.y);
      return d1 * (v2 - v1) / d2 + v1;
    }
    if (FuLog.isTrace()) {
        FuLog.trace("interpolation hazard");
    }
    //Le point est avant tmpSrc
    if (r < 0) {
      return v1;
    }
    //le point est apres tmpSrc2_
    return v2;
  }

}
