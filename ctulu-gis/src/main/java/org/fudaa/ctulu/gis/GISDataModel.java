/*
 *  @creation     20 mai 2005
 *  @modification $Date: 2006-12-05 10:12:51 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * Un mod�le associant des valeurs d'attributs � un ensemble de g�om�tries. 
 * Dans ce mod�le, les g�om�tries ont toutes le m�me nombre d'attributs du m�me type dans le m�me ordre.
 * Les g�om�tries peuvent �tre ou non de m�me type.
 * 
 * @author Fred Deniger
 * @version $Id: GISDataModel.java,v 1.6 2006-12-05 10:12:51 deniger Exp $
 */
public interface GISDataModel {

  /**
   * @return le nombre de geometries definies dans ce source
   */
  int getNumGeometries();

  /**
   * Cette m�thode est utile lorsqu'on charge des donn�es issues de sig.
   *  Elle permet de charger en memoire les donn�es des attributs <code>_att</code>
   * 
   * @param _att les attributs a charger
   */
  void preload(GISAttributeInterface[] _att, ProgressionInterface _prog);
  
  
  /**
   * @param xyToAdd
   * @return the gisDataModel with data translate with xy.
   */
  GISDataModel createTranslate(GISPoint xyToAdd);

  /**
   * Permet de recuperer la boite englobante si calcule (null sinon).
   * 
   * @return le boite englobante. Peut etre null si le calcul n'est pas immediat ou pas fait.
   */
  Envelope getEnvelopeInternal();

  /**
   * @return le nombre d'attributs definis.
   */
  int getNbAttributes();

  /**
   * @param _idxGeom l'indice de la geometrie [0,getNbGeometries[
   * @return la geometrie _i
   */
  Geometry getGeometry(int _idxGeom);

  /**
   * @param _idxAtt l'indice de l'attribute [0,getNbAttributes[
   * @return l'attribut ou <code>null</code> si non trouv�.
   */
  GISAttributeInterface getAttribute(final int _idxAtt);

  /**
   * @param _att l'attribut a tester
   * @return l'indice de l'attribut. -1 si l'attribut est introuvable.
   */
  int getIndiceOf(GISAttributeInterface _att);

  /**
   * @param _idxAtt l'indice de l'attribute
   * @param _idxGeom l'indice de la geometrie
   * @return la valeur demande ou null si mauvais indices
   */
  Object getValue(final int _idxAtt, int _idxGeom);

  /**
   * Dangereux: a utiliser que si on est sur que les valeurs sont des double.
   * 
   * @param _idxAtt l'indice de l'attributs
   * @param _idxGeom l'indice de la geom
   * @return la valeur correspondante.
   */
  double getDoubleValue(final int _idxAtt, int _idxGeom);

}
