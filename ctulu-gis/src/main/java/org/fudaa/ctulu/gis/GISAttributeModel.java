/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2008-02-01 14:42:44 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import gnu.trove.TIntIntHashMap;
import org.fudaa.ctulu.collection.CtuluCollection;

/**
 * Un mod�le associant une valeur � chaque g�ometrie d'une collection GIS {@link GISZoneCollection} pour
 * un attribut particulier.<p>
 * 
 * Une collection GIS peut comporter plusieurs de ces mod�les.
 * 
 * @author Fred Deniger
 * @version $Id: GISAttributeModel.java,v 1.5.6.1 2008-02-01 14:42:44 bmarchan Exp $
 */
public interface GISAttributeModel extends CtuluCollection {

  /**
   * Retourne l'attribut concern� par ce mod�le.
   * @return l'attribut
   */
  GISAttributeInterface getAttribute();

  /**
   * @return l'objet moyen pour ce model. pour des valeurs, il s'agira de la moyenne. Sinon la
   * premiere occurence sera renvoy�e
   */
  Object getAverage();

  /**
   * @param _idxToRemove les indices tri�s
   * @return le model
   */
  GISAttributeModel createSubModel(int[] _idxToRemove);

  /**
   * Cr�e un modele augment�. Les anciennes valeurs sont conserv�es, les nouvelles sont par d�faut.
   * @param _numObject le nombre d'objets du nouveau modele.
   * @param _newIdxOldIdx le tableau indice nouveau->indice ancien
   * @return le modele augment�.
   * @exception IllegalArgumentException Si le nombre d'objet du nouveau modele est inf�rieur � l'ancien.
   */
  GISAttributeModel createUpperModel(int _numObject,TIntIntHashMap _newIdxOldIdx);

  /**
   * @param _numObject le nombre de nouveau objet
   * @param _interpol l'interpolateur a utiliser pour cr�er les nouvelles valeurs
   * @return le nouveau model
   */
  GISAttributeModel deriveNewModel(int _numObject,GISReprojectInterpolateurI _interpol);
}
