/*
 *  @creation     21 mars 2005
 *  @modification $Date: 2008-04-01 07:22:48 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import gnu.trove.TIntIntHashMap;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;

/**
 * Une collection pour les multipoints.
 * 
 * @author Bertrand Marchand
 * @version $Id: GISZoneCollectionMultiPoint.java,v 1.1.2.2 2008-04-01 07:22:48 bmarchan Exp $
 */
public class GISZoneCollectionMultiPoint extends GISZoneCollectionGeometry {

  public GISZoneCollectionMultiPoint() {
    this(null);
  }
  
  /**
   * @param _listener le listener
   */
  public GISZoneCollectionMultiPoint(final GISZoneListener _listener) {
    this(10, _listener);
  }

  /**
   * @param _listener le listener
   */
  public GISZoneCollectionMultiPoint(final GISZoneListener _listener, final GISZoneCollectionMultiPoint _col) {
    super(_listener,_col);
  }

  public GISZoneCollectionMultiPoint(final int _nbObject, final GISZoneListener _listener) {
    super(_nbObject,_listener);
  }

  @Override
  public void addCoordinateSequence(final CoordinateSequence _seq, final Object[] _data,
      final CtuluCommandContainer _cmd) {
    if (_seq == null || _seq.size() < 1) {
      return;
    }
    
    GISMultiPoint geom=(GISMultiPoint)GISGeometryFactory.INSTANCE.createMultiPoint(_seq);
    addGeometry(geom,_data,_cmd);
  }

  @Override
  public int addGeometry(final Geometry _geom, final Object[] _data, final CtuluCommandContainer _cmd) {
    if (!(_geom instanceof GISMultiPoint))
      throw new IllegalArgumentException("Bad type geometry");
    
    return super.addGeometry(_geom, _data, _cmd);
  }

  /*
   * Surcharge pour sommet ajout� sans interpolation des valeurs atomiques.
   */
  @Override
  public int addAtomic(final int _idxGeom, final int _idxBefore, final double _x, final double _y,
      final CtuluCommandContainer _cmd) {
    if (!isGeomModifiable_) {
      return -1;
    }
    Geometry geom = (Geometry)super.geometry_.getValueAt(_idxGeom);
    final Coordinate[] oldcs = geom.getCoordinates();
    final int initSize = oldcs.length;
    if(_idxBefore<-1||_idxBefore>=initSize)
      throw new IllegalArgumentException("L'index du point � ajouter doit appartenir � la g�om�trie.");
    TIntIntHashMap newIdxOldIdx=new TIntIntHashMap(initSize);

    final Coordinate[] cs = new Coordinate[initSize + 1];
    int idx = 0;
    for (int i = -1; i < initSize; i++) {
      if (i>=0) {
        newIdxOldIdx.put(idx,i); // Pour les attributs
        cs[idx++] = (Coordinate)oldcs[i].clone();
      }
      if (i == _idxBefore) {
        cs[idx++] = new Coordinate(_x, _y, 0);
      }
    }

    //  setGeometry(_ligneIdx,geom, _cmd);
    CtuluCommandComposite cmp=new CtuluCommandComposite();
    geom = GISGeometryFactory.INSTANCE.createMultiPoint(cs);
    super.geometry_.setObject(_idxGeom, geom, cmp);

    for (int i = getNbAttributes() - 1; i >= 0; i--) {
      // attribut atomic
      if (getAttribute(i).isAtomicValue()) {
        // dans ce cas on recupere le model contenant cet attribut
        final GISAttributeModel m = getModelListener(i);
        // on recupere le sous-model concerne par la modif de geometrie
        final GISAttributeModel atomicModel = (GISAttributeModel) m.getObjectValueAt(_idxGeom);
        // on le change
        m.setObject(_idxGeom, atomicModel.createUpperModel(initSize+1, newIdxOldIdx), cmp);
      }
    }
    
    if (_cmd!=null) {
      _cmd.addCmd(cmp.getSimplify());
    }
    
    return _idxBefore+1;
  }

  @Override
  public Class getDataStoreClass() {
    return GISMultiPoint.class;
  }

  @Override
  public boolean addAll(final GISDataModel _model, final CtuluCommandContainer _cmd, final boolean _doPostImport) {
    if (!isGeomModifiable_ || _model == null) {
      return false;
    }
    // Controle que les geometries pass�e sont du bon type 
    for (int i=0; i<_model.getNumGeometries(); i++) {
    if (!(_model.getGeometry(i) instanceof GISMultiPoint))
      throw new IllegalArgumentException("Bad geometry : Not a MultiPoint");
    }
    return super.addAll(_model, _cmd, _doPostImport);
  }
}
