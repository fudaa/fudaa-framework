/*
 * @creation     6 mars 2009
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2009 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import org.fudaa.ctulu.CtuluCommandContainer;

/**
 * Une zone qui contient des blocs {@link GISGeometryCollection}. Les attributs portent sur la totalit� d'un bloc.
 * Ils ne sont pas d�clin�s par g�om�trie contenues dans le bloc. En cas d'attribut atomique, celui porte dans l'ordre sur
 * tous les sommets de toutes les g�om�tries contenues dans le bloc.
 * 
 * @statut Experimental
 * @author Bertrand Marchand
 * @version $Id$
 */
public class GISZoneCollectionBloc extends GISZoneCollectionGeometry {


  @Override
  public void addCoordinateSequence(CoordinateSequence _seq, Object[] _datas, CtuluCommandContainer _cmd) {
    throw new IllegalAccessError("Can't call this method");
  }

  @Override
  public int addGeometry(Geometry _geom, Object[] _data, CtuluCommandContainer _cmd) {
    if (!(_geom instanceof GISGeometryCollection))
      throw new IllegalArgumentException("Bad type geometry");

    return super.addGeometry(_geom, _data, _cmd);
  }
  
  @Override
  public Class<? extends GISGeometryCollection> getDataStoreClass() {
    return GISGeometryCollection.class;
  }

  @Override
  public void setCoordinateSequence(int _idx, CoordinateSequence seq, CtuluCommandContainer _cmd) {
    throw new IllegalAccessError("Can't call this method");
  }

  @Override
  public double getDoubleValue(int att, int geom) {
    throw new IllegalAccessError("Can't call this method");
  }

}
