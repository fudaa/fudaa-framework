/*
 * @creation 18 ao�t 2005
 * 
 * @modification $Date: 2006-07-13 13:34:36 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import java.text.NumberFormat;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluNumberFormatDefault;
import org.fudaa.ctulu.CtuluNumberFormatI;
import org.fudaa.ctulu.iterator.XMath;

/**
 * @author fred deniger
 * @version $Id: GISPrecision.java,v 1.3 2006-07-13 13:34:36 deniger Exp $
 */
public class GISPrecision {

  final int nbDigits_;
  double eps_;
  double pow_;

  /**
   * @param _eps
   */
  public GISPrecision(final int _eps) {
    super();
    nbDigits_ = _eps;
    eps_ = org.fudaa.ctulu.iterator.XMath.pow10(-nbDigits_);
    pow_ = XMath.pow10(nbDigits_);
  }

  CtuluNumberFormatI numberFormat;

  public CtuluNumberFormatI getFormatter() {
    if (numberFormat == null) {
      final NumberFormat fmt = CtuluLib.getDecimalFormat();
      fmt.setMaximumFractionDigits(2);
      numberFormat = new CtuluNumberFormatDefault();
      ((CtuluNumberFormatDefault) numberFormat).setFmt(fmt);
    }
    return numberFormat;

  }

  /**
   * Precision de 1E-5.
   */
  public GISPrecision() {
    this(5);
  }

  public boolean isSame(final double _x1, final double _y1, final double _x2, final double _y2) {
    return CtuluLibGeometrie.getDistance(_x1, _y1, _x2, _y2) <= eps_;
  }

  public double round(final double _x) {
    return Math.round(_x * pow_) / pow_;
  }

  protected double getEps() {
    return eps_;
  }

  protected int getNbDigits() {
    return nbDigits_;
  }

}
