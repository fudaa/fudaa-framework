/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2007-01-10 08:58:48 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import gnu.trove.TIntIntHashMap;
import gnu.trove.TIntIntIterator;
import java.util.Arrays;
import org.fudaa.ctulu.collection.CtuluArrayBoolean;

/**
 * @author Fred Deniger
 * @version $Id: GISAttributeModelBooleanArray.java,v 1.10 2007-01-10 08:58:48 deniger Exp $
 */
public class GISAttributeModelBooleanArray extends CtuluArrayBoolean implements
    GISAttributeModelObservable, GISAttributeModelBooleanInterface {

  GISAttributeInterface attribute_;

  protected transient GISAttributeListener listener_;

  private GISAttributeModelBooleanArray(final boolean[] _l, final GISAttributeModelBooleanArray _model) {
    super(0);
    list_ = _l;
    attribute_ = _model.attribute_;
    listener_ = _model.listener_;

  }

  @Override
  public GISAttributeModel createSubModel(final int[] _idxToRemove){
    final boolean[] newList = new boolean[getSize()];
    int count = 0;
    final int nb = newList.length;
    for (int i = 0; i < nb; i++) {
      if (Arrays.binarySearch(_idxToRemove, i) < 0) {
        newList[count++] = super.list_[i];
      }
    }
    final boolean[] finalList = new boolean[count];
    System.arraycopy(newList, 0, finalList, 0, finalList.length);
    return new GISAttributeModelBooleanArray(finalList, this);
  }

  protected GISAttributeModelBooleanArray() {
    super(0);
  }

  /**
   * @param _init
   * @param _attr
   */
  protected GISAttributeModelBooleanArray(final boolean[] _init, final GISAttributeInterface _attr) {
    super(_init);
    attribute_ = _attr;
  }

  /**
   * @param _init
   * @param _attr
   */
  protected GISAttributeModelBooleanArray(final Object[] _init, final GISAttributeInterface _attr) {
    super(_init);
    attribute_ = _attr;
  }

  /**
   * @param _init les valeurs initiales
   * @param _attr l'attribut
   */
  protected GISAttributeModelBooleanArray(final CtuluArrayBoolean _init, final GISAttributeInterface _attr) {
    super(_init);
    attribute_ = _attr;
  }

  @Override
  public Object getAverage(){
    return getSize() > 0 ? getObjectValueAt(0) : null;
  }

  /**
   * @param _nb le nombre initiale
   * @param _attr l'attribut
   */
  public GISAttributeModelBooleanArray(final int _nb, final GISAttributeInterface _attr) {
    super(_nb);
    Arrays.fill(list_, ((Boolean) _attr.getDefaultValue()).booleanValue());
    attribute_ = _attr;
  }

  @Override
  public GISAttributeModel createUpperModel(final int _numObject,final TIntIntHashMap _newIdxOldIdx){
    if (_numObject < getSize()) {
      throw new IllegalArgumentException("bad num objects");
    }
    if (_numObject == getSize()) {
      return this;
    }
    final boolean[] newList = new boolean[_numObject];
    Arrays.fill(newList, ((Boolean) getAttribute().getDefaultValue()).booleanValue());
    if (_newIdxOldIdx != null) {
      final TIntIntIterator it = _newIdxOldIdx.iterator();
      for (int i = _newIdxOldIdx.size(); i-- > 0;) {
        it.advance();
        final int newIdx = it.key();
        newList[newIdx] = list_[it.value()];
      }
    }
    return new GISAttributeModelBooleanArray(newList, this);
  }

  @Override
  public GISAttributeModel deriveNewModel(final int _numObject,final GISReprojectInterpolateurI _interpol){
    if (!(_interpol instanceof GISReprojectInterpolateurI.BooleanTarget)) {
      throw new IllegalArgumentException("bad interface");
    }
    return deriveNewModel(_numObject, (GISReprojectInterpolateurI.BooleanTarget) _interpol);
  }

  @Override
  public GISAttributeModelBooleanInterface deriveNewModel(final int _numObject,
    final GISReprojectInterpolateurI.BooleanTarget _interpol){
    final boolean[] newList = new boolean[_numObject];
    for (int i = newList.length - 1; i >= 0; i--) {
      newList[i] = _interpol.interpol(i);
    }
    return new GISAttributeModelBooleanArray(newList, this);
  }

  @Override
  protected void fireObjectChanged(int _indexGeom, Object _newValue){
    if (listener_ != null) {
      listener_.gisDataChanged(attribute_, _indexGeom, _newValue);
    }
  }

  protected final GISAttributeListener getListener(){
    return listener_;
  }

  @Override
  public final void setListener(final GISAttributeListener _listener){
    listener_ = _listener;
  }

  /**
   * @return l'attribut associe
   */
  @Override
  public final GISAttributeInterface getAttribute(){
    return attribute_;
  }

}
