/*
 GPL 2
 */
package org.fudaa.ctulu.gis.comparator;

import org.locationtech.jts.geom.Coordinate;
import java.util.Comparator;

/**
 *
 * @author Frederic Deniger
 */
public class CoordinateComparator extends AbstractComparator implements Comparator<Coordinate> {

  /**
   * comparaison par defaut a 1e-3
   */
  public CoordinateComparator() {
  }

  /**
   * @param eps le eps de comparaison
   */
  public CoordinateComparator(double eps) {
    super(eps);
  }

  @Override
  public int compare(Coordinate o1, Coordinate o2) {
    if (o1 == o2) {
      return 0;
    }
    if (o1 == null) {
      return -1;
    }
    if (o2 == null) {
      return 1;
    }
    return super.compare(o1.x, o1.y, o2.x, o2.y);
  }
}
