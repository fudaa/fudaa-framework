/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2006-09-19 14:36:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.fudaa.ctulu.editor.CtuluValueEditorDefaults;
import org.fudaa.ctulu.editor.CtuluValueEditorInteger;

/**
 * @author Fred Deniger
 * @version $Id$
 */
public class GISAttributeInteger extends GISAttribute {

  Integer def_ = new Integer(0);

  public GISAttributeInteger() {
    super(new CtuluValueEditorInteger());
  }

  @Override
  public GISAttributeModelObservable createAtomicModel(final Object[] _initValues, final int _nbValues) {
    if (_initValues != null) {
      if (_initValues.length != _nbValues) {
        throw new IllegalArgumentException("bad size value=" + _nbValues + " used=" + _initValues.length);
      }
      return new GISAttributeModelIntegerArray(_initValues, this);
    }
    return new GISAttributeModelIntegerArray(_nbValues, this);
  }

  public GISAttributeModelObservable createAtomicModel(final int _nbValues) {
    return createAtomicModelInteger(_nbValues);
  }

  public GISAttributeModelIntegerArray createAtomicModelInteger(final int _nbValues) {
    return new GISAttributeModelIntegerArray(_nbValues, this);
  }

  @Override
  public GISAttributeModelObservable createListModel(final int _defaultCap) {
    return createListModelInteger(_defaultCap);
  }

  public GISAttributeModelObservable createListModelInteger(final int _defaultCap) {
    return new GISAttributeModelIntegerList(_defaultCap, this);
  }

  /**
   * Cree un attribut integer, par defaut global.
   * 
   * @param _name Le nom de l'attribut
   */
  public GISAttributeInteger(final String _name) {
    super(new CtuluValueEditorInteger(), _name);
  }

  /**
   * @param _name
   * @param _atomic true si donnees sur vertex
   */
  public GISAttributeInteger(final String _name, final boolean _atomic) {
    super(new CtuluValueEditorInteger(), _name, _atomic);
  }

  /**
   * @param _name     le nom
   * @param _atomic   true si atomique : valeur pour les points des lignes bris�es.
   * @param _defValue la valeur par defaut
   */
  public GISAttributeInteger(final String _name, final boolean _atomic, final Integer _defValue) {
    this(_name, _atomic);
    def_ = _defValue;
  }

  @Override
  public final Class getDataClass() {
    return Integer.class;
  }

  @Override
  public Object getDefaultValue() {
    return def_;
  }
}
