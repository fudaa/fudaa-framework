/*
 * @creation 18 mars 2005
 *
 * @modification $Date: 2008-04-01 17:09:56 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.MultiPoint;
import org.locationtech.jts.geom.Point;

/**
 * Un multipoint est un ensemble de points, qui peut �tre r�duit � 1 point seulement.
 *
 * @author Bertrand Marchand
 * @version $Id: GISMultiPoint.java,v 1.8.6.2 2008-04-01 17:09:56 bmarchan Exp $
 */
public class GISMultiPoint extends MultiPoint implements GISCoordinateSequenceContainerInterface {
  CoordinateSequence seq_ = null;
  protected long id_ = GISGeometryFactory.INSTANCE.getNewId();

  public GISMultiPoint() {
    super(null, GISGeometryFactory.INSTANCE);
  }

  public GISMultiPoint(final Point[] _points) {
    super(_points, GISGeometryFactory.INSTANCE);
  }

  @Override
  public GISMultiPoint createTranslateGeometry(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    final int nb = getNumPoints();
    GISPoint[] translated = new GISPoint[nb];
    for (int i = 0; i < nb; i++) {
      final GISPoint p = (GISPoint) getGeometryN(i);
      translated[i] = p.createTranslateGeometry(xyToAdd);
    }
    return new GISMultiPoint(translated);
  }

  @Override
  public long getId() {
    return id_;
  }

  @Override
  public String toText() {
    return super.toText();
  }

  protected MultiPoint copyInternal() {
    Point[] points = new Point[this.geometries.length];
    for (int i = 0; i < points.length; i++) {
      points[i] = (Point) this.geometries[i].copy();
    }
    return new GISMultiPoint(points, factory);
  }

  @Override
  public boolean accept(final GISVisitor _v) {
    return _v.visitMultiPoint(this);
  }

  /**
   * Cette methode est ajout�e pour des questions de performance lors du rafraichissement de multipoints avec beaucoup
   * de points. La sequence agit directement sur les implementations internes des Point.
   */
  @Override
  public CoordinateSequence getCoordinateSequence() {
    if (seq_ == null) {
      // Doit g�rer des CoordinateSequence[]. Sinon, un getCoordinateSequence().setOrdinate(i,2) n'affecte pas le Z du multipoint,
      // mais le Z d'une copie des coordonn�es du multipoint.
      //      seq_=GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(this.getCoordinates());
      Point[] pts = new Point[getNumGeometries()];
      for (int i = 0; i < getNumGeometries(); i++)
        pts[i] = (Point) getGeometryN(i);
      seq_ = new GISSequencesCoordinateSequence(pts);
    }
    return seq_;
  }

  /**
   * @param _points
   * @param _factory
   */
  public GISMultiPoint(final Point[] _points, final GeometryFactory _factory) {
    super(_points, _factory);
  }

  public boolean fillListWithPoint(final GISCollectionPointInteface _listToFill) {
    final int nb = getNumPoints();
    for (int i = 0; i < nb; i++) {
      final GISPoint p = (GISPoint) getGeometryN(i);
      _listToFill.add(p.getX(), p.getY(), p.getZ());
    }
    return true;
  }
}
