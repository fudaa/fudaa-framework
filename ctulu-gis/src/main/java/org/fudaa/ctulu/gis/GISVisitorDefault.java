/*
 * @creation 16 ao�t 06
 * @modification $Date: 2006-09-19 14:36:53 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.GeometryCollection;
import org.locationtech.jts.geom.MultiLineString;
import org.locationtech.jts.geom.MultiPoint;
import org.locationtech.jts.geom.MultiPolygon;

/**
 * @author fred deniger
 * @version $Id: GISVisitorDefault.java,v 1.2 2006-09-19 14:36:53 deniger Exp $
 */
public abstract class GISVisitorDefault implements GISVisitor {

  @Override
  public boolean visitGeometryCollection(final GeometryCollection _p) {
    return GISLib.visitGeometry(_p, this);
  }

  @Override
  public boolean visitMultiPoint(final MultiPoint _p) {
    return GISLib.visitGeometry(_p, this);
  }

  @Override
  public boolean visitMultiPolygoneWithHole(final MultiPolygon _p) {
    return GISLib.visitGeometry(_p, this);
  }

  @Override
  public boolean visitMultiPolyligne(final MultiLineString _p) {
    return GISLib.visitGeometry(_p, this);
  }

  

}
