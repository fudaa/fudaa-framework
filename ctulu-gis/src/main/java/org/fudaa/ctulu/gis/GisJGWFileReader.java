/*
 GPL 2
 */
package org.fudaa.ctulu.gis;

import java.awt.geom.Point2D;
import java.io.File;
import java.util.List;
import org.fudaa.ctulu.CtuluIOResult;
import org.fudaa.ctulu.CtuluImageContainer;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLog;

/**
 *
 * @author Frederic Deniger
 */
public class GisJGWFileReader {

  public static TransformResult getTransformPoints(ReadResult transform, final int imageWidth, final int imageHeight) {
    if (transform == null) {
      return null;
    }
    TransformResult res = new TransformResult();
    res.imagePts = new Point2D.Double[3];
    res.reelPts = new Point2D.Double[3];

    //haut gauche
    res.imagePts[0] = new Point2D.Double(0, 0);
    res.reelPts[0] = new Point2D.Double(transform.tx, transform.ty);

    //bas gauche
    res.imagePts[1] = new Point2D.Double(0, imageHeight);
    //scaley est negatif
    res.reelPts[1] = new Point2D.Double(transform.tx, transform.ty + imageHeight * transform.scaleY);

    //bas droite
    res.imagePts[2] = new Point2D.Double(imageWidth, imageHeight);
    res.reelPts[2] = new Point2D.Double(transform.tx + imageWidth * transform.scaleX, transform.ty + imageHeight * transform.scaleY);


    return res;
  }

  public static class ReadResult {

    public double tx;
    public double ty;
    public double scaleX;
    public double scaleY;
  }

  public static class TransformResult {

    public Point2D.Double[] imagePts;
    public Point2D.Double[] reelPts;
  }

  public static TransformResult getTransformPoints(ReadResult transform, CtuluImageContainer image) {

    final int imageHeight = image.getImageHeight();
    final int imageWidth = image.getImageWidth();


    return getTransformPoints(transform, imageWidth, imageHeight);
  }

  public CtuluIOResult<ReadResult> read(File in) {
    CtuluLog log = new CtuluLog();
    CtuluIOResult<ReadResult> res = new CtuluIOResult<GisJGWFileReader.ReadResult>(log);
    List<String> litFichierLineByLine = CtuluLibFile.litFichierLineByLine(in);
    if (litFichierLineByLine.size() != 6) {
      log.addError(CtuluLib.getS("Le contenu du fichier de g�or�f�rencement n'est pas valide ( il doit contenir 6 lignes)"));
      return res;
    }
    ReadResult result = new ReadResult();
    try {
      result.scaleX = Double.parseDouble(litFichierLineByLine.get(0));
    } catch (NumberFormatException numberFormatException) {
      log.addError(CtuluLib.getS("La ligne {0} n'est pas valide et ne peut pas �tre transform�e en nombre", "1"));
    }
    try {
      result.scaleY = Double.parseDouble(litFichierLineByLine.get(3));
    } catch (NumberFormatException numberFormatException) {
      log.addError(CtuluLib.getS("La ligne {0} n'est pas valide et ne peut pas �tre transform�e en nombre", "3"));
    }
    try {
      result.tx = Double.parseDouble(litFichierLineByLine.get(4));
    } catch (NumberFormatException numberFormatException) {
      log.addError(CtuluLib.getS("La ligne {0} n'est pas valide et ne peut pas �tre transform�e en nombre", "5"));
    }
    try {
      result.ty = Double.parseDouble(litFichierLineByLine.get(5));
    } catch (NumberFormatException numberFormatException) {
      log.addError(CtuluLib.getS("La ligne {0} n'est pas valide et ne peut pas �tre transform�e en nombre", "6"));
    }
    res.setSource(result);
    return res;
  }
}
