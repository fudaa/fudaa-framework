/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2006-09-19 14:36:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.fudaa.ctulu.editor.CtuluValueEditorDefaults;

/**
 * @author Fred Deniger
 * @version $Id: GISAttributeBoolean.java,v 1.10 2006-09-19 14:36:53 deniger Exp $
 */
public class GISAttributeBoolean extends GISAttribute {

  /**
   *
   */
  public GISAttributeBoolean() {
    super(CtuluValueEditorDefaults.STRING_EDITOR);
  }

  /**
   * @param _name
   */
  public GISAttributeBoolean(final String _name) {
    super(CtuluValueEditorDefaults.STRING_EDITOR, _name);
  }

  public GISAttributeBoolean(final String _name, final boolean _isAtomic) {
    super(CtuluValueEditorDefaults.STRING_EDITOR, _name, _isAtomic);
  }

  @Override
  public GISAttributeModelObservable createAtomicModel(final Object[] _initValues, final int _nbValues) {
    if (_initValues != null) {
      if (_initValues.length != _nbValues) { throw new IllegalArgumentException("bad size value=" + _nbValues
          + " used=" + _initValues.length); }
      return new GISAttributeModelBooleanArray(_initValues, this);
    }

    return new GISAttributeModelBooleanArray(_nbValues, this);
  }

  public GISAttributeModelObservable createAtomicModel(final int _nbValues) {
    return createAtomicModelBoolean(_nbValues);
  }

  public GISAttributeModelBooleanArray createAtomicModelBoolean(final int _nbValues) {
    return new GISAttributeModelBooleanArray(_nbValues, this);
  }

  @Override
  public GISAttributeModelObservable createListModel(final int _nbCap) {
    return createListModelBoolean(_nbCap);
  }

  public GISAttributeModelBooleanList createListModelBoolean(final int _nbCap) {
    return new GISAttributeModelBooleanList(_nbCap, this);
  }

  @Override
  public final Class getDataClass() {
    return Boolean.class;
  }

  @Override
  public Object getDefaultValue() {
    return Boolean.FALSE;
  }
}
