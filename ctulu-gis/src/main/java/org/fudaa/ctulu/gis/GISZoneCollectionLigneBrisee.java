/*
 * @creation 21 mars 2005
 * 
 * @modification $Date: 2008-04-01 07:22:47 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.fudaa.ctulu.CtuluCommandContainer;

/**
 * @author Fred Deniger
 * @version $Id: GISZoneCollectionLigneBrisee.java,v 1.20.6.4 2008-04-01 07:22:47 bmarchan Exp $
 */
public class GISZoneCollectionLigneBrisee extends GISZoneCollectionGeometry {

  public GISZoneCollectionLigneBrisee() {
    this(null);
  }
  
  /**
   * @param _listener le listener
   */
  public GISZoneCollectionLigneBrisee(final GISZoneListener _listener) {
    this(10, _listener);
  }

  /**
   * @param _listener le listener
   */
  public GISZoneCollectionLigneBrisee(final GISZoneListener _listener, final GISZoneCollectionLigneBrisee _col) {
    super(_listener,_col);
  }

  public GISZoneCollectionLigneBrisee(final int _nbObject, final GISZoneListener _listener) {
    super(_nbObject,_listener);
  }

  private List createListForSeveralAdd(final Object[][] _att, final int _nbGeom) {
    final int nbAttributes = getNbAttributes();
    final List attListe = new ArrayList(nbAttributes);
    for (int i = 0; i < nbAttributes; i++) {
      final Object[] oi = new Object[_nbGeom];
      for (int j = 0; j < _nbGeom; j++) {
        oi[j] = _att[j][i];
      }
      attListe.add(oi);
    }
    return attListe;
  }

  public void addAllLineStringClosedOrNode(final LineString[] _gs, final CtuluCommandContainer _cmd) {
    if (isGeomModifiable_) {

      final Object[][] att = new Object[_gs.length][];
      for (int i = 0; i < _gs.length; i++) {
        att[i] = createAttributeList(null, _gs[i], _gs[i].isClosed());
      }

      geometry_.addAll(_gs, createListForSeveralAdd(att, _gs.length), _cmd);
    }
  }

  public void addAllPolygones(final GISPolygone[] _gs, final CtuluCommandContainer _cmd) {
    if (isGeomModifiable_) {
      final Object[][] att = new Object[_gs.length][];
      for (int i = 0; i < _gs.length; i++) {
        att[i] = createAttributeList(null, _gs[i], true);
      }
      geometry_.addAll(_gs, createListForSeveralAdd(att, _gs.length), _cmd);
    }
  }

  public void addAllPolylignes(final GISPolyligne[] _gs, final CtuluCommandContainer _cmd) {
    if (isGeomModifiable_) {
      final Object[][] att = new Object[_gs.length][];
      for (int i = 0; i < _gs.length; i++) {
        att[i] = createAttributeList(null, _gs[i], false);
      }
      geometry_.addAll(_gs, createListForSeveralAdd(att, _gs.length), _cmd);
    }
  }

  @Override
  public void addCoordinateSequence(final CoordinateSequence _seq, final Object[] _data,
      final CtuluCommandContainer _cmd) {
    if (_seq == null || _seq.size() < 2) {
      return;
    }
    if (_seq.size() > 3 && _seq.getCoordinate(0).equals2D(_seq.getCoordinate(_seq.size() - 1))) {
      addPolygone(_seq, _data, _cmd);
    } else {
      addPolyligne(_seq, _data, _cmd);
    }
  }

  @Override
  public int addGeometry(final Geometry _geom, final Object[] _data, final CtuluCommandContainer _cmd) {
    if (!(_geom instanceof LineString))
      throw new IllegalArgumentException("Bad type geometry");
    
    return super.addGeometry(_geom, _data, _cmd);
  }


  public void addPolygone(final CoordinateSequence _g, final CtuluCommandContainer _cmd) {
    addPolygone((GISPolygone) GISGeometryFactory.INSTANCE.createLinearRing(_g), _cmd);
  }

  public void addPolygone(final CoordinateSequence _g, final Object[] _data, final CtuluCommandContainer _cmd) {
    addPolygone((GISPolygone) GISGeometryFactory.INSTANCE.createLinearRing(_g), _data, _cmd);
  }

  public void addPolygone(final GISPolygone _g, final CtuluCommandContainer _cmd) {
    addPolygone(_g, null, _cmd);
  }

  /**
   * Ajoute un polygone avec ses datas.
   * Pour un data atomique, le nombre de valeurs du tableau peut �tre �gal a nb points-1. Le data atomique est alors recr��
   * avec nb points. La derniere valeur n'est pas prise en compte par le polygone, mais assure l'int�grit� de la GISZoneCollection.
   * @param _g Le polygone.
   * @param _data Les datas pour chaque attribut
   * @param _cmd LE manager de commandes.
   */
  public void addPolygone(final GISPolygone _g, final Object[] _data, final CtuluCommandContainer _cmd) {
    if (isGeomModifiable_) {
      super.geometry_.add(_g, Arrays.asList(createAttributeList(_data, _g, true)), _cmd);
    }
  }

  public void addPolygoneNiveau(final CoordinateSequence _g, final CtuluCommandContainer _cmd) {
    addPolygone((GISPolygone) GISGeometryFactory.INSTANCE.createLinearRingNiveau(_g), _cmd);
  }

  /**
   * Ajoute une polyligne sous forme d'une liste de coordonn�es, avec attributs et gestionnaire de commandes.
   * 
   * @param _g La liste de coordonn�es, renseign�e en Z.
   * @param _data Les attributs, dans l'ordre dans lequel ils sont d�finis dans la collection.
   * @param _cmd Le gestionnaire de commandes. Peut �tre null.
   */
  public void addPolyligne(final CoordinateSequence _g, final Object[] _data, final CtuluCommandContainer _cmd) {
    addPolyligne((GISPolyligne) GISGeometryFactory.INSTANCE.createLineString(_g), _data, _cmd);
  }

  public void addPolyligne(final GISCoordinateSequence _g, final CtuluCommandContainer _cmd) {
    addPolyligne((GISPolyligne) GISGeometryFactory.INSTANCE.createLineString(_g), _cmd);
  }

  public void addPolyligne(final GISPolyligne _g, final CtuluCommandContainer _cmd) {
    addPolyligne(_g, null, _cmd);
  }

  /**
   * Ajoute une polyligne, avec attributs et gestionnaire de commandes.
   * 
   * @param _g La polyligne, renseign�e en Z.
   * @param _data Les attributs, dans l'ordre dans lequel ils sont d�finis dans la collection.
   * @param _cmd Le gestionnaire de commandes. Peut �tre null.
   */
  public void addPolyligne(final GISPolyligne _g, final Object[] _data, final CtuluCommandContainer _cmd) {
    if (isGeomModifiable_) {
      super.geometry_.add(_g, Arrays.asList(createAttributeList(_data, _g, false)), _cmd);
    }
  }

  public void addPolyligneNiveau(final GISCoordinateSequence _g, final CtuluCommandContainer _cmd) {
    addPolyligne((GISPolyligne) GISGeometryFactory.INSTANCE.createLineStringNiveau(_g), _cmd);
  }

  /**
   * @return true si contient au moins un polygone (ligne brisee fermee).
   */
  public boolean containsPolygone() {
    final GISVisitorChooser chooser = new GISVisitorChooser();
    for (int i = getNumGeometries() - 1; i >= 0; i--) {
      ((GISGeometry) getGeometry(i)).accept(chooser);
      if (chooser.isPolygone()) { return true; }
    }
    return false;
  }

  /**
   * @return true si contient au moins un polygone (ligne brisee fermee).
   */
  public boolean containsPolyligne() {
    final GISVisitorChooser chooser = new GISVisitorChooser();
    for (int i = getNumGeometries() - 1; i >= 0; i--) {
      ((GISGeometry) getGeometry(i)).accept(chooser);
      if (chooser.isPolyligne()) { return true; }
    }
    return false;
  }

  @Override
  public Class getDataStoreClass() {
    return LineString.class;
  }
}
