/*
 GPL 2
 */
package org.fudaa.ctulu.gis.comparator;

import org.locationtech.jts.geom.Point;
import java.util.Comparator;

/**
 *
 * @author Frederic Deniger
 */
public class PointComparator extends AbstractComparator implements Comparator<Point> {

  /**
   * comparaison par defaut a 1e-3
   */
  public PointComparator() {
  }

  /**
   * @param eps le eps de comparaison
   */
  public PointComparator(double eps) {
    super(eps);
  }

  @Override
  public int compare(Point o1, Point o2) {
    if (o1 == o2) {
      return 0;
    }
    if (o1 == null) {
      return -1;
    }
    if (o2 == null) {
      return 1;
    }
    return super.compare(o1.getX(), o1.getY(), o2.getX(), o2.getY());
  }
}
