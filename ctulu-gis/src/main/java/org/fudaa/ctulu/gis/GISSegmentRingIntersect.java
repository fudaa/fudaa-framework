/*
 * @creation 29 mars 2006
 * @modification $Date: 2006-07-13 13:34:36 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.algorithm.CGAlgorithms;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.LinearRing;
import java.util.Arrays;

/**
 * @author fred deniger
 * @version $Id: GISSegmentRingIntersect.java,v 1.2 2006-07-13 13:34:36 deniger Exp $
 */
public class GISSegmentRingIntersect {

  private final Coordinate tmp1_ = new Coordinate();

  private final Coordinate tmp2_ = new Coordinate();

  private final Coordinate[] resCor_ = new Coordinate[2];

  private final int[] resIdx_ = new int[2];

  public GISSegmentRingIntersect() {}

  LinearRing frRing_;

  private final GISInfiniteLineIntersector line_ = new GISInfiniteLineIntersector();

  private void clearRes() {
    Arrays.fill(resIdx_, -1);
    Arrays.fill(resCor_, null);
  }

  public Coordinate getCoordinate1() {
    return resCor_[0];
  }

  public Coordinate getCoordinate2() {
    return resCor_[1];
  }

  public int getIdx1() {
    return resIdx_[0];
  }

  public int getIdx2() {
    return resIdx_[1];
  }

  public boolean isCoorNull() {
    return resCor_[0] == null || resCor_[1] == null;
  }

  /**
   * @param _l1 le point 1 de la ligne et le point source
   * @param _l2 le point 2 de la ligne
   * @param _p1 utilise pour arrang� les points tel que vect(_p1,_p2) et vect(_p1,_seq12[1]) soit direct
   * @param _p2 le point 2 pour orienter le resultat
   */
  public void compute(final Coordinate _l1, final Coordinate _l2, final Coordinate _p1, final Coordinate _p2) {
    clearRes();
    final int nbPt = frRing_.getNumPoints();
    // la distance la plus petite pour la parite sup
    double d1 = -1;
    // la deuxieme distance la plus petite pout la parite inf
    double d2 = -1;
    final CoordinateSequence seq = frRing_.getCoordinateSequence();
    for (int i = nbPt - 1; i > 0; i--) {
      seq.getCoordinate(i, tmp1_);
      seq.getCoordinate(i - 1, tmp2_);
      line_.computeIntersectionLineSegment(_l1, _l2, tmp1_, tmp2_);
      if (line_.hasIntersection()) {
        final Coordinate intersect = line_.getIntersectedPoint();
        final double d = CtuluLibGeometrie.getD2(_l1.x, _l1.y, intersect.x, intersect.y);
        if (CGAlgorithms.orientationIndex(_p1, _p2, intersect) == CGAlgorithms.COUNTERCLOCKWISE) {
          if (d2 < 0 || d < d2) {
            d2 = d;
            resCor_[1] = new Coordinate(intersect);
            resIdx_[1] = i - 1;
          }
        } else {
          if (d1 < 0 || d < d1) {
            d1 = d;
            resCor_[0] = new Coordinate(intersect);
            resIdx_[0] = i - 1;
          }

        }

      }
    }
    if (resCor_[1] != null) {
      final int trigo = CGAlgorithms.orientationIndex(_p1, _p2, resCor_[1]);
      if (trigo != CGAlgorithms.COUNTERCLOCKWISE) {
        final Coordinate tmp = resCor_[0];
        final int tmpi = resIdx_[0];
        resCor_[0] = resCor_[1];
        resIdx_[0] = resIdx_[1];
        resCor_[1] = tmp;
        resIdx_[1] = tmpi;
      }
    }
  }

  public LinearRing getFrRing() {
    return frRing_;
  }

  public void setFrRing(final LinearRing _frRing) {
    if (frRing_ != _frRing) {
      clearRes();
      frRing_ = _frRing;
    }
  }

}
