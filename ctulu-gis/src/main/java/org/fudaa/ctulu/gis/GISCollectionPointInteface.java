/*
 * @creation 11 f�vr. 2004
 * @modification $Date: 2006-02-09 08:59:28 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

/**
 * Inteface commune pour des listes contenants des coordonnees de points.
 * @author deniger
 * @version $Id: GISCollectionPointInteface.java,v 1.3 2006-02-09 08:59:28 deniger Exp $
 */
public interface GISCollectionPointInteface {

  /**
   * Ajoute le point de coordonnees _x,_y,_z. Ne fait pas de controle pour savoir si un point
   * identique est deja present ou non.
   * @param _x x du point a ajouter
   * @param _y y du point a ajouter
   * @param _z z du point a ajouter
   */
  boolean add(double _x,double _y,double _z);

  /**
   * Ajoute tous les points de la liste.
   * @param _v la liste a ajouter
   */
  boolean add(GISCollectionPointInteface _v);

  /**
   * Ajoute les coordonnees du point _p.
   * @param _p le point a ajouter
   */
  boolean addCopyOf(GISPoint _p);

  /**
   * @param _x x du point a tester
   * @param _y y du point a tester
   * @param _z z du point a tester
   * @return true si contient un point de coordonnees (_x,_y,_z)
   */
  boolean isContained(double _x,double _y,double _z);

  /**
   * @param _indiceDuPointAEnlever l'indice du point a enlever de la liste
   * @return true si point enlever
   */
  boolean remove(int _indiceDuPointAEnlever);

  /**
   * Insere le point (_x,_y,_z) a l'indice _i.
   * @param _i l'indice d'insertion
   * @param _x x du point a inserer
   * @param _y y du point a inserer
   * @param _z z du point a inserer
   * @return true si inserer
   */
  boolean insert(int _i,double _x,double _y,double _z);

  /**
   * @param _i l'index d'insertion
   * @param _p le point a inserer
   * @return true si point insere
   */
  boolean insert(int _i,GISPoint _p);

  /**
   * @return le nombre de point contenu dans cette liste
   */
  int getNbPoint();

  /**
   * Remplace le point a l'indice _idxToReplace par le point (_newX,_newY,_newZ).
   * @param _idxPtToReplace l'indice du point a remplacer
   * @param _newX nouvelle valeur de x
   * @param _newY nouvelle valeur de y
   * @param _newZ nouvelle valeur de z
   * @return true si operation reussie. false si par exemple l'indice est hors limite.
   */
  boolean set(int _idxPtToReplace,double _newX,double _newY,double _newZ);

  /**
   * Remplace le point a l'indice _idxToReplace par les coordonnes du point _p.
   * @param _idxToReplace
   * @param _p
   */
  boolean set(int _idxToReplace,GISPoint _p);

  /**
   * @param _i l'indice voulu
   * @return copie du point _i
   */
  GISPointMutable getMutable(int _i);

  /**
   * @param _i
   * @return point immutable
   */
  GISPoint get(int _i);

  /**
   * @param _i l'indice demande
   * @param _p le point a remplir avec les coordonnes du point _i
   * @return true si operation reussie (si _i est dans les limites de la liste)
   */
  boolean get(int _i,GISPointMutable _p);

  /**
   * @param _i l'indice du point
   * @return x du point _i
   */
  double getX(int _i);

  /**
   * @param _i l'indice du point
   * @return y du point _i
   */
  double getY(int _i);

  /**
   * @param _i l'indice du point
   * @return z du point _i
   */
  double getZ(int _i);

  /**
   * Vide la liste.
   */
  void removeAll();
}