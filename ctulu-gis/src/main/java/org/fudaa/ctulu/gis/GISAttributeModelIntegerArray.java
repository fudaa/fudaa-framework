/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2007-01-10 08:58:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import gnu.trove.TIntIntHashMap;
import gnu.trove.TIntIntIterator;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.collection.CtuluArrayInteger;

/**
 * @author Fred Deniger
 * @version $Id: GISAttributeModelIntegerArray.java,v 1.10 2007-01-10 08:58:47 deniger Exp $
 */
public class GISAttributeModelIntegerArray extends CtuluArrayInteger implements
    GISAttributeModelObservable,GISAttributeModelIntegerInterface {

  GISAttributeInterface attribute_;

  protected transient GISAttributeListener listener_;

  protected GISAttributeModelIntegerArray() {
    super(0);
  }

  @Override
  public Object getAverage(){
    if (list_.length == 0) {
      return new Integer(0);
    }
    return Integer.valueOf((int)CtuluLibArray.getMoyenne(list_));
  }

  protected GISAttributeModelIntegerArray(final int _size, final GISAttributeInterface _attr) {
    super(_size);
    Arrays.fill(list_, ((Integer) _attr.getDefaultValue()).intValue());
    attribute_ = _attr;
  }

  /**
   * @param _init
   */
  protected GISAttributeModelIntegerArray(final int[] _init, final GISAttributeInterface _attr) {
    super(_init);
    attribute_ = _attr;
  }

  /**
   * @param _init
   */
  protected GISAttributeModelIntegerArray(final Object[] _init, final GISAttributeInterface _attr) {
    super(_init);
    attribute_ = _attr;
  }

  private GISAttributeModelIntegerArray(final int[] _l, final GISAttributeModelIntegerArray _model) {
    super(0);
    list_ = _l;
    attribute_ = _model.attribute_;
    listener_ = _model.listener_;

  }

  @Override
  public GISAttributeModel deriveNewModel(final int _numObject,final GISReprojectInterpolateurI _interpol){
    if (!(_interpol instanceof GISReprojectInterpolateurI.IntegerTarget)) {
      throw new IllegalArgumentException("bad interface");
    }
    return deriveNewModel(_numObject, (GISReprojectInterpolateurI.IntegerTarget) _interpol);
  }

  @Override
  public GISAttributeModelIntegerInterface deriveNewModel(final int _numObject,
    final GISReprojectInterpolateurI.IntegerTarget _interpol){
    final int[] newList = new int[_numObject];
    for (int i = newList.length - 1; i >= 0; i--) {
      newList[i] = _interpol.interpol(i);
    }
    return new GISAttributeModelIntegerArray(newList, this);
  }

  @Override
  public GISAttributeModel createSubModel(final int[] _idxToRemove){
    final int[] newList = new int[getSize()];
    int count = 0;
    final int nb = newList.length;
    for (int i = 0; i < nb; i++) {
      if (Arrays.binarySearch(_idxToRemove, i) < 0) {
        newList[count++] = super.list_[i];
      }
    }
    final int[] finalList = new int[count];
    System.arraycopy(newList, 0, finalList, 0, finalList.length);
    return new GISAttributeModelIntegerArray(finalList, this);
  }

  @Override
  public GISAttributeModel createUpperModel(final int _numObject,final TIntIntHashMap _newIdxOldIdx){
    if (_numObject < getSize()) {
      throw new IllegalArgumentException("bad num objects");
    }
    if (_numObject == getSize()) {
      return this;
    }
    final int[] newList = new int[_numObject];
    Arrays.fill(newList, ((Integer) getAttribute().getDefaultValue()).intValue());
    if (_newIdxOldIdx != null) {
      final  TIntIntIterator it = _newIdxOldIdx.iterator();
      for (int i = _newIdxOldIdx.size(); i-- > 0;) {
        it.advance();
        final  int newIdx = it.key();
        newList[newIdx] = list_[it.value()];
      }
    }
    return new GISAttributeModelIntegerArray(newList, this);
  }

  @Override
  protected void fireObjectChanged(int _indexGeom, Object _newValue){
    if (listener_ != null) {
      listener_.gisDataChanged(attribute_, _indexGeom, _newValue);
    }
  }

  protected final GISAttributeListener getListener(){
    return listener_;
  }

  @Override
  public final void setListener(final GISAttributeListener _listener){
    listener_ = _listener;
  }

  /**
   * @return l'attribut associe
   */
  @Override
  public final GISAttributeInterface getAttribute(){
    return attribute_;
  }

}
