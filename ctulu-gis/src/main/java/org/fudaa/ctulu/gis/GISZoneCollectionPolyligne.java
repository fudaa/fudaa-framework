/*
 *  @creation     21 mars 2005
 *  @modification $Date: 2007-02-02 11:20:05 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.LineString;
import org.fudaa.ctulu.CtuluCommandContainer;

/**
 * @author Fred Deniger
 * @version $Id: GISZoneCollectionPolyligne.java,v 1.8 2007-02-02 11:20:05 deniger Exp $
 */
public class GISZoneCollectionPolyligne extends GISZoneCollectionLigneBrisee {

  public GISZoneCollectionPolyligne() {
    this(null);
  }

  /**
   * @param _listener le listener
   */
  public GISZoneCollectionPolyligne(final GISZoneListener _listener) {
    this(10, _listener);
  }

  @Override
  public final Class getDataStoreClass() {
    return LineString.class;
  }

  @Override
  public boolean containsPolygone() {
    return false;
  }

  @Override
  public boolean containsPolyligne() {
    return getNumGeometries() > 0;
  }

  /**
   * @param _nbObject le nombre d'objets attendus
   * @param _listener le listener
   */
  public GISZoneCollectionPolyligne(final int _nbObject, final GISZoneListener _listener) {
    super(_nbObject, _listener);
  }

  @Override
  public void addPolygone(final GISPolygone _g, final CtuluCommandContainer _cmd) {
    new Throwable().printStackTrace();
  }

  @Override
  public void addPolygone(final GISPolygone _g, final Object[] _data, final CtuluCommandContainer _cmd) {
    new Throwable().printStackTrace();
  }

  public void setValue(final int _idx, final GISPolygone _newLine, final CtuluCommandContainer _cmd) {
    new Throwable().printStackTrace();
  }

  @Override
  public void addAllPolygones(final GISPolygone[] _gs, final CtuluCommandContainer _cmd) {
    new Throwable().printStackTrace();
  }

}
