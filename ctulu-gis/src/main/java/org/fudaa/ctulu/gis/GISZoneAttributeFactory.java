/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2008-03-26 16:46:43 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import java.util.Arrays;

/**
 * @author Fred Deniger
 * @version $Id: GISZoneAttributeFactory.java,v 1.10.6.1 2008-03-26 16:46:43 bmarchan Exp $
 */
public final class GISZoneAttributeFactory {

  private GISZoneAttributeFactory() {
    super();
  }

  /**
   * @param _att l'attribut
   * @param _src la liste des points source
   * @param _target la liste des points cible
   * @param _model le model initial
   * @return l'interpolateur 1d
   */
  public static GISReprojectInterpolateurI create1DInterpolateur(final GISAttributeInterface _att,
      final GISCoordinateSequenceContainerInterface _src, final GISCoordinateSequenceContainerInterface _target, final GISAttributeModel _model) {
    return create1DInterpolateur(_att,_src.getCoordinateSequence(),_target.getCoordinateSequence(), _model);
  }

  /**
   * @param _att l'attribut
   * @param _src la liste des points source
   * @param _target la liste des points cible
   * @param _model le model initial
   * @return l'interpolateur 1d
   */
  public static GISReprojectInterpolateurI create1DInterpolateur(final GISAttributeInterface _att,
      final Geometry _src, final Geometry _target, final GISAttributeModel _model) {
    return create1DInterpolateur(_att,new GISCoordinateSequence(_src.getCoordinates()),new GISCoordinateSequence(_target.getCoordinates()), _model);
  }

  /**
   * @param _att l'attribut
   * @param _src la liste des points source
   * @param _target la liste des points cible
   * @param _model le model initial
   * @return l'interpolateur 1d
   */
  public static GISReprojectInterpolateurI create1DInterpolateur(final GISAttributeInterface _att, final CoordinateSequence _src, final CoordinateSequence _target, final GISAttributeModel _model) {
    final Class clazz = _att.getDataClass();
    if (clazz == Integer.class) {
      return new GISReprojectInterpolateur1DInteger(_src, _target, (GISAttributeModelIntegerInterface) _model);
    }
    else if (clazz == Double.class) {
      return new GISReprojectInterpolateur1DDouble(_src, _target, (GISAttributeModelDoubleInterface) _model);
    }
    else if (clazz == Boolean.class) {
      return new GISReprojectInterpolateur1DBoolean(_src, _target, (GISAttributeModelBooleanInterface) _model);
    }
    else if (clazz == Long.class) {
      return new GISReprojectInterpolateur1DLong(_src, _target, (GISAttributeModelLongInterface) _model);
    }
    else {
      return new GISReprojectInterpolateur1DObject(_src, _target, (GISAttributeModelObjectInterface) _model);
    }
  }

  public static GISAttributeModel createModelList(final GISAttributeInterface _att, final Object _valatt, final int _capacity,
      final GISAttributeListener _listener, final int _initValuesNb) {
    final GISAttributeModelObservable resfinal = _att.createListModel(_capacity);
    if (_initValuesNb > 0) {
      final Object[] add = new Object[_initValuesNb];
      Arrays.fill(add, _valatt);
      resfinal.addAllObject(add, null);
    }
    resfinal.setListener(_listener);
    return resfinal;
  }

  /**
   * @param _initSize la taille
   * @param _listener le listener
   * @return le model. null si class non valide
   */
  public static GISAttributeModel createModelArray(final GISAttributeInterface _att, final Object _valatt, final int _initSize,
      final GISAttributeListener _listener) {
    final GISAttributeModelObservable resfinal = (GISAttributeModelObservable)_att.createDataForGeom(_valatt, _initSize);
    resfinal.setListener(_listener);
    return resfinal;
  }

  public static Object[] createObjectArray(final GISAttributeInterface _att, final Object[] _init, final int _nbPt) {
    if (_init == null) {
      return null;
    }
    final Class clazz = _att.getDataClass();
    if (clazz == Integer.class) {
      final Integer[] r = new Integer[_nbPt];
      Arrays.fill(r, _att.getDefaultValue());

      for (int i = Math.min(r.length, _init.length) - 1; i >= 0; i--) {
        final Object o = _init[i];
        r[i] = (Integer) _att.getDefaultValue();
        if (o != null && o instanceof Integer) {
          r[i] = (Integer) o;
        }
      }
      return r;
    } else if (clazz == Double.class) {
      final Double[] r = new Double[_nbPt];
      Arrays.fill(r, _att.getDefaultValue());
      for (int i = Math.min(r.length, _init.length) - 1; i >= 0; i--) {
        final Object o = _init[i];
        r[i] = (Double) _att.getDefaultValue();
        if (o != null && o instanceof Double) {
          r[i] = (Double) o;
        }
      }
      return r;
    } else if (clazz == Boolean.class) {
      final Boolean[] r = new Boolean[_nbPt];
      Arrays.fill(r, _att.getDefaultValue());
      for (int i = Math.min(r.length, _init.length) - 1; i >= 0; i--) {
        final Object o = _init[i];
        r[i] = (Boolean) _att.getDefaultValue();
        if (o != null && o instanceof Boolean) {
          r[i] = (Boolean) o;
        }
      }
      return r;
    } else if (clazz == Long.class) {
      final Long[] r = new Long[_nbPt];
      Arrays.fill(r, _att.getDefaultValue());
      for (int i = Math.min(r.length, _init.length) - 1; i >= 0; i--) {
        final Object o = _init[i];
        r[i] = (Long) _att.getDefaultValue();
        if (o != null && o instanceof Long) {
          r[i] = (Long) o;
        }
      }
      return r;
    } else {
      final Object[] r = new Object[_nbPt];
      Arrays.fill(r, _att.getDefaultValue());
      for (int i = Math.min(r.length, _init.length) - 1; i >= 0; i--) {
        final Object o = _init[i];
        r[i] = _att.getDefaultValue();
        if (o != null) {
          r[i] = o;
        }
      }
      return r;
    }
  }

  /**
   * @param _att l'attribut
   * @param _initValues les valeurs initiales
   * @param _listener le listener
   * @return le model. null si class non valide
   */
  public static GISAttributeModel createModelArray(final GISAttributeInterface _att, final Object[] _initValues,
      final GISAttributeListener _listener) {
    GISAttributeModelObservable resfinal = null;
    final Class clazz = _att.getDataClass();
    if (clazz == Integer.class) {
      resfinal = new GISAttributeModelIntegerArray(_initValues, _att);
    } else if (clazz == Double.class) {
      resfinal = new GISAttributeModelDoubleArray(_initValues, _att);
    } else if (clazz == Boolean.class) {
      resfinal = new GISAttributeModelBooleanArray(_initValues, _att);
    } else if (clazz == Long.class) {
      resfinal = new GISAttributeModelLongArray(_initValues, _att);
    } else {
      resfinal = new GISAttributeModelObjectArray(_initValues, _att);
    }
    resfinal.setListener(_listener);
    return resfinal;
  }

  /**
   * @param _listener le listener
   * @param _attr les attributs
   * @return une zone correctement initialisée
   */
  public static GISZoneCollectionLigneBrisee createPolyligne(final GISZoneListener _listener,
      final GISAttributeInterface[] _attr) {
    final GISZoneCollectionLigneBrisee zone = new GISZoneCollectionLigneBrisee(_listener);
    zone.initAttributes(_attr);
    return zone;

  }

  /**
   * @param _listener le listener
   * @param _attr les attributs
   * @return une zone correctement initialisée
   */
  public static GISZoneCollectionPoint createPoint(final GISZoneListener _listener, final GISAttributeInterface[] _attr) {
    final GISZoneCollectionPoint zone = new GISZoneCollectionPoint(_listener);
    zone.initAttributes(_attr);
    return zone;

  }

  /**
   * @param _listener le listener
   * @param _attr les attributs
   * @return une zone correctement initialisée
   */
  public static GISZoneCollectionLigneBrisee createLigneBrisee(final GISZoneListener _listener,
      final GISAttributeInterface[] _attr) {
    final GISZoneCollectionLigneBrisee zone = new GISZoneCollectionLigneBrisee(_listener);
    zone.initAttributes(_attr);
    return zone;

  }

  /**
   * @param _zone la zone concernée
   * @param _att l'attribut
   * @return le model ou nul si inconnu
   */
  public static GISAttributeModel createModel(final GISZoneCollection _zone, final GISAttributeInterface _att) {
    final int nbGeom = _zone.getNumGeometries();
    if (_att == null) return null;
    Object valatt=_zone.fixedAttributes_.get(_att);
    if (valatt==null) valatt=_att.getDefaultValue();
    if (!_att.isAtomicValue() || (_zone instanceof GISZoneCollectionPoint)) {
      return createModelList(_att, valatt, (int) (nbGeom * 1.5), _zone, nbGeom);
    }
    final GISAttributeModel[] models = new GISAttributeModel[nbGeom];
    for (int i = 0; i < nbGeom; i++) {
      models[i] = createModelArray(_att, valatt, _zone.getGeometry(i).getNumPoints(), _zone);
    }
    final GISAttributeModelObjectList r = new GISAttributeModelObjectList(Arrays.asList(models), _att);
    r.setListener(_zone);
    return r;
  }
}
