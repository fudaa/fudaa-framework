/*
 * @creation 18 mars 2005
 *
 * @modification $Date: 2007-01-17 10:45:25 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Polygon;

/**
 * Un polygone avec des trous ...
 *
 * @author Fred Deniger
 * @version $Id: GISPolygoneWithHole.java,v 1.6 2007-01-17 10:45:25 deniger Exp $
 */
public class GISPolygoneWithHole extends Polygon implements GISGeometry {
  protected long id_ = GISGeometryFactory.INSTANCE.getNewId();

  @Override
  public long getId() {
    return id_;
  }

  public GISPolygoneWithHole() {
    super(new GISPolygone(), null, GISGeometryFactory.INSTANCE);
  }

  /**
   * @param _shell le bord
   * @param _holes les trous
   */
  public GISPolygoneWithHole(final LinearRing _shell, final LinearRing[] _holes) {
    super(_shell, _holes, GISGeometryFactory.INSTANCE);
  }

  @Override
  public GISPolygoneWithHole createTranslateGeometry(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    GISPolygone newSheel = new GISPolygone(GISGeometryFactory.INSTANCE.createTranslated(super.shell.getCoordinateSequence(), xyToAdd));
    GISPolygone[] newHoles = null;
    if (super.holes != null) {
      newHoles = new GISPolygone[super.holes.length];
      for (int i = 0; i < newHoles.length; i++) {
        newHoles[i] = new GISPolygone(GISGeometryFactory.INSTANCE.createTranslated(super.holes[i].getCoordinateSequence(), xyToAdd));
      }
    }

    return new GISPolygoneWithHole(newSheel, newHoles);
  }

  @Override
  protected Polygon copyInternal() {
    LinearRing shellCopy = (LinearRing) shell.copy();
    LinearRing[] holeCopies = new LinearRing[this.holes.length];
    for (int i = 0; i < holes.length; i++) {
      holeCopies[i] = (LinearRing) holes[i].copy();
    }
    return new GISPolygoneWithHole(shellCopy, holeCopies, factory);
  }

  @Override
  public boolean accept(final GISVisitor _v) {
    return _v.visitPolygoneWithHole(this);
  }

  /**
   * @param _shell le bord
   * @param _holes les trous
   * @param _factory l'usine
   */
  public GISPolygoneWithHole(final LinearRing _shell, final LinearRing[] _holes, final GeometryFactory _factory) {
    super(_shell, _holes, _factory);
  }
}
