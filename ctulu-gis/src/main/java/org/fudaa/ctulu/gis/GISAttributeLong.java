/*
 * @creation 7 avr. 2005
 * 
 * @modification $Date: 2006-09-19 14:36:53 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.fudaa.ctulu.editor.CtuluValueEditorLong;

/**
 * @author Fred Deniger
 * @version $Id: GISAttributeInteger.java,v 1.8 2006-09-19 14:36:53 deniger Exp $
 */
public class GISAttributeLong extends GISAttribute {

  Long def_ = Long.valueOf(0);

  /**
   * @param def the def_ to set
   */
  public void setDefaultValue(Long def) {
    def_ = def;
  }

  public GISAttributeLong() {
    super(new CtuluValueEditorLong());
  }

  /**
   * @param _name
   */
  public GISAttributeLong(final String _name) {
    super(new CtuluValueEditorLong(), _name);
  }

  /**
   * @param _name
   * @param _atomic true si donnees sur vertex
   */
  public GISAttributeLong(final String _name, final boolean _atomic) {
    super(new CtuluValueEditorLong(), _name, _atomic);
  }

  public GISAttributeModelObservable createAtomicModel(final int _nbValues) {
    return createAtomicModelLong(_nbValues);
  }

  @Override
  public GISAttributeModelObservable createAtomicModel(final Object[] _initValues, final int _nbValues) {
    if (_initValues != null) {
      if (_initValues.length != _nbValues) { throw new IllegalArgumentException("bad size value=" + _nbValues
          + " used=" + _initValues.length); }
      return new GISAttributeModelLongArray(_initValues, this);
    }
    return new GISAttributeModelLongArray(_nbValues, this);
  }

  public GISAttributeModelLongArray createAtomicModelLong(final int _nbValues) {
    return new GISAttributeModelLongArray(_nbValues, this);
  }

  @Override
  public GISAttributeModelObservable createListModel(final int _defaultCap) {
    return createListModelLong(_defaultCap);
  }

  public GISAttributeModelObservable createListModelLong(final int _defaultCap) {
    return new GISAttributeModelLongList(_defaultCap, this);
  }

  @Override
  public final Class getDataClass() {
    return Long.class;
  }

  @Override
  public Object getDefaultValue() {
    return def_;
  }
}
