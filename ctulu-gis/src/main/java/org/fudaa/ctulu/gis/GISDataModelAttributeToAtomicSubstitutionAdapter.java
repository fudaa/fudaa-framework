/*
 * @creation 14 oct. 2008
 * 
 * @modification $Date:$
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * Permet de prendre en compte les donn�es enregistrer avec des valeurs atomique rang�es dans des valeurs non atomiques. S'appuie sur le proc�d� de
 * GISDataModelFeatureAdapter pour reconstruire les donn�es atomiques.
 *
 * @author Fred Deniger
 * @version $Id$
 */
public class GISDataModelAttributeToAtomicSubstitutionAdapter implements GISDataModel {

  private final GISDataModel model_;
  private final GISAttributeInterface initNotAtomic;
  private final int idxAttribute;
  private final GISAttributeInterface targetAtomic;

  /**
   *
   * @param _model
   * @param initNotAtomic l'attribute non atomique a remplacer
   * @param targetAtomic l'attribute cible
   */
  public GISDataModelAttributeToAtomicSubstitutionAdapter(GISDataModel _model, GISAttributeInterface initNotAtomic, GISAttributeInterface targetAtomic) {
    if (initNotAtomic == null || targetAtomic == null) {
      throw new IllegalArgumentException("_substitutionTable et _model ne doivent pas �tre null.");
    }
    if (!targetAtomic.isAtomicValue()) {
      throw new IllegalArgumentException("target doit etre atomic");
    }
    model_ = _model;
    this.idxAttribute = model_.getIndiceOf(initNotAtomic);
    if (idxAttribute == -1) {
      throw new IllegalArgumentException(initNotAtomic.getID() + "n'est pas dans le model.");
    }
    this.initNotAtomic = initNotAtomic;
    this.targetAtomic = targetAtomic;

  }



  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    return new GISDataModelAttributeToAtomicSubstitutionAdapter(model_.createTranslate(xyToAdd), initNotAtomic, targetAtomic);
  }


  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getAttribute(int)
   */
  @Override
  public GISAttributeInterface getAttribute(int _att) {
    if (_att == idxAttribute) {
      return targetAtomic;
    }
    return model_.getAttribute(_att);
  }


  @Override
  public void preload(GISAttributeInterface[] _att, ProgressionInterface _prog) {
    if (_att == null) {
      return;
    }
    GISAttributeInterface[] targetAtt = new GISAttributeInterface[_att.length];
    for (int i = 0; i < targetAtt.length; i++) {
      targetAtt[i] = _att[i];
      if (ObjectUtils.equals(targetAtomic, targetAtt[i])) {
        targetAtt[i] = initNotAtomic;
      }
    }
    model_.preload(targetAtt, _prog);
  }

  @Override
  public Envelope getEnvelopeInternal() {
    return model_.getEnvelopeInternal();
  }

  @Override
  public Geometry getGeometry(int _geom) {
    return model_.getGeometry(_geom);
  }

  @Override
  public int getIndiceOf(GISAttributeInterface _att) {

    if (_att == targetAtomic || (ObjectUtils.equals(_att, targetAtomic))) {
      return idxAttribute;
    }
    return model_.getIndiceOf(_att);
  }

  @Override
  public int getNbAttributes() {
    return model_.getNbAttributes();
  }

  @Override
  public int getNumGeometries() {
    return model_.getNumGeometries();
  }

  @Override
  public double getDoubleValue(int _att, int _geom) {
    return model_.getDoubleValue(_att, _geom);
  }


  @Override
  public Object getValue(int _att, int _geom) {
    if (_att == idxAttribute && !initNotAtomic.isAtomicValue() && model_.getGeometry(_geom).getNumPoints() > 1) {
      String initStringValue = ObjectUtils.toString(model_.getValue(_att, _geom));
      initStringValue = StringUtils.trim(initStringValue);
      initStringValue = initStringValue.substring(1, initStringValue.length() - 1);
      Object[] values = new Object[model_.getGeometry(_geom).getNumPoints()];
      String[] svals = initStringValue.split(GISDataModelFeatureAdapter.VALUE_SEPARATOR);
      int max = Math.min(svals.length, values.length);
      for (int j = 0; j < svals.length; j++) {
        svals[j] = StringUtils.replace(svals[j], GISDataModelFeatureAdapter.VALUE_SEPARATOR_AS_STRING, GISDataModelFeatureAdapter.VALUE_SEPARATOR);
      }
      final Class targetDataClass = targetAtomic.getDataClass();
      if (targetDataClass.equals(Integer.class)) {
        for (int j = 0; j < max; j++) {
          values[j] = Integer.valueOf(svals[j]);
        }
      } else if (targetDataClass.equals(Boolean.class)) {
        for (int j = 0; j < max; j++) {
          values[j] = Boolean.valueOf(svals[j]);
        }
      } else if (targetDataClass.equals(Double.class)) {
        for (int j = 0; j < max; j++) {
          values[j] = Double.valueOf(svals[j]);
        }
      } else if (targetDataClass.equals(String.class)) {
        for (int j = 0; j < max; j++) {
          values[j] = svals[j];
        }
      }
      return targetAtomic.createDataForGeom(values, values.length);
    } else {
      return model_.getValue(_att, _geom);
    }

  }
}
