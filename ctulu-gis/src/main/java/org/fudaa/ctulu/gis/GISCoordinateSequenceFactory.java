/*
 * The JTS Topology Suite is a collection of Java classes that implement the fundamental operations required to validate
 * a given geo-spatial data set to a known topological specification.
 * 
 * Copyright (C) 2001 Vivid Solutions
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to
 * the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * For more information, contact:
 * 
 * Vivid Solutions Suite #1A 2328 Government Street Victoria BC V8T 5G5 Canada
 * 
 * (250)385-6040 www.vividsolutions.com
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.*;

/**
 * @author Fred Deniger
 * @version $Id: GISCoordinateSequenceFactory.java,v 1.7 2007-01-17 10:45:25 deniger Exp $
 */
public class GISCoordinateSequenceFactory implements CoordinateSequenceFactory {

  private GISCoordinateSequenceEmpty empty_;

  public GISCoordinateSequenceFactory() {
  }

  public GISCoordinateSequenceEmpty getEmpty() {
    if (empty_ != null) {
      empty_ = new GISCoordinateSequenceEmpty();
    }
    return empty_;
  }

  public GISCoordinateSequence transformToCustom(CoordinateSequence in) {
    if(in==null|| GISCoordinateSequence.class.equals(in.getClass())){
      return (GISCoordinateSequence) in;
    }
    return (GISCoordinateSequence) create(in);

  }

  @Override
  public CoordinateSequence create(final Coordinate[] _coordinates) {
    //vide on renvoie la sequence vide!
    if (_coordinates == null) {
      return null;
    }
    if (_coordinates.length == 0) {
      return getEmpty();
    }
    //unique on renvoie l'unique
    if (_coordinates.length == 1) {
      return new GISCoordinateSequenceUnique(_coordinates[0]);
    }
    //la sequence par defaut
    return new GISCoordinateSequence(_coordinates);
  }

  public CoordinateSequence createTranslated(CoordinateSequence init, Point xyToAdd) {
    if (init == null || xyToAdd == null) {
      return init;
    }
    Coordinate[] translated = new Coordinate[init.size()];
    for (int i = 0; i < translated.length; i++) {
      translated[i] = init.getCoordinateCopy(i);
      translated[i].x = translated[i].x + xyToAdd.getX();
      translated[i].y = translated[i].y + xyToAdd.getY();
    }
    return create(translated);

  }

  @Override
  public CoordinateSequence create(final CoordinateSequence _coordSeq) {
    if (_coordSeq == null) {
      return null;
    }
    if (_coordSeq == empty_ || _coordSeq.size() == 0) {
      return getEmpty();
    }
    if (_coordSeq.size() == 1) {
      return new GISCoordinateSequenceUnique(_coordSeq.getCoordinate(0));
    }
    return new GISCoordinateSequence(_coordSeq);
  }

  @Override
  public CoordinateSequence create(final int _size, final int _dim) {
    if (_size == 0) {
      return getEmpty();
    }
    if (_size == 1) {
      return new GISCoordinateSequenceUnique(0, 0, 0,_dim);
    }
    return new GISCoordinateSequence(_size,_dim);
  }

  public CoordinateSequence createImmutable(final Coordinate[] _coordinates) {
    //vide on renvoie la sequence vide!
    if (_coordinates == null) {
      return null;
    }
    if (_coordinates.length == 0) {
      return getEmpty();
    }
    //unique on renvoie l'unique
    if (_coordinates.length == 1) {
      return new GISCoordinateSequenceUniqueImmutable(_coordinates[0]);
    }
    //la sequence par defaut
    return new GISCoordinateSequenceImmutable(_coordinates);
  }

  public CoordinateSequence createImmutable(final CoordinateSequence _coordSeq) {
    if (_coordSeq == null) {
      return null;
    }
    if (_coordSeq == empty_ || _coordSeq.size() == 0) {
      return getEmpty();
    }
    if (_coordSeq.size() == 1) {
      return new GISCoordinateSequenceUniqueImmutable(_coordSeq.getCoordinate(0));
    }
    return new GISCoordinateSequenceImmutable(_coordSeq);
  }

  public CoordinateSequence createImmutable(final int _size, final int _dim) {
    if (_size == 0) {
      return getEmpty();
    }
    if (_size == 1) {
      return new GISCoordinateSequenceUniqueImmutable(0, 0, 0);
    }
    return new GISCoordinateSequenceImmutable(_size);
  }

}
