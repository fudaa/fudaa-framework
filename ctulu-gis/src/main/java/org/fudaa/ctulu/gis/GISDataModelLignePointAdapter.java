/*
 * @creation 26 mai 2005
 * 
 * @modification $Date: 2007-01-10 08:58:48 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import java.util.List;

/**
 * Cette classe permet d'adapter une zone a un comme source d'import. Il est possible de choisir les attributs a montrer
 * 
 * @author Fred Deniger
 * @version $Id: GISDataModelLignePointAdapter.java,v 1.10 2007-01-10 08:58:48 deniger Exp $
 */
public class GISDataModelLignePointAdapter extends GISDataModelAbstract {

  final int idxLine_;
  final boolean isFerme_;

  /**
   * @param _zone la zone a adapter
   * @param _numLigne l'indice de la ligne brisee utilisee
   * @param _attShown les indices a "montrer"
   */
  public GISDataModelLignePointAdapter(final GISDataModel _zone, final int _numLigne, final int[] _attShown, final boolean _isFerme) {
    super(_attShown, _zone);
    idxLine_ = _numLigne;
    isFerme_ = _isFerme;
  }

  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    return new GISDataModelLignePointAdapter(super.model_.createTranslate(xyToAdd), idxLine_, super.attShown_, isFerme_);
  }

  @Override
  public Envelope getEnvelopeInternal() {
    return model_.getGeometry(idxLine_).getEnvelopeInternal();
  }

  @Override
  public Geometry getGeometry(final int _idxGeom) {
    return ((LineString) model_.getGeometry(idxLine_)).getPointN(_idxGeom);
  }

  @Override
  public int getNumGeometries() {
    return isFerme_ ? model_.getGeometry(idxLine_).getNumPoints() - 1 : model_.getGeometry(idxLine_).getNumPoints();
  }

  @Override
  public Object getValue(final int _idxAtt, final int _idxPt) {
    final int idx = attShown_ == null ? _idxAtt : attShown_[_idxAtt];
    return getAttribute(_idxAtt).isAtomicValue() ? ((GISAttributeModel) model_.getValue(idx, idxLine_)).getObjectValueAt(_idxPt) : model_.getValue(
        idx, idxLine_);
  }

  @Override
  public double getDoubleValue(final int _idxAtt, final int _idxPt) {
    final int idx = attShown_ == null ? _idxAtt : attShown_[_idxAtt];
    return getAttribute(_idxAtt).isAtomicValue() ? ((GISAttributeModelDoubleInterface) model_.getValue(idx, idxLine_)).getValue(_idxPt) : model_
        .getDoubleValue(idx, idxLine_);
  }

  /**
   * @param _collection la zone a adapter
   * @param _idxLigne l'indice de la ligne
   * @param _att les attributs dans l'ordre voulu.
   * @return l'adapteur
   */
  public static GISDataModelLignePointAdapter buildAdapter(final GISDataModel _collection, final int _idxLigne, final GISAttributeInterface[] _att) {
    final int[] idx = new int[_att.length];
    for (int i = _att.length - 1; i >= 0; i--) {
      idx[i] = _collection.getIndiceOf(_att[i]);
    }
    return new GISDataModelLignePointAdapter(_collection, _idxLigne, idx, ((LineString) _collection.getGeometry(_idxLigne)).isClosed());

  }

  /**
   * @param _dest la destination recevant les GISDataModelLignePointAdapter
   * @param _collection la zone a adapter
   * @param _att les attributs dans l'ordre voulu.
   */
  public static void buildAdapters(final List _dest, final GISDataModel _collection, final GISAttributeInterface[] _att) {
    final int[] idx = new int[_att.length];
    for (int i = _att.length - 1; i >= 0; i--) {
      idx[i] = _collection.getIndiceOf(_att[i]);
    }
    final int nbPt = _collection.getNumGeometries();
    for (int i = 0; i < nbPt; i++) {
      _dest.add(new GISDataModelLignePointAdapter(_collection, i, idx, ((LineString) _collection.getGeometry(i)).isClosed()));
    }
  }
}
