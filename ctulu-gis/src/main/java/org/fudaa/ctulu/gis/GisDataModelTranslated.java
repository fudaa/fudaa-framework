package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Point;
import org.fudaa.ctulu.ProgressionInterface;

public class GisDataModelTranslated implements GISDataModel {

  private final GISDataModel init;
  private final double x;
  private final double y;
  private final GISPoint xyToAdd;

  public GisDataModelTranslated(GISDataModel init, Point xyToAdd) {
    this.init = init;
    this.x = xyToAdd == null ? 0 : xyToAdd.getX();
    this.y = xyToAdd == null ? 0 : xyToAdd.getY();
    this.xyToAdd = new GISPoint(this.x, this.y, 0);
  }

  public GisDataModelTranslated(GISDataModel init, double x, double y) {
    this.init = init;
    this.x = x;
    this.y = y;
    xyToAdd = new GISPoint(this.x, this.y, 0);
  }

  @Override
  public int getNumGeometries() {
    return init.getNumGeometries();
  }

  @Override
  public void preload(GISAttributeInterface[] _att, ProgressionInterface _prog) {
    init.preload(_att, _prog);
  }

  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    return new GisDataModelTranslated(this, xyToAdd);
  }

  @Override
  public Envelope getEnvelopeInternal() {
    Envelope internal = init.getEnvelopeInternal();
    if (internal == null) {
      return internal;
    }
    internal = new Envelope(internal);
    internal.translate(x, y);
    return internal;
  }

  @Override
  public int getNbAttributes() {
    return init.getNbAttributes();
  }

  @Override
  public Geometry getGeometry(int _idxGeom) {
    return (Geometry) ((GISGeometry) init.getGeometry(_idxGeom)).createTranslateGeometry(xyToAdd);
  }

  @Override
  public GISAttributeInterface getAttribute(int _idxAtt) {
    return init.getAttribute(_idxAtt);
  }

  @Override
  public int getIndiceOf(GISAttributeInterface _att) {
    return init.getIndiceOf(_att);
  }

  @Override
  public Object getValue(int _idxAtt, int _idxGeom) {
    return init.getValue(_idxAtt, _idxGeom);
  }

  @Override
  public double getDoubleValue(int _idxAtt, int _idxGeom) {
    return init.getDoubleValue(_idxAtt, _idxGeom);
  }

}
