/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2007-04-16 16:33:52 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.editor.CtuluValueEditorDefaults;
import org.fudaa.ctulu.editor.CtuluValueEditorDouble;

/**
 * @author Fred Deniger
 * @version $Id: GISAttributeDouble.java,v 1.10 2007-04-16 16:33:52 deniger Exp $
 */
public class GISAttributeDouble extends GISAttribute {

  Double def_ = CtuluLib.ZERO;

  public GISAttributeDouble() {
    super(CtuluValueEditorDefaults.DOUBLE_EDITOR);
  }

  /**
   * @param _name le nom
   * @param _atomic true si atomique : valeur pour les points des lignes bris�es.
   */
  public GISAttributeDouble(final String _name, final boolean _atomic) {
    super(CtuluValueEditorDefaults.DOUBLE_EDITOR, _name, _atomic);
  }

  /**
   * @param _name le nom
   * @param _atomic true si atomique : valeur pour les points des lignes bris�es.
   * @param _defValue la valeur par defaut
   */
  public GISAttributeDouble(final String _name, final boolean _atomic, final Double _defValue) {
    super(CtuluValueEditorDefaults.DOUBLE_EDITOR, _name, _atomic);
    def_ = _defValue;
  }

  public GISAttributeModelObservable createAtomicModel(final int _nbValues) {
    return createAtomicModelDouble(_nbValues);
  }

  @Override
  public GISAttributeModelObservable createAtomicModel(final Object[] _initValues, final int _nbValues) {
    if (_initValues != null) {
      if (_initValues.length != _nbValues) {
        throw new IllegalArgumentException("bad size value=" + _nbValues + " used=" + _initValues.length);
      }
      return new GISAttributeModelDoubleArray(_initValues, this);
    }

    return new GISAttributeModelDoubleArray(_nbValues, this);
  }

  public GISAttributeModelDoubleArray createAtomicModelDouble(final int _nbValues) {
    return new GISAttributeModelDoubleArray(_nbValues, this);
  }

  @Override
  public GISAttributeModelObservable createListModel(final int _defaultCap) {
    return createListModelDouble(_defaultCap);
  }

  public GISAttributeModelObservable createListModelDouble(final int _defaultCap) {
    return new GISAttributeModelDoubleList(_defaultCap, this);
  }

  @Override
  public final Class getDataClass() {
    return Double.class;
  }

  @Override
  public Object getDefaultValue() {
    return def_;
  }

  @Override
  public int getPrecision() {
    return 50;
  }

  public void setEditable(final boolean _b) {
    if (_b) {
      setInternEditor(CtuluValueEditorDefaults.DOUBLE_EDITOR);
    } else {
      final CtuluValueEditorDouble editor = new CtuluValueEditorDouble();
      editor.setEditable(false);
      setInternEditor(editor);
    }
  }
}
