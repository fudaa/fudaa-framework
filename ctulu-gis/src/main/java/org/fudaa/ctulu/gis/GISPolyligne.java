/*
 * @creation 18 mars 2005
 *
 * @modification $Date: 2007-01-17 10:45:25 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;

/**
 * @author Fred Deniger
 * @version $Id: GISPolyligne.java,v 1.7 2007-01-17 10:45:25 deniger Exp $
 */
public class GISPolyligne extends LineString implements GISPointContainerInterface, GISLigneBrisee {
  protected long id_ = GISGeometryFactory.INSTANCE.getNewId();

  @Override
  public long getId() {
    return id_;
  }

  @Override
  public GISPolyligne createTranslateGeometry(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    return new GISPolyligne(GISGeometryFactory.INSTANCE.createTranslated(getCoordinateSequence(), xyToAdd));
  }

  /**
   * @param _points
   */
  public GISPolyligne(final CoordinateSequence _points) {
    super(_points, GISGeometryFactory.INSTANCE);
  }

  public GISPolyligne() {
    super(new GISCoordinateSequenceEmpty(), GISGeometryFactory.INSTANCE);
  }

  @Override
  public boolean accept(final GISVisitor _visitor) {
    return _visitor.visitPolyligne(this);
  }

  /**
   * @return pour definir si c'est une courbe de niveau
   */
  public boolean isNiveau() {
    return false;
  }

  protected LineString copyInternal() {
    return new GISPolyligne(points.copy(), factory);
  }

  @Override
  public boolean fillListWithPoint(final GISCollectionPointInteface _listToFill) {
    final CoordinateSequence seq = getCoordinateSequence();
    final int nb = seq.size();
    for (int i = 0; i < nb; i++) {
      _listToFill.add(seq.getX(i), seq.getY(i), seq.getOrdinate(i, 2));
    }
    return true;
  }

  /**
   * @param _points
   * @param _factory
   */
  public GISPolyligne(final CoordinateSequence _points, final GeometryFactory _factory) {
    super(_points, _factory);
  }

  @Override
  public boolean getSegment(int idx, GISSegment seg) {
    if ((seg == null) || (idx < 0) || (idx > this.getNbSegment())) {
      return false;
    }

    seg.setPt1(this.getCoordinateN(idx));
    seg.setPt2(this.getCoordinateN(idx + 1));

    return true;
  }

  @Override
  public GISSegment getSegment(int idx) {
    GISSegment seg = new GISSegment();

    if (!this.getSegment(idx, seg)) {
      return null;
    }

    return seg;
  }

  @Override
  public int getNbSegment() {
    return (this.getNumPoints() - 1);
  }
}
