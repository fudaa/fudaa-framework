/*
 *  @creation     31 mai 2005
 *  @modification $Date: 2006-02-09 08:59:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;


/**
 * @author Fred Deniger
 * @version $Id: GISLigneBrisee.java,v 1.2 2006-02-09 08:59:28 deniger Exp $
 */
public interface GISLigneBrisee extends GISCoordinateSequenceContainerInterface{

  public boolean getSegment(int idx, GISSegment seg);
  public GISSegment getSegment(int idx);
  public int getNbSegment();
  public boolean isClosed();
}
