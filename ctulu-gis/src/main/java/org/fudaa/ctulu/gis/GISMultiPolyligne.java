/*
 * @creation 18 mars 2005
 *
 * @modification $Date: 2007-01-17 10:45:25 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.MultiLineString;

/**
 * @author Fred Deniger
 * @version $Id: GISMultiPolyligne.java,v 1.8 2007-01-17 10:45:25 deniger Exp $
 */
public class GISMultiPolyligne extends MultiLineString implements GISPointContainerInterface, GISGeometry {
  protected long id_ = GISGeometryFactory.INSTANCE.getNewId();

  @Override
  public long getId() {
    return id_;
  }

  public GISMultiPolyligne() {
    this(null);
  }

  public GISMultiPolyligne(final LineString[] _lineStrings) {
    super(_lineStrings, GISGeometryFactory.INSTANCE);
  }

  @Override
  public GISGeometry createTranslateGeometry(GISPoint xyToAdd) {
    if (xyToAdd == null || super.geometries == null) {
      return this;
    }
    LineString[] newLines = new LineString[super.geometries.length];
    for (int i = 0; i < newLines.length; i++) {
      final GISPolyligne p = (GISPolyligne) getGeometryN(i);
      newLines[i] = p.createTranslateGeometry(xyToAdd);
    }
    return new GISMultiPolyligne(newLines);
  }

  @Override
  public boolean accept(final GISVisitor _v) {
    return _v.visitMultiPolyligne(this);
  }

  @Override
  protected MultiLineString copyInternal() {
    LineString[] lineStrings = new LineString[this.geometries.length];
    for (int i = 0; i < lineStrings.length; i++) {
      lineStrings[i] = (LineString) this.geometries[i].copy();
    }
    return new GISMultiPolyligne(lineStrings, factory);
  }

  /**
   * @param _lineStrings
   * @param _factory
   */
  public GISMultiPolyligne(final LineString[] _lineStrings, final GeometryFactory _factory) {
    super(_lineStrings, _factory);
  }

  @Override
  public boolean fillListWithPoint(final GISCollectionPointInteface _listToFill) {
    final int nb = getNumGeometries();
    for (int i = 0; i < nb; i++) {
      final GISPolyligne p = (GISPolyligne) getGeometryN(i);
      p.fillListWithPoint(_listToFill);
    }
    return true;
  }
}
