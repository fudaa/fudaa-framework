/*
 * @creation 8 avr. 2005
 * 
 * @modification $Date: 2006-04-05 10:02:13 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

/**
 * @author Fred Deniger
 * @version $Id: GISReprojectInterpolateurI.java,v 1.3 2006-04-05 10:02:13 deniger Exp $
 */
public interface GISReprojectInterpolateurI {

  /**
   * Interpolateur pour objet.
   * 
   * @author Fred Deniger
   * @version $Id: GISReprojectInterpolateurI.java,v 1.3 2006-04-05 10:02:13 deniger Exp $
   */
  interface ObjectTarget {
    /**
     * @param _newIdx l'indice de la nouvelle valeur
     * @return la valeur
     */
    Object interpol(final int _newIdx);
  }

  /**
   * Interpolateur pour Boolean.
   */
  interface BooleanTarget {
    /**
     * @param _newIdx l'indice de la nouvelle valeur
     * @return la valeur
     */
    boolean interpol(final int _newIdx);
  }

  /**
   * Interpolateur pour double.
   */
  interface DoubleTarget {
    /**
     * @param _newIdx l'indice de la nouvelle valeur
     * @return la valeur
     */
    double interpol(final int _newIdx);
  }

  /**
   * Interpolateur pour entier.
   */
  interface IntegerTarget {
    /**
     * @param _newIdx l'indice de la nouvelle valeur
     * @return la valeur
     */
    int interpol(final int _newIdx);
  }

  interface LongTarget {
    /**
     * @param _newIdx l'indice de la nouvelle valeur
     * @return la valeur
     */
    long interpol(final int _newIdx);
  }
}
