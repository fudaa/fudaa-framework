package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import java.util.Comparator;

public class GISLigneBriseeComparator implements Comparator<GISLigneBrisee>
{
  private GISSegmentComparator comparator;
  
  public GISLigneBriseeComparator()
  {
    this(0.1d);
  }
  
  public GISLigneBriseeComparator(double eps)
  {
    this.comparator = new GISSegmentComparator(eps);
  }
  
  public double getEps()
  {
    return this.comparator.getEps();
  }

  public void setEps(double eps)
  {
    this.comparator.setEps(eps);
  }

  @Override
  public int compare(GISLigneBrisee line1, GISLigneBrisee line2)
  {
    if (line1.getNbSegment() < line2.getNbSegment())
    {
      return -1;
    }
    else if (line1.getNbSegment() > line2.getNbSegment())
    {
      return 1;
    }
    
    int nbSegs = line1.getNbSegment();
    int idxFirst = -1;
    GISSegment seg1 = new GISSegment();
    GISSegment seg2 = new GISSegment();
    
    line1.getSegment(0, seg1);
    
    // Cherche quel segment de la ligne 2 correspond au premier de la ligne 1
    for (int i = 0; i < nbSegs; i++)
    {
      line2.getSegment(i, seg2);
      
      if (this.comparator.compare(seg1, seg2) == 0)
      {
        idxFirst = i;
        
        break;
      }
    }
    
    if (idxFirst == -1)
    {
      return this.testLines(line1, line2);
    }
    
    boolean increment = true;
    int idx = this.getNextIdx(idxFirst, nbSegs, increment);
    
    line1.getSegment(1, seg1);
    line2.getSegment(idx, seg2);
    
    // Recherche dans quel sens boucler sur la ligne 2 pour �tre dans le m�me sens que la ligne 1.
    if (this.comparator.compare(seg1, seg2) != 0)
    {
      increment = false;
      idx = this.getNextIdx(idxFirst, nbSegs, increment);
      
      line2.getSegment(idx, seg2);
      
      if (this.comparator.compare(seg1, seg2) != 0)
      {
        return this.testLines(line1, line2);
      }
    }
    
    idx = idxFirst;
    
    for (int i = 0; i < nbSegs; i++)
    {
      line1.getSegment(i, seg1);
      line2.getSegment(idx, seg2);
      
      if (this.comparator.compare(seg1, seg2) != 0)
      {
        return this.testLines(line1, line2);
      }
      
      idx = this.getNextIdx(idx, nbSegs, increment);
    }
    
    return 0;
  }

  private int testLines(GISLigneBrisee line1, GISLigneBrisee line2)
  {
    double length1 = this.getLength(line1);
    double length2 = this.getLength(line2);
    
    if (length1 < length2)
    {
      return -1;
    }
    
    return 1;
  }
  
  private double getLength(GISLigneBrisee line)
  {
    double distance = 0d;
    CoordinateSequence coordinates = line.getCoordinateSequence();
    Coordinate coord1 = new Coordinate();
    Coordinate coord2 = new Coordinate();
    
    for (int i = 1; i < coordinates.size(); i++)
    {
      coordinates.getCoordinate(i - 1, coord1);
      coordinates.getCoordinate(i, coord2);
      
      distance += coord1.distance(coord2);
    }
    
    return distance;
  }
  
  private int getNextIdx(int idx, int nbSegs, boolean increment)
  {
    if (increment)
    {
      if (idx < (nbSegs - 1))
      {
        return idx + 1;
      }
      
      return 0;
    }
    else
    {
      if (idx > 0)
      {
        return idx - 1;
      }
      
      return nbSegs - 1;
    }
  }  
}
