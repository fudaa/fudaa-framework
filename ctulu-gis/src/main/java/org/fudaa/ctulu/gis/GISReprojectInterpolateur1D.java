/*
 *  @creation     8 avr. 2005
 *  @modification $Date: 2006-09-19 14:36:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import com.memoire.fu.FuLog;
import org.locationtech.jts.algorithm.CGAlgorithms;
import org.locationtech.jts.algorithm.LineIntersector;
import org.locationtech.jts.algorithm.RobustLineIntersector;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;

/**
 * Une interpolateur basique a utiliser pour interpoler sur une ligne.
 * @author Fred Deniger
 * @version $Id: GISReprojectInterpolateur1D.java,v 1.5 2006-09-19 14:36:53 deniger Exp $
 */
public abstract class GISReprojectInterpolateur1D implements GISReprojectInterpolateurI {

  CoordinateSequence src_;
  CoordinateSequence target_;

  double equalsEcart_ = 1E-8;
  Coordinate tmpTarget_ = new Coordinate();
  Coordinate tmpSrc_ = new Coordinate();

  /**
   * @param _src les coordonn�es sources
   * @param _target les coordonn�es cibles
   */
  public GISReprojectInterpolateur1D(final CoordinateSequence _src, final CoordinateSequence _target) {
    super();
    src_ = _src;
    target_ = _target;
  }

  /**
   * @param _targetIdx l'indice de la coordonn�e cible
   * @return l'indice d'une coordonn�e source tel que distance inf � "equalsEcart"
   */
  protected int getSrcEquivalent(final int _targetIdx){
    target_.getCoordinate(_targetIdx, tmpTarget_);
    for (int i = src_.size() - 1; i >= 0; i--) {
      src_.getCoordinate(i, tmpSrc_);
      if (CtuluLibGeometrie.getD2(tmpSrc_.x, tmpSrc_.y, tmpTarget_.x, tmpTarget_.y) <= equalsEcart_) {
        return i;
      }
    }
    return -1;
  }

  Coordinate tmpSrc2_;

  /**
   * @param _targetIdx l'indice de la coordonn�e cible
   * @return true si on a pu le determiner le point avec precision
   */
  protected boolean getSrcNearest(final int _targetIdx,final int[] _dest){
    target_.getCoordinate(_targetIdx, tmpTarget_);
    _dest[0] = -1;
    _dest[1] = -1;
    double distToCompare = Double.MAX_VALUE;
    final  int nb = src_.size() - 1;
    //tmpSrc represente le point source d'indice le plus haut
    src_.getCoordinate(nb, tmpSrc_);
    //  tmpSrc2 represente le point source d'indice le plus bas
    double d = CtuluLibGeometrie.getD2(tmpSrc_.x, tmpSrc_.y, tmpTarget_.x, tmpTarget_.y);
    //si egale on resort l'indice
    if (d < equalsEcart_) {
      _dest[0] = nb;
      _dest[1] = nb;
      return true;
    }
    if (tmpSrc2_ == null) {
      tmpSrc2_ = new Coordinate();
    }
    //pour
    final  LineIntersector lineIntersector = new RobustLineIntersector();
    for (int i = nb - 1; i >= 0; i--) {
      src_.getCoordinate(i, tmpSrc2_);
      d = CtuluLibGeometrie.getD2(tmpSrc2_.x, tmpSrc2_.y, tmpTarget_.x, tmpTarget_.y);
      //si egale on renvoie
      if (d < equalsEcart_) {
        _dest[0] = i;
        _dest[1] = i;
        return true;
      }
      lineIntersector.computeIntersection(tmpTarget_, tmpSrc_, tmpSrc2_);
      if (lineIntersector.hasIntersection()) {
        if(FuLog.isTrace()) {
          FuLog.trace("GIS: point on line found");
        }
        _dest[0] = i+1;
        _dest[1] = i;
        return true;
      }
      //on calcul la distance par rapport a la ligne
      final  double dist=CGAlgorithms.distancePointLine(tmpTarget_,tmpSrc_, tmpSrc2_);
      if(_dest[0]<0  || dist<distToCompare){
        distToCompare=dist;
        _dest[0]=i+1;
        _dest[1]=i;
      }
      
      tmpSrc_.x=tmpSrc2_.x;
      tmpSrc_.y=tmpSrc2_.y;
    }
    //pas exactement sur la ligne
    return false;
  }

  /**
   *
   */
  public GISReprojectInterpolateur1D() {
    super();
  }

}
