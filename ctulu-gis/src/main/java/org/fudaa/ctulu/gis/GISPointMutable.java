/*
 *  @creation     25 juin 2003
 *  @modification $Date: 2006-07-13 13:34:35 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Point;

/**
 * @author deniger
 * @version $Id: GISPointMutable.java,v 1.4 2006-07-13 13:34:35 deniger Exp $
 */
public class GISPointMutable extends GISPoint {
  /**
   * Ne fait rien.
   */
  public GISPointMutable() {
    super();
  }

  /**
   * @param _pt la source pour l'intialisation.
   */
  public GISPointMutable(final GISPoint _pt) {
    super(_pt);
  }

  /**
   * @param _x x intial
   * @param _y y intial
   * @param _z z intial
   */
  public GISPointMutable(final double _x, final double _y, final double _z) {
    super(_x, _y, _z);
  }

  public void initWith(final GISPoint _p) {
    if (_p != null) {
      setLocation(_p.getX(), _p.getY(), _p.getZ());
    }
  }

  @Override
  protected Point copyInternal() {
    return new GISPointMutable(this);
  }

  @Override
  public void setX(final double _x) {
    super.setX(_x);
  }

  @Override
  public void setY(final double _y) {
    super.setY(_y);
  }

  @Override
  public void setZ(final double _z) {
    super.setZ(_z);
  }

  /**
   * @param _x nouveau x
   * @param _y nouveau y
   * @param _z nouveau z
   */
  public void setLocation(final double _x, final double _y, final double _z) {
    setXYZ(_x, _y, _z);
  }
}
