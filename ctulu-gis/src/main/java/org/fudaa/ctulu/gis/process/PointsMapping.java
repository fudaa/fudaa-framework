/*
 GPL 2
 */
package org.fudaa.ctulu.gis.process;

import org.fudaa.ctulu.interpolation.SupportLocationI;

/**
 *
 * @author Frederic Deniger
 */
public interface PointsMapping extends SupportLocationI {

  int getInitialIdx(int _i);
  
  /**
   * Attention, pour des raisons de performances le tableau n'est pas copi�.
   * O
   * @return null si aucun filtre.
   */
  int[] getMapping();
  
}
