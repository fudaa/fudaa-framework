/*
 *  @creation     18 mai 2005
 *  @modification $Date: 2006-09-19 14:36:52 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import java.util.Comparator;

/**
 * @author Fred Deniger
 * @version $Id: GISAttributeNameComparator.java,v 1.6 2006-09-19 14:36:52 deniger Exp $
 */
public class GISAttributeNameComparator implements Comparator {

  public GISAttributeNameComparator() {
    super();
  }

  @Override
  public int compare(final Object _o1,final Object _o2){
    if (_o1 == _o2) {
      return 0;
    }
    if (_o1 == null || _o1 == GISAttributeConstants.TITRE) {
      return -1;
    }
    if (_o2 == null || _o2 == GISAttributeConstants.TITRE) {
      return 1;
    }
    return ((GISAttributeInterface) _o1).getName().compareTo(
        ((GISAttributeInterface) _o2).getName());
  }
}
