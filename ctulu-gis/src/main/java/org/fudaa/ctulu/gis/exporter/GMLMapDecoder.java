package org.fudaa.ctulu.gis.exporter;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.gis.*;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Point;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GMLMapDecoder {
  private Map in;
  private final String geometryKey = "geometry";
  private final String featureKey = "feature";
  private boolean applyIndexAttribute;

  /**
   * @param in la map issue de {@link GMLReader}
   * @param applyIndexAttribute if true, the attribute INDEX_GEOM will be used to order the geometries and will be removed.
   */
  public GMLMapDecoder(Map in, boolean applyIndexAttribute) {
    this.in = in;
    this.applyIndexAttribute = applyIndexAttribute;
  }

  private int findIndex(List<GISAttributeInterface> attributes) {
    for (int i = 0; i < attributes.size(); i++) {
      if (attributes.get(i).getID().equals(GISAttributeConstants.INDEX_GEOM.getID())) {
        return i;
      }
    }
    return -1;
  }

  public GISZoneCollectionGeometry decode() {
    List<GISAttributeInterface> attributes = buildAttributes();

    final Object featureCollection = in.get("featureCollection");
    if (featureCollection instanceof Map) {

      final Map featureCollectionAsMap = (Map) featureCollection;
      if (!(featureCollectionAsMap).containsKey(featureKey)) {
        featureCollectionAsMap.put(featureKey, Collections.singletonList(featureCollection));
      }
      List<GISAttributeInterface> newAttributes = null;
      String geomIndexProperty = null;
      if (applyIndexAttribute) {
        int idx = findIndex(attributes);
        if (idx >= 0) {
          geomIndexProperty = attributes.get(idx).getName();
          newAttributes = new ArrayList<>(attributes);
          newAttributes.remove(idx);
        }
      }
      final GISZoneCollectionGeometry zoneCollectionGeometry = decodeFeatureCollection(featureCollectionAsMap, attributes, geomIndexProperty);
      if (newAttributes != null) {
        zoneCollectionGeometry.setAttributes(newAttributes.toArray(new GISAttributeInterface[0]), null);
      }
      return zoneCollectionGeometry;
    }
    return createDefault();
  }

  private List<GISAttributeInterface> buildAttributes() {
    List<GISAttributeInterface> attributes = new ArrayList<>();
    if (in.get("JCSGMLInputTemplate") instanceof Map) {
      Map columns = (Map) ((Map) in.get("JCSGMLInputTemplate")).get("ColumnDefinitions");
      Object columnValue = columns.get("column");
      if (columnValue == null) {
        columnValue = Collections.singletonList(columns);
      } else if (columnValue instanceof Map) {
        columnValue = Collections.singletonList(columnValue);
      }
      if (columnValue instanceof List) {
        fillAttributes(attributes, (List) columnValue);
      }
    }
    return attributes;
  }

  private void fillAttributes(List<GISAttributeInterface> attributes, List columnAsMaps) {
    for (Object o : columnAsMaps) {
      Map column = (Map) o;
      String type = (String) column.get("type");
      if ("DOUBLE".equals(type)) {
        attributes.add(GISLib.createAttributeFrom((String) column.get("name"), Double.class));
      }
      if ("INTEGER".equals(type)) {
        attributes.add(GISLib.createAttributeFrom((String) column.get("name"), Integer.class));
      }
      if ("STRING".equals(type)) {
        attributes.add(GISLib.createAttributeFrom((String) column.get("name"), String.class));
      }
    }
  }

  private GISZoneCollectionGeometry decodeFeatureCollection(Map featureCollection, List<GISAttributeInterface> attributes, String geomIndexProperty) {

    if (featureCollection.get(featureKey) instanceof List) {
      List feature = (List) featureCollection.get(featureKey);
      if (geomIndexProperty != null) {
        feature = sortFeatures(feature, geomIndexProperty);
      }
      GISZoneCollectionGeometry res = createGISCollection(feature);
      if (!CollectionUtils.isEmpty(attributes)) {
        res.setAttributes(attributes.toArray(new GISAttributeInterface[0]), null);
      }
      res.setAttributeIsZ(GISAttributeConstants.BATHY);
      return decodeListOfFeature(res, feature, attributes);
    }
    return createDefault();
  }

  private List sortFeatures(List featureCollection, String indexedProperty) {
    Map[] res = new Map[featureCollection.size()];
    for (Object map : featureCollection) {
      final Map featureCollectionAsMap = (Map) map;
      List allPropertiesMap = getPropertyAsList(featureCollectionAsMap);
      if (allPropertiesMap != null) {
        for (Object o : allPropertiesMap) {
          if (!(o instanceof Map)) {
            continue;
          }
          Map propertiesMap = (Map) o;
          String propName = (String) propertiesMap.get("name");
          if (indexedProperty.equalsIgnoreCase(propName)) {
            int pos = Integer.parseInt((String) propertiesMap.get(null));
            res[pos] = featureCollectionAsMap;
          }
        }
      }
    }
    return Arrays.asList(res);
  }

  private List getPropertyAsList(Map featureCollectionAsMap) {
    Object property = featureCollectionAsMap.get("property");
    //can't understand GML parser. If there is one property, a map is given. if > 1, a list of map.
    if (property instanceof Map) {
      property = Arrays.asList(property);
    }
    return (List) property;
  }

  private GISZoneCollectionGeometry createDefault() {
    return new GISZoneCollectionGeometry();
  }

  private GISZoneCollectionGeometry createGISCollection(List featureCollection) {
    Set<Class> geometryClasses = new HashSet<>();
    for (Object map : featureCollection) {
      if (map instanceof Map && ((Map) map).containsKey(geometryKey)) {
        final Geometry geometry = (Geometry) ((Map) map).get(geometryKey);
        geometryClasses.add(geometry.getClass());
      }
    }
    if (geometryClasses.size() <= 1) {
      Class geoClass = geometryClasses.iterator().next();
      if (geometryClasses.size() == 1 && Point.class.isAssignableFrom(geoClass)) {
        return new GISZoneCollectionPoint();
      }
    }
    if (geometryClasses.stream().allMatch(GMLMapDecoder::isLine)) {
      return new GISZoneCollectionLigneBrisee();
    }
    return createDefault();
  }

  private static boolean isLine(Class geoClass) {
    return LineString.class.isAssignableFrom(geoClass) || LinearRing.class.isAssignableFrom(geoClass);
  }

  private GISZoneCollectionGeometry decodeListOfFeature(GISZoneCollectionGeometry gisZoneCollectionGeometry, List featureCollection, List<GISAttributeInterface> attributes) {
    final Map<String, Integer> attributePositions = IntStream.range(0, attributes.size()).boxed()
        .collect(Collectors.toMap(i -> attributes.get(i).getName(), Function.identity()));

    for (Object map : featureCollection) {
      final Map featureCollectionAsMap = (Map) map;
      if (featureCollectionAsMap.containsKey(geometryKey)) {
        final Geometry geometry = (Geometry) featureCollectionAsMap.get(geometryKey);
        Object[] data = extractData(attributes, attributePositions, featureCollectionAsMap, geometry.getNumPoints());

        gisZoneCollectionGeometry.addGeometry(GISGeometryFactory.INSTANCE.transformToCustom(geometry), data, null);
      }
    }

    return gisZoneCollectionGeometry;
  }

  private Object[] extractData(List<GISAttributeInterface> attributes, Map<String, Integer> attributePositions, Map featureCollectionAsMap, int numPoint) {
    Object[] data = new Object[attributePositions.size()];
    List allPropertiesMap = getPropertyAsList(featureCollectionAsMap);
    if (allPropertiesMap != null) {
      for (Object o : allPropertiesMap) {
        if (!(o instanceof Map)) {
          continue;
        }
        Map propertiesMap = (Map) o;
        String propName = (String) propertiesMap.get("name");
        propName = GISLib.createAttributeFrom(propName, String.class).getName();
        if (attributePositions.containsKey(propName)) {
          int position = attributePositions.get(propName);
          final GISAttributeInterface gisAttributeInterface = attributes.get(position);
          data[position] = getValue(gisAttributeInterface, (String) propertiesMap.get(null), numPoint);
        }
      }
    }
    return data;
  }

  private Object getValue(GISAttributeInterface att, String valueAsString, int numPoint) {
    if (!att.isAtomicValue()) {
      return att.getEditor().parseString(valueAsString);
    }
    String[] svals = valueAsString.substring(1, valueAsString.length() - 1).split(GISDataModelFeatureAdapter.VALUE_SEPARATOR);
    for (int j = 0; j < svals.length; j++) {
      svals[j] = StringUtils.replace(svals[j], GISDataModelFeatureAdapter.VALUE_SEPARATOR_AS_STRING, GISDataModelFeatureAdapter.VALUE_SEPARATOR);
    }
    Object[] values = new Object[svals.length];
    for (int j = 0; j < values.length; j++) {
      values[j] = att.getEditor().parseString(svals[j]);
    }
    if (numPoint == 1 && values.length == 1) {
      return values[0];
    }
    return values;
  }
}
