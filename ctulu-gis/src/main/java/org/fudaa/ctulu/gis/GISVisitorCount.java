/*
 * @creation 4 d�c. 06
 * @modification $Date: 2007-02-02 11:20:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;

/**
 * @author fred deniger
 * @version $Id: GISVisitorCount.java,v 1.2 2007-02-02 11:20:05 deniger Exp $
 */
public class GISVisitorCount extends GISVisitorDefault {

  public int nbPt_;
  public int nbPtTotal_;
  public int nbPolyligne_;
  public int nbPolygone_;
  public int nbPolygoneHole_;

  @Override
  public boolean visitPoint(final Point _p) {
    nbPt_++;
    nbPtTotal_++;

    return true;
  }

  @Override
  public boolean visitPolygone(final LinearRing _p) {
    nbPolygone_++;
    nbPtTotal_ += _p.getNumPoints();

    return true;
  }

  @Override
  public boolean visitPolygoneWithHole(final Polygon _p) {
    nbPolygoneHole_++;
    nbPtTotal_ += _p.getNumPoints();
    return true;
  }

  @Override
  public boolean visitPolyligne(final LineString _p) {
    nbPolyligne_++;
    nbPtTotal_ += _p.getNumPoints();
    return true;
  }

}
