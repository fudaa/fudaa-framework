/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2006-02-09 08:59:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

/**
 * Cette interface concerne la communication entre les models d'attributs et la
 * GISZoneCollection. Pour la communication entre la GISZoneCollection et le
 * model voir GISZoneListener et pour voir la communication entre le model et le
 * reste du monde, voir ZModeleListener et ZModeleGeometryListener.
 * 
 * @author Fred Deniger
 * @version $Id: GISAttributeListener.java,v 1.3 2006-02-09 08:59:28 deniger Exp
 *          $
 */
public interface GISAttributeListener {

  /**
   * M�thode appel�e lorsqu'une action de modification de la valeur d'un
   * attribut est effectu�e.
   * 
   * @param _att
   *          L'attribut concern�. Si null tout les attributs ont pu �tre
   *          modifi�s.
   * @param _indexGeom
   *          La g�om�trie concern�e. Si �gale � -1, toutes les g�om�tries ont
   *          pu �tre modifi�es.
   * @param _newValue
   *          La nouvelle valeur de l'attribut pour la g�om�trie indiqu�e. Si
   *          null toutes les g�om�tries ont pu �tre modifi�es.
   */
  public void gisDataChanged(GISAttributeInterface _att, int _indexGeom, Object _newValue);
}
