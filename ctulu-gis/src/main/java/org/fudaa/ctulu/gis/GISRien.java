/*
 *  @creation     2 sept. 2005
 *  @modification $Date: 2007-01-17 10:45:25 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.*;

/**
 * @author fred deniger
 * @version $Id: GISRien.java,v 1.5 2007-01-17 10:45:25 deniger Exp $
 */
public final class GISRien extends Geometry implements GISGeometry {
  @Override
  public long getId() {
    return -1;
  }

  public final static GISRien INSTANCE = new GISRien();

  private GISRien() {
    super(GISGeometryFactory.INSTANCE);
  }

  @Override
  protected int getTypeCode() {
    return 0;
  }



  @Override
  public GISGeometry createTranslateGeometry(GISPoint xyToAdd) {
    return this;
  }

  @Override
  public boolean accept(final GISVisitor _v) {
    return true;
  }

  @Override
  public Geometry reverse() {
    return this;
  }

  @Override
  protected Geometry reverseInternal() {
    return INSTANCE;
  }

  @Override
  public void apply(final CoordinateFilter _filter) {
  }

  @Override
  public void apply(CoordinateSequenceFilter filter) {

  }

  @Override
  public void apply(final GeometryComponentFilter _filter) {
  }

  @Override
  protected Geometry copyInternal() {
    return this;
  }

  @Override
  public void apply(final GeometryFilter _filter) {
  }

  @Override
  protected int compareToSameClass(final Object _o) {
    return -61;
  }

  @Override
  protected int compareToSameClass(final Object _o, final CoordinateSequenceComparator _comp) {
    return -61;
  }

  protected int getSortIndex() {
    return 0;
  }

  @Override
  protected Envelope computeEnvelopeInternal() {
    return null;
  }

  @Override
  public boolean equalsExact(final Geometry _other, final double _tolerance) {
    return false;
  }

  @Override
  public Geometry getBoundary() {
    return null;
  }

  @Override
  public int getBoundaryDimension() {
    return 0;
  }

  @Override
  public Coordinate getCoordinate() {
    return null;
  }

  @Override
  public Coordinate[] getCoordinates() {
    return null;
  }

  @Override
  public int getDimension() {
    return 0;
  }

  @Override
  public String getGeometryType() {
    return "NULL";
  }

  @Override
  public int getNumPoints() {
    return 0;
  }

  @Override
  public boolean isEmpty() {
    return true;
  }

  @Override
  public boolean isSimple() {
    return false;
  }

  @Override
  public void normalize() {
  }
}
