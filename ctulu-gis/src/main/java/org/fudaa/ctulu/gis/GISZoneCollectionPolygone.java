/*
 *  @creation     21 mars 2005
 *  @modification $Date: 2007-02-02 11:20:05 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.LinearRing;
import org.fudaa.ctulu.CtuluCommandContainer;

/**
 * @author Fred Deniger
 * @version $Id: GISZoneCollectionPolygone.java,v 1.9 2007-02-02 11:20:05 deniger Exp $
 */
public class GISZoneCollectionPolygone extends GISZoneCollectionLigneBrisee {

  public GISZoneCollectionPolygone() {
    this(null);
  }

  /**
   * @param _listener le listener peut etre null
   */
  public GISZoneCollectionPolygone(final GISZoneListener _listener) {
    this(10, _listener);
  }

  @Override
  public boolean containsPolygone(){
    return getNumGeometries()>0;
  }


  @Override
  public boolean containsPolyligne(){
    return false;
  }

  @Override
  public Class getDataStoreClass(){
    return LinearRing.class;
  }

  /**
   * @param _nbObject
   */
  public GISZoneCollectionPolygone(final int _nbObject, final GISZoneListener _listener) {
    super(_nbObject, _listener);
  }

  @Override
  public void addPolyligne(final GISPolyligne _g,final CtuluCommandContainer _cmd){
    new Throwable().printStackTrace();
  }

  public void setValue(final int _idx,final GISPolyligne _newLine,final CtuluCommandContainer _cmd){
    new Throwable().printStackTrace();

  }
  
  @Override
  public void addAllPolylignes(final GISPolyligne[] _gs, final CtuluCommandContainer _cmd) {
    new Throwable().printStackTrace();
  }

  @Override
  public void addPolyligne(final GISPolyligne _g,final Object[] _data,final CtuluCommandContainer _cmd){
    new Throwable().printStackTrace();
  }


}
