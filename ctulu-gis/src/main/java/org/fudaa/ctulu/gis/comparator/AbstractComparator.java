/*
 GPL 2
 */
package org.fudaa.ctulu.gis.comparator;

/**
 *
 * @author Frederic Deniger
 */
public class AbstractComparator {

  private double eps = 1e-3;

  public AbstractComparator() {
  }

  public AbstractComparator(double eps) {
    this.eps = eps;
  }

  public int compare(double x1, double y1, double x2, double y2) {
    if ((Math.abs(x1 - y1) <= eps) && (Math.abs(x2 - y2) <= eps)) {
      return 0;
    }
    if (x1 > x2) {
      return 1;
    }
    if (x1 < x2) {
      return -1;
    }
    if (y1 > y2) {
      return 1;
    }
    if (y1 < y2) {
      return -1;
    }

    return 0;
  }
}
