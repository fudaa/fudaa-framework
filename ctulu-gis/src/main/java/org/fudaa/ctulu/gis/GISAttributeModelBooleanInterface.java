/*
 *  @creation     8 avr. 2005
 *  @modification $Date: 2007-01-10 08:58:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.fudaa.ctulu.collection.CtuluCollectionBooleanInterface;

/**
 * @author Fred Deniger
 * @version $Id: GISAttributeModelBooleanInterface.java,v 1.5 2007-01-10 08:58:47 deniger Exp $
 */
public interface GISAttributeModelBooleanInterface extends GISAttributeModel,CtuluCollectionBooleanInterface{

  /**
   * @param _numObject le nombre d'objet du nouveau model
   * @param _interpol l'interpolateur
   * @return le model cree
   */
  GISAttributeModelBooleanInterface deriveNewModel(int _numObject,GISReprojectInterpolateurI.BooleanTarget _interpol);

}
