/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2007-05-04 13:43:25 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import gnu.trove.TIntIntHashMap;
import gnu.trove.TIntIntIterator;
import java.util.Arrays;
import org.fudaa.ctulu.collection.CtuluArrayObject;

/**
 * @author Fred Deniger
 * @version $Id: GISAttributeModelObjectArray.java,v 1.11 2007-05-04 13:43:25 deniger Exp $
 */
public class GISAttributeModelObjectArray extends CtuluArrayObject implements
    GISAttributeModelObjectInterface, GISAttributeModelObservable {

  protected transient GISAttributeListener listener_;

  GISAttributeInterface attribute_;

  @Override
  public Object getValue(final int _idx){
    return super.getValueAt(_idx);
  }

  /**
   * Constructeur pour la désérialisation.
   */
  public GISAttributeModelObjectArray() {
    super(0);
  }

  @Override
  public Object getAverage(){
    if (list_.length == 0) {
      return null;
    }
    return list_[0];
  }

  /**
   * @param _nb le nombre de valeurs.
   * @param _attr l'attribut concerne
   */
  public GISAttributeModelObjectArray(final int _nb, final GISAttributeInterface _attr) {
    super(_nb);
    if (_attr== null) {
      throw new IllegalArgumentException("attribute is null");
    }
    Arrays.fill(list_, _attr.getDefaultValue());
    attribute_ = _attr;

  }


  @Override
  public GISAttributeModel createUpperModel(final int _numObject,final TIntIntHashMap _newIdxOldIdx){
    if (_numObject < getSize()) {
      throw new IllegalArgumentException("bad num objects");
    }
    if (_numObject == getSize()) {
      return this;
    }
    final Object[] newList = new Object[_numObject];
    Arrays.fill(newList, getAttribute().getDefaultValue());
    if (_newIdxOldIdx != null) {
      final  TIntIntIterator it = _newIdxOldIdx.iterator();
      for (int i = _newIdxOldIdx.size(); i-- > 0;) {
        it.advance();
        final  int newIdx = it.key();
        newList[newIdx] = list_[it.value()];
      }
    }
    return new GISAttributeModelObjectArray(newList, this);
  }

  @Override
  public GISAttributeModel deriveNewModel(final int _numObject,final GISReprojectInterpolateurI _interpol){
    if (!(_interpol instanceof GISReprojectInterpolateurI.ObjectTarget)) {
      throw new IllegalArgumentException("bad interface");
    }
    return deriveNewModel(_numObject, (GISReprojectInterpolateurI.ObjectTarget) _interpol);
  }

  @Override
  public GISAttributeModelObjectInterface deriveNewModel(final int _numObject,
    final GISReprojectInterpolateurI.ObjectTarget _interpol){
    final Object[] newList = new Object[_numObject];
    for (int i = newList.length - 1; i >= 0; i--) {
      newList[i] = _interpol.interpol(i);
    }
    return new GISAttributeModelObjectArray(newList, this);
  }

  /**
   * @param _nb le nombre de valeurs.
   * @param _attr l'attribut concerne
   */
  public GISAttributeModelObjectArray(final Object[] _nb, final GISAttributeInterface _attr) {
    super(_nb);
    attribute_ = _attr;
    if (attribute_ == null) {
      throw new IllegalArgumentException("attribute is null");
    }
  }

  private GISAttributeModelObjectArray(final Object[] _l, final GISAttributeModelObjectArray _model) {
    super(0);
    list_ = _l;
    attribute_ = _model.attribute_;
    listener_ = _model.listener_;

  }

  @Override
  public GISAttributeModel createSubModel(final int[] _idxToRemove){
    final Object[] newList = new Object[getSize()];
    int count = 0;
    final int nb = newList.length;
    for (int i = 0; i < nb; i++) {
      if (Arrays.binarySearch(_idxToRemove, i) < 0) {
        newList[count++] = super.list_[i];
      }
    }
    final Object[] finalList = new Object[count];
    System.arraycopy(newList, 0, finalList, 0, finalList.length);
    return new GISAttributeModelObjectArray(finalList, this);
  }

  @Override
  protected void fireObjectChanged(int _indexGeom, Object _newValue){
    if (listener_ != null) {
      listener_.gisDataChanged(attribute_, _indexGeom, _newValue);
    }
  }

  protected final GISAttributeListener getListener(){
    return listener_;
  }


  /**
   * @return l'attribut associe
   */
  @Override
  public final GISAttributeInterface getAttribute(){
    return attribute_;
  }


  @Override
  public final void setListener(final GISAttributeListener _listener){
    listener_ = _listener;
  }

}
