/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2008-02-06 18:04:21 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import java.util.Comparator;
import org.fudaa.ctulu.CtuluVariable;
import org.fudaa.ctulu.editor.CtuluValueEditorI;

/**
 * Une interface pour un attribut qui peut �tre associ� aux objets GIS. Un attribut peut
 * �tre global � l'objet auquel il est associ�, ou atomique.<p>
 * 
 * Lorsqu'il est atomique, il est associ� � chaque sous partie d'un objet. Exemple : les
 * points d'une ligne.
 * 
 * @author Fred Deniger
 * @version $Id: GISAttributeInterface.java,v 1.9.8.2 2008-02-06 18:04:21 bmarchan Exp $
 */
public interface GISAttributeInterface extends CtuluVariable {

  /**
   * D�termine si l'attribut est �ditable dans les �diteurs.<p>
   * FIXME BM: Actuellement, un attribut non �ditable est invisible dans les �diteurs. 
   * @return Si true : Editable
   */
  boolean isEditable();

  /**
   * D�termine si l'attribut est visible de l'utilisateur. Si c'est le cas, il est �galement non �ditable.<p>
   * FIXME BM: Lorsque l'attribut est �ditable et invisible, on peut tout de m�me l'�diter.
   * @return Si true : Visible
   */
  boolean isUserVisible();
  
  boolean isSameContent(final GISAttributeInterface _att);


  /**
   * @param _nbValues
   * @return un modele a utilise pour des donn�es sur les vertex
   */
  GISAttributeModelObservable createAtomicModel(Object[] _initValues, int _nbValues);

  /**
   * Permet de creer facilement les donn�es correspondant a l'att.
   * 
   * @param _initValues la ou les valeurs initiales, peut etre null. Si c'est un tableau et l'attribute est global, la
   *          moyenne sera prise. Inversement, l'objet sera duplique si un tableau doit etre construit.
   * @param _nbPt le nombre de points pour l'objet geometriques correspondant
   * @return l'objet correspondant. Si atomique, un {@link GISAttributeModelObservable} est renvoy�. Sinon,
   *         retourne un objet de classe {@link #getDataClass()}
   */
  Object createDataForGeom(Object _initValues, int _nbPt);

  /**
   * @param _defaultCap la capacit� par defaut: optimisation.
   * @return le modele a utiliser pour une liste variable en taille.
   */
  GISAttributeModelObservable createListModel(int _defaultCap);

  /**
   * @return la valeur par defaut
   */
  Object getDefaultValue();

  boolean setName(String _s);

  /**
   * Retourne les donn�es utilisateur, autres que les valeurs d'attributs.
   * @return donnee utilisateur
   */
  Object getUserData();

  /**
   * Affecte des donn�es utilisateur, autres que les valeurs d'attributs.
   * @param _userData donnee utilisateur
   */
  void setUserData(Object _userData);

  /**
   * Par exemple renvoie double.
   * 
   * @return la classe primitive
   */
  Class getDataClass();

  /**
   * @return l'�diteur associe
   */
  CtuluValueEditorI getEditor();
  
  /**
   * @return Le comparateur de valeur, pour le tri sur un modele par exemple.
   */
  Comparator<?> getComparator();

  /**
   * @return le nombre de chiffres apr�s la virgule
   */
  int getPrecision();

  /**
   * Retourne l'atomicit� de l'attribut (a utiliser pour les polygones,polylignes).
   * 
   * @return si true, cet attribut signifie que les valeurs sont associees a chaque point
   */
  boolean isAtomicValue();

}
