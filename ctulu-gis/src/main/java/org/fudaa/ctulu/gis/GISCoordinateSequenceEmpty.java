/*
 *  @creation     18 mars 2005
 *  @modification $Date: 2006-07-13 13:34:35 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Envelope;

/**
 * @author Fred Deniger
 * @version $Id: GISCoordinateSequenceEmpty.java,v 1.5 2006-07-13 13:34:35 deniger Exp $
 */
public class GISCoordinateSequenceEmpty implements CoordinateSequence {
  public GISCoordinateSequenceEmpty() {
  }

  @Override
  public Object clone() {
    return this;
  }

  @Override
  public CoordinateSequence copy() {
    return this;
  }

  @Override
  public Envelope expandEnvelope(final Envelope envelope) {
    return envelope;
  }

  @Override
  public void getCoordinate(final int index, final Coordinate coordinate) {
    throw new IllegalArgumentException();
  }

  @Override
  public Coordinate getCoordinate(final int i) {
    throw new IllegalArgumentException();
  }

  @Override
  public Coordinate getCoordinateCopy(final int i) {
    throw new IllegalArgumentException();
  }

  @Override
  public double getOrdinate(final int index, final int ordinateIndex) {
    throw new IllegalArgumentException();
  }

  @Override
  public double getX(final int _index) {
    throw new IllegalArgumentException();
  }

  @Override
  public double getY(final int _index) {
    throw new IllegalArgumentException();
  }

  public double getZ(final int _index) {
    throw new IllegalArgumentException();
  }

  @Override
  public void setOrdinate(final int _index, final int _ordinateIndex, final double _value) {
    throw new IllegalArgumentException();
  }

  @Override
  public int size() {
    return 0;
  }

  @Override
  public int getDimension() {
    return 3;
  }

  @Override
  public Coordinate[] toCoordinateArray() {
    return new Coordinate[0];
  }
}
