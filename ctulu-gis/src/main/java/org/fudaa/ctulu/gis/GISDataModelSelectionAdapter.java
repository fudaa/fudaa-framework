/*
 * @creation     14 oct. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import java.util.ArrayList;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * Cette adapter permet de ne faire apparaitre dans le model que les g�om�tries
 * r�pondant � des conditions donn�es sur les attributs. Par exemple on peut
 * dire que seul les g�om�tries d'une certaine nature sont visbles.
 * Plusieurs conditions peuvent �tre donn�es. Elles seront g�r�es avec un OU
 * logique. Pour simuler un ET logique entre des conditions, il suffit d'empiler
 * plusieurs fois cet adapter sur le model.
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class GISDataModelSelectionAdapter implements GISDataModel {

  public static final String ATTRIBUT_EXIST = "< GISDataModelSelectionAdapter:ATTRIBUT_EXIST >";
  public static final String ATTRIBUT_NOT_EXIST = "< GISDataModelSelectionAdapter:ATTRIBUT_NOT_EXIST >";
  
  private final GISDataModel model_;
  private final Object[][] conditions_;
  /** Table de translation d'indices des g�om�tries. */
  private final ArrayList<Integer> translationTable_;

  /**
   * Cette adapter permet de ne faire apparaitre dans le model que les
   * g�om�tries r�pondant � des conditions donn�es sur les attributs.Par
   * exemple on peut dire que seul les g�om�tries d'une certaine nature sont
   * visbles. Plusieurs conditions peuvent �tre donn�es. Elles seront g�r�es
   * avec un OU logique. Pour simuler un ET logique entre des conditions, il
   * suffi d'empiler plusieurs fois cette adapter sur le model.
   * 
   * @param _model
   *          le model sur lequel va se faire la selection
   * @param _conditions
   *          un tableau de conditions. Une condition �tant d�finie par deux
   *          �l�ments : l'attribut concern� (GISAttributeInterface) et sa
   *          valeur (Object). Deux valeurs sp�cials sont d�finies dans cette
   *          classe : ATTRIBUT_EXIST pour indiquer que la condition est vrai a
   *          partir du moment o� l'attribut est d�finie ; ATTRIBUT_NOT_EXIST
   *          indique que la condition est vrai a partir du moment o� l'attribut
   *          n'existe pas.
   */
  public GISDataModelSelectionAdapter(GISDataModel _model, Object[][] _conditions){
    if(_model==null||_conditions==null)
      throw new IllegalArgumentException("_model et _conditions ne doivent pas �tre null.");
    translationTable_=new ArrayList<Integer>();
    model_=_model;
    // Verification de la validit� des conditions
    for(int i=0;i<_conditions.length;i++){
      if(_conditions[i].length!=2)
        throw new IllegalArgumentException("La taille de chaque condition doit �tre �gale � deux.");
      if(!(_conditions[i][0] instanceof GISAttributeInterface))
        throw new IllegalArgumentException("Le premier �l�ment de la condition doit �tre un GISAttributeInterface");
    }
    conditions_ =_conditions;
    // R�cup�ration des indices des attribtus concernant les conditions
    int[] indexAttributsConditions = new int[conditions_.length];
    for(int i=0;i<indexAttributsConditions.length;i++)
      indexAttributsConditions[i]=model_.getIndiceOf((GISAttributeInterface)conditions_[i][0]);
    // G�n�ration d'une table de translation d'index des g�om�tries
    for(int i=0;i<model_.getNumGeometries();i++){
      // Verification des conditions
      boolean valide=false;
      int j=-1;
      while(!valide&&++j<conditions_.length){
        if(indexAttributsConditions[j]==-1&&conditions_[j][1]==ATTRIBUT_NOT_EXIST)
          valide=true;
        else if(indexAttributsConditions[j]!=-1&&conditions_[j][1]==ATTRIBUT_EXIST)
          valide=true;
        else if(indexAttributsConditions[j]!=-1&&conditions_[j][1]==model_.getValue(indexAttributsConditions[j], i))
          valide=true;
      }
      if(valide)
        translationTable_.add(i);
    }
  }
  
  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if(xyToAdd==null){
      return this;
    }
    return new GISDataModelSelectionAdapter(model_.createTranslate(xyToAdd),this.conditions_);
  }
  
  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getAttribute(int)
   */
  @Override
  public GISAttributeInterface getAttribute(int _att) {
    return model_.getAttribute(_att);
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getDoubleValue(int, int)
   */
  @Override
  public double getDoubleValue(int _att, int _geom) {
    if(_geom>0&&_geom<translationTable_.size())
      return model_.getDoubleValue(_att, translationTable_.get(_geom));
    return 0;
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getEnvelopeInternal()
   */
  @Override
  public Envelope getEnvelopeInternal() {
    return model_.getEnvelopeInternal();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getGeometry(int)
   */
  @Override
  public Geometry getGeometry(int _geom) {
    if(_geom<0||_geom>=translationTable_.size())
      return null;
    return model_.getGeometry(translationTable_.get(_geom));
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getIndiceOf(org.fudaa.ctulu.gis.GISAttributeInterface)
   */
  @Override
  public int getIndiceOf(GISAttributeInterface _att) {
    return model_.getIndiceOf(_att);
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getNbAttributes()
   */
  @Override
  public int getNbAttributes() {
    return model_.getNbAttributes();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getNumGeometries()
   */
  @Override
  public int getNumGeometries() {
    return translationTable_.size();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getValue(int, int)
   */
  @Override
  public Object getValue(int _att, int _geom) {
    if(_geom>=0&&_geom<translationTable_.size())
      return model_.getValue(_att, translationTable_.get(_geom));
    return null;
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#preload(org.fudaa.ctulu.gis.GISAttributeInterface[], org.fudaa.ctulu.ProgressionInterface)
   */
  @Override
  public void preload(GISAttributeInterface[] _att, ProgressionInterface _prog) {
    model_.preload(_att, _prog);
  }

}
