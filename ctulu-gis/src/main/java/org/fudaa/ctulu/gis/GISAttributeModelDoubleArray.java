/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2007-04-16 16:33:52 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */

package org.fudaa.ctulu.gis;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntIntHashMap;
import gnu.trove.TIntIntIterator;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.collection.CtuluArrayDouble;
import org.fudaa.ctulu.collection.CtuluListDouble;

/**
 * @author Fred Deniger
 * @version $Id: GISAttributeModelDoubleArray.java,v 1.12 2007-04-16 16:33:52 deniger Exp $
 */
public class GISAttributeModelDoubleArray extends CtuluArrayDouble implements
    GISAttributeModelDoubleInterface, GISAttributeModelObservable {

  GISAttributeInterface attribute_;

  protected transient GISAttributeListener listener_;

  /**
   *
   */
  protected GISAttributeModelDoubleArray() {
    super(0);
  }

  private GISAttributeModelDoubleArray(final double[] _l, final GISAttributeModelDoubleArray _model) {
    super(0);
    list_ = _l;
    attribute_ = _model.attribute_;
    listener_ = _model.listener_;

  }

  @Override
  public GISAttributeModel createSubModel(final int[] _idxToRemove){
    final double[] newList = new double[getSize()];
    int count = 0;
    final  int nb = newList.length;
    for (int i = 0; i < nb; i++) {
      if (Arrays.binarySearch(_idxToRemove, i) < 0) {
        newList[count++] = super.list_[i];
      }
    }
    final  double[] finalList = new double[count];
    System.arraycopy(newList, 0, finalList, 0, finalList.length);
    return new GISAttributeModelDoubleArray(finalList, this);
  }

  protected GISAttributeModelDoubleArray(final int _i, final GISAttributeInterface _attr) {
    super(_i);
    Arrays.fill(list_, ((Double) _attr.getDefaultValue()).doubleValue());
    attribute_ = _attr;
  }

  @Override
  public GISAttributeModel deriveNewModel(final int _numObject,final GISReprojectInterpolateurI _interpol){
    if (!(_interpol instanceof GISReprojectInterpolateurI.DoubleTarget)) {
      throw new IllegalArgumentException("bad interface");
    }
    return deriveNewModel(_numObject, (GISReprojectInterpolateurI.DoubleTarget) _interpol);
  }

  @Override
  public GISAttributeModelDoubleInterface deriveNewModel(final int _numObject,
    final GISReprojectInterpolateurI.DoubleTarget _interpol){
    final double[] newList = new double[_numObject];
    for (int i = newList.length - 1; i >= 0; i--) {
      newList[i] = _interpol.interpol(i);
    }
    return new GISAttributeModelDoubleArray(newList, this);
  }

  @Override
  public Object getAverage(){
    return CtuluLib.getDouble(getDoubleAverage());
  }

  public double getDoubleAverage(){
    if (list_.length == 0) {
      return 0;
    }
    return CtuluLibArray.getMoyenne(list_);
  }

  /**
   * @param _init les valeurs initiales
   * @param _attr l'attribut
   */
  public GISAttributeModelDoubleArray(final CtuluListDouble _init,
      final GISAttributeInterface _attr) {
    super(_init);
    attribute_ = _attr;
  }

  @Override
  public GISAttributeModel createUpperModel(final int _numObject,final TIntIntHashMap _newIdxOldIdx){
    if (_numObject < getSize()) {
      throw new IllegalArgumentException("bad num objects");
    }
    if (_numObject == getSize()) {
      return this;
    }
    final double[] newList = new double[_numObject];
    Arrays.fill(newList, ((Double) getAttribute().getDefaultValue()).doubleValue());
    if (_newIdxOldIdx != null) {
      final TIntIntIterator it = _newIdxOldIdx.iterator();
      for (int i = _newIdxOldIdx.size(); i-- > 0;) {
        it.advance();
        final  int newIdx = it.key();
        newList[newIdx] = list_[it.value()];
      }
    }
    return new GISAttributeModelDoubleArray(newList, this);
  }

  /**
   * @param _init les valeurs initiales
   * @param _attr l'attribut
   */
  public GISAttributeModelDoubleArray(final double[] _init, final GISAttributeInterface _attr) {
    super(_init);
    attribute_ = _attr;
  }

  /**
   * @param _init les valeurs initiales
   * @param _attr l'attribut
   */
  public GISAttributeModelDoubleArray(final TDoubleArrayList _init,
      final GISAttributeInterface _attr) {
    super(_init);
    attribute_ = _attr;
  }

  /**
   * @param _init les valeurs initiales
   * @param _attr l'attribut
   */
  public GISAttributeModelDoubleArray(final Object[] _init, final GISAttributeInterface _attr) {
    super(_init);
    attribute_ = _attr;
  }

  @Override
  protected void fireObjectChanged(int _indexGeom, Object _newValue){
    if (listener_ != null) {
      listener_.gisDataChanged(attribute_, _indexGeom, _newValue);
    }
  }

  protected final GISAttributeListener getListener(){
    return listener_;
  }

  protected final void setAttribute(final GISAttributeInterface _attribute){
    attribute_ = _attribute;
  }

  @Override
  public final void setListener(final GISAttributeListener _listener){
    listener_ = _listener;
  }

  /**
   * @return l'attribut associe
   */
  @Override
  public final GISAttributeInterface getAttribute(){
    return attribute_;
  }

}
