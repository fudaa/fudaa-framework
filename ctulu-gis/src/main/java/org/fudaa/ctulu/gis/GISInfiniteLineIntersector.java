/*
 *  @creation     12 avr. 2005
 *  @modification $Date: 2006-07-13 13:34:36 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.algorithm.RobustLineIntersector;
import org.locationtech.jts.geom.Coordinate;

/**
 * Il semble qu'il y ait un  bug dans jump-jts.
 * @author Fred Deniger
 * @version $Id: GISInfiniteLineIntersector.java,v 1.5 2006-07-13 13:34:36 deniger Exp $
 */
public class GISInfiniteLineIntersector extends RobustLineIntersector {

  public GISInfiniteLineIntersector() {
    super();
  }

  /**
   * @return le point d'intersection a.
   */
  public Coordinate getIntersectedPoint(){
    return pa;
  }

  /**
   * @param _seg1 le point 1 du segment
   * @param _seg2 le point 2 du segment
   * @param _l1 le point 1 de la ligne
   * @param _l2 le point 2 de la ligne
   */
  public void computeIntersectionLineSegment(final Coordinate _l1,final Coordinate _l2,
    final Coordinate _seg1,final Coordinate _seg2){
    inputLines[0][0] = _l1;
    inputLines[0][1] = _l2;
    inputLines[1][0] = _seg1;
    inputLines[1][1] = _seg2;
    result = computeIntersectLineSegment(_l1, _l2, _seg1, _seg2);
  }
  /**
   * @param _l21 le point 1 de la ligne 2
   * @param _l22 le point 2 de la ligne 2
   * @param _l11 le point 1 de la ligne 1
   * @param _l12 le point 2 de la ligne 1
   */
  public void computeIntersectionLineLine(final Coordinate _l11,final Coordinate _l12,
    final Coordinate _l21,final Coordinate _l22){
    inputLines[0][0] = _l11;
    inputLines[0][1] = _l12;
    inputLines[1][0] = _l21;
    inputLines[1][1] = _l22;
    result = computeIntersectLineLine(_l11, _l12, _l21, _l22);
  }

  /**
   * @param _l1 le point 1 de la ligne
   * @param _l2 le point 2 de la ligne
   * @param _l3 le point 1 de la ligne 2
   * @param _l4 le point 2 de la ligne 2
   * @return INTERSECT, COLINEAR ou DONT_INTERSECT
   */
  private int computeIntersectLineLine(final Coordinate _l1,final Coordinate _l2,final Coordinate _l3,
    final Coordinate _l4){
    double a1;
    double b1;
    double c1;
    /*
     *  Coefficients of line eqns.
     */
    double a2;
    /*
     *  Coefficients of line eqns.
     */
    double b2;
    /*
     *  Coefficients of line eqns.
     */
    double c2;
    /*
     *  'Sign' values
     */
    //double denom, offset, num;     /* Intermediate values */
    isProper = false;

    /*
     *  Compute a1, b1, c1, where line joining points 1 and 2
     *  is "a1 x  +  b1 y  +  c1  =  0".
     */
    a1 = _l2.y - _l1.y;
    b1 = _l1.x - _l2.x;
    c1 = _l2.x * _l1.y - _l1.x * _l2.y;

    /*
     *  Compute a2, b2, c2
     */
    a2 = _l4.y - _l3.y;
    b2 = _l3.x - _l4.x;
    c2 = _l4.x * _l3.y - _l3.x * _l4.y;
    final  double denom = a1 * b2 - a2 * b1;
    if (denom == 0) {
      return COLLINEAR;
    }
    final  double numX = b1 * c2 - b2 * c1;
    pa.x = numX / denom;

    final  double numY = a2 * c1 - a1 * c2;
    pa.y = numY / denom;

    // check if this is a proper intersection BEFORE truncating values,
    // to avoid spurious equality comparisons with endpoints
    isProper = true;
    if (pa.equals(_l1) || pa.equals(_l2) || pa.equals(_l3) || pa.equals(_l4)) {
      isProper = false;
    }

    // truncate computed point to precision grid
    // TESTING - don't force coord to be precise
    if (precisionModel != null) {
      precisionModel.makePrecise(pa);
    }
    return DO_INTERSECT;
  }

  public static boolean isSameSignAndNonZero(double a, double b) {
    if (a == 0 || b == 0) {
      return false;
    }
    return (a < 0 && b < 0) || (a > 0 && b > 0);
  }

  /**
   * @param _seg1 le point 1 du segment
   * @param _seg2 le point 2 du segment
   * @param _l1 le point 1 de la ligne
   * @param _l2 le point 2 de la ligne
   * @return INTERSECT, COLINEAR ou DONT_INTERSECT
   */
  private int computeIntersectLineSegment(final Coordinate _l1,final Coordinate _l2,final Coordinate _seg1,
    final Coordinate _seg2){
    double a1;
    double b1;
    double c1;
    double a2;
    double b2;
    double c2;
    double r3;
    double r4;
    isProper = false;

    a1 = _l2.y - _l1.y;
    b1 = _l1.x - _l2.x;
    c1 = _l2.x * _l1.y - _l1.x * _l2.y;

    /*
     *  Compute r3 and r4.
     */
    r3 = a1 * _seg1.x + b1 * _seg1.y + c1;
    r4 = a1 * _seg2.x + b1 * _seg2.y + c1;

    /*
     *  Check signs of r3 and r4.  If both point 3 and point 4 lie on
     *  same side of line 1, the line segments do not intersect.
     */

    if (r3 != 0 && r4 != 0 && isSameSignAndNonZero(r3, r4)) {
      return DONT_INTERSECT;
    }

    /*
     *  Compute a2, b2, c2
     */
    a2 = _seg2.y - _seg1.y;
    b2 = _seg1.x - _seg2.x;
    c2 = _seg2.x * _seg1.y - _seg1.x * _seg2.y;

    /**
     *  Line segments intersect: compute intersection point.
     */
    final double denom = a1 * b2 - a2 * b1;
    if (denom == 0) {
      return COLLINEAR;
    }
    final double numX = b1 * c2 - b2 * c1;
    pa.x = numX / denom;

    final  double numY = a2 * c1 - a1 * c2;
    pa.y = numY / denom;

    //on teste que l'intersection se fasse bien sur le segment
    double minx = _seg1.x;
    double maxx = _seg2.x;
    if (minx > maxx) {
      minx = maxx;
      maxx = _seg1.x;
    }
    if (pa.x < minx || pa.x > maxx) {
      return DONT_INTERSECT;
    }

    // check if this is a proper intersection BEFORE truncating values,
    // to avoid spurious equality comparisons with endpoints
    isProper = true;
    if (pa.equals(_l1) || pa.equals(_l2) || pa.equals(_seg1) || pa.equals(_seg2)) {
      isProper = false;
    }

    // truncate computed point to precision grid
    // TESTING - don't force coord to be precise
    if (precisionModel != null) {
      precisionModel.makePrecise(pa);
    }
    return DO_INTERSECT;
  }

}
