/*
 * @creation 7 avr. 2005
 * 
 * @modification $Date: 2008-03-26 16:46:43 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.fudaa.ctulu.CtuluCommand;
import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.collection.CtuluCollection;
import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;
import org.fudaa.ctulu.collection.CtuluListObject;

/**
 * Une classe abstraite d'objets geometriques auquels sont associes des attributs dont les valeurs sont gerees par des modeles. Ces modeles
 * d'attributs sont ordonnes dans la liste des modeles d'attributs.
 *
 * @author Fred Deniger
 * @version $Id$
 */
public abstract class GISZoneCollection extends GISCollection implements GISAttributeListener, GISDataModel {

  private String title_;
  /**
   * La liste ordonn�e des mod�les d'attributs associ�es � la collection.
   */
  protected GISAttributeModelObservable[] attr_;
  /**
   * L'acc�s direct � l'indice de mod�le depuis un type d'attribut
   */
  protected Map<GISAttributeInterface, Integer> att2Idx_ = new HashMap<GISAttributeInterface, Integer>();
  /**
   * La valeur de la nature pour cette collection si elle est impos�e. null : non impos�e.
   */
  protected Map<GISAttributeInterface, Object> fixedAttributes_ = new HashMap<GISAttributeInterface, Object>();
  /**
   * L'attribut utilis� pour Z
   */
  protected GISAttributeDouble attributeIsZ_ = null;
  /**
   * L'indice dans la liste des mod�les de l'attribut pris pour Z (pour optimisation des temps). Est remis a jour � chaque modification sur la liste.
   * Si �gal -1, pas d'attribut pris pour Z
   */
  private int idxAttZ_ = -1;
  /**
   * L'indice de l'attribut pris pour Z doit �tre remis a jour
   */
  protected boolean badIdxAttZ_ = true;
  /**
   * La liste d'attribut a chang� : L'acc�s direct � l'indice de mod�le depuis un type doit �tre remis � jour.
   */
  protected boolean attListHasChanged_ = true;
  protected boolean isGeomModifiable_ = true;

  public GISZoneCollection() {
    super();
  }

  public GISZoneCollection(final boolean _isGeomModifiable) {
    isGeomModifiable_ = _isGeomModifiable;
  }

  public void setGeomModifiable(boolean isGeomModifiable) {
    this.isGeomModifiable_ = isGeomModifiable;
  }

  /**
   * @param _factory
   * @param _nbObject
   */
  public GISZoneCollection(final GeometryFactory _factory, final int _nbObject) {
    super(_factory, _nbObject);
  }

  /**
   * @param _nbObject
   */
  public GISZoneCollection(final int _nbObject) {
    super(_nbObject);
  }

  @Override
  public long getId() {
    return -1;
  }

  protected void fireAttributeAdded(int _newIdx, GISAttributeInterface _newAtt) {
    if (listener_ != null) {
      listener_.attributeAction(this, _newIdx, _newAtt, GISZoneListener.ATTRIBUTE_ACTION_ADD);
    }
  }

  protected void fireAttributeRemoved(int _oldIdx, GISAttributeInterface _oldAtt) {
    if (listener_ != null) {
      listener_.attributeAction(this, _oldIdx, _oldAtt, GISZoneListener.ATTRIBUTE_ACTION_REMOVE);
    }
  }

  protected void fireAttributeValueChanged(int _idxAtt, GISAttributeInterface _att, int _idxGeom, Object _newValue) {
    if (listener_ != null) {
      listener_.attributeValueChangeAction(this, _idxAtt, _att, _idxGeom, _newValue);
    }
  }

  @Override
  public void gisDataChanged(GISAttributeInterface _att, int _indexGeom, Object _newValue) {
    fireAttributeValueChanged(getIndiceOf(_att), _att, _indexGeom, _newValue);
  }

  protected void fireTitleChanged() {
  }

  @Override
  protected Geometry reverseInternal() {
    throw new UnsupportedOperationException("not supported");
  }

  /**
   * Retourne le mod�le d'attribut associ� � l'attribut en indice.
   *
   * @param _i L'indice suivant l'ordre de la liste des mod�le d'attributs.
   */
  protected CtuluCollection getData(final int _i) {
    return isDataCreated(_i) ? attr_[_i] : null;
  }

  protected Object getDefaultValuesObject(final int _idxValues) {
    return isDataCreated(_idxValues) ? attr_[_idxValues].getAttribute().getDefaultValue() : null;
  }

  @Override
  public Object clone() {
    GISZoneCollection cloned = (GISZoneCollection) super.superClone();
    cloned.setListener(null);
    cloned.setAttributes(null, null);

    final int n = getNumGeometries();
    cloned.geometry_ = cloned.createGisObjectContainer(getNumGeometries());
    for (int i = 0; i < n; i++) {
      // on caste pour etre sur que l'on ait une geometrie
      cloned.geometry_.addObject(getGeometryN(i).clone(), null);
    }
    //on ajoute les g�om�tries sans attributs.
    cloned.att2Idx_.clear();
    cloned.setAttributes(getAttributes(), null);
    for (int i = 0; i < attr_.length; i++) {
      cloned.att2Idx_.put(cloned.attr_[i].getAttribute(), Integer.valueOf(i));
    }
    if (cloned.attr_ != null) {
      for (int i = 0; i < cloned.attr_.length; i++) {
        final GISAttributeModelObservable modelToCopy = attr_[i];
        final GISAttributeInterface attribute = modelToCopy.getAttribute();
        if (attribute.isAtomicValue()) {
          //TODO le nombre d'objets attendu pourraient initialis�s
          int size = modelToCopy.getSize();
          CtuluListObject newObject = new CtuluListObject(Math.max(20,size));
          for (int j = 0; j < size; j++) {
            if (modelToCopy.getObjectValueAt(j) instanceof CtuluCollection) {
              CtuluCollection collection = (CtuluCollection) modelToCopy.getObjectValueAt(j);
              final GISAttributeModelObservable newAtomicModel = attribute.createAtomicModel(collection.getObjectValues(),
                      collection.getSize());
              newObject.addObject(newAtomicModel, null);
            }
            else{
              newObject.addObject(modelToCopy.getObjectValueAt(j), null);
            }
          }
          cloned.attr_[i].initWith(newObject, false);
        } else {
          cloned.attr_[i].initWith(attr_[i], false);
        }
      }
    }
    cloned.updateListeners();
    return cloned;
  }

  /**
   * @param _i l'indice de l'attribut
   * @return le model si existant (null sinon).
   */
  protected GISAttributeModelObservable getModelListener(final int _i) {
    return (GISAttributeModelObservable) getData(_i);
  }

  protected void initAttributes(final GISAttributeInterface[] _att) {
    if (_att != null) {
      badIdxAttZ_ = true;
      attListHasChanged_ = true;
      attr_ = new GISAttributeModelObservable[_att.length];
      for (int i = _att.length - 1; i >= 0; i--) {
        attr_[i] = (GISAttributeModelObservable) GISZoneAttributeFactory.createModel(this, _att[i]);
        // FIXME BM : Je laisse, mais c'est douteux. Dans une collection, l'attribut Z n'est pas forcement
        // la bathy.
        if (_att[i] == GISAttributeConstants.BATHY) {
          setAttributeIsZ(GISAttributeConstants.BATHY);
        }
      }
    }
  }

  protected void initAttributes(final GISAttributeModelObservable[] _att) {
    badIdxAttZ_ = true;
    attListHasChanged_ = true;
    attr_ = _att;
  }

  /**
   * A utiliser avec precaution: change les modeles sans aucune v�rification.
   *
   * @param _att
   * @param _cmd
   */
  protected void initAttributes(final GISAttributeModelObservable[] _att, final CtuluCommandContainer _cmd) {
    if (!Arrays.equals(_att, attr_)) {
      badIdxAttZ_ = true;
      attListHasChanged_ = true;
      final GISAttributeModelObservable[] old = attr_;
      attr_ = _att;
      fireAttributeValueChanged(-1, null, -1, null);
      if (_cmd != null) {
        _cmd.addCmd(new CtuluCommand() {
          @Override
          public void redo() {
            initAttributes(_att, null);
          }

          @Override
          public void undo() {
            initAttributes(old, null);
          }
        });
      }

    }

  }

  /**
   * @param _i l'indice des valeur
   * @return true si cree
   */
  protected boolean isDataCreated(final int _i) {
    return attr_ != null && attr_.length > _i && attr_[_i] != null;
  }

  @Override
  public abstract boolean accept(GISVisitor _v);

  /**
   * Ne fait pas de verif quant a l'origine des donnees. Remarque : Le modele a ajouter doit avoir le m�me nombre d'attributs que le mod�le courant.
   * Si ce n'est pas le cas, une erreur est d�clench�e.
   *
   * @param _model le model a ajouter
   * @param _cmd le receveur de commande
   * @param _doPostImport true si on doit lancer l'action de post-traitement.
   * @return true si ok.
   */
  public boolean addAll(final GISDataModel _model, final CtuluCommandContainer _cmd, final boolean _doPostImport) {
    if (!isGeomModifiable_ || _model == null) {
      return false;
    }
    if (_model.getNbAttributes() != getNbAttributes()) {
      throw new IllegalArgumentException("Bad attributes");
    }
    final int nbGeom = _model.getNumGeometries();
    final Geometry[] pt = new Geometry[nbGeom];
    for (int i = nbGeom - 1; i >= 0; i--) {
      pt[i] = _model.getGeometry(i);
    }
    final int nbAttribute = getNbAttributes();
    final List data = new ArrayList(nbAttribute);
    for (int iatt = 0; iatt < nbAttribute; iatt++) {
      final GISAttributeInterface gi = getAttribute(iatt);
      final Object[] os = new Object[nbGeom];
      for (int igeom = 0; igeom < nbGeom; igeom++) {
        // os[igeom] = gi.createDataForGeom(_model.getValue(iatt, igeom), pt[igeom].getNumPoints());
        os[igeom] = createOrAssignAttribut(iatt, _model.getValue(iatt, igeom), pt[igeom]);
      }
      data.add(nbGeom == 1 ? os[0] : os);

    }

    final int firstIdx = super.geometry_.getSize() - 1;
    super.geometry_.addAll(pt, data, _cmd);
    if (_doPostImport) {
      postImport(firstIdx);
    }
    updateListeners();

    return true;
  }

  /**
   * Cr�� ou assigne la valeur d'attribut pass�e. Si la valeur pass�e est null, une valeur par d�faut est affect�e. Si la valeur est scalaire et
   * l'attribut atomique, un tableau de valeurs est cr��. Si la valeur est un tableau et l'attribut est global, une valeur scalaire moyenne et cr��e.
   *
   * @param _iatt L'indice d'attribut dans la liste des attributs.
   * @param _init La valeur de l'attribut. Peut �tre null.
   * @param _dest La g�om�trie pour la cr�ation de la valeur.
   * @return La valeur pour l'attribut et la g�om�trie donn�e.
   */
  protected Object createOrAssignAttribut(final int _iatt, final Object _init, final Geometry _dest) {
    Object ret;
    final int nbPt = _dest.getNumPoints();
    Object val = fixedAttributes_.get(getAttribute(_iatt));
    if (val == null) {
      val = _init;
    }
    ret = getDataModel(_iatt).getAttribute().createDataForGeom(val, nbPt);
    //Fred pour mettre a jour le listener
    if (ret instanceof GISAttributeModelObservable) {
      ((GISAttributeModelObservable) ret).setListener(this);
    }
    // FIXME B.M. : Pas sur que ce soit le bienvenu. Si on veut des valeurs par d�faut et que la g�om�trie poss�de
    // des Z, il ne faudrait pas que ce soit initialis� (cas d'import de fichier et ajout par addAll).
    if (getIdxAttZ() != -1 && getAttributeIsZ() != null && getAttributeIsZ().isAtomicValue() && _init == null
            && getDataModel(_iatt).getAttribute() == getAttributeIsZ()) {
      final GISAttributeModelDoubleArray array = (GISAttributeModelDoubleArray) ret;
      for (int k = nbPt - 1; k >= 0; k--) {
        array.set(k, ((GISCoordinateSequenceContainerInterface) _dest).getCoordinateSequence().getOrdinate(k, 2));
      }
    }
    return ret;
  }

  public abstract void addCoordinateSequence(CoordinateSequence _seq, Object[] _datas, CtuluCommandContainer _cmd);

  /**
   * Ajoute une g�om�trie du bon type � la collection. En cas de mauvaise g�om�trie ajout�e, une Exception est lev�e.
   *
   * @param _geom La g�om�trie.
   * @param _datas Les valeurs d'attributs, dans l'ordre.
   * @param _cmd Le manager de commandes.
   * @return l'index de la g�om�trie cr��e, -1 si rien n'est cr��
   */
  public abstract int addGeometry(Geometry _geom, Object[] _datas, CtuluCommandContainer _cmd);

  public void removeGeometries(final int[] _idx, final CtuluCommandContainer _cmd) {
    if (!isGeomModifiable_) {
      return;
    }
    super.geometry_.remove(_idx, _cmd);
  }

  /**
   * Supprime toutes les g�om�tries.
   *
   * @param _cmd Le container de commande
   */
  public void removeAll(final CtuluCommandContainer _cmd) {
    if (isGeomModifiable_ && getNumGeometries() > 0) {
      geometry_.removeAll(_cmd);
    }
  }

  public GISAttributeModel[] getAtomicAttributeSubModel(final int _geomIdx) {
    final int nb = getNbAttributes();
    final List<GISAttributeModel> r = new ArrayList<GISAttributeModel>(nb);
    for (int i = 0; i < nb; i++) {
      final GISAttributeModel model = getDataModel(i);
      if (model.getAttribute().isAtomicValue()) {
        // cast inutile mais utilise pour detecte le plus tot les erreur d
        r.add((GISAttributeModel) model.getObjectValueAt(_geomIdx));
      }
    }
    final GISAttributeModel[] rf = new GISAttributeModel[r.size()];
    r.toArray(rf);
    return rf;
  }

  @Override
  public GISAttributeInterface getAttribute(final int _i) {
    final GISAttributeModel model = getDataModel(_i);
    return model == null ? null : model.getAttribute();
  }

  /**
   * @return l'attribut qui correspond � z: il sera automatiquement mis � jour lors de l'ajout de g�om�trie
   */
  public final GISAttributeDouble getAttributeIsZ() {
    return attributeIsZ_;
  }

  /**
   * @return les attributs utilises.
   */
  public GISAttributeInterface[] getAttributes() {
    final GISAttributeInterface[] res = new GISAttributeInterface[getNbAttributes()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = getAttribute(i);
    }
    return res;

  }

  /**
   * Retourne le mod�le d'attribut associ� � l'attribut en indice.
   *
   * @param _i L'indice suivant l'ordre de la liste des mod�le d'attributs. null si aucun mod�le associ�.
   */
  public GISAttributeModel getDataModel(final int _i) {
    return (GISAttributeModel) getData(_i);
  }

  /**
   * @return la classe a utiliser pour les sauvegardes dans les data stores
   */
  public abstract Class getDataStoreClass();

  @Override
  public int getIndiceOf(final GISAttributeInterface _att) {
    if (attListHasChanged_) {
      att2Idx_.clear();
      if (attr_ != null) {
        for (int i = 0; i < attr_.length; i++) {
          att2Idx_.put(attr_[i].getAttribute(), Integer.valueOf(i));
        }
      }
      attListHasChanged_ = false;
    }
    Integer idx = att2Idx_.get(_att);
    if (idx != null) {
      return idx.intValue();
    }
    return -1;
  }

  public GISAttributeInterface getAttributeWithID(final String _attributId) {
    if (attr_ != null && _attributId != null) {
      for (int i = attr_.length - 1; i >= 0; i--) {
        if (_attributId.equals(getAttribute(i).getID())) {
          return getAttribute(i);
        }
      }
    }
    return null;
  }

  /**
   * @param _att l'attribut recherche
   * @return le modele correspondant ou null si aucun.
   */
  public GISAttributeModel getModel(final GISAttributeInterface _att) {
    int idx = getIndiceOf(_att);
    if (idx != -1) {
      return attr_[idx];
    }
    return null;
  }

  public GISAttributeModel getModel(final int _i) {
    return (GISAttributeModelObservable) getData(_i);
  }

  public GISAttributeModel[] getModels() {
    final GISAttributeModel[] res = new GISAttributeModel[attr_.length];
    System.arraycopy(attr_, 0, res, 0, attr_.length);
    return res;
  }

  @Override
  public int getNbAttributes() {
    return attr_ == null ? 0 : attr_.length;
  }

  public int getNbGeometries() {
    return super.getNumGeometries();
  }

  /**
   * @return le titre ce cette collection
   */
  public final String getTitle() {
    return title_;
  }

  @Override
  public Object getValue(final int _idxAtt, final int _idxGeom) {
    if (_idxAtt < 0 || _idxAtt > getNbAttributes()) {
      return null;
    }
    return getModel(_idxAtt).getObjectValueAt(_idxGeom);
  }

  /**
   * Wrapper pour d�finir la valeur d'un attribut.
   *
   * @param _idxAtt L'indice de l'attribut.
   * @param _idxGeom La g�om�trie.
   * @param _data La valeur de l'attribut.
   * @param _cmd Le manager undo/redo.
   */
  public void setAttributValue(final int _idxAtt, final int _idxGeom, final Object _data,
          final CtuluCommandContainer _cmd) {
    if (_idxAtt < 0 || _idxAtt > getNbAttributes()) {
      return;
    }
    getModel(_idxAtt).setObject(_idxGeom, _data, _cmd);
  }

  /**
   * Mise a jour des listeners et du listener principal. A utiliser lors de la restauration uniquement.
   */
  public void initListeners(final GISZoneListener _listener) {
    setListener(_listener);
    updateListeners();
  }

  /**
   * @return true si les objets geometriques sont editables
   */
  public final boolean isGeomModifiable() {
    return isGeomModifiable_;
  }

  /**
   * Initialise le mod�le d'attribut utilis� pour Z avec les coordonn�es Z des points des g�om�tries. Principalement utilis� � la suite d'un import.
   *
   * @param _firstIdx l'indice de la premiere geometrie utilis�e pour l'initialisation.
   * @see #setAttributeIsZ(GISAttributeDouble)
   * @see {@link #prepareExport()} pour la m�thode inverse
   */
  public void postImport(final int _firstIdx) {
    if (getIdxAttZ() == -1) {
      return;
    }

    final int nb = getNumGeometries();
    for (int i = (_firstIdx < 0 ? 0 : _firstIdx); i < nb; i++) {
      initZAttribute(i);
    }
  }

  @Override
  public void preload(final GISAttributeInterface[] _att, final ProgressionInterface _prog) {
  }

  /**
   * Pr�pare l'export des g�om�tries, en affectant � la coordonn�e Z de leurs points les valeurs contenues dans l'attribut utilis� pour Z.
   *
   * @see #setAttributeIsZ(GISAttributeDouble)
   * @see {@link #postImport(int)} pour la m�thode inverse
   */
  public void prepareExport() {
    if (getIdxAttZ() == -1) {
      return;
    }

    final int nb = getNumGeometries();
    for (int i = 0; i < nb; i++) {
      initZCoordinate(i);
    }
  }

  protected int getIdxAttZ() {
    if (badIdxAttZ_) {
      idxAttZ_ = getIndiceOf(attributeIsZ_);
      badIdxAttZ_ = false;
    }
    return idxAttZ_;
  }

  /**
   * Initialise les coordonn�es Z d'une g�om�trie avec les valeurs contenues dans l'attribut utilis� pour Z.
   *
   * @param _idxGeom L'index de g�om�trie.
   */
  public void initZCoordinate(final int _idxGeom) {
    if (getIdxAttZ() == -1) {
      return;
    }

    // Il s'agit d'un polygone => Le dernier point doit prendre comme valeur celle du premier.
    boolean bgone = getGeometry(_idxGeom) instanceof GISPolygone;

    final GISAttributeModel model = getModel(getIdxAttZ());
    if (!model.getAttribute().isAtomicValue()) {
      final CoordinateSequence seq = getCoordinateSequence(_idxGeom);
      final Object objectValueAt = model.getObjectValueAt(_idxGeom);
      if (objectValueAt instanceof Double) {
        Double value = (Double) objectValueAt;
        for (int k = seq.size() - 1; k >= 0; k--) {
          seq.setOrdinate(k, 2, value);
        }
      }
    } else if (model instanceof GISAttributeModelObjectInterface) {
      final CoordinateSequence seq = getCoordinateSequence(_idxGeom);
      final GISAttributeModelDoubleArray arr = (GISAttributeModelDoubleArray) model.getObjectValueAt(_idxGeom);
      for (int k = seq.size() - 1; k >= 0; k--) {
        seq.setOrdinate(k, 2, arr.getValue(k));
      }
      if (bgone) {
        seq.setOrdinate(seq.size() - 1, 2, arr.getValue(0));
      }
    } else if (model instanceof CtuluCollectionDoubleEdit) {
      final CtuluCollectionDoubleEdit dModel = (CtuluCollectionDoubleEdit) model;
      final CoordinateSequence seq = getCoordinateSequence(_idxGeom);
      final double value = dModel.getValue(_idxGeom);
      for (int k = seq.size() - 1; k >= 0; k--) {
        seq.setOrdinate(k, 2, value);
      }
    }
  }

  /**
   * Initialise les valeurs dans l'attribut utilis� pour Z avec les coordonn�es Z d'une g�om�trie.
   *
   * @param _idxGeom L'index de g�om�trie.
   */
  public void initZAttribute(int _idxGeom) {
    if (getIdxAttZ() == -1) {
      return;
    }


    // Il s'agit d'un polygone => Le dernier point doit prendre comme valeur celle du premier.
    boolean bgone = getGeometry(_idxGeom) instanceof GISPolygone;

    final GISAttributeModel model = getModel(getIdxAttZ());

    if (model instanceof GISAttributeModelObjectInterface) {
      final CoordinateSequence seq = getCoordinateSequence(_idxGeom);
      final GISAttributeModelDoubleArray arr = (GISAttributeModelDoubleArray) model.getObjectValueAt(_idxGeom);
      for (int k = seq.size() - 1; k >= 0; k--) {
        arr.set(k, seq.getOrdinate(k, 2));
      }
      if (bgone) {
        arr.set(seq.size() - 1, seq.getOrdinate(0, 2));
      }
    } // L'attribut est global : On r�cup�re les coordonn�es, qu'on moyenne.
    else if (model instanceof CtuluCollectionDoubleEdit) {
      final CoordinateSequence seq = getCoordinateSequence(_idxGeom);
      double moy = 0;
      for (int k = seq.size() - 1; k >= 0; k--) {
        moy += seq.getOrdinate(k, 2);
      }
      model.setObject(_idxGeom, new Double(moy / seq.size()), null);
    }
  }

  /**
   * D�finit l'attribut qui correspond � Z. Il est alors utilis� lors des d�placements en Z de l'objet, ou pour la persistence en chaque point s'il
   * est atomique. Seul les attributs de type {@link GISAttributeDouble} sont autoris�s. <p> Remarque : L'attribut doit �tre dans la liste des
   * attributs de la collection pour que les traitements soient effectu�s en en tenant compte.
   *
   * @param _attributeIsZ l'attribut qui correspond � z: il sera automatiquement mis � jour lors de l'ajout de g�om�trie. Peut �tre null.
   */
  public void setAttributeIsZ(final GISAttributeDouble _attributeIsZ) {
    badIdxAttZ_ = true;
    attributeIsZ_ = _attributeIsZ;
  }

  /**
   * Definit les attributs pour lesquels la valeur est immuable. <p> <b>Remarque</b> : Si des objets existent d�j� avec une valeur pour l'attribut �
   * rendre immuable, ces valeurs ne seront pas remplac�e par la valeur constante. Utilisez {@link GISZoneCollection#setAttributes()} pour
   * r�initialiser les valeurs pour la collection.
   *
   * @param _att L'attribut � fixer.
   * @param _value La valeur constante. Si null, l'attribut n'est pas obligatoirement fix�.
   */
  public void setFixedAttributeValue(final GISAttributeInterface _att, final Object _value) {
    if (_value == null) {
      fixedAttributes_.remove(_att);
    } else {
      fixedAttributes_.put(_att, _value);
    }
  }

  /**
   * Retourne la valeur associ�e � un attribut immuable via la m�thode setFixedAttributeValue.
   *
   * @param _att : l'attribut recherch�
   * @return la valeur de l'attribut. Null si l'attribut n'est pas renseign�.
   */
  public Object getFixedAttributValue(GISAttributeInterface _att) {
    if (_att != null && fixedAttributes_.containsKey(_att)) {
      return fixedAttributes_.get(_att);
    }
    return null;
  }

  /**
   * Associe les attributs aux objets de la collection. Pour chaque attribut inexistant dans la liste est cr�� un nouveau mod�le.
   *
   * @param _att La liste des attributs, conditionnant l'ordre des mod�les dans la liste des mod�les.
   * @param _cmd Le conteneur de commande.
   */
  public void setAttributes(final GISAttributeInterface[] _att, final CtuluCommandContainer _cmd) {
    if (_att == attr_ || Arrays.equals(attr_, _att)) {
      return;
    }
    badIdxAttZ_ = true;
    attListHasChanged_ = true;
    // on sauvegarde les anciens attributs
    final GISAttributeModelObservable[] old = attr_;

    // on r�cup�re les valeurs d�j� utilis�es
    final Map<GISAttributeInterface, GISAttributeModel> exist = new HashMap<GISAttributeInterface, GISAttributeModel>(
            attr_ == null ? 0 : attr_.length);

    if (attr_ != null) {
      for (int i = attr_.length - 1; i >= 0; i--) {
        exist.put(attr_[i].getAttribute(), attr_[i]);
      }
    }
    final GISAttributeModelObservable[] newAtt = _att == null ? null : new GISAttributeModelObservable[_att.length];
    if (_att != null) {
      for (int i = _att.length - 1; i >= 0; i--) {
        newAtt[i] = (GISAttributeModelObservable) exist.get(_att[i]);
        if (newAtt[i] == null) {
          newAtt[i] = (GISAttributeModelObservable) GISZoneAttributeFactory.createModel(this, _att[i]);
        }
      }
    }
    attr_ = newAtt;
    //TODO: A am�liorer : appeler ces fire que quand il y a eu effectivement un ajout
    // ou une suppression d'attribut.
    fireAttributeAdded(-1, null);
    fireAttributeRemoved(-1, null);
    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          attr_ = newAtt;
          fireAttributeAdded(-1, null);
          fireAttributeRemoved(-1, null);
        }

        @Override
        public void undo() {
          attr_ = old;
          fireAttributeAdded(-1, null);
          fireAttributeRemoved(-1, null);
        }
      });
    }

  }

  public abstract void setCoordinateSequence(int _idx, CoordinateSequence _newSeq, CtuluCommandContainer _cmd);

  /**
   * @param _title le nouveau titre
   * @param _cmd la commande
   * @return true si modif
   */
  public final boolean setTitle(final String _title, final CtuluCommandContainer _cmd) {
    // test pour ne pas mettre le meme titre
    if (_title == title_ || (_title != null && _title.equals(title_))) {
      return false;
    }
    final String old = _title;
    title_ = _title;
    fireTitleChanged();
    if (_cmd != null) {
      _cmd.addCmd(new CtuluCommand() {
        @Override
        public void redo() {
          setTitle(_title, null);
        }

        @Override
        public void undo() {
          setTitle(old, null);
        }
      });
    }
    return true;

  }

  /**
   * Mise a jour des listeners.
   */
  public abstract void updateListeners();

  @Override
  protected void internActionCleared(final CtuluCommandContainer _c) {
    for (int i = getNbAttributes() - 1; i >= 0; i--) {
      getData(i).removeAll(_c);
    }

  }

  /**
   * Cette action est appele apres que le point ait �t� ajout�.
   */
  @Override
  protected void internActionPointAdded(final List _l, final CtuluCommandContainer _c, final int _nbValues) {
    if (_l == null) {
      for (int i = getNbAttributes() - 1; i >= 0; i--) {
        // si la valeur est cree, on ajoute automatiquement une valeur
        if (isDataCreated(i)) {
          final CtuluCollection model = getData(i);
          final Object defau = getDefaultValuesObject(i);
          if (_nbValues == 1) {
            model.addObject(defau, _c);
          } else {
            final Object[] newObject = new Object[_nbValues];
            Arrays.fill(newObject, defau);
            model.addAllObject(newObject, _c);
          }
        }
      }
    } else {
      for (int i = getNbAttributes() - 1; i >= 0; i--) {
        Object o = _l.get(i);
        // si o est null et les donn�es ne sont pas cr��es on ne fait rien
        if ((o != null) || isDataCreated(i)) {
          if (isDataCreated(i)) {
            if (o == null) {
              if (_nbValues == 1) {
                o = getDefaultValuesObject(i);
              } else {
                o = new Object[_nbValues];
                Arrays.fill((Object[]) o, getDefaultValuesObject(i));
              }
            }
            // la liste de donn�es est deja cree, on ajoute
            if (_nbValues > 1) {
              getData(i).addAllObject(o, _c);
            } else {
              getData(i).addObject(o, _c);
            }

          } else {
            // la liste est cree automatiquement
            // elle fait deja la taille du nombre de point
            if (_nbValues > 1) {
              final int[] idx = new int[_nbValues];
              final int max = getNumGeometries() - 1;

              for (int k = _nbValues - 1; k >= 0; k--) {
                idx[k] = max - k;
              }
              if (o == null) {
                getData(i).setObject(idx, getDefaultValuesObject(i), _c);
              } else {
                getData(i).setAllObject(idx, (Object[]) o, _c);
              }
            } else {
              if (o == null) {
                o = getDefaultValuesObject(i);
              }
              getData(i).setObject(getNumGeometries() - 1, o, _c);
            }
          }
        }
      }
    }

  }

  @Override
  protected void internActionPointInserted(final int _i, final List _l, final CtuluCommandContainer _c) {
    if (_l == null) {
      for (int i = getNbAttributes() - 1; i >= 0; i--) {
        if (isDataCreated(i)) {
          getData(i).insertObject(_i, null, _c);
        }
      }
    } else {
      for (int i = getNbAttributes() - 1; i >= 0; i--) {
        Object o = _l.get(i);
        if ((o != null) || isDataCreated(i)) {
          if (isDataCreated(i)) {
            if (o == null) {
              o = getDefaultValuesObject(i);
            }
            // la liste de donn�es est deja cree, on ajoute
            getData(i).insertObject(_i, o, _c);
          } else {
            if (o == null) {
              o = getDefaultValuesObject(i);
            }
            // la liste est cree automatiquement
            // elle fait deja la taille du nombre de point
            getData(i).setObject(_i, o, _c);
          }
        }
      }
    }

  }

  @Override
  protected void internActionPointRemoved(final int _i, final CtuluCommandContainer _c) {
    for (int i = getNbAttributes() - 1; i >= 0; i--) {
      if (isDataCreated(i)) {
        getData(i).remove(_i, _c);
      }
    }
  }

  @Override
  protected void internActionPointRemoved(final int[] _i, final CtuluCommandContainer _c) {
    for (int i = getNbAttributes() - 1; i >= 0; i--) {
      if (isDataCreated(i)) {
        getData(i).remove(_i, _c);
      }
    }
  }

  /**
   * Permute les deux g�om�tries indiqu�es par les deux index pass�s en param�tre. Si les index sont incoh�rents, {@link IllegalArgumentException} est
   * lev�.
   *
   * @param _index1 l'index de la premi�re g�om�trie
   * @param _index2 l'index de la seconde gom�trie
   * @param _cmd le command manager pour le undo/redo
   */
  @Override
  public void switchGeometries(int _index1, int _index2, CtuluCommandContainer _cmd) {
    if (_index1 < 0 || _index1 > geometry_.getSize() - 1 || _index2 < 0 || _index2 > geometry_.getSize() - 1) {
      throw new IllegalArgumentException("Au moins l'un des deux index est incoh�rent.");
    }
    if (_index1 != _index2) {
      CtuluCommandComposite cmd=new CtuluCommandComposite();
      // Permutation des g�om�tries
      super.switchGeometries(_index1, _index2, cmd);
      // Permutation des valeurs d'attributs

      if (attr_ != null) {
        for (int i=0; i < attr_.length; i++) {
          Object tmp=attr_[i].getObjectValueAt(_index1);
          attr_[i].setObject(_index1, attr_[i].getObjectValueAt(_index2), cmd);
          attr_[i].setObject(_index2, tmp, cmd);
        }
        fireAttributeValueChanged(-1, null, _index1, null);
        fireAttributeValueChanged(-1, null, _index2, null);
      }
      if (_cmd != null) {
        _cmd.addCmd(cmd.getSimplify());
      }
    }
  }
}
