/*
 * @creation 8 juin 2005
 * 
 * @modification $Date: 2006-12-05 10:12:50 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * @author Fred Deniger
 * @version $Id: GISDataModelPointPolyAdapter.java,v 1.8 2006-12-05 10:12:50 deniger Exp $
 */
public class GISDataModelPointPolyAdapter implements GISDataModel {

  GISDataModel init_;

  final LineString str_;

  final GISAttributeInterface[] att_;

  final Object[] values_;

  public static class PolyOption {

    boolean poly_;
    boolean check_;

    public boolean isCheck() {
      return check_;
    }

    public void setCheck() {
      check_ = true;
      poly_ = false;

    }

    public boolean isPoly() {
      return poly_;
    }

    public void setPoly() {
      poly_ = true;
      check_ = false;
    }
  }

  private final PolyOption polyOption;

  public GISDataModelPointPolyAdapter(final GISDataModel _init, final GISAttributeInterface[] _att, final PolyOption _poly) {
    super();
    this.polyOption = _poly;
    init_ = _init;
    att_ = _att;
    Coordinate[] cs = new Coordinate[_init.getNumGeometries()];
    for (int i = cs.length - 1; i >= 0; i--) {
      cs[i] = _init.getGeometry(i).getCoordinate();
    }
    if (_poly.isPoly()) {
      if (!cs[0].equals2D(cs[cs.length - 1])) {
        final Coordinate[] newCs = new Coordinate[cs.length + 1];
        System.arraycopy(cs, 0, newCs, 0, cs.length);
        newCs[newCs.length - 1] = cs[0];
        cs = newCs;
      }
      str_ = GISGeometryFactory.INSTANCE.createLinearRing(cs);
    } else if (_poly.isCheck() && cs[0].equals2D(cs[cs.length - 1])) {
      str_ = GISGeometryFactory.INSTANCE.createLinearRing(cs);
    } else {
      str_ = GISGeometryFactory.INSTANCE.createLineString(cs);
    }
    values_ = new Object[att_ == null ? 0 : att_.length];
    for (int i = values_.length - 1; i >= 0; i--) {
      final int idxAtt = _init.getIndiceOf(att_[i]);
      if (idxAtt >= 0) {
        final Object[] init = new Object[cs.length];
        final int max = Math.min(init.length, _init.getNumGeometries());
        for (int j = max - 1; j >= 0; j--) {
          init[j] = _init.getValue(idxAtt, j);
        }
        // values_[i] = att_[i].createDataForGeom(init, init.length);
        values_[i] = init;
      }

    }
  }

  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    return new GISDataModelPointPolyAdapter(init_.createTranslate(xyToAdd), att_, polyOption);
  }

  @Override
  public GISAttributeInterface getAttribute(final int _idxAtt) {
    return att_[_idxAtt];
  }

  @Override
  public void preload(final GISAttributeInterface[] _att, final ProgressionInterface _prog) {
    init_.preload(_att, null);

  }

  @Override
  public Envelope getEnvelopeInternal() {
    return str_.getEnvelopeInternal();
  }

  @Override
  public Geometry getGeometry(final int _idxGeom) {
    return str_;
  }

  @Override
  public int getIndiceOf(final GISAttributeInterface _att) {
    return CtuluLibArray.findObjectEgalEgal(att_, _att);
  }

  @Override
  public int getNbAttributes() {
    return att_ == null ? 0 : att_.length;
  }

  @Override
  public int getNumGeometries() {
    return 1;
  }

  @Override
  public Object getValue(final int _idxAtt, final int _idxGeom) {
    return values_[_idxAtt];
  }

  @Override
  public double getDoubleValue(final int _idxAtt, final int _idxGeom) {
    return ((Number) getValue(_idxAtt, _idxGeom)).doubleValue();
  }
}
