/*
 * @creation     14 oct. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import java.util.ArrayList;
import java.util.HashMap;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * Cet Adapter converti les polygones en Multipoints. Les g�om�tries faisant
 * partie du model ne pouvant �tre converties (parce que se ne sont pas des
 * polygones) deviennent invisibles.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class GISDataModelPolygoneToMultiPointAdapter implements GISDataModel {

  private final GISDataModel model_;
  /** Table de translation d'indices des g�om�tries. */
  private final ArrayList<Integer> translationTable_;
  /** Table cache des multipoints cr�es.*/
  private final HashMap<Integer, GISMultiPoint> cacheMultiPoint_;
  
  public GISDataModelPolygoneToMultiPointAdapter(GISDataModel _model){
    if(_model==null)
      throw new IllegalArgumentException("_model ne doit pas �tre null.");
    translationTable_=new ArrayList<Integer>();
    cacheMultiPoint_=new HashMap<Integer, GISMultiPoint>();
    model_=_model;
    // Construction de la table de translation
    for(int i=0;i<model_.getNumGeometries();i++)
      if(model_.getGeometry(i) instanceof GISPolygone)
        translationTable_.add(i);
  }
  
  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if(xyToAdd==null){
      return this;
    }
    return new GISDataModelPolygoneToMultiPointAdapter(model_.createTranslate(xyToAdd));
  }
  
  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getAttribute(int)
   */
  @Override
  public GISAttributeInterface getAttribute(int _att) {
    return model_.getAttribute(_att);
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getDoubleValue(int, int)
   */
  @Override
  public double getDoubleValue(int _att, int _geom) {
    if(_geom>0&&_geom<translationTable_.size())
      return getDoubleValue(_att, translationTable_.get(_geom));
    return 0;
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getEnvelopeInternal()
   */
  @Override
  public Envelope getEnvelopeInternal() {
    return model_.getEnvelopeInternal();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getGeometry(int)
   */
  @Override
  public Geometry getGeometry(int _geom) {
    if (_geom>=0&&_geom<translationTable_.size()) {
      if (!cacheMultiPoint_.containsKey(_geom))
        cacheMultiPoint_.put(_geom, GISLib.toMultipoint(((GISPolygone)model_.getGeometry(translationTable_.get(_geom))).getCoordinateSequence()));
      return cacheMultiPoint_.get(_geom);
    }
    return null;
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getIndiceOf(org.fudaa.ctulu.gis.GISAttributeInterface)
   */
  @Override
  public int getIndiceOf(GISAttributeInterface _att) {
    return model_.getIndiceOf(_att);
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getNbAttributes()
   */
  @Override
  public int getNbAttributes() {
    return model_.getNbAttributes();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getNumGeometries()
   */
  @Override
  public int getNumGeometries() {
    return translationTable_.size();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getValue(int, int)
   */
  @Override
  public Object getValue(int _att, int _geom) {
    if(_geom>0&&_geom<translationTable_.size())
      return model_.getValue(_att, translationTable_.get(_geom));
    return null;
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#preload(org.fudaa.ctulu.gis.GISAttributeInterface[], org.fudaa.ctulu.ProgressionInterface)
   */
  @Override
  public void preload(GISAttributeInterface[] _att, ProgressionInterface _prog) {
    model_.preload(_att, _prog);
  }
}
