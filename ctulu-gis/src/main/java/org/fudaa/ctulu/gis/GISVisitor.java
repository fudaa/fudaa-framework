/*
 *  @creation     31 mai 2005
 *  @modification $Date: 2006-08-17 16:26:13 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.*;

/**
 * @author Fred Deniger
 * @version $Id: GISVisitor.java,v 1.3 2006-08-17 16:26:13 deniger Exp $
 */
public interface GISVisitor {

  boolean visitPoint(Point _p);

  boolean visitPolygone(LinearRing _p);

  boolean visitPolyligne(LineString _p);

  boolean visitPolygoneWithHole(Polygon _p);

  boolean visitGeometryCollection(GeometryCollection _p);

  boolean visitMultiPoint(MultiPoint _p);

  boolean visitMultiPolyligne(MultiLineString _p);

  boolean visitMultiPolygoneWithHole(MultiPolygon _p);

}
