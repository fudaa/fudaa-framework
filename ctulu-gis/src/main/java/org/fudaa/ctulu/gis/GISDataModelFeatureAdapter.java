/*
 * @creation 4 sept. 06
 *
 * @modification $Date: 2008-02-14 16:55:49 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import com.memoire.fu.FuLog;
import gnu.trove.TIntArrayList;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.ProgressionInterface;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import java.util.ArrayList;
import java.util.List;

/**
 * Un adapteur {@link GISDataModel} qui s'appuie sur des donn�es {@link FeatureCollection}. Il est cr�� par la m�thode
 * {@link #load(FeatureCollection)}
 *
 * @author fred deniger
 * @version $Id: GISDataModelFeatureAdapter.java,v 1.5.6.1 2008-02-14 16:55:49 bmarchan Exp $
 */
public final class GISDataModelFeatureAdapter implements GISDataModel {
  public static final String VALUE_SEPARATOR = ",";
  public static final String VALUE_SEPARATOR_AS_STRING = "&#130;";
  final GISAttributeInterface[] att_;
  final Object[][] datas_;
  final Geometry[] geoms_;

  private GISDataModelFeatureAdapter(final GISAttributeInterface[] _att, final Object[][] _datas, final Geometry[] _geoms) {
    super();
    att_ = _att;
    datas_ = _datas;
    geoms_ = _geoms;
  }

  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    return new GisDataModelTranslated(this, xyToAdd);
  }

  @Override
  public GISAttributeInterface getAttribute(final int _idxAtt) {
    return att_[_idxAtt];
  }

  @Override
  public double getDoubleValue(final int _idxAtt, final int _idxGeom) {
    return ((Double) datas_[_idxAtt][_idxGeom]).doubleValue();
  }

  @Override
  public Envelope getEnvelopeInternal() {
    return null;
  }

  @Override
  public Geometry getGeometry(final int _idxGeom) {
    return geoms_[_idxGeom];
  }

  @Override
  public int getIndiceOf(final GISAttributeInterface _att) {
    return CtuluLibArray.findObject(att_, _att);
  }

  @Override
  public int getNbAttributes() {
    return att_.length;
  }

  @Override
  public int getNumGeometries() {
    return geoms_.length;
  }

  @Override
  public Object getValue(final int _idxAtt, final int _idxGeom) {
    return datas_[_idxAtt][_idxGeom];
  }

  @Override
  public void preload(final GISAttributeInterface[] _att, final ProgressionInterface _prog) {
  }

  /**
   * Cr�ation de l'adapteur de feature geotools.
   */
  public static GISDataModel load(final FeatureCollection<SimpleFeatureType, SimpleFeature> _coll) {
    if (_coll.size() == 0) {
      return null;
    }
    final SimpleFeatureType type = _coll.getSchema();
    final int initAttributeCount = type.getAttributeCount();
    final List atts = new ArrayList(initAttributeCount);
    int indexGeomAttr = -1; // Index de l'attribut n�c�ssaire au classement de la g�om�trie dans la zone
    final TIntArrayList idxInFeature = new TIntArrayList(initAttributeCount);
    for (int i = 0; i < initAttributeCount; i++) {
      final GISAttributeInterface att = GISLib.createAttributeFrom(type.getDescriptor(i));
      if (att != null) {
        if (att.getID().equals(GISAttributeConstants.INDEX_GEOM.getID())) {
          indexGeomAttr = i;
        } else {
          atts.add(att);
          idxInFeature.add(i);
        }
      }
    }
    final GISAttributeInterface[] finalAtt = (GISAttributeInterface[]) atts.toArray(new GISAttributeInterface[atts.size()]);
    final int finalAttributeCount = finalAtt.length;
    final List geom = new ArrayList();
    final List<Integer> indexGeom = new ArrayList<Integer>();
    final List[] values = new ArrayList[finalAttributeCount];
    for (int i = values.length - 1; i >= 0; i--) {
      values[i] = new ArrayList();
    }
    try (final FeatureIterator<SimpleFeature> it = _coll.features()) {
      while (it.hasNext()) {
        final SimpleFeature f = it.next();

        if (indexGeomAttr != -1) {
          Number i = (Number) f.getAttribute(indexGeomAttr);
          if (i == null) {
            FuLog.error("Probleme sur le parseur, probablement du a la version Java 1.6");
          }
          indexGeom.add(i.intValue());
        }
        geom.add(GISGeometryFactory.INSTANCE.transformToCustom((Geometry) f.getDefaultGeometry()));

        for (int i = 0; i < finalAttributeCount; i++) {
          // Les attributs atomiques sont autoris�s. La valeur lue sur les features doit �tre sous la forme {val1,val2,val3}
          if (finalAtt[i].isAtomicValue()) {
            String s = (String) f.getAttribute(idxInFeature.getQuick(i));
            String[] svals = s.substring(1, s.length() - 1).split(VALUE_SEPARATOR);
            for (int j = 0; j < svals.length; j++) {
              StringUtils.replace(svals[j], VALUE_SEPARATOR_AS_STRING, VALUE_SEPARATOR);
            }

            if (finalAtt[i].getDataClass().equals(Integer.class)) {
              Integer[] vals = new Integer[svals.length];
              for (int j = 0; j < vals.length; j++) {
                vals[j] = new Integer(svals[j]);
              }
              values[i].add(finalAtt[i].createDataForGeom(vals, vals.length));
            } else if (finalAtt[i].getDataClass().equals(Boolean.class)) {
              Boolean[] vals = new Boolean[svals.length];
              for (int j = 0; j < vals.length; j++) {
                vals[j] = Boolean.valueOf(svals[j]);
              }
              values[i].add(finalAtt[i].createDataForGeom(vals, vals.length));
            } else if (finalAtt[i].getDataClass().equals(Double.class)) {
              Double[] vals = new Double[svals.length];
              for (int j = 0; j < vals.length; j++) {
                vals[j] = new Double(svals[j]);
              }
              values[i].add(finalAtt[i].createDataForGeom(vals, vals.length));
            } else if (finalAtt[i].getDataClass().equals(String.class)) {
              String[] vals = new String[svals.length];
              for (int j = 0; j < vals.length; j++) {
                vals[j] = svals[j];
              }
              values[i].add(finalAtt[i].createDataForGeom(vals, vals.length));
            }
          } // La valeur lue est globale.
          else {
            values[i].add(f.getAttribute(idxInFeature.getQuick(i)));
          }
        }
      }

      if (indexGeomAttr != -1) {
        // Classement des g�om�tries en fonction de leur index \\
        /*
         * L'algorithme utilis� est simple : pour chaque index on permute le
         * contenu de cet index dans la case correspondant � ce contenu. Et cela
         * tant que le contenu de la case ne correspond pas � son index. Exemple :
         * [1, 2, 0, 4, 3] => i=0; permutation index : 0 avec 1
         * [2, 1, 0, 4, 3] => i=0; permutation index : 0 avec 2
         * [2, 1, 0, 4, 3] => i=1;
         * [2, 1, 0, 4, 3] => i=2;
         * [0, 1, 2, 4, 3] => i=3; permutation index : 3 avec 4
         * [0, 1, 2, 3, 4] => i=4; fin (le traitement du dernier index n'est jamais utile)
         */
        for (int i = 0; i < indexGeom.size(); i++) {
          while (i != indexGeom.get(i).intValue()) {
            int j = indexGeom.get(i);
            // permutation entre i et j dans indexGeom
            indexGeom.set(i, indexGeom.get(j));
            indexGeom.set(j, j);
            // permutation entre i et j dans geom
            Object tmpGeom = geom.get(i);
            geom.set(i, geom.get(j));
            geom.set(j, tmpGeom);
            // permutation entre i et j dans values
            for (int k = 0; k < values.length; k++) {
              Object tmpValue = values[k].get(i);
              values[k].set(i, values[k].get(j));
              values[k].set(j, tmpValue);
            }
          }
        }
      }
    }

    final Object[][] finalValues = new Object[values.length][geom.size()];
    for (int i = finalValues.length - 1; i >= 0; i--) {
      finalValues[i] = values[i].toArray();
    }
    return new GISDataModelFeatureAdapter(finalAtt, finalValues, (Geometry[]) geom.toArray(new Geometry[geom.size()]));
  }
}
