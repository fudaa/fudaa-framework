/*
 *  @creation     25 mai 2005
 *  @modification $Date: 2006-07-13 13:34:36 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;

/**
 * @author Fred Deniger
 * @version $Id: GISPolyligneNiveau.java,v 1.4 2006-07-13 13:34:36 deniger Exp $
 */
public class GISPolyligneNiveau extends GISPolyligne {

  /**
   * @param _points
   */
  public GISPolyligneNiveau(final CoordinateSequence _points) {
    super(_points);
  }

  /**
   *
   */
  public GISPolyligneNiveau() {
    super();
  }

  @Override
  protected LineString copyInternal() {
    return new GISPolyligneNiveau(points.copy());
  }

  /**
   * @return pour definir si c'est une courbe de niveau
   */
  @Override
  public boolean isNiveau(){
    return true;
  }

  /**
   * @param _points
   * @param _factory
   */
  public GISPolyligneNiveau(final CoordinateSequence _points, final GeometryFactory _factory) {
    super(_points, _factory);
  }

}
