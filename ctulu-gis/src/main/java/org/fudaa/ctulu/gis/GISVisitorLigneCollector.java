/*
 * @creation 5 d�c. 06
 * @modification $Date: 2007-02-02 11:20:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fred deniger
 * @version $Id: GISVisitorLigneCollector.java,v 1.2 2007-02-02 11:20:05 deniger Exp $
 */
public class GISVisitorLigneCollector extends GISVisitorDefault {

  final List l_ = new ArrayList();
  final boolean onlyPolygones_;

  public GISVisitorLigneCollector(final boolean _onlyPolygones) {
    super();
    onlyPolygones_ = _onlyPolygones;
  }

  @Override
  public boolean visitPoint(final Point _p) {
    return true;
  }

  @Override
  public boolean visitPolygone(final LinearRing _p) {
    l_.add(_p);
    return true;
  }

  @Override
  public boolean visitPolygoneWithHole(final Polygon _p) {
    return true;
  }

  @Override
  public boolean visitPolyligne(final LineString _p) {
    if (!onlyPolygones_) {
      l_.add(_p);
    }
    return true;
  }

  public List getPolygones() {
    return l_;
  }

}
