/*
 * @creation 14 oct. 2008
 * 
 * @modification $Date:$
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import java.util.HashMap;
import java.util.Map;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * Cette adapter permet de substituer des attributs existant par d'autre. Par exemple, me model poss�de deux attributs :
 * V1 (type double) et Z (type double), l'adapter permet de substituer V& � Z. Ainsi toutes les requ�tes sur Z seront
 * redirig�es sur V1. Les deux attributs doivent pr�alablement exist�s, avoir des types de valeur identique et �tre de
 * m�me niveau (global ou atomique). A not� que se ne sont que les valeurs qui sont substituer. En cas d'appelle � la
 * m�thod getAttribut sur l'attribut substituer, sont GISAttributInterface sera toujours renvoy� comme avant.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class GISDataModelAttributeSubstitutionAdapter implements GISDataModel {

  private final GISDataModel model_;
  /**
   * Table contenant les substitutions d'attributs � faire sur le model. Pour des raisons de performence, la
   * substitution est m�moris� par l'index des attributs.
   */
  private final HashMap<Integer, Integer> substitutionTable_;

  /**
   * Cette adapter permet de substituer des attributs existant par d'autre. Par exemple, me model poss�de deux attributs
   * : V1 (type double) et Z (type double), l'adapter permet de substituer V1 � Z. Ainsi toutes les requ�tes sur Z
   * seront redirig�es sur V1. Les deux attributs doivent pr�alablement exist�s et avoir des types de valeur identique.
   * Si les attributs sont de niveau (global ou atomique) diff�rent, l'adapter converti les valeurs en utilisant la
   * m�thod createDataForGeom. A not� que se ne sont que les valeurs qui sont substituer. En cas d'appelle � la m�thod
   * getAttribut sur l'attribut substituer, sont GISAttributInterface sera toujours renvoy� comme avant.
   * 
   * @param _model le model � adaoter
   * @param _substitutionTable cette table contient les substitutions � faire. Une substitution est un tableau � deux
   *          �l�ments : le premier l'attribut a substituer et le second l'attribut par lequel le substituer.
   *          _substitutionTable est donc un tableau de tableau � deux �l�ments.
   */
  public GISDataModelAttributeSubstitutionAdapter(GISDataModel _model, GISAttributeInterface[][] _substitutionTable) {
    if (_substitutionTable == null || _model == null)
      throw new IllegalArgumentException("_substitutionTable et _model ne doivent pas �tre null.");
    model_ = _model;
    substitutionTable_ = new HashMap<Integer, Integer>();
    // Verification des substitutions
    for (int i = 0; i < _substitutionTable.length; i++) {
      if (_substitutionTable[i].length != 2)
        throw new IllegalArgumentException("Une substitution est un tableau � deux �l�ments, ni plus, ni moins.");
      if (_substitutionTable[i][0].getDataClass() != _substitutionTable[i][1].getDataClass())
        throw new IllegalArgumentException("Les deux substitutions doivent avoir des valeurs de m�me type.");
      if (model_.getIndiceOf(_substitutionTable[i][0]) == -1)
        throw new IllegalArgumentException(_substitutionTable[i][0].getID() + "n'est pas dans le model.");
      if (model_.getIndiceOf(_substitutionTable[i][1]) == -1)
        throw new IllegalArgumentException(_substitutionTable[i][1].getID() + "n'est pas dans le model.");
      // Ajout de la substitution
      substitutionTable_.put(model_.getIndiceOf(_substitutionTable[i][0]), model_.getIndiceOf(_substitutionTable[i][1]));
    }
  }

  /**
   * Utilise par {@link #createTranslate(GISPoint)} uniquement
   * @param _model
   * @param substitutionTable
   */
  private GISDataModelAttributeSubstitutionAdapter(GISDataModel _model, HashMap<Integer, Integer> substitutionTable) {
    this.model_ = _model;
    this.substitutionTable_ = substitutionTable;
  }

  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    return new GISDataModelAttributeSubstitutionAdapter(model_.createTranslate(xyToAdd),substitutionTable_);
  }

  /**
   * Cette adapter permet de substituer des attributs existant par d'autre. Par exemple, me model poss�de deux attributs
   * : V1 (type double) et Z (type double), l'adapter permet de substituer V& � Z. Ainsi toutes les requ�tes sur Z
   * seront redirig�es sur V1. Les deux attributs doivent pr�alablement exist�s, avoir des types de valeur identique et
   * �tre de m�me niveau (global ou atomique). A not� que se ne sont que les valeurs qui sont substituer. En cas
   * d'appelle � la m�thod getAttribut sur l'attribut substituer, sont GISAttributInterface sera toujours renvoy� comme
   * avant.
   * 
   * @param _model le model � adaoter
   * @param _substitutionTable cette map contient les substitutions � faire. Le premier l'attribut a substituer et le
   *          second l'attribut par lequel le substituer.
   */
  public GISDataModelAttributeSubstitutionAdapter(GISDataModel _model, Map<GISAttributeInterface, GISAttributeInterface> _substitutionTable) {
    if (_substitutionTable == null || _model == null)
      throw new IllegalArgumentException("_substitutionTable et _model ne doivent pas �tre null.");
    model_ = _model;
    substitutionTable_ = new HashMap<Integer, Integer>();
    // Verification des substitutions
    for (Map.Entry<GISAttributeInterface, GISAttributeInterface> entry : _substitutionTable.entrySet()) {
      if (entry.getKey().getDataClass() != entry.getValue().getDataClass())
        throw new IllegalArgumentException("Les deux substitutions doivent avoir des valeurs de m�me type.");
      if (model_.getIndiceOf(entry.getKey()) == -1)
        throw new IllegalArgumentException(entry.getKey().getID() + " n'est pas dans le model.");
      if (model_.getIndiceOf(entry.getValue()) == -1)
        throw new IllegalArgumentException(entry.getValue().getID() + " n'est pas dans le model.");
      // Ajout de la substitution
      substitutionTable_.put(model_.getIndiceOf(entry.getKey()), model_.getIndiceOf(entry.getValue()));
    }
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getAttribute(int)
   */
  @Override
  public GISAttributeInterface getAttribute(int _att) {
    return model_.getAttribute(_att);
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getDoubleValue(int, int)
   */
  @Override
  public double getDoubleValue(int _att, int _geom) {
    if (substitutionTable_.containsKey(_att))
      return model_.getDoubleValue(substitutionTable_.get(_att), _geom);
    else return model_.getDoubleValue(_att, _geom);
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getEnvelopeInternal()
   */
  @Override
  public Envelope getEnvelopeInternal() {
    return model_.getEnvelopeInternal();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getGeometry(int)
   */
  @Override
  public Geometry getGeometry(int _geom) {
    return model_.getGeometry(_geom);
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getIndiceOf(org.fudaa.ctulu.gis.GISAttributeInterface)
   */
  @Override
  public int getIndiceOf(GISAttributeInterface _att) {
    return model_.getIndiceOf(_att);
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getNbAttributes()
   */
  @Override
  public int getNbAttributes() {
    return model_.getNbAttributes();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getNumGeometries()
   */
  @Override
  public int getNumGeometries() {
    return model_.getNumGeometries();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getValue(int, int)
   */
  @Override
  public Object getValue(int _att, int _geom) {
    if (substitutionTable_.containsKey(_att))
      if (getAttribute(_att).isAtomicValue() != getAttribute(substitutionTable_.get(_att)).isAtomicValue()) {
        Object value;
        if (getAttribute(substitutionTable_.get(_att)).isAtomicValue())
          value = ((GISAttributeModel) model_.getValue(substitutionTable_.get(_att), _geom)).getAverage();
        else value = model_.getValue(substitutionTable_.get(_att), _geom);
        return model_.getAttribute(_att).createDataForGeom(value, model_.getGeometry(_geom).getNumPoints());
      } else return model_.getValue(substitutionTable_.get(_att), _geom);
    else return model_.getValue(_att, _geom);
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#preload(org.fudaa.ctulu.gis.GISAttributeInterface[], org.fudaa.ctulu.ProgressionInterface)
   */
  @Override
  public void preload(GISAttributeInterface[] _att, ProgressionInterface _prog) {
    model_.preload(_att, _prog);
  }

}
