/*
 *  @creation     11 f�vr. 2004
 *  @modification $Date: 2006-02-09 08:59:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import java.io.File;
import org.fudaa.ctulu.CtuluIOOperationSynthese;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * Une interface qui permet de specifier les formats qui peuvent lire une serie de point.
 * @author deniger
 * @version $Id: GISFileFormatPointContainer.java,v 1.3 2006-02-09 08:59:28 deniger Exp $
 */
public interface GISFileFormatPointContainer {

  /**
   * Il est suppose que la source renvoye est de type GISPointContainer.
   * @param _f le fichier a lire
   * @param _prog la barre de progression
   * @return la synthese de l'operation
   */
  CtuluIOOperationSynthese readListPoint(File _f,ProgressionInterface _prog);

}
