/*
 *  @creation     11 f�vr. 2004
 *  @modification $Date: 2006-02-09 08:59:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.CoordinateSequence;

/**
 * Interface definissant des g�om�tries � base de {@link CoordinateSequence}.
 *
 * @author Bertrand Marchand
 * @version $Id: GISPointContainerInterface.java,v 1.3 2006-02-09 08:59:28 deniger Exp $
 */
public interface GISCoordinateSequenceContainerInterface extends GISGeometry {

  /**
   * @return La sequence de coordonn�es pour la g�om�trie.
   */
  CoordinateSequence getCoordinateSequence();
}
