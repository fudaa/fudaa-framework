/*
 *  @creation     8 avr. 2005
 *  @modification $Date: 2006-07-13 13:34:36 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.CoordinateSequence;

/**
 * @author Fred Deniger
 * @version $Id: GISReprojectInterpolateur1DInteger.java,v 1.4 2006-07-13 13:34:36 deniger Exp $
 */
public class GISReprojectInterpolateur1DInteger extends GISReprojectInterpolateur1D implements
    GISReprojectInterpolateurI.IntegerTarget {

  final GISAttributeModelIntegerInterface modelSrc_;

  /**
   * @param _src les coordonnées sources
   * @param _target les coordonnées cibles
   * @param _modelSrc le model source
   */
  public GISReprojectInterpolateur1DInteger(final CoordinateSequence _src, final CoordinateSequence _target,
      final GISAttributeModelIntegerInterface _modelSrc) {
    super(_src, _target);
    modelSrc_ = _modelSrc;
  }

  @Override
  public int interpol(final int _newIdx){
    final int i = getSrcEquivalent(_newIdx);
    return i >= 0 ? modelSrc_.getValue(i) : ((Integer) modelSrc_.getAttribute().getDefaultValue())
        .intValue();
  }

}
