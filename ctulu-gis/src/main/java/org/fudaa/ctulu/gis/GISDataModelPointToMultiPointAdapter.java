/*
 *  @creation     8 juin 2005
 *  @modification $Date: 2007-04-16 16:33:52 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.Point;
import java.util.ArrayList;
import java.util.HashMap;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * Un mod�le pour adapter un modele de points vers un mod�le de multipoints.
 * Lors de ce passage, si l'attribut ATT_NUM_BLOC existe, alors il est utilis�
 * pour g�n�rer les multipoints. Sinon, un seul multipoint est cr��.
 * 
 * @author Bertrand Marchand
 * @version $Id: GISDataModelListPtAdapter.java,v 1.8 2007-04-16 16:33:52
 *          deniger Exp $
 */
public class GISDataModelPointToMultiPointAdapter implements GISDataModel {

  /** Un attribut num�ro de bloc, pour faire des groupes de points. */
  public static final GISAttributeInterface ATT_NUM_BLOC=new GISAttributeInteger("numbloc");

  // Cache.
  private GISMultiPoint[] multi_=null;
  private Object[][] datas_=null;

  private int idxAttNumBloc_;
  private HashMap<Integer, ArrayList<Integer>> hibloc2ListPts_=new HashMap<Integer, ArrayList<Integer>>();
  private GISDataModel model_;

  /**
   * Constructeur. On ne controle pas que le modele contient bien des points.
   */
  public GISDataModelPointToMultiPointAdapter(final GISDataModel _mdpts) {
    model_=_mdpts;
    datas_=new Object[model_.getNbAttributes()][];
    
    idxAttNumBloc_=model_.getIndiceOf(ATT_NUM_BLOC);
    // Pas de blocs, 1 seul multipoint.
    if (idxAttNumBloc_==-1) {
      ArrayList<Integer> idxpts=new ArrayList<Integer>(model_.getNumGeometries());
      for (int i=0; i<model_.getNumGeometries(); i++)
        idxpts.add(i);
      hibloc2ListPts_.put(0, idxpts);
    }
    // Plusieurs blocs.
    else {
      for (int i=0; i<model_.getNumGeometries(); i++) {
        Integer ibloc=(Integer)_mdpts.getValue(idxAttNumBloc_, i);
        ArrayList<Integer> idxpts=hibloc2ListPts_.get(ibloc);
        if (idxpts==null) {
          idxpts=new ArrayList<Integer>();
          hibloc2ListPts_.put(ibloc, idxpts);
        }
        idxpts.add(i);
      }
    }
  }
  
  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if(xyToAdd==null){
      return this;
    }
    return new GISDataModelPointToMultiPointAdapter(model_.createTranslate(xyToAdd));
  }

  @Override
  public GISAttributeInterface getAttribute(final int _idxAtt) {
    return model_.getAttribute(_idxAtt);
  }

  @Override
  public void preload(final GISAttributeInterface[] _att, final ProgressionInterface _prog) {
    model_.preload(_att, _prog);
  }

  @Override
  public Envelope getEnvelopeInternal() {
    return model_.getEnvelopeInternal();
  }

  @Override
  public double getDoubleValue(final int _idxAtt, final int _idxGeom) {
    throw new IllegalAccessError("This method can't be accessed.");
  }

  @Override
  public Geometry getGeometry(final int _idxGeom) {
    if (multi_==null)
      multi_=new GISMultiPoint[getNumGeometries()];
    if (multi_[_idxGeom]==null) {
      ArrayList<Integer> idxpts=hibloc2ListPts_.get(_idxGeom);
      Point[] pts=new Point[idxpts.size()];
      for (int i=0; i<idxpts.size(); i++) {
        pts[i]=(Point)model_.getGeometry(idxpts.get(i));
      }
      multi_[_idxGeom]=(GISMultiPoint)GISGeometryFactory.INSTANCE.createMultiPoint(pts);
    }
    return multi_[_idxGeom];
  }

  @Override
  public int getIndiceOf(final GISAttributeInterface _att) {
    return model_.getIndiceOf(_att);
  }

  @Override
  public int getNbAttributes() {
    return model_.getNbAttributes();
  }

  @Override
  public int getNumGeometries() {
    return hibloc2ListPts_.size();
  }

  /**
   * Retourne un tableau de valeurs atomiques (Z par exemple).
   */
  @Override
  public Object getValue(final int _idxAtt, final int _idxGeom) {
    if (datas_[_idxAtt]==null)
      datas_[_idxAtt]=new Object[getNumGeometries()];
    if (datas_[_idxAtt][_idxGeom]==null) {
      ArrayList<Integer> idxpts=hibloc2ListPts_.get(_idxGeom);
      Object[] vals=new Object[idxpts.size()];
      for (int i=0; i<idxpts.size(); i++)
        vals[i]=model_.getValue(_idxAtt, idxpts.get(i));
      datas_[_idxAtt][_idxGeom]=getAttribute(_idxAtt).createDataForGeom(vals, vals.length);
    }
    return datas_[_idxAtt][_idxGeom];
  }
}
