/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2006-09-19 14:36:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.editor.CtuluValueEditorDefaults;
import org.fudaa.ctulu.editor.CtuluValueEditorI;

/**
 * @author Fred Deniger
 * @version $Id: GISAttributeString.java,v 1.9 2006-09-19 14:36:53 deniger Exp $
 */
public class GISAttributeString extends GISAttribute {

  public GISAttributeString() {
    super(CtuluValueEditorDefaults.STRING_EDITOR);
  }

  /**
   * @param _name le nom de l'attribut
   */
  public GISAttributeString(final String _name) {
    super(CtuluValueEditorDefaults.STRING_EDITOR, _name);
  }
  public GISAttributeString(final String _name,final boolean _atomic) {
    super(CtuluValueEditorDefaults.STRING_EDITOR, _name,_atomic);
  }
  public GISAttributeString(final CtuluValueEditorI _edit,final String _name) {
    super(_edit, _name);
  }
  public GISAttributeString(final CtuluValueEditorI _edit, final String _name, final boolean _atomic) {
    super(_edit, _name, _atomic);
  }

  @Override
  public final Class getDataClass(){
    return String.class;
  }

  @Override
  public Object getDefaultValue(){
    return CtuluLibString.EMPTY_STRING;
  }
  
  @Override
  protected Object createGlobalValues(final Object _initValues) {
    if (_initValues instanceof Object[]) { // Plusieurs valeurs en entr�e : On prend la premi�re.
      return ((Object[])_initValues)[0];
    }
    else {
      return super.createGlobalValues(_initValues);
    }
  }

  @Override
  public int getPrecision(){
    return 50;
  }
}
