/*
 *  @creation     2 sept. 2005
 *  @modification $Date: 2008-03-26 16:46:43 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.*;


/**
 * Une classe pour tester le type de la geometrie.
 * @author fred deniger
 * @version $Id: GISVisitorChooser.java,v 1.5.8.1 2008-03-26 16:46:43 bmarchan Exp $
 */
public class GISVisitorChooser implements GISVisitor {

  boolean isPt_;
  boolean isPolygone_;
  boolean isPolygoneWithHole_;
  boolean isPolyligne_;
  boolean isMultiPoint_;


  public final boolean isPolygone(){
    return isPolygone_;
  }
  
  


  @Override
  public boolean visitGeometryCollection(final GeometryCollection _p) {
    return false;
  }




  @Override
  public boolean visitMultiPoint(final MultiPoint _p) {
    clear();
    isMultiPoint_=true;
    return true;
  }




  @Override
  public boolean visitMultiPolygoneWithHole(final MultiPolygon _p) {
    return false;
  }




  @Override
  public boolean visitMultiPolyligne(final MultiLineString _p) {
    return false;
  }




  public final boolean isPolygoneWithHole(){
    return isPolygoneWithHole_;
  }


  public final boolean isPolyligne(){
    return isPolyligne_;
  }

  public final boolean isMultiPoint(){
    return isMultiPoint_;
  }

  public final boolean isPt(){
    return isPt_;
  }

  public void clear(){
    isPt_=false;
    isPolygone_=false;
    isPolygoneWithHole_=false;
    isPolyligne_=false;
    isMultiPoint_=false;
  }

  @Override
  public boolean visitPoint(final Point _p){
    clear();
    isPt_=true;
    return true;
  }

  @Override
  public boolean visitPolygone(final LinearRing _p){
    clear();
    isPolygone_=true;
    return true;
  }

  @Override
  public boolean visitPolygoneWithHole(final Polygon _p){
    clear();
    isPolygoneWithHole_=true;
    return true;
  }

  @Override
  public boolean visitPolyligne(final LineString _p){
    clear();
    isPolyligne_=true;
    return true;
  }

}
