/*
 * @creation 16 ao�t 06
 *
 * @modification $Date: 2006-09-19 14:36:53 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.*;

/**
 * Une Geometry de type bloc agregeant d'autres g�ometries simples. Le bloc n'accepte pas de contenir d'autres
 * collections.
 *
 * @author Bertrand Marchand
 * @version $Id: GISGeometryCollection.java,v 1.2 2006-09-19 14:36:53 deniger Exp $
 * @status Experimental
 */
public class GISGeometryCollection extends GeometryCollection implements GISCoordinateSequenceContainerInterface {
  CollectionCoordinateSequence seq_;

  private class CollectionCoordinateSequence implements CoordinateSequence {
    private int idxgeom_;
    private int idxsom_;
    private GISGeometryCollection coll_;

    public CollectionCoordinateSequence(GISGeometryCollection _coll) {
      coll_ = _coll;
    }

    @Override
    public Object clone() {
      return new CollectionCoordinateSequence(coll_);
    }

    @Override
    public CoordinateSequence copy() {
      return new CollectionCoordinateSequence(coll_);
    }

    @Override
    public Envelope expandEnvelope(Envelope _env) {
      _env.expandToInclude(coll_.getEnvelopeInternal());
      return _env;
    }

    @Override
    public Coordinate getCoordinate(int _index) {
      computeIndex2Geom(_index);
      return ((GISCoordinateSequenceContainerInterface) coll_.geometries[idxgeom_]).getCoordinateSequence().getCoordinate(idxsom_);
    }

    private void computeIndex2Geom(int _idx) {
      int idecal = 0;
      for (int i = 0; i < coll_.geometries.length; i++) {
        int nb = coll_.geometries[i].getNumPoints();
        if (_idx < idecal + nb) {
          idxgeom_ = i;
          idxsom_ = _idx - idecal;
          return;
        }
        idecal += nb;
      }
    }

    @Override
    public void getCoordinate(int _index, Coordinate _coord) {
      computeIndex2Geom(_index);
      ((GISCoordinateSequenceContainerInterface) coll_.geometries[idxgeom_]).getCoordinateSequence().getCoordinate(idxsom_, _coord);
    }

    @Override
    public Coordinate getCoordinateCopy(int _index) {
      return (Coordinate) getCoordinate(_index).clone();
    }

    @Override
    public int getDimension() {
      return coll_.getDimension();
    }

    @Override
    public double getOrdinate(int _index, int _ordinateIndex) {
      computeIndex2Geom(_index);
      return ((GISCoordinateSequenceContainerInterface) coll_.geometries[idxgeom_]).getCoordinateSequence().getOrdinate(idxsom_, _ordinateIndex);
    }

    @Override
    public double getX(int _index) {
      return getOrdinate(_index, 0);
    }

    @Override
    public double getY(int _index) {
      return getOrdinate(_index, 1);
    }

    @Override
    public void setOrdinate(int _index, int _ordinateIndex, double _value) {
      computeIndex2Geom(_index);
      ((GISCoordinateSequenceContainerInterface) coll_.geometries[idxgeom_]).getCoordinateSequence().setOrdinate(idxsom_, _ordinateIndex, _value);
    }

    @Override
    public int size() {
      return coll_.getNumPoints();
    }

    @Override
    public Coordinate[] toCoordinateArray() {
      return coll_.getCoordinates();
    }
  }

  public GISGeometryCollection(final Geometry[] _geometries, final GeometryFactory _factory) {
    super(_geometries, _factory);

    if (_geometries != null && containsCollection(_geometries)) {
      throw new IllegalArgumentException("Geometries can't be collections");
    }
  }

  @Override
  public GISGeometry createTranslateGeometry(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    Geometry[] translated = new Geometry[super.geometries.length];
    for (int i = 0; i < translated.length; i++) {
      translated[i] = (Geometry) ((GISGeometry) super.geometries[i]).createTranslateGeometry(xyToAdd);
    }
    return new GISGeometryCollection(translated, super.getFactory());
  }

  protected boolean containsCollection(Geometry[] _geoms) {
    for (Geometry g : _geoms) {
      if (g instanceof GISGeometryCollection) {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean accept(final GISVisitor _v) {
    return _v.visitGeometryCollection(this);
  }

  @Override
  public long getId() {
    return -1;
  }

  @Override
  public CoordinateSequence getCoordinateSequence() {
    if (seq_ == null) {
      seq_ = new CollectionCoordinateSequence(this);
    }
    return seq_;
  }
}
