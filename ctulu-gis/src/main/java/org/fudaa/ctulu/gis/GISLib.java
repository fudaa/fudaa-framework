/*
 * @creation 11 f�vr. 2004
 * @modification $Date: 2007-04-26 14:23:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import com.memoire.fu.FuLog;
import org.apache.commons.lang3.StringUtils;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.editor.CtuluValueEditorDefaults;
import org.geotools.feature.AttributeTypeBuilder;
import org.locationtech.jts.algorithm.CGAlgorithms;
import org.locationtech.jts.algorithm.locate.IndexedPointInAreaLocator;
import org.locationtech.jts.algorithm.locate.PointOnGeometryLocator;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.feature.type.AttributeType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author deniger
 */
public final class GISLib {
  /**
   * Un masque pour aucune g�ometrie
   */
  public static final int MASK_NONE = 0;
  /**
   * Un masque pour des polygones
   */
  public static final int MASK_POLYGONE = 1 << 0;
  /**
   * Un masque pour des polylignes
   */
  public static final int MASK_POLYLINE = 1 << 1;
  /**
   * Un masque pour des multipoints
   */
  public static final int MASK_MULTIPOINT = 1 << 2;
  /**
   * Un masque pour des points
   */
  public static final int MASK_POINT = 1 << 3;
  /**
   * Un masque pour toute geometrie
   */
  public static final int MASK_ALL = MASK_POLYGONE | MASK_POLYLINE | MASK_MULTIPOINT | MASK_POINT;

  public static double getDistance(final Envelope _e, final double _x, final double _y) {
    if (_e == null || _e.isNull()) {
      return Double.MAX_VALUE;
    }
    double r = CtuluLibGeometrie.distanceFromSegment(_e.getMinX(), _e.getMinY(), _e.getMaxX(), _e.getMinY(), _x, _y);
    double r1 = CtuluLibGeometrie.distanceFromSegment(_e.getMaxX(), _e.getMaxY(), _e.getMaxX(), _e.getMinY(), _x, _y);
    if (r1 < r) {
      r = r1;
    }
    r1 = CtuluLibGeometrie.distanceFromSegment(_e.getMaxX(), _e.getMaxY(), _e.getMinX(), _e.getMaxY(), _x, _y);
    if (r1 < r) {
      r = r1;
    }
    r1 = CtuluLibGeometrie.distanceFromSegment(_e.getMinX(), _e.getMinY(), _e.getMinX(), _e.getMaxY(), _x, _y);
    if (r1 < r) {
      r = r1;
    }
    return r;
  }

  /**
   * Raffine une sequence entre 2 index par ajouts de points interm�diaires entre 2 points. La m�thode utilis�e pour raffiner peut �tre :<p> 0 :
   * Suivant un nombre de points a ajouter entre 2 points cons�cutifs.<br> 1 : Suivant une distance maximale entre 2 points cons�cutifs.<br> 2 :
   * Suivant un nombre maximum total entre les 2 index (sans les 2 index)
   *
   * @param _seq La s�quence de coordonn�es.
   * @param _idxdeb L'indice de d�but de raffinement.
   * @param _idxfin L'indice de fin de raffinement.
   * @param _meth La m�thode de rafinement.
   * @param _nbpts Le nombre de points (entre 2 points - methode 0 - ou maxi - methode 2)
   * @param _dstmax La distance maximale.
   * @return La nouvelle sequence, ou la m�me si rien n'a �t� modifi�.
   */
  public static CoordinateSequence refine(final CoordinateSequence _seq, int _idxdeb, int _idxfin, int _meth, int _nbpts, double _dstmax) {
    if (_idxdeb >= _idxfin) {
      return _seq;
    }

    ArrayList<Coordinate> coords = new ArrayList<Coordinate>(_seq.size());

    for (int i = 0; i < _idxdeb; i++) {
      coords.add((Coordinate) _seq.getCoordinate(i).clone());
    }

    Coordinate[] cexts = new Coordinate[2];

    // Methode par nombre de points entre 2.
    if (_meth == 0) {
      for (int i = _idxdeb; i < _idxfin; i++) {
        cexts[0] = _seq.getCoordinate(i);
        cexts[1] = _seq.getCoordinate(i + 1);
        Coordinate[] cints = interpolateNCoordinates(cexts, _nbpts);
        coords.add((Coordinate) cexts[0].clone());
        for (int j = 0; j < cints.length; j++) {
          coords.add(cints[j]);
        }
      }
    } // Methode par distance maxi entre 2.
    else if (_meth == 1) {
      for (int i = _idxdeb; i < _idxfin; i++) {
        double dst = Math.sqrt((_seq.getX(i + 1) - _seq.getX(i)) * (_seq.getX(i + 1) - _seq.getX(i))
            + (_seq.getY(i + 1) - _seq.getY(i)) * (_seq.getY(i + 1) - _seq.getY(i)));
        int nbpts = (int) (dst / _dstmax);
        if (dst / _dstmax == (int) (dst / _dstmax)) {
          nbpts--;
        }
        cexts[0] = _seq.getCoordinate(i);
        cexts[1] = _seq.getCoordinate(i + 1);
        Coordinate[] cints = interpolateNCoordinates(cexts, nbpts);
        coords.add((Coordinate) cexts[0].clone());
        for (int j = 0; j < cints.length; j++) {
          coords.add(cints[j]);
        }
      }
    } // Methode par nombre total maxi
    else if (_meth == 2) {
      // Distances entre chaque point cons�cutifs
      int[] nbadd = new int[_idxfin - _idxdeb];
      for (int i = _idxdeb; i < _idxfin; i++) {
        nbadd[i - _idxdeb] = 0;
      }

      // Placement des points
      int nbpts = _nbpts - (_idxfin - _idxdeb - 1);
      while (nbpts > 0) {
        double dstmax = Double.NEGATIVE_INFINITY;
        int iadd = 0;
        for (int i = _idxdeb; i < _idxfin; i++) {
          double dst = Math.sqrt((_seq.getX(i + 1) - _seq.getX(i)) * (_seq.getX(i + 1) - _seq.getX(i))
              + (_seq.getY(i + 1) - _seq.getY(i)) * (_seq.getY(i + 1) - _seq.getY(i)));
          dst /= (nbadd[i - _idxdeb] + 1);
          if (dst > dstmax) {
            dstmax = dst;
            iadd = i - _idxdeb;
          }
        }
        nbadd[iadd]++;
        nbpts--;
      }
      for (int i = _idxdeb; i < _idxfin; i++) {
        cexts[0] = _seq.getCoordinate(i);
        cexts[1] = _seq.getCoordinate(i + 1);
        Coordinate[] cints = interpolateNCoordinates(cexts, nbadd[i - _idxdeb]);
        coords.add((Coordinate) cexts[0].clone());
        for (int j = 0; j < cints.length; j++) {
          coords.add(cints[j]);
        }
      }
    }

    for (int i = _idxfin; i < _seq.size(); i++) {
      coords.add((Coordinate) _seq.getCoordinate(i).clone());
    }

    return GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(coords.toArray(new Coordinate[0]));
  }

  /**
   * Retourne le tableau des coordonn�es de points r�partis uniform�ments entre 2 points.
   *
   * @param _coords Les coordonn�es des 2 points extr�mit�s
   * @param _nbpts Le nombre de points a calculer.
   * @return Les coordonn�es des points r�partis entre les 2 points extr�mit�s.
   */
  public static Coordinate[] interpolateNCoordinates(Coordinate[] _coords, int _nbpts) {
    Coordinate[] coords = new Coordinate[_nbpts];
    for (int i = 0; i < coords.length; i++) {
      Coordinate c = new Coordinate();
      c.x = _coords[0].x + (_coords[1].x - _coords[0].x) * (i + 1) / (_nbpts + 1.);
      c.y = _coords[0].y + (_coords[1].y - _coords[0].y) * (i + 1) / (_nbpts + 1.);
      c.z = _coords[0].z + (_coords[1].z - _coords[0].z) * (i + 1) / (_nbpts + 1.);
      coords[i] = c;
    }

    return coords;
  }

  /**
   * Retourne le tableau des coordonn�es de points r�partis uniform�ments entre 2 points.
   *
   * @param _coords Les coordonn�es des 2 points extr�mit�s
   * @param _dist La distance entre 2 points.
   * @return Les coordonn�es des points r�partis entre les 2 points extr�mit�s.
   */
  public static Coordinate[] interpolateCoordinates(Coordinate[] _coords, double _dist) {
    double distTot =
        Math.sqrt((_coords[1].x - _coords[0].x) * (_coords[1].x - _coords[0].x) + (_coords[1].y - _coords[0].y) * (_coords[1].y - _coords[0].y));
    int nbpts = (int) Math.round(distTot / _dist) - 1;

    Coordinate[] coords = new Coordinate[nbpts];
    for (int i = 0; i < coords.length; i++) {
      Coordinate c = new Coordinate();
      c.x = _coords[0].x + (_coords[1].x - _coords[0].x) * (i + 1) * _dist / distTot;
      c.y = _coords[0].y + (_coords[1].y - _coords[0].y) * (i + 1) * _dist / distTot;
      c.z = _coords[0].z + (_coords[1].z - _coords[0].z) * (i + 1) * _dist / distTot;
      coords[i] = c;
    }

    return coords;
  }

  /**
   * @param _s la ligne a tester
   * @param _other les lignes
   * @return true si _s intersecte une des lignes _other
   */
  public static boolean intesects(final LineString _s, final LineString[] _other) {
    if (_other == null || _s == null) {
      return false;
    }
    for (int i = _other.length - 1; i >= 0; i--) {
      if (_s.intersects(_other[i])) {
        return true;
      }
    }
    return false;
  }

  public static double[] interpolate(final LineString _s, final int _minIdx, final int _maxIdx, final double _minZ,
                                     final double _maxZ) {
    if (_minIdx > _maxIdx) {
      throw new IllegalArgumentException();
    }
    final double[] r = new double[_maxIdx - _minIdx + 1];
    r[0] = _minZ;
    r[r.length - 1] = _maxZ;
    if (r.length > 2) {
      // on doit calculer la taille de la polyligne entre minIdx et maxIdx
      double l = 0.0;
      final CoordinateSequence seq = _s.getCoordinateSequence();
      for (int i = _minIdx + 1; i <= _maxIdx; i++) {
        l += CtuluLibGeometrie.getDistance(seq.getX(i - 1), seq.getY(i - 1), seq.getX(i), seq.getY(i));
      }
      double di = 0;
      final double ecart = _maxZ - _minZ;
      for (int i = _minIdx + 1; i < _maxIdx; i++) {
        di += CtuluLibGeometrie.getDistance(seq.getX(i - 1), seq.getY(i - 1), seq.getX(i), seq.getY(i));
        r[i - _minIdx] = _minZ + ecart * di / l;
      }
    }
    return r;
  }

  public static boolean visitGeometry(final Geometry geometryCollection, final GISVisitor gisVisitor) {
    if (geometryCollection == null) {
      return false;
    }
    final int nb = geometryCollection.getNumGeometries();
    for (int i = 0; i < nb; i++) {
      if (!((GISGeometry) geometryCollection.getGeometryN(i)).accept(gisVisitor)) {
        return false;
      }
    }
    return true;
  }

  public static GISAttributeInterface[] getAttributeFrom(final GISDataModel _model) {
    if (_model == null) {
      return null;
    }
    final GISAttributeInterface[] res = new GISAttributeInterface[_model.getNbAttributes()];
    for (int i = res.length - 1; i >= 0; i--) {
      res[i] = _model.getAttribute(i);
    }
    return res;
  }

  /**
   * Test avec une precision de 1E-5.
   *
   * @param _pt le point a tester
   * @param _r la ligne fermee
   * @param _env l'enveloppe de la ligne fermee: si null on ne l'utilise pas
   * @param _tester le tester
   * @return true si compris dedanse
   */
  public static boolean isSelectedEnv(final Coordinate _pt, final LinearRing _r, final Envelope _env,
                                      final PointOnGeometryLocator _tester) {
    return isSelectedEnv(_pt, _r, _env, _tester, 1E-5);
  }

  public static boolean isSelectedEnv(final Coordinate _pt, final LinearRing _r, final Envelope _env,
                                      final PointOnGeometryLocator _tester, final double _prec) {
    if (_env == null) {
      return isSelected(_pt, _r, _tester, _prec);
    }
    return _env.contains(_pt) && isInside(_tester, _pt)
        || getDistanceFromFrontier(_r.getCoordinateSequence(), _pt) < _prec;
  }

  /**
   * @param _pt le point a tester
   * @param _r la ligne fermee
   * @param _tester le tester
   * @param _prec la precision pour g�rer les cas ou le point est tres pres de la ligne ferme mais n'est pas inclus dedans
   * @return true si compris dedanse
   */
  public static boolean isSelected(final Coordinate _pt, final LinearRing _r, final PointOnGeometryLocator _tester,
                                   final double _prec) {
    return isInside(_tester, _pt) || getDistanceFromFrontier(_r.getCoordinateSequence(), _pt) < _prec;
  }

  public static boolean isSelected(final Coordinate _pt, final LinearRing _r, final PointOnGeometryLocator _tester) {
    return isSelected(_pt, _r, _tester, 1E-5);
  }

  public static boolean isIn(final Coordinate _c, final List _env, final List _irTrees) {
    return isIn(_c, _env, _irTrees, 0);
  }

  /**
   * @param _c la cordonn�e a teste
   * @param _env la liste des enveloppe
   * @param _ptInRing la liste des PointInRing associ�es a la liste des enveloppe: meme taille donc
   * @param _firstIdx l'indice du debut pour les tests de _firstIdx a _env.size()-1
   * @return true si _c est comprise dans un polygon donne
   */
  public static boolean isIn(final Coordinate _c, final List _env, final List<? extends PointOnGeometryLocator> _ptInRing, final int _firstIdx) {
    if (_env == null || _ptInRing == null || _c == null) {
      return false;
    }
    final int nb = _env.size();
    for (int i = _firstIdx; i < nb; i++) {
      if (((Envelope) _env.get(i)).contains(_c) && isInside(_ptInRing.get(i), _c)) {
        return true;
      }
    }
    return false;
  }

  public static boolean isIn(final Coordinate _c, final Envelope[] _env, final PointOnGeometryLocator[] _ptInRing) {
    return isIn(_c, _env, _ptInRing, null, -1);
  }

  public static boolean isIn(final Coordinate _c, final Envelope[] _env, final PointOnGeometryLocator[] _ptInRing,
                             final GISPolygone[] _seq, final double _tolerance) {
    if (_env == null || _ptInRing == null || _c == null) {
      return false;
    }
    final boolean test = _tolerance > 0 && _seq != null;
    final int nb = _env.length;
    for (int i = 0; i < nb; i++) {
      if (_env[i].contains(_c) && isInside(_ptInRing[i], _c)) {
        return true;
      } else if (test && getDistanceFromFrontier(_seq[i].getCoordinateSequence(), _c) <= _tolerance) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param locator l'algo de recherche
   * @param c la coordonn�e
   * @return true si le point est � la frontiere ou dans la g�om�trie du locator.
   */
  public static boolean isInside(PointOnGeometryLocator locator, Coordinate c) {
    int res = locator.locate(c);
    return res == Location.BOUNDARY || res == Location.INTERIOR;
  }

  /**
   * Teste qu'une coordonn�e est a l'interieur d'un polygone.
   *
   * @param _c La coordonn�e
   * @param _poly Le polygone.
   * @param _ptInRing Le tester correspondant au polygone. A utiliser lorsque la m�thode est appel�e pour un grand nombre de points pour optimiser la
   *     vitesse de recherche. Peut �tre null.
   * @param _tolerance La tol�rance pour la recherche. Peut �tre = 0.
   * @return True : La coordonn�e _c est a l'interieur du polygone.
   */
  public static boolean isIn(final Coordinate _c, final GISPolygone _poly, PointOnGeometryLocator _ptInRing, double _tolerance) {
    Envelope env = _poly.getEnvelopeInternal();
    PointOnGeometryLocator pir = _ptInRing;
    if (pir == null) {
      pir = new IndexedPointInAreaLocator(_poly);
    }

    // La boite englobante && le pir contiennent le point...
    if (env.contains(_c) && isInside(pir, _c)) {
      return true;
    }
    // ... ou le point est a une distance inferieure a tol�rance du polygone.
    if (_tolerance > 0 && getDistanceFromFrontier(_poly.getCoordinateSequence(), _c) <= _tolerance) {
      return true;
    }

    return false;
  }

  public static int isInIdx(final Coordinate _c, final Envelope[] _env, final PointOnGeometryLocator[] _ptInRing) {
    if (_env == null || _ptInRing == null || _c == null) {
      return -1;
    }
    final int nb = _env.length;
    for (int i = 0; i < nb; i++) {
      if (_env[i].contains(_c) && isInside(_ptInRing[i], _c)) {
        return i;
      }
    }
    return -1;
  }

  public static int isInIdx(final CoordinateSequence _c, final Envelope[] _env, final PointOnGeometryLocator[] _ptInRing) {
    return isInIdx(_c, _env, _ptInRing, null);
  }

  /**
   * @param _c la sequence a tester
   * @param _env la liste des enveloppe
   * @param _ptInRing la liste des tester associ�es
   * @param _tmp coordonn�es temporaire permettant de stocker les coordonn�es de la sequence
   * @return le plus petit indice du couple enveloppe/PointInRing contenant une des coordonn�es
   */
  public static int isInIdx(final CoordinateSequence _c, final Envelope[] _env, final PointOnGeometryLocator[] _ptInRing,
                            final Coordinate _tmp) {
    if (_env == null || _ptInRing == null || _c == null) {
      return -1;
    }
    final Coordinate tmp = _tmp == null ? new Coordinate() : _tmp;
    final int nb = _c.size();
    for (int i = 0; i < nb; i++) {
      _c.getCoordinate(i, tmp);
      final int r = isInIdx(tmp, _env, _ptInRing);
      if (r >= 0) {
        return r;
      }
    }
    return -1;
  }

  /**
   * @param _seq la sequence de coordonn�es
   * @param _coord les coordonn�es du point a tester
   * @return la plus petite distance entre la ligne _seq et x,y
   */
  public static double getDistanceFromFrontier(final CoordinateSequence _seq, final Coordinate _coord) {
    return getDistanceFromFrontier(_seq, _coord, null, null);
  }

  public static double getDistanceFromFrontier(final CoordinateSequence[] _seq, final Coordinate _coord) {
    return getDistanceFromFrontier(_seq, _coord, null, null);
  }

  public static double getDistanceFromFrontier(final CoordinateSequence[] _seq, final Coordinate _coord,
                                               final Coordinate _c1, final Coordinate _c2) {
    double r = getDistanceFromFrontier(_seq[0], _coord, _c1, _c2);
    if (r == 0) {
      return r;
    }
    for (int i = _seq.length - 1; i > 0; i--) {
      final double t = getDistanceFromFrontier(_seq[i], _coord, _c1, _c2);
      if (t < r) {
        r = t;
        if (r == 0) {
          return r;
        }
      }
    }
    return r;
  }

  public static double getDistanceFromFrontier(final LineString[] _seq, final Coordinate _coord, final Coordinate _c1,
                                               final Coordinate _c2) {
    double r = getDistanceFromFrontier(_seq[0].getCoordinateSequence(), _coord, _c1, _c2);
    if (r == 0) {
      return r;
    }
    for (int i = _seq.length - 1; i > 0; i--) {
      final double t = getDistanceFromFrontier(_seq[i].getCoordinateSequence(), _coord, _c1, _c2);
      if (t < r) {
        r = t;
        if (r == 0) {
          return r;
        }
      }
    }
    return r;
  }

  public static Envelope[] getInternalEnvelop(final Geometry[] _a) {
    if (_a == null) {
      return null;
    }
    final Envelope[] r = new Envelope[_a.length];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = _a[i].getEnvelopeInternal();
    }
    return r;
  }

  public static PointOnGeometryLocator[] getTester(final LinearRing[] _a) {
    if (_a == null) {
      return null;
    }
    final PointOnGeometryLocator[] r = new PointOnGeometryLocator[_a.length];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = new IndexedPointInAreaLocator(_a[i]);
    }
    return r;
  }

  public static PointOnGeometryLocator createPolygoneTester(final LinearRing _a) {
    if (_a == null) {
      return null;
    }
    return new IndexedPointInAreaLocator(_a);
  }

  /**
   * @param _seq la sequence de coordonn�es
   * @param _coord les coordonn�es du point a tester
   * @param _tmp1 une coordonn�e temporaire peut etre nulle
   * @param _tmp2 une coordonn�e temporaire peut etre nulle
   * @return la plus petite distance entre la ligne _seq et x,y
   */
  public static double getDistanceFromFrontier(final CoordinateSequence _seq, final Coordinate _coord,
                                               final Coordinate _tmp1, final Coordinate _tmp2) {
    double minDistance = Double.MAX_VALUE;
    final Coordinate tmp1 = _tmp1 == null ? new Coordinate() : _tmp1;
    final Coordinate tmp2 = _tmp2 == null ? new Coordinate() : _tmp2;
    // brute force approach!
    for (int i = _seq.size() - 1; i > 0; i--) {
      _seq.getCoordinate(i, tmp1);
      _seq.getCoordinate(i - 1, tmp2);
      final double dist = CGAlgorithms.distancePointLine(_coord, tmp1, tmp2);
      if (dist < minDistance) {
        minDistance = dist;
      }
      if (minDistance <= 0.0) {
        return 0;
      }
    }
    return minDistance;
  }

  public static double getDistance(final double _x, final double _y, final Envelope _env) {
    double d = CtuluLibGeometrie.distanceFromSegment(_env.getMinX(), _env.getMinY(), _env.getMaxX(), _env.getMinY(),
        _x, _y);
    double d1 = CtuluLibGeometrie.distanceFromSegment(_env.getMinX(), _env.getMinY(), _env.getMinX(), _env.getMaxY(),
        _x, _y);
    if (d1 < d) {
      d = d1;
    }
    d1 = CtuluLibGeometrie.distanceFromSegment(_env.getMinX(), _env.getMaxY(), _env.getMaxX(), _env.getMaxY(), _x, _y);
    if (d1 < d) {
      d = d1;
    }
    d1 = CtuluLibGeometrie.distanceFromSegment(_env.getMaxX(), _env.getMinY(), _env.getMaxX(), _env.getMaxY(), _x, _y);
    if (d1 < d) {
      d = d1;
    }
    return d;
  }

  /**
   * Projection orthogonale d'un point sur un plan quelconque d�finit par 3 points.
   *
   * @param _plan Les points 3D du plan.
   * @param _pt Le point 3D � projeter.
   * @return LE point 3D sur le plan de projection.
   */
  public static GISPoint projectOnPlane(GISPoint[] _plan, GISPoint _pt) {
    // Teste si on a toutes les donn�es
    if (_plan == null || _pt == null || _plan.length < 3 || _plan[0] == null || _plan[1] == null || _plan[2] == null) {
      return null;
    }
    // On nomme : a, b, c les trois points d�finissant le plan
    double xa = _plan[0].getX();
    double ya = _plan[0].getY();
    double za = _plan[0].getZ();
    double xb = _plan[1].getX();
    double yb = _plan[1].getY();
    double zb = _plan[1].getZ();
    double xc = _plan[2].getX();
    double yc = _plan[2].getY();
    double zc = _plan[2].getZ();
    // Teste que les trois points d�finissant le plan sont diff�rents
    if ((xa == xb && ya == yb && za == zb) || (xb == xc && yb == yc && zb == zc) || (xa == xc && ya == yc && za == zc)) {
      return null;
    }
    // On nomme : i le point � projeter ; p le point projet�.
    double xi = _pt.getX();
    double yi = _pt.getY();
    double zi = _pt.getZ();
    double xp;
    double yp;
    double zp;
    // Calcul du vecteur ab nomm� v
    double xv = xb - xa;
    double yv = yb - ya;
    double zv = zb - za;
    // Calcul du vecteur ac nomm� u
    double xu = xc - xa;
    double yu = yc - ya;
    double zu = zc - za;
    // Calcul du vecteur w d�fini comme le produit vectoriel entre v et u
    double xw = yu * zv - zu * yv;
    double yw = zu * xv - xu * zv;
    double zw = xu * yv - yu * xv;
    // On calcul k qui est d�fini tel que : vecteur(ip) = k * w
    double k = ((xa - xi) * xw + (ya - yi) * yw + (za - zi) * zw) / (xw * xw + yw * yw + zw * zw);
    // Calcul des coordonn�es de p
    xp = xi + k * xw;
    yp = yi + k * yw;
    zp = zi + k * zw;

    return (GISPoint) GISGeometryFactory.INSTANCE.createPoint(xp, yp, zp);
  }

  public static Envelope computeEnveloppe(final GISDataModel[] _models, final ProgressionInterface _prog) {
    final ProgressionUpdater up = new ProgressionUpdater(_prog);
    final Envelope r = new Envelope();
    up.setValue(3, _models.length, 0, 60);
    for (int i = _models.length - 1; i >= 0; i--) {
      final Envelope ring = _models[i].getEnvelopeInternal();
      if (ring != null) {
        r.expandToInclude(ring);
        up.majAvancement();
      }
    }
    return r;
  }

  /**
   * @param _c1 La premi�re coordonn�e
   * @param _c2 La deuxi�me coordonn�e.
   * @return La coordonn�e milieu de 2 coordonn�es
   */
  public static Coordinate computeMiddle(Coordinate _c1, Coordinate _c2) {
    return new Coordinate((_c1.x + _c2.x) / 2., (_c1.y + _c2.y) / 2., (_c1.z + _c2.z) / 2.);
  }

  public static LineString createLineOrLinearFromList(final List<Coordinate> _coordinates) {
    if (_coordinates == null || _coordinates.size() < 2) {
      return null;
    }
    return createLineOrLinearStrings((Coordinate[]) _coordinates.toArray(new Coordinate[_coordinates.size()]));
  }

  public static LineString createLineOrLinearStrings(final Coordinate[] _cs) {
    if (_cs.length > 3 && _cs[0].distance(_cs[_cs.length - 1]) <= 1E-3) {
      _cs[0] = _cs[_cs.length - 1];
      return GISGeometryFactory.INSTANCE.createLinearRing(_cs);
    }
    return GISGeometryFactory.INSTANCE.createLineString(_cs);
  }

  public static void preloadAttributes(GISDataModel dataModel) {
    if (dataModel.getNbAttributes() > 0) {
      dataModel.preload(GISLib.getAttributeFrom(dataModel), null);
    }
  }

  public static Envelope computeEnveloppe(final Geometry[] _models) {
    return computeEnveloppe(Arrays.asList(_models));
  }

  public static Envelope computeEnveloppe(final List<Geometry> _models) {
    final Envelope r = new Envelope();
    for (Geometry model : _models) {
      r.expandToInclude(model.getEnvelopeInternal());
    }
    return r;
  }

  /**
   * @param _seq la sequence des coordonnees
   * @return la moyenne sur tous les z de la sequence. 0 si null ou vide
   */
  public static double getMoyZ(final CoordinateSequence _seq) {
    return getMoyOrdinate(_seq, 2);
  }

  public static double getMoyY(final CoordinateSequence _seq) {
    return getMoyOrdinate(_seq, 1);
  }

  public static double getMoyX(final CoordinateSequence _seq) {
    return getMoyOrdinate(_seq, 0);
  }

  public static Object[] getValues(final GISAttributeModel _model) {
    if (_model == null) {
      return null;
    }
    final Object[] r = new Object[_model.getSize()];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = _model.getObjectValueAt(i);
    }
    return r;
  }

  /**
   * @param _seq la sequence des coordonnees
   * @param _ordinate le num de l'ordonn�e 0=X; 1=Y; 2=Z
   * @return la moyenne sur tous les points de la sequence. 0 si null ou vide
   */
  public static double getMoyOrdinate(final CoordinateSequence _seq, final int _ordinate) {
    double r = 0;
    if (_seq != null) {
      final int nb = _seq.size();
      for (int i = nb - 1; i >= 0; i--) {
        r += _seq.getOrdinate(i, _ordinate);
      }
      if (nb > 0) {
        r /= nb;
      }
    }
    return r;
  }

  public static GISAttributeInterface createAttributeFrom(String name, Class<?> clazz) {
    return createCommonAttributeFrom(name, clazz, true);
  }

  public static GISAttributeInterface createCommonAttributeFrom(String name, Class<?> clazz, boolean useNameToFindKnownAtt) {
    boolean isAtomic = false;

    // Gestion des attributs atomiques.
    if (name.startsWith("[I]")) {
      clazz = Integer.class;
      name = name.substring(3);
      isAtomic = true;
    } else if (name.startsWith("[S]")) {
      clazz = String.class;
      name = name.substring(3);
      isAtomic = true;
    } else if (name.startsWith("[B]")) {
      clazz = Boolean.class;
      name = name.substring(3);
      isAtomic = true;
    } else if (name.startsWith("[D]")) {
      clazz = Double.class;
      name = name.substring(3);
      isAtomic = true;
    }

    if (useNameToFindKnownAtt) {
      GISAttributeInterface known = GISAttributeConstants.getConstantAttributeFromName(name);
      if (known != null && known.getDataClass().equals(clazz)) {
        return known;
      }
    }
    GISAttributeInterface known = GISAttributeConstants.getConstantAttribute(name);
    if (known != null && known.getDataClass().equals(clazz)) {
      return known;
    }

    final GISAttributeInterface attributeFrom = createAttributeFrom(name, clazz, isAtomic);
    if (attributeFrom instanceof GISAttribute) {
      GISAttribute init = (GISAttribute) attributeFrom;
      if (init.getInitialID() == null) {
        init.setID(StringUtils.upperCase(init.getID()));
      }
    }
    return attributeFrom;
  }

  private static class CountClassGeom implements GeometryFilter {
    Class c_;
    int i_;

    CountClassGeom(final Class _c) {
      c_ = _c;
    }

    @Override
    public void filter(final Geometry _geom) {
      if (c_.isInstance(_geom)) {
        i_++;
      }
    }
  }

  private GISLib() {
  }

  public static int getNbGeomOf(final Class _c, final Geometry _g) {
    final CountClassGeom counter = new CountClassGeom(_c);
    _g.apply(counter);
    return counter.i_;
  }

  public static int getNumPoint(final GISZoneCollectionPoint[] _pt) {
    int r = 0;
    if (_pt != null) {
      for (int i = _pt.length - 1; i >= 0; i--) {
        r += _pt[i].getNumPoints();
      }
    }
    return r;
  }

  public static int getNbGeometries(final GISDataModel[] _pt) {
    int r = 0;
    if (_pt != null) {
      for (int i = _pt.length - 1; i >= 0; i--) {
        r += _pt[i].getNumGeometries();
      }
    }
    return r;
  }

  /**
   * Calcul la distance entre un segment et un point X (x,y).
   *
   * @param _seg1 le point 1 du segment
   * @param _seg2 le point 2 du segment
   * @param _x le x du point X
   * @param _y le y du point X
   * @return la distance entre le segment et le point X.
   */
  public static double distanceXYFromSeg(final GISPoint _seg1, final GISPoint _seg2, final double _x, final double _y) {
    return CtuluLibGeometrie.distanceFromSegment(_seg1.getX(), _seg1.getY(), _seg2.getX(), _seg2.getY(), _x, _y);
  }

  /**
   * Calcul l'aire d'un triangle p1,p2,p3.
   *
   * @param _p1 pt 1 du triange
   * @param _p2 pt 2 du triange
   * @param _p3 pt 3 du triange
   * @return l'aire du triangle p1,p2,p3
   */
  public static double getAire(final GISPoint _p1, final GISPoint _p2, final GISPoint _p3) {
    return CtuluLibGeometrie.aireTriangle(_p1.getX(), _p1.getY(), _p2.getX(), _p2.getY(), _p3.getX(), _p3.getY());
  }

  public static double getAire(final Envelope _e) {
    if (_e == null || _e.isNull()) {
      return 0;
    }
    return (_e.getMaxX() - _e.getMinX()) * (_e.getMaxY() - _e.getMinY());
  }

  public static boolean isNiveau(final CoordinateSequence _s) {
    return isNiveau(_s, 1E-6);
  }

  public static boolean isNiveau(final CoordinateSequence _s, final double _eps) {
    if (_s == null || _s.size() == 0) {
      return true;
    }
    final double r = _s.getOrdinate(0, CoordinateSequence.Z);
    for (int i = _s.size() - 1; i > 0; i--) {
      final double t = Math.abs(_s.getOrdinate(i, CoordinateSequence.Z) - r);
      if (t > _eps) {
        return false;
      }
    }
    return true;
  }

  /**
   * Cr�e un attribut a partir d'un {@link AttributeType}, utilis� dans les DataStore.
   *
   * @param _type L'attribute type.
   * @return L'attribut.
   * @see {@link #createAttributeFrom(GISAttributeInterface, boolean)}
   */
  public static GISAttributeInterface createAttributeFrom(final AttributeDescriptor _type) {
    String name = _type.getName().getLocalPart();
    Class<?> clazz = _type.getType().getBinding();

    return createAttributeFrom(name, clazz);
  }

  /**
   * Cr�e un {@link AttributeType}, utilis� dans les DataStore a partir d'un attribut.<p> Les AttributeType n'autorisant pas les tableaux, les
   * attributs atomiques sont g�r�s de la mani�re suivante:<br> <ul> <li>Le nom de l'AttributeType (getName()) indique l'atomicit�, on y adjoint le
   * type du tableau ([I],[S],[B],[D]). Exemple : [I]Indices</li> <li>Le type de l'AttributType (getType()) est toujours String.class</li> <li>La
   * valeur de l'attribut sera une chaine contenant les valeurs, s�par�es par des virgules.</li> </ul>
   *
   * @param _att L'attribut.
   * @param _useIdAsName true if we want to use the id as the attribute name
   * @return L'attribute type.
   */
  public static AttributeDescriptor createAttributeFrom(final GISAttributeInterface _att, boolean _useIdAsName) {
    AttributeTypeBuilder attributeTypeBuilder = new AttributeTypeBuilder();
    // Pour les attributs atomique, le type d'attribut est syst�matique String, le type de tableau est mis dans le nom!!
    if (_att.isAtomicValue()) {
      String tbType = "";
      if (_att.getDataClass().equals(String.class)) {
        tbType = "[S]";
      } else if (_att.getDataClass().equals(Integer.class)) {
        tbType = "[I]";
      } else if (_att.getDataClass().equals(Double.class)) {
        tbType = "[D]";
      } else if (_att.getDataClass().equals(Boolean.class)) {
        tbType = "[B]";
      }
      return attributeTypeBuilder.binding(String.class).buildDescriptor(tbType + (_useIdAsName ? _att.getID() : _att.getName()));
    } // 18 pour les shapefiles ...;
    else {
      return attributeTypeBuilder.binding(_att
          .getDataClass()).buildDescriptor(_useIdAsName ? _att.getID() : _att.getName());
    }
  }

  public static GISAttributeInterface createAttributeFrom(final String _name, final Class _c, final boolean _isAtomic) {
    if (Number.class.isAssignableFrom(_c)) {
      if (Integer.class.isAssignableFrom(_c)) {
        return new GISAttributeInteger(_name, _isAtomic);
      }

      final GISAttributeDouble res = new GISAttributeDouble(_name, _isAtomic);
      if (GISAttributeConstants.BATHY.isSameContent(res)) {
        return GISAttributeConstants.BATHY;
      }
      return res;
    }
    return createNonNumberAttr(_name, _c, _isAtomic);
  }

  private static GISAttributeInterface createNonNumberAttr(final String _name, final Class _c, final boolean _isAtomic) {
    if (Boolean.class.isAssignableFrom(_c)) {
      return new GISAttributeBoolean(_name, _isAtomic);
    } else if (String.class.isAssignableFrom(_c)) {
      if (GISAttributeConstants.TITRE.getID().equals(_name)) {
        return GISAttributeConstants.TITRE;
      } else if (GISAttributeConstants.ETAT_GEOM.getID().equals(_name)) {
        return GISAttributeConstants.ETAT_GEOM;
      }
      return new GISAttributeString(_name, _isAtomic);
    } else if (!Geometry.class.isAssignableFrom(_c)) {
      return new GISAttribute(CtuluValueEditorDefaults.STRING_EDITOR, _name, _isAtomic);
    }
    return null;
  }

  /**
   * Converti la g�om�trie en GISMultiPoint.
   *
   * @return le GISMultiPoint correspondant. Null si il n'y a pas assez de point pour la construction.
   */
  public static GISMultiPoint toMultipoint(CoordinateSequence coord) {
    CoordinateSequence seq = coord;
    if (seq == null) {
      throw new IllegalArgumentException("coord doit �tre non null.");
    }
    if (seq.size() == 0) {
      FuLog.trace("toPolygone : Nombre de point minimum non respect�.");
      return null;
    }
    return GISGeometryFactory.INSTANCE.createMultiPoint(seq);
  }

  /**
   * Converti la g�om�trie en GISPolyligne.
   *
   * @return le GISPolyligne correspondant. Null si il n'y a pas assez de point pour la construction.
   */
  public static GISPolyligne toPolyligne(CoordinateSequence coord) {
    CoordinateSequence seq = coord;
    if (seq == null) {
      throw new IllegalArgumentException("coord doit �tre non null.");
    }
    if (seq.size() == 1) {
      FuLog.trace("toPolygone : Nombre de point minimum non respect�.");
      return null;
    }
    boolean ferme = coord.getCoordinate(0).equals(seq.getCoordinate(coord.size() - 1));
    if (ferme && seq.size() == 2) {
      FuLog.trace("toPolygone : Nombre de point minimum non respect�.");
      return null;
    }
    if (ferme) {
      seq = new CoordinateArraySequence(seq.size() - 1);
      // recopie
      for (int i = 0; i < coord.size() - 1; i++) {
        for (int j = 0; j < 3; j++) {
          seq.setOrdinate(i, j, coord.getOrdinate(i, j));
        }
      }
    }
    return GISGeometryFactory.INSTANCE.createLineString(seq);
  }

  /**
   * Converti la g�om�trie en GISPolygone.
   *
   * @return le GISPolygone correspondant. Null si il n'y a pas assez de point pour la construction.
   */
  public static GISPolygone toPolygone(CoordinateSequence coord) {
    CoordinateSequence seq = coord;
    if (seq == null) {
      throw new IllegalArgumentException("coord doit �tre non null.");
    }
    if (seq.size() <= 2) {
      FuLog.trace("toPolygone : Nombre de point minimum non respect�.");
      return null;
    }
    boolean ferme = coord.getCoordinate(0).equals(seq.getCoordinate(coord.size() - 1));
    if (ferme && seq.size() == 3) {
      FuLog.trace("toPolygone : Nombre de point minimum non respect�.");
      return null;
    }
    if (!ferme) {
      seq = new CoordinateArraySequence(seq.size() + 1);
      // recopie
      for (int i = 0; i < coord.size(); i++) {
        for (int j = 0; j < 3; j++) {
          seq.setOrdinate(i, j, coord.getOrdinate(i, j));
        }
      }
      // Fermeture
      for (int i = 0; i < 3; i++) {
        seq.setOrdinate(seq.size() - 1, i, seq.getOrdinate(0, i));
      }
    }
    return GISGeometryFactory.INSTANCE.createLinearRing(seq);
  }

  /**
   * Retourne la valeur pour une propri�t� hydraulique de type double.
   *
   * @param _comm Commentaire hydro sous la forme "&lt;prop1&gt;=&lt;val&gt;; &lt;prop2&gt;=&lt;val&gt;"
   * @param _prop La propri�t� a extraire.
   * @return La valeur de la propri�t�.
   */
  static public double getHydroCommentDouble(String _comm, String _prop) {
    double r = 0;

    if (_comm != null && !_comm.trim().equals("")) {
      for (String cpl : _comm.split(";")) {
        String[] nameval = cpl.split("=");
        if (nameval.length == 2 && nameval[0].equalsIgnoreCase(_prop)) {
          try {
            r = Double.parseDouble(nameval[1]);
            break;
          } // En cas d'erreur, retourne 0.
          catch (NumberFormatException _exc) {
          }
        }
      }
    }
    return r;
  }

  /**
   * Retourne vrai si la propri�t� est valu�e.
   */
  static public boolean isHydroCommentValued(final String _comm, String _prop) {
    if (_comm != null && !_comm.trim().equals("")) {
      for (String cpl : _comm.split(";")) {
        String[] nameval = cpl.split("=");
        if (nameval.length == 2 && nameval[0].equalsIgnoreCase(_prop)) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   * Retourne une nouvelle instance de commentaire ayant pour nouvelle valeur de '_prop' '_value'.
   *
   * @param _comm Commentaire hydro sous la forme "&lt;prop1&gt;=&lt;val&gt;; &lt;prop2&gt;=&lt;val&gt;"
   * @param _prop La propri�t�.
   * @param _value la nouvelle valeur pour prop.
   * @return Nouvelle instance du commentaire.
   */
  static public String setHydroCommentDouble(final String _comm, double _value, String _prop) {
    StringBuilder str = new StringBuilder();
    boolean found = false;
    if (_comm != null && !_comm.trim().equals("")) {
      for (String cpl : _comm.split(";")) {
        String[] nameval = cpl.split("=");
        if (nameval.length == 2 && nameval[0].equalsIgnoreCase(_prop)) {
          found = true;
          str.append(nameval[0] + "=" + Double.toString(_value));
        } else {
          str.append(cpl);
        }
      }
    }
    if (!found) {
      str.append(_prop + "=" + _value);
    }
    return str.toString();
  }

  /**
   * Ajoute une op�ration dans l'attribut l'historique
   *
   * @param _ope L'op�ration
   * @param _mes Le message li�
   * @param _history L'historique
   * @return L'historique r�sultant.
   */
  static public String appendHistory(String _ope, String _mes, String _history) {
    return _ope + "(" + _mes + ")" + (_history.trim().length() == 0 ? "" : "; " + _history.trim());
  }
}
