/*
 *  @creation     18 mars 2005
 *  @modification $Date: 2006-09-19 14:36:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Point;

/**
 * Une sequence de coordonn�es bas�e sur des sequences unique.<p>
 * Ceci permet notamment de modifier les coordonn�es d'un {@link GISMultiPoint} qui s'appuie sur les <code>Point</code>.
 * La modification des coordonn�es d'un {@link Point} ne ne peut se faire qu'au travers de sa {@link CoordinateSequence},
 * la Coordinate n'�tant qu'une copie de son impl�mentation interne.
 *
 * @author Bertrand Marchand
 * @version $Id: GISCoordinateSequenceUnique.java,v 1.9 2006-09-19 14:36:53 deniger Exp $
 */
public class GISSequencesCoordinateSequence implements CoordinateSequence {
  CoordinateSequence[] seqs_;

  /**
   * Construction a partir de sequences. Les sequences doivent �tre des {@link GISCoordinateSequenceUnique} (non control�).
   *
   * @param _seqs Les s�quences.
   */
  public GISSequencesCoordinateSequence(CoordinateSequence[] _seqs) {
    seqs_ = _seqs;
  }

  /**
   * Construction a partir de points.
   *
   * @param _pts Les points.
   */
  public GISSequencesCoordinateSequence(Point[] _pts) {
    seqs_ = new CoordinateSequence[_pts.length];
    for (int i = 0; i < _pts.length; i++) {
      seqs_[i] = _pts[i].getCoordinateSequence();
    }
  }

  @Override
  public int getDimension() {
    return 3;
  }

  @Override
  public Object clone() {
    return new GISSequencesCoordinateSequence(seqs_);
  }

  @Override
  public CoordinateSequence copy() {
    return new GISSequencesCoordinateSequence(seqs_);
  }

  @Override
  public Envelope expandEnvelope(final Envelope _env) {
    for (int i = 0; i < seqs_.length; i++) {
      _env.expandToInclude(seqs_[i].getX(0), seqs_[i].getY(0));
    }
    return _env;
  }

  @Override
  public void getCoordinate(final int _index, final Coordinate _coord) {
    _coord.x = seqs_[_index].getOrdinate(0, 0);
    _coord.y = seqs_[_index].getOrdinate(0, 1);
    _coord.z = seqs_[_index].getOrdinate(0, 2);
  }

  @Override
  public Coordinate getCoordinate(final int _index) {
    return seqs_[_index].getCoordinate(0);
  }

  @Override
  public Coordinate getCoordinateCopy(final int _index) {
    return (Coordinate) getCoordinate(_index).clone();
  }

  @Override
  public double getOrdinate(final int _index, final int _ordinateIndex) {
    switch (_ordinateIndex) {
      case CoordinateSequence.X:
      case CoordinateSequence.Y:
      case CoordinateSequence.Z:
        return seqs_[_index].getOrdinate(0, _ordinateIndex);
      default:
        throw new IllegalArgumentException("bad ordinate");
    }
  }

  @Override
  public double getX(final int _index) {
    return seqs_[_index].getOrdinate(0, 0);
  }

  @Override
  public double getY(final int _index) {
    return seqs_[_index].getOrdinate(0, 1);
  }

  @Override
  public void setOrdinate(final int _index, final int _ordinateIndex, final double _value) {
    switch (_ordinateIndex) {
      case CoordinateSequence.X:
      case CoordinateSequence.Y:
      case CoordinateSequence.Z:
        seqs_[_index].setOrdinate(0, _ordinateIndex, _value);
        break;
      default:
        throw new IllegalArgumentException("bad ordinate");
    }
  }

  @Override
  public int size() {
    return seqs_.length;
  }

  @Override
  public Coordinate[] toCoordinateArray() {
    Coordinate[] coords = new Coordinate[seqs_.length];
    for (int i = 0; i < coords.length; i++) {
      coords[i] = seqs_[i].getCoordinate(0);
    }
    return coords;
  }
}
