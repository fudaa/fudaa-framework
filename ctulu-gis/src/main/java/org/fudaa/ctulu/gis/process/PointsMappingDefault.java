/*
 GPL 2
 */
package org.fudaa.ctulu.gis.process;

import org.fudaa.ctulu.interpolation.SupportLocationI;

/**
 * Le r�sultat d'un filtre de points.
 *
 * @author Frederic Deniger
 */
public class PointsMappingDefault implements PointsMapping {

  private int[] mapping;
  SupportLocationI init;

  public PointsMappingDefault(int[] correspondance, SupportLocationI init) {
    this.mapping = correspondance;
    this.init = init;
  }

  @Override
  public int getPtsNb() {
    return mapping.length;
  }

  @Override
  public int[] getMapping() {
    return mapping;
  }

  @Override
  public int getInitialIdx(int _i) {
    return mapping[_i];
  }

  @Override
  public double getPtX(int _i) {
    return init.getPtX(mapping[_i]);
  }

  @Override
  public double getPtY(int _i) {
    return init.getPtY(mapping[_i]);
  }
}
