/*
 * @creation 12 juil. 06
 * @modification $Date: 2007-01-17 10:45:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis.exporter;

import com.memoire.bu.BuFileFilter;
import org.geotools.data.DataStore;
import org.geotools.data.FileDataStoreFactorySpi;
import org.locationtech.jts.geom.Envelope;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * A utiliser lorsqu'on veut exporter des donn�es dans un format SIG.
 *
 * @author fred deniger
 * @version $Id: GISExportDataStoreFactory.java,v 1.1 2007-01-17 10:45:27 deniger Exp $
 */
public final class GISExportDataStoreFactory {
  private GISExportDataStoreFactory() {
  }

  /**
   * Construit les filtres par default.
   *
   * @return table FileFilter->FileDataStoreFactorySpi
   */
  public static Map<BuFileFilter, GISFileFormat> buildFileFilterMap() {
    final HashMap<BuFileFilter, GISFileFormat> fileFilterDataStore = new HashMap<>();
    for (GISFileFormat ft : GISFileFormat.values()) {
      fileFilterDataStore.put(ft.createFileFilter(), ft);
    }
    return fileFilterDataStore;
  }

  /**
   * Construit les filtres par default.
   *
   * @return table FileFilter->FileDataStoreFactorySpi
   */
  public static Map<BuFileFilter, GISFileFormat> buildFileFilterMapExcludeGML() {
    final HashMap<BuFileFilter, GISFileFormat> fileFilterDataStore = new HashMap<>();
    for (GISFileFormat ft : GISFileFormat.values()) {
      if (!ft.isGML()) {
        fileFilterDataStore.put(ft.createFileFilter(), ft);
      }
    }
    return fileFilterDataStore;
  }

  /**
   * Construit et retoune le filtre correspondant au DataStore.
   *
   * @param _store Le data store.
   */
  public static BuFileFilter buildFileFilterFor(GISFileFormat _store) {
    final BuFileFilter ft = _store.createFileFilter();
    // les extensions sont deja mise par les data store factories
    return ft;
  }

  /**
   * Permet de creer un DataStore en specifier les limites (utilise par MapInfo).
   *
   * @param _fac l'usine a utiliser
   * @param _url le fichier de dest
   * @param _bounds l'enveloppe des donnees. Utiliser par la sortie MID/MIF
   * @return le DataStore correctement initialise
   */
  public static DataStore createDataStore(final FileDataStoreFactorySpi _fac, final URL _url, final Envelope _bounds,
                                          final boolean _write) throws IOException {
    if (_write) {
      final File f = new File(_url.getFile());
      if (f.exists()) {
        f.delete();
      }
    }
    return _fac.createDataStore(_url);
  }
}
