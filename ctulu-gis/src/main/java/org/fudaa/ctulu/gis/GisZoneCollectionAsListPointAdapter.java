/*
 GPL 2
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Coordinate;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.interpolation.SupportLocationI;

/**
 * Adapte une collection en un fournisseur de points.
 *
 * @author Frederic Deniger
 */
public class GisZoneCollectionAsListPointAdapter implements SupportLocationI {

  private final GISZoneCollection points;
  private final int[] nbPointsBySemis;
  private final int nbPts;

  public GisZoneCollectionAsListPointAdapter(GISZoneCollection points) {
    this.points = points;
    nbPointsBySemis = new int[points.getNbGeometries()];
    for (int i = 0; i < nbPointsBySemis.length; i++) {
      nbPointsBySemis[i] = points.getGeometry(i).getNumPoints();
    }
    nbPts = CtuluLibArray.getSum(nbPointsBySemis);
  }

  @Override
  public int getPtsNb() {
    return nbPts;
  }

  public double getValue(int idxPt, int idxAttribute) {
    int min = 0;
    for (int i = 0; i < nbPointsBySemis.length; i++) {
      if (idxPt < min + nbPointsBySemis[i]) {
        GISAttributeModelDoubleInterface values = (GISAttributeModelDoubleInterface) points.getDataModel(idxAttribute).getObjectValueAt(i);
        return values.getValue(idxPt - min);
      }
      min = min + nbPointsBySemis[i];
    }
    return 0;
  }

  public Coordinate getCoordinate(int idxPt) {
    int min = 0;
    for (int i = 0; i < nbPointsBySemis.length; i++) {
      if (idxPt < min + nbPointsBySemis[i]) {
        return points.getGeometry(i).getGeometryN(idxPt - min).getCoordinate();
      }
      min = min + nbPointsBySemis[i];
    }
    return null;
  }

  /**
   * 
   * @param idxSemis l'indice de la g�ometrie
   * @param idxPtInSemis l'indice du point dans la g�ometrie
   * @return  permet de connaitre l'indice dans ce r�f�rentiel.
   */
  public int getIdx(int idxSemis, int idxPtInSemis) {
    int sum = 0;
    int nb = Math.min(nbPointsBySemis.length, idxSemis);
    for (int i = 0; i < nb; i++) {
      sum = sum + nbPointsBySemis[i];
    }
    return sum + idxPtInSemis;
  }

  @Override
  public double getPtX(int idxPt) {
    return getCoordinate(idxPt).x;
  }

  @Override
  public double getPtY(int idxPt) {
    return getCoordinate(idxPt).y;
  }
}
