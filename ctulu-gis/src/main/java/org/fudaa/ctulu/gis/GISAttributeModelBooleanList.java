/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2007-05-04 13:43:25 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import gnu.trove.TIntIntHashMap;
import gnu.trove.TIntIntIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.fudaa.ctulu.collection.CtuluListBoolean;

/**
 * @author Fred Deniger
 * @version $Id: GISAttributeModelBooleanList.java,v 1.13 2007-05-04 13:43:25 deniger Exp $
 */
public class GISAttributeModelBooleanList extends CtuluListBoolean implements GISAttributeModelObservable,
    GISAttributeModelBooleanInterface {

  GISAttributeInterface attribute_;

  protected transient GISAttributeListener listener_;

  private GISAttributeModelBooleanList(final List _l, final GISAttributeModelBooleanList _model) {
    super(0);
    list_ = _l;
    attribute_ = _model.getAttribute();
    listener_ = _model.getListener();
  }

  protected GISAttributeModelBooleanList() {
    super(0);
  }

  /**
   * @param _nb le nombre de valeur
   * @param _attr l'attribut
   */
  public GISAttributeModelBooleanList(final int _nb, final GISAttributeInterface _attr) {
    super(_nb);
    attribute_ = _attr;
  }

  @Override
  protected void fireObjectChanged(int _indexGeom, Object _newValue){
    if (listener_ != null) {
      listener_.gisDataChanged(attribute_, _indexGeom, _newValue);
    }
  }

  protected final GISAttributeListener getListener() {
    return listener_;
  }

  @Override
  public GISAttributeModel createSubModel(final int[] _idxToRemove) {
    final ArrayList newList = new ArrayList(getSize());
    final int nb = getSize();
    for (int i = 0; i < nb; i++) {
      if (Arrays.binarySearch(_idxToRemove, i) < 0) {
        newList.add(list_.get(i));
      }
    }
    return new GISAttributeModelBooleanList(newList, this);
  }

  @Override
  public GISAttributeModel createUpperModel(final int _numObject, final TIntIntHashMap _newIdxOldIdx) {
    if (_numObject < getSize()) {
      throw new IllegalArgumentException("bad num objects");
    }
    if (_numObject == getSize()) {
      return this;
    }
    final ArrayList newList = new ArrayList(_numObject);
    Collections.fill(newList, getAttribute().getDefaultValue());
    if (_newIdxOldIdx != null) {
      final TIntIntIterator it = _newIdxOldIdx.iterator();
      for (int i = _newIdxOldIdx.size(); i-- > 0;) {
        it.advance();
        final int newIdx = it.key();
        newList.set(newIdx, list_.get(it.value()));
      }
    }
    return new GISAttributeModelBooleanList(newList, this);
  }

  @Override
  public GISAttributeModel deriveNewModel(final int _numObject, final GISReprojectInterpolateurI _interpol) {
    if (!(_interpol instanceof GISReprojectInterpolateurI.BooleanTarget)) {
      throw new IllegalArgumentException("bad interface");
    }
    return deriveNewModel(_numObject, (GISReprojectInterpolateurI.BooleanTarget) _interpol);
  }

  @Override
  public GISAttributeModelBooleanInterface deriveNewModel(final int _numObject,
      final GISReprojectInterpolateurI.BooleanTarget _interpol) {
    final ArrayList newList = new ArrayList(_numObject);
    for (int i = 0; i < _numObject; i++) {
      newList.add(Boolean.valueOf(_interpol.interpol(i)));
    }
    return new GISAttributeModelBooleanList(newList, this);
  }

  /**
   * @return l'attribut associe
   */
  @Override
  public final GISAttributeInterface getAttribute() {
    return attribute_;
  }

  @Override
  public Object getAverage() {
    return getSize() > 0 ? getObjectValueAt(0) : null;
  }

  @Override
  public final void setListener(final GISAttributeListener _listener) {
    listener_ = _listener;
  }

}
