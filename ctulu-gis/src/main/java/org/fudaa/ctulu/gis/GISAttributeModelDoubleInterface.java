/*
 *  @creation     8 avr. 2005
 *  @modification $Date: 2007-01-10 08:58:47 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.fudaa.ctulu.collection.CtuluCollectionDoubleEdit;

/**
 * @author Fred Deniger
 * @version $Id: GISAttributeModelDoubleInterface.java,v 1.6 2007-01-10 08:58:47 deniger Exp $
 */
public interface GISAttributeModelDoubleInterface extends GISAttributeModel, CtuluCollectionDoubleEdit{


  /**
   * @param _numObject le nombre d'objet
   * @param _interpol l'interpolateur
   * @return le modele derive
   */
  GISAttributeModelDoubleInterface deriveNewModel(int _numObject,GISReprojectInterpolateurI.DoubleTarget _interpol);


}
