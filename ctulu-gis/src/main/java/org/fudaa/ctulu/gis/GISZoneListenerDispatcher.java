package org.fudaa.ctulu.gis;

import java.util.HashSet;
import java.util.Set;

/**
 * A listener dispatching event for other listeners.
 * 
 * @author deniger
 */
public class GISZoneListenerDispatcher implements GISZoneListener {

  Set<GISZoneListener> listeners;

  public void addListener(final GISZoneListener l) {
    if (l == null) { return; }
    if (listeners == null) {
      listeners = new HashSet<GISZoneListener>();
    }
    listeners.add(l);
  }

  public void removeListener(final GISZoneListener l) {
    if (l == null) { return; }
    if (listeners != null) {
      listeners.remove(l);
    }
  }

  @Override
  public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action) {
    if (listeners != null) {
      for (GISZoneListener it : listeners) {
        it.attributeAction(_source, _indexAtt, _att, _action);
      }
    }
  }

  @Override
  public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexObj, Object _newValue) {
    if (listeners != null) {
      for (GISZoneListener it : listeners) {
        it.attributeValueChangeAction(_source, _indexAtt, _att, _indexObj, _newValue);
      }
    }
  }

  @Override
  public void objectAction(Object _source, int _indexObj, Object _obj, int _action) {
    if (listeners != null) {
      for (GISZoneListener it : listeners) {
        it.objectAction(_source, _indexObj, _obj, _action);
      }
    }
  }
}
