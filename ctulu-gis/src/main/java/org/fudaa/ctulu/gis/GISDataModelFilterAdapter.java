/*
 * @creation 6 juin 2005
 * 
 * @modification $Date: 2007-01-10 08:58:47 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import java.util.BitSet;
import org.fudaa.ctulu.CtuluLibArray;

/**
 * Un filtre permettant d'exposer certains attributs d'un mod�le de donn�es et/ou certaines g�om�tries. <p> <b>Important</b> : Le filtre autorise des
 * attributs qui ne sont pas dans le modele d'origine. Pour ces attributs, les valeurs retourn�es pour chaque g�om�trie sont nulles.
 *
 * @author Fred Deniger
 * @version $Id$
 */
public class GISDataModelFilterAdapter extends GISDataModelAbstract {

  /**
   * Cr�ation d'un filtre conservant toutes les g�om�tries et seulement les attributs donn�s.
   */
  public static GISDataModelFilterAdapter buildAdapter(final GISDataModel _collection, final GISAttributeInterface[] _att) {
    return buildAdapter(_collection, _att, null);
  }

  /**
   * Cr�ation d'un filtre ne conservant que les g�om�tries d'indices donn�s et les attributs donn�s.
   *
   * @param _collection la zone a adapter
   * @param _att les attributs dans l'ordre voulu.
   * @param _idxGeom Les indices des seules g�om�tries � exposer, dans l'ordre souhait�.
   * @return l'adapteur
   */
  public static GISDataModelFilterAdapter buildAdapter(final GISDataModel _collection, final GISAttributeInterface[] _att, final int[] _idxGeom) {

    final int[] idx = _att == null ? null : new int[_att.length];
    if (_att != null) {
      for (int i = _att.length - 1; i >= 0; i--) {
        idx[i] = _collection.getIndiceOf(_att[i]);
      }
    }
    return new GISDataModelFilterAdapter(_collection, idx, _idxGeom);

  }

  /**
   * Cr�ation d'un filtre ne conservant que les lignes et les d'attributs donn�s.
   */
  public static GISDataModelFilterAdapter buildLigneAdapter(final GISDataModel _collection, final GISAttributeInterface[] _att) {
    boolean allLigneBrisee = true;
    final GISVisitorChooser chooser = new GISVisitorChooser();
    final int nb = _collection.getNumGeometries();
    final BitSet set = new BitSet(nb);
    for (int i = 0; i < nb; i++) {
      ((GISGeometry) _collection.getGeometry(i)).accept(chooser);
      if (chooser.isPolyligne() || chooser.isPolygone()) {
        set.set(i);
      } else {
        allLigneBrisee = false;
      }
    }
    if (allLigneBrisee) {
      return buildAdapter(_collection, _att, null);
    }
    return buildAdapter(_collection, _att, CtuluLibArray.getSelectedIdx(set));
  }

  /**
   * Cr�ation d'un filtre ne conservant que les lignes ferm�es et les d'attributs donn�s.
   */
  public static GISDataModelFilterAdapter buildLigneFermeeAdapter(final GISDataModel _collection, final GISAttributeInterface[] _att) {
    boolean allLigneFermee = true;
    final GISVisitorChooser chooser = new GISVisitorChooser();
    final int nb = _collection.getNumGeometries();
    final BitSet set = new BitSet(nb);
    for (int i = 0; i < nb; i++) {
      ((GISGeometry) _collection.getGeometry(i)).accept(chooser);
      if (chooser.isPolygone()) {
        set.set(i);
      } else {
        allLigneFermee = false;
      }
    }
    if (allLigneFermee) {
      return buildAdapter(_collection, _att, null);
    }
    return buildAdapter(_collection, _att, CtuluLibArray.getSelectedIdx(set));
  }

  /**
   * Dans ce cas, les LineString ferm�es sont �galement accept�es
   *
   * @param _collection
   * @param _att
   * @return
   */
  public static GISDataModelFilterAdapter buildLigneFermeeCommonAdapter(final GISDataModel _collection, final GISAttributeInterface[] _att) {
    boolean allLigneFermee = true;
    final GISVisitorChooser chooser = new GISVisitorChooser();
    final int nb = _collection.getNumGeometries();
    final BitSet set = new BitSet(nb);
    for (int i = 0; i < nb; i++) {
      ((GISGeometry) _collection.getGeometry(i)).accept(chooser);
      if (chooser.isPolygone() || (chooser.isPolyligne() && ((LineString) _collection.getGeometry(i)).isClosed())) {
        set.set(i);
      } else {
        allLigneFermee = false;
      }
    }
    if (allLigneFermee) {
      return buildAdapter(_collection, _att, null);
    }
    return buildAdapter(_collection, _att, CtuluLibArray.getSelectedIdx(set));
  }

  /**
   * Cr�ation d'un filtre ne conservant que les lignes ouvertes et les d'attributs donn�s.
   */
  public static GISDataModelFilterAdapter buildLigneOuverteAdapter(final GISDataModel _collection, final GISAttributeInterface[] _att) {
    boolean allLigneBrisee = true;
    final GISVisitorChooser chooser = new GISVisitorChooser();
    final int nb = _collection.getNumGeometries();
    final BitSet set = new BitSet(nb);
    for (int i = 0; i < nb; i++) {
      ((GISGeometry) _collection.getGeometry(i)).accept(chooser);
      if (chooser.isPolyligne()) {
        set.set(i);
      } else {
        allLigneBrisee = false;
      }
    }
    if (allLigneBrisee) {
      return buildAdapter(_collection, _att, null);
    }
    return buildAdapter(_collection, _att, CtuluLibArray.getSelectedIdx(set));
  }

  /**
   * Cr�ation d'un filtre ne conservant que les points et les d'attributs donn�s.
   */
  public static GISDataModelFilterAdapter buildPointAdapter(final GISDataModel _collection, final GISAttributeInterface[] _att) {
    boolean allPoint = true;
    final GISVisitorChooser chooser = new GISVisitorChooser();
    final int nb = _collection.getNumGeometries();
    final BitSet set = new BitSet(nb);
    for (int i = 0; i < nb; i++) {
      ((GISGeometry) _collection.getGeometry(i)).accept(chooser);
      if (chooser.isPt()) {
        set.set(i);
      } else {
        allPoint = false;
      }
    }
    if (allPoint) {
      return buildAdapter(_collection, _att, null);
    }
    return buildAdapter(_collection, _att, CtuluLibArray.getSelectedIdx(set));
  }
  final int[] geometryShown_;

  /**
   * @param _model Le modele d'origine.
   * @param _idxAtt Les indices des seuls attributs a exposer. Peut etre <code>null</code>. Auquel cas tous les attributs sont expos�s.
   * @param _idxGeom Les indices des seules g�om�tries � exposer. Peut etre <code>null</code>. Auquel cas toutes les g�om�tries sont expos�es.
   */
  public GISDataModelFilterAdapter(final GISDataModel _model, final int[] _idxAtt, final int[] _idxGeom) {
    super(_idxAtt, _model);

    geometryShown_ = _idxGeom;
  }

  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    return new GISDataModelFilterAdapter(super.model_.createTranslate(xyToAdd), super.attShown_, geometryShown_);
  }

  private int getRealGeometryIndex(final int _init) {
    return geometryShown_ == null ? _init : geometryShown_[_init];
  }

  // geom
  @Override
  public double getDoubleValue(final int _idxAtt, final int _idxGeom) {
    final int idx = getRealAttributIndex(_idxAtt);
    if (idx < 0 || idx >= model_.getNbAttributes()) {
      return 0;
    }
    return model_.getDoubleValue(idx, getRealGeometryIndex(_idxGeom));
  }

  @Override
  public Geometry getGeometry(final int _idxGeom) {
    return model_.getGeometry(getRealGeometryIndex(_idxGeom));
  }

  @Override
  public int getNumGeometries() {
    return geometryShown_ == null ? model_.getNumGeometries() : geometryShown_.length;
  }

  @Override
  public Object getValue(final int _idxAtt, final int _idxGeom) {
    final int idx = getRealAttributIndex(_idxAtt);
    if (idx < 0 || idx >= model_.getNbAttributes()) {
      return null;
    }
    return model_.getValue(idx, getRealGeometryIndex(_idxGeom));
  }
}
