/*
 GPL 2
 */
package org.fudaa.ctulu.gis.process;

import org.fudaa.ctulu.interpolation.SupportLocationI;

/**
 * Adapter sans modification d'indice.
 *
 * @author Frederic Deniger
 */
public class PointsMappingEmpty implements PointsMapping {

  private final SupportLocationI init;

  public PointsMappingEmpty(SupportLocationI init) {
    this.init = init;
  }

  @Override
  public int getInitialIdx(int _i) {
    return _i;
  }

  @Override
  public int[] getMapping() {
    return null;
  }

  @Override
  public int getPtsNb() {
    return init.getPtsNb();
  }

  @Override
  public double getPtX(int _i) {
    return init.getPtX(_i);
  }

  @Override
  public double getPtY(int _i) {
    return init.getPtY(_i);
  }
}
