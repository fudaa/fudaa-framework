/*
 *  @creation     26 mai 2005
 *  @modification $Date: 2007-01-10 08:58:48 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import java.util.List;

/**
 * Cette classe permet d'adapter une zone a un comme source d'import. Il est possible de choisir les attributs a montrer
 * 
 * @author Fred Deniger
 * @version $Id: GISDataModelZoneLignePointAdapter.java,v 1.7 2007-01-10 08:58:48 deniger Exp $
 */
public class GISDataModelZoneLignePointAdapter extends GISDataModelAbstract {

  final int idxLine_;

  /**
   * @param _zone la zone a adapter
   * @param _numLigne l'indice de la ligne brisee utilisee
   * @param _attShown les indices a "montrer"
   */
  public GISDataModelZoneLignePointAdapter(final GISZoneCollectionLigneBrisee _zone, final int _numLigne,
      final int[] _attShown) {
    super(_attShown, _zone);
    idxLine_ = _numLigne;
  }
  
  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if(xyToAdd==null){
      return this;
    }
    return new GISDataModelZoneLignePointAdapter((GISZoneCollectionLigneBrisee) super.model_.createTranslate(xyToAdd),idxLine_,attShown_);
  }

  @Override
  public Geometry getGeometry(final int _idxGeom) {
    return model_.getGeometry(idxLine_).getGeometryN(_idxGeom);
  }

  protected GISZoneCollectionLigneBrisee getM() {
    return (GISZoneCollectionLigneBrisee) model_;
  }

  @Override
  public Envelope getEnvelopeInternal() {
    return model_.getGeometry(idxLine_).getEnvelopeInternal();
  }

  @Override
  public int getNumGeometries() {
    return model_.getGeometry(idxLine_).getNumPoints();
  }

  @Override
  public Object getValue(final int _idxAtt, final int _idxPt) {
    return getAttribute(_idxAtt).isAtomicValue() ? ((GISAttributeModel) getM().getDataModel(attShown_[_idxAtt])
        .getObjectValueAt(idxLine_)).getObjectValueAt(_idxPt) : getM().getDataModel(attShown_[_idxAtt])
        .getObjectValueAt(idxLine_);
  }

  @Override
  public double getDoubleValue(final int _idxAtt, final int _idxPt) {
    return getAttribute(_idxAtt).isAtomicValue() ? ((GISAttributeModelDoubleInterface) getM().getDataModel(
        attShown_[_idxAtt]).getObjectValueAt(idxLine_)).getValue(_idxPt) : ((GISAttributeModelDoubleInterface) getM()
        .getDataModel(attShown_[_idxAtt])).getValue(idxLine_);
  }

  /**
   * @param _collection la zone a adapter
   * @param _idxLigne l'indice de la ligne
   * @param _att les attributs dans l'ordre voulu.
   * @return l'adapteur
   */
  public static GISDataModelZoneLignePointAdapter buildAdapter(final GISZoneCollectionLigneBrisee _collection,
      final int _idxLigne, final GISAttributeInterface[] _att) {
    final int[] idx = new int[_att.length];
    for (int i = _att.length - 1; i >= 0; i--) {
      idx[i] = _collection.getIndiceOf(_att[i]);
    }
    return new GISDataModelZoneLignePointAdapter(_collection, _idxLigne, idx);

  }

  /**
   * @param _dest la destination recevant les GISDataModelLignePointAdapter
   * @param _collection la zone a adapter
   * @param _att les attributs dans l'ordre voulu.
   */
  public static void buildAdapters(final List _dest, final GISZoneCollectionLigneBrisee _collection,
      final GISAttributeInterface[] _att) {
    final int[] idx = new int[_att.length];
    for (int i = _att.length - 1; i >= 0; i--) {
      idx[i] = _collection.getIndiceOf(_att[i]);
    }
    final int nbPt = _collection.getNumGeometries();
    for (int i = 0; i < nbPt; i++) {
      _dest.add(new GISDataModelZoneLignePointAdapter(_collection, i, idx));
    }
  }
}
