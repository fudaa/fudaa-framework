package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Coordinate;

public class GISSegment
{
  private Coordinate pt1;
  private Coordinate pt2;  
  
  public GISSegment()
  {
    this(new Coordinate(), new Coordinate());
  }
  
  public GISSegment(Coordinate pt1, Coordinate pt2)
  {
    if ((pt1 == null) || (pt2 == null))
    {
      throw new NullPointerException();
    }
    
    this.pt1 = new Coordinate(pt1);
    this.pt2 = new Coordinate(pt2);
  }

  public Coordinate getPt1()
  {
    return new Coordinate(this.pt1);
  }

  public void setPt1(Coordinate pt1)
  {
    if (pt1 == null)
    {
      throw new NullPointerException();
    }

    this.pt1 = new Coordinate(pt1);
  }

  public Coordinate getPt2()
  {
    return new Coordinate(this.pt2);
  }

  public void setPt2(Coordinate pt2)
  {
    if (pt2 == null)
    {
      throw new NullPointerException();
    }

    this.pt2 = new Coordinate(pt2);
  }
}
