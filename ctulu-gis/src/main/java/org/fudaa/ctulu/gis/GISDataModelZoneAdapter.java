/*
 * @creation 26 mai 2005
 * 
 * @modification $Date: 2007-01-10 08:58:47 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Geometry;

/**
 * Cette classe permet d'adapter une zone a un comme source d'import. Il est possible de choisir les attributs a montrer
 * 
 * @author Fred Deniger
 * @version $Id: GISDataModelZoneAdapter.java,v 1.8 2007-01-10 08:58:47 deniger Exp $
 */
public class GISDataModelZoneAdapter extends GISDataModelAbstract {

  /**
   * @param _collection la zone a adapter
   * @param _att les attributs dans l'ordre voulu.
   * @return l'adapteur
   */
  public static GISDataModelZoneAdapter buildAdapter(final GISDataModel _collection, final GISAttributeInterface[] _att) {
    final int[] idx = new int[_att.length];
    for (int i = _att.length - 1; i >= 0; i--) {
      idx[i] = _collection.getIndiceOf(_att[i]);
    }
    return new GISDataModelZoneAdapter(_collection, idx);

  }

  /**
   * @param _zone la zone a adapter
   * @param _attShown les indices a "montrer"
   */
  public GISDataModelZoneAdapter(final GISDataModel _zone, final int[] _attShown) {
    super(_attShown, _zone);
  }

  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    return new GISDataModelZoneAdapter(super.model_.createTranslate(xyToAdd), attShown_);
  }

  @Override
  public double getDoubleValue(final int _idxAtt, final int _idxGeom) {
    return model_.getDoubleValue(attShown_ == null ? _idxAtt : attShown_[_idxAtt], _idxGeom);
  }

  @Override
  public Geometry getGeometry(final int _idxGeom) {
    return model_.getGeometry(_idxGeom);
  }

  @Override
  public int getNumGeometries() {
    return model_.getNumGeometries();
  }

  @Override
  public Object getValue(final int _idxAtt, final int _idxGeom) {
    return model_.getValue(attShown_ == null ? _idxAtt : attShown_[_idxAtt], _idxGeom);
  }

}
