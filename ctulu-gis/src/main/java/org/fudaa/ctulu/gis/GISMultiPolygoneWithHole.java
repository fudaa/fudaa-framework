/*
 * @creation 18 mars 2005
 *
 * @modification $Date: 2007-01-17 10:45:25 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Polygon;

/**
 * @author Fred Deniger
 * @version $Id: GISMultiPolygoneWithHole.java,v 1.8 2007-01-17 10:45:25 deniger Exp $
 */
public class GISMultiPolygoneWithHole extends MultiPolygon implements GISGeometry {
  protected long id_ = GISGeometryFactory.INSTANCE.getNewId();

  @Override
  public long getId() {
    return id_;
  }

  public GISMultiPolygoneWithHole() {
    super(null, GISGeometryFactory.INSTANCE);
  }

  /**
   * @param _polygons les polygones avec trous
   */
  public GISMultiPolygoneWithHole(final Polygon[] _polygons) {
    super(_polygons, GISGeometryFactory.INSTANCE);
  }

  @Override
  public GISGeometry createTranslateGeometry(GISPoint xyToAdd) {
    if (xyToAdd == null || super.geometries == null) {
      return this;
    }
    GISPolygoneWithHole[] newLines = new GISPolygoneWithHole[super.geometries.length];
    for (int i = 0; i < newLines.length; i++) {
      final GISPolygoneWithHole p = (GISPolygoneWithHole) getGeometryN(i);
      newLines[i] = p.createTranslateGeometry(xyToAdd);
    }
    return new GISMultiPolygoneWithHole(newLines);
  }

  @Override
  protected MultiPolygon copyInternal() {
    Polygon[] polygons = new Polygon[this.geometries.length];
    for (int i = 0; i < polygons.length; i++) {
      polygons[i] = (Polygon) this.geometries[i].copy();
    }
    return new GISMultiPolygoneWithHole(polygons, factory);
  }

  /**
   * @param _polygons
   * @param _factory
   */
  public GISMultiPolygoneWithHole(final Polygon[] _polygons, final GeometryFactory _factory) {
    super(_polygons, _factory);
  }

  @Override
  public boolean accept(final GISVisitor _v) {
    return _v.visitMultiPolygoneWithHole(this);
  }
}
