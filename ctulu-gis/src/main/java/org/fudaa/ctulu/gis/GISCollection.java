/*
 * @creation 18 mars 2005
 *
 * @modification $Date: 2007-01-10 08:58:47 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.fudaa.ctulu.CtuluCommandComposite;
import org.fudaa.ctulu.CtuluCommandContainer;
import org.fudaa.ctulu.collection.CtuluListObject;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.util.Assert;

import java.util.List;
import java.util.TreeSet;

/**
 * @author Fred Deniger
 * @version $Id: GISCollection.java,v 1.18 2007-01-10 08:58:47 deniger Exp $
 */
public abstract class GISCollection extends Geometry implements GISPointContainerInterface, GISGeometry {
  protected class GISObjectContainer extends CtuluListObject {
    public GISObjectContainer(final int _nb) {
      super(_nb);
    }

    public GISObjectContainer(final GISObjectContainer toCopy) {
      super(toCopy.list_);
    }

    @Override
    protected void fireObjectAdded(int _newIdx, Object _newGeom) {
      GISCollection.this.geometryChanged();
      GISCollection.this.fireGeometryAdded(_newIdx, (Geometry) _newGeom);
    }

    @Override
    protected void fireObjectChanged(int _idx, Object _geom) {
      fireObjectModified(_idx, _geom);
    }

    @Override
    protected void fireObjectModified(int _oldIdx, Object _oldGeom) {
      GISCollection.this.geometryChanged();
      GISCollection.this.fireGeometryModified(_oldIdx, (Geometry) _oldGeom);
    }

    @Override
    protected void fireObjectRemoved(int _oldIdx, Object _oldGeom) {
      GISCollection.this.geometryChanged();
      GISCollection.this.fireGeometryRemoved(_oldIdx, (Geometry) _oldGeom);
    }

    protected List getList() {
      return list_;
    }

    @Override
    protected void internActionCleared(final CtuluCommandContainer _c) {
      GISCollection.this.internActionCleared(_c);
    }

    @Override
    protected void internActionPointAdded(final List _values, final CtuluCommandContainer _c, final int _nbValues) {
      GISCollection.this.internActionPointAdded(_values, _c, _nbValues);
    }

    @Override
    protected void internActionPointInserted(final int _i, final List _values, final CtuluCommandContainer _c) {
      GISCollection.this.internActionPointInserted(_i, _values, _c);
    }

    @Override
    protected void internActionPointRemoved(final int _i, final CtuluCommandContainer _c) {
      GISCollection.this.internActionPointRemoved(_i, _c);
    }

    @Override
    protected void internActionPointRemoved(final int[] _i, final CtuluCommandContainer _c) {
      GISCollection.this.internActionPointRemoved(_i, _c);
    }

    @Override
    protected void internalAdd(final Object _v) {
      super.internalAdd(_v);
    }
  }

  protected GISObjectContainer geometry_;
  /**
   * Une instance �coutant les actions effectu�e sur les objets.
   */
  protected transient GISZoneListener listener_;

  public GISCollection() {
    this(GISGeometryFactory.INSTANCE, 20);
  }

  @Override
  public String toString() {
    return getClass().getName() + " " + getGeometryType();
  }

  /**
   * @param _factory
   * @param _nbObject le nombre d'objets attendus
   */
  public GISCollection(final GeometryFactory _factory, final int _nbObject) {
    super(_factory);
    geometry_ = new GISObjectContainer(_nbObject);
  }

  public GISCollection(final int _nbObject) {
    this(GISGeometryFactory.INSTANCE, _nbObject);
  }

  /**
   * Recopie de GeometryCollection.
   */
  @Override
  protected int compareToSameClass(final Object _obj) {
    final TreeSet theseElements = new TreeSet(geometry_.getList());
    final TreeSet otherElements = new TreeSet(((GISCollection) _obj).geometry_.getList());
    return compare(theseElements, otherElements);
  }

  protected Geometry reverseInternal() {
    throw new UnsupportedOperationException("not supported");
  }

  /**
   * Recopie de GeometryCollection.
   */
  @Override
  protected int compareToSameClass(final Object _o, final CoordinateSequenceComparator _comp) {
    final GISCollection gc = (GISCollection) _o;
    final int n1 = getNumGeometries();
    final int n2 = gc.getNumGeometries();
    int i = 0;
    while (i < n1 && i < n2) {
      final Geometry thisGeom = getGeometryN(i);
      final Geometry otherGeom = gc.getGeometryN(i);
      final int holeComp = thisGeom.compareTo(otherGeom, _comp);
      if (holeComp != 0) {
        return holeComp;
      }
      i++;
    }
    if (i < n1) {
      return 1;
    }
    if (i < n2) {
      return -1;
    }
    return 0;
  }

  protected void fireGeometryAdded(int _newIdx, Geometry _newGeom) {
    if (listener_ != null) {
      listener_.objectAction(this, _newIdx, _newGeom, GISZoneListener.OBJECT_ACTION_ADD);
    }
  }

  protected void fireGeometryRemoved(int _oldIdx, Geometry _oldGeom) {
    if (listener_ != null) {
      listener_.objectAction(this, _oldIdx, _oldGeom, GISZoneListener.OBJECT_ACTION_REMOVE);
    }
  }

  protected void fireGeometryModified(int _oldIdx, Geometry _oldGeom) {
    if (listener_ != null) {
      listener_.objectAction(this, _oldIdx, _oldGeom, GISZoneListener.OBJECT_ACTION_MODIFY);
    }
  }

  @Override
  protected Envelope computeEnvelopeInternal() {
    final Envelope e = new Envelope();
    final int n = getNumGeometries();
    for (int i = 0; i < n; i++) {
      final Envelope envelopeInternal = getGeometryN(i).getEnvelopeInternal();
      if (envelopeInternal != null) {
        e.expandToInclude(envelopeInternal);
      }
    }
    return e;
  }

  protected void internActionCleared(final CtuluCommandContainer _c) {
  }

  protected void internActionPointAdded(final List _values, final CtuluCommandContainer _c, final int _nbValues) {
  }

  protected void internActionPointInserted(final int _i, final List _values, final CtuluCommandContainer _c) {
  }

  protected void internActionPointRemoved(final int _i, final CtuluCommandContainer _c) {
  }

  protected void internActionPointRemoved(final int[] _i, final CtuluCommandContainer _c) {
  }

  public void setListener(final GISZoneListener _listener) {
    listener_ = _listener;
  }

  @Override
  public boolean accept(final GISVisitor _v) {
    return GISLib.visitGeometry(this, _v);
  }

  @Override
  public final void apply(final CoordinateFilter _filter) {
    final int n = getNumGeometries();
    for (int i = 0; i < n; i++) {
      getGeometryN(i).apply(_filter);
    }
  }

  @Override
  public void apply(CoordinateSequenceFilter filter) {
    final int n = getNumGeometries();
    for (int i = 0; i < n; i++) {
      getGeometryN(i).apply(filter);
    }
  }

  @Override
  public final void apply(final GeometryComponentFilter _filter) {
    _filter.filter(this);
    final int n = getNumGeometries();
    for (int i = 0; i < n; i++) {
      getGeometryN(i).apply(_filter);
    }
  }

  @Override
  public final void apply(final GeometryFilter _filter) {
    _filter.filter(this);
    final int n = getNumGeometries();
    for (int i = 0; i < n; i++) {
      getGeometryN(i).apply(_filter);
    }
  }

  protected Object superClone() {
    return super.clone();
  }

  protected GISObjectContainer createGisObjectContainer(int nbGeom) {
    return new GISObjectContainer(nbGeom);
  }

  /**
   * Ne copie que la g�ometrie.
   */
  @Override
  public Object clone() {
    final GISCollection gc = (GISCollection) super.clone();
    final int n = getNumGeometries();
    gc.geometry_ = gc.createGisObjectContainer(getNumGeometries());
    for (int i = 0; i < n; i++) {
      // on caste pour etre sur que l'on ait une geometrie
      gc.geometry_.addObject(getGeometryN(i).clone(), null);
    }
    return gc;// return the clone
  }

  @Override
  protected Geometry copyInternal() {
    return (Geometry) clone();
  }

  @Override
  public boolean equalsExact(final Geometry _other, final double _tolerance) {
    if (!isEquivalentClass(_other)) {
      return false;
    }
    final GISCollection otherCollection = (GISCollection) _other;
    if (getNumGeometries() != getNumGeometries()) {
      return false;
    }
    final int n = getNumGeometries();
    for (int i = 0; i < n; i++) {
      if (!(getGeometryN(i)).equalsExact(otherCollection.getGeometryN(i), _tolerance)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean fillListWithPoint(final GISCollectionPointInteface _listToFill) {
    final int n = getNumGeometries();
    for (int i = 0; i < n; i++) {
      ((GISPointContainerInterface) getGeometry(i)).fillListWithPoint(_listToFill);
    }
    return true;
  }

  /**
   * Returns the area of this <code>GeometryCollection</code>.
   *
   * @return the area of the polygon
   */
  @Override
  public double getArea() {
    double area = 0.0;
    final int n = getNumGeometries();
    for (int i = 0; i < n; i++) {
      area += getGeometry(i).getArea();
    }
    return area;
  }

  @Override
  public Geometry getBoundary() {
    Assert.shouldNeverReachHere();
    return null;
  }

  @Override
  public int getBoundaryDimension() {
    int dimension = Dimension.FALSE;
    final int n = getNumGeometries();
    for (int i = 0; i < n; i++) {
      dimension = Math.max(dimension, getGeometryN(i).getBoundaryDimension());
    }
    return dimension;
  }

  @Override
  public Coordinate getCoordinate() {
    if (isEmpty()) {
      return null;
    }
    return getGeometry(0).getCoordinate();
  }

  /**
   * Collects all coordinates of all subgeometries into an Array. Note that while changes to the coordinate objects
   * themselves may modify the Geometries in place, the returned Array as such is only a temporary container which is
   * not synchronized back.
   *
   * @return the collected coordinates
   */
  @Override
  public Coordinate[] getCoordinates() {
    final Coordinate[] coordinates = new Coordinate[getNumPoints()];
    int k = -1;
    final int n = getNumGeometries();
    for (int i = 0; i < n; i++) {
      final Coordinate[] childCoordinates = getGeometryN(i).getCoordinates();
      for (int j = 0; j < childCoordinates.length; j++) {
        k++;
        coordinates[k] = childCoordinates[j];
      }
    }
    return coordinates;
  }

  public abstract CoordinateSequence getCoordinateSequence(int _i);

  @Override
  public int getDimension() {
    int dimension = Dimension.FALSE;
    final int n = getNumGeometries();
    for (int i = 0; i < n; i++) {
      dimension = Math.max(dimension, getGeometryN(i).getDimension());
    }
    return dimension;
  }

  /**
   * @param _i l'indice de la geometry
   * @return la geometry d'indice _i
   */
  public Geometry getGeometry(final int _i) {
    return (Geometry) geometry_.getValueAt(_i);
  }

  @Override
  public Geometry getGeometryN(final int _n) {
    return getGeometry(_n);
  }

  @Override
  public String getGeometryType() {
    return "GeometryModelCollection";
  }

  @Override
  public double getLength() {
    double sum = 0.0;
    final int n = getNumGeometries();
    for (int i = 0; i < n; i++) {
      sum += getGeometry(i).getLength();
    }
    return sum;
  }

  public GISZoneListener getListener() {
    return listener_;
  }

  @Override
  public int getNumGeometries() {
    return geometry_.getSize();
  }

  @Override
  public int getNumPoints() {
    int numPoints = 0;
    final int n = getNumGeometries();
    for (int i = 0; i < n; i++) {
      numPoints += getGeometry(i).getNumPoints();
    }
    return numPoints;
  }

  /**
   * @param _g la geometrie a rechercher
   * @return son index
   */
  public int indexOf(final Geometry _g) {
    return geometry_.indexOf(_g);
  }

  @Override
  public boolean isEmpty() {
    final int n = getNumGeometries();
    for (int i = 0; i < n; i++) {
      if (!getGeometryN(i).isEmpty()) {
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean isSimple() {
    Assert.shouldNeverReachHere();
    return false;
  }

  @Override
  public void normalize() {
    Assert.shouldNeverReachHere();
  }

  /**
   * Permute les deux g�om�tries indiqu�es par les deux index pass�s en param�tre. Si les index sont incoh�rents,
   * {@link IllegalArgumentException} est lev�.
   *
   * @param _index1 l'index de la premi�re g�om�trie
   * @param _index2 l'index de la seconde gom�trie
   * @param _cmd le command manager pour le undo/redo
   */
  public void switchGeometries(int _index1, int _index2, CtuluCommandContainer _cmd) {
    if (_index1 < 0 || _index1 > geometry_.getSize() - 1 || _index2 < 0 || _index2 > geometry_.getSize() - 1) {
      throw new IllegalArgumentException("Au moins l'un des deux index est incoh�rent.");
    }
    if (_index1 != _index2) {
      CtuluCommandComposite cmd = new CtuluCommandComposite();
      // Permutation de la g�om�trie
      Object idx1 = geometry_.getValueAt(_index1);
      Object idx2 = geometry_.getValueAt(_index2);
      geometry_.set(_index1, idx2, cmd);
      geometry_.set(_index2, idx1, cmd);
      fireGeometryModified(_index1, (Geometry) idx1);
      fireGeometryModified(_index2, (Geometry) idx2);
      if (_cmd != null) {
        _cmd.addCmd(cmd.getSimplify());
      }
    }
  }
}
