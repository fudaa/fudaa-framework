/*
 * @creation     24 oct. 2008
 * @modification $Date:$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gis;


/**
 * Cette interface permet l'�coute des changements concernant les objets et les
 * attributs (si existant). Les changements sont : l'ajout d'un attribut, la
 * suppression d'un attribut, le changement d'une valeur d'un attribut, l'ajout
 * d'un objet, la supression d'un objet et la modification d'un objet.
 * 
 * @author Emmanuel MARTIN
 * @version $Id: AttributeListener.java 4103 2008-10-28 15:55:12Z
 *          emmanuel_martin $
 */
public interface GISZoneListener {

  /** Action indiquant l'ajout d'un attribut dans le conteneur d'attributs. */
  public static final int ATTRIBUTE_ACTION_ADD=0;
  /**
   * Action indiquant la supression d'un attribut dans le conteneur d'attributs.
   */
  public static final int ATTRIBUTE_ACTION_REMOVE=1;
  /** Action indiquant l'ajout d'une g�om�trie dans le conteneur de g�om�tries. */
  public static final int OBJECT_ACTION_ADD=2;
  /**
   * Action indiquant la supression d'un objet dans le conteneur de g�om�tries.
   */
  public static final int OBJECT_ACTION_REMOVE=3;
  /** Action indiquant la modification d'un objet. */
  public static final int OBJECT_ACTION_MODIFY=4;

  /**
   * M�thode appel�e lorsqu'une action d'ajout ou de suppression est effectu�e
   * sur un attribut.
   * 
   * @param _source
   *          L'instance appelant attributeAction.
   * @param _indexAtt
   *          L'index de l'attribut. Si l'action est l'ajout d'un attribut,
   *          l'index est valide. Si l'action est la supression, l'index
   *          correspond � l'ancienne emplacement et donc n'est plus valide. Si
   *          �gale � -1, tout les attributs ont pu �tre modifi�s.
   * @param _att
   *          L'attribut concern�. Si null tout les attributs ont pu �tre
   *          modifi�s.
   * @param _action
   *          L'action effectu�e sur l'attribut. Voir les ATTRIBUTE_ACTION_*.
   */
  public void attributeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _action);

  /**
   * M�thode appel�e lorsqu'une action de modification de la valeur d'un
   * attribut est effectu�e.
   * 
   * @param _source
   *          L'instance appelant attributeValueChangeAction.
   * @param _indexAtt
   *          L'index de l'attribut. Si �gale � -1, tout les attributs ont pu
   *          �tre modifi�s.
   * @param _att
   *          L'attribut concern�. Si null tout les attributs ont pu �tre
   *          modifi�s.
   * @param _indexObj
   *          L'objet concern�. Si �gale � -1, tout les objets ont pu �tre
   *          modifi�s.
   * @param _newValue
   *          La nouvelle valeur de l'attribut pour la g�om�trie indiqu�e. Si
   *          null toutes les g�om�tries ont pu �tre modifi�es.
   */
  public void attributeValueChangeAction(Object _source, int _indexAtt, GISAttributeInterface _att, int _indexObj, Object _newValue);

  /**
   * M�thode appel�e lorsqu'une action est effectu�e sur un objet.
   * 
   * @param _source
   *          L'instance appelant objectAction.
   * @param _indexObj
   *          L'index de l'objet. Si l'action est l'ajout ou la modification,
   *          l'index est valide. Si l'action est la supression, l'index
   *          correspond � l'ancienne emplacement et donc n'est plus valide. Si
   *          �gale � -1, tout les objets ont pu �tre modifi�es.
   * @param _obj
   *          La g�om�trie concern�e. Si null tout les objets ont pu �tre
   *          modifi�es.
   * @param _action
   *          L'action effectu�e sur la g�om�trie. Voir les OBJECT_ACTION_*.
   */
  public void objectAction(Object _source, int _indexObj, Object _obj, int _action);
}
