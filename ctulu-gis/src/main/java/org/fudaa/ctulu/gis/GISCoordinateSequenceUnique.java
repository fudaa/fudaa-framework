/*
 *  @creation     18 mars 2005
 *  @modification $Date: 2006-09-19 14:36:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Envelope;

/**
 * list of 1 point
 */
public class GISCoordinateSequenceUnique implements CoordinateSequence {

  double x_;

  double y_;

  double z_;

  private int dimension=-1;

  public GISCoordinateSequenceUnique() {
    this(0, 0, 0);
  }

  public GISCoordinateSequenceUnique(final double _x, final double _y, final double _z) {
    x_ = _x;
    y_ = _y;
    z_ = _z;
  }
  public GISCoordinateSequenceUnique(final double _x, final double _y, final double _z,int dimension) {
    x_ = _x;
    y_ = _y;
    z_ = _z;
    this.dimension=dimension;
  }

  public GISCoordinateSequenceUnique(final Coordinate _c) {
    this(_c.x, _c.y, _c.z);
  }

  @Override
  public int getDimension() {
    if(dimension>0) return dimension;
    return 3;
  }

  @Override
  public  Object clone() {
    return new GISCoordinateSequenceUnique(x_, y_, z_);
  }

  @Override
  public CoordinateSequence copy() {
    return new GISCoordinateSequenceUnique(x_, y_, z_);
  }

  @Override
  public Envelope expandEnvelope(final Envelope _env) {
    _env.expandToInclude(x_, y_);
    return _env;
  }

  @Override
  public void getCoordinate(final int _index, final Coordinate _coord) {
    _coord.x = x_;
    _coord.y = y_;
    _coord.z = z_;
  }

  @Override
  public Coordinate getCoordinate(final int _i) {
    if (_i != 0) {
      throw new ArrayIndexOutOfBoundsException(_i);
    }
    return new Coordinate(x_, y_, z_);
  }

  @Override
  public Coordinate getCoordinateCopy(final int _i) {
    if (_i != 0) {
      throw new ArrayIndexOutOfBoundsException(_i);
    }
    return getCoordinate(_i);
  }

  @Override
  public double getOrdinate(final int _index, final int _ordinateIndex) {
    if (_index != 0) {
      throw new ArrayIndexOutOfBoundsException(_index);
    }
    switch (_ordinateIndex) {
    case CoordinateSequence.X:
      return x_;
    case CoordinateSequence.Y:
      return y_;
    case CoordinateSequence.Z:
      return z_;
    default:
      throw new IllegalArgumentException("bad ordinate");
    }
    // return Double.NaN;
  }

  @Override
  public double getX(final int _index) {
    if (_index != 0) {
      throw new ArrayIndexOutOfBoundsException(_index);
    }
    return x_;
  }

  @Override
  public double getY(final int _index) {
    if (_index != 0) {
      throw new ArrayIndexOutOfBoundsException(_index);
    }
    return y_;
  }

  public double getZ(final int _index) {
    if (_index != 0) {
      throw new ArrayIndexOutOfBoundsException(_index);
    }
    return z_;
  }

  @Override
  public void setOrdinate(final int _index, final int _ordinateIndex, final double _value) {
    if (_index != 0) {
      throw new ArrayIndexOutOfBoundsException(_index);
    }
    switch (_ordinateIndex) {
    case CoordinateSequence.X:
      x_ = _value;
      break;
    case CoordinateSequence.Y:
      y_ = _value;
      break;
    case CoordinateSequence.Z:
      z_ = _value;
      break;
    default:
      throw new IllegalArgumentException("bad ordinate");
    }
  }

  @Override
  public int size() {
    return 1;
  }

  @Override
  public Coordinate[] toCoordinateArray() {
    return new Coordinate[] { getCoordinate(0) };
  }
}
