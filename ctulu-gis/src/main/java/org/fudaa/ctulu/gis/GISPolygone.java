/*
 * @creation 18 mars 2005
 *
 * @modification $Date: 2007-01-17 10:45:25 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.operation.IsSimpleOp;

/**
 * Une g�om�trie polygone. Un polygone est une polyligne ferm�e. Au sens Fudaa, sa premi�re et sa derni�re coordonn�es
 * sont identiques. Si un polygone est pour l'utilisateur constitu� de 4 points, son nombre de coordonn�es sera en
 * r�alit� de 5. Le nombre de coordonn�es du polygone doit �tre au minimum de 4.
 *
 * @author Fred Deniger
 * @version $Id$
 */
public class GISPolygone extends LinearRing implements GISPointContainerInterface, GISLigneBrisee {
  protected long id_ = GISGeometryFactory.INSTANCE.getNewId();

  @Override
  public long getId() {
    return id_;
  }

  GISPolygone() {
    super(GISGeometryFactory.INSTANCE.getCoordinateSequenceFactory().create(
        new Coordinate[]{new Coordinate(0, 0), new Coordinate(0, 1), new Coordinate(1, 1), new Coordinate(0, 0)}), GISGeometryFactory.INSTANCE);
  }

  /**
   * @param _points
   */
  public GISPolygone(final CoordinateSequence _points) {
    super(_points, GISGeometryFactory.INSTANCE);
  }

  @Override
  public GISPolygone createTranslateGeometry(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    return new GISPolygone(GISGeometryFactory.INSTANCE.createTranslated(getCoordinateSequence(), xyToAdd));
  }

  @Override
  protected LinearRing copyInternal() {
    return new GISPolygone(points.copy(), factory);
  }

  /**
   * @return pour definir si c'est une courbe de niveau
   */
  public boolean isNiveau() {
    return false;
  }

  @Override
  public double getArea() {
    final int nb = getNumPoints();
    if (nb < 3) {
      return 0.0;
    }
    double sum = 0.0;
    final CoordinateSequence seq = getCoordinateSequence();
    for (int i = 0; i < nb - 1; i++) {
      final double bx = seq.getX(i);
      final double by = seq.getY(i);
      final double cx = seq.getX(i + 1);
      final double cy = seq.getY(i + 1);
      sum += (bx + cx) * (cy - by);
    }
    return Math.abs(sum / 2.0);
  }

  @Override
  public boolean accept(final GISVisitor _v) {
    return _v.visitPolygone(this);
  }

  @Override
  public boolean isSimple() {
    return (new IsSimpleOp()).isSimple(this);
  }

  @Override
  public boolean fillListWithPoint(final GISCollectionPointInteface _listToFill) {
    final CoordinateSequence seq = getCoordinateSequence();
    final int nb = seq.size();
    for (int i = 0; i < nb; i++) {
      _listToFill.add(seq.getX(i), seq.getY(i), seq.getOrdinate(i, 2));
    }
    return true;
  }

  /**
   * @param _points the points
   * @param _factory GeometryFactory
   */
  public GISPolygone(final CoordinateSequence _points, final GeometryFactory _factory) {
    super(_points, _factory);
  }

  @Override
  public boolean getSegment(int idx, GISSegment seg) {
    if ((seg == null) || (idx < 0) || (idx > this.getNbSegment())) {
      return false;
    }

    seg.setPt1(this.getCoordinateN(idx));
    seg.setPt2(this.getCoordinateN(idx + 1));

    return true;
  }

  @Override
  public GISSegment getSegment(int idx) {
    GISSegment seg = new GISSegment();

    if (!this.getSegment(idx, seg)) {
      return null;
    }

    return seg;
  }

  @Override
  public int getNbSegment() {
    return (this.getNumPoints() - 1);
  }
}
