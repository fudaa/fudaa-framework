/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2008-03-26 16:46:43 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import java.util.Arrays;
import java.util.Comparator;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.collection.CtuluCollection;
import org.fudaa.ctulu.editor.CtuluValueEditorI;

/**
 * Une implementation g�n�rique d'un attribut d'objet GIS.
 *
 * @author Fred Deniger
 * @version $Id: GISAttribute.java,v 1.19.6.3 2008-03-26 16:46:43 bmarchan Exp $
 */
public class GISAttribute implements GISAttributeInterface {

  private transient CtuluValueEditorI editor_;
  final boolean isAtomic_;
  String name_;
  boolean isUserVisible_ = true;
  Object userData_;

  /**
   * Construction d'un attribut non atomique et anonyme.
   *
   * @param _editor L'editeur associ�.
   */
  public GISAttribute(final CtuluValueEditorI _editor) {
    this(_editor, false);
  }

  /**
   * Construction d'un attribut anonyme.
   *
   * @param _editor L'editeur associ�.
   * @param _isAtomic true : L'attribut est atomique.
   */
  public GISAttribute(final CtuluValueEditorI _editor, final boolean _isAtomic) {
    super();
    setInternEditor(_editor);
    isAtomic_ = _isAtomic;
  }

  @Override
  public String toString() {
    return GISAttributeConstants.toString(this);
  }

  /**
   * Construction d'un attribut non atomique.
   *
   * @param _editor L'editeur associ�.
   * @param _name le nom de l'attribut.
   */
  public GISAttribute(final CtuluValueEditorI _editor, final String _name) {
    this(_editor);
    name_ = _name;
  }

  /**
   * Constructeur g�n�ral.
   *
   * @param _editor L'editeur associ�.
   * @param _name le nom de l'attribut.
   * @param _isAtomic true si concerne tous les points meme les points des lignes bris�es.
   */
  public GISAttribute(final CtuluValueEditorI _editor, final String _name, final boolean _isAtomic) {
    this(_editor, _isAtomic);
    name_ = _name;
  }

  protected boolean isKnownValue(Object initValue) {
    if (initValue == null) {
      return false;
    }
    Class c = initValue.getClass();
    return Double.class.equals(c) || Integer.class.equals(c) || Long.class.equals(c) || String.class.equals(c) || Boolean.class.equals(c);
  }

  protected final Object createAtomicValues(final Object _initValues, final int _nbPt) {
// B.M. 23/03/2008 - Le nombre d'objets ne doit pas influer sur le modele d'attribut.
//    if (_nbPt == 1) {
//      return createGlobalValues(_initValues);
//    }
    Object[] values = null;
    if (_initValues instanceof CtuluCollection) {
      values = ((CtuluCollection) _initValues).getObjectValues();
    } else if (_initValues instanceof Object[]) {
      values = (Object[]) _initValues;
    } else if (isKnownValue(_initValues)) {
      values = new Object[]{_initValues};
    }
    if (values != null && values.length != _nbPt) {
      final int max = Math.min(_nbPt, values.length);
      final Object[] newVal = new Object[_nbPt];
      System.arraycopy(values, 0, newVal, 0, max);
      values = newVal;
      final Object def = getDefaultValue();
      for (int i = max; i < values.length; i++) {
        values[i] = def;
      }
    }

    return createAtomicModel(values, _nbPt);
  }

  /**
   * Cree une valeur globale (scalaire) � partir d'une collection, bas�e sur la moyenne de la collection.
   *
   * @param _initValues La collection pour l'initialisation. Si null, la valeur par d�faut est prise.
   * @return La valeur globale.
   */
  protected Object createGlobalValues(final Object _initValues) {
    Object r = _initValues;
    if (_initValues instanceof GISAttributeModel) {
      r = ((GISAttributeModel) _initValues).getAverage();
    }
    if (r == null) {
      r = getDefaultValue();
    }
    return r;

  }

  @Override
  public boolean isEditable() {
    return editor_ != null;
  }

  public final void setInternEditor(final CtuluValueEditorI _v) {
    editor_ = _v;

  }

  @Override
  public GISAttributeModelObservable createAtomicModel(final Object[] _initValues, final int _nbValues) {
    if (_initValues != null) {

      //TODO voir ici
      if (_initValues.length != _nbValues) {
        throw new IllegalArgumentException("bad size value=" + _nbValues + " used=" + _initValues.length);
      }
      return new GISAttributeModelObjectArray(_initValues, this);
    }
    return new GISAttributeModelObjectArray(_nbValues, this);
  }

  @Override
  public final Object createDataForGeom(final Object _initValues, final int _nbPt) {
    return isAtomic_ ? createAtomicValues(_initValues, _nbPt) : createGlobalValues(_initValues);
  }

  public Object[] getDefaultObject(final int _nb) {
    final Object[] res = new Object[_nb];
    Arrays.fill(res, getDefaultValue());
    return res;
  }

  @Override
  public GISAttributeModelObservable createListModel(final int _initCap) {
    return new GISAttributeModelObjectList(_initCap, this);
  }

  @Override
  public boolean equals(final Object _obj) {
    if (_obj == this) {
      return true;
    }
    if (_obj == null) {
      return false;
    }
    if (_obj instanceof GISAttribute) {
      final GISAttribute att = (GISAttribute) _obj;
      return isSameContent(att);

    }
    return false;
  }

  @Override
  public boolean isSameContent(final GISAttributeInterface _att) {
    return _att != null && isAtomic_ == _att.isAtomicValue() && getID().equals(_att.getID())
            && getDataClass().equals(_att.getDataClass()) && CtuluLib.isEquals(userData_, _att.getUserData());
  }

  @Override
  public Object getCommonUnit() {
    return null;
  }

  @Override
  public Class getDataClass() {
    return Object.class;
  }

  @Override
  public Object getDefaultValue() {
    return null;
  }

  @Override
  public CtuluValueEditorI getEditor() {
    return editor_;
  }
  String id;

  public void setID(String id) {
    this.id = id;
  }

  @Override
  public String getID() {
    return id == null ? name_ : id;
  }

  public String getInitialID(){
    return id;
  }

  @Override
  public String getLongName() {
    return isAtomic_ ? name_ + CtuluLibString.EMPTY_STRING + " - 3D" : name_;
  }

  @Override
  public String getName() {
    return name_;
  }

  @Override
  public int getPrecision() {
    return 0;
  }

  @Override
  public final Object getUserData() {
    return userData_;
  }

  /**
   * Calcul du hashcode. La surchage est necessaire, pour que 2 instances r�pondant au contrat att1.equals(att2)==true aient le m�me hashcode. En
   * particulier utilis� comme cl� des HashMap.<p>
   * Nous supposons que le hashcode est unique calcul� comme ca...
   */
  @Override
  public int hashCode() {
    int r = 17 * getClass().hashCode();
    r += name_.hashCode() * 7;
    if (isAtomic_) {
      r += 1;
    }
    if (userData_ != null) {
      r += userData_.hashCode();
    }
    r += getDataClass().hashCode();
    return r;
  }

  @Override
  public boolean isAtomicValue() {
    return isAtomic_;
  }

  @Override
  public final boolean setName(final String _name) {
    if (!GISAttributeConstants.isConstant(this) && _name != null && _name.trim().length() > 0 && !_name.equals(name_)) {
      name_ = _name.trim();
      return true;
    }
    return false;
  }

  @Override
  public final void setUserData(final Object _userData) {
    userData_ = _userData;
  }

  /**
   * L'attribut est-il visible pour l'utilisateur?
   *
   * @return true : Visible.
   */
  @Override
  public boolean isUserVisible() {
    return isUserVisible_;
  }

  /**
   * Rend l'attribut visible ou non � l'utilisateur.
   *
   * @param _isUserVisible L'attribut ne sera pas visible.
   */
  public void setUserVisible(final boolean _isUserVisible) {
    isUserVisible_ = _isUserVisible;
  }

  /**
   * @return Un comparateur g�n�rique des valeurs stock�es par le modele.
   */
  @Override
  public Comparator<?> getComparator() {
    if (Comparable.class
            .isAssignableFrom(getDataClass())) {
      return new Comparator<Object>() {
        @Override
        public int compare(Object o1, Object o2
        ) {
          return ((Comparable) o1).compareTo(o2);
        }
      };
    } else {
      return new Comparator<Object>() {
        @Override
        public int compare(Object o1, Object o2) {
          return o1 == null ? (o2 == null ? 0 : 1) : (o2 == null ? -1 : o1.toString().compareTo(o2.toString()));
        }
      };
    }
  }
}
