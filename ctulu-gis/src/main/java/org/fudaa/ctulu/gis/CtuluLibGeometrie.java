/*
 * @file CtuluGeometrie.java
 * 
 * @creation 29 janv. 2004
 * 
 * @modification $Date: 2006-10-24 12:47:38 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.org
 */
package org.fudaa.ctulu.gis;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Point;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import org.fudaa.ctulu.LocalSolve;

/**
 * @author deniger
 * @version $Id: CtuluLibGeometrie.java,v 1.13 2006-10-24 12:47:38 deniger Exp $
 */
public final class CtuluLibGeometrie {

  private CtuluLibGeometrie() {
  }

  /**
   * @param _x1 x du point 1
   * @param _y1 y du point 1
   * @param _x2 x du point 2
   * @param _y2 y du point 2
   * @return Renvoie la distance entre le point p1(_x1,_y1) et le point p2(_x2,_y2)
   */
  public static double getDistance(final double _x1, final double _y1, final double _x2, final double _y2) {
    double dy = _y2 - _y1;
    double dx = _x2 - _x1;
    //    return Math.hypot(dx, dy);//too slow...
    return Math.sqrt(dx * dx + dy * dy);
  }

  /**
   * @param _x1 x du point 1
   * @param _y1 y du point 1
   * @param _z1 z du point 1
   * @param _x2 x du point 2
   * @param _y2 y du point 2
   * @param _z2 z du point 2
   * @return Renvoie la distance entre le point p1(_x1,_y1,_z1) et le point p2(_x2,_y2,_z2)
   */
  public static double getDistance3D(final double _x1, final double _y1, final double _z1, final double _x2, final double _y2,
          final double _z2) {
    return Math.sqrt((_x2 - _x1) * (_x2 - _x1) + (_y2 - _y1) * (_y2 - _y1) + (_z2 - _z1) * (_z2 - _z1));
  }

  public static double getDistanceXY(final Coordinate _c1, final Coordinate _c2) {
    return getDistance(_c1.x, _c1.y, _c2.x, _c2.y);
  }

  /**
   * @param _x1 x du point 1
   * @param _y1 y du point 1
   * @param _x2 x du point 2
   * @param _y2 y du point 2
   * @return Renvoie la norme au carre.
   */
  public static double getD2(final double _x1, final double _y1, final double _x2, final double _y2) {
    return (_x2 - _x1) * (_x2 - _x1) + (_y2 - _y1) * (_y2 - _y1);
  }

  public static double getD2(final Coordinate _c1, final Coordinate _c2) {
    return getD2(_c1.x, _c1.y, _c2.x, _c2.y);
  }

  public static double getD2(final Point _c1, final Point _c2) {
    return getD2(_c1.getX(), _c1.getY(), _c2.getX(), _c2.getY());
  }

  /**
   * @param _segX1 x du point 1 du segment
   * @param _segY1 y du point 1 du segment
   * @param _segX2 x du point 2 du segment
   * @param _segY2 y du point 2 du segment
   * @param _x x du point a considerer
   * @param _y y du point a considerer
   * @return Renvoie la distance entre le point p(_x,_y) et le segment [P1,P2] P1=_segX1,_segY1. Tester dans ebli
   */
  public static double distanceFromSegment(final double _segX1, final double _segY1, final double _segX2, final double _segY2,
          final double _x,
          final double _y) {
    if ((_segX1 == _segX2) && (_segY1 == _segY2)) {
      return getDistance(_segX1, _segY1, _x, _y);
    }
    // P1=(_x1,_y1)
    // P2=(_x2,_y2)
    // X=thisPoint
    // vecteur A=X-P1
    // vecteur B=P2-P1
    // On calcule la projection de A sur B
    // le produit scalaire
    double p = (_x - _segX1) * (_segX2 - _segX1) + (_y - _segY1) * (_segY2 - _segY1);
    // si le point est "en dehors" du segment ,on prend la distance X P1
    if (p < 0) {
      return getDistance(_x, _y, _segX1, _segY1);
    }

    // le produit scalaire/norme de B
    p = p / ((_segX2 - _segX1) * (_segX2 - _segX1) + (_segY2 - _segY1) * (_segY2 - _segY1));
    // si p>1 p est en dehors du segment
    if (p > 1) {
      return getDistance(_x, _y, _segX2, _segY2);
    }
    // le vecteur projete a pour coordonnees PROJ:
    double projx = p * (_segX2 - _segX1);
    double projy = p * (_segY2 - _segY1);
    // le vecteur de norme (on reutilise le variable projx)
    projx = (_x - _segX1) - projx;
    projy = (_y - _segY1) - projy;
    return Math.sqrt(projx * projx + projy * projy);
//  too slow:  return Math.hypot(projx, projy);
  }

  //  public static double distanceAuCarreFromSegment(final double _segX1, final double _segY1, final double _segX2,
  //      final double _segY2, final double _x, final double _y) {
  //    if ((_segX1 == _segX2) && (_segY1 == _segY2)) { return getD2(_segX1, _segY1, _x, _y); }
  //    // P1=(_x1,_y1)
  //    // P2=(_x2,_y2)
  //    // X=thisPoint
  //    // vecteur A=X-P1
  //    // vecteur B=P2-P1
  //    // On calcule la projection de A sur B
  //    // le produit scalaire
  //    double p = (_x - _segX1) * (_segX2 - _segX1) + (_y - _segY1) * (_segY2 - _segY1);
  //    // si le point est "en dehors" du segment ,on prend la distance X P1
  //    if (p < 0) { return getD2(_x, _y, _segX1, _segY1); }
  //
  //    // le produit scalaire/norme de B
  //    p = p / ((_segX2 - _segX1) * (_segX2 - _segX1) + (_segY2 - _segY1) * (_segY2 - _segY1));
  //    // si p>1 p est en dehors du segment
  //    if (p > 1) { return getD2(_x, _y, _segX2, _segY2); }
  //    // le vecteur projete a pour coordonnees PROJ:
  //    double projx = p * (_segX2 - _segX1);
  //    double projy = p * (_segY2 - _segY1);
  //    // le vecteur de norme (on reutilise le variable projx)
  //    projx = (_x - _segX1) - projx;
  //    projy = (_y - _segY1) - projy;
  //    return projx * projx + projy * projy;
  //  }
  /**
   * Tester dans dodico TestMaillage.
   *
   * @param _x1 x de P1
   * @param _y1 y de P1
   * @param _x2 x de P2
   * @param _y2 y de P2
   * @param _x3 x de P3
   * @param _y3 y de P3
   * @return l'aire du triangle P1P2P3
   */
  public static double aireTriangle(final double _x1, final double _y1, final double _x2, final double _y2, final double _x3,
          final double _y3) {
    // ERROR aie
    double p = (_x3 - _x1) * (_x2 - _x1) + (_y3 - _y1) * (_y2 - _y1);
    final double d1d2 = getDistance(_x1, _y1, _x2, _y2);
    // le produit scalaire/norme de B
    p = p / (d1d2 * d1d2);
    // le vecteur projete a pour coordonnees PROJ:
    double projx = p * (_x2 - _x1);
    double projy = p * (_y2 - _y1);
    // le vecteur de norme (on reutilise le variable projx)
    projx = (_x3 - _x1) - projx;
    projy = (_y3 - _y1) - projy;
    return d1d2 * Math.sqrt(projx * projx + projy * projy) / 2;
  }

  /**
   * Donne la valeur de l'angle
   * <code>_angleInit</code> dans l'intervalle [-360;360].
   *
   * @param _angleInit l'angle a ajuster dans l'intervalle [-360;360]
   * @return l'equivalent dans l'intervalle [-360;360]
   */
  public static double getAngle(final double _angleInit) {
    return ((_angleInit > 360) || (_angleInit < -360)) ? _angleInit % 360 : _angleInit;
  }

  /**
   * une projection simple sans rotation ni d�formation de l'image.
   * Attention pour l'axe des y des images, l'axe est invers�
   * @param _ptImages
   * @param _ptReels
   * @return
   * @throws IllegalArgumentException
   */
  public static AffineTransform projection2Points(final Point2D[] _ptImages, final Point2D[] _ptReels) throws IllegalArgumentException {
    if ((_ptImages.length != _ptReels.length) || (_ptImages.length < 2)) {
      throw new IllegalArgumentException("badData");
    }
    Point2D ptImage1 = _ptImages[0];
    Point2D ptReel1 = _ptReels[0];

    double distReel = _ptReels[0].distance(_ptReels[1]);
    if (distReel < 1E-5) {
      return null;
    }
    double distPixel = _ptImages[0].distance(_ptImages[1]);
    if (distPixel < 1E-5) {
      return null;
    }
    Point2D[] newPtImage = new Point2D[3];
    Point2D[] newPtReel = new Point2D[3];
    newPtImage[0] = _ptImages[0];
    newPtImage[1] = _ptImages[1];
    //y inverse
    newPtImage[2] = new Point2D.Double(_ptImages[0].getX() - _ptImages[0].getY() + _ptImages[1].getY(),
            _ptImages[0].getY() - _ptImages[1].getX() + _ptImages[0].getX());

    newPtReel[0] = _ptReels[0];
    newPtReel[1] = _ptReels[1];
    newPtReel[2] = new Point2D.Double(
            _ptReels[0].getX() + _ptReels[0].getY() - _ptReels[1].getY(),
            _ptReels[0].getY() + _ptReels[1].getX() - _ptReels[0].getX());
    return projection(newPtImage, newPtReel);
  }

  public static AffineTransform projection(final Point2D[] _ptImages, final Point2D[] _ptReels) throws IllegalArgumentException {
    double[] proj = new double[6];
    if (_ptImages == null || _ptReels == null) {
      return null;
    }
    if (_ptImages.length == 2) {
      return projection2Points(_ptImages, _ptReels);
    }

    if ((_ptImages.length != _ptReels.length) || (_ptImages.length < 3)) {
      throw new IllegalArgumentException("badData");
    }

    if (_ptImages.length == 3) {
      try {
        // modif Gilbert : la division par 0 ne leve plus d'exception !!!
        final double diviseur = -_ptImages[2].getY() * _ptImages[0].getX() + _ptImages[2].getY() * _ptImages[1].getX() - _ptImages[0].getY()
                * _ptImages[1].getX() + _ptImages[1].getY() * _ptImages[0].getX() + _ptImages[0].getY() * _ptImages[2].getX() - _ptImages[1].getY()
                * _ptImages[2].getX();

        if (Math.abs(diviseur) < 0.0001) {
          throw new Exception("div0");
        }
        proj[0] = (float) ((-_ptImages[1].getY() * _ptReels[2].getX() - _ptImages[0].getY() * _ptReels[1].getX() + _ptImages[2].getY()
                * _ptReels[1].getX() + _ptImages[1].getY() * _ptReels[0].getX() + _ptImages[0].getY() * _ptReels[2].getX() - _ptImages[2].getY()
                * _ptReels[0].getX()) / diviseur);

        proj[1] = (float) (-(-_ptImages[1].getX() * _ptReels[2].getX() + _ptImages[2].getX() * _ptReels[1].getX() + _ptImages[1].getX()
                * _ptReels[0].getX() - _ptImages[0].getX() * _ptReels[1].getX() + _ptImages[0].getX() * _ptReels[2].getX() - _ptImages[2].getX()
                * _ptReels[0].getX()) / diviseur);

        proj[2] = (float) ((_ptImages[2].getX() * _ptImages[0].getY() * _ptReels[1].getX() - _ptImages[1].getX() * _ptImages[0].getY()
                * _ptReels[2].getX() + _ptImages[1].getY() * _ptImages[0].getX() * _ptReels[2].getX() + _ptImages[2].getY() * _ptImages[1].getX()
                * _ptReels[0].getX() - _ptImages[2].getY() * _ptImages[0].getX() * _ptReels[1].getX() - _ptImages[1].getY() * _ptImages[2].getX()
                * _ptReels[0].getX()) / diviseur);

        proj[3] = (float) (-1 / diviseur * (_ptImages[1].getY() * _ptReels[2].getY() - _ptImages[0].getY() * _ptReels[2].getY() - _ptImages[2].getY()
                * _ptReels[1].getY() - _ptImages[1].getY() * _ptReels[0].getY() + _ptImages[0].getY() * _ptReels[1].getY() + _ptImages[2].getY()
                * _ptReels[0].getY()));

        proj[4] = (float) ((_ptImages[1].getX() * _ptReels[2].getY() - _ptImages[1].getX() * _ptReels[0].getY() + _ptImages[0].getX()
                * _ptReels[1].getY() - _ptImages[0].getX() * _ptReels[2].getY() + _ptImages[2].getX() * _ptReels[0].getY() - _ptImages[2].getX()
                * _ptReels[1].getY()) / diviseur);

        proj[5] = (float) (-(_ptImages[2].getY() * _ptImages[0].getX() * _ptReels[1].getY() - _ptImages[2].getY() * _ptImages[1].getX()
                * _ptReels[0].getY() + _ptImages[1].getY() * _ptImages[2].getX() * _ptReels[0].getY() - _ptImages[1].getY() * _ptImages[0].getX()
                * _ptReels[2].getY() + _ptImages[1].getX() * _ptImages[0].getY() * _ptReels[2].getY() - _ptImages[2].getX() * _ptImages[0].getY()
                * _ptReels[1].getY()) / diviseur);

      } catch (final Exception e) {
        // MsgInfo erW = new MsgInfo(parent, MsgInfo.ERROR,"Verifiez que les points ne sont pas alignes !");
        throw new IllegalArgumentException("checkPts");
        // throw e;
      }
    } else {
      try {
        final int n = _ptImages.length;
        final int nc = 6;
        final double[] a = new double[2 * n * 6];
        final double[] b = new double[2 * n];
        for (int i = 0; i < n; i++) {
          b[2 * i] = _ptReels[i].getX();
          b[2 * i + 1] = _ptReels[i].getY();
          a[0 + 2 * i * nc] = _ptImages[i].getX();
          a[1 + 2 * i * nc] = _ptImages[i].getY();
          a[2 + 2 * i * nc] = 1.0;
          a[3 + (2 * i + 1) * nc] = _ptImages[i].getX();
          a[4 + (2 * i + 1) * nc] = _ptImages[i].getY();
          a[5 + (2 * i + 1) * nc] = 1.0;
        }
        proj = LocalSolve.leastsquare(a, b, 2 * n, nc);

        /*
         * System.out.println(ResourceManager.get("projMatrix")+"\n"+proj[0]+" "+proj[1]+" "+proj[2]+" "); System.out.println(proj[3]+" "+proj[4]+"
         * "+proj[5]+" ");
         */
      } catch (final Exception e) {
        FuLog.error(e);
        /*
         * //MsgInfo erW = new MsgInfo(parent, MsgInfo.ERROR,"Verifiez que les points ne sont pas alignes !"); throw new
         * Exception(ResourceManager.get("checkPts"));
         */
      }
    }
    return new AffineTransform(proj[0], proj[3], proj[1], proj[4], proj[2], proj[5]);
  }
}
