/*
 * @creation 7 oct. 2008
 * 
 * @modification $Date:$
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2008 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import java.util.ArrayList;
import java.util.List;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * Cet Adapter permet l'ajout d'attributs au DataModel.
 * 
 * @author Emmanuel MARTIN
 * @version $Id$
 */
public class GISDataModelAttributeAdapter implements GISDataModel {

  // Le model adapt�
  private GISDataModel dataModel_;
  // Les nouveaux attributs
  private List<GISAttributeInterface> newAttr_;
  private List<Object[]> newValues_;

  public GISDataModelAttributeAdapter(GISDataModel _dataModel) {
    dataModel_ = _dataModel;
    newAttr_ = new ArrayList<GISAttributeInterface>(1);
    newValues_ = new ArrayList<Object[]>(1);
  }

  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    GISDataModelAttributeAdapter res = new GISDataModelAttributeAdapter(dataModel_.createTranslate(xyToAdd));
    res.newAttr_ = newAttr_;
    res.newValues_ = newValues_;
    return res;
  }

  /**
   * Ajout un attribut pour tout les �l�ments si cet attribut n'existe pas d�j�. L'atomicit� (ou la globalit�) de
   * l'attribut est respect�.
   * 
   * @param _attr l'attribut a ajouter
   * @param _value une valeur unique qui sera dupliquer pour toutes les g�om�tries
   * @return 0 si l'attribut existe d�j� (il n'est pas remplac�) -2 si _attr ou _value sont null
   */
  public int addAttribut(GISAttributeInterface _attr, Object _value) {
    if (_attr == null || _value == null)
      return -2;
    if (dataModel_.getIndiceOf(_attr) == -1 && !newAttr_.contains(_attr)) {
      newAttr_.add(_attr);
      // Attribut atomique
      if (_attr.isAtomicValue()) {
        Object[] tmp = new Object[dataModel_.getNumGeometries()];
        for (int i = 0; i < tmp.length; i++)
          tmp[i] = _attr.createDataForGeom(_value, dataModel_.getGeometry(i).getNumPoints());
        newValues_.add(tmp);
      }
      // Attribut global
      else {
        Object[] tmp = new Object[dataModel_.getNumGeometries()];
        for (int i = 0; i < tmp.length; i++)
          tmp[i] = _value;
        newValues_.add(tmp);
      }
      return 1;
    } else return 0;
  }

  /**
   * Ajout un attribut pour tout les �l�ments si cet attribut n'existe pas d�j�. si _attr est un attribut atomique,
   * chaque valeur sera dupliqu� pour chaque sommet de la g�om�trie.
   * 
   * @param _attr l'attribut a ajouter
   * @param _values une valeur pour chaque g�om�trie.
   * @return 0 si l'attribut existe d�j� (il n'est pas remplac�) -1 en cas de tableau _values de mauvaise taille -2 si
   *         _attr ou _values sont null
   */
  public int addAttribut(GISAttributeInterface _attr, Object[] _values) {
    if (_attr == null || _values == null)
      return -2;
    if (dataModel_.getIndiceOf(_attr) == -1 && !newAttr_.contains(_attr)) {
      if (_values.length != dataModel_.getNumGeometries())
        return -1;
      // Attribut atomique
      if (_attr.isAtomicValue()) {
        Object[] tmp = new Object[dataModel_.getNumGeometries()];
        for (int i = 0; i < tmp.length; i++)
          tmp[i] = _attr.createDataForGeom(_values[i], dataModel_.getGeometry(i).getNumPoints());
        newAttr_.add(_attr);
        newValues_.add(tmp);
      }
      // Attribut global
      else {
        newAttr_.add(_attr);
        newValues_.add(_values);
      }
      return 1;
    } else return 0;
  }

  /**
   * Ajout un attribut pour tout les �l�ments si cet attribut n'existe pas d�j�. Cette m�thode n'est d�finie que pour
   * les attributs atomiques.
   * 
   * @param _attr l'attribut a ajouter
   * @param _values une valeur pour chaque sommet de chaque g�om�trie.
   * @return 0 si l'attribut existe d�j� (il n'est pas remplac�) -1 en cas de tableau _values de mauvaise taille -2 si
   *         _attr ou _values sont null -3 si l'attribut n'est pas atomique
   */
  public int addAttribut(GISAttributeInterface _attr, Object[][] _values) {
    if (_attr == null || _values == null)
      return -2;
    if (_attr.isAtomicValue())
      if (dataModel_.getIndiceOf(_attr) == -1 && !newAttr_.contains(_attr)) {
        if (_values.length != dataModel_.getNumGeometries())
          return -1;
        // Attribut atomique
        Object[] tmp = new Object[dataModel_.getNumGeometries()];
        for (int i = 0; i < tmp.length; i++) {
          if (_values[i].length != dataModel_.getGeometry(i).getNumPoints())
            return -1;
          tmp[i] = _attr.createDataForGeom(_values[i], _values[i].length);
        }
        newAttr_.add(_attr);
        newValues_.add(tmp);
        return 1;
      } else return 0;
    else return -3;
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getAttribute(int)
   */
  @Override
  public GISAttributeInterface getAttribute(int att) {
    if (att < 0)
      return null;
    if (dataModel_.getNbAttributes() > att)
      return dataModel_.getAttribute(att);
    else if (newAttr_.size() > att - dataModel_.getNbAttributes())
      return newAttr_.get(att - dataModel_.getNbAttributes());
    else return null;
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getDoubleValue(int, int)
   */
  @Override
  public double getDoubleValue(int att, int geom) {
    if (att >= 0 && geom >= 0) {
      if (dataModel_.getNbAttributes() > att)
        return dataModel_.getDoubleValue(att, geom);
      else if (newAttr_.size() > att - dataModel_.getNbAttributes())
        return (Double) newValues_.get(att)[geom];
      else return 0;
    }
    return 0;
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getEnvelopeInternal()
   */
  @Override
  public Envelope getEnvelopeInternal() {
    return dataModel_.getEnvelopeInternal();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getGeometry(int)
   */
  @Override
  public Geometry getGeometry(int geom) {
    if (geom < 0 || geom >= getNumGeometries())
      return null;
    return dataModel_.getGeometry(geom);
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getIndiceOf(org.fudaa.ctulu.gis.GISAttributeInterface)
   */
  @Override
  public int getIndiceOf(GISAttributeInterface _att) {
    if (dataModel_.getIndiceOf(_att) != -1)
      return dataModel_.getIndiceOf(_att);
    else if (CtuluLibArray.findObjectEgalEgal(newAttr_, _att) != -1)
      return dataModel_.getNbAttributes() + CtuluLibArray.findObjectEgalEgal(newAttr_, _att);
    else return -1;
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getNbAttributes()
   */
  @Override
  public int getNbAttributes() {
    return dataModel_.getNbAttributes() + newAttr_.size();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getNumGeometries()
   */
  @Override
  public int getNumGeometries() {
    return dataModel_.getNumGeometries();
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#getValue(int, int)
   */
  @Override
  public Object getValue(int att, int geom) {
    if (att < 0 || geom < 0)
      return null;
    if (dataModel_.getNbAttributes() > att && geom < dataModel_.getNumGeometries())
      return dataModel_.getValue(att, geom);
    else if (newAttr_.size() > att - dataModel_.getNbAttributes() && geom < dataModel_.getNumGeometries())
      return newValues_.get(att - dataModel_.getNbAttributes())[geom];
    return null;
  }

  /* (non-Javadoc)
   * @see org.fudaa.ctulu.gis.GISDataModel#preload(org.fudaa.ctulu.gis.GISAttributeInterface[], org.fudaa.ctulu.ProgressionInterface)
   */
  @Override
  public void preload(GISAttributeInterface[] _att, ProgressionInterface _prog) {
    dataModel_.preload(_att, _prog);
  }
}
