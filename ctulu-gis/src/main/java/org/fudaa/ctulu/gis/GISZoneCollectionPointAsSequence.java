package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.Envelope;

public class GISZoneCollectionPointAsSequence implements CoordinateSequence {
  private final GISZoneCollectionPoint collectionPoint;

  public GISZoneCollectionPointAsSequence(GISZoneCollectionPoint collectionPoint) {
    this.collectionPoint = collectionPoint;
  }

  @Override
  public int getDimension() {
    return collectionPoint.getDimension();
  }

  @Override
  public Coordinate getCoordinate(int i) {
    return collectionPoint.getCoordinate(i);
  }

  @Override
  public Coordinate getCoordinateCopy(int i) {
    return collectionPoint.getCoordinateCopy(i);
  }

  @Override
  public void getCoordinate(final int index, final Coordinate coordinate) {
    collectionPoint.getCoordinate(index, coordinate);
  }

  @Override
  public double getX(int index) {
    return collectionPoint.getX(index);
  }

  @Override
  public double getY(int index) {
    return collectionPoint.getY(index);
  }

  @Override
  public double getOrdinate(int index, int ordinateIndex) {
    return collectionPoint.getOrdinate(index, ordinateIndex);
  }

  @Override
  public int size() {
    return collectionPoint.getNumPoints();
  }

  @Override
  public void setOrdinate(int index, int ordinateIndex, double value) {
    throw new IllegalAccessError("not implemented");
  }

  @Override
  public Coordinate[] toCoordinateArray() {
    return collectionPoint.toCoordinateArray();
  }

  @Override
  public Envelope expandEnvelope(Envelope env) {
    return collectionPoint.expandEnvelope(env);
  }

  @Override
  public Object clone() {
    return copy();
  }

  @Override
  public CoordinateSequence copy() {
    final int size = size();
    Coordinate[] coordinates=new Coordinate[size];
    for (int i = 0; i < size; i++) {
      coordinates[i]=getCoordinateCopy(i);
    }
    return new GISCoordinateSequenceFactory().create(coordinates);
  }
}
