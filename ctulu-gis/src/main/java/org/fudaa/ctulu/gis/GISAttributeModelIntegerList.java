/*
 * @creation 7 avr. 2005
 * 
 * @modification $Date: 2007-01-10 08:58:47 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import gnu.trove.TIntArrayList;
import gnu.trove.TIntIntHashMap;
import gnu.trove.TIntIntIterator;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.collection.CtuluListInteger;

/**
 * @author Fred Deniger
 * @version $Id: GISAttributeModelIntegerList.java,v 1.9 2007-01-10 08:58:47 deniger Exp $
 */
public class GISAttributeModelIntegerList extends CtuluListInteger implements GISAttributeModelIntegerInterface,
    GISAttributeModelObservable {

  /**
   *
   */
  protected GISAttributeModelIntegerList() {
    super();
  }

  private GISAttributeModelIntegerList(final TIntArrayList _l, final GISAttributeModelIntegerList _model) {
    super(0);
    list_ = _l;
    attribute_ = _model.getAttribute();
    listener_ = _model.getListener();
  }

  @Override
  public Object getAverage() {
    if (list_.size() == 0) { return Integer.valueOf(0); }
    return new Integer((int) CtuluLibArray.getMoyenne(list_));
  }

  @Override
  public GISAttributeModel createSubModel(final int[] _idxToRemove) {
    final TIntArrayList newList = new TIntArrayList(getSize());
    final int nb = getSize();
    for (int i = 0; i < nb; i++) {
      if (Arrays.binarySearch(_idxToRemove, i) < 0) {
        newList.add(list_.get(i));
      }
    }
    return new GISAttributeModelIntegerList(newList, this);
  }

  /**
   * @param _init
   * @param _attr l'attribut
   */
  public GISAttributeModelIntegerList(final CtuluListInteger _init, final GISAttributeInterface _attr) {
    super(_init);
    attribute_ = _attr;
    if (attribute_ == null) { throw new IllegalArgumentException("attribute is null"); }
  }

  /**
   * @param _init
   * @param _attr l'attribut
   */
  public GISAttributeModelIntegerList(final int[] _init, final GISAttributeInterface _attr) {
    super(_init);
    attribute_ = _attr;
    if (attribute_ == null) { throw new IllegalArgumentException("attribute is null"); }
  }

  /**
   * @param _initCapacity la capacit� initiale
   * @param _attr l'attribut
   */
  public GISAttributeModelIntegerList(final int _initCapacity, final GISAttributeInterface _attr) {
    super(_initCapacity);
    attribute_ = _attr;
  }

  @Override
  public GISAttributeModel createUpperModel(final int _numObject, final TIntIntHashMap _newIdxOldIdx) {
    if (_numObject < getSize()) { throw new IllegalArgumentException("bad num objects"); }
    if (_numObject == getSize()) { return this; }
    final TIntArrayList newList = new TIntArrayList(_numObject);
    newList.fill(((Integer) getAttribute().getDefaultValue()).intValue());
    if (_newIdxOldIdx != null) {
      final TIntIntIterator it = _newIdxOldIdx.iterator();
      for (int i = _newIdxOldIdx.size(); i-- > 0;) {
        it.advance();
        final int newIdx = it.key();
        newList.set(newIdx, list_.get(it.value()));
      }
    }
    return new GISAttributeModelIntegerList(newList, this);
  }

  /**
   * @param _init
   */
  public GISAttributeModelIntegerList(final TIntArrayList _init) {
    super(_init);
  }

  GISAttributeInterface attribute_;

  protected transient GISAttributeListener listener_;

  @Override
  protected void fireObjectChanged(int _indexGeom, Object _newValue){
    if (listener_ != null) {
      listener_.gisDataChanged(attribute_, _indexGeom, _newValue);
    }
  }

  @Override
  public GISAttributeModel deriveNewModel(final int _numObject, final GISReprojectInterpolateurI _interpol) {
    if (!(_interpol instanceof GISReprojectInterpolateurI.IntegerTarget)) { throw new IllegalArgumentException(
        "bad interface"); }
    return deriveNewModel(_numObject, (GISReprojectInterpolateurI.IntegerTarget) _interpol);
  }

  @Override
  public GISAttributeModelIntegerInterface deriveNewModel(final int _numObject,
      final GISReprojectInterpolateurI.IntegerTarget _interpol) {
    final TIntArrayList newList = new TIntArrayList(_numObject);
    for (int i = 0; i < _numObject; i++) {
      newList.add(_interpol.interpol(i));
    }
    return new GISAttributeModelIntegerList(newList, this);
  }

  protected final GISAttributeListener getListener() {
    return listener_;
  }

  protected final void setAttribute(final GISAttributeInterface _attribute) {
    attribute_ = _attribute;
  }

  @Override
  public final void setListener(final GISAttributeListener _listener) {
    listener_ = _listener;
  }

  /**
   * @return l'attribut associe
   */
  @Override
  public final GISAttributeInterface getAttribute() {
    return attribute_;
  }

}
