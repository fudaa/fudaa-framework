/*
 * @creation 5 d�c. 06
 * @modification $Date: 2007-02-02 11:20:05 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.LinearRing;
import org.locationtech.jts.geom.MultiPoint;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import java.util.ArrayList;
import java.util.List;

/**
 * Un visiteur pour collectionner les g�om�tries d'un type sp�cifi� par le masque.
 * @author fred deniger
 * @version $Id: GISVisitorLigneCollector.java,v 1.2 2007-02-02 11:20:05 deniger Exp $
 */
public class GISVisitorGeometryCollector extends GISVisitorDefault {

  final List<Geometry> l_ = new ArrayList<Geometry>();
  int mask_=GISLib.MASK_ALL;

  public GISVisitorGeometryCollector(final int _mask) {
    super();
    mask_=_mask;
  }

  @Override
  public boolean visitPoint(final Point _p) {
    if ((mask_&GISLib.MASK_POINT)!=0) l_.add(_p);
    return true;
  }

  @Override
  public boolean visitPolygone(final LinearRing _p) {
    if ((mask_&GISLib.MASK_POLYGONE)!=0) l_.add(_p);
    return true;
  }

  @Override
  public boolean visitPolygoneWithHole(final Polygon _p) {
    return true;
  }

  @Override
  public boolean visitPolyligne(final LineString _p) {
    if ((mask_&GISLib.MASK_POLYLINE)!=0) l_.add(_p);
    return true;
  }

  @Override
  public boolean visitMultiPoint(final MultiPoint _p) {
    if ((mask_&GISLib.MASK_MULTIPOINT)!=0) l_.add(_p);
    return true;
  }

  public List<Geometry> getGeometries() {
    return l_;
  }

}
