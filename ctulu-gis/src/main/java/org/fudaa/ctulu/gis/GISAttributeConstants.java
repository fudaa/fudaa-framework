/*
 *  @creation     7 avr. 2005
 *  @modification $Date$
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import com.memoire.fu.FuLib;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.editor.CtuluValueEditorChoice;
import org.fudaa.ctulu.editor.CtuluValueEditorDefaults;

/**
 * Une classe pour d�finir des attributs syst�mes associables � des objets GIS. Les attributs syst�mes
 * sont en principe des attributs programme (non construits par l'utilisateur). Le systeme connait
 * ces attributs sp�cifiques, et peut r�agir en cons�quence.<p>
 * L'ID de chacun de ces attributs est ind�pendant de la langue pour �tre correctement sauv� dans 
 * les fichiers GML.
 * 
 * @author Fred Deniger
 * @version $Id$
 */
public final class GISAttributeConstants {

  /** Etat geom modifi�. */
  public final static String ATT_VAL_ETAT_MODI="MODI";
  /** Etat geom origine */
  public final static String ATT_VAL_ETAT_ORIG="ORIG";
  /** Une valeur d'attribut valant True */
  public final static String ATT_VAL_TRUE="T";
  /** Une valeur d'attribut valant False */
  public final static String ATT_VAL_FALSE="F";
  /** Une valeur d'attribut dans le commentaire hydraulique. */
  public final static String ATT_COMM_HYDRO_PK="PK";

  /** Nature Semis */
  public final static String ATT_NATURE_SM="SM";
  /** Nature Courbe niveau */
  public final static String ATT_NATURE_CN="CN";
  /** Nature Profil */
  public final static String ATT_NATURE_PF="PF";
  /** Nature Ligne directrice */
  public final static String ATT_NATURE_LD="LD";
  /** Nature Ligne de contrainte */
  public final static String ATT_NATURE_LC="LC";
  /** Nature Trace de profil */
  public final static String ATT_NATURE_TP="TP";
  /** Nature Zone */
  public final static String ATT_NATURE_ZN="ZN";
  /** Nature Contour d'�tude */
  public final static String ATT_NATURE_CE="CE";
  /** Nature axe hydraulique */
  public final static String ATT_NATURE_AH="AH";
  /** Nature rive (gauche ou droite) */
  public final static String ATT_NATURE_RV="RV";
  /** Nature limite de stockage */
  public final static String ATT_NATURE_LS="LS";
  /** Nature casier */
  public final static String ATT_NATURE_CA="CA";
  
  /**
   * Un attribut nom, global.
   */
  public final static GISAttributeString TITRE = new GISAttributeString(CtuluValueEditorDefaults.EXPR_STRING_EDITOR,CtuluLib.getS("Nom")) {
    /** Numero d'identifiant, incr�ment� � chaque nouvelle cr�ation de nom. */
    private int numid_=0;
    String def_ = CtuluLib.getS("<Defaut>");

    @Override
    public String getID() {
      return "ATTRIBUTE_TITLE";
    }
    
    @Override
    protected Object createGlobalValues(final Object _initValues) {
      Object r = _initValues;
      // Si la valeur en entr�e est la valeur par d�faut, alors une nouvelle valeur en sortie est cr��e.
      // Ceci permet d'affecter automatiquement un nom nouveau � une g�om�trie cr��e.
      if (r == null || def_.equals(r)) {
        return r="Geom_"+(++numid_);
      }
      else {
        return super.createGlobalValues(_initValues);
      }
    }

    @Override
    public Object getDefaultValue() {
      return def_;
    }
  };
  
  /**
   * Un attribut nature, global.
   */
  public final static GISAttributeString NATURE = new GISAttributeString(CtuluLib.getS("Nature"), false) {
    @Override
    public String getID() {
      return "ATTRIBUTE_NATURE";
    }

    @Override
    public boolean isEditable() {
      return false;
    }
  };
  
  /**
   * Un attribut etat, global a la g�om�trie.
   */
  public final static GISAttributeString ETAT_GEOM=
    new GISAttributeString(new CtuluValueEditorChoice(new String[]{ATT_VAL_ETAT_ORIG, ATT_VAL_ETAT_MODI},
        new String[]{CtuluLib.getS("Origine"), CtuluLib.getS("Modifi�")}), CtuluLib.getS("Etat"), false) {
    @Override
    public String getID() {
      return "ATTRIBUTE_ETAT_GEOM";
    }

    @Override
    protected Object createGlobalValues(Object _initValues) {
      if (_initValues instanceof Object[]) {
        // Si au moins une des valeurs est "modifi�", on retourne "modifi�".
        boolean modifie=false;
        int i=-1;
        while (!modifie&&++i<((Object[])_initValues).length)
          modifie=((Object[])_initValues)[i]==ATT_VAL_ETAT_MODI;
        if (modifie)
          _initValues=ATT_VAL_ETAT_MODI;
        else
          _initValues=ATT_VAL_ETAT_ORIG;
      }
      return super.createGlobalValues(_initValues);
    }
    
    @Override
    public boolean isEditable() {
      return true;
    }

    @Override
    public Object getDefaultValue() {
      return ATT_VAL_ETAT_ORIG;
    }
  };
  
  /**
   * Un attribut visibilit�, global a la g�om�trie.
   */
  public final static GISAttributeString VISIBILITE = 
    new GISAttributeString(new CtuluValueEditorChoice(new String[]{ATT_VAL_TRUE, ATT_VAL_FALSE},
        new String[]{CtuluLib.getS("Oui"), CtuluLib.getS("Non")}), CtuluLib.getS("Visibilit�"), false) {
    
    @Override
    public String getID() {
      return "ATTRIBUTE_VISIBILITY";
    }

    @Override
    public boolean isEditable() {
      return true;
    }

    @Override
    public Object getDefaultValue() {
      return ATT_VAL_TRUE;
    }
  };
  
  /**
   * Un attribut commentaire hydraulique, utilis� dans la lecture/ecriture des fichiers.
   */
  public final static GISAttributeString COMMENTAIRE_HYDRO = new GISAttributeString(CtuluLib.getS("Commentaire hydro"), false) {
    @Override
    public String getID() {
      return "ATTRIBUTE_HYDRO_COMMENT";
    }

    /**
     * La comparaison se fait uniquement sur le PK pour le moment.
     */
    @Override
    public Comparator<?> getComparator() {
      return new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
          double v=GISLib.getHydroCommentDouble(o1,ATT_COMM_HYDRO_PK)-GISLib.getHydroCommentDouble(o2,ATT_COMM_HYDRO_PK);
          return v==0 ? 0 : v>0 ? 1 : -1;
        }
      };
    }
    
    
  };
  
  /**
   * Un attribut historique, utilis� pour le stockage des manipulations sur une g�om�trie.
   */
  public final static GISAttributeString HISTORY = new GISAttributeString(CtuluLib.getS("Historique"), false) {
    @Override
    public String getID() {
      return "ATTRIBUTE_HISTORY";
    }
  };
  
  /**
   * Un attribut label, atomique.
   */
  public final static GISAttributeString LABEL = new GISAttributeString(CtuluLib.getS("Label"), true) {
    @Override
    public String getID() {
      return "ATTRIBUTE_LABEL";
    }
  };
  
  /**
   * Un attribut bathy, atomique.
   */
  public final static GISAttributeDouble BATHY = new GISAttributeDouble(CtuluLib.getS("z"), true) {

    @Override
    public String getID() {
      return "ATTRIBUTE_Z";
    }
  };
  
  /**
   * Un attribut index de g�om�trie.
   * Attention : Il n'est utilis� que lors de la sauvegarde, et n'apparait pas dans les calques autrement.
   */
  public final static GISAttributeInteger INDEX_GEOM = new GISAttributeInteger(CtuluLib.getS("Index"), false) {

    @Override
    public String getID() {
      return "ATTRIBUTE_INDEX_GEOM";
    }
  };

  /** La liste des attributs syst�mes. */
  protected final static List<GISAttribute> attrs_=
    Arrays.asList(new GISAttribute[]{BATHY,TITRE,NATURE,VISIBILITE,ETAT_GEOM,COMMENTAIRE_HYDRO,LABEL});
  
  private GISAttributeConstants() {}

  public static String toString(final GISAttributeInterface _att) {
    for (GISAttributeInterface att: attrs_) {
      if (_att==att) return _att.getID();
    }
    final String attributeSep = getAttributeSep();
    return _att.getDataClass().getName() + attributeSep + FuLib.replace(_att.getName(), attributeSep, "_")
        + attributeSep + CtuluLibString.toString(_att.isAtomicValue());
  }

  private static String getAttributeSep() {
    return "|";
  }

  /**
   * Retourne l'attribut syst�me a partir de son identifiant.
   * @param _idName L'identifiant.
   * @return L'attribut syst�me, ou null si aucun ne correspond a l'identifiant.
   */
  public static GISAttributeInterface getConstantAttribute(final String _idName) {
    for (GISAttributeInterface att: attrs_) {
      if (att.getID().equals(_idName)) return att;
    }
    return null;
  }
  public static GISAttributeInterface getConstantAttributeFromName(final String _idName) {
    for (GISAttributeInterface att: attrs_) {
      if (att.getName().equals(_idName)) return att;
    }
    return null;
  }

  public static GISAttributeInterface restoreFrom(final String _s) {
    GISAttributeInterface res = getConstantAttribute(_s);
    if (res != null) {
      return res;
    }
    final String[] s = CtuluLibString.parseString(_s, getAttributeSep());
    if (s == null || s.length != 3) {
      return null;
    }
    final String clazz = s[0];
    final String name = s[1];
    final boolean atomic = CtuluLibString.toBoolean(s[2]);
    if (clazz.equals(Double.class.getName())) {
      res = new GISAttributeDouble(name, atomic);
      if (BATHY.isSameContent(res)) {
        res = BATHY;
      }
    } else if (clazz.equals(Integer.class.getName())) {
      res = new GISAttributeInteger(name, atomic);
    } else {
      res = new GISAttributeString(name, atomic);
      if (TITRE.isSameContent(res)) {
        res = TITRE;
      }
      else if (NATURE.isSameContent(res)) {
        res = NATURE;
      }
      else if (ETAT_GEOM.isSameContent(res)) {
        res = ETAT_GEOM;
      }
      else if (VISIBILITE.isSameContent(res)) {
        res = VISIBILITE;
      }
      else if (COMMENTAIRE_HYDRO.isSameContent(res)) {
        res = COMMENTAIRE_HYDRO;
      }
    }

    return res;
  }

  /**
   * Retourne la liste des attributs syst�mes.
   * @return La liste.
   */
  public static List<GISAttribute> getDefaults() {
    return new ArrayList<GISAttribute>(attrs_); // Une copie, pour eviter les modifications.
  }

  /**
   * L'attribut est-il un attribut syst�me ?
   * @param _att L'attribut a tester.
   * @return True si l'attribut est systeme.
   */
  public static boolean isConstant(final GISAttributeInterface _att) {
    for (GISAttributeInterface att: attrs_) {
      if (_att==att) return true;
    }
    return false;
  }
  
  // Attributs 1D \\

  /**
   * Attribut contenant l'index du point correspondant � l'intersection entre
   * une rive gauche et un profil.
   */
  public final static GISAttributeInteger INTERSECTION_RIVE_GAUCHE=new GISAttributeInteger(CtuluLib.getS("Intersection rive gauche"), false){
    @Override
    public String getID() {
      return "ATTRIBUTE_INTERSECTION_RIVE_GAUCHE";
    }

    @Override
    public boolean isUserVisible() {
      return false;
    }

    @Override
    public boolean isEditable() {
      return false;
    }
  };

  /**
   * Attribut contenant l'index du point correspondant � l'intersection entre
   * une rive droite et un profil.
   */
  public final static GISAttributeInteger INTERSECTION_RIVE_DROITE=new GISAttributeInteger(CtuluLib.getS("Intersection rive droite"), false){
    @Override
    public String getID() {
      return "ATTRIBUTE_INTERSECTION_RIVE_DROITE";
    }

    @Override
    public boolean isUserVisible() {
      return false;
    }

    @Override
    public boolean isEditable() {
      return false;
    }
  };

  /**
   * Attribut contenant l'index du point correspondant � l'intersection entre
   * une limite de stockage gauche et un profil.
   */
  public final static GISAttributeInteger INTERSECTION_LIMITE_STOCKAGE_GAUCHE=new GISAttributeInteger(CtuluLib
      .getS("Intersection limite stockage gauche"), false){
    @Override
    public String getID() {
      return "ATTRIBUTE_INTERSECTION_LIMITE_STOCKAGE_GAUCHE";
    }

    @Override
    public boolean isUserVisible() {
      return false;
    }

    @Override
    public boolean isEditable() {
      return false;
    }
  };

  /**
   * Attribut contenant l'index du point correspondant � l'intersection entre
   * une limite de stockage droite et un profil.
   */
  public final static GISAttributeInteger INTERSECTION_LIMITE_STOCKAGE_DROITE=new GISAttributeInteger(CtuluLib
      .getS("Intersection limite stockage droite"), false){
    @Override
    public String getID() {
      return "ATTRIBUTE_INTERSECTION_LIMITE_STOCKAGE_DROITE";
    }

    @Override
    public boolean isUserVisible() {
      return false;
    }

    @Override
    public boolean isEditable() {
      return false;
    }
  };
  
  /**
   * Attribut contenant l'index des points correspondants aux l'intersections entre
   * les lignes directrices et un profil. Ces index sont stock�s dans une {@link GISAttributeModelIntegerList}.
   */
  public final static GISAttribute INTERSECTIONS_LIGNES_DIRECTRICES=new GISAttribute(null, CtuluLib
      .getS("Intersections lignes directrices"), false){
    @Override
    public String getID() {
      return "ATTRIBUTE_INTERSECTIONS_LIGNES_DIRECTRICES";
    }
    @Override
    protected Object createGlobalValues(Object _initValues) {
      if (_initValues instanceof GISAttributeModelIntegerList)
        return _initValues;
      else
        return getDefaultValue();
    };

    @Override
    public boolean isUserVisible() {
      return false;
    }

    @Override
    public boolean isEditable() {
      return false;
    }
  };
  
  /**
   * Attribut contenant le d�calage curviligne de l'axe hydraulique.
   */
  public final static GISAttribute CURVILIGNE_DECALAGE=new GISAttributeDouble(CtuluLib.getS("D�calage curviligne"), false) {
   @Override
   public String getID() {
     return "ATTRIBUTE_CURVILIGNE_DECALAGE";
   }
  };
}
