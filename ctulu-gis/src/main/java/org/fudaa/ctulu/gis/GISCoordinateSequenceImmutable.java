/*
 *  @creation     22 avr. 2005
 *  @modification $Date: 2006-09-19 14:36:53 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import com.memoire.fu.FuLog;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;
import gnu.trove.TDoubleArrayList;

/**
 * @author Fred Deniger
 * @version $Id: GiSCoordinateSequenceImmutable.java,v 1.6 2006-09-19 14:36:53 deniger Exp $
 */
public final class GISCoordinateSequenceImmutable extends GISCoordinateSequence {

  public GISCoordinateSequenceImmutable() {
    super(new Coordinate[0]);
  }

  /**
   * @param _coordinates
   */
  public GISCoordinateSequenceImmutable(final Coordinate[] _coordinates) {
    super(_coordinates);
  }

  /**
   * @param _l
   */
  public GISCoordinateSequenceImmutable(final TDoubleArrayList _l) {
    super(_l);
  }

  /**
   * @param _coordSeq
   */
  public GISCoordinateSequenceImmutable(final CoordinateSequence _coordSeq) {
    super(_coordSeq);
  }

  /**
   * @param _size
   */
  public GISCoordinateSequenceImmutable(final int _size) {
    super(_size);
  }

  private void error() {
    FuLog.error("not supported");
  }

  @Override
  public void setZ(final int _i, final double _value) {

  }

  @Override
  public void setOrdinate(final int _index, final int _ordinate, final double _value) {
    error();
  }

  @Override
  public void setX(final int _index, final double _value) {
    error();
  }

  @Override
  public void setY(final int _index, final double _value) {
    error();
  }
}
