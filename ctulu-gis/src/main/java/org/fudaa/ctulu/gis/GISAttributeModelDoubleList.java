/*
 *  @creation     7 avr. 2005
 *  @modification $Date: 2007-04-16 16:33:52 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import gnu.trove.TDoubleArrayList;
import gnu.trove.TIntIntHashMap;
import gnu.trove.TIntIntIterator;
import java.util.Arrays;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.collection.CDoubleArrayList;
import org.fudaa.ctulu.collection.CtuluListDouble;

/**
 * @author Fred Deniger
 * @version $Id: GISAttributeModelDoubleList.java,v 1.12 2007-04-16 16:33:52 deniger Exp $
 */
public class GISAttributeModelDoubleList extends CtuluListDouble implements GISAttributeModelDoubleInterface,
    GISAttributeModelObservable {

  protected transient GISAttributeListener listener_;

  GISAttributeInterface attribute_;

  private GISAttributeModelDoubleList(final CDoubleArrayList _l, final GISAttributeModelDoubleList _model) {
    super(0);
    list_ = _l;
    attribute_ = _model.getAttribute();
    listener_ = _model.getListener();
  }

  protected GISAttributeModelDoubleList() {
    super();
  }

  /**
   * @param _init
   */
  public GISAttributeModelDoubleList(final CtuluListDouble _init) {
    super(_init);
  }

  /**
   * @param _init
   */
  public GISAttributeModelDoubleList(final double[] _init) {
    super(_init);
  }

  /**
   * @param _initCapacity
   * @param _attr l'attribut
   */
  public GISAttributeModelDoubleList(final int _initCapacity, final GISAttributeInterface _attr) {
    super(_initCapacity);
    attribute_ = _attr;
  }

  public GISAttributeModelDoubleList( final int _initValues,final int _initCapacity,final GISAttributeInterface _attr) {
    super(_initValues,_initCapacity);
    attribute_ = _attr;
  }

  /**
   * @param _init
   */
  public GISAttributeModelDoubleList(final TDoubleArrayList _init) {
    super(_init);
  }

  @Override
  protected void fireObjectChanged(int _indexGeom, Object _newValue){
    if (listener_ != null) {
      listener_.gisDataChanged(attribute_, _indexGeom, _newValue);
    }
  }

  protected final GISAttributeListener getListener() {
    return listener_;
  }

  @Override
  public GISAttributeModel createSubModel(final int[] _idxToRemove) {
    final CDoubleArrayList newList = new CDoubleArrayList(getSize());
    final int nb = getSize();
    for (int i = 0; i < nb; i++) {
      if (Arrays.binarySearch(_idxToRemove, i) < 0) {
        newList.add(list_.get(i));
      }
    }
    return new GISAttributeModelDoubleList(newList, this);
  }

  @Override
  public GISAttributeModel createUpperModel(final int _numObject, final TIntIntHashMap _newIdxOldIdx) {
    if (_numObject < getSize()) {
      throw new IllegalArgumentException("bad num objects");
    }
    if (_numObject == getSize()) {
      return this;
    }
    final CDoubleArrayList newList = new CDoubleArrayList(_numObject);
    newList.fill(((Double) getAttribute().getDefaultValue()).doubleValue());
    if (_newIdxOldIdx != null) {
      final TIntIntIterator it = _newIdxOldIdx.iterator();
      for (int i = _newIdxOldIdx.size(); i-- > 0;) {
        it.advance();
        final int newIdx = it.key();
        newList.set(newIdx, list_.get(it.value()));
      }
    }
    return new GISAttributeModelDoubleList(newList, this);
  }

  @Override
  public GISAttributeModel deriveNewModel(final int _numObject, final GISReprojectInterpolateurI _interpol) {
    if (!(_interpol instanceof GISReprojectInterpolateurI.DoubleTarget)) {
      throw new IllegalArgumentException("bad interface");
    }
    return deriveNewModel(_numObject, (GISReprojectInterpolateurI.DoubleTarget) _interpol);
  }

  @Override
  public GISAttributeModelDoubleInterface deriveNewModel(final int _numObject,
      final GISReprojectInterpolateurI.DoubleTarget _interpol) {
    final  CDoubleArrayList newList = new CDoubleArrayList(_numObject);
    for (int i = 0; i < _numObject; i++) {
      newList.add(_interpol.interpol(i));
    }
    return new GISAttributeModelDoubleList(newList, this);
  }

  /**
   * @return l'attribut associe
   */
  @Override
  public final GISAttributeInterface getAttribute() {
    return attribute_;
  }

  @Override
  public Object getAverage() {
    if (list_.size() == 0) {
      return CtuluLib.ZERO;
    }
    return CtuluLib.getDouble(CtuluLibArray.getMoyenne(list_));
  }

  @Override
  public final void setListener(final GISAttributeListener _listener) {
    listener_ = _listener;
  }

}
