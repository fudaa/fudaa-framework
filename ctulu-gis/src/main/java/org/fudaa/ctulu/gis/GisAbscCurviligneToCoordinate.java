package org.fudaa.ctulu.gis;

import org.locationtech.jts.algorithm.CGAlgorithms;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.CoordinateSequence;

/**
 *
 * @author deniger ( genesis)
 */
public class GisAbscCurviligneToCoordinate {

  public static class Result {

    int segmentidx = -1;
    Coordinate coordinate;

    public void reset() {
      segmentidx = -1;
    }

    public Coordinate getCoordinate() {
      return coordinate;
    }

    public int getSegmentidx() {
      return segmentidx;
    }

    public boolean isFound() {
      return segmentidx >= 0;
    }
  }

  public static Result find(CoordinateSequence polyligne, double pourcentage) {
    Result res = new Result();
    find(polyligne, pourcentage, res);
    return res;
  }

  public static Result find(GISCoordinateSequenceContainerInterface polyligne, double pourcentage) {
    Result res = new Result();
    find(polyligne, pourcentage, res);
    return res;
  }

  public static void find(GISCoordinateSequenceContainerInterface polyligne, double pourcentage, Result res) {
    find(polyligne.getCoordinateSequence(), pourcentage, res);
  }

  /**
   *
   * @param coordinateSequence
   * @param pourcentage >=0 and <1
   * @return
   */
  public static void find(CoordinateSequence coordinateSequence, double pourcentage, Result res) {
    res.reset();
    if (coordinateSequence == null || coordinateSequence.size() < 2) {
      return;
    }
    final int size = coordinateSequence.size();
    if (pourcentage <= 0) {
      res.segmentidx = 0;
      if (res.coordinate == null) {
        res.coordinate = coordinateSequence.getCoordinateCopy(0);
      } else {
        res.coordinate.setCoordinate(coordinateSequence.getCoordinate(0));
      }
      return;
    }
    if (pourcentage >= 1) {
      res.segmentidx = size - 2;
      if (res.coordinate == null) {
        res.coordinate = coordinateSequence.getCoordinateCopy(size - 1);
      } else {
        res.coordinate.setCoordinate(coordinateSequence.getCoordinate(size - 1));
      }
      return;
    }
    double length = CGAlgorithms.length(coordinateSequence);
    double targetLength = pourcentage * length;
    double begin = 0;
    double end = 0;
    res.segmentidx = 0;
    for (int i = 1; i < size; i++) {
      Coordinate c1 = coordinateSequence.getCoordinate(i - 1);
      Coordinate c2 = coordinateSequence.getCoordinate(i);
      final double segmentLength = c2.distance(c1);
      end = begin + segmentLength;
      if (segmentLength > 0) {
        if (targetLength >= begin && targetLength <= end) {
          if (res.coordinate == null) {
            res.coordinate = new Coordinate();
          }
          double ratio = (targetLength - begin) / segmentLength;
          res.coordinate.x = c1.x + ratio * (c2.x - c1.x);
          res.coordinate.y = c1.y + ratio * (c2.y - c1.y);

          return;
        }
      }
      //we move to next segment
      begin = end;
      res.segmentidx++;
    }
    //non trouve:
    res.segmentidx = -1;
  }
}
