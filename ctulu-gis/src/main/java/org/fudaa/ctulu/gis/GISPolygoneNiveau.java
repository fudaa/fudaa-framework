/*
 *  @creation     25 mai 2005
 *  @modification $Date: 2006-07-13 13:34:35 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.CoordinateSequence;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LinearRing;

/**
 * @author Fred Deniger
 * @version $Id: GISPolygoneNiveau.java,v 1.4 2006-07-13 13:34:35 deniger Exp $
 */
public class GISPolygoneNiveau extends GISPolygone {



  /**
   * @param _points
   */
  public GISPolygoneNiveau(final CoordinateSequence _points) {
    super(_points);
  }



  /**
   *
   */
  public GISPolygoneNiveau() {
    super();
  }


  /**
   * @return pour definir si c'est une courbe de niveau
   */
  @Override
  public boolean isNiveau(){
    return true;
  }

  /**
   * @param _points
   * @param _factory
   */
  public GISPolygoneNiveau(final CoordinateSequence _points, final GeometryFactory _factory) {
    super(_points, _factory);
  }

  @Override
  protected LinearRing copyInternal() {
    return new GISPolygoneNiveau(points.copy(), factory);
  }

}
