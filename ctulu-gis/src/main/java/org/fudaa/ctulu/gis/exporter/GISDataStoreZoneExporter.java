/*
 * @creation 19 mai 2005
 *
 * @modification $Date: 2008-03-28 14:59:28 $
 *
 * @license GNU General Public License 2
 *
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis.exporter;

import com.memoire.fu.FuLog;
import gnu.trove.TObjectIntHashMap;
import org.fudaa.ctulu.CtuluActivity;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.gis.GISAttributeDouble;
import org.fudaa.ctulu.gis.GISDataModel;
import org.fudaa.ctulu.gis.GISDataModelToFeatureCollection;
import org.fudaa.ctulu.gis.GISZoneCollection;
import org.geotools.data.DataStore;
import org.geotools.data.FeatureWriter;
import org.geotools.data.Transaction;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;

import java.io.IOException;
import java.util.List;

/**
 * @author Fred Deniger
 * @version $Id: GISGMLZoneExporter.java,v 1.1.6.1 2008-03-28 14:59:28 bmarchan Exp $
 */
public class GISDataStoreZoneExporter implements CtuluActivity {
  boolean stop_;

  @Override
  public void stop() {
    stop_ = true;
  }

  public GISDataStoreZoneExporter() {
    super();
  }

  private boolean useIdAsName_;
  DataStore dataStore;

  protected FeatureWriter<SimpleFeatureType, SimpleFeature> createWriter(final SimpleFeatureType _type) throws IOException {
    dataStore.createSchema(_type);
    return dataStore.getFeatureWriter(dataStore.getTypeNames()[0], Transaction.AUTO_COMMIT);
  }

  /**
   * @param _prog la barre de progression
   * @param _zone la zone a exporter
   * @param _dest la destination
   */
  public void process(final ProgressionInterface _prog, final GISDataModel _zone, final GISAttributeDouble _attIsZ,
                      final DataStore _dest, Class commonGeomClass) {
    dataStore = _dest;
    process(_prog, _zone, _attIsZ, commonGeomClass);
  }

  public void processStore(final ProgressionInterface _prog, final GISDataModel _zone,
                           final DataStore _dest, Class commonGeomClass) {
    dataStore = _dest;
    process(_prog, _zone, null, _dest, commonGeomClass);
  }

  public void process(final ProgressionInterface _prog, final GISZoneCollection _zone, final DataStore _dest) {
    dataStore = _dest;
    _zone.prepareExport();
    process(_prog, _zone, _zone.getAttributeIsZ(), _zone.getDataStoreClass());
  }

  /**
   * Enregistre la zone sous un format GML. <p> Les attributs globaux sont enregistr�s comme attributs globaux de chaque g�om�trie, l'attribut pris
   * pour Z est enregistr� dans les coordonn�es Z des g�om�tries.<br> Les attributs atomiques sont enregistr�s sous forme de tableau de String,
   * s�par�s par une virgule.
   *
   * @param _prog Progression de la tache.
   * @param _zone La zone a enregistrer
   * @param _attIsZ L'attrribut pris pour Z (atomique ou non).
   */
  private void process(final ProgressionInterface _prog, final GISDataModel _zone, GISAttributeDouble _attIsZ, Class dataStoreClass) {
    /*
     * Pour que les z atomiques soient export�s correctement, il faut que prepareExport() ait �t� appel� en amont de cette
     * m�thode.
     */
    GISDataModelToFeatureCollection toFeatureCollection = new GISDataModelToFeatureCollection();
    stop_ = false;
    final TObjectIntHashMap<AttributeDescriptor> attIdx = new TObjectIntHashMap<>(_zone.getNbAttributes());
    if (stop_) {
      return;
    }
    final SimpleFeatureType featureType = toFeatureCollection.createAttributes(_zone, _attIsZ, attIdx, dataStoreClass);
    final List<AttributeDescriptor> atts = featureType.getAttributeDescriptors();
    try (FeatureWriter<SimpleFeatureType, SimpleFeature> writer = createWriter(featureType)) {
      if (stop_) {
        return;
      }
      final int nb = _zone.getNumGeometries();
      final ProgressionUpdater up = new ProgressionUpdater(_prog);
      up.setValue(9, nb, 10, 90);
      final int nbAttribute = atts.size();
      for (int i = 0; i < nb; i++) {
        if (stop_) {
          return;
        }
        final SimpleFeature feature = writer.next();
        feature.setDefaultGeometry(_zone.getGeometry(i));
        for (int j = 1; j < nbAttribute - 1; j++) {

          // Pour les attributs atomiques, les valeurs sont stock�es sous forme d'un tableau {a,b,c,...}
          if (_zone.getAttribute(attIdx.get(atts.get(j))).isAtomicValue()) {
            feature.setAttribute(j, GISDataModelToFeatureCollection.getValueAsString(_zone, attIdx, atts, i, j));
          } /// Atributs standards.
          else {
            feature.setAttribute(j, _zone.getValue(attIdx.get(atts.get(j)), i));
          }
        }

        // Enregistrement de l'index de la g�om�trie dans le fichier
        feature.setAttribute(nbAttribute - 1, Integer.valueOf(i));
        writer.write();

        up.majAvancement();
      }
    } catch (Exception ex) {
      FuLog.error(ex);
    }
  }

  /**
   * A utiliser pour des donn�es sensibles lorsque l'on veut que certains attributs soient retrouv� dans toutes les langues.
   *
   * @return true si on utilise l'ID de la variable pour construire l'attribut � inclure dans le fichier de sortie.
   */
  protected boolean isUseIdAsName() {
    return useIdAsName_;
  }

  public void setUseIdAsName(final boolean _useIdAsName) {
    useIdAsName_ = _useIdAsName;
  }
}
