/*
 * @creation 8 juin 2005
 * 
 * @modification $Date: 2007-04-16 16:33:52 $
 * 
 * @license GNU General Public License 2
 * 
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * 
 * @mail devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.ProgressionInterface;

/**
 * @author Fred Deniger
 * @version $Id: GISDataModelListPtAdapter.java,v 1.8 2007-04-16 16:33:52 deniger Exp $
 */
public class GISDataModelListPtAdapter implements GISDataModel {

  final GISPoint[] pt_;
  final GISAttributeInterface[] att_;
  final double[][] values_;

  /**
   * @param _pt
   * @param _att
   * @param _values
   */
  public GISDataModelListPtAdapter(final GISPoint[] _pt, final GISAttributeInterface[] _att, final double[][] _values) {
    super();
    pt_ = _pt;
    att_ = _att;
    values_ = _values;
  }

  @Override
  public GISDataModel createTranslate(GISPoint xyToAdd) {
    if (xyToAdd == null) {
      return this;
    }
    GISPoint[] newPt = new GISPoint[pt_.length];
    for (int i = 0; i < newPt.length; i++) {
      newPt[i] = new GISPoint(pt_[i].getX() + xyToAdd.getX(), pt_[i].getY() + xyToAdd.getY(), pt_[i].getZ());
    }
    return new GISDataModelListPtAdapter(newPt, att_, values_);
  }

  @Override
  public GISAttributeInterface getAttribute(final int _idxAtt) {
    return att_[_idxAtt];
  }

  @Override
  public void preload(final GISAttributeInterface[] _att, final ProgressionInterface _prog) {

  }

  @Override
  public Envelope getEnvelopeInternal() {
    return GISGeometryFactory.INSTANCE.createGeometryCollection(pt_).getEnvelopeInternal();
  }

  public Object[] getDataModelForGeom(final int _idxGeom) {
    final Object[] r = new Object[getNbAttributes()];
    for (int i = r.length - 1; i >= 0; i--) {
      r[i] = getValue(i, _idxGeom);
    }
    return r;
  }

  @Override
  public double getDoubleValue(final int _idxAtt, final int _idxGeom) {
    return values_[_idxAtt][_idxGeom];
  }

  @Override
  public Geometry getGeometry(final int _idxGeom) {
    return pt_[_idxGeom];
  }

  @Override
  public int getIndiceOf(final GISAttributeInterface _att) {
    return CtuluLibArray.findObjectEgalEgal(att_, _att);
  }

  @Override
  public int getNbAttributes() {
    return att_ == null ? 0 : att_.length;
  }

  @Override
  public int getNumGeometries() {
    return pt_.length;
  }

  @Override
  public Object getValue(final int _idxAtt, final int _idxGeom) {
    return CtuluLib.getDouble(getDoubleValue(_idxAtt, _idxGeom));
  }
}
