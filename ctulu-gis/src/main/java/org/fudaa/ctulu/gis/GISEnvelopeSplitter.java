/*
 *  @creation     13 avr. 2005
 *  @modification $Date: 2007-01-17 10:45:25 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *   @mail         devel@fudaa.fr
 */
package org.fudaa.ctulu.gis;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Fred Deniger
 * @version $Id: GISEnvelopeSplitter.java,v 1.7 2007-01-17 10:45:25 deniger Exp $
 */
public class GISEnvelopeSplitter {

  double maxX_;

  double maxY_;

  double minX_;

  double minY_;

  double prec_ = 1E-6;

  /**
   * @param _minX min x de l'envelop
   * @param _maxX max x de l'envelop
   * @param _minY min y de l'envelop
   * @param _maxY max y de l'envelope
   */
  public GISEnvelopeSplitter(final double _minX, final double _maxX, final double _minY, final double _maxY) {
    super();
    minX_ = _minX;
    maxX_ = _maxX;
    maxY_ = _maxY;
    minY_ = _minY;
  }

  public boolean isOnFrontier(final double _x, final double _y) {
    if (_x > (maxX_ + prec_) || _x < (minX_ - prec_)) {
      return false;
    }
    if (_y > (maxY_ + prec_) || _y < (minY_ - prec_)) {
      return false;
    }
    return (Math.abs(maxX_ - _x) < prec_) || Math.abs(minX_ - _x) < prec_ || Math.abs(maxY_ - _y) < prec_
        || Math.abs(minY_ - _y) < prec_;
  }

  public int getFrontier(final double _x, final double _y) {
    if (_x > (maxX_ + prec_) || _x < (minX_ - prec_)) {
      return -1;
    }
    if (_y > (maxY_ + prec_) || _y < (minY_ - prec_)) {
      return -1;
    }
    if (Math.abs(_y - minY_) <= prec_) {
      return 0;
    }
    if (Math.abs(_y - maxY_) <= prec_) {
      return 2;
    }
    if (Math.abs(_x - maxX_) <= prec_) {
      return 1;
    }
    if (Math.abs(_x - minX_) <= prec_) {
      return 3;
    }
    return -1;
  }

  private Coordinate getCoordinate(final int _i) {
    switch (_i) {
    case 0:
      return new Coordinate(minX_, minY_);
    case 1:
      return new Coordinate(maxX_, minY_);
    case 2:
      return new Coordinate(maxX_, maxY_);
    case 3:
      return new Coordinate(minX_, maxY_);
    default:
      return null;
    }
  }

  public Polygon split(final LineString _s) {
    final Point end = _s.getEndPoint();
    final Point start = _s.getStartPoint();
    final int endIdx = getFrontier(end.getX(), end.getY());
    final int startIdx = getFrontier(start.getX(), start.getY());
    // si les points extreme ne sont pas sur la frontiere
    if (endIdx < 0 || startIdx < 0) {
      return null;
    }
    if (_s.isClosed() || endIdx == startIdx) {
      return GISGeometryFactory.INSTANCE.createPolygon(GISGeometryFactory.INSTANCE.transformToLinearRing(_s), null);
    }
    final int minIdx = Math.min(endIdx, startIdx);
    final int maxIdx = Math.max(endIdx, startIdx);
    final List coords = new ArrayList();
    final boolean minIsEnd = minIdx == endIdx;
    Coordinate lastAdded = minIsEnd ? end.getCoordinate() : start.getCoordinate();
    coords.add(lastAdded);
    for (int i = minIdx; i < maxIdx; i++) {
      // le point a ajouter
      final Coordinate ci = getCoordinate((i + 1) % 4);
      // au cas ou les point d'intersection corresponde au points extremes
      if (ci.distance(lastAdded) > prec_) {
        lastAdded = ci;
        coords.add(lastAdded);
      }
    }
    if (minIsEnd) {
      // coords.add(start.getCoordinate());
      final  int nb = _s.getNumPoints();
      for (int i = 0; i < nb; i++) {
        coords.add(_s.getCoordinateSequence().getCoordinateCopy(i));
      }

    } else {
      // coords.add(end.getCoordinate());
      for (int i = _s.getNumPoints() - 1; i >= 0; i--) {
        coords.add(_s.getCoordinateSequence().getCoordinateCopy(i));
      }
    }
    if (coords.size() > 3) {
      final Coordinate[] csf = new Coordinate[coords.size()];
      coords.toArray(csf);
      return GISGeometryFactory.INSTANCE.createPolygon(GISGeometryFactory.INSTANCE.createLinearRing(csf), null);
    }
    return null;

  }
}
